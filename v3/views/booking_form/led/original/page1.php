<style>
@media print {
.no-print {display: none;}
body {background: transparent;}
.red{color:#c32027}
.black-bg {background-color:#010101}
.dark-bg{background-color:#282828}
.page-wrapper{ min-height:100%;}
}
 @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 150px;
        }
}
	table,tr,td{
		margin : auto !important;
	}
	td{
		padding-left : 5px !important;
	}
	
	.pdf_page{
		margin: 13px auto;
		box-shadow: 1px 1px 3px 1px #333;
		border-collapse: separate;
		width: 80%;
		padding:5px;
	}
</style>
<div class="page-wrapper d-block clearfix " >
<div style="padding:0; margin:0">
<form id="quick_quote_form">
<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
		<td align="left" valign="top"><img src="http://kugaroi.com.au/dev/admin/imagespdf2/cover1.jpg" alt="" style="display:block; width:100%;  height:444px; "></td>
	</tr>
      <tr>
		<td style="background:#232323 url('http://kugaroi.com.au/dev/admin/imagespdf2/cover2.jpg') center top;  background-size:cover; padding-top:0px; padding-bottom:45px; width:100%; height:837px; ">
		
		
			<table width="90%" border="0" align="center" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td width="70%" valign="top" align="left">&nbsp;</td>
						<td width="30%">&nbsp;</td>
					</tr>
					<tr valign="top" align="left">
						<td colspan="2"><h1 style="font-family: 'Montserrat', sans-serif; font-size:54px; color:#fff; padding:0; margin:0; text-transform:uppercase; font-weight:800;">LED Upgrade<br>Agreement</h1></td>
					</tr>
					<tr>
						<td valign="top" align="left">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					
					<!--<tr>
						<td width="70%" valign="top" align="left" style="font-family: 'Open Sans', sans-serif; font-size:14px; color:#fff;">Prepared for</td>
						<td width="30%">&nbsp;</td>
					</tr>-->
					<tr>
						<td style="font-family: 'Open Sans', sans-serif; font-size:19px; color:#fff;" width="70%" valign="top" align="left">KUGA Electrical<br><br></td>
						<td width="30%">&nbsp;</td>
					</tr>
					<tr>
						<td style="font-family: 'Open Sans', sans-serif; font-size:14px; color:#fff;" width="70%" valign="top" align="left">
						23 Lioel Road, Mount Waverley, VIC 3149<br>
						26 Prince William Drive, Seven Hills, NSW 2147
						</td>
						<td width="30%">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top" align="left">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="70%" valign="top" align="left">&nbsp;</td>
						<td width="30%">&nbsp;</td>
					</tr><tr>
    <td width="70%" align="left" valign="top" style="font-family: 'Open Sans', sans-serif; font-size:14px; color:#fff;">
     ABN 39 616 409 584<br>
	  VIC REC No. 27103<br>
      NSW REC No. 318148C 
  
  </td>
    <td width="30%">&nbsp;</td>
  </tr><tr>
						<td width="70%" valign="top" align="left">&nbsp;</td>
						<td width="30%">&nbsp;</td>
					</tr>
	
					<tr>
						<td style="font-family: 'Open Sans', sans-serif; font-size:14px; color:#fff;" width="80%" valign="top" align="left">
						
						<td width="20%" valign="top" align="right"><img src="http://kugaroi.com.au/dev/admin/imagespdf2/cover_img.jpg" alt="" title=""></td>
					</tr>
					
				
				</tbody>
			</table>
		
		</td>
	</tr>
    </table></td>
  </tr>
</table>
</div>


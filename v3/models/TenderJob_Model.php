<?php

class TenderJob_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->jobcrm_db = $this->load->database('jobcrm_db', TRUE);
        date_default_timezone_set('Australia/Sydney');
    }


    function num_rows($table, $cond) {
        $this->jobcrm_db->where($cond);
        return $this->jobcrm_db->get($table)->num_rows();
    }
    
    /**
     *
     * @param type $table
     * @param type $select
     * @param type $cond
     * @return type
     */
    function fetch_where($table, $select = '*', $cond = NULL) {
        $this->jobcrm_db->select($select);
        $this->jobcrm_db->from($table);
        if ($cond !== NULL) {
            $this->jobcrm_db->where($cond);
        }
        return $this->jobcrm_db->get()->result_array();
    }


     /**
     *
     * @param type $table
     * @param type $select
     * @param type $cond
     * @return type
     */
    function fetch_row($table, $select = '*', $cond = NULL) {
        $this->jobcrm_db->select($select);
        $this->jobcrm_db->from($table);
        if ($cond !== NULL) {
            $this->jobcrm_db->where($cond);
        }
        $result = $this->jobcrm_db->get()->row_array();
        return $result ? $result : array();
    }

    /**
     * insert a row data
     * @param type $table
     * @param type $value
     * @return boolean
     */
    function insert_data($table, $value) {
        $this->jobcrm_db->insert($table, $value);
        return $this->jobcrm_db->affected_rows();
    }

    /**
     * update data
     * @param type $table
     * @param type $value
     * @return boolean
     */
    function update_data($table, $cond, $data) {
        $this->jobcrm_db->where($cond);
        $this->jobcrm_db->update($table, $data);
        return $this->jobcrm_db->affected_rows();
    }
    
    /**
     * for do insert or update in single query
     * @param type $table
     * @param type $cond
     * @return boolean
     */
    function insert_or_update($table, $cond, $data) {
        $count = $this->num_rows($table, $cond);
        if ($count > 0) {
            return $this->update_data($table, $cond, $data);
        } else {
            return $this->insert_data($table, $data);
        }
    }
    
   
    function insert_or_update_customer($cust_id,$booking_form_id,$data) {

    	 try{
            $tender_data = $this->common->fetch_row('tbl_tender','*',array('id' => $cust_id));

            if(empty($tender_data)){
                return false;
            }

	    	//Start Transaction
	    	$this->jobcrm_db->trans_begin();

	    	//Perform Queries
            //Check if customer already present in job crm
            $job_crm_cond = array('isTender' => 1,'crm_cust_id' => $cust_id,'booking_form_id' => $booking_form_id);
            $this->jobcrm_db->select('*');
            $this->jobcrm_db->from('tbl_customers');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            $cust_data['isTender'] = 1;
            $cust_data['company_id'] = 1;
            $cust_data['crm_cust_id'] = $cust_id;
            $cust_data['booking_form_id'] = $booking_form_id;
            $cust_data['company_name'] = $data['business_details']['company_name'];
            $cust_data['first_name'] = $data['contacts']['projectManager']['first_name'];
            $cust_data['last_name'] = $data['contacts']['projectManager']['last_name'];
            $cust_data['customer_email'] = $data['contacts']['projectManager']['email'];
            $cust_data['customer_contact_no'] = $data['contacts']['projectManager']['contact_no'];
            $cust_data['customer_contact_no_1'] = $data['contacts']['projectManager']['landline'];
            $cust_data['position'] = $data['contacts']['projectManager']['position'];
            if(empty($result)){
                $cust_data['booking_form_id'] = $booking_form_id;
                $this->jobcrm_db->insert('tbl_customers', $cust_data);
                $job_crm_cust_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customers', $cust_data);
                $job_crm_cust_id = $result['id'];
            }

	        //Check Transaction
	        if ($this->jobcrm_db->trans_status() === FALSE) {
	            $this->jobcrm_db->trans_rollback();
	            return false;
	        } else {
	            $this->jobcrm_db->trans_commit();
	            return $job_crm_cust_id;
	        }
	    }catch(Exception $e){
	    	return false;
	    }
    }

    function insert_booking_form_additional_contacts($cust_id,$contacts) {
        try{
            $cust_data['cust_id'] = $cust_id;
            $this->jobcrm_db->where(array('cust_id'=>$cust_id));
            $this->jobcrm_db->delete('tbl_customer_contacts');
            foreach($contacts as $eachContact){
                if(count(array_filter($eachContact))>0) {
                    $cust_data['first_name'] = $eachContact['first_name'];
                    $cust_data['last_name'] = $eachContact['last_name'];
                    $cust_data['customer_email'] = $eachContact['email'];
                    $cust_data['customer_contact_no'] = $eachContact['contact_no'];
                    $cust_data['position'] = $eachContact['position'];
                    $this->jobcrm_db->insert('tbl_customer_contacts', $cust_data);
                }
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }



    function insert_or_update_customer_location($cust_id,$job_crm_cust_id,$booking_form_site_id) {
        try{
            $select = '*';

            if($booking_form_site_id != NULL){
                $siteData = $this->common->fetch_row('tbl_tender_sites',$select,array('tenderId'=>$cust_id,'id' => $booking_form_site_id));
            }



            if(empty($siteData)){
                return false;
            }

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            //Check if customer location already present in job crm
            $job_crm_cond = array('cust_id' => $job_crm_cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customer_locations');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();

            $cust_data['cust_id'] = $job_crm_cust_id;
            $cust_data['site_name'] = $siteData['name'];
            $cust_data['address'] = $siteData['address'];
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customer_locations', $cust_data);
                $job_crm_cust_loc_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customer_locations', $cust_data);
                $job_crm_cust_loc_id = $result['id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_loc_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_job($booking_form_id,$job_data) {
         try{

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            $job_crm_cond = array('booking_form_id' => $booking_form_id,'job_type' => $job_data['job_type']);
            $this->jobcrm_db->select('job_id');
            $this->jobcrm_db->from('tbl_jobs');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();


            if(empty($result)){
                $job_data['created_at'] = date('Y-m-d H:i:s');

                $this->jobcrm_db->insert('tbl_jobs', $job_data);
                $job_id = $this->jobcrm_db->insert_id();

                $this->jobcrm_db->insert('tbl_job_status',
                    array(
                        'status_id' => 1,
                        'sub_status_id' => 3,
                        'job_id' => $job_id,
                        'user_id' => 1
                    )
                );

            }else{
                unset($job_data['uuid']);
                $job_data['updated_at'] = date('Y-m-d H:i:s');
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_jobs', $job_data);
                $job_id = $result['job_id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_job_products($job_id,$job_type,$product_data) {
         try{
            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            $job_crm_cond = array('job_id' => $job_id);
            $this->jobcrm_db->select('id');
            $this->jobcrm_db->from('tbl_job_products');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            
            if(!empty($result)){
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->delete('tbl_job_products');
            }

            if (!empty($product_data)) {
                $pd = json_decode(json_encode($product_data),TRUE);
                $product_keys = array_keys($pd);
                for ($i = 0; $i < count($product_data->item_id); $i++) {
                    if($product_data->item_id[$i] != NULL && $product_data->item_id[$i] != '' && $product_data->item_name[$i]!=''){
                        //$crm_product_data = $this->common->fetch_row('tbl_led_new_products','type_id',array('np_id' => $product_data->item_id[$i]));
                        $insert_data = array();
                        $insert_data['job_id'] = $job_id;
                        $insert_data['crm_prd_id'] = $product_data->item_id[$i];
                        //$insert_data['crm_prd_type_id'] = $crm_product_data['type_id'];
						$insert_data['crm_prd_type_id'] = $product_data->type_id[$i];
						$insert_data['crm_prd_sensor'] = $product_data->item_sensor[$i];
						
                        $insert_data['name'] = $product_data->item_name[$i];
                        $insert_data['qty'] = $product_data->item_qty[$i];
                        $item_data = [];
                        foreach($product_keys as $k => $v){
                            $item_data[$v] = $product_data->$v[$i];
                        }
                        if(!empty($item_data)){
                            $insert_data['item_data'] = json_encode($item_data);
                        }
                        
                        $this->jobcrm_db->insert('tbl_job_products', $insert_data);
                    }
                }
            }
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    function insert_or_update_job_submission_docs($job_id,$images) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();
             $this->jobcrm_db->where(array('job_id' => $job_id));
             $this->jobcrm_db->delete('tbl_job_attachments');
            //Perform Queries
             $fileNames = array(
                 'ec_bill_1'=>'PHOTO_OF_FRONT_ELECTRICITY_BILL',
                 'ec_bill_2'=>'PHOTO_OF_BACK_ELECTRICITY_BILL',
                 'inverterGridProtection'=>'Inverter_Grid_Protection_Location',
                 'acRun'=>'AC_Run',
                 'connectionToSwitchBoard'=>'Connection_to_Switchboard',
                 'additionPhoto'=>'Additional_Photo',
                 'panelPalyout'=>'Panel_Layout',
                 'dcRun'=>'DC_Run',
                 );
            foreach($images as $key => $value){
                if($key=='more'){
                    $i=1;
                    foreach($images['more'] as $k=>$v) {
                        if (!empty($v)) {
                            $filename = strtoupper($v["name{$i}"]);
                            $ext = explode('.', $v["image{$i}"]);
                            $full_filename = $filename . '.' . $ext[1];
                            $job_crm_cond = array('job_id' => $job_id, 'file_name' => $full_filename);
                            $this->jobcrm_db->select('media_id');
                            $this->jobcrm_db->from('tbl_job_attachments');
                            $this->jobcrm_db->where($job_crm_cond);
                            $result = $this->jobcrm_db->get()->row_array();

                            $file = FCPATH . 'assets/uploads/tender_booking_form_files/' . $v["image{$i}"];
                            if (!file_exists($file)) {
                                $file = FCPATH . 'assets/uploads/tender_booking_form_files/' . $v["image{$i}"];
                            }
                            //$upload_path = 'D:\xampp\htdocs\job_crm/assets/uploads/media_gallery/'.$job_id;

                            $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/media_gallery/' . $job_id;
                            if (ENVIRONMENT == 'development'){
                                $upload_path = '/home/kugacrm/public_html/job_crm/staging/assets/uploads/media_gallery/' . $job_id;
                            }

                            if (!is_dir($upload_path)) {
                                mkdir($upload_path, 0777, TRUE);
                            }
                            if (!is_dir($upload_path . '/large')) {
                                mkdir($upload_path . '/large', 0777, TRUE);
                            }
                            if (!is_dir($upload_path . '/medium')) {
                                mkdir($upload_path . '/medium', 0777, TRUE);
                            }
                            if (!is_dir($upload_path . '/small')) {
                                mkdir($upload_path . '/small', 0777, TRUE);
                            }

                            $newfilename = $upload_path . '/large/' . $full_filename;

                            if (file_exists($newfilename)) {
                                unlink($newfilename);
                            }

                            copy($file, $newfilename);

                            //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                            $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/media_gallery/' . $job_id . '/';
                            if (ENVIRONMENT == 'development'){
                                $base_url = 'https://kugacrm.com.au/job_crm/staging/assets/uploads/media_gallery/' . $job_id . '/';
                            }

                            if (empty($result)) {
                                $insert_data = array(
                                    'base_url' => $base_url,
                                    'original_name' => $v["image{$i}"],
                                    'file_name' => $full_filename,
                                    'file_path' => $job_id,
                                    'file_size' => 0,
                                    'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                                    'module' => '',
                                    'job_id' => $job_id,
                                    'media_cat_id' => 12
                                );
                                $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                            } else {
                                $update_data = array(
                                    'base_url' => $base_url,
                                    'original_name' => $v["image{$i}"],
                                    'file_name' => $full_filename,
                                    'file_path' => $job_id,
                                    'file_size' => 0,
                                    'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                                    'module' => '',
                                    'job_id' => $job_id,
                                    'media_cat_id' => 12
                                );
                                $this->update_data('tbl_job_attachments', array('media_id' => $result['media_id']), $update_data);
                            }
                        }
                           if($i==2){
                               break;
                           }
                        $i++;
                    }
                } else {
                    if ($value != '') {
                        $filename = strtoupper($fileNames[$key]);
                        $ext = explode('.', $value);
                        $full_filename = $filename . '.' . $ext[1];
                        $job_crm_cond = array('job_id' => $job_id, 'file_name' => $full_filename);
                        $this->jobcrm_db->select('media_id');
                        $this->jobcrm_db->from('tbl_job_attachments');
                        $this->jobcrm_db->where($job_crm_cond);
                        $result = $this->jobcrm_db->get()->row_array();

                        $file = FCPATH . 'assets/uploads/tender_booking_form_files/' . $value;
                        if (!file_exists($file)) {
                            $file = FCPATH . 'assets/uploads/tender_booking_form_files/' . $value;
                        }
                        //$upload_path = 'D:\xampp\htdocs\job_crm/assets/uploads/media_gallery/'.$job_id;

                        $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/media_gallery/' . $job_id;
                        if (ENVIRONMENT == 'development'){
                            $upload_path = '/home/kugacrm/public_html/job_crm/staging/assets/uploads/media_gallery/' . $job_id;
                        }

                        if (!is_dir($upload_path)) {
                            mkdir($upload_path, 0777, TRUE);
                        }
                        if (!is_dir($upload_path . '/large')) {
                            mkdir($upload_path . '/large', 0777, TRUE);
                        }
                        if (!is_dir($upload_path . '/medium')) {
                            mkdir($upload_path . '/medium', 0777, TRUE);
                        }
                        if (!is_dir($upload_path . '/small')) {
                            mkdir($upload_path . '/small', 0777, TRUE);
                        }

                        $newfilename = $upload_path . '/large/' . $full_filename;

                        if (file_exists($newfilename)) {
                            unlink($newfilename);
                        }

                        copy($file, $newfilename);

                        //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                        //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                        $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/media_gallery/' . $job_id . '/';
                        if (ENVIRONMENT == 'development'){
                            $base_url = 'https://kugacrm.com.au/job_crm/staging/assets/uploads/media_gallery/' . $job_id . '/';
                        }

                        if (empty($result)) {
                            $insert_data = array(
                                'base_url' => $base_url,
                                'original_name' => $value,
                                'file_name' => $full_filename,
                                'file_path' => $job_id,
                                'file_size' => 0,
                                'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                                'module' => '',
                                'job_id' => $job_id,
                                'media_cat_id' => 12
                            );
                            $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                        } else {
                            $update_data = array(
                                'base_url' => $base_url,
                                'original_name' => $value,
                                'file_name' => $full_filename,
                                'file_path' => $job_id,
                                'file_size' => 0,
                                'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                                'module' => '',
                                'job_id' => $job_id,
                                'media_cat_id' => 12
                            );
                            $this->update_data('tbl_job_attachments', array('media_id' => $result['media_id']), $update_data);
                        }
                    }
                }
            }
            
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    
    function insert_or_update_heers_job_submission_docs($job_id,$images) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            foreach($images as $key => $value){
                if(($key == 'electricity_bill_front' || $key == 'electricity_bill_back') && $value != ''){
                    $filename = ($key == 'electricity_bill_front') ? 'PHOTO_OF_FRONT_ELECTRICITY_BILL' : 'PHOTO_OF_BACK_ELECTRICITY_BILL';
                    $ext = explode('.',$value);
                    $full_filename = $filename . '.' . $ext[1];
                    $job_crm_cond = array('job_id' => $job_id,'file_name' => $full_filename);
                    $this->jobcrm_db->select('media_id');
                    $this->jobcrm_db->from('tbl_job_attachments');
                    $this->jobcrm_db->where($job_crm_cond);
                    $result = $this->jobcrm_db->get()->row_array();
                    
                    $file = FCPATH . 'assets/uploads/led_booking_form_files/original/' . $value;
                    if (!file_exists($file)) {
                        $file = FCPATH . 'assets/uploads/led_booking_form_files/' . $value;
                    }
                    //$upload_path = 'D:\xampp\htdocs\job_crm/assets/uploads/media_gallery/'.$job_id;

                    $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/media_gallery/'.$job_id;

                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/large')) {
                       mkdir($upload_path.'/large', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/medium')) {
                        mkdir($upload_path.'/medium', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/small')) {
                       mkdir( $upload_path.'/small', 0777, TRUE);
                    }

                    $newfilename = $upload_path . '/large/' . $full_filename;

                    if(file_exists($newfilename)){
                        unlink($newfilename);
                    }

                    copy($file, $newfilename); 

                    //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                    $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/media_gallery/'.$job_id .'/';

                    if(empty($result)){
                        $insert_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                    }else{
                         $update_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->update_data('tbl_job_attachments', array('media_id' => $result['media_id']),$update_data);
                    }
                }
            }
            
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }

        function insert_booking_form_activity_note($job_id,$job_data) {
            $booking_form_url = $job_data['booking_form_file'];
            $booking_form_data = $job_data['booking_form_data'];
            $booking_form_data = json_decode($booking_form_data);
            $sales_repname = '';
            if($booking_form_data != '' && $booking_form_data != NULL){
                if(isset($booking_form_data->authorised_by_behalf) && $booking_form_data->authorised_by_behalf != ''){
                    $sales_repname = $booking_form_data->authorised_by_behalf->name;
                }
            }
            try{    
                $activity_data = array();
                $activity_data['activity_type'] = 'Booking Form';
                $activity_data['activity_title'] = $sales_repname . ' ' . date("F d, Y");
                $activity_note  = "Booking Form:<br/> -  <a target='__blank' href='".$booking_form_url."'>".$booking_form_url."</a><br/><br/>";
                $activity_data['activity_note'] = $activity_note;
                $activity_data['job_id'] = $job_id;
                $activity_data['user_id'] = 1;
                $activity_data['status'] = 1;
                $activity_data['created_at'] = date('Y-m-d H:i:s');
                $this->jobcrm_db->insert('tbl_activities', $activity_data);
            }catch(Exception $e){
                return false;
            }

        }



    function get_products_for_po($select,$custom_cond) {
        $sql = "SELECT tp.*,$select,tp.id as item_id FROM tbl_products as tp";
        $sql .= " LEFT JOIN tbl_product_types as pt ON pt.type_id=tp.type_id";
        if ($custom_cond) {
            $sql .= $custom_cond;
        }
       
        $query = $this->jobcrm_db->query($sql);
        return $query->result_array();
    }
    
    
    
    /** Warranty job realted Functions */

    function insert_or_update_warranty_customer($cust_id,$cust_data) {
        try{
            $select = 'id,simpro_cust_id,title,company_name,position,first_name,last_name,customer_email,customer_contact_no,is_processed';
        
            //Start Transaction
            $this->jobcrm_db->trans_begin();
        
            //Perform Queries
            //Check if customer already present in job crm against simpro cust id fields
            $job_crm_cond = array('company_id' => 1,'web_cust_id' => $cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customers');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            $cust_data['company_id'] = 1;
            $cust_data['web_cust_id'] = $cust_id;
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customers', $cust_data);
                $job_crm_cust_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customers', $cust_data);
                $job_crm_cust_id = $result['id'];
            }
        
                    //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_customer_location($location_data,$job_crm_cust_id) {
        try{
            $select = '*';
        
            //Start Transaction
            $this->jobcrm_db->trans_begin();
        
            //Perform Queries
            //Check if customer location already present in job crm
            $job_crm_cond = array('cust_id' => $job_crm_cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customer_locations');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            $location_data['cust_id'] = $job_crm_cust_id;
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customer_locations', $location_data);
                $job_crm_cust_loc_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customer_locations', $location_data);
                $job_crm_cust_loc_id = $result['id'];
            }
        
                    //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_loc_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_job($cust_id,$job_data) {
        try{

            $this->jobcrm_db->trans_begin();
        
            $job_crm_cond = array('cust_id' => $cust_id,'job_type' => $job_data['job_type'],'cost_centre_id' => 6);
            $this->jobcrm_db->select('job_id');
            $this->jobcrm_db->from('tbl_jobs');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_jobs', $job_data);
                $job_id = $this->jobcrm_db->insert_id();
        
                $this->jobcrm_db->insert('tbl_job_status', 
                    array(
                        'status_id' => 1,
                        'sub_status_id' => 3,
                        'job_id' => $job_id,
                        'user_id' => 1
                    )
                );
        
            }else{
                unset($job_data['uuid']);
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_jobs', $job_data);
                $job_id = $result['job_id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_job_docs($job_id,$images) {
       try{
    
            $this->jobcrm_db->trans_begin();
    
            foreach($images as $key => $value){
                if($value['file_name'] != '' && $value['file_name'] != NULL){
        
                    $media_cat_id = '';
                    if($value['name'] == 'Faulty Light'){
                        $media_cat_id = 15;
                    }else if($value['name'] == 'Model No and Wattage of Fitting'){
                        $media_cat_id = 16;
                    }else if($value['name'] == 'Certificate of Electrical Safety'){
                        $media_cat_id = 1;
                    }else if($value['name'] == 'Invoice'){
                        $media_cat_id = 8;
                    }
        
                    $filename = $value['file_name'];
                    $ext = explode('.',$filename);
                    $base_url = 'https://kugacrm.com.au/assets/uploads/warranty_claim_files/';
        
                    $insert_data = array(
                        'base_url' => $base_url,
                        'original_name' => $value['file_name'],
                        'file_name' => $value['file_name'],
                        'file_path' => $job_id,
                        'file_size' => 0,
                        'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                        'module' => '',
                        'job_id' => $job_id,
                        'media_cat_id' => $media_cat_id
                    );
                    $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                }
            }
    
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    function insert_or_update_job_crm_user($user_id,$job_crm_password) {
        try{

            $this->jobcrm_db->trans_begin();
    
            $aauth_user_data = $this->common->fetch_row('aauth_users','*',array('id' => $user_id));
    
            if(!empty($aauth_user_data)){
                
                //Check if User Exitis with same email on Job CRM
                $this->jobcrm_db->select('id');
                $this->jobcrm_db->from('aauth_users');
                $this->jobcrm_db->where(array('email' => $aauth_user_data['email']));
                $result = $this->jobcrm_db->get()->num_rows();
                
                if($result > 0){
                    return 'Email Exists';
                }
                    
                $user_details = $this->common->fetch_row('tbl_user_details','*',array('user_id' => $user_id));
                $user_locations = $this->common->fetch_row('tbl_user_locations','*',array('user_id' => $user_id));
                $job_crm_user_id = '';
    
                if($user_details['job_crm_user_id'] == NULL || $user_details['job_crm_user_id'] == ''){
                    //Create Aauth Table Data
                    unset($aauth_user_data['id']);
                    unset($aauth_user_data['banned']);
                    $this->jobcrm_db->insert('aauth_users',$aauth_user_data);
                    $job_crm_user_id = $this->jobcrm_db->insert_id();
    
                    $this->load->library("Aauth");
                    $job_crm_password = $this->aauth->hash_password($job_crm_password,$job_crm_user_id);
                    $this->jobcrm_db->where(array('id' => $job_crm_user_id));
                    $this->jobcrm_db->update('aauth_users',array('pass' => $job_crm_password));
                    
                    //Create User Details Table Data
                    unset($user_details['id']);
                    unset($user_details['user_id']);
                    unset($user_details['job_crm_user_id']);
                    $user_details['user_id'] = $job_crm_user_id;
                    $user_details['kuga_crm_user_id'] = $user_id;
                    $full_name = $user_details['full_name'];
                    $full_name = explode(' ',$full_name);
                    $user_details['first_name'] = (count($full_name) > 0) ? $full_name[0] : '';
                    $user_details['last_name'] = (count($full_name) > 1) ? $full_name[1] : '';
                    $this->jobcrm_db->insert('tbl_user_details', $user_details);
    
                    //Create User Locations Table Data
                    unset($user_locations['id']);
                    unset($user_locations['user_id']);
                    $user_locations['user_id'] = $job_crm_user_id;
                    $this->jobcrm_db->insert('tbl_user_locations', $user_locations);
    
                    //Create User Group
                    $this->jobcrm_db->insert('aauth_user_to_group',array('group_id' => 10, 'user_id' => $job_crm_user_id));
    
                    $this->common->update_data('tbl_user_details',array('user_id' => $user_id),array('job_crm_user_id' => $job_crm_user_id));
                }else{
                    $job_crm_user_id = $user_details['job_crm_user_id'];
                    
                    //Create User Details Table Data
                    unset($user_details['id']);
                    unset($user_details['user_id']);
                    $this->jobcrm_db->where(array('user_id' => $job_crm_user_id));
                    $this->jobcrm_db->update('tbl_user_details', $user_details);
    
                    //Create User Locations Table Data
                    unset($user_locations['id']);
                    unset($user_locations['user_id']);
                    $this->jobcrm_db->where(array('user_id' => $job_crm_user_id));
                    $this->jobcrm_db->insert('tbl_user_locations', $user_locations);
                }
    
                if ($this->jobcrm_db->trans_status() === FALSE) {
                    $this->jobcrm_db->trans_rollback();
                    return false;
                } else {
                    $this->jobcrm_db->trans_commit();
                    return $job_crm_user_id;
                }
            }
            return false;
        }catch(Exception $e){
            return false;
        }
    }
    
    public function get_job_status($job_id,$cond = FALSE){
        $sql = "SELECT js.*,stl.name as status_name,sstl.name as sub_status_name FROM tbl_job_status as js";
        $sql .= " LEFT JOIN tbl_status_list as stl ON stl.sid = js.status_id";
        $sql .= " LEFT JOIN tbl_status_list as sstl ON sstl.sid = js.sub_status_id";
        $sql .= " WHERE js.job_id=". $job_id . " $cond ORDER BY js.id DESC";
        $query = $this->jobcrm_db->query($sql);
        return $query->row_array();
    }
    
    
    function insert_or_update_solar_bill($job_id,$images,$electricity_bill) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            foreach($images as $key => $value){
                if(($key == 'ec_bill_1' || $key == 'ec_bill_2') && $value != ''){
                    $filename = ($key == 'ec_bill_1') ? 'PHOTO_OF_ELECTRICITY_BILL_1' : 'PHOTO_OF_ELECTRICITY_BILL_2';
                    $ext = explode('.',$value);
                    $full_filename = $filename . '.' . $ext[1];
                    $job_crm_cond = array('job_id' => $job_id,'file_name' => $full_filename);
                    $this->jobcrm_db->select('media_id');
                    $this->jobcrm_db->from('tbl_job_solar_attachments');
                    $this->jobcrm_db->where($job_crm_cond);
                    $result = $this->jobcrm_db->get()->row_array();
                    
                    $file = FCPATH . 'assets/uploads/solar_booking_form_files/original/' . $value;
                    if (!file_exists($file)) {
                        $file = FCPATH . 'assets/uploads/solar_booking_form_files/' . $value;
                    }

                    $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_media_gallery/'.$job_id;

                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/large')) {
                       mkdir($upload_path.'/large', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/medium')) {
                        mkdir($upload_path.'/medium', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/small')) {
                       mkdir( $upload_path.'/small', 0777, TRUE);
                    }

                    $newfilename = $upload_path . '/large/' . $full_filename;

                    if(file_exists($newfilename)){
                        unlink($newfilename);
                    }

                    copy($file, $newfilename); 

                    //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                    $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/solar_media_gallery/'.$job_id .'/';

                    if(empty($result)){
                        $insert_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 4
                        );
                        $this->jobcrm_db->insert('tbl_job_solar_attachments', $insert_data);
                    }else{
                         $update_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 4
                        );
                        $this->update_data('tbl_job_solar_attachments', array('media_id' => $result['media_id']),$update_data);
                    }
                }
            }
            
            if(isset($electricity_bill) && isset($electricity_bill->eb_nmi)){
                $cid = 4;
                $key = $this->input->get('key');
                $value = $this->input->get('value');
                $user_id = 1;
                $cdata = json_encode(array('nmi' => $electricity_bill->eb_nmi, 'meter_no' => $electricity_bill->eb_meter_no));
                $scs_data = $this->fetch_row('tbl_job_solar_attachment_category_status','id',array('cat_id' => $cid, 'job_id' => $job_id));
                if(empty($scs_data)){
                    $insert_data = array();
                    $insert_data['cat_id'] = $cid;
                    $insert_data['job_id'] = $job_id;
                    $insert_data['user_id'] = 1;
                    $insert_data['data'] = $cdata;
                    $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
                }else{
                    $update_data = array();
                    $update_data['data'] = $cdata;
                    $this->update_data('tbl_job_solar_attachment_category_status',array('job_id' => $job_id,'cat_id' => $cid),$update_data);
                }
            }
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }

}

<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td height="35" class="red" style="color:#c32027; font-size:16px; font-family:Arial, Helvetica, sans-serif; padding-left:40px;">
            <strong>Key points of our Terms and Conditions</strong>
        </td>
    </tr>
    <tr>
        <td height="35" style="padding-top:0px; padding-bottom:10px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:40px;">
            We'd like to highlight to you some key clauses of our Terms and Conditions, which are laid out in full to you on the following pages. 
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input required="" id="terms_and_conditions_0" name="booking_form[terms_and_conditions][0]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 1 - Payment</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        6.4.2 Post Installation, where a payment for Goods and Services purchased by the Buyer under this agreement, to Kuga Electrical is denied by 
                        the external Finance provider/payment plan provider, on the basis of any issue with the buyer (including but not limited to bad credit rating, 
                        non-payment of repayment amounts) that is not in Kuga Electrical's control, Kuga Electrical will have the right to claim the amount from the  
                        Buyer directly.<br/><br/>
                        6.4.3 In case where Kuga Electrical is denied payments as mentioned in 6.4.2 above and where Kuga Electrical is unable to recover the amount for 
                        Goods and Services purchased by the Buyer under this agreement, the Buyer acknowledges that Kuga Electrical will be entitled to repossession 
                        of the Goods supplied under this agreement. In such events, the Buyer acknowledges that all costs directly attributable to repossession of such 
                        Goods including but not limited to dismantling costs, installers charges, will be borne by the Buyer.
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
    <!--<tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input required="" id="terms_and_conditions_1" name="booking_form[terms_and_conditions][1]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 2 - STC Incentive</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        7.4 You hereby assign to us all of your existing and future rights, title and interest in and to all STCs created or able to be created in respect 
                        of the system.<br/><br/>
                        7.5 You must do anything we reasonably request of you for the purpose of perfecting, confirming or evidencing this assignment, including 
                        providing information and executing documents.
                    </td> 
                </tr>
            </table>
        </td>
    </tr> -->

    <tr>
        <td height="50">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="415" valign="top" style="padding-right:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="405" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333">ALL OF THE ABOBE HAS BEEN EXPLAINED AND I ACCEPT THE CHARGES AS LISTED. I HAVE READ AND UNDERSTOOD THE FULL TERMS AND CONDITIONS ON THE FOOLLOWING PAGES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="23" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised on behalf of:</td>
                            </tr>
                            <tr>
                                <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                    <input  id="authorised_on_behalf_company_name" name="booking_form[authorised_on_behalf][company_name]" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_name" name="booking_form[authorised_on_behalf][name]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_position" name="booking_form[authorised_on_behalf][position]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_date" name="booking_form[authorised_on_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">CLIENT SIGNATURE:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                            <td align="center" style=" padding:5px; height: 180px; ">
                                                <div class="signature-pad">
                                                    <div class="signature-pad--body">
                                                        <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_1" data-id="1" class="sign_create"></div>
                                                        <input required="" id="authorised_on_behalf_signature" name="booking_form[authorised_on_behalf][signature]" type="hidden" />
                                                    </div>
                                                </div>
                                                <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                    <img src="" />
                                                    <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> 
                    </td>
                    <td width="415" valign="top" style="padding-left:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333">BY SIGNING BELOW YOU ACKNOWLEGE THAT THE CUSTOMER IS AWARE OF THE PROSSIBLE EXTRA CHARGES. INCLUDING ALL ACCESS EQUIPMENT CHARGES AND RE-SCHEDULING FEES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="35" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="116" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of:</td>
                                            <td width="289" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">Kuga Electrical</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <select id="authorised_by_behalf_name" name="booking_form[authorised_by_behalf][name]" style="padding:5px; width:100%; height: inherit;" required="" >
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    if (!empty($users)) {
                                                        foreach ($users as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value['full_name']; ?>" ><?php echo $value['full_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_by_behalf_position" name="booking_form[authorised_by_behalf][position]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_by_behalf_date" name="booking_form[authorised_by_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">SIGNATURE:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                            <td align="center" style=" padding:5px; height: 180px; ">
                                                <div class="signature-pad">
                                                    <div class="signature-pad--body">
                                                        <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_2" data-id="2" class="sign_create"></div>
                                                         <input required="" id="authorised_by_behalf_sales_rep_signature" name="booking_form[authorised_by_behalf][sales_rep_signature]" type="hidden" />
                                                    </div>
                                                </div>
                                                <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                    <img src="" />
                                                    <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table></td>
    </tr>
    
    <tr>
        <td height="50">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>

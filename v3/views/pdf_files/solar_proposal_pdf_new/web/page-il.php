<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page-il-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 1287px !important;
            max-height: 910px !important;
            width: 1287px !important;
            height: 910px !important;
        }

        .page-il .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 1044px;
            padding-top: 30px;
        }

        .page-il .top hr {
            width: 76%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-il .text_color {
            color: #D01E2A;
        }

        .page-il .title {
            margin-top: -50px;
        }

        .page-il .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page-il .footer {
            margin-top: -3px;
            display: inline-flex;
        }

        .page-il .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-il .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }

        .page-il .ingBox {
            display: inline-flex;
            padding-top: 100px;
            padding-bottom: 188px;
        }

        .page-il .iiimage {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 650px;
            margin-left: 48px;
        }

        .page-il .iibox {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 500px;
            margin-left: 48px;
        }

        .page-il .fimagee img {
            margin-left: 1120px;
        }
    </style>
</head>

<?php
$inverter_design['snapshot'] = isset($inverter_design['snapshot']) && $inverter_design['snapshot'] != NULL ? $inverter_design['snapshot'] : $proposal_data['inverter_image'];
$inverter_design['notes'] = isset($inverter_design['notes']) && $inverter_design['notes'] != NULL ? $inverter_design['notes'] : '';
?>

<body>
    <div class="page-il-back-bg back-bg page-il">
        <div class="top">
            <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>YOUR SOLAR <span class="text_color"> INVERTER LAYOUT</span></h1>
        </div>
        <div class="ingBox">
            <div class="iiimage">
                <div class="add-picture1" data-src="<?php echo !empty($inverter_design) ? $inverter_design['snapshot'] : ''; ?>" style="background-image: url('<?php echo !empty($inverter_design) ? $inverter_design['snapshot'] : ''; ?>'); 
                           background-repeat: no-repeat; background-position: 50% 50%; 
                           background-size: 100% 100%; 
                           width: 650px;
                           height: 400px; ">
                </div>
            </div>
            <div class="iibox" style="padding-right: 20px;
    padding-left: 20px;">
                <a href="javascript:void(0);" class="btn" id="tool_inverter_design_notes" style="margin-top: -36px;padding-left: 92%;"><i class="fa fa-edit"></i> Edit</a>
                <span id="inverter_notes_html"><?php echo !empty($inverter_design) ? $inverter_design['notes'] : ''; ?></span>
            </div>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>


    <div class="modal fade" id="inverter_notes_modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title pb-1">Notes for Inverter Design</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="form-block d-block clearfix">
                            <textarea id="inverter_notes"><?php echo !empty($inverter_design) ? $inverter_design['notes'] : ''; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save_inverter_notes_btn">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var inverter_notes_html = $('#inverter_notes_html').html();

        $('#tool_inverter_design_notes').on('click', function() {
            CKEDITOR.replace(
                document.querySelector('#inverter_notes'), {
                    toolbar: [{
                            name: 'basicstyles',
                            groups: ['basicstyles', 'cleanup'],
                            items: ['Bold', 'Italic', 'Underline']
                        },
                        {
                            name: 'paragraph',
                            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                            items: ['NumberedList', 'BulletedList', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                        },
                        {
                            name: 'styles',
                            items: ['Styles', 'Format', 'Font', 'FontSize']
                        },
                    ]
                });
            $('#inverter_notes_modal').modal('show');
        });

        $(document).on('click', '#save_inverter_notes_btn', function() {
            var editor_data = CKEDITOR.instances.inverter_notes.getData();
            var data = {};
            data.key = 'notes';
            data.value = editor_data;
            data.proposal_id = '<?php echo $proposal_data["id"]; ?>';
            data.lead_id = '<?php echo $proposal_data["lead_id"]; ?>';
            data.site_id = '<?php echo $proposal_data["site_id"]; ?>';

            $.ajax({
                url: base_url + 'admin/inverter-tool/save_design_tool_data',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(data) {
                    toastr['success'](data.status);
                    $('#inverter_notes_html').html(editor_data);
                    $('#inverter_notes_modal').modal('hide');
                },
                error: function(data) {
                    toastr['success']('Data Saving Failed');
                },
            });
        });
    </script>
</body>

</html>
<style>
    .tile {
        width: 100%;
        display: inline-block;
        box-sizing: border-box;
        background: #fff;
        padding: 20px;
        margin-bottom: 10px;
        border-radius: 5px;
    }
    .tile .first {
        width: 80%;
        float: left;
    }
    .tile .second {
        width: 20%;
        float: left;
        text-align: center;
    }
    .tile .title {
        margin-top: 0px;
    }
    .tile.purple, .tile.blue, .tile.red, .tile.orange, .tile.green {
        color: #fff !important;
    }
    .tile.purple {
        background: #5133ab;
    }
    .tile.purple:hover {
        background: #3e2784;
    }
    .tile.red {
        background: #ac193d;
    }
    .tile.red:hover {
        background: #7f132d;
    }
    .tile.green {
        background: #00a600;
    }
    .tile.green:hover {
        background: #007300;
    }
    .tile.blue {
        background: #2672ec;
    }
    .tile.blue:hover {
        background: #125acd;
    }
    .tile.orange {
        background: #dc572e;
    }
    .tile.orange:hover {
        background: #b8431f;
    }

</style>
<div class="page-wrapper d-block clearfix">
    <div class="row">
            <div class="col-sm-3">
                <a href="<?php echo site_url('admin/booking_form/add_solar_booking_form?deal_ref='.$deal_ref.'&site_ref='.$site_ref.'&type_ref=1&cost_centre_id='.$cost_centre_id); ?>" class="tile purple">
                    <div class="first">
                        <h3 class="title">0 to 39KW </h3>
                        <small>Create 0 to 39KW Com Solar Sales Agreement</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-area-chart fa-3x"></i>
                    </div>
                </a>
            </div>

            <div class="col-sm-3">
                <a href="<?php echo site_url('admin/booking_form/add_solar_booking_form?deal_ref='.$deal_ref.'&site_ref='.$site_ref.'&type_ref=2&cost_centre_id='.$cost_centre_id); ?>" class="tile orange">
                    <div class="first">
                        <h3 class="title">40kW to 100KW</h3>
                        <small>Create 40kW to 100KW Com Solar Sales Agreement</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-area-chart fa-3x"></i>
                    </div>
                </a>
            </div>
        
            <div class="col-sm-3">
                <a href="<?php echo site_url('admin/booking_form/add_solar_booking_form?deal_ref='.$deal_ref.'&site_ref='.$site_ref.'&type_ref=3&cost_centre_id='.$cost_centre_id); ?>" class="tile red">
                    <div class="first">
                        <h3 class="title">100KW+</h3>
                        <small>Create 100KW and above Com Solar Sales Agreement</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-area-chart fa-3x"></i>
                    </div>
                </a>
            </div>
    </div>
</div>

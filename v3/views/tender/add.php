<style>
	.page-wrapper{background-color:#ECEFF1; min-height:100%;}
		.table-default table tr td{
			padding: 5px 5px 5px 5px !important;
		}
		.select2-container{
			height:inherit !important;
			padding:inherit !important;
		}
		.select2{
			display: none;
		}
		#locationstreetAddressVal{
			display: none;
		}

		form .form-control{
			border: 1px solid #A9A9A9;
			color:#000;
		}

		form .kg-form_field--orange{
			border: 1px solid orange;
		}
		i.fa-pencil,i.fa-forward,i.fa-backward{
			color: #DB1E30;
		}
		i.fa-pencil:hover{
			cursor: pointer;
		}
		input[type="file"] {
			display: none;
		}
		.photoZoom img {
			transition: 0.5s all ease-in-out;
		}
        a.addEffect:hover div{
            opacity: 1 !important;
        }
    a.addEffect:hover img{
        opacity: .3 !important;
    }
    a.addEffect div{
        opacity: 0 !important;
    }


		
</style>	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/commercial.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/style1.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
  <style>

    .am-card__body{
        display: inline-flex;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
    }
    .is-invalid{
        border: 1px solid #dc3545;
    }
    .am-scheduler{
        display:block;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
        padding: 10px;
        padding-bottom: 0px;
        width:100%;
    }

    .am-attendees{
        display:block;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
        margin-top: 10px !important;
        padding: 10px;
        padding-bottom: 0px;
        width: 100%;
    }

    .am-scheduler__item{
        width:100%;
    }

    @media only screen and (min-width: 992px){
        .modal-content{width:700px;}
    }

    @media only screen and (max-width: 991px){
        /**.modal-content{width:360px;}*/
    }

    @media only screen and (max-width: 764px){
        .am-scheduler{
            display:block;
            width: 100%;
            background-color: #ECEFF1;
            margin-bottom: 0px !important;
            padding: 10px;
            padding-bottom: 0px;
        }
        .am-scheduler__item{
            width:100%;
        }
        #scheduled_time,#scheduled_duration{margin-left:0px;}
    }
    .form-block{margin-bottom: 0px !important;}
    .mark_as_completed{
        position: absolute;
        margin: 10px;
        left: 0;
    }
</style>

<div class="page-wrapper d-block clearfix right-part">
			<!-- <div class="top-bar">
				<div class="row">
					<div class="col-lg-2 col-md-12 col-12">
						<p> <?php echo ucfirst($data['name']) ; ?> </p>
					</div>
				</div>
			</div> -->

			<ul class="fixed-menu">
				<li><a id='add_activity' href="#" title="Add Activity"><img src="<?php echo site_url(); ?>assets/images/icon17.svg" alt="icon"> </a></li>
				<li><a href="#" id="schedule_activity" title="Schedule Activity"><img src="<?php echo site_url(); ?>assets/images/icon16.svg" alt="icon"></a></li>
				<li><a href="#" id="booking_form" title="Booking Form"><img src="<?php echo site_url(); ?>assets/images/icon15.svg" alt="icon"></a></li>
			</ul>

			<div class="btngroup box">
				<ul> 
					<?php 
				
					foreach($statusTypes as $id=>$type) {
					echo "<li><a href='javascript:void(0);' data-statusId='{$id}' class='updateStatus ".(($id == $data['status'])?'active':'')."'>{$type} </a> </li>";
				} ?>
				</ul>
			</div>

			<div class="tendor-form box"> 
			<div id="error_message"></div>
                <div class="row">
                        <div class="text-right col-md-12 pull-right">
                            <a style="display: block; color: #DB1E30;" href="javascript:void(0);" data-toggle="modal" data-target="#myTenderModal">
                                <?php if($data['id'] == 0){ ?>
                                    <i  class="fa fa-plus"></i> Add
                                <?php } else {?>
                                    <i style="color: #DB1E30;" class="fa fa-pencil"></i> Edit
                                <?php } ?>
                            </a>
                        </div>
                </div>
					<div class="row">
						<div class="col-md-6 col-12">
							<div class="form-group"> 
								<label> Tender Name </label>
								<input type="text" placeHolder="Tender name" name="name" id="t_name" value="<?php echo $data['name'] ; ?>" class="form-control tenderData">
								
							</div>
							<div class="form-group"> 
								<label> Client </label>
								<input name="client" placeHolder="Tender Client" value="<?php echo $data['client'] ; ?>" id="t_client"  type="text" class="form-control tenderData">
								
							
							</div>
							<div class="form-group"> 
								<label> Contact Person </label>
								<input required placeHolder="Tender Contact Person"  name="contactPerson" id="t_contactPerson" value="<?php echo $data['contactPerson'] ; ?>" type="text" class="form-control tenderData">
								
							
							</div>	
						</div>
						<div class="col-md-6 col-12"> 

							<div class="form-group"> 
								<label> Mobile </label>
								<input type="text" name="mobile" id="t_mobile" value="<?php echo $data['mobile'] ; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
								
		
							</div>
							<div class="form-group"> 
								<label> Email </label>
								<input type="email" name="email"   id="t_email" value="<?php echo $data['email'] ; ?>" placeholder="Email Address" class="form-control tenderData" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter email address ">
							
		
							</div>
                            <div class="form-group">
                                <label> Contract Number</label>
                                <input type="text" name="contractNumber" id="t_contractNumber" value="<?php echo @$data['contractNumber'] ; ?>" class="form-control" placeholder="Contract Number" pattern="[0-9]{6,}" title="Contract Number should be number">


                            </div>

						</div>
					</div>
				
			</div>

			<div class="slider-part box">
				<div class="row">
					<div class="col-xl-4"> <ul id="navItems" class="nav nav-pills">


                        </ul></div>
					<div class="col-xl-5 col-lg-9 col-md-9 col-12"> 
						<div id="carouselExampleFade" data-interval='false' class="carousel slide carousel-fade" data-ride="carousel">
							<div class="carousel-inner">
							    
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-3 col-md-3 col-12">
						<a style="display: none;" href="javascript:void(0);" id="addSite" data-toggle="modal" data-target="#myModal"></a>
						<a href="javascript:void(0);" onclick="addEdit(0);" class="add-btn"> <img src="<?php echo site_url('assets/images/plus2.svg'); ?>" alt="icon"> Add Site</a>
					</div>
				</div>
			</div>
  <div id="sitesOptionsdata">
			<div class="upload-sec box"> 
				<div class="row"> 
					<div class="col-12">
						<div class="table-responsive upload-table">
							<table> 
								<tr>
									<th width="40%"> Type </th>
									<th width="60%" > Actions </th>
								</tr>
								<tr>
									<td> Panel Layout </td>
									<td> <a href="#"> <i class="fa fa-upload" aria-hidden="true"></i> Upload</a> </td>
								</tr>
								<tr>
									<td> Inverter Layout </td>
									<td> <a href="#"> <i class="fa fa-upload" aria-hidden="true"></i> Upload</a> </td>
								</tr>
								<tr>
									<td> Mounting </td>
									<td> <a href="#"> <i class="fa fa-upload" aria-hidden="true"></i> Upload</a> </td>
								</tr>

							</table>
						</div>
					</div>					
				</div>
			</div>

			<div class="sytem-table box"> 
				<div class="system-title">
					<h3> System Overview</h3>
					<a hreg="#" class="add-btn"> <img src="<?php echo site_url(); ?>assets/images/plus2.svg" alt="icon"> Add Site</a>
				</div>
				<div class="table-responsive mt-4 pt-4">
				

					<table class="tableoption tableoption-2 mt-5">
						<tr>
							<th class="empty"> </th>
							<th></th>
							<th class="bg-grey"> option1 </th>

						</tr>
						<tr>
							<td> System Size </td>
							<td></td>
							<td> 6.6kW </td>

						</tr>
						<tr>
							<td> Panels </td>
							<td></td>
							<td> EGing 370W  </td>

						</tr>
						<tr>
							<td> Inverter </td>
							<td></td>
							<td> Goodwe 5kW GW-5000DNS  </td>

						</tr>
						<tr>
							<td> Roof Type </td>
							<td></td>
							<td> Tile  </td>

						</tr>
						<tr> 
							<td colspan="3"  class="empty">
								<h4> Panel Layout</h4>
							</td>
						</tr>
						<tr> 
							<td> Flush </td> 
							<td></td>
							<td></td>

						</tr>
						<tr> 
							<td> Tilt </td> 
							<td></td>
							<td></td>

						</tr>
						<tr> 
							<td> Total Panels </td> 
							<td></td>
							<td></td>

						</tr>
						<tr > 
							<td colspan="3" class="empty">  </td>
						</tr>
						<tr> 
							<td> Price exc GST (inc site specific costs)</td> 
							<td> </td>	
							<td> <span class="dollar-btn">$</span> </td>

						</tr>						
					</table>

			
				</div>
			</div> 

			<div class="sytem-table box">
				<div class="system-title">
					<h3> Site specific system costs</h3>
					<a hreg="#" class="add-btn"> <img src="<?php echo site_url(); ?>assets/images/plus2.svg" alt="icon"> Add Site</a>
				</div>
				<div class="table-responsive pb-3">
					<table class="tableoption site-table">
						<tr>
							<th class="bg-grey">Type</th>
							<th class="bg-grey">Unit Price</th>
							<th class="bg-grey">Quantity</th>
							<th class="bg-grey">Total Price <a href="javascript:void(0)" class="arrow-btn arrow-btn1"><img src="<?php echo site_url(); ?>assets/images/arrow2.svg" alt="img"></a></th>
					</tr>

						<tr>
							<td> </td>
							<td> </td>
							<td> </td>
							<td> </td>	

						</tr>

						<tr>
							<td> </td>
							<td> </td>
							<td> </td>
							<td> </td>	

						</tr>

						<tr>
							<td> </td>
							<td> </td>
							<td> </td>
							<td> </td>	

						</tr>

						<tr>
							<td>Total </td>
							<td></td>
							<td></td>
							<td></td>

						</tr>

						<tr>
							<td colspan="3">Total </td>
							<td></td>
						</tr>

					</table>
				</div>
			</div>
	</div>

    <div class="compliance-sec mt-3">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="system-title">
                <h2>Attachments</h2>
            </div>
            <div class="d-block clearfix">
                <div class="table-responsive"  >
                    <table>
                        <thead>
                        <tr>
                            <th style="width:40% !important;">Address</th>
                            <th style="width:20% !important;">Name</th>
                            <th style="width:10% !important;">Date Created</th>
                            <th style="width:30% !important;">Action</th>
                        </tr>
                        </thead>
                        <tbody id="tenderBookFormsList">
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>


    <div class="compliance-sec mt-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="system-title">
							<h3> Tender Document</h3>
							<a  class="add-btn" href="javascript:void(0);" id="myDocuments" data-toggle="modal" data-target="#myDocumentModal">Add</a>
						</div>				
				    			 	            
			            <div class="d-block clearfix">
			                <div class="table-responsive ">
                                <form id="uploadTenderDocuments" action="<?php echo site_url('admin/tender/upload-tender-docs'); ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="docTypeId" id="docTypeId" value="0">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th> Document Type </th>
                                                <th> Status </th>
                                                <th> No. of Files Uploaded </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody id="attachment_type_table_body">

                                        </tbody>
                                    </table>
                                </form>
			        		</div> 
			    		</div>
			            <div class="databatn-group mt-3">
                            <?php if($data['id'] > 0 ) { ?>
			            	<a href="<?php echo site_url('admin/tender/downloadZip/'.$data['id']); ?>" class="btn org-btn"> <i class="fa fa-file-archive-o" aria-hidden="true"></i>&nbsp; Download ZIP File </a>
                            <?php } ?>
			            </div>
		        	</div>
		        </div>
	        </div>

			<div class="praposal-part">
				<div class="row">
			        <div id="all_activity_container" class="col-12 col-sm-12 col-md-12">
			        	<div class="card kg-activity__item kg-activity__item--default card_151051">
			        		<div class="card-body">
			        			<div class="kg-activity__item__title mb-2">
			        				<span class="mr-2"><i class="fa fa-street-view" aria-hidden="true"></i></span> Note -
			        			</div>	        			
			        			<div class="card-subtitle mb-2 pl-4"><span class="text-muted">30/08/2021 - Tracy Schiemer</span><span class="text-muted"><img src="<?php echo site_url(); ?>assets/images/icon50.svg" alt="icon" class="img-xs-width"> Louis Delgado</span>
			        			</div>
			        			<div class="description-box">
			        				<p class="kg-activity__item__description">Changed from New Lead to Appointment Booked on 30-08-2021</p>
			        			</div>
			        		</div>
			        	</div>

			        	<div class="card kg-activity__item kg-activity__item--default card_151050">
			        		<div class="card-body">
			        			<div class="kg-activity__item__title mb-2">
			        				<span class="mr-2"><i class="fa fa-building"></i></span>Visit - Tracy Schiemer
			        			</div>
			        	
			        			
			        			<div class="card-subtitle mb-2 pl-4"><span class="text-muted">30/08/2021 - Tracy Schiemer</span><span class="text-muted"><img src="<?php echo site_url(); ?>assets/images/icon50.svg" alt="icon" class="img-xs-width"> Louis Delgado</span>
			        			</div>			    				
			    				<div class="description-box">
			    					<p class="kg-activity__item__description">Zoom meeting booked. Bill: $300 per quarter. 2 people. single to double storey. Client will send picture. </p>
			    				</div>
			        		</div>
			    		</div>
					</div>
				</div>
			</div>
</div>


<div class="modal fade" id="myDocumentModal" role="dialog">
		<div class="modal-dialog modal-sm">

			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Document Type</h4> <button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form id="tenderDocumentType" method="post"> 
						<input type="hidden" name="tenderId" id="tenderId" value="<?php echo $data['id'] ; ?>" />
						<div class="row">
							<div class="col-md-12 col-12">
								<div class="form-group"> 
									<div class="col-sm-8 pull-left">
										<input type="text" placeHolder="Document Type" name="dcName" id="dcName" value="" class="form-control" />
									</div>		
									<div class="col-sm-2 pull-left">
										<button type="button" id="addDocType" class="btn btn-primary">Add</button>
									</div>
                                    <br/>
									<div class="text-center text-danger" id="dcNameDocCatError" style="display:none;"></div>
								</div>
							</div>
						</div>
					</form>

				</div>
				
			</div>

		</div>
	</div>



<div class="modal fade" id="mySiteCostType" role="dialog">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Site Cost Type</h4> <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="tenderSiteCostType" method="post">
                    <input type="hidden" name="siteId" id="costTypeSiteId" value="<?php echo $data['id'] ; ?>" />
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <div class="col-sm-8 pull-left">
                                    <input type="text" placeHolder="Cost Type" name="ctName" id="ctName" value="" class="form-control" />
                                </div>
                                <div class="col-sm-2 pull-left">
                                    <button type="button" id="saveCostType" class="btn btn-primary">Add</button>
                                </div>
                                <br/>
                                <div class="text-center text-danger" id="ctNameError" style="display:none;"></div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>


<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-md">
			<form id="tenderSite">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add/Edit Site</h4> <button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="siteId" id="siteId" value="0" />
					<div class="row">
						<div class="col-md-12">
					<div class="form-group">
						<label for="siteName">Site Name</label>
						<input type="text" class="form-control" name="siteName" id="siteName"/>
						<div class="text-center text-danger" id="nameSiteError" style="display:none;"></div>
		
					</div>
						</div></div>
					<div class="row">
						<input type="hidden" name="site[latitude]" id="locationLatitude" />
						<input type="hidden" name="site[longitude]" id="locationLongitude" />
						<div class="col-md-12">
							<div class="form-group">
								<label>Address</label>
								<a href="javascript:void(0);" id="manual_address_btn" style="font-size:12px; float:right;" >Edit Manually</a>
								<a href="javascript:void(0);" id="manual_address_hide_btn" style="font-size:12px; float:right;" class="hidden">Back to Autocomplete</a>
								<select class="select2 placecomplete form-control locationstreetAddress kg-form_field--orange" id="locationstreetAddress"  style="height:48px !important;"></select>
								<input class="locationstreetAddressVal form-control" type="text" id="locationstreetAddressVal" name="site[address]" />
								<div class="text-center text-danger" id="addressSiteError" style="display:none;"></div>
		
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button id="addUpdate" type="button" class="btn btn-primary">Add/Update</button>
					<button id="deleteSite" type="button" class="btn btn-danger">Delete</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
			</form>
		</div>
</div>


<div class="modal fade" id="myTenderModal" role="dialog">
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add/Edit Tender</h4> <button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form id="tenderDetails" method="post"> 
					<input type="hidden" name="tenderId" id="tenderId" value="<?php echo $data['id'] ; ?>" />
					<div class="row">
						<div class="col-md-6 col-12">
							<div class="form-group"> 
								<label> Tender Name </label>
								<input type="text" placeHolder="Tender name" name="name" id="tName" value="<?php echo $data['name'] ; ?>" class="form-control tenderData">
								<div class="text-center text-danger" id="nameError" style="display:none;"></div>
							</div>
							<div class="form-group"> 
								<label> Client </label>
								<input name="client" placeHolder="Tender Client" value="<?php echo $data['client'] ; ?>" id="tClient"  type="text" class="form-control tenderData">
								<div class="text-center text-danger" id="clientError" style="display:none;"></div>
							
							</div>
							<div class="form-group"> 
								<label> Contact Person </label>
								<input required placeHolder="Tender Contact Person" name="contactPerson" id="tcontactPerson" value="<?php echo $data['contactPerson'] ; ?>" type="text" class="form-control tenderData">
								<div class="text-center text-danger" id="contactPersonError" style="display:none;"></div>
							
							</div>	
						</div>
						<div class="col-md-6 col-12"> 
							<div class="form-group"> 
								<label> Mobile </label>
								<input type="text" name="mobile" id="tMobile" value="<?php echo $data['mobile'] ; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
								<div class="text-center text-danger" id="mobileError" style="display:none;"></div>
		
							</div>
							<div class="form-group"> 
								<label> Email </label>
								<input type="email" name="email"   id="tEmail" value="<?php echo $data['email'] ; ?>" placeholder="Email Address" class="form-control tenderData" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter email address ">
								<div class="text-center text-danger" id="emailError" style="display:none;"></div>
		
							</div>
                            <div class="form-group">
                                <label> Contract Number </label>
                                <input type="text" name="contractNumber"   id="tContractNumber" value="<?php echo @$data['contractNumber'] ; ?>" placeholder="Contract Number" class="form-control tenderData">
                                <div class="text-center text-danger" id="contractNumberError" style="display:none;"></div>

                            </div>
                        </div>
					</div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                            </div>
                            <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label>  </label>
                                <input type="submit" name="submit" data-loading-text="Saving..." class="btn btn-default" value="SAVE">
                            </div>
                            </div>
                        </div>
					</form>
		
			</div>
	</div>
        </div>
    </div>

<div class="modal fade" id="tenderDocumentsListModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tender Doucments List</h4> <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  id="tenderDocumentsList">

            </div>
        </div>
    </div>
</div>

<div id='imgModal' class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span class='icn-white' aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <img id='imgFullSize' src='' width='100%' alt='site image'>
        </div>
    </div>
</div>


<div class="modal fade" id="booking_form_choose_customer_location_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="margin-bottom: -10px;">Choose Site
                    <a style="margin-left:20px;" id="add_new_tender_site" href="javascript:addSIte(0);"><i class="fa fa-plus"></i> Add New</a></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div id="tender_booking_form_cost_centre_body" class="mb-4 d-flex"></div>
                        <table width="100%" border="0" class="table table-striped" id="customer_booking_form_location_table" >
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>Address</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody id="tender_booking_form_site_table_body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src='<?php echo site_url(); ?>assets/js/jquery-ui.min.js' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzX5cZK-FJWov4arDc_LhycU20Z6gNDp0&libraries=places,geometry"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/tender_activity.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/tender_booking_form.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script>
    var sitesData = <?php echo json_encode($data['sites']); ?>;
    var documentTypes = <?php echo json_encode($data['documentTypes']); ?>;
    var context = {};
    context.user_type = '1';
    context.userid = '1';
    context.tenderId = '<?php echo (int)$data['id']; ?>';
    context.siteData =  JSON.parse(JSON.stringify(sitesData)) ;
    context.costCenters = <?php echo json_encode(get_cost_center_by_type('solar', 'ccid,name')) ?>;
    var tender_manager = new tender_booking_form_manager(context);

    var ac_context = {};
    ac_context.view = 'lead';
    ac_context.lead_id = <?php echo (int)$data['id']; ?>;
    ac_context.lead_data = '<?php echo (isset($tender[0]) && !(empty($tender[0]))) ? json_encode($tender[0], JSON_HEX_APOS) : ''; ?>';
    var activity_manager_tool = new activity_manager(ac_context);

		function addEdit(siteId){
			if($('input#tenderId').val() == 0){
					toastr.error('Please add the tender.');
					setTimeout(()=>{
								$('#error_message').html('');
							}, 3000);
					return false;
				}
			if(siteId > 0) {
				$.each(sitesData,function(index,eachSiteObj){
					if(eachSiteObj.id == siteId){
						$('input#siteName').val(eachSiteObj.name);
						$('input#siteId').val(eachSiteObj.id);
						$('input#locationstreetAddressVal').val(eachSiteObj.address);
						setTimeout(function () {  
							var location_address = eachSiteObj.address;   
							          $('#select2-locationstreetAddress-container').html(location_address);         }, 1000);
					}
				});
			} else {
				$('input#siteName').val('');
				$('input#siteId').val(0);
				$('input#locationstreetAddressVal').val('');
				setTimeout(function () {     
							          $('#select2-locationstreetAddress-container').html('');         }, 1000);

			}
			
			$( "#addSite" ).trigger( "click" );
		}

		$(document).ready(function (){
            $('a#deal_link').hide();
			<?php if($data['id'] == 0){?>
			$('#myTenderModal').modal('show');
			<?php } else {?>

            //Activity Handler
            $('#add_activity').click(function () {
                //Make Activity Attachment Blank
                document.getElementById('activity_attachments').innerHTML = '';
                $('.dz-preview').remove();
                $('.dropzone').removeClass('dz-started');


                $('#scheduler').addClass('hidden');
                $('#attendees').addClass('hidden');
                $('#status').attr('checked','checked');
                $("#activity_type option[value='Scheduled Meeting']").hide();
                $("#scheduled_time_value").val('');
                $('#activity_add_form').trigger("reset");
                $('.custom-file-label').html('Choose attachment file');
                $('#activity_id').val('');
                $('#mark_as_completed').addClass('hidden');
                $('.datepicker').hide();
                self.activity_action = "Add Activity";
                window.activity_manager_tool.activity_trigger = 1;

                $('#activity_modal_body_left').removeClass('col-md-6');
                $('#activity_modal_body_left').addClass('col-md-12');

                $('#activityLocationPostCode').val('');
                $('#select2-activityAddress-container').html('');
                $('#activityLocationAddressVal').val('');
                $('#activityLocationState').val('');
                $('#activityDiv').removeClass('col-md-6').addClass('col-md-12');
                $('#activity_modal').modal('show');
            });
            $('#schedule_activity').click(function () {
                //Make Activity Attachment Blank
                document.getElementById('activity_attachments').innerHTML = '';
                $('.dz-preview').remove();
                $('.dropzone').removeClass('dz-started');


                $('#scheduler').removeClass('hidden');
                $('#attendees').removeClass('hidden');
                $('#status').removeAttr('checked');
                $("#activity_type option[value='Scheduled Meeting']").show();
                $("#scheduled_time_value").val('');
                $('#activity_add_form').trigger("reset");
                $('.custom-file-label').html('Choose attachment file');
                $('#activity_id').val('');
                $('#mark_as_completed').addClass('hidden');
                $('.datepicker').show();
                self.activity_action = "Scheduled Activity";
                window.activity_manager_tool.activity_trigger = 2;

                $('#activity_modal_body_left').addClass('col-md-6');
                $('#activity_modal_body_left').removeClass('col-md-12');

                $('#activityLocationPostCode').val('');
                $('#select2-activityAddress-container').html('');
                $('#activityLocationAddressVal').val(' ');
                $('#activityLocationState').val('');
                $('#activityDiv').removeClass('col-md-12').addClass('col-md-6');
                $('#activity_modal').modal('show');
            });
        <?php } ?>
            $(document).on('click','#addCostType',function(){
               $('#costTypeSiteId').val($(this).attr('data-siteId'));
                $('#mySiteCostType').modal('show');
            });
			$('#carouselExampleFade').on('slid.bs.carousel', function (ev) {
				var tenderSiteId = ev.relatedTarget.id ;
				var optionId=0;
                if($('a[id^=selectTab]').hasClass('active')){
                    $('a[id^=selectTab]').removeClass('active');
                }
               $('a#selectTab'+tenderSiteId).addClass('active');
				loadSiteOptions(tenderSiteId,optionId);
			});
            $('#link').on('click', function() {
                $('#carousel').carousel(i);
            });

			$('#manual_address_btn').click(function(){
				$(this).addClass('hidden');
				$('#manual_address_hide_btn').removeClass('hidden');
				$('#locationstreetAddress').next().css('display','none');
				$('#locationstreetAddressVal').attr('type','text').show();
			});
			$('#addUpdate').click(function(){
				$('div.text-danger').hide();
				$.ajax({
					url: '<?php echo site_url();?>admin/tender/save-site',
					type: 'POST',
					data: {
                        name:$('input#siteName').val(),
                        address:$('input#locationstreetAddressVal').val(),
                        siteId:$('input#siteId').val(),
                        tenderId:$('input#tenderId').val(),
                    },
					dataType: 'json',
					success: function(data) {
						if(data.success) {

							if($('#siteId').val() > 0){
                                toastr.success('Site information updated successfully.');
								$.each(sitesData,function(index,eachSiteObj){
									if(eachSiteObj.id == $('#siteId').val()){
										eachSiteObj.name = $('#siteName').val().trim();
										eachSiteObj.address = $('input#locationstreetAddressVal').val().trim();
									}
								});
							} else{
                                toastr.success('Site information added successfully.');
								sitesData.push({
									'id':data.id,
									'name':$('#siteName').val().trim(),
									'address':$('input#locationstreetAddressVal').val().trim(),
								})
							}
							//console.log(sitesData);
                            loadTenderForms();
							loadSitesScroller(sitesData,0);
							$('.close').trigger('click');
						} else if(!data.success) {
							if(Object.keys(data.errors).length > 0 ){
								$.each(data.errors,function(i,v){
									$('#'+i+'SiteError').show().text(v);
								})
							}
						}
					}
				});
			});
			$('#deleteSite').click(function(){
				var filteredAry = [];
				$.ajax({
					url: '<?php echo site_url('admin/tender/delete-site')?>',
					type: 'POST',
					data: {
						siteId: $('#siteId').val(),
					},
					dataType: 'json',
					success: function(data) {
						if(data.success) {
							if($('#siteId').val() > 0){
								 filteredAry = sitesData.filter(function(e) { return e.id !== $('#siteId').val() })
							}
							sitesData = filteredAry;
							loadSitesScroller(filteredAry,0);
                            toastr.success('Site information deleted successfully.');
							$('.close').trigger('click');
						}
					}
				});
			});
            $(document).on('click','#add_new_tender_site',function(){
                $('.close').trigger('click');
                addEdit(0);
            })

			$('#manual_address_hide_btn').click(function(){
				$(this).addClass('hidden');
				$('#manual_address_btn').removeClass('hidden');
				$('#locationstreetAddress').next().css('display','block')
				$('#locationstreetAddressVal').attr('type','hidden').hide();
			});

			$('#locationstreetAddressVal').keyup(function(){
				$('#select2-locationstreetAddress-container').html($(this).val());
			});
			function loadSitesScroller(sitesData,selectedPostion){
				var m=1;
				var total = sitesData.length;
				var scroller = '';
				var tabScroller = '';
				var selectedSiteId = 0;
				$('div.carousel-inner').html(scroller);
                $('#navItems').html('');
                //tender_manager.siteData = [];
                tender_manager.siteData =  sitesData;
				$.each(sitesData,function(k,v){
					if(k==selectedPostion) {
						selectedSiteId = v.id;
					}
                   // tender_manager.siteData = sitesData;
					scroller = scroller+'<div id="'+v.id+'" class="carousel-item '+((k==selectedPostion)?"active":"")+'">';
					scroller = scroller+'<p><i class="fa fa-pencil" aria-hidden="true" onclick="javascript:addEdit('+v.id+');"></i>&nbsp;'+v.name+'</p><h1>'+v.address+'</h1>';
					scroller = scroller+'<h2>'+m+' of '+total+' sites </h2></div>';
					m++;
                    tabScroller +="<li class='nav-item' data-postion='"+k+"'><a id='selectTab"+v.id+"' class='nav-link"+((k==selectedPostion)?" active":"")+"' aria-current='page' href='#'>"+v.name+"</a></li>";
				});
                $('#navItems').html(tabScroller);


				scroller = scroller+'<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>';
				$('div.carousel-inner').html(scroller);
                if(selectedSiteId > 0) {
                    loadSiteOptions(selectedSiteId, 0);
                }
			}
			loadSitesScroller(sitesData,0);
            $(document).on('click','ul#navItems li.nav-item',function(){
                var selectedPostion = $(this).attr('data-postion');
                loadSitesScroller(sitesData, selectedPostion);

            });

            $(document).on('click','#addMoreOption',function(){
                var siteId =$(this).attr('data-siteId');
                $.ajax({
                    type:'POST',
                    url: '<?php echo site_url('/admin/tender/add-site-option'); ?>',
                    data:{
                        siteId:siteId,
                    },
                    dataType:'json',
                    success:function(result){
                        if(result.success) {
                            loadSiteOptions(siteId, 0);
                        }
                    }
                });

            });
            function loadSiteOptions(siteId,optionId) {
                $.ajax({
                    url: '<?php echo site_url();?>admin/tender/loadoptions',
                    type: 'POST',
                    data:{
                        siteId:siteId,
                        optionId:optionId
                    },
                    dataType: 'html',

                    success: function(data) {
                        $('div#sitesOptionsdata').html(data);
                    }
                });

            }
            $('#addDocType').on('click',function(){
                $.ajax({
                    url: '<?php echo site_url();?>admin/tender/save-doc-cat',
                    type: 'POST',
                    data: $('form#tenderDocumentType').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if(data.success) {
                            $('#dcName').val('');
                            toastr.success('Document type added successfully.');
                            loadDocTypes();
                            $('.close').trigger('click');
                        } else if(!data.success) {
                            if(Object.keys(data.errors).length > 0 ){
                                $.each(data.errors,function(i,v){
                                    $('#'+i+'DocCatError').show().text(v);
                                })
                            }
                        }
                    }
                });

            });

            $('#saveCostType').on('click',function(){
                $.ajax({
                    url: '<?php echo site_url('admin/tender/save-cost-cat');?>',
                    type: 'POST',
                    data: $('form#tenderSiteCostType').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if(data.success) {
                            $('#ctName').val('');
                            toastr.success('Site Cost type added successfully.');
                            loadSiteOptions($('#costTypeSiteId').val(),0);
                            $('.close').trigger('click');
                        } else if(!data.success) {
                            if(Object.keys(data.errors).length > 0 ){
                                $.each(data.errors,function(i,v){
                                    $('#'+i+'Error').show().text(v);
                                })
                            }
                        }
                    }
                });

            });


			$('[data-toggle="tooltip"]').tooltip();
			$('#locationstreetAddress').placecomplete1({
				placeServiceResult1: function (data, status) {
					if (status != 'INVALID_REQUEST') {
						console.log(data);
						$('#show_something').html(data.adr_address);

						//Set lat lng
						var lat = data.geometry.location.lat(),
							lng = data.geometry.location.lng();
						if ($('#locationLatitude') != undefined) {
							$('#locationLatitude').val(lat);
						}
						if ($('#locationLongitude') != undefined) {
							$('#locationLongitude').val(lng);
						}
						//Set Address
						if ($('#locationstreetAddressVal') != undefined) {
							$('#locationstreetAddressVal').val(data.formatted_address);
						}
					} else {
						$('#locationstreetAddressVal').val($('#locationstreetAddress').val());
					}
				},
				language: 'en'
			});
			$('form#tenderDetails').submit(function(e){
				e.preventDefault();
				var form = $(this);
				$('div.text-danger').hide();
				$('#error_message').html('');
				$.ajax({
					url: '<?php echo site_url() ?>/admin/tender/save',
					type: 'POST',
					data: form.serialize(),
					dataType: 'json',
					success: function(data) {
						if(data.success) {
							$('#tenderId').val(data.id);
                            toastr.success('Tender data saved successfully');
							setTimeout(()=>{
								$('#error_message').html('');
							}, 3000);
                            window.location.href='<?php echo site_url('admin/tender/add/')?>'+data.id;
							$.each(data.tenderData,function(i,v){
								$('#t_'+i).val(v);
							});
							$("#myTenderModal .close").click();
						}else{
							if(Object.keys(data.errors).length > 0 ){
								$.each(data.errors,function(i,v){
									$('#'+i+'Error').show().text(v);
								})
							}
						}
					}
				});
				
			});
            function loadDocTypes(){
                $.ajax({
                    url: '<?php echo site_url();?>admin/tender/load-doc-cat',
                    type: 'POST',
                    data: {
                        tenderId:$('input#tenderId').val()
                    },
                    dataType: 'html',
                    success: function(data) {
                        $('tbody#attachment_type_table_body').html(data);
                    }
                });


            }
            loadDocTypes();
            function loadTenderForms(){
                $.ajax({
                    url: '<?php echo site_url();?>admin/tender/booking-forms-list',
                    type: 'POST',
                    data: {
                        tenderId:$('input#tenderId').val()
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.success) {
                            $('tbody#tenderBookFormsList').html(data.html);
                        }
                    }
                });


            }
            loadTenderForms();
            $('#uploadTenderDocuments').on('submit',(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url: $(this).attr('action'),
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success:function(result){
                        data = JSON.parse(result);
                        if(data.success){
                            toastr.success('Document uploaded successfully');
                            loadDocTypes();
                        } else {
                            toastr.error(data.errors.replace(/<\/?[^>]+(>|$)/g, ""));
                        }

                    },
                    error: function(result){

                    }
                });
                $('input.uploadDocuments').val();
            }));
            <?php if($data['id'] > 0){?>
            $(document).on('click','.updateStatus',function(){
               var statusId = $(this).attr('data-statusId');
                $.ajax({
                    url: '<?php echo site_url('admin/tender/update-status') ?>',
                    type: 'POST',
                    data: {
                        statusId : statusId,
                        tenderId : $('input#tenderId').val()
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.success) {
                            $('.updateStatus').removeClass('active');
                            $('.updateStatus').each(function(){
                                if($(this).attr('data-statusId')==statusId){
                                    $(this).addClass('active');
                                }
                            });
                            toastr.success('Tender status saved successfully');
                        }
                    }
                });
            });
            <?php } ?>

		});





</script>
	

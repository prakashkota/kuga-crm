<?php /* <div class="login-wrapper d-block clearfix">
  <div class="login-panel d-block vertical-middle clearfix">
  <div class="logo text-center"><img src="<?php echo site_url();?>assets/images/logo.svg" alt="logo"></div>
  <h2>Login with your account</h2>

  <form role="form" action="" method="post" name="frm" >
  <div class="login-form-block text-center d-block clearfix">
  <?php $message = $this->session->flashdata('message');
  echo (isset($message) && $message != NULL) ? $message : ''; ?>
  <div class="form-body d-block clearfix">
  <div class="row">
  <div class="col-12">
  <div class="form-group">
  <input type="text" name="email" id="email"  class="form-control" placeholder="Email" value="<?php echo (isset($email)) ? $email : ''; ?>">
  <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
  </div>
  </div>
  <div class="col-12">
  <div class="form-group">
  <input type="password" name="password" id="password"  class="form-control" placeholder="Password">
  <?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
  </div>
  </div>
  <div class="col-12">
  <button type="submit" class="btn">Log in</button>
  </div>
  </div>
  </div>
  </div>
  </form>
  <!-- <a href="#" class="forgot">Forgot password.</a> -->
  </div>
  </div>
  <??php */ ?>
  <style>
    .clearfix:after {
      visibility: hidden;
      display: block;
      font-size: 0;
      content: " ";
      clear: both;
      height: 0;
    }
    .login-box-outer{ height:100vh; position:relative;}
    .login-box{ position:absolute; top:50%; right:0; left:0; max-width:800px; margin:0 auto; transform:translateY(-50%);}
    .login-logo{width: 39%;

      float: left;

      padding-top: 65px;

      border: 1px solid #ccc;

      padding-left: 50px;

      min-height: 232px;

      text-align: center;

      padding-right: 50px;}
      .login-fild {width: 60%;float: right;border: solid 1px #dadada;}
      .login-fild h2 {display: block;margin: 0;padding: 10px 20px;background: #f5f5f5;border-bottom: solid 1px #dadada;font-size: 18px;}
      .form-box{ padding:20px 20px;}
      .form-box .form-group{ position:relative; margin-bottom:15px;}
      .input-group-addon{background:#eeeeee;display:block;position:absolute;top:1px;bottom:1px;left:1px;border-radius:3px 0 0 3px;border-right:solid 1px #dcdcdc;}
      .form-controlInt{ width:100%; border:none;height: 38px;border: solid 1px #dcdcdc; border-radius:3px; padding:0 0 0 50px; box-sizing:border-box;}
      .login-btn{ background:#428bca; border:none; color:#fff; font-size:16px; padding:10px 20px; cursor:pointer;border-radius:3px;}
      .text-left.text-danger {position: absolute;bottom: 11px;right: 10px;font-style: italic;font-size: 11px;letter-spacing: 1px;}

      @media only screen and (max-width:767px){
        .login-box {padding:25px 15px;transform: inherit; position: static;}  
        .login-fild {width:100%;}
        .login-logo {width: 100%;text-align: center;min-height: auto;padding: 15px;margin-bottom: 15px;border: none;}
        .form-box .form-group {margin-bottom: 30px;}
        .text-left.text-danger {right: auto;left: 38px;bottom: -17px;}
      }
      body{
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.428571429;
        color: #333333;
        background-color: #ffffff;
      }
    </style>
    <div class="login-box-outer">
      <div class="login-box clearfix">
        <?php $message = $this->session->flashdata('message');
        echo (isset($message) && $message != NULL) ? $message : '';
        ?>
        
        <div id="error_message"></div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="sales-tab" data-toggle="tab" href="#sales" role="tab" aria-controls="sales" aria-selected="true">SALES CRM LOGIN</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="jobs-tab" data-toggle="tab" href="#jobs" role="tab" aria-controls="jobs" aria-selected="false">Job CRM LOGIN</a>
          </li>
        </ul>

        <div class="login-logo"><img src="<?php echo site_url(); ?>assets/images/logo.svg" alt="logo"/></div>

        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="sales" role="tabpanel" aria-labelledby="sales-tab">
            <div class="login-fild">
              <h2>Enter Details To Login For SALES CRM</h2>
              <form role="form" action="" method="post" name="frm" >
                <div class="form-box clearfix">
                  <div class="form-group">
                    <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/user-icon.png" alt="user-icon"/></span>
                    <input type="text" placeholder="Your username" name="email" id="email_sales"  class="form-controlInt" value="<?php echo (isset($email)) ? $email : ''; ?>">
                    <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
                  </div>
                  <div class="form-group">
                    <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/lock-icon.png" alt="lock-icon"/></span>
                    <input type="password" placeholder="Password" name="password" id="password_sales"  class="form-controlInt">
                    <?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
                  </div>
                  <div class="row">
                    <span class="col-md-6"> <input type="button" class="login-btn" value="Login" id="sales_login_btn"> </span>
                    <span class="col-md-6 text-right"> <a href='https://kugacrm.com.au/account/forgot-password'>forgot password?</a> </span>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="jobs" role="tabpanel" aria-labelledby="jobs-tab">

            <div class="login-fild">
              <h2>Enter Details To Login For JOB CRM</h2>
              <form role="form" action="" method="post" name="frm" >
                <div class="form-box clearfix">
                  <div class="form-group">
                    <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/user-icon.png" alt="user-icon"/></span>
                    <input type="text" placeholder="Your username" name="email" id="email_jobs"  class="form-controlInt" value="<?php echo (isset($email)) ? $email : ''; ?>">
                    <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
                  </div>
                  <div class="form-group">
                    <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/lock-icon.png" alt="lock-icon"/></span>
                    <input type="password" placeholder="Password" name="password" id="password_jobs"  class="form-controlInt">
                    <?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
                  </div>
                  <div class="row">
                    <span class="col-md-6"> <input type="button" class="login-btn" value="Login" id="kuga_login_btn"> </span>
                    <span class="col-md-6 text-right"> <a href="https://kugacrm.com.au/job_crm/account/forgot-password">forgot password?</a> </span>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      var activeTab = 'sales-tab';
      
      $(document).ready(function(){
        var localStorage = window.localStorage;
        $('#jobs-tab').click(function(){
          activeTab = 'jobs-tab';
          localStorage.setItem('activeTab',activeTab);
        });
        $('#sales-tab').click(function(){
          activeTab = 'sales-tab';
          localStorage.setItem('activeTab',activeTab);
        });
        var lastActiveTab = localStorage.getItem('activeTab');

        if(lastActiveTab != '' && lastActiveTab != null){
          $('#'+lastActiveTab).click();
        }
      });
      
      $('#sales_login_btn').click(function(){
        $this = $(this);
        var data = {};
        data.email = $('#email_sales').val();
        data.password = $('#password_sales').val();
        $.ajax({
          type: 'POST',
          url: base_url + 'admin/login_ajax',
          datatype: 'json',
          data: data,
          beforeSend: function () {
            $this.hide();
            $('#error_message').html('');
            $('.error_pwd').remove();
            $('.error_email').remove();
          },
          success: function (stat) {
            if (stat.success == true) {
              toastr["success"](stat.status);
              if(stat.status){
                $('#error_message').html('<div class="alert alert-success"> '+stat.status+' </div>');
              }
              if(stat.is_superuser == true && activeTab  == 'sales-tab'){
                  $('#email_jobs').val($('#email_sales').val());
                  $('#password_jobs').val($('#password_sales').val());
                  toastr["info"]('Logging SuperUser in Job CRM Too.');
                  setTimeout(function(){
                      $('#kuga_login_btn').click();
                      window.open(base_url);
                  },1000);
              }else{
                  
                if(stat.is_tender_profile){
                    window.location.href = 'https://kugacrm.com.au/admin/tender-lead/manage?view=pipeline';
                }else{
                    window.location.href = base_url;
                }
              }
            } else {
              toastr["error"](stat.status);
              $('.error_pwd').remove();
              $('.error_email').remove();
              if(stat.errors){
                if(stat.errors.password){
                  $('#password_sales').after('<div class="text-left text-danger error_pwd">'+stat.errors.password+'</div>')
                }
                if(stat.errors.email){
                  $('#email_sales').after('<div class="text-left text-danger error_email">'+stat.errors.email+'</div>')
                }
              }
              if(stat.status){
                $('#error_message').html('<div class="alert alert-danger"> '+stat.status+' </div>');
              }
              $this.show();
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            $this.show();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      });

      $('#kuga_login_btn').click(function(){
        $this = $(this);
        var data = {};
        data.email = $('#email_jobs').val();
        data.password = $('#password_jobs').val();
        $.ajax({
          type: 'POST',
          url: 'https://kugacrm.com.au/job_crm/admin/login_ajax',
          datatype: 'json',
          data: data,
          beforeSend: function () {
            $this.hide();
            $('#error_message').html('');
            $('.error_pwd').remove();
            $('.error_email').remove();
          },
          success: function (stat) {
            if (stat.success == true) {
              toastr["success"](stat.status);
              if(stat.status){
                $('#error_message').html('<div class="alert alert-success"> '+stat.status+' </div>');
              }
              if(stat.is_superuser == true && activeTab  != 'sales-tab'){
                  $('#email_sales').val($('#email_jobs').val());
                  $('#password_sales').val($('#password_jobs').val());
                  toastr["info"]('Logging SuperUser in Sales CRM Too.');
                  setTimeout(function(){
                      $('#sales_login_btn').click();
                      window.open('https://kugacrm.com.au/job_crm');
                  },1000);
              }else{
                window.location.href = 'https://kugacrm.com.au/job_crm';
              }
            } else {
              toastr["error"](stat.status);
              $('.error_pwd').remove();
              $('.error_email').remove();
              if(stat.errors){
                if(stat.errors.password){
                  $('#password_sales').after('<div class="text-left text-danger error_pwd">'+stat.errors.password+'</div>')
                }
                if(stat.errors.email){
                  $('#email_sales').after('<div class="text-left text-danger error_email">'+stat.errors.email+'</div>')
                }
              }
              if(stat.status){
                $('#error_message').html('<div class="alert alert-danger"> '+stat.status+' </div>');
              }
              $this.show();
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            $this.show();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      });
      
    </script>
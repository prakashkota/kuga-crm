<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Quick Quote</title>

  <style>
    @media print {
      .no-print {display: none;}
      body {background: transparent;}
      .red{color:#c32027}
      .black-bg {background-color:#010101}
      .dark-bg{background-color:#282828}
    </style>
  </head>
  <body style="padding:0; margin:0">
      
    <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td><img src="<?php echo $page1_file; ?>" alt="" width="910" height="1287" /></td>
        </tr>
    </table>
    
    <?php if(isset($page2_file)) { ?>
    <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top:2px;">
        <tr>
            <td><img src="<?php echo $page2_file; ?>" alt="" width="910" height="1287" /></td>
        </tr>
    </table>
    <?php } ?>
    
    </body>
</html>

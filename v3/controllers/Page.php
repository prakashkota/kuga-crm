<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Page Controller
 * @version 1.0
 */
class Page extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("Email_Manager");
        $this->load->model("Article_Model", "article");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("User_Model", "user");
        $this->load->model("Log_Model");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function knowledgebase() {
        $data = array();
        $data['articles'] = array();
        $data['article_categories'] = $article_categories = $this->common->fetch_where('tbl_article_categories', '*', array('status' => 1));
        foreach ($article_categories as $key => $value) {
            $data['articles'][$value['id']] = $this->article->get_articles(FALSE, $value['id'], 5);
        }
        $data['latest_articles'] = $this->article->get_articles(FALSE, FALSE, 5);
        $this->load->view('page/header', $data);
        $this->load->view('page/knowledgebase');
        $this->load->view('page/sidebar');
        $this->load->view('page/footer');
    }

    public function article_category($slug = NULL) {

        if ($slug == NULL) {
            redirect('admin/knowledgebase/');
        }
        $data = array();
        $data['latest_articles'] = $this->article->get_articles(FALSE, FALSE, 5);
        $article_categories = $this->common->fetch_where('tbl_article_categories', '*', array('status' => 1, 'category_slug' => $slug));
        if (empty($article_categories)) {
            redirect('admin/knowledgebase/');
        }
        $data['article_category'] = $article_categories[0];
        $data['articles'] = $this->article->get_articles(FALSE, $article_categories[0]['id']);
        $this->load->view('page/header', $data);
        $this->load->view('page/article_category');
        $this->load->view('page/sidebar');
        $this->load->view('page/footer');
    }

    public function article($cat_slug = NULL, $article_slug = NULL) {
        $data = array();
        $data['latest_articles'] = $this->article->get_articles(FALSE, FALSE, 5);
        $data['article'] = $article = $this->article->get_article_by_slug($cat_slug, $article_slug);
        if (empty($article)) {
            redirect('admin/knowledgebase/');
        }
        $this->load->view('page/header', $data);
        $this->load->view('page/article');
        $this->load->view('page/sidebar');
        $this->load->view('page/footer');
    }

    public function search_articles() {
        $data = array();
        $data['latest_articles'] = $this->article->get_articles(FALSE, FALSE, 5);
        $keyword = $this->input->get('keyword');
        $articles = $this->article->get_articles_by_keyword($keyword);
        $data['articles'] = (!empty($articles)) ? $articles : [];
        $data['keyword'] = $keyword;
        $this->load->view('page/header', $data);
        $this->load->view('page/article_category');
        $this->load->view('page/sidebar');
        $this->load->view('page/footer');
    }
    
    
    public function get_product_data($pname){
        
        $product_data = array();
        if(strpos($pname, 'RHB') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 61;
            $product_data['crm_prd_type_id'] = 3;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Tube') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 21;
            $product_data['crm_prd_type_id'] = 4;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Batten') !== false){
            $product_data['job_id'] = $pname;
            $product_data['crm_prd_id'] = 21;
            $product_data['crm_prd_type_id'] = 4;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Downlight') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 25;
            $product_data['crm_prd_type_id'] = 7;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Canopy') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 10;
            $product_data['crm_prd_type_id'] = 3;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Panel') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 18;
            $product_data['crm_prd_type_id'] = 5;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Oyster') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 60;
            $product_data['crm_prd_type_id'] = 3;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else if(strpos($pname, 'Shopfitter') !== false){
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 24;
            $product_data['crm_prd_type_id'] = 7;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }else {
            $product_data['job_id'] = NULL;
            $product_data['crm_prd_id'] = 64;
            $product_data['crm_prd_type_id'] = 8;
            $product_data['name'] = $pname;
            $product_data['qty'] = NULL;
        }
        return $product_data;
        
    }
    public function log_issue() {
        $data = array();
        
    
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('description', 'Description', 'required');
            if ($this->form_validation->run() != FALSE) {
                $user_id = $this->input->post('user_id');
                $cust_id = $this->input->post('cust_id');
                $insert_data = $this->input->post();
                $insert_data['user_id'] = isset($user_id) ? $user_id : $this->aauth->get_user()->id;
                $stat = $this->common->insert_data('tbl_issues',$insert_data);
                if ($stat) {
                    $mail_data = array();
                    $mail_data = $insert_data;
                    $mail_data['status'] = 'Open';
                    $mail_data['franchise'] = $this->user->get_users(FALSE, $insert_data['user_id'], FALSE);
                    $mail_data['customer'] = (isset($cust_id) && $cust_id != '') ? $this->customer->get_customers(FALSE, $cust_id,FALSE) : NULL;
                    $this->email_manager->send_issue_email($mail_data,NOTIFICATION_MAIL);
                    $this->session->set_flashdata('message', '<div class="alert alert-success">Issue created successfuly.</div>');
                    redirect('admin/page/log-issue');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            } else {
                $data = $this->input->post();
            }
        }
        
        $user_id = $this->aauth->get_user_id();
        if ($this->aauth->is_member('Admin')) {
            $data['franchises'] = $this->user->get_users(FALSE, FALSE, FALSE);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $owner = $this->user->get_users(FALSE, $user_id, FALSE);
            $sub_users = $this->user->get_users(FALSE, FALSE, $user_id);
            $data['franchises'] = array_merge($owner,$sub_users);
            $data['all_reports_view_permission'] = 1;
        } else if ($this->components->is_sales_rep()) {
            $data['franchises'] = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['is_sales_rep'] = TRUE;
        }
        
        $data['user_id'] = $user_id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('page/issue');
        $this->load->view('partials/footer');
    }
    
    public function manage_log_issue() {
        $data = array();
        //$this->update_postcode();
        //die;
        $user_id = $this->aauth->get_user_id();
        $status = $this->input->get('status');
        $data['status']  = $status = (isset($status) && $status != '') ? $status : '0,1,2';
        if ($this->aauth->is_member('Admin')) {
            $data['issues'] = $this->Log_Model->get_log_issues(FALSE,$status);
        } else if ($this->components->is_team_leader()) {
            $data['issues'] = $this->Log_Model->get_log_issues($user_id,$status);
        } else if ($this->components->is_sales_rep()) {
            $data['issues'] = $this->Log_Model->get_log_issues($user_id,$status);
        }else {
            $data['issues'] = $this->Log_Model->get_log_issues($user_id,$status);
        }
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('page/manage_issue');
        $this->load->view('partials/footer');
    }
    
    
    public function update_postcode() {
        $filename = FCPATH. './v3/stc.csv';
		$handle = @fopen($filename, "r");
		$row_counter = 1;
		//print_r($handle);die;
		while(($row = fgetcsv($handle, 4096)) !== false){	    
			$postcode_start =  ($row[1]==0)?1:$row[1];
			$postcode_end =  ($row[2]==0)?1:$row[2];
			$postcode_zone =  (int) $row[3];
					
			if($postcode_zone == 1){
			    $postcode_rating =  1.622;
			}else if($postcode_zone == 2){
			    $postcode_rating =  1.536;
			}else if($postcode_zone == 3){
			    $postcode_rating =  1.382;
			}else if($postcode_zone == 4){
			    $postcode_rating =  1.185;
			}
			
			$update = $this->common->update_data('tbl_postcodes',array('postcode >=' => $postcode_start, 'postcode <=' => $postcode_end),array('zone' => $postcode_zone,'rating' => $postcode_rating));
			
			if($update){
			    echo 'Postcode '.$postcode_start.' to '.$postcode_end.' rating update : '.$postcode_rating.'<br>';
			} else {
			    echo 'Update Postcode data failed'.'<br>';
			}
		}
    }


}

<?php

class Service_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch service line item
     * @param type $id
     * @return array
     */
    function get_service_list_by_roles($group_id, $id = FALSE) {
        $sql = "SELECT sli.*,ud.full_name"
                . " FROM tbl_service_line_item as sli";
        $sql .= " LEFT JOIN aauth_users as admin ON admin.id = sli.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        $sql .= " WHERE sli.status=1";
        if ($group_id) {
            $sql .= " AND user_group.group_id = $group_id";
        }
        if ($id) {
            $sql .= " AND sli.user_id='" . $id . "'";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_service_line_item_data_for_proposal($owner_id,$data) {
        $no_of_panels = $data['no_of_panels'];
        $state = $data['prd_panel_data']['variant_state'];
        
        $sql = "SELECT lid.*,li.line_item,li.per_which_product "
                . " FROM tbl_service_line_item_data as lid";
        $sql .= " LEFT JOIN tbl_service_line_item as li ON li.id = lid.line_item_id";
        $sql .= " WHERE li.status=1 AND li.user_id='".$owner_id."' && lid.state IN ('All','" . $state . "') AND " . $no_of_panels . " BETWEEN lid.qty_from AND lid.qty_to";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

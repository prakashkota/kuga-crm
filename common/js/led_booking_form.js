
var led_booking_form_manager = function (options) {
    var self = this;
    this.lead_data = {};
    this.lead_uuid = (options.lead_uuid && options.lead_uuid != '') ? options.lead_uuid : {};
    this.booking_form_uuid = (options.booking_form_uuid && options.booking_form_uuid != '') ? options.booking_form_uuid : '';
    this.site_id = (options.site_id && options.site_id != '') ? options.site_id : '';
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.product_subtotal_excGST = 0;
    this.ae_subtotal_excGST = 0;
    this.total_incGST = 0;
    this.total_excGST = 0;
    this.total_GST = 0;
    this.signature_pad = [];
    this.signature_pad_ids = ['authorised_on_behalf_signature', 'authorised_by_behalf_sales_rep_signature', 'sales_rep_signature'];
    this.default_product_row_count = 0;
    this.calculator_items = null;
    this.product_types = options.product_types ? JSON.parse(options.product_types) : [];
    
    $("#authorised_on_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#authorised_by_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#booked_at").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });
    
    if(parseInt($('#cost_centre_id').val()) == 2){
        
        $('[name="business_details[entity_name]"]').change(function(){
            $('[name="get_nomination_form_details[energy_saver_details][entity_name]"]').val($(this).val());
            $('[name="site_assessor_form_details[business_name]"]').val($(this).val());
        });
        
        $('[name="business_details[abn]"]').change(function(){
            $('[name="get_nomination_form_details[energy_saver_details][abn]"]').val($(this).val());
            $('[name="site_assessor_form_details[abn]"]').val($(this).val());
        });
    
        $('[name="authorised_details[email]"]').change(function(){
            $('[name="get_nomination_form_details[energy_saver_details][email]"]').val($(this).val());
        });
    
        $('[name="authorised_details[contact_no]"]').change(function(){
            $('[name="get_nomination_form_details[energy_saver_details][contact_no]"]').val($(this).val());
            $('[name="site_assessor_form_details[contact_no]"]').val($(this).val());
        });
    
        $('[name="business_details[address]"]').change(function(){
            $('[name="get_nomination_form_details[certificate_provider_details][address]"]').val($(this).val());
            $('[name="site_assessor_form_details[address]"]').val($(this).val());
        });
    
        $('[name="business_details[description]"]').change(function(){
            $('[name="get_nomination_form_details[certificate_provider_details][description]"]').val($(this).val());
        });
    
        $('[name="booking_form[authorised_on_behalf][name]"]').change(function(){
            $('[name="get_nomination_form_details[authorised_details][name]"]').val($(this).val());
            $('[name="site_assessor_form_details[name]"]').val($(this).val());
        });
    
        $('[name="booking_form[authorised_on_behalf][date]"]').change(function(){
            $('[name="get_nomination_form_details[authorised_details][date]"]').val($(this).val());
        });
    
        $('[name="booking_form[authorised_by_behalf][date]"]').change(function(){
            $('[name="site_assessor_form_details[date]"]').val($(this).val());
            $('[name="site_assessor_form_details[date1]"]').val($(this).val());
        });
        
        $('[name="booking_form[authorised_by_behalf][date]"]').trigger('change');
    
        $("input[name='get_nomination_form_details[past_activities][is_nominated]']")[1].setAttribute('checked', 'checked');
    
        $('#authorised_by_behalf_name').change(function(){
            $('[name="site_assessor_form_details[name1]"]').val($(this).val());
            $('[name="site_assessor_form_details[name2]"]').val($(this).val());
        });
    
    }
    
    $(document).on('change','.item_name',function(){
        var prd_id = $(this).children("option:selected").attr('data-id');
        if(prd_id != null && prd_id != 'null' && prd_id != '' && prd_id != 'undefined' && prd_id != 'undefined'){
            $(this).parent().find('.item_id').val(prd_id);
            $(this).parent().find('.item_id').val(prd_id);
            $(this).removeClass('is-invalid');
            $(this).parent().removeClass('is-invalid');
            $(this).parent().parent().removeClass('is-invalid');
            $(this).next('.invalid-feedback').remove();
            $(this).parent().children('.invalid-feedback').remove();
        }
    });
    
    $(document).on('change','.item_bca_type',function(){
        var id = $(this).attr('data-id')
        var operatingHrs = $('.item_bca_type_'+id+' option:selected').attr('data-anual-op-hrs')
        $('.item_annual_op_hrs_'+id).val(operatingHrs);
    })
    
    $(document).on('change','.item_zone_classification ',function(){
        var zone = $(this).val();
        if(zone == 'Extra'){
            $(this).parent().parent().children('td:nth-child(2)').find('.item_id').removeAttr('required');
        }else{
            $(this).parent().parent().children('td:nth-child(2)').find('.item_id').attr('required','required');
        }
    });
        
    $(document).on('change','input,select',function(){
        var eleval = $(this).val();
        if(eleval != ''){
            $(this).removeClass('is-invalid');
            $(this).parent().removeClass('is-invalid');
            $(this).parent().parent().removeClass('is-invalid');
            $(this).next('.invalid-feedback').remove();
            $(this).parent().children('.invalid-feedback').remove();
            if($(this)[0].hasAttribute('required')){
                $(this).addClass('is-valid');
            }
        }
    });
    
    $(document).on('click','#add_more_led_products_row_btn',function(){
        self.create_led_product_row();
        self.default_product_row_count++;
    });
    
    
    self.handle_items();
    self.handle_cost_centre_change();
    
     $(document).on('change','.type_id',function(){
        var data_id = $(this).attr('data-id');
        $('.item_name_' + data_id).val('');
        $('.item_id_' + data_id).val('');
    });
    
    $('body').on('keyup keydown','.item_name',function(){
        var data_id = $(this).attr('data-id');
        var item_type = $('.type_id_' + data_id).val();
        console.log(item_type);
        $(this).autocomplete({
            delay: 300,
            autoSelect: false,
            autoFocus: false,
            minLength: 3,
            cache: false,
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/booking_form/fetch_products",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term,
                        request: 1,
					    cost_centre_id: $('#cost_centre_id').val(),
                        product_type_id: item_type,
                    },
                    success: function(data) {
                        response(data.products);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            search: function(event, ui) {
                $('.type_id_' + data_id).removeClass('is-invalid');
                if (item_type == '') {
                    this.value = '';
                    $('.type_id_' + data_id).addClass('is-invalid');
                    toastr["error"]("Please select product type first.");
                    return false;
                }
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.name);
                return false;
            },
            select: function(event, ui) {
                event.preventDefault();
                
                var product_name = ui.item.name;
                var brand = (ui.item.brand != '' && ui.item.brand != null) ? ui.item.brand : '';
                var model = (ui.item.model_no != '' && ui.item.model_no != null) ? ui.item.model_no : 'No Model';
                var item_id = (ui.item.item_id != '' && ui.item.item_id != null) ? ui.item.item_id : '';
                var sensor = (ui.item.sensor != '' && ui.item.sensor != null) ? parseInt(ui.item.sensor) : 0;
		        var type_id = (ui.item.type_id != '' && ui.item.type_id != null) ? parseInt(ui.item.type_id) : 8;
		        var data_id = $(this).attr('data-id');
		        
		        $('.type_id_' + data_id).val(type_id);
                $('.item_id_' + data_id).val(item_id);
                $('.item_name_'  + data_id).val(product_name);
                $('.item_brand_' + data_id).val(brand);
                $('.item_model_' + data_id).val(model);
                if(ui.item.sensor != '' && ui.item.sensor != null){
                    $('.item_sensor_' + data_id).val(sensor);
                }
                
                return false;
            }
        });
    });
    
    $(document).on('change','.item_name',function(){
        var name = $(this).val();
        
        if(name.length > 0){
            $(this).next('.item_id').prop('required',true);
        }else{
            $(this).next('.item_id').prop('required',false);
            $(this).next('.item_id').removeAttr('value');
        }
    })
    
    
    $(document).on('change','.ae_name',function(){
        if($(this).val() == 'Boom Lift' || $(this).val() == 'Site Inspection Required'){
            $('#boom_requirement_page').removeClass('hidden');
            $('#boom_requirement_area_1').attr("required",true);
            $('#boom_reach_up').attr("required",true);
            $('#boom_clearence').attr("required",true);
            $('#boom_reach_access').attr("required",true);
        }else{
           $('#boom_requirement_page').addClass('hidden');
           $('#boom_requirement_area_1').attr("required",false);
           $('#boom_reach_up').attr("required",false);
           $('#boom_clearence').attr("required",false);
           $('#boom_reach_access').attr("required",false);
        }    
    });
    self.fetch_calculator_items();
};


led_booking_form_manager.prototype.handle_cost_centre_change = function () {
    var self = this;
    $(document).on('change','#cost_centre_id',function(){
        var cost_centre_id = $(this).val();
        if(cost_centre_id == ''){
            toastr['error']('Please Select a Cost Centre Value');
            return false;
        }

        if(self.booking_form_uuid == ''){
            window.location.href = base_url + 'admin/booking_form/add_led_booking_form?deal_ref='+self.lead_uuid+'&site_ref='+self.site_id + '&cost_centre_id='+cost_centre_id;
            return false;
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'admin/booking_form/save_cost_centre',
            datatype: 'json',
            data: {
                'cost_centre_id' : cost_centre_id,
                'booking_form_uuid' : self.booking_form_uuid,
                'lead_uuid' : self.lead_uuid,
                'site_id': self.site_id,
                'type': 'LED'
            },
            success: function (res) {
                if (res.success == true) {
                    window.location.reload();
                }else{
                    toastr['error'](res.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

led_booking_form_manager.prototype.initialize = function () {
    var self = this;

    $('#save_booking_form').click(function () {
        self.save_booking_form();
    });

    $('#generate_booking_form_pdf').click(function () {
        self.save_booking_form(true);
    });

    //Handle Singature Pads
    $('.sign_create').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').hide();
        $('.sr-deal_actions').hide();
        $('#signatures').show();
        for (var i = 0; i < 4; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        window.scrollTo({top: 0, behavior: 'smooth'});
        //Resize Canvas for Signature pad because we assume user might have rotated the tablet
        self.resizeCanvas();
    });

    $('.sign_close').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').show();
        $('.sr-deal_actions').show();
        $('#signatures').hide();
        for (var i = 0; i < 3; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });

    $('#is_upfront').change(function () {
        $('#is_upfront').attr('checked', 'checked');
        $('#is_finance').removeAttr('checked');
    });

    $('#is_finance').change(function () {
        $('#is_finance').attr('checked', 'checked');
        $('#is_upfront').removeAttr('checked');
    });
    
    //Check box to copy details from Authroised to Accounts
    $('#same_as_authorised_details_btn').click(function () {
        if($(this)[0].checked == true){
            if($('#first_name').val() != ''){
                $('#acc_first_name').val($('#first_name').val()).trigger('change');
            }
            if($('#last_name').val() != ''){
                $('#acc_last_name').val($('#last_name').val()).trigger('change');
            }
            if($('#contact_no').val() != ''){
                $('#acc_contact_no').val($('#contact_no').val()).trigger('change');
            }
            if($('#landline').val() != ''){
                $('#acc_landline').val($('#landline').val()).trigger('change');
            }
            if($('#email').val() != ''){
                $('#acc_email').val($('#email').val()).trigger('change');
            }
        }else{
            if($('#first_name').val() != ''){
                $('#acc_first_name').val('');
            }
            if($('#last_name').val() != ''){
                $('#acc_last_name').val('');
            }
            if($('#contact_no').val() != ''){
                $('#acc_contact_no').val('');
            }
            if($('#landline').val() != ''){
                $('#acc_landline').val('');
            }
            if($('#email').val() != ''){
                $('#acc_email').val('');
            }
        }
    });

    self.handle_items();
    self.upload_image();
    self.show_image();
    self.hide_image();
    self.remove_signature_image();
    self.create_signature_pad();
    // console.log(self.booking_form_uuid);
    // if(self.booking_form_uuid){
    //     self.fetch_led_booking_form_data();
    // }
};

led_booking_form_manager.prototype.fetch_lead_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading lead data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                self.lead_data = stat.lead_data;

                $('#entity_name').val(self.lead_data.customer_company_name).trigger('change');
                $('#address').val(self.lead_data.address).trigger('change');
                $('#postcode').val(self.lead_data.postcode);

                $('#authorised_on_behalf_company_name').val(self.lead_data.customer_company_name);
                $('#authorised_on_behalf_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name).trigger('change');
                $('#authorised_on_behalf_position').val(self.lead_data.position);


                $('#first_name').val(self.lead_data.first_name);
                $('#last_name').val(self.lead_data.last_name);
                $('#contact_no').val(self.lead_data.customer_contact_no).trigger('change');
                $('#position').val(self.lead_data.position);
                $('#email').val(self.lead_data.customer_email).trigger('change');

                $("#authorised_on_behalf_date").val(moment(new Date()).format('DD/MM/YYYY')).trigger('change');
                $("#authorised_by_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
                $("#booked_at").val(moment(new Date()).format('DD/MM/YYYY'));

                self.initialize();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


led_booking_form_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

led_booking_form_manager.prototype.handle_items = function () {
    var self = this;
    
    $('.item_qty,.item_cost_excGST').change(function () {
        self.product_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        $('#booking_items_body tr').each(function () {
            //console.log("hrllo");
            var price = $(this).children().find(".item_cost_excGST").val();
            var qty = $(this).children().find(".item_qty").val();
            var total = price * qty;
            //console.log(total ,'=', price ,'*', qty)
            if (total != '' && !isNaN(total)) {
                $(this).children().find(".item_total_cost_excGst").val(total).trigger('change');
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST) + parseFloat(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST).trigger('change');
                $('#total_incGst').val(self.total_incGST);
            }else{
                total = (total == 0 && qty != '') ? total : '';
                $(this).children().find(".item_total_cost_excGst").val(0);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST).trigger('change');
                $('#total_incGst').val(self.total_incGST);
            }
        });
        //console.log('======',self.product_subtotal_excGST)
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST).trigger('change');
        $('#total_incGst').val(self.total_incGST).trigger('change');
        $('#total_Gst').val(self.total_GST).trigger('change');
    });
console.log('======',self.product_subtotal_excGST)
    $('.ae_qty,.ae_cost_excGST').change(function () {
        self.ae_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        for (var i = 0; i < 2; i++) {
            var price = $(".ae_cost_excGST_" + i).val();
            var qty = $(".ae_qty_" + i).val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(".ae_total_cost_excGst_" + i).val(total).trigger('change');;
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST) + parseFloat(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST).trigger('change');
            }else{
                $(".ae_total_cost_excGst_" + i).val(0);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }
        }
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST).trigger('change');
        $('#total_incGst').val(self.total_incGST).trigger('change');
        $('#total_Gst').val(self.total_GST).trigger('change');
    });


};

led_booking_form_manager.prototype.upload_image = function () {
    var self = this;
    $('.image_upload').change(function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'led_booking_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

led_booking_form_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/led_booking_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

led_booking_form_manager.prototype.hide_image = function () {
    var self = this;
    $('.image_close').click(function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};

led_booking_form_manager.prototype.create_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad2 = new SignaturePad(canvas2, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad3 = new SignaturePad(canvas3, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1, signaturePad2, signaturePad3];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();

        canvas2.width = canvas2.offsetWidth * ratio;
        canvas2.height = canvas2.offsetHeight * ratio;
        canvas2.getContext("2d").scale(ratio, ratio);
        signaturePad2.clear();

        canvas3.width = canvas3.offsetWidth * ratio;
        canvas3.height = canvas3.offsetHeight * ratio;
        canvas3.getContext("2d").scale(ratio, ratio);
        signaturePad3.clear();
    }

    //window.onresize = resizeCanvas;
    window.onorientationchange = resizeCanvas;
    resizeCanvas();

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        console.log(arr[(id - 1)]);
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 1)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 1)].toDataURL();
            var ele = $('#' + arr1[(id - 1)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
}

led_booking_form_manager.prototype.show_signature_image = function (ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.parent().removeClass('is-invalid');
            ele.prev().html('<img style="height:95px;" src="' + image + '" />');
            if(ele[0].name  == 'booking_form[authorised_on_behalf][signature]'){
                $('[name="get_nomination_form_details[authorised_details][signature]"]').val(image);
                $('#get_nomination_form_details_signature').attr('src',image);            
            }
            if(ele[0].name  == 'booking_form[authorised_by_behalf][sales_rep_signature]'){
                $('#site_assessor_form_details_signature').val(image);
                $('#site_assessor_form_details_signature').attr('src',image);            
            }
        } else {
            ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/led_booking_form_files/' + image + '" />');
            if(ele[0].name  == 'booking_form[authorised_on_behalf][signature]'){
                $('[name="get_nomination_form_details[authorised_details][signature]"]').val(image);
                $('#get_nomination_form_details_signature').attr('src',base_url + 'assets/uploads/led_booking_form_files/' + image);
            }
            if(ele[0].name  == 'booking_form[authorised_by_behalf][sales_rep_signature]'){
                $('#site_assessor_form_details_signature').val(image);
                $('#site_assessor_form_details_signature').attr('src',base_url + 'assets/uploads/led_booking_form_files/' + image);     
            }
        }
    }

    //Signateur Wrapper hide
    //ele.parent().parent().hide();
    //Show Image Wrapper
    //ele.parent().parent().next('div').show();
    //ele.parent().parent().next('div').children('img').attr('src', image);
};

led_booking_form_manager.prototype.remove_signature_image = function () {
    var self = this;
    $('.signature_image_close').click(function () {
        $(this).parent().prev('div').show();
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('img').attr('src', '');
    });
};

led_booking_form_manager.prototype.validate_booking_form = function () {
    var self = this;
    var flag = true;
    $('.invalid-feedback').remove();
    var elements = document.getElementById("led_booking_form").elements;
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                    $(elements[i]).prev('.item_name').addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
            } else {
                $(elements[i]).removeClass('is-invalid');
                $(elements[i]).parent().removeClass('is-invalid');
                $(elements[i]).parent().parent().removeClass('is-invalid');
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'checkbox') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }

    var payment_option_flag = true;
    var is_upfront_checked = document.getElementById('is_upfront').checked;
    var is_finance_checked = document.getElementById('is_finance').checked;
    $('#is_upfront').parent().parent().removeClass('is-invalid');
    $('#upfront_excGST').removeClass('is-invalid');
    $('#upfront_GST').removeClass('is-invalid');
    $('#upfront_incGST').removeClass('is-invalid');
    $('#finance_excGST').removeClass('is-invalid');
    $('#finance_GST').removeClass('is-invalid');
    $('#finance_incGST').removeClass('is-invalid');

    if (is_upfront_checked) {
        var upfront_excGST = document.getElementById('upfront_excGST').value;
        var upfront_GST = document.getElementById('upfront_GST').value;
        var upfront_incGST = document.getElementById('upfront_incGST').value;

        if (upfront_excGST == '') {
            $('#upfront_excGST').addClass('is-invalid');
            $('#upfront_excGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }

        if (upfront_GST == '') {
            $('#upfront_GST').addClass('is-invalid');
            $('#upfront_GST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }

        if (upfront_incGST == '') {
            $('#upfront_incGST').addClass('is-invalid');
            $('#upfront_incGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }
    } else if (is_upfront_checked == false && is_finance_checked == false) {
        $('#is_upfront').parent().parent().addClass('is-invalid');
        payment_option_flag = false;
    }
    
    $('#ec_bill_1').parent().parent().removeClass('is-invalid');
    $('#ec_bill_2').parent().parent().removeClass('is-invalid');
    if ($('#ec_bill_1').val() == '') {
        toastr.error('Please upload front of electricity bill');
        $('#ec_bill_1').parent().parent().addClass('is-invalid');
        flag = false;
    }else if ($('#ec_bill_2').val() == '') {
        toastr.error('Please upload back of electricity bill');
        $('#ec_bill_2').parent().parent().addClass('is-invalid');
        flag = false;
    }
    
    /**if (payment_option_flag && is_upfront_checked == false) {
        if (is_finance_checked) {
            var finance_excGST = document.getElementById('finance_excGST').value;
            var finance_GST = document.getElementById('finance_GST').value;
            var finance_incGST = document.getElementById('finance_incGST').value;

            if (finance_excGST == '') {
                $('#finance_excGST').addClass('is-invalid');
                $('#finance_excGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }

            if (finance_GST == '') {
                $('#finance_GST').addClass('is-invalid');
                $('#finance_GST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }

            if (finance_incGST == '') {
                $('#finance_incGST').addClass('is-invalid');
                $('#finance_incGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }
        }
    }*/

    flag = (payment_option_flag == false) ? false : flag;

    return flag;
};

led_booking_form_manager.prototype.save_booking_form = function (download) {
    var self = this;

    /** //Convert Signature to Images
     var signature_pad = self.signature_pad;
     var signature_pad_ids = self.signature_pad_ids;
     for (var i=0; i < signature_pad.length; i++) {
     if (signature_pad[i].isEmpty()) {
     } else {
     var dataURL = signature_pad[i].toDataURL();
     var ele = $('#' + signature_pad_ids[i]);
     self.show_signature_image(ele, dataURL);
     }
     } */

    if(download){
        var flag = self.validate_booking_form();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Please fill the fields marked in red');
            if($(".is-invalid").length){
                $('html, body').animate({
                    'scrollTop' : $(".is-invalid").position().top
                },2000);    
            }
            return false;
        }
    }

    if (self.booking_form_uuid == '') {
        var uuid = self.create_uuid();
        self.booking_form_uuid = uuid;
    }

    var formData = $('#led_booking_form').serialize();
    formData += '&uuid=' + self.booking_form_uuid;
    formData += '&lead_id=' + self.lead_data.id;
    formData += '&site_id=' + self.site_id;

    $.ajax({
        url: base_url + 'admin/booking_form/save_led_booking_form',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]('Saving Quote Data, Please Wait....');
            $("#save_booking_form").attr("disabled", "disabled");
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.booking_form_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/booking_form/edit_led_booking_form/' + self.booking_form_uuid);
                }
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["success"](response.status);
                    setTimeout(function(){
                        window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                    },1500);
                }
            } else {
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.fetch_led_booking_form_data = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/booking_form/fetch_led_booking_form_data',
        datatype: 'json',
        data: {uuid: self.booking_form_uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var booking_data = stat.booking_data;
                self.handle_led_booking_form_data(booking_data);
                if(booking_data.simpro_job_id != null && booking_data.simpro_job_id != ''){
                    $('.sr-deal_actions').hide();
                } else if (booking_data.kuga_job_id != null && booking_data.kuga_job_id != '') {
                    //$('.sr-deal_actions').hide();
                    $('#cost_centre_id').attr('disabled','disabled');
                    self.initialize();
                } else{
                    self.initialize();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.handle_led_booking_form_data = function (data) {
    var self = this;

    //Handle Business Details
    var business_details = JSON.parse(data.business_details);
    console.log(business_details);
    for (var key in business_details) {
        if(key == 'leased_or_owned' || key == 'bca_hours'){
            $("#"+key).val(business_details[key]);
        }else{
            if($("#"+key)){
              $("#"+key).val(business_details[key]).trigger('change');;  
            }else{
              $("input[name='business_details[" + key + "]']").val(business_details[key]).trigger('change');;
            }
        }
    }

    //Handle Authorised Details
    var authorised_details = JSON.parse(data.authorised_details);
    for (var key in authorised_details) {
        if($("#"+key)){
            $("#"+key).val(authorised_details[key]).trigger('change');;  
        }else{
            $("input[name='authorised_details[" + key + "]']").val(authorised_details[key]).trigger('change');;
        }
    }

    //Handle Account Details
    var account_details = JSON.parse(data.account_details);
    for (var key in account_details) {
        if($("#"+key)){
            $("#"+key).val(account_details[key]);  
        }else{
            $("input[name='account_details[" + key + "]']").val(account_details[key]);
        }
    }

    //Handle Space Type
    var space_type = JSON.parse(data.space_type);
    for (var key in space_type) {
        $("select[name='space_type[" + key + "]']").val(space_type[key]);
    }

    //Handle Ceiling Height
    var ceiling_height = JSON.parse(data.ceiling_height);
    for (var key in ceiling_height) {
        $("input[name='ceiling_height[" + key + "]']").val(ceiling_height[key]);
    }

    //Handle Boom Req
    var boom_req = JSON.parse(data.boom_req);
    for (var key in boom_req) {
        $("input[name='boom_req[" + key + "]']").val(boom_req[key]);
    }

    //Handle Product
     var product = JSON.parse(data.product);
    for (var key in product) {
        for (var i = 0; i < product[key].length; i++) {
            if(key == 'item_name' && product['item_name'][i] != ''){
                $('#add_more_led_products_row_btn').click();
                
            }
        }
    }
    
    for (var key in product) {
        for (var i = 0; i < product[key].length; i++) {
            if(product['item_name'] !== undefined){
                if(product['item_name'][i] != ''){
                    $("." + key + "_" + i).val(product[key][i]).trigger('change');
                }
            }
        }
    }
    

    //Handle Access Equipments
    var ae = JSON.parse(data.ae);
    for (var key in ae) {
        for (var i = 0; i < ae[key].length; i++) {
            $("." + key + "_" + i).val(ae[key][i]);
            if(ae[key][i] == 'Boom Lift'){
                $('#boom_requirement_page').removeClass('hidden');
                $('#boom_requirement_area_1').attr("required",true);
                $('#boom_reach_up').attr("required",true);
                $('#boom_clearence').attr("required",true);
                $('#boom_reach_access').attr("required",true);
            }
        }
    }
    $('.ae_qty').trigger('change');
    
    //Handle Booking Form Data
    var booking_form = JSON.parse(data.booking_form);
    for (var key in booking_form) {
        if (key == 'authorised_on_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'signature') {
                    self.show_signature_image($('#authorised_on_behalf_signature'), booking_form[key]['signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'authorised_by_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'sales_rep_signature') {
                    self.show_signature_image($('#authorised_by_behalf_sales_rep_signature'), booking_form[key]['sales_rep_signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'sales_rep_signature') {
            self.show_signature_image($('#sales_rep_signature'), booking_form[key]);
        } else if (key == 'terms_and_conditions') {
            for (var i = 0; i < 4; i++) {
                if (booking_form[key][i] == 'on') {
                    $('#terms_and_conditions_' + i).attr('checked', 'checked');
                }
            }
        } else if (key == 'is_upfront' || key == 'is_finance' || key == 'is_heers_panel' || key == 'is_heers_batten' || key == 'is_more_products') {
            $("#" + key).attr('checked', 'checked');
        } else {
            $("#" + key).val(booking_form[key]);
        }
    }

    //Handle images
    var booking_form_image = JSON.parse(data.booking_form_image);
    for (var key in booking_form_image) {
        if (booking_form_image[key] != '') {
            $("#" + key).val(booking_form_image[key]);
            self.show_image($("#" + key), booking_form_image[key]);
        }
    }

    //Handle Prev Upgrade
    var prev_upgrade = JSON.parse(data.prev_upgrade);
    for (var key in prev_upgrade) {
        if (key == 'is_occured_at_premise') {
            var is_checked = (prev_upgrade[key] == '1') ? 'yes' : 'no';
            $("#" + key + "_" + is_checked).attr('checked', 'checked');
        }
        $("#" + key).val(prev_upgrade[key]);
    }

    //Handle Customer Check List Some Issue in Ipad so do it with for loop associative key in integer
    var customer_check_list = JSON.parse(data.customer_check_list);
    if (customer_check_list.hasOwnProperty('notes')) {
        $("#customer_check_list_notes").val(customer_check_list['notes']);
    }
    for(var i=0; i< 17; i++) {
        if ($("#customer_check_list_" + i)) {
            if(customer_check_list[i] == 'on'){
                $("#customer_check_list_" + i).attr('checked', 'checked');
            }
        }
    }
    
    //Handle GET Nomination Form Details
    var get_nomination_form_details = JSON.parse(data.get_nomination_form_details);
    for (var key in get_nomination_form_details) {
        for (var key1 in get_nomination_form_details[key]) {
            if(key1 == 'is_nominated'){
                if(get_nomination_form_details[key][key1] == '1'){
                    $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']")[0].setAttribute('checked', 'checked');          
                }else{
                    $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']")[1].setAttribute('checked', 'checked');      
                }          
            }else if(key1 == 'signature'){
                    $("#get_nomination_form_details_signature").attr('src',base_url + 'assets/uploads/led_booking_form_files/' + get_nomination_form_details[key][key1]);
                    $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']").val(get_nomination_form_details[key][key1]);   
            }else{
                $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']").val(get_nomination_form_details[key][key1]);     
            } 
        }
    }
    
    if(data.cost_centre_id != null){
        $('#cost_centre_id').val(data.cost_centre_id);
    }
    
    setTimeout(function(){
        $('.item_qty').trigger('change');
    },1300);

};

led_booking_form_manager.prototype.save_booking_form_to_simpro = function () {
    var self = this;
    var url = base_url + 'admin/booking_form/led_booking_form_pdf_download/' + self.booking_form_uuid;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]("Saving Data to Job CRM. Please Wait...");
            $("#save_booking_form").attr("disabled", "disabled");
            $("#generate_booking_form_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            } else {
                toastr["error"](response.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
            toastr.remove();
            //Getting this weird issue but still boking form was completed save on simpro 
            if(xhr.statusText == "OK"){
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            }else{
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};

led_booking_form_manager.prototype.resizeCanvas = function () {
    var self = this;

    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var signaturePad1 = self.signature_pad[0];
    var signaturePad2 = self.signature_pad[1];
    var signaturePad3 = self.signature_pad[2];

    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas1.width = canvas1.offsetWidth * ratio;
    canvas1.height = canvas1.offsetHeight * ratio;
    canvas1.getContext("2d").scale(ratio, ratio);
    signaturePad1.clear();

    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    signaturePad2.clear();

    canvas3.width = canvas3.offsetWidth * ratio;
    canvas3.height = canvas3.offsetHeight * ratio;
    canvas3.getContext("2d").scale(ratio, ratio);
    signaturePad3.clear();

};


led_booking_form_manager.prototype.fetch_calculator_items = function() {
    var self = this;
    $.ajax({
        url: base_url + 'assets/calculator/vic.json?v='+Math.random(),
        type: 'get',
        crossDomain: true,
        dataType: "json",
        cahce:false,
        success: function(stat) {
            self.calculator_items = [];
            self.calculator_items = stat;
            if (self.booking_form_uuid == '' && self.lead_uuid != '') {
                var data = {};
                data.uuid = self.lead_uuid;
                data.site_id = self.site_id;
                self.fetch_lead_data(data);
        
                $('#add_more_led_products_row_btn').click();
                $('#add_more_led_products_row_btn').click();
                $('#add_more_led_products_row_btn').click();
                $('#add_more_led_products_row_btn').click();
                $('#add_more_led_products_row_btn').click();
            }else if (self.booking_form_uuid != '') {
                self.fetch_led_booking_form_data();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


led_booking_form_manager.prototype.create_led_product_row = function() {
    var self = this;
    
    var count = self.default_product_row_count;
   var tr = document.createElement('tr');
   
   var td_room_area = document.createElement('td');
   td_room_area.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var room_area_input = document.createElement('input');
   room_area_input.setAttribute('type','text');
   room_area_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   room_area_input.className = 'item_room_area item_room_area_'+count;
   room_area_input.setAttribute('name','product[item_room_area][]');
   if(count == 0){
       room_area_input.setAttribute('required',true);
   }
   td_room_area.appendChild(room_area_input);
   
   var td_ch = document.createElement('td');
   td_ch.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ch_input = document.createElement('input');
   ch_input.setAttribute('type','text');
   ch_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ch_input.className = 'item_ceiling_height item_ceiling_height_'+count;
   ch_input.setAttribute('name','product[item_ceiling_height][]');
   ch_input.setAttribute('data-id',count);
   if(count == 0){
       ch_input.setAttribute('required',true);
   }
   td_ch.appendChild(ch_input);
   
   var td_bca = document.createElement('td');
   td_bca.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var bca_select = document.createElement('select');
   bca_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   bca_select.className = 'item_bca_type item_bca_type_'+count;
   bca_select.setAttribute('name','product[item_bca_type][]');
   bca_select.setAttribute('data-id',count);
   if(count == 0){
       bca_select.setAttribute('required',true);
   }
   
   var td_annual_op = document.createElement('td');
   td_annual_op.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var annual_input = document.createElement('input');
   annual_input.setAttribute('type','text');
   annual_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   annual_input.className = 'item_annual_op_hrs item_annual_op_hrs_'+count;
   annual_input.setAttribute('name','product[item_annual_op_hrs][]');
   annual_input.setAttribute('readonly','readonly');
   td_annual_op.appendChild(annual_input);
   
   
   var bca_types = self.calculator_items['bca_types'];
   for(var key in bca_types){
       var option = document.createElement('option');
       option.innerHTML = bca_types[key]['type_of_space'];
       option.setAttribute('value',bca_types[key]['type_of_space']);
       option.setAttribute('data-anual-op-hrs',bca_types[key]['op_hrs_per_yr']);
       bca_select.appendChild(option);
   }
   td_bca.appendChild(bca_select);
   
   var td_lamp_type = document.createElement('td');
   td_lamp_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var lt_select = document.createElement('select');
   lt_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   lt_select.className = 'item_lamp_type item_lamp_type_'+count;
   lt_select.setAttribute('name','product[item_lamp_type][]');
   lt_select.setAttribute('data-id',count);
   if(count == 0){
       lt_select.setAttribute('required',true);
   }
   var lamp_types = self.calculator_items['lamp_types'];
   for(var key in lamp_types){
       var option = document.createElement('option');
       option.innerHTML = lamp_types[key];
       option.setAttribute('value',lamp_types[key]);
       lt_select.appendChild(option);
   }
   td_lamp_type.appendChild(lt_select);
   
   var td_nm_watts = document.createElement('td');
   td_nm_watts.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var nm_input = document.createElement('input');
   nm_input.setAttribute('type','text');
   nm_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   nm_input.className = 'item_nom_watts item_nom_watts_'+count;
   nm_input.setAttribute('name','product[item_nom_watts][]');
   nm_input.setAttribute('data-id',count);
   if(count == 0){
       nm_input.setAttribute('required',true);
   }
   td_nm_watts.appendChild(nm_input);
   
   
   var td_control_system = document.createElement('td');
   td_control_system.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var cs_select = document.createElement('select');
   cs_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   cs_select.className = 'item_control_system item_control_system_'+count;
   cs_select.setAttribute('name','product[item_control_system][]');
   cs_select.setAttribute('data-id',count);
   if(count == 0){
       cs_select.setAttribute('required',true);
   }
   var control_systems = self.calculator_items['control_systems'];
   for(var key in control_systems){
       var option = document.createElement('option');
       option.innerHTML = control_systems[key];
       option.setAttribute('value',control_systems[key]);
       cs_select.appendChild(option);
   }
   td_control_system.appendChild(cs_select);
   
   
   var td_air_con = document.createElement('td');
   td_air_con.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ac_select = document.createElement('select');
   ac_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ac_select.className = 'item_air_con item_air_con_'+count;
   ac_select.setAttribute('name','product[item_air_con][]');
   ac_select.setAttribute('data-id',count);
   if(count == 0){
       ac_select.setAttribute('required',true);
   }
   var air_con = ['Yes','No'];
   for(var key in air_con){
       var option = document.createElement('option');
       option.innerHTML = air_con[key];
       option.setAttribute('value',air_con[key]);
       ac_select.appendChild(option);
   }
   td_air_con.appendChild(ac_select);
   
   
   var td_no_of_lamps = document.createElement('td');
   td_no_of_lamps.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var no_of_lamps_input = document.createElement('input');
   no_of_lamps_input.setAttribute('type','text');
   no_of_lamps_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   no_of_lamps_input.className = 'item_existing_no_of_lamps item_existing_no_of_lamps_'+count;
   no_of_lamps_input.setAttribute('name','product[item_existing_no_of_lamps][]');
   no_of_lamps_input.setAttribute('data-id',count);
   if(count == 0){
       no_of_lamps_input.setAttribute('required',true);
   }
   td_no_of_lamps.appendChild(no_of_lamps_input);
   
   
   var td_activity_type = document.createElement('td');
   td_activity_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var activity_type_select = document.createElement('select');
   activity_type_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   activity_type_select.className = 'item_activity_type item_activity_type_'+count;
   activity_type_select.setAttribute('name','product[item_activity_type][]');
   activity_type_select.setAttribute('data-id',count);
   if(count == 0){
       activity_type_select.setAttribute('required',true);
   }
   var activity_types = self.calculator_items['activity_types'];
   for(var key in activity_types){
       var option = document.createElement('option');
       option.innerHTML = activity_types[key];
       option.setAttribute('value',activity_types[key]);
       activity_type_select.appendChild(option);
   }
   td_activity_type.appendChild(activity_type_select);
   
   
   var td_product_type = document.createElement('td');
   td_product_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_type_select = document.createElement('select');
   product_type_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_type_select.className = 'type_id type_id_'+count;
   product_type_select.setAttribute('name','product[type_id][]');
   product_type_select.setAttribute('data-id',count);
   if(count == 0){
       product_type_select.setAttribute('required',true);
   }
   var product_types = self.product_types;
   for(var key in product_types){
       var option = document.createElement('option');
       option.innerHTML = product_types[key]['type_name'];
       option.setAttribute('value',product_types[key]['type_id']);
       product_type_select.appendChild(option);
   }
   td_product_type.appendChild(product_type_select);
   
   var td_product_name = document.createElement('td');
   td_product_name.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px; min-width:100px;');
   
   var product_input = document.createElement('input');
   product_input.setAttribute('type','text');
   product_input.setAttribute('style','padding:5px;  min-width:100px; width:100%; height: inherit;');
   product_input.className = 'item_name item_name_'+count;
   product_input.setAttribute('name','product[item_name][]');
   product_input.setAttribute('data-id',count);
   if(count == 0){
       product_input.setAttribute('required',true);
   }
   td_product_name.appendChild(product_input);
   td_product_name.innerHTML += '<input type="hidden" class="item_id item_id_'+count+'" name="product[item_id][]" />';
   
   var td_sensor = document.createElement('td');
   td_sensor.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var sensor_select = document.createElement('select');
   sensor_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   sensor_select.className = 'item_sensor item_sensor_'+count;
   sensor_select.setAttribute('name','product[item_sensor][]');
   sensor_select.setAttribute('data-id',count);
   if(count == 0){
       sensor_select.setAttribute('required',true);
   }
   var option = document.createElement('option');
   option.innerHTML = 'Please Select';
   option.setAttribute('value','');
   sensor_select.appendChild(option);
       
   var sensors = [{'id': 1 , 'name' : 'Yes'},{'id': 0 , 'name' : 'No'}];
   for(var key in sensors){
       var option = document.createElement('option');
       option.innerHTML = sensors[key]['name'];
       option.setAttribute('value',sensors[key]['id']);
       sensor_select.appendChild(option);
   }
   td_sensor.appendChild(sensor_select);
   
   var td_product_qty = document.createElement('td');
   td_product_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_qty_input = document.createElement('input');
   product_qty_input.setAttribute('type','text');
   product_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_qty_input.className = 'item_qty item_qty_'+count;
   product_qty_input.setAttribute('name','product[item_qty][]');
   product_qty_input.setAttribute('data-id',count);
   if(count == 0){
       product_qty_input.setAttribute('required',true);
   }
   td_product_qty.appendChild(product_qty_input);
   
   
   var td_product_unit_cost = document.createElement('td');
   td_product_unit_cost.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_unit_cost_input = document.createElement('input');
   product_unit_cost_input.setAttribute('type','text');
   product_unit_cost_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_unit_cost_input.className = 'item_cost_excGST item_cost_excGST_'+count;
   product_unit_cost_input.setAttribute('name','product[item_cost_excGST][]');
   product_unit_cost_input.setAttribute('data-id',count);
   if(count == 0){
       product_unit_cost_input.setAttribute('required',true);
   }
   td_product_unit_cost.appendChild(product_unit_cost_input);
   
   
   var td_product_total_cost = document.createElement('td');
   td_product_total_cost.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_total_cost_input = document.createElement('input');
   product_total_cost_input.setAttribute('type','text');
   product_total_cost_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_total_cost_input.className = 'item_total_cost_excGst item_total_cost_excGst_'+count;
   product_total_cost_input.setAttribute('name','product[item_total_cost_excGst][]');
   product_total_cost_input.setAttribute('data-id',count);
   if(count == 0){
       product_total_cost_input.setAttribute('required',true);
   }
   td_product_total_cost.appendChild(product_total_cost_input);
   
   
   tr.appendChild(td_room_area);
   tr.appendChild(td_ch);
   tr.appendChild(td_bca);
   tr.appendChild(td_annual_op);
   tr.appendChild(td_lamp_type);
   tr.appendChild(td_nm_watts);
   tr.appendChild(td_control_system);
   tr.appendChild(td_air_con);
   tr.appendChild(td_no_of_lamps);
   tr.appendChild(td_activity_type);
   tr.appendChild(td_product_type);
   tr.appendChild(td_product_name);
   tr.appendChild(td_sensor);
   tr.appendChild(td_product_qty);
   tr.appendChild(td_product_unit_cost);
   tr.appendChild(td_product_total_cost);
   
   document.getElementById('booking_items_body').appendChild(tr);
};


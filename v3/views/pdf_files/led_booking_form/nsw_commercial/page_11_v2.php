<table class="pdf_page hidden" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" id="boom_requirement_page">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="300" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"></th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Reach Up:<br>(Ceiling Height)</th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Clearance:<br>(Height of item blocking access</th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Reach Access:<br>(Distance form the aisle to Light)</th>
                </tr>
                <tr>
                    <td width="300" align="center" valign="top" style="padding:13px 0; font-size:20px"><strong>Boom Requirement</strong></td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <strong><?php echo $booking_form->boom_reach_up; ?></strong>
                    </td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <strong><?php echo $booking_form->boom_clearence; ?></strong>
                    </td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <strong><?php echo $booking_form->boom_reach_access; ?></strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Boom Requirement – Area Photo 1</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Boom Requirement – Area Photo 2</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <?php if ($booking_form_image->boom_requirement_area_1 != '') { ?>
                            <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->boom_requirement_area_1; ?>" width="391" height="290" />
                        <?php } else { ?>
                            <img src="" width="391" height="290" />
                        <?php } ?>
                    </td>
                    <td align="center" valign="top" style="padding:13px 0">
                        <?php if ($booking_form_image->boom_requirement_area_2 != '') { ?>
                            <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->boom_requirement_area_2; ?>" width="391" height="290" />
                        <?php } else { ?>
                            <img src="" width="391" height="290" />
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td height="550">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
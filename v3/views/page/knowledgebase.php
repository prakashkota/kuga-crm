<!-- MAIN SECTION -->
<div class="container featured-area-dark padding-30">
    <div class="row">
        <div class="col-lg-8 padding-20">
            <!-- LATEST VIDEO TUTORIAL AREA -->
            <!--<div class="row margin-bottom-30">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="featured-box">
                            <div class="fb-heading">Latest Videos</div>
                            <hr class="style-three">
                            <div class="fb-content">
                                <iframe src="https://www.youtube.com/watch?v=pWk-lEK04hM" width="100%" height="360"
                                        frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END LATEST VIDEO TUTORIAL AREA -->
            <!-- ARTICLE CATEGORIES SECTION -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="fb-heading">Article Categories</div>
                    <hr class="style-three">
                    <div class="row">
                        <?php
                        if (!empty($article_categories)) {
                            foreach ($article_categories as $key => $value) {
                                ?>
                                <div class="col-lg-6 margin-bottom-20">
                                    <div class="fat-heading-abb"> <a href="<?php echo site_url('admin/knowledgebase/'.$value['category_slug']); ?>"><i class="fa fa-folder"></i> <?php echo $value['category_name']; ?> <span class="cat-count">(<?php echo count($articles[$value['id']]); ?>)</span></a>
                                    </div>
                                    <div class="fat-content-small padding-left-30">
                                        <ul><?php
                                            if (!empty($articles[$value['id']])) {
                                                foreach ($articles[$value['id']] as $key => $value) {
                                                    ?>
                                                    <li> <a href="<?php echo site_url('admin/knowledgebase/'.$value['category_slug'].'/'.$value['article_slug']); ?>"><i class="fa fa-file-text-o"></i><?php echo $value['article_title']; ?></a></li>
                                                <?php
                                                }
                                            } else {
                                                echo "No Articles Found";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END ARTICLES CATEOGIRES SECTION -->

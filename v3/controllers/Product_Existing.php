<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Product_Existing Controller
 * @version 1.0
 */
class Product_Existing extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("components");
        $this->load->model("Product_Model", "product");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function led_manage() {
        $data = array();

        $data['meta_title'] = 'Manage Existing LED Products';
        
        $product_list = $this->product->get_existing_led_products();
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $product_types = $this->product->get_product_types(" WHERE t1.parent_id = 1");
        
        $data['product_list'] = $product_list;
        $data['product_types'] = $product_types;
        $all_states = array_column($states, 'state_postal');
        $custom_state = array('All');
        $data['states'] = array_merge($custom_state,$all_states);
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_existing_manage');
        $this->load->view('partials/footer');
    }

    public function led_add() {
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('ep_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('ep_wattage', 'Wattage', 'required');
            $this->form_validation->set_rules('ep_rated_life', 'Rated Life', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                $insert_data['created_at'] = date('Y-m-d H:i:s');

                //Insert Product Item
                $this->common->insert_data('tbl_led_existing_products', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product created successfuly.</div>');
                    redirect('admin/product/led_existing/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id = 1");

        $all_states = array_column($states, 'state_postal');
        $custom_state = array('All');
        $data['states'] = array_merge($custom_state,$all_states);
        $data['product_application'] = unserialize(product_application);

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_existing_form');
        $this->load->view('partials/footer');
    }

    public function led_edit($id) {
        //Restricitng and Fetching of Data based on user logged In group   
        $product_data = $this->common->fetch_where('tbl_led_existing_products', '*', array('ep_id' => $id));

        if (empty($product_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Product found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }*/


        $data = array();

        if (!empty($product_data)) {
            $data['product_data'] = $product_data[0];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('ep_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('ep_wattage', 'Wattage', 'required');
            $this->form_validation->set_rules('ep_rated_life', 'Rated Life', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                //$update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Product Item
                $stat = $this->common->update_data('tbl_led_existing_products', ['ep_id' => $id], $update_data);

                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product updated successfuly.</div>');
                    redirect('admin/product/led_existing/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id = 1");

        $all_states = array_column($states, 'state_postal');
        $custom_state = array('All');
        $data['states'] = array_merge($custom_state,$all_states);
        $data['product_application'] = unserialize(product_application);
        $data['pid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_existing_form');
        $this->load->view('partials/footer');
    }

    public function led_delete($id = FALSE) {
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }*/

        if ($id) {
            $products = $this->common->fetch_where('tbl_led_existing_products', '*', array('ep_id' => $id));
            if (empty($products)) {
                redirect('admin/dashboard');
            } else {
                $stat = $this->common->update_data('tbl_led_existing_products', array('ep_id' => $id), array('status' => 0));
                if ($stat) {
                    if ($stat == -1) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Product cannot be deleted it is linked to some proposal.</div>');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Existing Product Deleted Successfuly.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
            redirect('admin/product/led_existing/manage');
        }
    }

}

<?php foreach ($thread as $li) { ?>
    <li class="timeline-inverted js-messgae-list" data-id="<?php echo $li['id']; ?>">
        <div class="timeline-badge warning"><i class="fa fa-envelope"></i></div>
        <div class="timeline-panel">
            <div class="timeline-heading">
                <h4 class="timeline-title"><?php echo $li['full_name'] ?>
                    <span class="pull-right time-line-msg-date">
                        <?php echo date('d M Y', strtotime($li['created_at'])); ?>
                        <?php echo 'at ' . date('h:i A', strtotime($li['created_at'])); ?>
                    </span>
                </h4>
            </div>
            <div class="timeline-body">
                <p><?php echo $li['message']; ?></p>
                <p>
                    <?php
                    $extension = strtolower(pathinfo($li['file'], PATHINFO_EXTENSION));
                    if (in_array($extension, ['jpeg', 'jpg', 'png', 'gif'])) {
                        ?>
                        <a target="_blank" href="<?php echo site_url('assets/uploads/issue_files/' . $li['file']); ?>">
                            <img src="<?php echo site_url('assets/uploads/issue_files/' . $li['file']); ?>" style="height:50px;width:50px;">
                        </a> 
                    <?php } else {
                        ?>
                        <a target="_blank" href="<?php echo site_url('assets/uploads/issue_files/' . $li['file']); ?>"><?php echo $li['file']; ?></a> 
                    <?php } ?></p>
            </div>
        </div>
    </li>
<?php } ?>

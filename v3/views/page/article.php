<!-- MAIN SECTION -->
  <div class="container featured-area-default padding-30">
    <div class="row">
      <div class="col-md-8 padding-20">
        <div class="row">
          <!-- BREADCRUMBS -->
          <div class="breadcrumb-container">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/knowledgebase'); ?>"><i class="fa fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/knowledgebase/'.$article['category_slug']); ?>"><?php echo ucfirst($article['category_name']); ?></a></li>
              <li class="breadcrumb-item active"><?php echo ucfirst($article['article_title']); ?></li>
            </ol>
          </div>
          <!-- END BREADCRUMBS -->
          <!-- ARTICLE  -->
          <div class="card card-default">
            <div class="article-heading margin-bottom-5">
              <a href="#"><i class="fa fa-pencil-square-o"></i> How to change account password?</a>
            </div>
            <div class="article-info">
              <div class="art-date"><i class="fa fa-calendar-o"></i> <?php echo date('m/d/Y',strtotime($article['created_at'])); ?></div>
              <div class="art-category"><a href="<?php echo site_url('admin/knowledgebase/'.$article['category_slug']); ?>"><i class="fa fa-folder"></i> <?php echo ucfirst($article['category_name']); ?> </a></div>
            </div>
            <div class="article-content">
                <?php echo $article['article_description']; ?>
            </div>
          </div>
          <!-- END ARTICLE -->
    </div>
  </div>
  <!-- END MAIN SECTION -->

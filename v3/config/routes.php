<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
 * */

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//General Routes
$route['default_controller'] = 'auth/index';
$route['changelog'] = 'auth/changelog';

//Auth and Dashboard Routes
$route['admin'] = 'Auth/dashboard';
$route['admin/login'] = 'Auth/login';
$route['admin/login_ajax'] = 'Auth/login_ajax';
$route['admin/logout'] = 'Auth/logout';
$route['admin/dashboard'] = 'Auth/dashboard';
$route['admin/ban_unban_user/(:num)/(:num)'] = 'Auth/ban_unban_user/$1/$2';
$route['account/forgot-password'] = 'Auth/forgot_password';
$route['account/reset_password/(:any)'] = 'Auth/reset_password/$1';
$route['admin/change_availability_status'] = 'Auth/change_availability_status';
$route['admin/delete_user'] = 'Auth/delete_user';
$route['admin/set_current_latlng'] = 'Auth/set_current_latlng';
$route['admin/create_job_crm_login'] = 'Auth/create_job_crm_login';

//Settings
$route['admin/settings/lead-stages/manage'] = 'Lead/manage_lead_stages';
$route['admin/settings/lead-stages/add'] = 'Lead/add_lead_stages';
$route['admin/settings/lead-stages/edit/(:num)'] = 'Lead/add_lead_stages/$1';
$route['admin/settings/lead-stages/delete/(:num)'] = 'Lead/delete_lead_stage/$1';

//Roles Related
$route['admin/roles/add'] = 'Auth/roles_add';
$route['admin/roles/manage'] = 'Auth/manage_roles';
$route['admin/roles/manage-permission/(:num)'] = 'Auth/roles_manage_permission/$1';

//Settings Related
$route['admin/settings/proposal-stages/manage/(:num)'] = 'Proposal/manage_proposal_stages/$1';
$route['admin/settings/proposal-stages/add/(:num)'] = 'Proposal/add_proposal_stages/$1';
$route['admin/settings/proposal-stages/edit/(:num)/(:num)'] = 'Proposal/add_proposal_stages/$1/$2';
$route['admin/settings/proposal-stages/delete/(:num)/(:num)'] = 'Proposal/delete_proposal_stage/$1/$2';
$route['admin/settings/change-password'] = 'Auth/change_password';
$route['admin/settings/change-password/(:num)'] = 'Auth/change_password/$1';
$route['admin/settings/simpro_sales/manage'] = 'Proposal/manage_simpro_sales_settings';
$route['admin/settings/simpro_sales/save'] = 'Proposal/save_simpro_sales_settings';
$route['admin/settings/upload_simpro_sales_dump'] = 'Proposal/upload_simpro_sales_dump';


//Product LED Routes
$route['admin/product/led/add'] = 'Product/led_add';
$route['admin/product/led/edit/(:num)'] = 'Product/led_edit/$1';
$route['admin/product/led/manage'] = 'Product/led_manage';
$route['admin/product/led/delete/(:num)'] = 'Product/led_delete/$1';

//Product LED EXISTING Routes
$route['admin/product/led_existing/add'] = 'Product_Existing/led_add';
$route['admin/product/led_existing/edit/(:num)'] = 'Product_Existing/led_edit/$1';
$route['admin/product/led_existing/manage'] = 'Product_Existing/led_manage';
$route['admin/product/led_existing/delete/(:num)'] = 'Product_Existing/led_delete/$1';


//Product Solar Routes
$route['admin/product/solar/add'] = 'Product/solar_add';
$route['admin/product/solar/edit/(:num)'] = 'Product/solar_edit/$1';
$route['admin/product/solar/manage'] = 'Product/solar_manage';
$route['admin/product/solar/delete/(:num)'] = 'Product/solar_delete/$1';

//Product Addtional Routes
$route['admin/product/fetch_product_variant_data'] = 'Product/fetch_product_variant_data';
$route['admin/product/upload_datasheet'] = 'Uploader/upload_file';
$route['admin/product/import_csv'] = 'Product/import_csv';
$route['admin/product/export_csv'] = 'Product/export_csv';
$route['admin/product/fetch_led_new_product_by_existing'] = 'Product/fetch_led_new_product_by_existing';
$route['admin/product/fetch_solar_product_by_type'] = 'Product/fetch_solar_product_by_type';


//Product Types Routes
$route['admin/product-type/add'] = 'Product/add_product_type';
$route['admin/product-type/edit/(:num)'] = 'Product/edit_product_type/$1';
$route['admin/product-type/manage'] = 'Product/manage_product_type';

//Service Category Routes
$route['admin/service/category/add'] = 'Service/add_category';
$route['admin/service/category/edit/(:num)'] = 'Service/edit_category/$1';
$route['admin/service/category/manage'] = 'Service/manage_category';
$route['admin/service/category_item_data'] = 'Service/category_item_data';
$route['admin/service/fetch_line_item_data']       = 'Service/fetch_line_item_data';
$route['admin/service/fetch_line_item_and_markup'] = 'Service/fetch_line_item_and_markup';

//Company Markup Routes
$route['admin/company-markup/add'] = 'Company_Markup/add';
$route['admin/company-markup/edit/(:num)'] = 'Company_Markup/edit/$1';
$route['admin/company-markup/manage'] = 'Company_Markup/manage';
$route['admin/company-markup/fetch_company_markup_data'] = 'Company_Markup/fetch_comapny_markup_data';


//Postcodes
$route['admin/fetch_postcodes'] = 'TeamLeader/fetch_postcodes';

//Team Leader Routes
$route['admin/team-leader/add'] = 'TeamLeader/add';
$route['admin/team-leader/edit/(:num)'] = 'TeamLeader/edit/$1';
$route['admin/team-leader/manage/(:num)'] = 'TeamLeader/manage/$1';
$route['admin/settings/team-leader/manage'] = 'TeamLeader/manage_settings';


//Team Leader Represenative Routes
$route['admin/team-representative/add'] = 'TeamRepresentative/add';
$route['admin/team-representative/edit/(:num)'] = 'TeamRepresentative/edit/$1';
$route['admin/team-representative/manage/(:num)'] = 'TeamRepresentative/manage/$1';
$route['admin/settings/team-representative/manage'] = 'TeamRepresentative/manage_settings';


//Lead Based Routes
$route['admin/lead/add'] = 'Lead/add';
$route['admin/lead/edit/(:any)'] = 'Lead/add/$1';
$route['admin/lead/manage'] = 'Lead/manage';
$route['admin/lead/detail/(:any)'] = 'Lead/detail/$1';
$route['admin/lead/save_lead_details'] = 'Lead/save_lead_details';
$route['admin/lead/update_lead_status'] = 'Lead/update_lead_status';
$route['admin/lead/update_lead_stage'] = 'Lead/update_lead_stage';
$route['admin/lead/fetch_lead_stage_data'] = 'Lead/fetch_lead_stage_data';
$route['admin/lead/fetch_proposal_totals'] = 'Lead/fetch_proposal_totals';
$route['admin/lead/update_lead_stage'] = 'Lead/update_lead_stage';
$route['admin/lead/add_quick_quote'] = 'Lead/add_quick_quote';
$route['admin/lead/edit_quick_quote/(:any)'] = 'Lead/add_quick_quote/$1';
$route['admin/lead/quote_pdf_download/(:any)'] = 'Lead/quote_pdf_download/$1';
$route['admin/lead/save_quick_quote'] = 'Lead/save_quick_quote';
$route['admin/lead/fetch_lead_quotes'] = 'Lead/fetch_lead_quotes';
$route['admin/lead/fetch_lead_booking_forms_data'] = 'Lead/fetch_lead_booking_forms_data';
$route['admin/lead/export_csv'] = 'Lead/export_csv';
$route['admin/lead/save_lead_allocation'] = 'Lead/save_lead_allocation';
$route['admin/lead/add_meter_data'] = 'Lead/add_meter_data';
$route['admin/lead/edit_meter_data/(:any)'] = 'Lead/add_meter_data/$1';
$route['admin/lead/save_meter_data'] = 'Lead/save_meter_data';
$route['admin/lead/meter_data_pdf_download/(:any)'] = 'Lead/meter_data_pdf_download/$1';
$route['admin/lead/fetch_lead_form_data'] = 'Lead/fetch_lead_form_data';
$route['admin/lead/fetch_nearby_leads'] = 'Lead/fetch_nearby_leads';
$route['admin/lead/fetch_lead_files'] = 'Lead/fetch_lead_files';
$route['admin/lead/fetch_lead_data'] = 'Lead/fetch_lead_data';
$route['admin/lead/manual_triggers'] = 'Lead/manual_triggers';
$route['admin/lead/check_job_exisits_for_lead'] = 'Lead/check_job_exisits_for_lead';
$route['admin/lead/fetch_form_data'] = 'Lead/fetch_form_data';
$route['admin/lead/add_solar_quick_quote'] = 'Lead/add_solar_quick_quote';
$route['admin/lead/edit_solar_quick_quote/(:any)'] = 'Lead/add_solar_quick_quote/$1';
$route['admin/lead/solar_quote_pdf_download/(:any)'] = 'Lead/solar_quote_pdf_download/$1';
$route['admin/lead/save_solar_quick_quote'] = 'Lead/save_solar_quick_quote';
$route['admin/lead/save_proposal_name'] = 'Lead/save_proposal_name';
$route['admin/lead/create-duplicate-proposal'] = 'Lead/createDuplicateProposal';

//Customer Based Routes
$route['admin/customer/add'] = 'Customer/add';
$route['admin/customer/edit/(:num)'] = 'Customer/edit/$1';
$route['admin/customer/manage'] = 'Customer/manage';
$route['admin/customer/manage-franchise'] = 'Customer/manage_franchise';
$route['admin/customer/check_unique_customer'] = 'Customer/check_unique_customer';
$route['admin/customer/save_customer'] = 'Customer/save_customer';
$route['admin/customer/save_customer_location'] = 'Customer/save_customer_location';
$route['admin/customer/delete_customer_location'] = 'Customer/delete_customer_location';
$route['admin/customer/save_customer_contact'] = 'Customer/save_customer_contact';
$route['admin/customer/fetch_customer_location'] = 'Customer/fetch_customer_location';
$route['admin/customer/fetch_customer_contacts'] = 'Customer/fetch_customer_contacts';
$route['admin/customer/fetch_customer_and_contacts'] = 'Customer/fetch_customer_and_contacts';
$route['admin/customer/fetch_customers_by_keyword'] = 'Customer/fetch_customers_by_keyword';
$route['admin/customer/assign_user_by_postcode'] = 'Customer/assign_user_by_postcode';



//Proposal Based Routes
$route['admin/proposal/add'] = 'Proposal/add';
$route['admin/proposal/calculate_finance'] = 'Proposal/calculate_finance';
$route['admin/proposal/save_finance'] = 'Proposal/save_finance';
$route['admin/proposal/delete_finance'] = 'Proposal/delete_finance';

$route['admin/proposal/commercial'] = 'Commercial/add';
$route['admin/proposal/commercial/edit/(:any)'] = 'Commercial/edit/$1';
$route['admin/proposal/commercial/pvwatts'] = 'Commercial/pvwatts';
$route['admin/proposal/commercial/calculate_veec_discount/(:num)'] = 'Commercial/calculate_veec_discount/$1';
$route['admin/proposal/commercial/validate_proposal_data/(:num)'] = 'Commercial/validate_proposal_data/$1';
$route['admin/proposal/commercial/save_veec_discount'] = 'Commercial/save_veec_discount';
$route['admin/proposal/commercial/degradation_excel_export'] = 'Commercial/degradation_excel_export';
$route['admin/proposal/commercial/saving_solar_system_details'] = 'Commercial/saving_solar_system_details';
$route['admin/proposal/commercial/solar_proposal_view/(:num)'] = 'Commercial/solar_proposal_view/$1';
$route['admin/proposal/commercial/calculate_finance'] = 'Commercial/calculate_finance';
$route['admin/proposal/commercial/getMappingToolData'] = 'Commercial/getMappingToolData';
$route['admin/proposal/commercial/upload_meter_data'] = 'Commercial/upload_meter_data';
$route['admin/proposal/commercial/remove_meter_data_file'] = 'Commercial/remove_meter_data_file';
$route['admin/proposal/commercial/upload_production_data/(:num)'] = 'Commercial/upload_production_data/$1';
$route['admin/proposal/commercial/fetch_financial_summary_data/(:num)'] = 'Commercial/fetch_financial_summary_data/$1';
$route['admin/proposal/commercial/calculate_degration_factor_data/(:num)/(:any)'] = 'Commercial/calculate_data_with_degradation_factor/$1/$2';
$route['admin/proposal/commercial/view_data_with_degradation_factor/(:num)/(:any)'] = 'Commercial/view_data_with_degradation_factor/$1/$2';
$route['admin/proposal/commercial/view_proposal/(:num)'] = 'Commercial/view_proposal/$1';
$route['admin/proposal/commercial/download_proposal_pdf/(:any)'] = 'Commercial/download_proposal_pdf/$1';
$route['admin/proposal/commercial/save_dataimg_to_png'] = 'Commercial/save_dataimg_to_png';
$route['admin/proposal/commercial/send_proposal_to_customer'] = 'Commercial/send_proposal_to_customer';
$route['admin/proposal/commercial/send_proposal_to_colleague'] = 'Commercial/send_proposal_to_colleague';
$route['admin/proposal/commercial/web_view/(:any)'] = 'Commercial/web_view/$1';

$route['admin/proposal/download_pack_view'] = 'Commercial/download_pack_view';
$route['admin/proposal/generate_pdf'] = 'Commercial/generate_pdf';


//LED Proposal Relate Routes
$route['admin/proposal/save_led_proposal'] = 'Proposal/save_led_proposal';
$route['admin/proposal/save_led_proposal_products'] = 'Proposal/save_led_proposal_products';
$route['admin/proposal/delete_led_proposal_products'] = 'Proposal/delete_led_proposal_products';
$route['admin/proposal/fetch_led_proposal_products'] = 'Proposal/fetch_led_proposal_products';
$route['admin/proposal/fetch_led_proposal_additional_items'] = 'Proposal/fetch_led_proposal_additional_items';
$route['admin/proposal/save_led_proposal_additional_items'] = 'Proposal/save_led_proposal_additional_items';
$route['admin/proposal/delete_led_proposal_additional_items'] = 'Proposal/delete_led_proposal_additional_items';
$route['admin/proposal/fetch_led_proposal_details'] = 'Proposal/fetch_led_proposal_details';
$route['admin/proposal/led_pdf_download/(:num)'] = 'Proposal/led_pdf_download/$1';

//SOLAR Proposal Relate Routes
$route['admin/proposal/save_solar_proposal'] = 'Proposal/save_solar_proposal';
$route['admin/proposal/save_solar_proposal_products'] = 'Proposal/save_solar_proposal_products';
$route['admin/proposal/delete_solar_proposal_products'] = 'Proposal/delete_solar_proposal_products';
$route['admin/proposal/fetch_solar_proposal_products'] = 'Proposal/fetch_solar_proposal_products';
$route['admin/proposal/fetch_solar_proposal_panel_data'] = 'Proposal/fetch_solar_proposal_panel_data';
$route['admin/proposal/calculate_solar_proposal_cost/(:num)'] = 'Proposal/calculate_solar_proposal_cost/$1';
$route['admin/proposal/fetch_solar_proposal_details'] = 'Proposal/fetch_solar_proposal_details';
$route['admin/proposal/solar_pdf_download/(:num)'] = 'Proposal/solar_pdf_download/$1';
$route['admin/proposal/api_calculate_solar_proposal_cost'] = 'Proposal/api_calculate_solar_proposal_cost';
$route['admin/proposal/api_calculate_solar_savings'] = 'Proposal/api_calculate_solar_savings';
$route['admin/proposal/save_dataimg_to_png1'] = 'Proposal/save_dataimg_to_png1';
$route['admin/proposal/solar_proposal_pdf_new'] = 'Proposal/solar_proposal_pdf_new';
$route['admin/proposal/solar_proposal_pdf_new_download'] = 'Proposal/solar_proposal_pdf_new_download';
/** Activity */
$route['admin/activity/manage'] = 'Activity/manage';
$route['admin/activity/save'] = 'Activity/save';
$route['admin/activity/delete'] = 'Activity/delete';
$route['admin/activity/fetch'] = 'Activity/fetch';
$route['admin/activity/mark_as_completed'] = 'Activity/mark_as_completed';
$route['admin/activity/sync_google_calendar_data'] = 'Activity/sync_google_calendar_data';
$route['admin/activity/getCalendarList'] = 'Activity/getCalendarList';

/** Google Api and Integration */
$route['admin/settings/gcal'] = 'Auth/google_calendar';
$route['google_oauth_callback'] = 'GoogleServiceAccount/google_oauth_callback';
$route['disconnect_google_service_account'] = 'GoogleServiceAccount/disconnect_google_service_account';


/** Uploader Routes */
$route['admin/uploader/upload_activity_file'] = 'Uploader/upload_file';
$route['admin/uploader/upload_file'] = 'Uploader/upload_file';
$route['admin/uploader/upload_lead_files'] = 'Uploader/upload_lead_files';
$route['admin/uploader/upload_attachment_files'] = 'Uploader/upload_attachment_files';
$route['admin/uploader/fetch_lead_attachments'] = 'Uploader/fetch_lead_attachments';
$route['admin/uploader/delete_lead_attachments'] = 'Uploader/delete_lead_attachments';
$route['admin/uploader/delete_activity_attachments'] = 'Uploader/delete_activity_attachments';
$route['admin/uploader/upload_proposal_image'] = 'Uploader/upload_proposal_image';
$route['admin/uploader/upload_inverter_image'] = 'Uploader/upload_inverter_image';

/** Log an Issue * */
$route['admin/page/log-issue'] = 'Page/log_issue';
$route['admin/page/log-issue/ediit/(:num)'] = 'Page/log_issue/$1';
$route['admin/page/log-issue/manage'] = 'Page/manage_log_issue';
$route['admin/page/upload_file'] = 'Uploader/upload_file';
//New
$route['admin/log/view/(:num)']    = 'Log/view/$1';
$route['admin/log/load-thread']    = 'Log/loadIssueThread';
$route['admin/log/create-message'] = 'Log/createMessage';

/** Reporting Routes */
//Dashboard
$route['admin/reports/dashboard/fetch_statistics_data'] = 'Report/fetch_statistics_data';
$route['admin/reports/dashboard/fetch_activity_data'] = 'Report/fetch_activity_data';
$route['admin/reports/dashboard/fetch_user_specific_report_data'] = 'Report/fetch_user_specific_report_data';
$route['admin/reports/dashboard/fetch_proposal_stages'] = 'Report/fetch_proposal_stages';
$route['admin/reports/dashboard/fetch_lead_stage_proposal_count'] = 'Report/fetch_lead_stage_proposal_count';
$route['admin/reports/dashboard/fetch_target_data'] = 'Report/fetch_target_data';
$route['admin/reports/dashboard/save_target_data'] = 'Report/save_target_data';
//MENU Reports
$route['admin/reports/manage'] = 'Report/manage';
$route['admin/reports'] = 'Report/reports';
$route['admin/reports/fetch_sales_rep_performance_data'] = 'Report/fetch_sales_rep_performance_data';
$route['admin/reports/fetch_sales_rep_lead_stage_data'] = 'Report/fetch_sales_rep_lead_stage_data';
$route['admin/reports/fetch_sales_rep_activity_data'] = 'Report/fetch_sales_rep_activity_data';
$route['admin/reports/fetch_sales_rep_closable_leads_data'] = 'Report/fetch_sales_rep_closable_leads_data';
$route['admin/reports/fetch_won_lead_data'] = 'Report/fetch_won_lead_data';

//Booking Form
//LED
$route['admin/booking_form/fetch_products'] = 'Booking_Form/fetch_products';
$route['admin/booking_form/add_led_booking_form'] = 'Booking_Form/add_led_booking_form';
$route['admin/booking_form/edit_led_booking_form/(:any)'] = 'Booking_Form/add_led_booking_form/$1';
$route['admin/booking_form/save_led_booking_form'] = 'Booking_Form/save_led_booking_form';
$route['admin/booking_form/fetch_led_booking_form_data'] = 'Booking_Form/fetch_led_booking_form_data';
$route['admin/booking_form/led_booking_form_pdf_download/(:any)'] = 'Booking_Form/led_booking_form_pdf_download/$1';
$route['admin/booking_form/led_booking_form_upload_pdf'] = 'Booking_Form/led_booking_form_upload_pdf';
$route['admin/booking_form/test_form_pdf/(:any)'] = 'Booking_Form/test_form_pdf/$1';
$route['admin/booking_form/convert_html_tom_image/(:any)'] = 'Booking_Form/convert_html_tom_image/$1';
$route['admin/booking_form/led_booking_form_generate_draft_pdf/(:any)'] = 'Booking_Form/led_booking_form_generate_draft_pdf/$1';


//SOLAR
$route['admin/booking_form/manage_solar_booking_form'] = 'Booking_Form/manage_solar_booking_form';
//$route['admin/booking_form/add_solar_booking_form'] = 'Booking_Form/add_solar_booking_form';
//$route['admin/booking_form/edit_solar_booking_form/(:any)'] = 'Booking_Form/add_solar_booking_form/$1';
$route['admin/booking_form/add_solar_booking_form'] = 'Booking_Form/add_solar_booking_form_v1';
$route['admin/booking_form/edit_solar_booking_form/(:any)'] = 'Booking_Form/add_solar_booking_form_v1/$1';
$route['admin/booking_form/save_solar_booking_form'] = 'Booking_Form/save_solar_booking_form';
$route['admin/booking_form/fetch_solar_booking_form_data'] = 'Booking_Form/fetch_solar_booking_form_data';
//$route['admin/booking_form/solar_booking_form_pdf_download/(:any)'] = 'Booking_Form/solar_booking_form_pdf_download/$1';
$route['admin/booking_form/solar_booking_form_pdf_download/(:any)'] = 'Booking_Form/solar_booking_form_pdf_download_v1/$1';
$route['admin/booking_form/solar_booking_form_upload_pdf'] = 'Booking_Form/solar_booking_form_upload_pdf';
$route['admin/booking_form/save_cost_centre'] = 'Booking_Form/save_cost_centre';
//$route['admin/booking_form/solar_booking_form_generate_draft_pdf/(:any)'] = 'Booking_Form/solar_booking_form_generate_draft_pdf/$1';
$route['admin/booking_form/solar_booking_form_generate_draft_pdf/(:any)'] = 'Booking_Form/solar_booking_form_generate_draft_pdf_v1/$1';

$route['admin/booking_form/add_solar_booking_form_v1'] = 'Booking_Form/add_solar_booking_form_v1';
$route['admin/booking_form/edit_solar_booking_form_v1/(:any)'] = 'Booking_Form/add_solar_booking_form_v1/$1';
$route['admin/booking_form/solar_booking_form_pdf_download_v1/(:any)'] = 'Booking_Form/solar_booking_form_pdf_download_v1/$1';
//Residential Solar
$route['admin/booking_form/manage_residential_solar_booking_form'] = 'Booking_Form/manage_residential_solar_booking_form';
//$route['admin/booking_form/add_residential_solar_booking_form'] = 'Booking_Form/add_solar_booking_form';
$route['admin/booking_form/add_residential_solar_booking_form'] = 'Booking_Form/add_solar_booking_form_v1';
$route['admin/booking_form/add_residential_solar_booking_form_v1'] = 'Booking_Form/add_solar_booking_form_v1';

//METER DATA
$route['admin/proposal/meter_data/(:num)'] = 'Proposal/manage_meter_data/$1';
$route['admin/uploader/upload_meter_data_file'] = 'Uploader/upload_meter_data_file';
$route['admin/proposal/fetch_meter_data_file'] = 'Proposal/fetch_meter_data_file';
$route['admin/proposal/inverter_layout'] = 'Proposal/inverter_layout';

/**
//Migration
$route['admin/lead/pipedrive_migrate'] = 'Lead/pipedrive_migrate';
$route['admin/lead/pipedrive_activity_migrate'] = 'Lead/pipedrive_activity_migrate';
$route['admin/lead/pipedrive_deal_migrate'] = 'Lead/pipedrive_deal_migrate';
$route['admin/lead/sales_migrate'] = 'Lead/sales_migrate';
$route['admin/lead/geocode_leads'] = 'Lead/geocode_leads';
$route['admin/lead/pipedrive_lead_migrate_new'] = 'Lead/pipedrive_lead_migrate_new';
$route['admin/lead/delete_leads_by_company_name'] = 'Lead/delete_leads_by_company_name';
$route['admin/lead/anaual_solar_data_migrate'] = 'Lead/anaual_solar_data_migrate';
$route['admin/lead/lead_owner_migrate'] = 'Lead/lead_owner_migrate';
 */

/** Notifications * */
$route['admin/notification/fetch']         = 'Notifications/fetch_notifications';
$route['admin/notification/change_status'] = 'Notifications/change_notification_status';

/** Solar Calculator */
$route['admin/proposal/api_calculate_solar_proposal_cost'] = 'Proposal/api_calculate_solar_proposal_cost';
$route['admin/proposal/api_calculate_solar_proposal_cost_for_react'] = 'Proposal/api_calculate_solar_proposal_cost_for_react';
$route['admin/proposal/save_dataimg_to_png'] = 'Proposal/save_dataimg_to_png';

//Search Based Routes:
$route['admin/search/manage'] = 'Search/manage';
$route['admin/search/search_users'] = 'Search/search_users';
$route['admin/search/save_search_activity_data'] = 'Search/save_search_activity_data';

/** Pipedrive */
$route['admin/pipedrive/get_deals'] = 'Pipedrive_Migration/getDeals';
$route['admin/pipedrive/get_files'] = 'Pipedrive_Migration/getFiles';
$route['admin/pipedrive/download_files'] = 'Pipedrive_Migration/downloadFiles';
$route['admin/pipedrive/update_customer'] = 'Pipedrive_Migration/updatePipedriveCustomer';

$route['admin/send_test_mail'] = 'Lead/send_mail';


/** Cron * */
$route['cron/push_weblead_to_crm']       = 'Cron/push_weblead_to_crm';
$route['cron/push_dataforcejob_to_simpro'] = 'Crons/DataForceCron/push_dataforcejob_to_simpro';
//$route['cron/fix_dataforcejob_to_simpro']       = 'Crons/DataForceCron/fix_dataforcejob_to_simpro';
$route['admin/automate-fb-leads'] = 'Cron/automate_fb_leads';
$route['admin/check_franchise_for_postcode'] = 'Cron/check_franchise_for_postcode';

/** Simpro Cron */
$route['cron/save_simpro_jobs_to_crm'] = 'Crons/SimproCron/save_simpro_jobs_to_crm';
$route['cron/process_warranty_jobs']       = 'Crons/SimproCron/process_warranty_jobs';

/** Webhooks */
$route['webhook/simpro_job_callback'] = 'Crons/DataForceCron/simpro_job_callback';

$route['warraty-evidence'] = 'Warranty/upload_evidence';

/** Follow Me Tracking */
$route['admin/tracking/manage']       = 'Tracking/manage';

/** Resiential Soalr Proposal */
$route['admin/product/residential_solar/add']                        = 'SolarProduct/add';
$route['admin/product/residential_solar/edit/(:num)']                = 'SolarProduct/edit/$1';
$route['admin/product/residential_solar/manage']                     = 'SolarProduct/manage';
$route['admin/product/residential_solar/delete/(:num)']              = 'SolarProduct/delete/$1';
$route['admin/product/residential_solar/fetch_product_variant_data'] = 'SolarProduct/fetch_product_variant_data';
$route['admin/product/residential_solar/upload_datasheet']           = 'SolarProduct/upload_file';
$route['admin/product/residential_solar/import_csv']                 = 'SolarProduct/import_csv';
$route['admin/product/residential_solar/export_csv']                 = 'SolarProduct/export_csv';
$route['admin/product/residential_solar/fetch_item_data']            = 'SolarProduct/fetch_item_data';
$route['admin/product/residential_solar/fetch_racking_item_data']    = 'SolarProduct/fetch_racking_item_data';
$route['admin/product/residential_solar/fetch_additional_item_data'] = 'SolarProduct/fetch_additional_item_data';
$route['admin/product/fetch_postcode']  = 'SolarProduct/fetch_postcode';
$route['admin/product/residential_solar/get_savings_calculation_data']  = 'SolarProduct/get_savings_calculation_data';
$route['admin/product/residential_solar/proposal_save']  = 'SolarProduct/proposal_save';
$route['admin/product/residential_solar/proposal_load']  = 'SolarProduct/proposal_load';
$route['admin/product/residential_solar/proposal_pdf_download/(:any)']  = 'SolarProduct/proposal_pdf_download/$1';
$route['admin/product/residential_solar/proposal_draft_pdf_download/(:any)']  = 'SolarProduct/generate_draft_pdf/$1';
$route['admin/product/residential_solar/proposal_to_image/(:any)']  = 'SolarProduct/proposal_to_image/$1';
$route['admin/product/residential_solar/save_dataimg_to_png']  = 'SolarProduct/save_dataimg_to_png';
$route['admin/product/residential_solar/save_chartimg_to_png']  = 'SolarProduct/save_chartimg_to_png';

$route['admin/product/residential_solar/sync_ted_product_to_sale']  = 'SolarProduct/sync_ted_product_to_kuga_sale_crm';

//Tender Quotes User Routes
$route['admin/tender-quotes/add'] = 'TenderQuotes/add';
$route['admin/tender-quotes/edit/(:num)'] = 'TenderQuotes/edit/$1';
$route['admin/tender-quotes/manage/(:num)'] = 'TenderQuotes/manage/$1';
$route['admin/settings/tender-quotes/manage'] = 'TenderQuotes/manage_settings';


//Tender Lead Based Routes
$route['admin/tender-lead/add'] = 'TenderLead/add';
$route['admin/tender-lead/edit/(:any)'] = 'TenderLead/add/$1';
$route['admin/tender-lead/manage'] = 'TenderLead/manage';
$route['admin/tender-lead/detail/(:any)'] = 'TenderLead/detail/$1';
$route['admin/tender-lead/save_lead_details'] = 'TenderLead/save_lead_details';
$route['admin/tender-lead/update_lead_status'] = 'TenderLead/update_lead_status';
$route['admin/tender-lead/update_lead_stage'] = 'TenderLead/update_lead_stage';
$route['admin/tender-lead/fetch_lead_stage_data'] = 'TenderLead/fetch_lead_stage_data';
$route['admin/tender-lead/fetch_lead_data'] = 'TenderLead/fetch_lead_data';
$route['admin/tender-lead/save_tender_lead_contact'] = 'TenderLead/save_tender_lead_contact';
$route['admin/tender-lead/fetch_tender_lead_contacts'] = 'TenderLead/fetch_tender_lead_contacts';
$route['admin/tender-lead/save_quote_details'] = 'TenderLead/save_quote_details';
$route['admin/tender-lead/quote_pdf_download/(:any)'] = 'TenderLead/quote_pdf_download/$1';
$route['admin/tender-lead/fetch_quotes'] = 'TenderLead/fetch_quotes';
$route['admin/tender-lead/fetch_quotes_details'] = 'TenderLead/fetch_quotes_details';
$route['admin/tender-lead/manage_solar_booking_form'] = 'TenderLead/manage_solar_booking_form';
$route['admin/tender-lead/manage_residential_solar_booking_form'] = 'TenderLead/manage_residential_solar_booking_form';
$route['admin/tender-lead/add_solar_booking_form'] = 'TenderLead/add_solar_booking_form';
$route['admin/tender-lead/save_solar_booking_form'] = 'TenderLead/save_solar_booking_form';
$route['admin/tender-lead/fetch_solar_booking_form_data'] = 'TenderLead/fetch_solar_booking_form_data';
$route['admin/tender-lead/solar_booking_form_pdf_download'] = 'TenderLead/solar_booking_form_pdf_download';
$route['admin/tender-lead/solar_booking_form_generate_draft_pdf/(:any)'] = 'TenderLead/solar_booking_form_generate_draft_pdf/$1';
$route['admin/tender-lead/fetch_lead_form_data'] = 'TenderLead/fetch_lead_form_data';
$route['admin/tender-lead/edit_solar_booking_form/(:any)'] = 'TenderLead/add_solar_booking_form/$1';



/** TenderActivity */
$route['admin/tender-lead/activity/manage'] = 'TenderActivity/manage';
$route['admin/tender-lead/activity/save'] = 'TenderActivity/save';
$route['admin/tender-lead/activity/delete'] = 'TenderActivity/delete';
$route['admin/tender-lead/activity/fetch'] = 'TenderActivity/fetch';
$route['admin/tender-lead/activity/mark_as_completed'] = 'TenderActivity/mark_as_completed';
$route['admin/tender-lead/activity/sync_google_calendar_data'] = 'TenderActivity/sync_google_calendar_data';
$route['admin/tender-lead/activity/getCalendarList'] = 'TenderActivity/getCalendarList';




/** JOB Card */
$route['admin/lead/add_job_card'] = 'JobCard/add';
$route['admin/lead/edit_job_card_form/(:any)'] = 'JobCard/add/$1';
$route['admin/lead/fetch_job_card_form_data'] = 'JobCard/fetch_job_card_form_data';
$route['admin/lead/save_job_card_form'] = 'JobCard/save_job_card_form';
$route['admin/lead/generate_card_pdf/(:any)'] = 'JobCard/generate_card_pdf/$1';
$route['admin/lead/generate_draft_card_pdf/(:any)'] = 'JobCard/generate_draft_card_pdf/$1';


/** Tender JOB Card */
$route['admin/tender-lead/add_job_card'] = 'TenderJobCards/add';
$route['admin/tender-lead/edit_job_card_form/(:any)'] = 'TenderJobCards/add/$1';
$route['admin/tender-lead/fetch_job_card_form_data'] = 'TenderJobCards/fetch_job_card_form_data';
$route['admin/tender-lead/save_job_card_form'] = 'TenderJobCards/save_job_card_form';
$route['admin/tender-lead/generate_card_pdf/(:any)'] = 'TenderJobCards/generate_card_pdf/$1';
$route['admin/tender-lead/generate_draft_card_pdf/(:any)'] = 'TenderJobCards/generate_draft_card_pdf/$1';


/** Solar Tool New Routes */
$route['admin/solar-tool/create'] = 'SolarTool/create';
$route['admin/solar-tool/save_mapping_data'] = 'SolarTool/save_mapping_data';
$route['admin/solar-tool/get_near_map_imagery'] = 'SolarTool/get_near_map_imagery';
$route['admin/solar-tool/save_near_map_imagery'] = 'SolarTool/save_near_map_imagery';
$route['admin/solar-tool/remove_near_map_imagery'] = 'SolarTool/remove_near_map_imagery';
$route['admin/solar-tool/fetch_mapping_tool_data'] = 'SolarTool/fetch_mapping_tool_data';
$route['admin/solar-tool/fetch_mapping_tool_objects_data'] = 'SolarTool/fetch_mapping_tool_objects_data';
$route['admin/solar-tool/save_mapping_object'] = 'SolarTool/save_mapping_object';
$route['admin/solar-tool/remove_mapping_object'] = 'SolarTool/remove_mapping_object';
$route['admin/solar-tool/save_snapshot'] = 'SolarTool/save_snapshot';
$route['admin/solar-tool/update_near_map_imagery'] = 'SolarTool/update_near_map_imagery';
$route['admin/solar-tool/save_mapping_object_bulk'] = 'SolarTool/save_mapping_object_bulk';
$route['admin/solar-tool/remove_mapping_object_bulk'] = 'SolarTool/remove_mapping_object_bulk';

/** Inverter Tool New Routes */
$route['admin/inverter-tool/fetch_design_tool_data'] = 'InverterDesignTool/fetch_design_tool_data';
$route['admin/inverter-tool/save_design_tool_data'] = 'InverterDesignTool/save_design_tool_data';
$route['admin/inverter-tool/save_design_tool_snapshot'] = 'InverterDesignTool/save_design_tool_snapshot';
$route['admin/inverter-tool/save_design_object'] = 'SolarTool/save_design_object';
$route['admin/inverter-tool/remove_design_object'] = 'SolarTool/remove_design_object';


/** Meter Data Related */
$route['admin/meter_data/upload_file'] = 'MeterData/upload_file';
$route['admin/meter_data/fetch_file_data'] = 'MeterData/fetch_file_data';
$route['admin/meter_data/save_meter_data_column_mapping'] = 'MeterData/save_meter_data_column_mapping';

/** Tender Related */
$route['admin/tender/add/(:any)'] = 'Tender/add/$1';
$route['admin/tender/add'] = 'Tender/add/$1';
$route['admin/tender/manage'] = 'Tender/manage';
$route['admin/tender/delete'] = 'Tender/delete';
$route['admin/tender/add'] = 'Tender/add/$1';
$route['admin/tender/save'] = 'Tender/saveTender';
$route['admin/tender/save-site'] = 'Tender/saveSite';
$route['admin/tender/loadoptions'] = 'Tender/loadoptions';
$route['admin/tender/uploadsiteimages/(:num)'] = 'Tender/uploadsiteimages/$1';
$route['admin/tender/deleteSiteType'] = 'Tender/deleteSiteType';
$route['admin/tender/delete-site'] = 'Tender/deleteSite';
$route['admin/tender/save-site-information/(:num)'] = 'Tender/saveSiteInformation/$1';
$route['admin/tender/save-doc-cat'] = 'Tender/saveTenderDocCat';
$route['admin/tender/upload-tender-docs'] = 'Tender/uploadTenderDocuments';
$route['admin/tender/load-doc-cat'] = 'Tender/loadTenderDocumentTypes';
$route['admin/tender/downloadZip/(:num)'] = 'Tender/downloadZip/$1';
$route['admin/tender/add-site-option'] = 'Tender/addSiteOption';
$route['admin/tender/get-documents'] = 'Tender/getTenderDocuments';
$route['admin/tender/delete-document'] = 'Tender/deleteTenderDocument';
$route['admin/tender/download-document/(:num)'] = 'Tender/downloadTenderDocument/$1';
$route['admin/tender/manage-tender'] = 'Tender/manageTender/';
$route['admin/tender/fetch_status_data'] = 'Tender/fetch_status_data';
$route['admin/tender/addBulkTenders'] = 'Tender/addBulkTenders';
$route['admin/tender/booking-form/(:any)'] = 'Tender/bookingForm/$1';
$route['admin/tender/booking-form'] = 'Tender/bookingForm';
$route['admin/tender/save_booking_form'] = 'Tender/save_booking_form';
$route['admin/tender/fetch_booking_form_data'] = 'Tender/fetch_booking_form_data';
$route['admin/tender/booking-form-download/(:any)'] = 'Tender/bookingFormDownload/$1';
$route['admin/tender/booking-forms-list'] = 'Tender/GetBookingForms';
$route['admin/tender/update-status'] = 'Tender/updateStatus';
$route['admin/tender/save-cost-cat'] = 'Tender/saveCostCat';


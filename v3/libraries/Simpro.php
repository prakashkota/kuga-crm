<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class Simpro {

    /**
     * Constructor
     */
    protected static $client_url = 'https://kugaelectrical.simprosuite.com';
    //protected static $access_token = 'Bearer 5ac09eed8b74574c4259e49c6080abfa2cbd6cf7';
    protected static $access_token = 'Bearer 24e5845835d547234496e72e6272a2cc123fce60';
    protected static $company_id = 4;

    public function __construct() {
        require __DIR__ . "./../../vendor/autoload.php";
    }

    public function create_customer($payload = []) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/customers/companies/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response =$client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_customer($payload = [],$sp_cust_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/customers/companies/'.$sp_cust_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response =$client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_customer_custom_fields($sp_cust_id = NULL, $sp_custom_field_id = NULL ,$sp_custom_field_value = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/customers/'.$sp_cust_id.'/customFields/'.$sp_custom_field_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => array('Value' => $sp_custom_field_value),
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function create_customer_site($payload = []) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/sites/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_customer_site($payload = [],$sp_site_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/sites/'.$sp_site_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response =$client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }else if($response->getStatusCode() === 422){
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
            echo $data;die;
        }
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
            print_r($data);die;
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function create_customer_site_contact($payload = [],$sp_site_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/sites/'.$sp_site_id.'/contacts/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function update_customer_site_contact($payload = [],$sp_site_id = NULL,$sp_contact_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/sites/'.$sp_site_id.'/contacts/'.$sp_contact_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response =$client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }else if($response->getStatusCode() === 422){
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
            echo $data;die;
        }
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
            print_r($data);die;
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    
    public function create_job($payload = []) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_job($payload = [],$sp_job_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$sp_job_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_job_custom_fields($sp_job_id = NULL, $sp_custom_field_id = NULL ,$sp_custom_field_value = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$sp_job_id.'/customFields/'.$sp_custom_field_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => array('Value' => $sp_custom_field_value),
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function create_job_section($payload = [],$simpro_job_id = NUL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$simpro_job_id.'/sections/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function create_job_cost_center($payload = [],$simpro_job_id = NUL,$simpro_section_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$simpro_job_id.'/sections/'.$simpro_section_id.'/costCenters/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function create_job_attachment($payload = [],$simpro_job_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$simpro_job_id.'/attachments/files/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function update_job_attachment($payload = [],$sp_job_id = NULL,$sp_attachment_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$sp_job_id.'/attachments/files/'.$sp_attachment_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('PATCH',self::$client_url . $api_url,[
            'headers' => $headers,
            'json' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 204) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
     public function list_jobs($payload = []) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function list_job_details($payload = [],$job_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$job_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function list_site_details($payload = [],$site_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/sites/'.$site_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }


    public function list_invoices($payload = [],$job_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$job_id.'/invoices/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function list_invoice_details($payload = [],$job_id = NULL,$invoice_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/'.$job_id.'/invoices/' . $invoice_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function list_catalog_item_details($payload = [],$catalog_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/catalogs/' . $catalog_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function list_job_attachment_files($payload = [],$job_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/' . $job_id . '/attachments/files/';
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

    public function delete_job_attachment_file($payload = [],$job_id = NULL,$file_id = NULL) {
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/api/v1.0/companies/'.self::$company_id.'/jobs/' . $job_id . '/attachments/files/'.$file_id;
        $headers = array(
            'Authorization' => self::$access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('DELETE',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }

}

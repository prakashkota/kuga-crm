
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tbody>
    <tr>
      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/header.jpg" width="910" height="127" alt=""/></td>
    </tr>
    <tr>
      <td height="45"></td>
    </tr>
    <tr>
      <td valign="top"><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td style="font-size:12px; font-style:italic; color:#6a6a6a">To be completed and signed by the lighting solution provider</td>
          </tr>
          <tr>
            <td height="40" style="color: #922a27; font-size:20px;"><strong>Section A - Building lighting quality statement</strong></td>
          </tr>
          <tr>
            <td style="border-top: solid 1px #969697; height:10px"></td>
          </tr>
          <tr>
            <td height="25" style="font-size:12px; color:#000000;">On behalf of <strong>Kuga Electrical Pty Ltd</strong>, I confirm that the lighting upgrade at</td>
          </tr>
          
          <tr>
            <td><input type="text" name="get_nomination_form_details[certificate_provider_details][site_address]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:37px; padding-left:5px; padding-right:5px"/></td>
          </tr>
          <tr>
            <td height="10"></td>
          </tr>
          <tr>
            <td style="font-size:12px; color:#000000;">has been implemented to meet the requirements of the AS/NZS 1680 Standard series. I confirm that the following details have been
            considered and satisfied for the lighting upgrade:</td>
          </tr>
          <tr>
            <td height="3"></td>
          </tr>
          
          <tr>
            <td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="16"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                  <td width="784" style="font-size:12px; color:#000000;">maintained illuminance (lighting) levels recommended by AS/NZS 1680 for each space where the lighting upgrade has been installed</td>
                </tr>
                <tr>
                  <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                  <td style="font-size:12px; color:#000000;">lumen (light level) depreciation of the lighting system over the life of the upgrade</td>
                </tr>
                <tr>
                  <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                  <td style="font-size:12px; color:#000000;">control of glare</td>
                </tr>
                <tr>
                  <td valign="top" style="padding-top: 3px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot-space.jpg" width="4" height="7" alt=""/></td>
                  <td style="font-size:12px; color:#000000;">uniformity of illuminance (lighting)</td>
                </tr>
                <tr>
                  <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                  <td style="font-size:12px; color:#000000;">any additional requirements of AS/NZS 1680 that are relevant to the areas where the lighting upgrade has been installed, and</td>
                </tr>
                <tr>
                  <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                  <td style="font-size:12px; color:#000000;">the recommended maintenance regime of the lighting system (attached to this document).</td>
                </tr>
              </tbody>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="font-size:12px; color:#000000;">In addition, I declare that:</td>
          </tr>
          <tr>
            <td valign="top">
              <table width="800" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                  <tr>
                    <td width="16"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                    <td width="784" style="font-size:12px; color:#000000;">The requirements of the Building Code of Australia (BCA) section F4.4 Safe Movement, have been met in all areas of the lighting upgrade</td>
                  </tr>
                  <tr>
                    <td valign="top" style="padding-top: 5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                    <td style="font-size:12px; color:#000000;">The illumination power density (IPD) of the lighting system after the upgrade does not exceed the maximum IPD allowed under Part J6 of the BCA</td>
                  </tr>
                  <tr>
                    <td valign="top" style="padding-top:5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                    <td style="font-size:12px; color:#000000;">The information I have provided is complete and accurate and I am aware that there are penalties for providing false and misleading  information in this form.</td>
                  </tr>
                </tbody>
              </table></td>
            </tr>
            <tr>
              <td height="30"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; color: #000000"><strong>Note:</strong></td>
            </tr>
            <tr>
              <td style="font-size:12px; color:#000000;">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly
              providing false or misleading information to the Scheme Administrator.</td>
            </tr>
            <tr>
              <td height="50"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; color: #000000"><strong>Signed by or on behalf of the Accredited Certificate Provider</strong></td>
            </tr>
            <tr>
              <td height="15"></td>
            </tr>
            <tr>
              <td valign="top">
                <table align="left" width="400" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td valign="top">
                        <table width="370" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td style="font-size:12px; color:#6a6a6a; height:25px">Signature</td>
                            </tr>
                            <tr>
                              <td valign="top">
                                <div class="signature-pad">
                                  <div class="signature-pad--body">
                                    <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_5" data-id="5" class="sign_create">
                                    </div>
                                    <input required="" id="get_nomination_form_details_certificate_provider_signature" name="get_nomination_form_details[certificate_provider_details][signature]" type="hidden" />
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Name</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[certificate_provider_details][name1]"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Date</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[certificate_provider_details][date]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Position</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[certificate_provider_details][position]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Company</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[certificate_provider_details][company]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td style="font-size:12px; color:#6a6a6a;">*this form must be signed <span style="font-style: italic">on or after</span> the installation date</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
            <td height="90"></td>
          </tr>
          </tbody>
      </table></td>
    </tr>
    <tr>
      <td><img src="img/bottom.jpg" width="910" height="102" alt=""/></td>
    </tr>
  </tbody>
</table>
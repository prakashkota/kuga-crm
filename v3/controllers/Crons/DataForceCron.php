<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Cron Controller
 * @version 1.0
 */
class DataForceCron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('../core/input');
        $this->load->library('Components');
        $this->load->library('Simpro');
        $this->load->library('DataForce');
        $this->load->model('Cron_Model','cron');
        date_default_timezone_set('Australia/Sydney');
    }

    /* Get Daily Jobs From DataFroce Api adn Push The Job to sIMPRO */
    public function push_dataforcejob_to_simpro(){
        
        return true;
        
        if($this->input->is_cli_request()) {
            //return true;
        }
        
        log_message('error', 'Dataforce Cron Initiated Latest.');
        
        $lock_file = $this->create_lock_file_and_check_existance();

        if($lock_file == false){
            return false;
        }

        $current_date = date('YmdHis');

        $jobs = $this->dataforce->list_jobs_by_appointment_api();
        $jobs = $jobs['Appointment'];

        //echo "<pre>";
        //print_r($jobs);die;
        if(!empty($jobs)){
            for($i = 0; $i < count($jobs); $i++){

                $appointment_data = $jobs[$i]['@attributes'];
                $job_id = $appointment_data['job_id'];
                $company_name = $appointment_data['company_name'];
                $address = trim($appointment_data['street_address'])==''?FALSE:trim($appointment_data['street_address']);
                
                if($appointment_data['modified_date'] < 20191024235418){
                    continue;
                }
                
                $is_data_in_dataforce_table = TRUE;
                
                //Check for Cusomter existance first in dataforce table
                $is_job_exists = $this->cron->fetch_dataforce_jobs($job_id,$company_name,$address);
                //If customer doesnt exisits in dataforce table check in simpro table
                if(empty($is_job_exists)){
                    $is_job_exists = $this->cron->fetch_simpro_jobs($job_id,$company_name,$address);
                    // print_r($is_job_exists );die;
                    $is_data_in_dataforce_table = FALSE;
                }
                
                
                
                //Check if work type is Assessment and job exists in db then only upload images to simpro
                if(!empty($is_job_exists) && isset($appointment_data['work_type']) && $this->components->find_string('Assessment',$appointment_data['work_type'],'i')){
                    if($is_data_in_dataforce_table){
                        $this->upload_images_to_simpro($is_job_exists,$appointment_data);
                    }
                    continue;
                }

                //Check if particular type is Installation then only proceed dont preoced for othe appointment types
                if(isset($appointment_data['work_type']) && !$this->components->find_string('Installation',$appointment_data['work_type'],'i')){
                    continue;
                }
                
                log_message('error', json_encode($jobs[$i]));
                
                $status = (isset($appointment_data['status'])) ? $appointment_data['status'] : NULL;
                $sub_status = (isset($appointment_data['sub_status'])) ? $appointment_data['sub_status'] : NULL;

 
                if(!empty($is_job_exists) && $is_job_exists['status'] == $status && $is_job_exists['sub_status'] == $sub_status){
                    if($is_data_in_dataforce_table){
                        $this->upload_images_to_simpro($is_job_exists,$appointment_data);
                    }
                    continue;
                }

                //Insert in Local DB to maintain the jobs being inserted in simpro from dataforce
                $appointemnt_id = $appointment_data['id'];  

                //Call appointemtn api to get detail job data
                $appointment_details_data = $this->dataforce->get_appointment_details($appointemnt_id);


                if(isset($appointment_details_data['error'])){
                    continue;
                }

                if(!isset($appointment_details_data['Appointments'])){
                    continue;
                }
                
                if(!isset($appointment_details_data['Appointments']['Appointment'])){
                    continue;
                }

                $appointment_details = $appointment_details_data['Appointments']['Appointment']['Details'];

                $appointment_details_questions = (isset($appointment_details_data['Appointments']['Appointment']['PhoneQuestions'])) ? $appointment_details_data['Appointments']['Appointment']['PhoneQuestions'] : [];

                $appointment_details_witness = (isset($appointment_details_data['Appointments']['Appointment']['FieldWorkerAction'])) ? $appointment_details_data['Appointments']['Appointment']['FieldWorkerAction']: [];

                $owner_tenant = NULL;
                $created_by = NULL;
                $contact_no = NULL;


                if(!is_array($appointment_details['HomePhone'])){
                    $contact_no = $appointment_details['HomePhone'];
                }

                if($contact_no != NULL && !is_array($appointment_details['MobilePhone'])){
                    $contact_no = $appointment_details['MobilePhone'];
                }
                
                if(!empty($appointment_details_witness) && isset($appointment_details_witness['Witness'])){
                    if(isset($appointment_details_witness['Witness']['FieldWorker'])){
                        $created_by =  (isset($appointment_details_witness['Witness']['FieldWorker']['Name'])) ? $appointment_details_witness['Witness']['FieldWorker']['Name'] : NULL;
                    }
                }
    
                if(!empty($appointment_details_questions) && isset($appointment_details_questions['Question'])){
                    $owner_tenant =  (isset($appointment_details_questions['Question']['3'])) ? $appointment_details_questions['Question'] : NULL;
                }

                $job_id_key = 'job_id';//$this->get_key_for_specific_column($jobs[0],'Job Id');
                $company_name_key = 'CompanyName';//$this->get_key_for_specific_column($jobs[0],'Company Name');
                $customer_fname_key = 'FirstName';//$this->get_key_for_specific_column($jobs[0],'Customer');
                $customer_lname_key = 'Surname';//$this->get_key_for_specific_column($jobs[0],'Customer');
                $customer_email_key = 'Email';//$this->get_key_for_specific_column($jobs[0],'Email Address');
                $address_key = 'Address';//$this->get_key_for_specific_column($jobs[0],'Address');
                $state_key = 'State';//$this->get_key_for_specific_column($jobs[0],'State');
                $suburb_key = 'Suburb';//$this->get_key_for_specific_column($jobs[0],'Suburb');
                $postcode_key = 'Postcode';

                if($job_id != null){
                    $insert_data = array(
                        'job_id' => $job_id,
                        'created_by' => $created_by,
                        'work_type' => (isset($appointment_data['work_type'])) ? $appointment_data['work_type'] : NULL,
                        'company_name' => (isset($appointment_details[$company_name_key])) ? $appointment_details[$company_name_key] : NULL,
                        'customer_fname' => (isset($appointment_details[$customer_fname_key])) ? $appointment_details[$customer_fname_key]  : NULL,
                        'customer_lname' => (isset($appointment_details[$customer_lname_key])) ? $appointment_details[$customer_lname_key] : NULL,
                        'contact_no' => $contact_no,
                        'customer_email' => (isset($appointment_details[$customer_email_key])) ? $appointment_details[$customer_email_key] : NULL,
                        'address' => (isset($appointment_details[$address_key])) ? $appointment_details[$address_key] : NULL,
                        'state' => (isset($appointment_details[$state_key])) ? $appointment_details[$state_key] : NULL,
                        'suburb' => (isset($appointment_details[$suburb_key])) ? $appointment_details[$suburb_key] : NULL,
                        'postcode' => (isset($appointment_details[$postcode_key])) ? $appointment_details[$postcode_key] : NULL,
                        'job_sync_status' => 2,
                        'status' => $status,
                        'sub_status' => $sub_status,
                        'data' => json_encode($appointment_details_data)
                    );

                    if($insert_data['company_name'] == NULL){
                        continue;
                    }

                    if(count($is_job_exists) > 0){
                        $job_data = $is_job_exists;
                        $job_data['abn'] = (isset($appointment_details['ABN_ACN'])) ? $appointment_details['ABN_ACN'] : NULL;
                        $job_data['owner_tenant'] = $owner_tenant;
                        $job_data['status'] = $status;
                        $job_data['sub_status'] = $sub_status;
                        unset($job_data['data']);
                        
                        if($is_data_in_dataforce_table == FALSE){
                            //Insert Job
                            $insert_data['simpro_job_id'] = $job_data['simpro_job_id'];
                            $insert_data['job_sync_status'] = 1;
                            $this->common->insert_data('tbl_dataforce_jobs',$insert_data);
                            $insert_data['owner_tenant'] = $owner_tenant;
                            $this->update_simpro_job_custom_fields($insert_data);
                        }else{
                            if($job_data['job_sync_status'] == 3 && $job_data['failed_retry'] < 4){
                                //If Api failed last time retry it max 3 times
                                //Create Job in sIMPRO
                                $response = $this->create_job_in_simpro($job_data);
                                $rstatus = ($response) ? 1 : 3;
                                $failed_retry = ($response) ? $job_data['failed_retry'] : $job_data['failed_retry'] + 1;
    
                                //Set Final Status
                                $this->common->update_data(
                                    'tbl_dataforce_jobs',
                                    array('id' => $job_data['id']),
                                    array('job_sync_status' => $rstatus , 'failed_retry' => $failed_retry)
                                );
                            }else{
                                //Only Update Status and Sub Status Field
                                $this->update_simpro_job_custom_fields($job_data);
                                $this->common->update_data('tbl_dataforce_jobs',
                                    array('id' => $job_data['id']),
                                    array('status' => $status,'sub_status' => $sub_status,'updated_at' => date('Y-m-d H:i:s'))
                                );
                            }
                        }
                    }else{
                        //Insert Job with Status : 2 i.e. Processing State
                        $this->common->insert_data('tbl_dataforce_jobs',$insert_data);

                        $job_data = $this->common->fetch_row('tbl_dataforce_jobs','*',array('job_id' => $job_id));
                        $job_data['abn'] = (isset($appointment_details['ABN_ACN'])) ? $appointment_details['ABN_ACN'] : NULL;
                        $job_data['owner_tenant'] = $owner_tenant;
                        unset($job_data['data']);

                        //print_r($job_data);die;

                        //Create Job in sIMPRO
                        $response = $this->create_job_in_simpro($job_data);
                        $rstatus = ($response) ? 1 : 3;

                        //Set Final Status
                        $this->common->update_data('tbl_dataforce_jobs',array('id' => $job_data['id']),array('job_sync_status' => $rstatus));

                        //Now proceed with job images/files
                        $appointment_details_files = array();

                        $appointment_details_datalist = (isset($appointment_details_data['Appointments']['Appointment']['Job']['DataList'])) ? $appointment_details_data['Appointments']['Appointment']['Job']['DataList'] : [];

                        if(!empty($appointment_details_datalist)){
                            $appointment_details_files = $appointment_details_data['Appointments']['Appointment']['Job']['DataList']['DataItem'];
                        }

                        //Upload Job Images to sIMPRO
                        if(!empty($appointment_details_files)){
                            $updated_job_data = $this->common->fetch_row('tbl_dataforce_jobs','simpro_job_id',array('job_id' => $job_id));
                            foreach($appointment_details_files as $file){
                                $job_dataid = $file['@attributes']['id'];
                                $this->create_job_images_in_simpro($job_id,$updated_job_data['simpro_job_id'],$job_dataid);
                            }
                        }
                    }

                }
            }
        }

        //Unlink Dataforce Script File
        $filename = FCPATH . "dataforce_script.lock";
        if(file_exists($filename)){
            unlink($filename);
        }
    }

    private function update_simpro_job_custom_fields($job_data){
        
        
            $simpro_job_id = (int) $job_data['simpro_job_id'];
    
            //Update Job Custom Field Data and Add Section and Cost Center
            $job_custom_fields = array(
                array('ID' => 47,'Name' => 'Job Type'),
                //array('ID' => 64,'Name' => 'Nature Of Ownership'),
                array('ID' => 140,'Name' => 'SUB STATUS (DATAFORCE)'),
                array('ID' => 139,'Name' => 'STATUS (DATA FORCE)'),
                array('ID' => 141,'Name' => 'WORK TYPE (DATAFORCE)'),
                array('ID' => 142,'Name' => 'JOB ID (DATAFORCE)'),
                array('ID' => 117,'Name' => 'Installer Type'),
                array('ID' => 138,'Name' => 'HIRED ACCESS EQUIPMENT'),
                array('ID' => 128,'Name' => 'AREA'),
            );
    
            if (!empty($job_custom_fields)) {
                foreach ($job_custom_fields as $key => $value) {
                    $sp_custom_field_id = $value['ID'];
                    $sp_custom_field_name = $value['Name'];
                    try{
                        if ($sp_custom_field_name == 'Job Type') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'LED');
                        }else if ($sp_custom_field_name == 'Nature Of Ownership') {
                            $nature_of_ownership_list = ["Owner","Tenant", "Landlord", "Unknown"];
                            if(in_array($job_data['owner_tenant'],$nature_of_ownership_list)){
                                $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['owner_tenant']);
                            }
                        } else if ($sp_custom_field_name == 'SUB STATUS (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['sub_status']);
                        } else if ($sp_custom_field_name == 'STATUS (DATA FORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['status']);
                        } else if ($sp_custom_field_name == 'WORK TYPE (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['work_type']);
                        } else if ($sp_custom_field_name == 'JOB ID (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['job_id']);
                        } else if ($sp_custom_field_name == 'Installer Type') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'Not booked');
                        } else if ($sp_custom_field_name == 'HIRED ACCESS EQUIPMENT') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'UNSURE');
                        } else if ($sp_custom_field_name == 'AREA') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'UNSURE');
                        }
                    }catch(Exception $e){
                        continue;
                    }
                }
            }
        return true;
    }


    private function create_job_in_simpro($job_data){
        //print_r($job_data);die;
        $data = array();
        $customer_payload = $site_payload = $site_contact_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
        $simpro_customer_response = $simpro_site_response = $simpro_contact_response = $simpro_job_response = $section_response = $cost_center_response = array();

        $user_data = $this->common->fetch_where('tbl_user_details','*',array('full_name' => $job_data['created_by']));

            //print_r($user_data);die;

        if (isset($user_data['simpro_user_id']) && ($user_data['simpro_user_id'] == '' || $user_data['simpro_user_id'] == NULL) && $job_data['created_by'] != NULL) {
                //return false;
        }

        try {

            //SIMPRO CUSTOMER API BODY
            $customer_payload['CompanyName'] = $job_data['company_name'];
            $customer_payload['Phone'] = $job_data['contact_no'];
            $customer_payload['AltPhone'] = $job_data['contact_no'];
            $customer_payload['Address'] = array(
                'Address' => $job_data['address'],
                'City' => $job_data['suburb'],
                'State' =>  $job_data['state'],
                'PostalCode' => $job_data['postcode'],
                'Country' => 'AUS'
            );
            $customer_payload['CustomerType'] = 'Customer';
            $customer_payload['Email'] = $job_data['customer_email'];
            $customer_payload['EIN'] = $job_data['abn'];

            if ($job_data['simpro_cust_id'] == NULL || $job_data['simpro_cust_id'] == '') {
                    //Create Customer in SIMPRO
                $simpro_customer_response = $this->simpro->create_customer($customer_payload);
                if (!isset($simpro_customer_response['error'])) {
                    $simpro_cust_id = (int) $simpro_customer_response['ID'];
                    $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_cust_id' => $simpro_cust_id));

                        //simPRO Customer Site
                    $site_payload['Name'] = $job_data['address'] . ' Site';
                    $site_payload['Address'] = array(
                        'Address' => $job_data['address'],
                        'City' => $job_data['suburb'],
                        'State' =>  $job_data['state'],
                        'PostalCode' => $job_data['postcode'],
                        'Country' => 'AUS'
                    );

                        //Site Primary Contact
                    $site_payload['PrimaryContact']['GivenName'] = $job_data['customer_fname'];
                    $site_payload['PrimaryContact']['FamilyName'] = $job_data['customer_lname'];
                    $site_payload['PrimaryContact']['Email'] =  $job_data['customer_email'];
                    $site_payload['PrimaryContact']['CellPhone'] =  $job_data['contact_no'];

                    $simpro_site_id = (int) $job_data['simpro_site_id'];
                    if ($simpro_site_id == NULL || $simpro_site_id == '') {
                            //Create Customer Site in SIMPRO
                        $site_payload['Customers'] = array($simpro_cust_id);
                        $simpro_site_response = $this->simpro->create_customer_site($site_payload);
                        $simpro_site_id = (int) $simpro_site_response['ID'];
                        $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_site_id' => $simpro_site_id));
                    } else {
                            //Update Customer Site in SIMPRO
                        $simpro_site_response = $this->simpro->update_customer_site($site_payload, $simpro_site_id);
                    }
                }
            } else {
                    //Update Customer in SIMPRO
                $simpro_cust_id = (int) $job_data['simpro_cust_id'];
                $simpro_site_id = (int) $job_data['simpro_site_id'];
                $simpro_customer_response = $this->simpro->update_customer($customer_payload, $simpro_cust_id);
                if (!isset($simpro_customer_response['error'])) {
                    $site_payload['Name'] = $job_data['address'] . ' Site';
                    $site_payload['Address'] = array(
                        'Address' => $job_data['address'],
                        'City' => $job_data['suburb'],
                        'State' =>  $job_data['state'],
                        'PostalCode' => $job_data['postcode'],
                        'Country' => 'AUS'
                    );

                        //Site Primary Contact
                    $site_payload['PrimaryContact']['GivenName'] = $job_data['customer_fname'];
                    $site_payload['PrimaryContact']['FamilyName'] = $job_data['customer_lname'];
                    $site_payload['PrimaryContact']['Email'] =  $job_data['customer_email'];
                    $site_payload['PrimaryContact']['CellPhone'] =  $job_data['contact_no'];

                    if ($simpro_site_id == NULL || $simpro_site_id == '') {
                            //Create Customer Site in SIMPRO
                        $site_payload['Customers'] = array($simpro_cust_id);
                        $simpro_site_response = $this->simpro->create_customer_site($site_payload);
                        $simpro_site_id = (int) $simpro_site_response['ID'];
                        $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_site_id' => $simpro_site_id));
                    } else {
                            //Update Customer Site in SIMPRO
                        $simpro_site_response = $this->simpro->update_customer_site($site_payload, $simpro_site_id);
                    }
                }
            }

                //Create or Update Job in Simpro
            if ($simpro_cust_id != NULL && $simpro_site_id != NULL) {
                $prds = $ae = '';
                $job_payload['Customer'] = (int) $simpro_cust_id;
                $job_payload['Site'] = (int) $simpro_site_id;
                $job_payload['DateIssued'] = date('Y-m-d');
                $job_payload['Stage'] = 'Pending';
                if (isset($user_data['simpro_user_id']) && ($user_data['simpro_user_id'] != '' && $user_data['simpro_user_id'] != NULL)) {
                    $job_payload['Salesperson'] = (int) $user_data['simpro_user_id'];
                }
                $job_payload['Tags'] = [33];

                $simpro_job_id = (int) $job_data['simpro_job_id'];
                if ($simpro_job_id == NULL) {
                        //Create JOB in Simpro
                    $job_payload['Type'] = 'Service';
                    $simpro_job_response = $this->simpro->create_job($job_payload);
                    $simpro_job_id = (int) $simpro_job_response['ID'];
                    $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_job_id' => $simpro_job_id));
                } else {
                        //Update JOB in Simpro
                    $simpro_job_response = $this->simpro->update_job($job_payload, $simpro_job_id);
                }
            }else{
                return false;
            }

            $nature_of_ownership_list = ["Owner","Tenant", "Landlord", "Unknown"];
                //Update Customer Custom Field Data
            if (!isset($simpro_customer_response['error'])) {
                $custom_fields = $simpro_customer_response['CustomFields'];
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $key => $value) {
                        $sp_custom_field_id = $value['CustomField']['ID'];
                        $sp_custom_field_name = $value['CustomField']['Name'];
                        if ($sp_custom_field_name == 'Eform ABN' && $job_data['abn'] != NULL) {
                            $this->simpro->update_customer_custom_fields($simpro_cust_id, $sp_custom_field_id, $job_data['abn']);
                        } else if ($sp_custom_field_name == 'Eform Email' && $job_data['customer_email'] != NULL) {
                            $this->simpro->update_customer_custom_fields($simpro_cust_id, $sp_custom_field_id, $job_data['customer_email']);
                        } else if ($sp_custom_field_name == 'Trading Name') {
                                //$this->simpro->update_customer_custom_fields($simpro_cust_id, $sp_custom_field_id, $business_details->trading_name);
                        } else if ($sp_custom_field_name == 'Nature Of Ownership') {
                            if(in_array($job_data['owner_tenant'],$nature_of_ownership_list)){
                                $this->simpro->update_customer_custom_fields($simpro_cust_id, $sp_custom_field_id, $job_data['owner_tenant']);
                            }
                        }
                    }
                }
            }

                //Update Job Custom Field Data and Add Section and Cost Center
            if (!isset($simpro_job_response['error'])) {
                $job_custom_fields = $simpro_job_response['CustomFields'];
                if (!empty($job_custom_fields)) {
                    foreach ($job_custom_fields as $key => $value) {
                        $sp_custom_field_id = $value['CustomField']['ID'];
                        $sp_custom_field_name = $value['CustomField']['Name'];
                        if ($sp_custom_field_name == 'Job Type') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'LED');
                        }else if ($sp_custom_field_name == 'Nature Of Ownership') {
                            if(in_array($job_data['owner_tenant'],$nature_of_ownership_list)){
                                $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['owner_tenant']);
                            }
                        } else if ($sp_custom_field_name == 'SUB STATUS (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['sub_status']);
                        } else if ($sp_custom_field_name == 'STATUS (DATA FORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['status']);
                        } else if ($sp_custom_field_name == 'WORK TYPE (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['work_type']);
                        } else if ($sp_custom_field_name == 'JOB ID (DATAFORCE)') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, $job_data['job_id']);
                        }
                    }
                }

                $simpro_section_id = (int) $job_data['simpro_section_id'];
                if ($simpro_section_id == NULL && $simpro_job_id != NULL) {
                        //Create Job Section and Cost Center
                    $section_response = $this->simpro->create_job_section([], $simpro_job_id);
                    $simpro_section_id = $section_response['ID'];
                    $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_section_id' => $simpro_section_id));
                    $cost_center_payload['CostCenter'] = (isset($job_data['state']) &&  $job_data['state'] == 'NSW') ? 71 : 12;
                    $cost_center_response = $this->simpro->create_job_cost_center($cost_center_payload, $simpro_job_id, $simpro_section_id);
                }
            }

            //Create Job Attachemnt (Booking Form)
            /**$attachment_payload['Filename'] = $filename_new;
            $attachment_payload['Base64Data'] = $b64_file;
            $attachment_payload['Public'] = FALSE;
            $attachment_payload['Email'] = FALSE;
            //Create JOB Attachment in Simpro
            $simpro_attachment_response = $this->simpro->create_job_attachment($attachment_payload, $simpro_job_id);
            $simpro_attachment_id = $simpro_attachment_response['ID'];
            $this->common->update_data('tbl_dataforce_jobs', array('id' => $job_data['id']), array('simpro_attachment_id' => $simpro_attachment_id));*/
            
        } catch (ErrorException $e) {
            return false;
        } catch (Exception $e) {
            return false;
        }

        if (!isset($simpro_job_response['error'])) {
            //Send Success Mail
            return true;
        } else {
            return false;
        }

        echo json_encode($data);
        die;
    }

    private function create_job_images_in_simpro($job_id,$simpro_job_id,$job_dataid){
        $is_attachment_exists = $this->common->num_rows('tbl_dataforce_job_attachments',array('job_id' => $job_id,'job_data_id' => $job_dataid));
        if($is_attachment_exists > 0){
            return true;
        }
        $b64_file = $this->dataforce->get_job_files($job_dataid);
        if(!isset($b64_file['error'])){
            $attachment_payload = array();
            $attachment_payload['Filename'] = md5(date('ymdhis')) . '.jpeg';
            $attachment_payload['Base64Data'] = $b64_file;
            $attachment_payload['Public'] = FALSE;
            $attachment_payload['Email'] = FALSE;

            //Create JOB Attachment in Simpro
            $simpro_attachment_response = $this->simpro->create_job_attachment($attachment_payload, $simpro_job_id);
            $simpro_attachment_id = $simpro_attachment_response['ID'];

            //Insert Attachment in Db
            $attachment_insert_data = array();
            $attachment_insert_data['job_id'] = $job_id;
            $attachment_insert_data['job_data_id'] = $job_dataid;
            $attachment_insert_data['simpro_attachment_id'] = $simpro_attachment_id;
            $this->common->insert_data('tbl_dataforce_job_attachments',$attachment_insert_data);
            
            return true;
        }
    }
    
    
    private function upload_images_to_simpro($job_data_db,$appointment_data){
       $appointemnt_id = $appointment_data['id'];  
       $appointment_details_data = $this->dataforce->get_appointment_details($appointemnt_id);

        if(isset($appointment_details_data['error'])){
            return true;
        }

        if(!isset($appointment_details_data['Appointments'])){
            return true;
        }

        //Now proceed with job images/files
        $appointment_details_files = array();

        $appointment_details_datalist = (isset($appointment_details_data['Appointments']['Appointment']['Job']['DataList'])) ? $appointment_details_data['Appointments']['Appointment']['Job']['DataList'] : [];

        if(!empty($appointment_details_datalist)){
            $appointment_details_files = $appointment_details_data['Appointments']['Appointment']['Job']['DataList']['DataItem'];
        }

        //Upload Job Images to sIMPRO
        if(!empty($appointment_details_files)){
            foreach($appointment_details_files as $file){
                if(isset($file['@attributes'])){
                    $job_dataid = $file['@attributes']['id'];
                    $this->create_job_images_in_simpro($job_data_db['job_id'],$job_data_db['simpro_job_id'],$job_dataid);
                }
            }
        }
        
        return true;
    }


    private function create_lock_file_and_check_existance(){
        $filename = FCPATH . "dataforce_script.lock";
        $lifelimit = 1790; // in Second lifetime to prevent errors

        /* check lifetime of file if exist */
        if(file_exists($filename)){
           $lifetime = time() - filemtime($filename);
        }else{
           $lifetime = 0;
        }
        
        /* check if file exist or if file is too old */
        if(!file_exists($filename) ||  $lifetime > $lifelimit){
            if($lifetime > $lifelimit){
                unlink($filename); //Suppress if exist and too old
                return true;
            }
            $file = fopen($filename, "w+"); // Create lockfile
            if($file == false){
                log_message('error', 'Some issue in creating file.');
                return false;
            }
            return true;
        }else{
            return false;
        }
    }


    private function get_key_for_specific_column($job_columns,$search_column){
        foreach($job_columns as $key => $value){
            if(trim($value) == trim($search_column)){
                return $key;
            }
        }
        return null;
    }
    
    public function fix_dataforcejob_to_simpro(){
        $jobs = $this->dataforce->list_jobs_by_appointment_api();
        $jobs = $jobs['Appointment'];

        //echo "<pre>";
        //print_r($jobs);die;
        if(!empty($jobs)){
            for($i = 0; $i < count($jobs); $i++){

                $appointment_data = $jobs[$i]['@attributes'];
                $job_id = $appointment_data['job_id'];

                if($i != 5){
                    //continue;      
                }
                
                if($appointment_data['modified_date'] < 20191024235418){
                   continue;
                }
                
                //Check if particular Job appointment is HEER Installation or Installation then only proceed dont preoced for othe appointment types
                if(isset($appointment_data['work_type']) && !$this->components->find_string('Installation',$appointment_data['work_type'],'i')){
                    continue;
                }
                
                $status = (isset($appointment_data['status'])) ? $appointment_data['status'] : NULL;
                $sub_status = (isset($appointment_data['sub_status'])) ? $appointment_data['sub_status'] : NULL;

                //Check if job exists with same status and sub status then dont proceed
                 $is_job_exists = $this->common->fetch_row('tbl_dataforce_jobs','*',array('job_id' => $job_id));
                 if(count($is_job_exists) > 0 && $is_job_exists['status'] == $status && $is_job_exists['sub_status'] == $sub_status){
                    continue;
                 }

                //Insert in Local DB to maintain the jobs being inserted in simpro from dataforce
                 $appointemnt_id = $appointment_data['id'];  
               

                if($job_id != null){
                    $job_data = $is_job_exists;
                    $job_data['abn'] = (isset($appointment_details['ABN_ACN'])) ? $appointment_details['ABN_ACN'] : NULL;
                    $job_data['status'] = $status;
                    $job_data['sub_status'] = $sub_status;
                    unset($job_data['data']);
    
                    //Only Update Status and Sub Status Field
                    $this->update_simpro_job_custom_fields($job_data);
                    $this->common->update_data('tbl_dataforce_jobs',
                        array('id' => $job_data['id']),
                        array('work_type' => $appointment_data['work_type'], 'status' => $status,'sub_status' => $sub_status,'updated_at' => date('Y-m-d H:i:s'))
                    );
                }
            }
        }
    }
    
    
    /** Simpro Job Webhook */
    
    public function simpro_job_callback(){
        log_message('error', 'Simrpo Webhook Callback Final Triggered');
        $callback_data = json_decode(file_get_contents("php://input"), true);
        $callback_trigger_id = $callback_data['ID'];
        if($callback_trigger_id == 'job.updated'){
            log_message('error', 'Simrpo Webhook Callback - Job Updated');
            $simpro_job_id = (isset($callback_data['reference'])) ? $callback_data['reference']['jobID'] : NULL;
            if($simpro_job_id != NULL && $simpro_job_id != ''){
                //Get Simpro Job Details
                $job_details = $this->simpro->list_job_details([],$simpro_job_id);
                //Now from Simpro Custom Fields Extract Dataforce Job Id
                $custom_fields = isset($job_details['CustomFields']) ? $job_details['CustomFields'] : NULL;
                $dataforce_job_id = NULL;
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $key => $value) {
                        $sp_custom_field_id = $value['CustomField']['ID'];
                        $sp_custom_field_name = $value['CustomField']['Name'];
                        if($sp_custom_field_name == 'JOB ID (DATAFORCE)'){
                            $dataforce_job_id = $value['Value'];
                        }
                    }
                }
                //Now if dataforce id is not null then get appointments from dataforce
                if($dataforce_job_id != NULL && $dataforce_job_id != ''){
                   $jobs = $this->dataforce->list_jobs_by_appointment_api();
                   $jobs = $jobs['Appointment'];
                   if(!empty($jobs)){
                       for($i = 0; $i < count($jobs); $i++){
                            $appointment_data = $jobs[$i]['@attributes'];
                            $job_id = $appointment_data['job_id'];
                            $company_name = $appointment_data['company_name'];
                            
                            if(($job_id == $dataforce_job_id) && isset($appointment_data['work_type']) && $this->components->find_string('Installation',$appointment_data['work_type'],'i')){
                                $this->handle_dataforce_job($jobs[$i],$simpro_job_id);
                                break;
                            }
                       }
                       //Now call another function for Assessment appointment
                       $this->proccess_assessment_appointment_for_simpro_job_trigger($jobs);
                    } 
                }
            }
        }
    }
    
    private function proccess_assessment_appointment_for_simpro_job_trigger($jobs){
        for($i = 0; $i < count($jobs); $i++){
            $appointment_data = $jobs[$i]['@attributes'];
            $job_id = $appointment_data['job_id'];
            $company_name = $appointment_data['company_name'];
            
            //Check for Cusomter existance first in dataforce table
            $is_job_exists = $this->cron->fetch_dataforce_jobs($job_id,FALSE,FALSE);
            
            //Check if work type is Assessment and job exists in db then only upload images to simpro
            if(!empty($is_job_exists) && isset($appointment_data['work_type']) && $this->components->find_string('Assessment',$appointment_data['work_type'],'i')){
                $this->upload_images_to_simpro($is_job_exists,$appointment_data);
            }
        }
    }
    
    
    private function handle_dataforce_job($jobs,$simpro_job_id){
        $appointment_data = $jobs['@attributes'];
        $job_id = $appointment_data['job_id'];
        $company_name = $appointment_data['company_name'];
                
                
        $is_data_in_dataforce_table = TRUE;
        //Check for Cusomter existance first in dataforce table
        $is_job_exists = $this->cron->fetch_dataforce_jobs($job_id,$company_name);
        if(empty($is_job_exists)){ $is_data_in_dataforce_table = FALSE; }
        
        log_message('error', 'Simpro Webhook Datforce: ' .json_encode($jobs));
        
        $status = (isset($appointment_data['status'])) ? $appointment_data['status'] : NULL;
        $sub_status = (isset($appointment_data['sub_status'])) ? $appointment_data['sub_status'] : NULL;
        
        //Insert in Local DB to maintain the jobs being inserted in simpro from dataforce
        $appointemnt_id = $appointment_data['id'];  
        
        //Call appointemtn api to get detail job data
        $appointment_details_data = $this->dataforce->get_appointment_details($appointemnt_id);
        
        if(isset($appointment_details_data['error'])){
            return true;
        }

        if(!isset($appointment_details_data['Appointments'])){
            return true;
        }
        
        if(!isset($appointment_details_data['Appointments']['Appointment'])){
            return true;
        }

        $appointment_details = $appointment_details_data['Appointments']['Appointment']['Details'];

        $appointment_details_questions = (isset($appointment_details_data['Appointments']['Appointment']['PhoneQuestions'])) ? $appointment_details_data['Appointments']['Appointment']['PhoneQuestions'] : [];

        $appointment_details_witness = (isset($appointment_details_data['Appointments']['Appointment']['FieldWorkerAction'])) ? $appointment_details_data['Appointments']['Appointment']['FieldWorkerAction']: [];

        $owner_tenant = NULL;
        $created_by = NULL;
        $contact_no = NULL;


        if(!is_array($appointment_details['HomePhone'])){
            $contact_no = $appointment_details['HomePhone'];
        }

        if($contact_no != NULL && !is_array($appointment_details['MobilePhone'])){
            $contact_no = $appointment_details['MobilePhone'];
        }
                
        if(!empty($appointment_details_witness) && isset($appointment_details_witness['Witness'])){
            if(isset($appointment_details_witness['Witness']['FieldWorker'])){
                $created_by =  (isset($appointment_details_witness['Witness']['FieldWorker']['Name'])) ? $appointment_details_witness['Witness']['FieldWorker']['Name'] : NULL;
            }
        }
    
        if(!empty($appointment_details_questions) && isset($appointment_details_questions['Question'])){
            $owner_tenant =  (isset($appointment_details_questions['Question']['3'])) ? $appointment_details_questions['Question'] : NULL;
        }

        $job_id_key = 'job_id';//$this->get_key_for_specific_column($jobs[0],'Job Id');
        $company_name_key = 'CompanyName';//$this->get_key_for_specific_column($jobs[0],'Company Name');
        $customer_fname_key = 'FirstName';//$this->get_key_for_specific_column($jobs[0],'Customer');
        $customer_lname_key = 'Surname';//$this->get_key_for_specific_column($jobs[0],'Customer');
        $customer_email_key = 'Email';//$this->get_key_for_specific_column($jobs[0],'Email Address');
        $address_key = 'Address';//$this->get_key_for_specific_column($jobs[0],'Address');
        $state_key = 'State';//$this->get_key_for_specific_column($jobs[0],'State');
        $suburb_key = 'Suburb';//$this->get_key_for_specific_column($jobs[0],'Suburb');
        $postcode_key = 'Postcode';

        if($job_id != null){
            $insert_data = array(
                'job_id' => $job_id,
                'created_by' => $created_by,
                'work_type' => (isset($appointment_data['work_type'])) ? $appointment_data['work_type'] : NULL,
                'company_name' => (isset($appointment_details[$company_name_key])) ? $appointment_details[$company_name_key] : NULL,
                'customer_fname' => (isset($appointment_details[$customer_fname_key])) ? $appointment_details[$customer_fname_key]  : NULL,
                'customer_lname' => (isset($appointment_details[$customer_lname_key])) ? $appointment_details[$customer_lname_key] : NULL,
                'contact_no' => $contact_no,
                'customer_email' => (isset($appointment_details[$customer_email_key])) ? $appointment_details[$customer_email_key] : NULL,
                'address' => (isset($appointment_details[$address_key])) ? $appointment_details[$address_key] : NULL,
                'state' => (isset($appointment_details[$state_key])) ? $appointment_details[$state_key] : NULL,
                'suburb' => (isset($appointment_details[$suburb_key])) ? $appointment_details[$suburb_key] : NULL,
                'postcode' => (isset($appointment_details[$postcode_key])) ? $appointment_details[$postcode_key] : NULL,
                'job_sync_status' => 2,
                'status' => $status,
                'sub_status' => $sub_status,
                'data' => json_encode($appointment_details_data)
            );

            if($insert_data['company_name'] == NULL){
                return true;
            }
            
            log_message('error', 'Simpro Webhook Final Update to Simpro');
            
            //Sleep for 30 seconds in order for user to close job on simpro
            sleep(30);
            
            if(count($is_job_exists) > 0){
                $job_data = $is_job_exists;
                $job_data['abn'] = (isset($appointment_details['ABN_ACN'])) ? $appointment_details['ABN_ACN'] : NULL;
                $job_data['owner_tenant'] = $owner_tenant;
                $job_data['status'] = $status;
                $job_data['sub_status'] = $sub_status;
                unset($job_data['data']);
                        
                if($is_data_in_dataforce_table == FALSE){
                    //Insert Job
                    $insert_data['simpro_job_id'] = $simpro_job_id;
                    $insert_data['job_sync_status'] = 1;
                    $this->common->insert_data('tbl_dataforce_jobs',$insert_data);
                    $insert_data['owner_tenant'] = $owner_tenant;
                    $this->update_simpro_job_custom_fields($insert_data);
                }else{
                    //Only Update Status and Sub Status Field
                    $this->update_simpro_job_custom_fields($job_data);
                    $this->common->update_data('tbl_dataforce_jobs',
                        array('id' => $job_data['id']),
                        array('simpro_job_id' => $simpro_job_id,'status' => $status,'sub_status' => $sub_status,'updated_at' => date('Y-m-d H:i:s'))
                    );
                }
            }else{
                $insert_data['simpro_job_id'] = $simpro_job_id;
                $insert_data['job_sync_status'] = 1;
                $this->common->insert_data('tbl_dataforce_jobs',$insert_data);
                $insert_data['owner_tenant'] = $owner_tenant;
                $this->update_simpro_job_custom_fields($insert_data);
            }
            
            //Now proceed with job images/files
            $appointment_details_files = array();

            $appointment_details_datalist = (isset($appointment_details_data['Appointments']['Appointment']['Job']['DataList'])) ? $appointment_details_data['Appointments']['Appointment']['Job']['DataList'] : [];

            if(!empty($appointment_details_datalist)){
                $appointment_details_files = $appointment_details_data['Appointments']['Appointment']['Job']['DataList']['DataItem'];
            }

            //Upload Job Images to sIMPRO
            if(!empty($appointment_details_files)){
                $updated_job_data = $this->common->fetch_row('tbl_dataforce_jobs','simpro_job_id',array('job_id' => $job_id));
                foreach($appointment_details_files as $file){
                    $job_dataid = $file['@attributes']['id'];
                    $this->create_job_images_in_simpro($job_id,$updated_job_data['simpro_job_id'],$job_dataid);
                }
            }
        }
    }

}

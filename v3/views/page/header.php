<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SolarRunApp Knowledgebase</title>
        <!-- LOADING STYLESHEETS -->
        <link href="<?php echo site_url('assets/page/css/bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/page/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/page/css/style.css'); ?>" rel="stylesheet">
        <meta type="title" value="SolarRunApp Knowledgebase" />
        <meta type="description" value="SolarRunApp Knowledgebase" />
        <style>
            .navigation{margin-top:0px;}
        </style>
        <script>
            var base_url = '<?php echo site_url(); ?>';
        </script>
    </head>
    <body>
        <div class="container-fluid featured-area-white-border">
            <div class="container">
                <div class="row">
                    <div class="pull-left">
                        <div class="login-box"> <a href="#"><i class="fa fa-arrow-left"></i> <a href="<?php echo site_url('admin/dashboard'); ?>">Back to Admin</a></a>
                        </div>
                    </div>
                    <!--<div class="col-lg-6">
                        <div class="login-box border-right-1"> <a href="#"><i class="fa fa-key"></i> Login</a>
                        </div>
                        <div class="login-box border-left-1 border-right-1"> <a href="#"><i class="fa fa-pencil"></i> Sign Up</a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- TOP NAVIGATION -->
        <!--  <div class="container-fluid">
              <div class="navigation">
                  <div class="row">
                      <ul class="topnav">
                          <li></li>
                          <li><a href="index.html"><i class="fa fa-home"></i>  Admin</a>
                          </li>
                          <li><a href="knowledge-base.html"><i class="fa fa-book"></i> Knowledge Base</a>
                          </li>
                          <li><a href="articles.html"><i class="fa fa-file-text-o"></i> Articles</a>
                          </li>
                          <li><a href="faq.html"><i class="fa fa-lightbulb-o"></i> FAQ</a>
                          </li>
                          <li class="icon"> <a href="javascript:void(0);" onclick="myFunction()">&#x2630;</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div> -->
        <!-- END TOP NAVIGATION -->
        <!-- SEARCH FIELD AREA -->
        <div class="searchfield bg-hed-six">
            <div class="container" style="padding-top: 20px; padding-bottom: 20px;">
                <div class="row text-center margin-bottom-20">
                    <h1 class="white"> Knowledge Base</h1>
                    <span class="nested"> Learn to use SolarRun App </span>
                </div>
                <br>
                <div class="row search-row">
                    <div class="col-md-12">
                        <form method="GET" action="<?php echo site_url('admin/knowledgebase/search'); ?>" >
                            <input type="text" name="keyword" class="search" placeholder="What do you need help with?"> <button type="submit" class="buttonsearch btn btn-info btn-lg" >Search</button>
                        </form>    
                    </div>
                </div>
            </div>
        </div>
        <!-- END SEARCH FIELD AREA -->
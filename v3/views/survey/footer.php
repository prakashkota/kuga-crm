<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>We can help with expert advice</h4>
            </div>
            <div class="col-sm-12">
                <div class="textwidget"><div class="sidebar-outer">
                        <ul>
                            <li><a><img src="https://www.solarrun.com.au/solar-panels/solar_panel/images/mobile-icon.png" alt=""> Call 1300 0 SOLAR</a></li>
                            <li><a><img src="https://www.solarrun.com.au/solar-panels/solar_panel/images/comment-icon.png" alt=""> Have your questions answered</a></li>
                            <li><a><img src="https://www.solarrun.com.au/solar-panels/solar_panel/images/home-icon.png" alt=""> Assess your premise</a></li>
                            <li><a><img src="https://www.solarrun.com.au/solar-panels/solar_panel/images/coffee-cup-icon.png" alt=""> Speak to our experts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <ul id="menu-footer-menu-1" class="anather-link"><li id="menu-item-418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-418"><a href="https://www.solarrun.com.au/solar-panels/">SOLAR DEALS</a></li>
                        <li id="menu-item-327" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-327"><a href="https://www.solarrun.com.au/battery-storage/">BATTERY STORAGE</a></li>
                        <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://www.solarrun.com.au/solar-hot-water/">SOLAR HOT WATER</a></li>
                    </ul>                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <ul class="social-links">
                    <li><a href=" https://www.facebook.com/solarrunaus"><i class="fa fa-2x fa-facebook"></i></a></li>
                    <li><a href=" https://au.linkedin.com/company/solar-run"><i class="fa fa-2x fa-linkedin"></i></a></li>
                    <li><a href=" https://www.youtube.com/channel/UC__jZUeTu8gv9060VjJFEbw"><i class="fa fa-2x fa-youtube"></i></a></li>
                </ul>
                <p>
                    Copyright � Solar Run 2018. All rights reserved.                </p>

            </div>

        </div>
    </div>
</footer>

<script>
    $(document).ready(function () {
        $('header nav').meanmenu();
        $('body').addClass("custom-body-class");
        $('.mean-bar a.meanmenu-reveal').click(function (e) {

            if ($('.custom-body-class').hasClass('custom-open-nav')) {
                $(".custom-body-class").removeClass("custom-open-nav");
            } else {
                $(".custom-body-class").addClass("custom-open-nav");
            }
            e.preventDefault();
        });             
    });
</script>
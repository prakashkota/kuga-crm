<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Add Article</h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/knowledgebase/article/manage') ?>"><i class="icon-arrow-left"></i> Manage Articles </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div id="message"></div>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Article Details</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <input type="text" name="article[article_title]"  class="form-control" value="<?php echo (!empty($article)) ? $article['article_title'] : ''; ?>" placeholder="First Name"  >
                                                <?php echo form_error('article[article_title]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Category</label>
                                                <select name="article[category_id]"  class="form-control"  required>
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    foreach ($article_categories as $key => $value) {
                                                        ?>
                                                    <option value="<?php echo $value['id']; ?>" <?php if((!empty($article) && ($value['id'] == $article['category_id']))){ ?> selected="" <?php } ?>><?php echo $value['category_name']; ?></option>   
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('article[category_id]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <textarea  name="article[article_description]"  class="form-control"><?php echo (!empty($article)) ? $article['article_description'] : ''; ?></textarea>
                                                <?php echo form_error('article[article_description]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                              <label>Status</label>
                                              <input type="checkbox" name="article[status]" <?php if(!empty($article) && $article['status'] == 1){ ?> checked="" <?php } ?> value="<?php echo (!empty($article)) ? $article['status'] : 1; ?>"  >
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttwrap">
                        <input type="submit" class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.1/tinymce.min.js"></script>
  <script>
      tinymce.init({ 
          selector:'textarea' 
      });
  </script>
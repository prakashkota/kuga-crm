<html>

<head>
    <title>Commercial Proposal</title>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }

        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo site_url('assets/css/commercial-proposal.css?v=' . time()); ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $(document).on('click', '#send_proposl_btn', function() {
            $('#send_proposal_to_customer_modal').modal('show');

            CKEDITOR.replace(
                document.querySelector('#invoice_mail_body'), {
                    toolbar: [{
                            name: 'basicstyles',
                            groups: ['basicstyles', 'cleanup'],
                            items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat']
                        },
                        {
                            name: 'paragraph',
                            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                        },
                        {
                            name: 'styles',
                            items: ['Styles', 'Format', 'Font', 'FontSize']
                        },
                    ]
                });
            //$('textarea#invoice_mail_body').summernote();
            return false;
        });

        $(document).on('click', '#send_proposal_to_customer_send_btn', function() {
            var email_to = $("#email_to").val();
            if (email_to == '') {
                toastr['error']('Please Provide a Email Address');
                return false;
            }

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(email_to)) {
                toastr['error']('Please Provide a Valid Email Address');
                return false;
            }

            var email_subject = $("#email_subject").val();
            if (email_subject == '') {
                toastr['error']('Please Provide a Email Subject');
                return false;
            }


            var formData = $('#send_proposal_to_customer_form').serialize();
            formData += '&uuid=<?php echo $proposal_data['proposal_uuid']; ?>';
            $.ajax({
                url: "<?php echo site_url(); ?>admin/proposal/commercial/send_proposal_to_customer",
                type: 'post',
                dataType: "json",
                data: formData,
                beforeSend: function() {
                    $('#send_proposal_to_customer_send_btn').attr('style', 'display:none;');
                },
                success: function(data) {
                    $('#send_proposal_to_customer_send_btn').removeAttr('style');
                    toastr['success'](data.status);
                    $('#send_proposal_to_customer_modal').modal('hide');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $('#send_proposal_to_customer_send_btn').removeAttr('style');
                    var r = JSON.parse(xhr.responseText);
                    toastr["error"](r.status);
                }
            });
        });

        $(document).ready(function() {
            $(document).on("scroll", onScroll);

            $('a[href^="#"]').on('click', function(e) {
                e.preventDefault();
                $(document).off("scroll");

                $('a').each(function() {
                    $(this).parent().removeClass('active');
                });

                $(this).parent().addClass('active');

                var target = this.hash,
                    menu = target;
                $target = $(target);
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top + 20
                }, 500, 'swing', function() {
                    window.location.hash = target;
                    $(document).on("scroll", onScroll);
                });
            });
        });

        function onScroll(event) {
            var scrollPos = $(document).scrollTop();
            $('.sidebar_container a').each(function() {
                var currLink = $(this);
                var refElement = $(currLink.attr("href"));
                if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                    $('.sidebar_container ol li').removeClass("active");
                    currLink.parent().addClass("active");
                } else {
                    currLink.parent().removeClass("active");
                }
            });
        }
    </script>

</head>

<?php
/** Manage Data Here */
$mapping_tool = empty($mapping_tool) ? [] : $mapping_tool;


if (isset($proposal_data['panel_layout_image'])) {
    $proposal_data['panel_layout_image'] = '';
}

$mapping_tool['snapshot1'] = isset($mapping_tool['snapshot1']) && $mapping_tool['snapshot1'] != NULL ? $mapping_tool['snapshot1'] : $proposal_data['panel_layout_image'];
$mapping_tool['snapshot2'] = isset($mapping_tool['snapshot2']) && $mapping_tool['snapshot2'] != NULL ? $mapping_tool['snapshot2'] : $proposal_data['panel_layout_image'];
$mapping_tool['notes'] = isset($mapping_tool['notes']) && $mapping_tool['notes'] != NULL ? $mapping_tool['notes'] : '';
?>

<body>
    <header class="header">
        <div class="top-head">
            <div class="container clearfix">
                <div class="head-top-nav">
                    <ul id="menu-top-menu" class="menu">
                        <li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="https://www.13kuga.com.au/contact-us/">&nbsp;</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header-md clearfix">
            <div class="container">
                <div class="logo"><a href="https://www.13kuga.com.au"><img src="https://www.13kuga.com.au/wp-content/uploads/2019/03/logo.png" alt="logo" class="lazyloading" data-was-processed="true"></a></div>
                <div class="navigation iphonNav clearfix" style="display: block;">
                    <ul id="nav" class="menu">
                        <li class="menu-item "><span class="menu-item-text">COMMERCIAL SOLAR AND BATTERY EXPERTS</span></li>

                    </ul>
                </div>
                <div class="call-info"><a href="tel:135842"><img src="https://www.13kuga.com.au/wp-content/uploads/2019/03/kuga-phno.png" alt="kuga-phno" class="lazyloading" data-was-processed="true"></a></div>
            </div>
        </div>
    </header>
    <div class="wrapper">

        <div class="sidebar">
            <div class="sidebar_container">
                <h3>Navigator:</h3>
                <ol>
                    <li>
                        <a href="#page_1" data-id="1">
                            <span class="sidebar__page_text">Cover Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_2" data-id="2">
                            <span class="sidebar__page_text">About KUGA</span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_3" data-id="3">
                            <span class="sidebar__page_text">Why Choose KUGA
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_4" data-id="4">
                            <span class="sidebar__page_text">Site Plan
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_5" data-id="5">
                            <span class="sidebar__page_text">Panel Layout
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_6" data-id="6">
                            <span class="sidebar__page_text">Inverter Layout
                            </span>
                        </a>
                    </li>
                    <!--
        <li>
          <a href="#page_7" data-id="7">
            <span class="sidebar__page_text">Mount Kit Installation
            </span>
          </a>
        </li>-->
                    <li>
                        <a href="#page_8" data-id="8">
                            <span class="sidebar__page_text">System Performance
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_9" data-id="9">
                            <span class="sidebar__page_text">Energy Consumption
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_10" data-id="10">
                            <span class="sidebar__page_text">Average Production</span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_11" data-id="11">
                            <span class="sidebar__page_text">Project Costs
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_12" data-id="12">
                            <span class="sidebar__page_text">Financial Summary
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_13" data-id="13">
                            <span class="sidebar__page_text">Installation Steps
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_14" data-id="14">
                            <span class="sidebar__page_text">Recent Installs
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_15" data-id="15">
                            <span class="sidebar__page_text">Recent Installs
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_17" data-id="17">
                            <span class="sidebar__page_text">Our Promise
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_18" data-id="18">
                            <span class="sidebar__page_text">Accreditation
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_19" data-id="19">
                            <span class="sidebar__page_text">CEC Approved Retailer
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_20" data-id="20">
                            <span class="sidebar__page_text">CEC Accredited Installer
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_21" data-id="21">
                            <span class="sidebar__page_text">Media
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#page_22" data-id="22">
                            <span class="sidebar__page_text">Closing page
                            </span>
                        </a>
                    </li>
                </ol>
            </div>
            <a class="btn btn-send" href="javascript:void(0);" id="send_proposl_btn"><i class="fa fa-envelope"></i> Send To Customer</a>
            <a class="btn btn-download" href="<?php echo site_url('admin/proposal/commercial/download_proposal_pdf/' . $proposal_data['proposal_uuid'] . '?view=charts'); ?>" target="__blank" style="text-decoration: none;"><i class="fa fa-file-pdf-o"></i> Download Pdf</a>
        </div>
        <div class="main">
            <div id="page_1">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-1-video', [$proposal_data, $lead_data], TRUE); ?>
            </div>
            <div id="page_2">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-2-video', [], TRUE); ?>
            </div>
            <div id="page_3">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-3', [], TRUE); ?>
            </div>
            <div id="page_4">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-8', [$proposal_data, $lead_data, $mapping_tool, $mappingPanelObjects, $product_data, $solar_panels, $inverter], TRUE); ?>
            </div>
            <div id="page_5">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-spl', [$proposal_data, $lead_data, $mapping_tool, $mappingPanelObjects], TRUE); ?>
            </div>
            <div id="page_6">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-il', [], TRUE); ?>
            </div>
            <div id="page_7" style="display:none;">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-mki', [], TRUE); ?>
            </div>
            <div id="page_8">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-10', [$saving_meter_calculation, $system_performance], TRUE); ?>
            </div>
            <div id="page_9">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-14', [$saving_meter_calculation], TRUE); ?>
            </div>
            <div id="page_10">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-15', [$saving_meter_calculation, $saving_graph_calculation], TRUE); ?>
            </div>
            <div id="page_11">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-16', [$proposal_data, $financial_summary_data, $ppa_year, $lead_data, $year], TRUE); ?>
            </div>
            <div id="page_12">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-17', [$proposal_data, $financial_summary_data, $lead_data, $year, $Cashflow], TRUE); ?>
            </div>
            <div id="page_13">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-9', [], TRUE); ?>
            </div>
            <!--  -->
            <div id="page_14">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-16-1', [], TRUE); ?>
            </div>
            <div id="page_15">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-16-2', [], TRUE); ?>
            </div>

            <div id="page_17">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-19', [], TRUE); ?>
            </div>
            <div id="page_18">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-5', [], TRUE); ?>
            </div>
            <div id="page_19">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-7', [], TRUE); ?>
            </div>
            <div id="page_20">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-6', [], TRUE); ?>
            </div>
            <div id="page_21">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-24-video', [], TRUE); ?>
            </div>
            <div id="page_22">
                <?php echo $this->load->view('pdf_files/solar_proposal_pdf_new/web/page-26', [], TRUE); ?>
            </div>
        </div>
        <div id="imageZoomModal" class="image-modal">
            <span class="image-close">×</span>
            <img class="image-modal-content" id="img01">
            <div id="image-caption"></div>
        </div>
    </div>
    <script>
        $(document).on('click', '.add-picture1', function() {
            var image_src = $(this).attr('data-src');
            var modal = document.getElementById("imageZoomModal");
            var modalImg = document.getElementById("img01");
            var span = document.getElementsByClassName("image-close")[0];
            span.onclick = function() {
                modal.style.display = "none";
            }
            modal.style.display = "block";
            modalImg.src = image_src;
        });
    </script>
</body>

</html>
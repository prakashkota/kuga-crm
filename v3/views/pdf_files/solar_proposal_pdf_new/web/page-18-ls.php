<html>
   <head>
      <title></title>
      <style>
         body{
         font-family: arial;
         }
         .page-18-ls-back-bg{
         background-color: #FFFFFF;
         background-repeat: no-repeat;
         margin: auto;
         padding: 0;
         background-size: 100% 100%;
         max-width: 1287px !important;
         max-height: 910px !important;
         width: 1287px !important;
         height: 910px !important;
         margin-bottom: 20px;
         }
         .page-18-ls .top h2{
         font-size:16px;
         color:#221E1F;
         padding-left: 1044px;
         padding-top: 30px;
         }
         .page-18-ls .top hr {
         width: 76%;
         height:2px;
         margin: -24px 0px 0px 52px;
         background-color: #D01E2A;
         }
         .page-18-ls .text_color{
         color:#D01E2A;
         }
         .page-18-ls .title{ margin-top: -50px;}
         .page-18-ls .title h1 {
         padding-left: 14px;
         margin-left: 48px;
         margin-top: 90px;
         border-left: 5px solid #D01E2A ;
         border-height: 10px;
         }
         .page-18-ls .plasting {
         height: 350px;
         border: 1px solid #c3c3c3;
         margin-left: 48px;
         margin-top: 10px;
         margin-right: 38px;
         }
         .page-18-ls .footer{
         display: inline-flex;
         }
         .page-18-ls .redf {
         height: 10px;
         width: 643px;
         background: red;
         }
         .page-18-ls .greyf {
         height: 10px;
         width: 643px;
         background: #EDEBEC;
         }
         .page-18-ls .iiimage img {
         width: 92%;
         height:200;
         margin-left: 48px;
         margin-top: 19px;
         }
         .page-18-ls .displayy{
         display: inline-flex;
         }
         .page-18-ls .fimagee img {
         margin-left: 1110px;
         }
         .page-18-ls .textp {
         margin-left: 48px;
         padding-bottom: 22px;
         }
         .page-18-ls .textp p {
         margin-top: 40px;
         color: red;
         font-weight: 700;
         font-size: 18px;
         }
         .button-container {
         margin-top: 50px;
         display: flex; /* displays flex-items (children) inline */
         justify-content: space-between; /* MDN: The items are evenly distributed within the alignment container along the main axis. The spacing between each pair of adjacent items is the same. */
         align-items: center; /* centers them vertically */
         }
         .button-left {
         margin-left: 48px;
         padding: 5px 10px;
         text-align: center;
         text-decoration: none;
         font-size: 15px;
         border-radius: 5px;
         border: 1px solid black;
         color: #D01E2A;
         }
         .button-right {
         margin-right: 38px;
         padding: 5px 10px;
         text-align: center;
         text-decoration: none;
         font-size: 15px;
         border-radius: 5px;
         border: 1px solid black;
         color: #D01E2A;
         }
      </style>
   </head>
   <body>
      <div class="page-18-ls-back-bg back-bg page-18-ls">
         <div class="top">
            <h2>
            <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
            <hr>
         </div>
         <div class="title">
            <h1>RECENT<span class="text_color"> INSTALLS</span></h1>
         </div>
         <div class="button-container">
            <a href="javascript:void(0);" class="button-left" style="opacity:0;">< Prev</a>
            <a href="javascript:void(0);" class="button-right">Next ></a>
         </div>
         <div class="site site_1" data-id="1">
            <div class="">
               <div class="iiimage plasting">
                  <iframe height="100%" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
               </div>
            </div>
            <div class="textp">
               <p class="">JOB DETAILS:</p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               <p class="">SYSTEM SIZE: </p>
               10kW
            </div>
         </div>
         <div class="site site_2" style="display:none;" data-id="2">
            <div class="">
               <div class="iiimage plasting">
                  <iframe height="100%" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
               </div>
            </div>
            <div class="textp">
               <p class="">JOB DETAILS:</p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               <p class="">SYSTEM SIZE: </p>
               20kW
            </div>
         </div>
         <div class="site site_3" style="display:none;" data-id="3">
            <div class="">
               <div class="iiimage plasting">
                  <iframe height="100%" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
               </div>
            </div>
            <div class="textp">
               <p class="">JOB DETAILS:</p>
               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               <p class="">SYSTEM SIZE: </p>
               30kW
            </div>
         </div>
         <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
         <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
         </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
      <script>
         var currentSite = 1;
         $('.button-right').click(function(){
             if(currentSite == 3){
                 return false;
             }
             currentSite = currentSite + 1;
             $('.site').hide();
             $('.site_'+currentSite).show();
             if(currentSite > 1){
                $('.button-left').css('opacity','1'); 
             }
             if(currentSite == 3){
                $('.button-right').css('opacity','0'); 
             }
         });
         $('.button-left').click(function(){
             if(currentSite == 1){
                 return false;
             }
             currentSite = currentSite - 1;
             $('.site').hide();
             $('.site_'+currentSite).show();
             if(currentSite == 1){
                $('.button-left').css('opacity','0'); 
             }
             if(currentSite < 3){
                $('.button-right').css('opacity','1'); 
             }
         });
      </script>
   </body>
</html>
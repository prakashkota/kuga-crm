
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tbody>
    <tr>
      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/header.jpg" width="910" height="127" alt=""/></td>
    </tr>
    <tr>
      <td height="45"></td>
    </tr>
    <tr>
      <td valign="top">
        <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td style="font-size:12px; font-style:italic; color:#6a6a6a">To be completed and signed by the authorised signatory from OES company</td>
            </tr>
            <tr>
              <td height="40" style="color: #922a27; font-size:20px;"><strong>Section C - Customer Declaration</strong></td>
            </tr>
            <tr>
              <td style="border-top: solid 1px #969697; height:10px"></td>
            </tr>
            <tr>
              <td><table width="800" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                  <tr>
                    <td width="251" style="font-size:12px; color:#000000;">By signing below, I confirm that on behalf of</td>
                    <td width="549"><input type="text" name="get_nomination_form_details[energy_saver_details][entity_name1]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:530px; height:37px; padding-left:5px; padding-right:5px"/></td>
                  </tr>
                </tbody>
              </table></td>
            </tr>

            <tr>
              <td></td>
            </tr>
            <tr>
              <td height="10"></td>
            </tr>
            <tr>
              <td height="3"></td>
            </tr>

            <tr>
              <td valign="top">
                <table width="800" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="16" valign="top" style="padding-top: 7px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                      <td width="784" style="font-size:12px; color:#000000;">I am satisfied that the lighting upgrade implemented by, or under the supervision of Kuga Electrical Pty Ltd maintains or improves on  the lighting service that was provided previously.</td>
                    </tr>
                    <tr>
                      <td height="50"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                      <td style="font-size:12px; color:#000000;">
                        <table width="780" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="287"><input type="text" name="get_nomination_form_details[energy_saver_details][name1]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:265px; height:37px; padding-left:5px; padding-right:5px"/></td>
                              <td width="493">has paid a net amount of at least $5 (excluding GST) for each megawatt hour of electricity  saved and this amount will not be reimbursed (e.g, through a credit or rebate).</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td valign="top" style="padding-top: 3px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot-space.jpg" width="4" height="7" alt=""/></td>
                      <td style="font-size:12px; color:#000000;">I have been provided with a recommended maintenance schedule to ensure that the installed lighting system (lamps and fittings)  continues to operate appropriately for its full lifetime</td>
                    </tr>
                    <tr>
                      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                      <td style="font-size:12px; color:#000000;">I am aware that I may be asked by IPART or an IPART approved auditor to provide information that assists in verifying that Energy  Savings Certificates are created in accordance with the requirements of the ESS.</td>
                    </tr>
                    <tr>
                      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/dot.jpg" width="4" height="4" alt=""/></td>
                      <td style="font-size:12px; color:#000000;">The information I have provided is complete and accurate and I am aware that there are penalties for providing false and misleading  information in this form.</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

            <tr>
              <td>&nbsp;</td>
            </tr>

            <tr>
              <td style="font-size: 12px; color: #000000"><strong>Note:</strong></td>
            </tr>

            <tr>
              <td style="font-size:12px; color:#000000;">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly  providing false or misleading information to the Scheme Administrator.</td>
            </tr>

            <tr>
              <td height="50"></td>
            </tr>

            <tr>
              <td style="font-size: 12px; color: #000000"><strong>Signed by or on behalf of the Original Energy Saver</strong></td>
            </tr>

            <tr>
              <td height="15"></td>
            </tr>

            <tr>
              <td valign="top">
                <table align="left" width="400" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td valign="top">
                        <table width="370" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td style="font-size:12px; color:#6a6a6a; height:25px">Signature</td>
                            </tr>
                            <tr>
                              <td valign="top">
                                <div class="signature-pad">
                                  <div class="signature-pad--body">
                                    <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_6" data-id="6" class="sign_create">
                                    </div>
                                    <input required="" id="get_nomination_form_details_energy_saver_details_signature" name="get_nomination_form_details[energy_saver_details][signature]" type="hidden" />
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Name</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[energy_saver_details][name2]"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Date</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[energy_saver_details][date]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Position</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[energy_saver_details][position]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td style="font-size:12px; color:#6a6a6a; height:25px">Company</td>
                          </tr>
                          <tr>
                            <td><input type="text" name="get_nomination_form_details[energy_saver_details][company]" value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:37px; padding-left:5px; padding-right:5px"/></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr> 
            <tr>
              <td height="90"></td>
            </tr>

          </tbody>
        </table></td>
      </tr>
      <tr>
        <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/bottom.jpg" width="910" height="102" alt=""/></td>
      </tr>
    </tbody>
  </table>
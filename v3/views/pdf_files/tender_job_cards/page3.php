<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Job Card</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            
            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=checkbox]:checked:before {
                color: black !important;
            }
        
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        
        </style>
    </head>
<body style="padding:0; margin:0">
<table class="hidden" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td style="position:relative;top:-1"><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td height="5">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top" height="1000">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>Site Details</strong>
                    </td>
                </tr>
                <tr>
                    <td height="5">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="400" valign="top">
                                    <table width="390" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="60" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Access:</td>
                                            <td width="180" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:15px; padding:5px">
                                                <input value="<?= isset($job_card_details->roof_access) ? $job_card_details->roof_access : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" /> Meters
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is forklift available on Site?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if(isset($job_card_details->is_forklift)){ ?>
                                                    <input type="radio" <?php if($job_card_details->is_forklift == 'yes') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" <?php if($job_card_details->is_forklift == 'no') { echo 'checked=""'; }?>/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php }else{ ?>
                                                    <input type="radio"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio"/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Notes</td>
                                        </tr>
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                <textarea style="padding:5px; width:100%; height: 100px;border:solid 1px #ccc;outline:none; box-sizing:border-box;"><?= isset($job_card_details->access_note)  ? $job_card_details->access_note : ''; ?></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" height="290">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Roof Access Photo 1</th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Roof Access Photo 2</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 250px; padding:5px 0;">
                                    <?php if (isset($job_card_details->roof_access_one) && $job_card_details->roof_access_one != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->roof_access_one; ?>" width="391" height="250" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="250" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 250px; padding:5px 0;">
                                    <?php if (isset($job_card_details->roof_access_two) && $job_card_details->roof_access_two != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->roof_access_two; ?>" width="391" height="250" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="250" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Roof Condition Photo 1</th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Roof Condition Photo 2</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 250px; padding:5px 0;">
                                    <?php if (isset($job_card_details->roof_condition_one) && $job_card_details->roof_condition_one != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->roof_condition_one; ?>" width="391" height="250" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="250" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 250px; padding:5px 0;">
                                    <?php if (isset($job_card_details->roof_condition_two) && $job_card_details->roof_condition_two != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->roof_condition_two; ?>" width="391" height="250" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="250" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="5">&nbsp;</td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td valign="bottom" style="position:relative;bottom: -5px;"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
</body>
</html>
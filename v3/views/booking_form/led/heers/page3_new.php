
<table class="pdf_page_1" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01-wide.jpg'; ?>" alt="" width="100%" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><a href="javascript:void(0);" id="add_more_led_products_row_btn" class="btn btn-kuga mb-2 pull-right"><i class="fa fa-plus"></i> Add More</a></td>
    </tr>
    
    <tr>
        <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="booking_items">
                <tr>
                    <th colspan="4" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">&nbsp;</th>
                    <th colspan="3" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Existing Lighting (Baseline)</th>
                    <th colspan="6" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Upgraded Lighting (Baseline)</th>
                </tr>
                <tr>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Area Name</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Celling Height</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Activity</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Customer Type</th>
                    
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Removed Lamp Type</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Nominal Lamp Power</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Qty</th>
                    
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Product Type</th>
                    <th style="width:150px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Product model</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Sensor</th>
                    
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Qty</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;padding: 5px;">Unit Cost exc GST</th>
                    <th style="width:60px; background:#000; border:1px solid #e8e8e8e8e8e8; color:#fff; text-align:center;padding: 5px;">Total Cost exc GST</th>
                </tr>
                <tbody id="booking_items_body">
                    
                </tbody>
            </table>
        </td>
   </tr>
   
    <tr>
        <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                <tr>
                    <td height="40" colspan="4" style="padding:5px;">
                        <table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="55%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Further Products quoted can be found on a separate sheet </td>
                                <td width="20%"><input id="" name="booking_form[is_more_products]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                <td width="25%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Sub Total exc GST:</td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="product_total_excGst" name="product[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="0" type="number" />
                    </td>
                </tr>
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Access Equipment</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Detailed Description of Access Equipment</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Quantity</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php for ($i = 0; $i < 2; $i++) {
                    $required = ($i == 0) ? 'required=""' : '';
                ?>
                    <tr>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <select class="ae_name ae_name_<?php echo $i; ?>" name="ae[ae_name][]"  style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <option value="Scissor Lift">Scissor Lift</option>
                                <option value="Site Inspection Required">Site Inspection Required</option>
                                <option value="Boom Lift">Boom Lift</option>
                                <option value="Other">Other</option>
                            </select>
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <select class="ae_description ae_description_<?php echo $i; ?>" name="ae[ae_description][]"  style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <?php
                                $ae = unserialize(BF_AE);
                                if (!empty($ae)) {
                                    foreach ($ae as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_qty ae_qty_<?php echo $i; ?>" name="ae[ae_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_cost_excGST ae_cost_excGST_<?php echo $i; ?>" name="ae[ae_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="0" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_total_cost_excGst ae_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="0" type="number" <?php echo $required; ?> />
                        </td>
                    </tr>   
                <?php } ?>
                <tr>
                    <td height="40" colspan="4" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Sub Total exc GST:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="ae_total_excGst" name="ae[total_excGst]"  style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
        <td valign="top">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_excGst" name="booking_form[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td width="560" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_Gst" name="booking_form[total_Gst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td width="560" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom-wide.jpg'; ?>" width="100%" height="89" /></td>
    </tr>
</table>

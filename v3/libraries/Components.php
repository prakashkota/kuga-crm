<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Components {

    /**
     * Constructor
     */
    public function __construct() {
        // get main CI object
        $this->lt = & get_instance();
    }

    function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    function array2csv(array &$array) {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    function uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    function get_region_by_postcode($postCode) {
        $postCodeWithRegion = [
            '2' => [
                ['low' => 1000, 'high' => 1000],
                ['low' => 2000, 'high' => 2599],
                ['low' => 2619, 'high' => 2899],
                ['low' => 2921, 'high' => 2999],
            ],
            '1' => [
                ['low' => 200, 'high' => 299],
                ['low' => 2600, 'high' => 2618],
                ['low' => 2900, 'high' => 2920]
            ],
            '7' => [
                ['low' => 3000, 'high' => 3999],
                ['low' => 8000, 'high' => 8999]
            ],
            '4' => [
                ['low' => 4000, 'high' => 4999],
                ['low' => 9000, 'high' => 9999],
                ['low' => 2900, 'high' => 2920]
            ],
            '5' => [
                ['low' => 5000, 'high' => 5799],
                ['low' => 5800, 'high' => 5999]
            ],
            '8' => [
                ['low' => 6000, 'high' => 6797],
                ['low' => 6800, 'high' => 6999]
            ],
            '6' => [
                ['low' => 7000, 'high' => 7799],
                ['low' => 7800, 'high' => 7999]
            ],
            '3' => [
                ['low' => 800, 'high' => 899],
                ['low' => 900, 'high' => 999]
            ]
        ];
        $regionFound = NULL;
        foreach ($postCodeWithRegion as $region => $ranges) {
            foreach ($ranges as $row) {
                if ($postCode >= $row['low'] && $postCode <= $row['high']) {
                    $regionFound = (int) $region;
                    break;
                }
            }
            if ($regionFound) {
                break;
            }
        }
        return $regionFound;
    }

    function get_gcal_color_id($lead_stage) {
        $colors = array(
            '0' => 8, //Grey
            '1' => 10, //Green
            '2' => 5, //Amber 
            '3' => 3, //Pink
            '4' => 11, //Red
            '5' => 9  //Blue
        );
        return ($lead_stage > 1 && $lead_stage <= 6) ? $colors[($lead_stage - 1)] :$colors[0] ;
    }
    
    public function is_team_leader(){
        $this->lt->load->library("Aauth");
        $user_groups = $this->lt->aauth->get_user_groups();
        if(empty($user_groups)){
           return FALSE;
        }
        $team_types = TEAM_LEADER_TYPES;
        $is_team_leader = FALSE;
        $user_group = $user_groups[0]->id;
        //If a team leader 
        if(array_key_exists($user_group, $team_types)){
           $is_team_leader = TRUE;
        }
        return $is_team_leader;
    }
    
    public function is_sales_team_leader(){
        $this->lt->load->library("Aauth");
        $user_groups = $this->lt->aauth->get_user_groups();
        if(empty($user_groups)){
           return FALSE;
        }
        $team_types = array(6);
        $is_team_leader = FALSE;
        $user_group = $user_groups[0]->id;
        //If a team leader 
        if(in_array($user_group, $team_types)){
           $is_team_leader = TRUE;
        }
        return $is_team_leader;
    }
    
    public function is_sales_rep(){
        $this->lt->load->library("Aauth");
        $user_groups = $this->lt->aauth->get_user_groups();
        if(empty($user_groups)){
           return FALSE;
        }
        $user_group = $user_groups[0]->id;
        $team_rep_types = TEAM_REP_TYPES;
        $is_team_rep = FALSE;
        //If a team rep
        if(array_key_exists($user_group, $team_rep_types)){
           $is_team_rep = TRUE;
        }
        return $is_team_rep;
    }
    
    public function is_lead_allocation_team_leader(){
        $this->lt->load->library("Aauth");
        $user_groups = $this->lt->aauth->get_user_groups();
        if(empty($user_groups)){
           return FALSE;
        }
        $user_group = $user_groups[0]->id;
        $team_types = array(4);
        $is_team_leader = FALSE;
        
        //If a team leader 
        if(in_array($user_group, $team_types)){
           $is_team_leader = TRUE;
        }
        return $is_team_leader;
    }
    
    public function is_lead_allocation_rep(){
        $this->lt->load->library("Aauth");
        $user_groups = $this->lt->aauth->get_user_groups();
        if(empty($user_groups)){
           return FALSE;
        }
        $user_group = $user_groups[0]->id;
        $team_rep_types = array(5);
        $is_team_rep = FALSE;
        //If a team rep
        if(in_array($user_group, $team_rep_types)){
           $is_team_rep = TRUE;
        }
        return $is_team_rep;
    }
    
    
    public function escape_data($data = []){
        $sanitized_data = [];
        foreach ($data as $key => $value) {
            $value = mysqli_real_escape_string($this->lt->db->conn_id,$value);
            $value = trim($value);
            $sanitized_data[$key] = $value;
        }
        return $sanitized_data;
    }
    
    public function days_in_month($month, $year){
        // calculate number of days in a month
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }
    
    // $i should be "" or "i" for case insensitive
    // if $word is "W" then word search instead of string in string search.
    // Put quotes around true and false above to return them as strings instead of as bools/ints.
    public function find_string($needle,$haystack,$i,$word = ""){   
        if (strtoupper($word)=="W"){   
            if (preg_match("/\b{$needle}\b/{$i}", $haystack)) 
            {
                return true;
            }
        }else{
            if(preg_match("/{$needle}/{$i}", $haystack)) 
            {
                return true;
            }
        }
        return false;
    }
    
    public function rotate_image_based_on_exif_oreintation($file){
        try{
            if (is_file($file)) {
                $info = @exif_read_data($file);
                if (isset($info['Orientation']) && $info['Orientation'] != 1) {
                    switch ($info['Orientation']) {
                        case 3:
                        $deg = 180;
                        break;
                        case 6:
                        $deg = 270;
                        break;
                        case 8:
                        $deg = 90;
                        break;
                    }
                    if ($deg) {
                        // If png
                        $filen = explode('.',$file);
                        $ext = end($filen);
                        if ($ext == "png") {
                            $img_new = imagecreatefrompng($file);
                            $img_new = imagerotate($img_new, $deg, 0);
                            // Save rotated image
                            imagepng($img_new,$file);
                        }else {
                            $img_new = imagecreatefromjpeg($file);
                            $img_new = imagerotate($img_new, $deg, 0);
                            // Save rotated image
                            imagejpeg($img_new,$file,80);
                        }
                    }
                }           
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    
    public function LaunchBackgroundProcess($command){
        if(PHP_OS=='WINNT' || PHP_OS=='WIN32' || PHP_OS=='Windows'){
            $command = 'start "" '. $command;
        } else {
            $command = $command .' /dev/null &';
        }
        $handle = popen($command, 'r');
        if($handle!==false){
            pclose($handle);
            return true;
        } else {
            return false;
        }
    }
    

}

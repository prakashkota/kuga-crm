<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Lead Controller
 * @version 1.0
 */
class Lead extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Notify");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Product_Model", "product");
        $this->load->model("Proposal_Model", "proposal");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(3) != 'solar_quote_pdf_download') {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            } else {
                redirect('admin/login');
            }
        }
    }

    public function manage()
    {
        //$this->load->library("Email_Manager", 'email_manager');
        //$this->email_manager->send_test_mail();
        $data = array();
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $data['is_sales_rep'] = $data['is_lead_allocation_sales_rep'] = FALSE;

        if ($this->aauth->is_member('Admin')) {
            $users = $this->user->get_users(FALSE, FALSE, FALSE);
            $view_folder = 'sales_rep';
        } else if ($this->components->is_team_leader()) {
            if ($this->components->is_lead_allocation_team_leader()) {
                $la_team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $la_sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $lead_allocation_users = array_merge($la_team_leader, $la_sales_rep);
                $data['lead_allocation_users'] = $lead_allocation_users;
                $team_leader = $this->user->get_users(6, FALSE, FALSE);
                $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                $users = array_merge($team_leader, $sales_rep);
                $data['is_lead_allocation_sales_rep'] = TRUE;
                $view_folder = 'lead_allocation';
            } else {
                $team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $users = array_merge($team_leader, $sales_rep);
                $view_folder = 'sales_rep';
            }
            $data['all_reports_view_permission'] = 1;
        } else if ($this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep()) {
                $la_team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $la_sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $lead_allocation_users = array_merge($la_team_leader, $la_sales_rep);
                $data['lead_allocation_users'] = $lead_allocation_users;
                $team_leader = $this->user->get_users(6, FALSE, FALSE);
                $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                $users = array_merge($team_leader, $sales_rep);
                $data['is_lead_allocation_sales_rep'] = TRUE;
                $view_folder = 'lead_allocation';
            } else {
                //For Sarvanis Case we need to show list of all users
                if ($user_id == 29) {
                    $team_leader = $this->user->get_users(6, FALSE, FALSE);
                    $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                    $users = array_merge($team_leader, $sales_rep);
                    $view_folder = 'sales_rep';
                } else {
                    //$user_id = $this->aauth->get_user_id();
                    //$users = $this->user->get_users(FALSE, $user_id, FALSE);
                    $team_leader = $this->user->get_users(6, FALSE, FALSE);
                    $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                    $users = array_merge($team_leader, $sales_rep);
                    $data['is_sales_rep'] = TRUE;
                    $view_folder = 'sales_rep';
                }
            }
        }
        $data['users'] = $users;

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['lead_stages'] = $this->common->fetch_orderBy('tbl_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $view = $this->input->get('view');
        $view = (isset($view) && $view != '') ? $view : 'list';
        if ($view == 'map') {
            $data['all_users'] = $users = $this->user->get_users(FALSE, FALSE, FALSE, 'WHERE user.id!=1');
            //Get Last History Data
            $search_log_data = $this->common->fetch_orderBy('tbl_lead_map_search_logs', '*', array('user_id' => $user_id), 'id', 'desc', 1);
            $data['search_log_data'] = array();
            if (!empty($search_log_data[0]['data'])) {
                $data['search_log_data'] = json_decode($search_log_data[0]['data'], true);
            }
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) {
                $data['lead_allocation_users'] = $this->user->get_users(5, FALSE, FALSE);
            }
            $data['logged_in_user_data'] = $this->common->fetch_row('tbl_user_locations', '*', array('user_id' => $user_id));
            $data['activity_modal_title'] = 'Add/Schedule Activity';
            $this->load->view('activity/activity_modal', $data);
        }
        $this->load->view('lead/' . $view_folder . '/manage_' . $view);
    }

    public function add($uuid = FALSE)
    {
        $data = array();
        //Check if Logged in user is Admin , Team Leader or Team Leader Rep
        $user_group = $this->aauth->get_user_groups()[0]->id;
        $user_id = $this->aauth->get_user_id();
        $data['user_id'] = $user_id = $this->aauth->get_user()->id;
        $data['is_sales_rep'] = $data['is_lead_allocation_sales_rep'] = FALSE;

        $la_team_leader = $this->user->get_users(4, FALSE, FALSE);
        $la_sales_rep = $this->user->get_sub_users(5, FALSE, FALSE);
        $lead_allocation_users = array_merge($la_team_leader, $la_sales_rep);
        $data['lead_allocation_users'] = $lead_allocation_users;
        $team_leader = $this->user->get_users(6, FALSE, FALSE);
        $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
        $admin = $this->user->get_users(FALSE, 1, FALSE);
        $users = array_merge($admin, $team_leader, $sales_rep);
        $data['users'] = $users;

        if (isset($user_group) && $this->components->is_team_leader()) {
            if ($this->components->is_lead_allocation_team_leader()) {
                $data['is_lead_allocation_sales_rep'] = TRUE;
            }
        } else if (isset($user_group) && $this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep()) {
                $data['is_lead_allocation_sales_rep'] = TRUE;
            } else {
                $data['is_sales_rep'] = TRUE;
            }
        }


        if ($uuid == FALSE) {
            $deal_ref = $this->input->get('deal_ref');
            if (isset($deal_ref) && $deal_ref != '') {
                $lead_data = $this->lead->get_leads($deal_ref);
                if (empty($lead_data)) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, no deal found with reference id.</div>');
                    redirect(site_url('admin/lead/add'));
                }
                //Due to Some ParseJSON issue we need to unset note column
                unset($lead_data['note']);
                $data['lead_id'] = $lead_id = $lead_data['id'];
                $data['lead_data'] = $lead_data;
                redirect(site_url('admin/lead/edit/' . $lead_data['uuid']));
            }
            $states = $this->common->fetch_where('tbl_state', '*', NULL);
            $data['states'] = $states;
            $data['user_group'] = $user_group;
            //According to new flow will show only add deal foem with address
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            if (!$this->aauth->is_member('Lead Creator Only')) {
                $this->load->view('lead/partials/business_create_form');
            } else {
                $this->load->view('lead/partials/lead_creator_business_create_form');
            }
            $this->load->view('partials/footer');
        } else {
            $lead_data = $this->lead->get_leads($uuid, FALSE);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> No lead found with referenced id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $data['id'] = $uuid;
            $data['lead_id'] = $lead_data['id'];
            unset($lead_data['note']);

            foreach ($lead_data as $key => $value) {
                $lead_data[$key] =  str_replace("\\", "", $value);
            }
            $data['lead_data'] = $lead_data;

            $data['user_id'] = $user_id;
            $data['user_group'] = $user_group;
            $data['product_types'] = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('parent_id =' => LED));
            $data['lead_stages'] = $this->common->fetch_orderBy('tbl_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
            $data['service_category'] = $this->common->fetch_where('tbl_service_category', '*', array('parent_id' => 0));

            $states = $this->common->fetch_where('tbl_state', '*', NULL);
            $data['states'] = $states;
            $data['title'] = 'Add Deal';
            $data['activity_modal_title'] = 'Add/Schedule Activity';

            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('customer/customer_modal_partial');
            $this->load->view('customer/customer_contact_modal_partial');
            $this->load->view('customer/customer_site_modal_partial');
            $this->load->view('customer/booking_form_customer_site_modal_partial');
            $this->load->view('activity/activity_modal');
            $this->load->view('lead/partials/lead_allocation_appointment_modal');
            $this->load->view('lead/form');
        }
    }

    public function fetch_lead_data()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_uuid = $this->input->get('uuid');
            $lead_uuid = (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : FALSE;
            $site_id = $this->input->get('site_id');
            $site_id = (isset($site_id) && $site_id != '') ? $site_id : FALSE;
            $lead_data = $this->lead->get_leads($lead_uuid, FALSE, $site_id);
            $data['lead_data'] = $lead_data;
            $data['calculator_items'] = json_decode(file_get_contents(APPPATH . '../assets/calculator/nsw_commercial.json'));
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function add_quick_quote($uuid = FALSE)
    {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        if (isset($deal_ref) && $deal_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Quick quote creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $quote_data = $this->common->fetch_orderBy('tbl_lead_quotes', 'quote_no', NULL, 'id', 'DESC', 1);
            $data['quote_no'] = (!empty($quote_data)) ? ($quote_data[0]['quote_no'] + 1) : 1000;
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('proposal/form_led_quick_quote');
        } else if ($uuid) {
            $quote_data = $this->common->fetch_row('tbl_lead_quotes', '*', array('uuid' => $uuid));
            if (empty($quote_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Quick quote creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $lead_data = $this->lead->get_leads(FALSE, $quote_data['lead_id']);
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $quote_data['quote_expires_at'] = date('d/m/Y', strtotime(str_replace('-', '/', $quote_data['quote_expires_at'])));
            $data['quote_data'] = $quote_data;
            $data['quote_no'] = (!empty($quote_data)) ? ($quote_data['quote_no'] + 1) : 1000;
            $data['quote_items'] = $this->common->fetch_where('tbl_lead_quote_items', '*', array('quote_id' => $quote_data['id']));
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('proposal/form_led_quick_quote');
        } else {
            redirect('admin/dashboard');
        }
    }

    public function save_quick_quote()
    {
        $data = array();
        $quote_data = $this->input->post('quote');
        $quote_product_data = $this->input->post('quote_product');
        if (!empty($this->input->post())) {
            $quote_data['quote_expires_at'] = date('Y-m-d', strtotime(str_replace('/', '-', $quote_data['quote_expires_at'])));
            $is_quote_exists = $this->common->fetch_row('tbl_lead_quotes', '*', array('uuid' => $quote_data['uuid']));
            if (empty($is_quote_exists)) {
                $insert_data = array();
                $insert_data = $quote_data;
                $insert_data['user_id'] = $this->aauth->get_user_id();
                $stat = $this->common->insert_data('tbl_lead_quotes', $insert_data);
                $quote_id = $this->common->insert_id();
            } else {
                $quote_id = $is_quote_exists['id'];
                $stat = $this->common->update_data('tbl_lead_quotes', array('id' => $quote_id), $quote_data);
            }
            if (!empty($quote_product_data)) {
                $stat1 = $this->handle_quick_quote_products($quote_id);
            }
            if ($stat || $stat1) {
                $data['status'] = 'Quote Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something went wrong or Nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    private function handle_quick_quote_products($quote_id)
    {
        $quote_product_data = array();
        $quote_product_insert_data = array();
        $quote_item_data = $this->input->post('quote_product');

        //Bulk Update Data
        $quote_item_existing_data = $this->common->fetch_where('tbl_lead_quote_items', '*', ['quote_id' => $quote_id]);
        $count = count($quote_item_existing_data);

        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                if (isset($quote_item_data['item_qty'][$i]) && $quote_item_data['item_qty'][$i] != '') {
                    $quote_product_data[$i]['item_name'] = $quote_item_data['item_name'][$i];
                    $quote_product_data[$i]['item_qty'] = $quote_item_data['item_qty'][$i];
                    $quote_product_data[$i]['item_cost_excGST'] = $quote_item_data['item_cost_excGST'][$i];
                    $quote_product_data_cond[$i]['item_id'] = $quote_item_existing_data[$i]['item_id'];
                    $stat = $this->common->update_data('tbl_lead_quote_items', $quote_product_data_cond[$i], $quote_product_data[$i]);
                }
            }
        }

        //Bulk Insert Data
        for ($j = $count, $k = 0; $j < count($quote_item_data['item_qty']); $j++, $k++) {
            if (isset($quote_item_data['item_qty'][$j]) && $quote_item_data['item_qty'][$j] != '') {
                $quote_product_insert_data[$k]['item_name'] = $quote_item_data['item_name'][$j];
                $quote_product_insert_data[$k]['item_qty'] = $quote_item_data['item_qty'][$j];
                $quote_product_insert_data[$k]['item_cost_excGST'] = $quote_item_data['item_cost_excGST'][$j];
                $quote_product_insert_data[$k]['quote_id'] = $quote_id;
                $stat = $this->common->insert_data('tbl_lead_quote_items', $quote_product_insert_data[$k]);
            }
        }

        //Check if delete Required
        if ($count > count($quote_item_data['item_qty'])) {
            for ($l = count($quote_item_data['item_qty']), $m = 0; $l < $count; $l++, $m++) {
                $quote_product_cond[$m]['item_id'] = $quote_item_existing_data[$l]['item_id'];
                $stat = $this->common->delete_data('tbl_lead_quote_items', $quote_product_cond[$m]);
            }
        }

        return $stat;
    }

    public function fetch_lead_quotes()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $quote_data = $this->common->fetch_where('tbl_lead_quotes', '*', array('lead_id' => $lead_id));
            $data['quote_data'] = $quote_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_lead_booking_forms_data()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $booking_form_data = $this->lead->get_lead_booking_forms_data($lead_id);
            $data['booking_form_data'] = $booking_form_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_lead_form_data()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $booking_form_data = $this->lead->get_lead_booking_forms_data($lead_id);
            $quote_data = $this->common->fetch_where('tbl_lead_quotes', '*', array('lead_id' => $lead_id));
            $meter_data = $this->common->fetch_where('tbl_lead_meter_data', '*', array('lead_id' => $lead_id));
            $solar_quote_data = $this->common->fetch_where('tbl_lead_solar_quotes', '*', array('lead_id' => $lead_id));

            $job_card_data = $this->common->fetch_where('tbl_job_card_form', '*', array('lead_id' => $lead_id));
            $proposal_data = $this->lead->get_lead_proposal_data($lead_id);
            $data['booking_form_data'] = $booking_form_data;
            $data['quote_data'] = $quote_data;
            $data['meter_data'] = $meter_data;
            $data['solar_quote_data'] = $solar_quote_data;
            $data['job_card_data'] = $job_card_data;
            $data['proposal_data'] = $proposal_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function quote_pdf_download($uuid)
    {
        $quote_data = $this->common->fetch_row('tbl_lead_quotes', '*', array('uuid' => $uuid));
        if (empty($quote_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Quick quote pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $quote_data['quote_expires_at'] = date('d/m/Y', strtotime(str_replace('-', '/', $quote_data['quote_expires_at'])));
        $data['quote_data'] = $quote_data;
        $data['quote_items'] = $this->common->fetch_where('tbl_lead_quote_items', '*', array('quote_id' => $quote_data['id']));
        $html = $this->load->view('pdf_files/additional/quick_quote', $data, TRUE);
        //echo $html;die;
        $filename = md5(time());
        $h = '';
        $f = '';
        if ($quote_data['file'] == '' || $quote_data['file'] == NULL) {
            $this->common->update_data('tbl_lead_quotes', array('uuid' => $uuid), array('file' => $filename . '.pdf'));
            $pdf_options = array(
                "source_type" => 'html',
                "source" => $html,
                "action" => 'save',
                "save_directory" => __DIR__ . '/../../assets/uploads/quote_files',
                "file_name" => $filename . '.pdf',
                "page_size" => 'A4',
                "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                "header" => $h,
                "footer" => $f
            );
            $this->pdf->phptopdf($pdf_options);

            //Send Mail
            $communication_data = array();
            $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/quote_files/' . $filename . '.pdf';
            $user_id = $this->aauth->get_user_id();
            $sales_rep_email = $this->common->fetch_cell('aauth_users', 'email', array('id' => $user_id));
            $this->email_manager->send_quick_quote_form_success_email($communication_data, $sales_rep_email);

            redirect(site_url('assets/uploads/quote_files/' . $filename . '.pdf'));
            exit();
        } else {
            redirect(site_url('assets/uploads/quote_files/' . $quote_data['file']));
            exit();
        }
    }


    public function add_solar_quick_quote($uuid = FALSE)
    {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        if (isset($deal_ref) && $deal_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Solar Quote creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $quote_data = $this->common->fetch_orderBy('tbl_lead_solar_quotes', 'quote_no', NULL, 'id', 'DESC', 1);
            $data['quote_no'] = (!empty($quote_data)) ? ($quote_data[0]['quote_no'] + 1) : 1000;
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('proposal/form_solar_quick_quote');
        } else if ($uuid) {
            $quote_data = $this->common->fetch_row('tbl_lead_solar_quotes', '*', array('uuid' => $uuid));
            if (empty($quote_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Solar Quote creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $lead_data = $this->lead->get_leads(FALSE, $quote_data['lead_id']);
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            unset($quote_data['description']);
            $data['quote_data'] = $quote_data;
            $data['quote_no'] = (!empty($quote_data)) ? ($quote_data['quote_no'] + 1) : 1000;
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('proposal/form_solar_quick_quote');
        } else {
            redirect('admin/dashboard');
        }
    }

    public function fetch_form_data()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $uuid = $this->input->get('uuid');
            $form_type = $this->input->get('form_type');
            if (isset($form_type) && $form_type == 'solar_quote') {
                $solar_quote_data = $this->common->fetch_row('tbl_lead_solar_quotes', '*', array('uuid' => $uuid));
                $data['solar_quote_data'] = $solar_quote_data;
            }
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function save_solar_quick_quote()
    {
        $data = array();
        $quote_data = $this->input->post('quote');
        if (!empty($this->input->post())) {
            //$quote_data['quote_expires_at'] = date('Y-m-d', strtotime(str_replace('/', '-', $quote_data['quote_expires_at'])));
            $is_quote_exists = $this->common->fetch_row('tbl_lead_solar_quotes', '*', array('uuid' => $quote_data['uuid']));
            if (empty($is_quote_exists)) {
                $insert_data = array();
                $insert_data = $quote_data;
                $insert_data['user_id'] = $this->aauth->get_user_id();
                $stat = $this->common->insert_data('tbl_lead_solar_quotes', $insert_data);
                $quote_id = $this->common->insert_id();
            } else {
                $quote_id = $is_quote_exists['id'];
                $stat = $this->common->update_data('tbl_lead_solar_quotes', array('id' => $quote_id), $quote_data);
            }

            if ($stat) {
                $data['status'] = 'Solar Quote Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }


    public function solar_quote_pdf_download($uuid)
    {
        $quote_data = $this->common->fetch_row('tbl_lead_solar_quotes', '*', array('uuid' => $uuid));
        if (empty($quote_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Solar Quick quote pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $data['quote_data'] = $quote_data;

        //echo $html;die;
        $view = $this->input->get('view');
        if (isset($view) && $view == 'download') {

            $upload_path = FCPATH . '/assets/uploads/quote_files/solar_quotes/' . $quote_data['id'];
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }

            $upload_path_img = $upload_path . '/page_solar_quick_quote1_' . $quote_data['id'] . '.jpg';
            $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 https://kugacrm.com.au/admin/lead/solar_quote_pdf_download/' . $uuid . '?view=html_to_image ' . $upload_path_img;
            exec($cmd);

            $desc = $quote_data['description'];
            $desc = str_replace('</p>', '', $desc);
            $desc_array = explode('<p>', $desc);
            if (count($desc_array) > 22) {
                $upload_path_img = $upload_path . '/page_solar_quick_quote2_' . $quote_data['id'] . '.jpg';
                $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 https://kugacrm.com.au/admin/lead/solar_quote_pdf_download/' . $uuid . '?view=html_to_image1 ' . $upload_path_img;
                exec($cmd);
                $data['page2_file'] = site_url('/assets/uploads/quote_files/solar_quotes/' . $quote_data['id'] . '/' . 'page_solar_quick_quote2_' . $quote_data['id'] . '.jpg');
            }

            $data['page1_file'] = site_url('/assets/uploads/quote_files/solar_quotes/' . $quote_data['id'] . '/' . 'page_solar_quick_quote1_' . $quote_data['id'] . '.jpg');
            $html = $this->load->view('pdf_files/additional/solar_quick_quote', $data, TRUE);
            //echo $html;die;

            $upload_path_img = $upload_path . '/page_solar_quick_quote_' . $quote_data['id'] . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/lead/solar_quote_pdf_download/' . $uuid . '?view=final_html ' . $upload_path_img;
            exec($cmd);

            $pdf_url = site_url('assets/uploads/quote_files/solar_quotes/' . $quote_data['id'] . '/page_solar_quick_quote_' . $quote_data['id'] . '.pdf');
            $this->common->update_data('tbl_lead_solar_quotes', array('uuid' => $uuid), array('file' => $pdf_url));

            redirect($pdf_url);
            exit();
        } else if (isset($view) && $view == 'final_html') {
            $data['page1_file'] = site_url('/assets/uploads/quote_files/solar_quotes/' . $quote_data['id'] . '/' . 'page_solar_quick_quote1_' . $quote_data['id'] . '.jpg');
            $desc = $quote_data['description'];
            $desc = str_replace('</p>', '', $desc);
            $desc_array = explode('<p>', $desc);
            if (count($desc_array) > 22) {
                $data['page2_file'] = site_url('/assets/uploads/quote_files/solar_quotes/' . $quote_data['id'] . '/' . 'page_solar_quick_quote2_' . $quote_data['id'] . '.jpg');
            }
            echo $this->load->view('pdf_files/additional/solar_quick_quote', $data, TRUE);
            die;
        } else if (isset($view) && $view == 'html_to_image') {
            echo $this->load->view('pdf_files/additional/solar_quick_quote_page', $data, TRUE);
            die;
        } else if (isset($view) && $view == 'html_to_image1') {
            echo $this->load->view('pdf_files/additional/solar_quick_quote_page_1', $data, TRUE);
            die;
        }
    }

    private function convert_image_to_pdf_dimension($file, $filename, $upload_path)
    {
        $data['page4_file'] = $file;
        $dst_x = 0;   // X-coordinate of destination point
        $dst_y = 0;   // Y-coordinate of destination point
        $src_x = 185; // Crop Start X position in original image
        $src_y = 0; // Crop Srart Y position in original image
        $dst_w = 910; // Thumb width
        $dst_h = 1287; // Thumb height
        $src_w = 910; // Crop end X position in original image
        $src_h = 1287; // Crop end Y position in original image

        // Creating an image with true colors having thumb dimensions (to merge with the original image)
        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
        // Get original image
        $img = file_get_contents($file);
        $src_image = imagecreatefromstring($img);
        // Cropping
        imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
        // Saving
        imagejpeg($dst_image, $upload_path . '/' . $filename . '.jpg', 99);
    }

    /** Request Meter Data */
    public function add_meter_data($uuid = FALSE)
    {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');

        if (isset($deal_ref) && $deal_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Request Meter form cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $meter_data = $this->common->fetch_row('tbl_lead_meter_data', '*', array('lead_id' => $lead_data['id']));
            if (!empty($meter_data)) {
                redirect(site_url('admin/lead/edit_meter_data/' . $meter_data['uuid']));
            }
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $user_id = $this->aauth->get_user_id();
            $user = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['user'] = (!empty($user)) ? $user[0] : [];

            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('lead/form_meter_data');
        } else if ($uuid) {
            $meter_data = $this->common->fetch_row('tbl_lead_meter_data', '*', array('uuid' => $uuid));
            if (empty($meter_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Request Meter form cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $lead_data = $this->lead->get_leads(FALSE, $meter_data['lead_id']);
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id'] = $lead_id = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $meter_data['from_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['from_date'])));
            $meter_data['to_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['to_date'])));
            $meter_data['request_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['request_date'])));
            $data['meter_data'] = $meter_data;
            $user_id = $this->aauth->get_user_id();
            $user = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['user'] = (!empty($user)) ? $user[0] : [];

            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('lead/form_meter_data');
        } else {
            redirect('admin/dashboard');
        }
    }

    /** Save Meter Data */
    public function save_meter_data()
    {
        $data = array();
        $meter_data = $this->input->post('meter_data');
        if (!empty($this->input->post())) {
            //Convert Base64 signatures to png
            if (isset($meter_data['signature'])) {
                $img = explode(';', $meter_data['signature']);
                if ($meter_data['signature'] != '' && count($img) > 1) {
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $meter_data['signature']));
                    $meter_data['signature'] = $newfilename = $this->components->uuid() . '.png';
                    file_put_contents('./assets/uploads/meter_data_form_files/' . $newfilename, $image);
                }
            }
            $meter_data['from_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $meter_data['from_date'])));
            $meter_data['to_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $meter_data['to_date'])));
            $meter_data['request_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $meter_data['request_date'])));
            $is_meter_data_exists = $this->common->fetch_row('tbl_lead_meter_data', '*', array('uuid' => $meter_data['uuid']));
            if (empty($is_meter_data_exists)) {
                $insert_data = array();
                $insert_data = $meter_data;
                $insert_data['user_id'] = $this->aauth->get_user_id();
                $stat = $this->common->insert_data('tbl_lead_meter_data', $insert_data);
                $id = $this->common->insert_id();
            } else {
                $id = $is_meter_data_exists['id'];
                $stat = $this->common->update_data('tbl_lead_meter_data', array('id' => $id), $meter_data);
            }

            if ($stat) {
                $data['status'] = 'Meter Data Form Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something went wrong or nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    /** Download meter Data pdf */
    public function meter_data_pdf_download($uuid)
    {

        $this->load->library("Email_Manager");

        try {
            $meter_data = $this->common->fetch_row('tbl_lead_meter_data', '*', array('uuid' => $uuid));
            if (empty($meter_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Pdf cant be created, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            $meter_data['from_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['from_date'])));
            $meter_data['to_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['to_date'])));
            $meter_data['request_date'] = date('d/m/Y', strtotime(str_replace('-', '/', $meter_data['request_date'])));
            $data['meter_data'] = $meter_data;
            $user_id = $meter_data['user_id'];
            $user = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['user'] = (!empty($user)) ? $user[0] : [];
            $html = $this->load->view('pdf_files/additional/meter_data', $data, TRUE);
            //echo $html;die;

            if (isset($meter_data['account_name']) && $meter_data['account_name'] != '') {
                $filename = 'KUGA ELECTRICAL-Meter Data Request Form_' . $meter_data['account_name'] . '-' . $meter_data['rep_name'] . '-' . date('m-d-Y_his') . '.pdf';
            } else {
                $filename = 'KUGA ELECTRICAL-Meter Data Request Form_' . $meter_data['rep_name'] . '-' . date('m-d-Y_his') . '.pdf';
            }
            $filename = preg_replace('/[^A-Za-z0-9 ._&\-]/', '', $filename);

            $h = '';
            $f = '';
            $pdf_options = array(
                "source_type" => 'html',
                "source" => $html,
                "action" => 'save',
                "save_directory" => __DIR__ . '/../../assets/uploads/meter_data_form_files',
                "file_name" => $filename,
                "page_size" => 'A4',
                "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                "header" => $h,
                "footer" => $f
            );

            set_error_handler(function ($errno, $errstr, $errfile, $errline, array $errcontext) {
                if (0 === error_reporting()) {
                    return false;
                }
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            });

            $this->pdf->phptopdf($pdf_options);

            //Get File
            if (!file_exists(FCPATH . 'assets/uploads/meter_data_form_files/' . $filename)) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting meter data form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }

            $this->common->update_data('tbl_lead_meter_data', array('uuid' => $uuid), array('pdf_file' => $filename));

            //Send Mail
            $communication_data = array();
            $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/meter_data_form_files/' . $filename;
            $user_id = $this->aauth->get_user_id();
            $sales_rep_email = $this->common->fetch_cell('aauth_users', 'email', array('id' => $user_id));
            $this->email_manager->send_meter_data_form_success_email($communication_data, $sales_rep_email);

            $data['status'] = 'Meter Data Pdf File Generated and Saved Succeffuly.';
            $data['success'] = TRUE;
            echo json_encode($data);
            die;
        } catch (ErrorException $e) {
            $status = 'Look like something went wrong while conversion of meter data form. Please try again later or contact support if issue still persists.';
            $data['success'] = FALSE;
            $data['status'] = $status;
        } catch (Exception $e) {
            $status = $e->getMessage();
            $data['success'] = FALSE;
            $data['status'] = $status;
            echo json_encode($data);
            die;
        }
    }

    public function save_lead_details()
    {
        if (!empty($this->input->post())) {
            $user_group = $this->aauth->get_user_groups()[0]->id;
            $lead_data = $this->input->post('lead');
            $lead_to_user = $this->input->post('lead_to_user');
            $uuid = $lead_data['uuid'];
            $is_lead_exists = $this->common->fetch_where('tbl_leads', '*', array('uuid' => $uuid));
            if (empty($is_lead_exists)) {
                $cust_id = $lead_data['cust_id'];
                $is_lead_exists = $this->common->fetch_where('tbl_leads', '*', array('cust_id' => $cust_id));
            }
            if (count($is_lead_exists) > 0) {
                $lead_data['updated_at'] = date('Y-m-d H:i:s');

                if ((isset($lead_data['lead_stage']) && $lead_data['lead_stage'] != '' && $lead_data['lead_stage'] != 'undefined' && $lead_data['lead_stage'] != 0)) {
                    $lead_data['prev_lead_stage'] = $is_lead_exists[0]['lead_stage'];
                }

                //So that the userid for lead created by will not change.
                if (isset($lead_data['user_id'])) {
                    unset($lead_data['user_id']);
                }

                if ((isset($lead_data['lead_type']) && $lead_data['lead_type'] != '' && $lead_data['lead_type'] != 'undefined')) {
                    $lead_data['lead_type '] =  $lead_data['lead_type'];
                }

                if ((isset($lead_data['lead_segment']) && $lead_data['lead_segment'] != '' && $lead_data['lead_segment'] != 'undefined')) {
                    $lead_data['lead_segment'] =  $lead_data['lead_segment'];
                }

                if ((isset($lead_data['lead_industry']) && $lead_data['lead_industry'] != '' && $lead_data['lead_industry'] != 'undefined')) {
                    $lead_data['lead_industry'] =  $lead_data['lead_industry'];
                }

                //Escape and Sanitize data before update
                $lead_data = $this->components->escape_data($lead_data);
                $stat = $this->common->update_data('tbl_leads', array('uuid' => $is_lead_exists[0]['uuid']), $lead_data);
                $id = $is_lead_exists[0]['id'];
                //Assign Lead to Same User or Another User
                if ($id && $lead_to_user != '' && ($user_group == 1 || $user_group == 6 || $user_group == 7)) {
                    $lead_user_update_data = array();
                    $lead_user_update_data['lead_id'] = $id;
                    $lead_user_update_data['user_id'] = $lead_to_user;
                    $this->common->insert_or_update('tbl_lead_to_user', array('lead_id' => $id), $lead_user_update_data);
                }
            } else {
                $lead_data['user_id'] = (isset($lead_data['user_id']) && $lead_data['user_id'] != '' && $lead_data['user_id'] != 'undefined') ? $lead_data['user_id'] : 1;

                if ((isset($lead_data['lead_type']) && $lead_data['lead_type'] != '' && $lead_data['lead_type'] != 'undefined')) {
                    $lead_data['lead_type '] =  $lead_data['lead_type'];
                }

                if ((isset($lead_data['lead_segment']) && $lead_data['lead_segment'] != '' && $lead_data['lead_segment'] != 'undefined')) {
                    $lead_data['lead_segment'] =  $lead_data['lead_segment'];
                } else {
                    $lead_data['lead_segment'] = 4;
                }

                if ((isset($lead_data['lead_industry']) && $lead_data['lead_industry'] != '' && $lead_data['lead_industry'] != 'undefined')) {
                    $lead_data['lead_industry'] =  $lead_data['lead_industry'];
                }

                $lead_data['created_at'] = date('Y-m-d H:i:s');
                //Escape and Sanitize data before insert
                $lead_data = $this->components->escape_data($lead_data);
                $stat = $this->common->insert_data('tbl_leads', $lead_data);
                $id = $this->common->insert_id();
                //Assign Lead to User
                if ($id && $lead_to_user != '' && ($user_group == 1 || $user_group == 6 || $user_group == 7)) {
                    $lead_user_insert_data = array();
                    $lead_user_insert_data['lead_id'] = $id;
                    $lead_user_insert_data['user_id'] = $lead_to_user;
                    $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
                }
                //If User is Lead Creator Only Then assign lead based on VIC and NSW and Send Emails
                if ($user_group == 8) {
                    $this->handle_lead_creator_assign_lead_to_user($id);
                }
            }
            if ($stat) {
                if (CACHING) {
                    //Delete Cached Leads
                    $this->db->cache_delete('admin', 'lead');
                }
                $data['status'] = 'Deal Saved Successfully';
                $data['id'] = $id;
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something went wrong while saving the data.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    private function handle_lead_creator_assign_lead_to_user($lead_id)
    {
        $lead_data = $this->lead->get_leads(FALSE, $lead_id);
        $user_id = 1;
        if ($lead_data['state_id'] == 2) {
            $user_id = 7; //Jamie
        } elseif ($lead_data['state_id'] == 7) {
            $user_id = 5; //Clement
        }
        $lead_user_insert_data = array();
        $lead_user_insert_data['lead_id'] = $lead_id;
        $lead_user_insert_data['user_id'] = $user_id;
        $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
        //Now we need to send SMS and EMAIL to appropirate Parties
        $notification_data       = [];
        $notification_data['id'] = $lead_id;
        $this->notify->store_notification('NEW_LEAD_BY_LEAD_CREATOR', $notification_data);
    }

    public function fetch_lead_stage_data()
    {
        $data = array();
        //Pagination Params
        $limit = $this->input->get('limit');
        $limit = (isset($limit) && $limit != '') ? (int) $limit : 300;
        $page = $this->input->get('page');
        $page = (isset($page) && $page != '') ? $page : 1;
        $offset = ($page - 1) * $limit;
        //Pagination End

        $stage_id = $this->input->get('stage_id');
        $stage_id = (isset($stage_id) && $stage_id != '') ? $stage_id : FALSE;

        $filters = $this->handle_lead_data_filters();
        $is_filter = ($filters != '') ? TRUE : FALSE;

        $view = $this->input->get('view');
        $view = (isset($view) && $view != '') ? $view : 'list';
        $user_id = $this->aauth->get_user_id();
        $custom_filter_params = $this->input->get('custom');
        $user_cond = "";

        //Having Filter
        $having = FALSE;

        if ($page == 1) {
            //Unset Lead Total Count
            $this->session->unset_userdata('kg_lead_total_count');
            $this->session->unset_userdata('kg_map_lead_total_count');
        }

        if ($view == 'list') {
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_company = 'cust.company_name';
                $search_by_fname = 'cust.first_name';
                $search_by_lname = 'cust.last_name';
                $search_by_phone = 'cust.customer_contact_no';
                $search_by_email = 'cust.customer_email';
                $filters = (isset($keyword) && $keyword != '') ?
                    " AND 
                        ($search_by_company LIKE '%$keyword%'
                            OR $search_by_phone LIKE '%$keyword%' 
                            OR $search_by_email LIKE '%$keyword%' 
                            OR $search_by_fname LIKE '%$keyword%' 
                            OR $search_by_lname LIKE '%$keyword%' 
                            OR CONCAT($search_by_fname,' ', $search_by_lname) LIKE '%$keyword%'
                        )" . $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }

            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->lead->get_leads_data_by_stage_count($stage_id, $filters, $having);
            $this->session->set_userdata('kg_lead_total_count', $total);

            $data['lead_data'] = $this->lead->get_leads_data_by_stage($stage_id, $filters, $having, $offset, $limit, $is_filter);
            if ((count($data['lead_data']) < 100) && ($total < 100)) {
                //So if Total leads with first page is less than 100 than bring all the data without limit.
                $offset = $limit = FALSE;
                $data['lead_data'] = $this->lead->get_leads_data_by_stage($stage_id, $filters, $having, $offset, $limit, $is_filter);
            }
            //echo $this->db->last_query();die;
            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        } else if ($view == 'map') {
            //Get Logged in user Coordiantes we set on after login
            $coordinates = $this->session->userdata('coordinates');
            $logged_in_user_state = $this->session->userdata('logged_in_user_state');
            $this->session->unset_userdata('logged_in_user_state');
            if (!empty($coordinates) && ($logged_in_user_state == '' || $logged_in_user_state == NULL)) {
                if ($coordinates['lat'] != NULL) {
                    $c_lead_data = $this->lead->get_state_by_user_current_latlng($coordinates['lat'], $coordinates['lng']);
                    if (!empty($c_lead_data)) {
                        $this->session->set_userdata('logged_in_user_state', $c_lead_data[0]['state_id']);
                        $logged_in_user_state = $c_lead_data[0]['state_id'];
                    }
                }
            }

            $cond = FALSE;
            $limit = $this->input->get('limit');
            $limit = (isset($limit) && $limit != '') ? (int) $limit : 300;
            $page = $this->input->get('page');
            $page = (isset($page) && $page != '') ? $page : 1;
            $offset = ($page - 1) * $limit;
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $filter_by_type = $this->input->get('filter_by_type');
            $filter_by_stage = $this->input->get('filter_by_stage');
            $filter_by_segment = $this->input->get('filter_by_segment');
            $filter_by_user = $this->input->get('filter_by_user');
            $lat = $this->input->get('lat');
            $lng = $this->input->get('lng');


            if (isset($filter_by_type) && $filter_by_type != '' && $filter_by_type != 'null' && $filter_by_type != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_type IN ($filter_by_type)";
                $is_filter = TRUE;
            }

            if (isset($filter_by_stage) && $filter_by_stage != '' && $filter_by_stage != 'null' && $filter_by_stage != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_stage IN ($filter_by_stage)";
                $is_filter = TRUE;
            }

            if (isset($filter_by_segment) && $filter_by_segment != '' && $filter_by_segment != 'null' && $filter_by_segment != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_segment IN ($filter_by_segment)";
                $is_filter = TRUE;
            }

            if (isset($filter_by_user) && $filter_by_user != '' && $filter_by_user != 'null' && $filter_by_user != 'undefined') {
                $sales_rep_users = [];
                $la_users = [];

                $filter_by_user = explode(',', $filter_by_user);
                foreach ($filter_by_user as $key => $value) {
                    $filter_by_user_type = $this->common->fetch_cell('aauth_user_to_group', 'group_id', array('user_id' => $value));
                    if ($filter_by_user_type == 6 || $filter_by_user_type == 7) {
                        $sales_rep_users[] = $value;
                    } else {
                        $la_users[] = $value;
                    }
                }

                $cond_col = '';
                if (!empty($sales_rep_users)) {
                    $sales_rep_users = implode(',', $sales_rep_users);
                    $sales_rep_users = (isset($sales_rep_users) && $sales_rep_users != '') ?  $sales_rep_users : '';
                    $cond_col .= ' AND ltu.user_id IN (' . $sales_rep_users . ')';
                }

                if (!empty($la_users)) {
                    $la_users = implode(',', $la_users);
                    $la_users = (isset($la_users) && $la_users != '') ? $la_users : '';
                    $cond_col .= ' AND leads.user_id IN (' . $la_users . ')';
                }

                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= $cond_col;
                //echo $cond;die;
                $is_filter = TRUE;
            }

            if (isset($keyword) && $keyword != '' && $keyword != 'null' && $keyword != 'undefined') {
                $search_by = $this->input->get('search_by');
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $original_keyword = $keyword;
                $keyword_single_escape = str_replace("'", "\'", $keyword);
                $keyword = preg_replace("/'/", "\\\\\\\\\\'", $keyword);
                $cond .= (isset($keyword) && $keyword != '') ? ' AND ' . $search_by . ' LIKE "%' . $original_keyword . '%" OR ' . $search_by . '="' . $keyword . '" OR ' . $search_by . '="' . $keyword_single_escape . '"' : '';
                $is_filter = TRUE;
                //echo $cond;die;
            }

            if (isset($lat) && $lat != '' && $lat != 'null' && $lat != 'undefined') {
                $this->fetch_nearby_leads($offset, $limit, $cond);
            }

            if ($is_filter == FALSE && isset($logged_in_user_state) && $logged_in_user_state != NULL) {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND cl.state_id IN ($logged_in_user_state)";
            }

            if ($cond == FALSE) {
                $cond = ' WHERE 1=1 ' . $filters;
            }

            //echo $cond;die;
            //Store latest search criteria
            if ($cond != FALSE) {
                $search_logs_data['user_id'] = $user_id;
                $search_logs_data['data'] = json_encode($this->input->get());
                $this->common->insert_data('tbl_lead_map_search_logs', $search_logs_data);
            }


            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->lead->get_leads_count($cond);
            $this->session->set_userdata('kg_map_lead_total_count', $total);

            if ($total < 200) {
                $limit = FALSE;
            }

            $data['lead_data'] = $this->lead->get_leads_data_for_map($offset, $limit, $cond, $is_filter);
            //echo $this->db->last_query();die;
            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        } else if ($view == 'calendar') {
            $start_date = $this->input->get('start_date');
            $end_date = $this->input->get('end_date');
            $filters = ' WHERE DATE(created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) {
                $filters = $filters . ' AND user_id IN(" . $user_id . ")';
            }
            $data['lead_data'] = $this->lead->get_leads_data_for_calendar($offset, $limit, $filters, $is_filter);
        } else if ($view == 'report') {
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_company = 'cust.company_name';
                $search_by_fname = 'cust.first_name';
                $search_by_lname = 'cust.last_name';
                $search_by_phone = 'cust.customer_contact_no';
                $search_by_email = 'cust.customer_email';
                $filters = (isset($keyword) && $keyword != '') ?
                    " AND (MATCH($search_by_company, $search_by_fname,$search_by_lname ,$search_by_phone,$search_by_email) AGAINST('$keyword' IN BOOLEAN MODE)
            ) OR $search_by_company LIKE '%$keyword'" . $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }
            $report_type = $this->input->get('report_type');
            $lead_source_filter = $this->input->get('filter');
            $lead_Source_Set = FALSE;
            if (isset($lead_source_filter['custom']) && !empty($lead_source_filter['custom'])) {
                foreach ($lead_source_filter['custom'] as $key => $value) {
                    if ($key == 'lead_sources') {
                        $lead_Source_Set = TRUE;
                    }
                }
            }
            if (!$lead_Source_Set) {
                if ($report_type == 'facebook') {
                    $filters .= ' AND leads.lead_source IN ("Facebook Lead","Kuga Lead")';
                    $is_filter = ($filters != '') ? TRUE : FALSE;
                }
            }
            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->lead->get_leads_data_by_report_count($stage_id, $filters, $having);
            $this->session->set_userdata('kg_lead_total_count', $total);

            $data['lead_data'] = $this->lead->get_leads_data_by_report($stage_id, $filters, $having, $offset, $limit, $is_filter);
            if ((count($data['lead_data']) < 100) && ($total < 100)) {
                //So if Total leads with first page is less than 100 than bring all the data without limit.
                $offset = $limit = FALSE;
                $data['lead_data'] = $this->lead->get_leads_data_by_report($stage_id, $filters, $having, $offset, $limit, $is_filter);
            }
            //echo $this->db->last_query();die;
            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        } else {
            $limit = $this->input->get('limit');
            $limit = (isset($limit) && $limit != '') ? (int) $limit : 20;
            $page = $this->input->get('page');
            $page = (isset($page) && $page != '') ? $page : 1;
            $offset = ($page - 1) * $limit;

            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_company = 'cust.company_name';
                $search_by_fname = 'cust.first_name';
                $search_by_lname = 'cust.last_name';
                $search_by_phone = 'cust.customer_contact_no';
                $search_by_email = 'cust.customer_email';
                $filters = (isset($keyword) && $keyword != '') ?
                    " AND ((MATCH($search_by_company, $search_by_fname,$search_by_lname ,$search_by_phone,$search_by_email) AGAINST('$keyword' IN BOOLEAN MODE)
            ) OR $search_by_company LIKE '%$keyword'" . $user_cond . ")" : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }

            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->lead->get_leads_data_by_stage_count_for_pipeline($stage_id, $filters, $having);
            $this->session->set_userdata('kg_lead_total_count', $total);

            if (isset($page) && $page == 1) {
                $leads_stages = $this->common->fetch_orderBy('tbl_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
                $i = 0;
                foreach ($leads_stages as $stage) {
                    $proposal_data_by_stages[($i++)] = $this->lead->get_leads_data_by_stage_for_pipeline($stage['id'], $filters, $having, $offset, $limit, $is_filter);
                }
                $data['proposal_stages'] = $leads_stages;
                $data['proposal_data_by_stages'] = $proposal_data_by_stages;
            } else {
                $proposal_data_by_stages = $this->lead->get_leads_data_by_stage_for_pipeline(FALSE, $filters, $having, $offset, $limit, $is_filter);
                $data['proposal_data_by_stages'] = $proposal_data_by_stages;
            }

            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        }
        $data['success'] = TRUE;
        echo json_encode($data);
        die;
    }

    public function fetch_nearby_leads($offset, $limit, $cond)
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lat = $this->input->get('lat');
            $lng = $this->input->get('lng');
            $save_search_log = $this->input->get('save_search_log');
            $save_search_log = (isset($save_search_log) && $save_search_log != 'false') ? TRUE : FALSE;

            $data['lead_data'] = $this->lead->get_leads_by_coordinates($offset, $limit, $cond, $lat, $lng);

            if ($save_search_log && !empty($data['lead_data'])) {
                $search_logs_data['user_id'] = $this->aauth->get_user_id();
                $search_logs_data['data'] = json_encode($this->input->get());
                $this->common->insert_data('tbl_lead_map_search_logs', $search_logs_data);
            }

            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    private function handle_lead_data_filters()
    {
        $user_id = $this->aauth->get_user_id();
        $parsed_filter_options = '';
        $filter = $this->input->get('filter');
        $is_filter_sales_user = FALSE;
        $is_filter_la_user = FALSE;
        $is_source_make_it_cheaper = FALSE;
        $view = $this->input->get('view');

        if (isset($filter) && !empty($filter)) {
            if (isset($filter['start_date']) && $filter['start_date'] != '') {
                $start_date = $filter['start_date'];
                $end_date = $filter['end_date'];

                if (isset($view) && $view == 'pipeline') {
                    if ($start_date != '2016-01-01') {
                        $parsed_filter_options .= ' AND DATE(activity.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
                    }
                } else {
                    $parsed_filter_options .= ' AND DATE(leads.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
                }
            }
            if (isset($filter['custom']) && !empty($filter['custom'])) {
                foreach ($filter['custom'] as $key => $value) {
                    switch ($key) {
                        case 'userid':
                            if ($value == '') {
                                $is_filter_sales_user = TRUE;
                            } else if ($value != 'All') {
                                $is_filter_sales_user = TRUE;
                                $parsed_filter_options .= " AND ltu.user_id IN(" . $value . ")";
                            } else if ($value == 'All') {
                                $is_filter_sales_user = TRUE;
                                $users = $this->user->get_users(7, FALSE, FALSE);
                                $all_users = array_column($users, 'user_id');
                                $all_users = implode(',', $all_users);
                                $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
                                $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id . $all_users . ")";
                            }
                            break;
                        case 'la_userid':
                            if ($key == 'la_userid') {
                                $is_filter_la_user = TRUE;
                                if ($value == '') {
                                    $is_filter_la_user = TRUE;
                                } else if ($value != 'All') {
                                    $is_filter_la_user = TRUE;
                                    $parsed_filter_options .= " AND leads.user_id IN(" . $value . ")";
                                } else if ($value == 'All') {
                                    $is_filter_la_user = TRUE;
                                    $users = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                                    $all_users = array_column($users, 'user_id');
                                    $all_users = implode(',', $all_users);
                                    $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
                                    $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . $all_users . ")";
                                }
                            }
                            break;
                        case 'state_id':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND cl.' . $key . '=' . $value;
                            }
                            break;
                        case 'lead_type':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '=' . $value;
                            }
                            break;
                        case 'lead_source':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '="' . $value . '"';
                                if ($value == 'Make it Cheaper') {
                                    $is_source_make_it_cheaper = TRUE;
                                }
                            }
                            break;
                        case 'lead_sources':
                            if ($value != '') {
                                $sources = implode('", "', $value);
                                $parsed_filter_options .= ' AND leads.lead_source IN ("' . $sources . '")';
                            }
                            //print_r($parsed_filter_options);die;
                            break;
                        case 'lead_stage':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '="' . $value . '"';
                            }
                            break;
                        case 'lead_location_type':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '="' . $value . '"';
                            }
                            break;
                        default:
                            if ($value != '') {
                                $parsed_filter_options .= ' AND ' . $key . '=' . $value;
                            }
                            break;
                    }
                }
            }
        }

        if ($this->components->is_team_leader()) {
            $make_it_cheaper_profile = 23;
            if ($this->components->is_lead_allocation_team_leader() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                //For Make it cheaper profile
                if (!$is_source_make_it_cheaper) {

                    $users = $this->user->get_users(FALSE, FALSE, $user_id);
                    $all_users = array_column($users, 'user_id');
                    $all_users = implode(',', $all_users);
                    $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';

                    $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id . $all_users . ")";
                }
            }
        } else if ($this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id . ")";
            }
        }



        return $parsed_filter_options;
    }

    /** Function to find sum of total of all proposal items and cost */
    public function fetch_proposal_totals()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $deal_ref = $this->input->get('deal_ref');
            $lead_data = $this->common->fetch_row('tbl_leads', 'id', array('uuid' => $deal_ref));
            $data['proposal_totals'] = $this->product->get_deal_products_sum_by_product_type($lead_data['id']);
            $data['solar_proposal_totals'] = $this->lead->get_solar_proposal_totals($lead_data['id']);
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    /** LEAD STAGES CURD FUNCTIONS * */
    public function manage_lead_stages()
    {
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $data['title'] = 'Admin - Manage Lead Stages';
        $data['stages'] = $this->common->fetch_where('tbl_lead_stages', '*', array('status' => 1));
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('lead/manage_stages');
        $this->load->view('partials/footer');
    }

    public function add_lead_stages($id = FALSE)
    {
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $data['title'] = 'Admin - Add Lead Stages';

        if ($id) {
            $data['title'] = 'Admin - Edit Lead Stages';
            $stage = $this->common->fetch_where('tbl_lead_stages', '*', array('id' => $id));
            if (empty($stage)) {
                redirect('admin/settings/lead-stages/manage/');
            }
            $data['stage_name'] = $stage[0]['stage_name'];
            $data['order_no'] = $stage[0]['order_no'];
        }

        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('stage_name', 'Stage Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                //Update Data to Details Table
                $stage_data = array();
                $stage_data['stage_name'] = $this->input->post('stage_name');
                $stage_data['order_no'] = $this->input->post('order_no');
                if ($id) {
                    $stat = $this->common->update_data('tbl_lead_stages', array('id' => $id), $stage_data);
                    $message = 'Stage Updated successfuly.';
                } else {
                    $stat = $this->common->insert_data('tbl_lead_stages', $stage_data);
                    $message = 'Stage Created successfuly.';
                }
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success">' . $message . '</div>');
                    redirect('admin/settings/lead-stages/manage/');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            } else {
                $data['stage_name'] = $this->input->post('stage_name');
                $data['order_no'] = $this->input->post('order_no');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('lead/form_stages');
        $this->load->view('partials/footer');
    }

    public function delete_lead_stage($id = FALSE)
    {
        if (!$this->aauth->is_member('Admin') && $type != '') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $tbl = 'tbl_lead_stages';

        if ($id) {
            $stage = $this->common->fetch_where($tbl, '*', array('id' => $id));
            if (empty($stage)) {
                redirect('admin/settings/lead-stages/manage/');
            } else {
                $stat = $this->common->delete_data($tbl, array('id' => $id));
                if ($stat) {
                    if ($stat == -1) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Stage cannot be deleted it is linked to some lead.</div>');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Stage Deleted successfuly.</div>');
                    }
                    redirect('admin/settings/lead-stages/manage/');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
        }
    }

    public function update_lead_stage()
    {
        if (!empty($this->input->post())) {
            $uuid = $this->input->post('uuid');
            $update = array();
            $deal_stage = $this->input->post('deal_stage');
            $deal_won_lost = $this->input->post('deal_won_lost');
            (isset($deal_stage) && $deal_stage != '') ? $update['lead_stage'] = $deal_stage : '';
            $update['updated_at'] = date('Y-m-d H:i:s');
            $lead_data = $this->common->fetch_row('tbl_leads', 'lead_stage', array('uuid' => $uuid));
            (!empty($lead_data) && $lead_data['lead_stage'] != '') ? $update['prev_lead_stage'] = $lead_data['lead_stage'] : '';
            //$user_id = $this->aauth->get_user()->id;
            $stat = $this->common->update_data('tbl_leads', ['uuid' => $uuid], $update);
            if ($stat) {
                if (CACHING) {
                    //Delete Cached Leads
                    $this->db->cache_delete('admin', 'lead');
                }
                $data['status'] = 'Deal information updated successfully';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function pipedrive_migrate()
    {
        $deal_data = $this->pipedrive_deal_migrate();
        //$filename = __DIR__ . './../../deals.csv';
        $filename = __DIR__ . './../../customer_new.csv';
        $file = fopen($filename, "r");
        ini_set('auto_detect_line_endings', true);
        $count = $c1 = 0;
        $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($count == 0) {
                $columns = ['first_name', 'last_name', 'company_name', 'customer_email', 'customer_contact_no'];
            } else {
                for ($i = 0; $i < count($getData); $i++) {
                    if ($i == 0) {
                        $name = explode(' ', $getData[$i]);
                        $rows[0] = (isset($name[0])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[0])) : NULL;
                        $rows[1] = (isset($name[1])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[1])) : NULL;
                    }
                    if ($i == 2) {
                        $rows[2] = trim($getData[$i]);
                    }
                    if ($i == 3) {
                        $rows[3] = trim($getData[$i]);
                    }
                    if ($i == 4) {
                        $rows[4] = trim($getData[$i]);
                    }
                }
                $combined_data = array_combine($columns, $rows);
                if ($combined_data['company_name'] != '') {
                    //$is_customer_exists = $this->common->fetch_where('tbl_customers', 'company_name', ['company_name' => $combined_data['company_name']]);
                    //if(empty($is_customer_exists)){
                    //$this->common->insert_data('tbl_customers', $combined_data);
                    //}
                    $all_data[($c1)] = $combined_data;
                    $c1++;
                }
            }
            $count++;
        }
        $cust_data = [];
        $cust_loc_data = [];
        $lead_data = [];
        //echo "<pre>";
        $i = 0;
        foreach ($deal_data as $key => $value) {
            $key = array_search($value['company_name'], array_column($all_data, 'company_name'));
            if ($key !== FALSE) {
                //Cust Data
                $cust_data[$i]['first_name'] = $all_data[$key]['first_name'];
                $cust_data[$i]['last_name'] = $all_data[$key]['last_name'];
                $cust_data[$i]['company_name'] = $value['company_name'];
                $cust_data[$i]['customer_email'] = $all_data[$key]['customer_email'];
                $cust_data[$i]['customer_contact_no'] = $all_data[$key]['customer_contact_no'];
                $cust_data[$i]['created_at'] = $value['created_at'];
                //Lead Data
                $stage_id = 1;
                $lead_status = 'open';
                if ($value['lead_stage'] == 'Closed') {
                    $stage_id = 12;
                    $lead_status = 'lost';
                } else if ($value['lead_stage'] == 'Waiting for customer response') {
                    $stage_id = 8;
                } else if ($value['lead_stage'] == 'In Progress - Cold' || $value['lead_stage'] == 'GONE COLD') {
                    $stage_id = 10;
                } else if ($value['lead_stage'] == 'New Appointment') {
                    $stage_id = 3;
                } else if ($value['lead_stage'] == 'Follow up required' || $value['lead_stage'] == 'FOLLOW UP') {
                    $stage_id = 2;
                } else if ($value['lead_stage'] == 'In Progress - Warm') {
                    $stage_id = 6;
                } else if ($value['lead_stage'] == 'Prospects' || $value['lead_stage'] == 'PROSPECT' || $value['lead_stage'] == 'prospect' || $value['lead_stage'] == 'Prospective Tenders') {
                    $stage_id = 1;
                } else if ($value['lead_stage'] == 'Sales Qualified Lead') {
                    $stage_id = 3;
                } else if ($value['lead_stage'] == 'PROPOSAL DELIVERED') {
                    $stage_id = 7;
                } else if ($value['lead_stage'] == 'Proposal Ready') {
                    $stage_id = 7;
                } else if ($value['lead_stage'] == 'WAITING FOR DECISION') {
                    $stage_id = 8;
                } else if ($value['lead_stage'] == 'Closable Deal') {
                    $stage_id = 9;
                } else if ($value['lead_stage'] == 'PROPOSAL GENERATION' || $value['lead_stage'] == 'meta data proposal required') {
                    $stage_id = 5;
                }

                if ($value['lead_status'] == 'Won') {
                    $stage_id = 11;
                    $lead_status = 'won';
                } else if ($value['lead_status'] == 'Lost') {
                    $stage_id = 12;
                    $lead_status = 'lost';
                }

                $lead_data[$i]['lead_stage'] = $stage_id;
                $lead_data[$i]['lead_status'] = $lead_status;
                $lead_data[$i]['created_at'] = $value['created_at'];
                $lead_data[$i]['updated_at'] = (isset($value['updated_at']) && $value['updated_at'] != NULL && $value['updated_at'] != '') ? $value['updated_at'] : $value['created_at'];
                $lead_data[$i]['note'] = $value['notes'];
                $lead_data[$i]['user_id'] = $value['full_name'];
                $lead_data[$i]['lead_value'] = $value['deal_value'];

                //Cust Loc Data
                $cust_loc_data[$i]['address'] = $value['address'];
                $cust_loc_data[$i]['site_name'] = $value['address'];
            } else {
                //Cust Data
                $name = explode(' ', $value['cust_name']);
                $cust_data[$i]['first_name'] = (isset($name[0])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[0])) : NULL;
                $cust_data[$i]['last_name'] = (isset($name[1])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[1])) : NULL;
                $cust_data[$i]['company_name'] = $value['company_name'];
                $cust_data[$i]['customer_email'] = NULL;
                $cust_data[$i]['customer_contact_no'] = NULL;
                $cust_data[$i]['created_at'] = $value['created_at'];
                //Lead Data
                $stage_id = 1;
                $lead_status = 'open';
                if ($value['lead_stage'] == 'Closed') {
                    $stage_id = 12;
                    $lead_status = 'lost';
                } else if ($value['lead_stage'] == 'Waiting for customer response') {
                    $stage_id = 8;
                } else if ($value['lead_stage'] == 'In Progress - Cold' || $value['lead_stage'] == 'GONE COLD') {
                    $stage_id = 10;
                } else if ($value['lead_stage'] == 'New Appointment') {
                    $stage_id = 3;
                } else if ($value['lead_stage'] == 'Follow up required' || $value['lead_stage'] == 'FOLLOW UP') {
                    $stage_id = 2;
                } else if ($value['lead_stage'] == 'In Progress - Warm') {
                    $stage_id = 6;
                } else if ($value['lead_stage'] == 'Prospects' || $value['lead_stage'] == 'PROSPECT' || $value['lead_stage'] == 'prospect' || $value['lead_stage'] == 'Prospective Tenders') {
                    $stage_id = 1;
                } else if ($value['lead_stage'] == 'Sales Qualified Lead') {
                    $stage_id = 3;
                } else if ($value['lead_stage'] == 'PROPOSAL DELIVERED') {
                    $stage_id = 7;
                } else if ($value['lead_stage'] == 'Proposal Ready') {
                    $stage_id = 7;
                } else if ($value['lead_stage'] == 'WAITING FOR DECISION') {
                    $stage_id = 8;
                } else if ($value['lead_stage'] == 'Closable Deal') {
                    $stage_id = 9;
                } else if ($value['lead_stage'] == 'PROPOSAL GENERATION' || $value['lead_stage'] == 'meta data proposal required') {
                    $stage_id = 5;
                }

                if ($value['lead_status'] == 'Won') {
                    $stage_id = 11;
                    $lead_status = 'won';
                } else if ($value['lead_status'] == 'Lost') {
                    $stage_id = 12;
                    $lead_status = 'lost';
                }

                $lead_data[$i]['lead_stage'] = $stage_id;
                $lead_data[$i]['lead_status'] = $lead_status;
                $lead_data[$i]['created_at'] = $value['created_at'];
                $lead_data[$i]['updated_at'] = (isset($value['updated_at']) && $value['updated_at'] != NULL && $value['updated_at'] != '') ? $value['updated_at'] : $value['created_at'];
                $lead_data[$i]['note'] = $value['notes'];
                $lead_data[$i]['user_id'] = $value['full_name'];
                $lead_data[$i]['lead_value'] = $value['deal_value'];

                //Csut Loc Data
                $cust_loc_data[$i]['address'] = $value['address'];
                $cust_loc_data[$i]['site_name'] = $value['address'];
            }
            $i++;
        }
        fclose($file);

        //echo "<pre>";
        //print_r($cust_data);die;

        foreach ($cust_data as $key => $value) {
            $cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name' => $value['company_name']));
            if (empty($cust_data)) {
                //Tbl Customers
                $this->common->insert_data('tbl_customers', $value);
                $cid = $this->common->insert_id();

                //Tbl Leads
                $user_data = $this->common->fetch_row('tbl_user_details', '*', array('full_name' => $lead_data[$key]['user_id']));
                if (!empty($user_data)) {
                    $lead_data[$key]['user_id'] = $user_data['user_id'];
                } else {
                    $lead_data[$key]['user_id'] = 1;
                }
                $lead_data[$key]['cust_id'] = $cid;
                $lead_data[$key]['uuid'] = $this->components->uuid();
                $this->common->insert_data('tbl_leads', $lead_data[$key]);

                //Tbl Lead to User
                $lid = $this->common->insert_id();
                $ltu = [];
                $ltu['user_id'] = $lead_data[$key]['user_id'];
                $ltu['lead_id'] = $lid;
                $ltu['created_at'] = $lead_data[$key]['created_at'];
                $this->common->insert_data('tbl_lead_to_user', $ltu);

                //Tbl Customer Location
                $cust_loc_data[$key]['cust_id'] = $cid;
                if (stripos($cust_loc_data[$key]['address'], 'NSW') !== false) {
                    $cust_loc_data[$key]['state_id'] = 2;
                } else if (stripos($cust_loc_data[$key]['address'], 'New South Wales') !== false) {
                    $cust_loc_data[$key]['state_id'] = 2;
                } else {
                    $cust_loc_data[$key]['state_id'] = 7;
                }
                $postcodes = array();
                preg_match('/[0-9]{4}/', $cust_loc_data[$key]['address'], $postcodes);
                if (!empty($postcodes)) {
                    $cust_loc_data[$key]['postcode'] = $postcodes[0];
                }
                $this->common->insert_data('tbl_customer_locations', $cust_loc_data[$key]);
            }
        }
    }

    public function pipedrive_deal_migrate()
    {
        //$filename = __DIR__ . './../../deals.csv';
        $filename = __DIR__ . './../../deals_new.csv';
        $file = fopen($filename, "r");
        ini_set('auto_detect_line_endings', true);
        $count = 0;
        $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($count == 0) {
                //print_r($getData);die;
                $columns = ['deal_value', 'company_name', 'cust_name', 'full_name', 'lead_stage', 'lead_status', 'address', 'created_at', 'notes', 'updated_at'];
            } else {
                for ($i = 0; $i < count($getData); $i++) {
                    if ($i == 1) {
                        $rows[0] = trim($getData[$i]);
                    }
                    if ($i == 2) {
                        $rows[1] = trim($getData[$i]);
                        if ($rows[1] == '') {
                            $rows[1] = trim($getData[1]);
                            $rows[1] = str_replace(' deal', '', $rows[1]);
                        }
                    }
                    if ($i == 3) {
                        $rows[2] = trim($getData[$i]);
                    }
                    if ($i == 6) {
                        $rows[3] = trim($getData[$i]);
                    }
                    if ($i == 7) {
                        $rows[4] = trim($getData[$i]);
                    }
                    if ($i == 8) {
                        $rows[5] = trim($getData[$i]);
                    }
                    if ($i == 9) {
                        $rows[6] = trim($getData[$i]);
                    }
                    if ($i == 12) {
                        $rows[7] = trim($getData[$i]);
                    }
                    if ($i == 13) {
                        $rows[8] = trim($getData[$i]);
                    }
                    if ($i == 14) {
                        $rows[9] = trim($getData[$i]);
                    }
                }
                $combined_data = array_combine($columns, $rows);
            }
            if ($count > 0) {
                if ($combined_data['company_name'] != '') {
                    $all_data[($count - 1)] = $combined_data;
                }
            }
            $count++;
        }




        /*         * foreach ($all_data as $key => $value) {
          $cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name' => $value['company_name']));
          if (!empty($cust_data)) {
          $lead_data = $this->common->fetch_row('tbl_leads', '*', array('cust_id' => $cust_data['id']));
          if (!empty($lead_data)) {
          $update_data = [];
          $update_data['updated_at'] = (isset($value['updated_at']) && $value['updated_at'] != NULL && $value['updated_at'] != '') ? $value['updated_at'] : $value['created_at'];
          $this->common->update_data('tbl_leads', array('id' => $lead_data['id']), $update_data);
          }
          }
          } */
        //echo "<pre>";
        //print_r($all_data);die;
        fclose($file);
        return $all_data;
    }

    public function pipedrive_activity_migrate()
    {
        $filename = __DIR__ . './../../activities.csv';
        $file = fopen($filename, "r");
        ini_set('auto_detect_line_endings', true);
        $count = $c1 = 0;
        $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($count == 0) {
                $columns = ['activity_title', 'lead_id', 'scheduled_duration', 'user_id', 'activity_note', 'activity_type', 'created_at'];
            } else {
                for ($i = 0; $i < count($getData); $i++) {
                    if ($i == 1) {
                        $rows[0] = trim($getData[$i]);
                    }
                    if ($i == 6) {
                        $rows[1] = trim($getData[$i]);
                    }
                    if ($i == 8) {
                        $rows[2] = trim($getData[$i]);
                    }
                    if ($i == 9) {
                        $rows[3] = trim($getData[$i]);
                    }
                    if ($i == 10) {
                        $rows[4] = trim($getData[$i]);
                    }
                    if ($i == 11) {
                        $rows[5] = trim($getData[$i]);
                    }
                    if ($i == 12) {
                        $rows[6] = trim($getData[$i]);
                    }
                }

                $combined_data = array_combine($columns, $rows);
                if ($combined_data['lead_id'] != '') {
                    $all_data[($c1)] = $combined_data;
                    $c1++;
                }
            }
            $count++;
        }
        fclose($file);


        foreach ($all_data as $key => $value) {
            $cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name' => $value['lead_id']));
            if (!empty($cust_data)) {
                $lead_data = $this->common->fetch_row('tbl_leads', '*', array('cust_id' => $cust_data['id']));
                if (!empty($lead_data)) {
                    $all_data[$key]['lead_id'] = $lead_data['id'];
                    $user_data = $this->common->fetch_row('tbl_user_details', '*', array('full_name' => $all_data[$key]['user_id']));
                    if (!empty($user_data)) {
                        $all_data[$key]['user_id'] = $user_data['user_id'];
                    } else {
                        $all_data[$key]['user_id'] = 1;
                    }
                    if ($all_data[$key]['activity_type'] == 'SITE VISIT') {
                        $all_data[$key]['activity_type'] = 'Visit';
                    } else if ($all_data[$key]['activity_type'] == 'CALL') {
                        $all_data[$key]['activity_type'] = 'Phone';
                    } else if ($all_data[$key]['activity_type'] == 'EMAIL') {
                        $all_data[$key]['activity_type'] = 'Email';
                    } else {
                        $all_data[$key]['activity_type'] = 'Note';
                    }
                    $this->common->insert_data('tbl_activities', $all_data[$key]);
                }
            }
        }
    }

    public function export_csv()
    {
        $data = array();
        $action = $this->input->get('action');

        if ($action == 'export_lead_csv') {
            $filters = $this->handle_lead_data_filters();
            $leads = $this->lead->get_leads_for_export($filters);
            $this->components->download_send_headers("leads_export_" . date("Y-m-d") . ".csv");
            echo $this->components->array2csv($leads);
            die();
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function sales_migrate()
    {
        //$filename = __DIR__ . './../../deals.csv';
        $filename = __DIR__ . './../../sales.csv';
        $file = fopen($filename, "r");
        ini_set('auto_detect_line_endings', true);
        $count = 0;
        $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();
        $fields = ['hb_submitted_to_ap'];
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($count == 7) {
                $columns = $getData;
            } else if ($count > 7) {
                for ($i = 0; $i < count($getData); $i++) {
                    $rows[$i] = $getData[$i];
                }
                $combined_data = array_combine($columns, $rows);
            }
            $all_data[($count - 1)] = $combined_data;
            $count++;
        }



        echo "<pre>";
        print_r($all_data);
        die;
        fclose($file);
        return $all_data;
    }

    public function geocode_leads()
    {
        $customer_locations = $this->common->fetch_where('tbl_customer_locations', 'id,address,latitude', NULL);
        //$customer_locations = $this->common->fetch_orderBy('tbl_customer_locations','id,address,latitude',NULL,'id','ASC',3);
        foreach ($customer_locations as $key => $value) {
            try {
                if (($value['latitude'] == NULL || $value['latitude'] == '') && ($value['address'] != '' && $value['address'] != NULL)) {
                    $address = $value['address'];
                    $address = str_replace(" ", "+", $address);
                    $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=AIzaSyBNRLQu2NqDjW6prGsgrDh5pHxR-0HHUqQ&address=$address&sensor=false&region=AU");
                    $json = json_decode($json);
                    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
                    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
                    $update_data = array();
                    $update_data['latitude'] = $lat;
                    $update_data['longitude'] = $long;
                    $this->common->update_data(
                        'tbl_customer_locations',
                        array('id' => $value['id']),
                        $update_data
                    );
                    sleep(3);
                }
            } catch (Exception $e) {
                continue;
            }
        }
    }

    public function pipedrive_lead_migrate_new()
    {
        try {
            //$filename = __DIR__ . './../../deals.csv';
            $filename = __DIR__ . './../../Migration8.csv';
            $file = fopen($filename, "r");
            ini_set('auto_detect_line_endings', true);
            $count = $c1 = 0;
            $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();

            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                if ($count == 0) {
                    $columns = ['first_name', 'last_name', 'company_name', 'customer_email', 'customer_contact_no', 'deal_value', 'full_name', 'lead_stage', 'lead_status', 'address', 'created_at', 'status', 'lead_type', 'state'];
                } else {
                    for ($i = 0; $i < count($getData); $i++) {
                        if ($i == 0) {
                            //First name and Last name
                            $name = explode(' ', $getData[3]);
                            $rows[0] = (isset($name[0])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[0])) : NULL;
                            $rows[1] = (isset($name[1])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[1])) : NULL;
                        }
                        if ($i == 2) {
                            //Company name
                            $rows[2] = str_replace('"', '', trim($getData[$i]));
                            $rows[2] = trim($rows[2]);
                        }
                        if ($i == 3) {
                            //Customer Email
                            //$rows[3] = trim($getData[13]);
                            $rows[3] = trim($getData[11]);
                        }
                        if ($i == 4) {
                            //customer_contact_no
                            //$rows[4] = trim($getData[14]);
                            $rows[4] = trim($getData[12]);
                        }
                        if ($i == 5) {
                            //deal_value
                            $rows[5] = trim($getData[1]);
                            $rows[5] = str_replace(',', '', $rows[5]);
                            $rows[5] = str_replace('AUD', '', $rows[5]);
                            $rows[5] = (float) $rows[5];
                        }
                        if ($i == 6) {
                            //full_name - owner name
                            $rows[6] = trim($getData[7]);
                        }
                        if ($i == 7) {
                            //lead_stage
                            $rows[7] = trim($getData[8]);
                        }
                        if ($i == 8) {
                            //lead_status
                            $rows[8] = trim($getData[9]);
                        }
                        if ($i == 9) {
                            //address
                            $rows[9] = trim($getData[10]);
                            $rows[9] = ($rows[9] != '') ? $rows[9] : '';
                            //$rows[9] = ($rows[9] != '') ? $rows[9] : trim($getData[18]); 
                        }
                        if ($i == 10) {
                            //created_at
                            $rows[10] = trim($getData[4]);
                        }
                        if ($i == 11) {
                            //status
                            //$rows[11] = trim($getData[15]);
                            $rows[11] = trim($getData[14]);
                            $rows[11] = ($rows[11] != '') ? $rows[11] : 0;
                        }
                        if ($i == 12) {
                            //lead_type
                            $types = array(
                                "1" => "Solar",
                                "2" => "LED",
                                "3" => "LED + Solar"
                            );
                            $rows[12] = array_search($getData[15], $types);
                            //$rows[12] = array_search($getData[17], $types);
                        }
                        if ($i == 13) {
                            //address
                            //$rows[13] = NULL;
                            $rows[13] = trim($getData[16]);
                            if (stripos($rows[13], 'NSW') !== false) {
                                $rows[13] = 2;
                            } else if (stripos($rows[13], 'New South Wales') !== false) {
                                $rows[13] = 2;
                            } else {
                                $rows[13] = 7;
                            }
                        }
                    }
                    $combined_data = array_combine($columns, $rows);
                    if ($combined_data['company_name'] != '') {
                        //$is_customer_exists = $this->common->fetch_where('tbl_customers', 'company_name', ['company_name' => $combined_data['company_name']]);
                        //if(empty($is_customer_exists)){
                        //$this->common->insert_data('tbl_customers', $combined_data);
                        //}
                        $all_data[($c1)] = $combined_data;
                        $c1++;
                    }
                }
                $count++;
            }
            $cust_data = [];
            $cust_loc_data = [];
            $lead_data = [];
            $company_name = [];
            //echo "<pre>";
            //print_r($all_data);
            //die;
            $i = 0;
            foreach ($all_data as $key => $value) {
                $key = array_search($value['company_name'], array_column($all_data, 'company_name'));
                if ($key !== FALSE) {
                    //Cust Data
                    $cust_data[$i]['first_name'] = $all_data[$key]['first_name'];
                    $cust_data[$i]['last_name'] = $all_data[$key]['last_name'];
                    $cust_data[$i]['company_name'] = $company_name[] = $value['company_name'];
                    $cust_data[$i]['customer_email'] = $all_data[$key]['customer_email'];
                    $cust_data[$i]['customer_contact_no'] = $all_data[$key]['customer_contact_no'];
                    $cust_data[$i]['created_at'] = $value['created_at'];
                    $cust_data[$i]['is_processed'] = 1;
                    //Lead Data
                    $stage_id = 1;
                    $lead_status = 'open';
                    $lead_status = ($value['lead_status'] != '') ? strtolower($value['lead_status']) : $lead_status;
                    if (trim($value['lead_stage']) == 'Prospect') {
                        $stage_id = 1;
                    } else if (trim($value['lead_stage']) == 'Follow Up') {
                        $stage_id = 2;
                    } else if (trim($value['lead_stage']) == 'New Lead') {
                        $stage_id = 3;
                    } else if (trim($value['lead_stage']) == 'Appointment Booked') {
                        $stage_id = 4;
                    } else if (trim($value['lead_stage']) == 'Need Proposal') {
                        $stage_id = 5;
                    } else if (trim($value['lead_stage']) == 'Proposal Rework') {
                        $stage_id = 6;
                    } else if (trim($value['lead_stage']) == 'Proposal Delivered') {
                        $stage_id = 7;
                    } else if (trim($value['lead_stage']) == 'Decision Due') {
                        $stage_id = 8;
                    } else if (trim($value['lead_stage']) == 'Closable Deal') {
                        $stage_id = 9;
                    } else if (trim($value['lead_stage']) == 'Gone Cold') {
                        $stage_id = 10;
                    } else if (trim($value['lead_stage']) == 'Won') {
                        $stage_id = 11;
                        $lead_status = 'won';
                    } else if (trim($value['lead_stage']) == 'Lost') {
                        $stage_id = 12;
                        $lead_status = 'lost';
                    } else if (trim($value['lead_stage']) == 'Proposal Ready') {
                        $stage_id = 13;
                    }

                    $lead_data[$i]['lead_stage'] = $stage_id;
                    $lead_data[$i]['lead_status'] = $lead_status;
                    $lead_data[$i]['created_at'] = date('Y-m-d H:i:s', strtotime($value['created_at']));
                    $lead_data[$i]['updated_at'] = (isset($value['updated_at']) && $value['updated_at'] != NULL && $value['updated_at'] != '') ? date('Y-m-d H:i:s', strtotime($value['updated_at'])) : date('Y-m-d H:i:s', strtotime($value['created_at']));
                    $lead_data[$i]['user_id'] = $value['full_name'];
                    $lead_data[$i]['lead_value'] = $value['deal_value'];
                    $lead_data[$i]['lead_type'] = $value['lead_type'];
                    $lead_data[$i]['status'] = $value['status'];

                    //Cust Loc Data
                    $cust_loc_data[$i]['address'] = $value['address'];
                    $cust_loc_data[$i]['state_id'] = $value['state'];
                    $cust_loc_data[$i]['site_name'] = $value['address'];
                } else {
                    //Cust Data
                    $name = explode(' ', $value['cust_name']);
                    $cust_data[$i]['first_name'] = (isset($name[0])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[0])) : NULL;
                    $cust_data[$i]['last_name'] = (isset($name[1])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[1])) : NULL;
                    $cust_data[$i]['company_name'] = $company_name[] = $value['company_name'];
                    $cust_data[$i]['customer_email'] = NULL;
                    $cust_data[$i]['customer_contact_no'] = NULL;
                    $cust_data[$i]['created_at'] = date('Y-m-d H:i:s', strtotime($value['created_at']));
                    $cust_data[$i]['is_processed'] = 1;
                    //Lead Data
                    $stage_id = 1;
                    $lead_status = 'open';
                    $lead_status = ($value['lead_status'] != '') ? strtolower($value['lead_status']) : $lead_status;
                    if (trim($value['lead_stage']) == 'Prospect') {
                        $stage_id = 1;
                    } else if (trim($value['lead_stage']) == 'Follow Up') {
                        $stage_id = 2;
                    } else if (trim($value['lead_stage']) == 'New Lead') {
                        $stage_id = 3;
                    } else if (trim($value['lead_stage']) == 'Appointment Booked') {
                        $stage_id = 4;
                    } else if (trim($value['lead_stage']) == 'Need Proposal') {
                        $stage_id = 5;
                    } else if (trim($value['lead_stage']) == 'Proposal Rework') {
                        $stage_id = 6;
                    } else if (trim($value['lead_stage']) == 'Proposal Delivered') {
                        $stage_id = 7;
                    } else if (trim($value['lead_stage']) == 'Decision Due') {
                        $stage_id = 8;
                    } else if (trim($value['lead_stage']) == 'Closable Deal') {
                        $stage_id = 9;
                    } else if (trim($value['lead_stage']) == 'Gone Cold') {
                        $stage_id = 10;
                    } else if (trim($value['lead_stage']) == 'Won') {
                        $stage_id = 11;
                        $lead_status = 'won';
                    } else if (trim($value['lead_stage']) == 'Lost') {
                        $stage_id = 12;
                        $lead_status = 'lost';
                    } else if (trim($value['lead_stage']) == 'Proposal Ready') {
                        $stage_id = 13;
                    }

                    $lead_data[$i]['lead_stage'] = $stage_id;
                    $lead_data[$i]['lead_status'] = $lead_status;
                    $lead_data[$i]['created_at'] = date('Y-m-d H:i:s', strtotime($value['created_at']));
                    $lead_data[$i]['updated_at'] = (isset($value['updated_at']) && $value['updated_at'] != NULL && $value['updated_at'] != '') ? date('Y-m-d H:i:s', strtotime($value['updated_at'])) : date('Y-m-d H:i:s', strtotime($value['created_at']));
                    $lead_data[$i]['user_id'] = $value['full_name'];
                    $lead_data[$i]['lead_value'] = $value['deal_value'];
                    $lead_data[$i]['lead_type'] = $value['lead_type'];
                    $lead_data[$i]['status'] = $value['status'];

                    //Csut Loc Data
                    $cust_loc_data[$i]['address'] = $value['address'];
                    $cust_loc_data[$i]['state_id'] = $value['state'];
                    $cust_loc_data[$i]['site_name'] = $value['address'];
                }
                $i++;
            }
            fclose($file);
            //echo "<pre>";
            //print_r($lead_data);die;
            //echo "<pre>";
            //echo count($company_name);
            //echo "<br>";
            //$this->db->select('id');
            //$this->db->from('tbl_customers');
            //$this->db->where_in('company_name',$company_name);
            //$cust_data =  $this->db->get()->result_array();
            //print_r(count($cust_data));die;

            foreach ($cust_data as $key => $value) {

                $is_cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name' => $value['company_name']));

                if (empty($is_cust_data)) {
                    //Tbl Customers
                    $this->common->insert_data('tbl_customers', $value);
                    $cid = $this->common->insert_id();

                    //Tbl Leads
                    $user_data = $this->common->fetch_row('tbl_user_details', '*', array('full_name' => $lead_data[$key]['user_id']));
                    if (!empty($user_data)) {
                        $lead_data[$key]['user_id'] = $user_data['user_id'];
                    } else if (stripos($cust_loc_data[$key]['address'], 'NSW') !== false) {
                        $lead_data[$key]['user_id'] = 7;
                    } else if (stripos($cust_loc_data[$key]['address'], 'New South Wales') !== false) {
                        $lead_data[$key]['user_id'] = 7;
                    } else if (stripos($cust_loc_data[$key]['address'], 'VIC') !== false) {
                        $lead_data[$key]['user_id'] = 5;
                    } else if (stripos($cust_loc_data[$key]['address'], 'Victoria') !== false) {
                        $lead_data[$key]['user_id'] = 5;
                    } else {
                        $lead_data[$key]['user_id'] = 1;
                    }

                    /**
                      if (!empty($user_data)) {
                      $lead_data[$key]['user_id'] = $user_data['user_id'];
                      } else if ($cust_loc_data[$key]['state_id'] == 2) {
                      $lead_data[$key]['user_id'] = 7;
                      } else if ($cust_loc_data[$key]['state_id'] == 7) {
                      $lead_data[$key]['user_id'] = 5;
                      } else {
                      $lead_data[$key]['user_id'] = 1;
                      } */
                    $lead_data[$key]['cust_id'] = $cid;
                    $lead_data[$key]['uuid'] = $this->components->uuid();
                    $this->common->insert_data('tbl_leads', $lead_data[$key]);

                    //Tbl Lead to User
                    $lid = $this->common->insert_id();
                    $ltu = [];
                    $ltu['user_id'] = $lead_data[$key]['user_id'];
                    $ltu['lead_id'] = $lid;
                    $ltu['created_at'] = $lead_data[$key]['created_at'];
                    $this->common->insert_data('tbl_lead_to_user', $ltu);

                    //Tbl Customer Location
                    $cust_loc_data[$key]['cust_id'] = $cid;
                    $postcodes = array();
                    preg_match('/[0-9]{4}/', $cust_loc_data[$key]['address'], $postcodes);
                    if (!empty($postcodes)) {
                        $cust_loc_data[$key]['postcode'] = $postcodes[0];
                    }
                    $this->common->insert_data('tbl_customer_locations', $cust_loc_data[$key]);
                } else {
                    //continue;
                    /**if ($is_cust_data['is_processed']) {
                        //continue;
                    }
                    $update_cust_data = $update_location_data = $update_lead_data = array();
                    //tbl customer data
                    //$update_cust_data['first_name'] = $value['first_name'];
                    //$update_cust_data['last_name'] = $value['last_name'];
                    //$update_cust_data['customer_email'] = $value['customer_email'];
                    $update_cust_data['created_at'] = $value['created_at'];
                    $update_cust_data['is_processed'] = 1;
                    $this->common->update_data('tbl_customers', array('id' => $is_cust_data['id']), $update_cust_data);
                    $update_lead_data['created_at'] = $lead_data[$key]['created_at'];
                    $update_lead_data['updated_at'] = $lead_data[$key]['updated_at'];
                    $this->common->update_data('tbl_leads', array('cust_id' => $is_cust_data['id']), $update_lead_data);
                    //Tbl Lead to User
                    $ldata = $this->common->fetch_row('tbl_leads', '*', array('cust_id' => $is_cust_data['id']));
                    $ltu = [];
                    $ltu['created_at'] = $lead_data[$key]['created_at'];
                    $this->common->update_data('tbl_lead_to_user', array('lead_id' => $ldata['id']), $ltu);
                    continue;

                    //Tbl Customer Location
                    $update_location_data['site_name'] = $cust_loc_data[$key]['address'];


                    /*                     * if (stripos($cust_loc_data[$key]['address'], 'NSW') !== false) {
                      $update_location_data['state_id'] = 2;
                      } else if (stripos($cust_loc_data[$key]['address'], 'New South Wales') !== false) {
                      $update_location_data['state_id'] = 2;
                      } else {
                      $update_location_data['state_id'] = 7;
                      } */

                    /**
                    $update_location_data['state_id'] = $cust_loc_data[$key]['state_id'];
                    $postcodes = array();
                    preg_match('/[0-9]{4}/', $cust_loc_data[$key]['address'], $postcodes);
                    if (!empty($postcodes)) {
                        $update_location_data['postcode'] = $postcodes[0];
                    } else {
                        $update_location_data['postcode'] = 0;
                    }
                    //$this->common->update_data('tbl_customer_locations', array('cust_id' => $is_cust_data['id']), $update_location_data);

                    //tbl lead data
                    $user_data = $this->common->fetch_row('tbl_user_details', '*', array('full_name' => $lead_data[$key]['user_id']));
                    /*                     * if (!empty($user_data)) {
                      $update_lead_data['user_id'] = $user_data['user_id'];
                      } else if (stripos($cust_loc_data[$key]['address'], 'NSW') !== false) {
                      $update_lead_data['user_id'] = 7;
                      } else if (stripos($cust_loc_data[$key]['address'], 'New South Wales') !== false) {
                      $update_lead_data['user_id'] = 7;
                      } else if (stripos($cust_loc_data[$key]['address'], 'VIC') !== false) {
                      $update_lead_data['user_id'] = 5;
                      } else if (stripos($cust_loc_data[$key]['address'], 'Victoria') !== false) {
                      $update_lead_data['user_id'] = 5;
                      } else {
                      $update_lead_data['user_id'] = 1;
                      } */

                    $user_data = $this->common->fetch_row('tbl_user_details', '*', array('full_name' => $lead_data[$key]['user_id']));
                    if (!empty($user_data)) {
                        $update_lead_data['user_id'] = $user_data['user_id'];
                    } else if ($cust_loc_data[$key]['state_id'] == 2) {
                        $update_lead_data['user_id'] = 7;
                    } else if ($cust_loc_data[$key]['state_id'] == 7) {
                        $update_lead_data['user_id'] = 5;
                    } else {
                        $update_lead_data['user_id'] = 1;
                    }

                    //$update_lead_data['status'] = $lead_data[$key]['status'];
                    //$update_lead_data['lead_stage'] = $lead_data[$key]['lead_stage'];
                    //$update_lead_data['lead_status'] = $lead_data[$key]['lead_status'];
                    //$update_lead_data['lead_type'] = $lead_data[$key]['lead_type'];
                    //$update_lead_data['lead_value'] = $lead_data[$key]['lead_value']; 
                    //$this->common->update_data('tbl_leads', array('cust_id' => $is_cust_data['id']), $update_lead_data);

                    //Tbl Lead to User
                    $ldata = $this->common->fetch_row('tbl_leads', '*', array('cust_id' => $is_cust_data['id']));
                    $ltu = [];
                    $ltu['user_id'] = $update_lead_data['user_id'];
                    //$ltu['created_at'] = $lead_data[$key]['created_at'];
                    $this->common->update_data('tbl_lead_to_user', array('lead_id' => $ldata['id']), $ltu);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function delete_leads_by_company_name()
    {
        $names = 'Aluminium Profiles Australia;Scott\'s Transport Group;Peerless Automotive & Auto Electrics 387878711;Nick Scali Furniture Distribution Centre;Bradnam\'s Windows & Doors;M1 Traffic Control Pty. Ltd;Yusen Logistics;33-35 edwards street, Lower Plenty;Joy Dennehy;1/5 Cudgee Court, Ashwood;43 Filbert Street, Caulfield South;9 Ashwood Avenue, Highett, Victoria, Australia;Melbourne, 48 Pascoe Street, Glen Iris, Victoria, Australia;12 Alberfeldie Dr, Truganina VIC 3029;14 Wing Circuit, Tarneit, Victoria, Australia;9 Cardigan Cres Taylor Lakes, VIC;26 Maracana Circuit Melton South VIC;4 Lachlan Ct, Werribee;A&L WINDOWS;39 Black Forest Rd, werribee;2 Woodside Ct, WERRIBEE;85 Synnot St, Werribee;18 Black Forest Rd, Werribee;10 John St, Werribee;31 Coventry Dr, Werribee;105 Purchas St, Werribee;6 Wolsely Cl, Werribee;58 Hotham Rd, Niddrie VIC 3042, Australia;25 Almond Cl, Werribee;1 Broken Ct, Werribee;13 Brolga Way, Lara 3212;12 Strathford Ct, Werribee;14 Nangiloc Cres, Werribee;14 Retreat Pl, Werribee;22 Selbourne Ave, Werribee;16 Albatross Ave, Werribee;13 Thompson Ct, Werribee;76 Edwards Rd, Werribee;3 Kestrel Pl, Werribee;2 Derwent Rd, Werribee;;23 Sinns Ave, Werribee;27 Selbourne Ave, Werribee;;Hardy Signs;11 Gallus Close Vermont;Cellarack Wine Racks;9 Margaret St, Werribee;1 Boyd Ct, Werribee;3 Strathford Ct, Werribee;312 Tarneit Rd, Werribee;3 Mont-Alto Pl, Werribee;18 Elizabeth Ave, Werribee;29 Blossom Lane, Werribee;43 Egan Cl, Werribee;19 Davern Ct, Werribee;26 Yarra St, Werribee;17 St Lawrence Cl, Werribee;6 STEVENAGE CRESCENT, Deer Park 3023;1 BLAKE COURT, TRUGANINA 3029;1 Oriole Dr, Werribee;25 Rowes Rd, Werribee;35 Stewart Dr, Werribee;26 Elizabeth St, Moe VIC 3825;84 HATCHLANDS DRIVE Deer Park, 3023;581 SAYERS ROAD, Hoppers Crossing  3029;12 ORLANDO DRIVE, TRUGANINA 3029;2 CHATEAU CLOSE Hoppers Crossing 3029;18 THORPE COURT, ALTONA MEADOWS 3028;25 LUSH COURT, ALTONA MEADOWS 3028;3 BYLAN STREET, TARNEIT 3029;65 Rosella Ave, Werribee;25 Nangiloc Cres, Werribee;9 Riverview Dr, Werribee;116 Thames Bvd, Werribee;24a Bateman Street Hampton, VIC;538 Bluff Road, Hampton, Victoria, Australia;DMCentral;;;MKC Constructions;3 Vantage place, truginina;64 Jerilderie Drive;Jason Snowball- 6 Okeefe Place;;Maria Faranda- 70 Bulban Road;Neil Wakefield - 49 Georgia Cres;Clifford Ralston -7 Newbury St;Satuma Ibrahim -125 Thames Bvd;Mausuf Shrestha - 13 Tamarind Cres;38 Tamarind Cres;Evelyn Agosta - 61 Church St;Marco Rocci - 5 Beldale Ave;Carolyn Whiteway - 36 Tracey St;Bianca Pante - 6 De Garis Pl;10 Laurina Avenue -Rashid Islam;3/28 Commercial Road;Australian wood creations- 15 Kepler Circuit;Trusswall- 21 Progress street;Mitchcon - 2/13 Downard Street;Inkspot - 34A Percy street;PFERD - 1-3 Conifer crescent;Darley Aluminium;Baronia Mechanical Services;Melded Fabrics;Australian wood creations- 15 Kepler Circuit;Doogood;Morgano plastics;VIC AIR SUPPLIES PTY LTD;Pacific power engineering;Valiant Spares;Temptation corner;15 Waterloo Street, Brighton;SelctTile import;sunlite glass pty;Track repairs pty ltd;Texstyle;AustralianSurface Treatments;Ironman vic pty ltd;Regal Caravans;Turkish bread;Anna and Anthony Stathopoulos;Clean Air Filtration;STRIKE Shopfitting;Titanium Caravans PTY LTD;NOVARE Fitouts;Coremetrics;Overall panels;FCL Transport services;ECOECO windows;Hazard Solutions;Galaxy products;3C Heat St, Sandringham;UpstreamSolutions;blue scope;Kensho;Patterson Cheney;Style ergonomics Australia';
        $company_name = explode(';', $names);
        //echo "<pre>";
        //print_r($company_name);
        //echo "<br>";
        $this->db->select('id');
        $this->db->from('tbl_customers');
        $this->db->where_in('company_name', $company_name);
        $cust_data = $this->db->get()->result_array();
        //echo $this->db->last_query();die;
        $cust_ids = array_column($cust_data, 'id');
        print_r($cust_ids);
        die;
        $lead_ids = $this->common->fetch_where_in('tbl_leads', 'id', 'cust_id', $cust_ids);
        $lead_ids = array_column($lead_ids, 'id');
        $led_proposal_ids = $this->common->fetch_where_in('tbl_led_proposal', 'id', 'lead_id', $lead_ids);
        $led_proposal_ids = array_column($led_proposal_ids, 'id');
        $solar_proposal_ids = $this->common->fetch_where_in('tbl_solar_proposal', 'id', 'lead_id', $lead_ids);
        $solar_proposal_ids = array_column($solar_proposal_ids, 'id');
        print_r($lead_ids);
        die;
        //DELETE ACTIVITY
        $this->common->delete_batch('tbl_activities', 'lead_id', $lead_ids);
        $this->db->delete('tbl_activities');

        if (!empty($led_proposal_ids)) {
            //DELTE LED PROPOSAL
            $this->common->delete_batch('tbl_led_proposal_products', 'proposal_id', $led_proposal_ids);
            $this->common->delete_batch('tbl_led_proposal_access_equipments', 'proposal_id', $led_proposal_ids);
            $this->common->delete_batch('tbl_led_proposal', 'id', $led_proposal_ids);
        }
        if (!empty($solar_proposal_ids)) {
            //DELTE SOLAR PROPOSAL
            $this->common->delete_batch('tbl_solar_proposal_panel_data', 'proposal_id', $solar_proposal_ids);
            $this->common->delete_batch('tbl_solar_proposal_products', 'proposal_id', $solar_proposal_ids);
            $this->common->delete_batch('tbl_solar_proposal', 'id', $solar_proposal_ids);
        }
        //DELETE LEAD
        $this->common->delete_batch('tbl_lead_to_user', 'lead_id', $lead_ids);
        $this->common->delete_batch('tbl_leads', 'id', $lead_ids);
        //DELETE CUSTOMER
        $this->common->delete_batch('tbl_customer_locations', 'cust_id', $cust_ids);
        $this->common->delete_batch('tbl_customer_contacts', 'cust_id', $cust_ids);
        $this->common->delete_batch('tbl_customers', 'id', $cust_ids);
    }

    public function save_lead_allocation()
    {
        $data = array();

        $lead_allocation_data = $this->input->post('lead_allocation');
        $follow = $this->input->post('follow');

        $status = 'Looks like something went wrong';
        if (!empty($lead_allocation_data)) {
            $lead_id = $lead_allocation_data['lead_id'];
            $assigned_to = $lead_allocation_data['user_id'];
            if ($follow == 'true') {
                $lead_user_data = $this->common->fetch_where('tbl_lead_to_user', '*', ['lead_id' => $lead_id]);
                $stat = TRUE;
                //Now we need to send SMS and EMAIL to appropirate Parties
                if (!empty($lead_user_data)) {
                    $notification_data = [];
                    $notification_data['id'] = $lead_id;
                    if ($self_lead == false) {
                        $this->notify->store_notification('LEAD_FOLLOW_UP', $notification_data);
                    }
                    $status = 'Sales Rep Follow-Up Send Successfully';
                } else {
                    $stat = FALSE;
                    $status = 'Looks like no sales rep is allocated to this lead. Please allocate first and then save.';
                }
            } else {
                unset($lead_allocation_data['user_id']);
                $is_exists_lead_allocation_data = $this->common->fetch_row('tbl_lead_allocations', 'id', array('lead_id' => $lead_id));
                if (!empty($is_exists_lead_allocation_data)) {
                    //Update Leads
                    $this->common->update_data('tbl_leads', array('id' => $lead_id), array('user_id' => $this->aauth->get_user_id()));

                    //Update Lead Allocation Data and Sales Rep
                    $lead_allocation_data['updated_at'] = date('Y-m-d H:i:s');
                    $lead_allocation_data['created_by'] = $this->aauth->get_user_id();
                    $lead_allocation_data['assigned_to'] = $assigned_to;
                    //Escape and Sanitize data before update
                    $lead_allocation_data = $this->components->escape_data($lead_allocation_data);
                    $stat = $this->common->update_data('tbl_lead_allocations', array('lead_id' => $lead_id), $lead_allocation_data);
                    $is_exists_ltu = $this->common->fetch_row('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
                    if (!empty($is_exists_ltu)) {
                        //Update Lead To User if Old Assigned Rep is Different
                        if ($is_exists_ltu['user_id'] != $assigned_to) {
                            $lead_user_update_data = array();
                            $lead_user_update_data['user_id'] = $assigned_to;
                            $stat = $this->common->update_data('tbl_lead_to_user', array('lead_id' => $lead_id), $lead_user_update_data);
                        }
                    } else {
                        //Insert Lead To User
                        $lead_user_insert_data = array();
                        $lead_user_insert_data['lead_id'] = $lead_id;
                        $lead_user_insert_data['user_id'] = $assigned_to;
                        $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
                    }
                } else {
                    //First Update Lead Stage and Lead Source
                    $this->common->update_data(
                        'tbl_leads',
                        array('id' => $lead_id),
                        array(
                            'lead_source' => 'Call Centre Lead',
                            'lead_stage' => 4,
                            'user_id' => $this->aauth->get_user_id()
                        )
                    );

                    //Insert Lead Allocation Data and Sales Rep
                    $lead_allocation_data['created_by'] = $this->aauth->get_user_id();
                    $lead_allocation_data['assigned_to'] = $assigned_to;
                    //Escape and Sanitize data before insert
                    $lead_allocation_data = $this->components->escape_data($lead_allocation_data);
                    $this->common->insert_data('tbl_lead_allocations', $lead_allocation_data);
                    $lead_user_insert_data = array();
                    $lead_user_insert_data['lead_id'] = $lead_id;
                    $lead_user_insert_data['user_id'] = $assigned_to;
                    $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
                }
                $status = 'Sales Rep assigned successfully';
            }

            if ($stat) {
                if (CACHING) {
                    //Delete Cached Leads
                    $this->db->cache_delete('admin', 'lead');
                }
                $data['success'] = TRUE;
                $data['status'] = $status;
            } else {
                $data['success'] = FALSE;
                $data['status'] = $status;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function anaual_solar_data_migrate()
    {
        try {
            //$filename = __DIR__ . './../../deals.csv';
            $filename = __DIR__ . './../../asd.csv';
            $file = fopen($filename, "r");
            ini_set('auto_detect_line_endings', true);
            $count = $c1 = 0;
            $columns = $combined_data = $all_data = $all_data1 = $combined_data1 = array();

            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                if ($count == 0) {
                    $columns = ['melbourne', 'sydney', 'canberra', 'darwin', 'brisbane', 'perth', 'adelaide', 'hobart', 'dateAndtime', 'days', '1rate', '2rate', '3rate'];
                    //print_r($columns);die;
                } else {
                    //print_r($getData);die;
                    for ($i = 0; $i < count($getData); $i++) {
                        if ($i == 8) {
                            $rows[$i] = date('d/m/Y H:i', strtotime($getData[$i]));
                        } else {
                            $rows[$i] = $getData[$i];
                        }
                    }
                    $combined_data = array_combine($columns, $rows);
                    $all_data[($c1)] = $combined_data;
                    $c1++;
                }
                $count++;
                if ($count == 8761) {
                    break;
                }
            }
            $this->common->insert_batch('tbl_annual_solar_data_1', $all_data);
            fclose($file);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function lead_owner_migrate()
    {
        $filters = ' AND ltu.user_id IS NULL';
        $leads = $this->lead->get_leads_data_by_stage(FALSE, $filters, FALSE, FALSE, FALSE, FALSE);
        //echo count($leads);die;
        $update_lead_data = array();
        foreach ($leads as $key => $value) {
            $update_lead_data['lead_id'] = $value['id'];
            if ($value['customer_state'] != '') {
                if (stripos($value['customer_state'], 'NSW') !== false) {
                    $update_lead_data['user_id'] = 7;
                } else if (stripos($value['customer_state'], 'New South Wales') !== false) {
                    $update_lead_data['user_id'] = 7;
                } else {
                    $update_lead_data['user_id'] = 5;
                }
            } else {
                if (stripos($value['customer_address'], 'NSW') !== false) {
                    $update_lead_data['user_id'] = 7;
                } else if (stripos($value['customer_address'], 'New South Wales') !== false) {
                    $update_lead_data['user_id'] = 7;
                } else {
                    $update_lead_data['user_id'] = 5;
                }
            }
            $this->common->insert_or_update('tbl_lead_to_user', array('lead_id' => $value['id']), $update_lead_data);
        }
    }

    public function fetch_lead_files()
    {
        $data = array();
        $lead_uuid = $this->input->get('uuid');
        if (!empty($this->input->get())) {
            $lead_data = $this->common->fetch_row('tbl_leads', 'cust_id', array('uuid' => $lead_uuid));
            $pipedrive_files = $this->lead->get_pipedrive_files($lead_data['cust_id']);
            foreach ($pipedrive_files as $key => $value) {
                $pipedrive_files[$key]['file_data'] = json_decode($pipedrive_files[$key]['file_data']);
            }
            $data['pipedrive_files'] = $pipedrive_files;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function manual_triggers()
    {
        $sql = "SELECT la.created_at,la.`lead_id`,la.`created_by`,la.`assigned_to`,lead.user_id as lead_user,ltu.user_id FROM tbl_lead_allocations as la LEFT JOIN tbl_leads as lead ON lead.id = la.lead_id LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id WHERE DATE_FORMAT(la.created_at,'%m') = '08'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            if ($value['created_by'] != $value['lead_user']) {
                $this->common->update_data('tbl_leads', array('id' => $value['lead_id']), array('user_id' => $value['created_by']));
            }
        }
    }

    public function send_mail()
    {
        $this->load->library("Email_Manager");
        $this->email_manager->send_test_mail();
    }

    public function check_job_exisits_for_lead()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $data['led_booking_form_data'] = $this->common->fetch_where('tbl_led_booking_form', '*', array('lead_id' => $lead_id));
            $data['solar_booking_form_data'] = $this->common->fetch_where('tbl_solar_booking_form', '*', array('lead_id' => $lead_id));
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
        die;
    }

    public function save_proposal_name()
    {
        $data = array();
        $proposal_uuid = $this->input->post('proposal_uuid');
        $proposal_name = $this->input->post('proposal_name');
        if (!empty($this->input->post())) {

            $stat = $this->common->update_data('tbl_solar_proposal', array('proposal_uuid' => $proposal_uuid), array('proposal_name' => $proposal_name));

            if ($stat) {
                $data['status'] = 'Proposal Name Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    public function createDuplicateProposal(){
        if($this->input->post('uuid')){
            $solarProposalRow = $this->common->fetch_row('tbl_solar_proposal',"*, 'Commercial Solar Proposal' as type",array('proposal_uuid'=>$this->input->post('uuid')));
            if(count($solarProposalRow)>0){

                $solarProposalRow['proposal_name'] = (empty($solarProposalRow['proposal_name']))?$solarProposalRow['type'].' Duplicate':$solarProposalRow['proposal_name'].' Duplicate';
                $solarProposalRow['proposal_uuid'] = $this->input->post('newUuid');
                unset($solarProposalRow['created_at']);
                unset($solarProposalRow['updated_at']);
                unset($solarProposalRow['email_status']);
                unset($solarProposalRow['type']);
                $proposalFinance = $this->common->fetch_row('tbl_proposal_finance','*',array('proposal_id'=>$solarProposalRow['id']));
                unset($solarProposalRow['id']);
                $this->common->insert_data('tbl_solar_proposal',$solarProposalRow);
                $proposalFinanceSummary = $this->common->fetch_row('tbl_solar_proposal_financial_summary','*',array('proposal_id'=>$solarProposalRow['id']));

                $proposal_id = $this->common->insert_id();
                if(count($proposalFinance) > 0) {
                    $proposalFinance['proposal_id'] = $proposal_id;
                    unset($proposalFinance['created_at']);
                    unset($proposalFinance['updated_at']);
                    unset($proposalFinance['pf_id']);
                    $this->common->insert_data('tbl_proposal_finance', $proposalFinance);
                }
                if(count($proposalFinanceSummary)>0){
                    $proposalFinanceSummary['proposal_id'] = $proposal_id;
                    unset($proposalFinanceSummary['id']);
                    $this->common->insert_data('tbl_solar_proposal_financial_summary', $proposalFinanceSummary);
                }

                $solarProposalData = $this->proposal->get_solar_proposal_data(FALSE, FALSE, $proposal_id, FALSE);
                $data['proposal_data'] = $solarProposalData;
                $data['status'] = 'Duplicate Proposal Created Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Please Select Valid Proposal';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
}

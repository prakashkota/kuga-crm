<?php

class Tender_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    /**
     * Fetch Leads List
     * @param string $uuid
     * @param int $id
     * @param int $site_id
     * @return array
     */
    function get_tenders($tenderId = FALSE) {
        $sql = "SELECT * from tbl_tenders ";

        if ($tenderId) {
            $sql .= " WHERE id='" . $tenderId . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }


    function get_tender_leads($id){
        $sql = "SELECT leads.* from tbl_tender_leads as leads";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = leads.created_by";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = leads.state_id";

        if ($uuid) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }


    function get_tender_data_by_status_count_for_pipeline($statusId = FALSE,$filters=false) {



        $cond = "";
        if ($statusId) {
            $cond .= " AND tbl_tender.status = '{$statusId}'";
        }



        $sql = "SELECT count(tbl_tender.id) as num_rows FROM tbl_tender";


        if ($cond != '') {
            $sql .= " WHERE 1=1 " . $cond ;
        } else if ($cond == '' ) {
            $sql .= " WHERE 1=1" ;
        }

        if($filters){
            $sql .= $filters;
        }

         $sql .= " ORDER BY tbl_tender.created_at DESC";

        $query = $this->db->query($sql);
        return $query->result_array()[0]['num_rows'];
    }

    function get_tender_data_by_status_for_pipeline($status_id = FALSE, $filters = '',$having = FALSE,$offset = FALSE, $limit = FALSE , $is_filter = FALSE, $user_id = FALSE) {

        $filter = $this->input->get('filter');
        $cond = "";
        if ($status_id) {
            $cond .= " AND status = $status_id";
        }
        $sql = " SELECT * FROM tbl_tender ";
        if ($cond != '' && $filters != '') {
            $sql .= " WHERE 1=1 " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        } else if ($cond != '' && $filters == '') {
            $sql .= " WHERE 1=1 " . $cond;
        }
        if($limit == FALSE){
            $sql .= "  $having ORDER BY created_at ASC, name ASC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= "  $having ORDER BY created_at desc, name ASC LIMIT $offset,$limit";
        }else{
            $sql .= " $having ORDER BY created_at desc, name ASC LIMIT $offset,$limit";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function get_quote_by_uuid($uuid = FALSE){

        $sql = "SELECT ttq.*, ttl.address, ttl.postcode FROM tbl_tender_quotes as ttq";
        $sql .= " LEFT JOIN tbl_tender_leads as ttl ON ttq.lead_id = ttl.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ttl.created_by";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ttl.state_id";
        $sql .= " WHERE ttq.uuid='" . $uuid . "'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }


    function get_lead_booking_forms_data($lead_id){

        $type_name = "IF(sbf.type_id = 1,'0 - 39kW',IF(sbf.type_id = 2,'40kW - 100kW',IF(sbf.type_id = 3,'100kW+','Residential 30kW')))";
        $sql2 = "SELECT sbf.uuid,sbf.created_at,sbf.booking_form,sbf.business_details,sbf.kuga_job_id,sbf.pdf_file,sbf.pdf_file_size,'SOLAR' as type,$type_name as type_name"
            . " FROM tbl_tender_solar_booking_form as sbf";
        $sql2 .= " WHERE sbf.lead_id = $lead_id";

        $query = $this->db->query($sql2);
        return $query->result_array();
    }
    
}

<?php

class TenderLead_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Fetch Leads List
     * @param string $uuid
     * @param int $id
     * @param int $site_id
     * @return array
     */
    function get_leads($uuid = FALSE) {
        $sql = "SELECT leads.*,ud.user_id,ud.simpro_user_id,ud.full_name,ud.company_name,ud.company_contact_no from tbl_tender_leads as leads";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = leads.user_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = leads.state_id";

        if ($uuid) {
            $sql .= " WHERE leads.uuid='" . $uuid . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }
    

    function get_tender_leads($id){
        $sql = "SELECT leads.* from tbl_tender_leads as leads";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = leads.created_by";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = leads.state_id";

        if ($uuid) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }


    function get_leads_data_by_stage_count_for_pipeline($stage_id = FALSE, $filters = '',$having = FALSE) {

        $filter = $this->input->get('filter');

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        
        if($filters == ''){
            $sql = "SELECT leads.id  FROM tbl_tender_leads as leads";
        }else{
            $sql = "SELECT leads.*,"
                . "ls.stage_name as deal_stage_name"
                . " FROM tbl_tender_leads as leads";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = leads.created_by";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = leads.state_id";
            $sql .= " LEFT JOIN tbl_tender_lead_stages as ls ON leads.lead_stage = ls.id";
        }

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }else if ($cond != '' && $filters == '') {
            $sql .= " WHERE " . $cond;
        }
        
        $sql .= " GROUP BY leads.id $having ORDER BY leads.closing_date DESC";

        //echo $sql;die;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    function get_leads_data_by_stage_for_pipeline($stage_id = FALSE, $filters = '',$having = FALSE,$offset = FALSE, $limit = FALSE , $is_filter = FALSE, $user_id = FALSE) {

        $filter = $this->input->get('filter');

        $cond = "";
        
        if($user_id){
            $cond .= "leads.created_by = $user_id";
        }
        
        if ($stage_id) {
            $cond .= " AND leads.lead_stage = $stage_id";
        }

        if ($is_filter == FALSE && $limit != FALSE) {
           $from_select = "(SELECT * FROM tbl_tender_leads ORDER BY closing_date ASC,project_name ASC LIMIT $offset,$limit) as leads";     
        }else{
           $from_select = "(SELECT * FROM tbl_tender_leads ORDER BY closing_date ASC,project_name ASC) as leads";     
        }
        
                       
        $sql = "SELECT leads.*,"
                . "ls.stage_name as deal_stage_name"
                . " FROM $from_select";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = leads.created_by";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = leads.state_id";
        $sql .= " LEFT JOIN tbl_tender_lead_stages as ls ON leads.lead_stage = ls.id";
        

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        } else if ($cond != '' && $filters == '') {
            $sql .= " WHERE " . $cond;
        }
        
        if($limit == FALSE){
           $sql .= " GROUP BY leads.id $having ORDER BY leads.closing_date ASC,leads.project_name ASC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= " GROUP BY leads.id $having ORDER BY leads.closing_date ASC,leads.project_name ASC";
        }else{
           $sql .= " GROUP BY leads.id $having ORDER BY leads.closing_date ASC,leads.project_name ASC LIMIT $offset,$limit";  
        }
        
        
        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    


    function get_quote_by_uuid($uuid = FALSE){

        $sql = "SELECT ttq.*, ttl.address, ttl.postcode FROM tbl_tender_quotes as ttq";
        $sql .= " LEFT JOIN tbl_tender_leads as ttl ON ttq.lead_id = ttl.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ttl.created_by";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ttl.state_id";
        $sql .= " WHERE ttq.uuid='" . $uuid . "'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    
    function get_lead_booking_forms_data($lead_id){

        $type_name = "IF(sbf.type_id = 1,'0 - 39kW',IF(sbf.type_id = 2,'40kW - 100kW',IF(sbf.type_id = 3,'100kW+','Residential 30kW')))";
        $sql2 = "SELECT sbf.uuid,sbf.created_at,sbf.booking_form,sbf.business_details,sbf.kuga_job_id,sbf.pdf_file,sbf.pdf_file_size,'SOLAR' as type,$type_name as type_name"
                . " FROM tbl_tender_solar_booking_form as sbf";
        $sql2 .= " WHERE sbf.lead_id = $lead_id";

        $query = $this->db->query($sql2);
        return $query->result_array();
    }
}
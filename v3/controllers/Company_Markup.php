<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property CompanyMarkup Controller
 * @version 1.0
 */
class Company_Markup extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("CompanyMarkup_Model", "company_markup");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function manage() {
        $data = array();
        //Restricitng and Fetching of Data based on user logged In group
        $group = $this->input->get('group');
        if (isset($group) && $group != '') {
            $data['meta_title'] = 'Manage Franchise Company Markup';
            if (!$this->aauth->is_member($group) && !$this->aauth->is_member('Admin')) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
                redirect($this->redirect_url);
            }
            if ($this->aauth->is_member('Admin')) {
                $company_markups = $this->company_markup->get_markup_list_by_roles($group);
            } else {
                $company_markups = $this->company_markup->get_markup_list_by_roles($group, $this->aauth->get_user()->id);
            }
        } else {
            $data['meta_title'] = 'Manage Company Markup';
            if ($this->aauth->is_member('Admin')) {
                $company_markups = $this->company_markup->get_markup_list_by_roles(1);
            } else if (!$this->aauth->is_member('Admin')) {
                $group_id = $this->aauth->get_user_groups()[0]->group_id;
                $company_markups = $this->company_markup->get_markup_list_by_roles($group_id, $this->aauth->get_user()->id);
            }
        }

        $data['company_markups'] = $company_markups;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('company_markup/manage');
        $this->load->view('partials/footer');
    }

    public function add() {

        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('state', 'State', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                unset($insert_data['company_markup_data']);
                $insert_data['user_id'] = $this->aauth->get_user()->id;
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                //Insert Line Item
                $this->common->insert_data('tbl_company_markup', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $company_markup_insert_data = array();
                    $company_markup_data = $this->input->post('company_markup_data');
                    if (!empty($company_markup_data)) {
                        foreach ($company_markup_data['qty_from'] as $key => $value) {
                            $company_markup_insert_data[$key]['company_markup_id'] = $id;
                            $company_markup_insert_data[$key]['qty_from'] = $company_markup_data['qty_from'][$key];
                            $company_markup_insert_data[$key]['qty_to'] = $company_markup_data['qty_to'][$key];
                            $company_markup_insert_data[$key]['price'] = $company_markup_data['price'][$key];
                            $company_markup_insert_data[$key]['created_at'] = date('Y-m-d H:i:s');
                        }
                        $this->common->insert_batch('tbl_company_markup_data', $company_markup_insert_data);
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Company Markup created successfuly.</div>');
                    redirect('admin/company-markup/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the company markup. </div>');
                }
            } else {
                $data['company_markup_data'] = $this->input->post('company_markup_data');
                $data['state'] = $this->input->post('state');
                $data['comments'] = $this->input->post('comments');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('company_markup/add');
        $this->load->view('partials/footer');
    }

    public function edit($id) {
        //Restricitng and Fetching of Data based on user logged In group   
        $company_markup_data = $this->common->fetch_where('tbl_company_markup', '*', array('id' => $id));

        if (empty($company_markup_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Line Item found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        if ($this->aauth->get_user()->id != $company_markup_data[0]['user_id'] && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }


        $data = array();
        $cmd_data = array();

        if (!empty($company_markup_data)) {
            $data['state'] = $company_markup_data[0]['state'];
            $data['comments'] = $company_markup_data[0]['comments'];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('state', 'State', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                unset($update_data['company_markup_data']);
                $update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Line Item
                $this->common->update_data('tbl_company_markup', ['id' => $id], $update_data);

                if ($id) {
                    $company_markup_update_data = array();
                    $company_markup_data = $this->input->post('company_markup_data');
                    //Bulk Update Data
                    $cmd_item_data = $this->common->fetch_where('tbl_company_markup_data', '*', ['company_markup_id' => $id]);
                    $count = count($cmd_item_data);
                    if ($count > 0) {
                        for ($i = 0; $i < $count; $i++) {
                            $company_markup_update_data[$i]['qty_from'] = $company_markup_data['qty_from'][$i];
                            $company_markup_update_data[$i]['qty_to'] = $company_markup_data['qty_to'][$i];
                            $company_markup_update_data[$i]['price'] = $company_markup_data['price'][$i];
                            $company_markup_update_data_cond[$i]['id'] = $cmd_item_data[$i]['id'];
                            $this->common->update_data('tbl_company_markup_data', $company_markup_update_data_cond[$i], $company_markup_update_data[$i]);
                        }
                    }

                    //Bulk Insert Data
                    for ($j = $count, $k = 0; $j < count($company_markup_data['qty_from']); $j++, $k++) {
                        $company_markup_insert_data[$k]['company_markup_id'] = $id;
                        $company_markup_insert_data[$k]['qty_from'] = $company_markup_data['qty_from'][$j];
                        $company_markup_insert_data[$k]['qty_to'] = $company_markup_data['qty_to'][$j];
                        $company_markup_insert_data[$k]['price'] = $company_markup_data['price'][$j];
                        $stat = $this->common->insert_data('tbl_company_markup_data', $company_markup_insert_data[$k]);
                    }

                    //Check if delete Required on less item data thean already stored
                    if ($count > count($company_markup_data['qty_from'])) {
                        for ($l = count($company_markup_data['qty_from']), $m = 0; $l < $count; $l++, $m++) {
                            $company_markup_cond[$m]['id'] = $cmd_item_data[$l]['id'];
                            $this->common->delete_data('tbl_company_markup_data', $company_markup_cond[$m]);
                        }
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Company Markup updated successfuly.</div>');
                    redirect('admin/company-markup/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['company_markup_data'] = $this->input->post('company_markup_data');
                $data['state'] = $this->input->post('state');
                $data['comments'] = $this->input->post('comments');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['lid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('company_markup/add');
        $this->load->view('partials/footer');
    }

    public function fetch_comapny_markup_data() {
        $data = array();
        $data['success'] = FALSE;
        $id = $this->input->get('company_markup_id');
        if (isset($id) && $id != '') {
            $company_markup_data = $this->common->fetch_where('tbl_company_markup_data', '*', array('company_markup_id' => $id));
            $data['company_markup_data'] = $company_markup_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

}

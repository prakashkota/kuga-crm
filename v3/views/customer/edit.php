<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; height:100%;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Edit Customer</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div id="message"></div>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Customer Details</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="customer_details[first_name]" id="firstname"  class="form-control" value="<?php echo (!empty($customer_details)) ? $customer_details['first_name'] : ''; ?>" placeholder="First Name"  >
                                                <?php echo form_error('customer_details[first_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Last Name</label>
                                                <input type="text" name="customer_details[last_name]" id="lastname"  class="form-control" value="<?php echo (!empty($customer_details)) ? $customer_details['last_name'] : ''; ?>" placeholder="Last Name"  >
                                                <?php echo form_error('customer_details[last_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="text" name="customer_details[customer_email]" id="customer_email" class="form-control" value="<?php echo (!empty($customer_details)) ? $customer_details['customer_email'] : ''; ?>" placeholder="Email"  >
                                                <?php echo form_error('customer_details[customer_email]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Mobile No.</label>
                                                <input type="text" name="customer_details[customer_contact_no]" id="customer_contact_no"  class="form-control" value="<?php echo (!empty($customer_details)) ? $customer_details['customer_contact_no'] : ''; ?>" placeholder="Mobile No."  >
                                                <?php echo form_error('customer_details[customer_contact_no]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <select name="customer_details[title]" class="form-control"  required>
                                                    <?php
                                                    $cust_title = unserialize(cust_title);
                                                    foreach ($cust_title as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value; ?>" <?php if($customer_details['title'] == $value){ ?> selected="" <?php } ?>><?php echo $value; ?></option>   
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('customer_details[title]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Bussiness/Franchise</label>
                                                <select name="customer_details[admin_id]" id="businessId" class="form-control"  required>
                                                     <option value="">Enter Customer Postcode First</option>
                                                </select>
                                                <?php echo form_error('customer_details[admin_id]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Location Details</h4>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label class="control-label">Address</label>
                                                <select class="select2 placecomplete form-control" id="locationstreetAddress"  style="height:48px !important;"></select>
                                                <input type="hidden" id="locationstreetAddressVal" name="customer_locations[address]" value="<?php echo (!empty($customer_locations)) ? $customer_locations['address'] : ''; ?>" />
                                                <?php echo form_error('customer_locations[address]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">State</label>
                                                <select name="customer_locations[state_id]" id="locationState" class="form-control" required>
                                                    <option value="">Select State</option>
                                                    <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                        <option value="<?php echo $states[$j]['state_id']; ?>" <?php if (!empty($customer_locations) && $customer_locations['state_id'] == $states[$j]['state_id']) { ?> selected="" <?php } ?>><?php echo $states[$j]['state_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('customer_locations[state_id]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">Post Code</label>
                                                <input class="form-control" placeholder="Please Enter Post Code" type="text" name="customer_locations[postcode]" value="<?php echo (!empty($customer_locations)) ? $customer_locations['postcode'] : ''; ?>" id="locationPostCode" value=""  required />
                                                <?php echo form_error('customer_locations[postcode]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="buttwrap">
                        <input type="submit"  id="add_customer_btn" class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing"></script>

<script>
    $(document).ready(function () {

        $('.placecomplete').placecomplete({
            placeServiceResult: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                    }
                                }
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                                 $('#locationPostCode').trigger('change');
                            }
                        }
                    }
                }
            },
            language: 'fr'
        });

    });

    (function ($) {

        if (typeof $.fn.select2 == 'undefined') {
            //alert("ERROR: Placecomplete need Select2 plugin.");
        }
        setTimeout(function () {
            var location_address = "<?php echo (!empty($customer_locations)) ? $customer_locations['address'] : ''; ?>";
            if (location_address != '') {
                $('#select2-locationstreetAddress-container').html(location_address);
            }
        }, 2000);


        //Google services
        var ac = null; //Autocomplete
        var ps = null; //Place

        //Google config
        // https://developers.google.com/maps/documentation/javascript/reference#AutocompletionRequest
        var googleAutocompleteOptions = {
            types: ['establishment', 'geocode'],
            componentRestrictions: {country: 'au'},
        };

        //Google init
        window.initGoogleMapsAPI = function () {
            ac = new google.maps.places.AutocompleteService();
            ps = new google.maps.places.PlacesService($('<div/>')[0]); //Google need a mandatory element to pass html result , we do not need this.

        }

        //Google Loading
        if (window.google && google.maps && google.maps.places) {
            window.initGoogleMapsAPI();
        } else {
            $.ajax({
                url: "https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing",
                dataType: "script",
                cache: true
            });
        }

        //Google placeservice result map

        var placeServiceResult = function (data, status) {
            var CIVIC = 0, STREET = 1, CITY = 2, SECTEUR = 3, STATE = 4, COUNTRY = 5, ZIPCODE = 6;
            //todo If the result does not have 7 element data map is not the same
            //maybe we will need that html element google put mandatory
            var adrc = data.address_components;
            if (adrc.length != 7)
                return;

            var address = adrc[CIVIC].long_name + ',' + adrc[STREET].long_name + adrc[CIVIC].long_name + ',' + adrc[STREET].long_name

            $('.address input.address').val(adrc[CIVIC].long_name + ' ' + adrc[STREET].long_name);
            $('.address input.city').val(adrc[CITY].long_name);
            $('.address input.state').val(adrc[STATE].long_name);
            $('.address input.country').val(adrc[COUNTRY].long_name);
            $('.address input.zipcode').val(adrc[ZIPCODE].long_name);
        }

        //Select2 default options
        var select2DefaultOptions = {
            closeOnSelect: true,
            debug: false,
            dropdownAutoWidth: false,
            //escapeMarkup: Utils.escapeMarkup,
            language: 'en',
            minimumInputLength: 2,
            maximumInputLength: 0,
            maximumSelectionLength: 0,
            minimumResultsForSearch: 0,
            selectOnClose: false,
            selectOnBlur: true,
            theme: 'default',
            width: '100%',
            placeholder: {
                id: '-1', // the value of the option
                text: 'Search for address'
            },
            ajax: {
                delay: 100
            },
        };

        //jQuery Plugin
        var pluginDefault = {
            placeServiceResult: placeServiceResult
        }
        $.fn.placecomplete = function (options) {
            this.each(function () {
                //Variable by instance
                var $s2 = $(this);
                //Init select2 for $this
                $s2.select2($.extend(true, {
                    ajax: {
                        transport: function (params, success, failure) {

                            // is caching enabled?
                            //TODO(sébastien) ajouter le cache pour google autocomplete
                            if ($s2.data('ajax--cache')) {

                            } else {
                                ac.getPlacePredictions($.extend(googleAutocompleteOptions, params.data), success);
                            }
                        },
                        data: function (params) {
                            return {input: params.term};
                        },
                        processResults: function (data, status) {
                            var response = {results: []}
                            if (data != undefined && data != null) {
                                $.each(data, function (index, item) {
                                    item.text = item.description;
                                    response.results.push(item)
                                });
                            } else {
                                var item = {};
                                item.id = status.term;
                                item.place_id = status.term;
                                item.text = status.term;
                                response.results.push(item)
                            }
                            return response;
                        },
                    }
                }, select2DefaultOptions, options || {}));

                options = $.extend(true, pluginDefault, options);
                $s2.on('select2:select', function (evt) {
                    ps.getDetails({placeId: evt.params.data.place_id}, options.placeServiceResult);
                });
            });
            return this;
        };

     
    })(jQuery);
    
    
    var franchise_manager = function (options) {
        var self = this;
        this.postcode = '';
        this.user_id = options.user_id;
        self.postcode = document.getElementById('locationPostCode').value;
        var data = {};
        data.postcode = self.postcode;
        self.fetch_franchise_list(self.user_id);
    }

    franchise_manager.prototype.autopopulate_franchise_list = function (data, selected) {
        var self = this;
        var option;
        var default_franchise = data.default_franchise;
        var option;
        var franchise_select = document.getElementById('businessId');
        franchise_select.innerHTML = '';
        if (data.franchise_list.length > 0) {
            var franchise_list = data.franchise_list;
            for (var i = 0; i < franchise_list.length; i++) {
                option = document.createElement('option');
                option.setAttribute('value', data.franchise_list[i].user_id);
                option.innerHTML = data.franchise_list[i].full_name + ' (' + data.franchise_list[i].email + ')';
                if (selected == data.franchise_list[i].user_id) {
                    option.setAttribute('selected', 'selected');
                }
                if ((selected == '' || selected == null) && data.selected_franchise.length > 0) {
                    if (data.selected_franchise[0].user_id == data.franchise_list[i].user_id) {
                        option.setAttribute('selected', 'selected');
                    }
                }
                option.setAttribute('data-item', JSON.stringify(data.franchise_list[i]));
                franchise_select.appendChild(option);
            }
        } else {
            var default_franchise = data.default_franchise;
            option = document.createElement('option');
            option.setAttribute('value', default_franchise.user_id);
            option.innerHTML = default_franchise.full_name + ' (' + default_franchise.email + ')';
            option.setAttribute('selected', 'selected');
            option.setAttribute('data-item', JSON.stringify(default_franchise));
            franchise_select.appendChild(option);
        }
    };

    franchise_manager.prototype.fetch_franchise_list = function (userid) {
        var self = this;
        $.ajax({
            url: base_url + 'admin/customer/franchise_auto_assign',
            type: 'get',
            data: {postcode: self.postcode,userid: userid},
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    self.autopopulate_franchise_list(response, userid);
                }
            }
        });
    };
    
    var context = {};
    context.user_id = "<?php echo (isset($customer_details['admin_id']) && $customer_details['admin_id'] != '') ? $customer_details['admin_id'] : ''; ?>";
    var franchise_manager_tool = new franchise_manager(context);
    
</script>
<?php 
$html = "<html>
<head><link href=\"https://fonts.googleapis.com/css?family=Open+Sans\" rel=\"stylesheet\">
<link href=\"https://fonts.googleapis.com/css?family=Montserrat:700\" rel=\"stylesheet\">
<link rel=\"stylesheet\" href=\"http://phptopdf.com/bootstrap.css\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Untitled Document</title>
<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
     <script type='text/javascript'>
     google.load('visualization', '1.0', {'packages':['corechart']});
     google.setOnLoadCallback(drawChart);
    function drawChart() {
       var data = google.visualization.arrayToDataTable([
           ['Year', 'No upgrade', 'LED Upgrade'],
		   ['YR 0',  0,   0 ],";


$html .= " ['YR 1',  " . $withoutUpgrade_yer1 . ",      " . $withUpgrade_yer1 . " ],
          ['YR 2',  " . $withoutUpgrade_yer2 . ",      " . $withUpgrade_yer2 . " ],
		  ['YR 3',  " . $withoutUpgrade_yer3 . ",      " . $withUpgrade_yer3 . " ],
		  ['YR 4',  " . $withoutUpgrade_yer4 . ",      " . $withUpgrade_yer4 . " ],
		  ['YR 5',  " . $withoutUpgrade_yer4 . ",      " . $withUpgrade_yer5 . " ]";

$html .= " ]);

        var options = {
          title: 'ROI : " . number_format($calculated_data['roi'], 2, '.', '') . " months',
          curveType: 'function',
		  width: 600,
		  height:230,
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }

  </script>
 <script type='text/javascript'>
     function drawVisualization() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Finance', 'New Bill', 'Old Bill' ],
    ['Year 1',  " . $Finance_Yr1 . "," . $NewBill_yer1 . "," . $OldBill2_yer1 . "],
	['Year 2',  " . $Finance_Yr2 . "," . $NewBill_yer2 . "," . $OldBill2_yer2 . "],
	['Year 3',  " . $Finance_Yr3 . "," . $NewBill_yer3 . "," . $OldBill2_yer3 . "],
	['Year 4',  " . $Finance_Yr4 . "," . $NewBill_yer4 . "," . $OldBill2_yer4 . "],
	['Year 5',  " . $Finance_Yr5 . "," . $NewBill_yer5 . "," . $OldBill2_yer5 . "],
	['Year 6',  '', " . $NewBill_yer6 . "," . $OldBill2_yer6 . "],
	['Year 7',  '', " . $NewBill_yer7 . "," . $OldBill2_yer7 . "],
	['Year 8',  '', " . $NewBill_yer8 . "," . $OldBill2_yer8 . "],
	['Year 9',  '', " . $NewBill_yer9 . "," . $OldBill2_yer9 . "],
	['Year 10',  '', " . $NewBill_yer10 . "," . $OldBill2_yer10 . "]
  ]);

 
  var options = {title:'Get a guaranteed saving on your power bill starting from year one even after financing your investment in your new LED lighting.',
		   titleTextStyle:{
              fontName: 'Open Sans',
			  italic: false,
			  bold:false,
			  fontSize: 14,
              fontStyle: 'normal' //or bold, italic, etc.
          },bar: {groupWidth: '60%'},
		  isStacked:true,
             width: 700,
		  height:300,
			legend: { position: 'bottom' },
            vAxis: {title: '',textStyle : {fontSize: 10}},
            hAxis: {title: '',textStyle : {fontSize: 10}}};
            
      var chart = new google.visualization.ColumnChart(document.getElementById('chart'));

        chart.draw(data, options);
}

google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawVisualization);
</script>

<script type='text/javascript'>
     function drawVisualization2() {
  var data = google.visualization.arrayToDataTable([
        ['Year',  'Savings' ],
    ['Year 1',   " . $TotalSavings_YR1 . " ],
	['Year 2',   " . $TotalSavings_YR2 . "],
	['Year 3',   " . $TotalSavings_YR3 . "],
	['Year 4',   " . $TotalSavings_YR4 . "],
	['Year 5',   " . $TotalSavings_YR5 . "],
	['Year 6',   " . $TotalSavings_YR6 . "],
	['Year 7',   " . $TotalSavings_YR7 . "],
	['Year 8',   " . $TotalSavings_YR8 . "],
	['Year 9',   " . $TotalSavings_YR9 . "],
	['Year 10',   " . $TotalSavings_YR10 . "]
  ]);

var options = {title:'As energy price increases every year, the armount of saving from your LED upgrade will also increase.',
		   titleTextStyle:{
              fontName: 'Open Sans',
			  italic: false,
			  bold:false,
			  fontSize: 14,
              fontStyle: 'normal' //or bold, italic, etc.
          },bar: {groupWidth: '60%'},
           width: 700,
		  height:300,
			legend: { position: 'bottom' },
            vAxis: {title: '',textStyle : {fontSize: 10}},
            hAxis: {title: '',textStyle : {fontSize: 10}}};
      
      var chart = new google.visualization.ColumnChart(document.getElementById('chart3'));
    chart.draw(data, options);
}

google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawVisualization2);
    </script>
	 <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\">  
<style>margin: 0 !important;
padding: 0 !important;</style>
</head>
<body style=\"padding:0; margin:0; font-family: 'Open Sans', sans-serif; font-size:14px; color:#000;\">";
$html .="<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
				<tbody>";

echo $html;
?>
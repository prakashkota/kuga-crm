<?php

class Lead_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Fetch Leads List
     * @param string $uuid
     * @param int $id
     * @param int $site_id
     * @return array
     */
    function get_leads($uuid = FALSE, $id = FALSE, $site_id = FALSE,$cust_business_name= FALSE) {
        $sql = "SELECT leads.*,leads.user_id as assigned_by,ltu.user_id as assigned_to,"
                . "ud.user_id,ud.simpro_user_id,ud.full_name,ud.company_name,ud.company_contact_no,"
                . "user.email as user_email,ul.address as user_address,"
                . "cust.first_name,cust.last_name,cust.customer_contact_no,cust.customer_email,cust.title,"
                . "cust.company_name as customer_company_name,cust.position,cust.simpro_cust_id,"
                . "cl.id as location_id,cl.state_id,cl.address,cl.postcode,cl.site_name,cl.site_contact_name,cl.site_contact_no,cl.suburb,cl.latitude,cl.longitude,"
                . "state.*,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,
                   (SELECT GROUP_CONCAT(full_name) FROM tbl_lead_to_user,tbl_user_details 
                    WHERE tbl_lead_to_user.lead_id = leads.id AND FIND_IN_SET(tbl_lead_to_user.user_id, tbl_user_details.user_id)) AS franchise"
                . " FROM tbl_leads as leads";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        if ($site_id) {
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.id=" . $site_id;
        } else {
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.address != ''";
        }
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";


        if ($uuid) {
            $sql .= " WHERE leads.uuid='" . $uuid . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($id) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($cust_business_name) {
            $sql .= " WHERE cust.company_name='" . $cust_business_name . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }
    
    function get_leads_count($cond = '') {
        if($cond == ''){
            $sql = "SELECT leads.id  FROM tbl_leads as leads";
        }else{
            $sql = "SELECT leads.id"
                    . " FROM tbl_leads as leads";
            $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
            $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
            $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
            $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
            $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        }
        $sql .= $cond . " GROUP BY leads.id ORDER BY leads.id DESC";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    /**
     * Get Franchise list where franchise is not assigned any other lead today other than than current lead
     * @return array
     */
    function get_franchise_list($postcode, $uuid) {
        //ul.postcode='" . $postcode . "' || AND ud.availability=1
        $sql = "SELECT admin.*,ud.*,user_group.group_id FROM aauth_users as admin";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $cond = " WHERE admin.banned=0 AND (find_in_set('" . $postcode . "',ul.workarea_postcodes) || find_in_set('" . $postcode . "',ul.workarea_postcodes_secondary)) <> 0 && admin.id NOT IN "
                . "(SELECT user_id FROM tbl_lead_to_user where Date(created_at) = CURDATE() && lead_id != (SELECT id from tbl_leads where uuid ='" . $uuid . "'))";
        $sql .= " $cond GROUP BY admin.id ORDER BY ul.workarea_postcodes";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_leads_data_for_communication($id = FALSE, $franchise_id = FALSE) {
        $sql = "SELECT leads.id,leads.uuid,leads.cust_id,leads.note,leads.created_at,leads.lead_status,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,"
                . "cl.address,cl.postcode,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud1.full_name as franchise_name,ud1.company_contact_no as franchise_contact_no,"
                . "ud1.full_name as franchise,"
                . "COALESCE(tp.uuid,NULL) as proposal_id,COALESCE(tp.deal_won_lost,NULL) as deal_won_lost"
                . " FROM tbl_leads as leads";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud1 ON ud1.user_id = user.id";
        $sql .= " LEFT JOIN tbl_proposal as tp ON tp.lead_id = leads.id";

        if ($id && $franchise_id) {
            $sql .= " WHERE leads.id='" . $id . "' && ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($id) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($franchise_id) {
            $sql .= " WHERE ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

    /**
     * fetch get lead activity by lead id
     * @param type $id
     * @return array
     */
    function get_lead_activity($id) {
        $sql = "SELECT activity.id,activity.attachment,activity.note,"
                . "DATE_FORMAT(`created_at`,'%d/%m/%Y') as created_at,"
                . "user.full_name"
                . " FROM tbl_lead_activity as activity";
        $sql .= " LEFT JOIN tbl_user_details as user ON user.user_id = activity.user_id";
        $sql .= " WHERE activity.lead_id='" . $id . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    /**function get_leads_data_by_stage_count($stage_id = FALSE, $filters = '',$having = FALSE) {

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        
        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";
        $sql = "SELECT leads.id,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select"
                . " FROM tbl_leads as leads";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";


        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
        $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";

        //echo $sql;die;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }*/
    
    function get_leads_data_by_stage_count($stage_id = FALSE, $filters = '',$having = FALSE) {

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        
        if($filters == ''){
            $sql = "SELECT leads.id  FROM tbl_leads as leads";
        }else{
            $sql = "SELECT leads.id"
                    . " FROM tbl_leads as leads";
            $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
            $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
            $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
            $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
            $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        }

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
        $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";

        //echo $sql;die;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    function get_leads_data_by_stage($stage_id = FALSE, $filters = '',$having = FALSE,$offset = FALSE, $limit = FALSE , $is_filter = FALSE) {

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";


        if ($is_filter == FALSE && $limit != FALSE) {
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other,current_monthly_electricity_bill,lead_location_type FROM tbl_leads ORDER BY id DESC LIMIT $offset,$limit"
                . ") as leads";     
        }else{
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other,current_monthly_electricity_bill,lead_location_type FROM tbl_leads ORDER BY id DESC"
                . ") as leads"; 
        }
        
        //LAC1 is activity for Scheduled but not completed and LAC2 is for completed
        $activity_columns =  "CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_duration WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_duration ELSE NULL END AS scheduled_duration,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_time WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_time ELSE NULL END AS scheduled_time,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_date WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_date ELSE NULL END AS scheduled_date,"
                            ."CASE WHEN lac1.status IS NOT NULL THEN '0' WHEN lac2.status IS NOT NULL THEN '1' ELSE '' END AS activity_completed";
                       
        $sql = "SELECT leads.id,leads.uuid,leads.note,DATE_FORMAT(leads.created_at,'%d/%m/%Y') as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,leads.lead_type,leads.user_id,leads.current_monthly_electricity_bill,leads.lead_location_type,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,state.state_postal as customer_state_postal,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,'New Lead') as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select,"
                . "nf.id as notification_id,IF(nf.id IS NULL,NULL,IF(nf.read_at,1,0)) as is_viewed,"
                . "$activity_columns"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
        $sql .= " LEFT JOIN tbl_notifications as nf ON nf.reference_id = leads.id AND nf.notification_type = 'NEW_LEAD'";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=0) as lac1 ON lac1.lead_id = leads.id AND lac1.status=0";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=1) as lac2 ON lac2.lead_id = leads.id AND lac2.status=1";


        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
        if($limit == FALSE){
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else{
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC LIMIT $offset,$limit";  
        }
        
        
        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function get_leads_data_by_stage_count_for_pipeline($stage_id = FALSE, $filters = '',$having = FALSE) {

        $filter = $this->input->get('filter');

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        
        if($filters == ''){
            $sql = "SELECT COUNT(leads.id) as cnt  FROM tbl_leads as leads";
        }else{
            $sql = "SELECT COUNT(leads.id) as cnt"
                    . " FROM tbl_leads as leads";
            $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
            $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
            $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
            $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
            $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
            if (isset($filter) && !empty($filter)) {
                if (isset($filter['start_date']) && $filter['start_date'] != '') {
                    $sql .= " LEFT JOIN tbl_activities as activity ON activity.lead_id = leads.id";
                }
            }
        }

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
       // $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";

        //echo $sql;die;

        $query = $this->db->query($sql);
        return $query->result_array()[0]['cnt'];
    }
    
    function get_leads_data_by_stage_for_pipeline($stage_id = FALSE, $filters = '',$having = FALSE,$offset = FALSE, $limit = FALSE , $is_filter = FALSE) {

        $filter = $this->input->get('filter');

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";
        $total_solar_kws = "Coalesce((SELECT SUM(tsp.total_system_size) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_system_size_proposal";

        if ($is_filter == FALSE && $limit != FALSE) {
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other 
                    FROM tbl_leads ORDER BY id DESC LIMIT $offset,$limit"
                . ") as leads";     
        }else{
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other 
                    FROM tbl_leads ORDER BY id DESC"
                . ") as leads"; 
        }
        
        
        
        //LAC1 is activity for Scheduled but not completed and LAC2 is for completed
        $activity_columns =  "CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_duration WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_duration ELSE NULL END AS scheduled_duration,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_time WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_time ELSE NULL END AS scheduled_time,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_date WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_date ELSE NULL END AS scheduled_date,"
                            ."CASE WHEN lac1.status IS NOT NULL THEN '0' WHEN lac2.status IS NOT NULL THEN '1' ELSE '' END AS activity_completed";
                       
        $sql = "SELECT leads.id,leads.uuid,leads.note,DATE_FORMAT(leads.created_at,'%d/%m/%Y') as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,leads.lead_type,leads.user_id,"
                . "leads.total_system_size,leads.total_battery_size,leads.total_highbay,leads.total_batten_light,leads.total_panel_light,leads.total_flood_light,leads.total_down_light,leads.total_other,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,'New Lead') as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select,"
                . "nf.id as notification_id,IF(nf.id IS NULL,NULL,IF(nf.read_at,1,0)) as is_viewed,"
                . "$activity_columns,"
                . "$total_solar_kws"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
        $sql .= " LEFT JOIN tbl_notifications as nf ON nf.reference_id = leads.id AND nf.notification_type = 'NEW_LEAD'";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=0) as lac1 ON lac1.lead_id = leads.id AND lac1.status=0";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=1) as lac2 ON lac2.lead_id = leads.id AND lac2.status=1";

        if (isset($filter) && !empty($filter)) {
            if (isset($filter['start_date']) && $filter['start_date'] != '') {
                $sql .= " LEFT JOIN tbl_activities as activity ON activity.lead_id = leads.id";
            }
        }

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        } else if ($cond != '' && $filters == '') {
            $sql .= " WHERE " . $cond;
        }
        
        if($limit == FALSE){
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else{
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC LIMIT $offset,$limit";  
        }
        
        
        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_proposal_form_data($proposal_uuid = FALSE) {
        $sql = "SELECT leads.id,leads.uuid,leads.note,leads.created_at,leads.lead_status,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_full_name,cust.customer_contact_no,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as company_email,ud1.full_name as company_full_name,ud1.company_contact_no,"
                . "tp.system_size as proposal_system_size,tp.system_size as proposal_system_size,"
                . "tp.no_of_stories as proposal_stories,tp.roof_type as proposal_roof_type,COALESCE(tp.id,NULL) as proposal_id,"
                . "COALESCE(tp.uuid,NULL) as proposal_uuid,COALESCE(tp.deal_won_lost,NULL) as deal_won_lost"
                . " FROM tbl_leads as leads";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud1 ON ud1.user_id = user.id";
        $sql .= " LEFT JOIN tbl_proposal as tp ON tp.lead_id = leads.id";

        if ($proposal_uuid) {
            $sql .= " WHERE tp.uuid='" . $proposal_uuid . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
    }

    function get_template_form_data($template_id = FALSE, $proposal_id = FALSE) {
        $sql = "SELECT temp_attr.*,td.*"
                . " FROM  tbl_template_data as td";
        $sql .= " LEFT JOIN tbl_templates as temp ON temp.id = td.template_id";
        $sql .= " LEFT JOIN tbl_template_attributes as temp_attr ON temp_attr.id = td.field_id";

        if ($template_id && $proposal_id) {
            $sql .= " WHERE td.template_id='" . $template_id . "' AND td.proposal_id='" . $proposal_id . "'";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

    function get_lead_count($postcode = FALSE) {
        $sql = "SELECT count(*) as lead_count,ltu.user_id,ltu1.last_allocated_at as last_allocated_at,ud.full_name as company_full_name,ud.company_contact_no FROM tbl_lead_to_user ltu";
        $sql .= " INNER JOIN (SELECT user_id, MAX(created_at) AS last_allocated_at FROM tbl_lead_to_user GROUP BY user_id) ltu1 ON ltu.user_id = ltu1.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = ud.user_id";
        if ($postcode) {
            $sql .= " WHERE (find_in_set('" . $postcode . "',ul.workarea_postcodes) || find_in_set('" . $postcode . "',ul.workarea_postcodes_secondary)) <> 0 AND user_group.group_id=4";
        }
        $sql .= "";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_franchise_performance_data($user_id, $start_date, $end_date) {
        $date_cond = '(DATE(lead.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '")';
        $deal_won_cond = '(lead.lead_stage=7)';

        $allocated_leads = "Coalesce((SELECT count(id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND lead.user_id !=" . $user_id . " AND $date_cond),0) as allocated_leads";

        $self_generated_leads = "Coalesce((SELECT count(id) FROM tbl_leads "
                . "WHERE user_id=" . $user_id . " AND DATE(created_at) BETWEEN '" . $start_date . "' AND '" . $end_date . "'),0) as self_generated";

        $won_allocated_leads = "Coalesce((SELECT count(lead.id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " LEFT JOIN tbl_proposal as tp ON tp.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND lead.user_id !=" . $user_id . " AND $date_cond AND $deal_won_cond),0) as won_allocated_leads";

        $won_self_generated_leads = "Coalesce((SELECT count(lead.id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_proposal as tp ON tp.lead_id = lead.id"
                . " WHERE lead.user_id=" . $user_id . " AND $date_cond AND $deal_won_cond),0) as won_self_generated_leads";

        $won_total_revenue = "Coalesce((SELECT SUM(tp.quote_total_inGst) FROM tbl_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND $date_cond AND $deal_won_cond),0) as total_revenue";

        $won_total_kws = "Coalesce((SELECT SUM(tp.system_size) FROM tbl_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND $date_cond AND $deal_won_cond),0) as total_kws";

        $sql = "SELECT DATE_FORMAT(lead.created_at,'%d/%m/%Y') as created_at,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "state.state_postal,"
                . "$allocated_leads,"
                . "$self_generated_leads,"
                . "$won_allocated_leads,"
                . "$won_self_generated_leads,"
                . "$won_total_revenue,"
                . "$won_total_kws"
                . " FROM tbl_leads as lead ";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $sql .= " LEFT JOIN tbl_proposal as tp ON tp.lead_id = lead.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = $user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ul.state_id";
        $sql .= " WHERE " . $date_cond . " AND ltu.user_id=" . $user_id . " GROUP BY ltu.user_id";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function get_franchise_performance_data_by_month($user_id, $month_year) {
        $date_cond = '(DATE_FORMAT(created_at,"%m-%Y") = "' . $month_year . '")';
        $date_cond1 = '(DATE_FORMAT(lead.created_at,"%m-%Y") = "' . $month_year . '")';

        $deal_won_cond = '(lead.lead_stage=7)';

        $allocated_leads = "Coalesce((SELECT count(id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND lead.user_id !=" . $user_id . " AND $date_cond1),0) as allocated_leads";

        $self_generated_leads = "Coalesce((SELECT count(id) FROM tbl_leads "
                . "WHERE user_id=" . $user_id . " AND $date_cond),0) as self_generated";

        $won_allocated_leads = "Coalesce((SELECT count(id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND $date_cond1 AND $deal_won_cond),0) as won_allocated_leads";

        $won_self_generated_leads = "Coalesce((SELECT count(id) FROM tbl_leads lead"
                . " WHERE lead.user_id=" . $user_id . " AND $date_cond1 AND $deal_won_cond GROUP BY lead.user_id),0) as won_self_generated_leads";

        $won_total_revenue = "Coalesce((SELECT SUM(tp.quote_total_inGst) FROM tbl_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND $date_cond1 AND $deal_won_cond),0) as total_revenue";

        $won_total_kws = "Coalesce((SELECT SUM(tp.system_size) FROM tbl_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE ltu.user_id=" . $user_id . " AND $date_cond1 AND $deal_won_cond),0) as total_kws";

        $sql = "SELECT DATE_FORMAT(lead.created_at,'%d/%m/%Y') as created_at,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "$allocated_leads,"
                . "$self_generated_leads,"
                . "$won_allocated_leads,"
                . "$won_self_generated_leads,"
                . "$won_total_revenue,"
                . "$won_total_kws"
                . " FROM tbl_leads as lead ";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $sql .= " LEFT JOIN tbl_proposal as tp ON tp.lead_id = lead.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = $user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ul.state_id";
        $sql .= " WHERE " . $date_cond1 . " AND ltu.user_id=" . $user_id . " GROUP BY ltu.user_id";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_led_proposal_data($proposal_id = FALSE) {
        $cond = "";
        $sql = "SELECT lp.*,"
                . "tpf.term,tpf.monthly_payment_plan,tpf.monthly_saving,tpf.monthly_net_cashflow,tpf.upfront_payment_options,"
                . "cust.company_name as customer_company_name,cust.position,cust.title,cust.first_name,cust.last_name,"
                . "cust.customer_email,cust.customer_contact_no,"
                . "cl.site_name,cl.site_contact_name,cl.site_contact_email,cl.site_contact_no,cl.address as customer_address,"
                . "ud.full_name as rep_name,ud.company_name as rep_company_name,ud.company_contact_no as rep_company_contact_no,"
                . "ud.company_abn as rep_company_abn,user.email as rep_email,ul.address as rep_company_address"
                . " FROM tbl_led_proposal as lp";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as lu ON lead.id = lu.lead_id";
        $sql .= " LEFT JOIN tbl_proposal_finance as tpf ON tpf.proposal_id = lp.id AND proposal_type = 1";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = lead.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = lu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = lu.user_id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        if ($proposal_id) {
            $cond .= " WHERE lp.id=" . $proposal_id;
        }
        $sql .= " $cond ORDER BY lp.id ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    private function lead_count_by_stage_subquery($start_date, $end_date, $user_cond, $lead_stage, $type = '1') {
        //$date_cond = '(DATE(lead.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '")';
        $date_cond = '1=1 ';
        $proposal_table = ($type == 'led') ? 'tbl_led_proposal' : 'tbl_solar_proposal';
        $lead_type_condition = ' AND lead.lead_type='.$type;
        /**$query = "Coalesce((SELECT count(lp.id) FROM $proposal_table lp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                //. " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE $date_cond $user_cond"
                . "AND lead.lead_stage = $lead_stage),0)";*/
        //echo $query;die;
        $query = "Coalesce((SELECT count(lead.id) FROM tbl_leads lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE $date_cond $user_cond $lead_type_condition"
                . " AND lead.lead_stage = $lead_stage),0)";

        
        return $query;
    }

    public function lead_count_by_lead_stages_for_csm_report($start_date, $end_date, $user_cond,$type) {
        $stage1_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 1, $type);
        $stage2_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 2, $type);
        $stage3_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 3, $type);
        $stage4_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 4, $type);
        $stage5_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 5, $type);
        $stage6_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 6, $type);
        $stage7_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 7, $type);
        $stage8_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 8, $type);
        $stage9_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 9, $type);
        $stage10_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 10, $type);


        $sql = "SELECT "
                . "$stage1_count as stage1_count,"
                . "$stage2_count as stage2_count,"
                . "$stage3_count as stage3_count,"
                . "$stage4_count as stage4_count,"
                . "$stage5_count as stage5_count,"
                . "$stage6_count as stage6_count,"
                . "$stage7_count as stage7_count,"
                . "$stage8_count as stage8_count,"
                . "$stage9_count as stage9_count,"
                . "$stage10_count as stage10_count"
                . " FROM tbl_leads as lead ";
        //echo $sql;die;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function led_lead_count_by_lead_stages($start_date, $end_date, $user_cond) {
        $stage1_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 1);
        $stage2_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 2);
        $stage3_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 3);
        $stage4_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 4);
        $stage5_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 5);
        $stage6_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 6);
        $stage7_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 7);
        $stage8_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 8);
        $stage9_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 9);
        $stage10_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 10);


        $sql = "SELECT "
                . "$stage1_count as stage1_count,"
                . "$stage2_count as stage2_count,"
                . "$stage3_count as stage3_count,"
                . "$stage4_count as stage4_count,"
                . "$stage5_count as stage5_count,"
                . "$stage6_count as stage6_count,"
                . "$stage7_count as stage7_count,"
                . "$stage8_count as stage8_count,"
                . "$stage9_count as stage9_count,"
                . "$stage10_count as stage10_count"
                . " FROM tbl_leads as lead ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function solar_lead_count_by_lead_stages($start_date, $end_date, $user_cond) {
        $stage1_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 1, 'solar');
        $stage2_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 2, 'solar');
        $stage3_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 3, 'solar');
        $stage4_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 4, 'solar');
        $stage5_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 5, 'solar');
        $stage6_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 6, 'solar');
        $stage7_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 7, 'solar');
        $stage8_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 8, 'solar');
        $stage9_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 9, 'solar');
        $stage10_count = $this->lead_count_by_stage_subquery($start_date, $end_date, $user_cond, 10, 'solar');


        $sql = "SELECT "
                . "$stage1_count as stage1_count,"
                . "$stage2_count as stage2_count,"
                . "$stage3_count as stage3_count,"
                . "$stage4_count as stage4_count,"
                . "$stage5_count as stage5_count,"
                . "$stage6_count as stage6_count,"
                . "$stage7_count as stage7_count,"
                . "$stage8_count as stage8_count,"
                . "$stage9_count as stage9_count,"
                . "$stage10_count as stage10_count"
                . " FROM tbl_leads as lead ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_lead_booking_forms_data($lead_id) {
        $sql1 = "SELECT lbf.uuid,lbf.created_at,lbf.booking_form,lbf.kuga_job_id,lbf.simpro_job_id,lbf.simpro_attachment_id,lbf.pdf_file,lbf.pdf_file_size,'LED' as type,NULL as type_name,"
                . "cl.id as location_id,cl.address,cl.site_name"
                . " FROM tbl_led_booking_form as lbf";
        $sql1 .= " LEFT JOIN tbl_customer_locations as cl ON cl.id = lbf.site_id";
        $sql1 .= " WHERE lbf.lead_id = $lead_id";

        //$type_name = "IF(sbf.type_id = 1,'0 - 39kW',IF(sbf.type_id = 2,'40kW - 100kW','100kW+'))";
        $type_name = "IF(sbf.type_id = 1,'0 - 39kW',IF(sbf.type_id = 2,'40kW - 100kW',IF(sbf.type_id = 3,'100kW+','Residential 30kW')))";
        $sql2 = "SELECT sbf.uuid,sbf.created_at,sbf.booking_form,sbf.kuga_job_id,sbf.simpro_job_id,sbf.simpro_attachment_id,sbf.pdf_file,sbf.pdf_file_size,'SOLAR' as type,$type_name as type_name,"
                . "cl.id as location_id,cl.address,cl.site_name"
                . " FROM tbl_solar_booking_form as sbf";
        $sql2 .= " LEFT JOIN tbl_customer_locations as cl ON cl.id = sbf.site_id";
        $sql2 .= " WHERE sbf.lead_id = $lead_id";

        $query = $this->db->query($sql1 . ' UNION ' . $sql2);
        return $query->result_array();
    }
    
    public function get_lead_proposal_data($lead_id) {
        $sql1 = "SELECT sp.*,'Commercial Solar Proposal' as type,"
                . "cl.id as location_id,cl.address,cl.site_name"
                . " FROM tbl_solar_proposal as sp";
        $sql1 .= " LEFT JOIN tbl_customer_locations as cl ON cl.id = sp.site_id";
        $sql1 .= " WHERE sp.lead_id = $lead_id";
        $query = $this->db->query($sql1);
        return $query->result_array();
    }

    function get_leads_for_export($filters = FALSE) {
        $sql = "SELECT cust.first_name as First_Name,cust.last_name as Last_Name,cust.company_name as Customer_Organization,cust.customer_email as Email,cust.customer_contact_no as Contact_No,cl.postcode as Postcode,state.state_name as State,cl.address as Address,"
                . "ud.full_name AS Owner,"
                . "ls.stage_name as Lead_Stage,leads.lead_status as Lead_Status,DATE(leads.created_at) Lead_Creation_Date, leads.lead_source as Lead_Source, leads.lead_location_type as Lead_Type"
                . " FROM (
           SELECT id,lead_status,created_at,cust_id,lead_stage,lead_source,lead_type, lead_location_type
           FROM     tbl_leads
           ORDER BY id
          ) leads ";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";

        if ($filters != FALSE) {
            $sql .= " WHERE 1=1" . $filters;
        }
        $sql .= " GROUP BY leads.id ORDER BY leads.created_at DESC";
        $query = $this->db->query($sql);
        
        $leads = $query->result_array();
        
        foreach($leads as $key => &$value){
            
            $lead_type = '';
            if($value['Lead_Type'] == 1){
                $lead_type = 'Residential';
            }else if($value['Lead_Type'] == 2){
                $lead_type = 'Commercial';
            }else if($value['Lead_Type'] == 3){
                $lead_type = 'Residential - Unbranded';
            }else if($value['Lead_Type'] == 4){
                $lead_type = 'Commercial- Unbranded';
            }
            $value['Lead_Type'] = $lead_type;
        }
        return $leads;
    }
    
    function get_leads_for_export1($filters = FALSE) {
        $sql = "SELECT CONCAT(cust.first_name,' ',cust.last_name) as Customer,cust.company_name as Customer_Organization,"
                . "ud.full_name AS Owner,"
                . "ls.stage_name as Lead_Stage,leads.lead_status as Lead_Status,DATE(leads.created_at) Lead_Creation_Date"
                . " FROM (
           SELECT id,lead_status,created_at,cust_id,lead_stage,lead_source,lead_type
           FROM     tbl_leads
           ORDER BY id
          ) leads ";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";

        if ($filters != FALSE) {
            $sql .= " WHERE 1=1" . $filters;
        }
        $sql .= " GROUP BY leads.id ORDER BY leads.created_at DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_leads_data_for_map($offset = 0, $limit = 300,$cond = FALSE,$is_filter = FALSE) {
        
        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";

        $left_join_activity = "LEFT JOIN ("
                . "SELECT lead_id,created_at as activity_last_at,user_id from tbl_activities ORDER BY id DESC LIMIT 1"
                . ") as ac ON ac.lead_id = leads.id";

        if ($is_filter == FALSE && $limit != FALSE) {
            $from_select = "(SELECT id,user_id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment FROM tbl_leads ORDER BY created_at DESC "
                . "LIMIT $offset,$limit) as leads";   
        }else{
           $from_select = "(SELECT id,user_id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment FROM tbl_leads ORDER BY created_at DESC "
                . ") as leads";
        }
        
        
        $sql = "SELECT leads.id,leads.user_id,leads.uuid,leads.note,DATE_FORMAT(leads.created_at,'%d/%m/%Y') as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,leads.lead_type,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,cl.latitude,cl.longitude,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.company_name as franchise_company_name,"
                . "ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,ud.full_name) as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select,"
                . "COALESCE(activity_last_at,DATE_FORMAT(leads.created_at,'%d/%m/%Y')) as activity_last_at,"
                . "COALESCE(acud.full_name,ud.full_name) as activity_user"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.latitude IS NOT NULL";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
        $sql .= " $left_join_activity";
        $sql .= " LEFT JOIN tbl_user_details as acud ON acud.user_id = ac.user_id";

        if($limit == FALSE){
           $sql .= $cond ." GROUP BY leads.id ORDER BY leads.id DESC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .=  $cond ." GROUP BY leads.id ORDER BY leads.id DESC";
        }else{
           $sql .=  $cond ." GROUP BY leads.id ORDER BY leads.id DESC LIMIT $offset,$limit";  
        }

        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_leads_by_coordinates($offset = 0, $limit = 300,$cond = FALSE,$lat,$lng){

        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";
        
        $distance = '(
           6371 *
           acos(cos(radians('.$lat.')) * 
           cos(radians(cl.latitude)) * 
           cos(radians(cl.longitude) - 
           radians('.$lng.')) + 
           sin(radians('.$lat.')) * 
           sin(radians(cl.latitude)))
        ) AS distance';
        
        $left_join_activity = "LEFT JOIN ("
                . "SELECT lead_id,created_at as activity_last_at,user_id from tbl_activities ORDER BY id DESC LIMIT 1"
                . ") as ac ON ac.lead_id = leads.id";

        $from_select = "(SELECT lead.id,lead.user_id,lead.uuid,lead.note,lead.created_at,"
                . "lead.lead_status,lead.lead_source,lead.cust_id,lead.roof_type,lead.no_of_stories,lead.lead_stage,lead.category_id,lead.sub_category_id,"
                . "lead.lead_type,lead.lead_segment,cl.latitude,cl.longitude,$distance FROM tbl_leads lead"
                . " LEFT JOIN tbl_customers as cust ON cust.id = lead.cust_id"
                . " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.latitude IS NOT NULL"
                . " HAVING distance < 25 ORDER BY distance LIMIT $offset,$limit) as leads"; 
        
        $sql = "SELECT leads.id,leads.user_id,leads.uuid,leads.note,DATE_FORMAT(leads.created_at,'%d/%m/%Y') as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,leads.lead_type,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,cl.latitude,cl.longitude,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.company_name as franchise_company_name,"
                . "ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,ud.full_name) as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select,"
                . "COALESCE(activity_last_at,DATE_FORMAT(leads.created_at,'%d/%m/%Y')) as activity_last_at,"
                . "COALESCE(acud.full_name,ud.full_name) as activity_user"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.latitude IS NOT NULL";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
        $sql .= " $left_join_activity";
        $sql .= " LEFT JOIN tbl_user_details as acud ON acud.user_id = ac.user_id";
        
        if($cond != FALSE){
            $sql .=  $cond ." GROUP BY leads.id ORDER BY leads.id DESC";
        }else{
            $sql .=  " GROUP BY leads.id ORDER BY leads.id DESC";
        }
        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function get_leads_data_for_calendar($offset = 0, $limit = 300,$cond = FALSE,$is_filter = FALSE) {
        
        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";

        $from_select = "(SELECT id,user_id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment FROM tbl_leads $cond ORDER BY created_at DESC "
                . ") as leads";
        
        //LAC1 is activity for Scheduled but not completed and LAC2 is for completed
        $activity_columns =  "CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_duration WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_duration ELSE NULL END AS scheduled_duration,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_time WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_time ELSE NULL END AS scheduled_time,"
                            ."CASE WHEN lac1.scheduled_duration IS NOT NULL THEN  lac1.scheduled_date WHEN lac2.scheduled_duration IS NOT NULL THEN  lac2.scheduled_date ELSE NULL END AS scheduled_date,"
                            ."CASE WHEN lac1.status IS NOT NULL THEN '0' WHEN lac2.status IS NOT NULL THEN '1' ELSE '' END AS activity_completed";

        
        $sql = "SELECT leads.id,leads.user_id,leads.uuid,leads.note,leads.created_at as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,cl.latitude,cl.longitude,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.company_name as franchise_company_name,"
                . "ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,ud.full_name) as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select,$activity_columns"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id AND cl.latitude IS NOT NULL";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=0) as lac1 ON lac1.lead_id = leads.id AND lac1.status=0";
        $sql .= " LEFT JOIN (SELECT lead_id,scheduled_duration,scheduled_time,scheduled_date,status from tbl_activities ac WHERE ac.scheduled_duration != '0.00' && ac.scheduled_time IS NOT NULL AND ac.status=1) as lac2 ON lac2.lead_id = leads.id AND lac2.status=1";

        if($limit == FALSE){
           $sql .=  " GROUP BY leads.id ORDER BY leads.id DESC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= " GROUP BY leads.id ORDER BY leads.id DESC";
        }else{
           $sql .=  " GROUP BY leads.id ORDER BY leads.id DESC LIMIT $offset,$limit";  
        }

        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_solar_proposal_totals($lead_id = FALSE) {
        $cond = "";

        $total_kws = "Coalesce(SUM(sp.total_system_size),0) as total_system_size";
        $total_solar_project_cost_select = "Coalesce(SUM(sp.price_before_stc_rebate),0) as total_system_value";

        $sql = "SELECT "
                . "$total_kws,"
                . "$total_solar_project_cost_select"
                . " FROM tbl_solar_proposal as sp";
        if ($lead_id) {
            $cond .= " WHERE sp.lead_id=" . $lead_id;
        }
        $sql .= " $cond ORDER BY sp.id ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_state_by_user_current_latlng($lat,$lng){
        #Set your users current location in LAT/LONG here
        $sql='SELECT
            state_id, (
              6371 * acos (
              cos ( radians("'.$lat.'") )
              * cos( radians( latitude ) )
              * cos( radians( longitude ) - radians("'.$lng.'") )
              + sin ( radians("'.$lat.'") )
              * sin( radians( latitude ) )
            )
        ) AS distance
        FROM tbl_customer_locations
        HAVING distance < 100
        ORDER BY id
        LIMIT 1 , 5;';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_pipedrive_files($cust_id){
        $sql = 'SELECT pdf.file_name,pdf.file_data,pdl.kuga_cust_id from tbl_pipedrive_files pdf';
        $sql .= " LEFT JOIN tbl_pipedrive_leads_data as pdl ON pdl.id = pdf.pd_lead_id";
        $sql .= " WHERE pdl.kuga_cust_id=".$cust_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    
    function get_leads_data_by_report_count($stage_id = FALSE, $filters = '',$having = FALSE) {

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        
        if($filters == ''){
            $sql = "SELECT leads.id  FROM tbl_leads as leads";
        }else{
            $sql = "SELECT leads.id"
                    . " FROM tbl_leads as leads";
            $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
            $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
            $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
            $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
            $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
        }

        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
        $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";

        //echo $sql;die;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    function get_leads_data_by_report($stage_id = FALSE, $filters = '',$having = FALSE,$offset = FALSE, $limit = FALSE , $is_filter = FALSE) {

        if(CACHING){
            //Enable Chace of Leads
            $this->db->cache_on();
        }

        $cond = "";
        if ($stage_id) {
            $cond .= "leads.lead_stage = $stage_id";
        }
        $total_led_project_cost_select = "Coalesce((SELECT SUM(tlp.total_project_cost_exGST) FROM tbl_led_proposal tlp WHERE tlp.lead_id = leads.id),0) as total_led";
        $total_solar_project_cost_select = "Coalesce((SELECT SUM(tsp.price_before_stc_rebate) FROM tbl_solar_proposal tsp WHERE tsp.lead_id = leads.id),0) as total_solar";


        if ($is_filter == FALSE && $limit != FALSE) {
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other,current_monthly_electricity_bill,lead_location_type FROM tbl_leads ORDER BY id DESC LIMIT $offset,$limit"
                . ") as leads";     
        }else{
           $from_select = "(SELECT id,uuid,note,created_at,"
                . "lead_status,lead_source,cust_id,roof_type,no_of_stories,lead_stage,category_id,sub_category_id,lead_type,lead_segment,user_id,
                    total_system_size,total_battery_size,total_highbay,total_batten_light,total_panel_light,total_flood_light,total_down_light,total_other,current_monthly_electricity_bill,lead_location_type FROM tbl_leads ORDER BY id DESC"
                . ") as leads"; 
        }
                       
        $sql = "SELECT leads.id,leads.uuid,leads.note,DATE_FORMAT(leads.created_at,'%d/%m/%Y') as created_at,"
                . "leads.lead_status,leads.lead_source,leads.cust_id,leads.roof_type,leads.no_of_stories,leads.lead_stage,leads.lead_type,leads.user_id,leads.current_monthly_electricity_bill,leads.lead_location_type,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,state.state_postal as customer_state_postal,"
                . "cat.category_name,sub_cat.category_name as sub_category_name,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,'New Lead') as deal_stage_name,"
                . "$total_led_project_cost_select,"
                . "$total_solar_project_cost_select"
                . " FROM $from_select";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = leads.category_id";
        $sql .= " LEFT JOIN tbl_service_category as sub_cat ON sub_cat.id = leads.sub_category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";


        if ($cond != '' && $filters != '') {
            $sql .= " WHERE " . $cond . $filters;
        } else if ($cond == '' && $filters != '') {
            $sql .= " WHERE 1=1" . $filters;
        }
        
        if($limit == FALSE){
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else if($is_filter == FALSE && $limit != FALSE){
            $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC";
        }else{
           $sql .= " GROUP BY leads.id $having ORDER BY leads.id DESC LIMIT $offset,$limit";  
        }
        
        
        //echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}

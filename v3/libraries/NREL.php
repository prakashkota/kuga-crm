<?php


class NREL
{

    public $url = 'https://developer.nrel.gov/api/';
    protected $version = 6;
    protected $authHeader;
    protected $responseRawData;
    protected $responseStatus;

    public function __construct()
    {
    }

    public function pvwatts($method)
    {
        $pvwattsUrl = $this->url . 'pvwatts/v' . $this->version . '.json?' . $method . '&api_key=faTU9oNdpQ3MDwxvgOh6JCawLmj1Z0tsSLcWrvek';


        $ch = curl_init($pvwattsUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        # Print response.
        return $result;

    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Article Controller
 * @version 1.0
 */
class Article extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("User_Model", "user");
        $this->load->model("Article_Model", "article");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function manage() {
        $data = array();
        $articles = $this->article->get_articles();
        $data['articles'] = $articles;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('article/manage');
        $this->load->view('partials/footer');
    }

    public function add($id = FALSE) {
        $data = array();
        $data['article'] = array();
        if ($id) {
            $article = $this->common->fetch_where('tbl_articles', '*', array('id' => $id));
            if (empty($article)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Article Found with sepcified Id.</div>');
                redirect('admin/knowledgebase/article/manage');
            }
            $data['article'] = $article[0];
        }

        $article_data = $this->input->post('article');
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('article[article_title]', 'Title', 'required');
            $this->form_validation->set_rules('article[category_id]', 'Category', 'required');
            $this->form_validation->set_rules('article[article_description]', 'Description', 'required');
            if ($this->form_validation->run() != FALSE) {
                $article_data['status'] = (isset($article_data['status'])) ? 1 : 0;
                $article_data['article_slug'] = url_title($article_data['article_title'], 'dash', true);
                if ($id) {
                    $stat = $this->common->update_data('tbl_articles', array('id' => $id), $article_data);
                } else {
                    $stat = $this->common->insert_data('tbl_articles', $article_data);
                }
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Article Saved Successfully.</div>');
                    redirect('admin/knowledgebase/article/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while saving the article. </div>');
                }
            } else {
                $data['article'] = $article_data;
            }
        }
        
        $article_categories = $this->common->fetch_where('tbl_article_categories', '*' , array('status' => 1));
        $data['article_categories'] = $article_categories;
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('article/form');
        $this->load->view('partials/footer');
    }

    public function manage_category() {
        $data = array();
        $article_categories = $this->common->fetch_where('tbl_article_categories', '*');
        $data['article_categories'] = $article_categories;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('article_category/manage');
        $this->load->view('partials/footer');
    }

    public function add_category($id = FALSE) {
        $data = array();
        $data['article_category'] =  array();

        if ($id) {
            $article_category = $this->common->fetch_where('tbl_article_categories', '*', array('id' => $id));
            if (empty($article_category)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Category Found with sepcified Id.</div>');
                redirect('admin/knowledgebase/article/manage_category');
            }
            $data['article_category'] = $article_category[0];
        }

        $article_category_data = $this->input->post('article_category');
        
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('article_category[category_name]', 'Name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $article_category_data['status'] = (isset($article_category_data['status'])) ? 1 : 0;
                $article_category_data['category_slug'] = url_title($article_category_data['category_name'], 'dash', true);
                if ($id) {
                    $stat = $this->common->update_data('tbl_article_categories', array('id' => $id), $article_category_data);
                } else {
                    $stat = $this->common->insert_data('tbl_article_categories', $article_category_data);
                }
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Category Saved Successfully.</div>');
                    redirect('admin/knowledgebase/article/manage_category');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while saving the category. </div>');
                }
            } else {
                $data['article_category'] = $article_category_data;
            }
        }
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('article_category/form');
        $this->load->view('partials/footer');
    }

}

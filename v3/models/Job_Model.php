<?php

class Job_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->jobcrm_db = $this->load->database('jobcrm_db', TRUE);
        date_default_timezone_set('Australia/Sydney');
    }


    function num_rows($table, $cond) {
        $this->jobcrm_db->where($cond);
        return $this->jobcrm_db->get($table)->num_rows();
    }
    
    /**
     *
     * @param type $table
     * @param type $select
     * @param type $cond
     * @return type
     */
    function fetch_where($table, $select = '*', $cond = NULL) {
        $this->jobcrm_db->select($select);
        $this->jobcrm_db->from($table);
        if ($cond !== NULL) {
            $this->jobcrm_db->where($cond);
        }
        return $this->jobcrm_db->get()->result_array();
    }


     /**
     *
     * @param type $table
     * @param type $select
     * @param type $cond
     * @return type
     */
    function fetch_row($table, $select = '*', $cond = NULL) {
        $this->jobcrm_db->select($select);
        $this->jobcrm_db->from($table);
        if ($cond !== NULL) {
            $this->jobcrm_db->where($cond);
        }
        $result = $this->jobcrm_db->get()->row_array();
        return $result ? $result : array();
    }

    /**
     * insert a row data
     * @param type $table
     * @param type $value
     * @return boolean
     */
    function insert_data($table, $value) {
        $this->jobcrm_db->insert($table, $value);
        return $this->jobcrm_db->affected_rows();
    }

    /**
     * update data
     * @param type $table
     * @param type $value
     * @return boolean
     */
    function update_data($table, $cond, $data) {
        $this->jobcrm_db->where($cond);
        $this->jobcrm_db->update($table, $data);
        return $this->jobcrm_db->affected_rows();
    }
    
    /**
     * for do insert or update in single query
     * @param type $table
     * @param type $cond
     * @return boolean
     */
    function insert_or_update($table, $cond, $data) {
        $count = $this->num_rows($table, $cond);
        if ($count > 0) {
            return $this->update_data($table, $cond, $data);
        } else {
            return $this->insert_data($table, $data);
        }
    }
    
   
    function insert_or_update_customer($cust_id,$booking_form_id) {
    	 try{
            $select = 'id,simpro_cust_id,title,company_name,position,first_name,last_name,customer_email,customer_contact_no,is_processed';

            $cust_data = $this->common->fetch_row('tbl_customers',$select,array('id' => $cust_id));

            if(empty($cust_data)){
                return false;
            }

	    	//Start Transaction
	    	$this->jobcrm_db->trans_begin();

	    	//Perform Queries
            //Check if customer already present in job crm
            $job_crm_cond = array('company_id' => 1,'crm_cust_id' => $cust_id,'booking_form_id' => $booking_form_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customers');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            
            /**if(empty($result)){
                //Check Customer in job crm by company name as that field is unique
                $job_crm_cond = array('company_id' => 1,'company_name' => $cust_data['company_name']);
                $this->jobcrm_db->select($select);
                $this->jobcrm_db->from('tbl_customers');
                $this->jobcrm_db->where($job_crm_cond);
                $result = $this->jobcrm_db->get()->row_array();
            }*/
	        
            unset($cust_data['id']);
            $cust_data['company_id'] = 1;
            $cust_data['crm_cust_id'] = $cust_id;
            if(empty($result)){
                $cust_data['booking_form_id'] = $booking_form_id;
                $this->jobcrm_db->insert('tbl_customers', $cust_data);
                $job_crm_cust_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customers', $cust_data);
                $job_crm_cust_id = $result['id'];
            }

	        //Check Transaction
	        if ($this->jobcrm_db->trans_status() === FALSE) {
	            $this->jobcrm_db->trans_rollback();
	            return false;
	        } else {
	            $this->jobcrm_db->trans_commit();
	            return $job_crm_cust_id;
	        }
	    }catch(Exception $e){
	    	return false;
	    }
    }

    function insert_or_update_customer_location($cust_id,$job_crm_cust_id,$booking_form_site_id) {
         try{
            $select = '*';
            
            if($booking_form_site_id != NULL){
                $cust_data = $this->common->fetch_row('tbl_customer_locations',$select,array('id' => $booking_form_site_id));
            }else{
                $cust_data = $this->common->fetch_row('tbl_customer_locations',$select,array('cust_id' => $cust_id));
            }
            
            

            if(empty($cust_data)){
                return false;
            }

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            //Check if customer location already present in job crm
            $job_crm_cond = array('cust_id' => $job_crm_cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customer_locations');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            
            unset($cust_data['id']);
            unset($cust_data['created_at']);
            $cust_data['cust_id'] = $job_crm_cust_id;
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customer_locations', $cust_data);
                $job_crm_cust_loc_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customer_locations', $cust_data);
                $job_crm_cust_loc_id = $result['id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_loc_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_job($booking_form_id,$job_data) {
         try{

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            $job_crm_cond = array('booking_form_id' => $booking_form_id,'job_type' => $job_data['job_type']);
            $this->jobcrm_db->select('job_id');
            $this->jobcrm_db->from('tbl_jobs');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            
            if(empty($result)){
                $job_data['created_at'] = date('Y-m-d H:i:s');
                
                $this->jobcrm_db->insert('tbl_jobs', $job_data);
                $job_id = $this->jobcrm_db->insert_id();
                
                $this->jobcrm_db->insert('tbl_job_status', 
                    array(
                        'status_id' => 1,
                        'sub_status_id' => 3,
                        'job_id' => $job_id,
                        'user_id' => 1
                    )
                );
                
            }else{
                unset($job_data['uuid']);
                $job_data['updated_at'] = date('Y-m-d H:i:s');
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_jobs', $job_data);
                $job_id = $result['job_id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_id;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    
    function insert_or_update_job_products($job_id,$job_type,$product_data) {
         try{
            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            $job_crm_cond = array('job_id' => $job_id);
            $this->jobcrm_db->select('id');
            $this->jobcrm_db->from('tbl_job_products');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
            
            if(!empty($result)){
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->delete('tbl_job_products');
            }

            if (!empty($product_data)) {
                $pd = json_decode(json_encode($product_data),TRUE);
                $product_keys = array_keys($pd);
                for ($i = 0; $i < count($product_data->item_id); $i++) {
                    if($product_data->item_id[$i] != NULL && $product_data->item_id[$i] != '' && $product_data->item_name[$i]!=''){
                        //$crm_product_data = $this->common->fetch_row('tbl_led_new_products','type_id',array('np_id' => $product_data->item_id[$i]));
                        $insert_data = array();
                        $insert_data['job_id'] = $job_id;
                        $insert_data['crm_prd_id'] = $product_data->item_id[$i];
                        //$insert_data['crm_prd_type_id'] = $crm_product_data['type_id'];
						$insert_data['crm_prd_type_id'] = $product_data->type_id[$i];
						$insert_data['crm_prd_sensor'] = $product_data->item_sensor[$i];
						
                        $insert_data['name'] = $product_data->item_name[$i];
                        $insert_data['qty'] = $product_data->item_qty[$i];
                        $item_data = [];
                        foreach($product_keys as $k => $v){
                            $item_data[$v] = $product_data->$v[$i];
                        }
                        if(!empty($item_data)){
                            $insert_data['item_data'] = json_encode($item_data);
                        }
                        
                        $this->jobcrm_db->insert('tbl_job_products', $insert_data);
                    }
                }
            }
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    function insert_or_update_job_submission_docs($job_id,$images) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            foreach($images as $key => $value){
                if(($key == 'ec_bill_1' || $key == 'ec_bill_2') && $value != ''){
                    $filename = ($key == 'ec_bill_1') ? 'PHOTO_OF_FRONT_ELECTRICITY_BILL' : 'PHOTO_OF_BACK_ELECTRICITY_BILL';
                    $ext = explode('.',$value);
                    $full_filename = $filename . '.' . $ext[1];
                    $job_crm_cond = array('job_id' => $job_id,'file_name' => $full_filename);
                    $this->jobcrm_db->select('media_id');
                    $this->jobcrm_db->from('tbl_job_attachments');
                    $this->jobcrm_db->where($job_crm_cond);
                    $result = $this->jobcrm_db->get()->row_array();
                    
                    $file = FCPATH . 'assets/uploads/led_booking_form_files/original/' . $value;
                    if (!file_exists($file)) {
                        $file = FCPATH . 'assets/uploads/led_booking_form_files/' . $value;
                    }
                    //$upload_path = 'D:\xampp\htdocs\job_crm/assets/uploads/media_gallery/'.$job_id;

                    $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/media_gallery/'.$job_id;

                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/large')) {
                       mkdir($upload_path.'/large', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/medium')) {
                        mkdir($upload_path.'/medium', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/small')) {
                       mkdir( $upload_path.'/small', 0777, TRUE);
                    }

                    $newfilename = $upload_path . '/large/' . $full_filename;

                    if(file_exists($newfilename)){
                        unlink($newfilename);
                    }

                    copy($file, $newfilename); 

                    //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                    $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/media_gallery/'.$job_id .'/';

                    if(empty($result)){
                        $insert_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                    }else{
                         $update_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->update_data('tbl_job_attachments', array('media_id' => $result['media_id']),$update_data);
                    }
                }
            }
            
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    
    function insert_or_update_heers_job_submission_docs($job_id,$images) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            foreach($images as $key => $value){
                if(($key == 'electricity_bill_front' || $key == 'electricity_bill_back') && $value != ''){
                    $filename = ($key == 'electricity_bill_front') ? 'PHOTO_OF_FRONT_ELECTRICITY_BILL' : 'PHOTO_OF_BACK_ELECTRICITY_BILL';
                    $ext = explode('.',$value);
                    $full_filename = $filename . '.' . $ext[1];
                    $job_crm_cond = array('job_id' => $job_id,'file_name' => $full_filename);
                    $this->jobcrm_db->select('media_id');
                    $this->jobcrm_db->from('tbl_job_attachments');
                    $this->jobcrm_db->where($job_crm_cond);
                    $result = $this->jobcrm_db->get()->row_array();
                    
                    $file = FCPATH . 'assets/uploads/led_booking_form_files/original/' . $value;
                    if (!file_exists($file)) {
                        $file = FCPATH . 'assets/uploads/led_booking_form_files/' . $value;
                    }
                    //$upload_path = 'D:\xampp\htdocs\job_crm/assets/uploads/media_gallery/'.$job_id;

                    $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/media_gallery/'.$job_id;

                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/large')) {
                       mkdir($upload_path.'/large', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/medium')) {
                        mkdir($upload_path.'/medium', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/small')) {
                       mkdir( $upload_path.'/small', 0777, TRUE);
                    }

                    $newfilename = $upload_path . '/large/' . $full_filename;

                    if(file_exists($newfilename)){
                        unlink($newfilename);
                    }

                    copy($file, $newfilename); 

                    //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                    $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/media_gallery/'.$job_id .'/';

                    if(empty($result)){
                        $insert_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                    }else{
                         $update_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 12
                        );
                        $this->update_data('tbl_job_attachments', array('media_id' => $result['media_id']),$update_data);
                    }
                }
            }
            
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }

        function insert_booking_form_activity_note($job_id,$job_data) {
            $booking_form_url = $job_data['booking_form_file'];
            $booking_form_data = $job_data['booking_form_data'];
            $booking_form_data = json_decode($booking_form_data);
            $sales_repname = '';
            if($booking_form_data != '' && $booking_form_data != NULL){
                if(isset($booking_form_data->authorised_by_behalf) && $booking_form_data->authorised_by_behalf != ''){
                    $sales_repname = $booking_form_data->authorised_by_behalf->name;
                }
            }
            try{    
                $activity_data = array();
                $activity_data['activity_type'] = 'Booking Form';
                $activity_data['activity_title'] = $sales_repname . ' ' . date("F d, Y");
                $activity_note  = "Booking Form:<br/> -  <a target='__blank' href='".$booking_form_url."'>".$booking_form_url."</a><br/><br/>";
                $activity_data['activity_note'] = $activity_note;
                $activity_data['job_id'] = $job_id;
                $activity_data['user_id'] = 1;
                $activity_data['status'] = 1;
                $activity_data['created_at'] = date('Y-m-d H:i:s');
                $this->jobcrm_db->insert('tbl_activities', $activity_data);
            }catch(Exception $e){
                return false;
            }

        }


    function get_job_details_from_job_crm($job_id) {
    

        $solar_kw = "(SELECT cfd.custom_field_value FROM tbl_job_custom_field_data as cfd WHERE cfd.custom_field_id=2 AND cfd.job_id = job.job_id)"; 
        
        $highbay_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 3 AND job.job_id = jp.job_id),0) as highbays_count";

        $floodlight_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 6 AND job.job_id = jp.job_id),0) as flood_lights_count";

        $panellight_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 5 AND job.job_id = jp.job_id),0) as panel_lights_count";

        $batten_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 4 AND job.job_id = jp.job_id),0) as batten_lights_count";

        $downlight_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 7 AND job.job_id = jp.job_id),0) as down_lights_count";

        $other_count = "Coalesce((SELECT SUM(jp.qty) FROM tbl_job_products as jp
            WHERE jp.crm_prd_type_id = 8 AND job.job_id = jp.job_id),0) as other_count";

        $sql = "SELECT job.job_id,
                    cc.name AS cost_center,
                    $highbay_count,
                    $floodlight_count,
                    $panellight_count,
                    $batten_count,
                    $downlight_count,
                    $other_count,
                    $solar_kw as solar_kw"
                . " FROM tbl_jobs as job";
        $sql .= " LEFT JOIN tbl_cost_centres AS cc ON job.cost_centre_id = cc.ccid";
        $sql .= " WHERE  job.job_id=".$job_id ;
        $query = $this->jobcrm_db->query($sql);
        return $query->row_array();
    }
    
    
    function get_products_for_po($select,$custom_cond) {
        $sql = "SELECT tp.*,$select,tp.id as item_id FROM tbl_products as tp";
        $sql .= " LEFT JOIN tbl_product_types as pt ON pt.type_id=tp.type_id";
        if ($custom_cond) {
            $sql .= $custom_cond;
        }
       
        $query = $this->jobcrm_db->query($sql);
        return $query->result_array();
    }
    
    
    
    /** Warranty job realted Functions */

    function insert_or_update_warranty_customer($cust_id,$cust_data) {
        try{
            $select = 'id,simpro_cust_id,title,company_name,position,first_name,last_name,customer_email,customer_contact_no,is_processed';
        
            //Start Transaction
            $this->jobcrm_db->trans_begin();
        
            //Perform Queries
            //Check if customer already present in job crm against simpro cust id fields
            $job_crm_cond = array('company_id' => 1,'web_cust_id' => $cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customers');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            $cust_data['company_id'] = 1;
            $cust_data['web_cust_id'] = $cust_id;
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customers', $cust_data);
                $job_crm_cust_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customers', $cust_data);
                $job_crm_cust_id = $result['id'];
            }
        
                    //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_customer_location($location_data,$job_crm_cust_id) {
        try{
            $select = '*';
        
            //Start Transaction
            $this->jobcrm_db->trans_begin();
        
            //Perform Queries
            //Check if customer location already present in job crm
            $job_crm_cond = array('cust_id' => $job_crm_cust_id);
            $this->jobcrm_db->select($select);
            $this->jobcrm_db->from('tbl_customer_locations');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            $location_data['cust_id'] = $job_crm_cust_id;
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_customer_locations', $location_data);
                $job_crm_cust_loc_id = $this->jobcrm_db->insert_id();
            }else{
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_customer_locations', $location_data);
                $job_crm_cust_loc_id = $result['id'];
            }
        
                    //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_crm_cust_loc_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_job($cust_id,$job_data) {
        try{

            $this->jobcrm_db->trans_begin();
        
            $job_crm_cond = array('cust_id' => $cust_id,'job_type' => $job_data['job_type'],'cost_centre_id' => 6);
            $this->jobcrm_db->select('job_id');
            $this->jobcrm_db->from('tbl_jobs');
            $this->jobcrm_db->where($job_crm_cond);
            $result = $this->jobcrm_db->get()->row_array();
        
            if(empty($result)){
                $this->jobcrm_db->insert('tbl_jobs', $job_data);
                $job_id = $this->jobcrm_db->insert_id();
        
                $this->jobcrm_db->insert('tbl_job_status', 
                    array(
                        'status_id' => 1,
                        'sub_status_id' => 3,
                        'job_id' => $job_id,
                        'user_id' => 1
                    )
                );
        
            }else{
                unset($job_data['uuid']);
                $this->jobcrm_db->where($job_crm_cond);
                $this->jobcrm_db->update('tbl_jobs', $job_data);
                $job_id = $result['job_id'];
            }

            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return $job_id;
            }
        }catch(Exception $e){
            return false;
        }
    }

    function insert_or_update_warranty_job_docs($job_id,$images) {
       try{
    
            $this->jobcrm_db->trans_begin();
    
            foreach($images as $key => $value){
                if($value['file_name'] != '' && $value['file_name'] != NULL){
        
                    $media_cat_id = '';
                    if($value['name'] == 'Faulty Light'){
                        $media_cat_id = 15;
                    }else if($value['name'] == 'Model No and Wattage of Fitting'){
                        $media_cat_id = 16;
                    }else if($value['name'] == 'Certificate of Electrical Safety'){
                        $media_cat_id = 1;
                    }else if($value['name'] == 'Invoice'){
                        $media_cat_id = 8;
                    }
        
                    $filename = $value['file_name'];
                    $ext = explode('.',$filename);
                    $base_url = 'https://kugacrm.com.au/assets/uploads/warranty_claim_files/';
        
                    $insert_data = array(
                        'base_url' => $base_url,
                        'original_name' => $value['file_name'],
                        'file_name' => $value['file_name'],
                        'file_path' => $job_id,
                        'file_size' => 0,
                        'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                        'module' => '',
                        'job_id' => $job_id,
                        'media_cat_id' => $media_cat_id
                    );
                    $this->jobcrm_db->insert('tbl_job_attachments', $insert_data);
                }
            }
    
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    function insert_or_update_job_crm_user($user_id,$job_crm_password) {
        try{

            $this->jobcrm_db->trans_begin();
    
            $aauth_user_data = $this->common->fetch_row('aauth_users','*',array('id' => $user_id));
    
            if(!empty($aauth_user_data)){
                
                //Check if User Exitis with same email on Job CRM
                $this->jobcrm_db->select('id');
                $this->jobcrm_db->from('aauth_users');
                $this->jobcrm_db->where(array('email' => $aauth_user_data['email']));
                $result = $this->jobcrm_db->get()->num_rows();
                
                if($result > 0){
                    return 'Email Exists';
                }
                    
                $user_details = $this->common->fetch_row('tbl_user_details','*',array('user_id' => $user_id));
                $user_locations = $this->common->fetch_row('tbl_user_locations','*',array('user_id' => $user_id));
                $job_crm_user_id = '';
    
                if($user_details['job_crm_user_id'] == NULL || $user_details['job_crm_user_id'] == ''){
                    //Create Aauth Table Data
                    unset($aauth_user_data['id']);
                    unset($aauth_user_data['banned']);
                    $this->jobcrm_db->insert('aauth_users',$aauth_user_data);
                    $job_crm_user_id = $this->jobcrm_db->insert_id();
    
                    $this->load->library("Aauth");
                    $job_crm_password = $this->aauth->hash_password($job_crm_password,$job_crm_user_id);
                    $this->jobcrm_db->where(array('id' => $job_crm_user_id));
                    $this->jobcrm_db->update('aauth_users',array('pass' => $job_crm_password));
                    
                    //Create User Details Table Data
                    unset($user_details['id']);
                    unset($user_details['user_id']);
                    unset($user_details['job_crm_user_id']);
                    $user_details['user_id'] = $job_crm_user_id;
                    $user_details['kuga_crm_user_id'] = $user_id;
                    $full_name = $user_details['full_name'];
                    $full_name = explode(' ',$full_name);
                    $user_details['first_name'] = (count($full_name) > 0) ? $full_name[0] : '';
                    $user_details['last_name'] = (count($full_name) > 1) ? $full_name[1] : '';
                    $this->jobcrm_db->insert('tbl_user_details', $user_details);
    
                    //Create User Locations Table Data
                    unset($user_locations['id']);
                    unset($user_locations['user_id']);
                    $user_locations['user_id'] = $job_crm_user_id;
                    $this->jobcrm_db->insert('tbl_user_locations', $user_locations);
    
                    //Create User Group
                    $this->jobcrm_db->insert('aauth_user_to_group',array('group_id' => 10, 'user_id' => $job_crm_user_id));
    
                    $this->common->update_data('tbl_user_details',array('user_id' => $user_id),array('job_crm_user_id' => $job_crm_user_id));
                }else{
                    $job_crm_user_id = $user_details['job_crm_user_id'];
                    
                    //Create User Details Table Data
                    unset($user_details['id']);
                    unset($user_details['user_id']);
                    $this->jobcrm_db->where(array('user_id' => $job_crm_user_id));
                    $this->jobcrm_db->update('tbl_user_details', $user_details);
    
                    //Create User Locations Table Data
                    unset($user_locations['id']);
                    unset($user_locations['user_id']);
                    $this->jobcrm_db->where(array('user_id' => $job_crm_user_id));
                    $this->jobcrm_db->insert('tbl_user_locations', $user_locations);
                }
    
                if ($this->jobcrm_db->trans_status() === FALSE) {
                    $this->jobcrm_db->trans_rollback();
                    return false;
                } else {
                    $this->jobcrm_db->trans_commit();
                    return $job_crm_user_id;
                }
            }
            return false;
        }catch(Exception $e){
            return false;
        }
    }
    
    public function get_job_status($job_id,$cond = FALSE){
        $sql = "SELECT js.*,stl.name as status_name,sstl.name as sub_status_name FROM tbl_job_status as js";
        $sql .= " LEFT JOIN tbl_status_list as stl ON stl.sid = js.status_id";
        $sql .= " LEFT JOIN tbl_status_list as sstl ON sstl.sid = js.sub_status_id";
        $sql .= " WHERE js.job_id=". $job_id . " $cond ORDER BY js.id DESC";
        $query = $this->jobcrm_db->query($sql);
        return $query->row_array();
    }
    
    
    function insert_or_update_solar_bill($job_id,$images,$electricity_bill) {
         try{

            $images = json_decode(json_encode($images),true);

            //Start Transaction
            $this->jobcrm_db->trans_begin();

            //Perform Queries
            foreach($images as $key => $value){
                if(($key == 'ec_bill_1' || $key == 'ec_bill_2') && $value != ''){
                    $filename = ($key == 'ec_bill_1') ? 'PHOTO_OF_ELECTRICITY_BILL_1' : 'PHOTO_OF_ELECTRICITY_BILL_2';
                    $ext = explode('.',$value);
                    $full_filename = $filename . '.' . $ext[1];
                    $job_crm_cond = array('job_id' => $job_id,'file_name' => $full_filename);
                    $this->jobcrm_db->select('media_id');
                    $this->jobcrm_db->from('tbl_job_solar_attachments');
                    $this->jobcrm_db->where($job_crm_cond);
                    $result = $this->jobcrm_db->get()->row_array();
                    
                    $file = FCPATH . 'assets/uploads/solar_booking_form_files/original/' . $value;
                    if (!file_exists($file)) {
                        $file = FCPATH . 'assets/uploads/solar_booking_form_files/' . $value;
                    }

                    $upload_path = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_media_gallery/'.$job_id;

                    if (!is_dir($upload_path)) {
                        mkdir($upload_path, 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/large')) {
                       mkdir($upload_path.'/large', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/medium')) {
                        mkdir($upload_path.'/medium', 0777, TRUE);
                    }
                    if (!is_dir( $upload_path.'/small')) {
                       mkdir( $upload_path.'/small', 0777, TRUE);
                    }

                    $newfilename = $upload_path . '/large/' . $full_filename;

                    if(file_exists($newfilename)){
                        unlink($newfilename);
                    }

                    copy($file, $newfilename); 

                    //$base_url = 'http://localhost/job_crm/assets/uploads/media_gallery/'.$job_id .'/';
                    $base_url = 'https://kugacrm.com.au/job_crm/assets/uploads/solar_media_gallery/'.$job_id .'/';

                    if(empty($result)){
                        $insert_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 4
                        );
                        $this->jobcrm_db->insert('tbl_job_solar_attachments', $insert_data);
                    }else{
                         $update_data = array(
                            'base_url' => $base_url,
                            'original_name' => $value,
                            'file_name' => $full_filename,
                            'file_path' => $job_id,
                            'file_size' => 0,
                            'mime_type' => ($ext[1] == 'jpg') ? 'jpeg' : $ext[1],
                            'module' => '',
                            'job_id' => $job_id,
                            'media_cat_id' => 4
                        );
                        $this->update_data('tbl_job_solar_attachments', array('media_id' => $result['media_id']),$update_data);
                    }
                }
            }
            
            if(isset($electricity_bill) && isset($electricity_bill->eb_nmi)){
                $cid = 4;
                $key = $this->input->get('key');
                $value = $this->input->get('value');
                $user_id = 1;
                $cdata = json_encode(array('nmi' => $electricity_bill->eb_nmi, 'meter_no' => $electricity_bill->eb_meter_no));
                $scs_data = $this->fetch_row('tbl_job_solar_attachment_category_status','id',array('cat_id' => $cid, 'job_id' => $job_id));
                if(empty($scs_data)){
                    $insert_data = array();
                    $insert_data['cat_id'] = $cid;
                    $insert_data['job_id'] = $job_id;
                    $insert_data['user_id'] = 1;
                    $insert_data['status'] = 3;
                    $insert_data['data'] = $cdata;
                    $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
                }else{
                    $update_data = array();
                    $update_data['data'] = $cdata;
                    $update_data['status'] = 3;
                    $this->update_data('tbl_job_solar_attachment_category_status',array('job_id' => $job_id,'cat_id' => $cid),$update_data);
                }
            }
            //Check Transaction
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
        }catch(Exception $e){
            return false;
        }
    }
    
    
    function insert_or_update_solar_attachments($job_id, $product,$uuid){
        
        // 4,5,6,7,8
        
        try{
            $this->jobcrm_db->trans_begin();
            
            $this->edcrm_db = $this->load->database('edcrm_db',TRUE);
            
            $upload_path_img3 = $upload_path_img4 = $upload_path_img5 = '';
            $upload_path_img6 = $upload_path_img7 = $upload_path_img8 = $upload_path_img9 = $upload_path_img10 = '';
            for($i = 0; $i < count($product->product_name); $i++){
                    
                if($product->product_type[$i] == 'Solar Panels' && $product->product_id[$i] != ''){
                    $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $product->product_id[$i], 'media_cat_id' => 1]);
                    if(!empty($attachment)){
                        $panel_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $panle_file = 'panel_sheet_'.$uuid;
                        $upload_path_img3 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$panle_file . '.pdf';
                        if(!file_exists($upload_path_img3)){
                            file_put_contents($upload_path_img3, file_get_contents($panel_attachment));
                        }
                    }
                    
                    $product_id = $product->product_id[$i];
                    $ted_crm_id = $this->common->fetch_cell('tbl_products','ted_crm_id',['id' => $product_id]);
                    
                    if($upload_path_img3 == ''){
                        $attachment = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 1])->get('tbl_product_attachments')->row_array();
                        $panel_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $panle_file = 'panel_sheet_'.$uuid;
                        $upload_path_img3 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$panle_file . '.pdf';
                        if(!file_exists($upload_path_img3)){
                            file_put_contents($upload_path_img3, file_get_contents($panel_attachment));
                        }
                    }
                    
                    
                    $panel_installation = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 4])->get('tbl_product_attachments')->row_array();
                    if(!empty($panel_installation)){
                        $panel_installation_attachment = $panel_installation['base_url'].'large/'.$panel_installation['file_name'];
                        $panle_installation_file = 'panel_installation_manual_'.$uuid;
                        $upload_path_img6 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$panle_installation_file . '.pdf';
                        if(!file_exists($upload_path_img6)){
                            file_put_contents($upload_path_img6, file_get_contents($panel_installation_attachment));
                        }
                    }
                }
                
                if($product->product_type[$i] == 'Inverters' && $product->product_id[$i] != ''){
                    $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $product->product_id[$i], 'media_cat_id' => 1]);
                    if(!empty($attachment)){
                        $inverter_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $inverter_file = 'inverter_sheet_'.$uuid;
                        $upload_path_img4 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$inverter_file . '.pdf';
                        if(!file_exists($upload_path_img4)){
                            file_put_contents($upload_path_img4, file_get_contents($inverter_attachment));
                        }
                    }
                    
                    $product_id = $product->product_id[$i];
                    $ted_crm_id = $this->common->fetch_cell('tbl_products','ted_crm_id',['id' => $product_id]);
                    
                    if($upload_path_img4 == ''){
                        $attachment = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 1])->get('tbl_product_attachments')->row_array();
                        $inverter_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $inverter_file = 'inverter_sheet_'.$uuid;
                        $upload_path_img4 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$inverter_file . '.pdf';
                        if(!file_exists($upload_path_img4)){
                            file_put_contents($upload_path_img4, file_get_contents($inverter_attachment));
                        }
                    }
                    
                    $inverter_installation = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 5])->get('tbl_product_attachments')->row_array();
                    if(!empty($inverter_installation)){
                        $inverter_installation_attachment = $inverter_installation['base_url'].'large/'.$inverter_installation['file_name'];
                        $inverter_installation_file = 'inverter_installation_manual_'.$uuid;
                        $upload_path_img7 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$inverter_installation_file . '.pdf';
                        if(!file_exists($upload_path_img7)){
                            file_put_contents($upload_path_img7, file_get_contents($inverter_installation_attachment));
                        }
                    }
                    
                    $inverter_wifi = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 6])->get('tbl_product_attachments')->row_array();
                    if(!empty($inverter_wifi)){
                        $inverter_wifi_attachment = $inverter_wifi['base_url'].'large/'.$inverter_wifi['file_name'];
                        $inverter_wifi_file = 'inverter_wifi_manual_'.$uuid;
                        $upload_path_img9 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$inverter_wifi_file . '.pdf';
                        if(!file_exists($upload_path_img9)){
                            file_put_contents($upload_path_img9, file_get_contents($inverter_wifi_attachment));
                        }
                    }
                }
                
                if($product->product_type[$i] == 'Battery' && $product->product_id[$i] != ''){
                    $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $product->product_id[$i], 'media_cat_id' => 1]);
                    if(!empty($attachment)){
                        $battery_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $battery_file = 'battery_sheet_'.$uuid;
                        $upload_path_img5 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$battery_file . '.pdf';
                        if(!file_exists($upload_path_img5)){
                            file_put_contents($upload_path_img5, file_get_contents($battery_attachment));
                        }
                    }
                    
                    $product_id = $product->product_id[$i];
                    $ted_crm_id = $this->common->fetch_cell('tbl_products','ted_crm_id',['id' => $product_id]);
                    
                    if($upload_path_img5 == ''){
                        $attachment = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 1])->get('tbl_product_attachments')->row_array();
                        $battery_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                        $battery_file = 'battery_sheet_'.$uuid;
                        $upload_path_img5 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$battery_file . '.pdf';
                        if(!file_exists($upload_path_img5)){
                            file_put_contents($upload_path_img5, file_get_contents($battery_attachment));
                        }
                    }
                    
                    $battery_installation = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 7])->get('tbl_product_attachments')->row_array();
                    if(!empty($battery_installation)){
                        $battery_installation_attachment = $battery_installation['base_url'].'large/'.$battery_installation['file_name'];
                        $battery_installation_file = 'battery_installation_manual_'.$uuid;
                        $upload_path_img8 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$battery_installation_file . '.pdf';
                        if(!file_exists($upload_path_img8)){
                            file_put_contents($upload_path_img8, file_get_contents($battery_installation_attachment));
                        }
                    }
                    
                    $battery_wifi = $this->edcrm_db->select('base_url,file_name')->where(['product_id' => $ted_crm_id, 'media_cat_id' => 8])->get('tbl_product_attachments')->row_array();
                    if(!empty($battery_wifi)){
                        $battery_wifi_attachment = $battery_wifi['base_url'].'large/'.$battery_wifi['file_name'];
                        $battery_wifi_file = 'battery_wifi_manual_'.$uuid;
                        $upload_path_img10 = '/home/kugacrm/public_html/job_crm/assets/uploads/solar_attachment_files/large/'.$battery_wifi_file . '.pdf';
                        if(!file_exists($upload_path_img10)){
                            file_put_contents($upload_path_img10, file_get_contents($battery_wifi_attachment));
                        }
                    }
                }
            }
                
            $upload_path_base_url = site_url() . 'job_crm/assets/uploads/solar_attachment_files/';
            if(!empty($upload_path_img3)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $panle_file . '.pdf',
                  'file_name' => $panle_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img3),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 6
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 6;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img4)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $inverter_file . '.pdf',
                  'file_name' => $inverter_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img4),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 8
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 8;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img5)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $battery_file . '.pdf',
                  'file_name' => $battery_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img5),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 10
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 10;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img6)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $panle_installation_file . '.pdf',
                  'file_name' => $panle_installation_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img6),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 7
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 7;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img7)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $inverter_installation_file . '.pdf',
                  'file_name' => $inverter_installation_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img7),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 9
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 9;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img8)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $battery_installation_file . '.pdf',
                  'file_name' => $battery_installation_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img8),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 11
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 11;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img9)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $inverter_wifi_file . '.pdf',
                  'file_name' => $inverter_wifi_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img9),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 22
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 22;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            
            if(!empty($upload_path_img10)){
                $media_id = $insert_id = $this->insert_data('tbl_job_solar_attachments', [
                  'base_url' => $upload_path_base_url,
                  'original_name' => $battery_wifi_file . '.pdf',
                  'file_name' => $battery_wifi_file . '.pdf',
                  'file_path' => $job_id,
                  'file_size' => $this->getFileSize($upload_path_img10),
                  'mime_type' => 'application/pdf',
                  'job_id' => $job_id,
                  'media_cat_id' => 23
                ]);
                
                $insert_data = array();
                $insert_data['cat_id'] = 23;
                $insert_data['job_id'] = $job_id;
                $insert_data['user_id'] = 1;
                $insert_data['status'] = 3;
                $this->insert_data('tbl_job_solar_attachment_category_status',$insert_data);
            }
            if ($this->jobcrm_db->trans_status() === FALSE) {
                $this->jobcrm_db->trans_rollback();
                return false;
            } else {
                $this->jobcrm_db->trans_commit();
                return true;
            }
            
        }catch(Exception $e){
            log_message("error","error occured while inserting solar attachments Error: ".$e->getMessage());
            return false;
        }
    }
    
    function getFileSize($path) {
        $size = filesize($path);
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }
}

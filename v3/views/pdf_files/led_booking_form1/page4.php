<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35" class="red" style="color:#c32027; font-size:16px; font-family:Arial, Helvetica, sans-serif;"><strong>Payment Options</strong></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                    <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center">
                                                <?php if (isset($booking_form->is_upfront)) { ?>
                                                    <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                                <?php } else { ?>
                                                    <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                                <?php } ?>
                                            </td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront</td>
                                        </tr>
                                    </table>

                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Payment Upon Completion</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->upfront_excGST; ?></strong></td>
                            </tr>
                            <tr>
                                <td height="35">&nbsp;</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">GST</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->upfront_GST; ?></strong></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->upfront_incGST; ?></strong></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center">
                                                <?php if (isset($booking_form->is_finance)) { ?>
                                                    <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                                <?php } else { ?>
                                                    <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                                <?php } ?>
                                            </td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 2 - Finance Through Energy Ease</td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Payment Upon Completion</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->finance_excGST; ?></strong></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">GST</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->finance_GST; ?></strong></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->finance_incGST; ?></strong></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="60">&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c42027"><strong>Terms &amp; Conditions</strong></td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000">When the agreement is formed the agreement existing between you and Kuga Electrical. The order form is completed by the Sales Agent, the form is signed by you and we receive appoval to proceed. Signing of the order form is evidence of your agreement to the bound by the agreement.</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="10" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td colspan="2" height="35" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px; padding-left:15px"><strong>ADDITIONAL CHARGES APPLY ON DAY OF INSTALLATION FOR THE BELOW REPLACEMENTS</strong></td>
                            </tr>
                            <tr>
                                <td width="506" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">Call out fee if a return visit is required</td>
                                <td width="324" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">$270 exc GST</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif;font-size:14px">Surface mount fitting replacement-fully installed</td>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">$130 exc GST</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">Troffer fitting replacement-fully installed</td>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">$130 exc GST</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">IP65 surface mount fitting replacement - fully installed</td>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">$150 exc GST</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">Adding plug base to existing wiring for high bay -  fully installed</td>
                                <td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px">$150 exc GST</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="50">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="830" border="0" align="center" cellpadding="5" cellspacing="0">
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000">The Electrician on the day will advise of fees before doing work. The above list the main replacements however it is not the full list. All work wil be quoted and require approval</td>
                            </tr>
                        </table></td>
                </tr>
    </tr>
</table></td>    </tr>
<tr>
    <td height="155">&nbsp;</td>
</tr>
<tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                        </tr>
                        <tr>
                            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                        </tr>
                    </table></td>
                <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                        </tr>
                    </table></td>
                <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />4</td>
            </tr>
        </table></td>
</tr>
</table>
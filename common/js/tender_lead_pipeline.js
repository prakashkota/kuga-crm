
var lead_pipeline_manager = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.is_lead_allocation_sales_rep = (options.is_lead_allocation_sales_rep) ? options.is_lead_allocation_sales_rep : '';
    this.filters = '';
    this.initial_limit = 30;
    this.limit = 100;
    this.page = 1;
    this.loading = false;
    this.stage_selections = [];
    this.localStorage = window.localStorage;
    this.electricity_bill_uploaded = false;
    this.current_card_item_ui = null;
    
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        self.page = 1;
        $('#lead_list_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById('start_date').value = start.format('YYYY-MM-DD');
        document.getElementById('end_date').value = end.format('YYYY-MM-DD');
        var data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.initial_limit;
        self.fetch_stage_list(data);
    }
    
    var currentDate = moment();
    var weekStart = currentDate.clone().startOf('isoWeek');
    var weekEnd = currentDate.clone().endOf('isoWeek');
    
    var thisWeek = [];
    var lastWeek = [];
    thisWeek.push(moment(weekStart).add(0, 'days'));
    thisWeek.push(moment(weekStart).add(6, 'days'));
    lastWeek.push(moment(weekStart).add(-7, 'days'));
    lastWeek.push(moment(weekStart).add(-1, 'days'));

    $('#lead_list_range').daterangepicker({
        numberOfMonths: 6,
        startDate: start,
        endDate: end,
        ranges: {
            'All': [moment('2016-01-01'), moment()],
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'This Week': thisWeek,
            'Last Week': lastWeek,
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#columnSelector').multiselect({
        onChange: function (option, checked) {
            var elementId = option[0].value;
            if (elementId == 'select_all') {
                var checkboxes = $('ul.multiselect-container li input[type=checkbox]');
                checkboxes.each(function (e) {
                    if ($(this).val() != 'select_all') {
                        if (checked) {
                            $(this).prop('checked', true);
                            $('#stage_' + $(this).val()).show();
                            $('#stage_header_' + $(this).val()).show();
                            if(self.stage_selections.indexOf($(this).val()) == -1){
                                self.stage_selections.push($(this).val());
                            }
                        } else {
                            $(this).prop('checked', false);
                            $('#stage_' + $(this).val()).hide();
                            $('#stage_header_' + $(this).val()).hide();
                            var index = self.stage_selections.indexOf($(this).val());
                            if (index > -1) {
                                self.stage_selections.splice(index, 1);
                            }
                        }
                    }
                });
            } else {
                if (checked) {
                    $('ul.multiselect-container li:first input[type=checkbox]').prop('checked', false);
                    $('#stage_' + elementId).show();
                    $('#stage_header_' + elementId).show();
                    if(self.stage_selections.indexOf(elementId) == -1){
                        self.stage_selections.push(elementId);
                    }
                } else {
                    $('ul.multiselect-container li:first input[type=checkbox]').prop('checked', false);
                    $('#stage_' + elementId).hide();
                    $('#stage_header_' + elementId).hide();
                    var index = self.stage_selections.indexOf(elementId);
                    if (index > -1) {
                        self.stage_selections.splice(index, 1);
                    }
                }
            }
            self.handle_stage_selections_to_LocalStorage();
        }
    });
    
    var localStorage_LastFilter = self.localStorage.getItem('tender_lead_last_filter_' + self.user_id);
    data = (localStorage_LastFilter != '' && localStorage_LastFilter != null) ? localStorage_LastFilter : 'view=pipeline&page=' + self.page + '&limit=' + self.initial_limit;
    self.fetch_stage_list(data);
    
    if(localStorage_LastFilter != '' && localStorage_LastFilter != null){
        var data_object = JSON.parse('{"' + decodeURI(localStorage_LastFilter).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
        //Set Values to filter 
        for(var key in data_object){
            if(key == 'filter[custom][userid]'){
                $('#userid').val(data_object[key]);
            }else if(key == 'filter[custom][lead_type]'){
                $("input.lead_type[value='" + data_object[key] + "']").prop('checked', true);
            }else if(key == 'filter[custom][lead_source]'){
                $("input.lead_source[value='" + data_object[key] + "']").prop('checked', true);
            }else if(key == 'filter[start_date]'){
                document.getElementById('start_date').value = data_object[key];
                self.is_date_filter = true;
            }else if(key == 'filter[end_date]'){
                document.getElementById('end_date').value = data_object[key];
            }
        }
    }

    $("input[name='filter[custom]']").change(function () {
        self.page = 1;
        var data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.initial_limit;
        self.fetch_stage_list(data);
    });

    $('#userid,#state_id,.lead_type,#la_userid,.lead_source').change(function () {
        self.page = 1;
        var data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.initial_limit;
        self.fetch_stage_list(data);
    });

    $('.lists_data__hasScrollbar').on('scroll', function () {
        var div = $(this).get(0);
        if (div.scrollTop + div.clientHeight >= div.scrollHeight) {
            if (!self.loading) {
                self.loading = true;
                self.page = self.page + 1;
                var data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.limit;
                self.fetch_more_stage_list(data);
            }
        }
    });

    $('body').on('click', '#load_more_deals', function (e) {
        if (!self.loading) {
            self.loading = true;
            self.page = self.page + 1;
            var data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.limit;
            self.fetch_more_stage_list(data);
        }
    });
    
    $('.reset_filter').click(function(){
        var filter_type = $(this).attr('data-id');
        $("."+filter_type).prop("checked",false); 
        $("."+filter_type).trigger("change");
    });
    
    $('#search_leads').change(function () {
        var search_val = $(this).val();
        self.page = 1;
        self.keyword = search_val;
        var data = '';
        if(search_val !=''){
            data = 'keyword=' + encodeURIComponent(search_val)  + '&view=pipeline&page='+self.page;
        }else{
            data = $('#lead_list_filters').serialize() + '&view=pipeline&page=' + self.page + '&limit=' + self.initial_limit;
        }
        self.fetch_stage_list(data);
    });
}

lead_pipeline_manager.prototype.handle_stage_selections_to_LocalStorage = function () {
    var self = this;
    console.log(self.stage_selections);
    self.localStorage.setItem('tender_stage_selection_' + self.user_id, JSON.stringify(self.stage_selections));
};

lead_pipeline_manager.prototype.create_stage_list = function (data) {
    var self = this;

    var div_list = document.createElement('div');
    if (self.is_lead_allocation_sales_rep == '1') {
        var la_arrow = (data.id < 4) ? 'list list_orange' : 'list'
        div_list.className = la_arrow;
    } else {
        div_list.className = "list";
    }
    div_list.setAttribute('id', 'stage_header_' + data.id);

    var div_list_header = document.createElement('header');
    div_list_header.innerHTML = data.stage_name + '<span class="" style="float:right;">'+data.count+'</span>';

    var div_list_ul = document.createElement('div');
    div_list_ul.className = "connectedSortable stage_list";
    div_list_ul.setAttribute('id', 'stage_' + data.id);
    div_list_ul.setAttribute('data-id', data.id);
    div_list_ul.setAttribute('data-name', data.stage_name);
    //var div_list_li = document.createElement('li');
    //div_list_li.setAttribute('onclick', 'loadNoteDetails(' + JSON.stringify(data) + ');');
    //div_list_li.innerHTML = data.notes_point;
    var div_list_footer = document.createElement('footer');
    //var addCardAction = document.createElement('a');
    //addCardAction.setAttribute('onclick', 'addNewTabPoint("' + uid + '","' + notes_tab + '")');
    //addCardAction.setAttribute('href', 'javascript:void(0);');
    //addCardAction.innerHTML = "Add a point..";
    //div_list_footer.appendChild(addCardAction);
    div_list.appendChild(div_list_header);
    document.querySelector('#stages_data').appendChild(div_list_ul);
    //div_list.appendChild(div_list_footer);
    return div_list;
}

lead_pipeline_manager.prototype.create_stage_list_item = function (data) {
    var self = this;
    var stage_list_ele;
    var stage = (data.lead_stage == '' || data.lead_stage == null || data.lead_stage == 'null') ? '1' : data.lead_stage;
    if (stage !== '' || stage !== null || stage !== 'null') {
        stage_list_ele = document.querySelector('#stage_' + stage);
    } else {
        return false;
    }

    if (typeof (stage_list_ele) != 'undefined' && stage_list_ele != null) {
        //var color = (data.activity_last_created_days != null && data.activity_last_created_days >= 5) ? 'bg-danger text-white' : '';
        var color = (data.is_viewed == '0' && !self.is_lead_allocation_sales_rep) ? 'bg-danger text-white' : '';
        color = (data.deal_won_lost != null && data.deal_won_lost != '') ? '' : color;
        if (data.closing_date != null && parseInt(stage) == 1) {
            var now = moment().format("YYYY-MM-DD");
            var end = moment(data.closing_date).format("YYYY-MM-DD");
            
            now = moment(now, "YYYY-MM-DD");
            end = moment(end, "YYYY-MM-DD");
  
            var diff = end.diff(now, 'days');
            if(diff == 0){
                color = 'bg-light_red text-white';
            }else if(diff == 1){
                 color = 'bg-warning text-white';
            }
        }else if(data.award_date != null && parseInt(stage) == 2){
            var now = moment().format("YYYY-MM-DD");
            var end = moment(data.award_date).format("YYYY-MM-DD");
                
            now = moment(now, "YYYY-MM-DD");
            end = moment(end, "YYYY-MM-DD");
      
            var diff = end.diff(now, 'days');
            if(diff > 0){
                color = 'bg-light_red text-white';
            }
        }

        var div_list_item = document.createElement('div');
        div_list_item.setAttribute('id', data.uuid);
        div_list_item.setAttribute('proposal_id', data.proposal_id);
        div_list_item.setAttribute('lead_id', data.id);
        div_list_item.setAttribute('data-lead_type', data.lead_type);
        div_list_item.className = "stage_list__item ui-state-default " + color;
        //div_list_li.setAttribute('onclick', 'loadNoteDetails("edit","' + itemCounter + '");');
        var div_list_item_link = document.createElement('a');
        div_list_item_link.href = base_url + 'admin/tender-lead/add?deal_ref=' + data.uuid;
        div_list_item_link.setAttribute('style', 'text-decoration:none; color: inherit;');
        div_list_item_link.innerHTML = '<i class="fas fa-chevron-circle-right pull-right"></i>';


        var div_list_item_heading = document.createElement('h2');
        div_list_item_heading.innerHTML = data.project_name + ' - ' + data.solar_system_size + ' kW';
        div_list_item_heading.appendChild(div_list_item_link);

        //var state = (data.customer_state != null && data.customer_state != '') ? data.customer_state : 'Unknown';
        //var postcode = (data.customer_postcode != null && data.customer_postcode != '') ? data.customer_postcode : 'No Postcode';

        /**var div_list_item_body = document.createElement('span');
        //div_list_item_body.innerHTML = 'LED: $' + data.total_led + '<br/> Solar: $' + data.total_solar + '<br/>' + state + '|' + postcode;
        var sys_size = (data.total_system_size != null && data.total_system_size != 0.00) ? data.total_system_size : 0;
        if(sys_size == 0){
            sys_size = (data.total_system_size_proposal != null && data.total_system_size_proposal != 0.00) ? data.total_system_size_proposal : 0;
        }
        var bat_size = (data.total_battery_size != null && data.total_battery_size != 0.00) ? data.total_battery_size : 0;
        div_list_item_body.innerHTML = 'Solar: ' + sys_size + ' kW<br/>' + 'Battery: ' + bat_size + ' kW<br/>';*/
    
        //input.value = notes_tab;
        div_list_item.appendChild(div_list_item_heading);
        //div_list_item.appendChild(div_list_item_body);

        stage_list_ele.appendChild(div_list_item);
    }
}

lead_pipeline_manager.prototype.fetch_stage_list = function (data) {
    var self = this;
    self.localStorage.setItem('tender_lead_last_filter_' + self.user_id, data);
    $.ajax({
        url: base_url + "admin/tender-lead/fetch_lead_stage_data",
        type: 'get',
        dataType: "json",
        data: data,
        beforeSend: function () {
            document.getElementById('stages_data').innerHTML = '';
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
            document.getElementById('stages_data').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('stages').innerHTML = '';
            document.getElementById('stages_data').innerHTML = '';

            if (data.success == false) {
                toastr['error'](data.status);
                window.location.reload();
                return false;
            }
            
            //Populate Left Lead Count in Lead More Button
            var total_leads = parseInt(data.meta.total);
            var leads_visited_count = parseInt(data.meta.curr_page) * parseInt(data.meta.per_page);
            var left_lead_count = total_leads - leads_visited_count;
            if (left_lead_count > 0) {
                $('#load_more_deals').show();
            } else {
                $('#load_more_deals').hide();
            }
            
            if (data.proposal_stages.length > 0) {
                window.scrollTo({top: 0, behavior: 'smooth'});
                for (var i = 0; i < data.proposal_stages.length; i++) {
                    var list = self.create_stage_list(data.proposal_stages[i]);
                    document.querySelector('#stages').appendChild(list);
                }
                
                
                //Get Stage Selection from Localstorage
                var localStorage_StageSelections = self.localStorage.getItem('tender_stage_selection_' + self.user_id);
                localStorage_StageSelections = (localStorage_StageSelections != '' && localStorage_StageSelections != null) ? JSON.parse(localStorage_StageSelections) : [];
                //Populate all stage_selection in array first
                var checkboxes = $('ul.multiselect-container li input[type=checkbox]');
                checkboxes.each(function (e) {
                    if (localStorage_StageSelections != '' && localStorage_StageSelections.length > 0) {
                        if(localStorage_StageSelections.includes($(this).val()) == false){
                            $(this).prop('checked', false);
                            $('#stage_' + $(this).val()).hide();
                            $('#stage_header_' + $(this).val()).hide();
                        }else{
                            $(this).prop('checked', true);
                            $('#stage_' + $(this).val()).show();
                            $('#stage_header_' + $(this).val()).show();
                            self.stage_selections.push($(this).val());
                        }
                    } else {
                        if ($(this).val() != 'select_all') {
                            self.stage_selections.push($(this).val());
                        }
                        self.handle_stage_selections_to_LocalStorage();
                    }
                });
                
                
                for (var i = 0; i < data.proposal_data_by_stages.length; i++) {
                    if (data.proposal_data_by_stages[i].length > 0) {
                        for (var j = 0; j < data.proposal_data_by_stages[i].length; j++) {
                            self.create_stage_list_item(data.proposal_data_by_stages[i][j]);
                        }
                    }
                }
                
                /*for (var j = 0; j < data.proposal_data_by_stages.length; j++) {
                    self.create_stage_list_item(data.proposal_data_by_stages[j]);
                }*/

                $('.stage_list__item').click(function () {
                    var id = $(this).attr('id');
                    window.location.href = base_url + 'admin/lead/add?deal_ref=' + id;
                });

                if (self.is_lead_allocation_sales_rep) {
                    //return false;
                }

                var currentlyScrolling = false;
                var SCROLL_AREA_WIDTH = 150; // Distance from container left and bottom right.
                //
                //Set max height for container to be able to scroll
                var screenHeight = $(window).height() - 110;
                $('.lists_data__hasScrollbar').attr('style', 'max-height:' + screenHeight + 'px; min-height:' + (screenHeight - 5) + 'px;');


                $("div.connectedSortable").sortable({
                    scroll: true,
                    connectWith: ".connectedSortable",
                    cancel: ".disable-sort-item",
                    tolerance: 'pointer',
                    placeholder: 'sortablePlaceholder',
                    forcePlaceholderSize: false,
                    forceHelperSize: false,
                    start: function (event, ui) {
                        //$('.sr-deal_actions').addClass('sr-deal_actions--visible');
                    },
                    stop: function (event, ui) {
                        //$('.sr-deal_actions').removeClass('sr-deal_actions--visible');
                    },
                    receive: function (event, ui) {
                        var receiver = ui.item.closest('div.stage_list');
                        if (receiver.attr('data-id') == undefined) {
                            self.update_proposal_won_lost(event, ui);
                        } else {
                            self.update_proposal_deal_stage(event, ui);
                        }
                    },
                    sort: function (event, ui) {

                        if (currentlyScrolling) {
                            return;
                        }

                        var continerWidth = $('.lists').width();

                        var mouseXPosition = event.clientX;

                        if ((mouseXPosition - SCROLL_AREA_WIDTH) < SCROLL_AREA_WIDTH) {
                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "-=" + continerWidth / 4 + "px" // Scroll up half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });

                        } else if (mouseXPosition > (continerWidth - SCROLL_AREA_WIDTH)) {

                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "+=" + continerWidth / 4 + "px" // Scroll down half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });
                        }
                    }
                }).disableSelection();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_pipeline_manager.prototype.fetch_more_stage_list = function (data) {
    var self = this;
    $.ajax({
        url: base_url + "admin/lead/fetch_lead_stage_data",
        type: 'get',
        dataType: "json",
        data: data,
        beforeSend: function () {
            $('#loader').html(createSpinner(''));
            $('#load_more_deals').attr('disabled', 'disabled');
            $('#load_more_deals').children('i').removeClass('hidden');
        },
        success: function (data) {
            $('#loader').html('');
            $('#load_more_deals').removeAttr('disabled');
            $('#load_more_deals').children('i').addClass('hidden');

            if (data.success == false) {
                toastr['error'](data.status);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }

            //Populate Left Lead Count in Lead More Button
            var total_leads = parseInt(data.meta.total);
            var leads_visited_count = parseInt(data.meta.curr_page) * parseInt(data.meta.per_page);
            var left_lead_count = total_leads - leads_visited_count;
            if (left_lead_count > 0) {
                $('#load_more_deals').show();
            } else {
                $('#load_more_deals').hide();
            }

            if (data.proposal_data_by_stages.length > 0) {
                self.loading = false;

                for (var j = 0; j < data.proposal_data_by_stages.length; j++) {
                    self.create_stage_list_item(data.proposal_data_by_stages[j]);
                }

                $('.stage_list__item').click(function () {
                    var id = $(this).attr('id');
                    window.location.href = base_url + 'admin/tender-lead/add?deal_ref=' + id;
                });

                if (self.is_lead_allocation_sales_rep) {
                    //return false;
                }

                var currentlyScrolling = false;
                var SCROLL_AREA_WIDTH = 150; // Distance from container left and bottom right.
                //
                //Set max height for container to be able to scroll
                var screenHeight = $(window).height() - 110;
                $('.lists_data__hasScrollbar').attr('style', 'max-height:' + screenHeight + 'px; min-height:' + (screenHeight - 5) + 'px;');


                $("div.connectedSortable").sortable({
                    scroll: true,
                    connectWith: ".connectedSortable",
                    cancel: ".disable-sort-item",
                    tolerance: 'pointer',
                    placeholder: 'sortablePlaceholder',
                    forcePlaceholderSize: false,
                    forceHelperSize: false,
                    start: function (event, ui) {
                        //$('.sr-deal_actions').addClass('sr-deal_actions--visible');
                    },
                    stop: function (event, ui) {
                        //$('.sr-deal_actions').removeClass('sr-deal_actions--visible');
                    },
                    receive: function (event, ui) {
                        var receiver = ui.item.closest('div.stage_list');
                        if (receiver.attr('data-id') == undefined) {
                            self.update_proposal_won_lost(event, ui);
                        } else {
                            self.update_proposal_deal_stage(event, ui);
                        }
                    },
                    sort: function (event, ui) {

                        if (currentlyScrolling) {
                            return;
                        }

                        var continerWidth = $('.lists').width();

                        var mouseXPosition = event.clientX;

                        if ((mouseXPosition - SCROLL_AREA_WIDTH) < SCROLL_AREA_WIDTH) {
                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "-=" + continerWidth / 4 + "px" // Scroll up half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });

                        } else if (mouseXPosition > (continerWidth - SCROLL_AREA_WIDTH)) {

                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "+=" + continerWidth / 4 + "px" // Scroll down half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });
                        }
                    }
                }).disableSelection();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_pipeline_manager.prototype.update_proposal_deal_stage = function (event, ui,dreceiver_id) {
    var self = this;
    
    var receiver_id = (dreceiver_id != null && dreceiver_id !=undefined) ? dreceiver_id : ui.item.closest('div.stage_list').attr('data-id');
    var current_item = $(ui.item);
    var current_item_id = $(ui.item).attr('id');
    var lead_id = $(ui.item).attr('lead_id');
    
    var data = {};
    data.deal_stage = receiver_id;
    data.uuid = current_item_id;
    data.action = 'update_proposal';
    
    $.ajax({
        url: base_url + "admin/tender-lead/update_lead_stage",
        type: 'post',
        dataType: "json",
        data: data,
        beforeSend: function () {
            current_item.removeClass('bg-danger');
            current_item.removeClass('bg-light_red');
            current_item.removeClass('bg-warning');
            current_item.removeClass('text-white');
            current_item.addClass('disable-sort-item');
            current_item.addClass('bg-disabled');
        },
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                var new_stage_name = ui.item.closest('ul').attr('data-name');
                var prev_stage_name = ui.sender.attr('data-name');
                var date = moment(new Date()).format("DD-MM-YYYY");
                var activity_data = "activity_action=Stage Change&activity[activity_type]=Note&activity[activity_note]=Changed from " + prev_stage_name + " to " + new_stage_name + " on " + date + '&lead_id=' + lead_id;
                    //self.save_activity_handler(activity_data);
            } else {
                toastr["error"](data.status);
            }
            if (data.success == false) {
                $(ui.sender).sortable('cancel');
            }
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $(ui.sender).sortable('cancel');
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        }
    });
};

lead_pipeline_manager.prototype.save_activity_handler = function (data) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/activity/save',
        datatype: 'json',
        data: data,
        success: function (stat) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            //toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};





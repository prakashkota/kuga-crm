<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property TeamRepresentative Controller
 * @version 1.0
 */
class TeamRepresentative extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("Email_Manager", 'email_manager');
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }
    
     /**
     * Manage Team Representative
     * @param int group_id
     * @return view
     */
    public function manage($group_id = FALSE) {
        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Lead Allocation Team Leader') && !$this->aauth->is_member('Sales Team Leader') && !$group_id) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        
        
        $data = array();
        $team_types = TEAM_REP_TYPES;
        
        //If not a team leader redirect to dashboard
        if(!array_key_exists($group_id, $team_types)){
           $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
           redirect('admin/dashboard'); 
        }
        
        $logged_in_user_grp_id = $this->aauth->get_user_groups()[0]->id;
        if($logged_in_user_grp_id != 1){
            $userid = $this->aauth->get_user_id();
            $users = $this->user->get_sub_users(FALSE,FALSE,$userid);
        }else{
            $users = $this->user->get_sub_users($group_id,FALSE,FALSE);
        }
        
        $data['admin'] = $this->user->get_users(FALSE,1,FALSE);
        $data['users'] = $users;
        $data['title'] = 'Manage '.$team_types[$group_id];
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = array_column($states, 'state_postal');
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_representative/manage');
        $this->load->view('partials/footer');
    }
    
    /**
     * Add Team Representative
     * @return view/form
     */
    public function add() {
        $data = array();

        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('user_details[full_name]', 'Team Representative Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Company Contact No', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[aauth_users.email]');
            $this->form_validation->set_rules('user_id', 'Team Representative', 'required');
            $this->form_validation->set_rules('users[password]', 'Pssword', 'required');
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');
            if($this->form_validation->run() != FALSE){
                $team_leader_id = $this->input->post('user_id');
                //Get representative user group on team leader id
                $team_leader = $this->user->get_users(FALSE,$team_leader_id,FALSE);
                if($team_leader[0]['group_id']==6){
                    $this->form_validation->set_rules('profile_id', 'Team Representative Profile', 'required');
                }
            }
           
            if ($this->form_validation->run() != FALSE) {
                //Start Trancsaction
                $this->db->trans_begin();
            
                $team_leader_group_id = $team_leader[0]['group_id'];
                $user_group =  TEAM_REP_TYPES_BY_TEAM_LEADER_TYPES[$team_leader_group_id];
                
                $userdata = $this->input->post('users');
                $userdata['email'] = $this->input->post('email');
                $userdata['profileId'] = $this->input->post('profile_id');
                //$username = $this->aauth->generate_username($user_details['full_name'], 10);
                $user_id = $this->aauth->create_user($userdata['email'], $userdata['password'], false, $userdata['profileId']);
                if ($user_id) {
                    //Add user to Specific Group
                    $this->aauth->add_member($user_id, $user_group);
                    //Add user to owner relation
                    $user_to_owner_data['user_id'] = $user_id;
                    $user_to_owner_data['owner_id'] = $team_leader_id;
                    $this->common->insert_data('aauth_user_to_user', $user_to_owner_data);
                    //Insert Data to Details Table
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $user_id;
                    
                    //Convert Base64 signatures to png
                    if (isset($user_details['signature'])) {
                        $img = explode(';', $user_details['signature']);
                        if ($user_details['signature'] != '' && count($img) > 1) {
                            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $user_details['signature']));
                            $user_details['signature'] = $newfilename = $this->components->uuid() . '.png';
                            file_put_contents('./assets/uploads/user_documents/' . $newfilename, $image);
                        }
                    }
                    
                    $this->common->insert_data('tbl_user_details', $user_details);
                    //Insert Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    //Populate Working Location Postcodes in comma seperated string
                    if (isset($user_locations['workarea_postcodes'])) {
                        $user_locations['workarea_postcodes'] = implode(',', $user_locations['workarea_postcodes']);
                    }
                    if (isset($user_locations['workarea_postcodes_secondary'])) {
                        $user_locations['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
                    }

                    $user_locations['user_id'] = $user_id;
                    $this->common->insert_data('tbl_user_locations', $user_locations);
                    
                    //Check Transaction status and take necessary action
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Looks like something went wrong while creating the user.</div>');
                        redirect('admin/team-representative/manage/'.$user_group);
                    } else {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('message', '<div class="alert alert-success"> Team Representative with Email: <strong>' . $userdata['email'] . '</strong> and Password: <strong>' . $userdata['password'] . '</strong> created successfuly.</div>');
                        redirect('admin/team-representative/manage/'.$user_group);
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
                $data['user_id'] = $this->input->post('user_id');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['team_types'] = TEAM_REP_TYPES;
        $data['title'] = 'Add Team Representative';
        $data['show_password_field'] = TRUE;
        $data['team_leaders'] = array();
       
        //If looged in user is team leader only get him
        $team_leader_types = array();
        $logged_in_user_grp_id = $this->aauth->get_user_groups()[0]->id;
        if(array_key_exists($logged_in_user_grp_id, TEAM_LEADER_TYPES)){
           $team_leader_types[$logged_in_user_grp_id] = TEAM_LEADER_TYPES[$logged_in_user_grp_id];
        }
        
        if($logged_in_user_grp_id == 1){
            foreach(TEAM_LEADER_TYPES as $key => $value){
                $team_leader = $this->user->get_users($key,FALSE,FALSE);
                if(!empty($team_leader)){
                    foreach($team_leader as $team_lead){
                        $data['team_leaders'][] = $team_lead;
                        //If logged in as Team Leader then loop on from itself and break
                        if($team_lead['user_id'] == $this->aauth->get_user_id()){
                            break;
                        }
                    }
                }
            }
        }else{
            $userid = $this->aauth->get_user_id();
            foreach($team_leader_types as $key => $value){
                $team_leader = $this->user->get_users(FALSE,$userid,FALSE);
                if(!empty($team_leader)){
                    $data['team_leaders'][] = $team_leader[0];
                    //If logged in as Team Leader then loop on from itself and break
                    if($team_leader[0]['user_id'] == $this->aauth->get_user_id()){
                        break;
                    }
                }
            }
        }
        $data['profile_types'] = TEAM_REP_PROFILE_TYPES;
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_representative/form');
        $this->load->view('partials/footer');
    }
    
    /**
     * Edit Team Representative
     * @return view/form
     */
    public function edit($id) {
        if ($this->aauth->get_user_id() != $id && !$this->aauth->is_member('Admin') && !$this->aauth->is_member('Lead Allocation Team Leader') && !$this->aauth->is_member('Sales Team Leader')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this team representative.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $user = $this->user->get_sub_users(FALSE,$id,FALSE);
       
        if (empty($user)) {
            redirect('/admin/dashboard');
        }
        
        $data['email'] = $user[0]['email'];
        $data['profile_id'] = $user[0]['profileId'];
        $data['users'] = $user[0];
        $data['user_id'] = $user[0]['owner_id'];
        $data['user_locations'] = $user[0];
        $data['user_details'] = $user[0];
        

        if (!empty($this->input->post())) {
            if ($this->input->post('email') != $user[0]['email']) {
                $is_unique = '|is_unique[aauth_users.email]';
            } else {
                $is_unique = '';
            }
            $this->form_validation->set_rules('user_details[full_name]', 'Franchise Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Company Contact No', 'required');
            $this->form_validation->set_rules('user_id', 'Team Representative', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email' . $is_unique);
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');
            if($this->form_validation->run() != FALSE){
                $team_leader_id = $this->input->post('user_id');
                //Get representative user group on team leader id
                $team_leader = $this->user->get_users(FALSE,$team_leader_id,FALSE);
                if($team_leader[0]['group_id']==6){
                    $this->form_validation->set_rules('profile_id', 'Team Representative Profile', 'required');
                }
            }
            if ($this->form_validation->run() != FALSE) {
                $team_leader_id = $this->input->post('user_id');
                //Get representative user group on team leader id
                //$team_leader = $this->user->get_users(FALSE,$team_leader_id,FALSE);
                $team_leader_group_id = $team_leader[0]['group_id'];
                $user_group = TEAM_REP_TYPES_BY_TEAM_LEADER_TYPES[$team_leader_group_id];;
                
                $email = $this->input->post('email');
                $stat = TRUE;
                if ($this->input->post('email') != $user[0]['email'] || $this->input->post('profile_id') != $user[0]['profileId']) {
                    $stat = $this->aauth->update_user($id, $email,false,false,$this->input->post('profile_id'));
                }

                if ($stat) {
                    
                    //Update user to Specific Group
                    if($user[0]['group_id'] != $user_group){
                        $this->aauth->remove_member($id, $user[0]['group_id']);
                        $this->aauth->add_member($id, $user_group);
                    }
                    
                    //Update user to owner relation
                    if($user[0]['owner_id'] != $team_leader_id){
                        $user_to_owner_data['user_id'] = $id;
                        $user_to_owner_data['owner_id'] = $team_leader_id;
                        $this->common->update_data('aauth_user_to_user', array('user_id' => $id),$user_to_owner_data);
                    }
                    
                    //Update Data to Details Table
                    $user_details = $this->input->post('user_details');
                    
                    //Convert Base64 signatures to png
                    if (isset($user_details['signature'])) {
                        $img = explode(';', $user_details['signature']);
                        if ($user_details['signature'] != '' && count($img) > 1) {
                            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $user_details['signature']));
                            $user_details['signature'] = $newfilename = $this->components->uuid() . '.png';
                            file_put_contents('./assets/uploads/user_documents/' . $newfilename, $image);
                        }
                    }
                    
                    $this->common->update_data('tbl_user_details', array('user_id' => $id), $user_details);
                    //Update Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    //Populate Working Location Postcodes in comma seperated string
                    if (isset($user_locations['workarea_postcodes'])) {
                        $user_locations['workarea_postcodes'] = implode(',', $user_locations['workarea_postcodes']);
                    }
                    if (isset($user_locations['workarea_postcodes_secondary'])) {
                        $user_locations['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
                    }
                    $user_locations['user_id'] = $id;
                    $this->common->insert_or_update('tbl_user_locations', array('user_id' => $id), $user_locations);
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Team Representative with Email: <strong>' . $email . '</strong> updated successfuly.</div>');
                    redirect('admin/team-representative/manage/'.$user_group);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
                $data['user_id'] = $this->input->post('user_id');
            }
        }

        $data['workarea_postcodes'] = array();
        $data['workarea_postcodes_secondary'] = array();
        if ($user[0]['workarea_postcodes'] != '' && $user[0]['workarea_postcodes'] != NULL) {
            $workarea_postcodes = explode(',', $user[0]['workarea_postcodes']);
            $data['workarea_postcodes'] = $workarea_postcodes;
        }
        if ($user[0]['workarea_postcodes_secondary'] != '' && $user[0]['workarea_postcodes_secondary'] != NULL) {
            $workarea_postcodes_secondary = explode(',', $user[0]['workarea_postcodes_secondary']);
            $data['workarea_postcodes_secondary'] = $workarea_postcodes_secondary;
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['team_types'] = TEAM_REP_TYPES;
        $data['team_leaders'] = array();
        
        //If looged in user is team leader only get him
        $team_leader_types = array();
        $logged_in_user_grp_id = $this->aauth->get_user_groups()[0]->id;
        if(array_key_exists($logged_in_user_grp_id, TEAM_LEADER_TYPES)){
           $team_leader_types[$logged_in_user_grp_id] = TEAM_LEADER_TYPES[$logged_in_user_grp_id];
        }
        
        if($logged_in_user_grp_id == 1){
            foreach(TEAM_LEADER_TYPES as $key => $value){
                $team_leader = $this->user->get_users($key,FALSE,FALSE);
                if(!empty($team_leader)){
                    foreach($team_leader as $team_lead){
                        $data['team_leaders'][] = $team_lead;
                        //If logged in as Team Leader then loop on from itself and break
                        if($team_lead['user_id'] == $this->aauth->get_user_id()){
                            break;
                        }
                    }
                }
            }
        }else{
            $userid = $this->aauth->get_user_id();
            foreach($team_leader_types as $key => $value){
                $team_leader = $this->user->get_users(FALSE,$userid,FALSE);
                if(!empty($team_leader)){
                    $data['team_leaders'][] = $team_leader[0];
                    //If logged in as Team Leader then loop on from itself and break
                    if($team_leader[0]['user_id'] == $this->aauth->get_user_id()){
                        break;
                    }
                }
            }
        }
        $data['profile_types'] = TEAM_REP_PROFILE_TYPES;
        $data['title'] = 'Edit Team Representative';
        $data['show_password_field'] = FALSE;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_representative/form');
        $this->load->view('partials/footer');
    }

    /**
     * Function to update team leader settings by himself
     * @return view/form
     */
    public function manage_settings() {
        $user_id = $this->aauth->get_user_id();
        
        if (!$this->aauth->is_member('Lead Allocation Representative') && !$this->aauth->is_member('Sales Representative') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this franchise.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $users = $this->user->get_users(FALSE,$user_id,FALSE);
        
        if (empty($users)) {
            redirect('/admin/dashboard');
        }

        $data['user_details'] = $users[0];
        $data['user_locations'] = $user_locations = $this->input->post('user_locations');

        if (!empty($this->input->post())) {
            //Update Data to Details Table
            $user_details = $this->input->post('user_details');
            $stat = $this->common->update_data('tbl_user_details', array('user_id' => $user_id), $user_details);
            //Update Data to Location Table
            //Populate Working Location Postcodes in comma seperated string
            $user_locations_update = array();
            $user_locations_update['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
            $stat1 = $this->common->update_data('tbl_user_locations', array('user_id' => $user_id), $user_locations_update);
            if ($stat || $stat1) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"> Settings updated successfuly.</div>');
                redirect('admin/settings/team-representative/manage');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the settings. </div>');
            }
        }
        
        $data['workarea_postcodes_secondary'] = array();
        if ($users[0]['workarea_postcodes_secondary'] != '' && $users[0]['workarea_postcodes_secondary'] != NULL) {
            $workarea_postcodes_secondary = explode(',', $users[0]['workarea_postcodes_secondary']);
            $data['workarea_postcodes_secondary'] = $workarea_postcodes_secondary;
        }
      
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_representative/manage_settings');
        $this->load->view('partials/footer');
    }
}

<style>
    .capitalize {
        text-transform:capitalize;
    }   
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .nav.nav-tabs.nav-topline .nav-item a.nav-link.active {
        background: none;
        box-shadow: inset 0 3px 0 #ffd914;
        color: #000;
        border-radius: 0;
        border-top-color: #ffd914;
        border-bottom: none;
    }
    .nav.nav-tabs.nav-topline .nav-item a.nav-link:hover{
        color: #000;
    }
    tfoot{display: block !important;}


    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .dropdown-menu > li > label {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.428571429;
    }
    .deal_view__actions{    
        display: flex;
        box-sizing: border-box;
        align-items: flex-start
    }

    .dt-button-collection button.buttons-columnVisibility:before,
    .dt-button-collection button.buttons-columnVisibility.active span:before {
        display:block;
        position:absolute;
        top:1.2em;
        left:0;
        width:12px;
        height:12px;
        box-sizing:border-box;
    }

    .dt-button-collection button.buttons-columnVisibility:before {
        content:' ';
        margin-top:-6px;
        margin-left:10px;
        border:1px solid black;
        border-radius:3px;
    }

    .dt-button-collection button.buttons-columnVisibility.active span:before {
        content:'\2714';
        margin-top:-11px;
        margin-left:12px;
        text-align:center;
        text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
    }

    .dt-button-collection button.buttons-columnVisibility span {
        margin-left:20px;    
    }

</style>
<div class="page-wrapper d-block clearfix">
    <form id="lead_list_filters">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="kg-page__heading">
                <div class="kg-page__heading_title">
                    Leads Report - Facebook,Web
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="columns columns-right btn-group pull-right">
                    <div class="ml-2">
                        <button type="button" class="btn btn-default" id="export_leads">
                            <i class="fa fa-export"></i> Export
                        </button>
                    </div>
                    <div class="ml-2">
                        <button class="btn btn-default" type="button" id="lead_list_range" >
                            <i class="fa fa-calendar"></i> &nbsp; <span></span> <i class="fa fa-caret-down" ></i>
                        </button>
                        <input type="hidden" name="filter[start_date]" id="start_date" value="" />
                        <input type="hidden" name="filter[end_date]" id="end_date" value="" />
                    </div>
                    
                    <div class="ml-2">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-filter"></i> Lead Source
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-height:100px; height:300px; overflow-y: scroll; min-width: 300px;">
                            <li><label><strong>Lead Source</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_source">Reset</a>)</label></li>
                            <?php $lead_source = unserialize(lead_source);
                                  foreach ($lead_source as $key => $value) { ?>
                            <?php if($value != 'Facebook Lead' && $value != 'Kuga Lead'){ ?>
                                <li class="hidden"><label><input class="lead_source" type="checkbox" name="filter[custom][lead_sources][]" value="<?php echo $value; ?>"> <?php echo $value; ?></label></li>
                            <?php }else { ?>
                                <li><label><input class="lead_source" type="checkbox" name="filter[custom][lead_sources][]" checked="" value="<?php echo $value; ?>"> <?php echo $value; ?></label></li>
                            <?php }} ?>
                        </ul>
                    </div> 
                    
                    <div class="ml-2">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-filter"></i> Other
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-height:100px; height:300px; overflow-y: scroll; min-width: 300px;">
                            <li><label><strong>Lead Type</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_location_type">Reset</a>)</label></li>
                            <?php foreach (LEAD_LOCATION_TYPE as $key => $value) { ?>
                            <li><label><input class="lead_location_type" type="radio" name="filter[custom][lead_location_type]" value="<?php echo $key; ?>"> <?php echo $value; ?></label></li>
                            <?php } ?>
                            <li><label><strong>Lead Stage</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_stage">Reset</a>)</label></li>
                            <?php foreach ($lead_stages as $key => $value) { ?>
                            <li><label><input class="lead_stage" type="radio" name="filter[custom][lead_stage]" value="<?php echo $value['id']; ?>"> <?php echo $value['stage_name']; ?></label></li>
                            <?php } ?>
                        </ul>
                    </div>  

                </div>
            </div>

        </div>
    </form>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div id="statistics_container">
                <div class="row ">
                <div class="col-12 col-sm-12 col-md-12"> 
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="lead_list" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>State</th>
                                                <th>Lead Source</th>
                                                <th>Lead Type</th>
                                                <th>Assigned To</th>
                                                <th>Deal Stage</th>
                                                <th>Deal Size (kW)</th>
                                                <th>Date</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Company</th>
                                                <th>Address</th>
                                                <th>Current Monthly Electricity Bill</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lead_list_body" >

                                        </tbody>
                                    </table>
                                </div>
                                <div id="table_loader" ></div>
                                <div  id="pagination" class="mt-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div> 

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/pagination/simplePagination.css'); ?>" />
    <script type="text/javascript" src="<?php echo site_url('assets/pagination/jquery.simplePagination.js'); ?>"></script>
    <script src="<?php echo site_url(); ?>common/js/reports/fb_leads.js?v=<?php echo version; ?>"></script>
    <script>
        var context = {};
        var deal_manager_tool = new deal_manager(context);
    </script>
</body>

</html>

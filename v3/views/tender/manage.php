<style>
    .sr-inactive_user{color:red;}
</style>
<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Manage Tenders</h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/tender/add') ?>"><i class="icon-plus"></i> Add New </a>
            </div>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="tender_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Mobile No</th>
                                        <th class="text-center">Client</th>
                                        <th class="text-center">Contact Person</th>
                                        <th class="text-center">Contract Number</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($tender))
                                        foreach ($tender as $key => $value) {
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $value['name']; ?></td>
                                                <td class="text-center"><?php echo $value['email']; ?></td>
                                                <td class="text-center"><?php echo $value['mobile']; ?></td>
                                                <td class="text-center"><?php echo $value['client']; ?></td>
                                                <td class="text-center"> <?php echo $value['contactPerson']; ?></td>
                                                <td class="text-center"> <?php echo $value['contractNumber']; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/tender/add/<?php echo $value['id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <a class="dropdown-item delete_tender" href="javascript:void(0);" data-tenderId="<?php echo $value['id']; ?>"><i class="icon-trash"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="delete_tender_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                <div class="modal-header">
                    <span class="modal-title"><i class="fa fa-user"></i> Confirm Tender Delete</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Tender? Once deleted it can't be rolled back.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  id="confirm_delete_tender">Confirm Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#tender_list').DataTable();

        //Handle Delete User
        $(document).on('click','.delete_tender',function () {
            var tenderId = $(this).attr('data-tenderId');
            $('#confirm_delete_tender').attr('data-tenderId',tenderId);
            $('#delete_tender_modal').modal('show');
        });
        
        $('#confirm_delete_tender').click(function(){
            var tenderId = $(this).attr('data-tenderId');
            
            if(tenderId == ''){
                return false;
            }
            
            $.ajax({
                type: 'POST',
                url:  '<?php echo site_url('admin/tender/delete'); ?>',
                datatype: 'json',
                data: {tenderId: tenderId},
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }else{
                        toastr["error"](stat.status);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
</script>
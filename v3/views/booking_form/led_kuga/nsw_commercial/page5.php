<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td height="35" class="red" style="color:#c32027; font-size:16px; font-family:Arial, Helvetica, sans-serif; padding-left:40px;">
            <strong>Key points of our Terms and Conditions</strong>
        </td>
    </tr>
    <tr>
        <td height="35" style="padding-top:0px; padding-bottom:10px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-left:40px;">
            Key points of our Terms and Conditions We'd like to highlight to you some key clauses of our Terms and Conditions, which are laid out in full to you on the following pages. 
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input  id="terms_and_conditions_0" name="booking_form[terms_and_conditions][0]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 1 - Payment</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        7.1 You must pay the net price in the installment's and at the the times specified in the Agreement. If the Agreement does not specify and such instalments
                        or times, then you must pay the Net Price no later than one Business Day after installation of the Goods, unless clause 7.2 says otherwise.
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input  id="terms_and_conditions_1" name="booking_form[terms_and_conditions][1]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 2 - Variation</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        6.1 If, after the Agreement is entered into, we determine that due to:<br/>
                        (a) any special or unusual aspect of the Premises that we could not reasonably have been aware of during our pre-installation site inspection (for
                        example, because that would have required dismantling fixtures or fittings);<br/>
                        (b) any changes having occurred at the Premises since our pre-installation site inspection;<br/>
                        (c) any act, matter or thing you could told us concerning the Premises or the proposed installation of the Goods being revealed to be incorrect, false or misleading;<br/>
                        (d) the state of any electrical wiring at the Premises, including such electrical wiring not being in compliance with all applicable laws and standards; or <br/>
                        (e) technical issues that could not have reasonably been foreseen by us when we entered into the Agreement,<br/>
                        additional work and/or changes not set out in the Agreement are required in order to supply and install the Goods at the Premises, then we 
                        may give you a notice setting out the additional work and/or changes (Variation Notice), whether before or during the Installation of the Goods. If we are entitled
                        to give you a Variation Notice and do so, we will endeavor to give it to you soon as reasonably possible after we make a determination under this clause 6.1.
                        If you accept the Variation Notice in accordance with clause 6.2, that clause and clause 6.3 will apply. Otherwise, clause 6.4 and 6.5 will apply.
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input  id="terms_and_conditions_2" name="booking_form[terms_and_conditions][2]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 3 - Certificates</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        9.3(b) replace the existing lighting on a one for one basis with LED lights(being the Goods). You acknowledge that we are required to recycle all old
                        globes and fittings in accordance to VEET and ESS regulations.
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <input  id="terms_and_conditions_3" name="booking_form[terms_and_conditions][3]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Key Point 4 (NSW Only) - Certificates</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="35" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:12px">
                        9.4(b) if you are required to make a minimum payment under the rules of the Scheme that applies to the supply and installation of Goods (for example,
                        if the New South Wales Scheme applies), you must make such payment to us as part of the Net Price, and that payment cannot be refunded or reimbursed.
                    </td> 
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="50">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="415" valign="top" style="padding-right:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="405" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333">ALL OF THE ABOBE HAS BEEN EXPLAINED AND I ACCEPT THE CHARGES AS LISTED. I HAVE READ AND UNDERSTOOD THE FULL TERMS AND CONDITIONS ON THE FOOLLOWING PAGES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="23" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised on behalf of:</td>
                            </tr>
                            <tr>
                                <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                    <input  id="authorised_on_behalf_company_name" name="booking_form[authorised_on_behalf][company_name]" style="padding:5px; width:100%; height: inherit;" type="text"  required="" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_name" name="booking_form[authorised_on_behalf][name]" style="padding:5px; width:100%; height: inherit;" type="text"required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_position" name="booking_form[authorised_on_behalf][position]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_on_behalf_date" name="booking_form[authorised_on_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">CLIENT SIGNATURE:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                            <td align="center" style=" padding:5px; height: 180px; ">
                                                <div class="signature-pad">
                                                    <div class="signature-pad--body">
                                                        <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_1" data-id="1" class="sign_create"></div>
                                                        <input required="" id="authorised_on_behalf_signature" name="booking_form[authorised_on_behalf][signature]" type="hidden" />
                                                    </div>
                                                </div>
                                                <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                    <img src="" />
                                                    <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> 
                    </td>
                    <td width="415" valign="top" style="padding-left:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333">BY SIGNING BELOW YOU ACKNOWLEDGE THAT THE CUSTOMER IS AWARE OF THE POSSIBLE EXTRA CHARGES. INCLUDING ALL ACCESS EQUIPMENT CHARGES AND RE-SCHEDULING FEES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="35" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="116" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of:</td>
                                            <td width="289" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">Kuga Electrical</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <select id="authorised_by_behalf_name" name="booking_form[authorised_by_behalf][name]" style="padding:5px; width:100%; height: inherit;" required="" >
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    if (!empty($users)) {
                                                        foreach ($users as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value['full_name']; ?>" ><?php echo $value['full_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_by_behalf_position" name="booking_form[authorised_by_behalf][position]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <input  id="authorised_by_behalf_date" name="booking_form[authorised_by_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">SIGNATURE:</td>
                                            <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                            <td align="center" style=" padding:5px; height: 180px; ">
                                                <div class="signature-pad">
                                                    <div class="signature-pad--body">
                                                        <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_2" data-id="2" class="sign_create"></div>
                                                         <input required="" id="authorised_by_behalf_sales_rep_signature" name="booking_form[authorised_by_behalf][sales_rep_signature]" type="hidden" />
                                                    </div>
                                                </div>
                                                <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                    <img src="" />
                                                    <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table></td>
    </tr>
    
    <tr>
        <td height="50">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
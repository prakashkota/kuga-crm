
<?php $html = "
<tr>
    <td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Calculating your benefit
    </td>
  </tr>

<tr>
<td align=\"left\" valign=\"top\">
<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;\">
We have reviewed your current lighting to give you an accurate estimate of your lighting upgrade requirement based on your lighting energy usage. Kuga is transparent and honest with our process in pursuit of maximizing your investment return.</td>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\"> Proposed fittings</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; text-align:justify; padding-top:10px;\">
<ol style=\"-webkit-padding-start: 15px;\">
<li style=\"padding-bottom:10px; padding-top:5px;\">Lighting upgrades are based on lighting requirements of the space type in each building of the premise.</li>
<li style=\"padding-bottom:10px;\">Upgrades are based on maximizing energy and cost saving while maintaining lighting and lumens standards for each space type.</li>
<li>Current Kuga lighting upgrade ensures upgrade to highest quality LED lighting replacement for older technologies .</li>
</ol>
</td>
</tr>

<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\"> Your savings </td>
</tr>
<tr>
<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; text-align:justify;  padding-top:10px;\">
<ol style=\"-webkit-padding-start: 15px;\">
<li style=\"padding-bottom:10px; padding-top:5px;\">Return on investment on upfront investment is calculated based on the investment return from monthly energy saving.</li>
<li style=\"padding-bottom:10px;\">Return on investment on monthly payment is calculated on the saving offset provided by monthly energy saving against the monthly outgoing payment.</li>
<li style=\"padding-bottom:10px;\">Total annual saving is based on the monthly energy saving in a year.</li>
<li style=\"padding-bottom:10px;\">First year return of investment is the amount of total saving in a year against upfront investment.</li>
<li style=\"padding-bottom:10px;\">10 years saving is calculated based on total annual saving in 1O years time.</li>
<li style=\"padding-bottom:10px;\">Total return of investment is calculated based on 10 years saving against upfront investment.</li>
<li>Customer has the option of an upfront payment option and 5 years financing option.</li>
</ol>
</td>
</tr>

<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\"> Your Certificate rebate</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; text-align:justify;  padding-top:10px;\">
<ol style=\"-webkit-padding-start: 15px;\">
<li style=\"padding-bottom:10px; padding-top:5px;\">Certificate Rebates are based on the value of certificate provided by the ESC at the time of issue.</li>
<li style=\"padding-bottom:10px;\">Certificate rebate amount may change according to ESC regulations and policies.</li>
</ol>
</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\"> Further Information</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; text-align:justify;  padding-top:10px;\">
<ol style=\"-webkit-padding-start: 15px;\">
<li style=\"padding-bottom:10px; padding-top:5px;\">Energy savings and cost savings are based on customer's contract with the current energy provider.</li>
<li style=\"padding-bottom:10px;\">Project investment and ROI estimates are valid for 14 days from the date of this proposal.</li>
</ol>
</td>
</tr>


</tbody>
</table>
</td>
</tr>

<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/footer.jpg\" alt=\"\" title=\"\" style=\"   width:100%; display:block;\" /></td>
</tr>
";
echo $html;
?>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_02.jpg'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <th height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:15px"><strong>Lighting Design (RCP)</strong></th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->ligthing_design_5 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->ligthing_design_5; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->ligthing_design_6 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->ligthing_design_6; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height:50px;">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" >
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <th height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:15px"><strong>Lighting Design (RCP)</strong></th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->ligthing_design_7 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->ligthing_design_7; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->ligthing_design_8 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/' . $booking_form_image->ligthing_design_8; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
        <td height="292">&nbsp;</td>
    </tr>
    
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed">
            <table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />13</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
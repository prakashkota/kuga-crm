<?php include 'header.php'; ?>
<div class="thank-you-page-outer">
<!--    <a href="" class="solar-logo"><img src="images/logo.png" alt="logo"/></a>-->
    <div class="thank-you-page">
        <img src="<?php echo site_url(); ?>assets/images/guarantee.png" alt="" class="thanku-icon"/>
        <h1>Thank <strong>You</strong></h1>
        <p>For submitting your valuable feedback.</p>
    </div>
</div>
<?php include 'footer.php'; ?>



<div class="fc-toolbar fc-header-toolbar">
    <div class="fc-left">
        <div class="fc-button-group">
            <a class="dayBtn active"  href="<?php echo $day_url; ?>">Day</a>
            <a class="weekBtn"  data-remote="true" href="<?php echo $week_url; ?>">Week</a>
            <a class="todayBtn" data-remote="true" href="<?php echo $today_url; ?>">Today</a>
        </div>
    </div>

    <div class="fc-center">
        <a class="leftBtn" data-remote="true" href="<?php echo $prev_url; ?>"> <i class="fa fa-arrow-left"></i> </a>
        <?php echo $header_center; ?>
        <a class="rightBtn" data-remote="true" href="<?php echo $next_url; ?>"> <i class="fa fa-arrow-right"></i> </a></div>
    <div class="fc-clear"></div>
</div>
<table class="table table-bordered  table-hover">
    <thead>
        <tr>
            <th class="bg-installer" width="11%">Installer</th>
            <?php foreach ($list_header_date as $key => $value) { ?>
                <th class="text-center <?php echo (date('d', strtotime($current_date)) == $key) ? 'current_date_td' : ''; ?>" width="12%"><?php echo $value; ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($installers as $key => $value) {
            $installer_scheduled_jobs = $scheduled_jobs[$value['user_id']];
            ?>
            <tr>
                <td class="bg-installer">
                    <a  href="#"><i class="fa fa-user"></i> <?php echo $value['full_name']; ?></a>
                </td>
                <?php if (!empty($installer_scheduled_jobs)) { 
                    foreach ($list_header_date as $key => $value) {
                            ?>
                            <?php if (isset($installer_scheduled_jobs[$key]) && array_key_exists($key,$installer_scheduled_jobs)) { ?>
                                <td class="<?php echo (date('d', strtotime($current_date)) == $key) ? 'current_date_td' : ''; ?>"><?php echo $installer_scheduled_jobs[$key]['title']; ?></td>
                            <?php } else { ?>
                                <td class="<?php echo (date('d', strtotime($current_date)) == $key) ? 'current_date_td' : ''; ?>"></td>
                        <?php }
                    } ?>
                    </tr>
            <?php  } else {
            foreach ($list_header_date as $key => $value) {
                ?>
                <td class="<?php echo (date('d', strtotime($current_date)) == $key) ? 'current_date_td' : ''; ?>"></td>
        <?php }
    }
} ?>

</tbody>
</table>

<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=q4hj2x73ddhzsvx7zf9mpc8qda8k4e8upc7jmzi6mm158ej9"></script>


<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }
    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }

    form .kg-form_field--orange{
        border: 1px solid orange;
    }

    .custom-file-label, .custom-file-label::after{
        height: 35px !important;
        padding: .70rem .75rem;
    }

    @media (min-width:768px){
        .mic_logo{margin-top:-15px;}
    }


</style>
<div class="page-wrapper d-block clearfix ">
    <img class="mic_logo" src="https://kugacrm.com.au/assets/images/mic_logo.jpg" alt="Make It Cheaper">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Add Deal</h1>
    </div>
    <form role="form"  action="" method="post" name="customerAdd" id="customerAdd" enctype="multipart/form-data">
        <div class="row">

            <div class="col-12 col-sm-6 col-md-6">
                <div class="d-block clearfix">
                    <input type="hidden" name="cust_id" id="company_cust_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div id="checkcompany"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Business Name</label>
                                <input type="text" id="bussiness_name" name="company[company_name]" class="form-control kg-form_field--orange" placeholder="Business Name"  required >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="company[first_name]" id="firstname"  class="form-control" placeholder="First Name"  required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="company[last_name]" id="lastname"  class="form-control" placeholder="Last Name"  required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="company[customer_contact_no]" id="contactMobilePhone"  class="form-control kg-form_field--orange" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Position</label>
                            <input type="text" id="position" name="company[position]" class="form-control" placeholder="Position"  required >
                        </div>
                    </div>
                </div>


                <div class="row">
                    <input type="hidden" name="site[latitude]" id="locationLatitude" />
                    <input type="hidden" name="site[longitude]" id="locationLongitude" />
                    <div class="col-md-12">
                        <div class="form-group control-group">
                            <label>Customer Address</label>
                            <a href="javscript:void(0);" id="manual_address_btn" style="font-size:12px; float:right;">Edit Manually</a>
                            <a href="javscript:void(0);" id="manual_address_hide_btn" style="font-size:12px; float:right;" class="hidden">Back to Autocomplete</a>
                            <select class="select2 placecomplete form-control locationstreetAddress kg-form_field--orange" id="locationstreetAddress"  style="height:48px !important;"></select>
                            <input class="locationstreetAddressVal form-control" type="hidden" id="locationstreetAddressVal" name="site[address]" />
                        </div>
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group control-group">
                            <label>State</label>
                            <select name="site[state_id]" id="locationState" class="form-control locationState" required>
                                <option value="">Select State</option>
                                <?php for ($j = 0; $j < count($states); $j++) { ?>
                                    <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group control-group">
                            <label>Post Code</label>
                            <input class="form-control locationPostCode " placeholder="Please Enter Post Code" type="text" name="site[postcode]" id="locationPostCode" value=""  required />

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Segment:</label>
                            <select class="form-control" name="lead[lead_segment]" class="" id="lead_segment">
                                <?php
                                $lead_segment = unserialize(LEAD_SEGMENT);
                                foreach ($lead_segment as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Industry Type</label>
                            <select name="lead[lead_industry]" class="form-control" id="lead_industry">
                                <?php
                                $lead_industry = unserialize(LEAD_INDSUTRY);
                                foreach ($lead_industry as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Electricity Bill (if available)</label>
                        <div class="custom-file">
                            <input type="file"  id="bill_file" class="custom-file-input">
                            <label class="custom-file-label" for="datasheets">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="buttwrap mt-5">
                    <button type="button" id="save_customer" class="btn" > <i class="fa fa-spinner hidden"></i> SAVE </button>
                </div> 
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            <div class="d-block clearfix">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Notes</label>
                        <div class="custom-file">
                            <textarea class="form-control" id="notes" placeholder="Note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
</div>

<div class="modal fade" id="confirm_customer_modal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span><i class="fa fa-exclamation-triangle"></i> Are you sure is the same customer?</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="confirm_customer_modal_body">

            </div>
            <div class="modal-footer">
                <a class="btn btn-danger text-white" id="confirm_customer_yes">Go to Lead</a>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/lead_add.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>

<script type='text/javascript'>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#locationstreetAddress').placecomplete1({
            placeServiceResult1: function (data, status) {
                if (status != 'INVALID_REQUEST') {

                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;
                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    console.log(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.getElementById("locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }
                                }

                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                                $('#locationPostCode').trigger('change');
                            }
                        }
                    }
                } else {
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'en'
        });

});
</script>

<script>
    var context = {};
    context.user_group = '<?php echo isset($user_group) ? $user_group : ''; ?>';
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    context.is_lead_allocation_sales_rep = '<?php echo $is_lead_allocation_sales_rep; ?>';
    var lead_add_manager = new lead_add_manager(context);
</script>

<script>
    tinymce.init({
        selector:'#notes',
        branding: false,
        menubar: false,
        statusbar: false,
        plugins: "lists,link,fullscreen",
        toolbar: 'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
        mode: "textareas",
        mobile: {
            theme: 'silver'
        },
        entity_encoding : "raw"
    });

 $('#manual_address_btn').click(function(){
     $(this).addClass('hidden');
     $('#manual_address_hide_btn').removeClass('hidden');
     $('#locationstreetAddress').next().css('display','none');
     $('#locationstreetAddressVal').attr('type','text');
 }); 

 $('#manual_address_hide_btn').click(function(){
     $(this).addClass('hidden');
     $('#manual_address_btn').removeClass('hidden');
     $('#locationstreetAddress').next().css('display','block')
     $('#locationstreetAddressVal').attr('type','hidden');
 });

 $('#locationstreetAddressVal').keyup(function(){
     $('#select2-locationstreetAddress-container').html($(this).val());
 });



</script>

<?php /**<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-storage.js"></script>
<script>
    var config = {
    apiKey: '7a18dec1b4860f1cf6ab3229c51f1afd401e586a',
    storageBucket: 'kugacrm-pipedrive'
  };
  firebase.initializeApp(config);
  var storage = firebase.storage();

  function uploadFile(){
      
      // Created a Storage Reference with root dir
      var storageRef = firebase.storage().ref();
      // Get the file from DOM
      var file = document.getElementById("product_file").files[0];
      console.log(file);
      
      //dynamically set reference to the file name
      var thisRef = storageRef.child(file.name);

      //put request upload file to firebase storage
      thisRef.put(file).then(function(snapshot) {
         alert("File Uploaded")
         console.log('Uploaded a blob or file!');
      });
    }
  console.log(storage);
</script>
*/ ?>
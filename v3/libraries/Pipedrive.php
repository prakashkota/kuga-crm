<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class Pipedrive {

    /**
     * Constructor
     */
    
    protected static $access_token = 'ef4f0f579c522545d04c135c5331fd3e403bc392';
    protected static $company_domain = 'laserelectrical';
    protected static $client_url = 'https://laserelectrical.pipedrive.com/v1/';

    public function __construct() {
        require __DIR__ . "./../../vendor/autoload.php";
    }

    public function get_deals($payload = []) {
        $payload['api_token'] = self::$access_token;
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = 'deals';
        $headers = array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function get_files($payload = []) {
        $payload['api_token'] = self::$access_token;
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = 'files';
        $headers = array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        
        $response = $client->request('GET',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            ]
        );
      
        try{
        if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
            $data = $response->getBody()->getContents();
            $data = json_decode($data,true);
        }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\RequestException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
    
    public function download_files($payload = [],$file_data = []) {
        $payload['api_token'] = self::$access_token;
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = 'files/'.$file_data->id.'/download';
        $headers = array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        try{
            $response = $client->request('GET',self::$client_url . $api_url,[
                'headers' => $headers,
                'query' => $payload,
                ]
            );
            if ($response->getStatusCode() === 201 || $response->getStatusCode() === 200) {
                $file = $response->getBody()->getContents();
                $data = array('error' => FALSE,'file' => $file);
            }else{
                $data = array('error' => TRUE);
            }
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }
}

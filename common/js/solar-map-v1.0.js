"use strict";

var solarMap = function (options) {
    var options = typeof options == 'undefined' ? {} : options;
    var self = this;
    this.debug = true;
    this.map_obj = null;
    this.current_location_marker = null;
    this.current_location_coordinates = null;
    this.shift = false;
    this.solarPanels = [];
    this.selectedSolarPanels = [];
    this.defaultSolarPanelColor = '#384950';
    this.activeSolarPanelColor = '#209bed';
    this.overlappedSolarPanelColor = '#ff0000';
    this.strokeColor = '#e2ebe8';
    this.fillOpacity = .95;
    this.strokeWeight = 1.5;
    this.defaultOrientation = 'portrait';
    this.defaultRotation = 0;
    this.rotationDirection = 0;
    this.rotationHeading = 0;
    this.panelLength = 0.9906;
    this.panelWidth = 1.651;
    this.panelId = 0;
    this.panelWatt = 0;
    this.totalPanels = 0;
    this.selectedPanels = 0;
    this.activeTool = 'panel_plotter';
    this.nearMapApiKey = '';
    this.nearMapOn = true;
    this.line = null;
    this.rectSelect = null;
    this.gapBetweenTiles = .01; // in meter 10mm
    this.drawnLine = null;
    this.distanceMarker = null;
    this.dragStartReference = null;

    /*user defined  values*/
    this.map_element_id = typeof options.map_element_id === 'undefined' ? 'map' : options.map_element_id;
    this.search_box_element_id = typeof options.search_box_element_id === 'undefined' ? 'pac-input' : options.search_box_element_id;
    //calc polygons intersection
    this.geometryFactory = new jsts.geom.GeometryFactory();

    // must be initialized with blank json 
    this.saved_data = '{}';

    this.map_default_options = {
        zoom: 20,
        center: {lat: -37.992033, lng: 145.1889573},
        mapTypeId: 'satellite',
        mapTypeControl: false,
        streetViewControl: false,
        rotateControl: false,
        draggableCursor: 'crosshair',
        tilt: 0
    };
    
    // this.transformationFixForImage = [
    //     {'elem': '.gm-style>div:first>div:first>div:last>div', 'transform': null},
    //     {'elem': '.gm-style>div:first>div:first>div:first>div:first>div', 'transform': null},
    //     {'elem': '.gm-style>div:first>div:first>div:nth-child(2)>div:first>div', 'transform': null},
    //     {'elem': '.gm-style>div:first>div:first>div:nth-child(3)>div:first>div', 'transform': null},
    //     {'elem': '.gm-style>div:first>div:first>div:nth-child(4)>div:first>div', 'transform': null}
    // ];
    
    this.transformationFixForImage = [
       
        {'elem': '.gm-style>div:nth-child(2)>div:first>div:last>div', 'transform': null},
        {'elem': '.gm-style>div:nth-child(2)>div:first>div:first>div:first>div', 'transform': null},
        {'elem': '.gm-style>div:nth-child(2)>div:first>div:nth-child(2)>div:first>div', 'transform': null},
        {'elem': '.gm-style>div:nth-child(2)>div:first>div:nth-child(3)>div:first>div', 'transform': null},
        {'elem': '.gm-style>div:nth-child(2)>div:first>div:nth-child(4)>div:first>div', 'transform': null}
    ];
    document.addEventListener('keydown', function (event) {
        self.shift = event.ctrlKey || event.shiftKey;
    });
    document.getElementById('tl-distance-calculator').addEventListener('click', function (event) {
        self.activeTool = 'distance_calculator';
        google.maps.event.clearListeners(self.map_obj, 'click');

        self.line = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYLINE,
            drawingControl: false,
            drawingControlOptions: {
                drawingModes: ['polyline']
            }
        });
        self.line.setMap(self.map_obj);
        google.maps.event.addListener(self.line, 'polylinecomplete', function (line) {
            if (self.drawnLine) {
                self.drawnLine.setMap(null);
                self.distanceMarker.setMap(null);
            }
            self.drawnLine = line;
            self.calculateDistanceBetween2Points(self.drawnLine);
        });
    });
    document.getElementById('tl-select').addEventListener('click', function (event) {
        self.activeTool = 'selector';
        if (self.drawnLine) {
            self.drawnLine.setMap(null);
            self.distanceMarker.setMap(null);
        }
        if (self.rectSelect) {
            self.rectSelect.setMap(null);
        }
        if (self.line) {
            self.line.setMap(null);
        }
        google.maps.event.clearListeners(self.map_obj, 'click');

        self.rectSelect = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.RECTANGLE,
            drawingControl: false,
            drawingControlOptions: {
                drawingModes: ['rectangle']
            },
            rectangleOptions: {
                strokeColor: '#fff',
                strokeWeight: 2,
                fillColor: self.activeSolarPanelColor,
                fillOpacity: 0.6
            }
        });
        self.rectSelect.setMap(self.map_obj);
        google.maps.event.addListener(self.rectSelect, 'rectanglecomplete', function (rect) {
            for (var key in self.solarPanels) {
                if (rect.getBounds().contains(self.solarPanels[key].getBounds().getCenter())) {
                    self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                    self.solarPanels[key].sp_selected = true;
                } else {
                    self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                    self.solarPanels[key].sp_selected = false;
                }
            }
            rect.setMap(null);
            self.countSelected();
            document.getElementById('tl-panel-plotter') ? document.getElementById('tl-panel-plotter').click() : '';
        });
    });
    document.getElementById('tl-panel-plotter').addEventListener('click', function (event) {
        self.activeTool = 'panel_plotter';
        google.maps.event.clearListeners(self.map_obj, 'click');
        if (self.drawnLine) {
            self.drawnLine.setMap(null);
            self.distanceMarker.setMap(null);
        }
        if (self.rectSelect) {
            self.rectSelect.setMap(null);
        }
        if (self.line) {
            self.line.setMap(null);
        }
        google.maps.event.addListener(self.map_obj, 'click', function (event) {
            self.plotSolarPanel(event.latLng.lat(), event.latLng.lng());
        });
    });
    document.addEventListener('keyup', function (event) {
        self.shift = event.shiftKey;
        if (event.keyCode == 46) {
            self.performAction(null, 'remove');
        }
    });
    setTimeout(function () {
        if (self.current_location_marker) {
            self.current_location_marker.setMap(null);
        }
    }, 10000);

};

/** initializeMap
 *  Load Map with default coorinate and configuration
 * */
solarMap.prototype.initializeMap = function () {
    if (this.debug)
        console.log('Initializing map')
    var self = this;
    self.loader('show', 'Loading map');

    var dimension = document.getElementById('panel_list').options[document.getElementById('panel_list').selectedIndex].getAttribute('data-dimension').split(',');
    self.panelLength = dimension[1];
    self.panelWidth = dimension[0];
    self.panelId = document.getElementById('panel_list').value;
    self.panelWatt = document.getElementById('panel_list').options[document.getElementById('panel_list').selectedIndex].getAttribute('data-capacity');
    //document.getElementById('prd_panel').value = document.getElementById('panel_list').value;

    var data = null;
    if (typeof self.saved_data != 'undefined') {
        data = JSON.parse(self.saved_data);
    }
    if (data != null && data.hasOwnProperty('center')) {
        self.map_default_options['center'] = {lat: data.center.lat, lng: data.center.lng};
    }
    self.map_obj = new google.maps.Map(document.getElementById(self.map_element_id), self.map_default_options);
    if (self.nearMapOn) {
        self.nearMap();
    }
    //Set Zoom Level
    self.map_obj.setZoom(self.map_default_options['zoom']);
    //Add Marker
    self.addMarker(self.map_default_options['center'].lat, self.map_default_options['center'].lng);

    //Enable Sidebar
    self.enableSideBar();

    // bind click event to plot solar panel on map 
    google.maps.event.addListener(this.map_obj, 'click', function (event) {
        self.plotSolarPanel(event.latLng.lat(), event.latLng.lng());
    });
    google.maps.event.addListenerOnce(this.map_obj, 'tilesloaded', function () {
        self.loader('hide', 'Loading complete');
        if (document.getElementById('site_address').value != '') {
            document.getElementById('pac-input').value = document.getElementById('site_address').value;
        }
        self.resumeEditing();
    });

    google.maps.Polygon.prototype.moveTo = function (latLng) {
        // our vars
        var boundsCenter = this.getBounds().getCenter(), // center of the polygonbounds
                paths = this.getPaths(), // paths that make up the polygon
                newPoints = [], // array on which we'll store our new points
                newPaths = []; // array containing the new paths that make up the polygon
        // geodesic enabled: we need to recalculate every point relatively
        if (this.geodesic) {
            // loop all the points of the original path and calculate the bearing + distance of that point relative to the center of the shape
            var path;
            for (var p = 0; p < paths.getLength(); p++) {
                path = paths.getAt(p);
                newPoints.push([]);
                for (var i = 0; i < path.getLength(); i++) {
                    newPoints[newPoints.length - 1].push({
                        heading: google.maps.geometry.spherical.computeHeading(boundsCenter, path.getAt(i)),
                        distance: google.maps.geometry.spherical.computeDistanceBetween(boundsCenter, path.getAt(i))
                    });
                }
            }
            // now that we have the "relative" points, rebuild the shapes on the new location around the new center
            for (var j = 0, jl = newPoints.length; j < jl; j++) {
                var shapeCoords = [],
                        relativePoint = newPoints[j];
                for (var k = 0, kl = relativePoint.length; k < kl; k++) {
                    shapeCoords.push(google.maps.geometry.spherical.computeOffset(
                            latLng,
                            relativePoint[k].distance,
                            relativePoint[k].heading
                            ));
                }
                newPaths.push(shapeCoords);
            }

        } else {
            // geodesic not enabled: adjust the coordinates pixelwise
            var latlngToPoint = function (map, latlng) {
                var normalizedPoint = map.getProjection().fromLatLngToPoint(latlng); // returns x,y normalized to 0~255
                var scale = Math.pow(2, map.getZoom());
                var pixelCoordinate = new google.maps.Point(normalizedPoint.x * scale, normalizedPoint.y * scale);
                return pixelCoordinate;
            };
            var pointToLatlng = function (map, point) {
                var scale = Math.pow(2, map.getZoom());
                var normalizedPoint = new google.maps.Point(point.x / scale, point.y / scale);
                var latlng = map.getProjection().fromPointToLatLng(normalizedPoint);
                return latlng;
            };

            // calc the pixel position of the bounds and the new latLng
            var boundsCenterPx = latlngToPoint(this.map, boundsCenter),
                    latLngPx = latlngToPoint(this.map, latLng);

            // calc the pixel difference between the bounds and the new latLng
            var dLatPx = (boundsCenterPx.y - latLngPx.y) * (-1),
                    dLngPx = (boundsCenterPx.x - latLngPx.x) * (-1);

            // adjust all paths
            var path = null;
            for (var p = 0; p < paths.getLength(); p++) {
                path = paths.getAt(p);
                newPaths.push([]);
                for (var i = 0; i < path.getLength(); i++) {
                    var pixels = latlngToPoint(this.map, path.getAt(i));
                    pixels.x += dLngPx;
                    pixels.y += dLatPx;
                    newPaths[newPaths.length - 1].push(pointToLatlng(this.map, pixels));
                }
            }
        }
        // Update the path of the Polygon to the new path
        this.setPaths(newPaths);
        // Return the polygon itself so we can chain
        return this;
    };

    google.maps.Polygon.prototype.getBounds = function (latLng) {
        var bounds = new google.maps.LatLngBounds();
        var paths = this.getPaths();
        var path;
        for (var p = 0; p < paths.getLength(); p++) {
            path = paths.getAt(p);
            for (var i = 0; i < path.getLength(); i++) {
                bounds.extend(path.getAt(i));
            }
        }
        return bounds;
    };
};
solarMap.prototype.addressToLatLng = function (address, callback) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            callback(results[0].geometry.location);
        } else {
            callback(false);
        }
    });
};
solarMap.prototype.resumeEditing = function () {
    var self = this;
    var data = null;
    if (typeof self.saved_data != 'undefined') {
        var data = JSON.parse(self.saved_data);
    }
    if (data != null && data.hasOwnProperty('center')) {
        var center = data.center;
        self.map_obj.setCenter(new google.maps.LatLng(center.lat, center.lng));
        self.map_obj.setZoom(100);
        document.getElementById('rotation').value = data.global_rotation;
        self.defaultRotation = data.global_rotation;
        if (data.sp_panel_id) {
            document.getElementById('panel_list').value = data.sp_panel_id;
        }
        var event = new Event('change');
        document.getElementById('panel_list').dispatchEvent(event);
        document.getElementById('tilt').value = data.tilt;
        for (var i = 0; i < data.tiles.length; i++) {
            var plotLocation = data.tiles[i]['position'];
            var x = new google.maps.LatLng(plotLocation.lat, plotLocation.lng)
            if (data.tiles[i].sp_orientation == 'portrait') {
                var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth, data.tiles[i]);
            } else {
                var a = self.getPanelDimensionByLatLong(x, self.panelWidth, self.panelLength, data.tiles[i]);
            }
            var input = data.tiles[i];
            delete input['position'];
            input['map'] = self.map_obj;
            input['bounds'] = a;
            var rectangle = new google.maps.Rectangle(input);
            var rectPoly = self.createPolygonFromRectangle(rectangle);
            this.solarPanels.push(rectPoly);
            this.rotatePanel(rectPoly, input.sp_rotation, input.sp_rotation);
            rectPoly.moveTo(x)
            google.maps.event.addListener(rectPoly, 'click', function (event) {
                self.resetSolarPanel(this, event);
            });
        }
    }
    self.countSelected();
};
solarMap.prototype.enableSideBar = function () {
    if (this.debug)
        console.log('Enabling side bar tools')
    var self = this;
    var input = document.getElementById('tool-bar');
    self.map_obj.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var tools = document.getElementsByClassName('tl');
    var tools_content = document.getElementsByClassName('tl-content');
    for (var t = 0; t < tools_content.length; t++) {
        tools_content[t].style.display = 'none';
    }
    for (var k in tools) {
        if (typeof tools[k].classList != 'undefined' && tools[k].classList.contains('active')) {
            var temp = document.getElementsByClassName(tools[k].getAttribute('data-tile') + '-content');
            for (var x = 0; x < temp.length; x++) {
                temp[x].style.display = 'block';
            }
        }
    }

    // bind actions 

    document.getElementById('rotation').addEventListener('change', function (e) {
        self.defaultRotation = this.value;
    });
    document.getElementById('panel_list').addEventListener('change', function (e) {
        var panelDimensionCsv = e.target.options[e.target.selectedIndex].getAttribute('data-dimension');
        var dimension = panelDimensionCsv.split(',');
        self.panelId = e.target.value;
        self.panelLength = dimension[1];
        self.panelWidth = dimension[0];
        self.panelWatt = e.target.options[e.target.selectedIndex].getAttribute('data-capacity');
        //document.getElementById('prd_panel').value = $(this).val();
    });
    document.getElementById('individual_rotation').addEventListener('change', function (e) {
        document.getElementById('rotation').value = this.value;
        self.defaultRotation = parseInt(self.defaultRotation);
//        if (self.defaultRotation < this.value) {
        self.rotationHeading = parseInt(this.value) - parseInt(self.rotationDirection);
        self.rotationDirection = parseInt(this.value);
//        } else {
//            self.rotationDirection = parseInt(self.rotationDirection) + parseInt(this.value);
//        }
        self.defaultRotation = this.value;
        self.performAction(this.value, 'rotate');
    });
    document.getElementById('delete-panel').addEventListener('click', function (e) {
        self.performAction(null, 'remove');
    });
    document.getElementById('tl-crt-img').addEventListener('click', function (e) {
        if (self.drawnLine) {
            self.drawnLine.setMap(null);
            if (self.distanceMarker) {
                self.distanceMarker.setMap(null);
            }
            setTimeout(function () {
                self.mapToImage();
            }, 500);
        } else {
            self.mapToImage();
        }
    });
    document.getElementById('tl-save').addEventListener('click', function (e) {
        self.exportMap();
    });
    document.getElementById('add-panel').addEventListener('click', function (e) {
        var number_of_panels = document.getElementById('no_of_panels').value;
        self.performAction(number_of_panels, 'add');
    });
    document.getElementById('tilt').addEventListener('change', function (e) {
        self.performAction(this.value, 'tilt');
    });
    document.getElementById('select-toggle').addEventListener('click', function (e) {
        var action = this.getAttribute('data-selected')
        if (action == null || action == 'none') {
            this.innerHTML = 'DESELECT ALL';
            this.setAttribute('data-selected', 'all');
            self.selectAll('all');
        } else if (this.getAttribute('data-selected') == 'all') {
            this.innerHTML = 'SELECT ALL';
            this.setAttribute('data-selected', 'none');
            self.selectAll('none');
        }
        self.countSelected();
    });
    for (var i = 0; i < document.getElementsByClassName('orientation').length; i++) {
        document.getElementsByClassName('orientation')[i].addEventListener('click', function (e) {
            self.panelOrientation(this.value);
        });
    }
    for (var i = 0; i < document.getElementsByClassName('mounting').length; i++) {
        document.getElementsByClassName('mounting')[i].addEventListener('click', function (e) {
            self.changeMounting(this.value);
        });
    }

    for (var i = 0; i < document.getElementsByClassName('tl').length; i++) {
        document.getElementsByClassName('tl')[i].addEventListener('click', function (e) {
            self.toggleTools(this);
        });
    }
};
solarMap.prototype.toggleMounting = function () {
    for (var i = 0; i < document.getElementsByClassName('mounting').length; i++) {
        if (document.getElementsByClassName('mounting')[i].checked) {
            if (!document.getElementsByClassName('mounting')[i].parentNode.classList.contains('active')) {
                document.getElementsByClassName('mounting')[i].parentNode.classList.toggle('active');
            }
        } else {
            if (document.getElementsByClassName('mounting')[i].parentNode.classList.contains('active')) {
                document.getElementsByClassName('mounting')[i].parentNode.classList.toggle('active');
            }
        }
    }
};
solarMap.prototype.toggleTools = function (ele) {
    var tools = document.getElementsByClassName('tl');
    for (var t = 0; t < tools.length; t++) {
        tools[t].classList.remove('active');
    }
    var tools_content = document.getElementsByClassName('tl-content');
    for (var t = 0; t < tools_content.length; t++) {
        tools_content[t].style.display = 'none';
    }
    var temp = document.getElementsByClassName(ele.getAttribute('data-tile') + '-content');
    ele.classList.add('active');
    for (var x = 0; x < temp.length; x++) {
        temp[x].style.display = 'block';
    }
    var temp = document.getElementsByClassName('tool-bar-window');
    var visibility = ele.getAttribute('data-toolbar') || 1;
    if (visibility == 1) {
        for (var x = 0; x < temp.length; x++) {
            temp[x].style.display = 'block';
        }
    } else {
        for (var x = 0; x < temp.length; x++) {
            temp[x].style.display = 'none';
        }
    }
};
/** enableSearchBox
 *  Used to convert input text box into address search handler
 * */
solarMap.prototype.enableSearchBox = function () {
    if (this.debug)
        console.log('enabling search box')
    var self = this;
    var searchBox = new google.maps.places.SearchBox(document.getElementById(self.search_box_element_id));
    self.map_obj.addListener('bounds_changed', function () {
        var bounds = self.map_obj.getBounds();
        searchBox.setBounds(bounds);
    });
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return null;
        }
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            //Add Marker
            self.addMarker(place.geometry.location.lat(), place.geometry.location.lng());
        });
        self.map_obj.fitBounds(bounds);
    });
};

/** addMarker
 *  Used to add marker for the address to be identified on map 
 * */
solarMap.prototype.addMarker = function (lat, lng) {
    var self = this;

    //Remove last marker if its a marker object
    if (self.current_location_marker != null) {
        self.current_location_marker.setMap(null);
    }
    //Add marker to map
    self.current_location_marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: self.map_obj,
        title: 'Current Location!'
    });
    self.current_location_coordinates = {lat: lat, lng: lng};
};

/** plotSolarPanel
 *  Used to place solar plate no map by simply clicking on map 
 * */
solarMap.prototype.plotSolarPanel = function (lat, lng) {
    if (this.debug)
        console.log('plotSolarPanel')
    var self = this;
    var x = new google.maps.LatLng(lat, lng)
    if (self.defaultOrientation == 'portrait') {
        var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth);
    } else {
        // in case of landscape just swap the width and length 
        var a = self.getPanelDimensionByLatLong(x, self.panelWidth, self.panelLength);
    }
    var tilt = document.getElementById('tilt').value || 0;
    var rectangle = new google.maps.Rectangle({
        sp_id: self.randomId(),
        sp_selected: true,
        strokeColor: self.strokeColor,
        strokeOpacity: 1,
        strokeWeight: self.strokeWeight,
        fillColor: self.activeSolarPanelColor,
        fillOpacity: self.fillOpacity,
        map: self.map_obj,
        draggable: true,
        bounds: a,
        sp_orientation: self.defaultOrientation,
        sp_rotation: self.defaultRotation,
        sp_tilt: tilt,
        sp_panel_value: self.panelWidth + "," + self.panelLength,
        sp_panel_id: self.panelId,
        sp_watt: self.panelWatt
    });
    document.getElementById('individual_rotation').value = rectangle.sp_rotation;
    for (var key in this.solarPanels) {
        this.solarPanels[key].sp_selected = false;
        this.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
    }

    var rectPoly = self.createPolygonFromRectangle(rectangle);
    this.rotatePanel(rectPoly, self.defaultRotation, self.defaultRotation);
    this.solarPanels.push(rectPoly);
    google.maps.event.addListener(rectPoly, 'click', function (event) {
        self.resetSolarPanel(this, event);
    });
    self.countSelected();
};
/** plotSolarPanel
 *  Used to place solar plate no map by simply clicking on map 
 * */
solarMap.prototype.addSolarPanel = function (no_of_panels, tiltIndex) {
    var overridePanel = typeof tiltIndex != 'undefined' ? tiltIndex : null;
    if (this.debug)
        console.log('addSolarPanel')
    var self = this;
    for (var i = 0; i < no_of_panels; i++) {
        if (self.solarPanels.length) {
            var idx = overridePanel ? overridePanel : self.getCurrentSelectedPanel();//(self.solarPanels.length - 1);
            var lastRectangle = self.solarPanels[idx];
            var c_idx = overridePanel ? 1 : 0;
            if (overridePanel == null) {
                var r = document.getElementById('individual_rotation').value;
                var rot = parseInt(r) + parseInt(90)
                if (self.defaultOrientation == 'landscape') {
                    // draw panel underneath the selected panel
                    var p = google.maps.geometry.spherical.computeOffset(lastRectangle.getPath().getAt(2), 0, rot);
                } else {
                    if (no_of_panels > 1) {
                        // now the drawing panel is different, first panel will go underneath the selected one then remaining will follow the same rule (beside the panel) 
                        if (i == 0) {
                            var p = google.maps.geometry.spherical.computeOffset(lastRectangle.getPath().getAt(2), self.gapBetweenTiles, rot);
                        } else {
                    var p = google.maps.geometry.spherical.computeOffset(lastRectangle.getPath().getAt(c_idx), self.gapBetweenTiles, rot);
                }
                    } else {
                        // draw panel beside the selected panel    
                        var p = google.maps.geometry.spherical.computeOffset(lastRectangle.getPath().getAt(c_idx), self.gapBetweenTiles, rot);
                    }

                }
                var plotLocation = {'lat': p.lat(), 'lng': p.lng()};
            } else {
                var plotLocation = {'lat': lastRectangle.getPath().getAt(c_idx).lat(), 'lng': lastRectangle.getPath().getAt(c_idx).lng()};
            }
        } else {
            var plotLocation = self.map_default_options.center;
        }
        var x = new google.maps.LatLng(plotLocation.lat, plotLocation.lng)
        if (self.defaultOrientation == 'portrait') {
            var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth);
        } else {
            // in case of landscape just swap the width and length 
            var a = self.getPanelDimensionByLatLong(x, self.panelWidth, self.panelLength);
        }
        var rectangle = new google.maps.Rectangle({
            sp_id: self.randomId(),
            sp_selected: true,
            sp_orientation: self.defaultOrientation,
            strokeColor: self.strokeColor,
            strokeOpacity: 1,
            strokeWeight: self.strokeWeight,
            fillColor: self.activeSolarPanelColor,
            fillOpacity: self.fillOpacity,
            map: self.map_obj,
            draggable: true,
            bounds: a,
            sp_rotation: self.defaultRotation,
            sp_tilt: document.getElementById('tilt').value,
            sp_panel_value: self.panelWidth + "," + self.panelLength,
            sp_panel_id: self.panelId,
            sp_watt: self.panelWatt
        });

        document.getElementById('individual_rotation').value = rectangle.sp_rotation;
        if (overridePanel == null) {
            for (var key in this.solarPanels) {
                this.solarPanels[key].sp_selected = false;
                this.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
            }
        }

        var rectPoly = self.createPolygonFromRectangle(rectangle);
        // rotation has to be from first coordinate
        var temp = overridePanel || 1;
        this.rotatePanel(rectPoly, self.defaultRotation, self.defaultRotation, temp);
        if (overridePanel != null) {
            self.solarPanels[idx].setMap(null);
            self.solarPanels[idx] = rectPoly;
        } else {
            this.solarPanels.push(rectPoly);
        }
        google.maps.event.addListener(rectPoly, 'click', function (event) {
            self.resetSolarPanel(this, event);
        });
    }
    self.countSelected();
};
solarMap.prototype.updateTileProperties = function (id, prop, value) {
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_id === id) {
            var temp = {};
            temp[prop] = value;
            self.solarPanels[key].setOptions(temp);
            break;
        }
    }

};
/** resetSolarPanel
 *  Used to unselect sola 
 * */
solarMap.prototype.resetSolarPanel = function (ele, event) {
    if (this.debug)
        console.log('resetSolarPanel')
    var self = this;
    document.querySelector('[data-tile="tl-2"]').click();
    for (var key in self.solarPanels) {
        if (self.shift) {
            if (ele.sp_id === self.solarPanels[key].sp_id && self.shift) {
                if (self.solarPanels[key].sp_selected) {
                    self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                } else {
                    self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                }
                self.solarPanels[key].sp_selected = ele.sp_selected ? false : true;
            }
            document.getElementById('individual_rotation').value = self.solarPanels[key].sp_rotation;
        } else {
            if (ele.sp_id === self.solarPanels[key].sp_id) {
                if (self.solarPanels[key].sp_selected) {
                    self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                } else {
                    self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                }
                self.solarPanels[key].sp_selected = ele.sp_selected ? false : true;
                document.getElementById('individual_rotation').value = self.solarPanels[key].sp_rotation;
                document.getElementById('tilt').value = self.solarPanels[key].sp_tilt;
                document.getElementById('panel_list').value = self.solarPanels[key].sp_panel_id;
                var event = new Event('change');
                document.getElementById('panel_list').dispatchEvent(event);
            } else {
                self.solarPanels[key].sp_selected = false;
                self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
            }
        }
    }
    self.countSelected('selection');
    self.toggleMounting();
};
solarMap.prototype.getCurrentSelectedPanel = function () {
    if (this.debug)
        console.log('getCurrentSelectedPanel')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            return key;
            break;
        }
    }
    return key;    
};
solarMap.prototype.selectAll = function (action) {
    var self = this;
    for (var key in self.solarPanels) {
        if (action == 'all') {
            self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
            self.solarPanels[key].sp_selected = true;
        } else {
            self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
            self.solarPanels[key].sp_selected = false;
        }
    }
};


/** removeSolarPanel
 *  Used to unselect sola 
 * */
solarMap.prototype.removeSolarPanel = function (ele) {
    var self = this;
    var deletedIndex = [];
    var deletedNotes = 0;
    var totalNodes = self.solarPanels.length;
    for (var k = 0; k < self.solarPanels.length; k++) {
        if (k < 0) {
            break;
        }
        if (self.solarPanels[k].sp_selected) {
            deletedNotes++;
            self.solarPanels[k].setMap(null);
            self.solarPanels.splice(k, 1);
            k--;
        }
    }
    var finalLength = self.solarPanels.length;
    if(finalLength){
        self.resetSolarPanel(self.solarPanels[finalLength-1],null);
    }
    if (deletedNotes == totalNodes) {
        var action = document.getElementById('select-toggle').getAttribute('data-selected')
        if (action == null || action == 'none') {
            document.getElementById('select-toggle').innerHTML = 'DESELECT ALL';
            document.getElementById('select-toggle').setAttribute('data-selected', 'all');
            self.selectAll('all');
        } else if (action == 'all') {
            document.getElementById('select-toggle').innerHTML = 'SELECT ALL';
            document.getElementById('select-toggle').setAttribute('data-selected', 'none');
            self.selectAll('none');
        }
    }

    if(self.solarPanels.length == 0 && window.solar_proposal_manager.panel_row_id != ''){
        $('.solar_proposal_product_delete_btn').each(function(){
            if($(this).attr('data-id') == window.solar_proposal_manager.panel_row_id){
                window.solar_proposal_manager.panel_row_id = '';
                $(this).click();
            }
        });
    }
    self.countSelected();
};

solarMap.prototype.performAction = function (amount, action) {
    var self = this;
    switch (action) {
        case 'add':
            self.addSolarPanel(amount);
            break;
        case 'remove':
            self.removeSolarPanel(self.solarPanels[key]);
            break;
        case 'tilt':
            self.tiltPanel(amount);
            break;
        case 'rotate':
            var distanceMatrixFromCentroid = self.centroid();
            var res = self.countSelected();
            if (res.selected > 1) {
                // selected panels are multiple so lets rotate them by their centroid
                var distanceMatrixFromCentroid = self.centroid();
                for (var key in distanceMatrixFromCentroid['distance']) {
                    var r = parseInt(distanceMatrixFromCentroid['distance'][key]['heading']) + parseInt(self.rotationHeading);
                    var p = google.maps.geometry.spherical.computeOffset(distanceMatrixFromCentroid['centroid'], distanceMatrixFromCentroid['distance'][key]['distance'], r);
                    var x = key.split('-');
                    self.solarPanels[x[1]].moveTo(p);
//                    break;
                }
            }
            for (var key in self.solarPanels) {
                if (self.solarPanels[key].sp_selected) {
                    var netRotation = amount - self.solarPanels[key].sp_rotation;
//                    netRotation = netRotation * -1;
                    var origin = {'lat': self.solarPanels[key].getPath().getAt(0).lat(), 'lng': self.solarPanels[key].getPath().getAt(0).lng()}
                    self.rotatePanel(self.solarPanels[key], netRotation, amount);
                }
            }
            break;
    }
};

/** getPanelDimensionByLatLong
 *  Get solar panel 4 coordinate by 1 coordinate and dimension input 
 * */
solarMap.prototype.getPanelDimensionByLatLong = function (coordinate, metersEast, metersSouth, tile) {
    if (this.debug)
        console.log('getPanelDimensionByLatLong')
    var indvidual_tilt_in_degree = typeof tile == 'undefined' ? document.getElementById('tilt').value : (tile.sp_tilt || document.getElementById('tilt').value);
    indvidual_tilt_in_degree = indvidual_tilt_in_degree * (Math.PI / 180);
    metersSouth = metersSouth * Math.cos(indvidual_tilt_in_degree);
    var ne = google.maps.geometry.spherical.computeOffset(coordinate, metersEast, 90);
    var sw = google.maps.geometry.spherical.computeOffset(coordinate, metersSouth, 180);
    return new google.maps.LatLngBounds(sw, ne);
};

solarMap.prototype.rotatePanel = function (polygon, angle, actualRotation, overridePanel) {
    if (this.debug)
        console.log('rotatePanel')
    if (typeof actualRotation != 'undefined' && (actualRotation >= 360 || actualRotation <= -360)) {
        if (actualRotation > 0) {
            actualRotation = actualRotation - 360;
        } else {
            actualRotation = parseInt(actualRotation) + parseInt(360);
        }
    }
    var self = this;
    actualRotation = actualRotation || 0;

    // if the rotation is negetive then change it to equivalent positive value
    if (actualRotation < 0) {
        actualRotation = 360 + actualRotation;
    }

    var map = polygon.getMap();
    var prj = map.getProjection();
    if (overridePanel != null) {
        var origin = prj.fromLatLngToPoint(polygon.getPath().getAt(1)); //rotate around first point    
    } else {
        var origin = prj.fromLatLngToPoint(polygon.getBounds().getCenter());
    }
    var coords = polygon.getPath().getArray().map(function (latLng) {
        var point = prj.fromLatLngToPoint(latLng);
        var rotatedLatLng = prj.fromPointToLatLng(self.rotatePoint(point, origin, angle));
        return {lat: rotatedLatLng.lat(), lng: rotatedLatLng.lng()};
    });
    var arrawCords = self.arrowCords(coords, polygon.sp_orientation);
    polygon.setPaths([coords, arrawCords]);
    polygon.setOptions({'sp_rotation': actualRotation});
    self.updateTileProperties(polygon.sp_id, 'sp_rotation', actualRotation);
    document.getElementById('individual_rotation').value = actualRotation;
};

solarMap.prototype.createPolygonFromRectangle = function (rectangle) {
    if (this.debug)
        console.log('createPolygonFromRectangle')
    var self = this;
    var map = rectangle.getMap();

    var coords = [
        {lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getNorthEast().lng()},
        {lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getSouthWest().lng()},
        {lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getSouthWest().lng()},
        {lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getNorthEast().lng()}
    ];
    var inner = self.arrowCords(coords, rectangle.sp_orientation);
    var properties = [
        "sp_watt",
        "sp_panel_value",
        "sp_panel_id",
        "sp_tilt",
        "sp_orientation",
        "strokeColor",
        "strokeOpacity",
        "strokeWeight",
        "fillOpacity",
        "fillColor",
        "draggable",
        "sp_selected",
        "sp_id",
        "sp_rotation"
    ];
    //inherit rectangle properties 
    var options = {};
    properties.forEach(function (property) {
        if (rectangle.hasOwnProperty(property)) {
            options[property] = rectangle[property];
        }
    });

    options['paths'] = [coords, inner];
    var rectPoly = new google.maps.Polygon(options);
    rectPoly.setOptions(options);
    rectangle.setMap(null);
    rectPoly.setMap(map);
    google.maps.event.addListener(rectPoly, 'mousedown', function (event) {
        self.dragStartReference = rectPoly.getBounds().getCenter();
    });
    google.maps.event.addListener(rectPoly, 'dragend', function (event) {
        if (rectPoly.sp_selected) {
            for (var key in self.solarPanels) {
                if (rectPoly.sp_id == self.solarPanels[key].sp_id) {
                    var te = self.solarPanels[key].getBounds().getCenter();
                    break;
                }
            }
            for (var key in self.solarPanels) {
                if (rectPoly.sp_id !== self.solarPanels[key].sp_id) {
                    if (self.solarPanels[key].sp_selected) {
                        var distance = google.maps.geometry.spherical.computeDistanceBetween(self.dragStartReference, te);
                        var heading = google.maps.geometry.spherical.computeHeading(self.dragStartReference, te);
                        var x = new google.maps.geometry.spherical.computeOffset(self.solarPanels[key].getBounds().getCenter(), distance, heading);
                        self.solarPanels[key].moveTo(x);
                    }
                }
            }
        }
    });
    google.maps.event.addListener(rectPoly, 'drag', function (event) {
        var a = self.createJstsPolygon(self.geometryFactory, rectPoly);
        for (var key in self.solarPanels) {
            if (rectPoly.sp_id != self.solarPanels[key].sp_id) {
                var b = self.createJstsPolygon(self.geometryFactory, self.solarPanels[key]);
                var intersection = a.intersects(b);
                if (intersection) {
                    self.solarPanels[key].setOptions({'fillColor': self.overlappedSolarPanelColor});
                } else {
                    if (self.solarPanels[key].sp_selected) {
                        if (self.solarPanels[key].fillColor == self.overlappedSolarPanelColor) {
                            self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                        }
                    } else {
                        self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                    }
                }
            }
        }
    });
    return rectPoly;
};
solarMap.prototype.createJstsPolygon = function (geometryFactory, polygon) {
    var path = polygon.getPath();
    var coordinates = path.getArray().map(function name(coord) {
        return new jsts.geom.Coordinate(coord.lat(), coord.lng());
    });
    coordinates.push(coordinates[0]);
    var shell = geometryFactory.createLinearRing(coordinates);
    return geometryFactory.createPolygon(shell);
};

solarMap.prototype.panelOrientation = function (o) {
    if (this.debug)
        console.log('panelOrientation')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            if (o == 'portrait') {
                // rotate 90 right
                var angle = 90;
                var temp = parseInt(self.solarPanels[key].sp_rotation) + parseInt(angle);
            } else {
                // rotate 90 left
                var angle = -90;
                var temp = parseInt(self.solarPanels[key].sp_rotation) + parseInt(angle);
            }
            //   self.solarPanels[key].setOptions({'sp_orientation': o});
            document.getElementById('rotation').value = temp;
            self.defaultRotation = temp;
            self.rotatePanel(self.solarPanels[key], angle, temp);
        }
    }
    self.countSelected();
};
solarMap.prototype.changeMounting = function (o) {
    if (this.debug)
        console.log('changeMounting')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            if (self.solarPanels[key].sp_orientation == o) {
                continue;
            }
            self.defaultOrientation = o;
            self.solarPanels[key].setOptions({'sp_orientation': o});
            self.tiltPanel(0);
        }
    }
    self.countSelected();
};
solarMap.prototype.tiltPanel = function (degree) {
    if (this.debug)
        console.log('tiltPanel')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            self.addSolarPanel(1, key);
        }
    }
};
solarMap.prototype.countSelected = function (action) {
    action = (action) ? action : true;
    if (this.debug)
        console.log('countSelected')
    var self = this;
    var total = 0;
    var selected = 0;
    var wattageCapacity = 0;
    for (var key in self.solarPanels) {
        total++;
        wattageCapacity = parseFloat(self.solarPanels[key].sp_watt) + parseFloat(wattageCapacity)
        if (self.solarPanels[key].sp_selected) {
            selected++;
        }
    }

    var wattageCapacityNum = (wattageCapacity / 1000);
    wattageCapacity = wattageCapacityNum + 'kW';
    self.totalPanels = total;
    self.selectedPanels = selected;
    if(total > 0){
        var product_data = 'solar_product[prd_id]='+self.panelId+'&solar_product[prd_qty]='+total+'&solar_product_id='+window.solar_proposal_manager.panel_row_id;
        window.solar_proposal_manager.add_proposal_product(product_data);
        document.getElementById('selected').innerHTML = selected + ' out of ' + total + ' are selected';
        document.getElementById('watt_capacity').innerHTML = wattageCapacity;
        document.getElementById('total_system_size').value = wattageCapacityNum;
        document.querySelector('.no_of_panels').value = parseInt(total);
    }
    
    //Save Map Data in Proposal
    self.exportMap();
    window.solar_proposal_manager.save_solar_proposal(false);
    //document.getElementById('system_size').value = wattageCapacityNum;
    //document.getElementById('sc_system_size').value = wattageCapacityNum;
    //document.getElementById('sc_sb_system_size').value = wattageCapacityNum;


    /**
    window.proposal_calculator_obj.near_map_panels = self.exportMap();
    if (window.proposal_calculator_obj.deal_type == '1') {
        window.proposal_calculator_obj.savings_calculator_only_solar(false);
    } else if (window.proposal_calculator_obj.deal_type == '2') {
        window.proposal_calculator_obj.savings_calculator_only_solar_battery(false);
    } else {
        window.proposal_calculator_obj.savings_calculator_only_solar(false);
    }*/
    self.toggleMounting();
    return {selected: selected, total: total, wattag_capacity: wattageCapacity};
};
solarMap.prototype.rotatePoint = function (point, origin, angle) {
    var angleRad = angle * Math.PI / 180.0;
    return {
        x: Math.cos(angleRad) * (point.x - origin.x) - Math.sin(angleRad) * (point.y - origin.y) + origin.x,
        y: Math.sin(angleRad) * (point.x - origin.x) + Math.cos(angleRad) * (point.y - origin.y) + origin.y
    };
};

solarMap.prototype.middlePoint = function (c1, c2) {
    var Bx = Math.cos(c2.lat) * Math.cos(c2.lng - c1.lng);
    var By = Math.cos(c2.lat) * Math.sin(c2.lng - c1.lng);
    var latMid = Math.atan(Math.sin(c1.lat) + Math.sin(c2.lat), Math.sqrt((Math.cos(c1.lat) + Bx) * (Math.cos(c1.lat) + Bx) + By * By));
    var longMid = c1.lng + Math.atan(By, Math.cos(c1.lat) + Bx);
    return new google.maps.LatLng(latMid, longMid);
};

solarMap.prototype.exportMap = function () {
    if (this.debug)
        console.log('exportMap')
    var self = this;
    var exported = [];
    var properties = [
        "sp_watt",
        "sp_panel_value",
        "sp_panel_id",
        "sp_tilt",
        "sp_orientation",
        "strokeColor",
        "strokeOpacity",
        "strokeWeight",
        "fillOpacity",
        "fillColor",
        "draggable",
        "sp_selected",
        "sp_id",
        "sp_rotation"
    ];
    var center = self.map_obj.getCenter();
    var main = {center: {lat: center.lat(), lng: center.lng()}, global_rotation: document.getElementById('rotation').value, tilt: document.getElementById('tilt').value};
    for (var i = 0; i < self.solarPanels.length; i++) {
        var options = {};
        options['position'] = self.solarPanels[i].getBounds().getCenter();//{lat: self.solarPanels[i].getPath().getAt(0).lat(), lng: self.solarPanels[i].getPath().getAt(2).lng()};
        properties.forEach(function (property) {
            if (self.solarPanels[i].hasOwnProperty(property)) {
                options[property] = self.solarPanels[i][property];
            }
        });
        options['sp_selected'] = false;
        options['fillColor'] = self.defaultSolarPanelColor;
        exported.push(options);
    }
    if (exported.length) {
        main['center'] = exported[0]['position'];
        main['sp_panel_value'] = exported[0]['sp_panel_value'];
        main['sp_panel_id'] = exported[0]['sp_panel_id'];
    }
    main['tiles'] = exported;
    var exportedData = JSON.stringify(main);
    $('#near-map-exported-data').val(exportedData);
    return exportedData;
};
solarMap.prototype.mapToImage = function () {
    var self = this;
//    self.loader('show', 'Processing map to generate image');
    function transform(div) {
        if (div != undefined) {
            var transform = $(div).css("transform");
            var comp = transform.split(",");
            var mapleft = parseFloat(comp[4]);
            var maptop = parseFloat(comp[5]);
            $(div).css({
                "transform": "none",
                "left": mapleft,
                "top": maptop,
            })
            return transform;
        } else {
            return null;
        }
    }
    function transformBack(div, transform) {
        if (div != undefined) {
            $(div).css({
                left: 0,
                top: 0,
                "transform": transform
            });
        }
    }
    for (var k = 0; k < self.transformationFixForImage.length; k++) {
        self.transformationFixForImage[k].transform = transform($(self.transformationFixForImage[k].elem)[0]);
    }
    if (self.current_location_marker) {
        self.current_location_marker.setMap(null);
    }
    $(".gm-style-pbc").hide();
    $('#tool-bar').css({"opacity": "0"});
    $('#tool-bar').hide();
    $('.gm-bundled-control').hide();
    $('.gm-bundled-control').css({"opacity": "0"});
    $('.gm-fullscreen-control').hide();
    $('.gm-fullscreen-control').css({"opacity": "0"});
    var width = document.getElementById(self.map_element_id).offsetWidth;
    var height = document.getElementById(self.map_element_id).offsetHeight;
    $('#proposal_image > .add-picture').removeAttr('style');

    html2canvas(document.getElementById(self.map_element_id), {
        useCORS: true,
        width: width,
        height: height,
        onrendered: function (canvas) {
            var dataUrl = canvas.toDataURL("image/png");
            $('#image').val(dataUrl);
            $('#proposal_no_image').attr('style', 'display:none !important;');
            $('#proposal_image').attr('style', 'display:block !important;');
//            var dataUrl = 'https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg';
            $('#proposal_image > .add-picture').css('background-image', 'url("' + dataUrl + '")');
            $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
            $('#proposal_image > .add-picture').css('background-position', '0px 0px');
            $('#proposal_image > .add-picture').css('background-size', '108% 103%');
            $('#tab_image_upload_btn').trigger('click');
            self.exportMap();
//            self.loader('hide', 'Image processing is complete');
            $('#tool-bar').show();
            $('#tool-bar').css({"opacity": "1"});
            $('.gm-bundled-control').show();
            $('.gm-fullscreen-control').show();
            $('.gm-fullscreen-control').css({"opacity": "1"});
            $('.gm-bundled-control').css({"opacity": "1"});
            $(".gm-style-pbc").show();
            for (var k = 0; k < self.transformationFixForImage.length; k++) {
                transformBack($(self.transformationFixForImage[k].elem)[0], self.transformationFixForImage[k].transform);
            }
            self.addMarker(self.current_location_coordinates.lat, self.current_location_coordinates.lng);
        }
    });
};

/** extend
 *  Object merger
 * */
solarMap.prototype.extend = function (a, b) {
    for (var key in b)
        if (b.hasOwnProperty(key))
            a[key] = b[key];
    return a;
};

/** randomId
 *  Used to get unique random string 
 * */
solarMap.prototype.randomId = function () {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
};

solarMap.prototype.nearMap = function () {
    if (this.debug)
        console.log('nearMap');
    var self = this;
    var apikey = this.nearMapApiKey;

    function degreesToRadians(deg) {
        return deg * (Math.PI / 180);
    }
    function radiansToDegrees(rad) {
        return rad / (Math.PI / 180);
    }
    function regionForCoordinate(x, y, zoom) {
        var x_z1 = x / Math.pow(2, (zoom - 1));
        if (x_z1 < 1) {
            return 'us';
        } else {
            return 'au';
        }
    }
    function rotateTile(coord, zoom, heading) {
        var numTiles = 1 << zoom;
        var x, y;
        switch (heading) {
            case 0:
                x = coord.x;
                y = coord.y;
                break;
            case 90:
                x = numTiles - (coord.y + 1);
                y = coord.x;
                break;
            case 180:
                x = numTiles - (coord.x + 1);
                y = numTiles - (coord.y + 1);
                break;
            case 270:
                x = coord.y;
                y = numTiles - (coord.x + 1);
                break
        }
        return new google.maps.Point(x, y);
    }

    function MercatorProjection(worldWidth, worldHeight) {
        this.pixelOrigin = new google.maps.Point(worldWidth / 2, worldHeight / 2);
        this.pixelsPerLonDegree = worldWidth / 360;
        this.pixelsPerLatRadian = worldHeight / (2 * Math.PI);
    }
    MercatorProjection.prototype.fromLatLngToPoint = function (latlng, opt_point) {
        var point = opt_point || new google.maps.Point(0, 0);
        var origin = this.pixelOrigin;
        var lat = latlng.lat();
        var lng = latlng.lng();
        point.x = origin.x + lng * this.pixelsPerLonDegree;
        var siny = Math.sin(degreesToRadians(lat));
        point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -this.pixelsPerLatRadian;
        return point;
    };
    MercatorProjection.prototype.fromPointToLatLng = function (point, noWrap) {
        var origin = this.pixelOrigin;
        var lng = (point.x - origin.x) / this.pixelsPerLonDegree;
        var latRadians = (point.y - origin.y) / -this.pixelsPerLatRadian;
        var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
        return new google.maps.LatLng(lat, lng);
    };
    const projVertical = new MercatorProjection(256, 256);

    function vertToWest(ll) {
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        ll = projVertical.fromPointToLatLng(pt);
        return new google.maps.LatLng(ll.lat(), -ll.lng());
    }

    function westToVert(ll) {
        ll = new google.maps.LatLng(ll.lat(), -ll.lng());
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        return projVertical.fromPointToLatLng(pt);
    }

    function vertToEast(ll) {
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        ll = projVertical.fromPointToLatLng(pt);
        return new google.maps.LatLng(-ll.lat(), ll.lng());
    }

    function eastToVert(ll) {
        ll = new google.maps.LatLng(-ll.lat(), ll.lng());
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        return projVertical.fromPointToLatLng(pt);
    }

    function fakeLatLng(mapTypeId, ll) {
        if (mapTypeId === 'W') {
            ll = vertToWest(ll);
        } else if (mapTypeId === 'E') {
            ll = vertToEast(ll);
        } else if (mapTypeId === 'S') {
            ll = new google.maps.LatLng(-ll.lat(), -ll.lng());
        }
        return ll;
    }

    function realLatLng(mapTypeId, ll) {
        if (mapTypeId === 'W') {
            ll = westToVert(ll);
        } else if (mapTypeId === 'E') {
            ll = eastToVert(ll);
        } else if (mapTypeId === 'S') {
            ll = new google.maps.LatLng(-ll.lat(), -ll.lng());
        }
        return ll;
    }

    function registerProjectionWorkaround(map) {
        var currMapType = map.getMapTypeId();
        map.addListener('maptypeid_changed', function () {
            var center = realLatLng(currMapType, map.getCenter());
            currMapType = map.getMapTypeId();
            map.setCenter(fakeLatLng(currMapType, center));
        })
    }

    function createMapType(tileWidth, tileHeight, heading, name) {
        var maptype = new google.maps.ImageMapType({
            name: name,
            tileSize: new google.maps.Size(tileWidth, tileHeight),
            isPng: !0,
            minZoom: 1,
            maxZoom: 24,
            getTileUrl: function (coord, zoom) {
                coord = rotateTile(coord, zoom, heading);
                var x = coord.x;
                var y = coord.y;
                var url = 'https://' + regionForCoordinate(x, y, zoom) + '0.nearmap.com/maps/hl=en' + '&x=' + x + '&y=' + y + '&z=' + zoom + '&nml=' + name + '&httpauth=false&version=2' + '&apikey=' + apikey;
                return url
            }
        });
        maptype.projection = new MercatorProjection(tileWidth, tileHeight);
        return maptype;
    }

    function createBookmarkControl(map) {
        var bookmarkControlDiv = document.createElement('div');
        var bookmarkControl = new BookmarkControl(bookmarkControlDiv, map);
        bookmarkControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(bookmarkControlDiv);
    }

    function BookmarkControl(controlDiv, map) {
        function createBookmarkLink(label, point) {
            var bookmarkLink = document.createElement('span');
            bookmarkLink.className = 'bookmark-link';
            bookmarkLink.innerHTML = label;
            bookmarkLink.addEventListener('click', function () {
                map.setCenter(fakeLatLng(map.getMapTypeId(), point));
            });
            return bookmarkLink;
        }
    }
    if (apikey === undefined || apikey.length === 0) {
        alert('Please provide your Nearmap API Key');
        return
    }
    var map_types = [createMapType(256, 256, 0, 'Vert'), createMapType(256, 181, 0, 'N'), createMapType(256, 181, 90, 'E'), createMapType(256, 181, 180, 'S'), createMapType(256, 181, 270, 'W'), ];
    var mapOptions = {
        zoom: 15,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'Vert', 'N', 'E', 'S', 'W'],
            streetViewControl: !1
        },
        disableDefaultUI: !0,
        center: self.map_default_options.center,
        mapTypeControl: !1,
        streetViewControl: !1,
        fullscreenControl: true,
        draggableCursor: 'crosshair',
        rotateControl: !1,
    };
    var spherical = google.maps.geometry.spherical;
    this.map_obj = new google.maps.Map(document.getElementById(self.map_element_id), mapOptions);
    for (var i = 0; i < map_types.length; i++) {
        var mt = map_types[i];
        this.map_obj.mapTypes.set(mt.name, mt);
    }
    registerProjectionWorkaround(this.map_obj);
    this.map_obj.setMapTypeId('Vert');
    createBookmarkControl(this.map_obj);
};
solarMap.prototype.loader = function (action, message) {
    document.getElementById('img-ldr-msg').innerHTML = message;
    if (action == 'show') {
        document.getElementById('img-ldr').style.display = 'block';
    } else {
        document.getElementById('img-ldr').style.display = 'none';
    }
};



solarMap.prototype.latLng2Point = function (latLng) {
    return this.map_obj.getProjection().fromLatLngToPoint(latLng);

//    var topRight = this.map_obj.getProjection().fromLatLngToPoint(this.map_obj.getBounds().getNorthEast());
//    var bottomLeft = this.map_obj.getProjection().fromLatLngToPoint(this.map_obj.getBounds().getSouthWest());
//    var scale = Math.pow(2, this.map_obj.getZoom());
//    var worldPoint = this.map_obj.getProjection().fromLatLngToPoint(latLng);
//    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
};

solarMap.prototype.point2LatLng = function (point) {
    return this.map_obj.getProjection().fromPointToLatLng(point)
//    var topRight = this.map_obj.getProjection().fromLatLngToPoint(this.map_obj.getBounds().getNorthEast());
//    var bottomLeft = this.map_obj.getProjection().fromLatLngToPoint(this.map_obj.getBounds().getSouthWest());
//    var scale = Math.pow(2, this.map_obj.getZoom());
//    var worldPoint = new google.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
//    return this.map_obj.getProjection().fromPointToLatLng(worldPoint);
};

solarMap.prototype.getX4Y4 = function (x1y1, x2y2, x4y4) {
    var x1 = x1y1.x;
    var y1 = x1y1.y;
    var x2 = x2y2.x;
    var y2 = x2y2.y;
    var x4 = x4y4.x;
    var y4 = x4y4.y;
    var x3 = x2 + x4 - x1;
    var y3 = y2 + y4 - y1;
    return {x: x3, y: y3}
};
solarMap.prototype.plotPolyline = function (event) {
    var self = this;
    var path = self.line.getPath();
    path.push(event.latLng);
    var marker = new google.maps.Marker({
        position: event.latLng,
        title: '#' + path.getLength(),
        map: self.map_obj
    });
};
solarMap.prototype.calculateDistanceBetween2Points = function (line) {
    var self = this;
    if (this.debug)
        console.log('calculateDistanceBetween2Points');
    var path = line.getPath().getArray();
    var distance = 0;

    if (path.length > 2) {
        for (var k in path) {
            var latLngA = path[k];
            var i = parseInt(k) + parseInt(1);
            if (typeof path[i] == 'undefined') {
                break;
            }
            var latLngB = path[i];
            var d = google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB);
            distance += parseFloat(d);
        }
    } else {
        var latLngA = path[0];
        var latLngB = path[path.length - 1];
        distance = google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB);
    }
    distance = distance.toFixed(1);
    if (self.distanceMarker) {
        self.distanceMarker.setMap(null);
    }

    var canvas = document.createElement("canvas");
    var halfDistance = distance / 2;
    distance = distance + 'm';
    canvas.width = 11 * distance.length;
    canvas.height = 20;
    var ctx = canvas.getContext("2d");
    ctx.font = "bold 16px Arial";
    ctx.fillStyle = '#000000';
    ctx.fillText(distance, 5, 15);
    var img = canvas.toDataURL("image/png");

    var a = self.latLng2Point(latLngA)
    var b = self.latLng2Point(latLngB)
    var c = {};
    c['x'] = (parseFloat(a.x) + parseFloat(b.x)) / 2;
    c['y'] = (parseFloat(a.y) + parseFloat(b.y)) / 2;
    var m = self.point2LatLng(c)
    self.distanceMarker = new google.maps.Marker({
        position: m,
        map: self.map_obj,
        icon: img
    });
};

solarMap.prototype.centroid = function () {
    var self = this;
    var bound = new google.maps.LatLngBounds();
    var distance = {};
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            bound.extend(self.solarPanels[key].getBounds().getCenter());
        }
    }
    var centroid = bound.getCenter();

//    self.distanceMarker = new google.maps.Marker({
//        position: centroid,
//        map: self.map_obj
//    });
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            var a = self.solarPanels[key].getBounds().getCenter();
            var t = 'key-' + key;
            distance[t] = {'distance': google.maps.geometry.spherical.computeDistanceBetween(centroid, a), 'heading': google.maps.geometry.spherical.computeHeading(centroid, a)};
        }
    }
    return {centroid: centroid, distance: distance};
};
solarMap.prototype.centerOfCoords = function (coord) {
    var bound = new google.maps.LatLngBounds();
    for (var i = 0; i < coord.length; i++) {
        bound.extend(coord[i]);
    }
    return bound.getCenter();
};

solarMap.prototype.arrowCords = function (coords, orientation) {
    var self = this;
    var c = self.centerOfCoords(coords);
    var a = self.centerOfCoords([coords[0], c]);
    // if (self.defaultOrientation == 'portrait') {
    var b = self.centerOfCoords([coords[1], c]);
    var mid = self.centerOfCoords([coords[0], coords[1]]);
//    } else {
//        var b = self.centerOfCoords([coords[3], c]);
//        var mid = self.centerOfCoords([coords[0], coords[3]]);
//    }
    var mid2 = self.centerOfCoords([mid, c]);
    var absMid = self.centerOfCoords([mid, mid2]);
    return [a, absMid, b, absMid, a];
};
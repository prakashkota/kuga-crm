var form_builder = function (options) {
    var self = this;
    this.template_id = options.template_id;
    this.proposal_id = options.proposal_id;
    this.field_counter = 0;
    document.onreadystatechange = function () {
        if (document.readyState == "interactive") {
            //$("#loader").show();
            $("#page-container").find('img').each(function () {
                $(this).attr('src', base_url + "assets/form_builder/uploads/" + self.template_id + "/" + $(this).attr('src'));
            });
        }
    };

    $("#save_btn").on('click', function () {
        self.save_template_data();
    });

    $(".delete_btn").on('click', function () {
        //self.delete_template_fields();
    });

    $("#download_btn").on('click', function () {
        function downloadInnerHtml(filename, elId, mimeType) {
            var elHtml = document.querySelector(elId).innerHTML;
            var data = {html: elHtml, template_id: self.template_id};
            $.ajax({
                type: 'post',
                datatype: 'json',
                data: data,
                url: base_url + 'admin/form-builder/generate_html',
                beforeSend: function (xhr) {

                },
                success: function (stat) {
                    stat = JSON.parse(stat);
                    if (stat.success) {
                        window.open('http://111.93.41.194:8066/tabletinduction/admin/company-template/generatehtml1?url=https://solarrunapp.com.au/staging/franchisor/assets/form_builder/uploads/' + self.template_id + '/1.html');
                    } else {

                    }
                }
            });
        }
        var fileName = 'tags.html';
        downloadInnerHtml(fileName, '#template_form', 'text/html');
        return false;

    });

    $("#radioElement,#selectElement").on('click', function () {
        $(this).next().toggle('slow');
    });

    $("#checkboxElement").on('click', function () {
        $(this).next().slideToggle();
    });


    if (self.template_id != '') {
        self.load_template();
    }


}

form_builder.prototype.load_template = function () {
    var self = this;
    $.ajax({
        type: 'get',
        datatype: 'json',
        url: base_url + 'admin/form-builder/fetch_template_fields?template_id=' + self.template_id + '&proposal_id=' + self.proposal_id,
        beforeSend: function (xhr) {

        },
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success) {
                self.handle_template_fields(stat);
            }
        }
    });
};

form_builder.prototype.hoverdiv = function (e, divid, type) {
    var div = document.getElementById(divid);
    if (type == 'show') {
        $("#" + divid).addClass('changeCursor');
        div.style.opacity = 1;
    } else {
        $("#" + divid).removeClass('changeCursor');
        div.style.opacity = 0;
    }
    return false;
};

form_builder.prototype.handle_template_fields = function (data) {
    var self = this;
    var attributes = data.template_attributes;
    var proposal_data = data.proposal_data;
    var template_data = data.template_data;
    self.field_counter = (attributes.length > 0) ? attributes.length : 0;
    $("#counter").val(self.field_counter);
    for (var i = 0; i < attributes.length; i++) {
        var page_no = (attributes[i].page_no) ? attributes[i].page_no : 1;
        //Radio and Checkbox
        if (attributes[i].field_type == 'radio-group' || attributes[i].field_type == 'checkbox-group') {
            var attribute_options = data.template_attribute_options[i];
            for (var j = 0; j < attribute_options.length; j++) {
                $("#pf" + page_no + " .pc").append(attribute_options[j].html_contain);
                $("#pf" + page_no + " .pc").children().last().find('div').remove();
                $("#pf" + page_no + " .pc").children().last().find('input').attr('id', attribute_options[j].id);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('Attrid', attribute_options[j].template_attribute_id);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('attr-label', attribute_options[j].field_label);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('name', 'form_data[' + attributes[i].id + ']');
                $("#pf" + page_no + " .pc").children().last().find('input').attr('value', attribute_options[j].value);
            }
        } else if (attributes[i].field_type == 'signature') {
            $("#pf" + page_no + " .pc").append(attributes[i].html_contain);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('data-id', attributes[i].id);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('title', attributes[i].field_title);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('label', attributes[i].field_label);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').removeAttr('id');
            $("#pf" + page_no + " .pc").children().last().find('div.signData').append('<input name="form_data[' + attributes[i].id + ']" type="hidden" id="' + attributes[i].id + '" /><img src="" id="signature_img_' + attributes[i].id + '">');
        } else {
            $("#pf" + page_no + " .pc").append(attributes[i].html_contain);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea,select').attr('id', attributes[i].id);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea,select').attr('name', 'form_data[' + attributes[i].id + ']');
            $("#pf" + page_no + " .pc").children().last().find('input,textarea,select').attr('title', attributes[i].field_title);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea,select').attr('label', attributes[i].field_label);

            var key = attributes[i].field_name;
            if (key in proposal_data) {
                $("#pf" + page_no + " .pc").children().last().find('input,textarea,select').attr('value', proposal_data[key]);
            }
            if (attributes[i].field_type == 'hidden') {
                $('body').append('<input type="file" style="display:none;"  id="image_upload_'+attributes[i].id+'" />');
            }
        }
    }

    //Run loop on template data
    for (var i = 0; i < template_data.length; i++) {
        $('#signature_img_' + template_data[i].field_id).attr('src', base_url + 'assets/uploads/lead_files/' + template_data[i].field_data);
        $("#" + template_data[i].field_id).val(template_data[i].field_data);

        //Image data
        if (template_data[i].field_data != '' && template_data[i].field_type == 'hidden') {
            $this = $("#" + template_data[i].field_id);
            $this.removeClass('focused');
            $this.next().val(template_data[i].field_data);
            $this.attr('src', base_url + 'assets/uploads/lead_files/' + template_data[i].field_data);
            $this.attr('style', 'width:inherit;height:inherit;');
            $this.siblings('span.image_area').hide();
            $("#" + template_data[i].field_id).on('click', function () {
                var ele = $(this);
                var id = ele.attr('id');
                $('#image_upload_'+id).click();
                self.create_image_upload(ele);
            });
        }
    }

    $.getScript(base_url + "assets/form_builder/js/jquery-ui.min.js").done(function (script, textStatus) {
        $("#page-container").css('overflow', 'hidden');
        var wcount = 0;
        $("#page-container div[id^='pf']").each(function () {
            wcount = parseInt(wcount + $(this).height());
        });
        $("#page-container").height(wcount)
        $("input,textarea,div.signData,select").removeClass('focused');

        $(".signData").on('click', function () {
            self.create_signature_pad($(this));
        });
        
    });
    
    $("span.image_area").on('click', function () {
       var ele = $(this).prev().prev();
       var id = ele.attr('id');
       $('#image_upload_'+id).click();
       self.create_image_upload(ele);
    });
    
};


form_builder.prototype.save_template_data = function (data) {
    var self = this;
    var form_data = $('form').serialize() + '&template_id=' + self.template_id + '&proposal_id=' + self.proposal_id;
    $('#loader1').html(createLoader('Please wait while data is being saved'));
    $.ajax({
        type: 'POST',
        datatype: 'json',
        data: form_data,
        url: base_url + 'admin/lead/save_template_data',
        beforeSend: function (xhr) {

        },
        success: function (stat) {
            stat = JSON.parse(stat);
            $('#loader1').html('');
            toastr["success"](stat.status);
        }
    });
};

form_builder.prototype.create_image_upload = function (ele) {
    var id = ele.attr('id');
    console.log($('#image_upload_'+id));
    $('#image_upload_'+id).off('change').on('change',function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'lead_files');
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/proposal/upload_image', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    console.log($('#' + id));
                    $('#' + id).next().val(res.file_name);
                    $('#' + id).attr('src', base_url + 'assets/uploads/lead_files/' + res.file_name);
                    $('#' + id).attr('style', 'width:inherit;height:inherit;');
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
}

form_builder.prototype.create_signature_pad = function (ele) {
    var id = ele.attr('data-id');
    $('#signature_pad_modal').modal('show');
    var wrapper = document.getElementById("signature-pad");
    var clearButton = document.querySelector("#clear_signature");
    var undoButton = document.querySelector("#undo_signature");
    var savePNGButton = document.querySelector("#save_signature");
    var canvas = wrapper.querySelector("canvas");
    var signaturePad = new SignaturePad(canvas, {
        backgroundColor: 'rgb(255, 255, 255)'
    });

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
    function resizeCanvas() {
        // When zoomed out to less than 100%, for some very strange reason,
        // some browsers report devicePixelRatio as less than 1
        // and only part of the canvas is cleared then.
        var ratio = Math.max(window.devicePixelRatio || 1, 1);

        // This part causes the canvas to be cleared
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);

        // This library does not listen for canvas changes, so after the canvas is automatically
        // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
        // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
        // that the state of this library is consistent with visual state of the canvas, you
        // have to clear it manually.
        signaturePad.clear();
    }

// On mobile devices it might make more sense to listen to orientation change,
// rather than window resize events.
    window.onresize = resizeCanvas;
    resizeCanvas();

    function download(dataURL, filename) {
        var blob = dataURLToBlob(dataURL);
        var url = window.URL.createObjectURL(blob);

        var a = document.createElement("a");
        a.style = "display: none";
        a.href = url;
        a.download = filename;

        document.body.appendChild(a);
        a.click();

        window.URL.revokeObjectURL(url);
    }

// One could simply use Canvas#toBlob method instead, but it's just to show
// that it can be done using result of SignaturePad#toDataURL.
    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.addEventListener("click", function (event) {
        signaturePad.clear();
    });

    undoButton.addEventListener("click", function (event) {
        var data = signaturePad.toData();

        if (data) {
            data.pop(); // remove the last dot or line
            signaturePad.fromData(data);
        }
    });

    savePNGButton.addEventListener("click", function (event) {
        if (signaturePad.isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            var dataURL = signaturePad.toDataURL();
            $('#signature_img_' + id).attr('src', dataURL);
            $('#' + id).val(dataURL);
            $('#signature_pad_modal').modal('hide');
        }
    });

};
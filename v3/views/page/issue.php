<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"> Log An Issue</h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/page/log-issue/manage') ?>"><i class="icon-list"></i> Manage Issues </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div id="message"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <form method="post" action="" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Customer</label>
                                       <select name="priority" class="form-control">
                                            <option value="1" > Business Critical </option>
                                            <option value="2"> Important </option>
                                            <option value="3"> Normal </option>
                                            <option value="4"> Low </option>
                                        </select>
                                        <?php echo form_error('user_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>URL</label>
                                        <input type="text" class="form-control" placeholder="URL" name="url" value="<?php echo isset($url) ? $url : ''; ?>" required />
                                    </div>
                                    <div class="form-group">
                                        <label>Attachment</label>
                                        <div class="custom-file">
                                            <input type="file" id="attachment" class="custom-file-input" value="<?php echo isset($image) ? $image : ''; ?>">
                                            <input type="hidden" name="image" id="image" class="form-control" value="<?php echo isset($image) ? $image : ''; ?>">
                                            <label class="custom-file-label" for="datasheets">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea type="text" name="description" class="form-control" placeholder="Description" required><?php echo isset($description) ? $description : ''; ?></textarea>
                                        <?php echo form_error('description', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Franchise</label>
                                        <select name="user_id" class="form-control"  required>
                                            <?php foreach ($franchises as $key => $value) { ?>
                                                <option value="<?php echo $value['user_id']; ?>" <?php if ($user_id == $value['user_id']) { ?> selected="" <?php } ?>><?php echo $value['full_name']; ?></option>   
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('user_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Customer</label>
                                        <input type="text" class="form-control" id="customer" />
                                        <input type="hidden" class="form-control" name="cust_id" id="cust_id" />
                                       
                                    </div>
                                    <div class="buttwrap">
                                        <input type="submit"  class="btn" id="submit_btn" value="Submit">
                                        <div id="loader"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<script src='<?php echo site_url(); ?>assets/js/jquery-ui.min.js' type='text/javascript'></script>

<script>
    $('#attachment').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'issue_files');
        toastr["info"]("Uploading image please wait...");
        $('#submit_btn').attr('disabled', 'disabled');
        $.ajax({
            url: base_url + 'admin/page/upload_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                $('#submit_btn').removeAttr('disabled');
                $('#loader').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#image').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function () {
                $('#submit_btn').removeAttr('disabled');
                $('#loader').html('');
            }
        });

    });

    //Some weird issue is coming thats why using setTimeout might be due to some js conflict,
    $('#customer').autocomplete({
        autoSelect: false,
        autoFocus: false,
        minLength: 2,
        source: function (request, response) {
            $.ajax({
                url: base_url + "admin/customer/fetch_customers_by_keyword",
                type: 'post',
                dataType: "json",
                data: {search: request.term, request: 1},
                success: function (data) {
                    response(data.customers);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },
        focus: function (event, ui) {
            $('#customer').val(ui.item.name);
            $('#cust_id').val(ui.item.value);
        },
        select: function (event, ui) {
            setTimeout(function(){
                $('#customer').val(ui.item.name);
                $('#cust_id').val(ui.item.value);
            },100);
        }
    });

</script>
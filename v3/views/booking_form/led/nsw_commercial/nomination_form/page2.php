<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td valign="top">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="100%" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/banner.png') ?>" width="100%" height="120" alt=""/></td>
                        </tr>
                        <tr>
                            <td height="18"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 18px; color: #00408a;"><strong>Post Implementation Declaration</strong></td>
                        </tr>
                        <tr>
                            <td height="35" style="font-family: Arial, sans-serif; font-size: 16px; color: #00408a;"><strong>Commercial Lighting Energy Savings Formula (building lighting)</strong></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td height="30" style="border-bottom: solid 4px #007bc4; font-family: Arial, sans-serif; font-size: 17px; color: #000000"><strong>Part A - Building lighting quality statement</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top" height="22">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td width="91" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">On behalf of</td>
                                            <td width="436" style="border-bottom: solid 1px #000000; font-family: Arial, sans-serif;font-size: 14px; color: #000000">&nbsp;</td>
                                            <td width="383" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">(solution provider) I confirm that the lighting upgrade at</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" height="22">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td width="452" style="border-bottom: solid 1px #000000; font-family: Arial, sans-serif;font-size: 14px; color: #000000">
                                                <input required="" type="text" style="width:80%; margin-left:40px; background:#fffcdc;" name="get_nomination_form_details[certificate_provider_details][site_address]" />
                                            </td>
                                            <td width="458" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">(site address) has been implemented to meet the requirements of the</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">AS/NZS 1680 Standard series. I confirm that the following details have been considered and satisfied for the lighting upgrade:</td>
                        </tr>
                        <tr>
                            <td height="15"></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                    <tbody>
                                        <tr>
                                            <td width="27" align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td width="883" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Maintained illuminance (lighting) levels recommended by AS/NZS 1680 for each space where the lighting upgrade has been installed</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Lumen (light level) depreciation of the lighting system over the life of the upgrade</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Control of glare</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Uniformity of illuminance (lighting)</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Any additional requirements of AS/NZS 1680 that are relevant to the areas where the lighting upgrade has been installed, and</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The recommended maintenance regime of the lighting system (attached to this document). In addition, I declare that:</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The requirements of the Building Code of Australia (BCA) section F4.4 Safe Movement, have been met in all areas of the
                                            lighting upgrade.</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The illumination power density (IPD) of the lighting system after the upgrade does not exceed the maximum IPD allowed under
                                            Part J6 of the BCA.</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The information I have provided is complete and accurate and I am aware that there are penalties for providing false and
                                            misleading information in this form.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid 4px #44546a" height="15"></td>
                        </tr>
                        <tr>
                            <td height="5"></td>
                        </tr>
                        <tr>
                            <td height="25" style="font-family: Arial, sans-serif; font-size: 14px; color: #44546a"><strong>Note:</strong></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 13px; color: #000000"><em>Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading 
                          information to the Scheme Administrator.</em></td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid 4px #44546a" height="15"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 15px; color: #44546a">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 15px; color: #44546a"><strong>Signed by or on behalf of the Accredited Certificate Provider</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="1" bordercolor="#007bc4" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border: solid 1px #007bc4">
                                    <tbody>
                                        <tr>
                                            <td width="181" height="35" style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Signature</strong></td>
                                            <td width="729">
                                                <div class="signature-pad">
													<div class="signature-pad--body">
														<div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_5" data-id="5" class="sign_create"></div>
														<input required="" id="get_nomination_form_details_certificate_provider_signature" name="get_nomination_form_details[certificate_provider_details][signature]" type="hidden"/>
													</div>
												</div>
												<div class="signature-pad-image signature_image_container" style="display:none !important;">
													<img src="" />
													<a class="signature_image_close" href="javscript:void(0);"  ></a>
												</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Name</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%; margin-top: 10px;" name="get_nomination_form_details[certificate_provider_details][name1]"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Position</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%; margin-top: 10px;" name="get_nomination_form_details[certificate_provider_details][position]"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Company</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%; margin-top: 10px;" name="get_nomination_form_details[certificate_provider_details][company]"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Date</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; width: 100%; margin-top:10px;" name="get_nomination_form_details[certificate_provider_details][date]"  />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
          
                        <tr>
                            <td height="100">&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/bottom.png') ?>" width="100%" height="56" alt=""/></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
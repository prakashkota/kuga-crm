<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td valign="top">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="100%" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/banner.png') ?>" width="100%" height="120" alt=""/></td>
                        </tr>
                        <tr>
                            <td height="18"></td>
                        </tr>
                        <tr>
                            <td height="30" style="border-bottom: solid 4px #007bc4; font-family: Arial, sans-serif; font-size: 17px; color: #000000"><strong>Part B - Maintenance schedule</strong></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">To ensure that the lighting system (lamps and fittings) continues to operate correctly for its full lifetime; maintenance of the lighting</td>
                        </tr>
                        <tr>
                            <td valign="top" height="22">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td width="214" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">equipment should be conducted.</td>
                                            <td width="242" style="border-bottom: solid 1px #000000; font-family: Arial, sans-serif;font-size: 14px; color: #000000">&nbsp;</td>
                                            <td width="454" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">(solution provider) encourages you to follow this maintenance schedule.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" style="font-family: Arial, sans-serif; font-size: 14px; color: #44546a"><strong>Recommended maintenance schedule</strong></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%" border="1" bordercolor="#007bc4" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border: solid 1px #007bc4;">
                                    <tbody>
                                        <tr>
                                            <td width="182" height="45" bgcolor="#d5dce4" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #007bc4"><strong>Area</strong></td>
                                            <td width="182" bgcolor="#d5dce4" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #007bc4"><strong>Lamp/fitting type</strong></td>
                                            <td width="182" bgcolor="#d5dce4" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #007bc4"><strong>Assumed hours of use per year</strong></td>
                                            <td width="182" bgcolor="#d5dce4" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #007bc4"><strong>Recommended lamp replacement interval</strong></td>
                                            <td width="182" bgcolor="#d5dce4" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #007bc4"><strong>Cleaning schedule</strong></td>
                                        </tr>
                                        <?php for($i=0; $i < 12; $i++){ ?>
                                            <tr>
                                              <td height="32" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697">
                                                  <input type="text" name="get_nomination_form_details[maintenance_schedule_items][area][<?php echo $i; ?>]" style="background:#fff; border:solid 1px #cecdcd;  height:30px; padding-left:5px; padding-right:5px; width:160px;" />
                                              </td>
                                              <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697">
                                                  <input type="text" name="get_nomination_form_details[maintenance_schedule_items][fitting_type][<?php echo $i; ?>]" style="background:#fff; border:solid 1px #cecdcd;  height:30px; padding-left:5px; padding-right:5px; width:160px;" />
                                              </td>
                                              <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697">
                                                  <input type="text" name="get_nomination_form_details[maintenance_schedule_items][hours][<?php echo $i; ?>]" style="background:#fff; border:solid 1px #cecdcd;  height:30px; padding-left:5px; padding-right:5px; width:160px;" />
                                              </td>
                                              <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697">
                                                   <input type="text" name="get_nomination_form_details[maintenance_schedule_items][recommended_lamp][<?php echo $i; ?>]" style="background:#fff; border:solid 1px #cecdcd;  height:30px; padding-left:5px; padding-right:5px; width:160px;" /> 
                                              </td>
                                              <td style="border-bottom: solid 1px #969697;">
                                                    <input type="text" name="get_nomination_form_details[maintenance_schedule_items][cleaning_schedule][<?php echo $i; ?>]" style="background:#fff; border:solid 1px #cecdcd;  height:30px; padding-left:5px; padding-right:5px; width:160px;" />
                                              </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="30" style="border-bottom: solid 4px #007bc4; font-family: Arial, sans-serif; font-size: 17px; color: #000000"><strong>Part C - Customer declaration</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td width="270" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">By signing below, I confirm that on behalf of</td>
                                            <td width="263" style="border-bottom: solid 1px #000000; font-family: Arial, sans-serif;font-size: 14px; color: #000000">
                                                <input required="" type="text" style="border:0; width:100%; margin-left:20px;" name="get_nomination_form_details[energy_saver_details][entity_name1]" />
                                            </td>
                                            <td width="365" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">(original energy saver):</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                    <tbody>
                                        <tr>
                                            <td width="27" align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td width="883" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">I am satisfied that the lighting upgrade implemented by, or under the supervision of National Carbon Bank of Australia, maintains
                        or improves on the lighting service that was provided previously.</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The corporate has paid a net amount of at least $5 (excluding GST) for each megawatt hour of electricity saved and this amount
                        will not be reimbursed (eg, through a credit or rebate).</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">I have been provided with a recommended maintenance schedule to ensure that the installed lighting system (lamps and fittings)
                        continues to operate appropriately for its full lifetime.</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">I am aware that I may be asked by IPART or an IPART approved auditor to provide information that assists in verifying that
                        Energy Savings Certificates are created in accordance with the requirements of the ESS.</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/arrow.png') ?>" width="10" height="9" alt=""/></td>
                                            <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">The information I have provided is complete and accurate and I am aware that there are penalties for providing false and
                        misleading information in this form.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" style="border-bottom: solid 4px #44546a;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="25" style="font-family: Arial, sans-serif; font-size: 14px; color: #44546a"><strong>Note:</strong></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 13px; color: #000000"><em>Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading 
            information to the Scheme Administrator.</em></td>
                        </tr>
                        <tr>
                            <td height="10" style="border-bottom: solid 4px #44546a;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="22"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 15px; color: #44546a"><strong>Signed by or on behalf of the original energy saver</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%" border="1" bordercolor="#007bc4" cellspacing="0" cellpadding="6" style="border-collapse: collapse; border: solid 1px #007bc4">
                                    <tbody>
                                        <tr>
                                            <td width="234" height="35" style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Signature</strong></td>
                                            <td width="650">
                                                <div class="signature-pad">
													<div class="signature-pad--body">
														<div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_6" data-id="6" class="sign_create"></div>
														<input required="" id="get_nomination_form_details_energy_saver_details_signature" name="get_nomination_form_details[energy_saver_details][signature]" type="hidden"/>
													</div>
												</div>
												<div class="signature-pad-image signature_image_container" style="display:none !important;">
													<img src="" />
													<a class="signature_image_close" href="javscript:void(0);"  ></a>
												</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Name</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%;margin-top: 10px;" name="get_nomination_form_details[energy_saver_details][name2]" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Position</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%;margin-top: 10px;" name="get_nomination_form_details[energy_saver_details][position]" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Implementation address(es)</strong></td>
                                            <td>
                                                <input type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%;margin-top: 10px;" name="get_nomination_form_details[certificate_provider_details][site_address]" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #c0e7ff; font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Date</strong></td>
                                            <td>
                                                <input required="" type="text" style="border: 0px; width: 100%; margin-top: 10px;" name="get_nomination_form_details[energy_saver_details][date]" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                      
                        <tr>
                            <td height="100">&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top"><img src="<?= base_url('job_crm/assets/customer_declaration_form/bottom.png') ?>" width="100%" height="56" alt=""/></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
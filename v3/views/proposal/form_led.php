<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;}
    .mask{    margin-left: -15px;}
    #proposal_image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 30px;
        right: 2px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }


    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #000 !important;
        opacity: 1; /* Firefox */
    }

    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }

    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }

    .remove_field{
        color:red;
    }

    .monthly-event-list{
        top:22.5em !important ;
        height:calc(100% - 25.5em) !important;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }
    .steps li {
        display: inline-block;
        margin-bottom: 3px;
    }
    .steps li a, .steps li p {
        background: #e5f4fd;
        padding: 8px 20px;
        color: #0077bf;
        display: block;
        font-size: 14px;
        font-weight: bold;
        position: relative;
        text-indent: 12px;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li a:before, .steps li p:before {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #fff;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        left: 0;
        top: 50%;
        width: 0;
        margin-top: -18px;
    }
    .steps li a:after, .steps li p:after {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #e5f4fd;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        /*right: -12px;*/
        left:100%;
        top: 50%;
        width: 0;
        margin-top: -18px;
        z-index: 1;
    }
    .steps li.active a, .steps li.active p {
        background: #5cb85c;
        color: #fff;
    }
    .steps li.active a:after, .steps li.active p:after {
        border-left: 12px solid #5cb85c;
    }
    .steps li.undone a, .steps li.undone p {
        background: #eee;
        color: #333;
    }
    .steps li.undone a:after, .steps li.undone p:after {
        border-left: 12px solid #eee;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }

    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }

    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }

    .sr-no_wrap{
        white-space: nowrap;
    }

    .sr-inactive_user{
        color:red;
    }

    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    .card_body__title h2{
        display:inline-flex;
        padding-bottom: 10px;
    }

    .form-block{margin-bottom: 0px !important;}

    .kg-activity{
        border-left-color: #f0ad4e;
        border: 1px solid #f0ad4e;
        border-left-width: .25rem;
        border-radius: .25rem;
    }

    .kg-activity__description{
        background-color:#ffffe0;
        margin: -12px;
        padding: 12px;
    }

    .card_row_1{
        min-height: 290px;
    }

    @media only screen and (min-width: 992px){
        .modal-content{width:900px;}
        .modal-dialog{max-width:800px;}
    }
    
    @media only screen and (max-width: 991px){
        .modal-content{width:360px;}
    }
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/lead/add?deal_ref=' . $lead_data['uuid']) ?>"><i class="icon-arrow-left"></i> Back to Deal </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Customer Details</h2>
                                            <a style="margin-left:20px;" id="customer_location_edit_btn" href="javascript:void(0);" ><i class="fa fa-pencil"></i> Edit Customer</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group">
                                                <input type="text"  id="site_name" placeholder="Site Name" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group">
                                                <input type="text"  id="site_address" placeholder="Address" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" id="customer_name" placeholder="Organization Contact Person" class="form-control gray-bg" readonly>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" id="site_contact_name" placeholder="Site Contact Person" class="form-control gray-bg" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-8">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Proposal Details</h2>  
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="row">
                                                <div class="col-4 col-sm-4 col-md-4">
                                                    <div class="table-default">
                                                        <table width="100%" border="0">
                                                            <tbody id="proposal_item_details_wrapper">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-sm-8 col-md-8">
                                                    <div class="table-default">
                                                        <table width="100%" border="0">
                                                            <tbody id="proposal_cost_details_wrapper">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="btn-block ">
                                                        <button id="manage_finance_proposal_btn" class="btn btn-primary"><i class="fa fa-money"></i> Manage Finance</button>
                                                        <a id="download_led_proposal" href="javascript:void(0);" class="btn btn-primary" target="__blank"><i class="fa fa-file-pdf-o"></i> Download Proposal</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card" id="led_proposal_finance_container" style="display:none;">
                            <div class="card-body">
                                <div class="card_body__title">
                                    <h2>Finance</h2>  
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="" method="post" id="proposal_finance_form">
                                        <div class="form-row">
                                            <input type="hidden" name="proposal_finance[proposal_type]" value="1" />
                                            <div class="col-md-4 mb-3">
                                                <label for="term">Term (Months)</label>
                                                <select class="form-control" name="proposal_finance[term]" id="term" required="">
                                                    <option value="">Select one</option>
                                                    <?php
                                                    $term_months = unserialize(TERM_MONTHS);
                                                    foreach ($term_months as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <label for="monthly_payment_plan">Monthly Payment Plan</label>
                                                    <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                        </div>
                                                        <input type="number" name="proposal_finance[monthly_payment_plan]" class="form-control" id="monthly_payment_plan"   required="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3" id="finance_loader"></div>
                                            <div class="btn-block mt-2">
                                                <input type="button" id="save_proposal_finance_btn" value="Update"  class="btn btn-primary">
                                                <input type="button" id="delete_proposal_finance_btn" value="Remove"  class="btn btn-primary hidden">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card_body__title">
                                    <h2>Energy Usage</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="" method="post" id="led_energy_form">
                                        <input type="hidden" name="energy" value="Energy">
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <label for="energy_rate">Energy Rate ($0.00/kwh)</label>
                                                <input type="number" name="led_proposal[energy_rate]" class="form-control" id="energy_rate" placeholder="Please Enter Energy Rate ($0.00/kwh)" min="0" step="0.01" oninput="validity.valid||(value='');"  required>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="operatingHoursPerDay">Operating Hours p/Day</label>
                                                <input type="number" name="led_proposal[op_hrs]" class="form-control" id="operatingHoursPerDay" placeholder="Please Enter Operating Hours"  min="1" max="24" step="0.01" oninput="validity.valid||(value='');"  required="">
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="energy_rate">Operating Days p/Week</label>
                                                <input type="number" name="led_proposal[op_days]" class="form-control" id="operatingDaysPerWeek" placeholder="Please Enter Operating Days" min="1"  max="7" step="0.01" oninput="validity.valid||(value='');" required="">
                                            </div>
                                        </div>

                                        <div class="btn-block mt-2">
                                            <input type="button" id="save_proposal_btn" value="Update" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card_body__title">
                                    <h2>Job List</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="#" method="post" id="led_products_form">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label>Space Type</label>
                                                    <select class="form-control" name="led_product[space_type_id]" id="space_type" required>
                                                        <option value="">Select one</option>
                                                        <?php
                                                        if (!empty($space_types)) {
                                                            foreach ($space_types as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['space_type_id']; ?>" data-hours="<?php echo $value['hours']; ?>"><?php echo $value['classification']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12 hidden">
                                                        <div class="form-group control-group">
                                                            <label class="required-label">Hours</label>
                                                            <input class="form-control" placeholder="Please Enter Hours" type="text" name="led_product[hours]" id="hours" value="" required="">
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-md-6">
                                                        <div class="form-group control-group">
                                                            <label class="required-label">Lifetime</label>
                                                            <select id="lifetime" name="led_product[lifetime]" class="form-control" disabled="">
                                                                <option value="">Select one</option>
                                                                <option value="Lifetime A">Lifetime A</option>
                                                                <option value="Lifetime C">Lifetime C</option>
                                                            </select> 
                                                        </div>
                                                    </div>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label>Existing Fitting</label>
                                                    <select class="form-control" name="led_product[ep_id]" name="ep_id" id="ep_id" required>
                                                        <option value="">Select one</option>
                                                        <?php
                                                        if (!empty($existing_fittings)) {
                                                            foreach ($existing_fittings as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['ep_id']; ?>" ><?php echo $value['ep_name']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Qty (Existing Fitting)</label>
                                                    <input class="form-control" placeholder="Please Enter Qty (Existing Fitting)" type="number" name="led_product[ep_qty]" name="ep_qty" id="ep_qty" value="" min="1" oninput="validity.valid||(value='');" required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label>New Fitting</label>
                                                    <div id="newFittingidDiv">
                                                        <select class="form-control" name="led_product[np_id]" id="np_id" required>
                                                            <option value="">Select one</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Qty (New Fitting)</label>
                                                    <input class="form-control" placeholder="Please Enter Qty (New Fitting)" type="number" name="led_product[np_qty]" id="np_qty" value="" min="1" oninput="validity.valid||(value='');" required="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label>Rebate value</label>
                                                    <input class="form-control" placeholder="Please Enter VEEC" type="number" name="led_product[veec]" id="veec" min="0" step="0.01" oninput="validity.valid||(value='');" value="" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Unit Price (exGST)</label>
                                                    <input class="form-control" placeholder="Please Enter Unit Price" type="number" name="led_product[price]" id="price" min="0" step="0.01" oninput="validity.valid||(value='');"  value="" required="">  
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group control-group">
                                                    <label>Do you want to attach product image ?</label>
                                                    &nbsp;&nbsp;<input type="radio" name="attach" value="1" checked=""> Yes &nbsp; <input type="radio" name="attach" value="0"> No
                                                </div>
                                            </div>
                                        </div> -->
                                        <input type="button" id="add_product_btn" value="Add Product" class="btn btn-primary">
                                        <input type="button" id="update_product_btn" value="Update Product" class="btn btn-primary hidden">
                                        <input type="button" id="cancel_product_btn" value="Cancel" class="btn btn-primary hidden">
                                        <input type="hidden" id="led_product_id" name="led_product_id">
                                    </form>
                                </div>

                                <div class="form-block d-block clearfix mt-5">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="led_proposal_products_table" >
                                            <thead>
                                                <tr>
                                                    <th>Space Type</th>
                                                    <th>Hours</th>
                                                    <th>Existing Fitting</th>
                                                    <th>Qty (Existing Fitting)</th>
                                                    <th>New Fitting</th>
                                                    <th>Qty (New Fitting)</th>
                                                    <th>Rebate</th>
                                                    <th>Unit Price (exGST)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="led_proposal_products_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card_body__title">
                                    <h2>Additional Items</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="" method="post" id="add_additional_item_form">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label>Item</label>
                                                    <select class="form-control"  name="ae_id" id="ae_id" required="">
                                                        <option value="">Select one</option>
                                                        <?php
                                                        if (!empty($access_equipments)) {
                                                            foreach ($access_equipments as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['ae_id']; ?>" data-cost="<?php echo $value['ae_cost']; ?>"><?php echo $value['ae_name']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Quantity</label>
                                                    <input class="form-control" placeholder="Please Enter Quantity" type="number" name="ae_qty" id="ae_qty"  min="1"  oninput="validity.valid||(value='');" value="1" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Cost (exGST)</label>
                                                    <input class="form-control" placeholder="Please Enter Cost" type="number" name="ae_cost" id="ae_cost" min="0" step="0.01" oninput="validity.valid||(value='');" value="" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="button" id="add_additional_item_btn" value="Add Additional Item" class="btn btn-primary">
                                    </form>
                                </div>

                                <div class="form-block d-block clearfix mt-5">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="led_proposal_additional_item_table" >
                                            <thead>
                                                <tr>
                                                    <th>Item Name</th>
                                                    <th>Qty</th>
                                                    <th>Cost (exGST)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="led_proposal_additional_item_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<script src="<?php echo site_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>assets/js/isLoading.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="<?php echo site_url(); ?>common/js/led_proposal.js?v=<?php echo version; ?>" type="text/javascript"></script>
<script>
     $(document).ready(function () {
        $('.locationstreetAddress').placecomplete2({
            placeServiceResult2: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }
                    
                    //Set Address
                    if ($('.locationstreetAddressVal') != undefined) {
                        $('.locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('.locationState') != undefined) {
                                var select = document.querySelector(".locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.querySelector(".locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    }
                                }
                               
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('.locationPostCode') != undefined) {
                                $('.locationPostCode').val(address_components[i].long_name);
                                $('.locationPostCode').trigger('change');
                            }
                        }
                    }
                } else{
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'fr'
        });

    });

    var context = {};
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.proposal_data = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? json_encode($proposal_data, JSON_HEX_APOS) : ''; ?>';
    context.lead_uuid = '<?php echo (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : ''; ?>';
    context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
    context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
    var led_proposal_manager = new led_proposal_manager(context);
</script>
</body>
</html>


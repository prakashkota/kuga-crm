
var led_booking_form_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.booking_form_uuid = (options.booking_form_uuid && options.booking_form_uuid != '') ? options.booking_form_uuid : '';
    this.site_id = (options.site_id && options.site_id != '') ? options.site_id : '';
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.product_subtotal_excGST = 0;
    this.ae_subtotal_excGST = 0;
    this.total_incGST = 0;
    this.total_excGST = 0;
    this.total_GST = 0;
    this.signature_pad = [];
    this.signature_pad_ids = ['authorised_on_behalf_signature', 'authorised_by_behalf_sales_rep_signature', 'sales_rep_signature'];

    $("#authorised_on_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#authorised_by_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#booked_at").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });


    if (self.booking_form_uuid != '') {
        self.fetch_led_booking_form_data();
    } else if (Object.keys(self.lead_data).length > 0) {
        $('#entity_name').val(self.lead_data.customer_company_name);
        $('#address').val(self.lead_data.address);
        $('#postcode').val(self.lead_data.postcode);

        $('#authorised_on_behalf_company_name').val(self.lead_data.customer_company_name);
        $('#authorised_on_behalf_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#authorised_on_behalf_position').val(self.lead_data.position);


        $('#first_name').val(self.lead_data.first_name);
        $('#last_name').val(self.lead_data.last_name);
        $('#contact_no').val(self.lead_data.customer_contact_no);
        $('#position').val(self.lead_data.position);
        $('#email').val(self.lead_data.customer_email);

        $("#authorised_on_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
        $("#authorised_by_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
        $("#booked_at").val(moment(new Date()).format('DD/MM/YYYY'));

    }

    $('#save_booking_form').click(function () {
        self.save_booking_form();
    });

    $('#generate_booking_form_pdf').click(function () {
        self.save_booking_form(true);
    });

    //Handle Singature Pads
    $('.sign_create').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').hide();
        $('.sr-deal_actions').hide();
        $('#signatures').show();
        for (var i = 0; i < 4; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        window.scrollTo({top: 0, behavior: 'smooth'});
    });

    $('.sign_close').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').show();
        $('.sr-deal_actions').show();
        $('#signatures').hide();
        for (var i = 0; i < 3; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });

    $('#is_upfront').change(function () {
        $('#is_upfront').attr('checked', 'checked');
        $('#is_finance').removeAttr('checked');
    });

    $('#is_finance').change(function () {
        $('#is_finance').attr('checked', 'checked');
        $('#is_upfront').removeAttr('checked');
    });
    
    //Check box to copy details from Authroised to Accounts
    $('#same_as_authorised_details_btn').click(function () {
        if($(this)[0].checked == true){
            if($('#first_name').val() != ''){
                $('#acc_first_name').val($('#first_name').val());
            }
            if($('#last_name').val() != ''){
                $('#acc_last_name').val($('#last_name').val());
            }
            if($('#contact_no').val() != ''){
                $('#acc_contact_no').val($('#contact_no').val());
            }
            if($('#landline').val() != ''){
                $('#acc_landline').val($('#landline').val());
            }
            if($('#email').val() != ''){
                $('#acc_email').val($('#email').val());
            }
        }else{
            if($('#first_name').val() != ''){
                $('#acc_first_name').val('');
            }
            if($('#last_name').val() != ''){
                $('#acc_last_name').val('');
            }
            if($('#contact_no').val() != ''){
                $('#acc_contact_no').val('');
            }
            if($('#landline').val() != ''){
                $('#acc_landline').val('');
            }
            if($('#email').val() != ''){
                $('#acc_email').val('');
            }
        }
    });
    
    self.handle_items();
    self.upload_image();
    self.show_image();
    self.hide_image();
    self.remove_signature_image();
    self.create_signature_pad();
};

led_booking_form_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


led_booking_form_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

led_booking_form_manager.prototype.handle_items = function () {
    var self = this;
    $('.item_qty,.item_cost_excGST').change(function () {
        self.product_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        $('table#booking_items tr').each(function () {
            var price = $(this).children().find(".item_cost_excGST").val();
            var qty = $(this).children().find(".item_qty").val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(this).children().find(".item_total_cost_excGst").val(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST) + parseFloat(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }else{
                $(this).children().find(".item_total_cost_excGst").val('');
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }
        });
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST);
        $('#total_incGst').val(self.total_incGST);
        $('#total_Gst').val(self.total_GST);
    });

    $('.ae_qty,.ae_cost_excGST').change(function () {
        self.ae_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        for (var i = 0; i < 2; i++) {
            var price = $(".ae_cost_excGST_" + i).val();
            var qty = $(".ae_qty_" + i).val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(".ae_total_cost_excGst_" + i).val(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST) + parseFloat(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }else{
                $(".ae_total_cost_excGst_" + i).val('');
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }
        }
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST);
        $('#total_incGst').val(self.total_incGST);
        $('#total_Gst').val(self.total_GST);
    });


};

led_booking_form_manager.prototype.upload_image = function () {
    var self = this;
    $('.image_upload').change(function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'led_booking_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

led_booking_form_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/led_booking_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

led_booking_form_manager.prototype.hide_image = function () {
    var self = this;
    $('.image_close').click(function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};

led_booking_form_manager.prototype.create_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad2 = new SignaturePad(canvas2, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad3 = new SignaturePad(canvas3, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1, signaturePad2, signaturePad3];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        //signaturePad1.clear();

        canvas2.width = canvas2.offsetWidth * ratio;
        canvas2.height = canvas2.offsetHeight * ratio;
        canvas2.getContext("2d").scale(ratio, ratio);
        //signaturePad2.clear();

        canvas3.width = canvas3.offsetWidth * ratio;
        canvas3.height = canvas3.offsetHeight * ratio;
        canvas3.getContext("2d").scale(ratio, ratio);
        //signaturePad3.clear();
    }

    window.onorientationchange = resizeCanvas;
    resizeCanvas();

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        console.log(arr[(id - 1)]);
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 1)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 1)].toDataURL();
            var ele = $('#' + arr1[(id - 1)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
}

led_booking_form_manager.prototype.show_signature_image = function (ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.prev().html('<img style="height:95px;" src="' + image + '" />');
        } else {
            ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/led_booking_form_files/' + image + '" />');
        }
    }

    //Signateur Wrapper hide
    //ele.parent().parent().hide();
    //Show Image Wrapper
    //ele.parent().parent().next('div').show();
    //ele.parent().parent().next('div').children('img').attr('src', image);
};

led_booking_form_manager.prototype.remove_signature_image = function () {
    var self = this;
    $('.signature_image_close').click(function () {
        $(this).parent().prev('div').show();
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('img').attr('src', '');
    });
};

led_booking_form_manager.prototype.validate_booking_form = function () {
    var self = this;
    var flag = true;
    $('.invalid-feedback').remove();
    var elements = document.getElementById("led_booking_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        $(elements[i]).parent().removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
            } else {
                $(elements[i]).parent().parent().removeClass('is-invalid');
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'checkbox') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }

    var payment_option_flag = true;
    var is_upfront_checked = document.getElementById('is_upfront').checked;
    var is_finance_checked = document.getElementById('is_finance').checked;
    $('#is_upfront').parent().parent().removeClass('is-invalid');
    $('#upfront_excGST').removeClass('is-invalid');
    $('#upfront_GST').removeClass('is-invalid');
    $('#upfront_incGST').removeClass('is-invalid');
    $('#finance_excGST').removeClass('is-invalid');
    $('#finance_GST').removeClass('is-invalid');
    $('#finance_incGST').removeClass('is-invalid');

    if (is_upfront_checked) {
        var upfront_excGST = document.getElementById('upfront_excGST').value;
        var upfront_GST = document.getElementById('upfront_GST').value;
        var upfront_incGST = document.getElementById('upfront_incGST').value;

        if (upfront_excGST == '') {
            $('#upfront_excGST').addClass('is-invalid');
            $('#upfront_excGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }

        if (upfront_GST == '') {
            $('#upfront_GST').addClass('is-invalid');
            $('#upfront_GST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }

        if (upfront_incGST == '') {
            $('#upfront_incGST').addClass('is-invalid');
            $('#upfront_incGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
            payment_option_flag = false;
        }
    } else if (is_upfront_checked == false && is_finance_checked == false) {
        $('#is_upfront').parent().parent().addClass('is-invalid');
        payment_option_flag = false;
    }



    if (payment_option_flag && is_upfront_checked == false) {
        if (is_finance_checked) {
            var finance_excGST = document.getElementById('finance_excGST').value;
            var finance_GST = document.getElementById('finance_GST').value;
            var finance_incGST = document.getElementById('finance_incGST').value;

            if (finance_excGST == '') {
                $('#finance_excGST').addClass('is-invalid');
                $('#finance_excGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }

            if (finance_GST == '') {
                $('#finance_GST').addClass('is-invalid');
                $('#finance_GST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }

            if (finance_incGST == '') {
                $('#finance_incGST').addClass('is-invalid');
                $('#finance_incGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                payment_option_flag = false;
            }
        }
    }

    flag = (payment_option_flag == false) ? false : flag;

    return flag;
};

led_booking_form_manager.prototype.save_booking_form = function (download) {
    var self = this;

    /** //Convert Signature to Images
     var signature_pad = self.signature_pad;
     var signature_pad_ids = self.signature_pad_ids;
     for (var i=0; i < signature_pad.length; i++) {
     if (signature_pad[i].isEmpty()) {
     } else {
     var dataURL = signature_pad[i].toDataURL();
     var ele = $('#' + signature_pad_ids[i]);
     self.show_signature_image(ele, dataURL);
     }
     } */

    if(download){
        var flag = self.validate_booking_form();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Please fill the fields marked in red');
            return false;
        }
    }

    if (self.booking_form_uuid == '') {
        var uuid = self.create_uuid();
        self.booking_form_uuid = uuid;
    }

    var formData = $('#led_booking_form').serialize();
    formData += '&uuid=' + self.booking_form_uuid;
    formData += '&lead_id=' + self.lead_data.id;
    formData += '&site_id=' + self.site_id;

    $.ajax({
        url: base_url + 'admin/booking_form/save_led_booking_form',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]('Saving Quote Data, Please Wait....');
            $("#save_booking_form").attr("disabled", "disabled");
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.booking_form_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/booking_form/edit_led_booking_form/' + self.booking_form_uuid);
                }
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["success"](response.status);
                    setTimeout(function(){
                        window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                    },1500);
                }
            } else {
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.fetch_led_booking_form_data = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/booking_form/fetch_led_booking_form_data',
        datatype: 'json',
        data: {uuid: self.booking_form_uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var booking_data = stat.booking_data;
                self.handle_led_booking_form_data(booking_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.handle_led_booking_form_data = function (data) {
    var self = this;

    //Handle Business Details
    var business_details = JSON.parse(data.business_details);
    for (var key in business_details) {
        $("input[name='business_details[" + key + "]']").val(business_details[key]);
    }

    //Handle Authorised Details
    var authorised_details = JSON.parse(data.authorised_details);
    for (var key in authorised_details) {
        $("input[name='authorised_details[" + key + "]']").val(authorised_details[key]);
    }

    //Handle Account Details
    var account_details = JSON.parse(data.account_details);
    for (var key in account_details) {
        $("input[name='account_details[" + key + "]']").val(account_details[key]);
    }

    //Handle Space Type
    var space_type = JSON.parse(data.space_type);
    for (var key in space_type) {
        
        $("select[name='space_type[" + key + "]']").val(space_type[key]);
    }

    //Handle Ceiling Height
    var ceiling_height = JSON.parse(data.ceiling_height);
    for (var key in ceiling_height) {
        $("input[name='ceiling_height[" + key + "]']").val(ceiling_height[key]);
    }

    //Handle Boom Req
    var boom_req = JSON.parse(data.boom_req);
    for (var key in boom_req) {
        $("input[name='boom_req[" + key + "]']").val(boom_req[key]);
    }

    //Handle Product
    var product = JSON.parse(data.product);
    for (var key in product) {
        for (var i = 0; i < product[key].length; i++) {
            $("." + key + "_" + i).val(product[key][i]);
        }
    }
    $('.item_qty').trigger('change');

    //Handle Access Equipments
    var ae = JSON.parse(data.ae);
    for (var key in ae) {
        for (var i = 0; i < ae[key].length; i++) {
            $("." + key + "_" + i).val(ae[key][i]);
        }
    }
    $('.ae_qty').trigger('change');


    //Handle Booking Form Data
    var booking_form = JSON.parse(data.booking_form);
    for (var key in booking_form) {
        if (key == 'authorised_on_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'signature') {
                    self.show_signature_image($('#authorised_on_behalf_signature'), booking_form[key]['signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'authorised_by_behalf') {

            for (var key1 in booking_form[key]) {
                if (key1 == 'sales_rep_signature') {
                    self.show_signature_image($('#authorised_by_behalf_sales_rep_signature'), booking_form[key]['sales_rep_signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'sales_rep_signature') {
            self.show_signature_image($('#sales_rep_signature'), booking_form[key]);
        } else if (key == 'terms_and_conditions') {
            for (var i = 0; i < 4; i++) {
                if (booking_form[key][i] == 'on') {
                    $('#terms_and_conditions_' + i).attr('checked', 'checked');
                }
            }
        } else if (key == 'is_upfront' || key == 'is_finance') {
            $("#" + key).attr('checked', 'checked');
        } else {
            $("#" + key).val(booking_form[key]);
        }
    }
    
     //Handle images
    var booking_form_image = JSON.parse(data.booking_form_image);
    for (var key in booking_form_image) {
        if (booking_form_image[key] != '') {
            $("#" + key).val(booking_form_image[key]);
            self.show_image($("#" + key), booking_form_image[key]);
        }
    }

    //Handle Prev Upgrade
    var prev_upgrade = JSON.parse(data.prev_upgrade);
    for (var key in prev_upgrade) {
        if (key == 'is_occured_at_premise') {
            var is_checked = (prev_upgrade[key] == '1') ? 'yes' : 'no';
            $("#" + key + "_" + is_checked).attr('checked', 'checked');
        }
        $("#" + key).val(prev_upgrade[key]);
    }
    
   
    
    //Handle Customer Check List Some Issue in Ipad so do it with for loop associative key in integer
    var customer_check_list = JSON.parse(data.customer_check_list);
    if (customer_check_list.hasOwnProperty('notes')) {
        $("#customer_check_list_notes").val(customer_check_list['notes']);
    }
    for(var i=0; i< 17; i++) {
        if ($("#customer_check_list_" + i)) {
            if(customer_check_list[i] == 'on'){
                $("#customer_check_list_" + i).attr('checked', 'checked');
            }
        }
    }


};

led_booking_form_manager.prototype.save_booking_form_to_simpro = function () {
    var self = this;
    var url = base_url + 'admin/booking_form/led_booking_form_pdf_download/' + self.booking_form_uuid;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]("Saving Data to simPRO. Please Wait...");
            $("#save_booking_form").attr("disabled", "disabled");
            $("#generate_booking_form_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            } else {
                toastr["error"](response.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
            toastr.remove();
            if(xhr.statusText == "OK"){
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            }else{
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <title>Welcome::</title>
    <style>
        .font-family {
            font-family: 'Arial', sans-serif;
        }
        
        .chart {
            position: relative;
            width: 165px;
            height: 165px;
            margin: 0 auto;
            font-weight: 300;
        }
        
        canvas {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
        }
        
        .span_canvas {
            color: #c52428;
            display: block;
            line-height: 165px;
            text-align: center;
            width: 165px;
            font-size: 40px;
            font-weight: 300;
            margin-left: 5px;
        }
    </style>
</head>

<body style="padding:0; margin:0">
    <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
        <tr>
            <td style="font-size:0px;"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1/home-top-bg.jpg'; ?>" alt="" /></td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="background:#c52428;" valign="top">
                            <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style=" font-size:70px; font-weight:900; color:#fff; line-height:68px; padding-top:50px; vertical-align:top;">YOUR
                                        <br/>SOLAR
                                        <br/>PROPOSAL</td>
                                </tr>
                                <tr>
                                    <td style="padding-top:20px; padding-bottom:40px;"><span style="width:80px; height:3px; background:#fff; display:block;"></span></td>
                                </tr>
                                <tr>
                                    <td style=" font-size:18px; font-weight:700; color:#fff; line-height:18px; border-bottom:solid 1px #fff; padding-bottom:30px;">LEARN THE POWER OF SOLAR.</td>
                                </tr>
                                <tr>
                                    <td style="padding:30px 0 40px">
                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style=" font-size:16px; font-weight:900; color:#fff;padding-bottom:20px;">CUSTOMISED FOR</td>
                                                        </tr>
                                                        <tr>
                                                            <td style=" font-size:14px; color:#fff;line-height:22px;">
                                                                <?php echo $lead_data['customer_company_name']; ?>
                                                                <br/>
                                                                <?php echo $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?>
                                                                    <br/>
                                                                <?php echo str_ireplace(", Australia", "", $lead_data['address']); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style=" font-size:14px; color:#fff;line-height:22px;"> <strong style="font-weight:700;">DATE PREPARED</strong>
                                                                <?php echo date_format(date_create($lead_data['created_at']), "l jS F Y"); ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style=" font-size:16px; font-weight:900; color:#fff;padding-bottom:20px;">PREPARED BY</td>
                                                        </tr>
                                                        <tr>
                                                            <td style=" font-size:14px; color:#fff;line-height:22px;">
                                                                <?php echo $lead_data['full_name']; ?>
                                                                    <br />
                                                                    <?php echo $lead_data['company_contact_no']; ?>
                                                                        <br />
                                                                        <?php echo $lead_data['user_email']; ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="font-size:0px;" valign="top" width="178"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1/home-left-bg.jpg'; ?>" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1/kugar-logo.jpg'; ?>" alt="" /></td>
        </tr>
        <tr>
            <td style="height:200px;">&nbsp;</td>
        </tr>
    </table>
</body>

</html>
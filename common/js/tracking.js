
var tracking_manager = function (options) {
    var self = this;
    this.userid = options.userid;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.logged_in_user_data = options.logged_in_user_data;
    this.map_container = document.getElementById('map');
    this.API_END_POINT = 'https://www.followmee.com/api/';
    this.API_KEY = 'c5114146e4928afe6d4db3efbc63497a'
    this.API_USER ='j.kurta';
    this.width = 0;
    this.locations = [];
    this.markers = [];
    this.markerCluster = null;
    this.map = {};
    this.selected_device = null;
    this.current_history_markers = [];
    this.current_history_markers_path = [];
    this.current_selected_history_markers_data = [];
    this.current_unselected_history_markers_data = [];
    this.date = null;
    this.date_start = null;
    this.date_end = null;
    this.marker_item_checkbox_all_checked = true;

    $('#search_history_by').change(function(){
        $('#date_pciker').addClass('hidden');
        $('#daterange_pciker').addClass('hidden');
        if($(this).val() == '-1'){
            $('#date_pciker').removeClass('hidden');
        }else if($(this).val() == '-2'){
            $('#daterange_pciker').removeClass('hidden');
        }
    });

    $('#search_history_btn').click(function(){
        self.clearMapMarker = true;
        $('#deal_map_filter_modal').modal('hide');
        var search_history_by = $('#search_history_by').val();
        var url = self.get_followme_api_endpoints('device_history_locations');
        var device_id = self.selected_device;

        if(device_id == null){
            toastr["error"]('Select a device first.');
            return false;
        }

        if(search_history_by == '-1'){

        }else if(search_history_by == '-2'){
            url += '&function=daterangefordevice&from='+self.date_start+'&to='+self.date_end+'&deviceid='+ device_id;
        }else if(search_history_by != ''){
            url += '&function=historyfordevice&history='+search_history_by+'&deviceid='+ device_id;
        }else{
            toastr["error"]('Select a history value.');
            return false;
        }
        self.get_device_history_location(url);
    });

    $(document).on('click', '.marker_item_checkbox_all', function () {
        self.marker_item_checkbox_all_checked = $(this)[0].checked;

        $('.marker_item_checkbox').each(function () {
            var data_id = $(this).attr('data-id');
            data_id = parseInt(data_id);

            if(!self.marker_item_checkbox_all_checked){
                self.current_unselected_history_markers_data.push(data_id);
            }else{
                for( var i = 0; i < self.current_unselected_history_markers_data.length; i++){ 
                    if (self.current_unselected_history_markers_data[i] === data_id) {
                        self.current_unselected_history_markers_data.splice(i, 1); 
                        i--;
                    }
                }
            }
        });

        self.handle_device_history_markers(self.current_selected_history_markers_data,false);
    });

    $(document).on('click', '.marker_item_checkbox', function () {
        var data_id = $(this).attr('data-id');
        data_id = parseInt(data_id);

        if(!$(this)[0].checked){
            self.current_unselected_history_markers_data.push(data_id);
        }else{
            for( var i = 0; i < self.current_unselected_history_markers_data.length; i++){ 
                if (self.current_unselected_history_markers_data[i] === data_id) {
                    self.current_unselected_history_markers_data.splice(i, 1); 
                    i--;
                }
            }
        }
        self.handle_device_history_markers(self.current_selected_history_markers_data,false);
    });

    $(function() {
        $('input[name="daterange"]').daterangepicker({}, function(start, end, label) {
           self.date_start = start.format('YYYY-MM-DD');
           self.date_end  = end.format('YYYY-MM-DD');
        });
    });

};

tracking_manager.prototype.initialize_map = function () {
    var self = this;
    var center = {};
    if (self.logged_in_user_data.latitude != null && self.logged_in_user_data.latitude != 'null' && self.logged_in_user_data.latitude != '') {
        center = {lat: parseFloat(self.logged_in_user_data.latitude), lng: parseFloat(self.logged_in_user_data.longitude)};
    } else {
        center = {lat: -25.344, lng: 131.036};
    }
    self.directionsService = new google.maps.DirectionsService();
    self.directionsDisplay = new google.maps.DirectionsRenderer();
    var width = window.innerWidth;
    var type_position = (width < 500) ? google.maps.ControlPosition.TOP_RIGHT : google.maps.ControlPosition.TOP_RIGHT;
    self.map = new google.maps.Map(self.map_container, {
        zoom: 4,
        center: center,
        zoomControl: true,
        fullscreenControl: false,
        mapTypeControl: true,
        mapTypeId: 'satellite',
        streetViewControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: type_position,
        },
        streetViewControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: type_position,
        },
        linksControl: false,
        panControl: false,
        enableCloseButton: true
    });

    self.map.getStreetView().setOptions({
        addressControlOptions: {
            position: type_position
        }
    });

    self.get_list_of_devices();

};

tracking_manager.prototype.get_followme_api_endpoints = function (type) {
    var self = this;
    var url = '';
    switch(type){
        case 'device_lists':
        url = self.API_END_POINT + 'info.aspx?key='+self.API_KEY+'&username='+self.API_USER+'&output=json&function=devicelist';
        break;
        case 'device_current_locations':
        url = self.API_END_POINT + 'tracks.aspx?key='+self.API_KEY+'&username='+self.API_USER+'&output=json&function=currentforalldevices';
        break;
        case 'device_history_locations':
        url = self.API_END_POINT + 'tracks.aspx?key='+self.API_KEY+'&username='+self.API_USER+'&output=json';
    }
    return url;
};


tracking_manager.prototype.get_device_history_location = function (url) {
    var self = this;
    $.ajax({
        type: 'GET',
        crossDomain: true,
        dataType: 'jsonp',
        url: url,
        beforeSend: function () {
            self.loading = true;
            $('#card_container').addClass('hidden');
            $('#card_container_placeholder').removeClass('hidden');
        },
        success: function (response) {
            $('#card_container').removeClass('hidden');
            $('#card_container_placeholder').addClass('hidden');
            if (!response.hasOwnProperty('Error')) {
                //var data = response.Data;
                var data = [{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:18:13 PM","Lat":-33.87786,"Lng":151.21360,"PosType":2,"CarState":-1,"Direction":132,"Speed":9,"Accuracy":10,"Altitude":20,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:41:04 PM","Lat":-33.87697,"Lng":151.21170,"PosType":2,"CarState":-1,"Direction":116,"Speed":0,"Accuracy":30,"Altitude":41,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:44:50 PM","Lat":-33.87681,"Lng":151.21180,"PosType":2,"CarState":-1,"Direction":-1,"Speed":0,"Accuracy":10,"Altitude":35,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:54:59 PM","Lat":-33.87676,"Lng":151.20980,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":41,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:56:18 PM","Lat":-33.87684,"Lng":151.20920,"PosType":2,"CarState":-1,"Direction":264,"Speed":6,"Accuracy":30,"Altitude":44,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:57:37 PM","Lat":-33.87474,"Lng":151.20530,"PosType":2,"CarState":-1,"Direction":352,"Speed":17,"Accuracy":10,"Altitude":33,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 12:59:10 PM","Lat":-33.87116,"Lng":151.20490,"PosType":2,"CarState":-1,"Direction":352,"Speed":15,"Accuracy":30,"Altitude":26,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:00:26 PM","Lat":-33.86819,"Lng":151.20450,"PosType":2,"CarState":-1,"Direction":354,"Speed":14,"Accuracy":10,"Altitude":34,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:01:49 PM","Lat":-33.86675,"Lng":151.20450,"PosType":2,"CarState":-1,"Direction":84,"Speed":10,"Accuracy":10,"Altitude":55,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:03:07 PM","Lat":-33.86659,"Lng":151.20600,"PosType":2,"CarState":-1,"Direction":88,"Speed":0,"Accuracy":10,"Altitude":44,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:06:48 PM","Lat":-33.86664,"Lng":151.20550,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":53,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:08:54 PM","Lat":-33.86669,"Lng":151.20580,"PosType":2,"CarState":-1,"Direction":240,"Speed":1,"Accuracy":10,"Altitude":62,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:10:28 PM","Lat":-33.86737,"Lng":151.20600,"PosType":2,"CarState":-1,"Direction":184,"Speed":2,"Accuracy":10,"Altitude":68,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:12:35 PM","Lat":-33.86878,"Lng":151.20610,"PosType":2,"CarState":-1,"Direction":154,"Speed":1,"Accuracy":10,"Altitude":49,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:14:45 PM","Lat":-33.86964,"Lng":151.20610,"PosType":2,"CarState":-1,"Direction":158,"Speed":0,"Accuracy":10,"Altitude":40,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:18:33 PM","Lat":-33.86950,"Lng":151.20620,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":53,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:22:58 PM","Lat":-33.86932,"Lng":151.20620,"PosType":2,"CarState":-1,"Direction":330,"Speed":2,"Accuracy":10,"Altitude":51,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:25:05 PM","Lat":-33.86791,"Lng":151.20600,"PosType":2,"CarState":-1,"Direction":16,"Speed":2,"Accuracy":10,"Altitude":49,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:26:49 PM","Lat":-33.86689,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":55,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:29:12 PM","Lat":-33.86707,"Lng":151.20580,"PosType":2,"CarState":-1,"Direction":-1,"Speed":0,"Accuracy":30,"Altitude":50,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:31:38 PM","Lat":-33.86683,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":50,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:33:49 PM","Lat":-33.86689,"Lng":151.20570,"PosType":2,"CarState":-1,"Direction":318,"Speed":2,"Accuracy":10,"Altitude":73,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:35:14 PM","Lat":-33.86718,"Lng":151.20600,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":58,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:37:23 PM","Lat":-33.86784,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":12,"Speed":0,"Accuracy":30,"Altitude":68,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:40:07 PM","Lat":-33.86811,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":182,"Speed":2,"Accuracy":10,"Altitude":92,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:42:13 PM","Lat":-33.86937,"Lng":151.20620,"PosType":2,"CarState":-1,"Direction":308,"Speed":0,"Accuracy":30,"Altitude":66,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:45:39 PM","Lat":-33.86947,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":47,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:50:18 PM","Lat":-33.86864,"Lng":151.20590,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":69,"Altitude":44,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 01:51:38 PM","Lat":-33.86948,"Lng":151.20610,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":50,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:00:20 PM","Lat":-33.87059,"Lng":151.20870,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":50,"Altitude":55,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:02:30 PM","Lat":-33.87052,"Lng":151.20980,"PosType":2,"CarState":-1,"Direction":112,"Speed":3,"Accuracy":10,"Altitude":58,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:04:37 PM","Lat":-33.87213,"Lng":151.21010,"PosType":2,"CarState":-1,"Direction":174,"Speed":3,"Accuracy":10,"Altitude":45,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:06:43 PM","Lat":-33.87350,"Lng":151.21000,"PosType":2,"CarState":-1,"Direction":190,"Speed":2,"Accuracy":10,"Altitude":41,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:08:54 PM","Lat":-33.87511,"Lng":151.21070,"PosType":2,"CarState":-1,"Direction":164,"Speed":2,"Accuracy":10,"Altitude":25,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:11:07 PM","Lat":-33.87666,"Lng":151.21100,"PosType":2,"CarState":-1,"Direction":70,"Speed":4,"Accuracy":10,"Altitude":38,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:12:40 PM","Lat":-33.87672,"Lng":151.21210,"PosType":2,"CarState":-1,"Direction":64,"Speed":0,"Accuracy":10,"Altitude":38,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:14:52 PM","Lat":-33.87738,"Lng":151.21310,"PosType":2,"CarState":-1,"Direction":86,"Speed":3,"Accuracy":10,"Altitude":39,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:16:59 PM","Lat":-33.87754,"Lng":151.21290,"PosType":2,"CarState":-1,"Direction":308,"Speed":2,"Accuracy":10,"Altitude":30,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:19:06 PM","Lat":-33.87723,"Lng":151.21250,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":38,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:21:12 PM","Lat":-33.87735,"Lng":151.21290,"PosType":2,"CarState":-1,"Direction":124,"Speed":2,"Accuracy":10,"Altitude":26,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:23:21 PM","Lat":-33.87796,"Lng":151.21370,"PosType":2,"CarState":-1,"Direction":144,"Speed":9,"Accuracy":10,"Altitude":35,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:24:45 PM","Lat":-33.88094,"Lng":151.21760,"PosType":2,"CarState":-1,"Direction":118,"Speed":3,"Accuracy":10,"Altitude":49,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:26:04 PM","Lat":-33.88486,"Lng":151.22540,"PosType":2,"CarState":-1,"Direction":100,"Speed":12,"Accuracy":10,"Altitude":65,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:27:22 PM","Lat":-33.88480,"Lng":151.22490,"PosType":2,"CarState":-1,"Direction":284,"Speed":23,"Accuracy":10,"Altitude":56,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:28:42 PM","Lat":-33.88343,"Lng":151.22100,"PosType":2,"CarState":-1,"Direction":310,"Speed":4,"Accuracy":10,"Altitude":43,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:29:10 PM","Lat":-33.88262,"Lng":151.22010,"PosType":2,"CarState":-1,"Direction":232,"Speed":4,"Accuracy":10,"Altitude":44,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:30:38 PM","Lat":-33.88265,"Lng":151.21980,"PosType":2,"CarState":-1,"Direction":240,"Speed":3,"Accuracy":10,"Altitude":60,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:32:37 PM","Lat":-33.88253,"Lng":151.21940,"PosType":2,"CarState":-1,"Direction":-1,"Speed":-1,"Accuracy":65,"Altitude":47,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:35:41 PM","Lat":-33.88271,"Lng":151.21980,"PosType":2,"CarState":-1,"Direction":146,"Speed":1,"Accuracy":10,"Altitude":48,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:39:53 PM","Lat":-33.88291,"Lng":151.21990,"PosType":2,"CarState":-1,"Direction":124,"Speed":12,"Accuracy":10,"Altitude":58,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:41:12 PM","Lat":-33.88042,"Lng":151.21660,"PosType":2,"CarState":-1,"Direction":312,"Speed":19,"Accuracy":10,"Altitude":44,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:42:50 PM","Lat":-33.87725,"Lng":151.21270,"PosType":2,"CarState":-1,"Direction":310,"Speed":9,"Accuracy":10,"Altitude":33,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:44:14 PM","Lat":-33.87701,"Lng":151.21010,"PosType":2,"CarState":-1,"Direction":164,"Speed":0,"Accuracy":10,"Altitude":19,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:45:34 PM","Lat":-33.87655,"Lng":151.20700,"PosType":2,"CarState":-1,"Direction":282,"Speed":9,"Accuracy":30,"Altitude":10,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:46:54 PM","Lat":-33.87547,"Lng":151.20550,"PosType":2,"CarState":-1,"Direction":10,"Speed":10,"Accuracy":10,"Altitude":15,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:48:39 PM","Lat":-33.87510,"Lng":151.20540,"PosType":2,"CarState":-1,"Direction":346,"Speed":0,"Accuracy":10,"Altitude":18,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:51:22 PM","Lat":-33.87469,"Lng":151.20540,"PosType":2,"CarState":-1,"Direction":354,"Speed":16,"Accuracy":10,"Altitude":20,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:53:13 PM","Lat":-33.87329,"Lng":151.20530,"PosType":2,"CarState":-1,"Direction":358,"Speed":2,"Accuracy":10,"Altitude":25,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:55:10 PM","Lat":-33.87291,"Lng":151.20520,"PosType":2,"CarState":-1,"Direction":350,"Speed":0,"Accuracy":10,"Altitude":23,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:56:26 PM","Lat":-33.87437,"Lng":151.19950,"PosType":2,"CarState":-1,"Direction":256,"Speed":13,"Accuracy":10,"Altitude":25,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:57:46 PM","Lat":-33.87392,"Lng":151.19480,"PosType":2,"CarState":-1,"Direction":326,"Speed":18,"Accuracy":10,"Altitude":16,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 02:59:04 PM","Lat":-33.87099,"Lng":151.19130,"PosType":2,"CarState":-1,"Direction":276,"Speed":8,"Accuracy":10,"Altitude":13,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:00:20 PM","Lat":-33.86963,"Lng":151.18720,"PosType":2,"CarState":-1,"Direction":286,"Speed":9,"Accuracy":10,"Altitude":32,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:01:43 PM","Lat":-33.86928,"Lng":151.18590,"PosType":2,"CarState":-1,"Direction":286,"Speed":2,"Accuracy":10,"Altitude":30,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:03:07 PM","Lat":-33.86869,"Lng":151.18360,"PosType":2,"CarState":-1,"Direction":288,"Speed":8,"Accuracy":10,"Altitude":28,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:04:52 PM","Lat":-33.86812,"Lng":151.18140,"PosType":2,"CarState":-1,"Direction":284,"Speed":5,"Accuracy":10,"Altitude":15,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:06:15 PM","Lat":-33.86814,"Lng":151.17940,"PosType":2,"CarState":-1,"Direction":248,"Speed":3,"Accuracy":10,"Altitude":7,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:08:12 PM","Lat":-33.86891,"Lng":151.17720,"PosType":2,"CarState":-1,"Direction":246,"Speed":5,"Accuracy":10,"Altitude":0,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:09:33 PM","Lat":-33.87116,"Lng":151.17290,"PosType":2,"CarState":-1,"Direction":232,"Speed":26,"Accuracy":10,"Altitude":-5,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:10:49 PM","Lat":-33.87424,"Lng":151.16540,"PosType":2,"CarState":-1,"Direction":256,"Speed":9,"Accuracy":10,"Altitude":15,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:12:08 PM","Lat":-33.87485,"Lng":151.15480,"PosType":2,"CarState":-1,"Direction":278,"Speed":33,"Accuracy":10,"Altitude":10,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:13:27 PM","Lat":-33.87006,"Lng":151.14660,"PosType":2,"CarState":-1,"Direction":284,"Speed":23,"Accuracy":10,"Altitude":-1,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:15:23 PM","Lat":-33.87055,"Lng":151.14270,"PosType":2,"CarState":-1,"Direction":244,"Speed":5,"Accuracy":10,"Altitude":0,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:16:41 PM","Lat":-33.87412,"Lng":151.13630,"PosType":2,"CarState":-1,"Direction":228,"Speed":30,"Accuracy":10,"Altitude":0,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:21:54 PM","Lat":-33.86137,"Lng":151.08080,"PosType":2,"CarState":-1,"Direction":296,"Speed":34,"Accuracy":10,"Altitude":16,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:23:13 PM","Lat":-33.85609,"Lng":151.06550,"PosType":2,"CarState":-1,"Direction":288,"Speed":45,"Accuracy":5,"Altitude":13,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:24:32 PM","Lat":-33.84823,"Lng":151.05160,"PosType":2,"CarState":-1,"Direction":312,"Speed":48,"Accuracy":5,"Altitude":2,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:25:51 PM","Lat":-33.83947,"Lng":151.03740,"PosType":2,"CarState":-1,"Direction":300,"Speed":45,"Accuracy":10,"Altitude":17,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:27:07 PM","Lat":-33.83469,"Lng":151.02840,"PosType":2,"CarState":-1,"Direction":292,"Speed":8,"Accuracy":10,"Altitude":10,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:28:26 PM","Lat":-33.83307,"Lng":151.02350,"PosType":2,"CarState":-1,"Direction":296,"Speed":15,"Accuracy":10,"Altitude":4,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:29:43 PM","Lat":-33.82934,"Lng":151.01770,"PosType":2,"CarState":-1,"Direction":310,"Speed":20,"Accuracy":10,"Altitude":11,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:31:00 PM","Lat":-33.82759,"Lng":151.01500,"PosType":2,"CarState":-1,"Direction":296,"Speed":10,"Accuracy":10,"Altitude":10,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:32:21 PM","Lat":-33.82710,"Lng":151.00820,"PosType":2,"CarState":-1,"Direction":276,"Speed":23,"Accuracy":10,"Altitude":19,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:33:40 PM","Lat":-33.82724,"Lng":150.99440,"PosType":2,"CarState":-1,"Direction":298,"Speed":55,"Accuracy":5,"Altitude":26,"Address":""},{"Error":"","DeviceID":"12154342","Dated":"2019-11-15 03:34:58 PM","Lat":-33.81824,"Lng":150.98030,"PosType":2,"CarState":-1,"Direction":298,"Speed":42,"Accuracy":5,"Altitude":34,"Address":""}];
                self.handle_device_history_markers(data);
            } else {
                if (response.hasOwnProperty('Error')) {
                    toastr['error'](response.Error);
                } else {
                    toastr["error"]('Something went wrong please try again.');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#load_nearby_deals > i').addClass('hidden');
            $('#card_container').removeClass('hidden');
            $('#card_container_placeholder').addClass('hidden');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

tracking_manager.prototype.get_list_of_devices = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        crossDomain: true,
        dataType: 'jsonp',
        url: self.get_followme_api_endpoints('device_current_locations'),
        beforeSend: function () {
            self.loading = true;
            $('#load_nearby_deals').unbind('click');
            $('#load_nearby_deals > i').removeClass('hidden');
            $('#card_container').addClass('hidden');
            $('#card_container_placeholder').removeClass('hidden');
        },
        success: function (response) {
            $('#load_nearby_deals > i').addClass('hidden');
            $('#card_container').removeClass('hidden');
            $('#card_container_placeholder').addClass('hidden');
            if (!response.hasOwnProperty('Error')) {
                var data = response.Data;
                self.handle_device_card(data);
            } else {
                if (response.hasOwnProperty('Error')) {
                    toastr['error'](response.Error);
                } else {
                    toastr["error"]('Something went wrong please try again.');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#load_nearby_deals > i').addClass('hidden');
            $('#card_container').removeClass('hidden');
            $('#card_container_placeholder').addClass('hidden');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


tracking_manager.prototype.handle_device_card = function (data) {
    var self = this;

    //Add Markers for Deals
    self.handle_device_marker_and_clusters(data);
    
    var lastLeftLocation = 0;
    $('.card-sc').scroll(function (e) {
        if (!self.loading) {
            var $elem = $('.card-sc');
            var newScrollLeft = $elem.scrollLeft(),
            width = $elem.outerWidth(),
            scrollWidth = $elem.get(0).scrollWidth;
            if (scrollWidth - newScrollLeft == width) {
                //$('#load_more_btn').slideDown();
            } else {
                //$('#load_more_btn').slideUp();
            }
        }
    });
};


tracking_manager.prototype.handle_device_marker_and_clusters = function (data) {
    var self = this;

    var oms = new OverlappingMarkerSpiderfier(self.map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });

    var counter = 0;
    var markers = data.map(function (device_data, i) {
        if (device_data.hasOwnProperty('Latitude')) {
            var key = device_data['DeviceID'];

            if(document.getElementById('card_' + key) == undefined){
                var device_card = self.create_device_card(device_data, key);
                document.getElementById('card_container').appendChild(device_card);
                self.width += 400;
                $('#card_container').attr('style', 'width:' + self.width + 'px;');
            }
            document.getElementById('card_' + key).scrollIntoView({block: "end", inline: "center"});
            $('#card_' + key).removeClass('hidden');

            //Bind Actions
            self.handle_actions();

            var mk = new google.maps.Marker({
                id: i,
                DeviceID: key,
                position: {'lat': parseFloat(device_data['Latitude']), lng: parseFloat(device_data['Longitude'])},
                icon: {
                    url: base_url + "assets/images/mk-blue.png", 
                }
            });

            google.maps.event.addListener(mk, 'click', function () {
                if (self.markers[this.id].selected) {
                    var key = self.markers[this.id].DeviceID;
                    self.markers[this.id].selected = false;
                    self.markers[i].setAnimation(null);
                    self.markers[i].setIcon(base_url + "assets/images/mk-blue.png?v=0.1");
                    if ($('.kg-lead_card').hasClass('selected')) {
                        $('.kg-lead_card').removeClass('selected');
                    }
                    $('.select_device_'+key).removeClass('hidden');
                    $('.select_device_'+key).next('i').addClass('hidden');
                    self.selected_device = null;

                } else {
                    var key = self.markers[this.id].DeviceID;
                    $('.select_device_'+key).addClass('hidden');
                    $('.select_device_'+key).next('i').removeClass('hidden');

                    self.markers[this.id].selected = true;
                    self.selected_device = key;
                    var icon = {
                        url: base_url + "assets/images/mk-pink.png", // url
                        scaledSize: new google.maps.Size(35, 35),
                    };
                    self.markers[this.id].setIcon(icon);
                }
            });

            self.markers.push(mk);
            oms.addMarker(mk);
            //mk.setMap(self.map);
            //So Pick First Marker Position and Centerized and Zoom the Map
            if (counter == 0) {
                self.map.setCenter(mk.getPosition());
                self.map.setZoom(11);
            }
            return mk;
        }
    });
    //self.markerCluster = new MarkerClusterer(self.map, markers, {imagePath: base_url + 'assets/images/m'});
};


tracking_manager.prototype.handle_actions = function () {
    var self = this;

    $('.select_device').click(function(){
        $('.select_device').removeClass('hidden');
        $('.select_device').next('i').addClass('hidden');

        var device_id = $(this).attr('data-id');
        self.selected_device = device_id;
        $(this).addClass('hidden');
        $(this).next('i').removeClass('hidden');

        for (var i = 0; i < self.markers.length; i++) {
            var key = self.markers[i].DeviceID;

            if (key != device_id) {
                self.markers[i].selected = false;
                self.markers[i].setAnimation(null);
                self.markers[i].setIcon(base_url + "assets/images/mk-blue.png?v=0.1");
            }else{
                self.markers[i].selected = true;
                var icon = {
                    url: base_url + "assets/images/mk-pink.png", // url
                    scaledSize: new google.maps.Size(35, 35),
                };
                self.markers[i].setIcon(icon);
            }
        }

    });
};

tracking_manager.prototype.create_device_card = function (data, id) {
    var self = this;
    //var lat = data.latitude;
    //var lng = data.longitude;
    //var distance = self.get_distance(lat, lng);
    var card = document.createElement('div');
    card.className = 'kg-lead_card  ';
    card.className += 'kg-lead_card--blue';
    card.setAttribute('id', 'card_' + id)

    var card_icon_mobile = document.createElement('span');
    card_icon_mobile.className = 'card_header_icon_dark';
    card_icon_mobile.innerHTML = '<i class="icon-screen-smartphone"></i>';
    card_icon_mobile.setAttribute('style','font-size:60px; float:left');

    var card_device_details = document.createElement('span');
    card_device_details.setAttribute('style','float:left; margin-top:5px; color:#fff;');
    card_device_details.innerHTML = 'Device Id: ' + data.DeviceID;
    card_device_details.innerHTML += '<br/> Device Name: ' + data.DeviceName;
    card_device_details.innerHTML += '<br/> Date: ' + moment(data.Date).format('D/M/Y');

    var card_device_actions = document.createElement('span');
    card_device_actions.setAttribute('style','float:right; margin-right:10px; margin-top:5px; color:#fff;');
    card_device_actions.innerHTML = '<a href="javascript:void(0);" class="btn btn-primary btn-sm select_device select_device_'+data.DeviceID+'" data-id="'+data.DeviceID+'">Select</a> <i class="fa fa-2x fa-check-circle hidden" style="color:#B30003;"></i>';

    var card_body = document.createElement('div');
    card_body.className = 'kg-lead_card__body';
    card_body.appendChild(card_icon_mobile);
    card_body.appendChild(card_device_details);
    card_body.appendChild(card_device_actions);

    card.appendChild(card_body);

    return card;
};


tracking_manager.prototype.handle_device_history_markers = function (data,is_fetched) {
    var self = this;

    self.current_selected_history_markers_data = data;


    document.getElementById('marker_list').innerHTML = '';

    //Remove all previous markers and clusters
    if (self.clearMapMarker == true) {
        for (var i = 0; i < self.markers.length; i++) {
            self.markers[i].setMap(null);
        }

        for (var i = 0; i < self.current_history_markers.length; i++) {
            self.current_history_markers[i].setMap(null);
        }

        //self.markers = [];
        self.current_history_markers = [];
    }

    if(is_fetched == false){
        self.current_history_markers_path[0].setMap(null);
        self.current_history_markers_path = [];
    }

    var oms = new OverlappingMarkerSpiderfier(self.map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });


    $('.device_history_markers').removeClass('hidden');

    var row = document.createElement('tr');
    row.setAttribute('style','cursor: default;');

    var column1 = document.createElement('td');
    column1.setAttribute('style','cursor: default; vertical-align: middle;');

    var marker_input = document.createElement('input');
    marker_input.setAttribute('style','cursor: default;');
    marker_input.setAttribute('type','checkbox');
    marker_input.setAttribute('data-id','-1');

    if(self.marker_item_checkbox_all_checked == true){
        console.log(self.marker_item_checkbox_all_checked);
        marker_input.setAttribute('checked','checked');
    }

    marker_input.className = 'marker_item_checkbox_all';

    column1.appendChild(marker_input);
    row.appendChild(column1);

    document.getElementById('marker_list').appendChild(row);

    var pointsPath = [];

    for(var key in data){

        var device_data = data[key];
        device_data['Date'] = device_data['Dated'];
        device_data['Latitude'] = device_data['Lat'];
        device_data['Longitude'] = device_data['Lng'];

        var icon_url = '';
        var marker_item_icon_url = '';
        if(data.length > 0){
            if(key == 0){
                icon_url = base_url + "assets/images/mk-pink.png?v=0.1";
                marker_item_icon_url = "//www.followmee.com/images/map_icon/r.png";
            }else if(key == (data.length - 1)){
                icon_url = base_url + "assets/images/mk-green.png?v=0.1";
                marker_item_icon_url = "//www.followmee.com/images/map_icon/g.png";
            }else{
                icon_url = base_url + "assets/images/mk-blue.png?v=0.1";
                marker_item_icon_url = "//www.followmee.com/images/map_icon/c.png";
            }
        }else{
            icon_url = base_url + "assets/images/mk-green.png?v=0.1";
            marker_item_icon_url = "//www.followmee.com/images/map_icon/g.png";
        }

        //Prepare Marker List Item
        var marker_list_item = self.create_marker_list_item(device_data,key,marker_item_icon_url);
        document.getElementById('marker_list').appendChild(marker_list_item);
        
        //Prepae Marker for Map 
        if(!self.current_unselected_history_markers_data.includes(parseInt(key))){
            var mk = new google.maps.Marker({
                id: key,
                uuid: device_data['DeviceID'],
                position: {'lat': parseFloat(device_data['Latitude']), lng: parseFloat(device_data['Longitude'])},
                icon: {
                    url: icon_url
                },
            });

            self.current_history_markers.push(mk);
            mk.setMap(self.map);

            //So Pick First Marker Position and Centerized and Zoom the Map
            if(key == 0 && is_fetched != false){
                self.map.setCenter(mk.getPosition());
                self.map.setZoom(11);
            }

            pointsPath.push(new google.maps.LatLng(device_data['Latitude'],device_data['Longitude']));
        }
    }

    //Draw Path Along with Direction arrow
    var flightPath = new google.maps.Polyline({
        path: pointsPath,
        geodesic: true,
        strkeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        geodesic: true,
        icons: [{
            icon: {path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW},
            offset: '100%',
            repeat: '200px'
        }]
    });

    flightPath.setMap(self.map);
    self.current_history_markers_path.push(flightPath);

};

tracking_manager.prototype.create_marker_list_item = function (data,key,icon_url) {
    var self = this;

    var row = document.createElement('tr');
    row.setAttribute('style','cursor: default;');

    var column1 = document.createElement('td');
    column1.setAttribute('style','cursor: default; vertical-align: middle;');

    var marker_input = document.createElement('input');
    marker_input.setAttribute('style','cursor: default;');
    marker_input.setAttribute('type','checkbox');

    if(!self.current_unselected_history_markers_data.includes(parseInt(key))){
        marker_input.setAttribute('checked','checked');
    }

    marker_input.setAttribute('data-id',key);
    marker_input.className = 'marker_item_checkbox';

    var marker_image = document.createElement('img');
    marker_image.setAttribute('src',icon_url);
    marker_image.setAttribute('style','cursor: default; width: 10px; margin-left:5px;');

    var marker_label = document.createElement('label');
    marker_label.setAttribute('style','cursor: default; margin-left:5px;');
    marker_label.innerHTML = moment(data.Date).format('YYYY-MM-DD HH:mm:ss');


    column1.appendChild(marker_input);
    column1.appendChild(marker_image);
    column1.appendChild(marker_label);

    row.appendChild(column1);

    return row;
};

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Product Controller
 * @version 1.0
 */
class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("components");
        $this->load->model("Product_Model", "product");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function led_manage() {
        $data = array();
        //Restricitng and Fetching of Data based on user logged In group
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect($this->redirect_url);
        }*/

        $data['meta_title'] = 'Manage LED Product';
        
        $product_list = $this->product->get_led_products();
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $product_types = $this->product->get_product_types(" WHERE t1.parent_id = 1");
        
        $data['product_list'] = $product_list;
        $data['product_types'] = $product_types;
        $data['states'] = array_column($states, 'state_postal');
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_manage');
        $this->load->view('partials/footer');
    }

    public function led_add() {
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('np_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('np_wattage', 'Wattage', 'required');
            $this->form_validation->set_rules('np_rated_life', 'Rated Life', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                $insert_data['created_at'] = date('Y-m-d H:i:s');

                //Insert Product Item
                $this->common->insert_data('tbl_led_new_products', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product created successfuly.</div>');
                    redirect('admin/product/led/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id = 1");

        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_form');
        $this->load->view('partials/footer');
    }

    public function led_edit($id) {
        //Restricitng and Fetching of Data based on user logged In group   
        $product_data = $this->common->fetch_where('tbl_led_new_products', '*', array('np_id' => $id));

        if (empty($product_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Product found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }*/


        $data = array();

        if (!empty($product_data)) {
            $data['product_data'] = $product_data[0];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('np_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('np_wattage', 'Wattage', 'required');
            $this->form_validation->set_rules('np_rated_life', 'Rated Life', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                //$update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Product Item
                $stat = $this->common->update_data('tbl_led_new_products', ['np_id' => $id], $update_data);

                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product updated successfuly.</div>');
                    redirect('admin/product/led/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id = 1");

        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);
        $data['pid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_led_form');
        $this->load->view('partials/footer');
    }

    public function led_delete($id = FALSE) {
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }*/

        if ($id) {
            $products = $this->common->fetch_where('tbl_led_new_products', '*', array('np_id' => $id));
            if (empty($products)) {
                redirect('admin/dashboard');
            } else {
                $stat = $this->common->update_data('tbl_led_new_products', array('np_id' => $id), array('status' => 0));
                if ($stat) {
                    if ($stat == -1) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Product cannot be deleted it is linked to some proposal.</div>');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Product and Its Variants Deleted Successfuly.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
            redirect('admin/product/led/manage');
        }
    }

    public function fetch_product_variant_data() {
        $data = array();
        $data['success'] = FALSE;
        $id = $this->input->get('product_id');
        if (isset($id) && $id != '') {
            $product_variant_data = $this->common->fetch_where('tbl_product_variants', '*', array('product_id' => $id));
            $data['product_variant_data'] = $product_variant_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function import_csv() {
        $data = array();

        if (!$this->aauth->is_member('Admin')) {
            $data['success'] = FALSE;
            $data['status'] = 'Only Admin is allowed to import porduct csv';
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            $action = $this->input->post('action');
            switch ($action) {
                case 'import_led_product_csv':
                    $data = $this->handle_led_product_csv_import();
                    break;
                case 'import_product_variant_csv':
                    $data = $this->handle_product_variant_csv_import();
                    break;
                default :
                    $data['success'] = FALSE;
                    $data['status'] = 'Invalid Request';
                    break;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function export_csv() {
        $data = array();

        if (!$this->aauth->is_member('Admin')) {
            $data['success'] = FALSE;
            $data['status'] = 'Only Admin is allowed to export porduct csv';
            echo json_encode($data);
            die;
        }

        $action = $this->input->get('action');
        if ($action == 'export_led_product_csv') {
            $products = $this->product->get_led_products();
            $this->components->download_send_headers("product_export_" . date("Y-m-d") . ".csv");
            //Filter Some Columns to be not included 

            $new_products = array();
            foreach ($products as $key => $product) {
                $product['type_id'] = $product['type_name'];
                unset($product['status'], $product['created_at'], $product['updated_at'], $product['type_name']
                );

                $new_products[$key] = $product;
            }
            echo $this->components->array2csv($new_products);
            die();
        } else if ('export_product_variant_csv') {
            $products = $this->common->fetch_where('tbl_product_variants', '*', NULL);
            $this->components->download_send_headers("product_variant_export_" . date("Y-m-d") . ".csv");
            //Filter Some Columns to be not included 
            $new_products = array();
            foreach ($products as $key => $product) {
                unset($product['last_updated_price']);
                $new_products[$key] = $product;
            }
            echo $this->components->array2csv($new_products);
            die();
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    private function handle_led_product_csv_import() {
        try {
            if (!isset($_FILES["led_product_csv_file"])) {
                $data['status'] = 'Please Select a CSV File';
                $data['success'] = FALSE;
                return $data;
            }
            $filename = $_FILES["led_product_csv_file"]["tmp_name"];
            if ($_FILES["led_product_csv_file"]["size"] > 0) {

                $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
                if (!in_array($_FILES['led_product_csv_file']['type'], $mimes)) {
                    $data['status'] = 'Invalid File type. Please Upload a CSV file.';
                    $data['success'] = FALSE;
                    return $data;
                }
                $file = fopen($filename, "r");
                ini_set('auto_detect_line_endings', true);
                $count = 0;
                $columns = [];
                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if ($count == 0) {
                        /*                         * $columns = explode(';', $getData[0]);
                          for ($i = 0; $i < count($columns); $i++) {
                          $columns1[$i] = str_replace('"', "", $columns[$i]);
                          } */
                        $columns1 = $getData;
                    } else {
                        /*                         * $explode_data = explode(';', $getData[0]); */
                        //$user_id = $this->aauth->get_user_id();
                        for ($i = 0; $i < count($getData); $i++) {
                            //$rows1[$i] = str_replace('"', "", $explode_data[$i]);
                            $rows[$i] = ($getData[$i] == '' || $getData[$i] == 'NULL') ? NULL : $getData[$i];
                        };
                        //First Check if product already present by name if , yes then update
                        $combined_data = array_combine($columns1, $rows);
                        $product = ($combined_data['np_id'] != '') ? $this->common->fetch_where('tbl_led_new_products', '*', ['np_id' => $combined_data['np_id']]) : [];
                        if (!empty($product)) {
                            //Here we only update already exisitng product.
                            $product_update_data = $combined_data;
                            $type_id = $this->common->fetch_cell('tbl_product_types', 'type_id', array('type_name' => trim($combined_data['type_id'])));
                            $product_update_data['type_id'] = (isset($type_id) && $type_id != NULL) ? $type_id : 3;
                            $product_update_data['updated_at'] = date('Y-m-d H:i:s');
                            $result = $this->common->update_data('tbl_led_new_products', ['np_id' => $product[0]['np_id']], $product_update_data);
                        } else {
                            //Insert Product
                            $product_insert_data = $combined_data;
                            //$product_insert_data['cost_price'] = floatval(str_replace(',', '', $combined_data['cost_price']));
                            //$product_insert_data['amount'] = floatval(str_replace(',', '', $combined_data['amount']));
                            $type_id = $this->common->fetch_cell('tbl_product_types', 'type_id', array('type_name' => trim($combined_data['type_id'])));
                            $product_insert_data['type_id'] = (isset($type_id) && $type_id != NULL) ? $type_id : 3;
                            $product_insert_data['created_at'] = date('Y-m-d H:i:s');
                            //$product_insert_data['user_id'] = $user_id;
                            $result = $this->common->insert_data('tbl_led_new_products', $product_insert_data);
                        }
                    }
                    $count++;
                }
                fclose($file);
                if (!isset($result)) {
                    $data['status'] = 'CSV File Import Failed';
                    $data['success'] = FALSE;
                } else {
                    $data['status'] = 'CSV File has been successfully Imported';
                    $data['success'] = TRUE;
                }
                return $data;
            }
        } catch (Exception $e) {
            $data['status'] = 'Looks like something wnt wrong while importing the csv';
            $data['success'] = FALSE;
            return $data;
        }
    }

    /**
     * Product Type Related Functions 
     * */
    public function manage_product_type() {
        $data = array();
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect($this->redirect_url);
        }

        $product_type_list = $this->product->get_product_types();
        $data['product_type_list'] = $product_type_list;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_manage');
        $this->load->view('partials/footer');
    }

    public function add_product_type() {

        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('type_name', 'Product Type Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data['type_name'] = $this->input->post('type_name');
                $parent_id = $this->input->post('parent_id');
                $insert_data['parent_id'] = (isset($parent_id) && $parent_id != '') ? $parent_id : NULL;
                $insert_data['user_id'] = $this->aauth->get_user_id();

                //Insert Product Type Item
                $this->common->insert_data('tbl_product_types', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product Type created successfuly.</div>');
                    redirect('admin/product-type/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product type. </div>');
                }
            } else {
                $data = $this->input->post();
            }
        }

        $data['parent_types'] = $this->common->fetch_where('tbl_product_types', '*', array('parent_id' => NULL));
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_form');
        $this->load->view('partials/footer');
    }

    public function edit_product_type($id) {

        $product_type_data = $this->common->fetch_where('tbl_product_types', '*', array('type_id' => $id));

        if (empty($product_type_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Data found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        if ($this->aauth->get_user_id() != $product_type_data[0]['user_id'] && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }


        $data = array();

        if (!empty($product_type_data)) {
            $data['type_name'] = $product_type_data[0]['type_name'];
            $data['parent_id'] = $product_type_data[0]['parent_id'];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('type_name', 'Product Type Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data['type_name'] = $this->input->post('type_name');
                $parent_id = $this->input->post('parent_id');
                $update_data['parent_id'] = (isset($parent_id) && $parent_id != '') ? $parent_id : NULL;
                $stat = $this->common->update_data('tbl_product_types', array('type_id' => $id), $update_data);
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product type updated successfuly.</div>');
                    redirect('admin/product-type/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data = $this->input->post();
            }
        }

        $data['parent_types'] = $this->common->fetch_where('tbl_product_types', '*', array('parent_id' => NULL));
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_form');
        $this->load->view('partials/footer');
    }

    /**
     * Proposal Realted Product functions 
     * 
     */
    public function fetch_led_new_product_by_existing() {
        $data = array();
        if (!empty($this->input->get())) {
            $ep_id = $this->input->get('ep_id');
            $state = $this->input->get('state');
            $ep_data = $this->common->fetch_row('tbl_led_existing_products', 'type_id', array('ep_id' => $ep_id));
            if($ep_data['type_id'] == 4){
               $battens = $this->common->fetch_where('tbl_led_new_products', '*', array('state' => $state, 'type_id' => $ep_data['type_id']));
               $panel_lights = $this->common->fetch_where('tbl_led_new_products', '*', array('state' => $state, 'type_id' => 5));
               $data['np_data'] = array_merge($battens,$panel_lights);
            }else{
              $data['np_data'] = $this->common->fetch_where('tbl_led_new_products', '*', array('state' => $state, 'type_id' => $ep_data['type_id']));  
            }
            $data['success'] = TRUE;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function fetch_solar_product_by_type() {
        $data = $product_data = array();
        if (!empty($this->input->get())) {
            $type_id = $this->input->get('type_id');
            if(isset($type_id) && $type_id != ''){
                $all_types = $this->common->fetch_where('tbl_product_types', 'type_id', array('parent_id' => $type_id));
                $all_types = array_column($all_types, 'type_id');
                $all_types = implode(',', $all_types);
                $all_types = (isset($all_types) && $all_types != '') ? $all_types : '';
                $cond = ($all_types != '') ? " WHERE prd.type_id IN(" . $type_id . $all_types . ")" : " WHERE prd.type_id IN(" . $type_id . ")";
                $product_data = $this->product->get_solar_products_by_cond($cond);
            }
            $data['product_data'] = $product_data;
            $data['success'] = TRUE;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }
    
    
    
    /**
     * SOLAR PRODUCTS RELATED FUNCTIONS
     */
    
    public function solar_manage() {
        $data = array();
        //Restricitng and Fetching of Data based on user logged In group
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect($this->redirect_url);
        }*/

        $data['meta_title'] = 'Manage Solar Product';
        
        $product_list = $this->product->get_solar_products_by_cond('WHERE prd.status=1');
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $product_types = $this->product->get_product_types(" WHERE t1.parent_id != 1 AND t1.type_id != 10");
        
        $data['product_list'] = $product_list;
        $data['product_types'] = $product_types;
        $data['states'] = array_column($states, 'state_postal');
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_solar_manage');
        $this->load->view('partials/footer');
    }

    public function solar_add() {
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('product_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('part_no', 'State', 'required');
            $this->form_validation->set_rules('manufacturer', 'State', 'required');
            $this->form_validation->set_rules('power_wattage', 'Wattage', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                $insert_data['created_at'] = date('Y-m-d H:i:s');

                //Insert Product Item
                $this->common->insert_data('tbl_solar_products', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product created successfuly.</div>');
                    redirect('admin/product/solar/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id != 1 AND t1.type_id != 10");

        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_solar_form');
        $this->load->view('partials/footer');
    }

    public function solar_edit($id) {
        //Restricitng and Fetching of Data based on user logged In group   
        $product_data = $this->common->fetch_where('tbl_solar_products', '*', array('prd_id' => $id));

        if (empty($product_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Product found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }*/


        $data = array();

        if (!empty($product_data)) {
            $data['product_data'] = $product_data[0];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('product_name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');
            $this->form_validation->set_rules('part_no', 'State', 'required');
            $this->form_validation->set_rules('manufacturer', 'State', 'required');
            $this->form_validation->set_rules('power_wattage', 'Wattage', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                $update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Product Item
                $stat = $this->common->update_data('tbl_solar_products', ['prd_id' => $id], $update_data);

                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product updated successfuly.</div>');
                    redirect('admin/product/solar/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_types'] = $this->product->get_product_types(" WHERE t1.parent_id != 1 AND t1.type_id != 10");

        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);
        $data['pid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_solar_form');
        $this->load->view('partials/footer');
    }

    public function solar_delete($id = FALSE) {
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }*/

        if ($id) {
            $products = $this->common->fetch_where('tbl_solar_products', '*', array('prd_id' => $id));
            if (empty($products)) {
                redirect('admin/dashboard');
            } else {
                $stat = $this->common->update_data('tbl_solar_products', array('prd_id' => $id), array('status' => 0));
                if ($stat) {
                    if ($stat == -1) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Product cannot be deleted it is linked to some proposal.</div>');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Solar Product Deleted Successfuly.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
            redirect('admin/product/solar/manage');
        }
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


/**
 *
 * @property MeterData Controller
 * @version 1.0
 */
class MeterData extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Components");
        $this->load->library("Email_Manager");
        $this->load->library("NREL");
        $this->load->model("Product_Model", "product");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Proposal_Model", "proposal");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function upload_file()
    {
        $data = array();

        $proposal_id = $this->input->post('proposal_id');
        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $filename  = $this->input->post('meterDataFileURL');
        $stat = FALSE;
        if (isset($_FILES['meterDataFileURL']['name']) && in_array($_FILES['meterDataFileURL']['type'], $file_mimes)) {
            $config['upload_path'] = "./assets/uploads/meter_data_files/";
            $config['allowed_types'] = 'csv|xls|xlsx';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload("meterDataFileURL")) {
                $update_data = array();
                $update_data['is_meter_data'] = 1;
                $update_data['meter_data_file'] = $this->upload->data()['file_name'];
                $stat = $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_id), $update_data);
            } else {
                $data['upload_errors'] = $this->upload->display_errors();
            }
        }

        if ($stat) {
            $data['file'] = site_url() . "assets/uploads/meter_data_files/" . $this->upload->data()['file_name'];
            $data['status'] = 'File Uploaded Successfully';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $data['status'] = 'File Uploading Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }


    public function fetch_file_data()
    {
        $data = array();
        $proposal_id = $this->input->get('proposal_id');
        $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

        if (empty($proposal_data)) {
            $data['status'] = 'Invalid Request';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }


        //Check for column mapping
        $meter_data_column_mapping = json_decode($proposal_data['meter_data_column_mapping']);
        $date_time_index = false;
        $date_index = false;
        $time_index = false;
        $kw_index = false;
        if (!empty($meter_data_column_mapping)) {
            foreach ($meter_data_column_mapping as $key => $value) {
                if ($value == 'DateTime') {
                    $date_time_index = $key;
                } else if ($value == 'Date') {
                    $date_index = $key;
                } else if ($value == 'Time') {  // Time
                    $time_index = $key;
                } else if ($value == 'W') {
                    $kw_index = $key;
                } else if ($value == 'kW') {
                    $kw_index = $key;
                } else if ($value == 'kwh') {
                    $kw_index = $key;
                }
            }
        }


        // print_r($meter_data_column_mapping);
        // die;

        $filename = $proposal_data['meter_data_file'];
        $filetype = '';
        $ext = explode('.', $filename);
        $file = FCPATH . '/assets/uploads/meter_data_files/' . $filename;

        switch (strtolower($ext[1])) {
            case 'xls':
                $filetype = 'Xls';
                break;
            case 'xlsx':
                $filetype = 'Xlsx';
                break;
            case 'csv':
                $filetype = 'Csv';
                break;
        }

        if ($filetype == '') {
            $data['status'] = 'Meter Data File Extension not supported.';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($filetype);
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load($file);
        $worksheet = $spreadsheet->getActiveSheet();

        $limit = 0;
        $i = $j = 0;
        $rows = [];
        $column_mapping_rows = [];
        $date_time_column = $grid_power =  '';
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];


            // foreach ($cellIterator as  $cell) {
            //     $cells[] = $cell->getValue();
            // }

            foreach ($cellIterator as  $cell) {

                $cells[] = $cell->getFormattedValue();
            }

            if ($limit <= 10) {
                $column_mapping_rows[$limit] = $cells;
                $limit++;
            }

            if ($i > 0) {
                if ($date_time_index != false && $kw_index != false) {
                    $dot_position = strpos($cells[$date_time_index], '.');
                    $datetime = ($dot_position != FALSE && $dot_position != 5) ? str_replace('.', '-', $cells[$date_time_index]) : $cells[$date_time_index];
                    if (is_numeric($datetime)) {
                        $is_sheet_column_datetime = TRUE;
                    }
                    $new_datetime = [];
                    if ($is_sheet_column_datetime) {
                        $new_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datetime);
                    }
                    $new_datetime = !empty($new_datetime) ? $new_datetime->format("d-m-Y H:i:s") : $datetime;
                    $rows[($i - 1)][0] = $new_datetime;

                    $kwh = $cells[$kw_index];
                    $rows[($i - 1)][1] = $kwh;
                }
                if ($date_time_index == false && $date_index != false && $time_index != false && $kw_index != false) {
                    $date = $cells[$date_index];
                    $time = $cells[$time_index];
                    $new_date = date('Y-m-d', strtotime($date));
                    $new_time = date('H:i:s', strtotime($time));
                    $new_datetime = $new_date . ' ' . $new_time;
                    $rows[($i - 1)][0] = $new_datetime;
                    // $rows[($i - 1)][0] = $new_datetime;

                    $kwh = $cells[$kw_index];
                    $rows[($i - 1)][1] = $kwh;
                }
            }
            $i++;
        }

        $meter_data = !empty($rows) ? $this->calculate_consumption_profile_data_by_meter_data($proposal_data, $rows) : [];
        $data['avg_monthly_load_graph_data'] = !empty($rows) ? $this->calcualte_avg_load_graph_data_from_meter_data($meter_data) : [];
        $data['meter_data']  = !empty($rows) ? $meter_data['final_data'] : [];
        $data['meter_data_missing_months'] = !empty($rows) ? $meter_data['missing_months'] : [];
        $data['column_mapping'] = $meter_data_column_mapping;
        $data['column_mapping_rows'] = $column_mapping_rows;
        $data['rows'] = $rows;

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function save_meter_data_column_mapping()
    {
        $proposal_id = $this->input->post('proposal_id');
        $column_mapping = $this->input->post('column_mapping');
        $meter_data_replicated = $this->input->post('meter_data_replicated');
        $update_data = [];
        $update_data['meter_data_column_mapping'] = json_encode($column_mapping);
        if (isset($meter_data_replicated) && $meter_data_replicated != '' && $meter_data_replicated != 'null') {
            $update_data['meter_data_replicated'] = ($meter_data_replicated);
        }
        $stat = $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_id), $update_data);

        $data['status'] = 'Data Saved Successfully';
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));

    }


    private function calculate_consumption_profile_data_by_meter_data($proposal_data = [], $meter_data = [])
    {
        $data                 = [];
        $column_count         = count($meter_data[0]);
        $full_year_meter_data = [];
        if ($column_count == 2) {
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $meter_data);
            $full_year_meter_data = $final_data['data'];
        } else if ($column_count == 3) {
            $new_meter_data       = $this->convert_three_column_to_two_columns_meter_data($meter_data);
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $new_meter_data);
            $full_year_meter_data = $final_data['data'];
        }

        for ($i = 0; $i < count($full_year_meter_data); $i++) {
            $data['consumption_date'][$i] = $full_year_meter_data[$i][0];
            $data['consumption_data'][$i] = $full_year_meter_data[$i][1];
            $data['consumption_time'][$i] = explode(" ", $full_year_meter_data[$i][0])[1];
            $data['final_data'][$i][0]    = $full_year_meter_data[$i][0];
            $data['final_data'][$i][1]    = $full_year_meter_data[$i][1];
            $month                        = (int) date('m', strtotime($full_year_meter_data[$i][0]));
            if ($month <= 3) {
                $data['consumption_keys_1_3'][] = $i;
            } else if ($month > 3 && $month <= 6) {
                $data['consumption_keys_4_6'][] = $i;
            } else if ($month > 6 && $month <= 9) {
                $data['consumption_keys_7_9'][] = $i;
            } else if ($month > 9 && $month <= 12) {
                $data['consumption_keys_10_12'][] = $i;
            }
        }
        $data['missing_months'] = $final_data['missing_months'];

        return $data;
    }

    private function convert_three_column_to_two_columns_meter_data($meter_data)
    {

        /** Dates in the m/d/y or d-m-y formats are disambiguated by looking at the separator between the various components: if the separator is a slash (/), then the American m/d/y is assumed; whereas if the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed. */
        $two_column_meter_data = [];
        foreach ($meter_data as $key => $value) {
            $time_in_24_hour_format = date("H:i", strtotime($value[1]));
            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date = $value[0];
            }
            $proper_date_format             = date('d-m-Y', strtotime($new_date));
            //echo $proper_date_format;die;
            $col1_col2                      = $proper_date_format . ' ' . $time_in_24_hour_format;
            $two_column_meter_data[$key][0] = $col1_col2;
            $two_column_meter_data[$key][1] = $value[2];
        }
        return $two_column_meter_data;
    }

    private function handle_two_columns_meter_data($proposal_data, $meter_data)
    {

        $is_in_1hr_interval = $is_in_30_mins_interval = $is_in_15_mins_interval = false;

        //Check if DateTime is in 1 hr interval then do nothing
        $dateTime = date('i', strtotime($meter_data[1][0]));

        if ($dateTime == 00) {
            $is_in_1hr_interval = true;
            $minimized_meter_data = $meter_data;
        }

        //Check if DateTime is in 30 mins interval then sum and take mean of 2
        if ($dateTime == 30 && $is_in_1hr_interval == false) {

            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_30 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;
                $minimized_meter_data[$b][0]  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_30) / 2;
                $a = $a + 2;
                $b++;
            }
        }

        //Check if DateTime is in 15 mins interval then sum and take mean of 4
        if ($dateTime == 15  && $is_in_1hr_interval == false  && $is_in_30_mins_interval == false) {
            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_15 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;
                $min_30 = (isset($meter_data[($a + 2)])) ? $meter_data[($a + 2)][1] : 0;
                $min_45 = (isset($meter_data[($a + 3)])) ? $meter_data[($a + 3)][1] : 0;

                $minimized_meter_data[$b][0] = $s  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_15 + $min_30 + $min_45) / 4;
                $a = $a + 4;
                $b++;
            }
        }

        //So first get dates and hours based array meter data
        $meter_data_without_years = [];
        $month_years = [];
        foreach ($minimized_meter_data as $key => $value) {

            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date =  $value[0];
            }

            $break_date = explode(' ', $new_date);

            $break_date_0 = explode('-', $break_date[0]);
            $break_date_1 = explode(':', $break_date[1]);

            if (strlen($break_date_0[0]) > 2) {
                $compiled_date = date('d-m H:i', strtotime($new_date));
            } else {
                $compiled_date = ((strlen($break_date_0[0]) == 2) ? $break_date_0[0] : '0' . $break_date_0[0]) . '-' . $break_date_0[1] . ' ' . ((strlen($break_date_1[0]) == 2) ? $break_date_1[0] : '0' . $break_date_1[0]) . ':' . $break_date_1[1];
            }

            $meter_data_without_years[$key][0] =  $compiled_date;
            $meter_data_without_years[$key][1] = $value[1];


            $month = (int) date('m', strtotime($new_date));
            if (!isset($month_years[$month])) {
                $year = (int) date('Y', strtotime($new_date));
                $month_years[$month] = $year;
            }
        }

        //Now loop for each month * no of days * 24hrs
        $final_meter_data['data'] = [];
        $final_meter_data['missing_months'] = [];
        $month = 1;
        $index = 0;
        while ($month <= 12) {
            $count_month_years = count($month_years);
            $prev_yr = ($count_month_years > 0) ? $month_years[$count_month_years - 1] : 2018;
            $year = (isset($month_years[$month])) ? $month_years[$month] : $prev_yr;
            $no_of_days = $this->components->days_in_month($month, $year);
            for ($i = 1; $i <= $no_of_days; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    $d = ($i < 10) ? '0' . $i : $i;
                    $m = ($month < 10) ? '0' . $month : $month;
                    $h = ($j < 10) ? '0' . $j . ':00' : $j . ':00';

                    $full_date = $d . '-' . $m . ' ' . $h;
                    $key = array_search($full_date, array_column($meter_data_without_years, '0'));
                    if ($key !== false) {
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = $meter_data_without_years[$key][1];
                    } else {
                        if (!in_array($m, $final_meter_data['missing_months'])) {
                            $final_meter_data['missing_months'][] = (int) $m;
                        }
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = 0;
                    }
                    $index++;
                }
            }
            $month++;
        }

        if (!empty($proposal_data) && $proposal_data['meter_data_replicated'] != NULL) {
            $meter_data_replicated = json_decode($proposal_data['meter_data_replicated']);
            foreach ($meter_data_replicated as $key => $value) {
                $to = (int) $value->replicated_to;
                $from = (int) $value->replicated_from;

                $start = $to_start = $end = $to_end = $j = 0;
                for ($i = 1; $i <= $from; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    $end += $no_of_days * 24;
                    if ($i == ($from - 1)) {
                        $start = $end;
                    }
                }
                for ($i = 1; $i <= $to; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    if ($i == $to) {
                        $to_start = $to_end;
                    } else {
                        $to_end += $no_of_days * 24;
                    }
                }

                for ($j = $start; $j < $end; $j++) {
                    $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
                    $to_start++;
                }
            }
        }



        return $final_meter_data;
    }

    private function calcualte_avg_load_graph_data_from_meter_data($meter_data)
    {

        $full_year_meter_data       = $meter_data['consumption_data'];
        $full_year_meter_data_dates = $meter_data['consumption_date'];
        $full_year_meter_data_times = $meter_data['consumption_time'];

        //Average Load vs Solar Production Graph Data
        $final_avg_load_graph_data    = $final_avg_sol_prd_graph_data = $final_avg_export_graph_data  = $final_avg_offset_graph_data  = [];
        $avg_load_graph_data          = $avg_sol_prd_graph_data       = $avg_export_prd_graph_data    = $avg_offset_prd_graph_data    = [];
        $is_trading_days              = true;

        foreach ($full_year_meter_data as $key => $value) {

            $month_int = date('m', strtotime($full_year_meter_data_dates[$key]));
            $year      = date('Y', strtotime($full_year_meter_data_dates[$key]));
            $month     = date('M', strtotime($full_year_meter_data_dates[$key]));
            $month     = strtolower($month);
            $hr        = (int) date('H', strtotime($full_year_meter_data_times[$key]));


            if ($is_trading_days && $value > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) < 6)) {
                $avg_load_graph_data[$month][$hr][]  = $value;
            } else if (!$is_trading_days && $value[$value] > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) >= 6)) {
                $avg_load_graph_data[$month][$hr][] = $value;
            }

            $no_of_days_24hr = $this->components->days_in_month($month_int, $year);

            if ($full_year_meter_data_dates[$key] == $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00') {

                //Load Final
                if (array_sum($avg_load_graph_data[$month][0]) == 0) {
                    for ($i = 0; $i < 24; $i++) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$i] = 0;
                    }
                } else {

                    foreach ($avg_load_graph_data[$month] as $key => $value) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$key] = (count($value) > 0) ? array_sum($value) / count($value) : 0;
                    }
                }
            }
        }

        return $final_avg_load_graph_data;
    }
}

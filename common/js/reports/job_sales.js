var sales_reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.sales_report_data = null;
    this.solar_total_sales_report_data = {'vic_team' : [],'nsw_team': []};
    this.led_total_sales_report_data = {'vic_team' : [],'nsw_team': []};
    this.unassigned_solar_report_data = {};
    this.unassigned_led_report_data = {};

    if (self.user_group == '1' && self.user_group != '6' && self.user_group != '7' && self.all_reports_view_permission == '0') {
        //$('.solar_sales_report_container').addClass('hidden');
        //$('.led_sales_report_container').addClass('hidden');
    }
    
    $(document).on('change','.assigned_tos',function(){
        $(".assigned_tos").each(function(){
            var uid = $(this).val();
            if($(this).prop("checked")){
                $('.solar_user_row_'+uid).removeClass('hidden');
            }else{
                $('.solar_user_row_'+uid).addClass('hidden');
            }
        });
    });
    
    $(document).on('change','.assigned_tos1',function(){
        $(".assigned_tos1").each(function(){
            var uid = $(this).val();
            if($(this).prop("checked")){
                $('.led_user_row_'+uid).removeClass('hidden');
            }else{
                $('.led_user_row_'+uid).addClass('hidden');
            }
        });
    });
    
    $('#unassigend').change(function(){
        if($(this).prop("checked")){
            $('.solar_unassiged_row').addClass('hidden');
        }else{
            $('.solar_unassiged_row').removeClass('hidden');
        }
    });
    
    $('#unassigend1').change(function(){
        if($(this).prop("checked")){
            $('.led_unassiged_row').addClass('hidden');
        }else{
            $('.led_unassiged_row').removeClass('hidden');
        }
    });
    
    $(document).on('click','.show_associated_jobs',function(){
        var data_item = JSON.parse($(this).attr('data-item'));
        var cost_centre_id = $(this).attr('data-cost_center_id');
        data_item.cost_centre_id = cost_centre_id;
        if(parseInt(self.user_id) == 1 || parseInt(self.user_id) == 5 || parseInt(self.user_id) == 7){
            self.show_job_report_data(data_item);
        }else if(parseInt(self.user_id) == parseInt(data_item.user_id)){
            self.show_job_report_data(data_item);
        }
    });
    
    $(document).on('click','.show_led_associated_jobs',function(){
        var data_item = JSON.parse($(this).attr('data-item'));
        if($('#year').prop("checked") == true){
            var month_year = $('#month_year').val();
            month_year = month_year.split('-');
            var year = month_year[1];
            data_item.year = year;
        }
        var cost_centre_id = $(this).attr('data-cost_center_id');
        data_item.cost_centre_id = cost_centre_id;
        if(parseInt(self.user_id) == 1 || parseInt(self.user_id) == 5 || parseInt(self.user_id) == 7){
            self.show_job_report_data_led(data_item);
        }else if(parseInt(self.user_id) == parseInt(data_item.user_id)){
            self.show_job_report_data_led(data_item);
        }
    });
};


sales_reporting.prototype.createSkeleton = function(ele,active,error,colspan){
    document.querySelector('#'+ele).innerHTML = '';
    if(active){
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.setAttribute('colspan',colspan);
        td.appendChild(createPlaceHolder(false));
        tr.appendChild(td);
        document.querySelector('#'+ele).appendChild(tr);
    }else if(error){
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.setAttribute('colspan',colspan);
        td.innerHTML = 'Looks Like Something Went Wrong';
        tr.appendChild(td);
        document.querySelector('#'+ele).appendChild(tr);
    }
};


sales_reporting.prototype.initialize_sales_report = function () {
    var self = this;

    $('.year_picker').datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });

    $(document).on('change','#year',function(){
        self.fetch_sales_report_data('solar');
    });

    $('.month_year_picker').datepicker({
        format: "yyyy",
        startView: "years", 
        minViewMode: "years"
    });

    $(document).on('change','#month_year,#year1',function(){
        self.fetch_sales_report_data('led');
    });

    self.fetch_sales_report_data('');
}

sales_reporting.prototype.fetch_sales_report_data = function (report_type) {
    var self = this;
    var form_data = $('#reporting_actions_solar').serialize();
    form_data += '&' +$('#reporting_actions_led').serialize();
    
    /**if($('#year1').prop("checked") && report_type == 'led'){
        var month_year = $('#month_year').val();
        month_year = month_year.split('-');
        var year = month_year[1];
        form_data += '&year1='+year;
    }*/

    $.ajax({
        url: "https://kugacrm.com.au/job_crm/admin/report/fetch_sales_report_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){

            if(report_type == ''){
                self.createSkeleton('solar_sales_report_table_body',true,false,16);
                self.createSkeleton('solar_total_sales_report_table_body',true,false,16);
                self.createSkeleton('led_sales_report_table_body',true,false,16);
                self.createSkeleton('led_total_sales_report_table_body',true,false,16);
            }else if(report_type == 'solar'){
                self.createSkeleton('solar_sales_report_table_body',true,false,16);
                self.createSkeleton('solar_total_sales_report_table_body',true,false,16);
            }else if(report_type == 'led'){
                self.createSkeleton('led_sales_report_table_body',true,false,16);
                self.createSkeleton('led_total_sales_report_table_body',true,false,16);
            }

            self.solar_total_sales_report_data = {'vic_team' : [],'nsw_team': [],'qld_team': []};
            self.led_total_sales_report_data = {'vic_team' : [],'nsw_team': []};

        },
        success: function (data) {
            self.sales_report_data = data;
            
            //if(report_type == 'solar'){
                //Solar Report
                self.createSkeleton('solar_sales_report_table_body',false,false,16);
                for(var key in self.sales_report_data['solar_sales_report_data']){
                    var row = self.create_solar_sales_report_table_row(key);
                    document.querySelector('#solar_sales_report_table_body').appendChild(row);
                }
                self.createSkeleton('solar_total_sales_report_table_body',false,false,16);
                
                for(var key in self.sales_report_data['total_solar_sales_report_data']){
                    if(key == 0){
                        var state_team = 'vic_team';
                    }else if(key == 1){
                        var state_team = 'nsw_team';
                    }else{
                        var state_team = 'qld_team';
                    }
                    self.solar_total_sales_report_data[state_team] = self.sales_report_data['total_solar_sales_report_data'][key];
                }
                
                $(".assigned_tos:checkbox:not(:checked)").each(function(){
                    var uid = $(this).val();
                    $('.solar_user_row_'+uid).addClass('hidden');
                });
                
                self.create_solar_total_sales_report_table_row();
                
            //}else if(report_type == 'led'){
                //Led Report
                self.createSkeleton('led_sales_report_table_body',false,false,16);
                for(var key in self.sales_report_data['led_sales_report_data']){
                    var mainKeys = ['HB/FL','B/PL','DL/Others'];
                    for(var key1 in mainKeys){
                        var row = self.create_led_sales_report_table_row(key,mainKeys[key1]);
                        document.querySelector('#led_sales_report_table_body').appendChild(row);
                    }
                }
                self.createSkeleton('led_total_sales_report_table_body',false,false,16);
                
                for(var key in self.sales_report_data['total_led_sales_report_data']){
                    if(key == 0){
                        var state_team = 'vic_team';
                    }else{
                        var state_team = 'nsw_team';
                    }
                    self.led_total_sales_report_data[state_team] = self.sales_report_data['total_led_sales_report_data'][key];
                }
                
                self.create_led_total_sales_report_table_row();
                
                $(".assigned_tos1:checkbox:not(:checked)").each(function(){
                    var uid = $(this).val();
                    $('.led_user_row_'+uid).addClass('hidden');
                });
            //}
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if(report_type == 'solar'){
                self.createSkeleton('solar_sales_report_table_body',false,true,16);
                self.createSkeleton('solar_total_sales_report_table_body',false,true,16);
            }else if(report_type == 'led'){
                self.createSkeleton('led_sales_report_table_body',false,true,16);
                self.createSkeleton('led_total_sales_report_table_body',false,true,16);
            }
        }
    });
};

sales_reporting.prototype.create_solar_sales_report_table_row = function (key) {
    var self = this;
    var sales_data = self.sales_report_data['solar_sales_report_data'][key];
    var vic_target_values = self.sales_report_data['vic_solar_sales_target_values'];
    var nsw_target_values = self.sales_report_data['nsw_solar_sales_target_values'];
    var qld_target_values = self.sales_report_data['qld_solar_sales_target_values'];
    
    if(sales_data['state_postal'] == 'VIC'){
        var target_values = vic_target_values;
        var state_team = 'vic_team';
    }else if(sales_data['state_postal'] == 'NSW'){
        var target_values = nsw_target_values;
        var state_team = 'nsw_team';
    }else{
        var target_values =  qld_target_values;
        var state_team = 'qld_team';
    }
    
    self.unassigned_solar_report_data = (sales_data['state_postal'] == '-') ? sales_data : {};
    var tr = document.createElement('tr');
    tr.className = 'solar_user_row_'+sales_data.user_id;
    
    if(sales_data.full_name == 'UnAssigned'){
        tr.className = 'unassiged_row';
        if($('#unassigend').prop("checked")){
            tr.className = 'unassiged_row hidden';
        }
    }

    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = sales_data.full_name;

    var td_state_postal = document.createElement('td');
    td_state_postal.innerHTML = sales_data.state_postal;
    
    tr.appendChild(td_sales_rep);
    tr.appendChild(td_state_postal);

    var td_tt_r_j = document.createElement('td');
    var di = {'year':$('#year').val(),'user_id':sales_data['user_id'],'type': 'tt_r_j'};
    //href="'+base_url + 'admin/report/job_user?ref=solar_report&year='+$('#year').val()+'&user_id='+rep_data['user_id']+'&type=tt_r_j" target="__blank" ''
    td_tt_r_j.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + parseInt(sales_data['tt_r_j']) + "</a>";
    tr.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'user_id':sales_data['user_id'],'type': 'not_installed_approved_j'};
    //'+base_url + 'admin/report/job_user?ref=solar_report&year='+$('#year').val()+'&user_id='+rep_data['user_id']+'&type=not_installed_approved_j"
    td_not_installed_approved_j.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + parseInt(sales_data['not_installed_approved_j']) + "</a>";
    tr.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'user_id':sales_data['user_id'],'type': 'not_installed_not_approved'};
    //href="'+base_url + 'admin/report/job_user?ref=solar_report&year='+$('#year').val()+'&user_id='+rep_data['user_id']+'&type=not_installed_not_approved" target="__blank"
    td_not_installed_not_approved_j.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + parseInt(sales_data['not_installed_not_approved_j']) + "</a>";
    tr.appendChild(td_not_installed_not_approved_j);
  
    
    // if(self.solar_total_sales_report_data[state_team]['not_installed_approved_j'] != undefined && rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['not_installed_approved_j'] = parseInt(self.solar_total_sales_report_data[state_team]['not_installed_approved_j']) + parseInt(sales_data['not_installed_approved_j']);
    // }else if(rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['not_installed_approved_j'] = parseInt(sales_data['not_installed_approved_j']);
    // }
    
    // if(self.solar_total_sales_report_data[state_team]['not_installed_not_approved_j'] != undefined && rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['not_installed_not_approved_j'] = parseInt(self.solar_total_sales_report_data[state_team]['not_installed_not_approved_j']) + parseInt(sales_data['not_installed_not_approved_j']);
    // }else if(rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['not_installed_not_approved_j'] = parseInt(sales_data['not_installed_not_approved_j']);
    // }

    // if(self.solar_total_sales_report_data[state_team]['tt_r_j'] != undefined && rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['tt_r_j'] = parseInt(self.solar_total_sales_report_data[state_team]['tt_r_j']) + parseInt(sales_data['tt_r_j']);
    // }else if(rep_data['state_postal'] != '-'){
    //     self.solar_total_sales_report_data[state_team]['tt_r_j'] = parseInt(sales_data['tt_r_j']);
    // }

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj';
        var td_cj = document.createElement('td');
        var di = {'month':monthsDbKey[key],'user_id':sales_data['user_id']};
        //href="'+base_url + 'admin/report/job_user?ref=solar_report&month='+monthsDbKey[key]+'&user_id='+rep_data['user_id']+'" target="__blank"
        td_cj.innerHTML = "<a  href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + parseInt(sales_data[k])+ "</a>";
        
        if(target_values){
            if(target_values[monthsDbKey[key]] != undefined && parseFloat(target_values[monthsDbKey[key]]) <= parseFloat(sales_data[k]) && parseFloat(target_values[monthsDbKey[key]]) > 0){    
                td_cj.setAttribute('style','background:lightgreen;');  
            }else if(target_values[monthsDbKey[key]] == undefined &&  parseFloat(sales_data[k]) > 0){
                //td_cj.setAttribute('style','background:lightgreen;');     
            }
        }
        tr.appendChild(td_cj);
        // if(self.solar_total_sales_report_data[state_team][k] != undefined&& rep_data['state_postal'] != '-'){
        //     self.solar_total_sales_report_data[state_team][k] = parseInt(self.solar_total_sales_report_data[state_team][k]) + parseFloat(sales_data[k]);
        // }else if(rep_data['state_postal'] != '-'){
        //     self.solar_total_sales_report_data[state_team][k] = parseInt(sales_data[k]);
        // }
    }
    return tr;
};


sales_reporting.prototype.create_solar_total_sales_report_table_row = function () {
    var self = this;
    
    var vic_target_values = self.sales_report_data['vic_solar_team_target_values'];
    var nsw_target_values = self.sales_report_data['nsw_solar_team_target_values'];
    var qld_target_values = self.sales_report_data['qld_solar_team_target_values'];
    
    //VIC TOTAL
    var tr = document.createElement('tr');
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = 'VIC TOTAL<span style="opacity:0;">Prasad Venkateswara</span>';
    tr.appendChild(td_sales_rep);
    
    var td_tt_r_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'tt_r_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['tt_r_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['vic_team']['tt_r_j']);
    td_tt_r_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='4' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j'])
    td_not_installed_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='4' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_not_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j']);
    td_not_installed_not_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='4' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_not_approved_j);

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj';
        var td_cj = document.createElement('td');
        var di = {'year':$('#year').val(), 'month':monthsDbKey[key]};
        var val = isNaN(self.solar_total_sales_report_data['vic_team'][k]) ? 0 : parseInt(self.solar_total_sales_report_data['vic_team'][k]);
        var val2 = "<a href='javascript:void(0);' data-cost_center_id='4' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val+"</a>";
        td_cj.innerHTML = val2;
        if(vic_target_values[monthsDbKey[key]] != undefined && parseFloat(vic_target_values[monthsDbKey[key]]) <= parseFloat(self.solar_total_sales_report_data['vic_team'][k])){    
            td_cj.setAttribute('style','background:lightgreen;');  
        }else if(vic_target_values[monthsDbKey[key]] == undefined &&  parseFloat(self.solar_total_sales_report_data['vic_team'][k]) > 0){
            //td_cj.setAttribute('style','background:lightgreen;');     
        }
        tr.appendChild(td_cj);
    }

    document.querySelector('#solar_total_sales_report_table_body').appendChild(tr);

    //NSW TOTAL
    var tr = document.createElement('tr');
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = 'NSW TOTAL<span style="opacity:0;">Prasad Venkateswa</span>';
    tr.appendChild(td_sales_rep);

    var td_tt_r_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'tt_r_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['nsw_team']['tt_r_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['nsw_team']['tt_r_j']);
    td_tt_r_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='5' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j'])
    td_not_installed_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='5' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_not_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j']);
    td_not_installed_not_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='5' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_not_approved_j);

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj'
        var td_cj = document.createElement('td');
        var di = {'year':$('#year').val(),'type': k, 'month':monthsDbKey[key]};
        var val = isNaN(self.solar_total_sales_report_data['nsw_team'][k]) ? 0 : parseInt(self.solar_total_sales_report_data['nsw_team'][k]);
        var val2 = "<a href='javascript:void(0);' data-cost_center_id='5' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val+"</a>";
        td_cj.innerHTML = val2;
        if(nsw_target_values[monthsDbKey[key]] != undefined && parseFloat(nsw_target_values[monthsDbKey[key]]) <= parseFloat(self.solar_total_sales_report_data['nsw_team'][k])){    
            td_cj.setAttribute('style','background:lightgreen;');  
        }else if(nsw_target_values[monthsDbKey[key]] == undefined &&  parseFloat(self.solar_total_sales_report_data['nsw_team'][k]) > 0){
            //td_cj.setAttribute('style','background:lightgreen;');     
        }
        tr.appendChild(td_cj);
    }

    document.querySelector('#solar_total_sales_report_table_body').appendChild(tr);

    //QLD TOTAL
    var tr = document.createElement('tr');
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = 'QLD TOTAL<span style="opacity:0;">Prasad Venkateswa</span>';
    tr.appendChild(td_sales_rep);

    var td_tt_r_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'tt_r_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['qld_team']['tt_r_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['qld_team']['tt_r_j']);
    td_tt_r_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='10' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['qld_team']['not_installed_approved_j'])
    td_not_installed_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='10' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var di = {'year':$('#year').val(),'type': 'not_installed_not_approved_j'};
    var val = isNaN(parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j'])) ? 0 : parseInt(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']);
    td_not_installed_not_approved_j.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='10' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val + "</a>";
    tr.appendChild(td_not_installed_not_approved_j);

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj'
        var td_cj = document.createElement('td');
        var di = {'year':$('#year').val(), 'month':monthsDbKey[key]};
        var val = isNaN(self.solar_total_sales_report_data['qld_team'][k]) ? 0 : parseInt(self.solar_total_sales_report_data['qld_team'][k]);
        var val2 = "<a href='javascript:void(0);' data-cost_center_id='10' data-item='"+JSON.stringify(di)+"' class='show_associated_jobs' style='text-decoration:none; color:#000;'>" + val+"</a>";
        td_cj.innerHTML = val2;
        if(qld_target_values){
            if(qld_target_values[monthsDbKey[key]] != undefined && parseFloat(qld_target_values[monthsDbKey[key]]) <= parseFloat(self.solar_total_sales_report_data['qld_team'][k])){    
                td_cj.setAttribute('style','background:lightgreen;');  
            }else if(qld_target_values[monthsDbKey[key]] == undefined &&  parseFloat(self.solar_total_sales_report_data['qld_team'][k]) > 0){
                //td_cj.setAttribute('style','background:lightgreen;');     
            }
        }
        tr.appendChild(td_cj);
    }

    document.querySelector('#solar_total_sales_report_table_body').appendChild(tr);
    
    
    
    //ALL TOTAL
    var tr = document.createElement('tr');
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = 'TOTAL<span style="opacity:0;">NSW Prasad Venkateswa</span>';
    tr.appendChild(td_sales_rep);

    var td_tt_r_j = document.createElement('td');
    var ttrj = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['tt_r_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['tt_r_j']);
    td_tt_r_j.innerHTML = parseInt(ttrj); 
    tr.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var tn = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_approved_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_approved_j']);
    td_not_installed_approved_j.innerHTML = parseInt(tn); 
    tr.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var tn = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']);
    td_not_installed_not_approved_j.innerHTML = parseInt(tn); 
    tr.appendChild(td_not_installed_not_approved_j);

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj'
        var td_cj = document.createElement('td');
        //console.log(self.solar_total_sales_report_data['vic_team'][k] + '==' + self.solar_total_sales_report_data['nsw_team'][k] + '==' + self.unassigned_solar_report_data[k]);
        var t = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team'][k]) + parseFloat(self.solar_total_sales_report_data['nsw_team'][k]) + parseFloat(self.solar_total_sales_report_data['qld_team'][k])) ? 0 : (parseFloat(self.solar_total_sales_report_data['vic_team'][k]) + parseFloat(self.solar_total_sales_report_data['nsw_team'][k]) + parseFloat(self.solar_total_sales_report_data['qld_team'][k])).toFixed(2);
        td_cj.innerHTML = parseInt(t);
        if(nsw_target_values[monthsDbKey[key]] != undefined && (parseFloat(nsw_target_values[monthsDbKey[key]]) + parseFloat(vic_target_values[monthsDbKey[key]])) <= parseInt(t) && parseInt(vic_target_values[monthsDbKey[key]]) > 0){    
            td_cj.setAttribute('style','background:lightgreen;');  
        }else if((nsw_target_values[monthsDbKey[key]] == undefined && vic_target_values[monthsDbKey[key]] == undefined) &&  parseInt(t) && parseInt(vic_target_values[monthsDbKey[key]]) > 0){
            //td_cj.setAttribute('style','background:lightgreen;');     
        }
        tr.appendChild(td_cj);
    }

    document.querySelector('#solar_total_sales_report_table_body').appendChild(tr);
    
    for(var key in self.unassigned_solar_report_data){
        self.unassigned_solar_report_data[key] = parseInt(self.unassigned_solar_report_data[key]);
    }
    
    //ALL TOTAL + UnAssigned
    var tr1 = document.createElement('tr');
    tr1.className = 'unassiged_row1';
    if($('#unassigend').prop("checked")){
        tr1.className = 'unassiged_row1 hidden';
    }
    
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = 'TOTAL + UNASSIGNED<span style="opacity:0;">NSW Prasad Venkateswa</span>';
    tr1.appendChild(td_sales_rep);
    tr1.setAttribute('style','background:lightgrey;')

    var td_tt_r_j = document.createElement('td');
    var ttrj = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['tt_r_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['tt_r_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['tt_r_j']);
    td_tt_r_j.innerHTML = parseInt(ttrj); 
    tr1.appendChild(td_tt_r_j);
    
    var td_not_installed_approved_j = document.createElement('td');
    var tn = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['not_installed_approved_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['not_installed_approved_j']);
    td_not_installed_approved_j.innerHTML = parseInt(tn); 
    tr1.appendChild(td_not_installed_approved_j);
    
    var td_not_installed_not_approved_j = document.createElement('td');
    var tn = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['not_installed_not_approved_j'])) ? 0 : parseFloat(self.solar_total_sales_report_data['vic_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['nsw_team']['not_installed_not_approved_j']) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data['not_installed_not_approved_j']);
    td_not_installed_not_approved_j.innerHTML = parseInt(tn); 
    tr1.appendChild(td_not_installed_not_approved_j);

    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    for(var key in monthsDbKey){
        var k = monthsDbKey[key] + '_cj'
        var td_cj = document.createElement('td');
        //console.log(self.solar_total_sales_report_data['vic_team'][k] + '==' + self.solar_total_sales_report_data['nsw_team'][k] + '==' + self.unassigned_solar_report_data[k]);
        var t = isNaN(parseFloat(self.solar_total_sales_report_data['vic_team'][k]) + parseFloat(self.solar_total_sales_report_data['nsw_team'][k]) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data[k])) ? 0 : (parseFloat(self.solar_total_sales_report_data['vic_team'][k]) + parseFloat(self.solar_total_sales_report_data['nsw_team'][k]) + parseFloat(self.solar_total_sales_report_data['qld_team']['not_installed_not_approved_j']) + parseFloat(self.unassigned_solar_report_data[k])).toFixed(2);
        td_cj.innerHTML = parseInt(t);
        if(nsw_target_values[monthsDbKey[key]] != undefined && (parseFloat(nsw_target_values[monthsDbKey[key]]) + parseFloat(vic_target_values[monthsDbKey[key]])) <= parseInt(t) && parseInt(vic_target_values[monthsDbKey[key]]) > 0){    
            td_cj.setAttribute('style','background:lightgreen;');  
        }else if((nsw_target_values[monthsDbKey[key]] == undefined && vic_target_values[monthsDbKey[key]] == undefined) &&  parseInt(t) && parseInt(vic_target_values[monthsDbKey[key]]) > 0){
            //td_cj.setAttribute('style','background:lightgreen;');     
        }
        tr1.appendChild(td_cj);
    }

    document.querySelector('#solar_total_sales_report_table_body').appendChild(tr1);
    
};



sales_reporting.prototype.create_led_sales_report_table_row = function (skey,mainItem) {
    var self = this;
    var sales_data = self.sales_report_data['led_sales_report_data'][skey];
    var state_team = (sales_data['state_postal'] == 'VIC') ? 'vic_team' : 'nsw_team';
    
    var vic_hb_fl_target_values = self.sales_report_data['vic_hb_fl_target_values'];
    var nsw_hb_fl_target_values = self.sales_report_data['nsw_hb_fl_target_values'];
    
    var vic_pl_b_target_values = self.sales_report_data['vic_pl_b_target_values'];
    var nsw_pl_b_target_values = self.sales_report_data['nsw_pl_b_target_values'];
    
    var vic_dl_target_values = self.sales_report_data['vic_dl_target_values'];
    var nsw_dl_target_values = self.sales_report_data['nsw_dl_target_values'];
    
    var vic_o_target_values = self.sales_report_data['vic_o_target_values'];
    var nsw_o_target_values = self.sales_report_data['nsw_o_target_values'];
    
    self.unassigned_led_report_data = (sales_data['state_postal'] == '-') ? sales_data : {};
    
    var monthKey = self.sales_report_data['month'];

    var tr = document.createElement('tr');
    tr.className = 'led_user_row_'+sales_data.user_id;
    
    var td_sales_rep = document.createElement('td');
    
    if(mainItem == 'HB/FL'){
        tr.setAttribute('style','/** border-top:3px solid; */;');
        td_sales_rep.innerHTML =  sales_data.full_name;
        td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px solid; text-align:center;');
    }else if(mainItem == 'DL/Others'){
        td_sales_rep.innerHTML =  '&nbsp;';
        td_sales_rep.className = 'bb1-black';
        td_sales_rep.setAttribute('style','border-top:0px solid;');
    }else{
        td_sales_rep.innerHTML =  '&nbsp;';
        td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px solid;');
    }

    var td_state_postal = document.createElement('td');
    if(mainItem == 'HB/FL'){
        tr.setAttribute('style','/** border-top:3px solid; */;');
        td_state_postal.innerHTML =  sales_data.state_postal;
        td_state_postal.setAttribute('style','border-top:0px solid; border-bottom:0px solid; text-align:center;');
    }else if(mainItem == 'DL/Others'){
        td_state_postal.innerHTML =  '&nbsp;';
        td_state_postal.className = 'bb1-black';
        td_state_postal.setAttribute('style','border-top:0px solid;');
    }else{
        td_state_postal.innerHTML =  '&nbsp;';
        td_state_postal.setAttribute('style','border-top:0px solid; border-bottom:0px solid;');
    }
    
    if(sales_data.full_name == 'UnAssigned'){
        tr.className = 'unassiged_row';
        if($('#unassigend').prop("checked")){
            tr.className = 'unassiged_row hidden';
        }
    }

    tr.appendChild(td_sales_rep);
    tr.appendChild(td_state_postal);
    
    var mt = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
    var itemKeys = ['HB/FL','tt_prd1','ibnp_prd1','nia_prd1','nina_prd1'];
    var itemKeysReport = ['HB/FL','tt','ibnp','nia','nina'];
    
    for(var k in mt){
        itemKeys.push(mt[k]+'_prd1');
    }
    
    var di = {};
    
    var mc = 0;
    var count = 0;
    var target_values = {};
    if(mainItem == itemKeys[0]){
        for(var key in itemKeys){
            var td_cj = document.createElement('td');
            var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
            
            if((key > 0 && key < 5)){
                di = {'year':$('#month_year').val(),'user_id':sales_data['user_id'],'type': itemKeysReport[key]};
            }else if((key >= 5)){
                monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                di = {'month':mt[mc],'user_id':sales_data['user_id']};
                mc++;
            }
            
            
            if(count != 0){
                td_cj.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                
                target_values = (sales_data['state_postal'] == 'VIC') ? vic_hb_fl_target_values : nsw_hb_fl_target_values;
                if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]]) && key >= 5){    
                    td_cj.setAttribute('style','background:lightgreen;');  
                }else if(target_values[monthKey] == undefined &&  parseInt(sales_data[itemKeys[key]]) > 0 && key >= 5){
                    //td_cj.setAttribute('style','background:lightgreen;');     
                }
            }else{
                td_cj.innerHTML = "<a href='javascript:void(0);'  style='text-decoration:none; color:#000;'>" + v + "</a>";
            }
            tr.appendChild(td_cj);
            count++;
            
            if(itemKeys[key] != 'Not Installed'){
                // if(self.led_total_sales_report_data[state_team][itemKeys[key]] != undefined && rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(self.led_total_sales_report_data[state_team][itemKeys[key]]) 
                //     + parseInt(sales_data[itemKeys[key]]);
                // }else if(rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(sales_data[itemKeys[key]]);
                // }
            }
        
        }
    }
    
    var itemKeys = ['B/PL','tt_prd2','ibnp_prd2','nia_prd2','nina_prd2'];
    for(var k in mt){
        itemKeys.push(mt[k]+'_prd2');
    }
    
    var mc = 0;
    var count = 0;
    if(mainItem == itemKeys[0]){
        for(var key in itemKeys){
            var td_cj = document.createElement('td');
            var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
            
            if((key > 0 && key < 5)){
                di = {'year':$('#month_year').val(),'user_id':sales_data['user_id'],'type': itemKeysReport[key]};
            }else if((key >= 5)){
                monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                di = {'month':mt[mc],'user_id':sales_data['user_id']};
                mc++;
            }
            
            if(count != 0){
                td_cj.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
            }else{
                td_cj.innerHTML = "<a href='javascript:void(0);'  style='text-decoration:none; color:#000;'>" + v + "</a>";
            }
            
            target_values = (sales_data['state_postal'] == 'VIC') ? vic_pl_b_target_values : nsw_pl_b_target_values;
            
            
            if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]]) && key >= 5){    
                td_cj.setAttribute('style','background:lightgreen;');  
            }else if(target_values[monthKey] == undefined &&  parseInt(sales_data[itemKeys[key]]) > 0 && key >= 5){
                //td_cj.setAttribute('style','background:lightgreen;');     
            }
            tr.appendChild(td_cj);
            count++;
            
            if(itemKeys[key] != 'Installed but not processed'){
                // if(self.led_total_sales_report_data[state_team][itemKeys[key]] != undefined && rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(self.led_total_sales_report_data[state_team][itemKeys[key]]) 
                //     + parseInt(sales_data[itemKeys[key]]);
                // }else if(rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(sales_data[itemKeys[key]]);
                // }
            }
        }
    }

    var itemKeys = ['DL/Others','tt_prd3','ibnp_prd3','nia_prd3','nina_prd3'];
    for(var k in mt){
        itemKeys.push(mt[k]+'_prd3');
    }
    
    var mc = 0;
    var count = 0;
    if(mainItem == itemKeys[0]){
        for(var key in itemKeys){
            var td_cj = document.createElement('td');
            td_cj.className = 'bb1-black';
            var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
            var di = {'month':monthKey,'user_id':sales_data['user_id']};
            
            if((key > 0 && key < 5)){
                di = {'year':$('#month_year').val(),'user_id':sales_data['user_id'],'type': itemKeysReport[key]};
            }else if((key >= 5)){
                monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                di = {'month':mt[mc],'user_id':sales_data['user_id']};
                mc++;
            }
            
            if(count != 0){
                td_cj.innerHTML = "<a href='javascript:void(0);' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
            }else{
                td_cj.innerHTML = "<a href='javascript:void(0);'  style='text-decoration:none; color:#000;'>" + v + "</a>";
            }
            
            target_values = (sales_data['state_postal'] == 'VIC') ? vic_dl_target_values : nsw_dl_target_values;
            target_values += (sales_data['state_postal'] == 'VIC') ? vic_o_target_values : nsw_o_target_values;
            if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]]) && key >= 5){    
                td_cj.setAttribute('style','background:lightgreen;');  
            }else if(target_values[monthKey] == undefined &&  parseInt(sales_data[itemKeys[key]]) > 0 && key >= 5){
                //td_cj.setAttribute('style','background:lightgreen;');     
            }
            
            tr.appendChild(td_cj);
            count++;
            if(itemKeys[key] != 'Complete'){
                // if(self.led_total_sales_report_data[state_team][itemKeys[key]] != undefined && rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(self.led_total_sales_report_data[state_team][itemKeys[key]]) 
                //     + parseInt(sales_data[itemKeys[key]]);
                // }else if(rep_data['state_postal'] != '-'){
                //     self.led_total_sales_report_data[state_team][itemKeys[key]] = parseInt(sales_data[itemKeys[key]]);
                // }
            }
        }
    }
    
    return tr;
};


sales_reporting.prototype.create_led_total_sales_report_table_row = function () {
    var self = this;
    
    var vic_hb_fl_team_target_values = self.sales_report_data['vic_hb_fl_team_target_values'];
    var nsw_hb_fl_team_target_values = self.sales_report_data['nsw_hb_fl_team_target_values'];
    
    var vic_pl_b_team_target_values = self.sales_report_data['vic_pl_b_team_target_values'];
    var nsw_pl_b_team_target_values = self.sales_report_data['nsw_pl_b_team_target_values'];
    
    var vic_dl_team_target_values = self.sales_report_data['vic_dl_team_target_values'];
    var nsw_dl_team_target_values = self.sales_report_data['nsw_dl_team_target_values'];
    
    var vic_o_team_target_values = self.sales_report_data['vic_o_team_target_values'];
    var nsw_o_team_target_values = self.sales_report_data['nsw_o_team_target_values'];
    
    for(var key in vic_dl_team_target_values){
        var olv = (vic_o_team_target_values[key] != undefined) ?  parseInt(vic_o_team_target_values[key]) : 0;
        vic_dl_team_target_values[key] = parseInt(vic_dl_team_target_values[key]) + parseInt(olv);
    }
    
    for(var key in nsw_dl_team_target_values){
        var olv = (nsw_o_team_target_values[key] != undefined) ?  parseInt(nsw_o_team_target_values[key]) : 0;
        nsw_dl_team_target_values[key] = parseInt(nsw_dl_team_target_values[key]) + parseInt(olv);
    }
    
   
    var total_hb_fl_team_target_values = {'jan' : 0,'feb' : 0,'mar' : 0,'apr' : 0,'may' : 0,'jun' : 0,'jul' : 0,'aug' : 0,'sep' : 0,'oct' : 0,'nov' : 0,'decm' : 0};
    for(var key in vic_hb_fl_team_target_values){
        total_hb_fl_team_target_values[key] = parseInt(vic_hb_fl_team_target_values[key]) + parseInt(nsw_hb_fl_team_target_values[key]);
    }
    
    var total_pl_b_team_target_values = {'jan' : 0,'feb' : 0,'mar' : 0,'apr' : 0,'may' : 0,'jun' : 0,'jul' : 0,'aug' : 0,'sep' : 0,'oct' : 0,'nov' : 0,'decm' : 0};
    for(var key in vic_pl_b_team_target_values){
        total_pl_b_team_target_values[key] = parseInt(vic_pl_b_team_target_values[key]) + parseInt(nsw_pl_b_team_target_values[key]);
    }
    
    var total_dl_team_target_values = {'jan' : 0,'feb' : 0,'mar' : 0,'apr' : 0,'may' : 0,'jun' : 0,'jul' : 0,'aug' : 0,'sep' : 0,'oct' : 0,'nov' : 0,'decm' : 0};
    for(var key in vic_dl_team_target_values){
        total_dl_team_target_values[key] = parseInt(vic_dl_team_target_values[key]) + parseInt(nsw_dl_team_target_values[key]) + parseInt(vic_o_team_target_values[key]) + parseInt(nsw_o_team_target_values[key]);
    }
    
    //VIC TOTAL
    var mainKeys = ['HB/FL','B/PL','DL/Others'];
    for(var key1 in mainKeys){

        var tr = document.createElement('tr');
        var td_sales_rep = document.createElement('td');
        td_sales_rep.innerHTML =  mainKeys[key1] == 'HB/FL' ? 'VIC TOTAL<span style="opacity:0 !important;">Prasad Venkatesw</span>' : '&nbsp;';
        if(mainKeys[key1] == 'DL/Others'){
            td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:1px solid; text-align:center;');
            td_sales_rep.className = 'bb1-black';
        }else{
            td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px solid; text-align:center;');
        }
        tr.appendChild(td_sales_rep);

        var sales_data = self.led_total_sales_report_data['vic_team'];
        

        var count = 0;

        var mt = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        var itemKeys = ['HB/FL','tt_prd1','ibnp_prd1','nia_prd1','nina_prd1'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd1');
        }

        var di = {};
        var mc = 0;

        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = vic_hb_fl_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='1' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['B/PL','tt_prd2','ibnp_prd2','nia_prd2','nina_prd2'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd2');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = vic_pl_b_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='1' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['DL/Others','tt_prd3','ibnp_prd3','nia_prd3','nina_prd3'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd3');
        }
    
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = vic_dl_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                td_cj.className = 'bb1-black';
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='1' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        document.querySelector('#led_total_sales_report_table_body').appendChild(tr);
    }


    //NSW TOTAL
    for(var key1 in mainKeys){

        var tr = document.createElement('tr');
        var td_sales_rep = document.createElement('td');
        td_sales_rep.innerHTML =  mainKeys[key1] == 'HB/FL' ? 'NSW TOTAL<span style="opacity:0 !important;">Prasad Venkatesw</span>' : '&nbsp;';
        if(mainKeys[key1] == 'DL/Others'){
            td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:1px solid; text-align:center;');
            td_sales_rep.className = 'bb1-black';
        }else{
            td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px solid; text-align:center;');
        }
        tr.appendChild(td_sales_rep);

        var sales_data = self.led_total_sales_report_data['nsw_team'];

        var count = 0;
        var mt = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        var itemKeys = ['HB/FL','tt_prd1','ibnp_prd1','nia_prd1','nina_prd1'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd1');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = nsw_hb_fl_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='2' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['B/PL','tt_prd2','ibnp_prd2','nia_prd2','nina_prd2'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd2');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = nsw_pl_b_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='2' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['DL/Others','tt_prd3','ibnp_prd3','nia_prd3','nina_prd3'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd3');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data[itemKeys[key]] = (sales_data[itemKeys[key]] != undefined) ? sales_data[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = nsw_dl_team_target_values;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(sales_data[itemKeys[key]])){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                td_cj.className = 'bb1-black';
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : sales_data[itemKeys[key]];
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='2' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        document.querySelector('#led_total_sales_report_table_body').appendChild(tr);
    }


    //VIC + NSW TOTAL
    for(var key1 in mainKeys){

        var tr = document.createElement('tr');
        var td_sales_rep = document.createElement('td');
        td_sales_rep.innerHTML =  mainKeys[key1] == 'HB/FL' ? 'TOTAL<span style="opacity:0 !important;">Prasad Venkatesw</span>' : '&nbsp;';
        td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px solid; text-align:center;');
        tr.appendChild(td_sales_rep);

        var sales_data1 = self.led_total_sales_report_data['vic_team'];
        var sales_data2 = self.led_total_sales_report_data['nsw_team'];

        var count = 0;
        var mt = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        var itemKeys = ['HB/FL','tt_prd1','ibnp_prd1','nia_prd1','nina_prd1'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd1');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_hb_fl_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['B/PL','tt_prd2','ibnp_prd2','nia_prd2','nina_prd2'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd2');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_pl_b_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['DL/Others','tt_prd3','ibnp_prd3','nia_prd3','nina_prd3'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd3');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_dl_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                td_cj.className = 'bb1-black';
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        document.querySelector('#led_total_sales_report_table_body').appendChild(tr);
    }
    
    //VIC + NSW + UNASSGIEND
    //console.log(self.unassigned_led_report_data);
    for(var key in self.unassigned_led_report_data){
        self.unassigned_led_report_data[key] = self.unassigned_led_report_data[key];
    }
    
    for(var key1 in mainKeys){

        var tr = document.createElement('tr');
        var td_sales_rep = document.createElement('td');
        td_sales_rep.innerHTML =  mainKeys[key1] == 'HB/FL' ? 'TOTAL + UNASSIGNED<span style="opacity:0 !important;">Prasad Venkatesw</span>' : '&nbsp;';
        td_sales_rep.setAttribute('style','border-top:0px solid; border-bottom:0px transparent; text-align:center; background:lightgrey;');
        if(mainKeys[key1] == 'HB/FL'){
            td_sales_rep.setAttribute('style','border-top:1px solid; border-bottom:0px transparent; text-align:center; background:lightgrey;');
        }
        tr.appendChild(td_sales_rep);

        var sales_data1 = self.led_total_sales_report_data['vic_team'];
        var sales_data2 = self.led_total_sales_report_data['nsw_team'];
        var sales_data3 = self.unassigned_led_report_data;
        

        var count = 0;
        var mt = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
        var itemKeys = ['HB/FL','tt_prd1','ibnp_prd1','nia_prd1','nina_prd1'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd1');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                sales_data3[itemKeys[key]] = (sales_data3[itemKeys[key]] != undefined) ? sales_data3[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_hb_fl_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]) + parseInt(sales_data3[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    
                    //console.log('monthKey',monthKey);
                    //console.log('target_values',target_values[monthKey]);
                    //console.log('sales_data',t);
                    
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['B/PL','tt_prd2','ibnp_prd2','nia_prd2','nina_prd2'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd2');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                sales_data3[itemKeys[key]] = (sales_data3[itemKeys[key]] != undefined) ? sales_data3[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_pl_b_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]) + parseInt(sales_data3[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        var itemKeys = ['DL/Others','tt_prd3','ibnp_prd3','nia_prd3','nina_prd3'];
        for(var k in mt){
            itemKeys.push(mt[k]+'_prd3');
        }
        if(mainKeys[key1] == itemKeys[0]){
            for(var key in itemKeys){
                sales_data1[itemKeys[key]] = (sales_data1[itemKeys[key]] != undefined) ? sales_data1[itemKeys[key]] : 0;
                sales_data2[itemKeys[key]] = (sales_data2[itemKeys[key]] != undefined) ? sales_data2[itemKeys[key]] : 0;
                sales_data3[itemKeys[key]] = (sales_data3[itemKeys[key]] != undefined) ? sales_data3[itemKeys[key]] : 0;
                var td_cj = document.createElement('td');
                
                if(key >= 5){
                    var monthKey = mt[key - 5];
                    monthKey = (monthKey == 'dec') ? 'decm' : monthKey;
                    var target_values = total_dl_team_target_values;
                    var t = parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]) + parseInt(sales_data3[itemKeys[key]]);
                    t = isNaN(t) ? 0 : t;
                    if(target_values[monthKey] != undefined && parseInt(target_values[monthKey]) <= parseInt(t) && parseInt(target_values[monthKey]) > 0){    
                        td_cj.setAttribute('style','background:lightgreen;');  
                    }
                }
                
                if((key > 0 && key < 5)){
                    di = {'year':$('#month_year').val(),'type': itemKeys[key]};
                }else if((key >= 5)){
                    monthKey = (mt[mc] == 'dec') ? 'decm' : mt[mc];
                    di = {'month':mt[mc]};
                    mc++;
                }

                var v = (count == 0) ? itemKeys[key] : parseInt(sales_data1[itemKeys[key]]) + parseInt(sales_data2[itemKeys[key]]);;
                td_cj.innerHTML = "<a href='javascript:void(0);' data-cost_center_id='3' data-item='"+JSON.stringify(di)+"' class='show_led_associated_jobs' style='text-decoration:none; color:#000;'>" + v + "</a>";
                tr.appendChild(td_cj);
                count++;
            }
        }

        document.querySelector('#led_total_sales_report_table_body').appendChild(tr);
    }
    

};

sales_reporting.prototype.show_job_report_data = function (data){
    var self = this;
    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    var month = Object.keys(monthsDbKey).find(key => monthsDbKey[key] === data.month);
    var filter_year = $('#year').val();
    var dates = self.getMonthDateRange(filter_year,(parseInt(month) + 1));
    var start = dates.start;
    var end = dates.end;
    if(data.user_id != undefined && data.user_id != 'undefined'){
        self.filter_user_id = data.user_id;
    }
    var form_data = 'is_solar_report=1';
    if(self.filter_user_id != null){
        form_data +='&user_ids[]='+self.filter_user_id;
    }
    if(data.type != undefined && data.type != 'undefined'){
        form_data += '&type='+data.type;
        form_data += '&start_date='+ moment(filter_year+'-01-01').format('YYYY-MM-DD');
        form_data += '&end_date='+ moment().format('YYYY-MM-DD');
        form_data += '&year='+ data.year;
    }else{
        form_data += '&start_date='+ start.format('YYYY-MM-DD');
        form_data += '&end_date='+ end.format('YYYY-MM-DD');
        form_data += '&status[]=28';
    }
    form_data += '&cost_centre_id[]=5';
    form_data += '&cost_centre_id[]=4';
    self.fetch_job_report_data(form_data);
    $('#job_list_modal').modal('show');
};


sales_reporting.prototype.show_job_report_data_led = function (data){
    var self = this;
    var monthsDbKey = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','decm'];
    var month = Object.keys(monthsDbKey).find(key => monthsDbKey[key] === data.month);
    var filter_year = $('#month_year').val();
    var dates = self.getMonthDateRange(filter_year,(parseInt(month) + 1));
    var start = dates.start;
    var end = dates.end;
    if(data.user_id != undefined && data.user_id != 'undefined'){
        self.filter_user_id = data.user_id;
    }
    var form_data = 'is_led_report=1';
    if(self.filter_user_id != null){
        form_data +='&user_ids[]='+self.filter_user_id;
    }
    if(data.type != undefined && data.type != 'undefined'){
        form_data += '&type='+data.type;
        form_data += '&start_date='+ moment(filter_year+'-01-01').format('YYYY-MM-DD');
        form_data += '&end_date='+ moment().format('YYYY-MM-DD');
        if(data.year != undefined){
            form_data += '&year='+ data.year;
        }
    }else{
        form_data += '&start_date='+ start.format('YYYY-MM-DD');
        form_data += '&end_date='+ end.format('YYYY-MM-DD');
        if(data.year != undefined){
            form_data += '&year='+ data.year;
        }
        form_data += '&status[]=28';
    }
    form_data += '&cost_centre_id[]=1';
    form_data += '&cost_centre_id[]=2';
    form_data += '&cost_centre_id[]=3';
    self.fetch_job_report_data(form_data);
    $('#job_list_modal').modal('show');
};

sales_reporting.prototype.fetch_job_report_data = function (data) {
    var self = this;

    $.ajax({
        url: "https://kugacrm.com.au/job_crm/admin/report/fetch_job_report_data?" + encodeURI(data),
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('job_table_body').innerHTML = '';
            document.getElementById('job_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('job_table_body').innerHTML = '';
            var report_data = data.report_data;
            

            if(report_data.length > 0){
                self.current_report_jobs = report_data;
                for(var key in report_data){
                    var row = self.create_job_report_table_row(report_data[key]);
                    document.querySelector('#job_table_body').appendChild(row);
                }
                //$('.show_dates').removeClass('hidden');
            }

            var valArr = self.table_column_selections;
            var checkboxes = $('ul.multiselect-container li input[type=checkbox]');
            checkboxes.each(function (e) {
                if (valArr != '' && valArr.length > 0) {
                    if(valArr.includes($(this).val()) == false && $(this).val() != 'select_all'){
                        $(this).prop('checked', true);
                        $(this).click();
                    }
                }
            });
    
            if($('#job_product_count_table_body')){
                self.create_job_prodcut_count_rows(report_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('job_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};


sales_reporting.prototype.create_job_report_table_row = function (data) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.status_name;
    td1.className = 'tbl_col_0';

    var td2 = document.createElement('td');
    td2.innerHTML = data.sub_status_name;
    td2.className = 'tbl_col_1';

    var td3 = document.createElement('td');
    td3.innerHTML = data.cost_center;
    td3.className = 'tbl_col_2';

    var td4 = document.createElement('td');
    td4.innerHTML = data.job_id;
    td4.className = 'tbl_col_3';

    var td5 = document.createElement('td');
    bname = (data.business_details != null) ? JSON.parse(data.business_details).entity_name : '';
    td5.className = 'tbl_col_4';
    
    if (bname.length > 50){
        bname =   bname.substring(0,50)+' .....';
    }
    
    var url =   'https://kugacrm.com.au/job_crm/admin/job/detail/' + data.uuid;
    td5.innerHTML = '<a href="'+url+'">' + bname + '</a>';

    var schedule_dates = (data.job_schedule_dates).split(',');
    var final_days = [];
    var final_dates = [];
    for(var i=0; i < schedule_dates.length; i++){
        final_days.push(moment(schedule_dates[i]).format('ddd') + '<span class="show_dates_span hidden">(' + moment(schedule_dates[i]).format('MMM-DD') + ')</span>');
        //final_dates.push(moment(schedule_dates[i]).format('MM-DD'));
    }
    var td6 = document.createElement('td');
    td6.innerHTML = final_days.join('/');
    td6.className = 'tbl_col_5';

    var schedule_electricians = (data.job_schedule_electricians).split(',');
    var final_ec_list = [];
    for(var i=0; i < schedule_electricians.length; i++){
        final_ec_list.push(schedule_electricians[i]);
    }
    var td7 = document.createElement('td');
    td7.innerHTML = final_ec_list.join('/');
    td7.className = 'tbl_col_6';

    var td8 = document.createElement('td');
    td8.innerHTML = (data.solar_kw != null && data.solar_kw != '') ? data.solar_kw + ' kW' : '0 kW';
    td8.className = 'tbl_col_7';

    var td_hbc = document.createElement('td');
    td_hbc.innerHTML = (data.highbays_count) ? data.highbays_count : '';
    td_hbc.innerHTML = (data.js_highbays_count && parseInt(data.js_highbays_count) > 0) ? data.js_highbays_count : td_hbc.innerHTML;
    td_hbc.className = (data.js_highbays_count && parseInt(data.js_highbays_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';
    
    var td_flc = document.createElement('td');
    td_flc.innerHTML = (data.flood_lights_count) ? data.flood_lights_count : '';
    td_flc.innerHTML = (data.js_flood_lights_count && parseInt(data.js_flood_lights_count) > 0) ? data.js_flood_lights_count : td_flc.innerHTML;
    td_flc.className = (data.js_flood_lights_count && parseInt(data.js_flood_lights_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';

    var td_plc = document.createElement('td');
    td_plc.innerHTML = (data.panel_lights_count) ? data.panel_lights_count : '';
    td_plc.innerHTML = (data.js_panel_lights_count && parseInt(data.js_panel_lights_count) > 0) ? data.js_panel_lights_count : td_plc.innerHTML;
    td_plc.className = (data.js_panel_lights_count && parseInt(data.js_panel_lights_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';

    var td_blc = document.createElement('td');
    td_blc.innerHTML = (data.batten_lights_count) ? data.batten_lights_count : '';
    td_blc.innerHTML = (data.js_batten_lights_count && parseInt(data.js_batten_lights_count) > 0) ? data.js_batten_lights_count : td_blc.innerHTML;
    td_blc.className = (data.js_batten_lights_count && parseInt(data.js_batten_lights_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';

    var td_dlc = document.createElement('td');
    td_dlc.innerHTML = (data.down_lights_count) ? data.down_lights_count : '';
    td_dlc.innerHTML = (data.js_down_lights_count && parseInt(data.js_down_lights_count) > 0) ? data.js_down_lights_count : td_dlc.innerHTML;
    td_dlc.className = (data.js_down_lights_count && parseInt(data.js_down_lights_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';

    var td_oc = document.createElement('td');
    td_oc.innerHTML = (data.other_count) ? data.other_count : '';
    td_oc.innerHTML = (data.js_other_count && parseInt(data.js_other_count) > 0) ? data.js_other_count : td_oc.innerHTML;
    td_oc.className = (data.js_other_count && parseInt(data.js_other_count) > 0) ? 'bg-success text-white' : 'bg-danger text-white';
    
    td_hbc.className += ' tbl_col_8';
    td_flc.className += ' tbl_col_9';
    td_plc.className += ' tbl_col_10';
    td_blc.className += ' tbl_col_11';
    td_dlc.className += ' tbl_col_12';
    td_oc.className += ' tbl_col_13';
    
    var no_of_certificates = 0;
    var cer_value = 0;
    if(data.certificate_calculation_details != null){
        var ccd = JSON.parse(data.certificate_calculation_details);
        no_of_certificates =  ccd.no_of_certificates;
        no_of_certificates = (!isNaN(no_of_certificates) && no_of_certificates != '' && no_of_certificates != '0') ? no_of_certificates : 0;
        cer_value =  ccd.price;
        cer_value = (!isNaN(cer_value) && cer_value != '' && cer_value != '0') ? cer_value : 0;
    }
    var td15 = document.createElement('td');
    td15.innerHTML = no_of_certificates;
    td15.className = 'tbl_col_14';

    var td16 = document.createElement('td');
    td16.innerHTML = (cer_value != null && cer_value != '') ? '$' + cer_value : '$0';
    td16.className = 'tbl_col_15';
    
    var td_sales_rep = document.createElement('td');
    td_sales_rep.innerHTML = (data.sales_rep_name != null && data.sales_rep_name != '') ? data.sales_rep_name : '';
    td_sales_rep.className = 'tbl_col_16';
    
    var noc = (!isNaN(no_of_certificates) && no_of_certificates != '' && no_of_certificates != '0') ? no_of_certificates : 0;
    noc = parseFloat(noc).toFixed(2);
    var cv = (!isNaN(cer_value) && cer_value != '' && cer_value != '0') ? parseFloat(cer_value).toFixed(2) : 0;
    cv = parseFloat(cv).toFixed(2);
            
    var td_total_certificate_value = document.createElement('td');
    td_total_certificate_value.innerHTML = '$' + (parseFloat(noc) * parseFloat(cv)).toFixed(2); 

	var processing_date = document.createElement('td');
    processing_date.innerHTML = (data.status_name == 'Processing') ? data.job_current_status_date : "";
    processing_date.className = 'tbl_col_17';
	
	var completed_date = document.createElement('td');
    completed_date.innerHTML = (data.status_name =='Completed') ? data.job_current_status_date : "";
    completed_date.className = 'tbl_col_18';
    
    
    var td0 = document.createElement('td');
    var td0_checkbox = document.createElement('input');
    td0_checkbox.setAttribute('type','checkbox');
    td0_checkbox.className = 'job_select';
    td0_checkbox.setAttribute('data-id',data.job_id);
    td0.appendChild(td0_checkbox);
    
    var td_invoice_count = document.createElement('td');
    td_invoice_count.setAttribute('data-id',data.job_id);
    td_invoice_count.setAttribute('data-job_uuid',data.uuid);
    td_invoice_count.innerHTML = (data.invoice_count != null && data.invoice_count != '') ? data.invoice_count : '0';
    td_invoice_count.className = (data.invoice_count != null && data.invoice_count != '' && parseInt(data.invoice_count) >= 2) ? 'tbl_col_19 invoice_count text-center bg-success text-white' : 'tbl_col_19 invoice_count text-center bg-danger text-white';

	//tr.appendChild(td0);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td_sales_rep);
    //tr.appendChild(td6);
	tr.appendChild(processing_date);
    tr.appendChild(completed_date);
    tr.appendChild(td7);
    tr.appendChild(td_invoice_count);
    tr.appendChild(td8);
    tr.appendChild(td_hbc);
    tr.appendChild(td_flc);
    tr.appendChild(td_plc);
    tr.appendChild(td_blc);
    tr.appendChild(td_dlc);
    tr.appendChild(td_oc);
    tr.appendChild(td15);
    tr.appendChild(td16);
    tr.appendChild(td_total_certificate_value);

    return tr;
};


sales_reporting.prototype.getMonthDateRange = function (year, month) {
    var self = this;
    // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
    // array is 'year', 'month', 'day', etc
    var startDate = moment([year, month - 1]);
    // Clone the value before .endOf()
    var endDate = moment(startDate).endOf('month');

    // just for demonstration:
    console.log(startDate.toDate());
    console.log(endDate.toDate());

    // make sure to call toDate() for plain JavaScript date type
    return { start: startDate, end: endDate };
}


sales_reporting.prototype.create_job_prodcut_count_rows = function(jobs){
    var cost_centers = [];
    jobs.map(function(value){
        var ccid = value.cost_centre_id;

        var no_of_certificates = 0;
        var cer_value = 0;

        if(value.certificate_calculation_details != null){
            var ccd = JSON.parse(value.certificate_calculation_details);
            no_of_certificates =  ccd.no_of_certificates;
            cer_value =  ccd.price;
            
            no_of_certificates = (!isNaN(no_of_certificates) && no_of_certificates != '' && no_of_certificates != '0') ? no_of_certificates : 0;
            no_of_certificates = parseFloat(no_of_certificates).toFixed(2);
            cer_value = (!isNaN(cer_value) && cer_value != '' && cer_value != '0') ? parseFloat(cer_value).toFixed(2) : 0;
            cer_value = parseFloat(cer_value).toFixed(2);
        }
        
        if(parseInt(ccid) == 4){
            //console.log(value.solar_kw);
        }
        
        value.highbays_count = (value.js_highbays_count && parseInt(value.js_highbays_count) > 0) ? value.js_highbays_count : value.highbays_count;
        value.flood_lights_count = (value.js_flood_lights_count && parseInt(value.js_flood_lights_count) > 0) ? value.js_flood_lights_count : value.flood_lights_count;
        value.panel_lights_count = (value.js_panel_lights_count && parseInt(value.js_panel_lights_count) > 0) ? value.js_panel_lights_count : value.panel_lights_count;
        value.batten_lights_count = (value.js_batten_lights_count && parseInt(value.js_batten_lights_count) > 0) ? value.js_batten_lights_count : value.batten_lights_count;
        value.down_lights_count = (value.js_down_lights_count && parseInt(value.js_down_lights_count) > 0) ? value.js_down_lights_count : value.down_lights_count;
        value.other_count = (value.js_other_count && parseInt(value.js_other_count) > 0) ? value.js_other_count : value.other_count;
        
        if(cost_centers[ccid] === undefined){
            cost_centers[ccid] = {};
            cost_centers[ccid].cost_centre_name = value.cost_center;
            cost_centers[ccid].solar_kw = (!isNaN(value.solar_kw) && value.solar_kw != '' && value.solar_kw != null && value.solar_kw != 'null') ? value.solar_kw : 0;
            cost_centers[ccid].highbays_count = value.highbays_count;
            cost_centers[ccid].flood_lights_count = value.flood_lights_count;
            cost_centers[ccid].panel_lights_count = value.panel_lights_count;
            cost_centers[ccid].batten_lights_count = value.batten_lights_count;
            cost_centers[ccid].down_lights_count = value.down_lights_count;
            cost_centers[ccid].other_count = value.other_count;
            cost_centers[ccid].no_of_certificates = (!isNaN(no_of_certificates) && no_of_certificates != '') ? no_of_certificates : 0;
            cost_centers[ccid].cer_value = (!isNaN(cer_value) && cer_value != '') ? cer_value : 0;
        }else{
            var skw = (!isNaN(value.solar_kw) && value.solar_kw != '' && value.solar_kw != null && value.solar_kw != 'null') ? value.solar_kw : 0;
            cost_centers[ccid].solar_kw = parseFloat(cost_centers[ccid].solar_kw) + parseFloat(skw);
            cost_centers[ccid].highbays_count = parseInt(cost_centers[ccid].highbays_count) + parseInt(value.highbays_count);
            cost_centers[ccid].flood_lights_count = parseInt(cost_centers[ccid].flood_lights_count) + parseInt(value.flood_lights_count);
            cost_centers[ccid].panel_lights_count = parseInt(cost_centers[ccid].panel_lights_count) + parseInt(value.panel_lights_count);
            cost_centers[ccid].batten_lights_count = parseInt(cost_centers[ccid].batten_lights_count) + parseInt(value.batten_lights_count);
            cost_centers[ccid].down_lights_count =  parseInt(cost_centers[ccid].down_lights_count) + parseInt(value.down_lights_count);
            cost_centers[ccid].other_count = parseInt(cost_centers[ccid].other_count) + parseInt(value.other_count);
            
            no_of_certificates = (!isNaN(no_of_certificates) && no_of_certificates != '') ? no_of_certificates : 0;
            cer_value = (!isNaN(cer_value) && cer_value != '') ? cer_value : 0;
            cost_centers[ccid].no_of_certificates = parseInt(cost_centers[ccid].no_of_certificates) + parseInt(no_of_certificates);
            cost_centers[ccid].cer_value = parseFloat(cost_centers[ccid].cer_value) + parseFloat(cer_value);
        }
        
    });


    document.querySelector('#job_product_count_table_body').innerHTML = '';
    
    var all_total = {'solar_kw' : 0,'highbays_count':0,'flood_lights_count':0,'panel_lights_count':0,'batten_lights_count':0,'down_lights_count':0,'other_count':0,'no_of_certificates':0,'cer_value':0};
    
    cost_centers.map(function(value){

        if(value.cost_centre_name != undefined){

            var tr = document.createElement('tr');

            var td_ccname = document.createElement('td');
            td_ccname.innerHTML = value.cost_centre_name;

            var td_kw_count = document.createElement('td');
            td_kw_count.innerHTML = !isNaN(value.solar_kw) ? Math.round(value.solar_kw) : 0;

            var td_highbays_count = document.createElement('td');
            td_highbays_count.innerHTML = !isNaN(value.highbays_count) ? value.highbays_count : 0;

            var td_flood_lights_count = document.createElement('td');
            td_flood_lights_count.innerHTML = !isNaN(value.flood_lights_count) ? value.flood_lights_count : 0;

            var td_panel_lights_count = document.createElement('td');
            td_panel_lights_count.innerHTML = !isNaN(value.panel_lights_count) ? value.panel_lights_count : 0;

            var td_batten_lights_count = document.createElement('td');
            td_batten_lights_count.innerHTML = !isNaN(value.batten_lights_count) ? value.batten_lights_count : 0;

            var td_down_lights_count = document.createElement('td');
            td_down_lights_count.innerHTML = !isNaN(value.down_lights_count) ? value.down_lights_count : 0;

            var td_other_count = document.createElement('td');
            td_other_count.innerHTML = !isNaN(value.other_count) ? value.other_count : 0;

            var td_no_of_certificates = document.createElement('td');
            td_no_of_certificates.innerHTML = !isNaN(value.no_of_certificates) ? value.no_of_certificates : 0;

            var td_certificate_value = document.createElement('td');
            td_certificate_value.innerHTML = !isNaN(value.cer_value) ? '$' + parseFloat(value.cer_value).toFixed(2) : '$0' ;
            
            var noc = !isNaN(value.no_of_certificates) ? value.no_of_certificates : 0;
            var cv = !isNaN(value.cer_value) ? parseFloat(value.cer_value).toFixed(2) : 0;
            
            var td_total_certificate_value = document.createElement('td');
            td_total_certificate_value.innerHTML = '$' + (parseFloat(noc) * parseFloat(cv)).toFixed(2); 
            
            
            

            tr.appendChild(td_ccname);
            tr.appendChild(td_kw_count);
            tr.appendChild(td_highbays_count);
            tr.appendChild(td_flood_lights_count);
            tr.appendChild(td_panel_lights_count);
            tr.appendChild(td_batten_lights_count);
            tr.appendChild(td_down_lights_count);
            tr.appendChild(td_other_count);
            tr.appendChild(td_no_of_certificates);
            tr.appendChild(td_certificate_value);
            tr.appendChild(td_total_certificate_value);
            document.querySelector('#job_product_count_table_body').appendChild(tr);
            
            
            all_total['solar_kw'] = parseFloat(all_total['solar_kw']) + parseFloat(!isNaN(value.solar_kw) ? parseFloat(value.solar_kw).toFixed(2) : 0);
            all_total['highbays_count'] = parseInt(all_total['highbays_count']) + parseInt(!isNaN(value.highbays_count) ? value.highbays_count : 0);
            all_total['flood_lights_count'] = parseInt(all_total['flood_lights_count']) + parseInt(!isNaN(value.flood_lights_count) ? value.flood_lights_count : 0);
            all_total['panel_lights_count'] = parseInt(all_total['panel_lights_count']) + parseInt(!isNaN(value.panel_lights_count) ? value.panel_lights_count : 0);
            all_total['batten_lights_count'] = parseInt(all_total['batten_lights_count']) + parseInt(!isNaN(value.batten_lights_count) ? value.batten_lights_count : 0);
            all_total['down_lights_count'] = parseInt(all_total['down_lights_count']) + parseInt(!isNaN(value.down_lights_count) ? value.down_lights_count : 0);
            all_total['other_count'] = parseInt(all_total['other_count']) + parseInt(!isNaN(value.other_count) ? value.other_count : 0);
            all_total['no_of_certificates'] = parseFloat(all_total['no_of_certificates']) + parseFloat(!isNaN(value.no_of_certificates) ? value.no_of_certificates : 0);
            all_total['cer_value'] = parseFloat(all_total['cer_value']) + parseFloat(!isNaN(value.cer_value) ? value.cer_value : 0);
        }
    });
    

    var tr = document.createElement('tr');

    var td_ccname = document.createElement('td');
    td_ccname.innerHTML = '<b>Total</b>';

    var td_kw_count = document.createElement('td');
    td_kw_count.innerHTML = '<b>' + (!isNaN(all_total.solar_kw) ? Math.round(all_total.solar_kw) : 0) + '</b>';

    var td_highbays_count = document.createElement('td');
    td_highbays_count.innerHTML = '<b>' + (!isNaN(all_total.highbays_count) ? all_total.highbays_count : 0) +  '</b>';

    var td_flood_lights_count = document.createElement('td');
    td_flood_lights_count.innerHTML = '<b>' +  (!isNaN(all_total.flood_lights_count) ? all_total.flood_lights_count : 0) + '</b>';

    var td_panel_lights_count = document.createElement('td');
    td_panel_lights_count.innerHTML = '<b>' +  (!isNaN(all_total.panel_lights_count) ? all_total.panel_lights_count : 0) + '</b>';

    var td_batten_lights_count = document.createElement('td');
    td_batten_lights_count.innerHTML = '<b>' +  (!isNaN(all_total.batten_lights_count) ? all_total.batten_lights_count : 0) + '</b>';

    var td_down_lights_count = document.createElement('td');
    td_down_lights_count.innerHTML = '<b>' +  (!isNaN(all_total.down_lights_count) ? all_total.down_lights_count : 0) + '</b>';

    var td_other_count = document.createElement('td');
    td_other_count.innerHTML = '<b>' +  (!isNaN(all_total.other_count) ? all_total.other_count : 0) + '</b>';

    var td_no_of_certificates = document.createElement('td');
    td_no_of_certificates.innerHTML = '<b>' +  (!isNaN(all_total.no_of_certificates) ? all_total.no_of_certificates : 0) + '</b>';

    var td_certificate_value = document.createElement('td');
    td_certificate_value.innerHTML = '<b>' +  (!isNaN(all_total.cer_value) ? '$' + parseFloat(all_total.cer_value).toFixed(2) : '$0') + '</b>' ;
            
    var noc = !isNaN(all_total.no_of_certificates) ? all_total.no_of_certificates : 0;
    var cv = !isNaN(all_total.cer_value) ? parseFloat(all_total.cer_value).toFixed(2) : 0;
            
    var td_total_certificate_value = document.createElement('td');
    td_total_certificate_value.innerHTML = '<b>' +  '$' + (parseFloat(noc) * parseFloat(cv)).toFixed(2) + '</b>'; 
            
    tr.appendChild(td_ccname);
    tr.appendChild(td_kw_count);
    tr.appendChild(td_highbays_count);
    tr.appendChild(td_flood_lights_count);
    tr.appendChild(td_panel_lights_count);
    tr.appendChild(td_batten_lights_count);
    tr.appendChild(td_down_lights_count);
    tr.appendChild(td_other_count);
    tr.appendChild(td_no_of_certificates);
    tr.appendChild(td_certificate_value);
    tr.appendChild(td_total_certificate_value);
    document.querySelector('#job_product_count_table_body').appendChild(tr);
    
};

sales_reporting.prototype.fetch_xero_invoice = function (data) {
    var self = this;

    $.ajax({
        url: base_url + 'admin/invoice/fetch_invoice',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            toastr.clear();
            document.getElementById('invoice_count_table_body').innerHTML = '';
            document.getElementById('invoice_count_table_body').appendChild(createLoader('Fetching Invoices..'));
        },
        success: function (response) {
            if (response.success == true) {
                self.invoices = response.invoices;
                document.querySelector('#invoice_count_table_body').innerHTML = '';
                
                var paid_total = 0;
                var due_total = 0;
                
                for(var key in response.invoices){
                    var row = self.create_invoice_table_row(key,response.invoices[key]);
                    document.querySelector('#invoice_count_table_body').appendChild(row);

                    paid_total += (response.invoices[key].xero_invoice_details != null) ? (response.invoices[key].xero_invoice_details.amount_paid) : 0;
                    due_total += (response.invoices[key].xero_invoice_details != null) ? (response.invoices[key].xero_invoice_details.amount_due) : 0;
                }
                
                
                if(response.invoices.length > 0){
                    var tr_total = document.createElement('tr');
                    
                    var td_total = document.createElement('td')
                    td_total.innerHTML = '<b>Total</b>';
                    td_total.setAttribute('colspan',6);
                    
                    var td_total_paid = document.createElement('td')
                    td_total_paid.innerHTML = '<b>'+parseFloat(paid_total).toFixed(2)+'</b>';
                    
                    var td_total_due = document.createElement('td')
                    td_total_due.innerHTML = '<b>'+parseFloat(due_total).toFixed(2)+'</b>';
                    td_total_due.setAttribute('colspan',3);
                    
                    tr_total.appendChild(td_total);
                    tr_total.appendChild(td_total_paid);
                    tr_total.appendChild(td_total_due);
                    
                    document.querySelector('#invoice_count_table_body').appendChild(tr_total);
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('invoice_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

sales_reporting.prototype.create_invoice_table_row = function (key,data) {

    var self = this;

    var tr = document.createElement('tr');

    var td_item = document.createElement('td');
    td_item.innerHTML = (data.xero_invoice_details != null) ? data.xero_invoice_details.invoice_number : '<label class="badge badge-info"> Not Xero Invoice </label>';
    
    var td_date = document.createElement('td');
    td_date.innerHTML = (data.xero_invoice_details != null) ? moment(data.invoice_details.invoice_due_date,'DD/MM/YYYY').format('LL') : '-';
    
    var td_user = document.createElement('td');
    td_user.innerHTML = data.full_name;
    

    var td_amt_exgst = document.createElement('td');
    td_amt_exgst.innerHTML = data.invoice_total_excGST;
    

    var td_amt_ingst = document.createElement('td');
    td_amt_ingst.innerHTML = data.invoice_total_incGST;
    
    
    var td_invoice_status = document.createElement('td');
    td_invoice_status.innerHTML = '<label class="badge badge-info">' + data.invoice_status + '</label>';
    
    
    var td_ref = document.createElement('td');
    td_ref.innerHTML = (data.xero_invoice_details != null) ? data.invoice_details.invoice_reference : '-';
    
    var td_to = document.createElement('td');
    td_to.innerHTML = (data.contact_data != null) ? data.contact_data.contact_name : '-';;
    
    var td_created_date = document.createElement('td');
    td_created_date.innerHTML = (data.created_at != null) ? moment(data.created_at).format('LL') : '-';
    
    var td_paid_ingst = document.createElement('td');
    td_paid_ingst.innerHTML = (data.xero_invoice_details != null) ? parseFloat(data.xero_invoice_details.amount_paid).toFixed(2) : '-';
    
    var td_due_ingst = document.createElement('td');
    td_due_ingst.innerHTML = (data.xero_invoice_details != null) ? parseFloat(data.xero_invoice_details.amount_due).toFixed(2) : '-';
    
    var td_sent = document.createElement('td');
    var sent_to_contact = (data.xero_invoice_details != null && data.xero_invoice_details.sent_to_contact != undefined) ? data.xero_invoice_details.sent_to_contact : '0';
    td_sent.innerHTML = (parseInt(sent_to_contact) == 1 || sent_to_contact == true) ? '<label class="badge badge-success">YES</label>': '<label class="badge badge-danger">NO</label>';
    
    var td_created_by = document.createElement('td');
    td_created_by.innerHTML = data.full_name;
    
    if(data.status == '0'){
        td_invoice_status.innerHTML = '<label class="badge badge-info">VOID</label>';
    }
    
    tr.appendChild(td_item);
    tr.appendChild(td_created_by);
    tr.appendChild(td_to);
    tr.appendChild(td_ref);
    tr.appendChild(td_created_date);
    tr.appendChild(td_date);
    //tr.appendChild(td_user);
    tr.appendChild(td_paid_ingst);
    tr.appendChild(td_due_ingst);
    tr.appendChild(td_invoice_status);
    tr.appendChild(td_sent);
    
    return tr;
};

sales_reporting.prototype.getParams = function (url) {
    var self = this;
	var params = {};
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);
	}
	return params;
};
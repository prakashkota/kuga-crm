<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Log Controller
 * @version 1.0
 */
class Log extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("Email_Manager");
        $this->load->model("Log_Model");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function view($id) {
        $data = array();
        $data['issue'] = $this->Log_Model->getIssueDetail($id);
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('logs/view', $data);
        $this->load->view('partials/footer');
    }

    public function loadIssueThread() {
        $issueId = $this->input->get('issue_id');
        $lastMessageId = $this->input->get('last_message_id');
        $data = [];
        $data['thread'] = $this->Log_Model->getIssueThread($issueId, $lastMessageId);
        $this->load->view('logs/message', $data);
    }

    public function createMessage() {
        $response = ['status' => false, 'message' => '', 'data' => []];
        $data = $this->input->post();
        $data['posted_by_user_id'] = $this->aauth->get_user_id();
        $newRow = $this->Log_Model->createMessageAgainstIssue($data);
        // send complete thread email
        $data = $this->Log_Model->threadEmailContent($data['issue_id']);
        
        if ($this->aauth->is_member('Admin')) {
            $data['to'] = $data['issue']['email'];
        } else if ($this->components->is_team_leader()) {
            $data['to'] = $data['issue']['email'];
        } else if ($this->components->is_sales_rep()) {
            $data['to'] = $data['issue']['email'];
        }

        $this->email_manager->send_issue_email_to_user($data);
        $response['status'] = true;
        $response['message'] = 'Your message has been posted successfully';
        echo json_encode($response);
        exit;
    }

}

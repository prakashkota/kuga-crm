<?php 
		$html = "
<tr>
    <td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Energy efficiency  partnerships
    </td>
  </tr>

<tr>
<td align=\"left\" valign=\"top\">
<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; \"><strong>Our Energy Efficiency Partners</strong></td>
</tr>
<tr>
<td height=\"40\" align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/energy_eff_partners.jpg\" alt=\"\"  width=\"90%\" /></td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:20px; font-weight:bold;\">VEET / Energy Savers Scheme</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;text-align: justify;\">
Australia has the world’s most expensive energy prices. With coal fire generation not a viable option any more, there are only 2 ways to solve the problem - Renewable generation  or  become  more  energy  efficient. Energy reduction schemes like VEET & IPART have delivered over $400m of energy savings. In Victoria the VEET scheme has created over 2,000 jobs and over 1.7mil households / 70,000 businesses have participated in the scheme. Kuga Electrical have created over 500,000 certificates to date and is one of the scheme’s most successful participants. Over 5,000 businesses have claimed rebates and successfully saved thousands through the Kuga Electrical.
</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:20px; font-weight:bold;\">Creating Energy Independence</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;text-align: justify;\">We have a mission to create greater energy independence for all consumers Australia wide. As the energy challenges of the future knock on our door steps, Kuga Electrical is proud to meet the needs and requirements of all Australians.
</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;text-align: justify;\">Our company is fully confident in our products and services we offer to you, and your satisfaction is our ultimate achievement. We are ready to help you further secure your energy needs and explore more possibilities for you to create energy independence and massive cost savings through technological excellence and expertise.</td>
</tr>

<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:20px; font-weight:bold;\">Promise of Excellence</td>
</tr>

<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;text-align: justify;\">Electrical and Energy Solutions are at the forefront of our company. Kuga Electrical has a long history in Electrical, LED Lighting and Solar Power, and just like the Cougar, we will strive to be fierce &amp; agile in an effort to exceed our customer’s expectations and develop stronger links with all our business partners and the community.</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;text-align: justify;\">Reliability and Customer First – these are the key foundations that we offer our customers. All work is covered by a 5 year workmanship warranty and a minimum 1 year warranty on all products.</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr height=\"80px\">
    <td>&nbsp;</td>
  </tr>
<tr>
<tr>
<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
</tr>

"; 
echo $html;
			?>

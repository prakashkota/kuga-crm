<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
       <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Details of Previous Upgrade ( If Any )</strong></td>
      </tr>
      <tr>
        <td height="5"></td>
      </tr>
      <tr>
        <td height="35" style="padding:5px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">How long has the current business occupied the premise?</td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;" width="676" valign="top"><strong>4 year</strong></td>
      </tr>
      <tr>
        <td height="30" style="padding:5px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">Has a previous upgrade under an Energy Efficiency Scheme occurred at this premise?</td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="150" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="33"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="58" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">YES</td>
            <td width="32"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="27" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">NO</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
      <tr>
        <td height="23" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>If a previous upgrade has occurred:</strong></td>
      </tr>
      <tr>
        <td height="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Which areas were previously upgraded?</td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;"><strong></strong></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
      <tr>
        <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">What type of upgrade occurred? (Tubes, High, Bays, etc...)</span></td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;"><strong></strong></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
       <tr>
        <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">How many fittings were upgraded?</span></td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong></strong></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
       <tr>
        <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">When did the previous upgrade occur? (Approximation to month and year is acceptable)</span></td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;"><strong></strong></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
       <tr>
        <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Which installation company carried out the previous upgrade? (If known)</span></td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;"><strong></strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333"><strong>****Make sure that if there has been a previous upgrade that the details are recorded on the RCP****</strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>
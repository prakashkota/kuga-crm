var inverter_layout_manager = function (options){
	var self =  this;
	this.canvas = null;
	this.fabric = null;
	this.inverters = [];
	this.inverter_cages = [];
	this.inverter_cage_lines = [];
	this.inverter_distance = 0;
	this.inverter_image = null;

	this.lineEnabled = false;
	this.textEnabled = false;
	this.cableTrayEnabled = false;
	this.npEnabled = false;
	this.init = false;
	

	this.lead_id = (options.lead_id != undefined) ? options.lead_id : '';
	this.site_id = (options.site_id != undefined) ? options.site_id : '';
	this.proposal_id = (options.proposal_id != undefined) ? options.proposal_id : '';
	this.tool_data = null;
	this.tool_data_save = false;

	this.curX = null;
	this.curY = null;


	$("a[href='#pills-inverter']").on('click', function(e) {
		if(!self.init){
			setTimeout(function(){
				var c = {};
				c.proposal_id = self.proposal_id;
				c.lead_id = self.lead_id;
				c.site_id = self.site_id;
				self.fetch_design_tool_data(c);
				self.init = true;
			},1000);
			setTimeout(function(){
				//self.canvas.setHeight(700);
			},2000);
		}
	});

	$(document).on('click','#add_inverter_notes_btn',function(){
		$('#inverter_notes_modal').modal('show');
	});

	$(document).on('click','#save_inverter_notes_btn',function(){
		var editor_data = tinyMCE.get('inverter_notes').getContent().trim();
		var data = {};
		data.key = 'notes';
		data.value = editor_data;
		data.proposal_id = self.proposal_id;
		data.lead_id = self.lead_id;
		data.site_id = self.site_id;
		self.save_design_tool_data(data);
	});
};

inverter_layout_manager.prototype.create_uuid = function () {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		.toString(16)
		.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


inverter_layout_manager.prototype.initialize = function(){
	var self = this;

	self.canvas = this.__canvas = new fabric.Canvas('inverter_layout_canvas');
	self.canvas.setWidth($("#il_tool_section").width() - 90 );
	self.canvas.setHeight($("#il_tool_section").height());
	self.canvas.calcOffset();
	self.fabric = fabric;
	self.fabric.Object.prototype.transparentCorners = false;
	self.fabric.Object.prototype.cornerColor = 'blue';
	self.fabric.Object.prototype.cornerStyle = 'circle';

	self.canvas.setBackgroundColor('rgba(227, 227, 227, 0.9)', self.canvas.renderAll.bind(self.canvas));
	//self.canvas.toggleDragMode(true); 

	$(document).ready(function () {

		  // Set up a Fabric.js canvas with some images for testing purposes.
		  var canvasWrapper = document.querySelector('.canvas-container'),fc = self.canvas;
		  // Handle right-button clicks.
		  function startPan(event) {
		  	if (event.button != 2) {
		  		return;
		  	}
		  	var x0 = event.screenX,
		  	y0 = event.screenY;
		  	function continuePan(event) {
		  		var x = event.screenX,
		  		y = event.screenY;
		  		fc.relativePan({ x: x - x0, y: y - y0 });
		  		x0 = x;
		  		y0 = y;
		  	}
		  	function stopPan(event) {
		  		$(window).off('mousemove', continuePan);
		  		$(window).off('mouseup', stopPan);
		  	};
		  	$(window).mousemove(continuePan);
		  	$(window).mouseup(stopPan);
		  	$(window).contextmenu(cancelMenu);
		  };
		  function cancelMenu() {
		  	$(window).off('contextmenu', cancelMenu);
		  	return false;
		  }
		  $(canvasWrapper).mousedown(startPan);
		});

	self.fabric.Object.prototype.controls.deleteControl = new fabric.Control({
		x: 0.5,
		y: -0.5,
		offsetY: 16,
		cursorStyle: 'pointer',
		mouseUpHandler: self.deleteObject,
		render: self.renderIcon,
		cornerSize: 24
	});

	var panning = false;
	self.canvas.on('mouse:up', function (e) {
		panning = false;
	});

	self.canvas.on('mouse:down', function (e) {
		panning = true;
	});

	self.canvas.on('mouse:move', function (e) {
		if (panning && e && e.e) {
			var units = 10;
			var delta = new fabric.Point(e.e.movementX, e.e.movementY);
	        //self.canvas.relativePan(delta);
	    }
	});

	self.canvas.on('mouse:wheel', function(opt) {
		var delta = opt.e.deltaY;
		var zoom = self.canvas.getZoom();
		zoom *= 0.999 ** delta;
		if (zoom > 20) zoom = 20;
		if (zoom < 0.01) zoom = 0.01;
		self.canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
		opt.e.preventDefault();
		opt.e.stopPropagation();
	});

	$(function () {
		$('#zoomIn').click(function () {
			self.canvas.setZoom(self.canvas.getZoom() * 1.1);
		});

		$('#zoomOut').click(function () {
			self.canvas.setZoom(self.canvas.getZoom() / 1.1);
		});
	});

	$(document).on('click','.tl',function(){
		var id = $(this).attr('data-id');
		$('.tool-bar1-window').addClass('d-none');
		$('.tl').removeClass('text-success');
		if(id != undefined){
			$('.tool-bar1-window').removeClass('d-none');
		}
		$(this).addClass('text-success');
		$('.tl-content').removeClass('d-block');
		$('.tl-content').addClass('d-none');
		$('#'+id).removeClass('d-none');
		$('#'+id).addClass('d-block');
	});

	$(document).on('change','#il_inverter_list',function(){
		var $this = $(this);
		var dimension = $this.find('option:selected').attr('data-dimension');
		dimension = dimension.split(',');
		$('#il_inverter_length').val(dimension[1]);
		$('#il_inverter_height').val(dimension[0]);
		
		var image = $this.find('option:selected').attr('data-image');
		//self.inverter_image = (image != '') ? image :  base_url + 'assets/images/inverter.png';
		self.inverter_image = (image != '') ? image :  null;
	});
	$('#il_inverter_list').trigger('change');

	let mline = null;
	let isDown;

	$(document).on('click','#capture_image',function(){
		var canvas = document.getElementById("inverter_layout_canvas");
		var dataURL = canvas.toDataURL('png');
		var d = {};
		d.proposal_id = self.proposal_id;
		d.lead_id = self.lead_id;
		d.site_id = self.site_id;
		d.snapshot = dataURL;
		self.save_design_tool_snapshot(d);
	});

	$(document).on('click','#select_tool',function(){
		self.lineEnabled = false;
		self.textEnabled = false;
		self.cableTrayEnabled = false;
		self.npEnabled = false;
		mline = null;
	});

	$(document).on('click','#add_il_inverter',function(){
		self.add_inverter();
	});

	$(document).on('click','#add_il_inverter_cage',function(){
		self.add_inverter_cage();
	});


	$(document).on('click','#add_measuring_line',function(){
		self.lineEnabled = true;
		$('#myMenu').hide();
		toastr['info']('Click on Canvas to draw the item.');
	});

	$(document).on('click','#add_connection_line',function(){
		self.cableTrayEnabled = true;
		$('#myMenu').hide();
		toastr['info']('Click on Canvas to draw the item.');
	});

	$(document).on('click','#add_text',function(){
		self.textEnabled = true;
		$('#myMenu').hide();
		toastr['info']('Click on Canvas to draw the item.');
	});

	$(document).on('click','#add_network_protection',function(){
		$('.tool-bar1-window').addClass('d-none');
		$('.tl-content').removeClass('d-block');
		$('.tl-content').addClass('d-none');
		self.npEnabled = true;
		toastr['info']('Click on Canvas to draw the item.');
	});

	$(document).on('click','#full_screen',function(){
		if(!$("#il_tool_section").hasClass('full_screen')){
			$(this).addClass('text-success');
			$('#il_tool_section').css({
				position: 'fixed',
				top: 0,
				right: 0,
				bottom: 0,
				left: 0,
				zIndex: 999
			});
			$("#il_tool_section").addClass('full_screen');
		}else{
			$(this).removeClass('text-success');
			$('#il_tool_section').css({
				position: 'relative',
			});
			$("#il_tool_section").removeClass('full_screen');
		}
		self.canvas.setWidth($("#il_tool_section").width() - 90);
		self.canvas.setHeight($("#il_tool_section").height() - 20);
	});

	$(document).on('click','#delete_il_all_inverter',function(){
		self.canvas.getObjects().forEach((obj) => {
			self.canvas.setActiveObject(obj);
			self.canvas.remove(obj);
		});
		self.canvas.discardActiveObject().renderAll()
		self.inverters = [];
		self.inverter_cages = [];
		$('#add_il_inverter_cage').parent().parent().removeClass('hidden');
	});

	$(document).on('click','#save_inverter_design_btn',function(){
		var d = {};
		d.proposal_id = self.proposal_id;
		d.lead_id = self.lead_id;
		d.site_id = self.site_id;
		d.key = 'last_config';
		d.value = JSON.stringify(self.canvas);
		self.save_design_tool_data(d);
	});

	self.canvas.on('mouse:down', function(o){
		isDown = true;
		if(self.lineEnabled){
			var pointer = self.canvas.getPointer(o.e);
			var points = [ pointer.x, pointer.y, pointer.x, pointer.y ];
			mline =  new LineWithArrow(points, {
				strokeWidth: 2,
				stroke: 'black',
			})
			self.canvas.add(mline);
			self.canvas.sendToBack(mline);
		}else if(self.textEnabled){
			var pointer = self.canvas.getPointer(o.e);
			var config = {
				fontFamily: 'arial black',
				left: pointer.x, 
				top: pointer.y ,
				fontSize:8
			};
			var text = new fabric.IText('Enter Some text',config);
			self.canvas.add(text);
			$('#select_tool').click();
		}else if(self.cableTrayEnabled){
			var pointer = self.canvas.getPointer(o.e);
			var cageConfig = {
				left: pointer.x,
				top: pointer.y,
				width:500,
				height:40,
				fill: 'lightgrey',
				objectCaching: false,
				stroke: 'lightgrey',
				strokeWidth: 4,
			};
			var rect = new fabric.Rect(cageConfig);
			self.canvas.add(rect);
			self.canvas.sendToBack(rect);
			$('#select_tool').click();
		}else if(self.npEnabled){
			var inverter_to_np_dsiatnce = parseInt($('#il_np_to_inv_distance').val());
			var pointer = self.canvas.getPointer(o.e);

			if(self.inverters.length > 0){
				var firstInverterObj =  self.inverters[0];
				self.canvas.getObjects().forEach((obj) => {
					if(obj.id == 'first_invereter'){
						firstInverterObj = obj;
						self.canvas.setActiveObject(firstInverterObj);
						firstInverterObj = self.canvas.getActiveObject();
						console.log(firstInverterObj);
					}
				});
				pointer.x = firstInverterObj.oCoords.bl.x;
				pointer.y = firstInverterObj.oCoords.bl.y;
			}

			/** Disatnce line between inverter and np */
			var l = pointer.x;
			var c1 = pointer.x - inverter_to_np_dsiatnce + 10;
			var c2 = l - 10;
			var c3 = pointer.y - 110
			var c4 = pointer.y - 110;

			var line2 = new LineWithArrow([c1, c3, c2, c4], {
				stroke: 'black',
			});
			self.canvas.add(line2);
			var c1 = (c1 + c2)/2 - 15;
			var c2 = (c3 + c4)/2;
			var text2 = new fabric.Text($('#il_np_to_inv_distance').val()+'mm', { left: c1, top: c2, fontSize: 25 });
			self.canvas.add(text2);

			var np_length = $('#il_np_length').val();
			np_length = parseInt(np_length);
			var np_height = $('#il_np_height').val();
			np_height = parseInt(np_height);

			var cageConfig = {
				left: pointer.x - np_length - inverter_to_np_dsiatnce,
				top: pointer.y - np_height - 10,
				width:np_length,
				height:np_height + 10,
				fill: '#d86336',
				objectCaching: false,
				stroke: '#d86336',
				strokeWidth: 4,
			};
			var rect = new fabric.Rect(cageConfig);
			self.canvas.add(rect);
			self.canvas.sendToBack(rect);

			self.canvas.setActiveObject(rect);
			activeObject = self.canvas.getActiveObject();

			/** Line Top */
			var c1 = activeObject.aCoords.tl.x + 5;
			var c2 = activeObject.aCoords.tr.x - 5;
			var c3 = activeObject.aCoords.tl.y - 20;
			var c4 = activeObject.aCoords.tr.y - 20;
			var line = new LineWithArrow([c1, c3, c2, c4], {
				stroke: 'black',
			});
			self.canvas.add(line);

			var c1 = (activeObject.aCoords.tl.x+activeObject.aCoords.tr.x)/2 - 20;
			var c3 = (activeObject.aCoords.tl.y+activeObject.aCoords.tr.y)/2 - 40;

			var np_length = $('#il_np_length').val();
			var text = new fabric.Text(np_length+'mm', { left: c1, top: c3, fontSize: 25 });
			self.canvas.add(text);

			/** Line Left */
			var c1 = activeObject.aCoords.tl.x - 20;
			var c2 = activeObject.aCoords.bl.x - 20;
			var c3 = activeObject.aCoords.tl.y + 5;
			var c4 = activeObject.aCoords.bl.y - 5;
			var line = new LineWithArrow([c1, c3, c2, c4], {
				stroke: 'black',
			});
			self.canvas.add(line);

			var c1 = (activeObject.aCoords.tl.x+activeObject.aCoords.bl.x)/2 - 40;
			var c3 = (activeObject.aCoords.tl.y+activeObject.aCoords.bl.y)/2 + 20;

			var np_height = $('#il_np_height').val();
			var text = new fabric.Text(np_height+'mm', { left: c1, top: c3, fontSize: 25, angle: 270 });
			self.canvas.add(text);

			var np_length = $('#il_np_length').val();
			np_length = parseInt(np_length) - 10;
			var np_height = $('#il_np_height').val();
			np_height = parseInt(np_height) - 10;
			var pointer = self.canvas.getPointer(o.e);
			var cageConfig = {
				left: pointer.x - np_length + 20 - inverter_to_np_dsiatnce,
				top: pointer.y - np_height + 10,
				width:np_length - 50,
				height:np_height - 40,
				fill: '#d86336',
				objectCaching: false,
				stroke: 'lightgrey',
				strokeWidth: 1,
			};
			var rect = new fabric.Rect(cageConfig);
			self.canvas.add(rect);
			self.canvas.bringToFront(rect);


			var np_length1  = 30;
			var np_height1 = 100;
			var pointer = self.canvas.getPointer(o.e);
			var cageConfig = {
				left: pointer.x - np_length + np_length1 + 10 - inverter_to_np_dsiatnce,
				top: pointer.y - np_height + np_height1 - 30,
				width:np_length1,
				height:np_height1,
				fill: '#d86336',
				objectCaching: false,
				stroke: 'lightgrey',
				strokeWidth: 1,
			};
			var rect = new fabric.Rect(cageConfig);
			self.canvas.add(rect);
			self.canvas.bringToFront(rect);

			$('#select_tool').click();
		}
	});

	self.canvas.on('mouse:move', function(o){
		if(self.lineEnabled){
			if (!isDown) return;
			var pointer = self.canvas.getPointer(o.e);
			mline.set({ x2: pointer.x, y2: pointer.y });
			self.canvas.renderAll();
		}
	});

	self.canvas.on('mouse:up', function(o){
		if(self.lineEnabled){
			isDown = false;
			$('#select_tool').click();
		}
	});

	if(self.tool_data != null){ 
		var fabricJson = self.tool_data.last_config;
		var fabricJsonObjects = self.tool_data.last_config.objects;
		
		var line_with_arrows = [];
		var images = [];

		for(var key in fabricJsonObjects){
			if(fabricJsonObjects[key]['type'] == 'line_with_arrow'){
				line_with_arrows.push(fabricJsonObjects[key]);
				delete fabricJsonObjects[key];
			}else if(fabricJsonObjects[key]['type'] == 'image'){
				images.push(fabricJsonObjects[key]);
				delete fabricJsonObjects[key];
			}
		}

		fabricJson['objects'] = fabricJsonObjects;
		fabricJson = JSON.stringify(fabricJson);
		
		//Load All Objects
		self.canvas.loadFromJSON(fabricJson, function() {
    		self.canvas.renderAll();
		});

		//Load Line With Arrows
		for(var key in line_with_arrows){
			var c1 = line_with_arrows[key]['x1'];
			var c2 = line_with_arrows[key]['x2'];
			var c3 = line_with_arrows[key]['y1'];
			var c4 = line_with_arrows[key]['y2'];
			var line = new LineWithArrow([c1, c3, c2, c4], line_with_arrows[key]);
			self.canvas.add(line);
		}

		//Load Images
		var config = null;
		var img_url = null;
		var activeObject = null;
		
		async function loadImages (images){
			for(var key in images){
				img_url = images[key]['src'];
				config = images[key];
				await createImage(img_url,config)
			
			}
		}

		function createImage(img_url,config){
			fabric.Image.fromURL(img_url, function(img) {
				var oImg = img.set(config);
				oImg.scaleToHeight(parseInt($('#il_inverter_height').val()));
				oImg.scaleToWidth(parseInt($('#il_inverter_length').val()));
				self.canvas.add(oImg);
				self.canvas.setActiveObject(oImg);
				activeObject = self.canvas.getActiveObject();
				self.inverters.push(activeObject);
			},{crossOrigin: 'anonymous'});
		}

		loadImages(images);
		
	}

	$('#pills-inverter').bind('contextmenu', function(e) {
    	e.preventDefault();
	});

    self.fabric.util.addListener(document.getElementsByClassName('upper-canvas')[0], 'contextmenu', function(e) {
        var cnvsPos = $('#pills-inverter').offset();
        self.curX = e.clientX - cnvsPos.left - 10;
        self.curY = e.clientY - cnvsPos.top;
        $('#myMenu').css({'top': self.curY, 'left': self.curX}).fadeIn('slow');
        //console.log("Position of the cursor point" + curX + "," + curY);
    });
  
  	$(document).click(function(event) {
        if (!$(event.target).closest('#myMenu').length) {
            if ($('#myMenu').css("display", "block")) {
                $('#myMenu').css("display", "none");
            }
        }
    });
};

inverter_layout_manager.prototype.add_inverter = function(){
	var self = this;

	var il_inverter_length = $('#il_inverter_length').val();
	if(il_inverter_length == ''){
		toastr['error']('Inverter length cannot be empty');
		return false;
	}

	var il_inverter_height = $('#il_inverter_height').val();
	if(il_inverter_height == ''){
		toastr['error']('Inverter height cannot be empty');
		return false;
	}

	var il_inverter_length = $('#il_inverter_length').val();
	if(il_inverter_length == ''){
		toastr['error']('Inverter length cannot be empty');
		return false;
	}
	
	if(self.inverter_image == null){
		toastr['error']('Inverter Design Image is not present.');
		return false;
	}

	var inverter_distance = $('#il_inverter_distance').val()
	self.inverter_distance = parseInt(inverter_distance);

	//Inverter Config
	var totalInverterObjects = (self.inverters.length);
	var lastInverterObj =  self.inverters[totalInverterObjects - 1];
	
	var config = {
		left: 200,
		top: 250,
		id: 'first_invereter',
		object_uuid: self.create_uuid(),
		object_id: $('#il_inverter_list').val(),
	};

	if(lastInverterObj){
		$('#add_il_inverter_cage').click();
		var inverter_distance = $('#il_inverter_distance').val()
		inverter_distance = parseInt(inverter_distance);

		var il_inverter_length = $('#il_inverter_length').val()
		il_inverter_length = parseInt(il_inverter_length);

		var left = lastInverterObj.left + inverter_distance + il_inverter_length;

		var config = {
			left: left,
			top: lastInverterObj.top,
			objectCaching: false,
			object_uuid: self.create_uuid(),
			object_id: $('#il_inverter_list').val(),
		};
	}

	//Add Inverter
	var activeObject = null;
	var img_url = self.inverter_image;
	fabric.Image.fromURL(img_url, function(img) { 
		var oImg = img.set(config);
		oImg.scaleToHeight(parseInt($('#il_inverter_height').val()));
		oImg.scaleToWidth(parseInt($('#il_inverter_length').val()));
		self.canvas.add(oImg);
		self.canvas.setActiveObject(oImg);
		activeObject = self.canvas.getActiveObject();
		self.inverters.push(activeObject);

		//var c1 = activeObject.aCoords.tl.x + 40;
		//var c2 = activeObject.aCoords.tr.x - 40;
		var c1 = activeObject.aCoords.tl.x + 5;
		var c2 = activeObject.aCoords.tr.x - 5;
		var c3 = activeObject.aCoords.tl.y - 20;
		var c4 = activeObject.aCoords.tr.y - 20;
		var line1 = new LineWithArrow([c1, c3, c2, c4], {
			stroke: 'black',
		});
		//self.canvas.add(line1);

	    //var c1 = activeObject.aCoords.tl.x + 140
		//var c3 = activeObject.aCoords.tl.y - 10;

		var c1 = activeObject.aCoords.tl.x + 140
		var c3 = activeObject.aCoords.tl.y - 40;

		var inverter_length = $('#il_inverter_length').val();
		var text1 = new fabric.Text(inverter_length+'mm', { left: c1, top: c3, fontSize: 22 });
		//self.canvas.add(text1);

		if(lastInverterObj){
			var l = activeObject.aCoords.tl.x;
	    	/**
		    var c1 = lastInverterObj.aCoords.tr.x - 28;
			var c2 = l + 28;
			var c3 = lastInverterObj.aCoords.tr.y + 100;
			var c4 = lastInverterObj.aCoords.tr.y + 100;
			*/

			var c1 = lastInverterObj.aCoords.tr.x + 10;
			var c2 = l - 10;
			var c3 = lastInverterObj.aCoords.tr.y + 100;
			var c4 = lastInverterObj.aCoords.tr.y + 100;

			var line2 = new LineWithArrow([c1, c3, c2, c4], {
				stroke: 'black',
			});
			self.canvas.add(line2);

			var inverter_distance = $('#il_inverter_distance').val();
			var c1 = (c1 + c2)/2 - 15;
			var c2 = (c3 + c4)/2;
			var text2 = new fabric.Text(inverter_distance+'mm', { left: c1, top: c2, fontSize: 22 });
			self.canvas.add(text2);
		}

		self.canvas.renderAll();

		var d = {};
		d.proposal_id = self.proposal_id;
		d.lead_id = self.lead_id;
		d.site_id = self.site_id;
		d.fabricJson = JSON.stringify(self.canvas);
		//self.save_design_tool_data(d);
	},{crossOrigin: 'anonymous'});

	$('#add_il_inverter_cage').parent().parent().addClass('hidden');
};

inverter_layout_manager.prototype.add_inverter_cage = function(){
	var self = this;

	self.inverter_distance = parseFloat($('#il_inverter_distance').val());
	self.inverter_distance = parseInt(self.inverter_distance);

	//Add Inverter Cage if Required
	//Inverter Cage Config
	if($('#il_inverter_length').val() == ''){
		toastr['error']('Provide Inverter length first');
		return false;
	}
	var il_inverter_length = parseInt($('#il_inverter_length').val());
	var cageConfig = {
		left: 140,
		top: 400,
		width:il_inverter_length,
		height:50,
		fill: 'black',
		objectCaching: false,
		stroke: 'black',
		strokeWidth: 4,
		id: 'main',
	};

	var totalInverterCageObjects = self.inverter_cages.length;
	var lastInverterCageObj =  self.inverter_cages[totalInverterCageObjects - 1];

	//Inverter Config
	var totalInverterObjects = (self.inverters.length);
	var lastInverterObj =  self.inverters[totalInverterObjects - 1];

	if(lastInverterCageObj){
		var mainCage = null;
		self.canvas.getObjects().forEach((obj) => {
			if(obj.id == 'main'){
				mainCage = obj;
			}
		});
		var rect = fabric.util.object.clone(mainCage);
		rect.set("left", 140 + 100 + lastInverterObj.left + self.inverter_distance);

		self.canvas.add(rect);
		self.canvas.bringToFront(rect);
		self.canvas.setActiveObject(rect);
		var activeObject = self.canvas.getActiveObject();
		self.inverter_cages.push(activeObject);

	}else{
		var rect = new fabric.Rect(cageConfig);
		if(self.inverters.length == 0){
			self.canvas.add(rect);
			self.canvas.bringToFront(rect);
			self.canvas.setActiveObject(rect);
			var activeObject = self.canvas.getActiveObject();
			self.inverter_cages.push(activeObject);
		}
	}

	$('#add_il_inverter_cage').parent().parent().addClass('hidden');
};

inverter_layout_manager.prototype.deleteObject = function(eventData, transform) {
	var self = this;
	var target = transform.target;
	var canvas = target.canvas;
	canvas.remove(target);
	canvas.requestRenderAll();
}

inverter_layout_manager.prototype.renderIcon = function(ctx, left, top, styleOverride, fabricObject) {
	var deleteIcon = "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'%3E%3Csvg version='1.1' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='595.275px' height='595.275px' viewBox='200 215 230 470' xml:space='preserve'%3E%3Ccircle style='fill:%23F44336;' cx='299.76' cy='439.067' r='218.516'/%3E%3Cg%3E%3Crect x='267.162' y='307.978' transform='matrix(0.7071 -0.7071 0.7071 0.7071 -222.6202 340.6915)' style='fill:white;' width='65.545' height='262.18'/%3E%3Crect x='266.988' y='308.153' transform='matrix(0.7071 0.7071 -0.7071 0.7071 398.3889 -83.3116)' style='fill:white;' width='65.544' height='262.179'/%3E%3C/g%3E%3C/svg%3E";
	var img = document.createElement('img');
	img.src = deleteIcon;
	var self = this;
	var size = this.cornerSize;
	ctx.save();
	ctx.translate(left, top);
	ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
	ctx.drawImage(img, -size/2, -size/2, size, size);
	ctx.restore();
};

inverter_layout_manager.prototype.handle_inverter_crud = function(inverter,action){
	var self = this;

	if(action == 'remove'){
		var mObj = {};
		mObj.object_type = 'PANEL';
		mObj.object_id = panel.options.sp_id;
		self.remove_mapping_object(mObj);
	}else{
		var mObj = {};
		mObj.object_type = 'PANEL';
		mObj.object_id = panel.options.sp_id;
		mObj.object_bounds = bounds;
		mObj.object_properties = panel.options;
		mObj.real_product_id = parseInt(panel.options.sp_panel_id);
		self.save_design_object(mObj);
	}
}

inverter_layout_manager.prototype.fetch_design_tool_data = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/inverter-tool/fetch_design_tool_data',
		type: 'get',
		data: formData,
		dataType: 'json',
		success: function (data) {

			if($('#inverter_notes')){
				tinymce.init({
					selector: "#inverter_notes",
					branding: false,
					menubar: false,
					statusbar: false,
					plugins: "lists,link",
					toolbar: 'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
					mode: "textareas",
					mobile: {
						theme: 'silver'
					},
					entity_encoding : "raw",
					height:500
				});
			}

			if(data.tool_data.id != undefined){
				self.tool_data = data.tool_data;
				if(data.tool_data.notes != null && data.tool_data.notes != ''){
					tinymce.get('inverter_notes').setContent(data.tool_data.notes);
					$('#inverter_notes').val(data.tool_data.notes);
				}

				if(data.tool_data.snapshot != null){
					$('#download_snapshot_link').removeClass('d-none');
					$('#download_snapshot_link').attr('href',data.tool_data.snapshot);
				}
			}

			self.initialize();
		}
	});
};

inverter_layout_manager.prototype.save_design_tool_snapshot = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/inverter-tool/save_design_tool_snapshot',
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function (data) {
			toastr['success'](data.status);
			$('#download_snapshot_link').removeClass('d-none');
			$('#download_snapshot_link').attr('href',data.snapshotUrl);
		}
	});
};

inverter_layout_manager.prototype.save_design_tool_data = function (data) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/inverter-tool/save_design_tool_data',
		type: 'post',
		data: data,
		dataType: 'json',
		success: function (data) {
			toastr['success'](data.status);
		},
		error: function (data) {
			toastr['success']('Data Saving Failed');
		},
	});
};

inverter_layout_manager.prototype.save_design_object = function (objectData) {
	var self = this;

	if(!self.tool_data_save){
		return false;
	}

	objectData.proposal_id = self.proposal_id;
	objectData.lead_id = self.lead_id;
	objectData.site_id = self.site_id;
	$.ajax({
		url: base_url + 'admin/inverter-tool/save_design_object',
		type: 'post',
		data: objectData,
		dataType: 'json',
		beforeSend:function(){
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
		},
		error: function (data) {
			$('#object_loader').addClass('d-none');
		},
	});
};

const LineWithArrow = self.fabric.util.createClass(self.fabric.Line, {
	type: 'line_with_arrow',

	hasBorders: true,
	hasControls: true,

	_getCacheCanvasDimensions() {
		var dim = this.callSuper('_getCacheCanvasDimensions');
      dim.width += 15; // found by trial and error
      dim.height += 15; // found by trial and error
      return dim;
  },
  

  _render: function(ctx) {
  	this.ctx = ctx;
  	this.callSuper('_render', ctx);
  	let p = this.calcLinePoints();
  	let xDiff = this.x2 - this.x1;
  	let yDiff = this.y2 - this.y1;
  	let angle = Math.atan2(yDiff, xDiff);
  	this.drawArrow(angle, p.x2, p.y2);
  	ctx.save();
  	xDiff = -this.x2 + this.x1;
  	yDiff = -this.y2 + this.y1;
  	angle = Math.atan2(yDiff, xDiff);
  	this.drawArrow(angle, p.x1, p.y1);
  },

  drawArrow: function(angle, xPos, yPos) {
  	this.ctx.save();
  	this.ctx.translate(xPos, yPos);
  	this.ctx.rotate(angle);
  	this.ctx.beginPath();
    // Move 5px in front of line to start the arrow so it does not have the square line end showing in front (0,0)
    this.ctx.moveTo(5, 0);
    this.ctx.lineTo(-5, 5);
    this.ctx.lineTo(-5, -5);
    this.ctx.closePath();
    this.ctx.fillStyle = this.stroke;
    this.ctx.fill();
    this.ctx.restore();
}
});

const STATE_IDLE = 'idle';
const STATE_PANNING = 'panning';
fabric.Canvas.prototype.toggleDragMode = function(dragMode) {
  // Remember the previous X and Y coordinates for delta calculations
  let lastClientX;
  let lastClientY;
  // Keep track of the state
  let state = STATE_IDLE;
  // We're entering dragmode
  if (dragMode) {
    // Discard any active object
    this.discardActiveObject();
    // Set the cursor to 'move'
    this.defaultCursor = 'move';
    // Loop over all objects and disable events / selectable. We remember its value in a temp variable stored on each object
    this.forEachObject(function(object) {
    	object.prevEvented = object.evented;
    	object.prevSelectable = object.selectable;
    	object.evented = false;
    	object.selectable = false;
    });
    // Remove selection ability on the canvas
    this.selection = false;
    // When MouseUp fires, we set the state to idle
    this.on('mouse:up', function(e) {
    	state = STATE_IDLE;
    });
    // When MouseDown fires, we set the state to panning
    this.on('mouse:down', (e) => {
    	state = STATE_PANNING;
    	lastClientX = e.e.clientX;
    	lastClientY = e.e.clientY;
    });
    // When the mouse moves, and we're panning (mouse down), we continue
    this.on('mouse:move', (e) => {
    	if (state === STATE_PANNING && e && e.e) {
        // let delta = new fabric.Point(e.e.movementX, e.e.movementY); // No Safari support for movementX and movementY
        // For cross-browser compatibility, I had to manually keep track of the delta

        // Calculate deltas
        let deltaX = 0;
        let deltaY = 0;
        if (lastClientX) {
        	deltaX = e.e.clientX - lastClientX;
        }
        if (lastClientY) {
        	deltaY = e.e.clientY - lastClientY;
        }
        // Update the last X and Y values
        lastClientX = e.e.clientX;
        lastClientY = e.e.clientY;

        let delta = new fabric.Point(deltaX, deltaY);
        this.relativePan(delta);
        this.trigger('moved');
    }
});
} else {
    // When we exit dragmode, we restore the previous values on all objects
    this.forEachObject(function(object) {
    	object.evented = (object.prevEvented !== undefined) ? object.prevEvented : object.evented;
    	object.selectable = (object.prevSelectable !== undefined) ? object.prevSelectable : object.selectable;
    });
    // Reset the cursor
    this.defaultCursor = 'default';
    // Remove the event listeners
    this.off('mouse:up');
    this.off('mouse:down');
    this.off('mouse:move');
    // Restore selection ability on the canvas
    this.selection = true;
}
};

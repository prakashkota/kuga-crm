<style>
    @font-face {
        font-family: 'CenturyGothic';
        font-weight: normal;
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf');
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('embedded-opentype'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('woff'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('truetype');
    }

    @font-face {
        font-family: 'CenturyGothic';
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf');
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('embedded-opentype'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('woff'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('truetype');
        font-weight: bold;
    }

    .century_gothic,
    .cls_004 {
        font-family: 'CenturyGothic';
    }

    .page-1-back-bg {
        background-color: #FFFFFF;
        background-repeat: no-repeat;
        margin: auto;
        padding: 0;
        background-size: 100% 100%;
        max-width: 1287px;
        max-height: 870px;
        box-shadow: 1px 1px 3px 1px #333;
        border-collapse: separate;
    }
    body {
        margin: 0;
    }
</style>

<body class="century_gothic">
    <div class="page-1-back-bg page-spl">
        <img src="https://kugacrm.com.au/job_crm/assets/evidence_form/pp-page1.jpg" width="1287px" height="870px">
        <div style="position:relative;">
            <div style="top: -100px; position: absolute;right: 578px;  color:#fff; font-size:14px;" class="cls_004"><span class="cls_004">Kuga Electrical</span></div>
            <div style="top: -100px; position: absolute;right: 378px;  color:#fff; font-size:14px;" class="cls_004"><span class="cls_004">15.11.2021</span></div>
            <div style="top: -100px;position: absolute;right: 103px;color:#fff;font-size:14px;" class="cls_004"><span class="cls_004">Clement Doudet</span></div>
            <div style="top: -80px;position: absolute;right: 130px;color:#fff;font-size:14px;" class="cls_004"><span class="cls_004">0410 569 860</span></div>
            <div style="top: -60px;position: absolute;right: 33px;color:#fff;font-size:14px;" class="cls_004"><span class="cls_004">c.doudet@13kuga.com.au</span></div>
        </div>
    </div>
</body>
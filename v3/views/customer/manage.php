<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?> Manage <?php if($this->uri->segment(3) ==  'manage-franchise'){ ?> Franchise <?php } ?> Customer</h1>
        <div class="content-header-right col-md-6 col-12">
            <div  class="float-md-right col-md-5" style="display:inline-flex;">
                <?php if ($this->aauth->is_member('Admin')) { ?>
                <select class="" name="userid" id="userid">
                    <option value="">Select User</option>
                    <option value="0" <?php if ($selected_user == 0) { ?>selected=""<?php } ?>>All</option>
                    <?php foreach ($users as $key => $value) { ?>
                        <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>" <?php if ($value['user_id'] == $selected_user) { ?>selected=""<?php } ?> value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                    <?php } ?>
                </select>
                <div class="form-check-inline" style="margin-left:5px;">
                    <input type="checkbox" class="form-check-input sr-show_inactive_users" value="1">
                    <i class="fa fa-user-times"></i>
                </div>
                <?php } ?>
                
            </div>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="franchise_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($customers))
                                        foreach ($customers as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $value['first_name'] .' '. $value['last_name'] ; ?></td>
                                                <td><?php echo $value['customer_email']; ?></td>
                                                <td><?php echo $value['customer_contact_no']; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <?php  if ($this->aauth->is_member('Admin')) { ?>
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/customer/edit/<?php echo $value['cust_id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <?php } else {?>
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/franchise/edit-customer/<?php echo $value['cust_id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <?php } ?>
                                                            <!--<a class="dropdown-item" href="productTypeManage.php?id=<?php echo $value['id']; ?>&action=delete"><i class="icon-trash"></i> Delete</a>-->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

 <script>
        $(document).ready(function () {
            // DataTable
            $('#franchise_list').DataTable();

            $('.sr-show_inactive_users').click(function () {
                $('.sr-inactive_user').addClass('hidden');
                if ($(this).prop("checked") == true) {
                    $('.sr-inactive_user').removeClass('hidden');
                }
            });

            $('#userid').change(function () {
                var id = $(this).val();
                window.location.href = base_url + 'admin/customer/manage-franchise?id=' + id;
            });

        });
    </script>

var solar_booking_form_manager = function (options) {
    var self = this;
    this.lead_data = {};
    this.lead_uuid = (options.lead_uuid && options.lead_uuid != '') ? options.lead_uuid : {};
    this.booking_form_uuid = (options.booking_form_uuid && options.booking_form_uuid != '') ? options.booking_form_uuid : '';
    this.site_id = (options.site_id && options.site_id != '') ? options.site_id : '';
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.ae_subtotal_excGST = 0;
    this.signature_pad = [];
    this.signature_pad_ids = ['authorised_on_behalf_signature', 'authorised_by_behalf_sales_rep_signature', 'signature'];
    this.is_vic_rebate = false;
    this.vic_rebate = 0;
    this.vic_loan = 0;
    this.deal_type = '';
    this.quote_total_init = false;
    this.saving_calculation_details = '';
    this.postcode = '';
    this.system_size = 0;
    this.no_of_panels = 0;
    this.prd_panel = 0;
    this.prd_inverter = 0;
    this.prd_battery = 0;
    this.prd_panel_price = 0;
    this.prd_inverter_price = 0;
    this.prd_battery_price = 0;
    this.system_total = 0;
    this.prev_system_total = 0;
    this.prev_total_cost = 0;
    this.custom_quote_total_inGst = 0;
    this.stored_custom_quote_total_inGst = 0;
    this.racking_price = 0;
    this.kliplock_price = 0;
    this.additional_cost_item_price = 0;
    this.line_item_price = 0;
    this.total_cost = 0;
    this.original = {};
    this.total_cost_incGst = 0;
    this.postcode_rating = 0;
    this.stc_deduction = 0;
    this.stc_price_assumption = options.stc_price_assumption;
    this.certificate_margin = options.certificate_margin;
    this.postcode = '';
        
    $("#authorised_on_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#authorised_by_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#booked_at").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('.network_type').on('change',function(){
        if($('.network_type:checked').val() == '1'){
            $('.wifi_result').removeClass('hidden');
            $('[name="business_details[wifi_username]"]').attr('required',true);
            $('[name="business_details[wifi_password]"]').attr('required',true);
        }else{
            $('.wifi_result').addClass('hidden');
            $('[name="business_details[wifi_username]"]').attr('required',false);
            $('[name="business_details[wifi_password]"]').attr('required',false);
        }
    });

    $('#expiry_date').datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('.vic_rebate').on('change',function(){
        if($('.vic_rebate:checked').val() == '1' || $('.vic_loan:checked').val() == '1'){
            $('.expiry_date_tr').removeClass('hidden');
            $('[name="business_details[expiry_date]"]').attr('required',true);
            
        }else{
            $('.expiry_date_tr').addClass('hidden');
            $('[name="business_details[expiry_date]"]').attr('required',false);
        }

        if($('.vic_rebate:checked').val() == '1'){
            self.is_vic_rebate = true;
            $('#vic_rebate').parent().parent().removeClass('hidden');
        }else{
            self.is_vic_rebate = false;
            $('#vic_rebate').parent().parent().addClass('hidden');
            self.vic_rebate = 0;
            $('#vic_rebate').val(self.vic_rebate);
        }
        if (self.quote_total_init) {
            self.calculate_sum();
        }
    });

    $('.vic_loan').on('change',function(){
        if($('.vic_loan:checked').val() == '1' || $('.vic_rebate:checked').val() == '1'){
            $('.expiry_date_tr').removeClass('hidden');
            $('[name="business_details[expiry_date]"]').attr('required',true);
        }else{
            $('.expiry_date_tr').addClass('hidden');
            $('[name="business_details[expiry_date]"]').attr('required',false);
        }

        if($('.vic_loan:checked').val() == '1'){
            self.is_vic_loan = true;
            $('#vic_loan').parent().parent().removeClass('hidden');
        }else{
            self.is_vic_loan = false;
            $('#vic_loan').parent().parent().addClass('hidden');
            self.vic_loan = 0;
            $('#vic_loan').val(self.vic_loan);
        }

        if (self.quote_total_init) {
            self.calculate_sum();
        }
    });

    $('.stc_calculate_inputs').change(function () {
        self.calculate_stc_rebate(true);
    });

    $('#save_stc_btn').click(function () {
        //self.calculate_payment_summary(true);
        self.calculate_sum();
        $('#stc_calculation_modal').modal('hide');
        self.calculate_stc_rebate(true);
    });

    $('#stc_deduction').click(function () {
        var dt = new Date();
        $('#stc_calculation_modal').modal('show');
        $('#stc_calculate_system_size').val(self.system_size);
        $('#stc_calculate_postcode').val(self.postcode);
        $('#stc_calculate_year').val(dt.getFullYear());
        self.calculate_stc_rebate();
    });
    
    $('#stc_deduction,#vic_rebate,#vic_loan, #total_payable').on('change',function () {
        self.calculate_sum();
    });
    
    $('#system_size').change(function () {
        self.fetch_line_item_data();
    });
    
    $('#prd_panel').change(function () {
        var id = $(this).find(':selected').attr('data-id');
        $(this).next('[name="product[product_id][]"]').val(id);
        
        var data_item = $(this).find(':selected').attr('data-item');
        data_item = data_item != null && data_item != '' ? JSON.parse(data_item) : '';
        if(data_item != ''){
            $('.product_capacity_1').val(data_item.capacity);
        }
    });
    
    $('#prd_inverter').change(function () {
        var id = $(this).find(':selected').attr('data-id');
        $(this).next('[name="product[product_id][]"]').val(id);
        
        var data_item = $(this).find(':selected').attr('data-item');
        data_item = data_item != null && data_item != '' ? JSON.parse(data_item) : '';
        if(data_item != ''){
            $('.product_capacity_2').val(data_item.capacity);
        }
    });

    $('#prd_battery').change(function () {
        var id = $(this).find(':selected').attr('data-id');
        $(this).next('[name="product[product_id][]"]').val(id);
        
        var data_item = $(this).find(':selected').attr('data-item');
        data_item = data_item != null && data_item != '' ? JSON.parse(data_item) : '';
        if(data_item != ''){
            $('.product_capacity_3').val(data_item.capacity);
        }
    });
    
    
    $('#pv_mounting').on('change',function(){
        var val = $(this).val();
        
        if(val == 'Not Required'){
            $('.product_qty_4').prop('required',false);
            $('.product_cost_excGST_4').prop('required',false);
            $('.product_total_cost_excGst_4').prop('required',false);
        }else{
            $('.product_qty_4').prop('required',true);
            $('.product_cost_excGST_4').prop('required',true);
            $('.product_total_cost_excGst_4').prop('required',true);
        }
    });
    
    
    $('#roof_type').on('change',function(){
        var val = $(this).val();
        
        if(val == 'Not Required'){
            $('.product_qty_5').prop('required',false);
            $('.product_cost_excGST_5').prop('required',false);
            $('.product_total_cost_excGst_5').prop('required',false);
        }else{
            $('.product_qty_5').prop('required',true);
            $('.product_cost_excGST_5').prop('required',true);
            $('.product_total_cost_excGst_5').prop('required',true);
        }
    });
    
    
    if (self.booking_form_uuid != '') {
        self.fetch_solar_booking_form_data();
    } else if (self.lead_uuid != '') {
        var data = {};
        data.uuid = self.lead_uuid;
        data.site_id = self.site_id;
        self.fetch_lead_data(data);
    }

    self.handle_items();

};

solar_booking_form_manager.prototype.initialize = function () {
    var self = this;

    $('#save_booking_form').click(function () {
        self.save_booking_form();
    });

    $('#generate_booking_form_pdf').click(function () {
        self.save_booking_form(true);
    });

    //Handle Singature Pads
    $('.sign_create').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').hide();
        $('.sr-deal_actions').hide();
        $('#signatures').show();
        for (var i = 0; i < 4; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        window.scrollTo({top: 0, behavior: 'smooth'});
        //Resize Canvas for Signature pad because we assume user might have rotated the tablet
        self.resizeCanvas();
    });

    $('.sign_close').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').show();
        $('.sr-deal_actions').show();
        $('#signatures').hide();
        for (var i = 0; i < 3; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });


    var is_upfront_30 = document.getElementById('is_upfront_30');
    var is_upfront_99 = document.getElementById('is_upfront_99');
    var is_upfront_100 = document.getElementById('is_upfront_100');

    if(is_upfront_30){
        $('#is_upfront_30_excGST').change(function () {
            var total_payable = $(this).val();
            //$('#is_upfront_30_deposit_50_after').val(parseFloat(total_payable * 0.5).toFixed(2));
            //$('#is_upfront_30_deposit_50_before').val(parseFloat(total_payable * 0.5).toFixed(2));
            //$('#is_upfront_30_deposit_0').val(0);
        });
    }

    if (is_upfront_99) {
        $('#is_upfront_99').change(function () {
            $('#is_upfront_99').attr('checked', 'checked');
            $('#is_finance').removeAttr('checked');
            $('#is_energy_plan').removeAttr('checked');
            $('#is_energy_plan_8').removeAttr('checked');
            $('#is_energy_plan_12').removeAttr('checked');
            $('#is_energy_plan_15').removeAttr('checked');
        });

        $('#is_finance').change(function () {
            $('#is_finance').attr('checked', 'checked');
            $('#is_upfront_99').removeAttr('checked');
            $('#is_energy_plan').removeAttr('checked');
            $('#is_energy_plan_8').removeAttr('checked');
            $('#is_energy_plan_12').removeAttr('checked');
            $('#is_energy_plan_15').removeAttr('checked');
        });

        $('#is_energy_plan').change(function () {
            $('#is_energy_plan').attr('checked', 'checked');
            $('#is_upfront_99').removeAttr('checked');
            $('#is_finance').removeAttr('checked');
        });

        $('#is_upfront_99_excGST').change(function () {
            var total_payable = $(this).val();
            $('#is_upfront_99_deposit_10').val(parseFloat(total_payable * 0.1).toFixed(2));
            $('#is_upfront_99_deposit_70').val(parseFloat(total_payable * 0.7).toFixed(2));
            $('#is_upfront_99_deposit_20').val(parseFloat(total_payable * 0.2).toFixed(2));
        });
    }

    if (is_upfront_100) {
        $('#is_upfront_100').change(function () {
            $('#is_upfront_100').attr('checked', 'checked');
            $('#is_finance').removeAttr('checked');
            $('#is_energy_plan').removeAttr('checked');
            $('#is_energy_plan_8').removeAttr('checked');
            $('#is_energy_plan_12').removeAttr('checked');
            $('#is_energy_plan_15').removeAttr('checked');
        });

        $('#is_finance').change(function () {
            $('#is_finance').attr('checked', 'checked');
            $('#is_upfront_100').removeAttr('checked');
            $('#is_energy_plan').removeAttr('checked');
            $('#is_energy_plan_8').removeAttr('checked');
            $('#is_energy_plan_12').removeAttr('checked');
            $('#is_energy_plan_15').removeAttr('checked');
        });

        $('#is_energy_plan').change(function () {
            $('#is_energy_plan').attr('checked', 'checked');
            $('#is_upfront_100').removeAttr('checked');
            $('#is_finance').removeAttr('checked');
        });

        $('#is_upfront_100_excGST').change(function () {
            var total_payable = $(this).val();
            $('#is_upfront_100_deposit_10').val(parseFloat(total_payable * 0.1).toFixed(2));
            $('#is_upfront_100_deposit_70').val(parseFloat(total_payable * 0.7).toFixed(2));
            $('#is_upfront_100_deposit_20').val(parseFloat(total_payable * 0.2).toFixed(2));
        });
    }


    $('#stc_deduction,#vic_rebate,#total_payable,#vic_loan').change(function () {
        self.calculate_sum();
    });

    self.handle_items();
    self.handle_cost_centre_change();
    self.upload_image();
    self.show_image();
    self.hide_image();
    self.remove_signature_image();
    self.create_signature_pad();
    self.fetch_postcode_rating(self.postcode);
};

solar_booking_form_manager.prototype.fetch_lead_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading lead data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                self.lead_data = stat.lead_data;
                
                console.log(self.lead_data);
                $('#first_name').val(self.lead_data.first_name);
                $('#last_name').val(self.lead_data.last_name);
                $('#contact_no').val(self.lead_data.customer_contact_no);
                $('#position').val(self.lead_data.position);
                $('#email').val(self.lead_data.customer_email);

                $('#entity_name').val(self.lead_data.customer_company_name);
                $('#address').val(self.lead_data.address);
                $('#select2-locationstreetAddress-container').html(self.lead_data.address);
                $('#postcode').val(self.lead_data.postcode);

                $('#authorised_on_behalf_company_name').val(self.lead_data.customer_company_name);
                $('#authorised_on_behalf_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
                $('#authorised_on_behalf_position').val(self.lead_data.position);

                $("#authorised_on_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
                $("#authorised_by_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
                $("#booked_at").val(moment(new Date()).format('DD/MM/YYYY'));

                self.postcode = self.lead_data.postcode;


                self.initialize();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_booking_form_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

solar_booking_form_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

solar_booking_form_manager.prototype.handle_cost_centre_change = function () {
    var self = this;
    $(document).on('change','#cost_centre_id',function(){
        var cost_centre_id = $(this).val();
        if(cost_centre_id == ''){
            toastr['error']('Please Select a Cost Centre Value');
            return false;
        }
        
        var type_id = $('[name="type_id"]').val();
        if(self.booking_form_uuid == ''){
            window.location.href = base_url + 'admin/booking_form/add_residential_solar_booking_form?deal_ref='+self.lead_uuid+'&site_ref='+self.site_id + '&type_ref='+type_id+'&cost_centre_id='+cost_centre_id;
            return false;
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'admin/booking_form/save_cost_centre',
            datatype: 'json',
            data: {
                'cost_centre_id' : cost_centre_id,
                'booking_form_uuid' : self.booking_form_uuid,
                'lead_uuid' : self.lead_uuid,
                'site_id': self.site_id,
                'type_id': type_id,
                'type': 'Solar'
            },
            success: function (res) {
                if (res.success == true) {
                    window.location.reload();
                }else{
                    toastr['error'](res.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

solar_booking_form_manager.prototype.handle_items = function () {
    var self = this;
    $('.product_qty,.product_cost_excGST').on('change',function () {
        var product_subtotal_excGST = 0.00;
        var total_excGST = 0;
        var total_incGST = 0;
        
        $('table#booking_items tr').each(function () {
            var price = $(this).children().find(".product_cost_excGST").val();
            var qty = $(this).children().find(".product_qty").val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(this).children().find(".product_total_cost_excGst").val(total);
                product_subtotal_excGST = parseFloat(product_subtotal_excGST) + parseFloat(total);
                $('#product_total_excGst').val(product_subtotal_excGST);
                // self.total_incGST = (parseFloat(self.stc_deduction) + parseFloat(self.vic_rebate) + parseFloat(self.vic_loan));
                total_incGST = total_incGST + total;
                $('#total_incGst').val(parseFloat(total_incGST).toFixed(2));
                self.total_cost_incGst = (parseFloat(total_incGST));
            }
        });
        
        self.total_cost_incGst = (parseFloat(self.total_cost_incGst) - (parseFloat(self.stc_deduction) + parseFloat(self.vic_rebate) + parseFloat(self.vic_loan)));
        $('#total_payable').val(parseFloat(self.total_cost_incGst).toFixed(2));
        self.calculate_sum();
    });

    $('.ae_qty,.ae_cost_excGST').change(function () {
        self.ae_subtotal_excGST = 0;
        for (var i = 0; i < 2; i++) {
            var price = $(".ae_cost_excGST_" + i).val();
            var qty = $(".ae_qty_" + i).val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(".ae_total_cost_excGst_" + i).val(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST) + parseFloat(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }
        }
    });

    
};

solar_booking_form_manager.prototype.upload_image = function () {
    var self = this;
    $('.image_upload').change(function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'solar_booking_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

solar_booking_form_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/solar_booking_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

solar_booking_form_manager.prototype.hide_image = function () {
    var self = this;
    $('.image_close').click(function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};

solar_booking_form_manager.prototype.create_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad2 = new SignaturePad(canvas2, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad3 = new SignaturePad(canvas3, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1, signaturePad2, signaturePad3];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();

        canvas2.width = canvas2.offsetWidth * ratio;
        canvas2.height = canvas2.offsetHeight * ratio;
        canvas2.getContext("2d").scale(ratio, ratio);
        signaturePad2.clear();

        canvas3.width = canvas3.offsetWidth * ratio;
        canvas3.height = canvas3.offsetHeight * ratio;
        canvas3.getContext("2d").scale(ratio, ratio);
        signaturePad3.clear();
    }

    //window.onresize = resizeCanvas;
    window.onorientationchange = resizeCanvas;
    resizeCanvas();

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 1)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 1)].toDataURL();
            var ele = $('#' + arr1[(id - 1)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
}

solar_booking_form_manager.prototype.show_signature_image = function (ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.prev().html('<img style="position:relative; height:95px;" src="' + image + '" />');
        } else {
            ele.prev().html('<img style="position:relative; height:95px;" src="' + base_url + 'assets/uploads/solar_booking_form_files/' + image + '" />');

        }
    }
    //Signateur Wrapper hide
    //ele.parent().parent().hide();
    //Show Image Wrapper
    //ele.parent().parent().next('div').show();
    //ele.parent().parent().next('div').children('img').attr('src', image);
};

solar_booking_form_manager.prototype.remove_signature_image = function () {
    var self = this;
    $('.signature_image_close').click(function () {
        $(this).parent().prev('div').show();
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('img').attr('src', '');
    });
};

solar_booking_form_manager.prototype.validate_booking_form = function () {
    var self = this;
    var flag = true;
    var eleArr = [];
    $('.invalid-feedback').remove();
    var elements = document.getElementById("solar_booking_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        $(elements[i]).parent().removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
                eleArr.push($(elements[i]).parent());
            } else {
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'checkbox') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                        eleArr.push($(elements[i]).parent().parent());
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }

    var payment_option_flag = true;
    var is_upfront_30 = document.getElementById('is_upfront_30');
    var is_upfront_99 = document.getElementById('is_upfront_99');
    var is_upfront_100 = document.getElementById('is_upfront_100');

    //Option2 and Option3
    var is_finance = document.getElementById('is_finance');
    var is_energy_plan = document.getElementById('is_energy_plan');

    if (is_upfront_30) {
        var is_upfront_30_checked = document.getElementById('is_upfront_30').checked;
        $('#is_upfront_30').parent().parent().removeClass('is-invalid');
        $('#is_upfront_30_deposit_0').removeClass('is-invalid');
        $('#is_upfront_30_deposit_50_before').removeClass('is-invalid');
        $('#is_upfront_30_deposit_50_after').removeClass('is-invalid');
        $('#is_upfront_30_excGST').removeClass('is-invalid');

        //$('#is_finance_70_incGST').removeClass('is-invalid');
        //$('#is_finance_30_incGST').removeClass('is-invalid');
        $('#is_finance_monthly_repayments').removeClass('is-invalid');
        $('#is_finance_terms').removeClass('is-invalid');

        if (is_upfront_30_checked) {
            var is_upfront_30_deposit_0 = document.getElementById('is_upfront_30_deposit_0').value;
            var is_upfront_30_deposit_50_before = document.getElementById('is_upfront_30_deposit_50_before').value;
          //  var is_upfront_30_deposit_50_after = document.getElementById('is_upfront_30_deposit_50_after').value;
            var is_upfront_30_excGST = document.getElementById('is_upfront_30_excGST').value;

            if (is_upfront_30_deposit_0 == '') {
                $('#is_upfront_30_deposit_0').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_30_deposit_0'));
            }

            if (is_upfront_30_deposit_50_before == '') {
                $('#is_upfront_30_deposit_50_before').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_30_deposit_50_before'));
            }

            /*if (is_upfront_30_deposit_50_after == '') {
                $('#is_upfront_30_deposit_50_after').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_30_deposit_50_after'));
            }
			*/
            if (is_upfront_30_excGST == '') {
                $('#is_upfront_30_excGST').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_30_excGST'));
            }
        } else if (!is_upfront_30_checked && !is_finance.checked) {
            $('#is_upfront_30').parent().parent().addClass('is-invalid');
            payment_option_flag = false;
            eleArr.push($('#is_upfront_30').parent().parent());
        }

        if (payment_option_flag && is_upfront_30_checked == false) {
            if (is_finance.checked) {
                //var is_finance_70_incGST = document.getElementById('is_finance_70_incGST').value;
                //var is_finance_30_incGST = document.getElementById('is_finance_30_incGST').value;
                var is_finance_monthly_repayments = document.getElementById('is_finance_monthly_repayments').value;
                var is_finance_terms = document.getElementById('is_finance_terms').value;

                /**if (is_finance_70_incGST == '') {
                    $('#is_finance_70_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }

                if (is_finance_30_incGST == '') {
                    $('#is_finance_30_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }*/

                if (is_finance_monthly_repayments == '') {
                    $('#is_finance_monthly_repayments').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_monthly_repayments'));
                }

                if (is_finance_terms == '') {
                    $('#is_finance_terms').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_terms'));
                }
            }
        }
    }

    if (is_upfront_99) {
        var is_upfront_99_checked = document.getElementById('is_upfront_99').checked;
        $('#is_upfront_99').parent().parent().removeClass('is-invalid');
        $('#is_upfront_99_deposit_20').removeClass('is-invalid');
        $('#is_upfront_99_deposit_70').removeClass('is-invalid');
        $('#is_upfront_99_deposit_10').removeClass('is-invalid');
        $('#is_upfront_99_excGST').removeClass('is-invalid');

        //$('#is_finance_90_incGST').removeClass('is-invalid');
        //$('#is_finance_10_incGST').removeClass('is-invalid');
        $('#is_finance_monthly_repayments').removeClass('is-invalid');
        $('#is_finance_terms').removeClass('is-invalid');


        if (is_upfront_99_checked) {
            var is_upfront_99_deposit_20 = document.getElementById('is_upfront_99_deposit_20').value;
            var is_upfront_99_deposit_70 = document.getElementById('is_upfront_99_deposit_70').value;
            var is_upfront_99_deposit_10 = document.getElementById('is_upfront_99_deposit_10').value;
            var is_upfront_99_excGST = document.getElementById('is_upfront_99_excGST').value;

            if (is_upfront_99_deposit_20 == '') {
                $('#is_upfront_99_deposit_20').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_99_deposit_20'));
            }

            if (is_upfront_99_deposit_70 == '') {
                $('#is_upfront_99_deposit_70').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_99_deposit_70'));
            }

            if (is_upfront_99_deposit_10 == '') {
                $('#is_upfront_99_deposit_10').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_99_deposit_10'));
            }

            if (is_upfront_99_excGST == '') {
                $('#is_upfront_99_excGST').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_99_excGST'));
            }

        } else if (!is_upfront_99_checked && !is_finance.checked && !is_energy_plan.checked) {
            $('#is_upfront_99').parent().parent().addClass('is-invalid');
            payment_option_flag = false;
        }

        if (payment_option_flag && is_upfront_99_checked == false && is_energy_plan.checked == false) {
            if (is_finance.checked) {
                //var is_finance_90_incGST = document.getElementById('is_finance_90_incGST').value;
                //var is_finance_10_incGST = document.getElementById('is_finance_10_incGST').value;
                var is_finance_monthly_repayments = document.getElementById('is_finance_monthly_repayments').value;
                var is_finance_terms = document.getElementById('is_finance_terms').value;

                /**if (is_finance_90_incGST == '') {
                    $('#is_finance_90_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }

                if (is_finance_10_incGST == '') {
                    $('#is_finance_10_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }*/

                if (is_finance_monthly_repayments == '') {
                    $('#is_finance_monthly_repayments').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_monthly_repayments'));
                }

                if (is_finance_terms == '') {
                    $('#is_finance_terms').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_terms'));
                }
            }
        }

        if (payment_option_flag && is_upfront_99_checked == false && is_finance.checked == false) {
            if (is_energy_plan.checked) {

                var is_energy_plan_8_checked = document.getElementById('is_energy_plan_8').checked;
                var is_energy_plan_12_checked = document.getElementById('is_energy_plan_12').checked;
                var is_energy_plan_15_checked = document.getElementById('is_energy_plan_15').checked;

                var is_energy_plan_8_kwh = document.getElementById('is_energy_plan_8_kwh').value;
                var is_energy_plan_12_kwh = document.getElementById('is_energy_plan_12_kwh').value;
                var is_energy_plan_15_kwh = document.getElementById('is_energy_plan_15_kwh').value;

                if (!is_energy_plan_8_checked && !is_energy_plan_12_checked && !is_energy_plan_15_checked) {
                    $('#is_energy_plan_8').parent().addClass('is-invalid');
                } else {

                    if (is_energy_plan_8_checked && is_energy_plan_8_kwh == '') {
                        $('#is_energy_plan_8_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_8_kwh'));
                    }

                    if (is_energy_plan_12_checked && is_energy_plan_12_kwh == '') {
                        $('#is_energy_plan_12_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_12_kwh'));
                    }

                    if (is_energy_plan_15_checked && is_energy_plan_15_kwh == '') {
                        $('#is_energy_plan_15_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_15_kwh'));
                    }
                }
            }
        }
    }


    if (is_upfront_100) {
        var is_upfront_100_checked = document.getElementById('is_upfront_100').checked;
        $('#is_upfront_100').parent().parent().removeClass('is-invalid');
        $('#is_upfront_100_deposit_20').removeClass('is-invalid');
        $('#is_upfront_100_deposit_70').removeClass('is-invalid');
        $('#is_upfront_100_deposit_10').removeClass('is-invalid');
        $('#is_upfront_100_excGST').removeClass('is-invalid');

        //$('#is_finance_90_incGST').removeClass('is-invalid');
        //$('#is_finance_10_incGST').removeClass('is-invalid');
        $('#is_finance_monthly_repayments').removeClass('is-invalid');
        $('#is_finance_terms').removeClass('is-invalid');


        if (is_upfront_100_checked) {
            var is_upfront_100_deposit_20 = document.getElementById('is_upfront_100_deposit_20').value;
            var is_upfront_100_deposit_70 = document.getElementById('is_upfront_100_deposit_70').value;
            var is_upfront_100_deposit_10 = document.getElementById('is_upfront_100_deposit_10').value;
            var is_upfront_100_excGST = document.getElementById('is_upfront_100_excGST').value;

            if (is_upfront_100_deposit_20 == '') {
                $('#is_upfront_100_deposit_20').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_100_deposit_20'));
            }

            if (is_upfront_100_deposit_70 == '') {
                $('#is_upfront_100_deposit_70').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_100_deposit_70'));
            }

            if (is_upfront_100_deposit_10 == '') {
                $('#is_upfront_100_deposit_10').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_100_deposit_10'));
            }

            if (is_upfront_100_excGST == '') {
                $('#is_upfront_100_excGST').addClass('is-invalid');
                payment_option_flag = false;
                eleArr.push($('#is_upfront_100_excGST'));
            }

        } else if (!is_upfront_100_checked && !is_finance.checked && !is_energy_plan.checked) {
            $('#is_upfront_100').parent().parent().addClass('is-invalid');
            payment_option_flag = false;
            eleArr.push($('#is_upfront_100').parent().parent());
        }

        if (payment_option_flag && is_upfront_100_checked == false && is_energy_plan.checked == false) {
            if (is_finance.checked) {
                //var is_finance_90_incGST = document.getElementById('is_finance_90_incGST').value;
                //var is_finance_10_incGST = document.getElementById('is_finance_10_incGST').value;
                var is_finance_monthly_repayments = document.getElementById('is_finance_monthly_repayments').value;
                var is_finance_terms = document.getElementById('is_finance_terms').value;

                /**if (is_finance_90_incGST == '') {
                    $('#is_finance_90_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }

                if (is_finance_10_incGST == '') {
                    $('#is_finance_10_incGST').addClass('is-invalid');
                    payment_option_flag = false;
                }*/

                if (is_finance_monthly_repayments == '') {
                    $('#is_finance_monthly_repayments').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_monthly_repayments'));
                }

                if (is_finance_terms == '') {
                    $('#is_finance_terms').addClass('is-invalid');
                    payment_option_flag = false;
                    eleArr.push($('#is_finance_terms'));
                }
            }
        }

        if (payment_option_flag && is_upfront_100_checked == false && is_finance.checked == false) {
            if (is_energy_plan.checked) {

                var is_energy_plan_8_checked = document.getElementById('is_energy_plan_8').checked;
                var is_energy_plan_12_checked = document.getElementById('is_energy_plan_12').checked;
                var is_energy_plan_15_checked = document.getElementById('is_energy_plan_15').checked;

                var is_energy_plan_8_kwh = document.getElementById('is_energy_plan_8_kwh').value;
                var is_energy_plan_12_kwh = document.getElementById('is_energy_plan_12_kwh').value;
                var is_energy_plan_15_kwh = document.getElementById('is_energy_plan_15_kwh').value;

                if (!is_energy_plan_8_checked && !is_energy_plan_12_checked && !is_energy_plan_15_checked) {
                    $('#is_energy_plan_8').parent().addClass('is-invalid');
                } else {

                    if (is_energy_plan_8_checked && is_energy_plan_8_kwh == '') {
                        $('#is_energy_plan_8_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_8_kwh'));
                    }

                    if (is_energy_plan_12_checked && is_energy_plan_12_kwh == '') {
                        $('#is_energy_plan_12_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_12_kwh'));
                    }

                    if (is_energy_plan_15_checked && is_energy_plan_15_kwh == '') {
                        $('#is_energy_plan_15_kwh').addClass('is-invalid');
                        payment_option_flag = false;
                        eleArr.push($('#is_energy_plan_15_kwh'));
                    }
                }
            }
        }
    }

    if($('[name="business_details[network_type]"]:checked').size() == 0){
        $('[name="business_details[network_type]"]').parent().parent().addClass('is-invalid');
        flag = false;
        eleArr.push($(elements[i]).parent().parent());
    }

    if(eleArr.length > 0){
        $('html, body').animate({
            scrollTop: eleArr[0].offset().top - 70
        }, 2000);
    }
    
    flag = (payment_option_flag == false) ? false : flag;

    return flag;
};

solar_booking_form_manager.prototype.save_booking_form = function (download) {
    var self = this;

    //Convert Signature to Images
    /**var signature_pad = self.signature_pad;
     var signature_pad_ids = self.signature_pad_ids;
     for (var i=0; i < signature_pad.length; i++) {
     if (signature_pad[i].isEmpty()) {
     } else {
     var dataURL = signature_pad[i].toDataURL();
     var ele = $('#' + signature_pad_ids[i]);
     self.show_signature_image(ele, dataURL);
     }
     }*/

    if(download){
        var flag = self.validate_booking_form();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Please fill the fields marked in red');
            return false;
        }
    }

    if (self.booking_form_uuid == '') {
        var uuid = self.create_uuid();
        self.booking_form_uuid = uuid;
    }

    var formData = $('#solar_booking_form').serialize();
    formData += '&uuid=' + self.booking_form_uuid;
    formData += '&lead_id=' + self.lead_data.id;
    formData += '&site_id=' + self.site_id;

    $.ajax({
        url: base_url + 'admin/booking_form/save_solar_booking_form',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]('Saving Quote Data, Please Wait....');
            $("#save_booking_form").attr("disabled", "disabled");
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.booking_form_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/booking_form/edit_solar_booking_form/' + self.booking_form_uuid);
                }
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                }else{
                    toastr["success"](response.status);
                    setTimeout(function(){
                        window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                    },1500);
                }
            } else {
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                }else{
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_booking_form_manager.prototype.fetch_solar_booking_form_data = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/booking_form/fetch_solar_booking_form_data',
        datatype: 'json',
        data: {uuid: self.booking_form_uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var booking_data = stat.booking_data;
                self.postcode = booking_data.postcode;
                var products = JSON.parse(booking_data.product);
                for(var k in products){
                    console.log(k);
                    if(k == 'product_name'){
                        self.system_size = products[k][0];
                    }
                }
                self.handle_solar_booking_form_data(booking_data);
                if(booking_data.simpro_job_id != null && booking_data.simpro_job_id != ''){
                    $('.sr-deal_actions').hide();
                }else{
                    self.initialize();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_booking_form_manager.prototype.handle_solar_booking_form_data = function (data) {
    var self = this;
    //Handle Business Details
    var business_details = JSON.parse(data.business_details);
    for (var key in business_details) {
        if(key == 'leased_or_owned' || key == 'bca_hours'){
            $("#"+key).val(business_details[key]);
        }else if (key == 'is_firewall') {
            $('#is_firewall_' + business_details[key]).attr('checked', 'checked');
        }else if(key == 'network_type'){
            $('#network_type_' + business_details[key]).attr('checked', 'checked').trigger('change');
        }else if(key == 'vic_rebate'){
            $('#vic_rebate_' + business_details[key]).attr('checked', 'checked').trigger('change');
        }else if(key == 'vic_loan'){
            $('#vic_loan_' + business_details[key]).attr('checked', 'checked').trigger('change');
        }else{
            if($("#"+key)){
              $("#"+key).val(business_details[key]);  
            }else{
              $("input[name='business_details[" + key + "]']").val(business_details[key]);
            }
            
            if(key == 'address'){
                $('#select2-locationstreetAddress-container').html(business_details[key]);
            }
        }
    }

    //Handle Authorised Details
    var authorised_details = JSON.parse(data.authorised_details);
    for (var key in authorised_details) {
        if($("#"+key)){
            $("#"+key).val(authorised_details[key]);  
        }else{
            $("input[name='authorised_details[" + key + "]']").val(authorised_details[key]);
        }
    }

    //Handle Electricity Details
    var electricity_bill = JSON.parse(data.electricity_bill);
    for (var key in electricity_bill) {
        if($("#"+key)){
            $("#"+key).val(electricity_bill[key]);  
        }else{
            $("input[name='electricity_bill[" + key + "]']").val(electricity_bill[key]);
        }
    }


    //Handle Product
    var product = JSON.parse(data.product);
    for (var key in product) {
        for (var i = 0; i < product[key].length; i++) {

            if(key == 'product_name'){
                
                if(i == 0){
                    $("." + key + "_" + i).val(product[key][i]);
                }else{
                    $("." + key + "_" + i + ' option[value="'+product[key][i]+'"]').prop('selected',true).trigger('change');
                }
            }else{

                $("." + key + "_" + i).val(product[key][i]);
            }
        }
    }
    $('.product_qty').trigger('change');

    //Handle Booking Form Data
    var booking_form = JSON.parse(data.booking_form);
    for (var key in booking_form) {
        if (key == 'authorised_on_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'signature') {
                    if (booking_form[key]['signature'] != '') {
                        self.show_signature_image($('#authorised_on_behalf_signature'), booking_form[key]['signature']);
                    }
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'authorised_by_behalf') {

            for (var key1 in booking_form[key]) {
                if (key1 == 'sales_rep_signature') {
                    if (booking_form[key]['sales_rep_signature'] != '') {
                        self.show_signature_image($('#authorised_by_behalf_sales_rep_signature'), booking_form[key]['sales_rep_signature']);
                    }
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'signature') {
            if (booking_form[key] != '') {
                self.show_signature_image($('#signature'), booking_form[key]);
            }
        } else if (key == 'terms_and_conditions') {
            for (var i = 0; i < 4; i++) {
                if (booking_form[key][i] == 'on') {
                    $('#terms_and_conditions_' + i).attr('checked', 'checked');
                }
            }
        } else if (key == 'is_upfront_30' || key == 'is_upfront_99' || key == 'is_upfront_100' ||
                key == 'is_finance' ||
                key == 'is_energy_plan' || key == 'is_energy_plan_8' || key == 'is_energy_plan_12' || key == 'is_energy_plan_15') {
            $("#" + key).attr('checked', 'checked');
        } else if (key == 'is_forklift_on_site') {
            var is_checked = (booking_form[key] == '1') ? 'yes' : 'no';
            $("#" + key + '_' + is_checked).attr('checked', 'checked');
        } else {
            $("#" + key).val(booking_form[key]);
        }
    }

    //Handle images
    var booking_form_image = JSON.parse(data.booking_form_image);
    for (var key in booking_form_image) {
        if (booking_form_image[key] != '') {
            $("#" + key).val(booking_form_image[key]);
            self.show_image($("#" + key), booking_form_image[key]);
        }
    }
};

solar_booking_form_manager.prototype.save_booking_form_to_simpro = function () {
    var self = this;
    var url = base_url + 'admin/booking_form/solar_booking_form_pdf_download/' + self.booking_form_uuid;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]("Saving Data to Job CRM. Please Wait...");
            $("#save_booking_form").attr("disabled", "disabled");
            $("#generate_booking_form_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            } else {
                toastr["error"](response.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
            toastr.remove();
            //Getting this weird issue but still boking form was completed save on simpro 
            if(xhr.statusText == "OK"){
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function(){
                    window.location.href = base_url + 'admin/lead/edit/'+self.lead_data.uuid;
                },1500);
            }else{
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};


solar_booking_form_manager.prototype.resizeCanvas = function () {
    var self = this;

    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var signaturePad1 = self.signature_pad[0];
    var signaturePad2 = self.signature_pad[1];
    var signaturePad3 = self.signature_pad[2];

    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas1.width = canvas1.offsetWidth * ratio;
    canvas1.height = canvas1.offsetHeight * ratio;
    canvas1.getContext("2d").scale(ratio, ratio);
    signaturePad1.clear();

    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    signaturePad2.clear();

    canvas3.width = canvas3.offsetWidth * ratio;
    canvas3.height = canvas3.offsetHeight * ratio;
    canvas3.getContext("2d").scale(ratio, ratio);
    signaturePad3.clear();

};



solar_booking_form_manager.prototype.calculate_stc_rebate = function (is_save) {
    var self = this;
    var dt = new Date();
    //Changed from 12 to 11
    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 11;
    var system_size = self.system_size;

    var no_of_stc = parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size);
    no_of_stc = Math.floor(no_of_stc);

    var price_assumption = $('#stc_calculate_price').val();
    price_assumption = parseFloat(price_assumption).toFixed(2);

    if(self.stc_deduction > 0){
        price_assumption = price_assumption;
    }else{
        price_assumption = 35;
        $('#stc_calculate_price').val(price_assumption);
    }

    var stc_deduction = no_of_stc * parseFloat(price_assumption);
    stc_deduction = parseFloat(stc_deduction).toFixed(2);


    $('#stc').val(no_of_stc);
    if(stc_deduction > 0){
        $('#stc_deduction').val(stc_deduction);
        $('#stc_deduction_inGST').val(parseFloat(stc_deduction * self.gst).toFixed(2));
    }else{
        $('#stc_deduction').val(0);
        $('#stc_deduction_inGST').val(0);
    }
    
    //Modal Values
    $('#stc_calculate_stc').val(no_of_stc);
    $('#stc_calculate_rebate').val(stc_deduction);
    
    if(is_save){
        self.calculate_sum();
    }
};


solar_booking_form_manager.prototype.calculate_total_cost = function () {
    var self = this;
    var additional_margin = 0;
 
    //Get panel price
    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }
 
    //Get System Size
    var system_size = self.calculate_system_size();
    system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);
 
 
    var prd_panel_ips = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseInt(prd_panel_data.variant_ips) : 0;
    var prd_panel_amt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_amt) : 0;
 
    var prd_panel_price = 0;
    if (prd_panel_ips == 0) {
        prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_cp) : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_amt) : 0;
    }
 
    var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.capacity)) : 0;
 
    (prd_panel_data !== null && prd_panel_data !== undefined && (self.system_size == 0 || isNaN(self.system_size))) ? $('#prd_panel_help').slideDown() : $('#prd_panel_help').slideUp();
    var no_of_panels = (system_size * 1000) / prd_panel_size;
    self.no_of_panels = Math.round(no_of_panels);
    document.getElementById('no_of_panels').innerHTML = isFinite(Math.floor(no_of_panels)) ? "<i class='fa fa-info-circle'></i> " + self.no_of_panels + " panels will be required." : "";
    var final_panel_price = self.no_of_panels * prd_panel_price; //Multiply 1000 because system size is in w and panel in kW
    self.prd_panel_price = (final_panel_price && isFinite(final_panel_price)) ? parseFloat(final_panel_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(additional_margin) + (parseFloat(self.no_of_panels) * parseFloat(prd_panel_data.variant_am)) : parseFloat(0).toFixed(2);
 
    //Get Inverter Price
    var prd_inverter = document.getElementById('prd_inverter');
    var prd_inverter_data = null;
    if (prd_inverter.value != '' && prd_inverter.value != undefined) {
        prd_inverter_data = JSON.parse(prd_inverter.options[prd_inverter.selectedIndex].getAttribute('data-item'));
    }
    var prd_inverter_ips = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseInt(prd_inverter_data.variant_ips) : 0;
    var prd_inverter_amt = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_data.variant_amt) : 0;
    var prd_inverter_price = 0;
    if (prd_inverter_ips == 0) {
        prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? prd_inverter_data.variant_cp : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_amt) : 0;
    }
    self.prd_inverter_price = (prd_inverter_price && isFinite(prd_inverter_price)) ? parseFloat(prd_inverter_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_inverter_data.variant_am) : parseFloat(0).toFixed(2);
 
    //Get battery price
    var prd_battery = document.getElementById('prd_battery');
    var prd_battery_data = null;
    if (prd_battery.value != '' && prd_battery.value != undefined) {
        prd_battery_data = JSON.parse(prd_battery.options[prd_battery.selectedIndex].getAttribute('data-item'));
    }
    var prd_battery_ips = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseInt(prd_battery_data.variant_ips) : 0;
    var prd_battery_amt = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_data.variant_amt) : 0;
 
    var prd_battery_price = 0;
    if (prd_battery_ips == 0) {
        prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? prd_battery_data.variant_cp : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_amt) : 0;
    }
    self.prd_battery_price = (prd_battery_price && isFinite(prd_battery_price)) ? parseFloat(prd_battery_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_battery_data.variant_am) : parseFloat(0).toFixed(2);
 
    //Get Additional item_price
    var additional_cost_item = document.getElementsByName("additional_cost_item_total[]");
    var additional_cost_item_price = 0;
 
    for (var i = 0; i < additional_cost_item.length; i++) {
        var additional_item_data = additional_cost_item[i].getAttribute('data-item');
        additional_item_data = JSON.parse(additional_item_data);
        var additional_item_ips = (additional_item_data !== null && additional_item_data !== undefined) ? parseInt(additional_item_data.variant_ips) : 0;
        var additional_item_amt = (additional_item_data !== null && additional_item_data !== undefined) ? parseFloat(additional_item_data.variant_amt) : 0;
 
        if (additional_item_ips == 0) {
            additional_cost_item_price += parseFloat(additional_cost_item[i].value);
        } else {
            additional_cost_item_price += parseFloat(additional_item_amt);
        }
 
        additional_margin = (additional_cost_item[i] !== null && additional_cost_item[i] !== undefined) ? parseFloat(additional_margin) + parseFloat(additional_item_data.variant_am) : parseFloat(0).toFixed(2);
    }
 
    additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);
 
    self.additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);
    self.additional_margin = (additional_margin && additional_margin != 0 && isFinite(additional_margin)) ? parseFloat(additional_margin).toFixed(2) : parseFloat(0).toFixed(2);
 
    //Get Line item_price
    var line_item = document.getElementsByName("line_item_price[]");
    var line_item_price = 0;
 
    for (var i = 0; i < line_item.length; i++) {
        no_of_panels = (no_of_panels && no_of_panels != 0 && isFinite(no_of_panels)) ? self.no_of_panels : 0;
        var current_item_price = parseFloat(line_item[i].value);
        line_item_price += current_item_price;
    }
 
    line_item_price = (line_item_price && isFinite(line_item_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
    parseFloat(line_item_price).toFixed(2) : parseFloat(0).toFixed(2);
 
    self.line_item_price = parseFloat(line_item_price).toFixed(2);
 
    //Get Racking Price
    var racking_price = (self.prd_panel_price > 0 && isFinite(prd_panel_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
    parseFloat(self.racking_price) : parseFloat(0).toFixed(2);
 
    var kliplock_price = (no_of_panels > 0) ? 2 * self.no_of_panels * parseFloat(3.90).toFixed(2) : parseFloat(0).toFixed(2);
    self.kliplock_price = parseFloat(kliplock_price).toFixed(2);
 
    //Calcaulte System Total
    var system_total = parseFloat(self.prd_panel_price) + parseFloat(self.prd_inverter_price) +
    parseFloat(self.prd_battery_price) + parseFloat(racking_price);
 
    console.log("-----------------");
    console.log("Panels: " + self.prd_panel_price);
    console.log("Inverter: " + self.prd_inverter_price);
    console.log("Battery: " + self.prd_battery_price);
    console.log("Racking: " + racking_price);
    console.log("Additional: " + self.additional_cost_item_price);
 
    var total_cost_before_markup = parseFloat(system_total) + parseFloat(self.additional_cost_item_price) + parseFloat(self.line_item_price);
 
    //console.log(self.line_item_price);
    //console.log("Total Without Markup: " +total_cost_before_markup);
 
    var certificate_margin = parseFloat(self.company_markup_price);
 
    var company_markup = (total_cost_before_markup > 0 && isFinite(total_cost_before_markup) && certificate_margin > 0 && isFinite(certificate_margin)) ? ((total_cost_before_markup / certificate_margin) - total_cost_before_markup) : parseFloat(0).toFixed(2);
 
    company_markup = (self.additional_margin > 0 && isFinite(self.additional_margin)) ? parseFloat(company_markup) + parseFloat(self.additional_margin) : parseFloat(company_markup);
    //var company_markup = (self.system_size != 0 && !isNaN(self.system_size) && self.company_markup_price && isFinite(self.company_markup_price)) ? (parseFloat(self.company_markup_price) * no_of_panels) + parseFloat(self.additional_margin) : parseFloat(0).toFixed(2);
    self.company_markup = (company_markup && isFinite(company_markup)) ? parseFloat(company_markup).toFixed(3) : parseFloat(0).toFixed(2);
    var total_cost = parseFloat(total_cost_before_markup) + parseFloat(self.company_markup);
    self.total_cost = (total_cost && isFinite(total_cost)) ? (total_cost * 1.1).toFixed(2) : parseFloat(0).toFixed(2);

};


solar_booking_form_manager.prototype.fetch_line_item_data = function () {
    var self = this;

    //Quickly claculate no of panels that will be rquired.
    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        data = prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item');
        prd_panel_data = (typeof data == 'string') ? JSON.parse(data) : data;
    }

    var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.capacity)) : 0;
    //Get System Size
    var system_size = parseFloat(document.getElementById('system_size').value);
    system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);
    self.system_size = system_size;

    self.calculate_stc_rebate();
    var no_of_panels = (system_size * 1000) / prd_panel_size;
    //self.no_of_panels = Math.floor(no_of_panels);
    self.no_of_panels = Math.round(no_of_panels);
    console.log(no_of_panels);
    var no_of_panels = (self.no_of_panels > 0 && isFinite(self.no_of_panels)) ? self.no_of_panels : 0;
    if (no_of_panels > 0) {
        $.ajax({
            url: base_url + 'admin/service/fetch_line_item_and_markup',
            type: 'POST',
            data: {
                prd_panel_data: prd_panel_data,
                no_of_panels: no_of_panels,
                action: 'fetch_line_item_data'
            },
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    if (response.line_item_data.length > 0) {
                        self.line_item_data = response.line_item_data;
                    }
                    self.company_markup_price = parseFloat(0).toFixed(3);
                    if (response.company_markup.length > 0) {
                        //(Select the Company Markup that matches the state and number of panels) 
                        //and add the additional margins for all the products
                        var company_markup_price = parseFloat(response.company_markup[0].price).toFixed(3);
                        self.company_markup_price = company_markup_price;
                    }
                    self.calculate_sum();
                    //self.create_quote_total();
                } else {
                    toastr["error"]('Error! While fetching line items data.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};


solar_booking_form_manager.prototype.calculate_sum = function () {
    var self = this;

    self.calculate_stc_rebate();
    var system_total = 0;
    var total_payable = document.querySelector('#total_payable').value;
    var stc_deduction = document.querySelector('#stc_deduction').value;
    var vic_rebate = document.querySelector('#vic_rebate').value;
    var vic_loan = document.querySelector('#vic_loan').value;
    
    self.stc_deduction = (stc_deduction && isFinite(stc_deduction)) ? parseFloat(stc_deduction).toFixed(2) : 0;
    self.vic_rebate = (vic_rebate && isFinite(vic_rebate)) ? parseFloat(vic_rebate).toFixed(2) : 0;
    self.vic_loan = (vic_loan && isFinite(vic_loan)) ? parseFloat(vic_loan).toFixed(2) : 0;
    self.total_cost_incGst = (total_payable && isFinite(total_payable)) ? parseFloat(total_payable).toFixed(2) : 0;
    self.system_total = (parseFloat(self.total_cost_incGst) + (parseFloat(self.stc_deduction) + parseFloat(self.vic_rebate) + parseFloat(self.vic_loan)));
    self.system_total =  parseFloat(self.system_total).toFixed(2);
    
    $('#total_incGst').val(self.system_total);
    $('#stc_deduction').val(self.stc_deduction);
    $('#vic_rebate').val(self.vic_rebate);
    $('#vic_loan').val(self.vic_loan);
    $('#total_payable').val(self.total_cost_incGst);
};


solar_booking_form_manager.prototype.fetch_postcode_rating = function (postcode) {
    var self = this;
    
    console.log(postcode);
    $.ajax({
        url: base_url + 'admin/product/fetch_postcode',
        type: 'get',
        data: {
            postcode: postcode
        },
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                console.log(response.postcode);
                self.postcode_rating = response.postcode.rating;
                if (self.system_size < 100) {
                    //Changed from 12 to 11
                    var dt = new Date();
                    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 11;
                    var system_size = (self.system_size) ? self.system_size : 0;

                    var no_of_stc = parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size);
                    no_of_stc = Math.floor(no_of_stc);

                    var stc_deduction = self.stc_deduction;
                    stc_deduction = parseFloat(stc_deduction).toFixed(2);

                    var price_assumption = (stc_deduction / no_of_stc)
                    price_assumption = parseFloat(price_assumption).toFixed(2);

                    $('#stc_calculate_price').val(price_assumption);
                    self.calculate_sum();
                    self.calculate_stc_rebate();
                }
                if(response.solar_area != ''){
                    $('#sc_sb_area').val(response.solar_area);
                }
            } else {
                toastr["error"]('Error! While Fetching Postcode Rating.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
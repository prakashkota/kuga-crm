<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property TenderLead Controller
 * @version 1.0
 */
class TenderLead extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Notify");
        $this->load->model("User_Model", "user");
        $this->load->model("TenderLead_Model", "tender_lead");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Job_Model", "job");
        $this->load->model("Product_Model", "product");

        if (!$this->aauth->is_loggedin()) {
            // if ($this->input->is_ajax_request()) {
            //     echo json_encode(array(
            //         'success' => FALSE,
            //         'status' => 'Unauthorized Access. Looks like you are not logged in.',
            //         'authenticated' => FALSE
            //     ));
            //     die;
            // } else {
            //     redirect('admin/login');
            // }
        }
    }

    public function manage() {
        $data = array();
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['lead_stages'] = $this->common->fetch_orderBy('tbl_tender_lead_stages', '*',array('status' => 1), 'order_no', 'ASC', NULL);
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $view = $this->input->get('view');
        $view = (isset($view) && $view != '') ? $view : 'list';
        $this->load->view('tender-lead/manage_' . $view);
    }

    public function add($uuid = FALSE) {
        $data = array();
        //Check if Logged in user is Admin , Team Leader or Team Leader Rep
        $user_group = $this->aauth->get_user_groups()[0]->id;
        $user_id = $this->aauth->get_user_id();
        $data['user_id'] = $user_id = $this->aauth->get_user()->id;

        if ($uuid == FALSE) {
            $deal_ref = $this->input->get('deal_ref');
            if (isset($deal_ref) && $deal_ref != '') {
                $lead_data = $this->tender_lead->get_leads($deal_ref);
                if (empty($lead_data)) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Lead cant be initiated, no deal found with reference id.</div>');
                    redirect(site_url('admin/tender-lead/add'));
                }
                redirect(site_url('admin/tender-lead/edit/' . $lead_data['uuid']));
            }
            $states = $this->common->fetch_where('tbl_state', '*', NULL);
            $data['states'] = $states;
            $data['user_group'] = $user_group;
            //According to new flow will show only add deal foem with address
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('tender-lead/partials/tender_business_create_form');
            $this->load->view('partials/footer');
        } else {
            $lead_data = $this->tender_lead->get_leads($uuid);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> No lead found with referenced id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            $data['lead_data'] = $lead_data;
            $data['lead_id'] = $lead_data['id'];
            $data['uuid'] = $uuid;
            $data['user_id'] = $user_id;
            $data['user_group'] = $user_group;
            $data['product_types'] = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', NULL);
            $data['lead_stages'] = $this->common->fetch_orderBy('tbl_tender_lead_stages', '*',array('status' => 1), 'order_no', 'ASC', NULL);

            $states = $this->common->fetch_where('tbl_state', '*', NULL);
            $data['states'] = $states;
            $data['title'] = 'Add Deal';
            $data['activity_modal_title'] = 'Add/Schedule Activity';

            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('activity/activity_modal');
            $this->load->view('customer/booking_form_customer_site_modal_partial');
            $this->load->view('tender-lead/form');

            $this->load->view('tender-lead/partials/tender_builder_details_modal_form');
        }
    }
    
    public function fetch_lead_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_uuid = $this->input->get('uuid');
            $lead_uuid = (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : FALSE;
            $lead_data = $this->tender_lead->get_leads($lead_uuid);
            $data['lead_data'] = $lead_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function save_lead_details() {
        
        if (!empty($this->input->post())) {
            
            $user_group = $this->aauth->get_user_groups()[0]->id;
            $lead_data = $this->input->post('lead');
            $lead_data['uuid'] = $uuid = $this->input->post('uuid');
            
            $is_lead_exists = $this->common->fetch_where('tbl_tender_leads', '*', array('uuid' => $uuid));
            
            if (count($is_lead_exists) > 0) {
                $lead_data['updated_at'] = date('Y-m-d H:i:s');
               
                if((isset($lead_data['lead_stage']) && $lead_data['lead_stage'] != '' && $lead_data['lead_stage'] != 'undefined' && $lead_data['lead_stage'] != 0)){ 
                    $lead_data['prev_lead_stage'] = $is_lead_exists[0]['lead_stage'];
                }
                
                //So that the userid for lead created by will not change.
                if(isset($lead_data['created_by'])){
                    unset($lead_data['created_by']);
                }
                
                if((isset($lead_data['lead_type']) && $lead_data['lead_type'] != '' && $lead_data['lead_type'] != 'undefined')){ 
                    $lead_data['lead_type '] =  $lead_data['lead_type'] ;
                }
                
                if((isset($lead_data['closing_date']) && $lead_data['closing_date'] != '' && $lead_data['closing_date'] != 'undefined')){ 
                    $closing_date = str_replace('/','-',$lead_data['closing_date']);
                    $lead_data['closing_date'] = date('Y-m-d',strtotime($closing_date));
                }
                
                if((isset($lead_data['award_date']) && $lead_data['award_date'] != '' && $lead_data['award_date'] != 'undefined')){ 
                    $award_date = str_replace('/','-',$lead_data['award_date']);
                    $lead_data['award_date'] = date('Y-m-d',strtotime($award_date));
                }
                
                //Escape and Sanitize data before update
                $lead_data = $this->components->escape_data($lead_data);
                $stat = $this->common->update_data('tbl_tender_leads', array('uuid' => $is_lead_exists[0]['uuid']), $lead_data);
                $id = $is_lead_exists[0]['id'];
                /**
                //Assign Lead to Same User or Another User
                if ($id && $lead_to_user != '' && ($user_group == 1 || $user_group == 6 || $user_group == 7 )) {
                    $lead_user_update_data = array();
                    $lead_user_update_data['lead_id'] = $id;
                    $lead_user_update_data['user_id'] = $lead_to_user;
                    $this->common->insert_or_update('tbl_lead_to_user', array('lead_id' => $id), $lead_user_update_data);
                }*/
                
            } else {
                $lead_data['created_by'] = $this->aauth->get_user_id();

                if((isset($lead_data['lead_type']) && $lead_data['lead_type'] != '' && $lead_data['lead_type'] != 'undefined')){ 
                    $lead_data['lead_type '] =  $lead_data['lead_type'] ;
                }
                
                if((isset($lead_data['closing_date']) && $lead_data['closing_date'] != '' && $lead_data['closing_date'] != 'undefined')){ 
                    $closing_date = str_replace('/','-',$lead_data['closing_date']);
                    $lead_data['closing_date'] = date('Y-m-d',strtotime($closing_date));
                }
                
                if((isset($lead_data['award_date']) && $lead_data['award_date'] != '' && $lead_data['award_date'] != 'undefined')){ 
                    $award_date = str_replace('/','-',$lead_data['award_date']);
                    $lead_data['award_date'] = date('Y-m-d',strtotime($award_date));
                }
                
                
                //Escape and Sanitize data before insert
                $lead_data = $this->components->escape_data($lead_data);
                $stat = $this->common->insert_data('tbl_tender_leads', $lead_data);
                $id = $this->common->insert_id();
                /**
                //Assign Lead to User
                if ($id && $lead_to_user != '' && ($user_group == 1 || $user_group == 6 || $user_group == 7 )) {
                    $lead_user_insert_data = array();
                    $lead_user_insert_data['lead_id'] = $id;
                    $lead_user_insert_data['user_id'] = $lead_to_user;
                    $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
                }
                //If User is Lead Creator Only Then assign lead based on VIC and NSW and Send Emails
                if($user_group == 8){
                    $this->handle_lead_creator_assign_lead_to_user($id);
                }
                */
            }
            if ($stat) {
                $data['status'] = 'Tender Deal Saved Successfully';
                $data['id'] = $id;
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something wnet wrong while saving the data.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    public function fetch_lead_stage_data() {
        $data = array();
        //Pagination Params
        $limit = $this->input->get('limit');
        $limit = (isset($limit) && $limit != '') ? (int) $limit : 300;
        $page = $this->input->get('page');
        $page = (isset($page) && $page != '') ? $page : 1;
        $offset = ($page - 1) * $limit;
        //Pagination End
        
        $stage_id = $this->input->get('stage_id');
        $stage_id = (isset($stage_id) && $stage_id != '') ? $stage_id : FALSE;
        
        $filters = $this->handle_lead_data_filters();
        $is_filter = ($filters != '') ? TRUE : FALSE;
        
        $view = $this->input->get('view');
        $view = (isset($view) && $view != '') ? $view : 'list';
        $user_id = $this->aauth->get_user_id();
        $custom_filter_params = $this->input->get('custom');
        $user_cond = "";

        //Having Filter
        $having = FALSE;
        
        if($page == 1){
            //Unset Lead Total Count
            $this->session->unset_userdata('kg_lead_total_count');
            $this->session->unset_userdata('kg_map_lead_total_count');
        }

        if ($view == 'list') {
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_project = 'leads.project_name';
                $filters = (isset($keyword) && $keyword != '') ?
                        " AND  $search_by_project LIKE '%$keyword'" . $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }

            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->tender_lead->get_leads_data_by_stage_count_for_pipeline($stage_id, $filters, $having);
            $this->session->set_userdata('kg_lead_total_count',$total);

            $data['lead_data'] = $this->tender_lead->get_leads_data_by_stage_for_pipeline($stage_id, $filters, $having, $offset, $limit, $is_filter,$user_id);
            if ((count($data['lead_data']) < 100) && ($total < 100)) {
                //So if Total leads with first page is less than 100 than bring all the data without limit.
                $offset = $limit = FALSE;
                $data['lead_data'] = $this->tender_lead->get_leads_data_by_stage_for_pipeline($stage_id, $filters, $having, $offset, $limit, $is_filter,$user_id);
            }

            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        } else if ($view == 'map') {
            //Get Logged in user Coordiantes we set on after login
            $coordinates = $this->session->userdata('coordinates');
            $logged_in_user_state = $this->session->userdata('logged_in_user_state');
            $this->session->unset_userdata('logged_in_user_state');
            if (!empty($coordinates) && ($logged_in_user_state == '' || $logged_in_user_state == NULL)) {
                if ($coordinates['lat'] != NULL) {
                    $c_lead_data = $this->tender_lead->get_state_by_user_current_latlng($coordinates['lat'], $coordinates['lng']);
                    if (!empty($c_lead_data)) {
                        $this->session->set_userdata('logged_in_user_state', $c_lead_data[0]['state_id']);
                        $logged_in_user_state = $c_lead_data[0]['state_id'];
                    }
                }
            }

            $cond = FALSE;
            $limit = $this->input->get('limit');
            $limit = (isset($limit) && $limit != '') ? (int) $limit : 300;
            $page = $this->input->get('page');
            $page = (isset($page) && $page != '') ? $page : 1;
            $offset = ($page - 1) * $limit;
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $filter_by_type = $this->input->get('filter_by_type');
            $filter_by_stage = $this->input->get('filter_by_stage');
            $filter_by_segment = $this->input->get('filter_by_segment');
            $filter_by_user = $this->input->get('filter_by_user');
            $lat = $this->input->get('lat');
            $lng = $this->input->get('lng');

            
            if (isset($filter_by_type) && $filter_by_type != '' && $filter_by_type != 'null' && $filter_by_type != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_type IN ($filter_by_type)";
                $is_filter = TRUE;
            }
            
            if (isset($filter_by_stage) && $filter_by_stage != '' && $filter_by_stage != 'null' && $filter_by_stage != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_stage IN ($filter_by_stage)";
                $is_filter = TRUE;
            }
            
            if (isset($filter_by_segment) && $filter_by_segment != '' && $filter_by_segment != 'null' && $filter_by_segment != 'undefined') {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND leads.lead_segment IN ($filter_by_segment)";
                $is_filter = TRUE;
            }
            
            if (isset($filter_by_user) && $filter_by_user != '' && $filter_by_user != 'null' && $filter_by_user != 'undefined') {
                $sales_rep_users = [];
                $la_users = [];

                $filter_by_user = explode(',',$filter_by_user);
                foreach($filter_by_user as $key => $value){
                    $filter_by_user_type = $this->common->fetch_cell('aauth_user_to_group','group_id',array('user_id' => $value));
                    if($filter_by_user_type == 6 || $filter_by_user_type == 7){
                        $sales_rep_users[] = $value;
                    }else{
                        $la_users[] = $value;
                    }
                }

                $cond_col = '';
                if(!empty($sales_rep_users)){
                    $sales_rep_users = implode(',', $sales_rep_users);
                    $sales_rep_users = (isset($sales_rep_users) && $sales_rep_users != '') ?  $sales_rep_users : '';
                    $cond_col .= ' AND ltu.user_id IN ('.$sales_rep_users.')';
                }

                if(!empty($la_users)){
                    $la_users = implode(',', $la_users);
                    $la_users = (isset($la_users) && $la_users != '') ? $la_users : '';
                    $cond_col .= ' AND leads.user_id IN ('.$la_users.')';
                }

                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= $cond_col;
                //echo $cond;die;
                $is_filter = TRUE;
            }
            
            if (isset($keyword) && $keyword != '' && $keyword != 'null' && $keyword != 'undefined') {
                $search_by = $this->input->get('search_by');
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $original_keyword = $keyword;
                $keyword_single_escape = str_replace("'", "\'", $keyword);
                $keyword = preg_replace("/'/", "\\\\\\\\\\'", $keyword);
                $cond .= (isset($keyword) && $keyword != '') ? ' AND '.$search_by.' LIKE "%'.$original_keyword.'%" OR '.$search_by.'="'.$keyword.'" OR '.$search_by.'="'.$keyword_single_escape.'"' : '';
                    $is_filter = TRUE;
                //echo $cond;die;
            }

            if (isset($lat) && $lat != '' && $lat != 'null' && $lat != 'undefined') {
                $this->fetch_nearby_leads($offset, $limit, $cond);
            }

            if ($is_filter == FALSE && isset($logged_in_user_state) && $logged_in_user_state != NULL) {
                $cond = ($cond != FALSE) ? $cond : ' WHERE 1=1';
                $cond .= " AND cl.state_id IN ($logged_in_user_state)";
            }
            
            if($cond == FALSE){
                $cond = ' WHERE 1=1 ' . $filters;
            }
            
           //echo $cond;die;
            //Store latest search criteria
            if ($cond != FALSE) {
                $search_logs_data['user_id'] = $user_id;
                $search_logs_data['data'] = json_encode($this->input->get());
                $this->common->insert_data('tbl_lead_map_search_logs', $search_logs_data);
            }
            
            
            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->tender_lead->get_leads_count($cond);
            $this->session->set_userdata('kg_map_lead_total_count',$total);
            
            if($total < 200){
                $limit = FALSE;    
            }
            
            $data['lead_data'] = $this->tender_lead->get_leads_data_for_map($offset, $limit, $cond, $is_filter);
            //echo $this->db->last_query();die;
            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
            
        } else if ($view == 'calendar') {
            $start_date = $this->input->get('start_date');
            $end_date = $this->input->get('end_date');
            $filters = ' WHERE DATE(created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) {
                $filters = $filters . ' AND user_id IN(" . $user_id . ")';
            }
            $data['lead_data'] = $this->tender_lead->get_leads_data_for_calendar($offset, $limit, $filters, $is_filter);
        } else {
            $limit = $this->input->get('limit');
            $limit = (isset($limit) && $limit != '') ? (int) $limit : 20;
            $page = $this->input->get('page');
            $page = (isset($page) && $page != '') ? $page : 1;
            $offset = ($page - 1) * $limit;
            
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_company = 'leads.project_name';
                $search_by_fname = 'cust.first_name';
                $search_by_lname = 'cust.last_name';
                $search_by_phone = 'cust.customer_contact_no';
                $search_by_email = 'cust.customer_email';
                $filters = (isset($keyword) && $keyword != '') ?
                        " AND $search_by_company LIKE '%$keyword'" . $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }
            
            $lead_total_count = $this->session->userdata('kg_lead_total_count');
            $total = (isset($lead_total_count) && $lead_total_count != '') ? $lead_total_count : $this->tender_lead->get_leads_data_by_stage_count_for_pipeline($stage_id, $filters, $having);
            $this->session->set_userdata('kg_lead_total_count',$total);

            if (isset($page) && $page == 1) {
                $leads_stages = $this->common->fetch_orderBy('tbl_tender_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
                $i = 0;
                foreach ($leads_stages as $key => $value) {
                    $leads_stages[$key]['count'] = $this->tender_lead->get_leads_data_by_stage_count_for_pipeline($value['id'], $filters, $having);
                    $proposal_data_by_stages[($i++)] = $this->tender_lead->get_leads_data_by_stage_for_pipeline($value['id'], $filters, $having, $offset, $limit, $is_filter,$user_id);
                }
                $data['proposal_stages'] = $leads_stages;
                $data['proposal_data_by_stages'] = $proposal_data_by_stages;
            } else {
                $proposal_data_by_stages = $this->tender_lead->get_leads_data_by_stage_for_pipeline(FALSE, $filters, $having, $offset, $limit, $is_filter,$user_id);
                $data['proposal_data_by_stages'] = $proposal_data_by_stages;
            }

            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        }
        $data['success'] = TRUE;
        echo json_encode($data);
        die;
    }

    private function handle_lead_data_filters() {
        $user_id = $this->aauth->get_user_id();
        $parsed_filter_options = '';
        $filter = $this->input->get('filter');
        $is_filter_sales_user = FALSE;
        $is_filter_la_user = FALSE;
        $is_source_make_it_cheaper = FALSE;
        $view = $this->input->get('view');
        
        if (isset($filter) && !empty($filter)) {
            if (isset($filter['start_date']) && $filter['start_date'] != '') {
                $start_date = $filter['start_date'];
                $end_date = $filter['end_date'];
                
                if(isset($view) && $view == 'pipeline'){
                    if($start_date != '2016-01-01'){
                        $parsed_filter_options .= ' AND DATE(activity.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
                    }
                }else{
                    $parsed_filter_options .= ' AND DATE(leads.created_at) BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
                }
            }
            if (isset($filter['custom']) && !empty($filter['custom'])) {
                foreach ($filter['custom'] as $key => $value) {
                    switch ($key) {
                        case 'userid':
                            if ($value == '') {
                                $is_filter_sales_user = TRUE;
                            } else if ($value != 'All') {
                                $is_filter_sales_user = TRUE;
                                $parsed_filter_options .= " AND ltu.user_id IN(" . $value . ")";
                            } else if ($value == 'All') {
                                $is_filter_sales_user = TRUE;
                                $users = $this->user->get_users(7, FALSE, FALSE);
                                $all_users = array_column($users, 'user_id');
                                $all_users = implode(',', $all_users);
                                $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
                                $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id . $all_users . ")";
                            }
                            break;
                        case 'la_userid':
                            if ($key == 'la_userid') {
                                $is_filter_la_user = TRUE;
                                if ($value == '') {
                                    $is_filter_la_user = TRUE;
                                } else if ($value != 'All') {
                                    $is_filter_la_user = TRUE;
                                    $parsed_filter_options .= " AND leads.user_id IN(" . $value . ")";
                                } else if ($value == 'All') {
                                    $is_filter_la_user = TRUE;
                                    $users = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                                    $all_users = array_column($users, 'user_id');
                                    $all_users = implode(',', $all_users);
                                    $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
                                    $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . $all_users . ")";
                                }
                            }
                            break;
                        case 'state_id':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND cl.' . $key . '=' . $value;
                            }
                            break;
                        case 'lead_type':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '=' . $value;
                            }
                            break;
                        case 'lead_source':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '="' . $value .'"';
                                if($value == 'Make it Cheaper'){
                                    $is_source_make_it_cheaper = TRUE;
                                }
                            }
                            break;
                        case 'lead_stage':
                            if ($value != '') {
                                $parsed_filter_options .= ' AND leads.' . $key . '="' . $value . '"';
                            }
                            break;
                        default:
                            if ($value != '') {
                                $parsed_filter_options .= ' AND ' . $key . '=' . $value;
                            }
                            break;
                    }
                }
            }
        }

        if ($this->components->is_team_leader()) {
            $make_it_cheaper_profile = 23;
            if ($this->components->is_lead_allocation_team_leader() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                //For Make it cheaper profile
                if (!$is_source_make_it_cheaper) {
                    $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id .")";
                }
                
            }
        } else if ($this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                $parsed_filter_options .= " AND ltu.user_id IN(" . $user_id . ")";
            }
        }

        return $parsed_filter_options;
    }
    
    public function update_lead_stage() {
        if (!empty($this->input->post())) {
            $uuid = $this->input->post('uuid');
            $update = array();
            $deal_stage = $this->input->post('deal_stage');
            $update['lead_stage'] = (isset($deal_stage) && $deal_stage != '') ? $deal_stage : '';
            $update['updated_at'] = date('Y-m-d H:i:s');
            $stat = $this->common->update_data('tbl_tender_leads', ['uuid' => $uuid], $update);
            if ($stat) {
                $data['status'] = 'Deal information updated successfully';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    public function save_tender_lead_contact() {
        $data = array();
        $contact_data = $this->input->post('company');
        if (!empty($this->input->post())) {
            $contact_id = $this->input->post('contact_id');
            if (isset($contact_id) && $contact_id != '') {
                //Update Contact
                $stat = $this->common->update_data('tbl_tender_lead_contacts', ['id' => $contact_id], $contact_data);
            } else {
                //Insert Contact
                $stat = $this->common->insert_data('tbl_tender_lead_contacts', $contact_data);
                $contact_id = $this->common->insert_id();
            }
            if ($stat) {
                $data['success'] = TRUE;
                $data['status'] = 'Tender Contact Saved Successfully.';
                $data['cust_contact_id'] = $contact_id;
                $data['contact'] = $this->common->fetch_where('tbl_tender_lead_contacts','*',array('lead_id' => $contact_data['lead_id']));
                ;
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Nothing to update.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function fetch_tender_lead_contacts() {
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $contacts = $this->common->fetch_where('tbl_tender_lead_contacts','*',array('lead_id' => $lead_id));
            $data['success'] = TRUE;
            $data['contacts'] = $contacts;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }
    

    public function save_quote_details(){

        $data = array();
        if (!empty($this->input->post())) {
            
            $lead_id = $this->input->post('lead_id');
            $cust_id = $this->input->post('cust_id');
            $uuid = $this->input->post('uuid');
            $quote_data = json_encode($this->input->post('quote'));

            $quote_id = $this->input->post('quote_id');

            $quote_data = [
                'uuid' => $uuid,
                'lead_id' => $lead_id,
                'cust_id' => $cust_id,
                'user_id' => $this->aauth->get_user_id(),
                'quote_data' => $quote_data,
            ];

            if(!empty($quote_id)){
                $quote_data['updated_at'] = date('Y-m-d H:i:s');
                $cond = "quote_id = $quote_id";
                $stat = $this->common->insert_or_update('tbl_tender_quotes',$cond, $quote_data);
            }else{
                $quote_data['created_at'] = date('Y-m-d H:i:s');
                $stat = $this->common->insert_data('tbl_tender_quotes', $quote_data);
            }

            if($stat){
                $data['status'] = 'Quote Saved successfully';
                $data['success'] = TRUE;
            }else{
                $data['status'] = 'Somthing went wrong! Please try again';
                $data['success'] = FALSE;
            }
        }else{
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }


    public function quote_pdf_download($uuid){

        $quotes = $this->tender_lead->get_quote_by_uuid($uuid);

        if(!empty($quotes)){
            $prepared_email = $this->common->fetch_row('aauth_users', 'email', array('id' => $quotes['user_id']));
            $prepared_data = $this->common->fetch_row('tbl_user_details', 'full_name, company_contact_no', array('user_id' => $quotes['user_id']));

            $prepared_by = [
                'name' => $prepared_data['full_name'],
                'email' => $prepared_email['email'],
                'phone' => $prepared_data['company_contact_no']
            ];

            $customer = $this->common->fetch_row('tbl_tender_lead_contacts','*',['id' => $quotes['cust_id']]);
            
            $lead = $this->common->fetch_row('tbl_tender_leads','project_name',['id' => $quotes['lead_id']]);
            $data['quote'] = $quotes;
            $data['prepared_by'] = $prepared_by;
            $data['customer'] = $customer;
            $data['project_name'] = $lead['project_name'];
            
            $data['products'] = json_decode($quotes['quote_data'], TRUE);
            // echo "<pre>";
            // print_r($data);
            // die;
            
            $html = $this->load->view('pdf_files/tender_quotes/main', $data, TRUE);
            $html .= $this->load->view('pdf_files/tender_quotes/page0', $data, TRUE);
            $html .= $this->load->view('pdf_files/tender_quotes/page1', $data, TRUE);
            
            
            $customer_name = $prepared_data['full_name'];
            $filename      = "Kuga_Electrical-Tender_Quote_Form_" . $customer_name . "-" . date('Y-m-d h:i:s');
            $filename      = str_replace(array('.', ',', ' '), '_', $filename);
            $h             = '';
            $f             = '';
            $pdf_options   = array(
                "source_type" => 'html',
                "source"      => $html,
                "action"      => 'download',
                "file_name"   => $filename . '.pdf',
                "page_size"   => 'A4',
                "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                "header"      => $h,
                "footer"      => $f);
            $this->pdf->phptopdf($pdf_options);
        }else{
            show_404();
        }
    }


    public function fetch_quotes(){
        $data = array();

        $lead_id = $this->input->post('lead_id');

        if(!empty($lead_id) && $lead_id != ''){
            $cond = "lead_id = '$lead_id'";
            $quotes = $this->common->fetch_where('tbl_tender_quotes','uuid, quote_id, created_at',$cond);

            if($quotes){
                $data['status'] = 'Quotes Found';
                $data['success'] = TRUE;
                $data['quotes'] = $quotes;
            }else{
                $data['status'] = 'Invalid Request';
                $data['success'] = FALSE;
            }

        }else{
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }


    public function fetch_quotes_details(){
        $data = array();

        $uuid = $this->input->post('uuid');

        if(!empty($uuid) && $uuid != ''){
            $cond = "uuid = '$uuid'";
            $quotes = $this->common->fetch_row('tbl_tender_quotes','*',$cond);

            if($quotes){
                $data['status'] = 'Quotes Found';
                $data['success'] = TRUE;
                $data['quote_detail'] = $quotes;
            }else{
                $data['status'] = 'Invalid Request';
                $data['success'] = FALSE;
            }

        }else{
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }
    
    
    public function fetch_lead_form_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $lead_id = $this->input->get('lead_id');
            $booking_form_data = $this->tender_lead->get_lead_booking_forms_data($lead_id);
            
            $job_card_data = $this->common->fetch_where('tbl_tender_job_card_form', '*', array('lead_id' => $lead_id));
            $data['booking_form_data'] = $booking_form_data;
            $data['job_card_data'] = $job_card_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    /** SOLAR Booking FORM */
    public function manage_solar_booking_form($uuid = FALSE) {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        $cost_centre_id = $this->input->get('cost_centre_id');
    
        $data['deal_ref'] = $deal_ref;
        $data['cost_centre_id'] = $cost_centre_id;
        if (isset($deal_ref) && $deal_ref != '') {
            $lead_data = $this->tender_lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('tender_booking_forms/solar/main');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/tender-lead/add'));
        }
    }
    
    /** Residential Solar Booking FORM */
    public function manage_residential_solar_booking_form($uuid = FALSE) {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        $cost_centre_id = $this->input->get('cost_centre_id');
    
        $data['deal_ref'] = $deal_ref;
        $data['cost_centre_id'] = $cost_centre_id;
        if (isset($deal_ref) && $deal_ref != '' && isset($site_ref) && $site_ref != '') {
            $lead_data = $this->tender_lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('tender_booking_forms/solar/residentialMain');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/tender-lead/add'));
        }
    }
    
    public function add_solar_booking_form($uuid = FALSE) {
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        $type_ref = $this->input->get('type_ref');
        $cost_centre_id = $this->input->get('cost_centre_id');
    
        if ($this->aauth->is_member('Admin')) {
            $data['users'] =  $this->user->get_users(FALSE, FALSE, FALSE);
        } else if ($this->components->is_team_leader()) {
            $owner_id = $this->aauth->get_user_id();
            $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
                //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
            $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
                //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
        }
        if (isset($deal_ref) && $deal_ref != '' && isset($type_ref) && $type_ref < 5) {
            $lead_data = $this->tender_lead->get_leads($deal_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
    
            $data['lead_uuid'] = $lead_data['uuid'];
            $data['lead_id'] = $lead_data['id'];
            $data['type_id'] = $type_ref;
            $data['cost_centre_id'] = $cost_centre_id;
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            if($type_ref == 4){			
    			$this->load->view('tender_booking_forms/solar/1/residential_solar/main');
    		}else{
    			$this->load->view('tender_booking_forms/solar/1/main');
    		}
        } else if ($uuid) {
            $booking_data = $this->common->fetch_row('tbl_tender_solar_booking_form', '*', array('uuid' => $uuid));
            if (empty($booking_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            $lead_uuid = $this->common->fetch_cell('tbl_leads','uuid',array('id' => $booking_data['lead_id']));
            $data['lead_uuid'] = $lead_uuid;
            $data['booking_form_uuid'] = $uuid;
            $data['type_id'] = $booking_data['type_id'];
            $data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            if($data['type_id'] == 4){			
    			$this->load->view('tender_booking_forms/solar/1/residential_solar/main');
    		}else{
    			$this->load->view('tender_booking_forms/solar/1/main');
    		}
        } else {
            redirect('admin/dashboard');
        }
    }
    
    public function save_solar_booking_form($uuid = FALSE) {
        $data = array();
        if (!empty($this->input->post())) {
            $is_booking_exists = $this->common->fetch_row('tbl_tender_solar_booking_form', '*', array('uuid' => $this->input->post('uuid')));
            $form_data = array();
    		$business_details = $this->input->post('business_details');
            $authorised_details = $this->input->post('authorised_details');
            if(!isset($business_details['entity_name'])){
                $business_details['entity_name'] = $authorised_details['first_name'] . ' ' . $authorised_details['last_name'];
            }
            if(!isset($business_details['abn'])){
                $business_details['abn'] = '';
            }
            if(!isset($business_details['trading_name'])){
                $business_details['trading_name'] = '';
            }
            if(!isset($business_details['leased_or_owned'])){
                $business_details['leased_or_owned'] = 'Owner';
            }
            if(!isset($business_details['address'])){
                $business_details['address'] = $authorised_details['address'];
            }
            
            $form_data['business_details'] = json_encode($business_details);
            $form_data['authorised_details'] = json_encode($authorised_details);
            $form_data['electricity_bill'] = json_encode($this->input->post('electricity_bill'));
            $form_data['product'] = json_encode($this->input->post('product'));
            $form_data['booking_form_image'] = json_encode($this->input->post('booking_form_image'));
    
            //Convert Base64 signatures to png
            $booking_form = $this->input->post('booking_form');
            if (isset($booking_form['authorised_on_behalf']['signature'])) {
                $img = explode(';', $booking_form['authorised_on_behalf']['signature']);
                if ($booking_form['authorised_on_behalf']['signature'] != '' && count($img) > 1) {
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_on_behalf']['signature']));
                    $newfilename = $this->components->uuid() . '.png';
                    file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename, $image);
                    $booking_form['authorised_on_behalf']['signature'] = $newfilename;
                }
            }
    
            if (isset($booking_form['authorised_by_behalf']['sales_rep_signature'])) {
                $img = explode(';', $booking_form['authorised_by_behalf']['sales_rep_signature']);
                if ($booking_form['authorised_by_behalf']['sales_rep_signature'] != '' && count($img) > 1) {
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_by_behalf']['sales_rep_signature']));
                    $newfilename1 = $this->components->uuid() . '.png';
                    file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename1, $image);
                    $booking_form['authorised_by_behalf']['sales_rep_signature'] = $newfilename1;
                }
            }
    
            if (isset($booking_form['signature'])) {
                $img = explode(';', $booking_form['signature']);
                if ($booking_form['signature'] != '' && count($img) > 1) {
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['signature']));
                    $newfilename2 = $this->components->uuid() . '.png';
                    file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename2, $image);
                    $booking_form['signature'] = $newfilename2;
                }
            }
    
            $form_data['booking_form'] = json_encode($booking_form);
            $form_data['cost_centre_id'] = $this->input->post('cost_centre_id');
    
            if (empty($is_booking_exists)) {
                $insert_data = array();
                $insert_data = $form_data;
                $insert_data['uuid'] = $this->input->post('uuid');
                $insert_data['lead_id'] = $this->input->post('lead_id');
                $insert_data['type_id'] = $this->input->post('type_id');
                $insert_data['user_id'] = $this->aauth->get_user_id();
                $stat = $this->common->insert_data('tbl_tender_solar_booking_form', $insert_data);
                $quote_id = $this->common->insert_id();
            } else {
                $booking_id = $is_booking_exists['id'];
                //print_r($form_data);die;
                $stat = $this->common->update_data('tbl_tender_solar_booking_form', array('id' => $booking_id), $form_data);
            }
            if ($stat) {
                $data['status'] = 'Booking Form Data Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    public function fetch_solar_booking_form_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $booking_data = $this->common->fetch_row('tbl_tender_solar_booking_form', '*', array('uuid' => $this->input->get('uuid')));
            $data['booking_data'] = $booking_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    public function solar_booking_form_pdf_download($uuid) {
        $data = array();
        $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
        $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();
    
        $booking_data = $this->common->fetch_row('tbl_tender_solar_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/tender-lead/add'));
        }
        $lead_data = $this->tender_lead->get_leads($booking_data['lead_id']);
        
        $view = $this->input->get('view');
        try {
    
            $form_data = array();
            $form_data['type_id'] = $booking_data['type_id'];
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            
    		if(isset($view) && $view == 'final_html'){
    		    if($form_data['type_id'] == 4){			
    			    $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/main', $form_data, TRUE); //residential booking form
        		}else{
        			$html = $this->load->view('pdf_files/solar_booking_form_v1/main', $form_data, TRUE);
        		}
        		echo $html;die;
            }else if(isset($view) && $view == 'page9'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/page9', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v1/page9', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'page10'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/page10', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v1/page10', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'final_pdf'){
                $filename = md5(time());
                $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                exec($cmd);
            }else {
                if($form_data['type_id'] == 4){
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                }else{
                    //Page 9
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page9.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page9 '. $upload_path_img;
                    exec($cmd);
                    
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page10.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page10 '. $upload_path_img;
                    exec($cmd);
                    
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                }
            }
            
            
            //Decode Data
            $business_details = json_decode($booking_data['business_details']);
            $authorised_details = json_decode($booking_data['authorised_details']);
            $electricity_bill = json_decode($booking_data['electricity_bill']);
            $product = json_decode($booking_data['product']);
            $booking_form = json_decode($booking_data['booking_form']);
            $booking_form_image = json_decode($booking_data['booking_form_image']);
    
    
            //Get File
            $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
            $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
            } else {
                $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
            }
    
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
            if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
            
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_tender_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
    
            //Save to Job CRM
            $job_crm_data = [];
            $job_crm_data['kuga_job_id'] = $booking_data['kuga_job_id'];
            $job_crm_data['booking_form_id'] = $booking_data['id'];
            $job_crm_data['booking_form_site_id'] = isset($booking_data['site_id']) ? $booking_data['site_id'] : NULL;
            $job_crm_data['business_details'] = $business_details;
            $job_crm_data['authorised_details'] = $authorised_details;
            $job_crm_data['booking_form'] = $booking_form;
            $job_crm_data['booking_form_image'] = $booking_form_image;
            $job_crm_data['filename'] = $filename;
            $job_crm_data['user_id'] = $lead_data['user_id'];
            $job_crm_data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $job_crm_data['electricity_bill'] = $electricity_bill;
            $job_id = $this->create_job_in_job_crm('Solar',$lead_data['cust_id'],$job_crm_data);
            if($job_id != null){
                $this->common->update_data('tbl_tender_solar_booking_form', array('uuid' => $uuid), array('kuga_job_id' => $job_id));
                $custom_fields = array();
                $kW = '';
                if(!empty($product) && isset($product->product_name[0]) && $product->product_name[0] != ''){
                    $kW = preg_replace("/[^0-9.]/","",$product->product_name[0]);
                }
                if($kW != '') {
                    $custom_fields[2] = $kW;
                }
                $this->create_job_custom_fields_in_job_crm($job_id,$custom_fields);
    
                //Send Success Mail
                $customer_payload['CompanyName'] = $business_details->entity_name;
                $customer_payload['Phone'] = $authorised_details->contact_no;
                $customer_payload['Email'] = $authorised_details->email;
                    
                $user_id = $this->aauth->get_user_id();
                $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
                $owner_data = $this->user->get_users(FALSE,$owner_id,FALSE,FALSE);
                $sales_rep_data = $this->user->get_users(FALSE,$user_id,FALSE,FALSE);
                $communication_data = array();
                $communication_data['customer'] = $customer_payload;
                $communication_data['type'] = 'Solar';
                $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"]. '/assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
                $to_sales_rep = $bcc_team_leader = '';
                if(!empty($owner_data)){
                  $communication_data['team_leader']['name'] = $owner_data[0]['full_name'];
                  $communication_data['team_leader']['email'] = $bcc_team_leader = $owner_data[0]['email'];
                }
                if(!empty($sales_rep_data)){
                    $communication_data['sales_rep']['name'] =  $sales_rep_data[0]['full_name'];
                    $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
                    //$this->email_manager->send_booking_form_success_email($communication_data,$to_sales_rep,$bcc_team_leader);
                }
                $communication_data['cost_centre_id'] = $booking_data['cost_centre_id'];
                $communication_data['job_id'] = $job_id;
                $this->add_booking_form_mail_to_queue($communication_data,1);
                $data['success'] = TRUE;
                $data['status'] = 'Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.';
            }

        } catch (ErrorException $e) {
            $status = $e->getMessage();
            $data['success'] = FALSE;
            $data['status'] = $status;
        } catch (Exception $e) {
            $response = $e->getResponse();
            $error = $response->getBody()->getContents();
            $error = json_decode($error, true);
            $status = $e->getMessage();
            if (!empty($error['errors'])) {
                $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
                $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
                $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
            }
            $data['success'] = FALSE;
            $data['status'] = $status;
            echo json_encode($data);
            die;
        }

        echo json_encode($data);
        die;
    }
    
    
    public function solar_booking_form_generate_draft_pdf($uuid) {
        $data = array();
        $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
        $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();
    
        $booking_data = $this->common->fetch_row('tbl_tender_solar_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/tender-lead/add'));
        }
        
        $lead_data = $this->tender_lead->get_leads($booking_data['lead_id']);
        
        $view = $this->input->get('view');
        try {
    
            $form_data = array();
            $form_data['uuid'] = $booking_data['uuid'];
            $form_data['type_id'] = $booking_data['type_id'];
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            
            
            if(isset($view) && $view == 'final_html'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/main', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v1/main', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'page9'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/page9', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v1/page9', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'page10'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v1/residential_solar/page10', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v1/page10', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'final_pdf'){
                $filename = md5(time());
                $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                exec($cmd);
            }else {
                
                if($form_data['type_id'] == 4){
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                }else{
                    //Page 9
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page9.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page9 '. $upload_path_img;
                    exec($cmd);
                    
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page10.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page10 '. $upload_path_img;
                    exec($cmd);
                    
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                }
            }
            
    
            //Decode Data
            $business_details = json_decode($booking_data['business_details']);
            $authorised_details = json_decode($booking_data['authorised_details']);
            $electricity_bill = json_decode($booking_data['electricity_bill']);
            $product = json_decode($booking_data['product']);
            $booking_form = json_decode($booking_data['booking_form']);
            $booking_form_image = json_decode($booking_data['booking_form_image']);
    
    
            //Get File
            $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
            $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . $lead_data['project_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
            } else {
                $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
            }
    
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
    
    
            if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
    
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_tender_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
            redirect(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf'));
            exit();
            
            
        }catch(Exception $e){
                $data['status'] = 'Looks like some issue in generating pdf. Error:'.json_encode($e->getMessage());
                $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    
    public function add_booking_form_mail_to_queue($communication_data,$type = 1){
        if($_SERVER['REMOTE_ADDR'] == '111.93.41.194' || $_SERVER['REMOTE_ADDR'] == '103.121.234.222'){
            return true;
        }
        //return true;
        $insert_data = array();
        $insert_data['payload'] = json_encode($communication_data);
        $insert_data['notification_type'] = $type;
        $this->job->insert_data('tbl_job_mail_queue',$insert_data);
    }
    
    
    private function create_job_custom_fields_in_job_crm($job_id,$data){
        foreach($data as $key => $value){ 
            $table_data = array();
            $table_data['job_id'] = $job_id;
            $table_data['custom_field_id'] = $key;
            $table_data['custom_field_value'] = $value;
    
            $cond = array('job_id' => $job_id,'custom_field_id' => $key);
            $stat = $this->job->insert_or_update('tbl_job_custom_field_data',$cond,$table_data);
        }
    }
    
    
    function getFileSize($path) {
        $size = filesize($path);
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }
    
    private function create_job_in_job_crm($job_type,$cust_id,$data){
        $booking_form_kuga_job_id = isset($data['kuga_job_id']) ? $data['kuga_job_id'] : NULL;
        $booking_form_uuid = isset($data['booking_form_uuid']) ? $data['booking_form_uuid'] : NULL;
        $business_details = isset($data['business_details']) ? $data['business_details'] : NULL;
        $authorised_details = isset($data['authorised_details']) ? $data['authorised_details'] : NULL;
        $booking_form_data = isset($data['booking_form']) ? $data['booking_form'] : NULL;
        $booking_form_image = isset($data['booking_form_image']) ? $data['booking_form_image'] : NULL;
        $filename = isset($data['filename']) ? $data['filename'] : '';
        $cost_centre_id =  (isset($data['cost_centre_id']) && ($data['cost_centre_id'] != NULL && $data['cost_centre_id'] != 0)) ? $data['cost_centre_id'] : 1;
        $product_data =  (isset($data['product_data'])) ? $data['product_data'] : NULL;
        $ap_company_id =  (isset($data['ap_company_id']) && ($data['ap_company_id'] != NULL && $data['ap_company_id'] != '')) ? $data['ap_company_id'] : 6;
        $booking_form_site_id = isset($data['booking_form_site_id']) ? $data['booking_form_site_id'] : NULL;
        $electricity_bill = isset($data['electricity_bill']) ? $data['electricity_bill'] : NULL;
        
        $crm_cust_id = $this->job->insert_or_update_customer($cust_id,$data['booking_form_id']);    
        
        if($crm_cust_id){
            $cust_loc_id = $this->job->insert_or_update_customer_location($cust_id,$crm_cust_id,$booking_form_site_id);
            
            $job_data = array();
            $job_data['uuid'] = $this->components->uuid();
            $job_data['company_id'] = 1;
            $job_data['cust_id'] = $crm_cust_id;
            $job_data['booking_form_id'] = $data['booking_form_id'];
            $job_data['cost_centre_id'] = $cost_centre_id;
            $job_data['ap_company_id'] = $ap_company_id;
            $job_data['assigned_to'] = isset($data['user_id']) ? $data['user_id'] : '';
            $job_data['business_details'] = json_encode($business_details);
            $job_data['authorised_details'] = json_encode($authorised_details);
            $job_data['booking_form_data'] = json_encode($booking_form_data);
            $job_data['booking_form_image'] = json_encode($booking_form_image);
            $job_data['job_type'] = $job_type;
            if($filename != ''){
                $folder = ($job_type == 'LED') ?  'led_booking_form_files' : 'solar_booking_form_files';
                $job_data['booking_form_file'] = site_url('assets/uploads/'.$folder.'/' . $filename . '.pdf');
            }
            
            if($booking_form_kuga_job_id != NULL){
                $job_id = $this->job->insert_or_update_job($data['booking_form_id'],$job_data);
    
                if($job_id != ''){
                    $this->job->insert_booking_form_activity_note($job_id, $job_data);
                }
    
                if($job_id != '' && !empty($product_data) && $job_type == 'LED'){
                    $this->job->insert_or_update_job_products($job_id,$job_type,$product_data);
                }
                
                return $job_id;
            }

            $job_id = $this->job->insert_or_update_job($data['booking_form_id'],$job_data);
    
            if($job_id != ''){
                $this->job->insert_booking_form_activity_note($job_id, $job_data);
            }
    
            if($job_id != '' && !empty($product_data) && $job_type == 'LED'){
                $this->job->insert_or_update_job_products($job_id,$job_type,$product_data);
            }
    
            if($job_id != '' && $cost_centre_id == 2){
                $this->job->insert_or_update_heers_job_submission_docs($job_id,$booking_form_image);
            }
            
            if($job_id != '' && $cost_centre_id == 1){
                $this->job->insert_or_update_job_submission_docs($job_id,$booking_form_image);
            }
            if($job_id != '' && $job_type == 'Solar'){
                $this->job->insert_or_update_solar_bill($job_id,$booking_form_image,$electricity_bill);
            }
    
            $this->send_booking_form_mail_to_john($job_id,$job_data,$filename);
    
            return $job_id;
        }
    
        return null;
    }
    
    
    private function send_booking_form_mail_to_john($job_id,$job_data,$bf_filename){
    
        if($_SERVER['REMOTE_ADDR'] == '111.93.41.194'){
            return true;
        }
    
        $business_details = json_decode($job_data['business_details']);
        $authorised_details = json_decode($job_data['authorised_details']);
    
        $customer_payload = array();
        $customer_payload['CompanyName'] = $business_details->entity_name;
        $customer_payload['Phone'] = $authorised_details->contact_no;
        $customer_payload['Email'] = $authorised_details->email;
    
        $user_id = $this->aauth->get_user_id();
        $sales_rep_data = $this->user->get_users(FALSE, $user_id, FALSE, FALSE);
    
        $communication_data = array();
        $communication_data['customer'] = $customer_payload;
        $communication_data['type'] = $job_data['job_type'];
        $communication_data['job_details'] = $this->job->get_job_details_from_job_crm($job_id);
    
        if($bf_filename != ''){
            $folder = ($job_data['job_type'] == 'LED') ?  'led_booking_form_files' : 'solar_booking_form_files';
                //$bf_filename = str_replace(' ', '%20', $bf_filename);
            $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/'.$folder.'/' . $bf_filename . '.pdf';
        }
    
        $communication_data['sales_rep'] = array();
        if (!empty($sales_rep_data)) {
            $communication_data['sales_rep']['name'] = $sales_rep_data[0]['full_name'];
            $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
        }
        //$this->email_manager->send_booking_form_success_email_to_john($communication_data);
        $communication_data['cost_centre_id'] = isset($job_data['cost_centre_id']) ? $job_data['cost_centre_id'] : NULL;
        $communication_data['job_id'] = $job_id;
        $this->add_booking_form_mail_to_queue($communication_data,2);
    
    }
}

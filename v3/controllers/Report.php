<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

ini_set('memory_limit', '-1');
/**
 * 
 * @property Report Controller
 * @version 1.0
 */
class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Role_Model", "role");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Proposal_Model", "proposal");
        if ($this->input->get('start_date')) {
            $this->start_date = $this->input->get('start_date');
            $this->end_date = $this->input->get('end_date');
        } else {
            $this->start_date = null;
            $this->end_date = null;
        }

        $this->userid = $this->input->get('userid');
        $this->user_group = $this->aauth->get_user_groups()[0]->id;

        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $this->user_group);
        $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;

        $lead_user_cond = '';
        if ($this->user_group != 1 && $all_reports_view_permission == 0) {
            $lead_user_cond = ' AND ltu.user_id=' . $this->userid;
        } else if ($this->user_group != 1 && $all_reports_view_permission == 1) {
            if ($this->userid != '' && !is_array($this->userid)) {
                $lead_user_cond = ' AND ltu.user_id=' . $this->userid;
            }
        } else if ($this->user_group == 1) {
            if ($this->userid != '' && !is_array($this->userid)) {
                $lead_user_cond = ' AND ltu.user_id=' . $this->userid;
            }
        }

        $this->user_cond = "";


        $team_types = TEAM_LEADER_TYPES;
        $this->is_team_leader = TRUE;

        //If not a team leader redirect to dashboard
        if (!array_key_exists($this->user_group, $team_types)) {
            $this->is_team_leader = FALSE;
        }

        $team_rep_types = TEAM_REP_TYPES;
        $this->is_team_rep = TRUE;
        //If not a team leader redirect to dashboard
        if (!array_key_exists($this->user_group, $team_rep_types)) {
            $this->is_team_rep = FALSE;
        }

        //State FILTER
        $this->state_id = '';
        $state_id = $this->input->get('state_id');
        if (isset($state_id) && $state_id != '') {
            $this->state_id = $state_id;
        }

        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    //Dashboard Reprot Page Functions

    private function prepare_led_product_type_total($cond) {
        $query = '';

        $higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=3 AND $cond),0) as total_highbay";

        $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=4 AND $cond),0) as total_batten_light";

        $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=5 AND $cond),0) as total_panel_light";

        $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=6 AND $cond),0) as total_flood_light";

        $down_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=7 AND $cond),0) as total_down_light";

        $other_select = "Coalesce((SELECT SUM(pro_item.ae_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_access_equipments as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_access_equipments as ae ON ae.ae_id = pro_item.ae_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = ae.type_id"
                . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE  prd_type.type_id=8 AND $cond),0) as total_other";

        $query = "$higbay_select,"
                . "$batten_light_select,"
                . "$panel_light_select,"
                . "$flood_light_select,"
                . "$down_light_select,"
                . "$other_select";

        return $query;
    }

    public function fetch_statistics_data() {
        $data = array();

        //Restricitng the Access to Data on users
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->is_team_leader) {
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
        } else if ($this->is_team_rep) {
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND ltu.user_id IN(" . $user_id . ")";
        }

        if ($this->userid != '') {
            $this->user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $date_cond = 'AND (DATE(lead.created_at) BETWEEN "' . $this->start_date . '" AND "' . $this->end_date . '")';

        //LED Progress TOTAL VALUES
        $prg_stage = WON_STAGE;
        $prg_no_of_sales = "Coalesce((SELECT count(lead.id) FROM tbl_leads as lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage < $prg_stage) $this->user_cond),0) as no_of_sales";

        $prg_total_revenue = "Coalesce((SELECT SUM(total_investment_exGST * 1.1) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage < $prg_stage) $this->user_cond),0) as total_revenue";

        $prg_total_kws = "Coalesce((SELECT SUM(tp.system_size) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage < $prg_stage) $this->user_cond),0) as total_kws";

        $prg_led_product_type_total = $this->prepare_led_product_type_total('lead.lead_stage < ' . $prg_stage . $user_cond);
        $prg_deals_sql = "SELECT "
                . "$prg_no_of_sales,"
                . "$prg_total_revenue,"
                . "$prg_led_product_type_total"
                //. "$prg_total_kws"
                . " FROM tbl_led_proposal as tp";
        $prg_deals_sql .= " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id";
        $prg_deals_sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $cond_prg_deals = "";

        $won_stage = WON_STAGE;
        $won_no_of_sales = "Coalesce((SELECT count(lead.id) FROM tbl_leads as lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$won_stage) $this->user_cond),0) as no_of_sales";

        $won_total_revenue = "Coalesce((SELECT SUM(total_investment_exGST * 1.1) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$won_stage) $this->user_cond),0) as total_revenue";

        $won_total_kws = "Coalesce((SELECT SUM(tp.system_size) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$won_stage) $this->user_cond),0) as total_kws";

        $won_led_product_type_total = $this->prepare_led_product_type_total('lead.lead_stage=' . $won_stage . $user_cond);

        $won_deals_sql = "SELECT "
                . "$won_no_of_sales,"
                . "$won_total_revenue,"
                . "$won_led_product_type_total"
                //. "$won_total_kws"
                . " FROM tbl_led_proposal as tp";
        $won_deals_sql .= " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id";
        $won_deals_sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        //$cond_won_deals = " WHERE lead.lead_stage=$won_stage " . $date_cond;
        $cond_won_deals = "";

        $lost_stage = LOST_STAGE;
        $lost_no_of_sales = "Coalesce((SELECT count(lead.id) FROM tbl_leads as lead"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$lost_stage) $this->user_cond),0) as no_of_sales";

        $lost_total_revenue = "Coalesce((SELECT SUM(total_investment_exGST * 1.1) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$lost_stage) $this->user_cond),0) as total_revenue";

        $lost_total_kws = "Coalesce((SELECT SUM(tp.system_size) FROM tbl_led_proposal tp"
                . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND (lead.lead_stage=$lost_stage) $this->user_cond),0) as total_kws";

        $lost_led_product_type_total = $this->prepare_led_product_type_total('lead.lead_stage=' . $lost_stage . $user_cond);

        $lost_deals_sql = "SELECT "
                . "$lost_no_of_sales,"
                . "$lost_total_revenue,"
                . "$lost_led_product_type_total"
                //. "$lost_total_kws"
                . " FROM tbl_led_proposal as tp";
        $lost_deals_sql .= " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id";
        $lost_deals_sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        //$cond_lost_deals = " WHERE lead.lead_stage=$lost_stage " . $date_cond;
        $cond_lost_deals = "";

        $cond = "";
        if ($this->user_cond != "") {
            $cond = " WHERE 1=1 " . $this->user_cond;
        }

        $response_prg_deals_q = $this->db->query($prg_deals_sql . $cond);
        $response_prg_deals = $response_prg_deals_q->result_array();

        $response_won_deals_q = $this->db->query($won_deals_sql . $cond);
        $response_won_deals = $response_won_deals_q->result_array();

        $response_lost_deals_q = $this->db->query($lost_deals_sql . $cond);
        $response_lost_deals = $response_lost_deals_q->result_array();

        $data['stats'][0]['title'] = 'Progress Deals';
        $data['stats'][1]['title'] = 'Won Deals';
        $data['stats'][2]['title'] = 'Lost Deals';
        $data['stats'][0]['deals_data'] = $response_prg_deals;
        $data['stats'][1]['deals_data'] = $response_won_deals;
        $data['stats'][2]['deals_data'] = $response_lost_deals;

        echo json_encode($data);
        die;
    }

    public function fetch_activity_data() {

        //Restricitng the Access to Data on users
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->is_team_leader) {
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
        } else if ($this->is_team_rep) {
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND ltu.user_id IN(" . $user_id . ")";
        }

        if ($this->userid != '') {
            $this->user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $ta_user_cond = '';
        if ($this->userid != '') {
            $ta_user_cond = ' AND ta.user_id=' . $this->userid;
        }

        $date_cond = '(DATE(ta.created_at) BETWEEN "' . $this->start_date . '" AND "' . $this->end_date . '") OR (DATE(ta.updated_at) BETWEEN "' . $this->start_date . '" AND "' . $this->end_date . '")';
        $sql = "SELECT  DATE_FORMAT(ta.updated_at,'%d/%m/%Y') as created_at,"
                . "SUM(activity_type = 'Note') as note_count,"
                . "SUM(activity_type = 'Visit') as visit_count,"
                . "SUM(activity_type = 'Email') as email_count,"
                . "SUM(activity_type = 'Phone') as phone_count"
                . " FROM tbl_activities as ta ";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id";
        $cond = " WHERE " . $date_cond . $user_cond;
        $query = $this->db->query($sql . $cond . $ta_user_cond . " GROUP BY DATE(ta.updated_at)");
        $response = $query->result_array();

        $date_cond1 = '(DATE(ta.updated_at) BETWEEN "' . $this->start_date . '" AND "' . $this->end_date . '")';
        $sql1 = "SELECT ta.activity_type,IF(ta.scheduled_date,DATE_FORMAT(ta.scheduled_date,'%d/%m/%Y'),NULL) as scheduled_at,ta.status,ta.id,ta.user_id,"
                . "DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(ta.scheduled_date)), '%Y-%m-%d'),CURDATE()) as days_left,"
                . "lead.cust_id,ls.stage_name,lead.uuid,cust.first_name,cust.last_name,cust.company_name,cust.customer_contact_no,"
                . "site.address,site.suburb,site.postcode,"
                . "ud.full_name,"
                . "IF(ta.updated_at,DATE_FORMAT(ta.updated_at,'%d/%m/%Y'),DATE_FORMAT(ta.created_at,'%d/%m/%Y')) as date_of_activity"
                . " FROM tbl_activities as ta ";
        $sql1 .= " LEFT JOIN tbl_leads as lead ON lead.id = ta.lead_id";
        $sql1 .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $sql1 .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = lead.lead_stage";
        $sql1 .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = lead.id";
        $sql1 .= " LEFT JOIN tbl_customers as cust ON cust.id = lead.cust_id";
        $sql1 .= " LEFT JOIN tbl_customer_locations as site ON site.id = lp.site_id";
        $sql1 .= " LEFT JOIN aauth_users as user ON user.id = ta.user_id";
        $sql1 .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $cond1 = " WHERE " . $date_cond1 . " AND ta.status=1";

        //echo $sql1 . $cond1 . $ta_user_cond;die;
        $query1 = $this->db->query($sql1 . $cond1 . $ta_user_cond . "  GROUP BY ta.id");
        $response1 = $query1->result_array();

        $data['activity'] = $response;
        $data['scheduled_activity'] = $response1;

        echo json_encode($data);
        die;
    }

    public function fetch_lead_stage_proposal_count() {
        //Restricitng the Access to Data on users
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->is_team_leader) {
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
        } else if ($this->is_team_rep) {
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND ltu.user_id IN(" . $user_id . ")";
        }

        if ($this->userid != '') {
            $this->user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $data['lead_stages'] = $this->common->fetch_orderBy('tbl_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
        $data['led_statistics'] = $this->lead->lead_count_by_lead_stages_for_csm_report($this->start_date, $this->end_date, $this->user_cond,1);
        $data['solar_statistics'] = $this->lead->lead_count_by_lead_stages_for_csm_report($this->start_date, $this->end_date, $this->user_cond,2);
        $data['led_solar_statistics'] = $this->lead->lead_count_by_lead_stages_for_csm_report($this->start_date, $this->end_date, $this->user_cond,3);
        echo json_encode($data);
        die;
    }


    public function fetch_target_data() {

        if ($this->userid == '') {
            $this->userid = $this->aauth->get_user_id();
        }
        $user_name = $this->common->fetch_cell('tbl_user_details','full_name',array('user_id' => $this->userid));
        $state_id = $this->common->fetch_cell('tbl_user_locations','state_id',array('user_id' => $this->userid));
        
        $data['items'] = array(
            ['name' => 'HB - Approved Booking Forms', 'class' => '', 'alias' => 'hb_approved_booking_forms', 'editable' => false],
            ['name' => 'HB - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'hb_submitted_to_ap', 'editable' => false],
            ['name' => 'PL / Batten / Tubes - Approved Booking Forms', 'class' => '', 'alias' => 'p_b_t_approved', 'editable' => false],
            ['name' => 'PL / Batten / Tubes - Installed', 'class' => 'group_last_element', 'alias' => 'p_b_t_installed', 'editable' => false],
            ['name' => 'Floodlights - Approved Booking Forms', 'class' => '', 'alias' => 'floodlight_approved', 'editable' => false],
            ['name' => 'Floodlights - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'floodlight_submitted', 'editable' => false],
            ['name' => 'Solar - Received', 'class' => '', 'alias' => 'solar_received', 'editable' => false],
            ['name' => 'Solar - Approved', 'class' => '', 'alias' => 'solar_approved', 'editable' => false],
            ['name' => 'Solar - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'solar_submitted', 'editable' => false],
            ['name' => 'Solar - Cancelled (Based on received Date)', 'class' => '', 'alias' => 'solar_cancelled', 'editable' => false],
            ['name' => 'PERCENTAGE TO TARGET', 'class' => 'group_last_element', 'alias' => 'percentage_to_target', 'editable' => false],
        );

        $state_id = ($state_id != 2 && $state_id != 7) ? 7 : $state_id; 
        $data['statistics'] = array();
        //$data['months'] = array();
        //$data['years'] = array();
    
        $today_month = (int) date("m", strtotime(date('Y-m-d')));
        $timeStart = strtotime("2018-01-01");
        $timeEnd = strtotime(date('Y-m-d'));
        // Adding current month + all months in each passed year
        $numMonths = 1 + (date("Y",$timeEnd)-date("Y",$timeStart))*12;
        // Add/subtract month difference
        $numMonths += date("m",$timeEnd)-date("m",$timeStart);
        $start = - (12 - $today_month);
        $end = $numMonths;
        for ($i = $start; $i < $end; $i++) {
            if ($i == 0) {
                $month_year = date("m-Y", strtotime(date('Y-m-d')));
                $month = date("m", strtotime(date('Y-m-d')));
                $year = date("Y", strtotime(date('Y-m-d')));
            } else {
                $month_year = date("m-Y", strtotime(date('Y-m-01') . " -$i months"));
                $month = date("m", strtotime(date('Y-m-01') . " -$i months"));
                $year = date("Y", strtotime(date('Y-m-01') . " -$i months"));
            }
            $month_years[] = $month_year;
            $months[] = $month;
            $years[] = $year;
        }
        $data['month_year_range'] = array('start' => $start,'end' => $end);
        $reverse_month_years = array_reverse($month_years);
        $target_data = $this->common->fetch_where('tbl_simpro_sales_target','*',array('state_id' => $state_id));
        foreach($target_data as $key => $value){
            $target_data[$key]['target'] = json_decode($target_data[$key]['target']);
            $target_data[$key]['weighting'] = json_decode($target_data[$key]['weighting']);
            $start_month_year = date("m-Y", strtotime($target_data[$key]['target_start_date']));
            $end_month_year = date("m-Y", strtotime($target_data[$key]['target_end_date']));
            $target_data[$key]['target_start_date'] = array_search ($start_month_year, $reverse_month_years);
            $target_data[$key]['target_end_date'] = array_search ($end_month_year, $reverse_month_years);
        }
        $data['target_data'] = $target_data;

        //$data['months'] = $months;
        //$data['years'] = $years;
        for ($i = 0; $i < count($month_years); $i++) {
            $stat = $this->proposal->get_dashboard_report_data_monthly($months[$i], $years[$i], $user_name);
            $data['statistics'][$i] = (!empty($stat)) ? $stat : [];
        }
        echo json_encode($data);
        die;
    }

    public function save_target_data() {
        if (!empty($this->input->post())) {
            $user_id = $this->aauth->get_user_id();
            $report_data = $this->input->post('report_data');
            $report_data['user_id'] = $user_id;
            $cond = array(
                'year' => $report_data['year'],
                'month' => $report_data['month'],
                'user_id' => $report_data['user_id']
            );
            $this->common->insert_or_update('tbl_dashboard_report_data_monthly', $cond, $report_data);
            $this->fetch_target_data();
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    /** Report SECTION */
    public function manage() {
        $this->load->view('partials/header');
        $this->load->view('partials/sidebar');
        $this->load->view('report/menu');
        $this->load->view('partials/footer');
    }

    public function reports() {
        $type = $this->input->get('type');
        if (isset($type) && $type != '') {
            switch ($type) {
                case 'sales':
                    $this->manage_sales_report();
                    break;
                case 'simpro':
                    $this->manage_simpro_report();
                    break;    
                case 'lead_allocation_report':
                    $this->manage_lead_allocation_report();
                    break;
                case 'facebook':
                    $this->manage_facebook_leads_report();
                    break;
                case 'won_leads':
                    $this->manage_won_leads_report();
                    break;
                default:
                    break;
            }
        } else {
            redirect('admin/reports/manage');
        }
    }

    public function manage_sales_report() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->is_team_leader) {
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
        } else if ($this->is_team_rep) {
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND ltu.user_id IN(" . $user_id . ")";
        }

        if ($this->userid != '') {
            $this->user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $cond = " WHERE 1=1 AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
        $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        $data['users'] = $all_users;
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('report/sales_report');
        $this->load->view('partials/footer');
    }

    public function manage_lead_allocation_report() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) { 
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND leads.user_id IN(" . $user_id . ")";
        } else {
            redirect('admin/dashboard');
            exit();
        }

        if ($this->userid != '') {
            $this->user_cond = " AND leads.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('report/lead_allocation_report');
        $this->load->view('partials/footer');
    }
    
    public function manage_simpro_report() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->is_team_leader) {
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
        } else if ($this->is_team_rep) {
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND ltu.user_id IN(" . $user_id . ")";
        }

        if ($this->userid != '') {
            $this->user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;

        $cond = " WHERE 1=1 AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
        $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        $data['users'] = $all_users;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('report/simpro_report');
        $this->load->view('partials/footer');
    }

    

    public function fetch_sales_rep_performance_data() {
        $data = array();
        $report_type = $this->input->get('report_type');
        $report_type = (isset($report_type) && $report_type  != '') ? $report_type : 'sales_rep_report';
        
        if(is_array($this->userid)){
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND  user.id IN (".implode(',',$this->userid).")  AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        }else if ($this->aauth->is_member('Admin')) {
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($report_type == 'lead_allocation'){
                $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($this->components->is_lead_allocation_team_leader()){
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            if($report_type == 'lead_allocation'){
                $logged_in_user = $this->user->get_users(FALSE, $user_id, FALSE,FALSE);
                $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
                $all_users = array_merge($logged_in_user,$all_users);
            }else{
                $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
            }
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($this->components->is_lead_allocation_rep()){
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE ud.user_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            $all_users = $this->user->get_users(FALSE, $user_id, FALSE);
        }
        
        //echo implode(',',array_column($all_users,'user_id'));die;

        if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep() || $report_type == 'lead_allocation') { 
            $data = $this->handle_franchise_performance_data_for_lead_allocation($all_users,$report_type);
        }else if($report_type == 'lead_statistics'){
            $data = $this->handle_franchise_lead_perfromance_data($all_users,$report_type);
        }else if($report_type == 'simpro_statistics'){
            $data = $this->handle_simpro_franchise_perfromance_data_for_sales($all_users,$report_type);
        }else{
            $data = $this->handle_franchise_perfromance_data_for_sales($all_users,$report_type);
        }
        
        echo json_encode($data);
        die;
    }
    
    function handle_franchise_lead_perfromance_data($all_users,$report_type){
        $data = array();
        $data['sales_stats'] = [];
        
        foreach ($all_users as $key => $value) {
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep() || $report_type == 'lead_allocation') { 
                $ltu_condition_col = "lead.user_id IN(" . $value['user_id'] . ")";
            }else{
                $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            }
            
            $sub_user_query = $this->db->query('SELECT group_concat(user_id) as user_csv FROM `aauth_user_to_user` WHERE owner_id = ' . $value['user_id']);
            $sub_users = $sub_user_query->row_array();
            $userIdCsv = (isset($sub_users['user_csv']) && $sub_users['user_csv'] != '') ? $sub_users['user_csv'] : '';
            //$sub_user_ltu_query  = ($userIdCsv != '') ? ' OR (ltu.user_id IN('.$userIdCsv.'))' : '';
            $sub_user_ltu_query = '';
            //$sub_user_generated_query = ($userIdCsv != '') ? ' OR (lead.user_id IN('.$userIdCsv.'))' : '';
            $sub_user_generated_query = '';
            //$not_sub_user_generated_query = ($userIdCsv != '') ? ' OR (lead.user_id NOT IN('.$userIdCsv.'))' : '';
            $not_sub_user_generated_query = '';
            
            $total_prospect_count = "Coalesce((SELECT COUNT(ta.lead_id)  FROM tbl_activities ta"
                    . " LEFT JOIN tbl_leads as lead ON ta.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) AND lead.lead_stage IN (1,2,14) AND ta.activity_type = 'Visit'),0)  as total_prospect_count";
                    
            //echo $total_prospect_count;die;
                    
            $total_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_leads";

            /**$total_self_generated_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) AND (lead.user_id =" . $value['user_id'] . " $sub_user_generated_query)),0)  as total_self_generated_leads_count";*/

            $prev_lead_stage = '1,2,10,14';
            $current_lead_stage = '3,4,5';        
            $total_self_generated_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "') AND ($ltu_condition_col $sub_user_ltu_query) AND lead.prev_lead_stage IN ($prev_lead_stage) AND lead.lead_stage IN ($current_lead_stage)),0)  as total_self_generated_leads_count";

            $total_leads_allocation_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) AND (lead.user_id !=" . $value['user_id'] . " $not_sub_user_generated_query) AND lead.lead_source='Call Centre Lead'),0)  as total_leads_allocation_count";

            $total_mitc_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) 
                        AND lead.lead_source='Make it Cheaper'),0)  as total_mitc_leads_count";
                        
            $total_fb_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) 
                        AND lead.lead_source='Facebook Lead'),0)  as total_fb_leads_count";
                        
            $total_web_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) 
                        AND lead.lead_source='Kuga Lead'),0)  as total_web_leads_count";

            $total_other_partner_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query) 
                        AND lead.lead_source NOT IN ('Make it Cheaper','Self Generated','Kuga Lead','Call Centre Lead')),0)  as total_other_partner_leads_count";

            $led_proposal_count = "Coalesce((SELECT COUNT(tp.id) FROM tbl_led_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_led_proposal";

            $solar_proposal_count = "Coalesce((SELECT COUNT(tp.id)  FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_solar_proposal";
                    
            $quick_quote_count = "Coalesce((SELECT COUNT(lq.id)  FROM  tbl_lead_quotes lq"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lq.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_quick_quote";

            $won_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_won_leads";
                    
            $won_leads_ids = "Coalesce((SELECT GROUP_CONCAT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as won_leads_ids";
            
            $user_id = $value['user_id'];
            $sql = "SELECT lead.id,"
                    . "$total_prospect_count,"
                    . "$total_leads_count,"
                    . "$total_self_generated_leads_count,"
                    . "$total_leads_allocation_count,"
                    . "$total_mitc_leads_count,"
                    . "$total_fb_leads_count,"
                    . "$total_web_leads_count,"
                    . "$total_other_partner_leads_count,"
                    . "$led_proposal_count,"
                    . "$solar_proposal_count,"
                    . "$quick_quote_count,"
                    . "$won_leads_count,"
                    . "$won_leads_ids,"
                    . "$user_id as user_id"
                    . " from tbl_leads lead LIMIT 1";
            
            //echo $sql;die;
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        return $data;
    }

    function handle_franchise_perfromance_data_for_sales($all_users,$report_type){
        $data = array();
        $data['sales_stats'] = [];

        foreach ($all_users as $key => $value) {
            $value['user_id'] = (isset($value['user_id'])) ? $value['user_id'] : $value;
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep() || $report_type == 'lead_allocation') { 
                $ltu_condition_col = "lead.user_id IN(" . $value['user_id'] . ")";
            }else{
                $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            }

            $sub_user_query = $this->db->query('SELECT group_concat(user_id) as user_csv FROM `aauth_user_to_user` WHERE owner_id = ' . $value['user_id']);
            $sub_users = $sub_user_query->row_array();
            $userIdCsv = (isset($sub_users['user_csv']) && $sub_users['user_csv'] != '') ? $sub_users['user_csv'] : '';
            $sub_user_ltu_query  = ($userIdCsv != '') ? ' OR (ltu.user_id IN('.$userIdCsv.'))' : '';

            $higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=3"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_highbays";

            $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=4"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_batten_light";

            $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=5"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_panel_light";

            $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=6"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_flood_light";       

            $won_total_kws = "Coalesce((SELECT SUM(tp.total_system_size) FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_kws ";

            $led_proposal_count = "Coalesce((SELECT COUNT(tp.id) FROM tbl_led_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_led_proposal";
            $solar_proposal_count = "Coalesce((SELECT COUNT(tp.id)  FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_solar_proposal";

            $won_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_won_leads";;

            $total_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_leads";

            $total_leads_allocation_count = "Coalesce((SELECT COUNT(lead_aloc.id)  FROM tbl_lead_allocations lead_aloc"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead_aloc.lead_id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead_aloc.created_by IN(" . $value['user_id'] . ")),0)  as total_leads_allocation_count";

            $sql = "SELECT lead.id,"
                    . "$won_total_kws,"
                    . "$higbay_select,"
                    . "$flood_light_select,"
                    . "$panel_light_select,"
                    . "$batten_light_select,"
                    . "$led_proposal_count,"
                    . "$solar_proposal_count,"
                    . "$won_leads_count,"
                    . "$total_leads_count,"
                    . "$total_leads_allocation_count"
                    . " from tbl_leads lead LIMIT 1";
            
            //echo $sql;die;
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        return $data;
    }

    function handle_franchise_performance_data_for_lead_allocation($all_users,$report_type){
        $data = array();
        $data['sales_stats'] = [];
        
        foreach ($all_users as $key => $value) {
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep() || $report_type == 'lead_allocation') { 
                $ltu_condition_col = "lead.user_id IN(" . $value['user_id'] . ")";
            }else{
                $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            }

            $higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=3"
                    . " AND $ltu_condition_col "
                    . " AND (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_highbays";

            $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=4"
                    . " AND $ltu_condition_col"
                    . " AND (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_batten_light";

            $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=5"
                    . " AND $ltu_condition_col"
                    . " AND (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_panel_light";

            $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=6"
                    . " AND $ltu_condition_col"
                    . " AND (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_flood_light";



            $won_total_kws = "Coalesce((SELECT SUM(tp.total_system_size) FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND $ltu_condition_col),0)  as total_kws ";

            $led_proposal_count = "Coalesce((SELECT COUNT(tp.id) FROM tbl_led_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND $ltu_condition_col),0)  as total_led_proposal";

            $solar_proposal_count = "Coalesce((SELECT COUNT(tp.id)  FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND $ltu_condition_col),0)  as total_solar_proposal";

            $won_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND $ltu_condition_col),0)  as total_won_leads";

            $total_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_allocations as lead_aloc ON lead_aloc.lead_id = lead.id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND $ltu_condition_col),0)  as total_leads";

            $total_leads_allocation_count = "Coalesce((SELECT COUNT(lead_aloc.id)  FROM tbl_lead_allocations lead_aloc"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead_aloc.lead_id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead_aloc.created_by IN(" . $value['user_id'] . ")),0)  as total_leads_allocation_count";

            $sql = "SELECT lead.id,"
                    . "$won_total_kws,"
                    . "$higbay_select,"
                    . "$flood_light_select,"
                    . "$panel_light_select,"
                    . "$batten_light_select,"
                    . "$led_proposal_count,"
                    . "$solar_proposal_count,"
                    . "$won_leads_count,"
                    . "$total_leads_count,"
                    . "$total_leads_allocation_count"
                    . " from tbl_leads lead LIMIT 1";
            
            //echo $sql;die;
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        return $data;
    }

    public function fetch_sales_rep_closable_leads_data() {
        $data = array();
        $data['sales_stats'] = [];

        if(is_array($this->userid)){
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND  user.id IN (".implode(',',$this->userid).")  AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->aauth->is_member('Admin')) {
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id!=1 $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, $user_id, FALSE);
        }

        foreach ($all_users as $key => $value) {
            $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            $sub_user_query = $this->db->query('SELECT group_concat(user_id) as user_csv FROM `aauth_user_to_user` WHERE owner_id = ' . $value['user_id']);
            $sub_users = $sub_user_query->row_array();
            $userIdCsv = (isset($sub_users['user_csv']) && $sub_users['user_csv'] != '') ? $sub_users['user_csv'] : '';
             //$sub_user_ltu_query  = ($userIdCsv != '') ? ' OR (ltu.user_id IN('.$userIdCsv.'))' : '';
            $sub_user_ltu_query = '';

            $higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty)s FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=3"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND lead.lead_stage=9"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_highbays";

            $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=4"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND lead.lead_stage=9"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_batten_light";

            $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=5"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND lead.lead_stage=9"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_panel_light";

            $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=6"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND lead.lead_stage=9"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_flood_light";

            $won_total_kws = "Coalesce((SELECT SUM(tp.total_system_size) FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead.lead_stage=9"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_kws ";

            $led_lead_count = "Coalesce((SELECT count(DISTINCT(lp.lead_id)) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead.lead_stage=9"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_led_leads";

            $solar_lead_count = "Coalesce((SELECT count(DISTINCT(slp.lead_id)) FROM tbl_solar_proposal slp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = slp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead.lead_stage=9"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_solar_leads";

            $won_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead.lead_stage=9"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_won_leads";

            $total_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead.lead_stage=9"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_leads";
            //echo $total_leads_count;die;
            $sql = "SELECT lead.id,"
                    . "$won_total_kws,"
                    . "$higbay_select,"
                    . "$flood_light_select,"
                    . "$panel_light_select,"
                    . "$batten_light_select,"
                    . "$led_lead_count,"
                    . "$solar_lead_count,"
                    . "$won_leads_count,"
                    . "$total_leads_count"
                    . " from tbl_leads lead LIMIT 1";
            //echo $sql;die;
            
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        echo json_encode($data);
        die;
    }

    public function fetch_sales_rep_lead_stage_data() {
        $data = array();
        $data['sales_stats'] = [];

        if(is_array($this->userid)){
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND  user.id IN (".implode(',',$this->userid).")  AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->aauth->is_member('Admin')) {
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id!=1 $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, $user_id, FALSE);
        }

        $data['lead_stages'] = $lead_stages = $this->common->fetch_orderBy('tbl_lead_stages', '*', array('status' => 1), 'order_no', 'ASC', NULL);
        foreach ($all_users as $key => $value) {
            $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            $sub_user_query = $this->db->query('SELECT group_concat(user_id) as user_csv FROM `aauth_user_to_user` WHERE owner_id = ' . $value['user_id']);
            $sub_users = $sub_user_query->row_array();
            $userIdCsv = (isset($sub_users['user_csv']) && $sub_users['user_csv'] != '') ? $sub_users['user_csv'] : '';
             //$sub_user_ltu_query  = ($userIdCsv != '') ? ' OR (ltu.user_id IN('.$userIdCsv.'))' : '';
            $sub_user_ltu_query = '';
            if($value['user_id'] == 5){
                //echo $this->db->last_query();die;
            }
            $lead_count_sql = '';
            $completed_site_visit = '';
            foreach ($lead_stages as $key1 => $lead_stage) {
                $lid = $lead_stage['id'];
                $stage_name = 'stage' . $lead_stage['order_no'] . '_count';
                $stage_name_visit = 'stage' . $lead_stage['order_no'] . '_visit_count';
                $comma = ((count($lead_stages) - 1) == $key1) ? '' : ',';
                if($lid != 3){
                    $lead_count_sql .= "Coalesce((SELECT count(DISTINCT(lead.id)) FROM tbl_leads lead"
                        . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                        . " LEFT JOIN tbl_activities as activity ON activity.lead_id = lead.id"
                        . " WHERE (DATE(activity.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                        . " AND ($ltu_condition_col $sub_user_ltu_query)"
                        . " AND lead.lead_stage = $lid),0) as $stage_name$comma";
                        
                }else{
                    $prev_lead_stage = '1,2,10,14';
                    $current_lead_stage = '3,4,5,6,7,8,13';
                    $lead_count_sql .= "Coalesce((SELECT count(DISTINCT(lead.id)) FROM tbl_leads lead"
                        . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                        . " LEFT JOIN tbl_activities as activity ON activity.lead_id = lead.id"
                        . " WHERE (DATE(activity.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                        . " AND ($ltu_condition_col $sub_user_ltu_query)"
                        . " AND lead.lead_stage = $lid),0) as $stage_name$comma";
                        
                    //This is for New Deal vs Closable Deal Report
                    $stage_name = 'stage' . $lead_stage['order_no'] . '_1_count';
                    $lead_count_sql .= "Coalesce((SELECT count(DISTINCT(lead.id)) FROM tbl_leads lead"
                        . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                        . " LEFT JOIN tbl_activities as activity ON activity.lead_id = lead.id"
                        . " WHERE (DATE(activity.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                        . " AND ($ltu_condition_col $sub_user_ltu_query)"
                        . " AND lead.prev_lead_stage IN ($prev_lead_stage) AND lead.lead_stage IN ($current_lead_stage) ),0) as $stage_name$comma";
                    //echo $lead_count_sql;die;
                }
                $completed_site_visit .= "Coalesce((SELECT COUNT(ta.id)  FROM tbl_activities ta"
                    . " LEFT JOIN tbl_leads as lead ON ta.lead_id = lead.id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                        . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                        . " AND ($ltu_condition_col $sub_user_ltu_query)"
                        . " AND lead.lead_stage = $lid AND ta.activity_type = 'Visit' AND ta.status = 1),0) as $stage_name_visit$comma";
                        
            }

            $sql = "SELECT lead.id,"
                    . "$lead_count_sql,"
                    . "$completed_site_visit"
                    . " from tbl_leads lead LIMIT 1";
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        echo json_encode($data);
        die;
    }

    public function fetch_sales_rep_activity_data() {
        $data = array();
        $data['activity_stats'] = [];
        
        $report_type = $this->input->get('report_type');
        $report_type = (isset($report_type) && $report_type  == 'lead_allocation') ? $report_type : 'sales_rep_report';
        if(is_array($this->userid)){
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND  user.id IN (".implode(',',$this->userid).")  AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        } else if ($this->aauth->is_member('Admin')) {
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($report_type == 'lead_allocation'){
                $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE 1=1 $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        }  else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($this->components->is_lead_allocation_team_leader()){
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            if($report_type == 'lead_allocation'){
                $logged_in_user = $this->user->get_users(FALSE, $user_id, FALSE,FALSE);
                $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
                $all_users = array_merge($logged_in_user,$all_users);
            }else{
                $all_users = $this->user->get_users(FALSE, FALSE, $user_id, $cond);
            }
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            if($this->components->is_lead_allocation_rep()){
                $cond = " WHERE utu.owner_id=$user_id $state_cond AND user.id NOT IN (1,23,24,28) AND user_group.group_id IN(4,5) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }else{
                $cond = " WHERE ud.user_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            }
            $all_users = $this->user->get_users(FALSE, $user_id, FALSE);
        }

        foreach ($all_users as $key => $value) {

            $note_count = "Coalesce((SELECT SUM(activity_type = 'Note') FROM tbl_activities ta"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
                    . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ta.user_id IN(" . $value['user_id'] . ") GROUP BY DATE(ta.activity_type)),0) as note_count";

            // $visit_count = "Coalesce((SELECT SUM(activity_type = 'Visit') FROM tbl_activities ta"
            //         . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
            //         . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
            //         . " AND ta.user_id IN(" . $value['user_id'] . ") GROUP BY DATE(ta.activity_type)),0) as visit_count";
                    
                      $visit_count = "Coalesce((SELECT count(ta.id) FROM tbl_activities ta"
                    . " JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
                    . " WHERE ta.activity_type = 'Visit' and  (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ltu.user_id IN(" . $value['user_id'] . ") ),0) as visit_count";

            $email_count = "Coalesce((SELECT SUM(activity_type = 'Email') FROM tbl_activities ta"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
                    . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ta.user_id IN(" . $value['user_id'] . ") GROUP BY DATE(ta.activity_type)),0) as email_count";

            $phone_count = "Coalesce((SELECT SUM(activity_type = 'Phone') FROM tbl_activities ta"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
                    . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ta.user_id IN(" . $value['user_id'] . ") GROUP BY DATE(ta.activity_type)),0) as phone_count";

            $meeting_count = "Coalesce((SELECT SUM(activity_type = 'Schedule Meeting') FROM tbl_activities ta"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = ta.lead_id"
                    . " WHERE (DATE(ta.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ta.user_id IN(" . $value['user_id'] . ") GROUP BY DATE(ta.activity_type)),0) as meeting_count";

            $sql = "SELECT DATE_FORMAT(ta.created_at,'%d/%m/%Y') as created_at,"
                    . "$note_count,"
                    . "$visit_count,"
                    . "$email_count,"
                    . "$phone_count,"
                    . "$meeting_count"
                    . " FROM tbl_activities as ta";
                    // echo$sql;die;
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['activity_stats'][$key] = $response;
        }

        echo json_encode($data);
        die;
    }
    
    /** Simrpo Report Functions */

    function handle_simpro_franchise_perfromance_data_for_sales($all_users,$report_type){
        $data = array();
        $data['sales_stats'] = [];

        foreach ($all_users as $key => $value) {
            $value['user_id'] = (isset($value['user_id'])) ? $value['user_id'] : $value;
            if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep() || $report_type == 'lead_allocation') { 
                $ltu_condition_col = "lead.user_id IN(" . $value['user_id'] . ")";
            }else{
                $ltu_condition_col = "ltu.user_id IN(" . $value['user_id'] . ")";
            }

            $sub_user_query = $this->db->query('SELECT group_concat(user_id) as user_csv FROM `aauth_user_to_user` WHERE owner_id = ' . $value['user_id']);
            $sub_users = $sub_user_query->row_array();
            $userIdCsv = (isset($sub_users['user_csv']) && $sub_users['user_csv'] != '') ? $sub_users['user_csv'] : '';
            $sub_user_ltu_query  = ($userIdCsv != '') ? ' OR (ltu.user_id IN('.$userIdCsv.'))' : '';

            /**$higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=3"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_highbays";*/

            $higbay_select = "Coalesce((SELECT SUM(sj.highbays) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_highbays";

            /**$batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=4"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_batten_light";*/

            $batten_light_select = "Coalesce((SELECT SUM(sj.battens) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_batten_light";

            /**$panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=5"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_panel_light";*/

            $panel_light_select = "Coalesce((SELECT SUM(sj.panel_lights) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_panel_light";

            /**$flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                    . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                    . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                    . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                    . " LEFT JOIN  tbl_leads as lead ON lead.id = lp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE  prd_type.type_id=6"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)"
                    . " AND (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_flood_light";*/

            $flood_light_select = "Coalesce((SELECT SUM(sj.flood_lights) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_flood_light";        

            /**$won_total_kws = "Coalesce((SELECT SUM(tp.total_system_size) FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_kws ";*/

            $won_total_kws = "Coalesce((SELECT SUM(sj.killo_watt) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_kws";   

            /**$led_proposal_count = "Coalesce((SELECT COUNT(tp.id) FROM tbl_led_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_led_proposal";*/

            $led_proposal_count = "Coalesce((SELECT COUNT(sj.id) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (sj.solar_panels IS NULL || sj.solar_panels = '' || sj.solar_panels =0) AND(DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_led_proposal";

            /**$solar_proposal_count = "Coalesce((SELECT COUNT(tp.id)  FROM tbl_solar_proposal tp"
                    . " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_solar_proposal";*/

            $solar_proposal_count = "Coalesce((SELECT COUNT(sj.id) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND (sj.solar_panels IS NOT NULL &&  sj.solar_panels != '' && sj.solar_panels != 0) AND(DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_solar_proposal";

            /**$won_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND (lead.lead_stage=" . WON_STAGE . ")"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_won_leads";*/

            $won_leads_count = "Coalesce((SELECT COUNT(sj.id) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'" '
                    . " AND(DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_won_leads";

            /**$total_leads_count = "Coalesce((SELECT COUNT(lead.id)  FROM tbl_leads lead"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id"
                    . " WHERE (DATE(lead.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND ($ltu_condition_col $sub_user_ltu_query)),0)  as total_leads";*/

            $total_leads_count = "Coalesce((SELECT COUNT(sj.id) FROM tbl_simpro_jobs sj"
                    . ' WHERE sj.salesperson_name="'.$value['full_name'].'"'
                    . " AND sj.status_name IS NOT NULL"
                    . " AND(DATE(sj.bf_received_date) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')),0) as total_leads";

            $total_leads_allocation_count = "Coalesce((SELECT COUNT(lead_aloc.id)  FROM tbl_lead_allocations lead_aloc"
                    . " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead_aloc.lead_id"
                    . " WHERE (DATE(lead_aloc.created_at) BETWEEN '" . $this->start_date . "' AND '" . $this->end_date . "')"
                    . " AND lead_aloc.created_by IN(" . $value['user_id'] . ")),0)  as total_leads_allocation_count";

            $sql = "SELECT lead.id,"
                    . "$won_total_kws,"
                    . "$higbay_select,"
                    . "$flood_light_select,"
                    . "$panel_light_select,"
                    . "$batten_light_select,"
                    . "$led_proposal_count,"
                    . "$solar_proposal_count,"
                    . "$won_leads_count,"
                    . "$total_leads_count,"
                    . "$total_leads_allocation_count"
                    . " from tbl_leads lead LIMIT 1";
            
            //echo $sql;die;
            $query = $this->db->query($sql);
            $response = $query->row_array();

            $response['full_name'] = $value['full_name'];
            $response['state'] = $value['state_postal'];

            $data['sales_stats'][$key] = $response;
        }

        return $data;
    }
    
    
    public function manage_facebook_leads_report() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $user_cond = "";
        
        // if($_SERVER['REMOTE_ADDR'] == '171.79.178.241'){
        //     print_r($this->components->is_lead_allocation_rep());
        //     die;
        // }
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) { 
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND leads.user_id IN(" . $user_id . ")";
        }else if($this->is_team_leader){
            $user_id = $this->aauth->get_user_id();
            $users = $this->user->get_users(FALSE, FALSE, $user_id);
            $all_users = array_column($users, 'user_id');
            $all_users = implode(',', $all_users);
            $all_users = (isset($all_users) && $all_users != '') ? ',' . $all_users : '';
            $user_cond = " AND ltu.user_id IN(" . $user_id . $all_users . ")";
            
        } else {
            redirect('admin/dashboard');
            exit();
        }

        if ($this->userid != '') {
            $this->user_cond = " AND leads.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $data['lead_stages'] = $this->common->fetch_orderBy('tbl_lead_stages', '*',array('status' => 1), 'order_no', 'ASC', NULL);
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('report/fb_leads_report');
        $this->load->view('partials/footer');
    }
    
    
    public function manage_won_leads_report() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $user_cond = "";
        if ($this->aauth->is_member('Admin')) {
            if ($this->userid != '') {
                $user_cond = " AND ltu.user_id IN(" . $this->userid . ")";
            }
        } else if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) { 
            $user_id = $this->aauth->get_user_id();
            $user_cond = " AND leads.user_id IN(" . $user_id . ")";
        } else {
            redirect('admin/dashboard');
            exit();
        }

        if ($this->userid != '') {
            $this->user_cond = " AND leads.user_id IN(" . $this->userid . ")";
        } else if ($this->user_cond == "") {
            $this->user_cond = $user_cond;
        }

        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $data['query_params'] = http_build_query($this->input->get());
        
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('report/won_leads_report');
        $this->load->view('partials/footer');
    }
    
    public function fetch_won_lead_data() {
        $data = array();
        $data['lead_data'] = [];

        $user_id = $this->input->get('user_id');
        if($user_id != '0'){
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id=$user_id $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        }else{
            $state_cond = ($this->state_id != '' && $this->state_id != '0') ? 'AND ul.state_id=' . $this->state_id : '';
            $cond = " WHERE ud.user_id!=1 $state_cond AND user.id NOT IN (1,3,4,23,24,28) AND user_group.group_id IN(6,7) AND user.banned=0 GROUP BY user.id ORDER BY state.state_postal DESC";
            $all_users = $this->user->get_users(FALSE, FALSE, FALSE, $cond);
        }
        
        $all_users = array_column($all_users, 'user_id');
        $all_users = implode(',', $all_users);
        $all_users = (isset($all_users) && $all_users != '') ? $all_users : '';
        $user_cond = " AND ltu.user_id IN(" . $all_users . ")";
        
        $sql = "SELECT leads.*,"
                . "ltu.user_id as franchise_id,"
                . "cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_name as customer_state,state.state_postal as customer_state_postal,"
                . "user.email as franchise_email,ud.full_name as franchise_name,ud.company_name as franchise_company_name,ud.company_contact_no as franchise_contact_no,"
                . "ul.address as franchise_address,"
                . "ud.full_name as created_by,"
                . "COALESCE(ls.stage_name,'New Lead') as deal_stage_name"
                . " FROM tbl_leads as leads";
            $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
            $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
            $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
            $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
            $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
            $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
            $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
            $sql .= " LEFT JOIN tbl_lead_stages as ls ON ls.id = leads.lead_stage";
            $sql .= " LEFT JOIN tbl_led_proposal as lp ON lp.lead_id = leads.id";
            
            $sql .= " WHERE 1=1 $user_cond 
                      AND (DATE(leads.created_at) BETWEEN '" . $this->start_date . "' 
                      AND '" . $this->end_date . "') AND (leads.lead_stage=" . WON_STAGE . ") GROUP BY leads.uuid";
            
        $query = $this->db->query($sql);
        $response = $query->result_array();
        
        $data['lead_data'] = $response;

        echo json_encode($data);
        die;
    }

}

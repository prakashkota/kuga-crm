<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
		input[type=checkbox] {
			-moz-appearance:none;
			-webkit-appearance:none;
			-o-appearance:none;
			outline: none;
			content: none;	
		}

		input[type=checkbox]:before {
			font-family: "FontAwesome";
			content: "\f00c";
			font-size: 25px;
			color: transparent !important;
			background: #fff;
			display: block;
			width: 20px;
			height: 20px;
			border: 2px solid black;
			margin-right: 5px;
		}

		input[type=checkbox]:checked:before {
			color: black !important;
		}
	</style>
</head>

<body style="padding:0; margin:0">
	
	<!-- Page 1 -->

	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td valign="top"><img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/01.jpg" width="910" height="1287" alt=""/></td>
			</tr>
		</tbody>
	</table>


	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/header.jpg" width="910" height="127" alt=""/></td>
			</tr>
			<tr>
				<td height="25"></td>
			</tr>
			<tr>
				<td valign="top"><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="35" style="color: #922a27; font-size:20px;"><strong>Site Assessor Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="400" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Name</td>
												</tr>
												<tr>
													<td><input type="text" name="site_assessor_form_details[name]" value="<?php echo $name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td width="400" valign="top"><table align="right" width="370" border="0"  cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Phone number</td>
												</tr>
												<tr>
													<td><input type="text"  name="site_assessor_form_details[contact_no]" value="<?php echo $contact_no; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Business name</td>
												</tr>
												<tr>
													<td><input type="text"  name="site_assessor_form_details[business_name]" value="<?php echo $business_name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Site address</td>
												</tr>
												<tr>
													<td><input type="text" name="site_assessor_form_details[address]" value="<?php echo $address; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">ABN</td>
												</tr>
												<tr>
													<td><input type="text"  name="site_assessor_form_details[abn]" value="<?php echo $abn; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Date of site assessment</td>
												</tr>
												<tr>
													<td><input type="text"  name="site_assessor_form_details[date]" value="<?php echo $date; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td height="40" style="color: #000000; font-size:12px;"><strong>Select all of the activities that will be implemented at the site for which the Accredited Certificate Provider is accredited to create Energy Savings Certificates.</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="113" height="25" style="color: #000000; font-size:12px;"><strong>Activity</strong></td>
										<td width="512" style="color: #000000; font-size:12px;">
											<strong>Definition</strong>
										</td>
										<td width="175" align="center" style="color: #000000; font-size:12px;">
											<strong>Tick which apply</strong>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E1</td>
										<td style="color: #000000; font-size:12px;">Replace halogen downlight with an LED luminaire and/or lamp</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[0])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E2</td>
										<td style="color: #000000; font-size:12px;">Replace a linear halogen floodlight with a high efficiency lamp</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[1])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E3</td>
										<td style="color: #000000; font-size:12px;">Replace parabolic aluminised reflector (PAR) lamp with efficient luminaire and/or lamp</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[2])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E4</td>
										<td style="color: #000000; font-size:12px;">Replace a T8 or T12 luminaire with a T5 luminaire</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[3])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E5</td>
										<td style="color: #000000; font-size:12px;">Replace a T8 or T12 luminaire with an LED luminaire</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[4])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td height="25" style="color: #000000; font-size:12px;">E11</td>
										<td style="color: #000000; font-size:12px;">Replace an Edison screw or bayonet lamp with an led lamp for general lighting purposes</td>
										<td align="center" style="color: #000000; font-size:12px;">
											<span style="font-size:12px; color:#6a6a6a;">
												<?php if(isset($is_activities[5])) { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } else { ?>
													<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
												<?php } ?>
											</span>
										</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Site Assessor Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:10px"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:18px;"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="17"style="font-size:12px; color:#000000;">I, </td>
										<td width="391"><input type="text" name="site_assessor_form_details[name1]" value="<?php echo $name1; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
										<td width="392" style="font-size:12px; color:#000000;">, hereby declare that:</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="16"><img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td width="784" style="font-size:12px; color:#000000;">I have undertaken the site assessment of the above site.</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td valign="top" style="font-size:12px; color:#000000;">The activities being implemented meet the eligibility requirements specified in Schedules D and E of the ESS Rule and I have collected<br />
										records to support this (as specified in the relevant table of the Home Energy Efficiency Retrofits Method Guide).</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td style="font-size:12px; color:#000000;">The information I have provided is complete and accurate and I am aware that there are penalties for providing false or misleading<br />
										information in this form.</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td style="font-size: 12px; color: #000000"><strong>Note:</strong></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly
							providing false or misleading information to the Scheme Administrator.</td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td valign="top">
								<table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:20px">Signature</td>
										</tr>
										<tr>
											<td style="border:1px solid lightgray;">
												<img style="height:65px; " src="<?php echo site_url('assets/uploads/led_booking_form_files/'.$signature); ?>" />
											</td>
										</tr>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:20px">Name</td>
										</tr>
										<tr>
											<td><input type="text" name="site_assessor_form_details[name2]" value="<?php echo $name2; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
										</tr>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:20px">Position</td>
										</tr>
										<tr>
											<td><input type="text" name="site_assessor_form_details[name2]" value="<?php echo $position; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
										</tr>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:20px">Date</td>
										</tr>
										<tr>
											<td><input type="text" name="site_assessor_form_details[date1]" value="<?php echo $date1; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:30px; padding-left:5px; padding-right:5px"/></td>
										</tr>
									</tbody>
								</table></td>
							</tr>
							<tr>
								<td style="font-size:12px; color:#6a6a6a;">*This form must be signed by the authorised signatory <span style="font-style: italic">on or before </span>the completion of the installation</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<img src="<?php echo $this->config->item('live_url'); ?>assets/site_assessor_form/bottom.jpg" width="910" height="85" alt=""/>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
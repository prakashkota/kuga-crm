

function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var residentail_solar_proposal_calculator = function (options) {

    var self = this;
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.proposal_id = (options.proposal_id && options.proposal_id != '') ? options.proposal_id : self.uuid();
    this.lead_id = (options.lead_id && options.lead_id != '') ? options.lead_id : '';
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.user_type = options.user_type;
    this.proposal_image = '';
    this.cust_id = '';
    this.system_size = 0;
    this.no_of_panels = 0;
    this.prd_panel = 0;
    this.prd_inverter = 0;
    this.prd_battery = 0;
    this.prd_panel_price = 0;
    this.prd_inverter_price = 0;
    this.prd_battery_price = 0;
    this.system_total = 0;
    this.prev_system_total = 0;
    this.prev_total_cost = 0;
    this.custom_quote_total_inGst = 0;
    this.stored_custom_quote_total_inGst = 0;
    this.racking_price = 0;
    this.kliplock_price = 0;
    this.additional_cost_item_price = 0;
    this.line_item_price = 0;
    this.total_cost = 0;
    this.original = {};
    this.total_cost_incGst = 0;
    this.postcode_rating = 0;
    this.stc_deduction = 0;
    this.stc_price_assumption = options.stc_price_assumption;
    this.certificate_margin = options.certificate_margin;
    this.company_markup_price = 0;
    this.company_markup = 0;
    this.additional_margin = 0;
    this.line_item_data = [];
    this.activity_action = '';
    this.deal_won_lost = '';
    this.prev_deal_won_lost = '';
    this.deal_stage = '';
    this.prev_deal_stage = '';
    this.deal_stage_name = '';
    this.prev_deal_stage_name = '';
    this.proposal_stages = options.proposal_stages;
    this.additional_cost_items_wrapper = 'additional_cost_items_wrapper'
    this.additional_cost_items_btn = 'add_additional_cost_items';
    this.line_items_wrapper = 'line_items_wrapper'
    this.line_items_btn = 'add_line_items';
    this.product_type_selector = 'additional_product_type';
    this.line_item_selector = 'line_items';
    this.postcode = '';
    this.is_vic_rebate = false;
    this.is_vic_loan = false;
    this.vic_rebate = 0;
    this.vic_loan = 0;
    this.deal_type = '';
    this.quote_total_init = false;
    this.gst = 1.1;
    this.near_map_panels = {};
    this.saving_calculation_details = '';
    this.po_stage = '';


    if (Object.keys(self.lead_data).length > 0) {
        self.cust_id = self.lead_data.cust_id;
        $('#cust_id').val(self.lead_data.cust_id);
        $('#site_id').val(self.lead_data.location_id);
        $('.vic').removeClass('hidden');
        if(self.lead_data.state_id != '7'){
            $('.vic').addClass('hidden');
        }

        $('#companyName').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#dd_cname').html(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('input[name=customer_email').val(self.lead_data.customer_email);
        $('input[name=customer_phone').val(self.lead_data.customer_contact_no);
        $('input[name=customer_address').val(self.lead_data.address);
        $('input[name=customer_postcode').val(self.lead_data.postcode);
        $('#sc_postcode').val(self.lead_data.postcode);
    }

    $('.deal_type').click(function () {
        $('.solar-only-list').removeClass('is-invalid');
        $('.deal_type').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        document.getElementById('proposal_deal_type').value = id;
        self.deal_type = id;

        //Manage Savings Calculation Form
        $('#sc_solar').addClass('hidden');
        $('#sc_solar_battery').addClass('hidden');
        $('#warranty_sb').removeClass('hidden');
        $('#warranty_b').addClass('hidden');
        if (id == '1') {
            $('#sc_solar').removeClass('hidden');
        } else if (id == '2') {
            $('#sc_solar_battery').removeClass('hidden');
        }else{
            $('#warranty_sb').addClass('hidden');
            $('#warranty_b').removeClass('hidden');
        }

        $('#tariff_type').change(function () {
            $('#tou_rate_section').removeClass('hidden');
            $('#sc_sb_flat_rate').attr('readonly', 'readonly');
            if ($('#tariff_type').val() == '1') {
                $('#tou_rate_section').addClass('hidden');
                $('#sc_sb_flat_rate').removeAttr('readonly');
            }
        });
        if ($('#tariff_type').val() == '1') {
            $('#tou_rate_section').addClass('hidden');
            $('#sc_sb_flat_rate').removeAttr('readonly');
        }
    });


    if (options.proposal_id && options.proposal_id != '') {
        $.isLoading({
            text: "Loading"
        });
        setTimeout(function () {
            self.fetch_system_item();
        }, 1000);
        self.calculate_system_size();
        setTimeout(function () {
            self.set_default_values_to_savings_calculatior();
            self.load_proposal();
        }, 1000);
    } else {
        setTimeout(function () {
            $('#customer_postcode').trigger('change');
            self.fetch_system_item();
        }, 1000);
        self.calculate_system_size();
        self.create_quote_total();
        self.set_default_values_to_savings_calculatior();
    }


    $('.roof_type_no_of_panels').on('change', function () {
        self.fetch_line_item_data();
        if (window.proposal_calculator_obj.deal_type == '1') {
            window.proposal_calculator_obj.savings_calculator_only_solar(false);
        } else if (window.proposal_calculator_obj.deal_type == '2') {
            window.proposal_calculator_obj.savings_calculator_only_solar_battery(false);
        } else {
            window.proposal_calculator_obj.savings_calculator_only_solar(false);
        }
    });


    $('#customer_postcode').on('change', function () {
        if($(this).val() != ''){
            self.fetch_postcode_rating(($(this).val()));
        }
    });

    $('#prd_panel,#system_size').change(function () {
        self.fetch_line_item_data();
    });

    $('#save_proposal').click(function () {
        self.save_proposal();
    });

    $('form button').on("click", function (e) {
        e.preventDefault();
    });

    $('#addNewCompany').click(function () {
        $('#add_new_company_modal').modal('show');
        $('#company_cust_id').val('');
    });

    $('#create_pdf').click(function (e) {
        self.save_proposal(true);
    });

  
    self.fetch_products();

    //self.autocomplete_customer();
    $('#contactEmailId').change(function () {
        var data = {};
        data.email = document.getElementById('contactEmailId').value;
        self.validate_customer(data);
    });

    $('#contactMobilePhone').change(function () {
        var data = {};
        data.contact_no = document.getElementById('contactMobilePhone').value;
        self.validate_customer(data);
    });

    $('#locationstreetAddressVal').change(function () {
        var data = {};
        data.address = document.getElementById('locationstreetAddressVal').value;
        self.validate_customer(data);
    });

    $('#locationPostCode').change(function () {
        self.postcode = document.getElementById('locationPostCode').value;
        $('#sc_postcode').val(self.postcode);
        //self.fetch_franchise_list();
    });
    
    $('.is_vic_rebate').click(function () {
        $('.is_vic_rebate').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        document.getElementById('is_vic_rebate').value = id;
        if (id == '1') {
            self.is_vic_rebate = true;
            $('#vic_rebate').parent().parent().removeClass('hidden');
            //$('#vic_rebate_row').removeClass('hidden');
        } else {
            self.is_vic_rebate = false;
            $('#vic_rebate').parent().parent().addClass('hidden');
            //$('#vic_rebate_row').addClass('hidden');
            self.vic_rebate = 0;
            $('#vic_rebate').val(self.vic_rebate);
        }
        if (self.quote_total_init) {
            self.calculate_sum();
        }
        //self.calculate_payment_summary();
    });
    
    $('.is_vic_loan').click(function () {
        $('.is_vic_loan').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        document.getElementById('is_vic_loan').value = id;
        if (id == '1') {
            self.is_vic_loan = true;
            $('#vic_loan').parent().parent().removeClass('hidden');
            //$('#vic_rebate_row').removeClass('hidden');
        } else {
            self.is_vic_loan = false;
            $('#vic_loan').parent().parent().addClass('hidden');
            //$('#vic_rebate_row').addClass('hidden');
            self.vic_loan = 0;
            $('#vic_loan').val(self.vic_loan);
        }
        if (self.quote_total_init) {
            self.calculate_sum();
        }
        //self.calculate_payment_summary();
    });

    $('#sc_postcode,#sc_system_size,#sc_electricity_rate,#sc_export_tariff,#sc_percentage_export').change(function () {
        self.savings_calculator_only_solar(true);
    });

    $('#sc_sb_area,#tariff_type,.sc_sb_input').change(function () {
        self.savings_calculator_only_solar_battery(true);
    });


    $('.stc_calculate_inputs').change(function () {
        self.calculate_stc_rebate(true);
    });

    $('#save_stc_btn').click(function () {
        //self.calculate_payment_summary(true);
        self.calculate_sum();
        $('#stc_calculation_modal').modal('hide');
        self.calculate_stc_rebate(true);
    });

    /**$('#system_total,#vic_rebate,#price_before_stc_rebate').change(function () {
        self.calculate_payment_summary();
        self.save_proposal();
    });*/

    self.create_additional_items();
    self.remove_additional_items();
    self.remove_line_items();
    self.upload_proposal_image();
    self.show_image();
    self.hide_image();
};

residentail_solar_proposal_calculator.prototype.set_default_values_to_savings_calculatior = function () {
    document.querySelector('.deal_type_1').click();
    document.querySelector('#sc_electricity_rate').value = 0.25;
    document.querySelector('#sc_export_tariff').value = 0.11;
    document.querySelector('#sc_percentage_export').value = 50;
    
    document.querySelector('#sc_sb_flat_rate').value = 0.25;
    document.querySelector('#sc_sb_export_tariff').value = 0.11;
    document.querySelector('#sc_sb_percentage_export').value = 50;
};

residentail_solar_proposal_calculator.prototype.savings_calculator_only_solar = function (validate) {
    var self = this;
    var postcode = $('#sc_postcode').val();
    var system_size = $('#sc_system_size').val();
    var er = $('#sc_electricity_rate').val();
    var ext = $('#sc_export_tariff').val();
    var pex = $('#sc_percentage_export').val();
    

    if (postcode == '' || system_size == '' || er == '' || ext == '' || pex == '') {
        if (validate) {
            toastr.clear();
            toastr['error']('Please enter all required values');
            return false;
        }
    }
    
    var data = {};
    data.sc_type = 1;
    data.postcode = postcode;
    data.total_system_size = system_size;
    data.electricity_rate = er;
    data.feed_in_tariff = ext;
    data.export = pex;
    data.lead_id = self.lead_id;
    data.panel_data = self.near_map_panels;
    self.fetch_solar_savings_data(data);
};

residentail_solar_proposal_calculator.prototype.savings_calculator_only_solar_battery = function (validate) {
    var self = this;
    var sc_sb_area = $('#sc_sb_area').val();
    var tariff_type = $('#tariff_type').val();
    var sc_sb_flat_rate = $('#sc_sb_flat_rate').val();
    var system_size = $('#sc_sb_system_size').val();
    var bs = $('#sc_sb_battery_size').val();
    var ext = $('#sc_sb_export_tariff').val();
    var pex = $('#sc_sb_percentage_export').val();
    

    if (sc_sb_area == '' || tariff_type == '' || system_size == '' || bs == '' || ext == '' || pex == '') {
        if (validate) {
            toastr.clear();
            toastr['error']('Please enter all required values');
            return false;
        }
    }

    var sc_sb_tr_peak = $('#sc_sb_tr_peak').val();
    var sc_sb_tr_off_peak = $('#sc_sb_tr_off_peak').val();
    var sc_sb_tr_shoulder = $('#sc_sb_tr_shoulder').val();

    var sc_sb_er_peak = $('#sc_sb_er_peak').val();
    var sc_sb_er_off_peak = $('#sc_sb_er_off_peak').val();
    var sc_sb_er_shoulder = $('#sc_sb_er_shoulder').val();

    var total_tr_rate = parseFloat(sc_sb_tr_peak) + parseFloat(sc_sb_tr_off_peak) + parseFloat(sc_sb_tr_shoulder);
    if (tariff_type == '2') {
        if (total_tr_rate > 100 || total_tr_rate < 100) {
            if (validate) {
                toastr.clear();
                toastr['error']('TOU Rate total is more than or less than 100% please check and correct that.');
            }
            return false;
        }
    }

    var data = {};
    data.sc_type = 2;
    data.sc_sb_area = sc_sb_area;
    data.tariff_type = tariff_type;

    data.total_system_size = system_size;
    data.battery_size = bs;
    data.feed_in_tariff = ext;
    data.export = pex;
    data.lead_id = self.lead_id;
    data.flat_rate = sc_sb_flat_rate;
    data.tr_rate = {'peak': sc_sb_tr_peak, 'off_peak': sc_sb_tr_off_peak, 'shoulder': sc_sb_tr_shoulder};
    data.er_rate = {'peak': sc_sb_er_peak, 'off_peak': sc_sb_er_off_peak, 'shoulder': sc_sb_er_shoulder};
    data.panel_data = self.near_map_panels;
    self.fetch_solar_savings_data(data);
};

residentail_solar_proposal_calculator.prototype.create_pdf = function () {
    var self = this;
    var url = base_url + 'admin/product/residential_solar/proposal_pdf_download/' + self.proposal_id + '?view=html';
    console.log(url);
    document.getElementById('create_pdf_download').setAttribute('href', url);
    document.getElementById('create_pdf_download').click();
};

residentail_solar_proposal_calculator.prototype.uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

residentail_solar_proposal_calculator.prototype.save_proposal_validate = function () {
    var self = this;
    var flag = true;
    $('#prd_panel').removeClass('is-invalid');
    $('#system_size').removeClass('is-invalid');
    $('#companyName').removeClass('is-invalid');
    $('#customer_postcode').removeClass('is-invalid');
    $('.solar-only-list').removeClass('is-invalid');

    var cust_id = $('#cust_id').val();
    var site_id = $('#site_id').val();
    var proposal_deal_type = $('#proposal_deal_type').val();
    if (self.prd_panel_price == 0 || self.prd_panel_price == null) {
        //flag = false;
        //$('#prd_panel').addClass('is-invalid');
    }
    if (self.prd_inverter_price == 0 || self.prd_inverter_price == null) {
        //flag = false;
        //$('#prd_inverter').addClass('is-invalid');
    }
    if (self.system_size == 0 || self.system_size == null) {
        //flag = false;
        //$('#system_size').addClass('is-invalid');
    }
    if (cust_id == '' || site_id == '') {
        flag = false;
        $('#companyName').addClass('is-invalid');
        $('#customer_postcode').addClass('is-invalid');
    }

    if (proposal_deal_type == '') {
        flag = false;
        $('.solar-only-list').addClass('is-invalid');
    }

    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
    }
    return flag;
};

residentail_solar_proposal_calculator.prototype.load_proposal = function () {
    var self = this;
    var uuid = self.proposal_id;
    var form_data = 'uuid=' + uuid + '&action=load';
    $('#loader').html(createLoader('Please wait while data is being loaded'));
    $.ajax({
        url: base_url + 'admin/product/residential_solar/proposal_load',
        type: 'post',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            $.isLoading("hide");
            $('#loader').html('');
            $('#save_proposal').removeAttr('disabled');
            if (response.success == true) {
                self.cust_id = response.proposal.cust_id;
                self.fetch_system_item(response);
            }
        }
    });
};

residentail_solar_proposal_calculator.prototype.handle_proposal_data = function (data) {
    var self = this;
    //Populate System Details
    //Handle Customer Data
    $('#cust_id').val(data.proposal.cust_id);
    var agent_id = (data.proposal.agent_id != '' && data.proposal.agent_id != null) ? data.proposal.agent_id : '';
    $('#agent_id').val(agent_id);
    $('#site_id').val(data.proposal.site_id);
    $('#companyName').val(data.proposal.first_name + ' ' + data.proposal.last_name);
    $('#dd_cname').html(data.proposal.first_name + ' ' + data.proposal.last_name);
    $('input[name=customer_phone').val(data.proposal.customer_contact_no);
    $('input[name=customer_address').val(data.proposal.address);
    $('input[name=customer_postcode').val(data.proposal.postcode).trigger('change');
    //Convert Add New Customer to Edit customer
    $('#addNewCompany').addClass('hidden');
    $('#editCompany').removeClass('hidden');
    $('#companyName').attr('data-item', JSON.stringify(data.proposal));

    $('.placecomplete').val(data.proposal.address).change();

    //Handle Image
    if (data.proposal.image != '') {
        self.proposal_image = data.proposal.image;
        $('#image').val(self.proposal_image);
        self.show_image();
    }
    self.stored_custom_quote_total_inGst = parseFloat(data.proposal.quote_total_inGst).toFixed(2);
    $('#no_of_stories').val(data.proposal.no_of_stories).trigger('change');
    $('#phase').val(data.proposal.phase).trigger('change');
    $('#comments').val(data.proposal.comments).trigger('change');

    //Handle Lead Type
    if (data.proposal.lead_type != 'null') {
        $('.deal_type_' + data.proposal.lead_type).click();
    }else{
        $('.deal_type_1').click();
    }
    //Handle Vic Rebate
    if (data.proposal.is_vic_rebate == '1') {
        $('.is_vic_rebate_1').click();
    }
    
    if (data.proposal.is_vic_loan == '1') {
        $('.is_vic_loan_1').click();
    }
    
    //Populate Cost Summary
    self.system_total = self.prev_system_total = data.proposal.system_total;
    self.stc_deduction = (data.proposal.stc_deduction && isFinite(data.proposal.stc_deduction)) ? data.proposal.stc_deduction : 0;
    self.vic_rebate = (data.proposal.vic_rebate && isFinite(data.proposal.vic_rebate)) ? data.proposal.vic_rebate : 0;
    self.vic_loan = (data.proposal.vic_loan && isFinite(data.proposal.vic_loan)) ? data.proposal.vic_loan : 0;
    var total_payable = (parseFloat(self.system_total) - (parseFloat(self.stc_deduction) + parseFloat(self.vic_rebate) + + parseFloat(self.vic_loan)));
    self.total_cost_incGst = (total_payable && isFinite(total_payable)) ? parseFloat(total_payable).toFixed(2) : 0;
    self.create_quote_total();

    //Populate Savings Calcualtion
    if (data.proposal.saving_calculation_details != null && data.proposal.saving_calculation_details.hasOwnProperty('sc_type')) {
        if (data.proposal.saving_calculation_details.sc_type == '1') {
            $('#sc_postcode').val(data.proposal.saving_calculation_details.postcode);
            $('#sc_system_size').val(data.proposal.saving_calculation_details.total_system_size);
            $("#system_size").val(data.proposal.saving_calculation_details.total_system_size);
            if(data.proposal.saving_calculation_details.electricity_rate != 0){
                $('#sc_electricity_rate').val(data.proposal.saving_calculation_details.electricity_rate);
            }
            if(data.proposal.saving_calculation_details.feed_in_tariff != 0){
                $('#sc_export_tariff').val(data.proposal.saving_calculation_details.feed_in_tariff);
            }
            if(data.proposal.saving_calculation_details.export != 0){
                $('#sc_percentage_export').val(data.proposal.saving_calculation_details.export);
            }

            var postcode = $('#sc_postcode').val();
            var system_size = $('#sc_system_size').val();
            var er = $('#sc_electricity_rate').val();
            var ext = $('#sc_export_tariff').val();
            var pex = $('#sc_percentage_export').val();

            if (postcode != '' && system_size != '' && er != '' && ext != '' && pex != '') {
                $('#sc_percentage_export').trigger('change');
            }
            self.savings_calculator_only_solar(false);
            
        } else if (data.proposal.saving_calculation_details.sc_type == '2') {

            $('#sc_sb_area').val(data.proposal.saving_calculation_details.sc_sb_area);
            $('#tariff_type').val(data.proposal.saving_calculation_details.tariff_type);
            $('#sc_sb_flat_rate').val(data.proposal.saving_calculation_details.flat_rate);
            $('#sc_sb_system_size').val(data.proposal.saving_calculation_details.total_system_size);
            $("#system_size").val(data.proposal.saving_calculation_details.total_system_size);
            $('#sc_sb_battery_size').val(data.proposal.saving_calculation_details.battery_size);
            $('#sc_sb_export_tariff').val(data.proposal.saving_calculation_details.feed_in_tariff);
            $('#sc_sb_percentage_export').val(data.proposal.saving_calculation_details.export);

            if (data.proposal.saving_calculation_details.hasOwnProperty('tr_rate')) {
                $('#sc_sb_tr_peak').val(data.proposal.saving_calculation_details.tr_rate.peak);
                $('#sc_sb_tr_off_peak').val(data.proposal.saving_calculation_details.tr_rate.off_peak);
                $('#sc_sb_tr_shoulder').val(data.proposal.saving_calculation_details.tr_rate.shoulder);

                $('#sc_sb_er_peak').val(data.proposal.saving_calculation_details.er_rate.peak);
                $('#sc_sb_er_off_peak').val(data.proposal.saving_calculation_details.er_rate.off_peak);
                $('#sc_sb_er_shoulder').val(data.proposal.saving_calculation_details.er_rate.shoulder);
            }

            var sc_sb_area = $('#sc_sb_area').val();
            var tariff_type = $('#tariff_type').val();
            var system_size = $('#sc_sb_system_size').val();
            var bs = $('#sc_sb_battery_size').val();
            var ext = $('#sc_sb_export_tariff').val();
            var pex = $('#sc_sb_percentage_export').val();
            
            if (sc_sb_area != '' || tariff_type != '' || system_size != '' || bs != '' || ext != '' || pex != '') {
                $('#tariff_type').trigger('change');
            }
            self.savings_calculator_only_solar_battery(false);
        }
    }

    //Handle Warranty data
    var warranty_details = data.proposal.warranty_details;
    if(self.deal_type == '3'){
        if(warranty_details != ''){
            warranty_details = warranty_details.split(';');
            document.getElementById('b_warranty_details_1').value = warranty_details[0];
        }
    }else{
        if(warranty_details != '' && warranty_details != null && warranty_details != 'null'){
            warranty_details = warranty_details.split(';');
            document.getElementById('sb_warranty_details_1').value = warranty_details[0];
            document.getElementById('sb_warranty_details_2').value = warranty_details[1];
            document.getElementById('sb_warranty_details_3').value = warranty_details[2];
            document.getElementById('sb_warranty_details_4').value = warranty_details[3];
        }
    }

    //Set Price Assumption
    $("#total_payable_exGST").val(parseFloat(data.proposal['quote_total_exGst']).toFixed(2));
    $("#total_payable_inGST").val(parseFloat(data.proposal['quote_total_exGst'] * self.gst).toFixed(2));

    $("#stc_deduction").val(self.stc_deduction);
    $("#stc_deduction_inGST").val(parseFloat(self.stc_deduction * self.gst).toFixed(2));

    $("#vic_rebate").val(self.vic_rebate);
    $("#vic_rebate_inGST").val(parseFloat(self.vic_rebate * self.gst).toFixed(2));
    
    $("#vic_loan").val(self.vic_loan);
    $("#vic_loan_inGST").val(parseFloat(self.vic_loan * self.gst).toFixed(2));
    
    var PriceBeforeSTCRebate = 
    parseFloat(data.proposal['quote_total_exGst']).toFixed(2) - 
    parseFloat(self.stc_deduction).toFixed(2) -
    parseFloat(self.vic_rebate).toFixed(2);

    $("#price_before_stc_rebate").val(parseFloat(PriceBeforeSTCRebate).toFixed(2));
    $("#price_before_stc_rebate_inGST").val(parseFloat(PriceBeforeSTCRebate * self.gst).toFixed(2));
};

residentail_solar_proposal_calculator.prototype.handle_proposal_item_data = function (data) {
    var self = this;
    //Add Panel,Inverter,Battery and additional data
    var proposal_data = data.proposal_data;
    for (var i = 0; i < proposal_data.length; i++) {
        if (proposal_data[i]['type_id'] == '2' && proposal_data[i]['item_type'] == '1') { //Panel
            $('#prd_panel').val(proposal_data[i]['item_id']).trigger('change');
            //self.prd_panel = proposal_data[i]['item_id'];
        } else if (proposal_data[i]['type_id'] == '1' && proposal_data[i]['item_type'] == '1') { //Inverter
            //self.prd_inverter = proposal_data[i]['item_id'];
            $('#prd_inverter').val(proposal_data[i]['item_id']).trigger('change');
        } else if (proposal_data[i]['type_id'] == '3' && proposal_data[i]['item_type'] == '1') { //Battery
            //self.prd_battery = proposal_data[i]['item_id'];
            $('#prd_battery').val(proposal_data[i]['item_id']).trigger('change');
        } else {
            self.create_additional_items(proposal_data[i]);
        }
    }

    var rt_nfp = data.proposal.roof_type_no_of_panels.split(';');
    $('.roof_type_no_of_panels').each(function (index) {
        $(this).val(rt_nfp[index])
    });


    var proposal_line_item_data = data.proposal_line_item_data;
    self.line_item_data = proposal_line_item_data;
    for (var i = 0; i < proposal_line_item_data.length; i++) {
        self.create_line_items(proposal_line_item_data[i]);
    }
    self.calculate_racking_price();
    self.fetch_line_item_data();
};

residentail_solar_proposal_calculator.prototype.calculate_racking_price = function () {
    var self = this;
    var prd_panel = document.getElementById('prd_panel');
    if (prd_panel.value == '' || prd_panel.value == null
            || prd_panel.value == 'Please Fill Customer Details First' || prd_panel.value == 'Select Panel') {
        return false;
    }
    $.ajax({
        url: base_url + 'admin/product/residential_solar/fetch_racking_item_data',
        type: 'post',
        data: $('form').serialize() + '&action=fetch_racking_item_data',
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                if (response.racking_data.length > 0) {
                    var racking_price = 0;
                    for (var i = 0; i < response.racking_data.length; i++) {
                        var data = response.racking_data[i];
                        var is_tile_tin = data.name.search("Tin/Tile");
                        var is_tilt = data.name.search("Tilt");
                        var roof_types = document.getElementsByClassName('roof_type_no_of_panels');
                        var tile_tin_nfp = parseInt(roof_types[0].value) + parseInt(roof_types[1].value);
                        var tilt_nfp = parseInt(roof_types[2].value);

                        if (is_tile_tin != -1) {
                            racking_price = parseFloat(racking_price) + parseFloat(data.variant_cp) * tile_tin_nfp;
                        } else if (is_tilt != -1) {
                            racking_price = parseFloat(racking_price) + (parseFloat(data.variant_cp) * tilt_nfp);
                        }
                    }

                    self.racking_price = (racking_price && !isNaN(racking_price)) ? racking_price.toFixed(2) : parseFloat(0).toFixed(2);
                    self.calculate_sum();
                    //self.create_quote_total();
                } else {
                    self.racking_price = parseFloat(0).toFixed(2);
                    self.calculate_sum();
                    //self.create_quote_total();
                }
            }
            setTimeout(function () {
                if (self.stored_custom_quote_total_inGst > 0 && isFinite(self.stored_custom_quote_total_inGst)) {
                    self.custom_quote_total_inGst = parseFloat(self.stored_custom_quote_total_inGst).toFixed(2);
                    $('#custom_quote_total_inGst').val(self.stored_custom_quote_total_inGst);
                    self.calculate_sum();
                    //self.create_quote_total();
                    self.stored_custom_quote_total_inGst = 0;
                }
            }, 1000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

residentail_solar_proposal_calculator.prototype.save_proposal = function (create_pdf) {
    var self = this;
    self.calculate_sum();
    //self.create_quote_total();
    var flag = self.save_proposal_validate();
    if (flag) {
        var prd_panel = document.getElementById('prd_panel');
        var prd_panel_data = null;
        if (prd_panel.value != '' && prd_panel.value != undefined) {
            prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
        }
        var panel_variant_id = (prd_panel_data != null) ? prd_panel_data.variant_id : '';

        var prd_inverter = document.getElementById('prd_inverter');
        var prd_inverter_data = null
        if (prd_inverter.value != '' && prd_inverter.value != undefined) {
            prd_inverter_data = JSON.parse(prd_inverter.options[prd_inverter.selectedIndex].getAttribute('data-item'));
        }
        var inverter_variant_id = (prd_inverter_data != null) ? prd_inverter_data.variant_id : '';

        var prd_battery = document.getElementById('prd_battery');
        var prd_battery_data = null;
        if (prd_battery.value != '' && prd_battery.value != undefined) {
            prd_battery_data = JSON.parse(prd_battery.options[prd_battery.selectedIndex].getAttribute('data-item'));
        }
        var battery_variant_id = (prd_battery_data != null) ? prd_battery_data.variant_id : '';

        $('#save_proposal').attr('disabled', 'disabled');
        var uuid = self.proposal_id;
        var lead_id = self.lead_id;
        var warranty_details = '';
        if(self.deal_type == '3'){
            var w1 = document.getElementById('b_warranty_details_1').value;
            warranty_details = (w1 != '') ? w1 : 0;
        }else{
            var w1 = document.getElementById('sb_warranty_details_1').value;
            var w2 = document.getElementById('sb_warranty_details_2').value;
            var w3 = document.getElementById('sb_warranty_details_3').value;
            var w4 = document.getElementById('sb_warranty_details_4').value;
            w1 = (w1 != '') ? w1 : 0;
            w2 = (w2 != '') ? w2 : 0;
            w3 = (w3 != '') ? w3 : 0;
            w4 = (w4 != '') ? w4 : 0;
            warranty_details = w1 + ';' + w2 + ';' + w3 + ';' + w4;
        }

        var form_data = $('form').serialize() + '&lead_id=' + lead_id + '&uuid=' + uuid + '&action=save&panel_variant_id=' + panel_variant_id + '&inverter_variant_id=' + inverter_variant_id + '&battery_variant_id=' + battery_variant_id + '&proposal[warranty_details]=' + warranty_details + '&proposal[saving_calculation_details]='+ JSON.stringify(self.saving_calculation_details);
        $('#loader').html(createLoader('Please wait while data is being saved'));

        toastr.clear();
        
        $.ajax({
            url: base_url + 'admin/product/residential_solar/proposal_save',
            type: 'post',
            data: form_data,
            dataType: 'json',
            beforeSend: function () {
                toastr["info"]('Saving proposal data, please wait....');
            },
            success: function (response) {
                toastr.remove();
                $('#loader').html('');
                $('#save_proposal').removeAttr('disabled');
                if (response.success == true) {
                    toastr["success"]('Proposal Saved Successfully.');
                    if (create_pdf) {
                        self.create_pdf();
                    }
                } else {
                    toastr["error"]('Error! Saving the proposal.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

residentail_solar_proposal_calculator.prototype.fetch_postcode_rating = function (postcode) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/product/fetch_postcode',
        type: 'get',
        data: {
            postcode: postcode
        },
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.postcode_rating = response.postcode.rating;
                if (self.system_size < 100) {
                    //Changed from 12 to 11
                    var dt = new Date();
                    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 11;
                    var system_size = (self.system_size) ? self.system_size : 0;

                    var no_of_stc = parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size);
                    no_of_stc = Math.floor(no_of_stc);

                    var stc_deduction = self.stc_deduction;
                    stc_deduction = parseFloat(stc_deduction).toFixed(2);

                    var price_assumption = (stc_deduction / no_of_stc)
                    price_assumption = parseFloat(price_assumption).toFixed(2);

                    $('#stc_calculate_price').val(price_assumption);
                    self.calculate_sum();
                    self.calculate_stc_rebate();
                }
                if(response.solar_area != ''){
                    $('#sc_sb_area').val(response.solar_area);
                }
            } else {
                toastr["error"]('Error! While Fetching Postcode Rating.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

residentail_solar_proposal_calculator.prototype.fetch_system_item = function (data) {
    var self = this;
    var cust_id = self.cust_id;
    if(self.cust_id == ''){
        return false;
    } 
    $.ajax({
        url: base_url + 'admin/product/residential_solar/fetch_item_data',
        type: 'get',
        data: {
            cust_id: cust_id,
            action: 'fetch_item_data'
        },
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.handle_item_data(response.inverter, 1, 'prd_inverter');
                self.handle_item_data(response.panel, 2, 'prd_panel');
                self.handle_item_data(response.battery, 3, 'prd_battery');
                //self.handle_item_data(response.panel, 2, 'panel_list');
                if (data) {
                    self.handle_proposal_data(data);
                    self.handle_proposal_item_data(data);
                }
            } else {
                toastr["error"]('Error! While fetching product data.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

residentail_solar_proposal_calculator.prototype.handle_item_data = function (data, item_type, item_container) {
    var self = this;
    var option;
    item_container = document.getElementById(item_container);
    item_container.innerHTML = '';
    if (data.length > 0) {
        var item_name = (item_type == 1) ? 'Inverter' : (item_type == 2) ? 'Panel' : 'Battery';
        option = document.createElement('option');
        option.innerHTML = 'Select ' + item_name;
        item_container.appendChild(option);
        for (var i = 0; i < data.length; i++) {
            option = self.create_product_select_option(data[i]);
            item_container.appendChild(option);
        }
    } else {
        option = document.createElement('option');
        option.innerHTML = 'No Item Found with choosen Location';
        item_container.appendChild(option);
    }
};

residentail_solar_proposal_calculator.prototype.fetch_products = function () {
    var self = this;
    var product_type_selector = document.getElementById(self.product_type_selector);
    product_type_selector.addEventListener("change", function () {
        var cust_id = self.cust_id;
        $.ajax({
            url: base_url + 'admin/product/residential_solar/fetch_additional_item_data',
            type: 'get',
            data: {
                type_id: this.value,
                cust_id: cust_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    var option;
                    document.getElementById('additional_product').innerHTML = '';
                    if (response.products.length > 0) {
                        for (var i = 0; i < response.products.length; i++) {
                            option = self.create_product_select_option(response.products[i]);
                            document.getElementById('additional_product').appendChild(option);
                        }
                    } else {
                        option = document.createElement('option');
                        option.innerHTML = 'No Item Found with Selected Type and Choosen Location';
                        document.getElementById('additional_product').appendChild(option);
                    }
                } else {
                    toastr["error"]('Error! While fetching product data.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

residentail_solar_proposal_calculator.prototype.fetch_line_item_data = function () {
    var self = this;

    //Quickly claculate no of panels that will be rquired.
    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }

    var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.capacity)) : 0;
    //Get System Size
    var system_size = self.calculate_system_size();
    system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);

    var no_of_panels = (system_size * 1000) / prd_panel_size;
    //self.no_of_panels = Math.floor(no_of_panels);
    self.no_of_panels = Math.round(no_of_panels);
    var no_of_panels = (self.no_of_panels > 0 && isFinite(self.no_of_panels)) ? self.no_of_panels : 0;
    if (no_of_panels > 0) {
        $.ajax({
            url: base_url + 'admin/service/fetch_line_item_and_markup',
            type: 'POST',
            data: {
                prd_panel_data: prd_panel_data,
                no_of_panels: no_of_panels,
                action: 'fetch_line_item_data'
            },
            dataType: 'json',
            success: function (response) {
                document.getElementById(self.line_items_wrapper).innerHTML = '';
                if (response.success == true) {
                    if (response.line_item_data.length > 0) {
                        self.line_item_data = response.line_item_data;
                        for (var i = 0; i < response.line_item_data.length; i++) {
                            self.create_line_items(response.line_item_data[i]);
                        }
                    }
                    self.company_markup_price = parseFloat(0).toFixed(3);
                    if (response.company_markup.length > 0) {
                        //(Select the Company Markup that matches the state and number of panels) 
                        //and add the additional margins for all the products
                        var company_markup_price = parseFloat(response.company_markup[0].price).toFixed(3);
                        self.company_markup_price = company_markup_price;
                    }
                    self.calculate_sum();
                    //self.create_quote_total();
                } else {
                    toastr["error"]('Error! While fetching line items data.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

residentail_solar_proposal_calculator.prototype.create_product_select_option = function (data, selected) {
    var option = document.createElement('option');
    var value = data.id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    if (data.id = selected) {
        option.setAttribute('selected', 'selected');
    }
    //option.innerHTML = data.name + ' [' + data.variant_state + '] ' + '[' + data.variant_application + ']';
    option.innerHTML = data.name;
    return option;
};

residentail_solar_proposal_calculator.prototype.create_line_item_data_select_option = function (data) {
    var option = document.createElement('option');
    var value = data.id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    option.innerHTML = data.state + ' (' + data.qty_from + ' - ' + data.qty_to + ') [$' + data.price + ']';
    return option;
};

residentail_solar_proposal_calculator.prototype.create_additional_items = function (data) {
    var self = this;
    var additional_cost_items_wrapper = document.getElementById(self.additional_cost_items_wrapper);
    var additional_cost_items_btn = document.getElementById(self.additional_cost_items_btn);
    if (!data) {
        additional_cost_items_btn.addEventListener("click", function (e) { //on add input button click
            var additional_product_type_name = $("#additional_product_type option:selected").html();
            var additional_product_data_item = $("#additional_product option:selected").attr('data-item');
            var additional_product_data = JSON.parse(additional_product_data_item);
            e.preventDefault();
            $(additional_cost_items_wrapper).append('<tr>' +
                    '<td>' + additional_product_type_name + '<input type="hidden" name="additional_cost_item[]"  value="' + additional_product_data.id + '" /></td>' +
                    '<td>' + additional_product_data.name + ' [' + additional_product_data.variant_state + '] ' + '[' + additional_product_data.variant_application + ']' + '<input type="hidden" name="additional_cost_item_variant[]" value="' + additional_product_data.variant_id + '" /></td>' +
                    '<td>$' + additional_product_data.variant_cp + '</td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_qty" name="additional_cost_item_qty[]" value="1" /></td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_price" name="additional_cost_item_price[]" value="' + additional_product_data.variant_cp + '" step="0.01" /></td>' +
                    "<td><input type='text' min='1' oninput='this.value = Math.abs(this.value)'  class='form-control additional_cost_item_total' name='additional_cost_item_total[]'   data-item='" + additional_product_data_item + "'  value='" + additional_product_data.variant_cp + "' readonly /></td>" +
                    '<td><a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            //self.calculate_sum();
            //self.create_quote_total();

            $('input[name="additional_cost_item_price[]"],input[name="additional_cost_item_qty[]"]').on('change', function () {
                $('.additional_cost_item_total').each(function () {
                    var price = $(this).closest('tr').find('.additional_cost_item_price').val();
                    var qty = $(this).closest('tr').find('.additional_cost_item_qty').val();
                    var total = (parseFloat(price) * parseFloat(qty)).toFixed(2);
                    $(this).val(total);
                });
                //self.calculate_sum();
                //self.create_quote_total();
            });
        });
    } else {
        var data_item = JSON.stringify(data);
        $(additional_cost_items_wrapper).append('<tr>' +
                '<td>' + data.typeName + '<input type="hidden" name="additional_cost_item[]" value="' + data.item_id + '" /></td>' +
                '<td>' + data.name + ' [' + data.variant_state + '] ' + '[' + data.variant_application + ']' + '<input type="hidden" name="additional_cost_item_variant[]" value="' + data.variant_id + '" /></td>' +
                '<td>$' + data.variant_cp + '</td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_qty" name="additional_cost_item_qty[]" value="' + data.qty + '" /></td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_price" name="additional_cost_item_price[]" value="' + data.custom_price + '" step="0.01" /></td>' +
                "<td><input type='text' min='1' oninput='this.value = Math.abs(this.value)'  class='form-control additional_cost_item_total' name='additional_cost_item_total[]'   data-item='" + data_item + "'  value='" + data.final_price + "' readonly /></td>" +
                '<td><a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');

        //self.calculate_sum();
        //self.create_quote_total();
        $('input[name="additional_cost_item_price[]"],input[name="additional_cost_item_qty[]"]').on('change', function () {
            $('.additional_cost_item_total').each(function () {
                var price = $(this).closest('tr').find('.additional_cost_item_price').val();
                var qty = $(this).closest('tr').find('.additional_cost_item_qty').val();
                var total = (parseFloat(price) * parseFloat(qty)).toFixed(2);
                $(this).val(total);
            });
            //self.calculate_sum();
            //self.create_quote_total();
        });
    }
};

residentail_solar_proposal_calculator.prototype.remove_additional_items = function () {
    var self = this;
    var additional_cost_items_wrapper = document.getElementById(self.additional_cost_items_wrapper);
    $(additional_cost_items_wrapper).on("click", ".remove_field", function (e) { //user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
        //self.calculate_sum();
        //self.create_quote_total();
    });
};

//Line Items
residentail_solar_proposal_calculator.prototype.create_line_items = function (data) {
    var self = this;
    var line_items_wrapper = document.getElementById(self.line_items_wrapper);
    var line_items_btn = document.getElementById(self.line_items_btn);
    if (!data) {
        line_items_btn.addEventListener("click", function (e) { //on add input button click
            var line_item_data = $("#line_items option:selected").attr('data-item');
            var line_item_data_data = $("#line_items_data option:selected").attr('data-item');
            line_item_data = JSON.parse(line_item_data);
            line_item_data_data = JSON.parse(line_item_data_data);
            e.preventDefault();
            $(line_items_wrapper).append('<tr>' +
                    '<td>' + line_item_data.line_item + '(' + line_item_data.per_which_product + ')<input type="hidden" name="line_item[]" value="' + line_item_data_data.id + '" /></td>' +
                    '<td>' + line_item_data_data.state + '</td>' +
                    '<td>' + line_item_data_data.qty_from + ' - ' + line_item_data_data.qty_to + '</td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control" name="line_item_price[]" step="0.01" value="' + line_item_data_data.price + '" /></td>' +
                    '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            //self.calculate_sum();
            //self.create_quote_total();
            $('input[name="line_item_price[]"]').on('change', function () {
                //self.calculate_sum();
                //self.create_quote_total();
            });
        });
    } else {
        var price = (data.custom_price) ? data.custom_price : data.price;
        /**  If it is per System then the price will be multiplied by 1
         If it is per kilowatt then the price will be multiplied by the number of kilowatts
         If it is per panel then the price will be mulitplied by the number of pane
         */
        price = (data.per_which_product == 'Panel') ? (price * self.no_of_panels) : (data.per_which_product == 'kiloWatts') ? (price * self.system_size) : price;

        $(line_items_wrapper).append('<tr>' +
                '<td>' + data.line_item + '(' + data.per_which_product + ')<input type="hidden" name="line_item[]" value="' + data.id + '" /></td>' +
                '<td>' + data.state + '</td>' +
                '<td>' + data.qty_from + ' - ' + data.qty_to + '</td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control" name="line_item_price[]" step="0.01" value="' + price + '" readonly /></td>' +
                '<td><!--<a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a>--></td>');
        //self.calculate_sum();
        //self.create_quote_total();
        $('input[name="line_item_price[]"]').on('change', function () {
            //self.calculate_sum();
            //self.create_quote_total();
        });
    }
};

residentail_solar_proposal_calculator.prototype.remove_line_items = function () {
    var self = this;
    var line_items_wrapper = document.getElementById(self.line_items_wrapper);
    $(line_items_wrapper).on("click", ".remove_lt_field", function (e) { //user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
        //self.calculate_sum();
        //self.create_quote_total();
    });
};

residentail_solar_proposal_calculator.prototype.calculate_system_size = function () {
    var self = this;

    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }

    var prd_panel_watt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.capacity) : 0;
    //Get System Size
    var rt_price = 0;
    var rt_nfp = document.getElementsByClassName('roof_type_no_of_panels');
    for (var i = 0; i < rt_nfp.length; i++) {
        rt_price = parseFloat(rt_price) + parseFloat(rt_nfp[i].value);

    }
    rt_price = parseFloat(rt_price) * parseFloat(prd_panel_watt / 1000);

    var system_size = (rt_price == 0 || isNaN(rt_price)) ? parseFloat(0).toFixed(2) : parseFloat(rt_price).toFixed(2);

    self.system_size = system_size;
    
    if(self.system_size > 0){
        document.getElementById('system_size').value = system_size;
        document.getElementById('sc_system_size').value = system_size;
    }
    self.calculate_stc_rebate();
    
    
    if (self.deal_type == '1') {
        self.savings_calculator_only_solar(false);
    } else if (self.deal_type == '2') {
        self.savings_calculator_only_solar_battery(false);
    } else {
        self.savings_calculator_only_solar(false);
    }
    
    return system_size;
};

residentail_solar_proposal_calculator.prototype.calculate_total_cost = function () {
 var self = this;
 var additional_margin = 0;
 
 //Get panel price
 var prd_panel = document.getElementById('prd_panel');
 var prd_panel_data = null;
 if (prd_panel.value != '' && prd_panel.value != undefined) {
    prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
 }
 
 //Get System Size
 var system_size = self.calculate_system_size();
 system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);
 
 
 var prd_panel_ips = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseInt(prd_panel_data.variant_ips) : 0;
 var prd_panel_amt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_amt) : 0;
 
 var prd_panel_price = 0;
 if (prd_panel_ips == 0) {
    prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_cp) : 0;
 } else {
     //Take amount value if Is Amount Set to Yes(1)
     prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_amt) : 0;
 }
 
 var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.capacity)) : 0;
 
 (prd_panel_data !== null && prd_panel_data !== undefined && (self.system_size == 0 || isNaN(self.system_size))) ? $('#prd_panel_help').slideDown() : $('#prd_panel_help').slideUp();
 var no_of_panels = (system_size * 1000) / prd_panel_size;
 self.no_of_panels = Math.round(no_of_panels);
 document.getElementById('no_of_panels').innerHTML = isFinite(Math.floor(no_of_panels)) ? "<i class='fa fa-info-circle'></i> " + self.no_of_panels + " panels will be required." : "";
 var final_panel_price = self.no_of_panels * prd_panel_price; //Multiply 1000 because system size is in w and panel in kW
 self.prd_panel_price = (final_panel_price && isFinite(final_panel_price)) ? parseFloat(final_panel_price).toFixed(2) : parseFloat(0).toFixed(2);
 additional_margin = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(additional_margin) + (parseFloat(self.no_of_panels) * parseFloat(prd_panel_data.variant_am)) : parseFloat(0).toFixed(2);
 
 //Get Inverter Price
 var prd_inverter = document.getElementById('prd_inverter');
 var prd_inverter_data = null;
 if (prd_inverter.value != '' && prd_inverter.value != undefined) {
    prd_inverter_data = JSON.parse(prd_inverter.options[prd_inverter.selectedIndex].getAttribute('data-item'));
 }
 var prd_inverter_ips = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseInt(prd_inverter_data.variant_ips) : 0;
 var prd_inverter_amt = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_data.variant_amt) : 0;
 var prd_inverter_price = 0;
 if (prd_inverter_ips == 0) {
    prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? prd_inverter_data.variant_cp : 0;
 } else {
     //Take amount value if Is Amount Set to Yes(1)
     prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_amt) : 0;
 }
 self.prd_inverter_price = (prd_inverter_price && isFinite(prd_inverter_price)) ? parseFloat(prd_inverter_price).toFixed(2) : parseFloat(0).toFixed(2);
 additional_margin = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_inverter_data.variant_am) : parseFloat(0).toFixed(2);
 
 //Get battery price
 var prd_battery = document.getElementById('prd_battery');
 var prd_battery_data = null;
 if (prd_battery.value != '' && prd_battery.value != undefined) {
    prd_battery_data = JSON.parse(prd_battery.options[prd_battery.selectedIndex].getAttribute('data-item'));
 }
 var prd_battery_ips = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseInt(prd_battery_data.variant_ips) : 0;
 var prd_battery_amt = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_data.variant_amt) : 0;
 
 var prd_battery_price = 0;
 if (prd_battery_ips == 0) {
    prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? prd_battery_data.variant_cp : 0;
 } else {
     //Take amount value if Is Amount Set to Yes(1)
     prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_amt) : 0;
 }
 self.prd_battery_price = (prd_battery_price && isFinite(prd_battery_price)) ? parseFloat(prd_battery_price).toFixed(2) : parseFloat(0).toFixed(2);
 additional_margin = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_battery_data.variant_am) : parseFloat(0).toFixed(2);
 
 //Get Additional item_price
 var additional_cost_item = document.getElementsByName("additional_cost_item_total[]");
 var additional_cost_item_price = 0;
 
 for (var i = 0; i < additional_cost_item.length; i++) {
 var additional_item_data = additional_cost_item[i].getAttribute('data-item');
 additional_item_data = JSON.parse(additional_item_data);
 var additional_item_ips = (additional_item_data !== null && additional_item_data !== undefined) ? parseInt(additional_item_data.variant_ips) : 0;
 var additional_item_amt = (additional_item_data !== null && additional_item_data !== undefined) ? parseFloat(additional_item_data.variant_amt) : 0;
 
 if (additional_item_ips == 0) {
    additional_cost_item_price += parseFloat(additional_cost_item[i].value);
 } else {
    additional_cost_item_price += parseFloat(additional_item_amt);
 }
 
 additional_margin = (additional_cost_item[i] !== null && additional_cost_item[i] !== undefined) ? parseFloat(additional_margin) + parseFloat(additional_item_data.variant_am) : parseFloat(0).toFixed(2);
 }
 
 additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);
 
 self.additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);
 self.additional_margin = (additional_margin && additional_margin != 0 && isFinite(additional_margin)) ? parseFloat(additional_margin).toFixed(2) : parseFloat(0).toFixed(2);
 
 //Get Line item_price
 var line_item = document.getElementsByName("line_item_price[]");
 var line_item_price = 0;
 
 for (var i = 0; i < line_item.length; i++) {
     no_of_panels = (no_of_panels && no_of_panels != 0 && isFinite(no_of_panels)) ? self.no_of_panels : 0;
     var current_item_price = parseFloat(line_item[i].value);
     line_item_price += current_item_price;
 }
 
 line_item_price = (line_item_price && isFinite(line_item_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
 parseFloat(line_item_price).toFixed(2) : parseFloat(0).toFixed(2);
 
 self.line_item_price = parseFloat(line_item_price).toFixed(2);
 
 //Get Racking Price
 var racking_price = (self.prd_panel_price > 0 && isFinite(prd_panel_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
 parseFloat(self.racking_price) : parseFloat(0).toFixed(2);
 
 var kliplock_price = (no_of_panels > 0) ? 2 * self.no_of_panels * parseFloat(3.90).toFixed(2) : parseFloat(0).toFixed(2);
 self.kliplock_price = parseFloat(kliplock_price).toFixed(2);
 
 //Calcaulte System Total
 var system_total = parseFloat(self.prd_panel_price) + parseFloat(self.prd_inverter_price) +
 parseFloat(self.prd_battery_price) + parseFloat(racking_price);
 
 console.log("-----------------");
 console.log("Panels: " + self.prd_panel_price);
 console.log("Inverter: " + self.prd_inverter_price);
 console.log("Battery: " + self.prd_battery_price);
 console.log("Racking: " + racking_price);
 console.log("Additional: " + self.additional_cost_item_price);
 
 var total_cost_before_markup = parseFloat(system_total) + parseFloat(self.additional_cost_item_price) + parseFloat(self.line_item_price);
 
 //console.log(self.line_item_price);
 //console.log("Total Without Markup: " +total_cost_before_markup);
 
 var certificate_margin = parseFloat(self.company_markup_price);
 
 var company_markup = (total_cost_before_markup > 0 && isFinite(total_cost_before_markup) && certificate_margin > 0 && isFinite(certificate_margin)) ? ((total_cost_before_markup / certificate_margin) - total_cost_before_markup) : parseFloat(0).toFixed(2);
 
 company_markup = (self.additional_margin > 0 && isFinite(self.additional_margin)) ? parseFloat(company_markup) + parseFloat(self.additional_margin) : parseFloat(company_markup);
 
 
 //var company_markup = (self.system_size != 0 && !isNaN(self.system_size) && self.company_markup_price && isFinite(self.company_markup_price)) ? (parseFloat(self.company_markup_price) * no_of_panels) + parseFloat(self.additional_margin) : parseFloat(0).toFixed(2);
 
 self.company_markup = (company_markup && isFinite(company_markup)) ? parseFloat(company_markup).toFixed(3) : parseFloat(0).toFixed(2);
 
 var total_cost = parseFloat(total_cost_before_markup) + parseFloat(self.company_markup);
 
 self.total_cost = (total_cost && isFinite(total_cost)) ? (total_cost * 1.1).toFixed(2) : parseFloat(0).toFixed(2);
 

 };

residentail_solar_proposal_calculator.prototype.calculate_sum = function (is_reverse) {
    var self = this;

    self.calculate_stc_rebate();
    var system_total = 0;
    if(is_reverse){
        system_total = $('#system_total').val();
    }
    
    var total_payable = document.querySelector('#custom_quote_total_inGst').value;
    var stc_deduction = document.querySelector('#stc_deduction').value;
    var vic_rebate = document.querySelector('#vic_rebate').value;
    var vic_loan = document.querySelector('#vic_loan').value;
    if(parseInt(total_payable) == 0){
        total_payable = (parseFloat(system_total) - (parseFloat(stc_deduction) + parseFloat(vic_rebate) + parseFloat(vic_loan)));
    }
    
    self.stc_deduction = (stc_deduction && isFinite(stc_deduction)) ? parseFloat(stc_deduction).toFixed(2) : 0;
    self.vic_rebate = (vic_rebate && isFinite(vic_rebate)) ? parseFloat(vic_rebate).toFixed(2) : 0;
    self.vic_loan = (vic_loan && isFinite(vic_loan)) ? parseFloat(vic_loan).toFixed(2) : 0;
    self.total_cost_incGst = (total_payable && isFinite(total_payable)) ? parseFloat(total_payable).toFixed(2) : 0;
    self.system_total = (parseFloat(self.total_cost_incGst) + (parseFloat(self.stc_deduction) + parseFloat(self.vic_rebate) + parseFloat(self.vic_loan)));
    self.system_total =  parseFloat(self.system_total).toFixed(2);

    document.querySelector('#system_total').value = self.system_total;
    document.querySelector('#custom_quote_total_inGst').value = self.total_cost_incGst;

    //Set Deal Details
    $('#dd_system_size').html(self.system_size + ' kW');
    $('#dd_deal_value').html('$' + self.total_cost_incGst);
    if(self.system_size > 0){
        $('#sc_system_size').val(self.system_size);
    }
}

residentail_solar_proposal_calculator.prototype.create_quote_total = function () {
    var self = this;
    self.quote_total_init = true;
    //System Cost Row
    var system_total_row = document.createElement('tr');
    var system_total = document.createElement('td');
    system_total.setAttribute('style', 'width:70%;');
    system_total.innerHTML = 'System Total (inc GST)';
    var system_total_value = document.createElement('td');
    system_total_value.setAttribute('style', 'background:#b92625; text-align:right;');
    system_total_value.innerHTML = '<input type="number" id="system_total" style="text-align: right; height: inherit; border: 1px solid #555; background: #b92625; padding: 4px 10px;width: 100%;"name="proposal[system_total]" value="' + self.system_total + '" step="0.01" min="0" />';
    system_total_row.appendChild(system_total);
    system_total_row.appendChild(system_total_value);

    //STC Deduction
    var stc_deduction = (self.stc_deduction > 0) ? self.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_row = document.createElement('tr');
    var stc_deduction_col = document.createElement('td');
    stc_deduction_col.innerHTML = 'STC Deduction';
    var stc_deduction_value = document.createElement('td');
    stc_deduction_value.innerHTML = '<input id="stc_deduction" style="text-align: right; height: inherit; border: 1px solid #555;  padding: 4px 10px;width: 100%; cursor:pointer;" type="number" step="0.01" min="0" name="proposal[stc_deduction]" value="' + stc_deduction + '" readonly="" />';
    stc_deduction_row.appendChild(stc_deduction_col);
    stc_deduction_row.appendChild(stc_deduction_value);

    //Solar Vic Rebate Cost Row
    var vic_rebate = (self.vic_rebate > 0 && isFinite(self.vic_rebate)) ? parseFloat(self.vic_rebate).toFixed(2) : 0;
    var vic_rebate_price_row = document.createElement('tr');
    vic_rebate_price_row.className = (self.is_vic_rebate == false) ? 'hidden' : '';
    var vic_rebate_price = document.createElement('td');
    vic_rebate_price.innerHTML = 'Solar VIC Rebate';
    var vic_rebate_price_input = document.createElement('td');
    vic_rebate_price_input.setAttribute('style', 'background:#b92625; text-align:right;');
    vic_rebate_price_input.innerHTML = '<input id="vic_rebate" style="text-align: right; height: inherit; border: 1px solid #555; background: #b92625; padding: 4px 10px;width: 100%;" type="number" step="0.01" min="0" name="proposal[vic_rebate]" class="form-control" value="' + vic_rebate + '"  id="vic_rebate" />';
    vic_rebate_price_row.appendChild(vic_rebate_price);
    vic_rebate_price_row.appendChild(vic_rebate_price_input);
    
    console.log(self.is_vic_rebate, self.is_vic_loan);
    
    //Solar Vic Loan Cost Row
    var vic_loan = (self.vic_loan > 0 && isFinite(self.vic_loan)) ? parseFloat(self.vic_loan).toFixed(2) : 0;
    var vic_loan_price_row = document.createElement('tr');
    vic_loan_price_row.className = (self.is_vic_loan == false) ? 'hidden' : '';
    var vic_loan_price = document.createElement('td');
    vic_loan_price.innerHTML = 'Solar VIC Loan';
    var vic_loan_price_input = document.createElement('td');
    vic_loan_price_input.setAttribute('style', 'background:#b92625; text-align:right;');
    vic_loan_price_input.innerHTML = '<input id="vic_loan" style="text-align: right; height: inherit; border: 1px solid #555; background: #b92625; padding: 4px 10px;width: 100%;" type="number" step="0.01" min="0" name="proposal[vic_loan]" class="form-control" value="' + vic_loan + '"  id="vic_loan" />';
    vic_loan_price_row.appendChild(vic_loan_price);
    vic_loan_price_row.appendChild(vic_loan_price_input);
    
    
    //Quote Total ex GST
    var quote_total_exGst_row = document.createElement('tr');
    var quote_total_exGst = document.createElement('td');
    quote_total_exGst.innerHTML = '&nbsp;';
    var quote_total_exGst_value = document.createElement('td');
    quote_total_exGst_value.innerHTML = '&nbsp;';
    quote_total_exGst_row.appendChild(quote_total_exGst);
    quote_total_exGst_row.appendChild(quote_total_exGst_value);

    //Quote Total inc GST
    var quote_total_inGst_row = document.createElement('tr');
    var quote_total_inGst = document.createElement('td');
    quote_total_inGst.innerHTML = 'Quote Total (inc GST)';
    var quote_total_inGst_value = document.createElement('td');
    quote_total_inGst_value.innerHTML = '$' + toCommas(self.total_cost_incGst) + '';
    quote_total_inGst_row.appendChild(quote_total_inGst);
    quote_total_inGst_row.appendChild(quote_total_inGst_value);

    //System Cost Row Custom Price
    var price = (self.custom_quote_total_inGst > 0 && isFinite(self.custom_quote_total_inGst)) ? parseFloat(self.custom_quote_total_inGst).toFixed(2) : self.total_cost_incGst;
    var custom_price_row = document.createElement('tr');
    var custom_price = document.createElement('td');
    custom_price.setAttribute('style', 'border:solid 2px #b92625; background:#fff;');
    custom_price.setAttribute('id', 'quote_total_text');
    custom_price.innerHTML = 'Total Payable (inc GST)';
    var custom_price_input = document.createElement('td');
    custom_price_input.setAttribute('style', 'text-align:right; border:solid 2px #b92625; background:#fff;');
    custom_price_input.innerHTML = ' <span style="display:inline-flex; width: 100%;"> <input style="text-align: right; height: inherit;border: 1px solid #dedede; padding: 4px 10px;width: 100%;" type="number" step="0.01" min="0" name="proposal[quote_total_inGst]" class="form-control" value="' + price + '"  id="custom_quote_total_inGst" /><a style="margin-left:10px; margin-top: 5px; display:none;" href="javascript:void(0);" id="show_total_quote_original"><i class="fa fa-info-circle"></i></a></span>';
    custom_price_row.appendChild(custom_price);
    custom_price_row.appendChild(custom_price_input);

    document.getElementById('total_quote_wrapper').innerHTML = '';
    document.getElementById('total_quote_wrapper').appendChild(system_total_row);
    //document.getElementById('total_quote_wrapper').appendChild(additional_total_row);
    document.getElementById('total_quote_wrapper').appendChild(stc_deduction_row);
    document.getElementById('total_quote_wrapper').appendChild(vic_rebate_price_row);
    document.getElementById('total_quote_wrapper').appendChild(vic_loan_price_row);
    document.getElementById('total_quote_wrapper').appendChild(quote_total_exGst_row);
    //document.getElementById('total_quote_wrapper').appendChild(quote_total_inGst_row);
    document.getElementById('total_quote_wrapper').appendChild(custom_price_row);

    $('#stc_deduction, #vic_rebate, #custom_quote_total_inGst, #vic_loan').change(function () {
        self.calculate_sum();
    });

    $('#stc_deduction,#vic_rebate,#custom_quote_total_inGst,#vic_loan').on('input',function () {
        if($(this).val() < 0){
            $(this).val('');
        }
    });


    $('#system_total').change(function () {
        self.calculate_sum(true);
    });

    $('#stc_deduction').click(function () {
        var dt = new Date();
        $('#stc_calculation_modal').modal('show');
        $('#stc_calculate_system_size').val(self.system_size);
        $('#stc_calculate_postcode').val($('#customer_postcode').val());
        $('#stc_calculate_year').val(dt.getFullYear());
        if(self.stc_deduction <= 0){
            //$('#stc_calculate_price').val(35);
        }
        self.calculate_stc_rebate();
    });
};

residentail_solar_proposal_calculator.prototype.create_quote_total_original = function () {
    var self = this;
    var system_total_row_org = document.createElement('tr');
    var system_total_org = document.createElement('td');
    system_total_org.innerHTML = 'System Total';
    var system_total_value_org = document.createElement('td');
    system_total_value_org.innerHTML = '<span class="total-price">$' + self.original.system_total + '</span>';
    system_total_row_org.appendChild(system_total_org);
    system_total_row_org.appendChild(system_total_value_org);


    //STC Deduction
    var stc_deduction = (self.system_total > 0) ? self.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_org = (self.original.system_total > 0) ? self.original.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_row_org = document.createElement('tr');
    var stc_deduction_col_org = document.createElement('td');
    stc_deduction_col_org.innerHTML = 'STC Deduction';
    var stc_deduction_value_org = document.createElement('td');
    stc_deduction_value_org.innerHTML = '<span class="total-price" style="background: indianred;">$' + toCommas(stc_deduction) + '</span>';
    stc_deduction_row_org.appendChild(stc_deduction_col_org);
    stc_deduction_row_org.appendChild(stc_deduction_value_org);

    //Quote Total ex GST
    var quote_total_exGst_row_org = document.createElement('tr');
    var quote_total_exGst_org = document.createElement('td');
    quote_total_exGst_org.innerHTML = 'Quote Total (ex GST)';
    var quote_total_exGst_value_org = document.createElement('td');
    quote_total_exGst_value_org.innerHTML = '<span class="total-price">$' + toCommas(self.original.total_cost) + ' </span>';
    quote_total_exGst_row_org.appendChild(quote_total_exGst_org);
    quote_total_exGst_row_org.appendChild(quote_total_exGst_value_org);

    //Quote Total inc GST
    var quote_total_inGst_row_org = document.createElement('tr');
    var quote_total_inGst_org = document.createElement('td');
    quote_total_inGst_org.innerHTML = 'Quote Total (inc GST)';
    var quote_total_inGst_value_org = document.createElement('td');
    quote_total_inGst_value_org.innerHTML = '<span class="total-price">$' + toCommas(self.original.total_cost_incGst) + '</span>';
    quote_total_inGst_row_org.appendChild(quote_total_inGst_org);
    quote_total_inGst_row_org.appendChild(quote_total_inGst_value_org);

    var price = (self.custom_quote_total_inGst > 0 && isFinite(self.custom_quote_total_inGst)) ? parseFloat(self.custom_quote_total_inGst).toFixed(2) : self.total_cost_incGst;
    var gst_row = document.createElement('tr');
    var gst_col = document.createElement('td');
    gst_col.innerHTML = 'GST (Current)';
    var gst_col_value = document.createElement('td');
    var diff = parseFloat(price) - parseFloat(self.total_cost);
    diff = Math.floor(diff.toFixed(2));

    gst_col_value.innerHTML = '<span class="total-price">$ ' + toCommas(diff) + '</span>';
    gst_row.appendChild(gst_col);
    gst_row.appendChild(gst_col_value);

    var gst_row_org = document.createElement('tr');
    var gst_col_org = document.createElement('td');
    gst_col_org.innerHTML = 'GST (Orignal)';
    var gst_col_value_org = document.createElement('td');
    var diff1 = parseFloat(self.original.total_cost_incGst) - parseFloat(self.original.total_cost);
    diff1 = Math.floor(diff1.toFixed(2));

    gst_col_value_org.innerHTML = '<span class="total-price">$ ' + toCommas(diff1) + '</span>';
    gst_row_org.appendChild(gst_col_org);
    gst_row_org.appendChild(gst_col_value_org);

    document.getElementById('total_quote_original_wrapper').innerHTML = '';
    document.getElementById('total_quote_original_wrapper').appendChild(system_total_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(stc_deduction_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(quote_total_exGst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(quote_total_inGst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(gst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(gst_row);
};

residentail_solar_proposal_calculator.prototype.create_quote_item_row = function (name, price) {
    var row = document.createElement('tr');
    var column = document.createElement('td');
    column.innerHTML = name;
    var column_price = document.createElement('td');
    price = (price && price != '') ? '$' + price : '';
    column_price.innerHTML = toCommas(price) + '</span>';
    row.appendChild(column);
    row.appendChild(column_price);
    return row;
};

residentail_solar_proposal_calculator.prototype.create_quote_items = function () {
    var self = this;
    if (self.user_type != '1' && self.user_type != '4') {
        document.getElementById('line_items_div').setAttribute('class', 'hidden');
        return false;
    }
    //System Item Price Row
    var panel_price_row = self.create_quote_item_row('Panels', self.prd_panel_price);
    var inverter_price_row = self.create_quote_item_row('Inverters', self.prd_inverter_price);
    var racking_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? self.racking_price : parseFloat(0).toFixed(2);
    var racking_price_row = self.create_quote_item_row('Racking', racking_price);
    var kliplock_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? self.kliplock_price : parseFloat(0).toFixed(2);
    var kliplock_price_row = self.create_quote_item_row('KlipLock', kliplock_price);
    var additional_items_price_row = self.create_quote_item_row('Additional Items', self.additional_cost_item_price);

    $('#quote_items').removeClass('hidden');
    document.getElementById('quote_items_wrapper').innerHTML = '';
    document.getElementById('quote_items_wrapper').appendChild(panel_price_row);
    document.getElementById('quote_items_wrapper').appendChild(inverter_price_row);
    document.getElementById('quote_items_wrapper').appendChild(racking_price_row);
    document.getElementById('quote_items_wrapper').appendChild(kliplock_price_row);
    document.getElementById('quote_items_wrapper').appendChild(additional_items_price_row);
    document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row('', ''));

    //Line Item Data
    var line_item_data = self.line_item_data;
    for (var i = 0; i < line_item_data.length; i++) {
        var line_item_price = parseFloat(line_item_data[i].price).toFixed(2);
        line_item_price = (line_item_data[i].per_which_product == 'Panel') ? (line_item_price * self.no_of_panels) : (line_item_data[i].per_which_product == 'kiloWatts') ? (line_item_price * self.system_size) : line_item_price;
        line_item_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? parseFloat(line_item_price).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row(line_item_data[i].line_item, line_item_price));
    }

    //Total and Company Markup
    var total_price_row = self.create_quote_item_row('Total (exGst)', self.total_cost);
    var company_markup_price_row = self.create_quote_item_row('Company Markup', self.company_markup);
    document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row('', ''));
    document.getElementById('quote_items_wrapper').appendChild(total_price_row);
    document.getElementById('quote_items_wrapper').appendChild(company_markup_price_row);
};

residentail_solar_proposal_calculator.prototype.show_image = function () {
    var self = this;
    var image = self.proposal_image;

    if (image != '') {
        $('#proposal_no_image').attr('style', 'display:none !important;');
        $('#proposal_image').attr('style', 'display:block !important;');
        $('#proposal_image > .add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/proposal_files/' + image + '")');
        $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
        $('#proposal_image > .add-picture').css('background-position', '50% 50%');
        $('#proposal_image > .add-picture').css('background-size', '100% 100%');
    }
};

residentail_solar_proposal_calculator.prototype.hide_image = function () {
    $('#proposal_image_close').click(function () {
        //document.getElementById('mapping_tool').setAttribute('style', 'display:block !important;');
        //document.getElementById('mapping_tool_image').setAttribute('style', 'display:none !important;');
        $('#proposal_no_image').attr('style', 'display:block !important;');
        $('#proposal_image').attr('style', 'display:none !important;');
        $('#proposal_image > .add-picture').css('background-image', '');
        $('#proposal_image > .add-picture').css('background-repeat', '');
        $('#proposal_image > .add-picture').css('background-position', '');
        $('#proposal_image > .add-picture').css('background-size', '');
    });
};

residentail_solar_proposal_calculator.prototype.upload_proposal_image = function () {
    var self = this;
    $('#proposal_image_upload').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'proposal_files');
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="proposal[image]"').val(res.file_name);
                    self.proposal_image = res.file_name;
                    self.show_image();
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};


residentail_solar_proposal_calculator.prototype.fetch_solar_savings_data = function (data) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/product/residential_solar/get_savings_calculation_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            var td = document.createElement('td');
            td.setAttribute('colspan', 5);
            td.appendChild(createLoader('Please wait while data is being loaded'));
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).innerHTML = '';
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(td);
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            
            for (var key in stat) { data[key] = stat[key]; }
            self.saving_calculation_details = data;

            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).innerHTML = '';
            var annual_prod, usage_savings, sc_export_credit, sc_total_savings, sc_annual_savings = 0;
            if (data.sc_type == '1') {
                annual_prod = parseFloat(stat.solar_annual_production).toFixed(0);
                usage_savings = parseFloat(stat.usage_savings).toFixed(0);
                sc_export_credit = parseFloat(stat.export_credit).toFixed(0);
                sc_total_savings = parseFloat(stat.total_savings).toFixed(0);
                sc_annual_savings = parseFloat(stat.annual_savings).toFixed(0);
                $('#sc_annual_prod').val(annual_prod);
            } else if (data.sc_type == '2') {
                var annual_prod = parseFloat(stat.solar_annual_production).toFixed(0);
                annual_prod = parseFloat(stat.solar_annual_production).toFixed(0);
                $('#sc_sb_kwh_day').val(stat.kwh_day);
                $('#sc_sb_annual_prod').val(annual_prod);
                usage_savings = parseFloat(stat.usage_savings).toFixed(0);
                sc_export_credit = parseFloat(stat.export_credit).toFixed(0);
                sc_total_savings = parseFloat(stat.total_savings).toFixed(0);
                sc_annual_savings = parseFloat(stat.annual_savings).toFixed(0);
            }

            var sc_ave_prd_col = document.createElement('td');
            sc_ave_prd_col.innerHTML = parseFloat(annual_prod / 12).toFixed(0);

            var sc_usage_savings_col = document.createElement('td');
            sc_usage_savings_col.innerHTML = '$' + parseFloat(usage_savings).toFixed(0);

            var sc_export_credit_col = document.createElement('td');
            sc_export_credit_col.innerHTML = '$' + parseFloat(sc_export_credit).toFixed(0);

            var sc_total_savings_col = document.createElement('td');
            sc_total_savings_col.innerHTML = '$' + parseFloat(sc_total_savings).toFixed(0);

            var sc_annual_savings_col = document.createElement('td');
            sc_annual_savings_col.innerHTML = '$' + parseFloat(sc_annual_savings).toFixed(0);

            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(sc_ave_prd_col);
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(sc_usage_savings_col);
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(sc_export_credit_col);
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(sc_total_savings_col);
            document.getElementById('monthly_savings_caluclation_data_' + data.sc_type).appendChild(sc_annual_savings_col);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

residentail_solar_proposal_calculator.prototype.calculate_stc_rebate = function (is_save) {
    var self = this;
    var dt = new Date();
    //Changed from 12 to 11
    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 11;
    var system_size = self.system_size;
    //console.log(self.postcode_rating);
    //console.log(year_val);
    //console.log(system_size);
    var no_of_stc = parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size);
    no_of_stc = Math.floor(no_of_stc);

    var price_assumption = $('#stc_calculate_price').val();
    price_assumption = parseFloat(price_assumption).toFixed(2);

    if(self.stc_deduction > 0){
        price_assumption = price_assumption;
    }else{
        price_assumption = 35;
        $('#stc_calculate_price').val(price_assumption);
    }

    var stc_deduction = no_of_stc * parseFloat(price_assumption);
    stc_deduction = parseFloat(stc_deduction).toFixed(2);


    $('#stc').val(no_of_stc);
    if(stc_deduction > 0){
        $('#stc_deduction').val(stc_deduction);
        $('#stc_deduction_inGST').val(parseFloat(stc_deduction * self.gst).toFixed(2));
    }

    //console.log(no_of_stc);
    //console.log(stc_deduction);
    //Modal Values
    $('#stc_calculate_stc').val(no_of_stc);
    $('#stc_calculate_rebate').val(stc_deduction);

    if(is_save){
        self.calculate_sum();
        self.save_proposal();
    }
};

residentail_solar_proposal_calculator.prototype.calculate_payment_summary = function (is_save) {
    var self = this;

    var net_payable = $('#price_before_stc_rebate').val();
    net_payable = parseFloat(net_payable).toFixed(2);

    var stc_deduction = $('#stc_deduction').val();
    stc_deduction = parseFloat(stc_deduction).toFixed(2);

    var vic_rebate = document.querySelector('#vic_rebate').value;
    self.vic_rebate = (vic_rebate && isFinite(vic_rebate)) ? vic_rebate : 0;

    var total_payable = parseFloat(stc_deduction) + parseFloat(net_payable) + parseFloat(self.vic_rebate);
    total_payable = parseFloat(total_payable).toFixed(2);

    $("#total_payable_exGST").val(total_payable);
    $("#total_payable_inGST").val(parseFloat(total_payable * self.gst).toFixed(2));
    $("#price_before_stc_rebate_inGST").val(parseFloat(net_payable * self.gst).toFixed(2));
    $("#vic_rebate_inGST").val(parseFloat(vic_rebate * self.gst).toFixed(2));

    var context = {};
    context.total_payable_exGST = total_payable;
    context.stc_deduction = stc_deduction;
    context.price_before_stc_rebate = net_payable;
    context.total_system_size = self.total_system_size;

    if(is_save){
        self.save_proposal();
        $('#stc_calculation_modal').modal('hide');
    }
};

residentail_solar_proposal_calculator.prototype.validate_solar_proposal_data = function (form) {
    var flag = true;
    var elements = document.getElementById(form).elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};


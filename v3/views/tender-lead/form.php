<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />

<style>
    
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }
    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }
    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
    .remove_field{
        color:red;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }
    .steps li {
        display: inline-block;
        margin: 0 5px 5px 0;
        max-width: 189px;
        width: 100%;
        text-align: center;
    }
    .steps li a, .steps li p {
        background: #231f20;
        padding: 4px 20px 4px;
        color: #ffffff;
        display: block;
        font-size: 13px;
        font-weight: bold;
        position: relative;
        border-radius: 0 10px 10px 10px;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li.active a, .steps li.active p {
        background: #b92625;
        color: #fff;
    }
    .steps li.undone a, .steps li.undone p {
        background: #d1d2d4;
        color: #ffffff;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }
    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }
    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }
    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }
    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }
    .sr-no_wrap{
        white-space: nowrap;
    }
    .sr-inactive_user{
        color:red;
    }
    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    .form-block{margin-bottom: 0px !important;}
    .card_row_1{
        min-height: 330px;
    }

    .kg-activity__item{
        border: 1px solid;
        border-radius: 20px;
    }
    .kg-activity__item--default{
        border-color: #f0ad4e !important;
        box-shadow: inset 4px 0 0 0 #fff !important;
        border-color: rgba(255, 0, 0, 0.5) !important;
        /**color:rgba(255, 0, 0, 0.5) !important;*/
    }
    .kg-activity__item--green{
        border-color: green !important;
        box-shadow: inset 0px 0 0 0 #008000 !important;
    }
    .kg-activity__item--amber{
        border-color: rgba(255, 191, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }
    .kg-activity__item--pink{
        border-color: rgba(255, 192, 203, 0.24) !important;
        background-color: rgba(255, 192, 203, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }
    .kg-activity__item--red{
        border-color: rgba(255, 0, 0, 1) !important;
        box-shadow: inset 0px 0 0 0 #ff0000 !important;
    }
    .kg-activity__item--blue{
        border-color: rgba(49, 122, 226, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }
    .kg-activity__item__title{
        display:inline-flex;
        margin-bottom: 10px;
        font-size: 16px;
        font-weight: 500;
        margin: 0;
        padding: 0 0 15px;
        line-height: 1.2;
        color: inherit;
        font-family: 'Poppins', sans-serif;
        position: relative;
        cursor:pointer;
    }

    .kg-activity__item__title--green{
        color:rgba(0,128,0, 0.5) !important;
    }


    .kg-activity__item__description{
        /**background-color:#ffffe0;*/
        margin: 0 -13px -13px -13px;
        padding: 12px;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
        background-color:#b92625;
        color: #ffffff;
    }
    
    .kg-activity__item__description p{
        color: #ffffff;
    }

    .kg-activity__item__action{
        float:right;
        margin: 0;
        padding: 0;
    }
    .kg-activity__item__action:after {
        content: '\2807';
        font-size: 1.5em;
        color: #2e2e2e;
    }

    .kg-activity__attachment_item{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }


    @media only screen and (max-width: 991px){
        .kg-btn_block{
            display:inline-grid;
        }
        .kg-btn_block a{
            margin:5px;
        }
    }

    @media only screen and (min-width: 991px){
        .btn{
           padding: 0 15px 0px 15px;
min-width: 160px;
margin-bottom: 1.25rem;
        }
    }

    @media only screen and (max-width:767px){
      .steps{margin:0 -5px;}
      .steps li, #deal_stages .steps li{max-width: calc(50% - 10px); margin: 0 5px 10px; display: inline-block;}
  }

  @media only screen and (max-width:479px){
      .steps li, #deal_stages .steps li{max-width: 100%;}
  }
  
  p{padding-bottom: 0px !important;}
  
  .btn-group-outer{padding-bottom:0;}
  .btn-group-outer .kg-btn_block{display:flex; flex-wrap:wrap;}

</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-block d-block clearfix">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <h1 id="dd_company_name" style="margin:0px;">Business Name</h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <div id="deal_stages">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-5">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <form role="form"  action="" method="post" name="tender_lead_form" id="tender_lead_form" enctype="multipart/form-data">
                                            <div class="kg-activity__item__title">
                                                <h2>Tender Details</h2>
                                                <a style="margin-left:20px;" id="save_lead_details" href="javascript:void(0);" ><i class="fa fa-save"></i> Save</a>
                                            </div>
    
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Project Name</label>
                                                            <input type="text" id="project_name" name="lead[project_name]" class="form-control kg-form_field--orange" placeholder="Project Name"  required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Type</label>
                                                            <select class="form-control kg-form_field--orange" name="lead[lead_type]">
                                                                <?php foreach(TENDER_LEAD_TYPES as $key => $value){ ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Solar System Size (kW)</label>
                                                            <input type="text" name="lead[solar_system_size]" id="solar_system_size"  class="form-control kg-form_field--orange" placeholder="Solar System Size"  required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Battery System Size (kWh)</label>
                                                            <input type="text" name="lead[battery_system_size]" id="battery_system_size"  class="form-control" placeholder="Battery System Size">
                                                        </div>
                                                    </div>
                                                </div>
                            
                                                <div class="row">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Closing Date</label>
                                                        <input type="text" name="lead[closing_date]" id="closing_date"  class="form-control kg-form_field--orange" placeholder="Closing Date" required>
                                                    </div>
                                                </div>
                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Award Date</label>
                                                        <input type="text" name="lead[award_date]" id="award_date"  class="form-control kg-form_field--orange" placeholder="Award Date" required>
                                                    </div>
                                                </div>
                                            </div>
                            
                                            <div class="row">
                                                <input type="hidden" name="lead[latitude]" id="locationLatitude" />
                                                <input type="hidden" name="lead[longitude]" id="locationLongitude" />
                                                <div class="col-md-12">
                                                    <div class="form-group control-group">
                                                        <label>Site Address</label>
                                                        <a href="javascript:void(0);" id="manual_address_btn" style="font-size:12px; float:right;">Edit Manually</a>
                                                        <a href="javascript:void(0);" id="manual_address_hide_btn" style="font-size:12px; float:right;" class="hidden">Back to Autocomplete</a>
                                                        <select class="select2 placecomplete form-control locationstreetAddress kg-form_field--orange" id="locationstreetAddress"  style="height:48px !important;"></select>
                                                        <input class="locationstreetAddressVal form-control" type="hidden" id="locationstreetAddressVal" name="lead[address]" />
                                                    </div>
                                                </div> 
                                            </div>
                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group control-group">
                                                        <label>State</label>
                                                        <select name="lead[state_id]" id="locationState" class="form-control locationState" required>
                                                            <option value="">Select State</option>
                                                            <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                                <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                            
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group control-group">
                                                        <label>Post Code</label>
                                                        <input class="form-control locationPostCode " placeholder="Please Enter Post Code" type="text" name="lead[postcode]" id="locationPostCode" value=""  required />
                            
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-7">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Bidding Builder Details</h2>
                                            <a style="margin-left:20px;" class="add_new_tender_builder_contact_btn" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="table-responsive"  >
                                                <table width="100%" border="0" class="table table-striped" id="tender_lead_contact_table" >
                                                    <thead>
                                                        <tr>
                                                            <th>Company Name</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tender_lead_contact_table_body">

                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CUSTOMER SITE BLOCK START -->
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <form method="post" action="" enctype="multipart/form-data" id="tender_quotes_form" style="width: 100%;">
                                <input type="hidden" name="quote_id" id="quote_id">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="kg-activity__item__title" style="padding:0px !important;">
                                                <h2>Quote Number</h2>
                                                <a style="margin-left:20px;" id="add_new_quote_btn" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add Site</a>
                                            </div>
                                            <div class="form-block d-block clearfix" id="site_details">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group control-group">
                                                            <input type="text" class="form-control" name="quote[quote_details][0][title]" id="quote_title_0" placeholder="Title" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <a style="margin-left:20px;" href="javascript:void(0);" class="pull-right add_new_product_row_btn" data-site_id="0"><i class="fa fa-plus"></i> Add Row</a>
                                                    </div>
                                                </div>
                                                <div class="table-responsive"  >
                                                    <table width="100%" border="0" class="table table-striped" id="quote_items_table_0" >
                                                        <thead>
                                                            <tr>
                                                                <th>Product Type</th>
                                                                <th>Product Name</th>
                                                                <th>Model</th>
                                                                <th>Capacity</th>
                                                                <th>Description</th>
                                                                <th>Warranty</th>
                                                                <th>Price</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="quote_items_table_body_0">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-block d-block clearfix">
                                                <div class="mt-1">
                                                    <h2>Inclusions</h2>
                                                </div>
                                                <textarea class="form-control" name="quote[inclusions]"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="col-md-8" style="padding-bottom:30px;">
                                                        <div class="btn-block">
                                                            <a href="javascript:void(0);" id="save_quote" class="btn mrg-r10 ">Save Draft</a>
                                                            <a href="javascript:void(0);" id="create_pdf" class="btn">Save & Create PDF</a> 
                                                            <a href="#" id="create_pdf_download" target="_blank" rel="noopener noreferrer" class="hidden"></a> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h2>Attachments</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="lead_forms_data_table" >
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Address</th>
                                                    <th>Date Created</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lead_forms_data_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title" style="padding:0px !important;">
                                            <h2>Quotes Created</h2>
                                        </div>
                                        <div class="form-block d-block clearfix" id="quotes_detail">
                                            <div class="table-responsive"  >
                                                <table width="100%" border="0" class="table table-striped" id="quotes_table" >
                                                    <thead>
                                                        <tr>
                                                            <th>Date Created</th>
                                                            <th>Quote ID</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="quotes_table_body">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6">
                                        <div class="card">
                                            <div class="card-body btn-group-outer">
                                                <div class="btn-block kg-btn_block">
                                                    <a href="javascript:void(0);" id="add_activity" class="btn mrg-r10">Add Activity</a>
                                                    <a href="javascript:void(0);" id="add_job_card" class="btn mrg-r10">Job Card</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6">
                                        <div class="card">
                                            <div class="card-body btn-group-outer">
                                                <div class="btn-block kg-btn_block">
                                                    <a href="javascript:void(0);" id="solar_booking_form_btn" class="btn mrg-r10">Solar Booking Form</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12" id="all_activity_container">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src='<?php echo site_url(); ?>assets/js/jquery-ui.min.js' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/tender_lead.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/tender_activity.js?v=<?php echo version; ?>' type='text/javascript'></script>


<script type='text/javascript'>

    var ac_context = {};
    ac_context.view = 'lead';
    ac_context.lead_data = '<?php echo (isset($lead_data) && !(empty($lead_data))) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';

    var activity_manager_tool = new activity_manager(ac_context);

    function closeModal(id) {
        $('#' + id).modal('hide');
    }

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#locationstreetAddress').placecomplete1({
            placeServiceResult1: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    
                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    

                    var address_components = data.address_components;
                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    console.log(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.getElementById("locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }
                                }
                               
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                                $('#locationPostCode').trigger('change');
                            }
                        }
                    }
                } else {
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'en'
        });

    });


    var context = {};
    context.user_type = '<?php echo isset($user_group) ? $user_group : ''; ?>';
    context.lead_stages = '<?php echo (isset($lead_stages) && !empty($lead_stages)) ? json_encode($lead_stages) : ''; ?>';
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.uuid = '<?php echo isset($uuid) ? $uuid : ''; ?>';
    context.lead_id = '<?php echo isset($lead_id) ? $lead_id : ''; ?>';
    context.lead_data = '<?php echo (isset($lead_data) && !(empty($lead_data))) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.product_types = '<?php echo (isset($product_types) && !empty($product_types)) ? json_encode($product_types) : ''; ?>';

    context.led_cost_centers = '<?php echo json_encode(get_cost_center_by_type('LED', 'ccid,name')) ?>';
    context.solar_cost_centers = '<?php echo json_encode(get_cost_center_by_type('solar', 'ccid,name')) ?>';
    context.residential_cost_centers = '<?php echo json_encode(get_cost_center_by_type('residential', 'ccid,name')) ?>';
    var lead_manager = new lead_manager(context);
</script>

</body>
</html>


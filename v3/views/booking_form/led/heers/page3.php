
<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="828" border="1" bordercolor="#e8e8e8" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">&nbsp;</th>
                    <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 1</th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 2</th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 3</th>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Space Type:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="form-control" name="space_type[0]" id="space_type_0" required>
                            <option value="">Select one</option>
                            <?php
                            $space_types = unserialize(BF_ZONE_CLASSIFICATION);
                            if (!empty($space_types)) {
                                foreach ($space_types as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="form-control" name="space_type[1]" id="space_type_1" >
                            <option value="">Select one</option>
                            <?php
                            $space_types = unserialize(BF_ZONE_CLASSIFICATION);
                            if (!empty($space_types)) {
                                foreach ($space_types as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="form-control" name="space_type[2]" id="space_type_2" >
                            <option value="">Select one</option>
                            <?php
                            $space_types = unserialize(BF_ZONE_CLASSIFICATION);
                            if (!empty($space_types)) {
                                foreach ($space_types as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Celling Height:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="ceiling_height_0" name="ceiling_height[0]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  id="ceiling_height_1" name="ceiling_height[1]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  id="ceiling_height_2" name="ceiling_height[2]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                </tr>
                <tr>
                    <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px"></th>
                    <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Reach Up:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Ceiling Height)</td>
                            </tr>
                        </table>
                    </th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Clearance:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Height of item blocking access)</td>
                            </tr>
                        </table>
                    </th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Reach Across:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Distance from the alsle to light)</td>
                            </tr>
                        </table>
                    </th>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Boom Requirements:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  id="boom_req_reach_up" name="boom_req[reach_up]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  id="boom_req_clearance" name="boom_req[clearance]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  id="boom_req_reach_across" name="boom_req[reach_across]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="30" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td><a href="javascript:void(0);" style="float:right; margin-right:50px;" id="add_more_led_products_row_btn"><i class="fa fa-plus"></i> Add More</a></td>
    </tr>
    <tr>
        <td valign="top">
            <table id="booking_items" width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Zone Classification</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Description of Upgrade product</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Sensor</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php for ($i = 0; $i < 15; $i++) { 
                    $required = ($i == 0) ? 'required=""' : '';
                    $hidden = ($i > 6) ? 'hidden' : '';
                    ?>
                    <tr class="product_row_<?php echo $i; ?> <?php echo $hidden; ?>">
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <select style="padding:5px; width:100%; height: inherit;" class="item_zone_classification item_zone_classification_<?php echo $i; ?>" name="product[item_zone_classification][]" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <?php
                                $zone_classification = unserialize(BF_ZONE_CLASSIFICATION);
                                if (!empty($zone_classification)) {
                                    foreach ($zone_classification as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                          <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <?php /* if($i < 5){ ?>
                            <select class="item_name item_name_<?php echo $i; ?>" name="product[item_name][]" style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <?php
                                //$product_name = unserialize(BF_PRODUCT);
                                $product_name = $led_products;
                                if (!empty($product_name)) {
                                    foreach ($product_name as $key => $value) {
                                        ?>
                                        <option data-id="<?php echo $value['np_id']; ?>"  value="<?php echo $value['np_name']; ?>" ><?php echo $value['np_name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" class="item_id item_id_<?php echo $i; ?>" name="product[item_id][]" />
                            <?php }else{ ?>
                              
                            <?php } */ ?>
							<input type="hidden" class="type_id type_id_<?php echo $i; ?>" name="product[type_id][]" data-id="<?php echo $i; ?>" />
							<input type="hidden" class="item_id item_id_<?php echo $i; ?>" name="product[item_id][]" data-id="<?php echo $i; ?>" />
                           <input type="text" class="item_name item_name_<?php echo $i; ?>" name="product[item_name][]" data-id="<?php echo $i; ?>"  style="padding:5px; width:100%; height: inherit;" />
                            <!--<input  class="item_sensor item_sensor_<?php echo $i; ?>" name="product[item_sensor][]" style="padding:5px; width: 100%; height: inherit;" type="hidden" value="" data-id="<?php // echo $i; ?>" />-->
							 <input  class="item_model item_model_<?php echo $i; ?>" name="product[item_model][]" data-id="<?php echo $i; ?>" style="padding:5px; width: 100%; height: inherit;" type="hidden" value=""  />
                            <input  class="item_brand item_brand_<?php echo $i; ?>" name="product[item_brand][]" data-id="<?php echo $i; ?>" style="padding:5px; width: 100%; height: inherit;" type="hidden" value="" />
                        </td>
						
						<td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
							<select class="item_sensor item_sensor_<?php echo $i; ?>" name="product[item_sensor][]" style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                        <option data-id="1"  value="1" >Yes</option>
										<option data-id="0"  value="0" >No</option>
								</select>
						</td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="item_qty item_qty_<?php echo $i; ?>" name="product[item_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="item_cost_excGST item_cost_excGST_<?php echo $i; ?>" name="product[item_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="item_total_cost_excGst item_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                    </tr>   
                <?php } ?>

                <tr style="display: none;">
                    <td height="40" colspan="4" style="padding:5px;">
                        <table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="55%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">&nbsp; </td>
                                <td width="20%">&nbsp;</td>
                                <td width="25%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Sub Total exc GST:</td>
                            </tr>
                        </table></td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input id="product_total_excGst" name="product[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <th width="154" height="40" class="black-bg" colspan="2" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Access Equipment</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Detailed Description of Access Equipment</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Quantity</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php for ($i = 0; $i < 2; $i++) {
                    $required = ($i == 0) ? '' : '';
                    ?>
                    <tr>
                        <td colspan="2" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="ae_name ae_name_<?php echo $i; ?>" name="ae[ae_name][]" style="padding:5px; width:100%; height: inherit;" type="text" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <select class="ae_description ae_description_<?php echo $i; ?>" name="ae[ae_description][]"  style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <?php
                                $ae = unserialize(BF_AE);
                                if (!empty($ae)) {
                                    foreach ($ae as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_qty ae_qty_<?php echo $i; ?>" name="ae[ae_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_cost_excGST ae_cost_excGST_<?php echo $i; ?>" name="ae[ae_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_total_cost_excGst ae_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                    </tr>   
                <?php } ?>
                <tr style="display: none;">
                    <td height="40" colspan="4" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Sub Total exc GST:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input id="ae_total_excGst" name="ae[total_excGst]"  style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_excGst" name="booking_form[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">    &nbsp;
                                </td>
                                <td width="212">
                                   &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_Gst" name="booking_form[total_Gst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    &nbsp;
                                </td>
                                <td width="212" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    &nbsp;
                                </td>
                            </tr>
                        </table></td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>

var sales_reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.start_date = '';
    this.end_date = '';

    $(function () {
        
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('start_date').value = start.format('YYYY-MM-DD');
            document.getElementById('end_date').value = end.format('YYYY-MM-DD');
            self.start_date = start.format('d/M/Y');
            self.end_date = end.format('d/M/Y');
            self.initialize();
        }
        
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All': [moment('2016-01-01'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
        
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('.sr-show_inactive_users').click(function () {
        $('.sr-inactive_user').addClass('hidden');
        if ($(this).prop("checked") == true) {
            $('.sr-inactive_user').removeClass('hidden');
        }
    });

    $('#state_id').on('change', function () {
        self.initialize();
    });
    

};

sales_reporting.prototype.initialize = function () {
    var self = this;
    self.fetch_sales_rep_performance_data();
    self.fetch_sales_rep_closable_leads_data();
    self.fetch_sales_rep_lead_stage_data();
    self.fetch_sales_rep_activity_data();
}

sales_reporting.prototype.fetch_sales_rep_performance_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_performance_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_statistics_chart_container').innerHTML = '';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            document.getElementById('sales_statistics_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_performance_report_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_statistics_chart_container').innerHTML = '';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = '';
            if (data.sales_stats.length > 0) {
                google.charts.load('current', {'packages': ['corechart', 'bar']});
                google.charts.setOnLoadCallback(function () {
                    self.create_sales_statistics_chart(data.sales_stats);
                });

                var total_led_proposals = 0;
                var total_solar_proposals = 0;
                var total_proposals = 0;
                var total_won_leads = 0;
                var total_kws = 0;
                var total_conv_rate = 0;
                var total_highbays = 0;
                var total_flood_light = 0;
                var total_panel_light = 0;
                var total_batten_light = 0;

                for (var i = 0; i < data.sales_stats.length; i++) {
                    var row = self.create_sales_performance_report_data(data.sales_stats[i]);
                    document.getElementById('sales_rep_performance_report_table_body').appendChild(row);


                    var led_proposals = data.sales_stats[i].total_led_proposal;
                    var solar_proposals = data.sales_stats[i].total_solar_proposal;
                    var proposals_count = parseInt(led_proposals) + parseInt(solar_proposals);
                    var leads_count = data.sales_stats[i].total_leads;
                    var won_leads_count = data.sales_stats[i].total_won_leads;
                    var kws_count = data.sales_stats[i].total_kws;
                    var highbay_count = data.sales_stats[i].total_highbays;
                    var flood_light_count = data.sales_stats[i].total_flood_light;
                    var panel_light_count = data.sales_stats[i].total_panel_light;
                    var batten_light_count = data.sales_stats[i].total_batten_light;
                    var conv_rate = 0;

                    if (leads_count != 0 && won_leads_count != 0) {
                        conv_rate = (won_leads_count / leads_count) * 100;
                        conv_rate = conv_rate.toFixed(2);
                        total_conv_rate = parseFloat(total_conv_rate) + parseFloat(conv_rate);
                        total_conv_rate = parseFloat(total_conv_rate).toFixed(2);
                    }

                    total_led_proposals = parseInt(total_led_proposals) + parseInt(led_proposals);
                    total_solar_proposals = parseInt(total_solar_proposals) + parseInt(solar_proposals);
                    total_proposals = parseInt(total_proposals) + parseInt(proposals_count);
                    total_won_leads = parseInt(total_won_leads) + parseInt(won_leads_count);
                    total_kws = parseFloat(total_kws) + parseFloat(kws_count);
                    total_kws = parseFloat(total_kws).toFixed(2);
                    total_highbays = parseInt(total_highbays) + parseInt(highbay_count);
                    total_flood_light = parseInt(total_flood_light) + parseInt(flood_light_count);
                    total_panel_light = parseInt(total_panel_light) + parseInt(panel_light_count);
                    total_batten_light = parseInt(total_batten_light) + parseInt(batten_light_count);

                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var td5 = document.createElement('td');
                var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                var td9 = document.createElement('td');
                var td10 = document.createElement('td');
                var td11 = document.createElement('td');
                //var td3 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '2');
                tr_total.appendChild(td1);
                td2.innerHTML = total_proposals;
                td3.innerHTML = total_led_proposals;
                td4.innerHTML = total_solar_proposals;
                var avg_conv = parseFloat(total_conv_rate / (data.sales_stats).length);
                avg_conv = avg_conv.toFixed(2);
                avg_conv = (isNaN(avg_conv)) ? '0.00' : avg_conv;
                td5.innerHTML = avg_conv + '%';
                td6.innerHTML = total_won_leads;
                td7.innerHTML = total_kws + ' kW';
                td8.innerHTML = total_highbays;
                td9.innerHTML = total_flood_light;
                td10.innerHTML = total_panel_light;
                td11.innerHTML = total_batten_light;
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                tr_total.appendChild(td5);
                tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                tr_total.appendChild(td9);
                tr_total.appendChild(td10);
                tr_total.appendChild(td11);
                document.getElementById('sales_rep_performance_report_table_body').appendChild(tr_total);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_statistics_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

sales_reporting.prototype.create_sales_statistics_chart = function (stat_data) {
    var total_stats = [];
    total_stats.push(['Sales Rep', 'kW Sold', 'Highbay Sold']);
    for (var i = 0; i < stat_data.length; i++) {
        total_stats.push([stat_data[i].full_name, parseFloat(stat_data[i].total_kws), parseFloat(stat_data[i].total_highbays)]);
    }
    var data = google.visualization.arrayToDataTable(total_stats);
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        vAxis: {format:'#', gridlines: {count: 2},}
    };

    var chart = new google.charts.Bar(document.getElementById('sales_statistics_chart_container'));
    chart.draw(data, google.charts.Bar.convertOptions(options));

};

sales_reporting.prototype.create_sales_performance_report_data = function (data) {

    var led_proposals = data.total_led_proposal;
    var solar_proposals = data.total_solar_proposal;
    var total_proposals = parseInt(led_proposals) + parseInt(solar_proposals);
    var total_leads = data.total_leads;
    var total_won_leads = data.total_won_leads;
    var conv_rate = 0;
    if (total_leads != 0 && total_won_leads != 0) {
        var conv = (total_won_leads / total_leads) * 100;
        conv = conv.toFixed(2);
        conv_rate = conv + '%';
    }

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.full_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state;

    var td3 = document.createElement('td');
    td3.innerHTML = total_proposals;

    var td4 = document.createElement('td');
    td4.innerHTML = led_proposals;

    var td5 = document.createElement('td');
    td5.innerHTML = solar_proposals;

    var td6 = document.createElement('td');
    td6.innerHTML = conv_rate;

    var td7 = document.createElement('td');
    td7.innerHTML = total_won_leads;

    var td8 = document.createElement('td');
    td8.innerHTML = data.total_kws + ' kW';

    var td9 = document.createElement('td');
    td9.innerHTML = data.total_highbays;

    var td10 = document.createElement('td');
    td10.innerHTML = data.total_flood_light;

    var td11 = document.createElement('td');
    td11.innerHTML = data.total_panel_light;

    var td12 = document.createElement('td');
    td12.innerHTML = data.total_batten_light;


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);
    tr.appendChild(td11);
    tr.appendChild(td12);

    return tr;
};

/** Closable Lead Data Functions */

sales_reporting.prototype.create_closeable_leads_statistics_chart = function (stat_data) {
    var total_stats = [];
    total_stats.push(['Sales Rep', 'kW Sold', 'Highbay Sold']);
    for (var i = 0; i < stat_data.length; i++) {
        total_stats.push([stat_data[i].full_name, parseFloat(stat_data[i].total_kws), parseFloat(stat_data[i].total_highbays)]);
    }
    var data = google.visualization.arrayToDataTable(total_stats);
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        vAxis: {format:'#', gridlines: {count: 2},}
        
    };
    var chart = new google.charts.Bar(document.getElementById('sales_rep_closable_lead_chart_container'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
};

sales_reporting.prototype.create_closeable_leads_table_row = function (data) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.full_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state;

    var td3 = document.createElement('td');
    td3.innerHTML = data.total_leads;

    var td4 = document.createElement('td');
    td4.innerHTML = data.total_led_leads;

    var td5 = document.createElement('td');
    td5.innerHTML = data.total_solar_leads;

    //var td6 = document.createElement('td');
    //td6.innerHTML = conv_rate;

    //var td7 = document.createElement('td');
    //td7.innerHTML = total_won_leads;

    var td8 = document.createElement('td');
    td8.innerHTML = data.total_kws + ' kW';

    var td9 = document.createElement('td');
    td9.innerHTML = data.total_highbays;

    var td10 = document.createElement('td');
    td10.innerHTML = data.total_flood_light;

    var td11 = document.createElement('td');
    td11.innerHTML = data.total_panel_light;

    var td12 = document.createElement('td');
    td12.innerHTML = data.total_batten_light;


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    //tr.appendChild(td6);
    //tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);
    tr.appendChild(td11);
    tr.appendChild(td12);

    return tr;
};


sales_reporting.prototype.fetch_sales_rep_closable_leads_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_closable_leads_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_closable_lead_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = '';
            if (data.sales_stats.length > 0) {
                google.charts.load('current', {'packages': ['corechart', 'bar']});
                google.charts.setOnLoadCallback(function () {
                    self.create_closeable_leads_statistics_chart(data.sales_stats);
                });
                var columns1 = ['Sales Rep', 'State' , 'Total Deals', 'Solar Deals', 'LED Deals', 'kW', 'Highbays', 'Floodlights', 'Panel Lights', 'Batten Lights' ];
                var lead_stage_table = self.create_sales_rep_table('sales_rep_closable_lead_table', columns1);
                var total_led_leads = 0;
                var total_solar_leads = 0;
                var total_proposals = 0;
                var total_leads = 0;
                var total_kws = 0;
                var total_highbays = 0;
                var total_flood_light = 0;
                var total_panel_light = 0;
                var total_batten_light = 0;
                
                var tbody = document.createElement('tbody');
                for (var i = 0; i < data.sales_stats.length; i++) {
                    var row = self.create_closeable_leads_table_row(data.sales_stats[i]);
                    tbody.appendChild(row);

                    var led_leads = data.sales_stats[i].total_led_leads;
                    var solar_leads = data.sales_stats[i].total_solar_leads;
                    var leads_count = data.sales_stats[i].total_leads;
                    var kws_count = data.sales_stats[i].total_kws;
                    var highbay_count = data.sales_stats[i].total_highbays;
                    var flood_light_count = data.sales_stats[i].total_flood_light;
                    var panel_light_count = data.sales_stats[i].total_panel_light;
                    var batten_light_count = data.sales_stats[i].total_batten_light;

                    total_led_leads = parseInt(total_led_leads) + parseInt(led_leads);
                    total_solar_leads = parseInt(total_solar_leads) + parseInt(solar_leads);
                    //total_proposals = parseInt(total_proposals) + parseInt(proposals_count);
                    total_leads = parseInt(total_leads) + parseInt(leads_count);
                    total_kws = parseFloat(total_kws) + parseFloat(kws_count);
                    total_kws = parseFloat(total_kws).toFixed(2);
                    total_highbays = parseInt(total_highbays) + parseInt(highbay_count);
                    total_flood_light = parseInt(total_flood_light) + parseInt(flood_light_count);
                    total_panel_light = parseInt(total_panel_light) + parseInt(panel_light_count);
                    total_batten_light = parseInt(total_batten_light) + parseInt(batten_light_count);

                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                //var td5 = document.createElement('td');
                //var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                var td9 = document.createElement('td');
                var td10 = document.createElement('td');
                var td11 = document.createElement('td');
                //var td3 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '2');
                tr_total.appendChild(td1);
                td2.innerHTML = total_leads;
                td3.innerHTML = total_led_leads;
                td4.innerHTML = total_solar_leads;
                //td6.innerHTML = total_won_leads;
                td7.innerHTML = total_kws + ' kW';
                td8.innerHTML = total_highbays;
                td9.innerHTML = total_flood_light;
                td10.innerHTML = total_panel_light;
                td11.innerHTML = total_batten_light;
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                //tr_total.appendChild(td5);
                //tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                tr_total.appendChild(td9);
                tr_total.appendChild(td10);
                tr_total.appendChild(td11);
                tbody.appendChild(tr_total);
                lead_stage_table.appendChild(tbody);
                document.getElementById('sales_rep_closable_lead_table_container').appendChild(lead_stage_table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Lead Stage Data Functions */

sales_reporting.prototype.create_sales_rep_lead_stage_table_row = function (stages, data, lead_stage_table) {
    var tbody = document.createElement('tbody');
    var total_counts = [];
    console.log(stages);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);
        for (var key in stages) {
            if (stages[key].id != 11 && stages[key].id != 12) {
                var td = document.createElement('td');
                var stage = 'stage' + (parseInt(key) + 1) + '_count';
                if (total_counts[key]) {
                    total_counts[key] = parseInt(total_counts[key]) + parseInt(data[i][stage]);
                } else {
                    total_counts[key] = data[i][stage];
                }
                td.innerHTML = data[i][stage];
                tr.appendChild(td);
            }
        }
        tbody.appendChild(tr);
    }

    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.setAttribute('colspan', '2');
    td.innerHTML = '<b>Total</b>';
    tr.appendChild(td);
    for (var key in total_counts) {
        var td = document.createElement('td');
        td.innerHTML = total_counts[key];
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
    lead_stage_table.appendChild(tbody);
};

sales_reporting.prototype.create_sales_rep_nd_vs_cd_table_row = function (stages, data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_counts = [];
    var chart_data = [];
    chart_data.push(['Sales Rep', 'New Leads', 'New Closable Leads']);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);
        
        for (var key in stages) {
            if (stages[key].id == 3 || stages[key].id == 9) {
                var td = document.createElement('td');
                var stage = 'stage' + (parseInt(key) + 1) + '_count';
                if (total_counts[key]) {
                    total_counts[key] = parseInt(total_counts[key]) + parseInt(data[i][stage]);
                } else {
                    total_counts[key] = data[i][stage];
                }
                td.innerHTML = data[i][stage];
                tr.appendChild(td);
            }
        }

        chart_data.push([data[i].full_name, parseInt(data[i].stage3_count), parseInt(data[i].stage9_count)]);
        tbody.appendChild(tr);
    }

    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.setAttribute('colspan', '2');
    td.innerHTML = '<b>Total</b>';
    tr.appendChild(td);
    for (var key in total_counts) {
        var td = document.createElement('td');
        td.innerHTML = total_counts[key];
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
    table.appendChild(tbody);
    
    
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            gridlines: {count: 2},
            format: '#'
        }
    };


    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_nd_vs_cd_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.create_sales_rep_completed_site_visits_table_row = function (stages, data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_counts = [];
    var chart_data = [];
    chart_data.push(['Sales Rep', 'Appointment Booked', 'Decision Due', 'Proposal Delivered','Closable Deal']);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);

        var keys = [3,7,8,9];
        for (var key in keys) {
            var td = document.createElement('td');
            var stage = 'stage' + (parseInt(keys[key]) + 1) + '_count';
            if (total_counts[i]) {
                total_counts[i] = parseInt(total_counts[i]) + parseInt(data[i][stage]);
            } else {
                total_counts[i] = parseInt(data[i][stage]);
            }
            td.innerHTML = data[i][stage];
            tr.appendChild(td);
        }
        var td_total = document.createElement('td');
        td_total.innerHTML = total_counts[i];
        tr.appendChild(td_total);
        
        tbody.appendChild(tr);
        chart_data.push([data[i].full_name, parseInt(data[i].stage4_count), parseInt(data[i].stage7_count), parseInt(data[i].stage8_count), parseInt(data[i].stage9_count)]);
    }

    table.appendChild(tbody);
    
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            gridlines: {count: 2},
            format: '#'
        }
    };

    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_completed_site_visits_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.fetch_sales_rep_lead_stage_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_lead_stage_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            document.getElementById('sales_rep_lead_stage_data_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_nd_vs_cd_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_nd_vs_cd_table_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_completed_site_visits_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_completed_site_visits_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            if (data.sales_stats.length > 0) {
                var columns1 = ['Sales Rep', 'State'];
                var columns2 = ['Sales Rep', 'State'];
                var columns3 = ['Sales Rep', 'State'];
                var stages = data.lead_stages;
                for (var key in stages) {
                    if (stages[key].id != 11 && stages[key].id != 12) {
                        columns1.push(stages[key].stage_name);
                    }
                    //New Deals vs Closable Deals
                    if (stages[key].id == 3 || stages[key].id == 9) {
                        columns2.push(stages[key].stage_name);
                    }

                }

                var lead_stage_table = self.create_sales_rep_table('sales_rep_lead_stage_data_table', columns1);
                self.create_sales_rep_lead_stage_table_row(stages, data.sales_stats, lead_stage_table);
                document.getElementById('sales_rep_lead_stage_data_container').appendChild(lead_stage_table);

                //New Deals vs Closable Deals
                var nd_vs_cd_table = self.create_sales_rep_table('sales_rep_nd_vs_cd_table', columns2);
                self.create_sales_rep_nd_vs_cd_table_row(stages, data.sales_stats, nd_vs_cd_table);
                document.getElementById('sales_rep_nd_vs_cd_table_container').appendChild(nd_vs_cd_table);

                //Completed Site Visits

                columns3.push(stages[3].stage_name);
                columns3.push(stages[7].stage_name);
                columns3.push(stages[8].stage_name);
                columns3.push(stages[9].stage_name);
                columns3.push('Total');
                var completed_stie_visits_table = self.create_sales_rep_table('sales_rep_completed_site_visits_table', columns3);
                self.create_sales_rep_completed_site_visits_table_row(stages, data.sales_stats, completed_stie_visits_table);
                document.getElementById('sales_rep_completed_site_visits_table_container').appendChild(completed_stie_visits_table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Activity Data Functions */

sales_reporting.prototype.create_sales_rep_activity_table_row = function (data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_counts = [];
    var chart_data = [];
    chart_data.push(['Sales Rep', 'Site Visits', 'Phone Calls','Schedule Meeting','Emails', 'Notes']);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);

        var keys = ['visit_count', 'phone_count','meeting_count' ,'email_count', 'note_count'];
        for (var key in keys) {
            var td = document.createElement('td');
            var stage = keys[key];
            if (total_counts[i]) {
                total_counts[i] = parseInt(total_counts[i]) + parseInt(data[i][stage]);
            } else {
                total_counts[i] = parseInt(data[i][stage]);
            }
            td.innerHTML = data[i][stage];
            tr.appendChild(td);
        }
        var td_total = document.createElement('td');
        td_total.innerHTML = total_counts[i];
        tr.appendChild(td_total);
        
        tbody.appendChild(tr);
        chart_data.push([data[i].full_name, parseInt(data[i].visit_count), parseInt(data[i].phone_count), parseInt(data[i].email_count), parseInt(data[i].note_count)]);
    }

    table.appendChild(tbody);

    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            minValue: min,
            maxValue: max,
            format: '#'
        }
    };
    
    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_activity_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.fetch_sales_rep_activity_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_activity_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_activity_chart_container').innerHTML = '';
            document.getElementById('sales_rep_activity_table_container').innerHTML = '';
            document.getElementById('sales_rep_activity_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_activity_table_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_activity_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_activity_chart_container').innerHTML = '';
            document.getElementById('sales_rep_activity_table_container').innerHTML = '';
            if (data.activity_stats.length > 0) {
                var columns = ['Sales Rep', 'State', 'Site Visits', 'Phone Calls','Schedule Meeting','Emails', 'Notes', 'Total'];
                var table = self.create_sales_rep_table('sales_rep_activity_table', columns);
                self.create_sales_rep_activity_table_row(data.activity_stats, table);
                document.getElementById('sales_rep_activity_table_container').appendChild(table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_activity_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_activity_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Common Functions */

sales_reporting.prototype.create_sales_rep_bar_chart = function (container, chart_data , options) {
    var data = google.visualization.arrayToDataTable(chart_data);
    var chart = new google.charts.Bar(document.getElementById(container));
    chart.draw(data, google.charts.Bar.convertOptions(options));

};

sales_reporting.prototype.create_sales_rep_table = function (id, columns) {
    var table = document.createElement('table');
    table.className = 'table';
    table.setAttribute('id', id);

    var table_head = document.createElement('thead');
    var table_head_row = document.createElement('tr');

    for (var i = 0; i < columns.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = columns[i];
        table_head_row.appendChild(th);
    }

    table_head.appendChild(table_head_row);
    table.appendChild(table_head);

    return table;
};

sales_reporting.prototype.format_number = function (number) {
    return Number(parseInt(number)).toLocaleString('en');
};
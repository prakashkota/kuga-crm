<div class="modal fade" id="stc_calculation_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="margin-bottom: -10px;">Calculate STC </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table width="100%" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <td>System Size (kW)</td>
                                    <td>Postcode</td>
                                    <td>Installation Year</td>
                                    <td># STC</td>
                                    <td>Price Assumption</td>
                                    <td>Rebate Amount</td>
                                </tr>
                            </thead>
                            <tbody >
                                <tr>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_system_size" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_postcode" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_year" readonly=""></td>
                                    <td><input type="number" min="1" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_stc" readonly=""></td>
                                    <td><input type="number" min="1" step="0.01" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_price" ></td>
                                    <td><input type="number" min="1" step="0.01" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_rebate" readonly=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="btn-block mt-2 ">
                        <input type="button" id="save_stc_btn" value="Save" class="btn btn-primary float-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
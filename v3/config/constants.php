<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/* Custom Constants */

define('CACHING', FALSE);

define('SITE_EMAIL', 'no-reply@kugacrm.com.au');

define('NOTIFICATION_MAIL', 'webleads@kugacrm.com.au');

define('DEVELOPER_MAIL', 'ashish.sharma@dotsquares.com');

//User Group related constants

//TEAMLEADERS
define('GROUP_LATL_ID', '4'); //From DB
define('GROUP_STL_ID', '6'); //From DB

//TEAM REPRESENTATIVES
define('GROUP_LAR_ID', '5'); //From DB
define('GROUP_SR_ID', '7'); //From DB
define('GROUP_TQU_ID', '9'); //From DB

define('TEAM_LEADER_TYPES', array(
    '4' => 'Lead Allocation Team Leader',
    '6' => 'Sales Team Leader'
));

define('TEAM_REP_TYPES', array(
    '5' => 'Lead Allocation Team Representative',
    '7' => 'Sales Team Representative'
));

define('TEAM_REP_PROFILE_TYPES', array(
    '1' => 'Commercial Sales Manager',
    '2' => 'Tender Manager'
));
define('SITE_TYPES',array(
    'pl'=>'Panel Layout',
    'il'=>'Inverter Layout',
    'mu'=> 'Mounting'
));
define('ROOF_OPTIONS',array('1'=>'Tile',2=>'Tin',3=>'Clip Lock'));
define('TENDER_OPTIONS',4);
define('SITE_UPLOAD_PATH',"./assets/uploads/site_images/");
define('TEAM_REP_TYPES_BY_TEAM_LEADER_TYPES', array(
    '4' => '5',
    '6' => '7'
));
//User Group related constants END

define('LED', 1);
define('SOLAR', 2);
define('STATE_FILTER', serialize(array(
    '0' => 'All',
    '7' => 'VIC',
    '2' => 'NSW'
)));

define('TENDER_LEAD_TYPES', array(
    '1' => 'Tender',
    '2' => 'Quotes'
));

define('TENDER_STATUS_TYPES', array(
    '1' => ' TENDER NOT SUBMITTED ',
    '2' => 'TENDER SUBMITTED',
    '3' => 'WON',
    '4' => 'PROJECT COMPLETED',
    '5' => 'LOST',
));

define('REDUCES_CARBON', array(
    'ACT' => '0.79',
    'NSW' => '0.79',
    'QLD' => '0.80',
    'SA' => '0.35',
    'VIC' => '0.96',
    'WA' => '0.68',
    'NT' => '0.62'
));
define('TENDER_SITE_UPLOAD_PATH',"./assets/uploads/tender-documents/");
define("GST", 1.1);

define("TERM_MONTHS",serialize(array(12,24,36,48,60,72,84)));

define('WON_STAGE', 11);
define('LOST_STAGE', 12);
define('PROPOSAL_DELIVERED_STAGE', 7);

//Solar Proposal Realted Constatns
define("RATE", serialize(array(1, 2, 3)));
define("NO_OF_WORKING_DAYS", serialize(array(5, 6, 7)));
define("DAYS_OF_WEEK", serialize(array(1, 2, 3, 4, 5, 6, 7)));

$current_date = strtotime(date('Y-m-d H:i:s'));

define('version', $current_date);

define("product_application", serialize(array("Residential", "Commercial")));

define("per_which_product", serialize(array("Panel", "kiloWatts", "System")));

define("product_state", serialize(array("NSW", "VIC", "SA", "WA", "TAS", "NT", "ACT")));

define("activity_type", serialize(array("Email", "Phone", "Visit", "Note", "Scheduled Meeting")));

define("cust_title", serialize(array("Mr", "Ms", "Mrs", "Miss", "Mx", "Sir", "Dr")));

define("lead_source", serialize(array(
    "Self Generated",
    "Kuga Lead",
    "Call Centre Lead",
    "Partnership Lead",
    "Facebook Lead",
    "Referral",
    "SEMMA Lead",
    "Collingwood Lead",
    "Melbourne Victory Lead",
    "Tender Lead",
    "Make it Cheaper",
    "NSW Partnership Lead",
    "NSW Door to Door / Referral",
    "NSW Tender Lead",
    "VIC - Agile",
    "VIC - Self Generated",
    "VIC- Door to Door",
    "VIC - Tender Lead",
    "VIC - Semma",
    "VIC - Amtil",
    "VIC - MNFG",
    "VIC - Ausems",
    "VIC - Make It Cheaper",
    "VIC - Selectricity",
    "VIC - Leading Edge",
    "VIC - AussieNRG",
    "VIC - Solar Choice",
    "VIC - Lendfin",
    "VIC - SAF",
    "VIC - Colliers",
    "VIC - Centuria",
    "VIC - JLL",
    "VIC - Gillon Group",
    "VIC - Melbourne Body Corporate",
    "VIC - Advantage Owners Corporation",
    "VIC - Specifying Dynamic",
    "VIC - Facebook / Social Media",
    "VIC - Royal Energy",
    "VIC - Pitt & Sherry (Energy Consultant)",
    "VIC - Solar Quotes",
    "Others"
)));

define ("LEAD_TYPE", serialize (array (
        "1" => "Solar",
        "2" => "LED",
        "4" => "PFC",
        "5" => "Generator",
        "6" => "Battery",
        "7" => "VO",
        "3" => "Other",
    )
));

define ("LOCATION_TYPE", serialize (array (
        "1" => "Residential",
        "2" => "Commercial",
        "3" => "Both",
    )
));

define ("LEAD_SEGMENT", serialize (array (
        "1" => "Owner / Occupier",
        "2" => "Landlord",
        "3" => "Tenant",
        "4" => "Unknown",
    )
));

define ("LEAD_INDSUTRY", serialize (array (
        "1" => "Food Manufacturing",
        "2" => "General Manufacturing",
        "3" => "Engineering",
        "4" => "Cold Storage",
        "5" => "Retail",
        "6" => "Warehouse",
        "7" => "Shopping Center",
        "8" => "Office",
        "9" => "Automotive",
        "10" => "Car Dealership",
        "11" => "Other",
    )
));

define('LEAD_LOCATION_TYPE', array(
    '1' => 'Residential',
    '2' => 'Commercial',
    '3' => 'Residential - Unbranded',
    '4' => 'Commercial - Unbranded',
));

define("BF_ZONE_CLASSIFICATION", serialize(
    array("Office",
        "Retail",
        "Cafe / Restaurant",
        "Factory DIV C",
        "Health Care",
        "Undercover Carpark",
        "Open Air Carpark",
        "Warehouse",
        "Service / Repair",
        "Hotel",
        "Aged Care",
        "School / Public Assembly",
        "Extra"
    )
));

//BOKING FORM CONSTANTS

define("BF_PRODUCT", serialize(
    array(
        "RHB - 110w Eco",
        "RHB - 120w X-Series",
        "RHB - 120w Smart Series (with Sensor)",
        "RHB - 150w Z Series (No Sensor)",
        "RHB - 150w Smart Series (With Sensor)",
        "RHB - 150w Onsemi",
        "RHB - 150w Optiboost",
        "RHB - 200w Optiboost",
        "RHB - 240w Optiboost",
        "Tube - 600m",
        "Tube - 1200mm",
        "Tube - 1500mm",
        "Batten - 600mm (Replacing single tube fitting)",
        "Batten - 600mm (Replacing twin tube fitting)",
        "Batten - 1200mm (Replacing single tube fitting)",
        "Batten - 1200mm (Replacing twin tube fitting)",
        "Batten - 25W Zilo Batten Light",
        "Batten - 1500mm (Replacing twin tube fitting)",
        "IP65 Batten - 600mm (Replacing single tube fitting)",
        "IP65 Batten - 600mm (Replacing twin tube fitting)",
        "IP65 Batten - 1200mm (Replacing single tube fitting)",
        "IP65 Batten - 1200mm (Replacing twin tube fitting)",
        "IP65 Batten - 1500mm (Replacing single tube fitting)",
        "IP65 Batten - 1500mm (Replacing twin tube fitting)",
        "Shopfitter - Rectangle",
        "Shopfitter - Round",
        "Downlight - Globe only",
        "Downlight - Globe and driver",
        "Downlight - Dimmable",
        "Floodlight - XT-FL-150w",
        "Floodlight - XT-FL-200w",
        "Canopy - 100w",
        "Canopy - 150w",
        "Canopy - 200w",
        "Panel - 600x300mm 5000k",
        "Panel - 600x600mm 5000k",
        "Panel - 1200x300mm 5000k",
        "Panel - 25W Zilo Panel Light",
        "Panel - 17W Panel",
        "Misc - Daylight sensor",
        "Oyster Light",
        "Misc - Electronic Fuse",
        "Misc - Daylight Sensor",
        "Misc - Plaster Mount Kit",
        "Misc - Surface Mount Kit",
        "Other - See Notes",
        "RHB - 120W ONSEMI",
        "RHB - 120W OPTIBOOST",
        "RHB - 150W BSS LED",
    )
));

define("BF_AE", serialize(
    array(
        "Less than 4m",
        "4m-8m No Charge",
        "8m - 13m $910",
        "13m + Call 4 Price",
        "Boom 4m - 11m $910",
        "Boom 11m+ CALL 4 Price"
    )
));

define("BF_ROOF_TYPE", serialize(
    array(
        "Clip Lock",
        "Corrugated",
        "Tile",
        "Tin"
    )
));

define("BF_LEASED_OR_OWNED", serialize(
    array(
        "Owner",
        "Tenant",
        "Landlord"
    )
));

define("BF_BCA_HOURS", serialize(
    array(
        "2000",
        "3000",
        "5000",
        "5000 Lifetime A",
        "5000 Lifetime C",
        "5000 Li",
    )
));

define("LEAD_TYPES", serialize(
    array(
        '1' => "Solar Only",
        '2' => "Solar + Battery",
        '3' => "Battery Only"
)));

//NEAR MAP API KEY
define('NEAR_MAP_API_KEY_GLOBAL', '');

//DEFAULT FRANCHISE

define('DEFAULT_FRANCHISE', 4);

//Php to pdf
//define("API_KEY", "31c3e025d28115fc90885e89d043a5cf7e707092");
//6e57afea3130df50d8d648019ab935ec206688e1
define("API_KEY", "31c3e025d28115fc90885e89d043a5cf7e707092");

define("PHPTOPDF_URL", "http://phptopdf.com/generatePDF");

define("PHPTOPDF_URL_SSL", "https://phptopdf.com/generatePDF");

define("PHPTOPDF_URL_BETA", "http://phptopdf.com/generatePDF_beta");

define("PHPTOPDF_API", "v2.3.15.0");

//SMS API KEY
define('SMS_API_KEY', 'a55212f3e06f146f8abf5cc460ccc4b3');
define('SMS_API_SECRET', 'sjfWEbEtreK8');

//Form Builder Mapping fields
define("mapping_fields_name", serialize(
    array(
        "company_full_name" => "company_full_name",
        "company_email" => "company_email",
        "company_contact_no" => "company_contact_no",
        "customer_full_name" => "customer_full_name",
        "customer_contact_no" => "customer_contact_no",
        "customer_email" => "customer_email",
        "customer_address" => "customer_address",
        "customer_state" => "customer_state",
        "customer_postcode" => "customer_postcode",
        "proposal_system_size" => "proposal_system_size",
        "proposal_panel" => "proposal_panel",
        "proposal_inverter" => "proposal_inverter",
        "proposal_stories" => "proposal_stories",
        "proposal_roof_type" => "proposal_roof_type"
    )
));

define("TIME_PICKER_VALUES", serialize(
    array(
        /**"12:00 AM" => "12:00am (0 mins)",
        "12:30 AM" => "12:30am (30 mins)",
        "01:00 AM" => "1:00am (1 hr)",
        "01:30 AM" => "1:00am (1 hr)",
        "02:00 AM" => "1:00am (1 hr)",
        "02:30 AM" => "1:00am (1 hr)",
        "03:00 AM" => "1:00am (1 hr)",
        "03:30 AM" => "1:00am (1 hr)",
        "04:00 AM" => "1:00am (1 hr)",
        "04:30 AM" => "1:00am (1 hr)",
        "05:00 AM" => "1:00am (1 hr)",
        "05:30 AM" => "1:00am (1 hr)",*/
        "06:00 AM" => "1:00am (1 hr)",
        "06:30 AM" => "1:00am (1 hr)",
        "07:00 AM" => "1:00am (1 hr)",
        "07:30 AM" => "1:00am (1 hr)",
        "08:00 AM" => "1:00am (1 hr)",
        "08:30 AM" => "1:00am (1 hr)",
        "09:00 AM" => "1:00am (1 hr)",
        "09:30 AM" => "1:00am (1 hr)",
        "10:00 AM" => "1:00am (1 hr)",
        "10:30 AM" => "1:00am (1 hr)",
        "11:00 AM" => "1:00am (1 hr)",
        "11:30 AM" => "1:00am (1 hr)",
        "12:00 PM" => "1:00am (1 hr)",
        "12:30 PM" => "1:00am (1 hr)",
        "01:00 PM" => "1:00am (1 hr)",
        "01:30 PM" => "1:00am (1 hr)",
        "02:00 PM" => "1:00am (1 hr)",
        "02:30 PM" => "1:00am (1 hr)",
        "03:00 PM" => "1:00am (1 hr)",
        "03:30 PM" => "1:00am (1 hr)",
        "04:00 PM" => "1:00am (1 hr)",
        "04:30 PM" => "1:00am (1 hr)",
        "05:00 PM" => "1:00am (1 hr)",
        "05:30 PM" => "1:00am (1 hr)",
        "06:00 PM" => "1:00am (1 hr)",
        /**"06:30 PM" => "1:00am (1 hr)",
        "07:00 PM" => "1:00am (1 hr)",
        "07:30 PM" => "1:00am (1 hr)",
        "08:00 PM" => "1:00am (1 hr)",
        "08:30 PM" => "1:00am (1 hr)",
        "09:00 PM" => "1:00am (1 hr)",
        "09:30 PM" => "1:00am (1 hr)",
        "10:00 PM" => "1:00am (1 hr)",
        "10:30 PM" => "1:00am (22.5 hr)",
        "11:00 PM" => "11:00pm (23 hr)",*/
    )
));


define("DURATION_PICKER_VALUES", serialize(
    array(
        "00:30",
        "01:00",
        "01:30",
        "02:00",
        "02:30",
        "03:00",
        "03:30",
        "04:00",
        "04:30",
        "05:00",
        "05:30",
        "06:00",
        "06:30",
        "07:00",
        "07:30",
        "08:00",
        "08:30",
    )
));

define("GOOGLE_API_KEY", "AIzaSyDzX5cZK-FJWov4arDc_LhycU20Z6gNDp0");
//AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
  defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
  defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
  defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
  defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
  defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
  defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
  defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

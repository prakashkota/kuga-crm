<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @property Uploader Controller
 * @version 1.0
 */
class Uploader extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('image_lib');
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function upload_file() {
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        try {
            $upload_path = $this->input->post('upload_path');
            $do_compress = $this->input->post('do_compress');
            $isImage = $this->input->post('isImage');
            $do_compress = (isset($do_compress) && $do_compress != '') ? FALSE : TRUE;
            $isImage = (isset($isImage) && $isImage != '') ? FALSE : TRUE;
            if ($upload_path == 'led_booking_form_files' || $upload_path == 'solar_booking_form_files') {
                $config['upload_path'] = 'assets/uploads/' . $upload_path . '/original';
                $compress_path = 'assets/uploads/' . $upload_path . '/';
            } else {
                $config['upload_path'] = (isset($upload_path) && $upload_path != '') ? 'assets/uploads/' . $upload_path : 'assets/uploads/product_files';
                $compress_path = $config['upload_path'];
            }
            if(!is_dir(FCPATH.$config['upload_path'])){
               mkdir(FCPATH.$config['upload_path']);
            }
            $config['allowed_types'] = '*';
            $config['max_size'] = 10000;
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $data['status'] = $this->upload->display_errors();
                $data['success'] = FALSE;
            } else {
                $filename = $this->upload->data()['file_name'];
                try {
                    $file = FCPATH . $config['upload_path'] . '/' . $filename;
                    if($isImage) {
                        $this->components->rotate_image_based_on_exif_oreintation($file);
                    }
                } catch (Exception $e) {

                }
                $data['status'] = 'File uploaded successfully';
                $data['file_name'] = $filename;
                $data['success'] = TRUE;

                if ($do_compress) {
                    // Process resize image before upload (Compresion)
                    $configer = array(
                        'image_library' => 'gd2',
                        'source_image' => $this->upload->data()['full_path'],
                        'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                        'maintain_ratio' => TRUE,
                        'quality' => '90%', //tell CI to reduce the image quality and affect the image size
                        'width' => 640, //new size of image
                        'height' => 480, //new size of image
                        'new_image' => $compress_path
                    );
                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                }
            }
        }
        catch (Exception $e) {
            $data['status'] = 'Looks like an exception occured while uploading the file. Please try again.';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }

    public function upload_meter_data_file() {
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        $proposal_id = $this->input->post('proposal_id');
        $config['upload_path'] = './assets/uploads/meter_data_files';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $data['status'] = $this->upload->display_errors();
            $data['success'] = FALSE;
        } else {
            $data['status'] = 'File uploaded successfully';
            $data['file_name'] = $this->upload->data()['file_name'];
            $data['success'] = TRUE;
        }
        echo json_encode($data);
        die;
    }

    public function upload_lead_files(){
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            try {
                $lead_id = $this->input->post('lead_id');
                if (isset($lead_id) && $lead_id == '' || $lead_id == NULL || $lead_id == 'undefined') {
                    $data['status'] = 'Invalid Lead cant attach file';
                    $data['success'] = FALSE;
                    echo json_encode($data);
                    die;
                }
                $dir = './assets/uploads/lead_files/' . $lead_id;
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }

                $config['upload_path'] = $dir;
                $config['allowed_types'] = '*';
                $config['max_size'] = 10000;
                $config['remove_spaces'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file')) {
                    $data['status'] = $this->upload->display_errors();
                    $data['success'] = FALSE;
                } else {
                    //Make entry in lead attachments table
                    $insert_data = array();
                    $insert_data['lead_id'] = $lead_id;
                    $insert_data['user_id'] = $this->aauth->get_user_id();
                    $insert_data['file_name'] = $this->upload->data()['file_name'];
                    $this->common->insert_data('tbl_lead_attachments', $insert_data);
                    $data['status'] = 'File uploaded successfully';
                    $data['file_name'] = $this->upload->data()['file_name'];
                    $data['success'] = TRUE;
                }
            } catch (Exception $e) {
                $data['status'] = $e->getMessage();
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_lead_attachments(){
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->get())) {
            try {
                $lead_id = $this->input->get('lead_id');
                if (isset($lead_id) && $lead_id == '' || $lead_id == NULL || $lead_id == 'undefined') {
                    $data['status'] = 'No lead found with specified reference id.';
                    $data['success'] = FALSE;
                    echo json_encode($data);
                    die;
                }
                $data['attachments'] = $this->common->fetch_where('tbl_lead_attachments', '*', array('lead_id' => $lead_id));
                $data['success'] = TRUE;
            } catch (Exception $e) {
                $data['status'] = $e->getMessage();
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function delete_lead_attachments(){
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            try {
                $id = $this->input->post('id');
                if (isset($id) && $id == '' || $id == NULL || $id == 'undefined') {
                    $data['status'] = 'No Attachment found with specified reference id.';
                    $data['success'] = FALSE;
                    echo json_encode($data);
                    die;
                }
                $attachment_data = $this->common->fetch_row('tbl_lead_attachments', '*', array('id' => $id));
                $stat = $this->common->delete_data('tbl_lead_attachments', array('id' => $id));
                if ($stat) {
                    if (!empty($attachment_data)) {
                        $file = './assets/uploads/lead_files/' . $attachment_data['lead_id'] . '/' . $attachment_data['file_name'];
                        unlink($file);
                    }
                    $data['success'] = TRUE;
                    $data['status'] = 'Attachment deleted successfully';
                } else {
                    $data['success'] = FALSE;
                    $data['status'] = 'Looks like something went wrong';
                }
            } catch (Exception $e) {
                $data['status'] = $e->getMessage();
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }


    public function upload_attachment_files(){
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }


            try{
                if(!empty($this->input->post('lead_id')) ||  !empty($this->input->post('tenderId'))) {
                    if ($this->input->post('lead_id')) {
                        $lead_id = $this->input->post('lead_id');
                        $dir = './assets/uploads/lead_files/'. $lead_id;
                    } else if ($this->input->post('tenderId')) {
                        $lead_id = $this->input->post('tenderId');
                        $dir = './assets/uploads/tender_files/'. $lead_id;
                    }
                    if (isset($lead_id) && $lead_id == '' || $lead_id == NULL || $lead_id == 'undefined') {
                        $data['status'] = 'Invalid Lead cant attach file';
                        $data['success'] = FALSE;
                        echo json_encode($data);
                        die;
                    }

                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }

                    $config['upload_path'] = $dir;
                    $config['allowed_types'] = '*';
                    $config['max_size'] = 15000;
                    $config['remove_spaces'] = TRUE;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $data['status'] = $this->upload->display_errors();
                        $data['success'] = FALSE;
                    } else {
                        $data['status'] = 'File uploaded successfully';
                        $data['file_name'] = $this->upload->data()['file_name'];
                        $data['file_size'] = $this->formatSizeUnits($dir . '/' . $data['file_name']);
                        $data['success'] = TRUE;
                    }
                } else {
                    throwException('Invalid data');
                }
            } catch (Exception $e) {
                $data['status'] = $e->getMessage();
                $data['success'] = FALSE;
            }

        echo json_encode($data);
        die;
    }

    public function delete_activity_attachments(){
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            try {
                $id = $this->input->post('id');
                if(isset($id) && $id == '' || $id == NULL || $id == 'undefined'){
                    $data['status'] = 'No Attachment found with specified reference id.';
                    $data['success'] = FALSE;
                    echo json_encode($data);
                    die;
                }
                $attachment_data = $this->common->fetch_row('tbl_activity_attachments','*',array('id' => $id));
                if(empty($attachment_data)){
                    $data['status'] = 'No Attachment found with specified reference id.';
                    $data['success'] = FALSE;
                    echo json_encode($data);
                    die;
                }
                $activity_data = $this->common->fetch_row('tbl_activities','*',array('id' => $attachment_data['activity_id']));
                $stat = $this->common->delete_data('tbl_activity_attachments',array('id' => $id));
                if($stat){
                    $file = './assets/uploads/lead_files/'.$activity_data['lead_id'].'/'.$attachment_data['file_name'];
                     unlink($file);
                    $data['success'] = TRUE;
                    $data['status'] = 'Attachment deleted successfully';
                }else{
                    $data['success'] = FALSE;
                    $data['status'] = 'Looks like something went wrong';
                }
            }catch(Exception $e){
                $data['status'] = $e->getMessage();
                $data['success'] = FALSE;
            }
        }else{
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }


    public function upload_proposal_image()
    {
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        try {
            $proposal_uuid = $this->input->post('proposal_uuid');
            $upload_path = $this->input->post('upload_path');
            $do_compress = $this->input->post('do_compress');
            $do_compress = (isset($do_compress) && $do_compress != '') ? FALSE : TRUE;
            if ($upload_path == 'led_booking_form_files' || $upload_path == 'solar_booking_form_files') {
                $config['upload_path'] = './assets/uploads/' . $upload_path . '/original';
                $compress_path = './assets/uploads/' . $upload_path . '/';
            } else {
                $config['upload_path'] = (isset($upload_path) && $upload_path != '') ? './assets/uploads/' . $upload_path : './assets/uploads/product_files';
                $compress_path = $config['upload_path'];
            }
            $config['allowed_types'] = '*';
            $config['max_size'] = 10000;
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $data['status'] = $this->upload->display_errors();
                $data['success'] = FALSE;
            } else {
                $filename = $this->upload->data()['file_name'];
                try {
                    $file = FCPATH . $config['upload_path'] . '/' . $filename;
                    $this->components->rotate_image_based_on_exif_oreintation($file);
                } catch (Exception $e) {
                }
                $data['status'] = 'File uploaded successfully';
                $data['file_name'] = $filename;
                $data['success'] = TRUE;
                $this->common->update_data('tbl_solar_proposal', array('proposal_uuid' => $proposal_uuid), array('panel_layout_image' => site_url($config['upload_path'] . '/' . $filename)));

                /**
                if($do_compress){
                // Process resize image before upload (Compresion)
                $configer = array(
                'image_library' => 'gd2',
                'source_image' => $this->upload->data()['full_path'],
                'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                'maintain_ratio' => TRUE,
                'quality' => '90%', //tell CI to reduce the image quality and affect the image size
                'width' => 640, //new size of image
                'height' => 480, //new size of image
                'new_image' => $compress_path
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                }
                 */
            }
        } catch (Exception $e) {
            $data['status'] = 'Looks like an exception occured while uploading the file. Please try again.';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }


    public function upload_inverter_image()
    {
        $data = array();

        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        try {
            $proposal_uuid = $this->input->post('proposal_uuid');
            $upload_path = $this->input->post('upload_path');
            $do_compress = $this->input->post('do_compress');
            $do_compress = (isset($do_compress) && $do_compress != '') ? FALSE : TRUE;
            if ($upload_path == 'led_booking_form_files' || $upload_path == 'solar_booking_form_files') {
                $config['upload_path'] = './assets/uploads/' . $upload_path . '/original';
                $compress_path = './assets/uploads/' . $upload_path . '/';
            } else {
                $config['upload_path'] = (isset($upload_path) && $upload_path != '') ? './assets/uploads/' . $upload_path : './assets/uploads/product_files';
                $compress_path = $config['upload_path'];
            }
            $config['allowed_types'] = '*';
            $config['max_size'] = 10000;
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $data['status'] = $this->upload->display_errors();
                $data['success'] = FALSE;
            } else {
                $filename = $this->upload->data()['file_name'];
                try {
                    $file = FCPATH . $config['upload_path'] . '/' . $filename;
                    $this->components->rotate_image_based_on_exif_oreintation($file);
                } catch (Exception $e) {
                }
                $data['status'] = 'File uploaded successfully';
                $data['file_name'] = $filename;
                $data['success'] = TRUE;
                $this->common->update_data('tbl_solar_proposal', array('proposal_uuid' => $proposal_uuid), array('inverter_image' => site_url($config['upload_path'] . '/' . $filename)));

                /**
                if($do_compress){
                // Process resize image before upload (Compresion)
                $configer = array(
                'image_library' => 'gd2',
                'source_image' => $this->upload->data()['full_path'],
                'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                'maintain_ratio' => TRUE,
                'quality' => '90%', //tell CI to reduce the image quality and affect the image size
                'width' => 640, //new size of image
                'height' => 480, //new size of image
                'new_image' => $compress_path
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
                }
                 */
            }
        } catch (Exception $e) {
            $data['status'] = 'Looks like an exception occured while uploading the file. Please try again.';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }

    function formatSizeUnits($path)
    {
        $size = filesize($path);
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }

}

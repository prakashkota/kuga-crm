<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="300" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Prodcut Type</th>
                    <th width="336" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">System</th>
                    <th width="80" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                    <th width="100" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="100" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                 <?php for($i = 0; $i < count($product->product_name); $i++){
                    $total_cost_excGST = '';
                    if($product->product_qty[$i] != '' && $product->product_cost_excGST[$i] != ''){
                        $total_cost_excGST = $product->product_qty[$i]  * $product->product_cost_excGST[$i];
                    }
                    ?>
                    <tr>
                        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong><?php echo $product->product_type[$i]; ?></strong></td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong><?php echo $product->product_name[$i]; ?></strong></td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong><?php echo $product->product_qty[$i]; ?></strong></td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong><?php echo $product->product_cost_excGST[$i]; ?></strong></td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong><?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php } ?>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="156" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_excGst; ?></strong></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Is there a forklift on site?</td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_Gst; ?></strong></td>
                </tr>
                <tr>
                    <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="345">
                                    <?php if (isset($booking_form->is_forklift_on_site) && $booking_form->is_forklift_on_site == '1') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" /> YES
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" /> YES
                                    <?php } ?>
                                        
                                    <?php if (isset($booking_form->is_forklift_on_site) && $booking_form->is_forklift_on_site == '0') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" /> NO
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" /> NO
                                    <?php } ?>
                                </td>
                            </tr>
                            
                        </table></td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_incGst; ?></strong></td>
                </tr>
            </table></td>
    </tr>
    <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                    <tr>
                        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Array Details</strong></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Height:</td>
                                                <td width="240" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong><?php echo $booking_form->roof_height; ?></strong></td>
                                            </tr>
                                        </table></td>
                                    <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Type:</td>
                                                <td width="251" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong><?php echo $booking_form->roof_type; ?></strong></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr>
                        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="162" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Equipments:</td>
                                                <td width="665" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong><?php echo $booking_form->access_equipments; ?></strong></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </td>
        </tr>
    <tr>
        <?php if($type_id == 1){ ?>
         <td height="70">&nbsp;</td>
        <?php }else if($type_id == 2){ ?>
         <td height="70">&nbsp;</td>
        <?php }else { ?>
         <td height="70">&nbsp;</td>
        <?php } ?>
    </tr>
    <tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />3</td>
      </tr>
    </table></td>
  </tr>
</table>

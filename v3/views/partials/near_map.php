<style>
    #map_near_map {
    width: 100%;
    height: 600px;
}

#tool-bar {
    background: rgba(0, 0, 0, .8);
    bottom: 0px;
    width: 50px;
    padding-top: 8px;
}

#tool-bar ul {
    margin: 0;
    padding: 0;
}

#tool-bar ul li {
    display: block;
    list-style: none;
    text-align: center;
}

#tool-bar ul li a {
    color: #929292;
    padding: 15px 0px;
    display: block;
}

#tool-bar ul li a.active,
#tool-bar ul li a:hover {
    color: #fff;
}

#tool-bar .tool-bar-window {
    position: absolute;
    top: 15px;
    width: 350px;
    background: rgba(0, 0, 0, .8);
    left: 65px;
    padding: 15px;
    color: #fff;
    border-radius: 5px;
}

#tool-bar .tool-bar-window input {
    color: #000;
}

.tl-content {
    display: none
}

.preview {
    width: 1400px;
}

.preview img {
    width: 100%;
}

.tool-overlay {
    display: none;
    position: fixed;
    z-index: 1;
    background: rgba(0, 0, 0, .7);
    color: #fff;
    top: 0px;
    right: 0px;
    bottom: 0px;
    left: 0px;
}

.tool-overlay div {
    position: absolute;
    top: 50%;
    text-align: center;
    width: 100%;
}

.orientation {
    opacity: 0;
    position: absolute;
}

.rotate-by-90 {
    display: flex;
    justify-content: space-between;
}

.counter-inpt {
    width: 68px !important;
    margin: 0;
}

.rotate-by-90.rd-btn {
    justify-content: end;
}

.rotate-by-90.rd-btn label {
    margin: 0 0 0 6px;
    cursor: pointer;
}

.portrait {
    position: relative;
}

.portrait::after {
    background: #000;
    width: 14px;
    height: 22px;
    content: "";
    position: absolute;
    top: 4px;
    left: 0;
    right: 0;
    margin: 0 auto;
    border-radius: 2px;
}

.landscape {
    position: relative;
}

.landscape::after {
    background: #000;
    width: 22px;
    height: 14px;
    content: "";
    position: absolute;
    top: 8px;
    left: 0;
    right: 0;
    margin: 0 auto;
    border-radius: 2px;
}

.rotate-by-90.rd-btn label {
    background: #bdbdbd !important;
}

.rotate-by-90.rd-btn label.active {
    background: #b92625 !important;
    background: #b92625 !important;
    background: #b92625 !important;
    background: #b92625 !important;
}

#rt-landscape,#rt-portrait{margin: 10px;}

</style>

<div id="map_near_map"></div>
<div id="preview" class="preview"></div>
<div class="tool-overlay" id="img-ldr">
    <div>
        <p>
            <i class="fas fa-3x fa-cog fa-spin"></i>
        </p>
        <p id="img-ldr-msg">Processing image</p>
    </div>
</div>
<div id="tool-bar">
    <ul>
        <li>
            <a href="javascript:void(0)" data-tile="tl-1" class="tl active" title="Search">
                <i class="fas fa-2x fa-search"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" id="tl-panel-plotter" data-tile="tl-2" class="tl" title="Solar Panel">
                <i class="fas fa-2x fa-solar-panel"></i>
            </a>
        </li>
        <!--<li><a href="javascript:void(0)" data-tile="tl-3" class="tl hidden" title="Global Rotation"><i class="fas fa-2x fa-cog"></i></a></li> -->
        <li>
            <a href="javascript:void(0)" id="tl-crt-img" title="Create Image">
                <i class="fas fa-2x fa-image"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" id="tl-save" style="display:none;" title="Save">
                <i class="fas fa-2x fa-save"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" id="tl-distance-calculator" data-toolbar = "0" data-tile="tl-3" class="tl" title="Distance Calculator">
                <i class="fas fa-2x fa-arrows-alt-h"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" id="tl-select" data-toolbar = "0" data-tile="tl-3" class="tl" title="Select Panel">
                <i class="fas fa-2x fa-object-group"></i>
            </a>
        </li>
    </ul>
    <div class="tool-bar-window">
        <div class="tl-1-content tl-content">
            <input id="pac-input" class="form-control" type="text" placeholder="Search Box">
        </div>
        <div class="tl-2-content tl-content">
            <div class="form-group">
                <select class="form-control" id="panel_list">
                    <?php foreach ($solar_panels as $row) { ?>
                        <option 
                            data-dimension="<?php echo $row['length']; ?>,<?php echo $row['width']; ?>" 
                            data-capacity="<?php echo $row['power_wattage']; ?>"
                            value="<?php echo $row['prd_id']; ?>">
                            <?php echo $row['product_name']; ?>
                        </option>
                <?php } ?>
            </select>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <div class="input-group input-group-sm btn-inset">
                        <input id="no_of_panels" type="number" class="form-control"  placeholder="Enter no of panels to add" min="1" value="1" />
                        <button id="add-panel" type="button" class="btn btn-info btn-flat">ADD</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <button id="select-toggle" class="btn btn-xs  btn-info select-typ" title="Select All">SELECT ALL</button>
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    <button id="delete-panel" class="btn btn-xs btn-info" title="Delete">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group input-group-sm">
                    <label>Rotation</label>
                    <div class="rotate-by-90">
                        <label for="rt-left">
                            <i class="fas fa-undo" title="Rotate Left">
                                <input type="radio" id="rt-left" class="orientation" name="orientation" value="landscape"/>
                            </i>
                        </label>
                        <input id="individual_rotation" type="number" class="counter-inpt form-control" placeholder="Rotaion" value="0" min="0" max="361"/>
                        <label for="rt-right">
                            <i class="fas fa-redo" title="Rotate Right">
                                <input type="radio" id="rt-right" class="orientation" name="orientation" value="portrait"/>
                            </i>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group-sm">
                    <label>Tilt</label>
                    <input id="tilt" type="number" class="form-control" placeholder="Tilt" value="15" min="0" max="91"/>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <label title="Panel Mounting">Mounting</label>
                <div class="rotate-by-90 rd-btn">
                    <label for="rt-landscape" class="landscape" title="Landscape" >
                        <input type="radio" id="rt-landscape" class="mounting" name="mounting" value="landscape"/>
                    </label>
                    <label for="rt-portrait" title="Portrait" class="portrait">
                        <input type="radio" id="rt-portrait" class="mounting" name="mounting" value="portrait" checked="checked"/>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="display:none;">
                <div class="form-group">
                    <label>Global Rotation</label>
                    <input id="rotation" type="number" class="form-control" placeholder="Rotaion" min="0" max="360" value="0">
                </div>
            </div>
            <div class="col-sm-12">
                <label id="selected"></label>
            </div>
            <div class="col-sm-12">
                <label id="watt_capacity"></label>
            </div>
        </div>
    </div>
</div>
</div>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
<script src="<?php echo site_url(); ?>common/js/solar-map-v1.0.js?v=<?php echo version; ?>"></script>
<script>
    var nearMapLoaded = false;
    $(document).ready(function () {
        var near_map_data = '<?php echo (isset($proposal_data) && !(empty($proposal_data))) ? json_encode($proposal_data, JSON_HEX_APOS) : ''; ?>';
        if (near_map_data == '' || near_map_data == null) {
            near_map_data = '{}';
        } else {
            var output = JSON.parse(near_map_data);
            near_map_data = JSON.stringify(output['near_map_data']);
        }
        
        $(document).on('click', '#tab_near_map_btn', function () {
            if (nearMapLoaded == false) {
                nearMapLoaded = true;
                var tool = new solarMap({map_element_id: 'map_near_map'});
                tool.saved_data = near_map_data;
                if (document.getElementById('site_address').value != '') {
                    tool.addressToLatLng('Regimental Dr, Camperdown NSW 2006, Australia', function (response) {
                        if (response) {
                            tool.map_default_options['center'] = {lat: response.lat(), lng: response.lng()};
                        }
                        tool.initializeMap();
                        tool.enableSearchBox();
                    });
                } else {
                    tool.initializeMap();
                    tool.enableSearchBox();
                }
            }
        });
    });
</script>

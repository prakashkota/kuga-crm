<style>
    @font-face {
        font-family: 'CenturyGothic';
        font-weight: normal;
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
        url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
        url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
    }
    @font-face {
        font-family: 'CenturyGothic';
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
        url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
        url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
        font-weight: bold;
    }
    body{
        font-family: 'CenturyGothic';
        margin: 0;
        padding: 0;
    }
</style>

<div style="page-break-after:always;">
    <table width="910" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page8_'.$proposal_data['id'].'.jpg');?>" >
            </td>
        </tr>    
    </table>
</div>

<div style="page-break-after:always;">
    <table width="910" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page9_'.$proposal_data['id'].'.jpg');?>" >
            </td>
        </tr>    
    </table>
</div>

<div style="page-break-after:always;">
    <table width="910" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page10_'.$proposal_data['id'].'.jpg');?>" >
            </td>
        </tr>    
    </table>
</div>

<div style="page-break-after:always;">
    <table width="910" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page11_'.$proposal_data['id'].'.jpg');?>" >
            </td>
        </tr>    
    </table>
</div>

<div style="page-break-after:always;">
    <table width="910" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page12_'.$proposal_data['id'].'.jpg');?>" >
            </td>
        </tr>    
    </table>
</div>

<?php if (count($financial_summary_data) && $financial_summary_data['type'] === 'VEEC') { ?>
    <div style="page-break-after:always;">
        <table width="910" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="<?php echo site_url('assets/uploads/solar_proposal_pdf_new/page12_1_'.$proposal_data['id'].'.jpg');?>" >
                </td>
            </tr>    
        </table>
    </div>
<?php } ?>

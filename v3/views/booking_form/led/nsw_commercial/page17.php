<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <?php /*  <tr>
                    <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Additional Notes</strong></td>
                </tr>
                <tr>
                    <td height="100" valign="top" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px;  padding:5px">
                        <textarea id="additional_notes" name="booking_form[additional_notes]" style="padding:5px; width:100%; height: inherit;" ></textarea>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Other Kuga Electrical representative involved, if applicable. Place their name here.</td>
                </tr>
                <tr>
                    <td style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                        <input id="other_rep_name" name="booking_form[other_rep_name]" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                </tr> */ ?>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Call the office on 1300 005 842 to arrange temporary booking date and time.</strong></td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">If you can’t arrange a booking. put today’s date and the reason why you can’t make the booking in the “Scheduler who booked the job:” field</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="402" height="30" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Date and Time of Temporary Booking:</td>
                                <td width="23" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">&nbsp;</td>
                                <td width="405" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Scheduler who booked the job:</td>
                            </tr>
                            <tr>
                                <td style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <input required="" id="booked_at" name="booking_form[booked_at]" style="padding:5px; width:100%; height: inherit;" type="text" />
                                </td>
                                <td></td>
                                <td valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <input required="" id="booked_by" name="booking_form[booked_by]" style="padding:5px; width:100%; height: inherit;" type="text" />
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
              <?php /*   <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="163" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Pipedrive Deal ID.</td>
                                <td width="667" valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <input id="pipedrive_id" name="booking_form[pipedrive_id]" style="padding:5px; width:100%; height: inherit;" type="number" />
                                </td>
                            </tr>
                        </table></td>
                </tr> */ ?>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">By signing this form you acknowledge that all information is tru and correct. You also accept responsibility for any and all costs incurred by providing inaccurate information.</td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>KUGA ELECTRICAL SALES REPRESENTATIVE SIGNATURE</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style=" padding:5px; height: 180px; width:100%;">
                        <div class="signature-pad">
                            <div class="signature-pad--body">
                                <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_3" data-id="3" class="sign_create"></div>
                                <input required="" id="sales_rep_signature" name="booking_form[sales_rep_signature]" type="hidden" />
                            </div>
                        </div>
                        <div class="signature-pad-image signature_image_container" style="display:none !important;">
                            <img src="" />
                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="35" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Plesae Sign Here</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>

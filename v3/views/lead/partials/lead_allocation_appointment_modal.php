<style>
    .horizontal_group{
        display: flex !important;
    }
</style>
<div class="modal fade" id="lead_allocation_appointment_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_customer_location_modal_title">Lead Allocation</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post" id="lead_allocation_form" enctype="multipart/form-data">
                            <input type="hidden" name="lead_allocation[lead_id]" id="lead_allocation_lead_id">


                            <div class="form-block d-block clearfix" >
                                <div class="form-group row horizontal_group" id="current_sales_rep_assigned" style="display:none !important;">
                                    <label class="col-sm-4 col-form-label"> Currently Assigned To:</label>
                                    <div class="col-sm-8" id="current_sales_rep_assigned_name">

                                    </div>
                                </div>

                                <div class="form-group row horizontal_group">
                                    <label class="col-sm-4 col-form-label">Date of Lead:</label>
                                    <div class="col-sm-8" id="la_date_of_lead">

                                    </div>
                                </div>
                                <div class="form-group row horizontal_group">
                                    <label class="col-sm-4 col-form-label">Business Name:</label>
                                    <div class="col-sm-8" id="la_business_name">

                                    </div>
                                </div>


                                <div class="form-group row horizontal_group">
                                    <label class="col-sm-3 col-form-label">Site 

                                    </label>
                                    <div class="col-sm-7">
                                     <select class="form-control" name="lead_allocation[site_id]" id="la_site_id">
                                        <option value="">Please Select Site</option>
                                    </select> 
                                </div>
                                <div class="col-md-2">
                                    <a class="add_new_customer_location_btn" data-return='<?php echo json_encode(array("id" => "set_appointment_btn", "action" => "click")); ?>' href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>


                            <div class="form-group row horizontal_group">
                                <label class="col-sm-3 col-form-label">Contact Person
                                </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="lead_allocation[cust_contact_id]" id="la_cust_contact_id">
                                        <option value="">Please Select Contact Person</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <a class="add_new_customer_contact_btn" data-return='<?php echo json_encode(array("id" => "set_appointment_btn", "action" => "click")); ?>' href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>

                            <div class="form-group row horizontal_group">
                                <label class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-9">
                                    <input type="number" name="lead_allocation[contact_no]" id="la_contact_no"  class="form-control gray-bg" placeholder="Contact Number" required>
                                </div>
                            </div>

                            <div class="form-group row horizontal_group">
                                <label class="col-sm-3 col-form-label">Contact Email</label>
                                <div class="col-sm-9">
                                    <input type="email" name="lead_allocation[contact_email]" id="la_contact_email"  class="form-control gray-bg" placeholder="Contact Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                </div>
                            </div>

                            <div class="form-group row horizontal_group">
                                <div class="col-md-3 mb-3">
                                    <div class="form-group control-group ">
                                        <label>Calendar Meeting?</label>
                                        <br/>
                                        <span class="sr-switch">
                                            <label for="is_calendar_meeting">Yes</label>
                                            <input type="checkbox" class="sr-switch"  id="is_calendar_meeting" checked="" />
                                            <label for="is_calendar_meeting">No</label>
                                        </span>
                                        <input type="hidden" name="lead_allocation[is_calendar_meeting]" id="is_calendar_meeting_value" />
                                    </div>
                                </div>
                                <div class="col-sm-9 hidden" id="calendar_meeting_option">
                                    <div class="row">
                                        <div class="col-md-4">
                                           <div class="form-group" >
                                            <span style="display:block !important;"><i class="fa fa-calendar"></i> Date 
                                                <a href="#" data-toggle="tooltip" title="Enter the date when the activity will take place." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                            </span>
                                            <div class="input-group date" data-provide="datepicker" style="margin-bottom: 1.5rem;">
                                                <input class="datepicker am-scheduler__item form-control" id="la_scheduled_date_value" value="<?php echo date('d-m-Y'); ?>">
                                                <div class="input-group-addon hidden">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <span style="display:block !important;"><i class="fa fa-clock"></i> Time 
                                                <a href="#" data-toggle="tooltip" title="Enter the time when the activity will start from." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                            </span>
                                            <!--<input class="am-scheduler__item form-control" name="activity[scheduled_time]" id="scheduled_time_value" value=""> -->
                                            <select class="am-scheduler__item form-control"  id="la_scheduled_time_value">
                                                <option value="">Select Time</option>
                                                <option value="12:00 AM">12:00am (0 mins)</option>
                                                <option value="12:30 AM">12:30am (30 mins)</option>
                                                <?php $TIME_PICKER_VALUES = unserialize(TIME_PICKER_VALUES); 
                                                $i = 0.5;
                                                foreach($TIME_PICKER_VALUES as $key => $value){
                                                    $dispaly_hr = $i + 0.5;
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo  strtolower(preg_replace('/\s+/', '', $key)) . ' (' . ($dispaly_hr) . ' hr)'; ?> </option>
                                                    <?php $i = $i + 0.5; } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <span style="display:block !important;"><i class="fa fa-clock"></i> Duration 
                                                    <a href="#" data-toggle="tooltip" title="Enter the duration when the activity will end after the specifed duration from specifed time." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                </span>
                                                <!-- <input class="am-scheduler__item form-control" name="activity[scheduled_duration]"  id="scheduled_duration_value" value=""> -->
                                                <select class="am-scheduler__item form-control"  id="la_scheduled_duration_value">
                                                    <option value="">Select Duration</option>
                                                    <?php $DURATION_PICKER_VALUES = unserialize(DURATION_PICKER_VALUES);
                                                    foreach($DURATION_PICKER_VALUES as $key => $value){
                                                        ?>
                                                        <option value="<?php echo $value; ?>"><?php echo $value ?> </option>
                                                    <?php  } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-row">
                            <div class="col-sm-3 col-md-3 mb-3">
                                <label for="total_highbay">Daily Average Usage</label>
                                <input type="number" class="form-control" name="lead_allocation[daily_average_usage]" id="la_daily_average_usage" placeholder="Daily Average Usage" />
                            </div>
                            <div class="col-sm-2 col-md-2 mb-3">
                                <label for="total_highbay">Highbay</label>
                                <input type="number" class="form-control" name="lead_allocation[highbay]"  id="la_highbay" placeholder="Highbay" />
                            </div>
                            <div class="col-sm-2 col-md-2 mb-3">
                                <label for="total_panel_light">Panel Light</label>
                                <input type="number" class="form-control" name="lead_allocation[panel_light]"  id="la_panel_light" placeholder="Panel Light" />
                            </div>
                            <div class="col-sm-2 col-md-2 mb-3">
                                <label for="total_flood_light">Flood Light</label>
                                <input type="number" class="form-control" name="lead_allocation[flood_light]"  id="la_flood_light" placeholder="Flood Light" />
                            </div>
                            <div class="col-sm-2 col-md-2 mb-3">
                                <label for="total_batten_light">Battens</label>
                                <input type="number" class="form-control" name="lead_allocation[batten_light]"  id="la_batten_light" placeholder="Battens" />
                            </div>
                        </div>  


                        <div class="form-group">
                            <label>Note </label>
                            <textarea name="lead_allocation[note]" id="la_note" class="form-control" placeholder="Note"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h2>Fill Deal Category Details</h2>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group">
                                                <label class="control-label">Service Category </label>
                                                <select class="form-control" name="lead_allocation[category_id]" id="category_id">
                                                    <option data-item="{}" value="">Please Select Service Category</option>
                                                    <?php foreach ($service_category as $key => $value) { ?>
                                                        <option value="<?php echo $value['id']; ?>" data-item='<?php echo json_encode($value); ?>' ><?php echo $value['category_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h2>Deal Allocated to:</h2>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group">
                                                <label>Sales Rep</label>
                                                <select class="form-control" name="lead_allocation[user_id]" data-live-search="true" id="businessId" title="Enter Customer Postcode First" >
                                                    <option value="">Please Select Site First</option>
                                                </select>
                                            </div>
                                            <div class="form-group hidden">
                                                <label><b>Sales Rep Name:</b></label>
                                                <span id="franchise_name"></span>
                                            </div>
                                            <div class="form-group">
                                                <label><b>Sales Rep Contact Name:</b></label>
                                                <span id="franchise_contact_name"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card" id="lead_actions">
                                <div class="card-body" style="position:relative; margin-bottom:0px !important;">
                                    <!-- <button type="button" id="lead_btn_next" data-action="next" class="btn lead_btn" style="float:right;" >Next</button>-->
                                    <button type="button" id="lead_btn_reset" data-action="prev" class="btn lead_btn" style="float:left;" >Reset</button> 
                                    <button type="button" id="lead_btn_finish" data-action="finish" class="btn lead_btn" style="float:right;" >Submit</button>
                                    <button type="button" id="lead_btn_follow" data-action="finish" class="btn lead_btn hidden" style="float:right;" >Follow-Up</button>
                                </div> 
                            </div> 
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

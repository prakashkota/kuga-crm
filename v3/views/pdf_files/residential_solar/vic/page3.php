<?php
$panel_size = (!empty($panel_data)) ? $panel_data['capacity'] : 0;
$panel_size = (int) $panel_size;
$inverter_size = (!empty($inverter_data)) ? $inverter_data['capacity'] : 0;
$battery_size = (!empty($battery_data)) ? $battery_data['capacity'] : 0;
$phase = (isset($proposal['phase']) && $proposal['phase'] == 0) ? ' - ' : ((isset($proposal['phase']) && $proposal['phase'] == 1) ? 'Single Phase' : 'Three Phase');
$roof_type_no_of_panels = explode(';', $proposal['roof_type_no_of_panels']);
$roof_types = explode(';', $proposal['roof_type']);
$roof_type = '';
$is_tilt = 'No';
$no_of_stories =  $proposal['no_of_stories'];
foreach ($roof_type_no_of_panels as $key => $value) {
    // if ($key == 0) {
    //     $roof_type = $roof_types[$key];
    // } else if ($value > 0) {
    //     $roof_type .= ($roof_type != '') ? '/' . $roof_types[$key] : $roof_types[$key];
    // }
    if ($value > 0) {
        $roof_type .= ($roof_type != '') ? '/' . $roof_types[$key] : $roof_types[$key];
    }
    if ($key == 2 && $value > 0) {
        $is_tilt = 'Yes';
    }
}

$image_path = site_url() . 'assets/uploads/proposal_files/'. $proposal['image'];
if($proposal['image'] != '' && $proposal['image'] != NULL){
    list($width, $height) = ($proposal['image'] != '' && $proposal['image'] != NULL) ? getimagesize($image_path) : array(0,0);
    $extra_height =  ($height > 0) ? (496 - $height) : 0;
}else{
    $proposal['image'] = 'https://us.123rf.com/450wm/pavelstasevich/pavelstasevich1811/pavelstasevich181101065/112815953-stock-vector-no-image-available-icon-flat-vector.jpg?ver=6';
    list($width, $height) = ($proposal['image'] != '' && $proposal['image'] != NULL) ? getimagesize($proposal['image']) : array(0,0);
    $extra_height =  ($height > 0) ? (496 - $height) : 0;
}
?>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff" height="1280">
  <tr>
    <td valign="top" style="border: 0; font: 0"><table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td><table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2" height="40"></td>
              </tr>
              <tr>
                  <td style="font-family:Arial, Helvetica, sans-serif; font-size:35px; font-weight:900; color:#DE1717;">
            	    <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/header_lines.jpg" width="45" height="40" alt=""/>
                    SYSTEM <strong style="font-size: 35px; color:#000;">SUMMARY</strong>
                </td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="30"></td>
        </tr>
		<tr>
		    <td width="600" valign="top" style="border: 0; font: 0">
		        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" height="510">
		            <tbody>
		                <tr>
                            <td align="center" valign="top" style="width:600px !important;background: #DE1717;color: #ffffff;padding: 15px;text-align:left;margin-left: 12%;position: relative;top: 51px;opacity: 0.9;word-wrap: break-word;z-index: 9;">
                                YOUR ADDRESS: <?php echo str_ireplace(", Australia", "", $customer_data['address']); ?>
                            </td>
                        </tr>
                        <tr height="450">
                            <td style="position:relative;margin-top:-60px;" align="center" valign="top">
                                <img src="<?php echo $this->config->item('live_url'); ?>assets/uploads/proposal_files/<?php echo $proposal['image']; ?>" alt=""/>
                		    </td>
                        </tr> 
		            </tbody>
		        </table>
		    </td>
		</tr>      
        <tr>
          <td height="30"></td>
        </tr>
        
        <tr>
          <td valign="top"><table width="880" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td width="46"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/black_lines.jpg" width="34" height="30" alt=""/></td>
                <td width="834" style="font-size:18px; color: #000000;"><strong>PROPOSED SOLAR PLACEMENT</strong></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="30"></td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="880" border="1" bordercolor="#999999" cellspacing="0" cellpadding="7" style="border-collapse:collapse; border:solid 1px #999999">
            <tbody>
              <tr>
                <td width="424" style="font-size:14px; color: #000000"><strong>System Size:</strong></td>
                <td width="422" style="font-size:14px; color: #000000"><strong><?php echo $proposal['system_size']; ?> kW</strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Number of Panels:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $no_of_panels; ?></strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Panel Power Output:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $panel_size; ?>W</strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Inverter Size:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $inverter_size; ?>kW</strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Roof Type:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $roof_type; ?></strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Incoming Power Supply:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $phase; ?></strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Tilt</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $is_tilt; ?></strong></td>
              </tr>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>No. of Storeys:</strong></td>
                <td style="font-size:14px; color: #000000"><strong><?php echo $no_of_stories; ?></strong></td>
              </tr>
              <?php if($battery_size != 0){ ?>
                <tr>
                    <td style="font-size:14px; color: #000000"><strong>Battery Storage:</strong></td>
                    <td style="font-size:14px; color: #000000"><strong><?php echo $battery_size; ?>kW</strong></td>
                </tr>
                <?php } ?>
            </tbody>
          </table></td>
        </tr>
        <?php if($battery_size != 0){ ?>
            <tr>
                <td height="41"></td>
            </tr>
        <?php }else{ ?>
            <tr>
              <td height="80"></td>
            </tr>
        <?php } ?>
        <tr>
            <td align="bottom" style="position:relative;bottom:-90px;">
                <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/footer.jpg" width="100%" alt=""/>
            </td>
        </tr>
      </tbody>
    </table></td>
  </tr>
</table>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Google Service Account Controller
 * @version 1.0
 */
class GoogleServiceAccount extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('GoogleAuth');
        $this->load->library("Aauth");
        $this->load->model("GoogleServiceAccount_Model", 'gsaM');
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function disconnect_google_service_account() {
        $data = array();
        $service_id = $this->input->post('service_id');
        $account_data['service_id'] = $service_id;
        $account_data['user_id'] = $this->aauth->get_user()->id;
        $account_data['access_token'] = NULL;
        $account_data['email'] = NULL;
        $account_data['domain'] = NULL;
        $account_data['permission'] = NULL;
        $account_data['status'] = 0;
        $account_data['updated_at'] = date('Y-m-d H:i:s');
        $response = $this->gsaM->create_google_account_for_user($account_data);
        if ($response) {
            $this->session->unset_userdata('is_google_account_connected');
            $this->session->unset_userdata('access_token');
            $data['status'] = 'Account Disconnected Succesfully';
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Looks like something went wrong';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function google_oauth_callback() {
        $data = $account_data = array();
        $code = $this->input->get('code');
        $service_id = $this->input->post('service_id');
        try {
            if (isset($code) && $code != '') {
                //If got code exchange it for token
                //If New, Insert to Database
                $this->googleauth->client->authenticate($code);
                $access_token = $this->googleauth->client->getAccessToken();
                $objOAuthService = new Google_Service_Oauth2($this->googleauth->client);
                $userData = $objOAuthService->userinfo->get();
                if (!empty($userData)) {
                    $account_data['service_id'] = $this->session->userdata('service_id');
                    $account_data['user_id'] = $this->aauth->get_user()->id;
                    $account_data['access_token'] = json_encode($access_token);
                    $account_data['email'] = $userData['email'];
                    $account_data['domain'] = NULL;
                    $account_data['permission'] = NULL;
                    $account_data['status'] = 1;
                    $account_data['updated_at'] = date('Y-m-d H:i:s');
                    $this->gsaM->create_google_account_for_user($account_data);
                }
                echo "<script>window.opener.google_account_connect_success(true);</script>";
            } else {
                $this->session->set_userdata('service_id', $service_id);
                $account_data['service_id'] = $service_id;
                $account_data['user_id'] = $this->aauth->get_user()->id;
                $account_data['access_token'] = NULL;
                $account_data['email'] = NULL;
                $account_data['domain'] = NULL;
                $account_data['permission'] = NULL;
                $account_data['status'] = 0;
                $this->gsaM->create_google_account_for_user($account_data);
                $data['auth_url'] = $this->googleauth->client->createAuthUrl();
                echo json_encode($data);
                die;
            }
        } catch (Google_Service_Exception $gse) {
            echo "<script>window.opener.google_account_connect_success(false);</script>";
        }
    }

}

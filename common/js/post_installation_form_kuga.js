var post_installation_form_manager = function(options) {
    var self = this;
    this.job_uuid = (options.job_uuid && options.job_uuid != '') ? options.job_uuid : '';
    this.signature_pad = [];
    this.signature_pad_ids = ['post_installation_form_details_installer_signature', 'post_installation_form_details_purchaser_signature'];

    $('[name="post_installation_form_details[site_details][installation_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('[name="post_installation_form_details[installer_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('[name="post_installation_form_details[purchaser_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('[name="post_installation_form_details[installer_details][ess_training_date]"]').datepicker({
        format: 'dd/mm/yyyy',
    });

    $('[name="post_installation_form_details[contractor_ess_training_date]"]').datepicker({
        format: 'dd/mm/yyyy',
    });

    $('[name="post_installation_form_details[electrician_ess_training_date]"]').datepicker({
        format: 'dd/mm/yyyy',
    });

    $('[name="post_installation_form_details[site_details][installation_date]"]').datepicker("setDate", moment(new Date()).format('DD/MM/YYYY'));
    $('[name="post_installation_form_details[installer_date]"]').datepicker("setDate", moment(new Date()).format('DD/MM/YYYY'));
    $('[name="post_installation_form_details[purchaser_date]"]').datepicker("setDate", moment(new Date()).format('DD/MM/YYYY'));
    $('.sign_create').click(function() {
        var id = $(this).attr('data-id');
        $('#post_installation_form').hide();
        $('.sr-deal_actions').hide();
        $('.signatures').show();
        for (var i = 5; i < 7; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        console.log($('#signpad_create_' + id));
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });

        self.resizeCanvas();
    });

    $('.sign_close_pif').click(function() {
        var id = $(this).attr('data-id');
        $('#post_installation_form').show();
        $('.sr-deal_actions').show();
        $('.signatures').hide();
        for (var i = 5; i < 7; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });

    $(document).on('click', '#save_post_installation_form', function() {
        var form_data = $('#post_installation_form').serialize();
        form_data += '&job_uuid=' + self.job_uuid;
        form_data += '&geo_tag_data[latitude]=' + $('#geo_tag_latitude').val();
        form_data += '&geo_tag_data[longitude]=' + $('#geo_tag_longitude').val();
        form_data += '&geo_tag_data[altitude]=' + $('#geo_tag_altitude').val();
        self.save_post_installation_form_data(form_data);
    });

    $('[name="post_installation_form_details[purchaser_name1]"]').change(function() {
        $('[name="post_installation_form_details[purchaser_name]"]').val($(this).val());
    });

    $('.job_product_model').autocomplete({
        autoSelect: false,
        autoFocus: false,
        minLength: 2,
        maxItems: 10,
        source: function(request, response) {
            $.ajax({
                url: base_url + "admin/job/fetch_products",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term,
                    request: 1,
                },
                success: function(data) {
                    response(data.products);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },
        search: function(event, ui) {

        },
        focus: function(event, ui) {
            event.preventDefault();
            $(this).val(ui.item.name);
            return false;
        },
        select: function(event, ui) {
            event.preventDefault();
            var product_name = ui.item.name;
            var brand = (ui.item.brand != '' && ui.item.brand != null) ? ui.item.brand : '';
            var model = (ui.item.model_no != '' && ui.item.model_no != null) ? ui.item.model_no : 'No Model';
            var item_crm_id = (ui.item.item_crm_id != '' && ui.item.item_crm_id != null) ? ui.item.item_crm_id : '';
            var data_id = $(this).attr('data-id');
            $('.item_heers_crm_id_' + data_id).val(item_crm_id);
            $('.item_heers_name_' + data_id).val(product_name);
            $('.item_heers_brand_' + data_id).val(brand);
            return false;
        }
    });

    $(document).on('blur', '.input_calculate_box', function() {
        var form_data = $('#post_installation_form').serialize();
        form_data += '&job_uuid=' + self.job_uuid;
        form_data += '&job_id=' + window.job_manager_tool.job_details.job_id;
        self.save_post_heer_calculator_data(form_data);
    });

    $(document).on('click', '.calculate_delete_btn', function() {
        if (confirm('Are you sure want to delete this row data')) {
            var delete_id = $(this).attr('data-id');
            var delete_data = 'delete_id=' + delete_id;
            self.delete_post_heer_calculator_data(delete_data);
        }
    });

    self.show_image();
    self.hide_image();
    self.remove_signature_image();
    self.create_signature_pad();
};

post_installation_form_manager.prototype.load_default_Values = function() {
    var self = this;
    var business_details = window.job_manager_tool.job_details.business_details;
    var authorised_details = window.job_manager_tool.job_details.authorised_details;
    
    $('[name="post_installation_form_details[is_activities][4]"]').prop('checked','true');
    $('[name="post_installation_form_details[installer_details][completion_date_work]').val(moment(new Date()).format('DD/MM/YYYY'));
    $('[name="post_installation_form_details[site_details][premises_type]"]').val('Small business building');
    $('[name="post_installation_form_details[site_details][address]"]').val(business_details.address);
    $('[name="post_installation_form_details[site_details][company_name]"]').val(business_details.entity_name);
    $('[name="post_installation_form_details[site_details][authorised_person]"]').val(authorised_details.first_name + ' ' + authorised_details.last_name);
    $('[name="post_installation_form_details[site_details][contact_no]"]').val(authorised_details.contact_no);
    $('[name="post_installation_form_details[site_details][alternate_contact_no]"]').val(authorised_details.landline);
    $('[name="post_installation_form_details[site_details][email]"]').val(authorised_details.email);
    $('[name="post_installation_form_details[site_details][abn]"]').val(business_details.abn);
    $('[name="post_installation_form_details[site_details][tenant_landlord]"]').val(business_details.leased_or_owned);
    $('[name="post_installation_form_details[purchaser_name]"]').val(authorised_details.first_name + ' ' + authorised_details.last_name);
    $('[name="post_installation_form_details[purchaser_name1]"]').val(authorised_details.first_name + ' ' + authorised_details.last_name);
    $('[name="post_installation_form_details[purchaser_position]"]').val(authorised_details.position);
    
    var electrician_details = window.job_manager_tool.job_details.logged_in_electrician_details;
    if (electrician_details != '' && electrician_details != null) {
        $('[name="post_installation_form_details[installer_details][name]"]').val(electrician_details.first_name + ' ' + electrician_details.last_name);
        $('[name="post_installation_form_details[installer_details][business_name]"]').val(electrician_details.company_name);
        $('[name="post_installation_form_details[installer_details][abn]"]').val(electrician_details.company_abn);
        $('[name="post_installation_form_details[installer_details][email]"]').val(electrician_details.email);
        $('[name="post_installation_form_details[installer_details][contact_no]"]').val(electrician_details.company_contact_no);
        $('[name="post_installation_form_details[installer_name]"]').val(electrician_details.first_name + ' ' + electrician_details.last_name);
        $('[name="post_installation_form_details[installer_name1]"]').val(electrician_details.first_name + ' ' + electrician_details.last_name);
        $('[name="post_installation_form_details[installer_details][licence_type]"]').val(electrician_details.type_of_license);
        $('[name="post_installation_form_details[installer_details][licence_no]"]').val(electrician_details.electrician_license_no);
    }

    var ec_details = window.job_manager_tool.job_details.electrician_details;
    if (ec_details != '' && ec_details != null) {
        $('[name="post_installation_form_details[installer_details][name]"]').val(ec_details.first_name + ' ' + ec_details.last_name);
        $('[name="post_installation_form_details[installer_details][business_name]"]').val(ec_details.company_name);
        $('[name="post_installation_form_details[installer_details][abn]"]').val(ec_details.company_abn);
        $('[name="post_installation_form_details[installer_details][email]"]').val(ec_details.email);
        $('[name="post_installation_form_details[installer_details][contact_no]"]').val(ec_details.company_contact_no);
        $('[name="post_installation_form_details[installer_name]"]').val(ec_details.first_name + ' ' + ec_details.last_name);
        $('[name="post_installation_form_details[installer_name1]"]').val(ec_details.first_name + ' ' + ec_details.last_name);
        $('[name="post_installation_form_details[installer_details][licence_type]"]').val(ec_details.type_of_license);
        $('[name="post_installation_form_details[installer_details][licence_no]"]').val(ec_details.electrician_license_no);
        $('[name="post_installation_form_details[installer_details][site_address]"]').val(ec_details.company_address);
        
        if(parseInt(ec_details.group_id) == 7){
            $('[name="post_installation_form_details[installer_details][business_name]"]').val('Kuga Electrical Pty Ltd');
            $('[name="post_installation_form_details[installer_details][abn]"]').val('39616409584');
            $('[name="post_installation_form_details[installer_details][email]"]').val('service@13kuga.com.au');
            $('[name="post_installation_form_details[installer_details][contact_no]"]').val('135482');
            $('[name="post_installation_form_details[installer_details][site_address]"]').val('2/6 turbo Rd, Kingspark, NSW 2148');
        }

    }

    var booking_form_data = window.job_manager_tool.job_details.booking_form_data;
    if (booking_form_data != '' && booking_form_data != 'null' && booking_form_data != null) {
        if (booking_form_data.total_excGst != undefined) {
            $('[name="post_installation_form_details[purchaser_paid]"]').val(booking_form_data.total_excGst);
            $('[name="post_installation_form_details[site_details][total_purchase_exGst]"]').val(booking_form_data.total_excGst);
        }
    }


    var post_installation_form_details = window.job_manager_tool.job_details.post_installation_form_details;
    if (post_installation_form_details != '' && post_installation_form_details != 'null') {
        post_installation_form_details = JSON.parse(post_installation_form_details);
        console.log(post_installation_form_details);
        for (var key in post_installation_form_details) {
            if (key == 'site_details' || key == 'installer_details') {
                for (var key1 in post_installation_form_details[key]) {
                    $('[name="post_installation_form_details[' + key + '][' + key1 + ']"]').val(post_installation_form_details[key][key1]);
                }
            } else if (key == 'is_activities') {
                for (var key1 in post_installation_form_details[key]) {
                    $('[name="post_installation_form_details[' + key + '][' + key1 + ']"]').prop('checked', true);
                }
            } else if (key == 'installer_signature' || key == 'purchaser_signature') {
                self.show_signature_image($('[name="post_installation_form_details[' + key + ']"]'), post_installation_form_details[key]);
            } else {
                $('[name="post_installation_form_details[' + key + ']"]').val(post_installation_form_details[key]);
            }
        }
    }
}

post_installation_form_manager.prototype.show_image = function(ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '' && image != undefined) {
        var img_count = image.split('/');
        var img_url = (img_count.length > 1) ? base_url + 'assets/uploads/media_gallery/' + file_name : base_url + 'assets/uploads/post_installation_form_files/' + image;
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture1').css('background-image', 'url("' + img_url + '")');
        $(ele).parent().next('div').children('.add-picture1').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture1').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture1').css('background-size', '100% 100%');
        $(ele).parent().next('div').children('.add-picture1').attr('data-src', img_url);
        $(ele).parent().next('div').children('.image_close').removeClass('hidden');
    }
};

post_installation_form_manager.prototype.hide_image = function() {
    var self = this;
    $(document).on('click', '.image_close', function() {
        $(this).addClass('hidden');
        console.log('hi');
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture1').css('background-image', '');
        $(this).parent().children('.add-picture1').css('background-repeat', '');
        $(this).parent().children('.add-picture1').css('background-position', '');
        $(this).parent().children('.add-picture1').css('background-size', '');
    });
};

post_installation_form_manager.prototype.create_signature_pad = function() {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_5");
    var canvas2 = document.getElementById("signature_pad_6");

    var clearButton = $(".sign_clear_pif");
    var undoButton = $(".sign_undo_pif");
    var savePNGButton = $(".sign_save_pif");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad2 = new SignaturePad(canvas2, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1, signaturePad2];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();

        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas2.width = canvas2.offsetWidth * ratio;
        canvas2.height = canvas2.offsetHeight * ratio;
        canvas2.getContext("2d").scale(ratio, ratio);
        signaturePad2.clear();

    }


    window.addEventListener("resize", function(event) {
        resizeCanvas();
    });

    window.addEventListener("orientationchange", function(event) {
        resizeCanvas();
    });

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {
            type: contentType
        });
    }

    clearButton.on("click", function(event) {
        var id = this.getAttribute('data-id');
        arr[(id - 5)].clear();
    });

    undoButton.on("click", function(event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 5)].toData();
        if (data) {
            data.pop();
            arr[(id - 5)].fromData(data);
        }
    });

    savePNGButton.on("click", function(event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 5)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 5)].toDataURL();
            var ele = $('#' + arr1[(id - 5)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
};

post_installation_form_manager.prototype.show_signature_image = function(ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.prev().html('<img style="height:95px;" src="' + image + '" />');

            if (ele[0].name == 'post_installation_form_details[installer_signature]') {
                var canvas = document.getElementById('canvas');
                var ctx = canvas.getContext("2d");
                var cimage = new Image();
                cimage.src = image;
                cimage.onload = function() {
                    ctx.drawImage(cimage, 0, 0);
                    console.log(cimage.height);
                    ctx.font = "14pt Calibri";
                    var name = $('[name="post_installation_form_details[installer_name1]"]').val();
                    var position = 'Installer';
                    var date = $('[name="post_installation_form_details[installer_date]"]').val();
                    ctx.fillText("Name: " + name, 20, 260);
                    ctx.fillText("Position: " + position, 20, 280);
                    ctx.fillText("Date: " + date, 20, 300);
                };

                setTimeout(function() {
                    dataURL = canvas.toDataURL();
                    $('[name="post_installation_form_details[custom_installer_signature]"]').val(dataURL);
                }, 1000);
            }

            if (ele[0].name == 'post_installation_form_details[purchaser_signature]') {
                var canvas = document.getElementById('canvas');
                var ctx = canvas.getContext("2d");
                var cimage = new Image();
                cimage.src = image;
                cimage.onload = function() {
                    ctx.drawImage(cimage, 0, 0);
                    console.log(cimage.height);
                    ctx.font = "14pt Calibri";
                    var name = $('[name="post_installation_form_details[purchaser_name1]"]').val();
                    var position = $('[name="post_installation_form_details[purchaser_position]"]').val();
                    var date = $('[name="post_installation_form_details[purchaser_date]"]').val();
                    ctx.fillText("Name: " + name, 20, 260);
                    ctx.fillText("Position: " + position, 20, 280);
                    ctx.fillText("Date: " + date, 20, 300);
                };

                setTimeout(function() {
                    dataURL = canvas.toDataURL();
                    $('[name="post_installation_form_details[custom_purchaser_signature]"]').val(dataURL);
                }, 1000);
            }
        } else {
            ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/post_installation_form_files/' + image + '" />');
        }
    }

};

post_installation_form_manager.prototype.remove_signature_image = function() {
    var self = this;
    $('.signature_image_close').click(function() {
        $(this).parent().prev('div').show();
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('img').attr('src', '');
    });
};

post_installation_form_manager.prototype.resizeCanvas = function() {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_5");
    var signaturePad1 = self.signature_pad[0];
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas1.width = canvas1.offsetWidth * ratio;
    canvas1.height = canvas1.offsetHeight * ratio;
    canvas1.getContext("2d").scale(ratio, ratio);
    signaturePad1.clear();

    var canvas2 = document.getElementById("signature_pad_6");
    var signaturePad2 = self.signature_pad[1];
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    signaturePad2.clear();
};

post_installation_form_manager.prototype.save_post_installation_form_data = function(form_data, submit) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/job/save_post_installation_form_data',
        datatype: 'json',
        data: form_data,
        beforeSend: function() {
            $.isLoading({
                text: "Saving Form Data, Please Wait...."
            });
        },
        success: function(response) {
            $.isLoading("hide");
            if (response.success) {
                toastr['success'](response.status);
            } else {
                toastr['error'](response.status);
            }
            if (submit) {
                var form_data = 'job_uuid=' + self.job_uuid + '&status=SUBMITTED_TO_OPERATIONS&action=SUBMIT_FORM_TO_OPERATIONS';
                self.submit_job(form_data, 'assignment_form_actions');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


var activity_manager = function (options) {
    var self = this;
    this.view = options.view;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : '';
    this.is_sales_rep = options.is_sales_rep;
    this.activity_action = '';
    this.activity_trigger = 2;  //1:Add 2:Schedule
    self.initialize();
    self.save_activity();
    self.edit_activity_proposal();
    self.upload_activity_file();
    self.delete_activity();
    self.mark_as_completed();
    self.handle_dropzone();
    self.delete_activity_attachment();
};

activity_manager.prototype.initialize = function () {
    var self = this;
    var minDate = moment().add(1, 'd').toDate();
    var defaultDate = moment().add(1, 'M').toDate();
    $.fn.datepicker.defaults.format = "dd-mm-yyyy";

    $('.datepicker').datepicker({
        autoclose: true,
        useCurrent: false,
        startDate: minDate,
    }).on('changeDate', function () {
        $('.datepicker').hide();
    });

    /**$('#scheduled_time input').timepicker({
        'minTime': '12:00am',
        'maxTime': '11:00pm',
        'showDuration': true
    });
    
    $('#scheduled_duration input').timepicker({
        'minTime': '0:30',
        'maxTime': '08:30',
        'timeFormat': 'H:i'
    });
    */

    tinymce.init({
        selector:'#activity_note',
        branding: false,
        menubar: false,
        statusbar: false,
        plugins: "lists,link,fullscreen",
        toolbar: 'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
        mode: "textareas",
        mobile: {
            theme: 'silver'
        },
        entity_encoding : "raw"
    });


    self.create_attendees_select2([]);

    $('#activity_type').change(function () {
        var activity_type = $(this).val();
        $('#activity_location').addClass('hidden');

        if (activity_type === 'Visit' && self.activity_trigger == 2) {
            $('#activity_location').removeClass('hidden');
        }
        
        if(activity_type === 'Visit'){
           $('#activity_title').val($('#dd_company_name').html()); 
        }

        if (activity_type === 'Note') {
            $('#scheduler').addClass('hidden');
            $('#attendees').addClass('hidden');
        } else {
            if (self.activity_trigger == 2) {
                $('#scheduler').removeClass('hidden');
                $('#attendees').removeClass('hidden');

                /**$('#scheduled_time input').timepicker({
                        'minTime': '12:00am',
                        'maxTime': '11:00pm',
                        'showDuration': true
                    });
    
                    $('#scheduled_duration input').timepicker({
                        'minTime': '0:30',
                        'maxTime': '08:30',
                        'timeFormat': 'H:i'
                    });
                    */
            }
        }

    });

    if (self.is_sales_rep == '') {
        $('#userid').change(function () {
            if (self.view == 'list') {
                document.getElementById('activity_table_body').innerHTML = '';
                //document.getElementById('activity_table_body').appendChild(createLoader('Fetching activity data....'));
                document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
                document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
            } else if (self.view == 'calendar') {
                $('#calendar').fullCalendar('destroy');
                document.getElementById('calendar').innerHTML = '';
                //document.getElementById('calendar').appendChild(createLoader('Fetching activity data....'));
                document.getElementById('calendar').appendChild(createPlaceHolder(false));
                document.getElementById('calendar').appendChild(createPlaceHolder(false));
            }
            var userid = $(this).val();
            var data = 'user_id=' + userid;
            self.fetch_activity_data(data);
        });
    }

    if (self.view == 'list') {
        document.getElementById('activity_table_body').innerHTML = '';
        //document.getElementById('activity_table_body').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
        document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
        self.fetch_activity_data();
    } else if (self.view == 'calendar') {
        $('#calendar').fullCalendar('destroy');
        document.getElementById('calendar').innerHTML = '';
        //document.getElementById('calendar').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('calendar').appendChild(createPlaceHolder(false));
        document.getElementById('calendar').appendChild(createPlaceHolder(false));
        self.fetch_activity_data();
    } else if (self.view == 'proposal') {
        document.getElementById('all_activity_table_body').innerHTML = '';
        //document.getElementById('all_activity_table_body').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('all_activity_table_body').appendChild(createPlaceHolder(false));
        document.getElementById('all_activity_table_body').appendChild(createPlaceHolder(false));
        var form_data = 'lead_id=' + self.lead_data.id;
        self.fetch_activity_data(form_data);
    } else if (self.view == 'lead') {
        document.getElementById('all_activity_container').innerHTML = '';
        //document.getElementById('all_activity_container').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('all_activity_container').appendChild(createPlaceHolder(false));
        document.getElementById('all_activity_container').appendChild(createPlaceHolder(false));
        var form_data = 'lead_id=' + self.lead_data.id;
        self.fetch_activity_data(form_data);
    }

};

activity_manager.prototype.create_attendees_select2 = function (data) {
    var self = this;
    $(".attendees").select2({
        data: data,
        dropdownParent: $('#activity_modal'),
        placeholder: 'Enter Emails',
        minimumInputLength: 5,
        tags: true,
        // automatically creates tag when user hit space or comma:
        tokenSeparators: [",", " "],
        createTag: function (term, data) {
            var value = term.term;
            if (self.validateEmail(value)) {
                return {
                    id: value,
                    text: value
                };
            }
            return null;
        }
    });
};

activity_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

activity_manager.prototype.floatToTime = function (num, other_format) {
    var sign = num >= 0 ? 1 : -1;
    var min = 1 / 60;

    // Get positive value of num
    num = num * sign;

    // Separate the int from the decimal part
    var intpart = Math.floor(num);
    var decpart = num - intpart;

    // Round to nearest minute
    decpart = min * Math.round(decpart / min);

    var minutes = Math.floor(decpart * 60);

    // Sign result
    sign = sign == 1 ? '' : '-';

    // pad() adds a leading zero if needed
    // return sign + intpart + 'h' + pad(minutes, 2);
    var duration = '';
    var other_duration = '';
    if (intpart != 0) {
        duration += intpart + 'h';
        other_duration += '0' + intpart;
    }
    if (minutes != 0) {
        duration += minutes + 'm';
        other_duration += (minutes > 10) ? ':' + minutes : ':0' + minutes;
    }
    if (intpart == 0 && minutes == 0) {
        duration += '-';
    }
    if (intpart != 0 && minutes == 0) {
        other_duration = '0' + intpart + ':00';
    } else if (intpart == 0 && minutes != 0) {
        other_duration = (minutes > 10) ? '00:' + minutes : '00:0' + minutes;
    }

    if (intpart == 0 && minutes == 0) {
        duration += '-';
    }
    if (other_format == true) {
        return other_duration;
    } else {
        return sign + duration;
    }

}

activity_manager.prototype.create_activity_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    /**var mark_as_completed_checkbox = document.createElement('input');
     mark_as_completed_checkbox.setAttribute('type', 'checkbox');
     mark_as_completed_checkbox.setAttribute('class', 'event_edit');
     mark_as_completed_checkbox.setAttribute('data-item', JSON.stringify(data));
     */

    var mark_as_completed_checkbox = document.createElement('i');
    mark_as_completed_checkbox.setAttribute('class', 'fa fa-pencil event_edit');
    mark_as_completed_checkbox.setAttribute('style', 'cursor:pointer;');
    mark_as_completed_checkbox.setAttribute('data-item', JSON.stringify(data));


    var td1 = document.createElement('td');
    td1.appendChild(mark_as_completed_checkbox);

    var td2 = document.createElement('td');
    td2.innerHTML = (data.scheduled_date != null) ? data.scheduled_date : '-';

    var td3 = document.createElement('td');
    td3.innerHTML = (data.scheduled_time != null) ? data.scheduled_time : '-';

    var td4 = document.createElement('td');
    td4.innerHTML = (data.scheduled_duration != null) ? self.floatToTime(data.scheduled_duration) : '-';

    var td5 = document.createElement('td');
    td5.innerHTML = data.activity_type;

    var td6 = document.createElement('td');
    td6.innerHTML = data.owner_name;

    var td7 = document.createElement('td');
    td7.innerHTML = data.first_name + ' ' + data.last_name;

    var td8 = document.createElement('td');
    td8.innerHTML = data.customer_address;

    var td9 = document.createElement('td');
    td9.innerHTML = data.customer_contact_no;

    var td10 = document.createElement('td');
    td10.innerHTML = (data.status == 1) ? "<i class='fa fa-check-circle' style='color:green; font-size:18px; text-align:center;'></i>" : "<i class='fa fa-times-circle' style='color:red; font-size:18px; text-align:center;'></i>";

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);

    return tr;
};

activity_manager.prototype.create_activity_card = function (data) {
    var self = this;

    var activity_icon = '';
    if (data.activity_type == 'Phone') {
        activity_icon = 'phone';
    } else if (data.activity_type == 'Visit') {
        activity_icon = 'building';
    } else if (data.activity_type == 'Email') {
        activity_icon = 'envelope';
    } else if (data.activity_type == 'Note') {
        activity_icon = 'pencil';
    }else if(data.activity_type == 'Schedule Meeting' || data.activity_type == 'Scheduled Meeting'){
        activity_icon = 'handshake';
    }


    var now = moment().format("YYYY-MM-DD");
    var end = moment(data.scheduled_date_calendar).format("YYYY-MM-DD");

    var actiivity_card = document.createElement('div');
    var card_color = card_title_color = '';
    if(data.scheduled_duration == '0.00' || data.scheduled_time == null){
        card_color = 'kg-activity__item--default'; 
    }else if(data.status == '0'){
        card_color = ((now > end)) ? 'kg-activity__item--red' : 'kg-activity__item--default';
        card_title_color = ((now > end)) ? 'kg-activity__item__title--red' : 'kg-activity__item__title--default';
    }else if(data.status == '1'){
        card_color = 'kg-activity__item--green';
        card_title_color = 'kg-activity__item__title--green';
    }
    actiivity_card.className = "card kg-activity__item "+card_color+"  card_" + data.id;

    var actiivity_card_body = document.createElement('div');
    actiivity_card_body.className = "card-body";

    //var duration = moment.duration(now.diff(end));
    //var days = duration.asDays();

    var actiivity_card_title = document.createElement('div');
    actiivity_card_title.className = "kg-activity__item__title activity_edit "+ card_title_color;
    actiivity_card_title.setAttribute('data-item', JSON.stringify(data));
    var pre_title = (data.activity_type != 'Scheduled Meeting' && (data.scheduled_duration != '0.00' && data.scheduled_time != null && data.scheduled_time_calendar != '00:00:00')) ? 'Scheduled '+ data.activity_type : data.activity_type;
    if(data.activity_title != 'NEW CALL CENTRE LEAD'){
        actiivity_card_title.innerHTML = "<span class='mr-2'><i class='fa fa-" + activity_icon + "'></i></span>" + pre_title + ' - ' + data.activity_title ;
    }else{
        actiivity_card_title.innerHTML = data.activity_title;
    }

    var attachment_count = (data.attachment != null && data.attachment != '') ? 1 : 0;
    attachment_count = parseInt(attachment_count) + parseInt(data.attachment_count);

    var activity_card_attachment = document.createElement('a');
    activity_card_attachment.className = "ml-5";
    //activity_card_attachment.setAttribute('href', base_url + 'assets/uploads/activity_files/' + data.attachment);
    activity_card_attachment.setAttribute('data-toggle',"collapse");
    activity_card_attachment.setAttribute('href',"#card_attachment_collapse_" + data.id);
    activity_card_attachment.innerHTML = "<span><i class='fa fa-paperclip'></i></span> <small>"+attachment_count+"</small>";


    var activity_card_attachment_collapse = document.createElement('div');
    activity_card_attachment_collapse.className = 'collapse';
    activity_card_attachment_collapse.setAttribute('id',"card_attachment_collapse_" + data.id);

    var activity_card_attachment_collapse_body = document.createElement('div');
    activity_card_attachment_collapse_body.className = 'card';

    var attachment_list = document.createElement('ul');
    attachment_list.className = 'list-group';

    var multi_attachments = (data.multi_attachments != null && data.multi_attachments != '') ? data.multi_attachments.split(',') : [];
    var multi_attachment_sizes = (data.multi_attachment_sizes != null && data.multi_attachment_sizes != '') ? data.multi_attachment_sizes.split(',') : [];
    var multi_attachment_owners = (data.multi_attachment_owners != null && data.multi_attachment_owners != '') ? data.multi_attachment_owners.split(',') : [];
    var multi_attachment_dates = (data.multi_attachment_dates != null && data.multi_attachment_dates != '') ? data.multi_attachment_dates.split(',') : [];
    var multi_attachment_ids = (data.multi_attachment_ids != null && data.multi_attachment_ids != '') ? data.multi_attachment_ids.split(',') : [];
    for(var i=0; i<attachment_count; i++){
        var attachment_link = attachment_link_path = attachment_size = attachment_owner = attachment_date = attachment_id = '';
        if(i==0){
            attachment_link = (data.attachment != null && data.attachment != '') ?  data.attachment.trim() : multi_attachments[i].trim();
            attachment_link_path = (data.attachment != null && data.attachment != '') ?  'assets/uploads/activity_files/'+attachment_link : 'assets/uploads/lead_files/'+data.lead_id+'/'+attachment_link;
            attachment_link = "<a href='"+base_url+attachment_link_path+"' target='__blank'>"+attachment_link+"</a>";
            attachment_owner = (data.attachment != null && data.attachment != '') ?  data.owner_name : multi_attachment_owners[i];
            attachment_size = (data.attachment != null && data.attachment != '') ?  '-' : multi_attachment_sizes[i];
            attachment_date = (data.attachment != null && data.attachment != '') ?  moment(data.created_at_calendar).format('LLL') : moment(multi_attachment_dates[i]).format('LLL');
            attachment_id = (data.attachment != null && data.attachment != '') ?  '' : multi_attachment_ids[i];
        }else{
            attachment_link = multi_attachments[i].trim();
            attachment_link_path = 'assets/uploads/lead_files/'+ data.lead_id +'/'+attachment_link;
            attachment_link = "<a href='"+base_url+attachment_link_path+"' target='__blank'>"+attachment_link+"</a>";
            attachment_owner = multi_attachment_owners[i];
            attachment_size = multi_attachment_sizes[i];
            attachment_date = moment(multi_attachment_dates[i]).format('LLL');
            attachment_id = multi_attachment_ids[i];
        }


        var additional_attachment_details = '<div class="text-muted">'+attachment_date+' <i class="fa fa-user"></i> '+attachment_owner+' <i class="fa fa-file"></i> '+attachment_size+'</div>';
        var attachment_list_item = document.createElement('li');

        attachment_list_item.className = 'list-group-item d-flex justify-content-between align-items-center';
        attachment_list_item.innerHTML = "<span class='kg-activity__attachment_item'><i class='fa fa-paperclip'></i> "+ attachment_link + additional_attachment_details +"</span>" ;

        if(attachment_id != ''){
            var actiivity_attachment_card_action = document.createElement('a');
            actiivity_attachment_card_action.className = "kg-activity__item__action";
            actiivity_attachment_card_action.setAttribute('href', '#');
            actiivity_attachment_card_action.setAttribute('role', 'button');
            actiivity_attachment_card_action.setAttribute('data-toggle', 'dropdown');
            actiivity_attachment_card_action.setAttribute('aria-haspopup', 'true');
            actiivity_attachment_card_action.setAttribute('aria-expanded', 'false');

            var actiivity_attachment_card_action_dropdown = document.createElement('div');
            actiivity_attachment_card_action_dropdown.className = "dropdown-menu";

            var actiivity_attachment_card_action_dropdown_item_delete = document.createElement('a');
            actiivity_attachment_card_action_dropdown_item_delete.className = "dropdown-item activity_attachment_delete";
            actiivity_attachment_card_action_dropdown_item_delete.innerHTML = 'Delete';
            actiivity_attachment_card_action_dropdown_item_delete.setAttribute('data-id',attachment_id);
            actiivity_attachment_card_action_dropdown_item_delete.setAttribute('href', 'javascript:void(0)');

            actiivity_attachment_card_action_dropdown.appendChild(actiivity_attachment_card_action_dropdown_item_delete);

            attachment_list_item.appendChild(actiivity_attachment_card_action);
            attachment_list_item.appendChild(actiivity_attachment_card_action_dropdown);
            attachment_list_item.setAttribute('id','attachment_card_'+attachment_id);
        }

        attachment_list.appendChild(attachment_list_item);
    }

    activity_card_attachment_collapse_body.appendChild(attachment_list);
    activity_card_attachment_collapse.appendChild(activity_card_attachment_collapse_body);


    var activity_mark_as_completed_action_div = document.createElement('div');
    activity_mark_as_completed_action_div.className = 'col-auto my-1';

    var activity_mark_as_completed_action_div_custome_checkbox = document.createElement('div');
    activity_mark_as_completed_action_div_custome_checkbox.className = 'custom-control custom-checkbox mr-sm-2';

    var activity_mark_as_completed_action_div_input = document.createElement('input');
    activity_mark_as_completed_action_div_input.className = 'custom-control-input';
    activity_mark_as_completed_action_div_input.setAttribute('type','checkbox');

    var activity_mark_as_completed_action_div_label = document.createElement('label');
    activity_mark_as_completed_action_div_label.className = 'custom-control-label';
    activity_mark_as_completed_action_div_label.setAttribute('for','checkbox');
    activity_mark_as_completed_action_div_label.innerHTML = 'Mrak as Completed';

    activity_mark_as_completed_action_div_custome_checkbox.appendChild(activity_mark_as_completed_action_div_label);
    activity_mark_as_completed_action_div_custome_checkbox.appendChild(activity_mark_as_completed_action_div_input);
    activity_mark_as_completed_action_div.appendChild(activity_mark_as_completed_action_div_custome_checkbox);


    var actiivity_card_action = document.createElement('a');
    actiivity_card_action.setAttribute('style','display:inline-flex; float:right;')
    //actiivity_card_action.className = "kg-activity__item__action";
    //actiivity_card_action.setAttribute('href', '#');
    //actiivity_card_action.setAttribute('role', 'button');
    //actiivity_card_action.setAttribute('data-toggle', 'dropdown');
    //actiivity_card_action.setAttribute('aria-haspopup', 'true');
    //actiivity_card_action.setAttribute('aria-expanded', 'false');

    var actiivity_card_action_dropdown = document.createElement('div');
    actiivity_card_action_dropdown.className = "dropdown-menu";

    var actiivity_card_action_dropdown_item_complete = document.createElement('a');
    actiivity_card_action_dropdown_item_complete.className = "activity_complete btn btn-sm mr-2";
    actiivity_card_action_dropdown_item_complete.innerHTML = 'Mark as Completed';
    actiivity_card_action_dropdown_item_complete.setAttribute('data-id', data.id);
    actiivity_card_action_dropdown_item_complete.setAttribute('href', 'javascript:void(0)');

    var actiivity_card_action_dropdown_item_edit = document.createElement('a');
    actiivity_card_action_dropdown_item_edit.className = "activity_edit btn btn-sm mr-2";
    actiivity_card_action_dropdown_item_edit.innerHTML = 'Edit';
    actiivity_card_action_dropdown_item_edit.setAttribute('data-item', JSON.stringify(data));
    actiivity_card_action_dropdown_item_edit.setAttribute('href', 'javascript:void(0)');

    var actiivity_card_action_dropdown_item_delete = document.createElement('a');
    actiivity_card_action_dropdown_item_delete.className = "activity_delete btn btn-sm";
    actiivity_card_action_dropdown_item_delete.innerHTML = 'Delete';
    actiivity_card_action_dropdown_item_delete.setAttribute('data-id', data.id);
    actiivity_card_action_dropdown_item_delete.setAttribute('href', 'javascript:void(0)');


    if(data.status == '0' && data.scheduled_duration != '0.00' && data.scheduled_time_calendar != '00:00:00'){
        actiivity_card_action.appendChild(actiivity_card_action_dropdown_item_complete);
    }

    actiivity_card_action.appendChild(actiivity_card_action_dropdown_item_edit);
    //actiivity_card_action_dropdown.appendChild(actiivity_card_action_dropdown_item_delete);

    var actiivity_card_subtitle = document.createElement('div');
    actiivity_card_subtitle.className = 'card-subtitle mb-4';

    var actiivity_card_subtitle_item_date = document.createElement('span');
    actiivity_card_subtitle_item_date.className = 'text-muted';
    actiivity_card_subtitle_item_date.innerHTML = '<i class="fa fa-calendar"></i> Date of Lead: ' + moment(data.created_at_calendar).format('LLL') + ' <br/>';
    
    var text_color = (now > end) ? 'text-danger' : 'text-success';
    var activity_date = (data.scheduled_duration == '0.00' || data.scheduled_time == null) ? '' : '<small class="ml-1 '+text_color+'">' + moment(data.scheduled_date_calendar).format('LL') + '</small>';
    var activity_time = (data.scheduled_duration == '0.00' || data.scheduled_time == null) ? '' : '<small class="ml-1 '+text_color+'">' + data.scheduled_time + '</small>';
    
    if(data.scheduled_duration != '0.00' && data.scheduled_time_calendar != '00:00:00'){
        if(activity_date != ''){
            actiivity_card_subtitle_item_date.innerHTML += '<i class="fa fa-calendar"></i>  Appointment Date: ' + activity_date + ' <br/>';
        }
        if(activity_time != ''){
            actiivity_card_subtitle_item_date.innerHTML += '<i class="fa fa-clock"></i>  Appointment Time: ' + activity_time + ' <br/>';
        }
    }
    
    actiivity_card_subtitle_item_date.innerHTML += '<i class="fa fa-user"></i> Activity Created By: ' + data.owner_name + ' <br/>'; 

    if(data.lead_source === 'Call Centre Lead'){
        actiivity_card_subtitle_item_date.innerHTML += '<i class="fa fa-user"></i> Call Centre Rep: ' + data.created_by + ' <br/>'; 
    }

    actiivity_card_subtitle_item_date.innerHTML += '<i class="fa fa-user"></i> Lead Allocated to: ' + data.assigned_to + ' <br/>'; 

    var actiivity_card_subtitle_item_user = document.createElement('span');
    actiivity_card_subtitle_item_user.className = 'text-muted';
    actiivity_card_subtitle_item_user.innerHTML =  data.first_name + ' ' + data.last_name;

    var address = (data.customer_address != '' && data.customer_address != null) ? data.customer_address : 'None';
    var actiivity_card_subtitle_item_site = document.createElement('span');
    actiivity_card_subtitle_item_site.className = 'text-muted ml-3';
    actiivity_card_subtitle_item_site.innerHTML = '<i class="fa fa-building"></i> ' + address;
    
    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_date);
    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_user);
    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_site);

    var actiivity_card_description = document.createElement('p');
    actiivity_card_description.className = 'kg-activity__item__description';
    actiivity_card_description.innerHTML = data.activity_note;

    actiivity_card_body.appendChild(actiivity_card_title);
    actiivity_card_body.appendChild(actiivity_card_action);
    actiivity_card_body.appendChild(actiivity_card_action_dropdown);

    if (attachment_count != 0) {
        actiivity_card_body.appendChild(activity_card_attachment);
        actiivity_card_body.appendChild(activity_card_attachment_collapse);
    }
    actiivity_card_body.appendChild(actiivity_card_subtitle);
    actiivity_card_body.appendChild(actiivity_card_description);

    actiivity_card.appendChild(actiivity_card_body);

    return actiivity_card;
};

activity_manager.prototype.fetch_activity_data = function (form_data) {
    var self = this;
    $.ajax({
        url: base_url + "admin/activity/fetch?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (stat) {
            if (stat.success == true) {
                if (stat.activities.length > 0) {
                    if (self.view == 'list') {
                        document.getElementById('activity_table_body').innerHTML = '';
                        self.handle_list_view(stat);
                    } else if (self.view == 'calendar') {
                        document.getElementById('calendar').innerHTML = '';
                        self.handle_calendar_view(stat);
                    } else if (self.view == 'lead') {
                        document.getElementById('all_activity_container').innerHTML = '';
                        self.handle_lead_view(stat);
                    }
                } else {
                    if (self.view == 'list') {
                        document.getElementById('activity_table_body').innerHTML = '<tr><td class="text-center" colspan="10">No Activity Data Found</td></tr>';
                    } else if (self.view == 'calendar') {
                        document.getElementById('calendar').innerHTML = '<div class="alert alert-danger text-center">No Activity Data Found</div>';
                    } else if (self.view == 'lead') {
                        document.getElementById('all_activity_container').innerHTML = '<div class="alert alert-danger text-center">No Activity Data Found</div>';
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (self.view == 'list') {
                document.getElementById('activity_table_body').innerHTML = 'Looks Like Something Went Wrong';
            } else if (self.view == 'calendar') {
                document.getElementById('calendar').innerHTML = 'Looks Like Something Went Wrong';
            } else if (self.view == 'calendar') {
                document.getElementById('all_activity_container').innerHTML = 'Looks Like Something Went Wrong';
            }

        }
    });
};

activity_manager.prototype.handle_list_view = function (stat) {
    var self = this;
    if ($('#activity_table')) {
        $('#activity_table').DataTable().clear();
        $('#activity_table').DataTable().destroy();
    }
    for (var i = 0; i < stat.activities.length; i++) {
        var stats = self.create_activity_table_row(stat.activities[i]);
        document.getElementById('activity_table_body').appendChild(stats);
    }
    var table = $('#activity_table').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "order": [
            [0, "asc"]
        ],
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": true,
        paging: false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Columns',
                postfixButtons: ['colvisRestore'],
            }
        ]
    });
    $('.event_edit').click(function () {
        var activity_data = $(this).attr('data-item');
        activity_data = JSON.parse(activity_data);
        self.edit_activity(activity_data);
    });
    $('[data-toggle="tooltip"]').tooltip();
};

activity_manager.prototype.handle_color = function (lead_stage) {
    if (lead_stage > 0 && lead_stage < 3) {
        return 'yellow';
    } else if (lead_stage > 2 && lead_stage < 8) {
        return 'grey';
    } else if (lead_stage > 7 && lead_stage < 10) {
        return 'amber';
    } else if (lead_stage == 10) {
        return 'blue';
    } else if (lead_stage == 11) {
        return 'green';
    } else if (lead_stage == 12) {
        return 'red';
    }
};

activity_manager.prototype.handle_calendar_view = function (stat) {
    var self = this;
    var events = [];
    for (var i = 0; i < stat.activities.length; i++) {
        var event_obj = {};
        event_obj.title = ' ';
        event_obj.title += (stat.activities[i].activity_title != '') ? stat.activities[i].activity_type : stat.activities[i].activity_type;
        event_obj.title += ' - ' + stat.activities[i].company_name;
        event_obj.className = 'event_' + stat.activities[i].activity_type;
        event_obj.className += ' calendar_grid-item--' + self.handle_color(parseInt(stat.activities[i].lead_stage));

        var schedule_date = stat.activities[i].scheduled_date_calendar;
        var schedule_time = stat.activities[i].scheduled_time_calendar;
        var schedule_duration = stat.activities[i].scheduled_duration;

        if (schedule_date != '' && schedule_date != null && schedule_time != '' && schedule_time != null && schedule_duration != '' && schedule_duration != null) {
            var start_date = stat.activities[i].scheduled_date_calendar;
            var start_time = stat.activities[i].scheduled_time_calendar;
            event_obj.start = moment(start_date).format('YYYY-MM-DD') + 'T' + moment(start_time, 'HH:mm:ss').format('HH:mm');
            var end_date = start_date;
            event_obj.end = end_date + 'T' + moment(start_time, 'HH:mm:ss').add(parseFloat(stat.activities[i].scheduled_duration), 'hours').format('HH:mm');
        } else {
            event_obj.start = moment(stat.activities[i].created_at_calendar).format('YYYY-MM-DD HH:mm:ss');
        }

        if (stat.activities[i].activity_type == 'Phone') {
            event_obj.icon = 'phone';
        } else if (stat.activities[i].activity_type == 'Visit') {
            event_obj.icon = 'building';
        } else if (stat.activities[i].activity_type == 'Email') {
            event_obj.icon = 'envelope';
        } else if (stat.activities[i].activity_type == 'Note') {
            event_obj.icon = 'pencil';
        }else if(stat.activities[i].activity_type == 'Schedule Meeting' || stat.activities[i].activity_type == 'Scheduled Meeting'){
            event_obj.icon = 'handshake';
        }
        event_obj.data = stat.activities[i];
        event_obj.is_completed = stat.activities[i].status;
        event_obj.is_google_synced = (stat.activities[i].gcal_event_id && stat.activities[i].gcal_event_id != null) ? true : false;
        events.push(event_obj);
    }

    $('#calendar').fullCalendar({
        minTime: "06:00:00",
        maxTime: "19:00:00",
        height: 'auto',
        defaultView: 'eventWeekDay',
        groupByResource: true,
        customButtons: {
            eventPhone: {
                text: 'Phone',
                click: function () {
                    $('.event_Phone').toggle();
                }
            },
            eventVisit: {
                text: 'Site Visit',
                click: function () {
                    $('.event_Visit').toggle();
                }
            },
            eventEmail: {
                text: 'Email',
                click: function () {
                    $('.event_Email').toggle();
                }
            }
        },
        header: {
            left: 'eventPhone,eventVisit,eventEmail',
            center: 'title',
            right: 'prev,next,agendaDay,eventWeekDay'
        },
        views: {
            eventWeekDay: {
                type: 'agenda',
                duration: {days: 7},
                eventLimit: 6
            }
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        navLinks: false,
        events: events,
        eventRender: function (event, element) {
            if (event.icon) {
                element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i> ");
            }

            if (event.is_google_synced) {
                element.find(".fc-time").append('<i class="fa fa-google" aria-hidden="true" style="float:right; margin-left:5px;"></i>');
            }

            if (event.is_completed == '1') {
                element.find(".fc-time").append("<span style='float:right;'> <i class='fa fa-check-circle' style='color:green; font-size:12px;'></i> </span>");
            }
        },
        eventClick: function (event, jsEvent, view) {
            var activity_data = event.data;
            self.edit_activity(activity_data);
        },
        eventAllow: function (dropLocation, draggedEvent) {
            if(draggedEvent.start != null && draggedEvent.end == null){
                toastr.clear();
                toastr["error"]('Oops you cant reschedule an non-scheduled activity.');
                return false;
            }else {
                return true;
            }
        },
        eventDrop: function (event, jsEvent, view) {
            var activity_data = event.data;
            if(event.start != null && event.end != null){
                activity_data.scheduled_date_calendar = event.start;
                var start_time = moment(event.start).format('LT');
                var end_time = moment(event.end).format('LT');
                var start_duration_hour = event.start._i[3];
                var start_duration_min = (event.start._i[4] == 0) ? 0 : ((event.start._i[4] == 30)) ? 0.50 : 1;
                
                var end_duration_hour = event.end._i[3];
                var end_duration_min = (event.end._i[4] == 0) ? 0 : ((event.end._i[4] == 30)) ? 0.50 : 1;
                var start_duration = parseFloat(start_duration_hour) + parseFloat(start_duration_min);
                var end_duration = parseFloat(end_duration_hour) + parseFloat(end_duration_min);
               
                var duration = (parseFloat(start_duration - end_duration));
                activity_data.scheduled_time = start_time;
                activity_data.scheduled_duration = duration;
                self.reschedule_activity(activity_data);
            }else if(event.start != null && event.end == null){
                var start_time = moment(event.start).format('LT');
                var end_time = moment(event.end).format('LT');
                var start_duration_hour = event.start._i[3];
                var start_duration_min = (event.start._i[4] == 0) ? 0 : ((event.start._i[4] == 30)) ? 0.50 : 1;
                activity_data.scheduled_time = start_time;
                activity_data.created_at_calendar = event.start;
            }
        },
        eventResize: function (event, jsEvent, view) {
            var activity_data = event.data;
            if(activity_data.scheduled_duration == '0.00'){
                toastr.clear();
                toastr["error"]('Oops you cant reschedule an non-scheduled activity.');
                var start_time = moment(event.start).format('LT');
                var end_time = moment(event.end).format('LT');
                var start_duration_hour = event.start._i[3];
                var start_duration_min = (event.start._i[4] == 0) ? 0 : ((event.start._i[4] == 30)) ? 0.50 : 1;
                activity_data.scheduled_time = start_time;
                activity_data.created_at_calendar = event.start;
            }else if(event.start != null && event.end != null){
                activity_data.scheduled_date_calendar = event.start;
                var start_time = moment(event.start).format('LT');
                var end_time = moment(event.end).format('LT');
                var start_duration_hour = event.start._i[3];
                var start_duration_min = (event.start._i[4] == 0) ? 0 : ((event.start._i[4] == 30)) ? 0.50 : 1;
                
                var end_duration_hour = event.end._i[3];
                var end_duration_min = (event.end._i[4] == 0) ? 0 : ((event.end._i[4] == 30)) ? 0.50 : 1;
                var start_duration = parseFloat(start_duration_hour) + parseFloat(start_duration_min);
                var end_duration = parseFloat(end_duration_hour) + parseFloat(end_duration_min);
               
                var duration = (parseFloat(start_duration - end_duration));
                activity_data.scheduled_time = start_time;
                activity_data.scheduled_duration = duration;
                self.reschedule_activity(activity_data);
            }
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
}

activity_manager.prototype.handle_lead_view = function (data) {
    var self = this;
    document.getElementById('all_activity_container').innerHTML = "";
    for (var i = 0; i < data.activities.length; i++) {
        var activity_card = self.create_activity_card(data.activities[i]);
        document.getElementById('all_activity_container').appendChild(activity_card);
    }
    $('.kg-activity__item').click(function (e) {
        if (e.originalEvent.detail === 2) {
            $(this).find('div > p.kg-activity__item__description').slideToggle();
        }
    });
};

activity_manager.prototype.save_activity = function () {
    var self = this;
    $(document).on('click', '#addActivity', function () {
        var flag = true;
        var activity_title = $('#activity_title').val();
        var activity_type = $('#activity_type').val();
        $('#activity_title').removeClass('is-invalid');
        $('#activity_type').removeClass('is-invalid');
        $('#scheduled_date_value').removeClass('is-invalid');
        $('#scheduled_time_value').removeClass('is-invalid');
        $('#scheduled_duration_value').removeClass('is-invalid');

        if (activity_title == '') {
            $('#activity_title').addClass('is-invalid');
            flag = false;
        }

        if (activity_type == '') {
            $('#activity_type').addClass('is-invalid');
            flag = false;
        }

        if (activity_type != 'Note' && self.activity_trigger == 2) {
            var scheduled_date_value = $('#scheduled_date_value').val();
            var scheduled_time_value = $('#scheduled_time_value').val();
            var scheduled_duration_value = $('#scheduled_duration_value').val();

            if (scheduled_date_value == '') {
                $('#scheduled_date_value').addClass('is-invalid');
                flag = false;
            }
            if (scheduled_time_value == '') {
                $('#scheduled_time_value').addClass('is-invalid');
                flag = false;
            }
            if (scheduled_duration_value == '') {
                $('#scheduled_duration_value').addClass('is-invalid');
                flag = false;
            }

        }

        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            $("#addActivity").attr("disabled", "disabled");
            var activity_add_form_data = $('#activity_add_form').serialize();
            self.save_activity_handler(activity_add_form_data,true);
            $("#addActivity").removeAttr("disabled");
        }
    });
};

activity_manager.prototype.save_activity_handler = function (data,reintialize) {
    var self = this;
    if (self.lead_data !== '') {
        data += '&lead_id=' + self.lead_data.id;
    }
    if (self.activity_action !== '') {
        data += '&activity_action=' + self.activity_action;
    }
    
    var editor_data = tinyMCE.get('activity_note').getContent().trim();
    editor_data = encodeURIComponent(editor_data);
    data += '&activity[activity_note]='+ editor_data;

    $.ajax({
        type: 'POST',
        url: base_url + 'admin/activity/save',
        datatype: 'json',
        data: data + '&activity_action=' + self.activity_action + '&action=save_activity',
        beforeSend: function () {
            toastr["info"]('Activity is being saved. Please wait...');
        },
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#activity_add_form').trigger("reset");
                $('.custom-file-label').html('Choose attachment file');
                $('#activity_modal').modal('hide');

                if (stat.gcal_success != undefined && stat.gcal_success == false) {
                    toastr["error"]('Looks like their was some issue in syncronizing the event with Google Calendar. ('+stat.gcal_status+')');
                }
                if(reintialize){
                    self.initialize();
                }
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

activity_manager.prototype.edit_activity = function (activity_data) {
    var self = this;
    self.lead_data = {};
    self.lead_data.id = activity_data.lead_id;

    $('#mark_as_completed').removeClass('hidden');
    $('#activity_add_form').trigger("reset");

    //Make Activity Attachment Blank
    document.getElementById('activity_attachments').innerHTML = '';
    $('.dz-preview').remove();
    $('.dropzone').removeClass('dz-started');
    
    $('#status').removeAttr('checked');
    if (activity_data.status == '1') {
        $('#status').attr('checked', 'checked');
        $('#status').prop("checked", true);
    }
    
    var deal_link = base_url + '/admin/lead/add?deal_ref=' + activity_data.uuid;
    $('#deal_link').attr('href', deal_link);
    
    //Cehck if Add or Schedule
    var scheduled_duration_value = activity_data.scheduled_duration;
    if (scheduled_duration_value != '0.00') {
       self.activity_trigger = 2;
        $('#activity_modal_body_left').addClass('col-md-6');
        $('#activity_modal_body_left').removeClass('col-md-12');
        $('#scheduler').removeClass('hidden');
        $('#attendees').removeClass('hidden');
    } else {
        self.activity_trigger = 1;
        $('#scheduler').addClass('hidden');
        $('#attendees').addClass('hidden');
        $('#activity_modal_body_left').removeClass('col-md-6');
        $('#activity_modal_body_left').addClass('col-md-12');
    }
    
    //Append Activity Data to inputs
    $('#activity_title').val(activity_data.activity_title);
    $('#activity_type').val(activity_data.activity_type);
    $('#activity_note').val(activity_data.activity_note);
    tinyMCE.get('activity_note').setContent(activity_data.activity_note);
    $('#activity_id').val(activity_data.id);
    $('#activity_file_value').val(activity_data.attachment);
    if (activity_data.attachment != '' && activity_data.attachment != null) {
        $('.custom-file-label').html(activity_data.attachment);
    } else {
        $('.custom-file-label').html('Choose attachment file');
    }

    if (activity_data.scheduled_date_calendar != '' && activity_data.scheduled_date_calendar != null) {
        $('#scheduled_date_value').val(moment(activity_data.scheduled_date_calendar).format('DD-MM-YYYY'));
    }
    if (activity_data.scheduled_time != '' && activity_data.scheduled_time != null) {
        //console.log(activity_data.scheduled_time);
        //$('#scheduled_time_value').timepicker('setTime', activity_data.scheduled_time);
        //console.log(activity_data.scheduled_time);
        $('#scheduled_time_value').val(activity_data.scheduled_time);
    }
    if (activity_data.scheduled_duration != '' && activity_data.scheduled_duration != null) {
        //console.log(activity_data.scheduled_duration);
        var time = self.floatToTime(activity_data.scheduled_duration, true);
        //$('#scheduled_duration_value').timepicker('setTime', time);
        $('#scheduled_duration_value').val(time);
    }
    
    console.log(activity_data);
    
    //Append Location data to customer location default if activity location data is null
    if (activity_data.address != 'null' && activity_data.address != '') {
        $('#activityLocationPostCode').val(activity_data.customer_postcode);
        $('#activityLocationState').val(activity_data.customer_state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.customer_address);
        $('#activityLocationAddressVal').val(activity_data.customer_address);
    } else {
        $('#activityLocationPostCode').val(activity_data.postcode);
        $('#activityLocationState').val(activity_data.state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.address);
        $('#activityLocationAddressVal').val(activity_data.address);
    }

    //Get GCAL EVENT DATA
    var gcal_event_data = JSON.parse(activity_data.gcal_event_data);

    //If gcal_event_data empty or null open and dont proceed furthur
    if (gcal_event_data === '' || gcal_event_data === null) {
        //Set Send Updates to false
        document.getElementById("sendUpdates").checked = false;
        document.getElementById("event_reminders_duration").value = '';
        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2();

        $('#activity_modal').modal('show');
        return false;
    }

    //Append Attendees if any
    if (gcal_event_data.event_data.attendees !== undefined) {
        var attendees = [];
        if (gcal_event_data.event_data.attendees.length > 0) {
            for (var i = 0; i < gcal_event_data.event_data.attendees.length; i++) {
                var obj = {};
                obj.id = gcal_event_data.event_data.attendees[i].email;
                obj.text = gcal_event_data.event_data.attendees[i].email;
                obj.selected = true;
                attendees.push(obj);
            }
        }

        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2(attendees);
    }

    //Check SendUpates if enabled
    if (gcal_event_data.event_optional_data.sendUpdates !== undefined && gcal_event_data.event_optional_data.sendUpdates == 'all') {
        document.getElementById("sendUpdates").checked = true;
    }

    //Check if Reminder Time set
    if (gcal_event_data.event_data.reminders !== undefined && gcal_event_data.event_data.reminders.overrides !== undefined) {
        document.getElementById("event_reminders_duration").value = gcal_event_data.event_data.reminders.overrides[0].minutes;
    }


    $('#activity_modal').modal('show');
};

activity_manager.prototype.reschedule_activity = function (activity_data) {
    var self = this;
    $('#activity_add_form').trigger("reset");

    //Make Activity Attachment Blank
    document.getElementById('activity_attachments').innerHTML = '';
    $('.dz-preview').remove();
    $('.dropzone').removeClass('dz-started');

    if (activity_data.status == '1') {
        $('#status').attr('checked', 'checked');
    }
    var deal_link = base_url + '/admin/lead/add?deal_ref=' + activity_data.uuid;
    $('#deal_link').attr('href', deal_link);

    //Append Activity Data to inputs
    $('#activity_title').val(activity_data.activity_title);
    $('#activity_type').val(activity_data.activity_type);
    $('#activity_note').val(activity_data.activity_note);
    tinyMCE.get('activity_note').setContent(activity_data.activity_note);
    $('#activity_id').val(activity_data.id);
    $('#activity_file_value').val(activity_data.attachment);
    if (activity_data.attachment != '' && activity_data.attachment != null) {
        $('.custom-file-label').html(activity_data.attachment);
    } else {
        $('.custom-file-label').html('Choose attachment file');
    }

    if (activity_data.scheduled_date_calendar != '' && activity_data.scheduled_date_calendar != null) {
        $('#scheduled_date_value').val(moment(activity_data.scheduled_date_calendar).format('DD-MM-YYYY'));
    }
    if (activity_data.scheduled_time != '' && activity_data.scheduled_time != null) {
        //console.log(activity_data.scheduled_time);
        //$('#scheduled_time_value').timepicker('setTime', activity_data.scheduled_time);
        $('#scheduled_time_value').val(activity_data.scheduled_time);
    }
    if (activity_data.scheduled_duration != '' && activity_data.scheduled_duration != null) {
        //console.log(activity_data.scheduled_duration);
        var time = self.floatToTime(activity_data.scheduled_duration, true);
        //$('#scheduled_duration_value').timepicker('setTime', time);
        $('#scheduled_duration_value').val(time);
    }

    //Append Location data to customer location default if activity location data is null
    if (activity_data.address != 'null' && activity_data.address != '') {
        $('#activityLocationPostCode').val(activity_data.customer_postcode);
        $('#activityLocationState').val(activity_data.customer_state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.customer_address);
        $('#activityLocationAddressVal').val(activity_data.customer_address);
    } else {
        $('#activityLocationPostCode').val(activity_data.postcode);
        $('#activityLocationState').val(activity_data.state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.address);
        $('#activityLocationAddressVal').val(activity_data.address);
    }

    //Get GCAL EVENT DATA
    var gcal_event_data = JSON.parse(activity_data.gcal_event_data);

    //If gcal_event_data empty or null open and dont proceed furthur
    if (gcal_event_data === '' || gcal_event_data === null) {
        //Set Send Updates to false
        document.getElementById("sendUpdates").checked = false;
        document.getElementById("event_reminders_duration").value = '';
        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2();

        $("#addActivity").attr("disabled", "disabled");
        var activity_add_form_data = $('#activity_add_form').serialize();
        self.save_activity_handler(activity_add_form_data,false);
        $("#addActivity").removeAttr("disabled");
        return false;
    }

    //Append Attendees if any
    if (gcal_event_data.event_data.attendees !== undefined) {
        var attendees = [];
        if (gcal_event_data.event_data.attendees.length > 0) {
            for (var i = 0; i < gcal_event_data.event_data.attendees.length; i++) {
                var obj = {};
                obj.id = gcal_event_data.event_data.attendees[i].email;
                obj.text = gcal_event_data.event_data.attendees[i].email;
                obj.selected = true;
                attendees.push(obj);
            }
        }

        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2(attendees);
    }

    //Check SendUpates if enabled
    if (gcal_event_data.event_optional_data.sendUpdates !== undefined && gcal_event_data.event_optional_data.sendUpdates == 'all') {
        document.getElementById("sendUpdates").checked = true;
    }

    //Check if Reminder Time set
    if (gcal_event_data.event_data.reminders !== undefined && gcal_event_data.event_data.reminders.overrides !== undefined) {
        document.getElementById("event_reminders_duration").value = gcal_event_data.event_data.reminders.overrides[0].minutes;
    }

    $("#addActivity").attr("disabled", "disabled");
    var activity_add_form_data = $('#activity_add_form').serialize();
    self.save_activity_handler(activity_add_form_data,false);
    $("#addActivity").removeAttr("disabled");
}

activity_manager.prototype.edit_activity_proposal = function () {
    var self = this;
    $(document).on('click', '.activity_edit', function () {
        var $this = $(this);
        var activity_data = JSON.parse($this.attr('data-item'));
        self.edit_activity(activity_data);
    });
};

activity_manager.prototype.mark_as_completed = function () {
    var self = this;
    $(document).on('click', '.activity_complete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/activity/mark_as_completed',
            datatype: 'json',
            data: 'id=' + id,
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    self.initialize();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

activity_manager.prototype.upload_activity_file = function () {
    var self = this;
    $('#activity_file').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'activity_files');
        toastr["info"]("Uploading file please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_activity_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                toastr["info"]('File is being uploaded. Please wait...');
            },
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="activity[attachment]"').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

activity_manager.prototype.delete_activity = function () {
    var self = this;
    $(document).on('click', '.activity_delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/activity/delete',
            datatype: 'json',
            data: 'id=' + id + '&action=delete_activity',
            beforeSend: function () {
                toastr["info"]('Activity deletion is in progress. Please wait...');
            },
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $('.card_' + id).remove();
                    if (stat.gcal_success != undefined && stat.gcal_success == false) {
                        toastr["error"]('Looks like their was some issue in deleting the event with Google Calendar, please try to delete it manually.');
                    }
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

activity_manager.prototype.delete_activity_attachment = function () {
    var self = this;
    $(document).on('click', '.activity_attachment_delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/uploader/delete_activity_attachments',
            datatype: 'json',
            data: 'id=' + id,
            beforeSend: function () {
                toastr["info"]('Attachment deletion is in progress. Please wait...');
            },
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $('#attachment_card_' + id).remove();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

activity_manager.prototype.handle_dropzone = function(){
    var self = this;

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#lead_dropzone",
        {
        timeout: 0,
        maxFilesize: 15,
        url: base_url + "admin/uploader/upload_attachment_files",
        sending: function (file, xhr, formData) {
            formData.append("lead_id", self.lead_data.id);
        },
        success: function (file, response) {
            response = JSON.parse(response);
            if (response.success) {
                toastr['success'](response.status);
                var activity_attachments = document.getElementById('activity_attachments');
                var attachment_input = document.createElement('input');
                attachment_input.setAttribute('type','hidden');
                attachment_input.setAttribute('name','activity_attachments[]');
                attachment_input.setAttribute('value',response.file_name);

                var attachment_size_input = document.createElement('input');
                attachment_size_input.setAttribute('type','hidden');
                attachment_size_input.setAttribute('name','activity_attachment_sizes[]');
                attachment_size_input.setAttribute('value',response.file_size);

                activity_attachments.appendChild(attachment_input);
                activity_attachments.appendChild(attachment_size_input);
                setTimeout(function(){
                    //file.previewElement.parentNode.removeChild(file.previewElement);
                    //$('.dropzone').removeClass('dz-started');
                },1500);
            } else {
                toastr['error'](response.status);
            }
        },
        error: function (file, response) {
            toastr["error"](response);
        }
    });
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notify {

    /**
     * Constructor
     */
    public function __construct() {
        // get main CI object
        $this->lt = & get_instance();
        $this->lt->load->model("Notification_Model", "notificaton");
        $this->lt->load->library("Email_Manager", 'email_manager');
        $this->lt->load->library("Sms");
    }

    public function store_notification($type, $notification_data = []) {
        //Preapare Data to be stored
        if (!empty($notification_data)) {
            $insert_data = $this->handle_notification_data($type, $notification_data);
            $this->lt->common->insert_data('tbl_notifications', $insert_data);
        }
    }

    private function handle_notification_data($type, $notitifcation_data) {
        $data = array();
        switch ($type) {
            case 'NEW_LEAD' :
                $data = $this->parse_new_lead_data($notitifcation_data);
                break;
            case 'LEAD_FOLLOW_UP' :
                $data = $this->parse_lead_follow_up_data($notitifcation_data);
                break;
            case 'NEW_LEAD_SURVEY_4W' :
                $data = $this->parse_lead_survey_data($notitifcation_data);
                break;
            case 'NEW_LEAD_SURVEY_2W' :
                $data = $this->parse_lead_survey_followup_data($notitifcation_data);
                break;
            case 'NEW_LEAD_BY_LEAD_CREATOR' :
                $data = $this->parse_new_lead_by_lead_creator($notitifcation_data);
                break;
            case 'NEW_WEB_LEAD' :
                $data = $this->parse_new_web_lead_data($notitifcation_data);
                break;
            case 'WEB_LEAD_FOLLOW_UP' :
                $data = $this->parse_new_web_lead_follow_up_data($notitifcation_data);
                break;
            case 'default':
                break;
        }
        return $data;
    }

    private function parse_new_lead_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_allocation_data($notitifcation_data['id']);
       
        if (!empty($data)) {
            $parse_data['reference_id'] = $data['id'];
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'NEW_LEAD';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            //Create new lead self Generate Activity
            //$this->create_new_lead_activity($data);
            //Send Emails and SMS
            //$this->lt->email_manager->send_leademail_to_franchise($data);
            $this->lt->email_manager->send_leademail_to_leaders($data);
            $this->lt->sms->send_sms($data);
        }
        return $parse_data;
    }
    
    private function parse_new_web_lead_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_notification_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['reference_id'] = $data['id'];
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'NEW_WEB_LEAD';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            //Send Emails
            $this->lt->email_manager->send_web_leademail($data);
        }
        return $parse_data;
    }
    
    private function parse_new_web_lead_follow_up_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_notification_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['reference_id'] = $data['id'];
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'WEB_LEAD_FOLLOW_UP';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            //Send Emails
            $this->lt->email_manager->send_web_leademail($data,false);
        }
        return $parse_data;
    }

    private function parse_lead_follow_up_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_allocation_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['reference_id'] = $data['id'];
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'LEAD_FOLLOW_UP';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            //Send Emails and SMS
            //$this->lt->email_manager->send_leademail_to_franchise($data, FALSE);
            //$this->lt->email_manager->send_leademail_to_webleads($data, FALSE);
            //$this->lt->sms->send_sms($data, FALSE);
        }
        return $parse_data;
    }

    private function parse_lead_survey_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_allocation_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'NEW_LEAD_SURVEY_4W';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            $this->lt->email_manager->send_survey_email_to_customer($data, FALSE);
        }
        return $parse_data;
    }

    private function parse_lead_survey_followup_data($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_allocation_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'NEW_LEAD_SURVEY_2W';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            $this->lt->email_manager->send_survey_followup_email_to_customer($data, FALSE);
        }
        return $parse_data;
    }
    
    private function parse_new_lead_by_lead_creator($notitifcation_data = []) {
        $parse_data = array();
        $data = $this->lt->notificaton->get_lead_notification_data($notitifcation_data['id']);
        if (!empty($data)) {
            $parse_data['reference_id'] = $data['id'];
            $parse_data['receiver_user_id'] = $data['franchise_id'];
            $parse_data['receiver_cust_id'] = $data['cust_id'];
            $parse_data['notification_type'] = 'NEW_LEAD_BY_LEAD_CREATOR';
            $parse_data['url'] = NULL;
            $parse_data['data'] = json_encode($data);
            //Send Emails
            $this->lt->email_manager->send_leademail_by_lead_creator_to_leaders($data);
        }
        return $parse_data;
    }

    private function create_new_lead_activity($activity_data) {
        if (!empty($activity_data)) {

            $activity_note = "Sales Manager allocated to: " . $activity_data['franchise_name']. "<br/><br/>";

            $activity_note .= "Service category: " . $activity_data['category_name']. "<br/>";
            
            if(isset($activity_data['daily_average_usage']) && $activity_data['daily_average_usage'] != 0){
                $activity_note .= "Daily Average Usage: " . $activity_data['daily_average_usage']. "<br/>";
            }
            if(isset($activity_data['highbay']) && $activity_data['highbay'] != 0){
                $activity_note .= "Highbay: " . $activity_data['highbay']. "<br/>";
            }
            if(isset($activity_data['panel_light']) && $activity_data['panel_light'] != 0){
                $activity_note .= "Panel Light: " . $activity_data['panel_light']. "<br/>";
            }
            if(isset($activity_data['flood_light']) && $activity_data['flood_light'] != 0){
                $activity_note .= "Flood Light: " . $activity_data['flood_light']. "<br/>";
            }
            if(isset($activity_data['batten_light']) && $activity_data['batten_light'] != 0){
                $activity_note .= "Battens: " . $activity_data['batten_light']. "<br/>";
            }

            if(isset($activity_data['note'])){
                $activity_note .= "<br/>Note: " . $activity_data['note'];
            }

            $parse_data['lead_id'] = $activity_data['id'];
            $parse_data['user_id'] = $activity_data['lead_creator_id'];
            $parse_data['activity_type'] = 'Note';
            $parse_data['activity_note'] = $activity_note;
            $parse_data['activity_title'] = 'NEW CALL CENTRE LEAD';
            $parse_data['scheduled_date'] = NULL;
            $parse_data['scheduled_time'] = NULL;
            $parse_data['scheduled_duration'] = NULL;
            $parse_data['status	'] = 1;
            $this->lt->common->insert_data('tbl_activities', $parse_data);
        }
    }

}

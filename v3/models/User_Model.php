<?php

class User_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    
    /**
     * fetch User List
     * @param int group_id
     * @param int id
     * @param int owner_id
     * @return array
     */
    function get_users($group_id = FALSE, $id = FALSE,$owner_id = FALSE , $custom_cond = FALSE) {
        
        $cond = '';
        
        if ($group_id) {
            $cond .= " WHERE user_group.group_id = $group_id";
        }
        
        if ($id) {
            $cond .= " WHERE user.id='" . $id . "'";
        }
        
        if ($owner_id) {
            $cond .= " WHERE utu.owner_id='".$owner_id."'";
        }
        
        if ($custom_cond) {
            $cond = $custom_cond;
        }
        
        $sql = "SELECT user.*,ud.*,"
                . "ul.address,ul.postcode,ul.workarea_postcodes,ul.workarea_postcodes_secondary,ul.state_id,ul.latitude,ul.longitude,ul.location_type,"
                . "user_group.group_id,"
                . "state.state_postal"
                . " FROM aauth_users as user";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ul.state_id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = user.id";
        if ($owner_id) {
            $sql .= " LEFT JOIN aauth_user_to_user as utu ON utu.user_id = user.id";
        }
        if(!$custom_cond){
            $sql .= " $cond GROUP BY user.id";
        }else{
            $sql .= " $cond"; 
        }
       
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    /**
     * fetch Sub User List
     * @param int group_id
     * @param int id
     * @param int owner_id
     * @return array
     */
    function get_sub_users($group_id = FALSE, $id = FALSE,$owner_id = FALSE) {
        
        $cond = '';
        
        if ($group_id) {
            $cond .= " WHERE user_group.group_id = $group_id";
        }
        
        if ($id) {
            $cond .= " WHERE ud.user_id='" . $id . "'";
        }
        
        if ($owner_id) {
            $cond .= " WHERE utu.owner_id='".$owner_id."'";
        }
        
        $sql = "SELECT user.*,ud.*,"
                . "ul.address,ul.postcode,ul.workarea_postcodes,ul.workarea_postcodes_secondary,ul.state_id,ul.latitude,ul.longitude,ul.location_type,"
                . "user_group.group_id,"
                . "utu.owner_id,"
                . "state.state_postal"
                . " FROM aauth_users as user";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = ul.state_id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = user.id";
        $sql .= " LEFT JOIN aauth_user_to_user as utu ON utu.user_id = user.id";
        
        $sql .= " $cond GROUP BY user.id";          
       
        $query = $this->db->query($sql);
        return $query->result_array();
    }
      
    
}

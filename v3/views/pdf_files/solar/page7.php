<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
 <tr>
  <td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img1.jpg" alt="" style="display:inline-block"/>6</td>
</tr>
<tr>
 <td style="padding:0 50px 70px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/heading3.jpg" alt=""/></td>
</tr>
<tr>
 <td style="padding:0 112px 40px">
   <span style="display:block; float:left;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/sol-img.jpg" alt=""/></span>
   <span style="margin: 0;float: left;padding: 0 30px;color: #4a4a4b;border-top:solid 1px #c52428;border-right: solid 1px #c52428;border-bottom: solid 1px #c52428;height: 38px;line-height: 38px;font-weight: 700;">SOLAR PRODUCTION <strong style="font-weight:500;font-size: 16px;">(AVG MONTHLY)</strong></span>
 </td>
</tr>
<tr>
  <td>
    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td style="padding: 0 50px 0px;">
          <div id="avg_kw_per_day_chart"></div>

        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
 <td style="padding:0 50px 0px;">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
    <th width="50%" style="border:solid 1px #e19091; padding:15px 10px;">AVERAGE DAILY ENERGY PRODUCTION</th>
    <th width="50%" style="border:solid 1px #e19091; padding:15px 0;">% OF ENERGY PROVIDED BY SOLAR</th>
  </tr>
  <tr>
   <td style="height:40px;"></td>
   <td></td>
 </tr>
 <tr>
   <td style="text-align:center; color:#c52428; font-size:90px; line-height:90px; font-weight:600; border-right:solid 1px #e19091; padding:10px 0 30px;"><?php echo number_format((float) ($AverageDailyProduction), 2, '.', ''); ?><strong style="text-align:center; color:#231f20; font-size:40px;line-height:40px; display:block; font-weight:400;">KWh</strong></td>
   <td>
    <div class="chart" id="graph1" data-percent="<?php echo (int)$EnergyProvidedBySolar; ?>" data-color="#c52428">
        </div>

  </td>
</tr>
</table>
</td>
</tr>
<tr>
   <td style="height:120px;"></td>
 </tr>
<tr>
  <td style="padding:20px 50px 0; text-align:left; font-size:13px; color:#666;">
    WWW.13KUGA.COM.AU   |   <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>
  </td>
</tr>
</table>
<script>

    window.onload = function() {
 
    var el;
    var options;
    var canvas;
    var span;
    var ctx;
    var radius;

    var createCanvasVariable = function(id){  // get canvas
        el = document.getElementById(id);
    };

    var createAllVariables = function(){
        options = {
            percent:  el.getAttribute('data-percent') || 25,
            size: el.getAttribute('data-size') || 165,
            lineWidth: el.getAttribute('data-line') || 15,
            rotate: el.getAttribute('data-rotate') || 0,
            color: el.getAttribute('data-color')
        };

        canvas = document.createElement('canvas');
        span = document.createElement('span');
        span.className = 'span_canvas';
        span.textContent = options.percent + '%';

        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        ctx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        el.appendChild(span);
        el.appendChild(canvas);

        ctx.translate(options.size / 2, options.size / 2); // change center
        ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        radius = (options.size - options.lineWidth) / 2;
    };


    var drawCircle = function(color, lineWidth, percent) {
        percent = Math.min(Math.max(0, percent || 1), 1);
        ctx.beginPath();
        ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
        ctx.strokeStyle = color;
        ctx.lineCap = 'square'; // butt, round or square
        ctx.lineWidth = lineWidth;
        ctx.stroke();
    };

    var drawNewGraph = function(id){
        el = document.getElementById(id);
        createAllVariables();
        drawCircle('#efefef', options.lineWidth, 100 / 100);
        drawCircle(options.color, options.lineWidth, options.percent / 100);


    };
    drawNewGraph('graph1');

};

</script>

<script type='text/javascript' async>
  google.load('visualization', '1', {packages: ['corechart']});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'kWh', 'Average'],
      ['JAN', <?php echo $averageDailyKWhPerMonth1; ?>,<?php echo $AverageDailyProduction; ?>],
      ['FEB', <?php echo $averageDailyKWhPerMonth2; ?>,<?php echo $AverageDailyProduction; ?>],
      ['MAR', <?php echo $averageDailyKWhPerMonth3; ?>,<?php echo $AverageDailyProduction; ?>],
      ['APR', <?php echo $averageDailyKWhPerMonth4; ?>,<?php echo $AverageDailyProduction; ?>],
      ['MAY', <?php echo $averageDailyKWhPerMonth5; ?>,<?php echo $AverageDailyProduction; ?>],
      ['JUN', <?php echo $averageDailyKWhPerMonth6; ?>,<?php echo $AverageDailyProduction; ?>],
      ['JUL', <?php echo $averageDailyKWhPerMonth7; ?>,<?php echo $AverageDailyProduction; ?>],
      ['AUG', <?php echo $averageDailyKWhPerMonth8; ?>,<?php echo $AverageDailyProduction; ?>],
      ['SEP', <?php echo $averageDailyKWhPerMonth9; ?>,<?php echo $AverageDailyProduction; ?>],
      ['OCT', <?php echo $averageDailyKWhPerMonth10; ?>,<?php echo $AverageDailyProduction; ?>],
      ['NOV', <?php echo $averageDailyKWhPerMonth11; ?>,<?php echo $AverageDailyProduction; ?>],
      ['DEC', <?php echo $averageDailyKWhPerMonth12; ?>,<?php echo $AverageDailyProduction; ?>],
      ]);
    var options = {
      curveType: 'function',
      titleTextStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: false,
        fontStyle: 'normal', 
        fontSize: 12
      },
      legend: {position: 'bottom',
      textStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: false,
        fontSize: 14,
        fontStyle: 'normal'
      }
    },
    hAxis: {
      format: '#\'$\'',
      titleTextStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: false,
        fontSize: 11,
        fontStyle: 'normal'
      },
      textStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: false,
        fontSize: 11,
        fontStyle: 'normal'
      }
    },
    vAxis: {
      format: '#\'kWh\'',
      title:"AVERAGE kWH PER DAY",
      titleTextStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: true,
        fontSize: 13,
        fontStyle: 'normal'
      },
      textStyle: {
        fontName: 'Arial, sans-serif;',
        italic: false,
        bold: false,
        fontSize: 11,
        fontStyle: 'normal'
      }
    },
    width: 810,
    height: 400,
    legend: {position: 'none', textStyle: {fontSize: 4, }},
    seriesType: 'bars',
    series: {
      0: {color: '#c52428'},
      1: {
        type: 'line', color: '#000',
        visibleInLegend: false
      }
    }
  };
  var chart = new google.visualization.ComboChart(document.getElementById('avg_kw_per_day_chart'));
  chart.draw(data, options);
}
</script>
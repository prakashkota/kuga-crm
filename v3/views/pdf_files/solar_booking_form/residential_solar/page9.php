<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/payment.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <?php if ($type_id == 4) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_30)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">Deposit Payment</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_0; ?></strong></td>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">Upon Installation Completion</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_50_before; ?></strong></td>
                                </tr>                                
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>

                     <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_finance)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 2 - Finance</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">Monthly Repayments </td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px">inc GST:</td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_finance_monthly_repayments; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">Term (years)</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_finance_terms; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>

                <?php } ?>
                <tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
					
					Signing the order form is evidence of your intention to be bound by this agreement. Such signing include signing by the owner, operator,authorised person, landlord or tenant of company or businesses and you warrant that any such signature is binding on you. </br></br>
					
                    Without prejudice to any other rights or remedy, we may terminate this agreement with your if you are in breach of any of these terms and conditions, and you may terminate if we are in material breach of these terms and conditions. </br></br>

                    'This agreement' or 'the agreement' means and includes these provisions, and any and all documents referenced in it and attached to it and/or provided to you in relation to it, including most recent quote, and technical documents.
					
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
                    <td valign="top">
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" >
                            <tr>
            
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                       
                                        <tr>
                                            <td height="23" style="font-family:Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised Person :</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                                        <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                            <strong><?php echo $booking_form->authorised_on_behalf->name; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
            								
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                                        <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                            <strong><?php echo $booking_form->authorised_on_behalf->date; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Client Signature:</td>
                                                        <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                            <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/'. $booking_form->authorised_on_behalf->signature; ?>" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
            
                                    </table> 
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        
                                        <tr>
                                            <td height="20" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="116" style="font-family:Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of Kuga Electrical:</td>
                                                        
                                                    </tr>
                                                </table></td>
                                        </tr>
            							<tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                                        <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                            <strong><?php echo $booking_form->authorised_by_behalf->name; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                                        <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                            <strong><?php echo $booking_form->authorised_by_behalf->date; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Sales <br />
                                                            Consultant Signature:</td>
                                                        <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                            <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/'. $booking_form->authorised_by_behalf->sales_rep_signature; ?>" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table></td>
                </tr>
	
            </table>
        </td>
    </tr>
    <?php if ($type_id == 4) { ?>
    <tr>
        <td height="275">&nbsp;</td>
    </tr>
    <?php }  ?>
	
				
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />9</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
"use strict";

var solarMap = function (options) {
    var options = typeof options == 'undefined' ? {} : options;
    var self = this;
    this.map_obj = null;
    this.current_location_marker = null;
    this.current_location_coordinates = null;
    this.shift = false;
    this.solarPanels = [];
    this.selectedSolarPanels = [];
    this.defaultSolarPanelColor = '#384950';
    this.activeSolarPanelColor = '#209bed';
    this.strokeColor = '#e2ebe8';
    this.fillOpacity = .95;
    this.strokeWeight = 1.5;
    this.defaultRotation = 0;
    this.panelLength = 1.651;
    this.panelWidth = 0.9906;
    this.nearMapApiKey = 'ZmRhYzE3MjMtNTMzMy00ZmI0LThmYTctZGIxZGNmN2YxZDdi';
    this.nearMapOn = true;
    /*user defined  values*/
    this.map_element_id = typeof options.map_element_id === 'undefined' ? 'map' : options.map_element_id;
    this.search_box_element_id = typeof options.search_box_element_id === 'undefined' ? 'pac-input' : options.search_box_element_id;
    // must be initialized with blank json 
    this.saved_data = '{}';
    this.map_default_options = {
        zoom: 20,
        center: {lat: -33.87382104369, lng: 151.25736213534617},
        mapTypeId: 'satellite',
        mapTypeControl: false,
        streetViewControl: false,
        rotateControl: false,
        draggableCursor: 'crosshair',
        tilt: 0
    };
    this.transformationFixForImage = [
        {'elem': '.gm-style>div:first>div:first>div:last>div', 'transform': null},
        {'elem': '.gm-style>div:first>div:first>div:first>div:first>div', 'transform': null},
        {'elem': '.gm-style>div:first>div:first>div:nth-child(2)>div:first>div', 'transform': null},
    ];

    document.addEventListener('keydown', function (event) {
        self.shift = event.shiftKey;
    });
    document.addEventListener('keyup', function (event) {
        self.shift = event.shiftKey;
    });

};

/** initializeMap
 *  Load Map with default coorinate and configuration
 * */
solarMap.prototype.initializeMap = function () {
    console.log('Initializing map')
    var self = this;
    self.loader('show', 'Loading map');
    var data = JSON.parse(self.saved_data);
    if (data != null && data.hasOwnProperty('center')) {
        self.map_default_options['center'] = {lat: data.center.lat, lng: data.center.lng};
    }
    self.map_obj = new google.maps.Map(document.getElementById(self.map_element_id), self.map_default_options);
    if (self.nearMapOn) {
        self.nearMap();
    }
    //Set Zoom Level
    self.map_obj.setZoom(self.map_default_options['zoom']);
    //Add Marker
    self.addMarker(self.map_default_options['center'].lat,self.map_default_options['center'].lng);
    //Enable Sidebar
    self.enableSideBar();
    // bind click event to plot solar panel on map 
    google.maps.event.addListener(this.map_obj, 'click', function (event) {
        self.plotSolarPanel(event.latLng.lat(), event.latLng.lng());
    });
    google.maps.event.addListenerOnce(this.map_obj, 'tilesloaded', function () {
        self.loader('hide', 'Loading complete');
        self.resumeEditing();
    });
};
solarMap.prototype.resumeEditing = function () {
    var self = this;
    var data = JSON.parse(self.saved_data);
    if (data != null && data.hasOwnProperty('center')) {
        var center = data.center;
        self.map_obj.setCenter(new google.maps.LatLng(center.lat, center.lng));
        self.map_obj.setZoom(self.map_default_options['zoom']);

        document.getElementById('rotation').value = data.global_rotation;
        document.getElementById('tilt').value = data.tilt;
        for (var i = 0; i < data.tiles.length; i++) {
            var plotLocation = data.tiles[i]['position'];
            var x = new google.maps.LatLng(plotLocation.lat, plotLocation.lng)
            var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth, data.tiles[i]);
            var input = data.tiles[i];
            delete input['position'];
            input['map'] = self.map_obj;
            input['bounds'] = a;
            var rectangle = new google.maps.Rectangle(input);
            var rectPoly = self.createPolygonFromRectangle(rectangle);
            this.solarPanels.push(rectPoly);
            this.rotatePanel(rectPoly, input.sp_rotation, input.sp_rotation);
            google.maps.event.addListener(rectPoly, 'click', function (event) {
                self.resetSolarPanel(this, event);
            });
        }
    }
};
solarMap.prototype.enableSideBar = function () {
    console.log('Enabling side bar tools')
    var self = this;
    var input = document.getElementById('tool-bar');
    self.map_obj.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var tools = document.getElementsByClassName('tl');
    var tools_content = document.getElementsByClassName('tl-content');
    for (var t = 0; t < tools_content.length; t++) {
        tools_content[t].style.display = 'none';
    }
    for (var k in tools) {
        if (typeof tools[k].classList != 'undefined' && tools[k].classList.contains('active')) {
            var temp = document.getElementsByClassName(tools[k].getAttribute('data-tile') + '-content');
            for (var x = 0; x < temp.length; x++) {
                temp[x].style.display = 'block';
            }
        }
    }

    // bind actions 

    document.getElementById('rotation').addEventListener('change', function (e) {
        self.defaultRotation = this.value;
    });
    document.getElementById('individual_rotation').addEventListener('change', function (e) {
        self.performAction(this.value, 'rotate');
    });
    document.getElementById('delete-panel').addEventListener('click', function (e) {
        self.performAction(null, 'remove');
    });
    document.getElementById('tl-crt-img').addEventListener('click', function (e) {
        self.mapToImage();
    });
    document.getElementById('tl-save').addEventListener('click', function (e) {
        self.exportMap();
    });
    document.getElementById('add-panel').addEventListener('click', function (e) {
        var number_of_panels = document.getElementById('no_of_panels').value;
        self.performAction(number_of_panels, 'add');
    });
    document.getElementById('tilt').addEventListener('change', function (e) {
        self.performAction(this.value, 'tilt');
    });
    document.getElementById('select-toggle').addEventListener('click', function (e) {
        var action = this.getAttribute('data-selected')
        if (action == null || action == 'none') {
            this.innerHTML = 'DESELECT ALL';
            this.setAttribute('data-selected', 'all');
            self.selectAll('all');
        } else if (this.getAttribute('data-selected') == 'all') {
            this.innerHTML = 'SELECT ALL';
            this.setAttribute('data-selected', 'none');
            self.selectAll('none');
        }
    });
    for (var i = 0; i < document.getElementsByClassName('orientation').length; i++) {
        document.getElementsByClassName('orientation')[i].addEventListener('click', function (e) {
            self.panelOrientation(this.value);
        });
    }
    for (var i = 0; i < document.getElementsByClassName('tl').length; i++) {
        document.getElementsByClassName('tl')[i].addEventListener('click', function (e) {
            self.toggleTools(this);
        });
    }
};
solarMap.prototype.toggleTools = function (ele) {
    var tools = document.getElementsByClassName('tl');
    for (var t = 0; t < tools.length; t++) {
        tools[t].classList.remove('active');
    }
    var tools_content = document.getElementsByClassName('tl-content');
    for (var t = 0; t < tools_content.length; t++) {
        tools_content[t].style.display = 'none';
    }
    var temp = document.getElementsByClassName(ele.getAttribute('data-tile') + '-content');
    ele.classList.add('active');
    for (var x = 0; x < temp.length; x++) {
        temp[x].style.display = 'block';
    }
};
/** enableSearchBox
 *  Used to convert input text box into address search handler
 * */
solarMap.prototype.enableSearchBox = function () {
    console.log('enabling search box')
    var self = this;
    var searchBox = new google.maps.places.SearchBox(document.getElementById(self.search_box_element_id));
    self.map_obj.addListener('bounds_changed', function () {
        var bounds = self.map_obj.getBounds();
        searchBox.setBounds(bounds);
    });
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return null;
        }
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            //Add Marker
            self.addMarker(place.geometry.location.lat(),place.geometry.location.lng());
        });
        self.map_obj.fitBounds(bounds);
    });
};

/** addMarker
 *  Used to add marker for the address to be identified on map 
 * */
solarMap.prototype.addMarker = function (lat, lng) {
    var self = this;
    
    //Remove last marker if its a marker object
    if(self.current_location_marker != null){
        self.current_location_marker.setMap(null);
    }
    //Add marker to map
    self.current_location_marker = new google.maps.Marker({
        position : {lat: lat, lng: lng},
        map: self.map_obj,
        title: 'Current Location!'
    });
    self.current_location_coordinates = {lat: lat, lng: lng};
};

/** plotSolarPanel
 *  Used to place solar plate no map by simply clicking on map 
 * */
solarMap.prototype.plotSolarPanel = function (lat, lng) {
    console.log('plotSolarPanel')
    var self = this;
    var x = new google.maps.LatLng(lat, lng)
    var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth);
    var tilt = document.getElementById('tilt').value || 0;
    var rectangle = new google.maps.Rectangle({
        sp_id: self.randomId(),
        sp_selected: true,
        strokeColor: self.strokeColor,
        strokeOpacity: 1,
        strokeWeight: self.strokeWeight,
        fillColor: self.activeSolarPanelColor,
        fillOpacity: self.fillOpacity,
        map: self.map_obj,
        draggable: true,
        bounds: a,
        sp_rotation: self.defaultRotation,
        sp_tilt: tilt
    });
    document.getElementById('individual_rotation').value = rectangle.sp_rotation;
    for (var key in this.solarPanels) {
        this.solarPanels[key].sp_selected = false;
        this.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
    }

    var rectPoly = self.createPolygonFromRectangle(rectangle);
    this.rotatePanel(rectPoly, self.defaultRotation, self.defaultRotation);
    this.solarPanels.push(rectPoly);
    google.maps.event.addListener(rectPoly, 'click', function (event) {
        self.resetSolarPanel(this, event);
    });
};
/** plotSolarPanel
 *  Used to place solar plate no map by simply clicking on map 
 * */
solarMap.prototype.addSolarPanel = function (no_of_panels) {
    console.log('addSolarPanel')
    var self = this;
    for (var i = 0; i < no_of_panels; i++) {
        if (self.solarPanels.length) {
            var lastRectangle = self.solarPanels[self.solarPanels.length - 1];
            var plotLocation = {'lat': lastRectangle.getPath().getAt(0).lat(), 'lng': lastRectangle.getPath().getAt(0).lng()}
        } else {
            var plotLocation = self.map_default_options.center;
        }
        var x = new google.maps.LatLng(plotLocation.lat, plotLocation.lng)
        var a = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth);
        var rectangle = new google.maps.Rectangle({
            sp_id: self.randomId(),
            sp_selected: true,
            sp_orientation: 'portrait',
            strokeColor: self.strokeColor,
            strokeOpacity: 1,
            strokeWeight: self.strokeWeight,
            fillColor: self.defaultSolarPanelColor,
            fillOpacity: self.fillOpacity,
            map: self.map_obj,
            draggable: true,
            bounds: a,
            sp_rotation: self.defaultRotation,
            sp_tilt: document.getElementById('tilt').value
        });
        document.getElementById('individual_rotation').value = rectangle.sp_rotation;
        for (var key in this.solarPanels) {
            this.solarPanels[key].sp_selected = false;
            this.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
        }

        var rectPoly = self.createPolygonFromRectangle(rectangle);
        this.rotatePanel(rectPoly, self.defaultRotation, self.defaultRotation);
        this.solarPanels.push(rectPoly);
        google.maps.event.addListener(rectPoly, 'click', function (event) {
            self.resetSolarPanel(this, event);
        });
    }
};
solarMap.prototype.updateTileProperties = function (id, prop, value) {
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_id === id) {
            var temp = {};
            temp[prop] = value;
            self.solarPanels[key].setOptions(temp);
            break;
        }
    }

};
/** resetSolarPanel
 *  Used to unselect sola 
 * */
solarMap.prototype.resetSolarPanel = function (ele, event) {
    console.log('resetSolarPanel')
    var self = this;
    document.querySelector('[data-tile="tl-2"]').click();
    for (var key in self.solarPanels) {
        if (self.shift) {
            if (ele.sp_id === self.solarPanels[key].sp_id && self.shift) {
                if (self.solarPanels[key].sp_selected) {
                    self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                } else {
                    self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                }
                self.solarPanels[key].sp_selected = ele.sp_selected ? false : true;
            }
            document.getElementById('individual_rotation').value = self.solarPanels[key].sp_rotation;
        } else {
            if (ele.sp_id === self.solarPanels[key].sp_id) {
                if (self.solarPanels[key].sp_selected) {
                    self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
                } else {
                    self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
                }
                self.solarPanels[key].sp_selected = ele.sp_selected ? false : true;
                console.log(self.solarPanels[key].sp_rotation)
                document.getElementById('individual_rotation').value = self.solarPanels[key].sp_rotation;
            } else {
                self.solarPanels[key].sp_selected = false;
                self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
            }
        }
    }
};
solarMap.prototype.selectAll = function (action) {
    var self = this;
    for (var key in self.solarPanels) {
        if (action == 'all') {
            self.solarPanels[key].setOptions({'fillColor': self.activeSolarPanelColor});
            self.solarPanels[key].sp_selected = true;
        } else {
            self.solarPanels[key].setOptions({'fillColor': self.defaultSolarPanelColor});
            self.solarPanels[key].sp_selected = false;
        }
    }
};


/** removeSolarPanel
 *  Used to unselect sola 
 * */
solarMap.prototype.removeSolarPanel = function (ele) {
    var self = this;
    var deletedIndex = [];

    for (var k = 0; k < self.solarPanels.length; k++) {
        if (k < 0) {
            break;
        }
        if (self.solarPanels[k].sp_selected) {
            self.solarPanels[k].setMap(null);
            self.solarPanels.splice(k, 1);
            k--;
        }
    }
};

solarMap.prototype.performAction = function (amount, action) {
    var self = this;
    switch (action) {
        case 'add':
            self.addSolarPanel(amount);
            break;
        case 'remove':
            self.removeSolarPanel(self.solarPanels[key]);
            break;
        case 'tilt':
            self.tiltPanel(amount);
            break;
        case 'rotate':
            for (var key in self.solarPanels) {
                if (self.solarPanels[key].sp_selected) {


                    var netRotation = amount - self.solarPanels[key].sp_rotation;
                    var origin = {'lat': self.solarPanels[key].getPath().getAt(0).lat(), 'lng': self.solarPanels[key].getPath().getAt(0).lng()}

//                        self.solarPanels[key].rotate(netRotation, origin);
                    self.rotatePanel(self.solarPanels[key], netRotation, amount);
                }
            }
            break;
    }
};

/** getPanelDimensionByLatLong
 *  Get solar panel 4 coordinate by 1 coordinate and dimension input 
 * */
solarMap.prototype.getPanelDimensionByLatLong = function (coordinate, metersEast, metersSouth, tile) {
    console.log('getPanelDimensionByLatLong')
    var indvidual_tilt_in_degree = typeof tile == 'undefined' ? document.getElementById('tilt').value : (tile.sp_tilt || document.getElementById('tilt').value);
    indvidual_tilt_in_degree = indvidual_tilt_in_degree * (Math.PI / 180);
    metersEast = metersEast * Math.cos(indvidual_tilt_in_degree);
    var ne = google.maps.geometry.spherical.computeOffset(coordinate, metersEast, 90);
    var sw = google.maps.geometry.spherical.computeOffset(coordinate, metersSouth, 180);
    return new google.maps.LatLngBounds(sw, ne);
};

solarMap.prototype.rotatePanel = function (polygon, angle, actualRotation) {
    console.log('rotatePanel')
    if (typeof actualRotation != 'undefined' && (actualRotation >= 360 || actualRotation <= -360)) {
        if (actualRotation > 0) {
            actualRotation = actualRotation - 360;
        } else {
            actualRotation = parseInt(actualRotation) + parseInt(360);
        }
    }
    var self = this;
    actualRotation = actualRotation || 0;

    var map = polygon.getMap();
    var prj = map.getProjection();
    var origin = prj.fromLatLngToPoint(polygon.getPath().getAt(0)); //rotate around first point

    var coords = polygon.getPath().getArray().map(function (latLng) {
        var point = prj.fromLatLngToPoint(latLng);
        var rotatedLatLng = prj.fromPointToLatLng(self.rotatePoint(point, origin, angle));
        return {lat: rotatedLatLng.lat(), lng: rotatedLatLng.lng()};
    });
    polygon.setPath(coords);
    polygon.setOptions({'sp_rotation': actualRotation});
    self.updateTileProperties(polygon.sp_id, 'sp_rotation', actualRotation);
    document.getElementById('individual_rotation').value = actualRotation;
};

solarMap.prototype.createPolygonFromRectangle = function (rectangle) {
    console.log('createPolygonFromRectangle')
    var map = rectangle.getMap();

    var coords = [
        {lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getNorthEast().lng()},
        {lat: rectangle.getBounds().getNorthEast().lat(), lng: rectangle.getBounds().getSouthWest().lng()},
        {lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getSouthWest().lng()},
        {lat: rectangle.getBounds().getSouthWest().lat(), lng: rectangle.getBounds().getNorthEast().lng()}
    ];

    var properties = ["sp_tilt", "sp_orientation", "strokeColor", "strokeOpacity", "strokeWeight", "fillOpacity", "fillColor", "draggable", "sp_selected", "sp_id", "sp_rotation"];
    //inherit rectangle properties 
    var options = {};
    properties.forEach(function (property) {
        if (rectangle.hasOwnProperty(property)) {
            options[property] = rectangle[property];
        }
    });

    options['path'] = coords;
    var rectPoly = new google.maps.Polygon(options);
    rectPoly.setOptions(options);
    rectangle.setMap(null);
    rectPoly.setMap(map);
//    google.maps.event.addListener(rectPoly, 'drag', function (event) {
//        //console.log(this)
//    });
    return rectPoly;
};

solarMap.prototype.panelOrientation = function (o) {
    console.log('panelOrientation')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            if (o == 'portrait') {
                // rotate 90 right
                var angle = 90;
                console.log(self.solarPanels[key].sp_rotation)
                var temp = parseInt(self.solarPanels[key].sp_rotation) + parseInt(angle);
            } else {
                // rotate 90 left
                var angle = -90;
                var temp = parseInt(self.solarPanels[key].sp_rotation) + parseInt(angle);
            }
            self.rotatePanel(self.solarPanels[key], angle, temp);
        }
    }
};
solarMap.prototype.tiltPanel = function (degree) {
    console.log('tiltPanel')
    var self = this;
    for (var key in self.solarPanels) {
        if (self.solarPanels[key].sp_selected) {
            var map = self.solarPanels[key].getMap();
            var prj = map.getProjection();
            var origin = prj.fromLatLngToPoint(self.solarPanels[key].getPath().getAt(0)); //rotate around first point
            var origin = self.solarPanels[key].getPath().getAt(0);


            var x = new google.maps.LatLng(origin.lat(), origin.lng())
            var coords = self.getPanelDimensionByLatLong(x, self.panelLength, self.panelWidth);
//            var coords = self.solarPanels[key].getPath().getArray().map(function (latLng) {
//                var point = prj.fromLatLngToPoint(latLng);
//                var rotatedLatLng = prj.fromPointToLatLng();
//                return {lat: rotatedLatLng.lat(), lng: rotatedLatLng.lng()};
//            });

            self.solarPanels[key].setPath(coords);
        }
    }
};
solarMap.prototype.rotatePoint = function (point, origin, angle) {
    var angleRad = angle * Math.PI / 180.0;
    return {
        x: Math.cos(angleRad) * (point.x - origin.x) - Math.sin(angleRad) * (point.y - origin.y) + origin.x,
        y: Math.sin(angleRad) * (point.x - origin.x) + Math.cos(angleRad) * (point.y - origin.y) + origin.y
    };
};

solarMap.prototype.middlePoint = function (c1, c2) {
    var Bx = Math.cos(c2.lat) * Math.cos(c2.lng - c1.lng);
    var By = Math.cos(c2.lat) * Math.sin(c2.lng - c1.lng);
    var latMid = Math.atan(Math.sin(c1.lat) + Math.sin(c2.lat), Math.sqrt((Math.cos(c1.lat) + Bx) * (Math.cos(c1.lat) + Bx) + By * By));
    var longMid = c1.lng + Math.atan(By, Math.cos(c1.lat) + Bx);
    return new google.maps.LatLng(latMid, longMid);
};

solarMap.prototype.exportMap = function () {
    console.log('exportMap')
    var self = this;
    var exported = [];
    var properties = ["sp_tilt", "sp_orientation", "strokeColor", "strokeOpacity", "strokeWeight", "fillOpacity", "fillColor", "draggable", "sp_selected", "sp_id", "sp_rotation"];
    var center = self.map_obj.getCenter();
    var main = {center: {lat: center.lat(), lng: center.lng()}, global_rotation: document.getElementById('rotation').value, tilt: document.getElementById('tilt').value};
    for (var i = 0; i < self.solarPanels.length; i++) {
        var options = {};
        options['position'] = {lat: self.solarPanels[i].getPath().getAt(0).lat(), lng: self.solarPanels[i].getPath().getAt(2).lng()};
        properties.forEach(function (property) {
            if (self.solarPanels[i].hasOwnProperty(property)) {
                options[property] = self.solarPanels[i][property];
            }
        });
        options['sp_selected'] = false;
        options['fillColor'] = self.defaultSolarPanelColor;
        exported.push(options);
    }
    if (exported.length) {
        main['center'] = exported[0]['position'];
    }
    main['tiles'] = exported;
    var exportedData = JSON.stringify(main);
    $('#near-map-exported-data').val(exportedData);
};
solarMap.prototype.mapToImage = function () {
    var self = this;
//    self.loader('show', 'Processing map to generate image');
    function transform(div) {
        if (div != undefined) {
            var transform = $(div).css("transform");
            var comp = transform.split(",");
            var mapleft = parseFloat(comp[4]);
            var maptop = parseFloat(comp[5]);
            $(div).css({
                "transform": "none",
                "left": mapleft,
                "top": maptop,
            })
            return transform;
        } else {
            return null;
        }
    }
    function transformBack(div, transform) {
        if (div != undefined) {
            $(div).css({
                left: 0,
                top: 0,
                "transform": transform
            });
        }
    }
    for (var k = 0; k < self.transformationFixForImage.length; k++) {
        self.transformationFixForImage[k].transform = transform($(self.transformationFixForImage[k].elem)[0]);
    }
    self.current_location_marker.setMap(null);
    $(".gm-style-pbc").hide();
    $('#tool-bar').css({"opacity": "0"});
    $('#tool-bar').hide();
    $('.gm-bundled-control').hide();
    $('.gm-bundled-control').css({"opacity": "0"});
    $('.gm-fullscreen-control').hide();
    $('.gm-fullscreen-control').css({"opacity": "0"});
    var width = document.getElementById(self.map_element_id).offsetWidth;
    var height = document.getElementById(self.map_element_id).offsetHeight;
    $('#proposal_image > .add-picture').removeAttr('style');

    html2canvas(document.getElementById(self.map_element_id), {
        useCORS: true,
        width: width,
        height: height,
        onrendered: function (canvas) {
            var dataUrl = canvas.toDataURL("image/png");
            $('#image').val(dataUrl);
            $('#proposal_no_image').attr('style', 'display:none !important;');
            $('#proposal_image').attr('style', 'display:block !important;');
//            var dataUrl = 'https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg';
            $('#proposal_image > .add-picture').css('background-image', 'url("' + dataUrl + '")');
            $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
            $('#proposal_image > .add-picture').css('background-position', '0px 0px');
            $('#proposal_image > .add-picture').css('background-size', '108% 103%');
            $('#tab_image_upload_btn').trigger('click');
            self.exportMap();
//            self.loader('hide', 'Image processing is complete');
            $('#tool-bar').show();
            $('#tool-bar').css({"opacity": "1"});
            $('.gm-bundled-control').show();
            $('.gm-fullscreen-control').show();
            $(".gm-style-pbc").show();
            for (var k = 0; k < self.transformationFixForImage.length; k++) {
                transformBack($(self.transformationFixForImage[k].elem)[0], self.transformationFixForImage[k].transform);
            }
            self.addMarker(self.current_location_coordinates.lat,self.current_location_coordinates.lng);
        }
    });
};

/** extend
 *  Object merger
 * */
solarMap.prototype.extend = function (a, b) {
    for (var key in b)
        if (b.hasOwnProperty(key))
            a[key] = b[key];
    return a;
};

/** randomId
 *  Used to get unique random string 
 * */
solarMap.prototype.randomId = function () {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
};

solarMap.prototype.nearMap = function () {
    console.log('nearMap');
    var self = this;
    var apikey = this.nearMapApiKey;

    function degreesToRadians(deg) {
        return deg * (Math.PI / 180);
    }
    function radiansToDegrees(rad) {
        return rad / (Math.PI / 180);
    }
    function regionForCoordinate(x, y, zoom) {
        var x_z1 = x / Math.pow(2, (zoom - 1));
        if (x_z1 < 1) {
            return 'us';
        } else {
            return 'au';
        }
    }
    function rotateTile(coord, zoom, heading) {
        var numTiles = 1 << zoom;
        var x, y;
        switch (heading) {
            case 0:
                x = coord.x;
                y = coord.y;
                break;
            case 90:
                x = numTiles - (coord.y + 1);
                y = coord.x;
                break;
            case 180:
                x = numTiles - (coord.x + 1);
                y = numTiles - (coord.y + 1);
                break;
            case 270:
                x = coord.y;
                y = numTiles - (coord.x + 1);
                break
        }
        return new google.maps.Point(x, y);
    }

    function MercatorProjection(worldWidth, worldHeight) {
        this.pixelOrigin = new google.maps.Point(worldWidth / 2, worldHeight / 2);
        this.pixelsPerLonDegree = worldWidth / 360;
        this.pixelsPerLatRadian = worldHeight / (2 * Math.PI);
    }
    MercatorProjection.prototype.fromLatLngToPoint = function (latlng, opt_point) {
        var point = opt_point || new google.maps.Point(0, 0);
        var origin = this.pixelOrigin;
        var lat = latlng.lat();
        var lng = latlng.lng();
        point.x = origin.x + lng * this.pixelsPerLonDegree;
        var siny = Math.sin(degreesToRadians(lat));
        point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -this.pixelsPerLatRadian;
        return point;
    };
    MercatorProjection.prototype.fromPointToLatLng = function (point, noWrap) {
        var origin = this.pixelOrigin;
        var lng = (point.x - origin.x) / this.pixelsPerLonDegree;
        var latRadians = (point.y - origin.y) / -this.pixelsPerLatRadian;
        var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
        return new google.maps.LatLng(lat, lng);
    };
    const projVertical = new MercatorProjection(256, 256);

    function vertToWest(ll) {
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        ll = projVertical.fromPointToLatLng(pt);
        return new google.maps.LatLng(ll.lat(), -ll.lng());
    }

    function westToVert(ll) {
        ll = new google.maps.LatLng(ll.lat(), -ll.lng());
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        return projVertical.fromPointToLatLng(pt);
    }

    function vertToEast(ll) {
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        ll = projVertical.fromPointToLatLng(pt);
        return new google.maps.LatLng(-ll.lat(), ll.lng());
    }

    function eastToVert(ll) {
        ll = new google.maps.LatLng(-ll.lat(), ll.lng());
        var pt = projVertical.fromLatLngToPoint(ll);
        pt = new google.maps.Point(pt.y, pt.x);
        return projVertical.fromPointToLatLng(pt);
    }

    function fakeLatLng(mapTypeId, ll) {
        if (mapTypeId === 'W') {
            ll = vertToWest(ll);
        } else if (mapTypeId === 'E') {
            ll = vertToEast(ll);
        } else if (mapTypeId === 'S') {
            ll = new google.maps.LatLng(-ll.lat(), -ll.lng());
        }
        return ll;
    }

    function realLatLng(mapTypeId, ll) {
        if (mapTypeId === 'W') {
            ll = westToVert(ll);
        } else if (mapTypeId === 'E') {
            ll = eastToVert(ll);
        } else if (mapTypeId === 'S') {
            ll = new google.maps.LatLng(-ll.lat(), -ll.lng());
        }
        return ll;
    }

    function registerProjectionWorkaround(map) {
        var currMapType = map.getMapTypeId();
        map.addListener('maptypeid_changed', function () {
            var center = realLatLng(currMapType, map.getCenter());
            currMapType = map.getMapTypeId();
            map.setCenter(fakeLatLng(currMapType, center));
        })
    }

    function createMapType(tileWidth, tileHeight, heading, name) {
        var maptype = new google.maps.ImageMapType({
            name: name,
            tileSize: new google.maps.Size(tileWidth, tileHeight),
            isPng: !0,
            minZoom: 1,
            maxZoom: 24,
            getTileUrl: function (coord, zoom) {
                coord = rotateTile(coord, zoom, heading);
                var x = coord.x;
                var y = coord.y;
                var url = 'http://' + regionForCoordinate(x, y, zoom) + '0.nearmap.com/maps/hl=en' + '&x=' + x + '&y=' + y + '&z=' + zoom + '&nml=' + name + '&httpauth=false&version=2' + '&apikey=' + apikey;
                return url
            }
        });
        maptype.projection = new MercatorProjection(tileWidth, tileHeight);
        return maptype;
    }

    function createBookmarkControl(map) {
        var bookmarkControlDiv = document.createElement('div');
        var bookmarkControl = new BookmarkControl(bookmarkControlDiv, map);
        bookmarkControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(bookmarkControlDiv);
    }

    function BookmarkControl(controlDiv, map) {
        function createBookmarkLink(label, point) {
            var bookmarkLink = document.createElement('span');
            bookmarkLink.className = 'bookmark-link';
            bookmarkLink.innerHTML = label;
            bookmarkLink.addEventListener('click', function () {
                map.setCenter(fakeLatLng(map.getMapTypeId(), point));
            });
            return bookmarkLink;
        }
    }
    if (apikey === undefined || apikey.length === 0) {
        alert('Please provide your Nearmap API Key');
        return
    }
    var map_types = [createMapType(256, 256, 0, 'Vert'), createMapType(256, 181, 0, 'N'), createMapType(256, 181, 90, 'E'), createMapType(256, 181, 180, 'S'), createMapType(256, 181, 270, 'W'), ];
    var mapOptions = {
        zoom: 15,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'Vert', 'N', 'E', 'S', 'W'],
            streetViewControl: !1
        },
        disableDefaultUI: !0,
        center: self.map_default_options.center,
        mapTypeControl: !1,
        streetViewControl: !1,
        fullscreenControl: true,
        draggableCursor: 'crosshair',
        rotateControl: !1,
    };
    var spherical = google.maps.geometry.spherical;
    this.map_obj = new google.maps.Map(document.getElementById(self.map_element_id), mapOptions);
    for (var i = 0; i < map_types.length; i++) {
        var mt = map_types[i];
        this.map_obj.mapTypes.set(mt.name, mt);
    }
    registerProjectionWorkaround(this.map_obj);
    this.map_obj.setMapTypeId('Vert');
    createBookmarkControl(this.map_obj);
};
solarMap.prototype.loader = function (action, message) {
    document.getElementById('img-ldr-msg').innerHTML = message;
    if (action == 'show') {
        document.getElementById('img-ldr').style.display = 'block';
    } else {
        document.getElementById('img-ldr').style.display = 'none';
    }
};
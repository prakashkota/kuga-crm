<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Cron Controller
 * @version 1.0
 */
class SimproCron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('../core/input');
        $this->load->library('Components');
        $this->load->library('Simpro');
        $this->load->library("Email_Manager");
        $this->load->model("Cron_Model", "cron");
        $this->load->model("Job_Model", "job");
        date_default_timezone_set('Australia/Sydney');
    }

   public function save_simpro_jobs_to_crm($page = 1){

        log_message('error', 'Simrpo Dump Cron for page '.$page.' initiated at '. date('Y-m-d H:i:s'));
        
        //increase max execution time of this script to 150 min:
        ini_set('max_execution_time', 90000);
        //increase Allowed Memory Size of this script:
        ini_set('memory_limit','960M');

        $payload = array(
            'columns' => 'ID',
            'pageSize' => 250,
            'page' => $page,

        );
        $simpro_jobs = $this->simpro->list_jobs($payload);

        if(!empty($simpro_jobs)){
            $next_page = $page + 1;
            $this->handle_simpro_jobs($simpro_jobs,$next_page);
        }
    }

    private function handle_simpro_jobs($simpro_jobs,$next_page){
        $existing_jobs = $this->common->fetch_where('tbl_simpro_jobs','job_id',NULL);
        $existing_job_ids = array_column($existing_jobs,'job_id');
        $final_data = array();
        $count = 0;
        //$simpro_jobs[0]['ID'] = 23076; //15363;//15462;
        foreach($simpro_jobs as $jobs){
            $job_details = $this->simpro->list_job_details([],$jobs['ID']);
            //print_r($job_details);die;
            if(!empty($job_details)){
                //Get Item Details
                $final_data[$count]  = $handle_simpro_job_invoice_items = $this->handle_simpro_job_invoice_items($job_details['ID']);

                //Push Other Fields Now
                $site_details =  $this->simpro->list_site_details([],$job_details['Site']['ID']);
                $state = isset($site_details['Address']) ? $site_details['Address']['State'] : NULL;
                $final_data[$count]['address'] = isset($site_details['Address']) ? $site_details['Address']['Address'] : NULL;
                $final_data[$count]['company_name'] = isset($job_details['Customer']) ? $job_details['Customer']['CompanyName'] : NULL;
                $final_data[$count]['job_id'] = $job_details['ID'];
                $final_data[$count]['status_name'] = isset($job_details['Status']['Name']) ? $job_details['Status']['Name'] : NULL;
                $final_data[$count]['job_type'] = isset($job_details['Type']) ? $job_details['Type'] : NULL;
                //$final_data[$count]['Note Date'] = isset($job_details['DateIssued']) ? $job_details['DateIssued'] : '';
                //$final_data[$count]['Last note'] = isset($job_details['DateIssued']) ? $job_details['DateIssued'] : '';
                $final_data[$count]['salesperson_name'] = isset($job_details['Salesperson']['Name']) ? $job_details['Salesperson']['Name'] : NULL;
                $final_data[$count]['state'] = $state;
                $final_data[$count]['bf_received_date'] = isset($job_details['DateIssued']) ? $job_details['DateIssued'] : NULL;
                $final_data[$count]['stage_name'] = isset($job_details['Stage']) ? $job_details['Stage'] : NULL;

                $custom_fields = isset($job_details['CustomFields']) ? $job_details['CustomFields'] : NULL;
                $items = ['Panels','Highbays','Tubes','Floodlight','Battens','Other'];
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $key => $value) {
                        $sp_custom_field_id = $value['CustomField']['ID'];
                        $sp_custom_field_name = $value['CustomField']['Name'];
                        if (in_array($sp_custom_field_name,$items) && empty($invoice_items)) {
                            if($sp_custom_field_name == 'Battens'){
                                $sp_custom_field_name = 'battens';
                            }else if($sp_custom_field_name == 'Highbays'){
                                $sp_custom_field_name = 'highbays';
                            }else if($sp_custom_field_name == 'Tubes'){
                                $sp_custom_field_name = 'tubes';
                            }else if($sp_custom_field_name == 'Floodlight'){
                                $sp_custom_field_name = 'flood_lights';
                            }else if($sp_custom_field_name == 'Panels'){
                                $sp_custom_field_name = 'solar_panels';
                            }else if($sp_custom_field_name == 'Other'){
                                $sp_custom_field_name = 'panel_lights';
                            }
                            $final_data[$count][$sp_custom_field_name] = $value['Value'];
                        }
                        if($sp_custom_field_name == 'Certificates'){
                            $final_data[$count]['estimated_certificates'] = $value['Value'];
                        }
                        if($sp_custom_field_name == 'Solar - kW'){
                            $final_data[$count]['killo_watt'] = $value['Value'];
                        }
                        if($sp_custom_field_name == 'JOB ID (DATAFORCE)'){
                            $final_data[$count]['dataforce_job_id'] = $value['Value'];
                        }
                    }
                }
                
                if(in_array($final_data[$count]['job_id'],$existing_job_ids)){
                    $final_data[$count]['updated_at'] = date('Y-m-d H:i:s');
                    $this->common->update_data('tbl_simpro_jobs',array('job_id' => $final_data[$count]['job_id']),$final_data[$count]);
                }else{
                    $this->common->insert_data('tbl_simpro_jobs',$final_data[$count]);
                }
            }
        }
        //Call Next Batch of 250 Jobs
        $this->save_simpro_jobs_to_crm($next_page);
    }

    private function handle_simpro_job_invoice_items($job_id){
        $items = [];
        $invoices = $this->simpro->list_invoices([],$job_id);
        $invoice_id = NULL;
        foreach($invoices as $invoice){
            //DateIssued
            //68 - Invoices : Fully-paid
            //64 - Invoices : 1-30 days overdue
            if($invoice['Status']['ID'] == 68 || $invoice['Status']['ID'] == 64){
                $invoice_id = $invoice['ID'];
                break;
            }
        }

        if($invoice_id == NULL){
            return $items;
        }

        //Get Invoice Details
        $invoice_details = $this->simpro->list_invoice_details([],$job_id,$invoice_id);
        //print_r($invoice_details);die;
        $items['install_complete_date'] = (isset($invoice_details['Period'])) ? $invoice_details['Period']['EndDate'] : NULL;
        if(!empty($invoice_details)){
            if(isset($invoice_details['CostCenters'])){
                foreach($invoice_details['CostCenters'] as $cost_center){
                    if(isset($cost_center['Items']['Catalogs'])){
                        foreach($cost_center['Items']['Catalogs'] as $key => $value){
                            //echo "<pre>";
                            //print_r($cost_center);die;
                            $catalog_id = $value['Catalog']['ID'];
                            $qty = $value['Total']['Qty'];

                            //Get Item type from Item Details
                            $item_details = $this->simpro->list_catalog_item_details([],$catalog_id);
                            if(!isset($item_details['Group']['Name'])){
                                continue;
                            }
                            $item_type = $item_details['Group']['Name'];
                            $item_name = '';
                            if($item_type == 'Battens' || $this->components->find_string('Batten',$item_type,'i')){
                                $item_name = 'battens';
                            }else if($item_type == 'Highbays' || $this->components->find_string('Highbay',$item_type,'i')){
                                $item_name = 'highbays';
                            }else if($item_type == 'Tubes' || $this->components->find_string('Tube',$item_type,'i')){
                                $item_name = 'tubes';
                            }else if($item_type == 'Floodlight' || $this->components->find_string('Flood',$item_type,'i')){
                                $item_name = 'flood_lights';
                            }else if($item_type == 'Solar Panels' || $this->components->find_string('Solar Panel',$item_type,'i')){
                                $item_name = 'solar_panels';
                            }else if($item_type == 'Panel Lights' || $this->components->find_string('Panel Light',$item_type,'i')){
                                $item_name = 'panel_lights';
                            }
                            if($item_name != ''){
                                if(isset($items[$item_name])){
                                    $items[$item_name] += $qty;
                                }else{
                                    $items[$item_name] = $qty;
                                }
                            }
                        }
                    }
                }
            }
        }
        //echo "<pre>";
        //print_r($items);die;
        return $items;
    }
    
    
    public function process_warranty_jobs(){
        $enquiry_data = $this->cron->fetch_kuga_web_where('web_warranty_enquiry','*',array('status' => 0));
        //print_r($enquiry_data);die;
        if (empty($enquiry_data)) {
                log_message('info', 'Warranty Enquiry Data is empty');
                log_message('info', '*********** Cron Ended ************');
                return false;
        }

        $enquiry_data = $enquiry_data[0];

        log_message('error', 'Warranty Enquiry Data : ');
        log_message('error', json_encode($enquiry_data));

        //Set Warranty Job Process in Progress
        log_message('error', 'Warranty Job Process in Progress');
        $this->cron->update_kuga_web_data('web_warranty_enquiry', 
            array('id' => $enquiry_data['id']),
            array('status' => 2,'updated_at' => date('Y-m-d H:i:s'))
        );

        //Create Job in Simpro and then set final status
        log_message('error', 'Start warranty job creation in simpro');
        //$stat = $this->create_simpro_warranty_job($enquiry_data);
        $stat = $this->create_job_in_job_crm($enquiry_data);
        $stat['updated_at'] = date('Y-m-d H:i:s');
        $this->cron->update_kuga_web_data('web_warranty_enquiry', 
            array('id' => $enquiry_data['id']), $stat
        );

        log_message('info', '*********** Cron Ended ************');
    }


    public function create_simpro_warranty_job($job_data) {
        $data = array();
        //print_r($job_data);die;
        $customer_payload = $site_payload = $site_contact_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();

        $simpro_customer_response = $simpro_site_response = $simpro_contact_response = $simpro_job_response = $section_response = $cost_center_response = array();

        //Fetch State and CostCenter
        $state_data = $this->common->fetch_row('tbl_state','*',array('state_name' => $job_data['state']));
        $state = 'VIC';
        if(!empty($state_data)){
            $state = $state_data['state_postal'];
        }
        $cost_center = ($state == 'VIC') ? 15 : 71;

        try {

            //SIMPRO CUSTOMER API BODY
            $customer_payload['CompanyName'] = $job_data['business_name'];
            $customer_payload['Phone'] = $job_data['number'];
            $customer_payload['AltPhone'] = NULL;
            $customer_payload['Address'] = array(
                'Address' => (isset($job_data['address']) && $job_data['address'] != '') ? $job_data['address'] : NULL,
                'City' => NULL,
                'State' => $state,
                'PostalCode' => (isset($job_data['postcode']) && $job_data['postcode'] != 0) ? $job_data['postcode'] : NULL,
                'Country' => 'AUS'
            );
            $customer_payload['CustomerType'] = 'Customer';
            $customer_payload['Email'] = NULL;
            $customer_payload['EIN'] = NULL;

            $name = explode(' ',$job_data['name']);
            $fname = (count($name) >= 1) ? $name[0] : '';
            $lname = (count($name) > 1) ? $name[1] : '';

           

            if ($job_data['simpro_cust_id'] == NULL || $job_data['simpro_cust_id'] == '') {
                //Create Customer in SIMPRO
                $simpro_customer_response = $this->simpro->create_customer($customer_payload);

                if (!isset($simpro_customer_response['error'])) {
                    $simpro_cust_id = (int) $simpro_customer_response['ID'];
                    $this->cron->update_kuga_web_data('web_warranty_enquiry', array('id' => $job_data['id']), array('simpro_cust_id' => $simpro_cust_id));

                    //simPRO Customer Site
                    $site_payload['Name'] = $job_data['address'] . ' Site';
                    $site_payload['Address'] = array(
                        'Address' => (isset($job_data['address']) && $job_data['address'] != '') ? $job_data['address'] : NULL,
                        'City' => NULL,
                        'State' => $state,
                        'PostalCode' => (isset($job_data['postcode']) && $job_data['postcode'] != 0) ? $job_data['postcode'] : NULL,
                        'Country' => 'AUS'
                    );

                    //Site Primary Contact
                    $site_payload['PrimaryContact']['GivenName'] = $fname;
                    $site_payload['PrimaryContact']['FamilyName'] = $lname;
                    $site_payload['PrimaryContact']['Email'] = NULL;
                    $site_payload['PrimaryContact']['Position'] = NULL;
                    $site_payload['PrimaryContact']['CellPhone'] = $job_data['number'];
                    $site_payload['PrimaryContact']['WorkPhone'] = NULL;

                    $simpro_site_id = (int) $job_data['simpro_site_id'];
                    if ($simpro_site_id == NULL || $simpro_site_id == '') {
                        //Create Customer Site in SIMPRO
                        $site_payload['Customers'] = array($simpro_cust_id);
                        $simpro_site_response = $this->simpro->create_customer_site($site_payload);
                        $simpro_site_id = (int) $simpro_site_response['ID'];
                        $this->cron->update_kuga_web_data('web_warranty_enquiry', array('id' => $job_data['id']), array('simpro_site_id' => $simpro_site_id));
                    } else {
                        //Update Customer Site in SIMPRO
                        $simpro_site_response = $this->simpro->update_customer_site($site_payload, $simpro_site_id);
                    }
                }
            } else {
                //Update Customer in SIMPRO
                $simpro_cust_id = (int) $job_data['simpro_cust_id'];
                $simpro_site_id = (int) $job_data['simpro_site_id'];
                $simpro_customer_response = $this->simpro->update_customer($customer_payload, $simpro_cust_id);
                if (!isset($simpro_customer_response['error'])) {
                    $site_payload['Name'] = $job_data['address'] . ' Site';
                    $site_payload['Address'] = array(
                        'Address' => (isset($job_data['address']) && $job_data['address'] != '') ? $job_data['address'] : NULL,
                        'City' => NULL,
                        'State' => $state,
                        'PostalCode' => (isset($job_data['postcode']) && $job_data['postcode'] != 0) ? $job_data['postcode'] : NULL,
                        'Country' => 'AUS'
                    );

                    //Site Primary Contact
                    $site_payload['PrimaryContact']['GivenName'] = $fname;
                    $site_payload['PrimaryContact']['FamilyName'] = $lname;
                    $site_payload['PrimaryContact']['Email'] = NULL;
                    $site_payload['PrimaryContact']['Position'] = NULL;
                    $site_payload['PrimaryContact']['CellPhone'] = $job_data['number'];
                    $site_payload['PrimaryContact']['WorkPhone'] = NULL;

                    if ($simpro_site_id == NULL || $simpro_site_id == '') {
                        //Create Customer Site in SIMPRO
                        $site_payload['Customers'] = array($simpro_cust_id);
                        $simpro_site_response = $this->simpro->create_customer_site($site_payload);
                        $simpro_site_id = (int) $simpro_site_response['ID'];
                        $this->cron->update_kuga_web_data('web_warranty_enquiry', array('id' => $job_data['id']), array('simpro_site_id' => $simpro_site_id));
                    } else {
                        //Update Customer Site in SIMPRO
                        $simpro_site_response = $this->simpro->update_customer_site($site_payload, $simpro_site_id);
                    }
                }
            }

            //Create or Update Job in Simpro
            if ($simpro_cust_id != NULL && $simpro_site_id != NULL) {
                $description = '<b>Name of Customer:</b> ' . $fname . ' ' . $lname . '<br/>';
                $description .= '<b>Description of Fault:</b> ' . $job_data['description'] . '<br/>';
                $description .= '<b>No. of lights out:</b> ' . $job_data['cfl'] . '<br/>';
                $description .= '<b>Height of ceiling:</b> ' . $job_data['hlf'] . '<br/>';
                
                $job_payload['Description'] = $description;
                $job_payload['Customer'] = (int) $simpro_cust_id;
                $job_payload['Site'] = (int) $simpro_site_id;
                $job_payload['DateIssued'] = date('Y-m-d');
                $job_payload['Stage'] = 'Pending';
                $job_payload['Tags'] = [4];
                $job_payload['Status'] = 129;

                $simpro_job_id = (int) $job_data['simpro_job_id'];
                if ($simpro_job_id == NULL) {
                    //Create JOB in Simpro
                    $job_payload['Type'] = 'Service';
                    $simpro_job_response = $this->simpro->create_job($job_payload);
                    $simpro_job_id = (int) $simpro_job_response['ID'];
                    $this->cron->update_kuga_web_data('web_warranty_enquiry', array('id' => $job_data['id']), array('simpro_job_id' => $simpro_job_id));
                } else {
                    //Update JOB in Simpro
                    $simpro_job_response = $this->simpro->update_job($job_payload, $simpro_job_id);
                }
            }else{
                $data['status'] = 3;
                $data['status_info'] = 'Looks like their was some issue in creating customer or site in simPRO. So job cant be processed.';
                return $data;
            }

            //Update Job Custom Field Data and Add Section and Cost Center
            if (!isset($simpro_job_response['error'])) {
                $job_custom_fields = $simpro_job_response['CustomFields'];
                if (!empty($job_custom_fields)) {
                    foreach ($job_custom_fields as $key => $value) {
                        $sp_custom_field_id = $value['CustomField']['ID'];
                        $sp_custom_field_name = $value['CustomField']['Name'];
                        if ($sp_custom_field_name == 'Job Type') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'Warranty');
                        }  else if ($sp_custom_field_name == 'Nature Of Ownership') {
                            $this->simpro->update_job_custom_fields($simpro_job_id, $sp_custom_field_id, 'Owner');
                        }
                    }
                }

                $simpro_section_id = (int) $job_data['simpro_section_id'];
                if ($simpro_section_id == NULL && $simpro_job_id != NULL) {
                    //Create Job Section and Cost Center
                    $section_response = $this->simpro->create_job_section([], $simpro_job_id);
                    $simpro_section_id = $section_response['ID'];
                    $this->cron->update_kuga_web_data('web_warranty_enquiry', array('id' => $job_data['id']), array('simpro_section_id' => $simpro_section_id));
                    $cost_center_payload['CostCenter'] = $cost_center;
                    $cost_center_response = $this->simpro->create_job_cost_center($cost_center_payload, $simpro_job_id, $simpro_section_id);
                }
            }

            $attachment_files = $this->cron->fetch_kuga_web_where('web_warranty_enquiry_attachments','*',array('uuid' => $job_data['uuid']));
            
            foreach($attachment_files as $key => $value){
                
                $attachment_column_db = 'simpro_attachment_id'; 
                
                if(($value['file_name'] != '' && $value['file_name'] != NULL) && $value[$attachment_column_db] == NULL){
                    $filename = '';
                    $b64_file = '';

                    $file = explode('.',$value['file_name']);
                    $filename = $value['name'] .'.' . $file[1];

                    $b64_file = base64_encode(file_get_contents('https://kugacrm.com.au/assets/uploads/warranty_claim_files/' . $value['file_name']));

                    //Create Job Attachemnt
                    $attachment_payload = [];
                    $simpro_attachment_response = [];

                    $attachment_payload['Filename'] = $filename;
                    $attachment_payload['Base64Data'] = $b64_file;
                    $attachment_payload['Public'] = FALSE;
                    $attachment_payload['Email'] = FALSE;
                    //Create JOB Attachment in Simpro
                    $simpro_attachment_response = $this->simpro->create_job_attachment($attachment_payload, $simpro_job_id);
                    if (isset($simpro_attachment_response['ID'])) {
                        $simpro_attachment_id = $simpro_attachment_response['ID'];
                        $this->cron->update_kuga_web_data('web_warranty_enquiry_attachments', array('id' => $value['id']), array($attachment_column_db => $simpro_attachment_id));
                    }
                }
            }

        }catch (Exception $e) {
            $response = $e->getResponse();
            $error = $response->getBody()->getContents();
            $error = json_decode($error, true);
            $status = $e->getMessage();
            if (!empty($error['errors'])) {
                $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
                $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
                $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
            }
            $data['status'] = 3;
            $data['status_info'] = $status;
            return $data;
        }

        if (!isset($simpro_job_response['error'])) {
            $data['status'] = 1;
            $data['status_info'] = 'Job Created Successfully';
            $this->email_manager->send_warranty_job_success_email($job_data,$attachment_files);
        } else {
            $data['status'] = 3;
            $data['status_info'] = 'Looks like something went wrong while creating the job';
        }
        return $data;
    }
    
    
    private function create_job_in_job_crm($data){
        $business_data = array();
        $business_data['entity_name'] = $data['business_name'];
          $business_data['abn'] = '';
          $business_data['trading_name'] = '';
          $business_data['leased_or_owned'] = '';
          $business_data['address'] = $data['address'];
          $business_data['suburb'] = '';
          $business_data['postcode'] = $data['postcode'];
          $business_data['bca_hours'] = '';
          $business_data['description'] = $data['description'];
          $business_data['boh'] = $data['boh'];
          $business_data['hlf'] = $data['hlf'];
          $business_data['cfl'] = $data['cfl'];
    
          $name = explode(' ',$data['name']);
          $fname = (count($name) >= 1) ? $name[0] : '';
          $lname = (count($name) > 1) ? $name[1] : '';
    
          $authorised_data = array();
          $authorised_data['first_name'] = $fname;
          $authorised_data['last_name'] = $lname;
          $authorised_data['position'] = '';
          $authorised_data['email'] = '';
          $authorised_data['contact_no'] = $data['number'];
          $authorised_data['landline'] = '';
            
            
          $cust_data = array();
          $cust_data['company_name'] = $data['business_name'];
          $cust_data['first_name'] = $fname;
          $cust_data['last_name'] = $lname;
          $cust_data['customer_contact_no'] = $data['number'];
          $crm_cust_id = $this->job->insert_or_update_warranty_customer($data['id'],$cust_data);    
    
          if($crm_cust_id){
            
            $state_data = $this->common->fetch_row('tbl_state','*',array('state_name' => $data['state']));
            
            $location_data = array();
            $location_data['address'] = $data['address'];
            $location_data['state_id'] = !empty($state_data) ? $state_data['state_id'] : 7;
            $location_data['postcode'] = $data['postcode'];
            
            $cust_loc_id = $this->job->insert_or_update_warranty_customer_location($location_data,$crm_cust_id);
            $job_data = array();
            $job_data['uuid'] = $this->components->uuid();
            $job_data['company_id'] = 1;
            $job_data['cust_id'] = $crm_cust_id;
            $job_data['booking_form_id'] = NULL;
            
            // Check if lead is from which state, 2= NSW, 4=QLD
            if($location_data['state_id'] == 2){
                $cost_center = 6; // Assign lead to NSW-Warranty cost center
            }else if($location_data['state_id'] == 4){
                $cost_center = 6; // Assign lead to QLD-Warranty cost center
            }else{
                $cost_center = 6; // Assign lead to VIC-Warranty cost center
            }
            
            $job_data['cost_centre_id'] = $cost_center;
            $job_data['assigned_to'] = isset($data['user_id']) ? $data['user_id'] : '';
            $job_data['business_details'] = json_encode($business_data);
            $job_data['authorised_details'] = json_encode($authorised_data);
            
            $job_data['job_type'] = 'LED';
            $job_data['created_at'] = date('Y-m-d H:i:s');
            $warranty_customer_agreement_details = array(
                "warranty_business_name" => $data['business_name'],
                "warranty_address" => $data['address'],
                "warranty_name" => $data['name'],
                "warranty_number" => $data['number'],
                "warranty_boh" => $data['boh'],
                "warranty_hlf" => $data['hlf'],
                "warranty_cfl" => $data['cfl'],
                "warranty_description" => $data['description']
            );
            $job_data['warranty_customer_agreement_details'] = json_encode($warranty_customer_agreement_details);
            $job_data['important_notes'] = $data['description'];
            $job_id = $this->job->insert_or_update_warranty_job($crm_cust_id,$job_data);
    
            if($job_id != ''){
                $attachment_files = $this->cron->fetch_kuga_web_where('web_warranty_enquiry_attachments','*',array('uuid' => $data['uuid']));
                $this->job->insert_or_update_warranty_job_docs($job_id,$attachment_files);
            }
            
            $this->email_manager->send_warranty_job_success_email($data,$attachment_files);
            
            $rdata['status'] = 1;
            $rdata['status_info'] = 'Job Created Successfully';
            return $rdata;
        }
        
        $rdata['status'] = 3;
        $rdata['status_info'] = 'Looks like something went wrong while creating the job';
        
        return $rdata;
    }

}

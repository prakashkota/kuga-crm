<?php
$space_type = json_decode(json_encode($space_type), true);
$ceiling_height = json_decode(json_encode($ceiling_height), true);
$defaultHeight = 700;
?>
<div style="page-break-after: always;"></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding:0; margin:0;">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01-wide.jpg'; ?>" alt="" width="100%" height="100" /></td>
    </tr>
    <tr>
        <td height="10">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                   <th colspan="4" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">&nbsp;</th>
                   <th  colspan="6" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Existing Lighting (Baseline)</th>
                   <th  colspan="6" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Upgraded Lighting (Baseline)</th>
                </tr>
                <tr>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Room/Area</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Celling Height</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Space Type/BCA</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Anual Operating Hours</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Lamp Type &amp; Ballast/Control Gear</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Nom.Watts</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Control System</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Air-con?</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">No.of Lamps</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Activity type</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Product Type</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Product Brand and model number</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Sensor</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">No.of Lamps</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px">Unit Cost exc GST</th>
                   <th style=" background:#000; border:1px solid #e8e8e8e8e8e8; color:#fff; text-align:center; font-size:12px">Total Cost exc GST</th>
                </tr>
                <tbody>
                    <?php
                        $height = 130;
                        $subtotal = 0;
                        for ($i = 0; $i < count($product_chunks); $i++) {
                            // $total_cost_excGST = '';
                            // if ($product->item_qty[$i] != '' && $product->item_cost_excGST[$i] != '') {
                            //     $total_cost_excGST = $product->item_qty[$i] * $product->item_cost_excGST[$i];
                            // }
                            if($product_chunks[$i] != ''){
                                // $typeData = $this->common->fetch_row('tbl_product_types','*',['type_id'=>$product->type_id[$i]]);
                                // $anual_op_hrs = 0;
                                // for($idx=0;$idx<count($calculator_items['bca_types']);$idx++){
                                //     if($calculator_items['bca_types'][$idx]['type_of_space'] == $product->item_bca_type[$i]){
                                //         $anual_op_hrs = $calculator_items['bca_types'][$idx]['op_hrs_per_yr'];        
                                //     }
                                // }
                    ?>
                   <tr>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_room_area']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_ceiling_height']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_bca_type']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['anual_op_hrs']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_lamp_type']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_nom_watts']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_control_system']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_air_con']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_existing_no_of_lamps']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_activity_type']; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['type_name']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_name']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_sensor']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_qty']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['item_cost_excGST']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product_chunks[$i]['total_cost_excGST']; ?></strong></td>
                    </tr>
                <?php 
                    if(isset($product_chunks[$i]['total_cost_excGST']) && !empty($product_chunks[$i]['total_cost_excGST'])){
                        $subtotal = $subtotal+$product_chunks[$i]['total_cost_excGST'];
                    }
                    }} ?> 
                <tr>
                    <th height="40" colspan="6" style="text-align: right; padding-right: 10px;">Further Products quoted can be found on a separate sheet: </th>
                    <th colspan="4" style="text-align: left; padding-left: 10px;">
                       <?php if (isset($booking_form->is_more_products)) { ?>
                            <input type="checkbox" checked="" style="margin-left:10px;" />
                        <?php } else { ?>
                            <input type="checkbox" style="margin-left:10px;" />
                        <?php } ?>
                    </th>
                    <?php if($product->is_end){ ?>
                        <th colspan="5" style="text-align: right; padding-right: 10px;">Sub Total exc GST:</th>
                        <th>$<?php echo $product->total_excGst; ?></th>
                    <?php }else{ ?>
                        <th colspan="5" style="text-align: right; padding-right: 10px;">Sub Total exc GST:</th>
                        <th>$<?php echo $subtotal; ?></th>
                    <?php } ?>
                </tr>
                <?php if($product->is_end){ ?>
                    <tr>
                        <th colspan="4" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:12px;">Access Equipment</th>
                        <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Detailed Description of Access Equipment</th>
                        <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Quantity</th>
                        <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Unit Cost exc GST</th>
                        <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Total Cost exc GST</th>
                    </tr>
                <?php for ($i = 0; $i < count($ae->ae_name); $i++) {
                        $total_cost_excGST = '';
                        if ($ae->ae_qty[$i] != '' && $ae->ae_cost_excGST[$i] != '') {
                            $total_cost_excGST = $ae->ae_qty[$i] * $ae->ae_cost_excGST[$i];
                        }
                ?>
                    <tr>
                        <td colspan="4" height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_name[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_description[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_qty[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_cost_excGST[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td colspan="13" height="40" colspan="4" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;">Sub Total exc GST:</td>
                        <td colspan="2"  style="padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $ae->total_excGst; ?></strong></td>
                    </tr>
                <?php } ?>
            </tbody>
            </table></td>
        </tr>
        <tr>
            <?php if($product->is_end){ ?>
                <td valign="top">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="560" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                            <td width="147" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                            <td width="147" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_excGst; ?></strong></td>
                        </tr>
                        <tr>
                            <td width="560" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                            <td align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>GST:</strong></td>
                            <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_Gst; ?></strong></td>
                        </tr>
                        <tr>
                            <td width="560" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                            <td height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                            <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_incGst; ?></strong></td>
                        </tr> 
                    </table>
                </td>
            <?php } ?>
        </tr>
    <?php
        if($product->is_end){
            $height = 70;   
        }else{
            $height = 250;
        }
    ?>
    <tr>
        <td height="<?= $height ?>">&nbsp;</td>
    </tr>
    <tr>
        <td height="70" class="bg-bottom" style="background:#ededed"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:12px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 4 Bridge Rd, Keysborough VIC 3173</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 6 Turbo Rd, Kings Park, NSW 2148</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 31 Chetwynd St, Loganholme, QLD 4129 </td>
                            </tr>
                             
                        </table></td>
                    <td align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" /></td>
                </tr>
            </table></td>
    </tr>
</table>

<style>
    .sr-inactive_user{color:red;}
</style>
<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Team Leader - <?php } ?><?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/team-representative/add') ?>"><i class="icon-plus"></i> Add New </a>
            </div>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="franchise_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Representative Name</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Mobile No</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Job CRM Login Created</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($users))
                                        foreach ($users as $key => $value) {
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $value['full_name']; ?> <span class="hidden"><?php echo $value['state_postal']; ?></span></td>
                                                <td class="text-center"><?php echo $value['email']; ?></td>
                                                <td class="text-center"><?php echo $value['company_contact_no']; ?></td>
                                                <td class="text-center"><?php echo ($value['banned'] == 1) ? '<span class="badge badge-danger">Not Current</span>' : '<span class="badge badge-success">Current</span>'; ?></td>
                                                <td class="text-center"><?php echo ($value['job_crm_user_id'] == NULL) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>'; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/team-representative/edit/<?php echo $value['user_id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/settings/change-password/<?php echo $value['user_id']; ?>"><i class="icon-pencil"></i> Change Password</a> 
                                                            <?php if ($value['banned']) { ?>
                                                                <a class="dropdown-item" data-href="<?php echo site_url('admin/ban_unban_user/' . $value['user_id'] . '/' . $value['banned']); ?>"  data-toggle="modal" data-target="#confirm-unban"><i class="icon-user"></i> Unban</a>
                                                            <?php } else { ?>
                                                                <a class="dropdown-item" data-href="<?php echo site_url('admin/ban_unban_user/' . $value['user_id'] . '/' . $value['banned']); ?>"  data-toggle="modal" data-target="#confirm-ban"><i class="icon-user"></i> Ban</a>
                                                            <?php } ?>
                                                            <a class="dropdown-item delete_user" href="javascript:void(0);" data-userid="<?php echo $value['user_id']; ?>"><i class="icon-trash"></i> Delete</a>    
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="delete_user_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                <div class="modal-header">
                    <span class="modal-title"><i class="fa fa-user"></i> Confirm User Delete</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this user? Once deleted it can't be rolled back.</p>
                    <p><strong>Would you like to transfer this user deal to someone else? Then choose the user from below dropdown.</strong></p>
                    <select class="form-control" id="assign_user">
                        <option value="">Select User</option>
                        <?php if (!empty($admin)) {
                            foreach ($admin as $key => $value) {
                                ?>
                                <option class="option_user <?php if ($value['banned'] == 1) { ?>sr-inactive_user <?php } ?>" value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name'] . ' (Admin)'; ?></option>
                        <?php }} ?>
                        <?php if (!empty($users)) {
                            foreach ($users as $key => $value) {
                                ?>
                                <option class="option_user <?php if ($value['banned'] == 1) { ?>sr-inactive_user <?php } ?>" value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?> <?php if ($value['banned'] == 1) { ?> (Banned) <?php } ?></option>
                        <?php }} ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  id="confirm_delete_user">Confirm Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#franchise_list').DataTable();

        //Filter State    
        var states = '<?php echo (isset($states) && !empty($states)) ? json_encode($states, JSON_HEX_APOS) : ''; ?>';

        states = JSON.parse(states);
        var state_select = document.createElement('select');
        state_select.setAttribute('id', 'state_filter');
        //state_select.className = 'form-control';
        var option = document.createElement('option');
        option.setAttribute('value', '');
        option.innerHTML = 'Select State';
        state_select.appendChild(option);
        for (var i = 0; i < states.length; i++) {
            var option = document.createElement('option');
            option.setAttribute('value', states[i]);
            option.innerHTML = states[i];
            state_select.appendChild(option);
        }
        var state_select_div = document.createElement('div');
        state_select_div.className = 'col-md-2';
        state_select_div.setAttribute('style', 'display:inline-block;');
        state_select_div.innerHTML = '<label style="margin:0 auto;">Filter By State:</label>';
        state_select_div.appendChild(state_select);

        document.getElementById("franchise_list_filter").parentNode.insertBefore(state_select_div, document.getElementById("franchise_list_filter"));

        var table = $('#franchise_list').dataTable().api();
        $('#state_filter').change(function () {
            table.columns(0)
                    .search($(this).val(), true, false)
                    .draw();
        });

        //Handle Delete User
        $(document).on('click','.delete_user',function () {
            $(".option_user").removeClass('hidden');
            var userid = $(this).attr('data-userid');
            $('#confirm_delete_user').attr('data-userid',userid);
            $('#delete_user_modal').modal('show');
            $("#assign_user option[value=" + userid + "]").addClass('hidden');
        });
        
        $('#confirm_delete_user').click(function(){
            var userid = $(this).attr('data-userid');
            var assign_user = $('#assign_user').val();
            
            if(userid == ''){
                return false;
            }
            
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/delete_user/',
                datatype: 'json',
                data: {userid: userid,assign_user:assign_user},
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }else{
                        toastr["error"](stat.status);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>


	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="generator" content="vBulletin 3.7.2">

<meta name="keywords" content=" Trying to use Fckeditor in form, php coding serverside scripting open source mysql">
<meta name="description" content=" Trying to use Fckeditor in form General Help">


<!-- CSS Stylesheet -->
<style type="text/css" id="vbulletin_css">
/**
* vBulletin 3.7.2 CSS
* Style: 'PHPbuilderVB3.7.2'; Style ID: 7
*/
body
{
	background: #000077;
	color: #000000;
	 background: url("/images/grad_bg.gif") repeat-y;
}
a:link, body_alink
{
	color: #000077;
}
a:visited, body_avisited
{
	color: #000077;
}
a:hover, a:active, body_ahover
{
	color: #cc3333;
}
.page
{
	background: #FFFFFF;
	color: #000077;
}
td, th, p, li
{
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.tborder
{
	background: #ffffff;
}
.tcat
{
	background: #ffffff;
	color: #FFFFFF;
	font-weight: bold;
	 background: url("/images/grad_bg.gif") repeat-y;
}
.tcat a:link, .tcat_alink
{
	color: #FFFFFF;
	text-decoration: none;
}
.tcat a:visited, .tcat_avisited
{
	color: #FFFFFF;
	text-decoration: none;
}
.tcat a:hover, .tcat a:active, .tcat_ahover
{
	text-decoration: underline;
}
.thead
{
	color: #FFFFFF;
	font-size: 9pt;
	font-weight: bold;
	 background: url("/images/grad_bg.gif") repeat-y;
}
.thead a:link, .thead_alink
{
	color: #FFFFFF;
}
.thead a:visited, .thead_avisited
{
	color: #FFFFFF;
}
.tfoot
{
	background: #000088;
	color: #FFFFFF;
	 background: url("/images/grad_bg.gif") repeat-y;
}
.tfoot a:link, .tfoot_alink
{
	color: #FFFFFF;
}
.tfoot a:visited, .tfoot_avisited
{
	color: #FFFFFF;
}
.tfoot a:hover, .tfoot a:active, .tfoot_ahover
{
	color: #FFFF66;
}
.alt1, .alt1Active
{
	background: #F5F5FF;
	color: #333333;
}
.alt2, .alt2Active
{
	background: #E1E4F2;
	color: #000000;
}
.inlinemod
{
	background: #FFFFCC;
	color: #000000;
}
.wysiwyg
{
	background: #F5F5FF;
	color: #000000;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	margin: 5px 10px 10px 10px;
	padding: 0px;
}
.wysiwyg a:link, .wysiwyg_alink
{
	color: #22229C;
}
.wysiwyg a:visited, .wysiwyg_avisited
{
	color: #22229C;
}
.wysiwyg a:hover, .wysiwyg a:active, .wysiwyg_ahover
{
	color: #FF4400;
}
textarea, .bginput
{
	background: #FFFFFF;
	font: 10pt verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.bginput option, .bginput optgroup
{
	font-size: 10pt;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.button
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
select
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
option, optgroup
{
	font-size: 11px;
	font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.smallfont
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.time
{
	color: #000077;
}
.navbar
{
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.highlight
{
	color: #FF0000;
	font-weight: bold;
}
.fjsel
{
	background: #000088;
	color: #FFFFFF;
}
.fjdpth0
{
	background: #F7F7F7;
	color: #000000;
}
.panel
{
	background: #DDDDDD;
	color: #000000;
	padding: 10px;
	border: 2px outset;
}
.panelsurround
{
	background: #CCCCCC;
	color: #000000;
}
legend
{
	color: #000077;
	font: 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
}
.vbmenu_control
{
	color: #FFFFFF;
	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	padding: 3px 6px 3px 6px;
	white-space: nowrap;  background: url("/images/grad_bg.gif") repeat-y;overflow:hidden;
}
.vbmenu_control a:link, .vbmenu_control_alink
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:visited, .vbmenu_control_avisited
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_control a:hover, .vbmenu_control a:active, .vbmenu_control_ahover
{
	color: #FFFFFF;
	text-decoration: underline;
}
.vbmenu_popup
{
	background: #DDDDDD;
	color: #000000;
	border: 1px solid #0B198C;
}
.vbmenu_option
{
	background: #BBC7CE;
	color: #000000;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_option a:link, .vbmenu_option_alink
{
	color: #000077;
	text-decoration: none;
}
.vbmenu_option a:visited, .vbmenu_option_avisited
{
	color: #000077;
	text-decoration: none;
}
.vbmenu_option a:hover, .vbmenu_option a:active, .vbmenu_option_ahover
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_hilite
{
	background: #8A949E;
	color: #FFFFFF;
	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
	white-space: nowrap;
	cursor: pointer;
}
.vbmenu_hilite a:link, .vbmenu_hilite_alink
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_hilite a:visited, .vbmenu_hilite_avisited
{
	color: #FFFFFF;
	text-decoration: none;
}
.vbmenu_hilite a:hover, .vbmenu_hilite a:active, .vbmenu_hilite_ahover
{
	color: #FFFFFF;
	text-decoration: none;
}
BODY {
}
SELECT {
	FONT-FAMILY: Verdana,Arial,Helvetica,sans-serif;
	FONT-SIZE: 11px;
	COLOR: #000000;
	BACKGROUND-COLOR: #CFCFCF;
}
TEXTAREA, .bginput {
FONT-SIZE: 12px;
	FONT-FAMILY: Verdana,Arial,Helvetica,sans-serif;
	COLOR: #000000;
	BACKGROUND-COLOR: #FFFFFF;
}
A:link, A:visited, A:active {
	COLOR: {linkcolor};
}
A:hover {
	COLOR: {hovercolor};
}
#cat A:link, #cat A:visited, #cat A:active {
	COLOR: {categoryfontcolor};
	TEXT-DECORATION: none;
}
#cat A:hover {
	COLOR: {categoryfontcolor};
	TEXT-DECORATION: underline;
}
#ltlink A:link, #ltlink A:visited, #ltlink A:active {
	COLOR: {linkcolor};
	TEXT-DECORATION: none; }
#aup { font-size:10pt }
.bigusername { font-size:12pt }

tr.trhead {  background: url("/images/grad_bg.gif") repeat-y; }
</style>
<link rel="stylesheet" type="text/css" href="showthread_files/vbulletin_important.css">


<!-- / CSS Stylesheet -->

<script type="text/javascript" src="showthread_files/yahoo-dom-event.js"></script>
<script type="text/javascript" src="showthread_files/connection-min.js"></script>
<script type="text/javascript">
<!--
var SESSIONURL = "";
var SECURITYTOKEN = "guest";
var IMGDIR_MISC = "images/misc";
var vb_disable_ajax = parseInt("0", 10);
// -->
</script>
<script type="text/javascript" src="showthread_files/vbulletin_global.js"></script>
<script type="text/javascript" src="showthread_files/vbulletin_menu.js"></script>


	<link rel="alternate" type="application/rss+xml" title="PHPBuilder.com RSS Feed" href="http://www.phpbuilder.com/board/external.php?type=RSS2">
	
		<link rel="alternate" type="application/rss+xml" title="PHPBuilder.com - General Help - RSS Feed" href="http://www.phpbuilder.com/board/external.php?type=RSS2&amp;forumids=2">
	


<script type="text/JavaScript">

function xmlhttpPost(strURL) {
/*<![CDATA[ */

var superTracker = _gat._getTracker("UA-2838492-1"); 
 superTracker._initData();
 superTracker._trackPageview();
 
    pageTracker._initData();
    pageTracker._trackPageview();

 
    var xmlHttpReq = false;
    var self = this;
    // Mozilla/Safari/Opera
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState == 4) {
            updatepage(self.xmlHttpReq.responseText);
        }
    }
    self.xmlHttpReq.send(getquerystring());
}

function getquerystring() {

qstr='<?'+
 'oas_setup("intm/webdev/www.phpbuilder.com/board@'+
 '468x60-1,468x60-2,125x125-1,336x280-2,house_ribbon,flex"); '+
 'echo oas_ad(\'OAS_468-1\');'+
 '?>';

    return qstr;

}

function updatepage(str){
var ads=str.split('|');
var leo=ads[0];
var rib=ads[1];
var sky=ads[2];
var bot=ads[3];

    document.getElementById("leo").innerHTML = leo; 
    document.getElementById("rib").innerHTML = rib;
    document.getElementById("sky").innerHTML = sky;
    document.getElementById("bot").innerHTML = bot;

}
/* ]]> */
</script>
	<title> Trying to use Fckeditor in form - PHPBuilder.com</title>
	<script type="text/javascript" src="showthread_files/vbulletin_post_loader.js"></script>
	<style type="text/css" id="vbulletin_showthread_css">
	<!--
	
	#links div { white-space: nowrap; }
	#links img { vertical-align: middle; }
	-->
	</style>
</head><body onload="">
<center>
<script language="JavaScript">
<!--
OAS_url = 'http://mjxads.internet.com/RealMedia/ads/';
OAS_sitepage = 'intm/webdev/www.phpbuilder.com/board/showthread.php/index';
OAS_listpos = '468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost';
OAS_query = '';
OAS_target = '_top';
OAS_version = 10;
OAS_rn = '001234567890'; OAS_rns = '1234567890';
OAS_rn = new String (Math.random()); OAS_rns = OAS_rn.substring (2, 11);
function OAS_NORMAL(pos) {
var_a=('<A HREF="' + OAS_url + 'click_nx.cgi/' + OAS_sitepage );
var_b=(var_a + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + '?' );
var_c=(var_b+ OAS_query + '" TARGET=' + OAS_target + '>');
document.write(var_c);
var_aa=('<IMG SRC="' + OAS_url + 'adstream_nx.cgi/' + OAS_sitepage );
var_bb=( var_aa + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + '?');
var_cc=( var_bb + OAS_query + '" BORDER=0></A>');
document.write(var_cc);
}
//-->
</script>
<script language="JavaScript1.1">
<!--
OAS_version = 11;
if (navigator.userAgent.indexOf('Mozilla/3') != -1 )
OAS_version = 10;
if (navigator.userAgent.indexOf('Mozilla/4.0 WebTV') != -1)
OAS_version = 10;
if (OAS_version >= 11) {
var_aaa=('<SCRIPT LANGUAGE=JavaScript1.1 SRC="' + OAS_url);
var_bbb=(var_aaa + 'adstream_mjx.ads/' + OAS_sitepage + '/1');
var_ccc=(var_bbb + OAS_rns + '@' + OAS_listpos + '?' + OAS_query );
var_ddd=(var_ccc + '"><\/SCRIPT>');
document.write(var_ddd);
}
//-->
</script><script language="JavaScript1.1" src="showthread_files/1454525112468x60-1468x60-2125x125-1sitetext-1cp1cp2cp3cp4cp5c.js"></script>
<script language="JavaScript">
<!--
document.write('');
function OAS_AD(pos) {
if (OAS_version >= 11)
OAS_RICH(pos);
else
OAS_NORMAL(pos);
}
//-->
</script>

<!-- OAS AD 'house_ribbon' begin ------>
<script language="JavaScript">
<!--
 OAS_AD('house_ribbon');
//-->
</script><script type="text/javascript" src="showthread_files/fsmenu_commented.js"></script>
<script type="text/javascript" src="showthread_files/menuset.js"></script>
<script type="text/javascript" src="showthread_files/menusetv2.js"></script>
<script type="text/javascript" src="showthread_files/menusetv3.js"></script>
<link rel="stylesheet" type="text/css" href="showthread_files/listmenu_h.css">
<link rel="stylesheet" type="text/css" href="showthread_files/listmenu_h_settings.css">

<style type="text/css">

#navitoolbarcontainer a {
	line-height: 16px;
	}

</style>



<!-- START TOP TOOLBAR -->
<div id="heightcontainer"></div>		

<div style="width: 100%;" id="navitoolbarcontainer">
		  <div style="margin: 0px; padding: 0px; width: 100%;">
		  <div id="navitile" style="width: 100%;">
			<div style="padding-left: 0px; margin-left: 0px; padding-top: 5px; padding-right: 5px; float: left; white-space: nowrap;">

			<a title="View Internet.com - The Network for Technology Professionals" href="http://www.internet.com/" class="gt"><img title="Internet.com - The Network for Technology Professionals" alt="Internet.com - The Network for Technology Professionals" src="showthread_files/icom_logo_global.png" border="0"></a>

			</div>		
				<ul class="menulist" id="listMenuRoot" style="height: 22px;">
				 <li style="margin-top: 0px;">
				  <a class="gt" href="http://www.internet.com/it/" style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;"><span class="subind">&gt;</span> <b>IT</b> </a>

				  
				  
				  <ul id="listMenu-id-1">
                   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/it">internet.com/IT</a></li>
		   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/cio">internet.com/CIO</a></li>  
                   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/security">internet.com/Security</a></li>  
                   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/networking">internet.com/Networking</a></li>  
                   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/storage">internet.com/Storage</a></li>  
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.bitaplanet.com/">bITa Planet</a></li>  
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.cioupdate.com/">CIO Update</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.databasejournal.com/">Database Journal</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.datamation.com/">Datamation</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.enterpriseitplanet.com/">Enterprise IT Planet</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.enterprisenetworkingplanet.com/">Enterprise Networking Planet</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.enterprisestorageforum.com/">Enterprise Storage Forum</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.esecurityplanet.com/">eSecurity Planet</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.hardwarecentral.com/">Hardware Central</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.intranetjournal.com/">Intranet Journal</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.isp-planet.com/">ISP Planet</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.itsmwatch.com/">ITSMwatch</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.itchannelplanet.com/">IT Channel Planet</a></li> 
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.linuxplanet.com/">Linux Planet</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.opennetworkstoday.com/">Open Networks Today</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://serverwatch.internet.com/">ServerWatch</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.voipplanet.com/">VoIP Planet</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.webvideouniverse.com/">WebVideoUniverse</a> 
				   </li><li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.wi-fiplanet.com/">Wi-Fi Planet</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.windrivers.com/">WinDrivers.com</a></li>   
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/sections/">Network Map</a></li>  
				  </ul>
				 </li>
				
				 <li style="margin-top: 0px;">
				  <a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/developer" class="gt"><span class="subind">&gt;</span> <b>Developer</b> </a>
				   <ul id="listMenu-id-2">
                   <li style="background-image: url(/icom_includes/toolbars/globaltoolbar/img/background_topsub.jpg); background-repeat: repeat-x;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none; font-weight: bold;" href="http://www.internet.com/developer/">internet.com/Developer</a></li>
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.15seconds.com/">15 Seconds</a></li>

				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.4guysfromrolla.com/">4GuysFromRolla.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.asp101.com/">ASP101</a></li>		
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.codeguru.com/">CodeGuru</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.developer.com/">Developer.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.devx.com/">DevX</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.flashkit.com/">FlashKit.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.developer.com/java/">Gamelan</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.jars.com/">JARS</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.javascript.com/">JavaScript.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://javascriptsource.com/">JavaScriptSource</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.phpbuilder.com/">PHPBuilder.com</a></li>   
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.scriptsearch.com/">ScriptSearch</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.vbforums.com/">VB Forums</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.vbwire.com/">VB Wire</a></li>

				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.webdeveloper.com/">WebDeveloper.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.webreference.com/">Webreference</a></li>
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/sections/">Network Map</a></li>  
				  </ul>

				 </li>
				
				 <li style="margin-top: 0px;">
				  <a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internetnews.com/" class="gt"><span class="subind">&gt;</span> <b>News</b> </a>
				   <ul id="listMenu-id-3">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internetnews.com/">Internetnews.com</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.linuxtoday.com/">Linux Today</a></li>   		   
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/sections/">Network Map</a></li>  
				  </ul>
				 </li>
				
				
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.smallbusinesscomputing.com/" class="gt"><span class="subind">&gt;</span> <b>Small Business</b> </a>
				   <ul id="listMenu-id-4">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.ecommerce-guide.com/">Ecommerce Guide</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.refer-it.com/">Refer-It</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.smallbusinesscomputing.com/">SmallBusinessComputing</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.webopedia.com/">Webopedia</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.winplanet.com/">WinPlanet</a></li>
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/sections/">Network Map</a></li>  
				  </ul>
				 </li>
				 
				
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/personaltechnology/" class="gt"><span class="subind">&gt;</span> <b>Personal Tech</b> </a>

				   <ul id="listMenu-id-5">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.blackberrytoday.com/">BlackBerryToday</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://blog.iphoneguide.com/">iPhoneGuide</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.jumbo.com/">Jumbo</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.megapixel.net/">Megapixel.net</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.palmblvd.com/">Palm Boulevard</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.pdastreet.com/">PDAStreet</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.pocketpcwire.com/">PocketPCWire</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.sharkyextreme.com/">SharkyExtreme</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.smartphonetoday.com/">Smart Phone Today</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://asp.thelist.com/">The List: ASPs</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://broadband.thelist.com/">The List: Broadband</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.thelist.com/">The List: ISPs</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://webhosts.thelist.com/">The List: WebHosts</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://webdesign.thelist.com/">The List: WebDesigners</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.wi-fihotspotlist.com/">Wi-FiHotSpotList</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.windowsmobiletoday.com/">WindowsMobileToday</a></li>
                   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/sections/">Network Map</a></li>  
				  </ul>
				 </li>		
				</ul>
				
				
				
				<ul style="height: 22px;" id="listMenuv2Root" class="menulistv2">
				 <li style="margin-top: 0px;">

				  <a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica;" href="http://www.jupiterevents.com/" class="gt"><span class="subind">&gt;</span> Events </a>

				  <ul id="listMenuv2-id-1">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.jupiterevents.com/">JupiterEvents</a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/webcasts">internet.com/webcasts</a></li>
				  </ul>
				 </li>
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/blogs/" class="gt"> Blogs </a></li>
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.justtechjobs.com/" class="gt"> Jobs </a></li>
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.jupitermedia.com/partners/" class="gt"> Partners </a></li>

				 <li style="margin-top: 0px;">
				  <a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/solutions" class="gt"><span class="subind">&gt;</span> Solutions </a>
				  <ul id="listMenuv2-id-2">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/ebook"> eBooks </a></li>
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/video"> Video </a></li>
				  </ul>
				 </li>

				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internetshopper.com/" class="" gt=""> Shop </a></li>

				 
				</ul>
		
		
				<ul class="menulistv3" id="listMenuv3Root" style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; height: 22px;">
				 <li style="margin-top: 0px;"><a class="gt" href="http://member.internet.com/" style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;"><span class="subind">&gt;</span> Login </a>

				 
				  <ul id="listMenuv3-id-1">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://member.internet.com/profile/postal">Manage My Profile</a></li>

				  </ul>

				 
				 
				 </li>
				 <li style="margin-top: 0px;"><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://member.internet.com/register" class="gt"><span class="subind">&gt;</span>Register </a>
				 
				   <ul id="listMenuv3-id-2">
				   <li><a style="color: rgb(255, 255, 255); font-size: 10px; font-family: arial,verdana,helvetica; text-decoration: none;" href="http://www.internet.com/WhyJoin">Why Join?</a></li>
				  </ul>
				 
				 
				 </li>
		
				 
				</ul>

		<div style="margin-top: 0px;" id="lightgrey">

		<form target="_top" action="http://search.internet.com/www.jupiterweb.com" method="post">
			<input value="1" name="IC_Summary" type="hidden">
			<input value="0" name="IC_StartNumber" type="hidden">
			<input value="10" name="IC_BatchSize" type="hidden">

			<input value="50" name="IC_RelevanceThreshold" type="hidden">
			<input value="all" name="IC_QueryDatabase" type="hidden">
			<input title="Search Internet.com" onfocus="this.value='';" class="inputglobal" size="11" name="IC_QueryText" value="" type="text">

			<input alt="go" src="showthread_files/searchgt_button.gif" name="SUBMIT" value="Find" align="absmiddle" border="0" height="18" type="image" vspace="2" width="42">
		</form>
		</div>
	</div>
	</div>

	<!--
	<div style="width:100%;">
    	<img src="/icom_includes/toolbars/globaltoolbar/img/shad.png" style="width:100%; height:11px; padding:0px; margin-top:22px;" />
	</div>
	-->
	</div>
   
   <script src="showthread_files/pngadapt.js" type="text/javascript"></script>


<!-- END TOP TOOLBAR -->
<img src="showthread_files/37346331386162333439326135306130_004.gif" height="2" width="2">
<noscript>
<A
HREF="http://63.236.18.118/RealMedia/ads/click_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!house_ribbon">
<IMG
SRC="http://63.236.18.118/RealMedia/ads/adstream_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!house_ribbon"
border=0>
</A>
</noscript>
<!------ OAS AD 'house_ribbon' end ------>



<span style="font-family: verdana; font-style: normal; font-variant: normal; font-weight: normal; font-size: 8pt; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; color: rgb(255, 255, 255); position: relative; top: -15px;">To register for an Internet.com membership to receive newsletters and white papers, use the Register button ABOVE. <br>
To participate in the message forums BELOW, click <a href="http://www.phpbuilder.com/board/register.php?do=signup">here</a></span>


</center>

<!-- logo -->
<a name="top"></a>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
	<td align="left"><a href="http://www.phpbuilder.com/"><img title="PHPBuilder.com" src="showthread_files/logo.gif" alt="PHPBuilder.com" border="0"></a></td>
	<td id="header_right_cell" align="right">
		&nbsp;
	</td>
</tr>
</tbody></table>
<!-- /logo -->
<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td background="showthread_files/background.gif" bgcolor="#ffffff" height="11" valign="top" width="11"><img src="showthread_files/topleft.gif" height="11" width="11"></td>
<td colspan="3" align="right" height="11"></td><td>
</td><td background="showthread_files/background.gif" bgcolor="#ffffff" height="11" valign="top" width="11"><img src="showthread_files/topright.gif" height="11" width="11"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td colspan="3" id="leo" align="center">
<!-- OAS AD '468x60-1' begin ------>
<script language="JavaScript">
<!--
 OAS_AD('468x60-1');
//-->
</script><iframe src="showthread_files/011409746351.htm" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" allowtransparency="true" frameborder="0" height="90" scrolling="no" width="728">&lt;script
language="JavaScript" type="text/javascript"&gt;
document.write('&lt;a
href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/1409746351/468x60-1/OasDefault/MSFT_SQL_FY09Q2_INTM_1q/msftsqlfy09q2intmronleo.html/37346331386162333439326135306130?http://clk.atdmt.com/MRT/go/115933820/direct/01/1409746351"
target="_blank"&gt;&lt;img
src="http://view.atdmt.com/MRT/view/115933820/direct/01/1409746351"/&gt;&lt;/a&gt;');
&lt;/script&gt;&lt;noscript&gt;&lt;a
href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/1409746351/468x60-1/OasDefault/MSFT_SQL_FY09Q2_INTM_1q/msftsqlfy09q2intmronleo.html/37346331386162333439326135306130?http://clk.atdmt.com/MRT/go/115933820/direct/01/1409746351"
target="_blank"&gt;&lt;img border="0"
src="http://view.atdmt.com/MRT/view/115933820/direct/01/1409746351"
/&gt;&lt;/a&gt;&lt;/noscript&gt;</iframe><img src="showthread_files/37346331386162333439326135306130.gif" height="1" width="1">
<noscript>
<A
HREF="http://63.236.18.118/RealMedia/ads/click_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!468x60-1">
<IMG
SRC="http://63.236.18.118/RealMedia/ads/adstream_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!468x60-1"
border=0>
</A>
</noscript>
<!------ OAS AD '468x60-1' end ------>

<br><br>
</td>

</tr><tr>
<td bgcolor="#ffffff" width="11">&nbsp;</td>
<td valign="top">
<div id="rib" align="center"><!-- OAS AD 'ciu' begin ------>
<script language="JavaScript">
<!--
 OAS_AD('ciu');
//-->
</script><table bgcolor="#000000" border="0" cellpadding="1" cellspacing="0">
        <tbody><tr>
          <td>
            <table bgcolor="#cccccc" border="0" cellpadding="1" cellspacing="0">
              <tbody><tr> 
                <td><font face="Verdana, Arial, Helvetica, sans-serif" size="-2"><a href="http://mjxads.internet.com/RealMedia/ads/click_lx.cgi/intm/webdev/www.phpbuilder.com/board/showthread.php/index/1789483274/ciu/OasDefault/VMware_GEMS_1ai/CIU_VMwareOnline.html/37346331386162333439326135306130">
<b>VMware Online Virtualization Forum:</b> Learn about the benefits of
a virtual infrastructure, network with industry experts without the
hassle or expense of travel. Register today!
</a>

</font></td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      </tbody></table><img src="showthread_files/37346331386162333439326135306130_005.gif" height="1" width="1">
<noscript><A
HREF="http://63.236.18.118/RealMedia/ads/click_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!ciu">
<IMG
SRC="http://63.236.18.118/RealMedia/ads/adstream_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!ciu"
border=0>
</A>
</noscript>
<!------ OAS AD 'ciu' end ------>
</div>
<table id="main"><tbody><tr width="100%"><td valign="top" width="100%">
<!-- content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="width: 100%; text-align: left;">
		<div style="padding: 0px 10px;" align="left">





<br>

<!-- breadcrumb, login, pm info -->
<table class="tborder" align="center" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody><tr>
	<td class="alt1" width="100%">
		
			<table border="0" cellpadding="0" cellspacing="0">
			<tbody><tr valign="bottom">
				<td><a href="#" onclick="history.back(1); return false;"><img title="Go Back" src="showthread_files/navbits_start.gif" alt="Go Back" border="0"></a></td>
				<td>&nbsp;</td>
				<td width="100%"><span class="navbar"><a href="http://www.phpbuilder.com/board/index.php" accesskey="1">PHPBuilder.com</a></span> 
	<span class="navbar">&gt; <a href="http://www.phpbuilder.com/board/forumdisplay.php?f=1">PHP Help</a></span>


	<span class="navbar">&gt; <a href="http://www.phpbuilder.com/board/forumdisplay.php?f=2">General Help</a></span>

</td>
			</tr>
			<tr>
				<td class="navbar" style="font-size: 10pt; padding-top: 1px;" colspan="3"><a href="http://www.phpbuilder.com/board/showthread.php?t=10350156"><img title="Reload this Page" class="inlineimg" src="showthread_files/navbits_finallink_ltr.gif" alt="Reload this Page" border="0"></a> <strong>
	 Trying to use Fckeditor in form

</strong></td>
			</tr>
			</tbody></table>
		
	</td>

	<td class="alt2" style="padding: 0px;" nowrap="nowrap">
		<!-- login form -->
		<form action="login.php?do=login" method="post" onsubmit="md5hash(vb_login_password, vb_login_md5password, vb_login_md5password_utf, 0)">
		<script type="text/javascript" src="showthread_files/vbulletin_md5.js"></script>
		<table border="0" cellpadding="0" cellspacing="3">
		<tbody><tr>
			<td class="smallfont" style="white-space: nowrap;"><label for="navbar_username">User Name</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_username" id="navbar_username" size="10" accesskey="u" tabindex="101" value="User Name" onfocus="if (this.value == 'User Name') this.value = '';" type="text"></td>
			<td class="smallfont" nowrap="nowrap"><label for="cb_cookieuser_navbar"><input name="cookieuser" value="1" tabindex="103" id="cb_cookieuser_navbar" accesskey="c" type="checkbox">Remember Me?</label></td>
		</tr>
		<tr>
			<td class="smallfont"><label for="navbar_password">Password</label></td>
			<td><input class="bginput" style="font-size: 11px;" name="vb_login_password" id="navbar_password" size="10" tabindex="102" type="password"></td>
			<td><input class="button" value="Log in" tabindex="104" title="Enter your username and password in the boxes provided to login, or click the 'register' button to create a profile for yourself." accesskey="s" type="submit"></td>
		</tr>
		</tbody></table>
		<input name="s" value="" type="hidden">
		<input name="securitytoken" value="guest" type="hidden">
		<input name="do" value="login" type="hidden">
		<input name="vb_login_md5password" type="hidden">
		<input name="vb_login_md5password_utf" type="hidden">
		</form>
		<!-- / login form -->
	</td>

</tr>
</tbody></table>
<!-- / breadcrumb, login, pm info -->

<!-- nav buttons bar -->
<div class="tborder" style="padding: 1px; border-top-width: 0px;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr align="center">
		
		
			<td class="vbmenu_control"><a href="http://www.phpbuilder.com/board/register.php" rel="nofollow">Register</a></td>
		
		
		<td class="vbmenu_control"><a href="http://www.phpbuilder.com/board/faq.php" accesskey="5">FAQ</a></td>
		
			<td class="vbmenu_control"><a href="http://www.phpbuilder.com/board/memberlist.php">Members List</a></td>
		
		<td class="vbmenu_control"><a href="http://www.phpbuilder.com/board/calendar.php">Calendar</a></td>
		
			
				
				<td class="vbmenu_control"><a href="http://www.phpbuilder.com/board/search.php?do=getdaily" accesskey="2">Today's Posts</a></td>
				
				<td id="navbar_search" class="vbmenu_control"><a href="http://www.phpbuilder.com/board/search.php" accesskey="4" rel="nofollow">Search</a> </td>
			
			
		
		
		
		</tr>
	</tbody></table>
</div>
<!-- / nav buttons bar -->

<br>






<!-- NAVBAR POPUP MENUS -->

	
	<!-- header quick search form -->
	<div class="vbmenu_popup" id="navbar_search_menu" style="display: none;" align="left">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead">Search Forums</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="nohilite">
				<form action="search.php?do=process" method="post">

					<input name="do" value="process" type="hidden">
					<input name="quicksearch" value="1" type="hidden">
					<input name="childforums" value="1" type="hidden">
					<input name="exactname" value="1" type="hidden">
					<input name="s" value="" type="hidden">
					<input name="securitytoken" value="guest" type="hidden">
					<div><input class="bginput" name="query" size="25" tabindex="1001" type="text"><input class="button" value="Go" tabindex="1004" type="submit"></div>
					<div style="margin-top: 6px;">
						<label for="rb_nb_sp0"><input name="showposts" value="0" id="rb_nb_sp0" tabindex="1002" checked="checked" type="radio">Show Threads</label>
						&nbsp;
						<label for="rb_nb_sp1"><input name="showposts" value="1" id="rb_nb_sp1" tabindex="1003" type="radio">Show Posts</label>
					</div>
				</form>
			</td>
		</tr>
		
		<tr>
			<td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/tags.php" rel="nofollow">Tag Search</a></td>
		</tr>
		
		<tr>
			<td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/search.php" accesskey="4" rel="nofollow">Advanced Search</a></td>
		</tr>
		
		</tbody></table>
	</div>
	<!-- / header quick search form -->
	

	
<!-- / NAVBAR POPUP MENUS -->

<!-- PAGENAV POPUP -->
	<div class="vbmenu_popup" id="pagenav_menu" style="display: none;">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead" nowrap="nowrap">Go to Page...</td>
		</tr>
		<tr>
			<td class="vbmenu_option" title="nohilite">
			<form action="index.php" method="get" onsubmit="return this.gotopage()" id="pagenav_form">
				<input class="bginput" id="pagenav_itxt" style="font-size: 11px;" size="4" type="text">
				<input class="button" id="pagenav_ibtn" value="Go" type="button">
			</form>
			</td>
		</tr>
		</tbody></table>
	</div>
<!-- / PAGENAV POPUP -->








<a name="poststop" id="poststop"></a>

<!-- controls above postbits -->
<table style="margin-bottom: 3px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	
		<td class="smallfont"><a href="http://www.phpbuilder.com/board/newreply.php?do=newreply&amp;noquote=1&amp;p=10846995" rel="nofollow"><img title="Reply" src="showthread_files/reply.gif" alt="Reply" border="0"></a></td>
	
	
</tr>
</tbody></table>
<!-- / controls above postbits -->

<!-- toolbar -->
<table class="tborder" style="border-bottom-width: 0px;" align="center" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody><tr>
	<td class="tcat" width="100%">
		<div class="smallfont">
		
		&nbsp;
		</div>
	</td>
	<td style="cursor: pointer;" class="vbmenu_control" id="threadtools" nowrap="nowrap">
		<a href="http://www.phpbuilder.com/board/showthread.php?t=10350156&amp;nojs=1#goto_threadtools">Thread Tools</a>
		<script type="text/javascript"> vbmenu_register("threadtools"); </script> <img alt="" title="" src="showthread_files/menu_open.gif" border="0">
	</td>
	
	
		<td style="cursor: pointer;" class="vbmenu_control" id="threadrating" nowrap="nowrap">
			<a href="http://www.phpbuilder.com/board/showthread.php?t=10350156&amp;nojs=1#goto_threadrating"><span id="threadrating_current">Rate Thread</span></a>
			<script type="text/javascript"> vbmenu_register("threadrating"); </script> <img alt="" title="" src="showthread_files/menu_open.gif" border="0">
		</td>
	
	
	<td style="cursor: pointer;" class="vbmenu_control" id="displaymodes" nowrap="nowrap">
		<a href="http://www.phpbuilder.com/board/showthread.php?t=10350156&amp;nojs=1#goto_displaymodes">Display Modes</a>
		<script type="text/javascript"> vbmenu_register("displaymodes"); </script> <img alt="" title="" src="showthread_files/menu_open.gif" border="0">
	</td>
	

	

</tr>
</tbody></table>
<!-- / toolbar -->



<!-- end content table -->

		</div>
	</div>
</div>

<!-- / close content container -->
<!-- / end content table -->





<div id="posts"><!-- post #10846995 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="width: 100%; text-align: left;">
		<div style="padding: 0px 10px;" align="left">

	<div id="edit10846995" style="padding: 0px 0px 6px;">
	<!-- this is not the last post shown on the page -->



<table id="post10846995" class="tborder" align="center" border="0" cellpadding="6" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="border-style: solid none solid solid; border-color: rgb(255, 255, 255) -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255); border-width: 1px 0px 1px 1px; font-weight: normal;">
			<!-- status icon and date -->
			<a name="post10846995"><img title="Old" class="inlineimg" src="showthread_files/post_old.gif" alt="Old" border="0"></a>
			01-20-2008, 11:35 AM
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="border-style: solid solid solid none; border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgb(255, 255, 255) -moz-use-text-color; border-width: 1px 1px 1px 0px; font-weight: normal;" align="right">
			&nbsp;
			#<a href="http://www.phpbuilder.com/board/showpost.php?p=10846995&amp;postcount=1" target="new" rel="nofollow" id="postcount10846995" name="1"><strong>1</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-style: none solid; border-color: -moz-use-text-color rgb(255, 255, 255); border-width: 0px 1px;" width="175">

			<div id="postmenu_10846995">
				
				<a class="bigusername" href="http://www.phpbuilder.com/board/member.php?u=181188">David P</a>
				<script type="text/javascript"> vbmenu_register("postmenu_10846995", true); </script>
				
			</div>

			<div class="smallfont">Member</div>
			
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>Join Date: Aug 2006</div>
				<div>Location: Montgomery, AL</div>
				
				<div>
					Posts: 48
				</div>
				
				
				
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_10846995" style="border-right: 1px solid rgb(255, 255, 255);">
	
		
		
			<!-- icon and title -->
			<div class="smallfont">
				
				<strong>Trying to use Fckeditor in form</strong>
			</div>
			<hr style="color: rgb(255, 255, 255); background-color: rgb(255, 255, 255);" size="1">
			<!-- / icon and title -->
		

		<!-- message -->
		<div id="post_message_10846995">ANyone here familiar with fckeditor?<br>
<br>I already had fckeditor installed on my server and running fine.
But I redesigned my website and but using the same code as before only
now the editor does not show up and I am getting this javascript error
message:<br>
<br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">PHP Code:</div>
	<div class="alt2" dir="ltr" style="border: 1px inset ; margin: 0px; padding: 6px; overflow: auto; width: 840px; height: 114px; text-align: left;">
		<code style="white-space: nowrap;">
			<!-- php buffer start --><code><font color="#000000">
<font color="#0000bb">Line</font><font color="#007700">: </font><font color="#0000bb">91
<br>Char</font><font color="#007700">: </font><font color="#0000bb">292
<br>Error</font><font color="#007700">: </font><font color="#dd0000">'undefined' </font><font color="#0000bb">is null </font><font color="#007700">or </font><font color="#0000bb">not an object
<br>Code</font><font color="#007700">: </font><font color="#0000bb">0
<br>URL</font><font color="#007700">: </font><font color="#0000bb">http</font><font color="#007700">:</font><font color="#ff8000">//www.mysite.com/fckeditor/editor/fckeditor.html?
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#0000bb">InstanceName</font><font color="#007700">=</font><font color="#0000bb">content</font><font color="#007700">&amp;</font><font color="#0000bb">MyToolbar 
<br></font>
</font>
</code><!-- php buffer end -->
		</code>
	</div>
</div>I have this on my form:<br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">PHP Code:</div>
	<div class="alt2" dir="ltr" style="border: 1px inset ; margin: 0px; padding: 6px; overflow: auto; width: 840px; height: 146px; text-align: left;">
		<code style="white-space: nowrap;">
			<!-- php buffer start --><code><font color="#000000">
<font color="#0000bb">&lt;?php
<br>$oFCKeditor</font><font color="#007700">-&gt;</font><font color="#0000bb">BasePath </font><font color="#007700">= </font><font color="#dd0000">'/fckeditor/' </font><font color="#007700">;
<br></font><font color="#0000bb">$oFCKeditor </font><font color="#007700">= new </font><font color="#0000bb">FCKeditor</font><font color="#007700">(</font><font color="#dd0000">'content'</font><font color="#007700">) ;
<br></font><font color="#0000bb">$oFCKeditor</font><font color="#007700">-&gt;</font><font color="#0000bb">ToolbarSet </font><font color="#007700">= </font><font color="#dd0000">'MyToolbar' </font><font color="#007700">;
<br></font><font color="#0000bb">$oFCKeditor</font><font color="#007700">-&gt;</font><font color="#0000bb">Height </font><font color="#007700">= </font><font color="#dd0000">'400' </font><font color="#007700">;
<br></font><font color="#0000bb">$oFCKeditor</font><font color="#007700">-&gt;</font><font color="#0000bb">Value </font><font color="#007700">= </font><font color="#0000bb">$content </font><font color="#007700">;
<br></font><font color="#0000bb">$oFCKeditor</font><font color="#007700">-&gt;</font><font color="#0000bb">Create</font><font color="#007700">() ;
<br></font><font color="#0000bb">?&gt;</font>
</font>
</code><!-- php buffer end -->
		</code>
	</div>
</div>And this on top of my page above the html:<br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">PHP Code:</div>
	<div class="alt2" dir="ltr" style="border: 1px inset ; margin: 0px; padding: 6px; overflow: auto; width: 840px; height: 34px; text-align: left;">
		<code style="white-space: nowrap;">
			<!-- php buffer start --><code><font color="#000000">
<font color="#0000bb"></font><font color="#007700">include(</font><font color="#dd0000">'fckeditor/fckeditor.php'</font><font color="#007700">); 
<br></font><font color="#0000bb"></font>
</font>
</code><!-- php buffer end -->
		</code>
	</div>
</div><br>
This is the toolbar I created in the fckconfig.js file (no comma after last row. I also cleared my browser's cache.):<br>
<div style="margin: 5px 20px 20px;">
	<div class="smallfont" style="margin-bottom: 2px;">PHP Code:</div>
	<div class="alt2" dir="ltr" style="border: 1px inset ; margin: 0px; padding: 6px; overflow: auto; width: 840px; height: 130px; text-align: left;">
		<code style="white-space: nowrap;">
			<!-- php buffer start --><code><font color="#000000">
<font color="#0000bb">FCKConfig</font><font color="#007700">.</font><font color="#0000bb">ToolbarSets</font><font color="#007700">[</font><font color="#dd0000">"MyToolbarNew"</font><font color="#007700">] = [
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[</font><font color="#dd0000">'Cut'</font><font color="#007700">,</font><font color="#dd0000">'Copy'</font><font color="#007700">],
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[</font><font color="#dd0000">'Undo'</font><font color="#007700">,</font><font color="#dd0000">'Redo'</font><font color="#007700">,</font><font color="#dd0000">'-'</font><font color="#007700">,</font><font color="#dd0000">'Bold'</font><font color="#007700">,</font><font color="#dd0000">'Italic'</font><font color="#007700">,</font><font color="#dd0000">'Underline'</font><font color="#007700">],
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#dd0000">'/'</font><font color="#007700">,
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[</font><font color="#dd0000">'OrderedList'</font><font color="#007700">,</font><font color="#dd0000">'UnorderedList'</font><font color="#007700">,</font><font color="#dd0000">'-'</font><font color="#007700">,</font><font color="#dd0000">'Outdent'</font><font color="#007700">,</font><font color="#dd0000">'Indent'</font><font color="#007700">],
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[</font><font color="#dd0000">'Link'</font><font color="#007700">,</font><font color="#dd0000">'Unlink'</font><font color="#007700">,</font><font color="#dd0000">'-'</font><font color="#007700">,</font><font color="#dd0000">'SpellCheck'</font><font color="#007700">]
<br>] ; 
<br></font><font color="#0000bb"></font>
</font>
</code><!-- php buffer end -->
		</code>
	</div>
</div><br>
Any ideas as to what could be the problem?</div>
		<!-- / message -->

		

		

		

		

		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-style: none solid solid; border-color: -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255); border-width: 0px 1px 1px;">
		<img title="David P is offline" class="inlineimg" src="showthread_files/user_offline.gif" alt="David P is offline" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-style: none solid solid none; border-color: -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255) -moz-use-text-color; border-width: 0px 1px 1px 0px;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.phpbuilder.com/board/newreply.php?do=newreply&amp;p=10846995" rel="nofollow"><img title="Reply With Quote" src="showthread_files/quote.gif" alt="Reply With Quote" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 10846995 popup menu -->
<div class="vbmenu_popup" id="postmenu_10846995_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">David P</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/member.php?u=181188">View Public Profile</a></td></tr>
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://www.freehelpwanted.com/">Visit David P's homepage!</a></td></tr>
	
	
		<tr><td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/search.php?do=finduser&amp;u=181188" rel="nofollow">Find More Posts by David P</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 10846995 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #10846995 --><!-- post #10847184 -->

	<!-- open content container -->

<div align="center">
	<div class="page" style="width: 100%; text-align: left;">
		<div style="padding: 0px 10px;" align="left">

	<div id="edit10847184" style="padding: 0px 0px 6px;">
	



<table id="post10847184" class="tborder" align="center" border="0" cellpadding="6" cellspacing="0" width="100%">
<tbody><tr>
	
		<td class="thead" style="border-style: solid none solid solid; border-color: rgb(255, 255, 255) -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255); border-width: 1px 0px 1px 1px; font-weight: normal;">
			<!-- status icon and date -->
			<a name="post10847184"><img title="Old" class="inlineimg" src="showthread_files/post_old.gif" alt="Old" border="0"></a>
			01-21-2008, 06:46 PM
			
			<!-- / status icon and date -->
		</td>
		<td class="thead" style="border-style: solid solid solid none; border-color: rgb(255, 255, 255) rgb(255, 255, 255) rgb(255, 255, 255) -moz-use-text-color; border-width: 1px 1px 1px 0px; font-weight: normal;" align="right">
			&nbsp;
			#<a href="http://www.phpbuilder.com/board/showpost.php?p=10847184&amp;postcount=2" target="new" rel="nofollow" id="postcount10847184" name="2"><strong>2</strong></a>
			
		</td>
	
</tr>
<tr valign="top">
	<td class="alt2" style="border-style: none solid; border-color: -moz-use-text-color rgb(255, 255, 255); border-width: 0px 1px;" width="175">

			<div id="postmenu_10847184">
				
				<a class="bigusername" href="http://www.phpbuilder.com/board/member.php?u=164438">gardnc</a>
				<script type="text/javascript"> vbmenu_register("postmenu_10847184", true); </script>
				
			</div>

			<div class="smallfont">Blueridger</div>
			
			

			

			<div class="smallfont">
				&nbsp;<br>
				<div>Join Date: Nov 2004</div>
				<div>Location: Sylva, NC USA</div>
				
				<div>
					Posts: 201
				</div>
				
				
				
				
				<div>    </div>
			</div>

	</td>
	
	<td class="alt1" id="td_post_10847184" style="border-right: 1px solid rgb(255, 255, 255);">
	
		
		

		<!-- message -->
		<div id="post_message_10847184">I
use the editor frequently and have found caching of the javascript to
be a real pain when I make changes. Clearing my browser cache and
firewall (squid) cache generally repairs it.<br>
<br>
{Edited - <br>
You have changed the toolbar set name, but you still call the old one<br>
<br>
$oFCKeditor-&gt;ToolbarSet = 'MyToolbar' ;<br>
<br>
and<br>
<br>
FCKConfig.ToolbarSets["MyToolbarNew"] = [ <br>
<br>
]<br>
<br>
Larry</div>
		<!-- / message -->

		

		

		

		

		
		<!-- edit note -->
			<div class="smallfont">
				<hr style="color: rgb(255, 255, 255); background-color: rgb(255, 255, 255);" size="1">
				<em>
					
						Last edited by gardnc; 01-21-2008 at <span class="time">06:50 PM</span>.
					
					
				</em>
			</div>
		<!-- / edit note -->
		

	</td>
</tr>
<tr>
	<td class="alt2" style="border-style: none solid solid; border-color: -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255); border-width: 0px 1px 1px;">
		<img title="gardnc is offline" class="inlineimg" src="showthread_files/user_offline.gif" alt="gardnc is offline" border="0">


		
		
		
		
		&nbsp;
	</td>
	
	<td class="alt1" style="border-style: none solid solid none; border-color: -moz-use-text-color rgb(255, 255, 255) rgb(255, 255, 255) -moz-use-text-color; border-width: 0px 1px 1px 0px;" align="right">
	
		<!-- controls -->
		
		
		
			<a href="http://www.phpbuilder.com/board/newreply.php?do=newreply&amp;p=10847184" rel="nofollow"><img title="Reply With Quote" src="showthread_files/quote.gif" alt="Reply With Quote" border="0"></a>
		
		
		
		
		
		
		
			
		
		
		<!-- / controls -->
	</td>
</tr>
</tbody></table>


<!-- post 10847184 popup menu -->
<div class="vbmenu_popup" id="postmenu_10847184_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">gardnc</td>
	</tr>
	
		<tr><td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/member.php?u=164438">View Public Profile</a></td></tr>
	
	
	
	
		<tr><td class="vbmenu_option"><a href="http://jacksoncounty-nc.com/">Visit gardnc's homepage!</a></td></tr>
	
	
		<tr><td class="vbmenu_option"><a href="http://www.phpbuilder.com/board/search.php?do=finduser&amp;u=164438" rel="nofollow">Find More Posts by gardnc</a></td></tr>
	
	
	
	</tbody></table>
</div>
<!-- / post 10847184 popup menu -->


	</div>
	
		</div>
	</div>
</div>

<!-- / close content container -->

<!-- / post #10847184 --><div id="lastpost"></div></div>

<!-- start content table -->
<!-- open content container -->

<div align="center">
	<div class="page" style="width: 100%; text-align: left;">
		<div style="padding: 0px 10px;" align="left">

<!-- / start content table -->

<!-- controls below postbits -->
<table style="margin-top: -3px;" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="top">
	
		<td class="smallfont"><a href="http://www.phpbuilder.com/board/newreply.php?do=newreply&amp;noquote=1&amp;p=10847184" rel="nofollow"><img title="Reply" src="showthread_files/reply.gif" alt="Reply" border="0"></a></td>
	
	
</tr>
</tbody></table>
<!-- / controls below postbits -->




<!-- social bookmarking links -->
	<br>
	<table class="tborder" align="center" border="0" cellpadding="6" cellspacing="1" width="100%">
	<tbody><tr>
		<td class="thead">Bookmarks</td>
	</tr>
	<tr>
		<td class="alt2" style="padding-top: 0px;"><div style="clear: both;"></div><ul style="margin: 0px; padding: 0px; list-style-type: none;"><li class="smallfont" style="width: 25%; min-width: 160px; float: left; margin-top: 6px;">
	
		<a href="http://digg.com/submit?phrase=2&amp;url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark"><img title="Submit Thread to Digg" src="showthread_files/bookmarksite_digg.gif" alt="Submit Thread to Digg" class="inlineimg" border="0"></a>
	
	<a href="http://digg.com/submit?phrase=2&amp;url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark" style="text-decoration: none;">Digg</a>
</li><li class="smallfont" style="width: 25%; min-width: 160px; float: left; margin-top: 6px;">
	
		<a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark"><img title="Submit Thread to del.icio.us" src="showthread_files/bookmarksite_delicious.gif" alt="Submit Thread to del.icio.us" class="inlineimg" border="0"></a>
	
	<a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark" style="text-decoration: none;">del.icio.us</a>
</li><li class="smallfont" style="width: 25%; min-width: 160px; float: left; margin-top: 6px;">
	
		<a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark"><img title="Submit Thread to StumbleUpon" src="showthread_files/bookmarksite_stumbleupon.gif" alt="Submit Thread to StumbleUpon" class="inlineimg" border="0"></a>
	
	<a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark" style="text-decoration: none;">StumbleUpon</a>
</li><li class="smallfont" style="width: 25%; min-width: 160px; float: left; margin-top: 6px;">
	
		<a href="http://www.google.com/bookmarks/mark?op=edit&amp;output=popup&amp;bkmk=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark"><img title="Submit Thread to Google" src="showthread_files/bookmarksite_google.gif" alt="Submit Thread to Google" class="inlineimg" border="0"></a>
	
	<a href="http://www.google.com/bookmarks/mark?op=edit&amp;output=popup&amp;bkmk=http%3A%2F%2Fwww.phpbuilder.com%2Fboard%2Fshowthread.php%3Ft%3D10350156&amp;title=Trying+to+use+Fckeditor+in+form" target="socialbookmark" style="text-decoration: none;">Google</a>
</li></ul><div style="clear: both;"></div></td>
	</tr>
	</tbody></table>
<!-- / social bookmarking links -->







<!-- lightbox scripts -->
	<script type="text/javascript" src="showthread_files/vbulletin_lightbox.js"></script>
	<script type="text/javascript">
	<!--
	vBulletin.register_control("vB_Lightbox_Container", "posts", 1);
	//-->
	</script>
<!-- / lightbox scripts -->










<!-- next / previous links -->
	<br>
	<div class="smallfont" align="center">
		<strong>�</strong>
			<a href="http://www.phpbuilder.com/board/showthread.php?t=10350156&amp;goto=nextoldest" rel="nofollow">Previous Thread</a>
			|
			<a href="http://www.phpbuilder.com/board/showthread.php?t=10350156&amp;goto=nextnewest" rel="nofollow">Next Thread</a>
		<strong>�</strong>
	</div>
<!-- / next / previous links -->



<!-- currently active users -->
	<br>
	<table class="tborder" align="center" border="0" cellpadding="6" cellspacing="1" width="100%">
	<tbody><tr>
		<td class="tcat" colspan="2">
			Currently Active Users Viewing This Thread: 1 <span class="normal">(0 members and 1 guests)</span>
		</td>
	</tr>
	<tr>
		<td class="alt1" colspan="2">
			<span class="smallfont">&nbsp;</span>
		</td>
	</tr>
	</tbody></table>
<!-- end currently active users -->



<!-- popup menu contents -->
<br>

<!-- thread tools menu -->
<div class="vbmenu_popup" id="threadtools_menu" style="display: none;">
<form action="postings.php?t=10350156&amp;pollid=" method="post" name="threadadminform">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Thread Tools<a name="goto_threadtools"></a></td>
	</tr>
	<tr>
		<td class="vbmenu_option"><img title="Show Printable Version" class="inlineimg" src="showthread_files/printer.gif" alt="Show Printable Version"> <a href="http://www.phpbuilder.com/board/printthread.php?t=10350156" accesskey="3" rel="nofollow">Show Printable Version</a></td>
	</tr>
	<tr>
		<td class="vbmenu_option"><img title="Email this Page" class="inlineimg" src="showthread_files/sendtofriend.gif" alt="Email this Page"> <a href="http://www.phpbuilder.com/board/sendmessage.php?do=sendtofriend&amp;t=10350156" rel="nofollow">Email this Page</a></td>
	</tr>
	
	
<!-- JP "Thread resolving" -->
	<tr>
		<td class="vbmenu_option"><img title="Resolve" class="inlineimg" src="showthread_files/subscribe.gif" alt="Resolve"> <a href="http://www.phpbuilder.com/board/showthread.php?&amp;t=10350156&amp;is_resolved=1">
Mark Thread Resolved</a></td>
	</tr>
<!--/JP -->
	
	</tbody></table>
</form>
</div>
<!-- / thread tools menu -->

<!-- **************************************************** -->

<!-- thread display mode menu -->
<div class="vbmenu_popup" id="displaymodes_menu" style="display: none;">
	<table border="0" cellpadding="4" cellspacing="1">
	<tbody><tr>
		<td class="thead">Display Modes<a name="goto_displaymodes"></a></td>
	</tr>
	<tr>
	
		<td class="vbmenu_option" title="nohilite"><img title="Linear Mode" class="inlineimg" src="showthread_files/mode_linear.gif" alt="Linear Mode"> <strong>Linear Mode</strong></td>
	
	</tr>
	<tr>
	
		<td class="vbmenu_option"><img title="Hybrid Mode" class="inlineimg" src="showthread_files/mode_hybrid.gif" alt="Hybrid Mode"> <a href="http://www.phpbuilder.com/board/showthread.php?mode=hybrid&amp;t=10350156">Switch to Hybrid Mode</a></td>
	
	</tr>
	<tr>
	
		<td class="vbmenu_option"><img title="Threaded Mode" class="inlineimg" src="showthread_files/mode_threaded.gif" alt="Threaded Mode"> <a href="http://www.phpbuilder.com/board/showthread.php?p=10846995&amp;mode=threaded#post10846995">Switch to Threaded Mode</a></td>
	
	</tr>
	</tbody></table>
</div>
<!-- / thread display mode menu -->

<!-- **************************************************** -->



<!-- **************************************************** -->


	<!-- thread rating menu -->
	<div class="vbmenu_popup" id="threadrating_menu" style="display: none;">
	<form action="threadrate.php?t=10350156" method="post" id="showthread_threadrate_form">
		<table border="0" cellpadding="4" cellspacing="1">
		<tbody><tr>
			<td class="thead">Rate This Thread<a name="goto_threadrating"></a></td>
		</tr>
		
		<tr>
			<td class="vbmenu_option" title="nohilite" id="threadrating_options">

			<div><img title="Excellent" class="inlineimg" src="showthread_files/rating_5.gif" alt="Excellent"><label for="vote5"><input name="vote" id="vote5" value="5" type="radio">Excellent</label></div>
			<div><img title="Good" class="inlineimg" src="showthread_files/rating_4.gif" alt="Good"><label for="vote4"><input name="vote" id="vote4" value="4" type="radio">Good</label></div>
			<div><img title="Average" class="inlineimg" src="showthread_files/rating_3.gif" alt="Average"><label for="vote3"><input name="vote" id="vote3" value="3" type="radio">Average</label></div>
			<div><img title="Bad" class="inlineimg" src="showthread_files/rating_2.gif" alt="Bad"><label for="vote2"><input name="vote" id="vote2" value="2" type="radio">Bad</label></div>
			<div><img title="Terrible" class="inlineimg" src="showthread_files/rating_1.gif" alt="Terrible"><label for="vote1"><input name="vote" id="vote1" value="1" type="radio">Terrible</label></div>
			</td>
		</tr>
		<tr id="threadrating_submit">
			<td class="vbmenu_option" title="nohilite" align="center">
				<input name="s" value="" type="hidden">
				<input name="securitytoken" value="guest" type="hidden">
				<input name="t" value="10350156" type="hidden">
				<input name="pp" value="15" type="hidden">
				<input name="page" value="1" type="hidden">
				<input value="Vote Now" class="button" type="button">
			</td>
		</tr>
		
		</tbody></table>
	</form>
	</div>
	<!-- / thread rating menu -->

	<script type="text/javascript" src="showthread_files/vbulletin_ajax_threadrate.js"></script>
	<script type="text/javascript">
	<!--
	vB_AJAX_ThreadRate_Init('showthread_threadrate_form');
	var threadid = 10350156;
	//-->
	</script>


<!-- / popup menu contents -->


<!-- forum rules and admin links -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr valign="bottom">
	<td valign="top" width="100%">
		<table class="tborder" border="0" cellpadding="6" cellspacing="1" width="210">
<thead>
<tr>
	<td class="thead">
		<a style="float: right;" href="#top" onclick="return toggle_collapse('forumrules')"><img id="collapseimg_forumrules" src="showthread_files/collapse_thead.gif" alt="" border="0"></a>
		Posting Rules
	</td>
</tr>
</thead>
<tbody id="collapseobj_forumrules" style="">
<tr>
	<td class="alt1" nowrap="nowrap"><div class="smallfont">
		
		<div>You <strong>may not</strong> post new threads</div>
		<div>You <strong>may not</strong> post replies</div>
		<div>You <strong>may not</strong> post attachments</div>
		<div>You <strong>may not</strong> edit your posts</div>
		<hr>
		
		<div><a href="http://www.phpbuilder.com/board/misc.php?do=bbcode" target="_blank">BB code</a> is <strong>On</strong></div>
		<div><a href="http://www.phpbuilder.com/board/misc.php?do=showsmilies" target="_blank">Smilies</a> are <strong>On</strong></div>
		<div><a href="http://www.phpbuilder.com/board/misc.php?do=bbcode#imgcode" target="_blank">[IMG]</a> code is <strong>Off</strong></div>
		<div>HTML code is <strong>Off</strong></div>
	</div></td>
</tr>
</tbody>
</table>
	</td>
	<td class="smallfont" align="right">
		<table border="0" cellpadding="0" cellspacing="0">
		
		<tbody><tr>
			<td>
			<div class="smallfont" style="text-align: left; white-space: nowrap;">
	<form action="forumdisplay.php" method="get">
	<input name="s" value="" type="hidden">
	<input name="daysprune" value="" type="hidden">
	<strong>Forum Jump</strong><br>
	<select name="f" onchange="this.form.submit();">
		<optgroup label="Site Areas">
			<option value="cp">User Control Panel</option>
			<option value="pm">Private Messages</option>
			<option value="subs">Subscriptions</option>
			<option value="wol">Who's Online</option>
			<option value="search">Search Forums</option>
			<option value="home">Forums Home</option>
		</optgroup>
		
		<optgroup label="Forums">
		<option value="1" class="fjdpth0"> PHP Help</option>
<option value="2" class="fjsel" selected="selected">&nbsp; &nbsp;  General Help</option>
<option value="3" class="fjdpth1">&nbsp; &nbsp;  Newbies</option>
<option value="6" class="fjdpth1">&nbsp; &nbsp;  Database</option>
<option value="7" class="fjdpth1">&nbsp; &nbsp;  Install</option>
<option value="10" class="fjdpth1">&nbsp; &nbsp;  Coding</option>
<option value="24" class="fjdpth1">&nbsp; &nbsp;  Code Critique</option>
<option value="25" class="fjdpth1">&nbsp; &nbsp;  Upgrading PHP</option>
<option value="4" class="fjdpth0"> Discussion</option>
<option value="11" class="fjdpth1">&nbsp; &nbsp;  Echo Lounge</option>
<option value="23" class="fjdpth1">&nbsp; &nbsp;  Mods' Bods</option>
<option value="5" class="fjdpth1">&nbsp; &nbsp;  Feedback</option>
<option value="17" class="fjdpth0"> Tools</option>
<option value="18" class="fjdpth1">&nbsp; &nbsp;  Zend Studio</option>
<option value="19" class="fjdpth1">&nbsp; &nbsp;  Dreamweaver</option>
<option value="21" class="fjdpth1">&nbsp; &nbsp;  Misc Tools</option>
<option value="8" class="fjdpth0"> Misc Help</option>
<option value="27" class="fjdpth1">&nbsp; &nbsp;  ClientSide Technologies</option>
<option value="9" class="fjdpth1">&nbsp; &nbsp;  Windows Help</option>
<option value="12" class="fjdpth1">&nbsp; &nbsp;  Linux/Unix Help</option>
<option value="26" class="fjdpth0"> PHPBuilder Forum Archives</option>

		</optgroup>
		
	</select><input class="button" value="Go" type="submit">
	</form>
</div>
			</td>
		</tr>
		</tbody></table>
	</td>
</tr>
</tbody></table>
<!-- /forum rules and admin links -->

<br>





<br>
<div class="smallfont" align="center">All times are GMT -4. The time now is <span class="time">01:33 AM</span>.</div>
<br>


		</div>
	</div>
</div>

<!-- / close content container -->
<!-- /content area table -->
</td><td id="sky" valign="top"><br>
<!-- OAS AD '125x800' begin ------>
<script language="JavaScript">
<!--
 OAS_AD('125x800');
//-->
</script><table style="border: 1px solid rgb(0, 0, 0);" bgcolor="#ffffff" border="0" cellpadding="3" cellspacing="0" width="125">
<tbody><tr height="20"><td style="border-bottom: 1px solid rgb(0, 0, 0); font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none;" colspan="2" bgcolor="#81f7f3">
<b>Get CXO Tools &amp; Whitepapers:<a href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/890480245/125x800/OasDefault/IBM_CXO_ResCen_GEMS_1i/one.gif/37346331386162333439326135306130?IBMSC/Door/31521"><b><br>Visit the IBM CXO Resource Center</b></a>
</b></td></tr>

<tr>
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" valign="top">
<font color="#f88800">&#8226;</font></td> 
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" height="" valign="top">
<a href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/890480245/125x800/OasDefault/IBM_CXO_ResCen_GEMS_1i/two.gif/37346331386162333439326135306130?IBMSC/Link/38950">
<b>Whitepaper:</b>
The 2008 CEO Study-Implications for the CIO</a> <br>
Learn about five key insights from the study and what they mean for the CIO as change agent. </td></tr>

<tr>
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" valign="top">
<font color="#f88800">&#8226;</font></td> 
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" height="" valign="top">
<a href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/890480245/125x800/OasDefault/IBM_CXO_ResCen_GEMS_1i/three.gif/37346331386162333439326135306130?IBMSC/Link/38948">
<b>Whitepaper:</b>
The Outsourcing Decision for a Globally Integrated Enterprise</a> <br>
Globalization and advances in technology have changed the way business
gets done. Learn how IBM helps turn outsourcing into opportunity.
</td></tr>

<tr>
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" valign="top">
<font color="#f88800">&#8226;</font></td> 
<td style="font-family: Arial,Helvetica,sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; padding-top: 5px;" height="" valign="top">
<a href="http://mjxads.internet.com/RealMedia/ads/click_lx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index/890480245/125x800/OasDefault/IBM_CXO_ResCen_GEMS_1i/four.gif/37346331386162333439326135306130?IBMSC/Link/38937">
<b>Case Study:</b>
SOA for Electronics: Rewiring for Global Integration</a> <br>Boost
collaboration within your business ecosystem to meet global electronics
industry competition and short production lifecycles with service
oriented architecture (SOA).
</td></tr>
</tbody></table>
<img src="showthread_files/37346331386162333439326135306130_002.gif" height="1" width="1">
<noscript><A
HREF="http://63.236.18.118/RealMedia/ads/click_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!125x800">
<IMG
SRC="http://63.236.18.118/RealMedia/ads/adstream_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!125x800"
border=0>
</A>
</noscript>
<!------ OAS AD '125x800' end ------>

</td></tr></tbody></table>
<!--/closeTableMain-->
<form action="index.php" method="get">

<table class="page" align="center" border="0" cellpadding="6" cellspacing="0" width="100%">
<tbody><tr>
	
	
	<td class="tfoot" align="right" width="100%">
		<div class="smallfont">
			<strong>
				<a href="http://www.phpbuilder.com/board/sendmessage.php" rel="nofollow" accesskey="9">Contact Us</a> -
				<a href="http://www.phpbuilder.com/">PHP Builder: manuals, content management systems, scripts, classes and more.</a> -
				
				
				<a href="http://www.phpbuilder.com/board/archive/index.php">Archive</a> -
				
				
				<a href="#top" onclick="self.scrollTo(0, 0); return false;">Top</a>
			</strong>
		</div>
	</td>
</tr>
</tbody></table>

<br>
<center id="bot">
<!-- OAS AD '468x60-2' begin ------>
<script language="JavaScript">
<!--
 OAS_AD('468x60-2');
//-->
</script><a href="http://mjxads.internet.com/RealMedia/ads/click_lx.cgi/intm/webdev/www.phpbuilder.com/board/showthread.php/index/1687618624/468x60-2/OasDefault/House_UGCX_1y/ugcx_750x100g.jpg/37346331386162333439326135306130" target="_top"><img src="showthread_files/ugcx_750x100g.jpg" alt="" border="0"></a><img src="showthread_files/37346331386162333439326135306130_003.gif" border="0" height="1" width="1">
<noscript><A
HREF="http://63.236.18.118/RealMedia/ads/click_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!468x60-2">
<IMG
SRC="http://63.236.18.118/RealMedia/ads/adstream_nx.ads/intm/webdev/www.phpbuilder.com/board/showthread.php/index@468x60-1,468x60-2,125x125-1,sitetext-1,cp1,cp2,cp3,cp4,cp5,cp6,cp7,cp8,cp9,cp10,cp11,cp12,cp13,cp14,flex,house_ribbon,ciu,125x800,accessunit,marketplace01,stickypost!468x60-2"
border=0>
</A>
</noscript>
<!------ OAS AD '468x60-2' end ------>

</center>

<!-- Google Analytics -->
<script src="showthread_files/ga.js" type="text/javascript">
</script>

<script type="text/javascript">
<!--     hide from no js browsers 
delete Array.prototype.itemValidation;
delete Array.prototype.isArray;
var superTracker = _gat._getTracker("UA-2838492-1");
superTracker._initData();
superTracker._trackPageview();
-->
</script>
<script type="text/javascript">
  var site_name =  location.hostname;
  if ( site_name.indexOf("www.") != 0 ) {
    site_name  = "www."+site_name ;
  }
  document.write("<SCR" + "IPT LANGUAGE='JavaScript1.2' SRC='/icom_includes/footers/sites/"+ site_name +".js' TYPE='text/javascript'><\/SCR" + "IPT>");
-->
</script><script language="JavaScript1.2" src="showthread_files/www.js" type="text/javascript"></script>
<script type="text/javascript">
<!--     hide from no js browsers 
  pageTracker._initData();
  pageTracker._trackPageview();
  // -->
</script>
<!-- End Google Analytics -->

<br clear="all"><center><table border="0" cellpadding="5" cellspacing="0">
                <tbody><tr> 
                  
                    <td align="center">
                    <font face="verdana, arial, helvetica" size="-2">
                    <a href="http://www.jupitermedia.com/corporate/privacy/aup.html" id="aup">Acceptable Use Policy</a><br><br>
                  <img title="JupiterOnlineMedia" src="showthread_files/onlinemedia_footer.gif" alt="JupiterOnlineMedia" border="0" height="17" hspace="0" width="175">
                      </font><p align="center">

</p><table border="0"><tbody><tr><td class="foot_alt" align="center">
                      <a href="http://www.internet.com/" class="foot" target="_top"><img title="internet.com" src="showthread_files/icom_foot.gif" alt="internet.com" align="absmiddle" border="0" height="15" hspace="0" width="97"></a><img src="showthread_files/ruledivide_foot.gif" alt="" align="absmiddle" border="0" height="25" hspace="10" width="1"><a href="http://www.earthweb.com/" target="_top"><img title="earthweb.com" src="showthread_files/earthweb_foot2.gif" alt="earthweb.com" align="absmiddle" border="0" height="12" hspace="5" width="93"></a><img src="showthread_files/ruledivide_foot.gif" alt="" align="absmiddle" border="0" height="25" hspace="10" width="1"><a href="http://www.devx.com/" target="_top"><img title="Devx.com" src="showthread_files/devx_foot2.gif" alt="Devx.com" align="absmiddle" border="0" height="30" hspace="0" width="35"></a><img src="showthread_files/ruledivide_foot.gif" alt="" align="absmiddle" border="0" height="25" hspace="10" width="1"><a href="http://www.mediabistro.com/" target="_top"><img title="mediabistro.com" src="showthread_files/mediabistro_foot.gif" alt="mediabistro.com" align="absmiddle" border="0" height="48" hspace="0" width="100"></a><img src="showthread_files/ruledivide_foot.gif" alt="" align="absmiddle" border="0" height="25" hspace="10" width="1"><a href="http://www.graphics.com/" target="_top"><img title="Graphics.com" src="showthread_files/grcom_foot.gif" alt="Graphics.com" align="absmiddle" border="0" height="18" hspace="0" width="97"></a></td></tr></tbody></table>
<font face="verdana, arial, helvetica" size="-2">                      </font><p><font face="verdana, arial, helvetica" size="-2"> 
                      <font face="verdana, arial, helvetica" size="-2"><b>Search:</b> 
                      <input name="IC_Summary" value="1" type="hidden">
                      <input name="IC_StartNumber" value="0" type="hidden">
                      <input name="IC_BatchSize" value="10" type="hidden">
                      <input name="IC_RelevanceThreshold" value="50" type="hidden">
                      <input name="IC_QueryDatabase" value="all" type="hidden">
                      <input name="IC_QueryText" size="45" type="text">
                      <input value="Find" name="SUBMIT" src="showthread_files/but_find.gif" align="absmiddle" border="0" height="20" type="image" width="41">
                      </font>
                      </font></p><p>
<font face="verdana, arial, helvetica" size="-2">                      <font face="verdana, arial, helvetica" size="-2">
                     <a href="http://www.jupitermedia.com/" class="foot"> Jupitermedia Corporation</a> has two divisions: <a href="http://www.jupiterimages.com/" class="foot">Jupiterimages</a> and <a href="http://www.jupitermedia.com/onlinemedia.html" class="foot">JupiterOnlineMedia</a></font></font></p><p><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"> <font face="verdana, arial, helvetica" size="-2"><a href="http://www.jupitermedia.com/corporate/" class="foot" target="_top">Jupitermedia Corporate  Info</a> </font></font></font></p><p>
<font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2">                      </font></font></font></p><div class="foot">
<!--Copyright 2006-->
<script type="text/javascript">
document.write(' Copyright ');
var now = new Date();
document.write(  + now.getFullYear());
document.write(' Jupitermedia Corporation All Rights Reserved.');
</script><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"> Copyright 2008 Jupitermedia Corporation All Rights Reserved.
<noscript> Copyright 2007 Jupitermedia Corporation All Rights Reserved.</noscript><br>
                        <a href="http://www.jupitermedia.com/corporate/legal.html" class="foot" target="_top">Legal 
                        Notices</a>, <a href="http://www.jupitermedia.com/corporate/reprints.html#Licensing1" class="foot" target="_top">Licensing</a>, 
                        <a href="http://www.jupitermedia.com/corporate/reprints.html#Reprints1" class="foot" target="_top">Reprints</a>, 
                        &amp; <a href="http://www.jupitermedia.com/corporate/reprints.html#Permissions" class="foot" target="_top">Permissions</a>, 
                        <a href="http://www.jupitermedia.com/corporate/privacy/privacypolicy.html" class="foot" target="_top">Privacy 
                        Policy</a>. </font>
                      </font></font><p><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"> <font face="verdana, arial, helvetica" size="-2"><a href="http://www.webhost.thelist.com/" class="foot" target="_top">Web Hosting</a> | <a href="http://e-newsletters.internet.com/" class="foot">Newsletters</a> 
                         | <a href="http://jobs.internet.com/" class="foot" target="_top">Tech 
                        Jobs</a> | <a href="http://www.internetshopper.com/" class="foot" target="_top"> Shopping</a> | <a href="http://e-newsletters.internet.com/announcement_list.html" class="foot" target="_top">E-mail 
                        Offers</a></font></font></font></p></div><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"><font face="verdana, arial, helvetica" size="-2"></font>

                    </font></font></td>
                  
                </tr>
              </tbody></table></center>

<br>
<div align="center">
	<div class="smallfont" align="center">
	<!-- Do not remove this copyright notice -->
	Powered by vBulletin� Version 3.7.2<br>Copyright �2000 - 2008, Jelsoft Enterprises Ltd.
	<!-- Do not remove this copyright notice -->
	</div>

	<div class="smallfont" align="center">
	<!-- Do not remove  or your scheduled tasks will cease to function -->
	
	<!-- Do not remove  or your scheduled tasks will cease to function -->

	
	</div>
</div>






<script type="text/javascript">
<!--
	// Main vBulletin Javascript Initialization
	vBulletin_init();
//-->
</script>

<!-- temp -->
<div style="display: none;">
	<!-- thread rate -->
	
		<!-- thread ratings form here -->
	
	<!-- / thread rate -->
</div>

</form></td></tr></tbody></table></body></html>
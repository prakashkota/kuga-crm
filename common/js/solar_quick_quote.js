
var quick_quote_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.quote_data = (options.quote_data && options.quote_data != '') ? JSON.parse(options.quote_data) : {};
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.subtotal_excGST = 0;
    this.total_incGST = 0;
    if (Object.keys(self.quote_data).length > 0) {
        $('#quote_no').val(self.quote_data.quote_no);
        $('#customer_company_name').val(self.quote_data.customer_company_name);
        $('#customer_name').val(self.quote_data.customer_name);
        $('#customer_contact_no').val(self.quote_data.customer_contact_no);
        $('#customer_address').val(self.quote_data.customer_address);
        $('#customer_postcode').val(self.quote_data.customer_postcode);
        $('#customer_suburb').val(self.quote_data.customer_suburb);
        $('#rep_name').val(self.quote_data.rep_name);
        $('#quote_no').attr('readonly', 'readonly');
        self.total_incGST = self.quote_data.total_incGST;
        $('#total_incGst').val(self.total_incGST);
        $('#sub_total_excGst').val(self.quote_data.total_excGST);
        $('#total_gst').val(self.quote_data.total_gst);
        $('#valid_for').val(self.quote_data.valid_for);
        self.fetch_form_data({'uuid' : self.quote_data.uuid,'form_type' : 'solar_quote'});
    } else if (Object.keys(self.lead_data).length > 0) {
        $('#customer_company_name').val(self.lead_data.customer_company_name);
        $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#customer_contact_no').val(self.lead_data.customer_contact_no);
        $('#customer_address').val(self.lead_data.address);
        $('#customer_postcode').val(self.lead_data.postcode);
        $('#rep_name').val(self.lead_data.full_name);
        $('#valid_for').val('30 day(s)');
    }

    /**$('#save_quote').click(function () {
        self.save_quick_quote();
    });*/

    $('#close_quote').click(function () {
        try{
            window.history.back();
            window.close();
        }catch{
            window.history.back();
        }
    });

    $('#generate_quote_pdf').click(function () {
        self.save_quick_quote();
    });
    
    $('#total_incGst').change(function(){
       var total_incGst = $(this).val();
       var total_excGst = 0;
       var total_gst = 0;
       total_excGst = parseFloat(total_incGst / 1.1).toFixed(2);
       total_gst = parseFloat(total_incGst - total_excGst).toFixed(2);
       $('#sub_total_excGst').val(total_excGst);
       $('#total_gst').val(total_gst);
    });
    
    tinymce.init({
        selector:'#description',
        branding: false,
        menubar: false,
        statusbar: false,
        plugins: "lists,link,fullscreen",
        toolbar: 'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
        mode: "textareas",
        mobile: {
            theme: 'silver'
        },
        height : "300",
        entity_encoding : "raw",
        setup: function (editor) {
            editor.on('init', function () {
                if (Object.keys(self.quote_data).length > 0) {
                    this.setContent(self.quote_data.description);
                }
            });
        },
    });

};

quick_quote_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

quick_quote_manager.prototype.validate_quick_quote = function () {
    var self = this;
    var flag = true;
    var elements = document.getElementById("quick_quote_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

quick_quote_manager.prototype.save_quick_quote = function () {
    var self = this;

    var flag = self.validate_quick_quote();
    if (!flag) {
        toastr["error"]('Please fill the fields marked in red');
        return false;
    }

    if (self.quote_data.uuid == undefined) {
        var uuid = self.create_uuid();
        self.quote_data.uuid = uuid;
    }

    var formData = $('#quick_quote_form').serialize();
    formData += '&quote[uuid]=' + self.quote_data.uuid;
    formData += '&quote[lead_id]=' + self.lead_data.id;
    
    var editor_data = tinyMCE.get('description').getContent().trim();
    editor_data = encodeURIComponent(editor_data);
    formData += '&quote[description]='+ editor_data;

    $.ajax({
        url: base_url + 'admin/lead/save_solar_quick_quote',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('Saving Quote Data, Please Wait....');
            $("#save_quick_quote").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.quote_data.id == undefined) {
                    window.history.pushState(self.quote_data, '', base_url + 'admin/lead/edit_solar_quick_quote/' + self.quote_data.uuid);
                }
                $("#save_quick_quote").removeAttr("disabled");
                var url = base_url + 'admin/lead/solar_quote_pdf_download/' + self.quote_data.uuid + '?view=download';
                window.location.href = url;
            } else {
                toastr["error"](response.status);
                $("#save_quick_quote").removeAttr("disabled");
                var url = base_url + 'admin/lead/solar_quote_pdf_download/' + self.quote_data.uuid + '?view=download';
                window.location.href = url;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


quick_quote_manager.prototype.fetch_form_data = function (data) {
    var self = this;

    $.ajax({
        url: base_url + 'admin/lead/fetch_form_data',
        type: 'get',
        data: data,
        dataType: 'json',
        success: function (response) {
            //response = JSON.parse(response);
            if(response.solar_quote_data){
                //console.log(response.solar_quote_data.description);
                $('#description').val(response.solar_quote_data.description);
                tinymce.get("description").setContent(response.solar_quote_data.description);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
};


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<style>
    
    .calendar_grid-item--yellow{
        border-color: rgba(250,250,210,0.60) !important;
        background-color: rgba(250,250,210,0.60) !important;
        box-shadow: inset 4px 0 0 0 #F4F463 !important;
    }
    
    .calendar_grid-item--grey{
        border-color: rgba(211,211,211,0.24) !important;
        background-color: rgba(211,211,211,0.24) !important;
        box-shadow: inset 4px 0 0 0 #D3D3D3 !important;
    }

    .calendar_grid-item--amber{
        border-color: rgba(255, 191, 0, 0.24) !important;
        background-color: rgba(255, 191, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }
    
    .calendar_grid-item--blue{
        border-color: rgba(49, 122, 226, 0.24) !important;
        background-color: rgba(49, 122, 226, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }
    
    .calendar_grid-item--green{
        border-color: green !important;
        box-shadow: inset 4px 0 0 0 #fff !important;
    }
    
    .calendar_grid-item--red{
        border-color: rgba(255, 192, 203, 0.24) !important;
        background-color: rgba(255, 192, 203, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }
    
    .fc-content{
        padding:10px;
        box-sizing: border-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        padding: 2px 4px 2px 8px;
        height: 100%;
    }

    .kg-page__heading{display:flex;width: 100%;}
    .kg-page__heading_title{
        font-size: 16px;
        font-weight: 500;
        margin-bottom: .5rem;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.2;
        color: inherit;
        padding: 5px 0 15px;
    }
    .kg-page__heading_action_left{margin-left:10px;}
</style>
<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="kg-page__heading">
            <div class="kg-page__heading_title">
                <?php echo (isset($meta_title)) ? $meta_title : 'Manage Activity (Calendar)'; ?>
            </div> 
            <?php if (isset($is_sales_rep) && $is_sales_rep == FALSE) { ?>
                <div class="kg-page__heading_action_left">
                    <select class="form-control" name="userid" id="userid" style="width:150px;">
                        <option value="">Select User</option>
                        <?php foreach ($users as $key => $value) { ?>
                            <option class="<?php if ($value['banned'] == 0.24) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            <?php } ?>
        </div>
        <?php if (isset($is_google_account_connected) && $is_google_account_connected == 'false') { ?>
            <div class="alert alert-primary" style="float:left" role="alert">
                    <i class="fa fa-info-circle"></i>  Connect <a href="<?php echo site_url('admin/settings/gcal'); ?>">Google Calendar</a> in order to sync activity data.
            </div>
        <?php } ?>
        <div class="float-right d-flex">
            <a class="btn btn-default mr-2" href="<?php echo site_url('admin/lead/manage?view=calendar'); ?>">
                <i class="fa fa-calendar"></i> Manage Leads
            </a>
            <a class="btn btn-default" href="<?php echo site_url('admin/activity/manage?view=list'); ?>">
                <i class="fa fa-list"></i>
            </a>
        </div> 
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix" id="calendar">

                    </div>
                </div>
            </div>
        </div>
    </div> 
</div> 
<!-- CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<!-- SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

<script src='<?php echo site_url(); ?>common/js/activity.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script type='text/javascript'>
    var context = {};
    context.view = 'calendar';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    var activity_manager_tool = new activity_manager(context);
    function initMap(){}
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry,drawing&callback=initMap"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>
</body>

</html>

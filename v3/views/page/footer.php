
<!-- COPYRIGHT INFO -->
<div class="container-fluid footer-copyright marg30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="float-left">Maintained By SolarRun</div>
                <div class="float-right">
                    Reach Us:
                    <i class="fa fa-phone"></i> 0123456789 &#xA0; 
                    <i class="fa fa-envelope"></i> test@123.com
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END COPYRIGHT INFO -->
<!-- LOADING MAIN JAVASCRIPT -->
<script src="<?php echo site_url('assets/page/js/jquery-2.2.4.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/page/js/main.js'); ?>"></script>
<script src="<?php echo site_url('assets/page/js/bootstrap.min.js'); ?>"></script>
<script src='https://cdn.rawgit.com/VPenkov/okayNav/master/app/js/jquery.okayNav.js'></script>
</body>

</html>
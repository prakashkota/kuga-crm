<div class="left-nav-block"> 
    <a href="<?php echo site_url(); ?>admin/dashboard" class="dash-logo">
        <img src="<?php echo site_url(); ?>assets/images/logo.svg" alt="logo">
        <br/>
        <p style="font-size:10px; color:#fff; text-align: center; padding:0px;" class="d-none d-sm-block">
            <?php echo $this->session->userdata('full_name'); ?>
        </p>
    </a>
    <div class="head-right">
        <?php if ($this->aauth->is_member('Admin')) { ?>
            <a href="<?php echo site_url(); ?>admin/lead/manage?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>
            <a href="<?php echo site_url(); ?>admin/reports/manage" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="icon-bar-chart"></i></a> 
            <a href="<?php echo site_url(); ?>admin/lead/manage?view=map" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-map"></i></i></a>
            <a href="<?php echo site_url(); ?>admin/tracking/manage" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-map-marker"></i></a>
        <?php } else if ($this->components->is_team_leader()) { ?>
            <?php if ($this->components->is_lead_allocation_team_leader()) { ?> 
                <a href="<?php echo site_url(); ?>admin/lead/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
                <a href="<?php echo site_url(); ?>admin/lead/manage?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>
                <a href="<?php echo site_url(); ?>admin/lead/manage?view=map" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-map"></i></a>
                <a href="<?php echo site_url(); ?>admin/activity/manage?view=calendar" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-calendar"></i></a>
                <a href="<?php echo site_url(); ?>admin/reports/manage" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="icon-bar-chart"></i></a> 
            <?php } else { ?>
                <a href="<?php echo site_url(); ?>admin/lead/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
                <a href="<?php echo site_url(); ?>admin/lead/manage?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>
                <a href="<?php echo site_url(); ?>admin/lead/manage?view=map" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-map"></i></a>
                <a href="<?php echo site_url(); ?>admin/activity/manage?view=calendar" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-calendar"></i></a>
                <a href="<?php echo site_url(); ?>admin/reports/manage" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="icon-bar-chart"></i></a> 
            <?php } ?>
        <?php } else if ($this->components->is_sales_rep()) {
            if($this->session->userdata('profileId') == 2){ ?>
                <a href="<?php echo site_url(); ?>admin/tender/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
                <a href="<?php echo site_url(); ?>admin/tender/manage-tender?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>

            <?php }else {
            ?>
            <a href="<?php echo site_url(); ?>admin/lead/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
            <a href="<?php echo site_url(); ?>admin/lead/manage?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>
            <a href="<?php echo site_url(); ?>admin/lead/manage?view=map" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-map"></i></i></a>
            <?php } if ($this->components->is_lead_allocation_rep()) { ?>
                <a href="<?php echo site_url(); ?>admin/activity/manage?view=calendar" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-calendar"></i></a>
                <a href="<?php echo site_url(); ?>admin/reports/manage" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="icon-bar-chart"></i></a> 
            <?php } else {
            if($this->session->userdata('profileId') == 2) { ?>
                <a href="<?php echo site_url(); ?>admin/tender-lead/activity/manage?view=calendar" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-calendar"></i></i></a>
            <?php } else {
                ?>
                 <a href="<?php echo site_url(); ?>admin/activity/manage?view=calendar" class="add-more text-white" style="font-size: 35px; margin: 15px auto 0;"><i class="fa fa-calendar"></i></i></a>
            <?php } } ?>
        <?php } else if ($this->aauth->is_member('Lead Creator Only')) { ?>
             <a href="<?php echo site_url(); ?>admin/lead/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
        <?php } else if ($this->aauth->is_member('Tender Quotes Users')) { ?> 
                <a href="<?php echo site_url(); ?>admin/tender-lead/add" class="add-more"><img src="<?php echo site_url(); ?>assets/images/plus.svg" alt="plus"></a>
                <a href="<?php echo site_url(); ?>admin/tender-lead/manage?view=pipeline" class="add-more text-white" style="font-size: 35px;  margin: 15px auto 0;" id="list_deals"><i class="fa fa-dollar"></i></a>
        <?php } ?>
        <?php if (!$this->aauth->is_member('Lead Creator Only')) { ?>
            <a href="#" class="menu-toggle-icon" style="font-size: 35px; margin: 15px auto 0;"><img src="<?php echo site_url(); ?>assets/images/menu-burger.svg" alt="menu-icon"></a>
        <?php } ?>
        <?php if(!$this->aauth->is_member('Tender Quotes Users')){ ?>
            <a href="<?php echo site_url(); ?>admin/page/log-issue"  class="sidebar__mapping_tool-icon"><i class="icon-question"></i></a>
        <?php } ?>
        <a href="<?php echo site_url(); ?>admin/logout" class="sidebar__shutdown-icon"><i class="icon-logout"></i></a>
    </div>
</div>
<div class="slide-nav">
    <!--navigation-->
    <ul class="sidebar-menu">
        <?php if ($this->aauth->is_member('Admin')) { ?>
            <li class="treeview <?php if ($this->uri->segment(2) == 'team-leader') { ?> active <?php } ?>" >
                <a href="#"><span><i class="fa fa-user"></i> Team Leader</span> <i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/team-leader/add">Add Team Leader</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-leader/manage/<?php echo GROUP_LATL_ID; ?>">Manage Lead Allocation Team Leader</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-leader/manage/<?php echo GROUP_STL_ID; ?>">Manage Sales Team Leader</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(2) == 'team-representative') { ?> active <?php } ?>" >
                <a href="#"><span><i class="fa fa-users"></i> Team Representatives</span> <i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/add">Add Team Representative</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/manage/<?php echo GROUP_LAR_ID; ?>">Manage Lead Allocation Team Representatives</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/manage/<?php echo GROUP_SR_ID; ?>">Manage Sales Team Representatives</a> </li>
                </ul>
            </li>
            
            <li class="treeview <?php if ($this->uri->segment(2) == 'tender-quotes') { ?> active <?php } ?>" >
                <a href="#"><span><i class="fa fa-list"></i> Tender & Quotes </span> <i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/tender-quotes/add">Add Tender & Quotes Profile</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/tender-quotes/manage/<?php echo GROUP_TQU_ID; ?>">Manage Tender & Quotes Profiles</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(1) == 'product') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> LED Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/product/led/add">Add Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led/manage">Manage Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led_existing/manage">Manage Existing Products</a> </li>
                </ul>
            </li>
            
            <li class="treeview <?php if ($this->uri->segment(2) == 'solar') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> Solar Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <?php /**<li><a href="<?php echo site_url(); ?>admin/product/solar/add">Add Commerical Products</a> </li> */ ?>
                    <?php /** <li><a href="<?php echo site_url(); ?>admin/product/solar/manage">Manage Products</a> </li> */ ?>
                    <?php /** <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/add">Add Commerical Products</a> </li> */ ?>
                    <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/manage">Manage Products</a> </li>
                </ul>
            </li>

            <li>
                <a href="#"><span><i class="fa fa-shopping-cart"></i> Product Types</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/product-type/manage">Manage Product Types</a> </li> 
                </ul>
            </li>

            <?php /*             * <li class="treeview <?php if ($this->uri->segment(1) == 'service') { ?>active <?php } ?>">
              <a href="#"><span><i class="fa fa-sitemap"></i> Services</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
              <ul class="treeview-menu">
              <li><a href="<?php echo site_url(); ?>admin/service/category/manage">Manage Category</a> </li>
              <li><a href="<?php echo site_url(); ?>admin/service/line-item/add">Add Global Line Item</a> </li>
              <li><a href="<?php echo site_url(); ?>admin/service/line-item/manage">Manage Global Line Item</a> </li>
              </ul>
              </li>

              <li class="treeview <?php if ($this->uri->segment(1) == 'company-markup') { ?>active <?php } ?>">
              <a href="#"><span><i class="fa fa-building"></i> Company Markup</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
              <ul class="treeview-menu">
              <li><a href="<?php echo site_url(); ?>admin/company-markup/add">Add Global Company Markup</a> </li>
              <li><a href="<?php echo site_url(); ?>admin/company-markup/manage">Manage Global Company Markup</a> </li>
              </ul>
              </li> */ ?>

            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/lead-stages/manage">Manage Lead Stages</a></li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/simpro_sales/manage">Manage Sales Dashboard</a></li>
                </ul>
            </li>
        <?php } else if ($this->aauth->is_member('Lead Allocation Team Leader')) { ?>
            <li class="treeview <?php if ($this->uri->segment(2) == 'team-representative') { ?> active <?php } ?>" >
                <a href="#"><span><i class="fa fa-users"></i> Team Representatives</span> <i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/add">Add Team Representative</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/manage/<?php echo GROUP_LAR_ID; ?>">Manage Team Representatives</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(1) == 'product') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> LED Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/product/led/add">Add Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led/manage">Manage Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led_existing/manage">Manage Existing Products</a> </li>
                </ul>
            </li>
            
            <li class="treeview <?php if ($this->uri->segment(2) == 'solar') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> Solar Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <?php /**<li><a href="<?php echo site_url(); ?>admin/product/solar/add">Add Commerical Products</a> </li> */ ?>
                    <li><a href="<?php echo site_url(); ?>admin/product/solar/manage">Manage Commercial Products</a> </li>
                    <?php /** <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/add">Add Commerical Products</a> </li> */ ?>
                    <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/manage">Manage Residential Products</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/team-leader/manage">Manage Settings</a></li>

                </ul>
            </li>
        <?php } else if ($this->aauth->is_member('Lead Allocation Representative')) { ?>
            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/team-representative/manage">Manage Settings</a></li>
                </ul>
            </li>
        <?php } else if ($this->aauth->is_member('Sales Team Leader')) { ?>
            <li class="treeview <?php if ($this->uri->segment(2) == 'team-representative') { ?> active <?php } ?>" >
                <a href="#"><span><i class="fa fa-users"></i> Team Representatives</span> <i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/add">Add Team Representative</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/team-representative/manage/<?php echo GROUP_SR_ID; ?>">Manage Team Representatives</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(1) == 'product') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> LED Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/product/led/add">Add Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led/manage">Manage Products</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/product/led_existing/manage">Manage Existing Products</a> </li>
                </ul>
            </li>
            
            <li class="treeview <?php if ($this->uri->segment(2) == 'solar') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-shopping-cart"></i> Solar Products</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <?php /**<li><a href="<?php echo site_url(); ?>admin/product/solar/add">Add Commerical Products</a> </li> */ ?>
                    <li><a href="<?php echo site_url(); ?>admin/product/solar/manage">Manage Commercial Products</a> </li>
                    <?php /** <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/add">Add Commerical Products</a> </li> */ ?>
                    <li><a href="<?php echo site_url(); ?>admin/product/residential_solar/manage">Manage Residential Products</a> </li>
                </ul>
            </li>

            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/team-leader/manage">Manage Settings</a></li>
                </ul>
            </li>
        <?php } else if ($this->aauth->is_member('Sales Representative')) { ?>
            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                    <li><a href="<?php echo site_url(); ?>admin/settings/team-representative/manage">Manage Settings</a></li>
                    <li>
                        <a href="#"><span>Google</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                        <ul class="treeview-menu">
                            <li class="treeview <?php if ($this->uri->segment(1) == 'franchise') { ?> active <?php } ?>">
                                <a href="<?php echo site_url(); ?>admin/settings/gcal">Google Calendar</a> 
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        <?php } else if ($this->aauth->is_member('Admin') || $this->aauth->is_member('Tender Quotes Users')) { ?>

            <li class="treeview <?php if ($this->uri->segment(1) == 'change-password') { ?>active <?php } ?>">
                <a href="#"><span><i class="fa fa-cog"></i> Settings</span><i class="fas fa-angle-right pull-right toggle-arrow"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url(); ?>admin/settings/change-password">Change Password</a> </li>
                </ul>
            </li>
        <?php } ?>
        
        <?php if(!$this->aauth->is_member('Tender Quotes Users')){ ?>
         <li class="treeview <?php if ($this->uri->segment(1) == 'search') { ?>active <?php } ?>">
            <a href="<?php echo site_url(); ?>admin/search/manage"><span><i class="fas fa-search"></i> Search</span></a>
        </li>
        <?php } ?>
    </ul>
    <!--navigation-->
</div>
<!--leftpanel-->

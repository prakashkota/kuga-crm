<html>

<head>
    <title></title>
    <style>
        .page24 .back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page24 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 610px;
            padding-top: 30px;
        }

        .page24 .top hr {
            width: 70%;
            height: 2px;
            margin: -24px 0px 0px 0px;
            background-color: #D01E2A;
        }

        .page24 .text_color {
            color: #D01E2A;
        }

        .page24 .title {
            margin-top: -50px;
            margin-bottom: :30px;
        }

        .page24 .title h1 {
            padding-left: 14px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page24 .col-4 {
            max-width: 32.5%;
            display: inline-flex;
        }

        .page24 .imagesbox {
            margin-bottom: 30px;
        }

        .page24 .imagesbox img {
            width: 100%;
        }

        .page24 .banntext h2 {
            color: red;
            font-size: 20px;
        }

        .page24 .socialmed {
            margin-top: 39PX;
        }

        .page24 .imagesbox h2 {
            font-size: 16px;
            line-height: 1.3;
        }

        .page24 .footer {
            display: inline-flex;
        }

        .page24 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page24 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page24 .images img {
            width: 100%;
        }

        .page24 .imagese img {
            margin-left: 730px;
        }

        .page24 .socialbox .boxfrist p {
            display: inline-block;
            margin-left: 15px;
            vertical-align: super;
        }

        .page24 .socialbox .boxfrist img {
            display: inline-block;
            width: 130px;
        }

        .page24 .paddsec {
            padding: 0 40px;
        }

        .page24 .imagesbox iframe {
            border: 0;
        }

        .page24 .socialbox {
            padding-bottom: 93px;
        }
    </style>
</head>

<body>
    <div class="back-bg page24">
        <div class="paddsec">
            <div class="top">
                <h2>
                    <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                    <hr>
            </div>
            <div class="title">
                <h1>MEDIA</h1>
            </div>
            <div class="banntext">
                <h2>See our work and recent installations on <a href="https://www.youtube.com/channel/UCNvIeQncB97SLR8rlnXLZQA"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/social2.png'); ?>" width="100"></a></h2>
            </div>
            <div class="imagessec">
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/jdt36OuhyfY">
                        </iframe>
                        <h2>Medline - 450kW Solar Install</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/6nJZpJHsrgk">
                        </iframe>
                        <h2>Ellinbank Dairy Research Center 100kW Solar Installation</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/vP0kNLpUK2g">
                        </iframe>
                        <h2>99.63kW IGA Creswick</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/tc3kdH3FXQM">
                        </iframe>
                        <h2>100kW Solar Install Progression</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/n1P_Wub40LA">
                        </iframe>
                        <h2>520kW Commercial Solar Installation</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/y_SHfzapCGk">
                        </iframe>
                        <h2>$152,000 First Year Savings with a 525kW Solar System</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/M-afg87EvSs">
                        </iframe>
                        <h2>Altimate Foods - 239kW</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/yZuUUzxSyQE">
                        </iframe>
                        <h2>300kW Solar Installation at Maxton Fox</h2>
                    </div>
                </div>
                <div class="col-4">
                    <div class="imagesbox">
                        <iframe allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="265" height="160" src="https://www.youtube.com/embed/0aryAHoiUbQ">
                        </iframe>
                        <h2>Half Price Electricity with 99kW of Solar Power</h2>
                    </div>
                </div>
            </div>
            <div class="socialmed">
                <h2> Follow us on social</h2>
                <p>We regularly post new contents on our social media channels, follow us for <br>the latest news in the industry.</p>
            </div>
            <div class="socialbox">
                <table style="width: 100%; padding-top: 20px;">
                    <tr>
                        <td>
                            <div class="boxfrist">
                                <a href="https://www.facebook.com/13kuga"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/social1.png'); ?>"></a>
                            </div>
                        </td>
                        <td style="text-align:center;">
                            <div class="boxfrist">
                                <a href="https://www.youtube.com/channel/UCNvIeQncB97SLR8rlnXLZQA"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/social2.png'); ?>"></a>
                            </div>
                        </td>
                        <td style="text-align:right;">
                            <div class="boxfrist">
                                <a href="https://www.linkedin.com/company/kuga-electrical/?originalSubdomain=au"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/social3.png'); ?>"></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="imagese"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
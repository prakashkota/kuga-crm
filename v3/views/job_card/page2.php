<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <strong>Panel Layout Photo (Pointing MSB/ Transformer/ Inverter)</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="panel_layout" />
                                        <input type="hidden" id="panel_layout" name="job_card[panel_layout]" required="" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <strong>Screenshot of KUGA Quoting Tool</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="quoting_tool_screenshot" />
                                        <input type="hidden" id="quoting_tool_screenshot" name="job_card[quoting_tool_screenshot]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="200"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
<div class="modal fade" id="add_customer_location_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_customer_location_modal_title">Edit Customer Location</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="#" method="post" name="locationAdd" id="locationAdd">
                            <input type="hidden" name="site_id" id="site_id">
                            <input type="hidden" name="cust_id" id="company_cust_id">
                            <input type="hidden" name="site[latitude]" id="locationLatitude" />
                            <input type="hidden" name="site[longitude]" id="locationLongitude" />
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="form-section">Customer Details
                                        <div class="buttwrap" style="float:right;">
                                            <div id="customer_loader"></div>
                                            <input style="font-size: 10px;" type="button" id="save_customer" name="submit" data-loading-text="Saving..." class="btn" value="Save Customer" >
                                        </div>
                                    </h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="company[first_name]" id="contact_firstname"  class="form-control" placeholder="First Name"  required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="company[last_name]" id="contact_lastname"  class="form-control" placeholder="Last Name"  required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="company[customer_email]" id="contact_email"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" name="company[customer_contact_no]" id="contact_phone"  class="form-control" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required></div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text"  name="company[position]" id="contact_position" class="form-control" placeholder="Position"  required >
                                    </div>
                                </div>
                            </div>
                            

                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="form-section">Site Details
                                    <div class="buttwrap" style="float:right;">
                                        <div id="customer_site_loader"></div>
                                        <input style="font-size: 10px;" type="button" id="save_customer_location" name="submit" data-loading-text="Saving..." class="btn" value="Save Site" >
                                    </div> 
                                    </h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="form-group control-group">
                                        <label>Customer Address</label>
                                        <select class="select2 form-control locationstreetAddress" id="cust_locationstreetAddress"  style="height:48px !important;"></select>
                                        <input class="locationstreetAddressVal" type="hidden"  name="site[address]" />
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group control-group">
                                        <label>State</label>
                                        <select name="site[state_id]" class="form-control locationState" required>
                                            <option value="">Select State</option>
                                            <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group control-group">
                                        <label>Post Code</label>
                                        <input class="form-control locationPostCode" placeholder="Please Enter Post Code" type="text" name="site[postcode]" id="locationPostCode" value=""  required />

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
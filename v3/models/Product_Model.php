<?php

class Product_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function get_product_types($cond = FALSE){
       //$inner_select_parent_type_name = "IF(type.parent_id,NULL,Coalesce((SELECT type_name FROM tbl_product_types WHERE parent_id=type.type_id),NULL)) as parent_type_name";
        $sql = "SELECT t1.*,t2.type_name as parent_type_name"
                . " FROM tbl_product_types as t1"
                . " LEFT JOIN tbl_product_types as t2 ON t2.type_id = t1.parent_id";
        
        $query = $this->db->query($sql . $cond);
        return $query->result_array();
    }
    
    
     public function get_led_products(){
        $cond = "WHERE nlp.status = 1";
        $sql = "SELECT nlp.*,type.type_name"
                . " FROM tbl_led_new_products as nlp";
        $sql .= " LEFT JOIN tbl_product_types as type ON nlp.type_id = type.type_id";
        $sql .= " $cond ORDER BY nlp.np_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
     public function get_existing_led_products(){
        $cond = "WHERE elp.status = 1";
        $sql = "SELECT elp.*,type.type_name"
                . " FROM tbl_led_existing_products as elp";
        $sql .= " LEFT JOIN tbl_product_types as type ON elp.type_id = type.type_id";
        $sql .= " $cond ORDER BY elp.ep_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_solar_products_by_cond($cond = ''){
        $sql = "SELECT prd.*,type.type_name"
                . " FROM tbl_solar_products as prd";
        $sql .= " LEFT JOIN tbl_product_types as type ON prd.type_id = type.type_id";
        $sql .= " $cond ORDER BY prd.prd_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_led_proposal_products($proposal_id = FALSE){
        $cond = "";
        $sql = "SELECT pprd.*,"
                . "ep.ep_name,ep.ep_wattage,ep.ep_rated_life,ep.globe_cost,ep.driver_consumption,"
                . "ep.replacement_time,ep.labour_rate,ep.cost_per_minute,ep.replacement_cost,"
                . "np.np_name,np.np_wattage,np.np_rated_life,np.rebate_2000,np.rebate_3000,"
                . "np.rebate_5000,np.rebate_5000_a,np.rebate_5000_c,"
                . "st.classification as space_type_name"
                . " FROM tbl_led_proposal_products as pprd";
        $sql .= " LEFT JOIN tbl_led_existing_products as ep ON ep.ep_id = pprd.ep_id";
        $sql .= " LEFT JOIN tbl_led_new_products as np ON np.np_id = pprd.np_id";
        $sql .= " LEFT JOIN tbl_led_space_types as st ON st.space_type_id = pprd.space_type_id";
        if($proposal_id){
            $cond .= " WHERE pprd.proposal_id=".$proposal_id;
        }
        $sql .= " $cond ORDER BY pprd.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    public function get_led_proposal_additional_items($proposal_id = FALSE){
        $cond = "";
        $sql = "SELECT pae.*,"
                . "ae.ae_name"
                . " FROM tbl_led_proposal_access_equipments as pae";
        $sql .= " LEFT JOIN tbl_led_access_equipments as ae ON ae.ae_id = pae.ae_id";
        if($proposal_id){
            $cond .= " WHERE pae.proposal_id=".$proposal_id;
        }
        $sql .= " $cond ORDER BY pae.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    public function get_led_proposal_products_sum_by_product_type($proposal_id = FALSE){
        $cond = "";
        
        $higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal_products pro_item"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=3),0) as total_highbay";
        
        $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal_products pro_item"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=4),0) as total_batten_light";
        
        $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal_products pro_item"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=5),0) as total_panel_light";
        
        $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal_products pro_item"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=6),0) as total_flood_light";
        
        $down_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal_products pro_item"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=7),0) as total_down_light";
        
        $other_select = "Coalesce((SELECT SUM(pro_item.ae_qty * pro_item.ae_cost) FROM tbl_led_proposal_access_equipments pro_item"
                . " LEFT JOIN tbl_led_access_equipments as ae ON ae.ae_id = pro_item.ae_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = ae.type_id"
                . " WHERE pro_item.proposal_id=" . $proposal_id . " AND prd_type.type_id=8),0) as total_other";
        
        $sql = "SELECT pro.*,"
                . "$higbay_select,"
                . "$batten_light_select,"
                . "$panel_light_select,"
                . "$flood_light_select,"
                . "$down_light_select,"
                . "$other_select"
                . " FROM tbl_led_proposal as pro";
        if($proposal_id){
            $cond .= " WHERE pro.id=".$proposal_id;
        }
        $sql .= " $cond ORDER BY pro.id ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    public function get_deal_products_sum_by_product_type($lead_id = FALSE){
        $cond = "";
        
        //SUM(pro_item.np_qty * pro_item.price)
        //LED TOTAL VALUES
        /**$higbay_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=3),0) as total_highbay";
        
        $batten_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=4),0) as total_batten_light";
        
        $panel_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=5),0) as total_panel_light";
        
        $flood_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=6),0) as total_flood_light";
        
        $down_light_select = "Coalesce((SELECT SUM(pro_item.np_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_products as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_new_products as prd ON prd.np_id = pro_item.np_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = prd.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=7),0) as total_down_light";
        
        $other_select = "Coalesce((SELECT SUM(pro_item.ae_qty) FROM tbl_led_proposal lp"
                . " LEFT JOIN tbl_led_proposal_access_equipments as pro_item ON pro_item.proposal_id = lp.id"
                . " LEFT JOIN tbl_led_access_equipments as ae ON ae.ae_id = pro_item.ae_id"
                . " LEFT JOIN tbl_product_types as prd_type ON prd_type.type_id = ae.type_id"
                . " WHERE lp.lead_id=" . $lead_id . " AND prd_type.type_id=8),0) as total_other";*/
        


        $higbay_select = "Coalesce((lead.total_highbay),0) as total_highbay";
        
        $batten_light_select = "Coalesce((lead.total_batten_light),0) as total_batten_light";
        
        $panel_light_select = "Coalesce((lead.total_panel_light),0) as total_panel_light";
        
        $flood_light_select = "Coalesce((lead.total_flood_light),0) as total_flood_light";
        
        $down_light_select = "Coalesce((lead.total_down_light),0) as total_down_light";
        
        $other_select = "Coalesce((lead.total_other),0) as total_other";
        
        $total_led_project_cost_select = "Coalesce(SUM(pro.total_project_cost_exGST),0) as total_led";
        
        //SOLAR TOTAL VALUES
                
        $sql = "SELECT "
                . "$total_led_project_cost_select,"
                . "$higbay_select,"
                . "$batten_light_select,"
                . "$panel_light_select,"
                . "$flood_light_select,"
                . "$down_light_select,"
                . "$other_select"
                . " FROM tbl_led_proposal as pro";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.id = pro.lead_id";
        if($lead_id){
            $cond .= " WHERE lead.id=".$lead_id;
        }
        $sql .= " $cond ORDER BY pro.id ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    public function get_solar_proposal_products($proposal_id = FALSE){
        $cond = "";
        $sql = "SELECT pprd.*,sp.*,"
                . "pt.type_name"
                . " FROM tbl_solar_proposal_products as pprd";
        $sql .= " LEFT JOIN tbl_solar_products as sp ON sp.prd_id = pprd.prd_id";
        $sql .= " LEFT JOIN tbl_product_types as pt ON pt.type_id = sp.type_id";
        if($proposal_id){
            $cond .= " WHERE pprd.proposal_id=".$proposal_id;
        }
        $sql .= " $cond ORDER BY pprd.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    /** Function for Residendtial soalr proposal copied from solarrun */

     function get_item_data_by_cond($cond = FALSE) {
        $sql = "SELECT prd.*,tpv.state as variant_state,tpv.id as variant_id,tpv.application as variant_application,"
                . "tpv.cost_price as variant_cp,tpv.is_price_set as variant_ips,tpv.amount as variant_amt,tpv.additional_margin as variant_am"
                . " FROM tbl_products as prd";
        $sql .= " LEFT JOIN tbl_product_variants as tpv ON prd.id = tpv.product_id";
        $sql .= " LEFT JOIN tbl_product_type as type ON type.typeId = prd.type_id";
        if ($cond) {
            $sql .= $cond . " AND prd.ted_crm_id IS NOT NULL AND prd.status=1 and prd.availability<>0";
            // $sql .= $cond;
        } else {
            $sql .= " where prd.ted_crm_id IS NOT NULL AND prd.availability<>0";
        }
        $sql .= ' GROUP BY prd.id order by prd.name asc';
        // echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_item_data_by_cond_by_attachment($cond = FALSE) {
        $inverter_design_image = "(SELECT CONCAT(prda1.base_url,'large/',prda1.file_name) from tbl_product_attachments as prda1 WHERE prda1.media_cat_id=9 AND prda1.product_id = prd.ted_crm_id ORDER BY prda1.media_id DESC LIMIT 1)";
        $sql = "SELECT prd.*,tpv.state as variant_state,tpv.id as variant_id,tpv.application as variant_application,"
                . "tpv.cost_price as variant_cp,tpv.is_price_set as variant_ips,tpv.amount as variant_amt,tpv.additional_margin as variant_am, $inverter_design_image as inverter_design_image"
                . " FROM tbl_products as prd";
        $sql .= " LEFT JOIN tbl_product_variants as tpv ON prd.id = tpv.product_id";
        $sql .= " LEFT JOIN tbl_product_type as type ON type.typeId = prd.type_id";
        if ($cond) {
            $sql .= $cond . " AND prd.ted_crm_id IS NOT NULL AND prd.status=1 and prd.availability<>0";
            // $sql .= $cond;
        } else {
            $sql .= " where prd.ted_crm_id IS NOT NULL AND prd.availability<>0";
        }
        $sql .= ' GROUP BY prd.id order by prd.name asc';
        // echo $sql;die;
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_racking_item_data($data) {
        $panel_id               = $data['prd_panel'];
        $roof_type              = $data['proposal']['roof_type'];
        $roof_type_no_of_panels = $data['proposal']['roof_type_no_of_panels'];
        $like_where             = "";
        foreach ($roof_type_no_of_panels as $key => $value) {
            if ($key == 2 && $value != 0) {
                if ($like_where == '') {
                    $like_where .= "prd.name LIKE '%Tilt%' ";
                } else {
                    $like_where .= "OR prd.name LIKE '%Tilt%' ";
                }
            } else if ($value != 0) {
                $like_where = "prd.name LIKE '%Tin\/Tile%' ";
            }
        }

        if ($like_where != '') {
            $sql   = "SELECT prd.*,tpv.state as variant_state,tpv.id as variant_id,tpv.application as variant_application,"
                    . "type.typeName,tpv.cost_price as variant_cp,tpv.is_price_set as variant_ips,tpv.amount as variant_amt,tpv.additional_margin as variant_am"
                    . " FROM tbl_products as prd";
            $sql   .= " LEFT JOIN tbl_product_variants as tpv ON prd.id = tpv.product_id";
            $sql   .= " LEFT JOIN  tbl_product_type as type ON type.typeId = prd.type_id";
            $sql   .= " WHERE tpv.state IN (SELECT state from tbl_product_variants WHERE product_id=" . $panel_id . ") AND prd.status=1 AND prd.type_id=4 AND " . $like_where;
            $sql   .= "GROUP BY CASE
                    WHEN prd.name LIKE '%Tin\/Tile%' THEN 'tile_tin'
                    WHEN prd.name LIKE '%Tilt%' THEN 'tilt'
                    ELSE NULL
                END";
            $query = $this->db->query($sql);
            return $query->result_array();
        } else {
            return array();
        }
    }

    function get_products($select,$custom_cond) {
        $sql = "SELECT tp.*,$select,tp.id as item_crm_id,NULL as item_ed_id FROM tbl_products as tp";
        $sql .= " LEFT JOIN tbl_product_type as pt ON pt.typeId=tp.type_id";
        if ($custom_cond) {
            $sql .= $custom_cond;
        }
       
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function get_products_list_by_roles($group_id, $id = FALSE) {

        $cond = " WHERE prd.status=1";
        if ($group_id) {
            $cond .= " AND user_group.group_id = $group_id";
        }
        if ($id) {
            $cond .= " AND prd.user_id='" . $id . "'";
        }

        $sql = "SELECT * FROM (SELECT prd.*,type.typeName,"
                . "prd_variant.state as variant_state,prd_variant.application as variant_application,prd_variant.cost_price as variant_cost_price,"
                . "prd_variant.is_price_set as variant_is_price_set,prd_variant.amount as variant_amount,"
                . "prd_variant.additional_margin as variant_additional_margin,prd_variant.last_updated_price as variant_last_updated_price,ud.full_name"
                . " FROM tbl_products as prd";
        $sql .= " LEFT JOIN tbl_product_type as type ON type.typeId = prd.type_id";
        $sql .= " LEFT JOIN tbl_product_variants as prd_variant ON prd_variant.product_id = prd.id";
        $sql .= " LEFT JOIN aauth_users as admin ON admin.id = prd.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        $sql .= " $cond ORDER BY prd.id ASC) AS a GROUP BY id";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
   
    public function get_ed_products($cond = FALSE){
        
        $this->edcrm_db = $this->load->database('edcrm_db', TRUE);
        
        $from_select = "(SELECT * FROM tbl_products WHERE status=1 ORDER BY id DESC) as prd";
        
        $productDataSheet = "Coalesce((SELECT CONCAT(prdatt_ds.base_url,'large/',prdatt_ds.file_name) FROM  tbl_product_attachments as prdatt_ds"
            . " WHERE prd.id = prdatt_ds.product_id AND prdatt_ds.media_cat_id = 1"
            . " GROUP BY prdatt_ds.product_id LIMIT 1),'')";
            
        $productPhoto = "Coalesce((SELECT CONCAT(prdatt_ph.base_url,'medium/',prdatt_ph.file_name) FROM  tbl_product_attachments as prdatt_ph"
            . " WHERE prd.id = prdatt_ph.product_id AND prdatt_ph.media_cat_id = 3"
            . " GROUP BY prdatt_ph.product_id LIMIT 1),'')";

        $sql = "SELECT prd.*,
                prd.name as item_name,
                pt.typeName as product_type,
                pt.typeId as product_type_id,
                tpv.state as variant_state,
                COALESCE(prdprvar_sr.availability,prdprvar_sr.availability,'') as pricing_sr_availability,
                COALESCE(prdprvar_sr.markup,prdprvar_sr.markup,'') as pricing_sr_markup,
                COALESCE(prdprvar_sr.sell_price,prdprvar_sr.sell_price,'') as pricing_sr_sell_price,                
                $productDataSheet as product_datasheet,
                $productPhoto as product_photo
                FROM $from_select";
        $sql .= " LEFT JOIN tbl_product_pricing_variants as prdprvar_sr ON prd.id = prdprvar_sr.product_id AND prdprvar_sr.company = 2";        
        $sql .= " LEFT JOIN tbl_product_type as pt ON pt.typeId = prd.type_id";
        $sql .= " LEFT JOIN tbl_product_variants as tpv ON tpv.product_id = prd.id";
        
        if($cond){
            $sql .= $cond . " AND prd.status=1 and prdprvar_sr.availability<>0";
            //$sql .= $cond;
        }
        
        $sql .= " GROUP BY prd.id ORDER BY prd.name,pt.typeId DESC";

		
        $query = $this->edcrm_db->query($sql);
        return $query->result_array();
    
	
	}
}

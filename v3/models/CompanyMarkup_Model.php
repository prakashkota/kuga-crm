<?php

class CompanyMarkup_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch service line item
     * @param type $id
     * @return array
     */
    function get_markup_list_by_roles($group_id , $id = FALSE) {
        $sql = "SELECT cm.*,ud.full_name"
                . " FROM tbl_company_markup as cm";
        $sql .= " LEFT JOIN aauth_users as admin ON admin.id = cm.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        if($group_id){
            $sql .= " WHERE user_group.group_id = $group_id";
        }
        if($id){
            $sql .= " AND cm.user_id='" . $id . "'";
        }
        
       $query = $this->db->query($sql);
       return $query->result_array();
    }
    
    public function get_markup_data_for_proposal($owner_id,$data) {
        $no_of_panels = $data['no_of_panels'];
        $state = $data['prd_panel_data']['variant_state'];
        $sql = "SELECT cm.*,cmd.price,cmd.qty_from,cmd.qty_to"
                . " FROM tbl_company_markup as cm";
        $sql .= " LEFT JOIN tbl_company_markup_data as cmd ON cm.id = cmd.company_markup_id";
        $sql .= " WHERE cm.user_id='".$owner_id."' && cm.state IN ('All','" . $state . "') AND " . $no_of_panels . " BETWEEN cmd.qty_from AND cmd.qty_to";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

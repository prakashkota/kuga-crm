<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/payment.png'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">

                <!--<tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                    <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_30" name="booking_form[is_upfront_30]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 30kW</td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">No deposit, 100% due upon handover of Certificate of Electricity Safety</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit" name="booking_form[is_upfront_30_deposit]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_30_excGST" name="booking_form[is_upfront_30_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr> -->
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_99" name="booking_form[is_upfront_99]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 40kW - 100kW </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">20% Deposit</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_99_deposit_20" name="booking_form[is_upfront_99_deposit_20]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">70% on project commencement</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_99_deposit_70" name="booking_form[is_upfront_99_deposit_70]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">10% on handover of CES</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_99_deposit_10" name="booking_form[is_upfront_99_deposit_10]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_99_excGST" name="booking_form[is_upfront_99_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
               <!-- <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_100" name="booking_form[is_upfront_100]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 100kW </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Payment (20%)</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_20" name="booking_form[is_upfront_100_deposit_20]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Payment on installation commencement (70%)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_70" name="booking_form[is_upfront_100_deposit_70]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Upon providing Certificate of Electrical Safety (10%)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_10" name="booking_form[is_upfront_100_deposit_10]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_100_excGST" name="booking_form[is_upfront_100_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr> -->
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                 <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_finance" name="booking_form[is_finance]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 2 - Finance  for 40kW - 100kW</td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <!--<tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">90% on project commencement</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc Gst:</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_90_incGST" name="booking_form[is_finance_90_incGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">10% on handover of CES</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_10_incGST" name="booking_form[is_finance_10_incGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>-->
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Monthly Repayments</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc GST:</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_monthly_repayments" name="booking_form[is_finance_monthly_repayments]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Term (years)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_terms" name="booking_form[is_finance_terms]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
                
                <tr class="hidden">
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_energy_plan" name="booking_form[is_energy_plan]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 3 - Energy Plan </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_8" name="booking_form[is_energy_plan_8]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 8YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_8_kwh" name="booking_form[is_energy_plan_8_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_12" name="booking_form[is_energy_plan_12]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 12YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_12_kwh" name="booking_form[is_energy_plan_12_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_15" name="booking_form[is_energy_plan_15]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 15YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_15_kwh" name="booking_form[is_energy_plan_15_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                        </table></td>
                </tr>

            </table></td>
    </tr>
    <tr>
        <td height="450">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
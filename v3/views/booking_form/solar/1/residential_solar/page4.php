<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">House Frontage</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Proposed Array Location</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="business_frontage" />
                            <input type="hidden" id="business_frontage" name="booking_form_image[business_frontage]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="proposed_array_location" />
                            <input type="hidden" id="proposed_array_location" name="booking_form_image[proposed_array_location]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Proposed Inverter Location</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Array Access Point</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="proposed_inverter_location" />
                            <input type="hidden" id="proposed_inverter_location" name="booking_form_image[proposed_inverter_location]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                    <td align="center" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="array_access_point" />
                            <input type="hidden" id="array_access_point" name="booking_form_image[array_access_point]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/solar_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
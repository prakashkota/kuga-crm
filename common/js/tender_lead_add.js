
var lead_add_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? parseInt(options.userid) : '';
    this.user_group = (options.user_group && options.user_group != '') ? options.user_group : '';
    this.cust_location_data = [];
    this.uuid = '';
    this.exist_lead_data = {};

    $('#manual_address_btn').click(function(){
        $(this).addClass('hidden');
        $('#manual_address_hide_btn').removeClass('hidden');
        $('#locationstreetAddress').next().css('display','none');
        $('#locationstreetAddressVal').attr('type','text');
    }); 
 
    $('#manual_address_hide_btn').click(function(){
        $(this).addClass('hidden');
        $('#manual_address_btn').removeClass('hidden');
        $('#locationstreetAddress').next().css('display','block')
        $('#locationstreetAddressVal').attr('type','hidden');
    });
 
    $('#locationstreetAddressVal').keyup(function(){
        $('#select2-locationstreetAddress-container').html($(this).val());
    });

    setTimeout(function(){
        $('.select2-container').addClass('kg-form_field--orange');
    },100);
    
    $( function() {
        $("#closing_date").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date(),
            autoclose: true,
            onSelect: function(date){
                date = moment(date,'D/M/YYYY').format('M/D/YYYY')
                var tempStartDate = new Date(date);
                var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth() + 4, tempStartDate.getDate());
                console.log(tempStartDate);
                console.log(default_end);
                $('#award_date').datepicker('setDate', default_end); // Set as default 
            }
        });
        $("#closing_date" ).datepicker("setDate", new Date());

        $("#award_date").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date(),
            autoclose: true,
        });
        var end_date = new Date();
        var award_date = new Date(end_date.getFullYear(), end_date.getMonth()+4, end_date.getDate());
        $("#award_date" ).datepicker("setDate", award_date);

    });
  
  
    
    
    $('#confirm_customer_yes').click(function(){
        window.location.href = base_url + 'admin/tender-lead/edit/'+ self.exist_lead_data.uuid;
    });
    
    $('#save_lead').click(function(){
        self.save_lead();
    });

};

/** Common Functions **/

lead_add_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_add_manager.prototype.validate_lead_data = function () {
    var self = this;
    
    var flag = true;
    
    var elements = document.getElementById('tender_lead_form').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }

    return flag;
};

lead_add_manager.prototype.save_lead = function () {
    toastr.clear();
    var self = this;
    
    var flag = self.validate_lead_data();
    
    if(!flag){
        toastr['error']('Please fill the required fields marked in red');
        return false;
    }
    
    if (self.uuid == '') {
        var uuid = self.create_uuid();
        self.uuid = uuid;
    }
    
    var formData = $('#tender_lead_form').serialize();
    formData += '&uuid=' + self.uuid;
    $.ajax({
        url: base_url + 'admin/tender-lead/save_lead_details',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.clear();
            toastr["info"]('Saving Lead....');
            $("#save_lead").attr("disabled","disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function () {
                    window.location.href = base_url + 'admin/tender-lead/add?deal_ref=' + self.uuid;
                }, 500);
            } else {
                toastr["error"](stat.status);
                $("#save_lead").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_lead").removeAttr("disabled");
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


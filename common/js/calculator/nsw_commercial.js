var calculator_manager_nsw_c = function(options) {
    var self = this;
    this.job_uuid = (options.job_uuid && options.job_uuid != '') ? options.job_uuid: '';
    this.logged_in_user_id = (options.logged_in_user_id && options.logged_in_user_id != '') ? options.logged_in_user_id: '';
    this.group_id = (options.group_id && options.group_id != '') ? options.group_id: '';
    this.calculator_items = [];
    this.baseline_product_row_count = 0;
    this.default_product_row_count = 0;
    this.area_name_array_nsw = [];
    this.onload_quantity_removed = 0;
    this.onload_quantity_installed = 0;
    this.rnf_value = 0;
    this.total_mwh_savings = 0;
    this.ti = 0;
    this.all_job_products = [];

    
    $(document).on('change','[name="calculator[postcode]"]',function(){
        $('[name="calculator[regional_site]"]').val($(this).find(':selected').attr('data-regional'));
        $('[name="calculator[rnf]"]').val($(this).find(':selected').attr('data-rnf'));
        for(var key in self.all_job_products){
           self.populate_basline_calculator_data(key);
        }
        $('[name="calculator[quantity_removed]"]').val(self.onload_quantity_removed);
        $('[name="calculator[quantity_installed]"]').val(self.onload_quantity_installed);
        $('[name="calculator[total_mwh_saving]"]').val(self.total_mwh_savings.toFixed(2));
        $('[name="calculator[min_co_payment]"]').val((self.total_mwh_savings*5).toFixed(2));
        $('[name="calculator[est_esc_s]"]').val((self.total_mwh_savings*1.06).toFixed(2));
    })

    $(document).on('change','[name="product[product_model][]"]',function(){
        var current_tr = $(this).parent().parent();
        current_tr.find('.lcp').val($(this).val());
    })

    $(document).on('change','[name="product[item_bca_type][]"]',function(){
        var dataid = $(this).attr('data-id');
        $('.eligible_aoh_'+dataid).val($(this).find(':selected').attr('data-op-hrs-per-yr'));
        for(var key in self.all_job_products){
           self.populate_basline_calculator_data(key);
        }
        $('[name="calculator[quantity_removed]"]').val(self.onload_quantity_removed);
        $('[name="calculator[quantity_installed]"]').val(self.onload_quantity_installed);
        $('[name="calculator[total_mwh_saving]"]').val(self.total_mwh_savings.toFixed(2));
        $('[name="calculator[min_co_payment]"]').val((self.total_mwh_savings*5).toFixed(2));
        $('[name="calculator[est_esc_s]"]').val((self.total_mwh_savings*1.06).toFixed(2));
    })

    $(document).on('change','[name="product[area_name_type][]"]',function(){
        var dataid = $(this).attr('data-id');
        var datakey = $(this).find(':selected').attr('data-key');
        var job_products = self.all_job_products[datakey];
        $('.existing_lighting_qty_'+dataid).val(JSON.parse(job_products.item_data).item_qty);
        $('.nominal_lamp_power_'+dataid).val(job_products.nlp);
        $('.upgraded_lighting_qty_'+dataid).val(job_products.qty);
        $('.lcp_low_mode_'+dataid).val(job_products.lcp);
        self.populate_basline_calculator_data(dataid);
        $('[name="calculator[quantity_removed]"]').val(self.onload_quantity_removed);
        $('[name="calculator[quantity_installed]"]').val(self.onload_quantity_installed);
        $('[name="calculator[total_mwh_saving]"]').val(self.total_mwh_savings.toFixed(2));
        $('[name="calculator[min_co_payment]"]').val((self.total_mwh_savings*5).toFixed(2));
        $('[name="calculator[est_esc_s]"]').val((self.total_mwh_savings*1.06).toFixed(2));
    })

    $(document).on('change','[name="product[lamp_ballast_combination][]"], [name="product[control_device][]"], [name="product[product_model][]"], [name="product[control_device_upgrade_lighting][]"], [name="product[special_occupancy][]"], [name="product[bca_classification][]"]',function(){
        var key = $(this).attr('data-id');
        self.populate_basline_calculator_data(key);
        $('[name="calculator[quantity_removed]"]').val(self.onload_quantity_removed);
        $('[name="calculator[quantity_installed]"]').val(self.onload_quantity_installed);
        $('[name="calculator[total_mwh_saving]"]').val(self.total_mwh_savings.toFixed(2));
        $('[name="calculator[min_co_payment]"]').val((self.total_mwh_savings*5).toFixed(2));
        $('[name="calculator[est_esc_s]"]').val((self.total_mwh_savings*1.06).toFixed(2));
        
    });

    
    $(document).on('click','#export_nsw_calc',function (e) {
        var dname = $(this).attr('data-name');
        var tableToExcel = (function () {
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
                var ctx = { worksheet: 'Worksheet', table: table.innerHTML }
                var element = document.createElement('a');
                element.setAttribute('href', uri + base64(format(template, ctx)));
                element.setAttribute('download', name);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }
        })()
        var name = dname + '_' + moment(new Date()).format('Y_M_D_h_m_s') + '.xls';
        var table = $('#calc-table').clone().attr('id','data_export_table');
        $('#export-data-container').html(table);
        $('#data_export_table >').find('table').each((i,tbl)=>{
            $(tbl).find('th').each((j,th)=>{
                var attrList = $(th)[0].attributes
                var text = $(th).text();
                var attrString = '';
                for(var k of attrList){
                    var ks = k.nodeName+'="'+k.textContent+'"'
                    attrString += ' '+ks+' '
                }
                $(th).replaceWith("<td "+attrString+">" + text + "</td>");
            })
            $(tbl).find('td').each((j,ele)=>{
                if($(ele).find('input').length){
                  var v = $(ele).find('input').val();
                  $(ele).html(v)
                }else if($(ele).find('select').length){
                  var v = $(ele).find('select option:selected').text();
                  $(ele).html(v);
                }
            })    
        })
        $('#data_export_table').find('.nth').attr('style','text-align:center;background:#807E7E;color:#ffffff;'); 
        $('#data_export_table').find('.nth1').attr('style','text-align:center;background:#474244;color:#ffffff;'); 
        $('#data_export_table').find('.table-bordered').attr('style','border: 1px solid #dee2e6;'); 
        tableToExcel('data_export_table',name)
    });
     
    $(document).on('change','.item_bca_type,#calculator_installation_site_postcode,.item_lamp_type,.item_nom_watts,.item_control_system,.item_air_con,.item_existing_no_of_lamps,.item_upgrade_lamp_type,.item_id,.item_upgrade_control_system,.item_qty,.item_lcd_activated',function(){
        if($(this).attr('id')=='calculator_installation_site_postcode'){
            var yes_no_value = self.calculator_items.post_code_regional_vic.find(pc=>{
                return pc.post_code == this.value
            })
            if(yes_no_value){
                if(yes_no_value.regional_vic.toLowerCase() == 'yes'){
                    $('#calculator_regional_vic').val('Yes').attr('readonly','readonly');
                    $('#calculator_rnf').val(1.04).attr('readonly','readonly');
                }else{
                    $('#calculator_regional_vic').val('No').attr('readonly','readonly');
                    $('#calculator_rnf').val(0.98).attr('readonly','readonly');
                }
            }else{
                toastr.error('Postcode seems to be incorrect to decide regional VIC');
                return false;
            }
        }
        var total_exisiting_lamps_count = 0;
        var total_upgraded_lamps_count = 0;
        var total_veecs_count = 0;
        var allItems = $('.item_existing_no_of_lamps');
        
        
        allItems.each(function(){
            var data_id = $(this).attr('data-id');
            var anualOperatingHrsFromBca = $('.item_bca_type_'+data_id+' option:selected').attr('data-op-hrs-per-yr');
            //$('.item_annual_op_hrs_'+data_id).val(anualOperatingHrsFromBca);
            
            var item_existing_no_of_lamps = $('.item_existing_no_of_lamps_'+data_id).val();
            item_existing_no_of_lamps = !isNaN(item_existing_no_of_lamps) ? parseInt(item_existing_no_of_lamps) : 0;
            
            var item_qty = $('.item_qty_'+data_id).val();
            item_qty = !isNaN(item_qty) ? parseInt(item_qty) : 0;
            
            
            // veec calculation 
            /*
            var am = 
            vecc = (baseline - upgrade) * lifetime * RNF
            baseline = lcp_old * cm old * existing no of lamps * 1.095
            upgrade = lcp_new * cm new * am * upgraded no of lamps * 1.095
            */
            try{
                var anual_operating_hrs = $('.item_annual_op_hrs_'+data_id).val();
                var product_life_time = $('.item_name_'+data_id).attr('data-lifetime') 
                var lcpFormulaData = $('.item_lamp_type_'+data_id+' option:selected').attr('data-lcp');
                
                var lcpFormula = JSON.parse(lcpFormulaData).formula;
                var activity_asset_lifetime = $('.item_activity_type_'+data_id+' option:selected').attr('data-lifetime') 
                var asset_life_time = 5; //default
                if(activity_asset_lifetime=='formula_calculated'){
                    asset_life_time = product_life_time/anual_operating_hrs;    
                }else{
                    asset_life_time = activity_asset_lifetime;
                }
                
                var life_time = (anual_operating_hrs * asset_life_time)/1000000;
                var item_upgraded_no_of_lamps = $('.item_qty_'+data_id).val(); 
                var lcp_old = 0; 
                var lcp_new = 0; // it is nlp multiplied by multipler provided in the xls
                var nom_watt = $('.item_nom_watts_'+data_id).val();
                if(!isNaN(nom_watt)){
                    lcpFormula = lcpFormula.replace(/no_of_watt/g,nom_watt);
                    lcp_old = eval(lcpFormula)
                }
                
                var lcp_new_json = $('.item_upgrade_lamp_type_'+data_id+' option:selected').attr('data-lcp');
                lcp_new_json = lcp_new_json!=''?JSON.parse(lcp_new_json):{}
                
                var nlp = $('.item_name_'+data_id).attr('data-nlp') 
                var rnf = $('#calculator_rnf').val();
                
                var lcp_new_multiplier = 1;
                if(lcp_new_json){
                    lcp_new_multiplier =lcp_new_json['multiplier'];
                }
                lcp_new = nlp *lcp_new_multiplier;
                var cm_old = $('.item_control_system_'+data_id+' option:selected').attr('data-multiplier');
                var cm_new = $('.item_upgrade_control_system_'+data_id+' option:selected').attr('data-multiplier');
                var am = $('.item_air_con_'+data_id).val();
                if(am.toLowerCase()=='yes'){
                    am = 1.05
                }else{
                    am = 1;
                }
                console.log('------------------------')
                console.log('lcp_old=',lcp_old,'lcp_new=',lcp_new,'cm_old=',cm_old,'cm_new=',cm_new,'am=',am,'life_time=',life_time,'asset_life_time = ',asset_life_time,'product_life_time=',product_life_time,'anual_operating_hrs=',anual_operating_hrs)
                var baseline = lcp_old * cm_old * am * item_existing_no_of_lamps * 1.095;
                console.log('baseline=',baseline)
                var upgrade = lcp_new * cm_new * am * item_upgraded_no_of_lamps * 1.095;
                console.log('upgrade=',upgrade)
                console.log('------------------------')
                var veec = (baseline-upgrade) * life_time * rnf;
                $('.item_no_of_veecs_'+data_id).val(veec)
            }catch(er){
                console.log(er)
            }
            
            var item_no_of_veecs = $('.item_no_of_veecs_'+data_id).val();
            item_no_of_veecs = !isNaN(item_no_of_veecs) ? parseInt(item_no_of_veecs) : 0;
            
            total_exisiting_lamps_count += item_existing_no_of_lamps;
            total_upgraded_lamps_count += item_qty;
            total_veecs_count += item_no_of_veecs;
        });
        $('#total_exisiting_lamps_count').html(total_exisiting_lamps_count);
        $('#total_upgraded_lamps_count').html(total_upgraded_lamps_count);
        $('#total_veecs_count').html(total_veecs_count);
        //self.save_veecs(total_veecs_count);
    })
};


calculator_manager_nsw_c.prototype.fetch_calculator_items = function() {
    var self = this;
    $.ajax({
        url: base_url + 'admin/calculator/fetch_items',
        type: 'get',
        dataType: "json",
        data:{'job_uuid' : self.job_uuid},
        cahce:false,
        beforeSend:function(){
            self.default_product_row_count = 0;
            document.getElementById('calculator_item_body').innerHTML = '';
        },
        success: function(stat) {
            self.all_job_products = stat.job_products;
            var job_details = window.job_manager_tool.job_details;
            var authorised_details = job_details.authorised_details;
            var business_details = job_details.business_details;
            
            //$('#calculator_company_name').val(business_details.entity_name);
            $('#calculator_date_of_poject_completion').val('');
            if(authorised_details != ''){
                $('#calculator_signatory_fname').html(authorised_details.first_name);
                $('#calculator_signatory_sname').val(authorised_details.last_name);
                $('#calculator_signatory_phone').val(authorised_details.contact_no);
                $('#calculator_signatory_email').val(authorised_details.email);
            }
            
            var location_data = window.job_manager_tool.cust_location_data;
            for (var key in location_data) {
                if (Object.keys(location_data[key]).length > 0) {
                    if (location_data[key].address == '' || location_data[key].address == null || location_data[key].address == 'null') {
                        continue;
                    }
                    
                    var address = location_data[key].address;
                    
                    // if(address){
                    //     var parsedAdd = parseAddress.parseLocation(address);
                    //     var unitType = (parsedAdd.unitType) ? parsedAdd.unitType : '';
                    //     var unitNumber = (parsedAdd.unitNumber) ? parsedAdd.unitNumber : '';
                    //     var streetNumber = (parsedAdd.streetNumber) ? parsedAdd.streetNumber : '';
                    //     var streetName = (parsedAdd.streetName) ? parsedAdd.streetName : '';
                    //     var streetType = (parsedAdd.streetType) ? parsedAdd.streetType : '';
                    //     var streetSuffix = (parsedAdd.streetSuffix) ? parsedAdd.streetSuffix : '';
                    //     var suburb = (parsedAdd.suburb) ? parsedAdd.suburb : '';
                    //     var state = (parsedAdd.state) ? parsedAdd.state : '';
                    //     var postcode = (parsedAdd.postcode) ? parsedAdd.postcode : '';
                        
                    //     $('.js-unit-type').val(unitType);
                    //     $('.js-unit-number').val(unitNumber);
                    //     $('.js-street-number').val(streetNumber);
                    //     $('.js-street-name').val(streetName);
                    //     $('.js-street-type').val(streetType);
                    //     $('.js-street-suffix').val(streetSuffix);
                    //     $('#calculator_installation_site_postcode').val(postcode);
                    //     $('#calculator_installation_site_suburb').val(suburb);
                        
                    //     var yes_no_value = stat.calculator_items.post_code_regional_vic.find(pc=>{
                    //         return pc.post_code == location_data[key].postcode;
                    //     })
                    //     if(yes_no_value){
                    //         if(yes_no_value.regional_vic.toLowerCase() == 'yes'){
                    //             $('#calculator_regional_vic').val('Yes').attr('readonly','readonly');
                    //             $('#calculator_rnf').val(1.04).attr('readonly','readonly');
                    //         }else{
                    //             $('#calculator_regional_vic').val('No').attr('readonly','readonly');
                    //             $('#calculator_rnf').val(0.98).attr('readonly','readonly');
                    //         }
                    //     }
                    // }
                }
            }
            


            self.calculator_items = [];
            
            self.calculator_items = stat.nsw_calculator_items;
            
            self.handle_basic_calculator_details(stat.nsw_calculator_items);
            
            var products = stat.job_products;
            for(var key in products){
                var prd = products[key];
                self.create_calculator_row(prd);
                
                self.populate_calculator_item_data(key,prd);
            }

            for(var key in products){
                var prd = products[key];
               self.handle_create_baseline_row(stat.nsw_calculator_items,prd); 
               self.populate_basline_calculator_data(key);
            }


            
            $('[name="calculator[quantity_removed]"]').val(self.onload_quantity_removed);
            $('[name="calculator[quantity_installed]"]').val(self.onload_quantity_installed);
            $('[name="calculator[total_mwh_saving]"]').val(self.total_mwh_savings.toFixed(2));
            $('[name="calculator[min_co_payment]"]').val((self.total_mwh_savings*5).toFixed(2));
            $('[name="calculator[est_esc_s]"]').val((self.total_mwh_savings*1.06).toFixed(2));
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

calculator_manager_nsw_c.prototype.handle_create_baseline_row = function(data,products){
    
   var self = this;
   var count = self.baseline_product_row_count;
   var tr = document.createElement('tr');
   var td_area_name = document.createElement('td');
   td_area_name.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var area_name_select = document.createElement('select');
   area_name_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   area_name_select.className = 'item_area_name_type item_area_name_type_'+count;
   area_name_select.setAttribute('name','product[area_name_type][]');

   area_name_select.setAttribute('data-id',count);
   
   var area_name_array = self.area_name_array_nsw;
   
   for(var key in area_name_array){
     var option = document.createElement('option');
      option.innerHTML = area_name_array[key];
      option.setAttribute('value',area_name_array[key]);
      option.setAttribute('data-key',key);
        if(JSON.parse(products.item_data).item_zone_classification == area_name_array[key]){
            option.setAttribute('selected',true);
        }
      area_name_select.appendChild(option);
   }
   td_area_name.appendChild(area_name_select);
   
   var td_existing_lighting_qty = document.createElement('td');
   td_existing_lighting_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var existing_lighting_qty_input = document.createElement('input');
   existing_lighting_qty_input.setAttribute('type','text');
   existing_lighting_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   existing_lighting_qty_input.className = 'existing_lighting_qty existing_lighting_qty_'+count;
   existing_lighting_qty_input.setAttribute('name','product[existing_lighting_qty][]');
   self.onload_quantity_removed = parseInt(JSON.parse(products.item_data).item_qty) + parseInt(self.onload_quantity_removed);//console.log(JSON.parse(products.item_data).item_qty);
   existing_lighting_qty_input.setAttribute('value',JSON.parse(products.item_data).item_qty);

   td_existing_lighting_qty.appendChild(existing_lighting_qty_input);

    var td_lamp_ballast_combination = document.createElement('td');
    td_lamp_ballast_combination.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var lamp_ballast_combination_select = document.createElement('select');
    lamp_ballast_combination_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    lamp_ballast_combination_select.className = 'lamp_ballast_combination lamp_ballast_combination_'+count;
    lamp_ballast_combination_select.setAttribute('name','product[lamp_ballast_combination][]');
    lamp_ballast_combination_select.setAttribute('data-id',count);

    var lamp_ballast_combination_array = data.LCP_OLD;

    for(var key in lamp_ballast_combination_array)
    {
        var option = document.createElement('option');
        option.innerHTML = lamp_ballast_combination_array[key].EquipmentClass;
        option.setAttribute('value',lamp_ballast_combination_array[key].EquipmentClass);
        option.setAttribute('data-lcp',lamp_ballast_combination_array[key].LCP1);
        if(key == 0){
            option.setAttribute('selected',true);
        }
        lamp_ballast_combination_select.appendChild(option);
   }
    td_lamp_ballast_combination.appendChild(lamp_ballast_combination_select);
 
   var td_nominal_lamp_power = document.createElement('td');
   td_nominal_lamp_power.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var nominal_lamp_power_input = document.createElement('input');
   nominal_lamp_power_input.setAttribute('type','text');
   nominal_lamp_power_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   nominal_lamp_power_input.className = 'nominal_lamp_power nominal_lamp_power_'+count;
   nominal_lamp_power_input.setAttribute('name','product[nominal_lamp_power][]');
   nominal_lamp_power_input.setAttribute('value',products.nlp);
   td_nominal_lamp_power.appendChild(nominal_lamp_power_input);

    var td_control_device = document.createElement('td');
    td_control_device.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var control_device_select = document.createElement('select');
    control_device_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    control_device_select.className = 'control_device control_device_'+count;
    control_device_select.setAttribute('name','product[control_device][]');
    control_device_select.setAttribute('data-id',count);

    var control_device_array = data.BCA;

    for(var key in control_device_array){
        if(control_device_array[key].SystemList !== undefined){
     var option = document.createElement('option');
      option.innerHTML = control_device_array[key].SystemList;
      option.setAttribute('value',control_device_array[key].SystemList);
      option.setAttribute('data-multiplier',control_device_array[key].ControlMultiplierCM);
      control_device_select.appendChild(option);
  }
    }
    td_control_device.appendChild(control_device_select);

    var td_product_model = document.createElement('td');
    td_product_model.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var product_model_select = document.createElement('select');
    product_model_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    product_model_select.className = 'product_model product_model_'+count;
    product_model_select.setAttribute('name','product[product_model][]');
     product_model_select.setAttribute('data-id',count);
    

    var product_model_array = data.Products;

    for(var key in product_model_array){
        if(product_model_array[key].AppearInCalculator == "Y"){  
         var option = document.createElement('option');
          option.innerHTML = product_model_array[key].Brand + " "+ product_model_array[key].ModelNumber;
          option.setAttribute('value',product_model_array[key].AcceptedLCPValue);
          product_model_select.appendChild(option);
        }
    }
    td_product_model.appendChild(product_model_select);


   var td_upgraded_lighting_qty = document.createElement('td');
   td_upgraded_lighting_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var upgraded_lighting_qty_input = document.createElement('input');
   upgraded_lighting_qty_input.setAttribute('type','text');
   upgraded_lighting_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   upgraded_lighting_qty_input.className = 'upgraded_lighting_qty upgraded_lighting_qty_'+count;
   upgraded_lighting_qty_input.setAttribute('name','product[upgraded_lighting_qty][]');
   upgraded_lighting_qty_input.setAttribute('value',products.qty);
    self.onload_quantity_installed = parseInt(products.qty) + parseInt(self.onload_quantity_installed);
   td_upgraded_lighting_qty.appendChild(upgraded_lighting_qty_input);

   var td_lcp = document.createElement('td');
   td_lcp.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var lcp_input = document.createElement('input');
   lcp_input.setAttribute('type','text');
   lcp_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   lcp_input.className = 'lcp lcp_'+count;
   lcp_input.setAttribute('name','product[lcp][]');

   td_lcp.appendChild(lcp_input);

   var td_control_device_upgrade_lighting = document.createElement('td');
    td_control_device_upgrade_lighting.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var control_device_upgrade_lighting_select = document.createElement('select');
    control_device_upgrade_lighting_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    control_device_upgrade_lighting_select.className = 'control_device_upgrade_lighting control_device_upgrade_lighting_'+count;
    control_device_upgrade_lighting_select.setAttribute('name','product[control_device_upgrade_lighting][]');
    control_device_upgrade_lighting_select.setAttribute('data-id',count);

    var control_device_upgrade_lighting_array = data.BCA;

    for(var key in control_device_upgrade_lighting_array){
        if(control_device_upgrade_lighting_array[key].SystemList !== undefined){
            var option = document.createElement('option');
            option.innerHTML = control_device_upgrade_lighting_array[key].SystemList;
            option.setAttribute('value',control_device_upgrade_lighting_array[key].SystemList);
            option.setAttribute('data-multiplier',control_device_upgrade_lighting_array[key].ControlMultiplierCM);
            control_device_upgrade_lighting_select.appendChild(option);
        }
    }
    td_control_device_upgrade_lighting.appendChild(control_device_upgrade_lighting_select);

    var td_special_occupancy = document.createElement('td');
    td_special_occupancy.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var special_occupancy_select = document.createElement('select');
    special_occupancy_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    special_occupancy_select.className = 'special_occupancy special_occupancy_'+count;
    special_occupancy_select.setAttribute('name','product[special_occupancy][]');
    special_occupancy_select.setAttribute('data-id',count);

    var special_occupancy_array = data.BCA;

    for(var key in special_occupancy_array){
        if(special_occupancy_array[key].Specialccupancy !== undefined){
            var option = document.createElement('option');
            option.innerHTML = control_device_upgrade_lighting_array[key].Specialccupancy;
            option.setAttribute('value',control_device_upgrade_lighting_array[key].Specialccupancy);
            special_occupancy_select.appendChild(option);
        }
    }
    td_special_occupancy.appendChild(special_occupancy_select);

    var td_lcp_low_mode = document.createElement('td');
   td_lcp_low_mode.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var lcp_low_mode_input = document.createElement('input');
   lcp_low_mode_input.setAttribute('type','text');
   lcp_low_mode_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   lcp_low_mode_input.className = 'lcp_low_mode lcp_low_mode_'+count;
   lcp_low_mode_input.setAttribute('name','product[lcp_low_mode][]');
   lcp_low_mode_input.setAttribute('value',products.lcp);
   td_lcp_low_mode.appendChild(lcp_low_mode_input);

   var td_eligible_aoh = document.createElement('td');
   td_eligible_aoh.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var eligible_aoh_input = document.createElement('input');
   eligible_aoh_input.setAttribute('type','text');
   eligible_aoh_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   eligible_aoh_input.className = 'eligible_aoh eligible_aoh_'+count;
   eligible_aoh_input.setAttribute('name','product[eligible_aoh][]');
   eligible_aoh_input.setAttribute('value',$('.item_bca_type_'+count).find(':selected').attr('data-op-hrs-per-yr'));
   //var selectedAreaName = $('[name="product[eligible_aoh][]"]').parent().parent();
   //$('.eligible_aoh_'+count).val(selectedAreaName.find('[name="product[area_name_type][]"]').val());
   
   var space_type_array = data.Space;

   //eligible_aoh_input.setAttribute('value',selectedAreaName.find('.item_area_name_type').val());
   td_eligible_aoh.appendChild(eligible_aoh_input);



   var td_applicable_lifetime = document.createElement('td');
   td_applicable_lifetime.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var applicable_lifetime_input = document.createElement('input');
   applicable_lifetime_input.setAttribute('type','text');
   applicable_lifetime_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   applicable_lifetime_input.className = 'applicable_lifetime applicable_lifetime_'+count;
   applicable_lifetime_input.setAttribute('name','product[applicable_lifetime][]');
   applicable_lifetime_input.setAttribute('value',products.life_time);
   td_applicable_lifetime.appendChild(applicable_lifetime_input);

   var td_pre_upgrade_mwh = document.createElement('td');
   td_pre_upgrade_mwh.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   

   var pre_upgrade_mwh_input = document.createElement('input');
   pre_upgrade_mwh_input.setAttribute('type','text');
   pre_upgrade_mwh_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   pre_upgrade_mwh_input.className = 'pre_upgrade_mwh pre_upgrade_mwh_'+count;
   pre_upgrade_mwh_input.setAttribute('name','product[pre_upgrade_mwh][]');
   td_pre_upgrade_mwh.appendChild(pre_upgrade_mwh_input);

   var td_post_upgrade_mwh = document.createElement('td');
   td_post_upgrade_mwh.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var post_upgrade_mwh_input = document.createElement('input');
   post_upgrade_mwh_input.setAttribute('type','text');
   post_upgrade_mwh_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   post_upgrade_mwh_input.className = 'post_upgrade_mwh post_upgrade_mwh_'+count;
   post_upgrade_mwh_input.setAttribute('name','product[post_upgrade_mwh][]');
   td_post_upgrade_mwh.appendChild(post_upgrade_mwh_input);

   var td_mwh_savings = document.createElement('td');
   td_mwh_savings.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var mwh_savings_input = document.createElement('input');
   mwh_savings_input.setAttribute('type','text');
   mwh_savings_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   mwh_savings_input.className = 'mwh_savings mwh_savings_'+count;
   mwh_savings_input.setAttribute('name','product[mwh_savings][]');
   td_mwh_savings.appendChild(mwh_savings_input);

    var td_installed_power_load = document.createElement('td');
    td_installed_power_load.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var installed_power_load_input = document.createElement('input');
    installed_power_load_input.setAttribute('type','text');
    installed_power_load_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
    installed_power_load_input.className = 'installed_power_load installed_power_load_'+count;
    installed_power_load_input.setAttribute('name','product[installed_power_load][]');
    td_installed_power_load.appendChild(installed_power_load_input);

    tr.appendChild(td_area_name);
    tr.appendChild(td_existing_lighting_qty);
    tr.appendChild(td_lamp_ballast_combination);
    tr.appendChild(td_nominal_lamp_power);
    tr.appendChild(td_control_device);
    tr.appendChild(td_upgraded_lighting_qty);
    tr.appendChild(td_product_model);
    tr.appendChild(td_lcp);
    tr.appendChild(td_control_device_upgrade_lighting);
    tr.appendChild(td_special_occupancy);
    tr.appendChild(td_lcp_low_mode);
    tr.appendChild(td_eligible_aoh);
    tr.appendChild(td_applicable_lifetime);
    tr.appendChild(td_pre_upgrade_mwh);
    tr.appendChild(td_post_upgrade_mwh);
    tr.appendChild(td_mwh_savings);
    tr.appendChild(td_installed_power_load);

self.baseline_product_row_count++;
   
   document.getElementById('baseline_nsw_c').appendChild(tr);
}

calculator_manager_nsw_c.prototype.handle_basic_calculator_details = function(data){
    var self = this;
    
    document.querySelector('#installation_overview_nsw_c').innerHTML = '';
    document.querySelector('#energy_consumer_details_nsw_c').innerHTML = '';
    document.querySelector('#installation_site_address_nsw_c').innerHTML = '';
    var job_details = window.job_manager_tool.job_details;
    
    var tr = document.createElement('tr');
    
    var signed_date = document.createElement('td');
    signed_date.innerHTML = '<input type="text" class="form-control" name="calculator[signed_date]">';
    
    var installation_date = document.createElement('td');
    installation_date.innerHTML = '<input type="text" class="form-control" name="calculator[date_of_poject_completion]">';
    
   
    
    var postcode = document.createElement('td');
    var postcode_select = document.createElement('select');
    postcode_select.className = 'form-control';
    postcode_select.setAttribute('name','calculator[postcode]');
    var postcodes = data.Postcode;
    for(var k in  postcodes){
        var postcode_options = document.createElement('option');
        postcode_options.innerHTML = postcodes[k]['Postcode'];
        postcode_options.setAttribute('value',postcodes[k]['Postcode']);
        postcode_options.setAttribute('data-regional',postcodes[k]['Regional']);
        postcode_options.setAttribute('data-rnf',postcodes[k]['Factor']);
        if(job_details.business_details.postcode == postcodes[k]['Postcode']){
            self.rnf_value = postcodes[k]['Factor'];
            postcode_options.setAttribute('selected',true);
            $('[name="calculator[regional_site]"]').val(postcodes[k]['Regional']);
            $('[name="calculator[rnf]"]').val(postcodes[k]['Factor'])
        }
        postcode_select.appendChild(postcode_options);
    }
    postcode.appendChild(postcode_select);
    
     var rnf = document.createElement('td');
    rnf.innerHTML = '<input type="text" class="form-control" name="calculator[rnf]" value='+self.rnf_value+'>';

    var total_purchase_cost = document.createElement('td');
    total_purchase_cost.innerHTML = '<input type="text" class="form-control" name="calculator[total_purchase_cost]">';
    
    var emptytd = document.createElement('td');
    
    var quantity_removed = document.createElement('td');
    quantity_removed.innerHTML = '<input type="text" class="form-control" name="calculator[quantity_removed]">';
    
    var quantity_installed = document.createElement('td');
    quantity_installed.innerHTML = '<input type="text" class="form-control" name="calculator[quantity_installed]">';
    
    var total_mwh_saving = document.createElement('td');
    total_mwh_saving.innerHTML = '<input type="text" class="form-control" name="calculator[total_mwh_saving]">';
    
    var min_co_payment = document.createElement('td');
    min_co_payment.innerHTML = '<input type="text" class="form-control" name="calculator[min_co_payment]">';
    
    var est_esc_s = document.createElement('td');
    est_esc_s.innerHTML = '<input type="text" class="form-control" name="calculator[est_esc_s]">';
    
    tr.appendChild(signed_date);
    tr.appendChild(installation_date);
    tr.appendChild(postcode);
    tr.appendChild(rnf);
    tr.appendChild(total_purchase_cost);
    tr.appendChild(emptytd);
    tr.appendChild(quantity_removed);
    tr.appendChild(quantity_installed);
    tr.appendChild(total_mwh_saving);
    tr.appendChild(min_co_payment);
    tr.appendChild(est_esc_s);
    
    document.querySelector('#installation_overview_nsw_c').appendChild(tr);
    
    var tr = document.createElement('tr');
    
    var company_name = document.createElement('td');
    company_name.innerHTML = '<input type="text" class="form-control" name="calculator[company_name]" />';
    
    var date_of_poject_completion = document.createElement('td');
    date_of_poject_completion.innerHTML = '<input type="text" class="form-control" name="calculator[date_of_poject_completion]" />';
    
    var signatory_fname = document.createElement('td');
    signatory_fname.innerHTML = '<input type="text" class="form-control" name="calculator[signatory_fname]" />';
    
    var signatory_sname = document.createElement('td');
    signatory_sname.innerHTML = '<input type="text" class="form-control" name="calculator[signatory_sname]" />';
    
    var signatory_phone = document.createElement('td');
    signatory_phone.innerHTML = '<input type="text" class="form-control" name="calculator[signatory_phone]" />';
    
    var signatory_email = document.createElement('td');
    signatory_email.innerHTML = '<input type="text" class="form-control" name="calculator[signatory_email]" />';
    
    tr.appendChild(company_name);
    tr.appendChild(date_of_poject_completion);
    tr.appendChild(signatory_fname);
    tr.appendChild(signatory_sname);
    tr.appendChild(signatory_phone);
    tr.appendChild(signatory_email);
    
    
    document.querySelector('#energy_consumer_details_nsw_c').appendChild(tr);

    var tr = document.createElement('tr');
   
    
    var unit_type = document.createElement('td');
    unit_type.innerHTML = '<input type="text" class="form-control" name="unit_type" />';
    
    var unit_number = document.createElement('td');
    unit_number.innerHTML = '<input type="text" class="form-control" name="unit_number" />';
    
    var level_type = document.createElement('td');
    level_type.innerHTML = '<input type="text" class="form-control" name="level_type" />';
    
    var level_number = document.createElement('td');
    level_number.innerHTML = '<input type="text" class="form-control" name="level_number" />';
    
    var street_number = document.createElement('td');
    street_number.innerHTML = '<input type="text" class="form-control" name="street_number" />';
    
    var street_name = document.createElement('td');
    street_name.innerHTML = '<input type="text" class="form-control" name="street_name" />';
    
    var street_type = document.createElement('td');
    street_type.innerHTML = '<input type="text" class="form-control" name="street_type" />';
    
    var street_type_suffix = document.createElement('td');
    street_type_suffix.innerHTML = '<input type="text" class="form-control" name="street_type_suffix" />';
    
    var installation_suburb = document.createElement('td');
    installation_suburb.innerHTML = '<input type="text" class="form-control" name="calculator[installation_site_suburb]" />';
    
    var installation_suburb_1 = document.createElement('td');
    installation_suburb_1.innerHTML = '<input type="text" class="form-control" name="calculator[installation_site_suburb]" />';
    
    tr.appendChild(unit_type);
    tr.appendChild(unit_number);
    tr.appendChild(level_type);
    tr.appendChild(level_number);
    tr.appendChild(street_number);
    tr.appendChild(street_name);
    tr.appendChild(street_type);
    tr.appendChild(street_type_suffix);
    tr.appendChild(installation_suburb);
    tr.appendChild(installation_suburb_1);
    
    document.querySelector('#installation_site_address_nsw_c').appendChild(tr);
    
};




calculator_manager_nsw_c.prototype.populate_calculator_item_data = function(key,data) {
    var self = this;
    
    var item_data = JSON.parse(data['item_data']);
    self.area_name_array_nsw.push(item_data.item_zone_classification);
  
    if(item_data != null){
        $('.item_room_area_'+key).val(item_data.item_zone_classification);
        $('.item_bca_type_'+key+' option[value="'+item_data.item_zone_classification+'"]').attr("selected", true);
        $('.item_lamp_type_'+key+' option[value="'+item_data.item_lamp_type+'"]').attr("selected", true);        
        $('.item_control_system_'+key+' option[value="'+item_data.item_control_system+'"]').attr("selected", true);
        $('.item_air_con_'+key+' option[value="'+item_data.item_air_con+'"]').attr("selected", true);
    }
    
    $('.item_upgrade_lamp_type'+key+' option[value="LED Integrated Luminaire"]').attr("selected", true);
    if(parseInt(data.type_id) == 7){
        $('.item_upgrade_lamp_type'+key+' option[value="Non-integrated LED lamp with remote driver or ELC"]').attr("selected", true);
    }
    $('.item_name_'+key).val(data.name).attr('data-nlp',data.nlp).attr('data-lifetime',data.life_time);
    
    $('.item_id_'+key).val(data.crm_prd_id);
    $('.item_qty_'+key).val(data.qty.replace(/\D/g,''));
    
    var sensor = parseInt(data.sensor);
    if(sensor == 1){
        $('.item_upgrade_control_system_'+key+' option[value="Occupancy Plus Daylight-linked Control (1-2 luminaires)"]').attr("selected", true);
        $('.item_lcd_activated_'+key+' option[value="Yes"]').attr("selected", true);
    }else{
        $('.item_upgrade_control_system_'+key+' option[value="No Control System"]').attr("selected", true);
        $('.item_lcd_activated_'+key+' option[value="No"]').attr("selected", true);
    }
    
    var bca_types = self.calculator_items['bca_types'];
    // for(var key1 in bca_types){
    //    if(item_data.item_bca_type.trim() == bca_types[key1]['type_of_space'].trim()){
    //        var op_hrs_per_yr = bca_types[key1]['op_hrs_per_yr'];
    //        $('.item_annual_op_hrs_'+key).val(op_hrs_per_yr);
    //    }
    // }
}


calculator_manager_nsw_c.prototype.populate_basline_calculator_data = function(key){
    var self = this;
    
    data_id = key;
    var lcp_old = $('.lamp_ballast_combination_'+data_id+' option:selected').attr('data-lcp');
    var cm_old = $('.control_device_'+data_id+' option:selected').attr('data-multiplier');
    var am = $('.item_air_con_'+data_id+' option:selected').attr('data-am');
    var lifetime = $('.applicable_lifetime_'+data_id).val();
    var eligible_aoh = $('.eligible_aoh_'+data_id).val();
    var existing_qty = $('.existing_lighting_qty_'+data_id).val();
    var pre_upgrade_mwh = lcp_old*cm_old*am*lifetime*eligible_aoh*existing_qty;
    var product_model_value = $('.product_model_'+data_id+' option:selected').val();
    var lcp = product_model_value;
    var cm_new = $('.control_device_upgrade_lighting_'+data_id+' option:selected').attr('data-multiplier');
    var upgrade_qty = $('.upgraded_lighting_qty_'+data_id).val();
    var post_upgrade_mwh = lcp*cm_new*am*lifetime*eligible_aoh*upgrade_qty;
    var rnf = $('[name="calculator[rnf]"]').val();
    var mwh_savings = (pre_upgrade_mwh-post_upgrade_mwh)*rnf;
    var installed_power_load = upgrade_qty*lcp;
    
    self.total_mwh_savings = self.total_mwh_savings+parseFloat(mwh_savings);
    
    $('.lcp_'+data_id).val(lcp);
    $('.pre_upgrade_mwh_'+data_id).val((pre_upgrade_mwh/1000000).toFixed(2));
    $('.post_upgrade_mwh_'+data_id).val((post_upgrade_mwh/1000000).toFixed(2));
    $('.mwh_savings_'+data_id).val(mwh_savings.toFixed(2));
    $('.installed_power_load_'+data_id).val(installed_power_load.toFixed(2));
}


calculator_manager_nsw_c.prototype.create_calculator_row = function(product) {
    var self = this;

    var count = self.default_product_row_count;
    var tr = document.createElement('tr');
    
    var td_room_area = document.createElement('td');
    td_room_area.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
    var room_area_input = document.createElement('input');
    room_area_input.setAttribute('type','text');
    room_area_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
    room_area_input.className = 'item_room_area item_room_area_'+count;
    room_area_input.setAttribute('name','product[item_room_area][]');
    td_room_area.appendChild(room_area_input);

   
   var td_bca = document.createElement('td');
   td_bca.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var bca_select = document.createElement('select');
   bca_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   bca_select.className = 'item_bca_type item_bca_type_'+count;
   bca_select.setAttribute('name','product[item_bca_type][]');
   bca_select.setAttribute('data-id',count);
   
   var bca_types = self.calculator_items['bca_types'];
   for(var key in bca_types){
       var option = document.createElement('option');
       option.innerHTML = bca_types[key]['type_of_space'];
       option.setAttribute('value',bca_types[key]['type_of_space'].trim());
       option.setAttribute('data-op-hrs-per-yr',bca_types[key]['op_hrs_per_yr']);
       
       bca_select.appendChild(option);
   }
   td_bca.appendChild(bca_select);
   
    /* Exisiting Columns */
    
   var td_lamp_type = document.createElement('td');
   td_lamp_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var lt_select = document.createElement('select');
   lt_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   lt_select.className = 'bca_classification bca_classification_'+count;
   lt_select.setAttribute('name','product[bca_classification][]');
   lt_select.setAttribute('data-id',count);
   
   var lamp_types = self.calculator_items['BCA'];
   //console.log("lamp_types",lamp_types)
   
   for(var key in lamp_types){
    if(lamp_types[key].BuildingClassificationList !== undefined){
       var option = document.createElement('option');
       option.innerHTML = lamp_types[key].BuildingClassificationList;
       option.setAttribute('value',$.trim(lamp_types[key].BuildingClassificationList));
       
       lt_select.appendChild(option);
   }
   }
   td_lamp_type.appendChild(lt_select);
   
  var td_activities_taken = document.createElement('td');
  td_activities_taken.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
  var activity_input = document.createElement('input');
  activity_input.setAttribute('type','text');
  activity_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
  activity_input.className = 'item_nom_watts item_nom_watts_'+count;
  activity_input.setAttribute('name','product[item_nom_watts][]');
  activity_input.setAttribute('data-id',count);
   
  td_activities_taken.appendChild(activity_input);
   
   
   var td_control_system = document.createElement('td');
   td_control_system.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var cs_select = document.createElement('select');
   cs_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   cs_select.className = 'item_control_system item_control_system_'+count;
   cs_select.setAttribute('name','product[item_control_system][]');
   cs_select.setAttribute('data-id',count);
   
   var control_systems = self.calculator_items['control_system_with_multiplier'];
   for(var key in control_systems){
       var option = document.createElement('option');
       option.innerHTML = control_systems[key].control_system;
       option.setAttribute('value',control_systems[key].control_system);
       option.setAttribute('data-multiplier',control_systems[key].multiplier);
       cs_select.appendChild(option);
   }
   td_control_system.appendChild(cs_select);
   
   
   var td_air_con = document.createElement('td');
   td_air_con.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ac_select = document.createElement('select');
   ac_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ac_select.className = 'item_air_con item_air_con_'+count;
   ac_select.setAttribute('name','product[item_air_con][]');
   ac_select.setAttribute('data-id',count);
   
   var air_con = self.calculator_items['BCA'];
   for(var key in air_con){
    if(air_con[key].SpaceAirConditioningSystem !== undefined){
       var option = document.createElement('option');
       option.innerHTML = air_con[key].SpaceAirConditioningSystem;
       option.setAttribute('value',air_con[key].SpaceAirConditioningSystem);
       option.setAttribute('data-am',air_con[key].SpaceAirConditioningSystemAM);
       ac_select.appendChild(option);
       }
   }
   td_air_con.appendChild(ac_select);
   
   
   var td_no_of_lamps = document.createElement('td');
   td_no_of_lamps.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var no_of_lamps_input = document.createElement('input');
   no_of_lamps_input.setAttribute('type','text');
   no_of_lamps_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   no_of_lamps_input.className = 'item_existing_no_of_lamps item_existing_no_of_lamps_'+count;
   no_of_lamps_input.setAttribute('name','product[item_existing_no_of_lamps][]');
   no_of_lamps_input.setAttribute('data-id',count);
   
   td_no_of_lamps.appendChild(no_of_lamps_input);
   
   
   var td_activity_type = document.createElement('td');
   td_activity_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var activity_type_select = document.createElement('select');
   activity_type_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   activity_type_select.className = 'item_activity_type item_activity_type_'+count;
   activity_type_select.setAttribute('name','product[item_activity_type][]');
   activity_type_select.setAttribute('data-id',count);
   
   var activity_types = [
       
'General movement and orientation (corridors, walkways)',
'Rough intermittent use (e.g. change rooms, loading bays, storage rooms)',
'Simple tasks and work places (e.g. waiting rooms, rough bench and machine work, entrance halls)',
'Moderately easy tasks',
'Office spaces', 
'Moderately difficult tasks (fine detail)',
'Difficult tasks (very fine detail)',
'Very difficult tasks (e.g. paint retouching, fine manufacturing, colour matching)',
'Extremely difficult tasks (e.g.hand tailoring, extra fine bench work, inspection of dark goods)',
'Exceptionally difficult tasks (e.g. jewellery and watchmaking)',
'OUTDOOR: Assembly, fabrication, manufacture or maintenance',  
'OUTDOOR: Loading and unloading – manual',
'OUTDOOR: Loading and unloading – forklift',
'OUTDOOR: General storage – pedestrian access with through traffic',  
'OUTDOOR: General storage – pedestrian access', 
'OUTDOOR: General storage – no pedestrian access'

];

   for(var key in activity_types){
       var option = document.createElement('option');
       option.innerHTML = activity_types[key];
       option.setAttribute('value',activity_types[key]);
       activity_type_select.appendChild(option);
   }
   td_activity_type.appendChild(activity_type_select);
   
   
   
   /* Upgrade Columns */
   
   var td_space_cleanliness = document.createElement('td');
   td_space_cleanliness.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var space_cleanliness_select = document.createElement('select');
   space_cleanliness_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   space_cleanliness_select.className = 'item_upgrade_lamp_type item_upgrade_lamp_type_'+count;
   space_cleanliness_select.setAttribute('name','product[item_upgrade_lamp_type][]');
   space_cleanliness_select.setAttribute('data-id',count);
   
   var space_cleanliness_type = ['Clean','Nornmal','Dirty'];
   for(var key in space_cleanliness_type){
     var option = document.createElement('option');
      option.innerHTML = space_cleanliness_type[key];
      option.setAttribute('value',space_cleanliness_type[key]);
      space_cleanliness_select.appendChild(option);
   }
   td_space_cleanliness.appendChild(space_cleanliness_select);
   
   var td_product_name = document.createElement('td');
   td_product_name.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px; min-width:100px;');
   
   var product_input = document.createElement('input');
   product_input.setAttribute('type','text');
   product_input.setAttribute('style','padding:5px;  min-width:100px; width:100%; height: inherit;');
   product_input.className = 'item_name item_name_'+count;
   product_input.setAttribute('name','product[item_name][]');
   product_input.setAttribute('data-id',count); 
   
   td_product_name.appendChild(product_input);
   td_product_name.innerHTML += '<input type="hidden" class="item_id item_id_'+count+'" name="product[item_id][]" />';
   
   
   var td_minimum_required_lux = document.createElement('td');
   td_minimum_required_lux.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var minimum_required_lux_input = document.createElement('input');
   minimum_required_lux_input.setAttribute('type','text');
   minimum_required_lux_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   minimum_required_lux_input.className = 'minimum_required_lux minimum_required_lux_'+count;
   minimum_required_lux_input.setAttribute('name','product[minimum_required_lux][]');
   minimum_required_lux_input.setAttribute('data-id',count);
   
   td_minimum_required_lux.appendChild(minimum_required_lux_input);
   
   
   var td_product_qty = document.createElement('td');
   td_product_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_qty_input = document.createElement('input');
   product_qty_input.setAttribute('type','text');
   product_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_qty_input.className = 'item_qty item_qty_'+count;
   product_qty_input.setAttribute('name','product[item_qty][]');
   product_qty_input.setAttribute('data-id',count);
   
   td_product_qty.appendChild(product_qty_input);
   
   
   var td_ipd_allowence = document.createElement('td');
   td_ipd_allowence.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ipd_allowence_input = document.createElement('input');
   ipd_allowence_input.setAttribute('type','text');
   ipd_allowence_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ipd_allowence_input.className = 'ipd_allowence ipd_allowence_'+count;
   ipd_allowence_input.setAttribute('name','product[ipd_allowence][]');
   ipd_allowence_input.setAttribute('data-id',count);
   
   td_ipd_allowence.appendChild(ipd_allowence_input);
   
   var td_wm = document.createElement('td');
   td_wm.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var wm_input = document.createElement('input');
   wm_input.setAttribute('type','text');
   wm_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   wm_input.className = 'wm wm_'+count;
   wm_input.setAttribute('name','product[wm][]');
   wm_input.setAttribute('data-id',count);
   
   td_wm.appendChild(wm_input);
   
   var td_adjustment_factor = document.createElement('td');
   td_adjustment_factor.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var adjustment_factor_input = document.createElement('input');
   adjustment_factor_input.setAttribute('type','text');
   adjustment_factor_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   adjustment_factor_input.className = 'adjustment_factor adjustment_factor_'+count;
   adjustment_factor_input.setAttribute('name','product[adjustment_factor][]');
   adjustment_factor_input.setAttribute('data-id',count);
   
   td_adjustment_factor.appendChild(adjustment_factor_input);
   
   
   var td_annual_op_hrs = document.createElement('td');
   td_annual_op_hrs.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var annual_op_hrs_input = document.createElement('input');
   annual_op_hrs_input.setAttribute('type','text');
   annual_op_hrs_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   annual_op_hrs_input.className = 'item_annual_op_hrs item_annual_op_hrs_'+count;
   annual_op_hrs_input.setAttribute('name','product[item_annual_op_hrs][]');
   annual_op_hrs_input.setAttribute('data-id',count);
   
   td_annual_op_hrs.appendChild(annual_op_hrs_input);
   
   
   var td_no_of_veecs = document.createElement('td');
   td_no_of_veecs.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var no_of_veecs_input = document.createElement('input');
   no_of_veecs_input.setAttribute('type','text');
   no_of_veecs_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   no_of_veecs_input.className = 'item_no_of_veecs item_no_of_veecs_'+count;
   no_of_veecs_input.setAttribute('name','product[item_no_of_veecs][]');
   no_of_veecs_input.setAttribute('data-id',count);
   
   td_no_of_veecs.appendChild(no_of_veecs_input);
   
   tr.appendChild(td_room_area);
   tr.appendChild(td_bca);
   tr.appendChild(td_lamp_type);
   tr.appendChild(td_air_con);
   tr.appendChild(td_activities_taken);
 
   tr.appendChild(td_no_of_lamps);
   
  
   tr.appendChild(td_product_name);
  
   tr.appendChild(td_product_qty);
   tr.appendChild(td_activity_type);
   tr.appendChild(td_space_cleanliness);
   tr.appendChild(td_annual_op_hrs);
   tr.appendChild(td_no_of_veecs);
   tr.appendChild(td_minimum_required_lux);
   tr.appendChild(td_ipd_allowence);
   tr.appendChild(td_wm);
   tr.appendChild(td_adjustment_factor);

   document.getElementById('calculator_item_body').appendChild(tr);


   self.default_product_row_count++;
};

calculator_manager_nsw_c.prototype.save_veecs = function(total_veecs) {
    var self = this;

    $.ajax({
        url: base_url + 'admin/calculator/save_veecs',
        type: 'get',
        dataType: "json",
        data:{'job_uuid' : self.job_uuid, "total_veecs" : total_veecs},
        cahce:false,
        beforeSend:function(){
            
        },
        success: function(stat) {
            if(stat.success){
                // toastr["success"]('VEECs Saved successfully');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
}

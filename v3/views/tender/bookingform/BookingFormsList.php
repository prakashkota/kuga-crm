<?php if(count($tenderBookingForms) > 0) {
    foreach($tenderBookingForms as $eachDocument) { ?>
<tr>
 <td>
        <?php echo $eachDocument['address'] ?></td>
    <td> <?php echo $eachDocument['name']; ?> </td>
   
    <td> <?php echo date('d/m/Y',strtotime($eachDocument['created_at'])); ?> </td>
    <td> 
	<?php if($eachDocument['pdf_file']){
        if(file_exists(__DIR__."/../../../../assets/uploads/tender_booking_form_files/{$eachDocument['pdf_file']}")){
        ?>
	<a class="btn-primary btn-sm mr-2" href="<?php echo site_url("/assets/uploads/tender_booking_form_files/{$eachDocument['pdf_file']}") ?>" target="__blank"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
     <?php } }
    if($eachDocument['uuid'])
    { ?>

        <a class="btn-info btn-sm" target="_blank" href="<?php echo site_url("admin/tender/booking-form/{$eachDocument['uuid']}") ?>"><i class="fa fa-eye"></i> View / Edit</a> </td>


    <?php }?>

</tr>
<?php } ?>
<?php } ?>

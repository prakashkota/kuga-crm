<div class="table-responsive compliance-sec">
<table>
    <thead>
    <tr>
        <th class="bg-grey" width="50%"> Name </th>
        <th class="bg-grey" width="50%"> Action </th>
    </tr>
    </thead>
    <tbody>
<?php if(count($tenderDocuments) > 0) {
    foreach($tenderDocuments as $eachDocument) { ?>
<tr id="docRow_<?php echo $eachDocument['id']; ?>">
    <td> <?php echo $eachDocument['filename']; ?> </td>
    <td><i onclick="javascript:downloadDocument('<?php echo $eachDocument['id']; ?>');" class="fa fa-download padding-left-10"></i> <i onclick="javascript:deleteDocument('<?php echo $eachDocument['id']; ?>');" class="fa fa-trash padding-left-10"></i></td>
</tr>
<?php } ?>
<?php } else {?>
    <tr>
        <td colspan="2" align="center"> No Documents found. </td>
    </tr>
<?php } ?>
    </tbody>
</table>
</div>

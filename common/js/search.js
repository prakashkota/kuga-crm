
var search_manager = function (options) {
    var self = this;
    this.cust_id = '';
    this.cust_data = '';
    this.uuid = (options.uuid) ? options.uuid : '';
    this.user_list_container = document.getElementById('user_list');

    var search_input = document.getElementById('search_input');
    search_input.addEventListener("keyup", function () {
        $('#customer_details').addClass('hidden');
        self.user_list_container.innerHTML = '';
        if (this.value.length < 2)
            return;
        self.search(this.value);
    });

    $('#add_new_customer').click(function () {
        window.location.href = base_url + 'admin/lead/add';
    });

    $('#locationPostCode').change(function () {
        self.auto_assign_franchise();
    });


    self.check_customer_name();
    self.save_customer();
    //self.autocomplete_franchise();

};

search_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

search_manager.prototype.search = function (keyword) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/search/search_users',
        type: 'get',
        data: {
            search_keyword: keyword
        },
        dataType: 'json',
        beforeSend: function (response) {
            self.user_list_container.innerHTML = '';
            $('#user_list').slideDown();
            var item = document.createElement('li');
            item.className = 'list-group-item';
            item.appendChild(createLoader('Searching for user, please wait..'));
            self.user_list_container.appendChild(item);
        },
        success: function (response) {
            if (response.success == true) {
                if (response.user_list.length > 0) {
                    self.user_list_container.innerHTML = '';
                    for (var i = 0; i < response.user_list.length; i++) {
                        var list_item = self.create_search_list_item(response.user_list[i]);
                        self.user_list_container.appendChild(list_item);
                    }
                    $('.user_detail_btn').click(function () {
                        $('#customer_details').removeClass('hidden');
                        var item = $(this).attr('data-item');
                        self.cust_data = JSON.parse(item);
                        self.uuid = self.cust_data.uuid;
                        self.create_customer_details();
                        $('#user_list').slideUp();
                    })
                } else {
                    self.user_list_container.innerHTML = '<li class="text-center list-group-item">No results found matching the search keyword. Click <a href="javascript:void(0);" class="add_new_customer"><i class="fa fa-plus"></i></a> to Add New Customer</li>';
                    $('.add_new_customer').click(function () {
                        window.location.href = base_url + 'admin/lead/add';
                    });

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#user_list').slideUp();
            self.user_list_container.innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

search_manager.prototype.create_search_list_item = function (data) {
    var self = this;

    self.uuid = '';

    var item = document.createElement('li');
    item.className = 'list-group-item';

    var item_header = document.createElement('h4');
    item_header.className = 'list-group-item-heading';

    var item_type_badge = document.createElement('span');
    item_type_badge.className = 'badge badge-primary';
    item_type_badge.innerHTML = 'Customer';

    item_header.innerHTML = data.customer_company_name + ' ';
    //item_header.appendChild(item_type_badge);

    var item_info = document.createElement('div');
    item_info.className = 'search_list_item__info';

    //Address
    var item_address_icon = document.createElement('span');
    item_address_icon.className = 'fa fa-map-marker text-muted m5';

    var item_address = document.createElement('span');
    item_address.className = 'text-muted mr-3';
    item_address.innerHTML = data.address + '<br/>';

    //Contact Number
    var item_contact_icon = document.createElement('span');
    item_contact_icon.className = 'fa fa-phone text-muted m5';

    var item_contact = document.createElement('span');
    item_contact.className = 'text-muted mr-3';
    item_contact.innerHTML = '<a href="tel:'+ data.customer_contact_no +'">' + data.customer_contact_no + '</a><br/>';

    //Email
    var item_email_icon = document.createElement('span');
    item_email_icon.className = 'fa fa-envelope text-muted  m5';

    var item_email = document.createElement('span');
    item_email.className = 'text-muted mr-3';
    item_email.innerHTML = '<a href="mailto:'+ data.customer_email +'">' + data.customer_email + '</a><br/>';
    
    //Sales Rep
    var item_sales_rep_icon = document.createElement('span');
    item_sales_rep_icon.className = 'fa fa-user text-muted  m5';

    var item_sales_rep = document.createElement('span');
    item_sales_rep.className = 'text-muted mr-3';
    item_sales_rep.innerHTML =  data.franchise_name;


    //View Detail 
    var item_detail = document.createElement('span');
    item_detail.className = 'ml-2';

    var item_detail_btn = document.createElement('a');
    item_detail_btn.setAttribute('href', base_url + 'admin/lead/edit/'+data.uuid);
    item_detail_btn.className = 'badge badge-light';
    item_detail_btn.setAttribute('style', 'border:1px solid grey;');

    var item_detail_icon = document.createElement('i');
    item_detail_icon.className = 'fa fa-eye';
    item_detail_icon.setAttribute('style', 'color:#000');
    item_detail_btn.appendChild(item_detail_icon);
    item_detail.appendChild(item_detail_btn);


    
    if(data.address != '' && data.address != 'null' && data.address != null){
        item_info.appendChild(item_address_icon);
        item_info.appendChild(item_address);
    }

    if(data.customer_contact_no != '' && data.customer_contact_no != 'null' && data.customer_contact_no != null){
        item_info.appendChild(item_contact_icon);
        item_info.appendChild(item_contact);
    }
    
    if(data.customer_email != '' && data.customer_email != 'null' && data.customer_email != null){
        item_info.appendChild(item_email_icon);
        item_info.appendChild(item_email);
    }
    
    item_info.appendChild(item_sales_rep_icon);
    item_info.appendChild(item_sales_rep);
    
    
    item_header.appendChild(item_detail);
    item.appendChild(item_header);
    item.appendChild(item_info);

    return item;

};


search_manager.prototype.check_customer_name = function () {
    $(document).on('blur', '#firstname,#lastname', function () {
        var firstname = $("input#firstname").val();
        var lastname = $("input#lastname").val();
        var data = 'firstname=' + firstname + '&lastname=' + lastname;
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/customer/check_unique_customer',
            data: data,
            datatype: 'json',
            success: function (stat) {
                var stat = JSON.parse(stat);
                if (stat.success == false) {
                    $('#checkcompany').html('<div style="color:#fe0000">' + stat.status + '</div>');
                    $("#addNewCompanyBtn").attr("disabled", "disabled");
                } else {
                    $('#checkcompany').html('');
                    $("#addNewCompanyBtn").removeAttr("disabled", "disabled");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

search_manager.prototype.save_customer = function () {
    var self = this;
    $(document).on('click', '#addNewCompanyBtn', function () {
        var flag = true;
        var locationPostCode = $('#locationPostCode').val();
        var locationState = $('#locationState').val();
        var locationstreetAddress = $('#locationstreetAddressVal').val();
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var contactMobilePhone = $('#contactMobilePhone').val();
        var businessId = $('#businessId').val();
        $('#companyNameForm').val(firstname + ' ' + lastname);
        var companyNameForm = $('#companyNameForm').val();
        //Remove invalid
        $('#locationPostCode').removeClass('is-invalid');
        $('#locationState').removeClass('is-invalid');
        $('#locationstreetAddress').removeClass('is-invalid');
        $('#contactMobilePhone').removeClass('is-invalid');
        $('#firstname').removeClass('is-invalid');
        $('#companyNameForm').removeClass('is-invalid');
        $('#businessId').removeClass('is-invalid');

        if (locationPostCode == '') {
            flag = false;
            $('#locationPostCode').addClass('is-invalid');
        }
        if (locationState == '') {
            flag = false;
            $('#locationState').addClass('is-invalid');
        }
        if (locationstreetAddress == '') {
            flag = false;
            $('#locationstreetAddress').addClass('is-invalid');
        }
        if (companyNameForm == '') {
            flag = false;
            $('#companyNameForm').addClass('is-invalid');
        }
        if (contactMobilePhone == '') {
            flag = false;
            $('#contactMobilePhone').addClass('is-invalid');
        }
        if (firstname == '') {
            flag = false;
            $('#firstname').addClass('is-invalid');
        }
        if (businessId == '') {
            flag = false;
            $('#businessId').addClass('is-invalid');
        }
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }

        if (flag) {
            $("#addNewCompanyBtn").attr("disabled", "disabled");
            var customerAddForm = $('#customerAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer',
                datatype: 'json',
                data: customerAddForm + '&action=add',
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        $("#addNewCompanyBtn").removeAttr("disabled");
                        self.cust_id = stat.customer_id;
                        setTimeout(function () {
                            $('#add_new_company_modal').modal('hide');
                        }, 2000);
                        $.ajax({
                            url: base_url + 'admin/search/search_users',
                            type: 'get',
                            data: {
                                id: self.cust_id
                            },
                            dataType: 'json',
                            success: function (response) {
                                if (response.success == true) {
                                    $('#customer_details').removeClass('hidden');
                                    self.cust_data = response.user_list;
                                    self.create_customer_details();
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $('#customer_details').removeClass('hidden');
                                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#addNewCompanyBtn").removeAttr("disabled");
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};


search_manager.prototype.autocomplete_franchise = function () {
    var self = this;
    $(document).on('keydown', '#businessId', function () {
        $('#businessId').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: base_url + "admin/franchise/fetch_franchise_by_keyword",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term,
                        request: 1
                    },
                    success: function (data) {
                        response(data.franchises);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            select: function (event, ui) {
                $('#businessId').val(ui.item.value);
                $('#businessId').val(ui.item.id);
            }
        });
    });
};


search_manager.prototype.create_customer_details = function () {
    var self = this;
    var cust_data = {};
    cust_data.customer_name = self.cust_data.name;
    cust_data.customer_address = self.cust_data.address;
    cust_data.customer_contact_no = self.cust_data.contact_no;
    cust_data.franchise_name = self.cust_data.franchise_name;
    cust_data.franchise_address = self.cust_data.franchise_address;
    cust_data.franchise_contact_no = self.cust_data.franchise_contact_no;

    var customer_details_body = document.getElementById('customer_details_body');
    var list_group = document.createElement('ul');
    list_group.className = "list-group list-group-flush";

    var list_group_item;

    for (var key in cust_data) {
        list_group_item = document.createElement('li');
        list_group_item.className = "list-group-item";
        var item_name = key.split('_');
        var item_name_new = '';
        for (var i = 0; i < item_name.length; i++) {
            if (i == 0) {
                item_name_new = item_name[i].charAt(0).toUpperCase() + item_name[i].slice(1);
            } else {
                item_name_new = item_name_new + " " + item_name[i].charAt(0).toUpperCase() + item_name[i].slice(1);
            }
        }

        list_group_item.innerHTML = "<b>" + item_name_new + "</b>: " + cust_data[key];
        list_group.appendChild(list_group_item);
    }
    //Additional Notes
    var additional_notes_block = document.createElement('div');
    additional_notes_block.className = "mt-3 p-3";

    var additional_notes_heading = document.createElement('h2');
    additional_notes_heading.innerHTML = "Additional Notes";

    var additional_notes_form_group = document.createElement('div');
    additional_notes_form_group.className = "form-group";

    var additional_notes_textarea = document.createElement('textarea');
    additional_notes_textarea.className = "form-control";
    additional_notes_textarea.setAttribute('id', 'activity_note')

    additional_notes_block.appendChild(additional_notes_heading);
    additional_notes_form_group.appendChild(additional_notes_textarea);
    additional_notes_block.appendChild(additional_notes_form_group);

    //Save Note
    var save_notes_block = document.createElement('div');
    save_notes_block.className = "pl-3";

    var save_notes_btn = document.createElement('a');
    save_notes_btn.className = "btn btn-primary";
    save_notes_btn.setAttribute('href', 'javascript:void(0);');
    save_notes_btn.setAttribute('id', 'save_search_activity');
    save_notes_btn.innerHTML = 'Save';

    save_notes_block.appendChild(save_notes_btn);

    customer_details_body.innerHTML = '';
    customer_details_body.appendChild(list_group);
    customer_details_body.appendChild(additional_notes_block);
    customer_details_body.appendChild(save_notes_block);

    $('#save_search_activity').click(function () {
        self.save_search_activity();
    });
};

search_manager.prototype.save_search_activity = function () {
    var self = this;
    var data = {};
    data.cust_data = self.cust_data;
    data.activity_note = $('#activity_note').val();
    data.uuid = (self.uuid && self.uuid != '') ? self.uuid : self.create_uuid();
    $.ajax({
        url: base_url + "admin/search/save_search_activity_data",
        type: 'post',
        dataType: "json",
        data: data,
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

search_manager.prototype.auto_assign_franchise = function () {
    var self = this;
    var flag = true;
    var postcode = $('#locationPostCode').val();
    $('#locationPostCode').removeClass('is-invalid');
    if (postcode == '') {
        flag = false;
        $('#locationPostCode').addClass('is-invalid');
        toastr["error"]('Error! Postcode is required in order to auto-assign franchise.');
    }
    if (flag) {
        $.ajax({
            url: base_url + "admin/customer/franchise_auto_assign",
            type: 'GET',
            dataType: "json",
            data: {postcode: postcode},
            success: function (stat) {
                if (stat.success == true) {
                    self.autopopulate_franchise_list(stat);
                    if (stat.franchise_list.length == 0) {
                        toastr["error"]('Looks like no appropriate franchise found, assigning default franchise SolarRun Mount Waverley.');
                    } else {
                        toastr["success"](stat.status);
                    }
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

search_manager.prototype.autopopulate_franchise_list = function (data) {
    var option;
    var franchise_select = document.getElementById('businessId');
    franchise_select.innerHTML = '';
    if (data.franchise_list.length > 0) {
        var franchise_list = data.franchise_list;
        for (var i = 0; i < franchise_list.length; i++) {
            option = document.createElement('option');
            option.setAttribute('value', data.franchise_list[i].user_id);
            option.innerHTML = data.franchise_list[i].full_name + ' (' + data.franchise_list[i].email + ')';
            if (data.selected_franchise.user_id == data.franchise_list[i].user_id) {
                option.setAttribute('selected', 'selected');
            }
            franchise_select.appendChild(option);
        }
    } else {
        var default_franchise = data.default_franchise;
        option = document.createElement('option');
        option.setAttribute('value', default_franchise.user_id);
        option.innerHTML = default_franchise.full_name + ' (' + default_franchise.email + ')';
        option.setAttribute('selected', 'selected');
        franchise_select.appendChild(option);
    }

}
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tbody>
    <tr>
      <td valign="top">
        <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td valign="top" style="font-family: Arial, sans-serif; font-size: 17px; color: #28a745"><strong>CL</strong></td>
            </tr>
            <tr>
                <td valign="top">
                    <table width="810" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td width="720" valign="top">
                                    <table width="720" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="300" valign="top" style="padding-top: 8px">
                                                    <table width="300" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td width="96" style="font-family: Arial, sans-serif; font-size: 12px; color: #333333">NCBA RESA #</td>
                                                                <td width="10" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333; border-bottom: solid 1px #333333">
                                                                    <input type="text" value="<?= isset($acp_details->ncba_resa) ? $acp_details->ncba_resa : ''; ?>" style="border: none;">
                                                                </td>
                                                                <td align="left" style="font-family: Arial, sans-serif; font-size: 12px; color: #333333">(office use only)</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="420" valign="top" style="padding-top: 8px">
                                                    <table width="420" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td width="420" align="center" style="font-family: Arial, sans-serif; font-size: 21px; color: #333333"><strong>Energy Saver  Nomination Form</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="font-family: Arial, sans-serif; font-size: 15px; color: #28a745">Commercial Lighting Upgrade</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="186" align="right" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/ncba.png" width="186" height="66" alt=""/></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
              <td height="5" style="border-bottom: solid 2px #333333"></td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="26" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">1.</td>
                      <td width="100%" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="213" height="30" style="border-bottom: solid 1px #333333"><strong>ACP Details</strong></td>
                              <td width="631">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="213" height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333; border-bottom: solid 1px #333333">ACP (company):</td>
                              <td width="631" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">National Carbon Bank of Australia Pty Ltd</td>
                            </tr>
                            <tr>
                              <td height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">ABN/ACN: </td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">39 159 474 889</td>
                            </tr>
                            <tr>
                              <td height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #000000;border-bottom: solid 1px #333333">Contact name :</td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #000000;border-bottom: solid 1px #333333">Nick Butler</td>
                            </tr>
                            <tr>
                              <td height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">Email:</td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">compliance@ncba.net.au</td>
                            </tr>
                            <tr>
                              <td height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;">Phone number:</td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">02 9939 5559</td>
                            </tr>
                            <tr>
                              <td height="2" style="border-bottom: solid 2px #333333"></td>
                              <td style="border-bottom: solid 2px #333333"></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="26" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">1.</td>
                      <td width="100%" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="213" height="30" style="border-bottom: solid 1px #333333"><strong>Original Energy Saver  Details</strong></td>
                              <td width="631">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="100" height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333; border-bottom: solid 1px #333333">Corporation/entity name:</td>
                              <td width="631" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333"><input required="" type="text" style="border: 0px;width: 100%;" value="<?php echo $energy_saver_details->entity_name; ?>" /></td>
                            </tr>
                            <tr>
                              <td height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">ABN/ACN: </td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333"><input required="" type="number" style="border: 0px;" value="<?php echo $energy_saver_details->abn; ?>" /></td>
                            </tr>
                            <tr>
                              <td valign="top" colspan="2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>
                                      <td width="255" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 4px #676666">Email: </td>
                                      <td height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 4px #676666">
                                        <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $energy_saver_details->email; ?>" />
                                      </td>
                                      <td width="213" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 4px #676666">Phone number:</td>
                                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 4px #676666">
                                        <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $energy_saver_details->contact_no; ?>" />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <!-- <tr>
                              <td height="2" style="border-bottom: solid 2px #333333"></td>
                              <td style="border-bottom: solid 2px #333333"></td>
                            </tr> -->
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="26" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">3.</td>
                      <td width="100%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="213" height="30" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333"><strong>Implementation  Details</strong></td>
                              <td width="631">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="350" height="30" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 13px; color: #333333;">3.1 Implementation site(s) address:</td>
                              <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 13px; color: #333333;">
                                <p style="border: 0px; border-bottom: 1px solid black; width: 95%; font-size:13px;"><?php echo $certificate_provider_details->address; ?></p>
                              </td>
                            </tr>
                            <tr>
                              <td width="350" height="30" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 13px; color: #333333;">3.2 Energy  saving  activity  implemented at this site(s):</td>
                              <td>
                                <strong>Commercial Lighting Upgrade</strong>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" height="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;">3.3 Past  activities :</td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;">Have you or, to the best of your knowledge,  has any other person,  previously nominated an energy  saver for this activity or activities  at this site(s)? </td>
                            </tr>
                            <tr>
                              <td colspan="2" height="7"></td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">Yes <input <?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '1') { echo "checked";} ?>  type="radio" value="1" /> If yes, please provide a reason for the new nomination form <span style="font-size:13px;border-bottom:solid 1px #333333;"><?php echo $past_activities->reason; ?></span></td>
                            </tr>
                            <tr>
                              <td colspan="2" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">No &nbsp;<input <?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '0'){ echo "checked"; }?> type="radio" value="0" /></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td height="5" style="border-bottom: solid 4px #676666"></td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="26" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">4.</td>
                      <td width="844" height="30" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>Nomination by the Original Energy Saver</strong></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">I, on behalf of the original energy saver, nominate the Accredited Certificate Provider as the energy saver in respect of the activity or activities at the implementation sites listed above. For the purpose of creating energy savings certificates in the ESS, this nomination entitles the Accredited Certificate Provider to create and own all energy savings in respect of each of the above activities at the implementation sites listed above, once they have been completed and energy savings have commenced. </td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">This nomination takes effect from the date this nomination form is signed and remains in force until the registration of the ESCs relevant to the activity(ies) and the date this  nomination is revoked by the original energy saver. </td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">I hereby declare that: </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td valign="top">
                <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td width="26" align="center"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td width="824" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">I am authorised to execute this nomination form on the original energy saver’s behalf. </td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">I have received the contact details of the Accredited Certificate Provider.</td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">Except as set out above, I have not previously nominated an energy saver under the NSW Energy Savings Scheme (ESS) in respect of the activity or activities at the implementation sites listed above and neither, to the best of my knowledge, has any other person.</td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">The original energy saver agrees to provide information regarding the above activities and provide reasonable access to the implementation sites to a representative of the ESS or a member of the ESS Audit Services Panel, upon request.</td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">The date recorded below is the date when I signed this form.</td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">The information provided in this nomination form is accurate and is not misleading by inclusion or omission.</td>
                    </tr>
                    <tr>
                      <td height="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td align="center" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/images/points.png" width="9" height="7" alt=""/></td>
                      <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">I am aware that this nomination form contains information that may be provided to the Scheme Administrator and that there are penalties for providing false or misleading information to the Scheme Administrator.</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
             <td height="5" style="border-bottom: solid 4px #676666"></td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333">Note:</td>
            </tr>
            <tr>
              <td style="font-style: italic;font-family: Arial, sans-serif; font-size: 13px; color: #333333">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading information to the Scheme Administrator. </td>
            </tr>
            <tr>
              <td height="5" style="border-bottom: solid 4px #676666"></td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333"><strong>Signed by  the Original Energy  Saver's authorised signatory</strong></td>
            </tr>
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="50" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333; border-bottom: solid 1px #333333">Signature:</td>
                              <td width="749" height="50" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333; border-bottom: solid 1px #333333">
                                <?php if(isset($form_signatures) && !empty($form_signatures)){ 
									$sign = $form_signatures[count($form_signatures) - 1]; 
								?>
							        <img style="width:200px; height:45px;" src="<?php echo $sign['signature_url']; ?>" width="100px" />
							    <?php } else if(isset($nomination_form_signature) && $nomination_form_signature != ''){ ?>
									<img style="width:200px; height:45px;" src="<?php echo $nomination_form_signature; ?>" width="100px" />
								<?php }else { ?>
							        <?php if(isset($authorised_details->signature)){ ?>
								        <img style="width:200px; height:45px;" src="<?php echo site_url('assets/uploads/led_booking_form_files/'.$authorised_details->signature); ?>" width="100px" />
								    <?php } else { ?>
								        <img style="width:200px; height:45px;" src="https://kugacrm.com.au/assets/uploads/led_booking_form_files/<?php echo $authorised_details->signature; ?>" width="100px" />
								    <?php } ?>
							    <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="50" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">Name:</td>
                              <td style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">
                                <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $authorised_details->name;?>" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="55" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">Position: </td>
                              <td width="150" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">
                                <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $authorised_details->position;?>" />
                              </td>
                              <td width="30" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">Company:</td>
                              <td width="435" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">
                                <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $energy_saver_details->entity_name; ?>" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td width="62" height="30"  style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">Date:</td>
                              <td width="750" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;border-bottom: solid 1px #333333">
                                <input required="" type="text" style="border: 0px; width: 100%;" value="<?php echo $authorised_details->date;?>" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td height="5" style="border-bottom: solid 4px #676666"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="font-family: Arial, sans-serif; font-size: 13px; color: #333333;">NCBA –  Commercial Lighting  – Nomination and Terms  NCBA-NOM Rev G</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<?php
$system_cost = number_format($proposal['system_total'], 0, '.', ',');
$stc_deduction = number_format($proposal['stc_deduction'], 0, '.', ',');

$subtotal = $proposal['system_total'] - $proposal['stc_deduction'];

if ($proposal['quote_total_inGst'] != NULL) {
  $total_payable = number_format($proposal['quote_total_inGst'], 0, '.', ',');
  $gst = $proposal['quote_total_inGst'] - $proposal['quote_total_exGst'];
} else {
  $total_payable = number_format((($proposal['system_total'] * 1.1) - $proposal['stc_deduction']), 0, '.', ',');
  $gst = (($proposal['system_total'] * 1.1) - $proposal['stc_deduction']) - $proposal['quote_total_exGst'];
}

$count = 0;
if (!empty($panel_data)) {
    //$height = $height - 30;
  $count = 1;
}
if (!empty($inverter_data)) {
    //$height = $height - 30;
  $count += 1;
}
if (!empty($battery_data)) {
    //$height = $height - 30;
  $count += 1;
}
if (!empty($proposal_additional_data)) {
    //$height = $height - (count($proposal_additional_data) * 30);
}

$count += count($proposal_additional_data);
$new_count = 6 - $count;

$height = 90;
if($stc_deduction > 0){
  $height = $height - 30;  
}
if($system_cost > 0){
  $height = $height - 30;  
}
$color = 'white';
$ncolor = '#f0f0f0';

$gst = number_format($gst, 0, '.', ',');
$is_vic_rebate = $proposal['is_vic_rebate'];
$vic_rebate = number_format($proposal['vic_rebate'], 0, '.', ',');
$is_vic_loan = $proposal['is_vic_loan'];
$vic_loan = isset($proposal['vic_loan']) ? number_format($proposal['vic_loan'], 0, '.', ',') : 0;

if(!$is_vic_rebate){
  $height = $height + 30;  
}else{
  $height = $height - 30;   
}

$defaultHeight = 0;
?>

<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
<style>
    .font-family{font-family: 'Open Sans', sans-serif;}
    .table-border{border:solid 1px #ccc;}
    .w910{width:910px; }
    body{margin:0px !important;}
</style>
</head>
<body style="padding:0; margin:0">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff" height="1285">
  <tr>
    <td valign="top" style="border: 0; font: 0"><table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td><table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2" height="30"></td>
              </tr>
              <tr>
                <td style="font-family:Arial, Helvetica, sans-serif; font-size:35px; font-weight:900; color:#DE1717;">
            	    <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/header_lines.jpg" width="45" height="40" alt=""/>
                    YOUR <strong style="font-size: 35px; color:#000;">SOLAR SYSTEM AND INCLUSIONS</strong>
                </td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="50"></td>
        </tr>
        <tr>
          <td valign="top"><table width="880" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td width="440" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td style="color:#DE1717; font-size:16px"><strong>Invoice to</strong></td>
                    </tr>
                    <tr>
                      <td height="15"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 15px; color: #000000"><strong><?php echo $customer_data['first_name'] . ' ' . $customer_data['last_name']; ?></strong></td>
                    </tr>
                    <tr>
                      <td height="20" style="font-size: 14px; color: #000000"><?php echo str_ireplace(", Australia", "", $customer_data['address']); ?></td>
                    </tr>
                  </tbody>
                </table></td>
                <td width="440" align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td height="40" align="right" style="font-size: 25px; border-bottom: solid 2px #DE1717"><strong>YOUR QUOTE</strong></td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td width="22"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/calender.jpg" width="16" height="16" alt=""/></td>
                            <td width="300" style="font-size: 15px; color: #000000"><strong>DATE:</strong> <?php echo date('d-m-Y'); ?></td>
                            <td width="22" align="right"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/quote.jpg" width="16" height="16" alt=""/></td>
                            <td width="122" align="right" style="font-size: 15px; color: #000000"><strong>QUOTE ID:</strong> <?php echo $proposal['id']; ?></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td height="18"></td>
                    </tr>
                  </tbody>
                </table></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="25"></td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="880" border="1" bordercolor="#999999" cellspacing="0" cellpadding="7" style="border-collapse:collapse; border:solid 1px #999999">
            <tbody>
              <tr>
                <th width="424" align="left" style="font-size:16px; color: #ffffff; background: #DE1717; text-transform: uppercase"><strong>KUGA SYSTEM</strong></th>
                <th width="422" align="right" style="font-size:16px; color: #ffffff; background: #DE1717;  text-transform: uppercase"><strong>QTY</strong></th>
              </tr>
              <?php if (!empty($panel_data)) {?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $panel_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong><?php echo $no_of_panels; ?></strong></td>
                </tr>
              <?php $defaultHeight = $defaultHeight + 30;} ?>
              <?php if (!empty($inverter_data)) { ?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $inverter_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong>1</strong></td>
                </tr>
              <?php $defaultHeight = $defaultHeight + 30; } ?>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Mounting Systems</strong></td>
                <td align="right" style="font-size:14px; color: #000000"><strong>1 pack</strong></td>
              </tr>
              <?php if (!empty($battery_data)) { ?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $battery_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong>1</strong></td>
                </tr>
              <?php $defaultHeight = $defaultHeight + 30; } ?>
              <?php foreach ($proposal_additional_data as $key => $value) {?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $value['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong><?php echo$value['qty']; ?></strong></td>
                </tr>
              <?php $defaultHeight = $defaultHeight + 30; } ?>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="20"></td>
        </tr>
        <tr>
          <td height="110" align="right" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="7">
            <tbody>

              <?php if($system_cost > 0){ ?>
      				  <!-- <tr>
                  <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>GST AMOUNT</strong></td>
                  <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo number_format(((float) $proposal['system_total']/11), 0, '.', ','); ?></strong></td>
                </tr>
      			    <tr>
                  <td colspan="2" style="padding: 1px !important;"></td>
                </tr> -->
                <tr>
                  <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>PROJECT PRICE</strong></td>
                  <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $system_cost; ?></strong></td>
                </tr>
              <?php } ?>
              <?php if($stc_deduction > 0){ ?>
                <tr>
                  <td colspan="2" style="padding: 1px !important;"></td>
                </tr>
                <tr>
  			          <?php if(isset($saving_calculation_details['postcode_rating']) && !empty($saving_calculation_details['total_system_size'])){
  								  $qty = $saving_calculation_details['postcode_rating'] * $saving_calculation_details['total_system_size'] * 10
  							  ?>
                    <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>LESS STC INCENTIVE QTY <?php echo floor($qty);?></strong></td>
  				        <?php }else{?>
  				          <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>LESS STC INCENTIVE</strong></td>
  				        <?php }?>
                  <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>($<?php echo $stc_deduction; ?>)</strong></td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="2" style="padding: 1px !important;"></td>
              </tr>
              <tr>
                <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>SUB TOTAL</strong></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $subtotal; ?></strong></td>
              </tr>
              <tr>
                <td colspan="2" style="padding: 1px !important;height: 15px;"></td>
              </tr>
              <?php if ($is_vic_rebate) { ?>
                <tr>
                  <td colspan="2" style="padding: 1px !important;"></td>
                </tr>
                <tr>
                  <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>LESS SOLAR VIC REBATE</strong></td>
                  <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $vic_rebate; ?></strong></td>
                </tr>
              <?php } ?>
              <?php if($is_vic_loan){ ?>
			    <tr>
                  <td colspan="2" style="padding: 1px !important;"></td>
                </tr>
                
                <tr>
                  <td width="278" style="background: #4d4d4d; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>LESS SOLAR VIC LOAN</strong></td>
                  <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $vic_loan; ?></strong></td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="2" style="padding: 1px !important;height: 15px;"></td>
              </tr>
              <tr>
                <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>TOTAL PAYABLE</strong> <span style="font-size: 14px">(GST INCLUSIVE)</span></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$ <?php echo $total_payable; ?> </strong></td>
              </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td height="30"></td>
    </tr>
    <tr>
      <td align="left" valign="top" style="border-bottom: solid 1px #000000; padding-bottom: 5px"><table width="880" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="38"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/services.jpg" width="30" height="30" alt=""/></td>
            <td width="842" style="color: #DE1717; font-size: 18px"><strong>Services</strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td height="18"></td>
    </tr>
    <tr>
      <td align="left" valign="top"><table width="880" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="440" height="27" style="font-size: 14px; color: #000000;"><strong>Cable Reticulation</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
          <tr>
            <td height="27" style="font-size: 14px; color: #000000;"><strong>Fixing, Support, Drilling, Waterproofing</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
          <tr>
            <td height="27" style="font-size: 14px; color: #000000;"><strong>Installation</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px;"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td height="30" align="left" valign="top">&nbsp;</td>
    </tr>
    <?php /*for($i = 0; $i < $new_count; $i++) { ?>
        <tr>
            <td height="40" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php } */?>
    <tr>
      <td align="left" valign="top" style="border-bottom: solid 1px #000000; padding-bottom: 5px"><table width="880" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="38"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/warranty.jpg" width="30" height="30" alt=""/></td>
            <td width="842" style="color: #DE1717; font-size: 18px"><strong>Warranty</strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td  height="<?php $height; ?>"></td>
    </tr>
    <tr>
      <td height="27" align="left" style="font-size: 14px; color: #000000;"><strong>Please visit the link below for the Warranty Infromation:</strong></td>
    </tr>
    <tr>
      <td height="20" align="left" style="color: #DE1717; font-size: 14px"><strong><a  style="color: #DE1717; font-size: 14px;text-decoration: none;" href="https://www.13kuga.com.au/warranty-information">https://www.13kuga.com.au/warranty-information/</a></strong></td>
    </tr>
    
    <?php if ($is_vic_rebate && $vic_loan){ $finalHeight = (100 - $defaultHeight);?>
        <tr>
          <td height="<?= $finalHeight ?>" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php }else if ($is_vic_rebate){ $finalHeight = (110 - $defaultHeight);?>
        <tr>
          <td height="<?= $finalHeight ?>" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php }else if ($vic_loan){ $finalHeight = (110 - $defaultHeight);?>
        <tr>
          <td height="<?= $finalHeight ?>" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php }else{ $finalHeight = (120 - $defaultHeight);?>
        <tr>
          <td height="<?= $finalHeight ?>" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php } ?>
    <tr>
      <td height="30" align="left" valign="top" style="width: 40%;">
        <p style="border-left: 3px solid #DE1717;font-size: 13px;width: 54%;padding-left: 10px;">*Solar Victoria Rebate and Interest Free loan is only applicable to eligible 
            households. Rebate and Interest Free Loan is subject to Solar Victoria Terms 
            and Conditions. If not eligible amount payable is Total Payable plus the Solar 
            Victoria Rebate and Interest free Loan amount
        </p>
      </td>
    </tr>
    <tr>
      <td height="15" align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="bottom" style="position:relative;bottom:-90px;">
            <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/footer.jpg" width="100%" alt=""/>
        </td>
    </tr>
  </tbody>
</table></td>
</tr>
</table>
</body>
</html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page19 .back-bg {
		background-color: #FFFFFF;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page19  .side-image img {
		width: 421px;
		margin-top: -35px;
	}
	
	.page19  .vktopimg img {
		width: 421px;
		margin-top: 26px;
		margin-left: 35px;
		box-shadow: 10px 10px 15px 0px #d6d6d6;
	}
	
	.page19  .flexx {
		display: inline-flex;
	}
	
	.page19  .title-why h2 {
		color: #FA9521;
		margin-left: 37px;
		margin-top: 35px;
	}
	
	.page19  .h5color h5 {
		color: #1679BD;
		margin-left: 40px;
		margin-right: 10px;
		text-align: right;
		width: 23%;
		margin-top: 12px;
		font-size: 12px;
	}
	
	.page19  .h5color .pracl {
		width: 70%;
	}
	
	.page19  .pracl img {
		margin: 0;
		width: 45px;
		box-shadow: none;
		display: inline-block;
	}
	
	.page19  .pracl p {
		font-size: 14px;
		padding-left: 11px;
	}
	
	.page19  .h5color {
	    padding-top: 15px;
	}
	
	.page19  .vktopimg {
		margin-top: 26px;
		margin-left: 35px;
	}
	
	.page19  .vktopimg iframe {
		box-shadow: 10px 10px 15px 0px #d6d6d6;
		border: 0;
	}
	</style>
</head>

<body>
	<div class="back-bg page19">
		<div class=""> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/page19header.png'); ?>" width="910px"> </div>
		<div class="flexx">
			<div class="side-image"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/page19side.png'); ?>"> </div>
			<div class="sideright">
				<div class="vktopimg">
					<iframe width="421" height="250" src="https://www.youtube.com/embed/n1P_Wub40LA"> </iframe>
				</div>
				<div class="title-why">
					<h2>WHY SUNPOWER?</h2>
					<div class="h5color" style="display: inline-flex;">
						<h5>TRUSTED <br>DURABILITY</h5>
						<div style="width:10%;"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/1.png'); ?>"> </div>
						<div class="pracl">
							<p>Supported by 35 years of SunPower materials, engineering and manufacturing expertise </p>
						</div>
					</div>
					<div class="h5color" style="display: inline-flex;">
						<h5>PROVEN <br>RELIABILITY</h5>
						<div style="width:10%;"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/2.png'); ?>"> </div>
						<div class="pracl">
							<p>Proven results inreal world conditions, backed by a comprehensive 25 year warranty </p>
						</div>
					</div>
					<div class="h5color" style="display: inline-flex;">
						<h5>MORE LIFETIME <br>ENERGY</h5>
						<div style="width:10%;"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/3.png'); ?>"> </div>
						<div class="pracl">
							<p>Great energy production overa system�s lifetime </p>
						</div>
					</div>
					<div class="h5color" style="display: inline-flex;">
						<h5>INNOVATIVE <br>LEADERSHIP</h5>
						<div style="width:10%;"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/4.png'); ?>"> </div>
						<div class="pracl">
							<p>Peace of mind from demonstrated market leadership & technology innovation </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
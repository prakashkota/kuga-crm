<?php 
		$html = "<tr>
    <td  style=\" background-color:#cc242e;  width:100%;  font-family: 'Montserrat', sans-serif;  font-size: 36px;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Energy Savings</td>
  </tr> 
	
	<tr height=\"60px\">
    <td>&nbsp;</td>
  </tr>
	<tr>
<td>
<table width=\"90%\" align=\"center\">
	<tr>
		<td align=\"left\" valign=\"top\"><table width=\"35%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#000000;\">
		  <tbody><tr>
			<td height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; color:#fff; font-size:14px; \">LIGHTING ENERGY COST (PER YEAR)</td>
		  </tr>
		</tbody>
		</table></td>
	  </tr>
	  <tr>
	  <td>
	   <table width=\"\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: 1px solid black;\">
      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
 <tr>
        <td width=\"50%\"><table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-bottom:1px solid #404041;\">
          <tr>
            <td valign=\"bottom\">
            <table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
              <tr>
                <td align=\"center\" valign=\"bottom\" style=\"font-family: 'Open Sans', sans-serif; font-size:22px; color:#000; line-height:30px;\">$".$before."</td>
              </tr>
              <tr>
                <td valign=\"bottom\">
                	<div style=\"margin:0 auto; width:130px; height:150px; background:#8d8e91; text-align:center; font-size:14px; color:#fff; line-height:30px; font-family: 'Open Sans', sans-serif;\">BEFORE</div>
                
                </td>
              </tr>
            </table>
            </td>
            <td valign=\"bottom\"><table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
              <tr>
                <td align=\"center\" valign=\"bottom\" style=\"font-family: 'Open Sans', sans-serif; font-size:22px; color:#b5232a; line-height:30px;\">$".$after."</td>
              </tr>
              <tr>
                <td valign=\"bottom\">
                <div style=\"font-family: 'Open Sans', sans-serif; margin:0 auto; width:130px; height:50px; background:#b5232a; text-align:center; font-size:14px; color:#fff; line-height:30px; font-family: 'Open Sans', sans-serif;\">AFTER</div>
                
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        <td width=\"50%\" valign=\"top\">
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
          <tr>
            <td width=\"20%\" align=\"left\" valign=\"top\">&nbsp;</td>
            <td width=\"80%\" align=\"left\" valign=\"top\">&nbsp;</td>
          </tr>
          <tr>
            <td width=\"20%\" align=\"right\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/tick.jpg\" alt=\"\" title=\"\" /></td>
            <td width=\"80%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:28px;\">".$calculated_data['energy_reduction_lighting']."% LOWER ENERGY BILL</td>
          </tr>
          <tr>
            <td width=\"20%\" align=\"right\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/tick.jpg\" alt=\"\" title=\"\" /></td>
            <td width=\"80%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:28px;\">NEW HIGH EFFICIENCY LIGHTING</td>
          </tr>
          <tr>
            <td width=\"20%\" align=\"right\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/tick.jpg\" alt=\"\" title=\"\" /></td>
            <td width=\"80%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:16px; color:#000; line-height:28px;\">FAST RETURN ON INVESTMENT</td>
          </tr>
          <tr>
            <td width=\"20%\" align=\"right\" valign=\"top\">&nbsp;</td>
            <td width=\"80%\" align=\"left\" valign=\"top\">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:24px;\">New lighting energy bill</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td align=\"center\"><div style=\"font-family: 'Open Sans', sans-serif; font-size:20px; color:#b5232a; line-height:24px; display:inline-block; border:1px solid #b5232a; padding:5px 20px;\">$".$after."</div></td>
        <td style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; padding: 10px 0px 10px 5px;\">Take control of your power bill by upgrading your  old lighting with energy efficient KUGA LED Lights.</td>
      </tr>
 
      </table>
	  </td>
	  </tr>
	  
	  

	</table>
		
	<tr>
    <td>
	<table width=\"90%\" align=\"center\" >
	<tr>
				<td align=\"left\" valign=\"top\"  height=\"0px\">&nbsp;</td>
			</tr>
	<tr>
		<td align=\"left\" valign=\"top\">
        <table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color:#000000;\">
		  <tbody>
		  <tr>
			<td width=\"30%\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; color:#fff; font-size:14px;\">COST COMPARISON</td>
		  </tr>
		</tbody>
        </table>
		</td>
	  </tr>
	  <tr>
		<td  width=\"100%\" align=\"center\" valign=\"top\" >
		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: 1px solid black;\">
		<tr>
		<td width=\"100%\" align=\"center\" valign=\"top\" colspan=\"2\"><!--<div id=\"curve_chart\"></div>--><img src=\"https://kugacrm.com.au/assets/uploads/pdf_files/chart1_".$led_proposal_data['id'].".png\" /></td>
		</tr>
          <tr>
            <td width=\"50%\" align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:24px;\">Saving from upgrade (5 years)</td>
            <td width=\"50%\" valign=\"top\">&nbsp;</td>
          </tr>
          <tr>
            <td width=\"50%\" align=\"center\" valign=\"top\">
			<div style=\"font-family: 'Open Sans', sans-serif; font-size:20px; color:#b5232a; line-height:24px; display:inline-block; border:1px solid #b5232a; padding:5px 20px;\">$".$Savingsfromupgrade."</div>
			</td>

          <td style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; padding: 10px 0px 10px 5px;\">This is the extra amount that you have to pay on your power bill without any energy upgrade.</td>
		  </tr>
          </table>
		
		</td>
	  </tr>
	  	<tr>
	<td align=\"left\" valign=\"top\">&nbsp;</td>
	</tr>

	</table>
    </td>
    </tr>
</td>
</tr>
    
<tr height=\"60px\">
    <td>&nbsp;</td>
  </tr>
	<tr>
	<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/imagespdf2/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
	</tr>";

echo $html;
			?>

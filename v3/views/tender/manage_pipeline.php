<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />
<style>

    body, html {
        overflow: hidden;
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
    }


    .page-wrapper {background-color: #ECEFF1;}
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
    .sr-inactive_user{color:red;}

    .ui {
        height: 100vh;
        display: grid;
        grid-template-rows: 40px 50px 1fr;
        color: #eee;
    }

    .lists {
        width: 100%;
        display: table;
        border-collapse: collapse;
        table-layout: fixed;
    }

    .lists::-webkit-scrollbar {
        width: 5px;
        height:5px;
    }


    .lists::-webkit-scrollbar-thumb {
        background: #666;
        border-radius: 20px;
    }

    .lists::-webkit-scrollbar-track {
        background: #ddd;
        border-radius: 20px;
    }

    .lists > * {
        flex: 0 0 auto;
        margin-left: 10px;
    }

    .lists::after {
        content: '';
        flex: 0 0 10px;
    }

    .list {
        /** width: 240px; **/
        display: table-cell;
        text-align: left;
        color: #26292c;
        padding: 8px 24px 4px 12px;
        background-color: #f7f7f7;
        background: url(../../assets/images/stage-arrow.png) no-repeat 100% 0,linear-gradient(#fff,#f7f7f7);
        background-size: 20px 100%,100% 100%;
        border-right: 1px solid #b9babb;
        line-height: 1.3em;
        overflow: visible;
        vertical-align: middle;
        width: 100%;
    }



    .list > * {

    }

    .list header {
        line-height: 36px;
        font-weight: bold;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        text-align: left;
        font: 600 16px/32px Open Sans,sans-serif;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        width: 100%;

    }

    .list header.fixed {
        width:inherit;
        position:fixed; 
    }

    .list footer {
        line-height: 36px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        color: #888;
    }

    .status_list {
        display: table-cell;
        border-left: 1px solid;
        border-color: #e5e5e5;
        text-align: left;
        overflow: hidden;
        background-color: transparent;
        transition: background-color .05s ease-in-out;
    }

    .status_list::-webkit-scrollbar {
        width: 1px;
        height:1px;
    }


    .status_list::-webkit-scrollbar-thumb {
        background: transparent;
        border-radius: 20px;
    }

    .status_list::-webkit-scrollbar-track {
        background: transparent;
        border-radius: 20px;
    }

    .status_list__item {
        background-color: #fff;
        padding: 10px;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        cursor:pointer;
    }

    .status_list__item:not(:last-child) {
        /** margin-bottom: 10px; */
    }

    .status_list__item img {
        display: block;
        width: calc(100% + 2 * 10px);
        margin: -10px 0 10px -10px;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }

    .bg-disabled{
        background:#dddddd !important;
    }

    .sr-deal_actions{
        opacity: 0;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: -67px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }

    .lists_data{
        width: 100%;
        height: 100%;
        display: table;
        border-collapse: collapse;
        table-layout: fixed;
    }

    .lists_data__hasScrollbar{
        overflow-x: auto;
        overflow-y: scroll;
        height: 100%;
    }

    .lists_data__hasScrollbar::-webkit-scrollbar {
        width: 1px;
        height:1px;
    }


    .lists_data__hasScrollbar::-webkit-scrollbar-thumb {
        background: transparent;
        border-radius: 20px;
    }

    .lists_data__hasScrollbar::-webkit-scrollbar-track {
        background: transparent;
        border-radius: 20px;
    }

    .flex-wrap{
        margin-bottom: 0px !important;
        /*padding-bottom: 0px !important;*/
    }

    #reporting_container table tr td h2 a i{ position: absolute; right: 0; top: 0; }
    .list header { font-size: 14px; }

    @media only screen and (min-width: 992px){
        .page-wrapper { padding: 3px 0px 0px 100px !important;}
    }

    @media only screen and (max-width: 991px){
        .list{ padding: 8px 5px 4px 5px; }
        .list header{ font-size: 12px; line-height: normal; text-overflow: inherit; white-space: normal; overflow: auto; } 
        #reporting_container table tr td h2{ font-size: 12px; }
        .status_list__item{ padding: 5px; font-size: 80%; }

    }

    .kg-page__heading{display:flex;width: 100%;}
    .kg-page__heading_title{
        font-size: 16px;
        font-weight: 500;
        margin-bottom: .5rem;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.2;
        color: inherit;
        padding: 5px 0 15px;
    }
    .kg-page__heading_action_left{margin-left:10px;}
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .dropdown-menu > li > label {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.428571429;
    }

    .floating_btn--right {
        font-size: 12px;
        text-align: center;
        position: fixed;
        right: 50px;
        bottom: 50px;
        transition: all 0.1s ease-in-out;
        display: none;
    }

    .floating_btn--right:hover {
        box-shadow: 0 6px 14px 0 #666;
        transform: scale(1.05);
    }

    .kg-lead__item__title--green{
        color:rgba(0,128,0, 0.5) !important;
    }

    .kg-lead__item__title--red{
        color:rgba(255, 0, 0, 0.5) !important;
    }

    .bg-light_red {
        background-color:rgba(255, 0, 0, 0.5) !important;
    }

    .bg-light_green {
        background-color: rgba(0,128,0, 0.5) !important;
    }
    
    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 3.375rem;
        text-align: center;
        pointer-events: none;
        color: #000;
    }
    .modal-content {
        width: 700px;
    }
</style>

<div class="page-wrapper d-block clearfix">
    <form id="tender_list_filters">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom pl-5">
            <div class="kg-page__heading">
                <div class="kg-page__heading_title">
                    Manage Tender
                </div> 
                    <?php if($this->aauth->is_member('Admin')){?>
                    <div class="kg-page__heading_action_left">
                        <select class="form-control" name="filter[custom][userid]" id="userid" style="width:150px;">
                            <option value="">Select User</option>
                            <option value="All">All</option>
                            <?php foreach ($users as $key => $value) { ?>
                                <option data-profileId="<?php echo $value['profileId']?>" class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user1 hidden1 text-danger<?php } ?>" <?php if ($value['user_id'] == $user_id) { ?> selected="" <?php } ?> value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                    
                    <div class="form-group has-search ml-5">
                        <span class="fa fa-search form-control-feedback"></span>
                        <input type="text" class="form-control" id="search_tender" placeholder="Search">
                    </div>
                

            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="columns columns-right btn-group pull-right">
                    <select id="columnSelector" multiple="multiple" >
                        <option value="select_all">Select Tender status</option>
                        <?php
                        if (!empty($tenderStatus)) {
                            foreach ($tenderStatus as $key => $value) {
                                ?>
                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>

                    <div class="ml-2">
                        <a class="btn btn-default" style="margin-left:5px;" href="<?php echo site_url('admin/tender/manage-tender?view=list'); ?>">
                            <i class="fa fa-list"></i>
                        </a>
                    </div>

                    <div class="ml-2">
                        <button class="btn btn-default" type="button" id="lead_list_range" >
                            <i class="fa fa-calendar"></i> &nbsp; <span></span> <i class="fa fa-caret-down" ></i>
                        </button>
                        <input type="hidden" name="filter[start_date]" id="start_date" value="" />
                        <input type="hidden" name="filter[end_date]" id="end_date" value="" />
                    </div>


                </div>
            </div>
        </div>
    </form>

    <div id="reporting_container">
        <div class="ui">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div class="lists" id="status">

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                            <div class="lists_data__hasScrollbar">
                                <div  class="lists_data" id="status_data">

                                </div>
                            </div>
                        </td>
                    </tr>   
                </tbody>   
            </table>
        </div>

        <div  class="sr-deal_actions">
            <div class=" col-md-6  sr-deal_actions__options">
                <div class="connectedSortable  sr-deal_actions__options__item  sr-deal_actions__options__item--header bg-success" data-status="won">Won</div>
            </div>
            <div class=" col-md-6  sr-deal_actions__options">
                <div class="connectedSortable  sr-deal_actions__options__item  sr-deal_actions__options__item--header bg-danger" data-status="lost">Lost</div>
            </div>
        </div>
    </div>
    <div class="container hidden" id="error_container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-template">
                    <div class="error-details">
                        Sorry, you don't have permissions to access reports!
                    </div>
                    <div class="error-actions">
                        <a href="proposalManage.php" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-pencil"></span>
                            Manage proposal </a><a href="mailto:service@solarrun.com.au" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="floating_btn--right btn btn-primary" id="load_more_deals"> <i class='fa fa-circle-o-notch fa-spin hidden'></i>  Load More </div>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src='<?php echo site_url(); ?>common/js/tender.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
<script>
    var context = {};
    context.user_id = '<?php echo $user_id; ?>';
    context.user_group = '<?php echo $user_group; ?>';
    context.all_reports_view_permission = '<?php echo $all_reports_view_permission; ?>';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    context.is_lead_allocation_sales_rep = '<?php echo (isset($is_lead_allocation_sales_rep)) ? $is_lead_allocation_sales_rep : FALSE; ?>';
    new tender_manager(context);

    //Notification Update Status to Read
    /* var notification_context = {};
    notification_context.type = 'NEW_TENDER';
    setTimeout(function () {
        notify_manager.change_notification_status(notification_context);
    }, 3000);*/

    /** On Browser Back Button Before Unload Reset Filters */
    $(document).ready(function(){
        <?php if($user_id > 0 ) { ?>
        $('#userid').trigger('change');
        <?php } ?>
    });
    window.addEventListener('beforeunload', function (e) {
        //Reset Select
        document.getElementById("userid").selectedIndex = -1;
        //Reset Checkbox and Radio
        var inputs = document.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type === 'checkbox' && inputs[i].value != 'select_all' && inputs[i].checked == false) {
                inputs[i].click();
            } else if (inputs[i].type === 'radio') {
                inputs[i].checked = false;
            }
        }
    });

</script>


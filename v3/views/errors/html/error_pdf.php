<style>
    @import url('https://fonts.googleapis.com/css?family=Lato|Roboto+Slab');

* {
  position: relative;
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

.centered {
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}

h1 {
  margin-bottom: 30px;
  font-family: 'Lato', sans-serif;
  font-size: 20px;
  display:none;
}

.message {
  display: inline-block;
  line-height: 1.2;
  transition: line-height .2s, width .2s;
  overflow: hidden;
}

.message,
.hidden {
  font-family: 'Roboto Slab', serif;
  font-size: 18px;
}

.hidden {
  color: #FFF;
}
</style>

<section class="centered">
  <h1>PDf Creation Failed :(</h1>
  <div class="container">
    <span class="message">Looks like their was some issue from third-party library while creating the pdf. Please try to perform these steps.</span>
    <br/><br/>
    <ul>
        <li>Try to download it again. If the error still persists, you can contact support or use below link for another method to create pdf.</li>
        <li><a href="<?php echo $pdf_url; ?>" />Download By Method 2</a></li>
    </ul>
  </div>
</section>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" />
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.css">
<link href='<?php echo site_url(); ?>assets/dropzone/dropzone.min.css' type='text/css' rel='stylesheet'>

<style>
    body{overflow: hidden;}
    .capitalize { text-transform:capitalize;}   
    .page-wrapper{background-color:#ECEFF1; min-height:100%; height: 677px; padding:0px;overflow: hidden;}
    #map {height: calc(100%);width: calc(100%);float: right;overflow: hidden; position: fixed !important;}

    .card-sc {
        width: 100%;
        overflow: scroll;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
    }

    div.card-sc::-webkit-scrollbar { 
        width: 0;
        height: 10px !important;
    }

    .kg-lead_card {
        margin-left: 5px;
        margin-right: 5px;
        width: 350px;
        float: left;
        border-radius: 5px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0);
    }

    .kg-lead_card--center {
        position:fixed;
        bottom:25px;
        right:25%;
        left:50%;
        margin-left:-150px;
    }

    .kg-lead_card--white{
        background: #fff;
    }


    .kg-lead_card--yellow{
        border-color: rgba(255,255,102,1) !important;
        background-color: rgba(255,255,102,1) !important;
        box-shadow: inset 4px 0 0 0 #ffff66 !important;
    }

    .kg-lead_card--orange{
        border-color: rgba(255,109,0,1) !important;
        background-color: rgba(255,109,0,1) !important;
        box-shadow: inset 4px 0 0 0 #FF6D00 !important;
    }


    .kg-lead_card--grey{
        border-color: rgba(211,211,211,1) !important;
        background-color: rgba(211,211,211,1) !important;
        box-shadow: inset 4px 0 0 0 #D3D3D3 !important;
    }

    .kg-lead_card--black{
        border-color: rgba(00,00,00,1) !important;
        background-color: rgba(00,00,00,1) !important;
        box-shadow: inset 4px 0 0 0 #000 !important;
    }

    .kg-lead_card--amber{
        border-color: rgba(255, 191, 0, 1) !important;
        background-color: rgba(255, 191, 0, 1) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }

    .kg-lead_card--blue{
        border-color: rgba(49, 122, 226, 1) !important;
        background-color: rgba(49, 122, 226, 1) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }

    .kg-lead_card--green{
        border-color: rgb(0,255,66) !important;
        background-color: rgb(0,255,66) !important;
        box-shadow: inset 4px 0 0 0 #00FF42 !important;
    }

    .kg-lead_card--red{
        border-color: rgba(255, 0, 0, 1) !important;
        background-color: rgba(255, 0, 0, 1) !important;
        box-shadow: inset 4px 0 0 0 #ff0000 !important;
    }

    .kg-lead_card--pink{
        border-color: rgba(255, 192, 203, 1) !important;
        background-color: rgba(255, 192, 203, 1) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }


    .kg-lead_card__title{
        font-family: poppins;
        font-weight: 600;
        margin-left:10px;
        letter-spacing: .01785714em;
        font-family: 'Google Sans',Roboto,Arial,sans-serif;
        font-size: 1.3rem;
        line-height: 1.25rem;
        color: #3c4043;
        max-height: none;
        position: static;
        word-break: break-word;
    }

    .kg-lead_card__body{
        margin-top:5px;
        color:rgba(55,55,55,0.8);
        font-family: poppins;
    }

    .kg-lead_card__title--border{
        margin-left:45px;
        border-bottom: 2px solid grey;
        width:80%;
    }

    .kg-lead_card__title_direction_icon{
        position: fixed;
        margin-left: 195px;
        margin-top: -32px;
        z-index: 200000;
        font-size: 40px;
        color: green;
        font-family: poppins;
    }

    .kg-lead_card__body_actions{
        margin-top:10px;
        display: block;
        background: white;
        color: black;
        font-family: poppins;
    }

    .kg-lead_card__body_address{
        border-bottom: 1px solid rgba(55,55,55,0.2);
        margin-bottom: 5px;
        padding: 10px 0px 10px 0px;
        font-family: poppins;
    }

    .kg-lead_card__body_actions_item{
        width:24%;
        font-size:30px;
        display: inline-block;
        vertical-align: top;
        color: #333;
        text-align: center;
        text-decoration: none;
        cursor:pointer;
    }

    .kg-lead_card__body_actions_item_text{
        font-size: 10px;
        display: block;
        margin: auto;
        border: 0;
    }


    .placeholder__card {
        position:fixed;
        bottom:25px;
        right:25%;
        left:50%;
        height: 100px;
        width: 300px;
        margin-left:-150px;
        background: #fff;
    }

    #card_container {
        display:block;
    }

    .material-icons {color:green;}
    .kg-lead_card__body_actions_item_text {color:green;}
    .kg-lead_card__title{color:#000;}

    .kg-lead_card--pink  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--pink  .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--pink .kg-lead_card__body{color:#fff;}
    .kg-lead_card--pink .kg-lead_card__title{color:#fff;}
    .kg-lead_card--pink .kg-lead_card__title--border{border-bottom:2px solid #fff;}

    .kg-lead_card--red  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--red .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--red .kg-lead_card__body{color:#fff;}
    .kg-lead_card--red .kg-lead_card__title{color:#fff;}
    .kg-lead_card--red .kg-lead_card__title--border{border-bottom:2px solid #fff;}

    .kg-lead_card--black  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--black  .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--black .kg-lead_card__body{color:#fff;}
    .kg-lead_card--black .kg-lead_card__title{color:#fff;}
    .kg-lead_card--black .kg-lead_card__title--border{border-bottom:2px solid #fff;}


    .kg-more_lead_btn{
        position: fixed;
        margin: auto;
        left: 200px;
        right: 0px;
        top: 20px;
        /* bottom: 70%; */
        height: 45px;
        /* width: 175px; */
        display: none;
        font-weight: 700;
    }

    .kg-search_filter__block{
        position: fixed;
        /**width: 400px;*/
        z-index: 1;
        margin: 20px 0px 0px 135px;
    }

    @media only screen and (min-width: 768px) {
        #card_container {
            margin-left:300px;
        }
    }


    @media only screen and (max-width: 991px) {
        .kg-search_filter__block{
            position: fixed;
            /**width: 400px;*/
            z-index: 1;
            margin: 8px 0px 0px 15px;
        }
        .page-wrapper{margin-top: 57px;}

       /** .gm-iv-address {
            margin-top: 60px;
        }

        .gm-iv-address-link{
            margin-top: 40px;
        }

        .gm-iv-container,.gm-iv-marker{
            margin-top: 60px;
        }*/
        
       .kg-more_lead_btn{
            position: fixed;
            margin: auto;
            left: 15px;
            right: 0px;
            top: 115px;
            /* bottom: 70%; */
            height: 45px;
            /* width: 175px; */
            display: none;
            font-weight: 700;
        }   
    }

    .kgsticky-toolbar {
        width: 46px;
        position: fixed;
        top: 0;
        right: 0;
        list-style: none;
        padding: 5px 0;
        margin: 0;
        z-index: 50;
        background: #fff;
        -webkit-box-shadow: 0px 0px 50px 0px rgba(82,63,105,0.15);
        box-shadow: 0px 0px 50px 0px rgba(82,63,105,0.15);
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        border-radius: 3px 0 0 3px;
    }

    .kgsticky-toolbar .kgsticky-toolbar__item {
        margin: 0;
        padding: 5px;
        text-align: center;
    }

    .kg_lead_card__close_btn{ 
        cursor:pointer;
    }

    .dropdown.dropdown-lg .dropdown-menu {
        margin-top: -1px;
        padding: 6px 20px;
        width: 350px;
    }
    .input-group-btn .btn-group {
        display: flex !important;
    }
    .btn-group .btn {
        border-radius: 0;
        margin-left: -1px;
    }
    .btn-group .btn:last-child {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }
    .btn-group .form-horizontal .btn[type="submit"] {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    .form-horizontal .form-group {
        margin-left: 0;
        margin-right: 0;
    }
    .form-group .form-control:last-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    @media screen and (min-width: 768px) {
        #adv-search {
            width: 500px;
            margin: 0 auto;
        }
        .dropdown.dropdown-lg {
            position: static !important;
        }
        .dropdown.dropdown-lg .dropdown-menu {
            min-width: 500px;
            margin-left:130px;
        }
    }

    .form-block{margin-bottom: 0px !important;}
    .dropzone{
        border-radius: 10px;
    }

    .dropzone .dz-message {
        text-align: center;
        margin: 3em 0;
    }

    .floating_btn--right {
        font-size: 12px;
        text-align: center;
        position: fixed;
        right: 50px;
        bottom: 50px;
        transition: all 0.1s ease-in-out;
    }

    .floating_btn--right:hover {
        box-shadow: 0 6px 14px 0 #666;
        transform: scale(1.05);
    }
    
    .btn{-webkit-border-radius:0px;}
    
    .btn_filter{width:45px;height:45px;padding:5px;}
    
    @media only screen and (min-width: 992px){
        .modal-content {
            width: 450px;
        }
    }

    .device_history_markers{
        max-height:462px; 
        overflow:auto;
        top:50px;
        position: fixed;
        right:0;
        padding: 10px;
        background: beige;
        display: inline;
        margin-right:10px;
        width: 230px;
    }

    @media only screen and (max-width: 768px){
        .device_history_markers{
            max-height: 80px !important;
            top: 107px !important;
        }
    }


</style>
<div class="page-wrapper d-block clearfix">
    <div class="kg-search_filter__block">
        <a href="javascript:void(0);" class="btn btn-primary text-white btn_filter" data-toggle="modal" data-target="#deal_map_filter_modal"><span class="fa fa-filter"></span></a>
    </div>

    <div id="map"></div>
    <div class="card-sc">
        <div class="card-group" id="card_container">
        </div>
        <div class="card-group" id="card_container_placeholder">
            <div class="placeholder__card card">
                <div class="bars">
                    <div class="bar bar1 loading"></div>
                    <div class="bar bar2 loading"></div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary kg-more_lead_btn" id="load_more_btn">Load More Deals</button>
    </div>

    <div id="infowindow-content">
        <img id="place-icon" src="" height="16" width="16">
        <span id="place-name"  class="title"></span><br>
        <span id="place-address"></span>
        <div id="place-add-deal" class="mt-3"></div>
    </div>


    <div class="device_history_markers hidden">
        <table id="marker_list" border="0" style="">

        </table>
    </div>
</div>

<div class="modal fade" id="deal_map_filter_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title" id="add_customer_location_modal_title">Filters</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="input-group">  
                                    <select id="search_history_by" style="height: 45px;" class="form-control border-secondary border-right-0 rounded-0">
                                        <option value="">Location History</option>
                                        <option value="1">1 Hour</option>
                                        <option value="2">2 Hours</option>
                                        <option value="5">5 Hours</option>
                                        <option value="10">10 Hours</option>
                                        <option value="16">16 Hours</option>
                                        <option value="24">24 Hours</option>
                                        <!-- <option value="-1">Enter a Date</option> -->
                                        <option value="-2">Enter Date Range</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group hidden" id="date_pciker">
                                <input class="form-control" type="text" name="date" value="" />
                            </div>
                            <div class="form-group hidden" id="daterange_pciker">
                                <input class="form-control" type="text" name="daterange" value="" />
                            </div>
                            <button type="button" class="btn btn-primary border-secondary border-right-0 rounded-0 text-center" id="search_history_btn"><i class="fa fa-search"></i> Show</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo site_url(); ?>assets/dropzone/dropzone.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing,geocode" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/placecomplete.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/marker_cluster.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/activity.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/tracking.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    var ac_context = {};
    ac_context.view = 'map';
    ac_context.lead_data = '';
    var activity_manager_tool = new activity_manager(ac_context);

    var context = {};
    context.user_type = '<?php echo isset($user_details) ? $user_details['group_id'] : ''; ?>';
    
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.logged_in_user_data = '<?php echo (isset($logged_in_user_data) && !empty($logged_in_user_data)) ? json_encode($logged_in_user_data, JSON_HEX_APOS) : ''; ?>';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    var tracking_tool = new tracking_manager(context);
    tracking_tool.initialize_map();
</script>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SolarRunApp FormBuilder</title>
        <!-- LOADING STYLESHEETS -->
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/admin/font1.css'); ?>" />
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/admin/style.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/admin/responsive.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/admin/custom.css?v='.version); ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/vendor/dist/css/oneui.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/vendor/plugins/sweetalert/sweetalert.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo site_url('assets/form_builder/css/jquery-ui.css'); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/toastr.css" />
        <style>
            .navigation{margin-top:0px;}

            #navbar {
                overflow: hidden;
                background-color: #099;
                position: fixed;
                top: 0;
                width: 100%;
                padding-top: 3px;
                padding-bottom: 3px;
                padding-left: 20px;
                display: none;
            }
            #navbar a {
                float: left;
                display: block;
                color: #666;
                text-align: center;
                padding-right: 20px;
                text-decoration: none;
                font-size: 17px;
            }
            #navbar a:hover {
                background-color: #ddd;
                color: black;
            }
            #navbar a.active {
                background-color: #4CAF50;
                color: white;
            }
            .main {
                padding: 16px;
                margin-top: 30px;
                width: 100%;
                height: 100vh;
                overflow: auto;
                cursor: grab;
                cursor: -o-grab;
                cursor: -moz-grab;
                cursor: -webkit-grab;
            }
            .main img {
                height: auto;
                width: 100%;
            }
            .button {
                width: 300px;
                height: 60px;
            }   
            .form-input-group input:focus,.form-input-group textarea:focus,.focused{
                cursor: inherit;
                border: 2px solid #009ef4;
                box-shadow: 0 8px 32px rgba(0, 0, 0, 0.2);
            }
            #sidebar.opened + #page-container {
                left: 0 !important;
            }
            select{
                height: inherit;
            }
       
            tfoot{display: none;}
            .loader {
                border: 5px solid #f3f3f3;
                -webkit-animation: spin 1s linear infinite;
                animation: spin 1s linear infinite;
                border-top: 5px solid #555;
                border-radius: 50%;
                width: 50px;
                height: 50px;
                margin: 0 auto;
                text-align:center;
                    position: initial;
            }


            @-webkit-keyframes spin {
                0% { 
                    -webkit-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    transform: rotate(0deg);
                }

                100% {
                    -webkit-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }

            @keyframes spin {
                0% { 
                    -webkit-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    transform: rotate(0deg);
                }

                100% {
                    -webkit-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }
        </style>
        <script>
            var base_url = '<?php echo site_url(); ?>';
            var baseurl = '<?php echo site_url('assets/form_builder/'); ?>';
            
        </script>
    </head>
    <body>
        <div class="hdr-btm full-wdth clearfix" style="top:0px !important;">
            <div class="container-fluid"> 
                <a class="pull-left cmn-btn-top back-btn" href="javascript: history.go(-1)">
                    <img src="<?php echo site_url('assets/form_builder/'); ?>img/back-arw.png" alt=""/> Back to Admin
                </a>
            </div>
        </div>
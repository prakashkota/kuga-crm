<html>

<head>
    <title></title>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }
        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }
        body{
            font-family: 'CenturyGothic';
            margin: 0;
            padding: 0;
        }
        .page14 .back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page14 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page14 .top hr {
            width: 65%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page14 .text_color {
            color: #D01E2A;
        }

        .page14 .title {
            margin-top: -50px;
        }

        .page14 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page14 .title h2 {
            padding: 5px 10px;
            margin-left: 48px;
            margin-top: 100px;
            background-color: #D01E2A;
            width: 50%;
            color: #ffffff;
            background: linear-gradient(125deg, #D01E2A 87%, transparent 15%);
            letter-spacing: 3px;
            font-size: 20px;

            display: flex;
            align-items: center;
        }

        .page14 .footer {
            display: inline-flex;
        }

        .page14 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page14 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page14 .enimage img {
            margin-top: 15px;
            margin-left: 50px;
        }

        .page14 .fimagee img {
            margin-left: 730px;
            margin-top: 33px;
        }

        #avg_load_area_chart2 {
            margin-left: 2rem;
            margin-top: 3rem;
        }

        #avg_load_area_chart3 {
            margin-left: 2rem;
            margin-top: 3rem;
        }
    </style>
</head>

<body>
    <div class="back-bg page14">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
            </div>
            <div class="title">
                <h1>ENERGY <span class="text_color"> CONSUMPTION</span></h1>
                <h2>
                    <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g id="Icon">
                            <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                            <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                            <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                        </g>
                    </svg>
                    AVERAGE MONTHLY
                </h2>
            </div>
            <div class=" enimage">
                <div id="avg_load_area_chart2"></div>
            </div>
            <div class="title">
                <h2>
                    <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g id="Icon">
                            <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                            <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                            <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                        </g>
                    </svg>
                    AVERAGE WEEK DAY
                </h2>
            </div>
            <div class=" enimage">
                <div id="avg_load_area_chart3"></div>
            </div>
            <div class="fimagee">
                <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
            </div>
            <div class="footer">
                <div class="redf">
                </div>
                <div class="greyf">
                </div>
            </div>
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script>
            var graph_data = <?php echo $saving_meter_calculation; ?>;

            $(document).ready(function() {
                console.log(graph_data);

                show_graph(graph_data.avg_monthly_load_graph_data);
                weekend_graph(graph_data.weekend_load_graph_data);
            })

            function show_graph(avg_load_graph_data) {
                var labels = {};
                Highcharts.chart('avg_load_area_chart2', {
                    chart: {
                        width: 780,
                        height: 360,
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    title: {
                        style: {
                            display: 'none'
                        }
                    },
                    xAxis: {
                        title: {
                            text: 'Hours',
                            style: {
                                "fontSize": "16px",
                                "color": '#000',
                                "fontWeight": 'bold'
                            }
                        },
                        type: 'datetime',
                        tickInterval: 3780 * 1000,
                        labels: {
                            formatter: function() {
                                return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                            },
                        },
                    },
                    yAxis: {
                        allowDecimals: true,
                        title: {
                            text: 'POWER',
                            style: {
                                "fontSize": "16px",
                                "color": '#000',
                                "fontWeight": 'bold'
                            }
                        },
                        labels: {
                            formatter: function() {
                                return this.value + 'kW';
                            }
                        }
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        x: 0,
                        y: 0,
                        itemStyle: {
                            "fontSize": "9px",
                            "color": '#666',
                            "fontWeight": 'normal'
                        }
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                        }
                    },
                    plotOptions: {
                        area: {
                            pointStart: 1940,
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 3,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        },
                        series: {
                            pointStart: Date.UTC(2016, 0, 17),
                            pointInterval: 1 * 3600 * 1000
                        }
                    },
                    series: [{
                        name: 'Jan',
                        data: [
                        avg_load_graph_data['jan'][0],
                        avg_load_graph_data['jan'][1],
                        avg_load_graph_data['jan'][2],
                        avg_load_graph_data['jan'][3],
                        avg_load_graph_data['jan'][4],
                        avg_load_graph_data['jan'][5],
                        avg_load_graph_data['jan'][6],
                        avg_load_graph_data['jan'][7],
                        avg_load_graph_data['jan'][8],
                        avg_load_graph_data['jan'][9],
                        avg_load_graph_data['jan'][10],
                        avg_load_graph_data['jan'][11],
                        avg_load_graph_data['jan'][12],
                        avg_load_graph_data['jan'][13],
                        avg_load_graph_data['jan'][14],
                        avg_load_graph_data['jan'][15],
                        avg_load_graph_data['jan'][16],
                        avg_load_graph_data['jan'][17],
                        avg_load_graph_data['jan'][18],
                        avg_load_graph_data['jan'][19],
                        avg_load_graph_data['jan'][20],
                        avg_load_graph_data['jan'][21],
                        avg_load_graph_data['jan'][22],
                        avg_load_graph_data['jan'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Feb',
                        data: [
                        avg_load_graph_data['feb'][0],
                        avg_load_graph_data['feb'][1],
                        avg_load_graph_data['feb'][2],
                        avg_load_graph_data['feb'][3],
                        avg_load_graph_data['feb'][4],
                        avg_load_graph_data['feb'][5],
                        avg_load_graph_data['feb'][6],
                        avg_load_graph_data['feb'][7],
                        avg_load_graph_data['feb'][8],
                        avg_load_graph_data['feb'][9],
                        avg_load_graph_data['feb'][10],
                        avg_load_graph_data['feb'][11],
                        avg_load_graph_data['feb'][12],
                        avg_load_graph_data['feb'][13],
                        avg_load_graph_data['feb'][14],
                        avg_load_graph_data['feb'][15],
                        avg_load_graph_data['feb'][16],
                        avg_load_graph_data['feb'][17],
                        avg_load_graph_data['feb'][18],
                        avg_load_graph_data['feb'][19],
                        avg_load_graph_data['feb'][20],
                        avg_load_graph_data['feb'][21],
                        avg_load_graph_data['feb'][22],
                        avg_load_graph_data['feb'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Mar',
                        data: [
                        avg_load_graph_data['mar'][0],
                        avg_load_graph_data['mar'][1],
                        avg_load_graph_data['mar'][2],
                        avg_load_graph_data['mar'][3],
                        avg_load_graph_data['mar'][4],
                        avg_load_graph_data['mar'][5],
                        avg_load_graph_data['mar'][6],
                        avg_load_graph_data['mar'][7],
                        avg_load_graph_data['mar'][8],
                        avg_load_graph_data['mar'][9],
                        avg_load_graph_data['mar'][10],
                        avg_load_graph_data['mar'][11],
                        avg_load_graph_data['mar'][12],
                        avg_load_graph_data['mar'][13],
                        avg_load_graph_data['mar'][14],
                        avg_load_graph_data['mar'][15],
                        avg_load_graph_data['mar'][16],
                        avg_load_graph_data['mar'][17],
                        avg_load_graph_data['mar'][18],
                        avg_load_graph_data['mar'][19],
                        avg_load_graph_data['mar'][20],
                        avg_load_graph_data['mar'][21],
                        avg_load_graph_data['mar'][22],
                        avg_load_graph_data['mar'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Apr',
                        data: [
                        avg_load_graph_data['apr'][0],
                        avg_load_graph_data['apr'][1],
                        avg_load_graph_data['apr'][2],
                        avg_load_graph_data['apr'][3],
                        avg_load_graph_data['apr'][4],
                        avg_load_graph_data['apr'][5],
                        avg_load_graph_data['apr'][6],
                        avg_load_graph_data['apr'][7],
                        avg_load_graph_data['apr'][8],
                        avg_load_graph_data['apr'][9],
                        avg_load_graph_data['apr'][10],
                        avg_load_graph_data['apr'][11],
                        avg_load_graph_data['apr'][12],
                        avg_load_graph_data['apr'][13],
                        avg_load_graph_data['apr'][14],
                        avg_load_graph_data['apr'][15],
                        avg_load_graph_data['apr'][16],
                        avg_load_graph_data['apr'][17],
                        avg_load_graph_data['apr'][18],
                        avg_load_graph_data['apr'][19],
                        avg_load_graph_data['apr'][20],
                        avg_load_graph_data['apr'][21],
                        avg_load_graph_data['apr'][22],
                        avg_load_graph_data['apr'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'May',
                        data: [
                        avg_load_graph_data['may'][0],
                        avg_load_graph_data['may'][1],
                        avg_load_graph_data['may'][2],
                        avg_load_graph_data['may'][3],
                        avg_load_graph_data['may'][4],
                        avg_load_graph_data['may'][5],
                        avg_load_graph_data['may'][6],
                        avg_load_graph_data['may'][7],
                        avg_load_graph_data['may'][8],
                        avg_load_graph_data['may'][9],
                        avg_load_graph_data['may'][10],
                        avg_load_graph_data['may'][11],
                        avg_load_graph_data['may'][12],
                        avg_load_graph_data['may'][13],
                        avg_load_graph_data['may'][14],
                        avg_load_graph_data['may'][15],
                        avg_load_graph_data['may'][16],
                        avg_load_graph_data['may'][17],
                        avg_load_graph_data['may'][18],
                        avg_load_graph_data['may'][19],
                        avg_load_graph_data['may'][20],
                        avg_load_graph_data['may'][21],
                        avg_load_graph_data['may'][22],
                        avg_load_graph_data['may'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Jun',
                        data: [
                        avg_load_graph_data['jun'][0],
                        avg_load_graph_data['jun'][1],
                        avg_load_graph_data['jun'][2],
                        avg_load_graph_data['jun'][3],
                        avg_load_graph_data['jun'][4],
                        avg_load_graph_data['jun'][5],
                        avg_load_graph_data['jun'][6],
                        avg_load_graph_data['jun'][7],
                        avg_load_graph_data['jun'][8],
                        avg_load_graph_data['jun'][9],
                        avg_load_graph_data['jun'][10],
                        avg_load_graph_data['jun'][11],
                        avg_load_graph_data['jun'][12],
                        avg_load_graph_data['jun'][13],
                        avg_load_graph_data['jun'][14],
                        avg_load_graph_data['jun'][15],
                        avg_load_graph_data['jun'][16],
                        avg_load_graph_data['jun'][17],
                        avg_load_graph_data['jun'][18],
                        avg_load_graph_data['jun'][19],
                        avg_load_graph_data['jun'][20],
                        avg_load_graph_data['jun'][21],
                        avg_load_graph_data['jun'][22],
                        avg_load_graph_data['jun'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Jul',
                        data: [
                        avg_load_graph_data['jul'][0],
                        avg_load_graph_data['jul'][1],
                        avg_load_graph_data['jul'][2],
                        avg_load_graph_data['jul'][3],
                        avg_load_graph_data['jul'][4],
                        avg_load_graph_data['jul'][5],
                        avg_load_graph_data['jul'][6],
                        avg_load_graph_data['jul'][7],
                        avg_load_graph_data['jul'][8],
                        avg_load_graph_data['jul'][9],
                        avg_load_graph_data['jul'][10],
                        avg_load_graph_data['jul'][11],
                        avg_load_graph_data['jul'][12],
                        avg_load_graph_data['jul'][13],
                        avg_load_graph_data['jul'][14],
                        avg_load_graph_data['jul'][15],
                        avg_load_graph_data['jul'][16],
                        avg_load_graph_data['jul'][17],
                        avg_load_graph_data['jul'][18],
                        avg_load_graph_data['jul'][19],
                        avg_load_graph_data['jul'][20],
                        avg_load_graph_data['jul'][21],
                        avg_load_graph_data['jul'][22],
                        avg_load_graph_data['jul'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Aug',
                        data: [
                        avg_load_graph_data['aug'][0],
                        avg_load_graph_data['aug'][1],
                        avg_load_graph_data['aug'][2],
                        avg_load_graph_data['aug'][3],
                        avg_load_graph_data['aug'][4],
                        avg_load_graph_data['aug'][5],
                        avg_load_graph_data['aug'][6],
                        avg_load_graph_data['aug'][7],
                        avg_load_graph_data['aug'][8],
                        avg_load_graph_data['aug'][9],
                        avg_load_graph_data['aug'][10],
                        avg_load_graph_data['aug'][11],
                        avg_load_graph_data['aug'][12],
                        avg_load_graph_data['aug'][13],
                        avg_load_graph_data['aug'][14],
                        avg_load_graph_data['aug'][15],
                        avg_load_graph_data['aug'][16],
                        avg_load_graph_data['aug'][17],
                        avg_load_graph_data['aug'][18],
                        avg_load_graph_data['aug'][19],
                        avg_load_graph_data['aug'][20],
                        avg_load_graph_data['aug'][21],
                        avg_load_graph_data['aug'][22],
                        avg_load_graph_data['aug'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Sep',
                        data: [
                        avg_load_graph_data['sep'][0],
                        avg_load_graph_data['sep'][1],
                        avg_load_graph_data['sep'][2],
                        avg_load_graph_data['sep'][3],
                        avg_load_graph_data['sep'][4],
                        avg_load_graph_data['sep'][5],
                        avg_load_graph_data['sep'][6],
                        avg_load_graph_data['sep'][7],
                        avg_load_graph_data['sep'][8],
                        avg_load_graph_data['sep'][9],
                        avg_load_graph_data['sep'][10],
                        avg_load_graph_data['sep'][11],
                        avg_load_graph_data['sep'][12],
                        avg_load_graph_data['sep'][13],
                        avg_load_graph_data['sep'][14],
                        avg_load_graph_data['sep'][15],
                        avg_load_graph_data['sep'][16],
                        avg_load_graph_data['sep'][17],
                        avg_load_graph_data['sep'][18],
                        avg_load_graph_data['sep'][19],
                        avg_load_graph_data['sep'][20],
                        avg_load_graph_data['sep'][21],
                        avg_load_graph_data['sep'][22],
                        avg_load_graph_data['sep'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Oct',
                        data: [
                        avg_load_graph_data['oct'][0],
                        avg_load_graph_data['oct'][1],
                        avg_load_graph_data['oct'][2],
                        avg_load_graph_data['oct'][3],
                        avg_load_graph_data['oct'][4],
                        avg_load_graph_data['oct'][5],
                        avg_load_graph_data['oct'][6],
                        avg_load_graph_data['oct'][7],
                        avg_load_graph_data['oct'][8],
                        avg_load_graph_data['oct'][9],
                        avg_load_graph_data['oct'][10],
                        avg_load_graph_data['oct'][11],
                        avg_load_graph_data['oct'][12],
                        avg_load_graph_data['oct'][13],
                        avg_load_graph_data['oct'][14],
                        avg_load_graph_data['oct'][15],
                        avg_load_graph_data['oct'][16],
                        avg_load_graph_data['oct'][17],
                        avg_load_graph_data['oct'][18],
                        avg_load_graph_data['oct'][19],
                        avg_load_graph_data['oct'][20],
                        avg_load_graph_data['oct'][21],
                        avg_load_graph_data['oct'][22],
                        avg_load_graph_data['oct'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Nov',
                        data: [
                        avg_load_graph_data['nov'][0],
                        avg_load_graph_data['nov'][1],
                        avg_load_graph_data['nov'][2],
                        avg_load_graph_data['nov'][3],
                        avg_load_graph_data['nov'][4],
                        avg_load_graph_data['nov'][5],
                        avg_load_graph_data['nov'][6],
                        avg_load_graph_data['nov'][7],
                        avg_load_graph_data['nov'][8],
                        avg_load_graph_data['nov'][9],
                        avg_load_graph_data['nov'][10],
                        avg_load_graph_data['nov'][11],
                        avg_load_graph_data['nov'][12],
                        avg_load_graph_data['nov'][13],
                        avg_load_graph_data['nov'][14],
                        avg_load_graph_data['nov'][15],
                        avg_load_graph_data['nov'][16],
                        avg_load_graph_data['nov'][17],
                        avg_load_graph_data['nov'][18],
                        avg_load_graph_data['nov'][19],
                        avg_load_graph_data['nov'][20],
                        avg_load_graph_data['nov'][21],
                        avg_load_graph_data['nov'][22],
                        avg_load_graph_data['nov'][23]
                        ],

                        zIndex: 1,

                        fillOpacity: 0,

                        marker: {

                            lineWidth: 2,

                            enabled: true,

                            symbol: 'circle'

                        }

                    },

                    {
                        name: 'Dec',
                        data: [
                        avg_load_graph_data['dec'][0],

                        avg_load_graph_data['dec'][1],

                        avg_load_graph_data['dec'][2],

                        avg_load_graph_data['dec'][3],

                        avg_load_graph_data['dec'][4],

                        avg_load_graph_data['dec'][5],

                        avg_load_graph_data['dec'][6],

                        avg_load_graph_data['dec'][7],

                        avg_load_graph_data['dec'][8],

                        avg_load_graph_data['dec'][9],

                        avg_load_graph_data['dec'][10],

                        avg_load_graph_data['dec'][11],

                        avg_load_graph_data['dec'][12],

                        avg_load_graph_data['dec'][13],

                        avg_load_graph_data['dec'][14],

                        avg_load_graph_data['dec'][15],

                        avg_load_graph_data['dec'][16],

                        avg_load_graph_data['dec'][17],

                        avg_load_graph_data['dec'][18],

                        avg_load_graph_data['dec'][19],

                        avg_load_graph_data['dec'][20],

                        avg_load_graph_data['dec'][21],

                        avg_load_graph_data['dec'][22],

                        avg_load_graph_data['dec'][23]

                        ],

                        zIndex: 1,

                        fillOpacity: 0,

                        marker: {

                            lineWidth: 2,

                            enabled: true,

                            symbol: 'circle'

                        }

                    },



                    ]





                });
}

function weekend_graph(weekend_load_graph_data) {
            // console.log(avg_load_graph_data)
            var labels = {};

            Highcharts.chart('avg_load_area_chart3', {

                chart: {
                    width: 780,
                    height: 360,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    type: 'datetime',
                    // step: 24,
                    tickInterval: 3780 * 1000,
                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value + 'kW';
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }

                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        // pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                    name: 'Mon',
                    data: [
                    weekend_load_graph_data['mon'][0],
                    weekend_load_graph_data['mon'][1],
                    weekend_load_graph_data['mon'][2],
                    weekend_load_graph_data['mon'][3],
                    weekend_load_graph_data['mon'][4],
                    weekend_load_graph_data['mon'][5],
                    weekend_load_graph_data['mon'][6],
                    weekend_load_graph_data['mon'][7],
                    weekend_load_graph_data['mon'][8],
                    weekend_load_graph_data['mon'][9],
                    weekend_load_graph_data['mon'][10],
                    weekend_load_graph_data['mon'][11],
                    weekend_load_graph_data['mon'][12],
                    weekend_load_graph_data['mon'][13],
                    weekend_load_graph_data['mon'][14],
                    weekend_load_graph_data['mon'][15],
                    weekend_load_graph_data['mon'][16],
                    weekend_load_graph_data['mon'][17],
                    weekend_load_graph_data['mon'][18],
                    weekend_load_graph_data['mon'][19],
                    weekend_load_graph_data['mon'][20],
                    weekend_load_graph_data['mon'][21],
                    weekend_load_graph_data['mon'][22],
                    weekend_load_graph_data['mon'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Tue',
                    data: [
                    weekend_load_graph_data['tue'][0],
                    weekend_load_graph_data['tue'][1],
                    weekend_load_graph_data['tue'][2],
                    weekend_load_graph_data['tue'][3],
                    weekend_load_graph_data['tue'][4],
                    weekend_load_graph_data['tue'][5],
                    weekend_load_graph_data['tue'][6],
                    weekend_load_graph_data['tue'][7],
                    weekend_load_graph_data['tue'][8],
                    weekend_load_graph_data['tue'][9],
                    weekend_load_graph_data['tue'][10],
                    weekend_load_graph_data['tue'][11],
                    weekend_load_graph_data['tue'][12],
                    weekend_load_graph_data['tue'][13],
                    weekend_load_graph_data['tue'][14],
                    weekend_load_graph_data['tue'][15],
                    weekend_load_graph_data['tue'][16],
                    weekend_load_graph_data['tue'][17],
                    weekend_load_graph_data['tue'][18],
                    weekend_load_graph_data['tue'][19],
                    weekend_load_graph_data['tue'][20],
                    weekend_load_graph_data['tue'][21],
                    weekend_load_graph_data['tue'][22],
                    weekend_load_graph_data['tue'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Wed',
                    data: [
                    weekend_load_graph_data['wed'][0],
                    weekend_load_graph_data['wed'][1],
                    weekend_load_graph_data['wed'][2],
                    weekend_load_graph_data['wed'][3],
                    weekend_load_graph_data['wed'][4],
                    weekend_load_graph_data['wed'][5],
                    weekend_load_graph_data['wed'][6],
                    weekend_load_graph_data['wed'][7],
                    weekend_load_graph_data['wed'][8],
                    weekend_load_graph_data['wed'][9],
                    weekend_load_graph_data['wed'][10],
                    weekend_load_graph_data['wed'][11],
                    weekend_load_graph_data['wed'][12],
                    weekend_load_graph_data['wed'][13],
                    weekend_load_graph_data['wed'][14],
                    weekend_load_graph_data['wed'][15],
                    weekend_load_graph_data['wed'][16],
                    weekend_load_graph_data['wed'][17],
                    weekend_load_graph_data['wed'][18],
                    weekend_load_graph_data['wed'][19],
                    weekend_load_graph_data['wed'][20],
                    weekend_load_graph_data['wed'][21],
                    weekend_load_graph_data['wed'][22],
                    weekend_load_graph_data['wed'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Thu',
                    data: [
                    weekend_load_graph_data['thu'][0],
                    weekend_load_graph_data['thu'][1],
                    weekend_load_graph_data['thu'][2],
                    weekend_load_graph_data['thu'][3],
                    weekend_load_graph_data['thu'][4],
                    weekend_load_graph_data['thu'][5],
                    weekend_load_graph_data['thu'][6],
                    weekend_load_graph_data['thu'][7],
                    weekend_load_graph_data['thu'][8],
                    weekend_load_graph_data['thu'][9],
                    weekend_load_graph_data['thu'][10],
                    weekend_load_graph_data['thu'][11],
                    weekend_load_graph_data['thu'][12],
                    weekend_load_graph_data['thu'][13],
                    weekend_load_graph_data['thu'][14],
                    weekend_load_graph_data['thu'][15],
                    weekend_load_graph_data['thu'][16],
                    weekend_load_graph_data['thu'][17],
                    weekend_load_graph_data['thu'][18],
                    weekend_load_graph_data['thu'][19],
                    weekend_load_graph_data['thu'][20],
                    weekend_load_graph_data['thu'][21],
                    weekend_load_graph_data['thu'][22],
                    weekend_load_graph_data['thu'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Fri',
                    data: [
                    weekend_load_graph_data['fri'][0],
                    weekend_load_graph_data['fri'][1],
                    weekend_load_graph_data['fri'][2],
                    weekend_load_graph_data['fri'][3],
                    weekend_load_graph_data['fri'][4],
                    weekend_load_graph_data['fri'][5],
                    weekend_load_graph_data['fri'][6],
                    weekend_load_graph_data['fri'][7],
                    weekend_load_graph_data['fri'][8],
                    weekend_load_graph_data['fri'][9],
                    weekend_load_graph_data['fri'][10],
                    weekend_load_graph_data['fri'][11],
                    weekend_load_graph_data['fri'][12],
                    weekend_load_graph_data['fri'][13],
                    weekend_load_graph_data['fri'][14],
                    weekend_load_graph_data['fri'][15],
                    weekend_load_graph_data['fri'][16],
                    weekend_load_graph_data['fri'][17],
                    weekend_load_graph_data['fri'][18],
                    weekend_load_graph_data['fri'][19],
                    weekend_load_graph_data['fri'][20],
                    weekend_load_graph_data['fri'][21],
                    weekend_load_graph_data['fri'][22],
                    weekend_load_graph_data['fri'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Sat',
                    data: [
                    weekend_load_graph_data['sat'][0],
                    weekend_load_graph_data['sat'][1],
                    weekend_load_graph_data['sat'][2],
                    weekend_load_graph_data['sat'][3],
                    weekend_load_graph_data['sat'][4],
                    weekend_load_graph_data['sat'][5],
                    weekend_load_graph_data['sat'][6],
                    weekend_load_graph_data['sat'][7],
                    weekend_load_graph_data['sat'][8],
                    weekend_load_graph_data['sat'][9],
                    weekend_load_graph_data['sat'][10],
                    weekend_load_graph_data['sat'][11],
                    weekend_load_graph_data['sat'][12],
                    weekend_load_graph_data['sat'][13],
                    weekend_load_graph_data['sat'][14],
                    weekend_load_graph_data['sat'][15],
                    weekend_load_graph_data['sat'][16],
                    weekend_load_graph_data['sat'][17],
                    weekend_load_graph_data['sat'][18],
                    weekend_load_graph_data['sat'][19],
                    weekend_load_graph_data['sat'][20],
                    weekend_load_graph_data['sat'][21],
                    weekend_load_graph_data['sat'][22],
                    weekend_load_graph_data['sat'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                },

                {
                    name: 'Sun',
                    data: [
                    weekend_load_graph_data['sun'][0],
                    weekend_load_graph_data['sun'][1],
                    weekend_load_graph_data['sun'][2],
                    weekend_load_graph_data['sun'][3],
                    weekend_load_graph_data['sun'][4],
                    weekend_load_graph_data['sun'][5],
                    weekend_load_graph_data['sun'][6],
                    weekend_load_graph_data['sun'][7],
                    weekend_load_graph_data['sun'][8],
                    weekend_load_graph_data['sun'][9],
                    weekend_load_graph_data['sun'][10],
                    weekend_load_graph_data['sun'][11],
                    weekend_load_graph_data['sun'][12],
                    weekend_load_graph_data['sun'][13],
                    weekend_load_graph_data['sun'][14],
                    weekend_load_graph_data['sun'][15],
                    weekend_load_graph_data['sun'][16],
                    weekend_load_graph_data['sun'][17],
                    weekend_load_graph_data['sun'][18],
                    weekend_load_graph_data['sun'][19],
                    weekend_load_graph_data['sun'][20],
                    weekend_load_graph_data['sun'][21],
                    weekend_load_graph_data['sun'][22],
                    weekend_load_graph_data['sun'][23]
                    ],

                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        lineWidth: 2,
                        enabled: true,
                        symbol: 'circle'
                    }
                }
                ]
            });
}
</script>

</body>

</html>
<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?> <?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/team-leader/add') ?>"><i class="icon-plus"></i> Add New </a>
            </div>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="franchise_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Team Leader Name</th>
                                        <th>Company Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th>Status</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($users))
                                        foreach ($users as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $value['full_name']; ?></td>
                                                <td><?php echo $value['company_name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['company_contact_no']; ?></td>
                                                 <td><?php echo ($value['banned'] == 1) ? '<span class="badge badge-danger">Not Current</span>' : '<span class="badge badge-success">Current</span>'; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/team-leader/edit/<?php echo $value['user_id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/settings/change-password/<?php echo $value['user_id']; ?>"><i class="icon-pencil"></i> Change Password</a> 
                                                            <?php if($value['banned']) { ?>
                                                                <a class="dropdown-item" data-href="<?php echo site_url('admin/ban_unban_user/' . $value['user_id'] . '/' .$value['banned']); ?>"  data-toggle="modal" data-target="#confirm-unban"><i class="icon-user"></i> Unban</a>
                                                            <?php }else{ ?>
                                                                <a class="dropdown-item" data-href="<?php echo site_url('admin/ban_unban_user/' . $value['user_id'] . '/' .$value['banned']); ?>"  data-toggle="modal" data-target="#confirm-ban"><i class="icon-user"></i> Ban</a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#franchise_list').DataTable();
    });
</script>
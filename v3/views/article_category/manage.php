<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">
            <?php echo $this->aauth->get_user_groups()[0]->name; ?> - <?php echo (isset($meta_title)) ? $meta_title : 'Manage Article Categories'; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/knowledgebase/article/add_category') ?>"><i class="icon-plus"></i> Add New </a>
            </div>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="list" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Category Name</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($article_categories as $key => $value) { ?>
                                        <tr class="text-center">
                                            <td><?php echo ($key + 1); ?></td>
                                            <td><?php echo $value['category_name']; ?></td>
                                            <td><?php echo ($value['status'] == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">In-Active</span>'; ?></td>
                                            <td><?php echo date("d/m/Y", strtotime($value['created_at'])); ?></td>
                                            <td><?php echo ($value['updated_at'] != '' ) ? date("d/m/Y", strtotime($value['updated_at'])) : '-'; ?></td>
                                            <?php if ($this->aauth->is_member('Admin')) { ?>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="<?php echo site_url('admin/knowledgebase/article/add_category/' . $value['id']) ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <!-- <a class="dropdown-item" data-href="<?php echo site_url('admin/product/delete/' . $value['id']) ?>"  data-toggle="modal" data-target="#confirm-delete"><i class="icon-trash"></i> Delete</a> -->
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                <td align="right"></td>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script>
        $(document).ready(function () {
            $('#list').DataTable();
        });
    </script>
</body>

</html>

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Activity Controller
 * @version 1.0
 */
class Activity extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('GoogleAuth');
        $this->load->library('Components');
        $this->load->library("Notify");
        $this->load->model("Activity_Model", "activity");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("GoogleServiceAccount_Model", 'gsaM');
        if (!$this->aauth->is_loggedin()) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE, 
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            }else{
                redirect('admin/login');
            }
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
        $this->client = $this->googleauth->client();
        $this->timezone = 'Australia/Sydney';
    }

    public function manage() {
        $data = array();
        $userid = $this->aauth->get_user()->id;
        $is_account_exists = $this->common->num_rows('google_service_accounts', array('service_id' => 1, 'user_id' => $userid, 'status' => 1));
        $data['is_google_account_connected'] = (isset($is_account_exists) && $is_account_exists > 0) ? 'true' : 'false';
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['activity_modal_title'] = 'Edit Activity';
        $data['is_sales_rep'] = FALSE;

        if ($this->aauth->is_member('Admin')) {
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
            $sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
            $users = array_merge($team_leader, $sales_rep);
            $data['users'] = $users;
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $data['users'] = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['is_sales_rep'] = TRUE;
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $this->load->view('activity/activity_modal');
        if ($this->aauth->is_member('Admin')) {
            redirec('admin/dashboard');
        } else if ($this->components->is_team_leader() || $this->components->is_sales_rep()) {
            $view = $this->input->get('view');
            $view = (isset($view) && $view != '') ? $view : 'list';
            $this->load->view('activity/manage_' . $view);
        }
    }

    public function save() {
        if (!empty($this->input->post())) {
            $lead_id = $this->input->post('lead_id');
            $activity_data = $this->input->post('activity');
            if (isset($lead_id) && $lead_id != '') {
                $data = $this->handle_save($lead_id, $activity_data);
            } else {
                $data = $this->handle_save(FALSE, $activity_data);
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function handle_save($lead_id, $activity_data = []) {
        $data = array();

        //Check if google service account connected in order to push it to google calendar
        $activity_userid = $this->input->post('userid');
        $activity_userid = (isset($activity_userid) && $activity_userid != '') ? $activity_userid : FALSE;
        if($activity_userid){
            $is_google_account_connected = false;
            $google_account_data = $this->common->fetch_where('google_service_accounts', '*', array('service_id' => 1, 'user_id' => $activity_userid, 'status' => 1));
            if (!empty($google_account_data)) {
                $is_google_account_connected = true;
            }     
        }else{
           $is_google_account_connected = $this->isGoogleServiceAccountConnected(1); 
        }

        //Prepare Data and Add it to Google calendar
        if (isset($activity_data['scheduled_date']) && $activity_data['scheduled_date'] != '' &&
                isset($activity_data['scheduled_time']) && $activity_data['scheduled_time'] != '' &&
                isset($activity_data['scheduled_duration']) && $activity_data['scheduled_duration'] != '' &&
                isset($activity_data['activity_type']) && $activity_data['activity_type'] != 'Note' &&
                $is_google_account_connected == true) {

            $lid = ($lead_id != FALSE) ? $lead_id : $this->common->fetch_cell('tbl_activities', 'lead_id', array('id' => $activity_data['id']));

            //Prepare Start date and end date in GCAL REQUIRED FORMAT
            $start_date = date("Y-m-d", strtotime($activity_data['scheduled_date']));
            $start_time = date("H:i:sP", strtotime($activity_data['scheduled_time']));
            $event_start_date = $start_date . "T" . $start_time;

            $end_date = date("Y-m-d", strtotime($activity_data['scheduled_date']));
            $end_time_secs = strtotime($activity_data['scheduled_duration']) - strtotime("00:00:00");
            $end_time = date("H:i:sP", strtotime($activity_data['scheduled_time']) + $end_time_secs);
            $event_end_date = $end_date . "T" . $end_time;

            $event_data = $optional_data = array();

            $event_data['summary'] = $activity_data['activity_title'];

            $lead_data = $this->lead->get_leads(FALSE,$lid);
            if(!empty($lead_data)){
                $description_text = "Contact: {FIRST_NAME} {LAST_NAME}\r\n"
                                . "Phone: {CUSTOMER_MOBILE}\r\n"
                                . "Organization: {COMPANY_NAME}\r\n"
                                . "<hr align='left' width='50%'>\r\n\r\n";
                $description_text = str_replace("{FIRST_NAME}", $lead_data['first_name'], $description_text);
                $description_text = str_replace("{LAST_NAME}", $lead_data['last_name'], $description_text);
                $description_text = str_replace("{CUSTOMER_MOBILE}", '<a style="color:blue; text-decoration:none;" href="tel:"'.$lead_data['customer_contact_no'].'">'.$lead_data['customer_contact_no'].'</a>', $description_text);
                $description_text = str_replace("{COMPANY_NAME}", $lead_data['customer_company_name'], $description_text);

                $event_description = $description_text . $activity_data['activity_note'];
            }else{
                $event_description = $activity_data['activity_note'];
            }
            $event_data['description'] = $event_description;
            $event_data['start'] = array('dateTime' => $event_start_date, 'timeZone' => $this->timezone);
            $event_data['end'] = array('dateTime' => $event_end_date, 'timeZone' => $this->timezone);

            //Could be done from frontend but prefered backend in order to pick even the updated deal stage.
            //Set Event Color
            $lead_stage = $this->common->fetch_cell('tbl_leads', 'lead_stage', array('id' => $lid));
            $event_data['colorId'] = $this->components->get_gcal_color_id($lead_stage);

            //Get Event related extra data
            $location = isset($activity_data['address']) ? $activity_data['address'] : NULL;
            $attachment = isset($activity_data['attachment']) ? $activity_data['attachment'] : NULL;
            $event_post_data = $this->input->post('event');

            //If any location
            if (isset($location) && $location != '' && $location != NULL) {
                $event_data['location'] = $location;
            }

            //If any attachment
            if (isset($attachment) && $attachment != '' && $attachment != NULL) {
                $event_data['attachments'][0]['fileUrl'] = $this->config->item('live_url') . 'assets/uploads/activity_files/' . $attachment;
                $event_data['supportsAttachments'] = TRUE;
            }

            //If any attendes or optional data or reminders
            if (isset($event_post_data)) {
                if (isset($event_post_data['attendees'])) {
                    $attendees = $event_post_data['attendees'];
                    foreach ($attendees as $key => $value) {
                        $event_data['attendees'][$key]['email'] = $value;
                    }
                }
                if (isset($event_post_data['optional']['sendUpdates'])) {
                    $sendUpdates = ($event_post_data['optional']['sendUpdates'] == 'on') ? 'all' : FALSE;
                    $optional_data['sendUpdates'] = $sendUpdates;
                }

                if (isset($event_post_data['reminders']) && $event_post_data['reminders'] != '') {
                    $reminder = $event_post_data['reminders'];
                    $event_data['reminders']['useDefault'] = FALSE;
                    $event_data['reminders']['overrides'] = array(
                        array('method' => 'email', 'minutes' => $reminder),
                        array('method' => 'popup', 'minutes' => $reminder),
                    );
                } else {
                    $event_data['reminders']['useDefault'] = TRUE;
                }
            }

            $gcal_event_data['event_data'] = $event_data;
            $gcal_event_data['event_optional_data'] = $optional_data;

            //Prepare Data to be stored for database
            $activity_data['gcal_event_data'] = json_encode($gcal_event_data);

            try {
                //Check if event is already created
                $activities = $this->common->fetch_where('tbl_activities', 'gcal_event_id', ['id' => $activity_data['id'], 'gcal_event_id !=' => NULL, 'gcal_event_id !=' => '']);
                if (!empty($activities)) {
                    $event = $this->addEvent('primary', $activities[0]['gcal_event_id'], $event_data, $optional_data);
                    $event_id = $event->getId();
                    $activity_data['gcal_event_id'] = $event_id;
                } else {
                    $event = $this->addEvent('primary', FALSE, $event_data, $optional_data,$activity_userid);
                    $event_id = $event->getId();
                    $activity_data['gcal_event_id'] = $event_id;
                }
                $data['gcal_success'] = TRUE;
                $data['gcal_status'] = 'Synced to Google Calendar Successfully';
            } catch (Google_Service_Exception $gse) {
                $data['gcal_success'] = FALSE;
                $data['gcal_status'] = $gse->getMessage();
                //Will create new if in case error is 404 
                $gresponse = json_decode($gse->getMessage());
                if(isset($gresponse->error->code) && $gresponse->error->code == '404'){
                    print_r($event_data);die;
                    $event = $this->addEvent('primary', FALSE, $event_data, $optional_data,$activity_userid);
                    $event_id = $event->getId();
                    $activity_data['gcal_event_id'] = $event_id;
                    $data['gcal_success'] = TRUE;
                    $data['gcal_status'] = 'Synced to Google Calendar Successfully';
                }
            }
        }

        if (isset($activity_data['scheduled_date']) && $activity_data['scheduled_date'] != '') {
            //Convert Date to Mysql Date
            $activity_data['scheduled_date'] = date("Y-m-d", strtotime($activity_data['scheduled_date']));
        }

        if (isset($activity_data['scheduled_time']) && $activity_data['scheduled_time'] != '') {
            //Convert time to H:i format
            $time = date("H:i", strtotime($activity_data['scheduled_time']));
            $activity_data['scheduled_time'] = $time . PHP_EOL;
        }

        if (isset($activity_data['scheduled_duration']) && $activity_data['scheduled_duration'] != '') {
            //Convert Duration time to float
            $parts = explode(':', $activity_data['scheduled_duration']);
            $activity_data['scheduled_duration'] = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
        }

        $activity_data['status'] = (isset($activity_data['status']) && $activity_data['status'] == 'on') ? 1 : 0;
        
        try {
            if (isset($activity_data['id']) && $activity_data['id'] != '' && $activity_data['id'] != 0) {
                $activity_data['updated_at'] = date('Y-m-d H:i:s');
                $result = $this->common->update_data('tbl_activities', ['id' => $activity_data['id']], $activity_data);
                $aid = $activity_data['id'];
            } else {
                if ($lead_id == '' || $lead_id == 0 || $lead_id == NULL) {
                    $data['success'] = FALSE;
                    $data['status'] = 'Looks like something went wrong while saving data. Please try again later.';
                    return $data;
                }
                $activity_data['lead_id'] = $lead_id;
                $activity_data['user_id'] = $this->aauth->get_user()->id;
                $activity_data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->common->insert_data('tbl_activities', $activity_data);
                $aid = $this->common->insert_id();
                
                //If Title is NEW CALL CENTRE LEAD THEN SAVE IT IN LEAD ALLOCATION TABLE AS REFERENCE
                if(isset($activity_data['activity_title']) && $activity_data['activity_title'] == 'NEW CALL CENTRE LEAD'){
                    $this->common->update_data('tbl_lead_allocations', ['lead_id' => $lead_id],['activity_id' => $aid]);
                    //Now we need to send SMS and EMAIL to appropirate Parties
                    $notification_data = [];
                    $notification_data['id'] = $lead_id;
                    $this->notify->store_notification('NEW_LEAD', $notification_data);
                }
            }

            //Activity Attachments
            $activity_attachments = $this->input->post('activity_attachments');
            $activity_attachment_sizes = $this->input->post('activity_attachment_sizes');
            if(isset($activity_attachments) && !empty($activity_attachments)){
                $insert_attachment_data = [];
                foreach($activity_attachments as $key => $value){
                   $insert_attachment_data[$key]['activity_id'] = $aid;
                   $insert_attachment_data[$key]['user_id'] = $this->aauth->get_user()->id;
                   $insert_attachment_data[$key]['file_name'] = $value;
                   $insert_attachment_data[$key]['file_size'] = $activity_attachment_sizes[$key];
                }
                $this->common->insert_batch('tbl_activity_attachments',$insert_attachment_data);
            }

            if ($result) {
                $data['success'] = TRUE;
                $data['status'] = 'Activity saved Successfully';
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong while saving data.';
            }
        } catch (Exception $e) {
            $data['success'] = FALSE;
            $data['status'] = 'Looks like something went wrong while saving data. Please try again later.';
        }

        return $data;
    }

    public function delete() {
        if (!empty($this->input->post())) {
            $id = $this->input->post('id');
            if (isset($id) && $id != '') {
                try {
                    //Check if event is having gcal linked
                    $activities = $this->common->fetch_where('tbl_activities', 'gcal_event_id', ['id' => $id, 'gcal_event_id !=' => NULL]);
                    if (!empty($activities)) {
                        $this->deleteEvent('primary', $activities[0]['gcal_event_id']);
                        $data['gcal_success'] = TRUE;
                    }
                    $this->common->delete_data('tbl_activities', ['id' => $id]);
                    $data['success'] = TRUE;
                    $data['status'] = 'Activity deleted successfully';
                } catch (Google_Service_Exception $gse) {
                    $data['success'] = FALSE;
                    $data['gcal_success'] = FALSE;
                    $data['status'] = 'Looks like something went wrong.';
                }
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Users should not be able to remove activities (only admins or franchise owners)';
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    public function mark_as_completed() {
    if (!empty($this->input->post())) {
        $id = $this->input->post('id');
        if (isset($id) && $id != '') {
            try {
                //Check if event is having gcal linked
                $activities = $this->common->fetch_where('tbl_activities', 'gcal_event_id', ['id' => $id]);
                if (empty($activities)) {
                    $data['success'] = FALSE;
                    $data['status'] = 'No Activity Found with specified reference.';
                }
                $this->common->update_data('tbl_activities', ['id' => $id],['status' => 1]);
                $data['success'] = TRUE;
                $data['status'] = 'Activity marked completed successfully';
            } catch (Exception $gse) {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Looks like something went wrong.';
        }
    } else {
        $data['status'] = 'Invalid Request';
        $data['success'] = FALSE;
    }
    echo json_encode($data);
    die;
}

    public function fetch() {
        if (!$this->aauth->is_loggedin()) {
            $data['success'] = FALSE;
            $data['status'] = 'Unauthorized Access';
        }
        $data = array();
        $data['activities'] = array();
        $lead_id = $this->input->get('lead_id');
        $user_id = $this->input->get('user_id');
        if (isset($lead_id) && $lead_id != '') {
            $activities = $this->activity->get_activities(FALSE, $lead_id);
            $data['activities'] = $activities;
            $data['success'] = TRUE;
        } else {
            if ($user_id == '') {
                $user_id = $this->aauth->get_user()->id;
            }
            $activities = $this->activity->get_activities(FALSE, FALSE, $user_id, FALSE);
            $data['activities'] = $activities;
            $data['success'] = TRUE;
        }
        echo json_encode($data);
        die;
    }

    /** Functions for handling Google calendar integration * */
    public function isGoogleServiceAccountConnected($service_id) {
        $is_google_account_connected = $this->session->userdata('is_google_account_connected');
        if ($is_google_account_connected != '' || $is_google_account_connected == NULL) {
            $google_account_data = $this->common->fetch_where('google_service_accounts', '*', array('service_id' => $service_id, 'user_id' => $this->aauth->get_user()->id, 'status' => 1));
            if (!empty($google_account_data)) {
                $this->session->set_userdata('google_account_data', $google_account_data);
                $this->session->set_userdata('is_google_account_connected', true);
                $is_google_account_connected = true;
            } else {
                $this->session->set_userdata('is_google_account_connected', false);
                $is_google_account_connected = false;
            }
        }
        return $is_google_account_connected;
    }

    public function fetchGoogleServiceAccount($service_id) {
        $accessToken = $this->session->userdata('access_token');
        if ($accessToken == '' || $accessToken == NULL) {
            $userid = $this->aauth->get_user()->id;
            $account_data = $this->common->fetch_where('google_service_accounts', '*', array('service_id' => $service_id, 'user_id' => $userid, 'status' => 1));
            if (!empty($account_data)) {
                $account_data = $account_data[0];
                $accessToken = $account_data['access_token'];
                $this->setGoogleAccountAccessToken($accessToken);
            }
        } else {
            $this->setGoogleAccountAccessToken($accessToken);
        }
    }


    public function fetchGoogleServiceAccountByUserId($service_id, $userid) {
        $account_data = $this->common->fetch_where('google_service_accounts', '*', array('service_id' => $service_id, 'user_id' => $userid, 'status' => 1));
        if (!empty($account_data)) {
            $account_data = $account_data[0];
            $accessToken = $account_data['access_token'];
            $this->setGoogleAccountAccessToken($accessToken);
        }
    }

    public function setGoogleAccountAccessToken($accessToken) {
        $client = $this->client;
        $client->setAccessToken($accessToken);
        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            // save refresh token to some variable
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $refreshTokenSaved = $client->getRefreshToken();
                // update access token
                $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
                // pass access token to some variable
                $accessTokenUpdated = $client->getAccessToken();

                $client->setAccessToken($accessTokenUpdated);

                $this->session->set_userdata('access_token', json_encode($accessTokenUpdated));
                // save to db
                $account_data['service_id'] = 1;
                $account_data['user_id'] = $this->aauth->get_user()->id;
                $account_data['access_token'] = json_encode($accessTokenUpdated);
                $account_data['updated_at'] = date('Y-m-d H:i:s');
                $account_data['status'] = 1;
                $this->gsaM->create_google_account_for_user($account_data);
            }
        }
        return true;
    }

    public function getEvents($calendarId = 'primary', $timeMin = false, $timeMax = false, $maxResults = 1000) {
        $this->calendar = new Google_Service_Calendar($this->googleauth->client());

        if (!$timeMin) {
            $timeMin = date("c", strtotime(date('Y-m-d ') . ' 00:00:00'));
        } else {
            $timeMin = date("c", strtotime($timeMin));
        }
        if (!$timeMax) {
            $timeMax = date("c", strtotime(date('Y-m-d ') . ' 23:59:59'));
        } else {
            $timeMax = date("c", strtotime($timeMax));
        }
        $optParams = array(
            'maxResults' => $maxResults,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => $timeMin,
            'timeMax' => $timeMax,
            'timeZone' => 'Australia/Sydney',
        );
        $results = $this->calendar->events->listEvents($calendarId);

        $data = array();
        foreach ($results->getItems() as $item) {
            
            $start = date('d-m-Y H:i', strtotime($item->getStart()->dateTime));
            array_push(
                    $data, array(
                'id' => $item->getId(),
                'summary' => $item->getSummary(),
                'description' => $item->getDescription(),
                'creator' => $item->getCreator(),
                'start' => $item->getStart()->dateTime,
                'end' => $item->getEnd()->dateTime,
                    )
            );
        }
        echo "<pre>";
            print_r($results->getItems());die;
        //$data[0]['creator']->self
        return $data;
    }

    public function sync_google_calendar_data() {
        $is_sync_running = $this->common->num_rows('google_service_accounts', array('sync_data' => 1));
        if ($is_sync_running == 0) {
            $google_account_data = $this->common->fetch_row('google_service_accounts', '*', array('sync_data' => 0));
            //Set Status to Running
            $this->common->update_data('google_service_accounts', array('user_id' => $google_account_data['user_id']), array('sync_data' => 1));
            //Set Token and Get Calendar Data
            $this->fetchGoogleServiceAccountByUserId(1, $google_account_data['user_id']);
            $calendar_data = $this->getEvents('primary');
            foreach($calendar_data as $key => $value){
                $is_activity_exists = $this->common->num_rows('tbl_activities', array('gcal_event_id' => $value['id']));
                if($is_activity_exists == 0){
                    $insert_data = array();
                    $insert_data['gcal_event_id'] = $value['id'];
                    $insert_data['gcal_event_id'] = $value['id'];
                    $insert_data['gcal_event_id'] = $value['id'];
                    $insert_data['gcal_event_id'] = $value['id'];
                    $insert_data['gcal_event_id'] = $value['id'];
                }
            }
        }
    }

    public function addEvent($calendarId = 'primary', $eventId = FALSE, $data, $optional = [],$userid = FALSE) {
        if($userid){
            $this->fetchGoogleServiceAccountByUserId(1,$userid);
        }else{
            $userid = $this->aauth->get_user_id();
            //$this->fetchGoogleServiceAccount(1);
            $this->fetchGoogleServiceAccountByUserId(1,$userid);
        }
        $this->calendar = new Google_Service_Calendar($this->client);
        //date format is => 2016-06-18T17:00:00+03:00
        $event = new Google_Service_Calendar_Event(
                $data
        );

        if ($eventId) {
            return $this->calendar->events->update($calendarId, $eventId, $event, $optional);
        } else {
            return $this->calendar->events->insert($calendarId, $event, $optional);
        }
    }

    public function deleteEvent($calendarId = 'primary', $eventId) {
        $userid = $this->aauth->get_user_id();
        //$this->fetchGoogleServiceAccount(1);
        $this->fetchGoogleServiceAccountByUserId(1,$userid);
        $this->calendar = new Google_Service_Calendar($this->client);
        return $this->calendar->events->delete($calendarId, $eventId);
    }
    
    public function getCalendarList(){
         $this->fetchGoogleServiceAccount(1);
         $this->calendar = new Google_Service_Calendar($this->client);
         $calendarList = $this->calendar->calendarList->listCalendarList();
         echo "<pre>";
         print_r($calendarList);die;
    }

}

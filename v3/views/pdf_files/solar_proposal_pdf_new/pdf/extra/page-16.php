<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page16 .back-bg {
		background-color: white;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page16 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 630px;
		padding-top: 30px;
	}
	
	.page16 .top hr {
		width: 70%;
		height: 2px;
		margin: -24px 0px 0px;
		background-color: #D01E2A;
	}
	
	.page16 .text_color {
		color: #D01E2A;
	}
	
	.vkcontainer {
		margin: 0 30px;
	}
	
	.page16 .title h1 {
		padding-left: 14px;
		margin-top: 40px;
		border-left: 5px solid #D01E2A;
	}
	
	.page16 .vktitle h2 {
		font-weight: 500;
		letter-spacing: 3px;
		padding: 12px;
		margin-top: 15px;
		background-color: #D01E2A;
		width: 100%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page16 table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}
	
	.page16 tr {
		border-bottom: 1px solid #cac8c8;
	}
	
	.page16 td {
		padding: 15px;
		color: #3D3B3C;
	}
	
	.page16 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page16 .image img {
		margin-top: -50px;
	}
	
	.page16 .fimagee img {
		margin-top: -24px;
		margin-left: 650px;
		width: 230;
	}
	
	.page16 .vk-innerbox {
		width: 33%;
		float: left;
		text-align: center;
	}
	
	.page16 .vk-innerbox h4 {
		margin: 0px;
	}
	
	.page16 .vkid-section {
		padding-top: 40px;
		margin-bottom: 60px;
	}
	
	.page16 .vktitle h2 span {
		background: #fff;
		padding: 2px 4px;
		margin-right: 15px;
		color: #fff;
		font-size: 12px;
	}
	
	.page16 .headertable tr {
		border: none;
		text-align: center;
	}
	
	.page16 .headertable td {
		width: 33.33%;
	}
	
	.page16 table.headertable {
		margin-top: 75px;
	}
	
	.page16 .tablefooter tr {
		border: none;
	}
	
	.page16 .tablefooter tr th {
		text-align: left;
		padding-left: 20px;
		color: #D01E2A;
	}
	
	.page16 .tablefooter td {
		width: 50%;
		padding-left: 20px;
	}
	
	.page16 .comfooter {
		border-top: 1px dashed #D01E2A;
		border-bottom: 1px dashed #D01E2A;
		padding: 30px 0;
		margin-top: 70px;
	}
	</style>
</head>

<body>
	<div class="back-bg page16">
		<div class="vkcontainer">
			<div class="top vk-header" style="width:100%;posi">
				<div class="headertext">
					<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
					</div>
					<hr>
				</div>
				<div class="title">
					<h1 style="text-transform:uppercase;">Project <span class="text_color"> Costs</span></h1>
					
				</div>
				
				<div class="vkheader-top">
					<table class="headertable">
						<tr>
							<th style="border-right: 2px solid #D01E2A;">Quote ID</th>
							<th style="border-right: 2px solid #D01E2A;">Date</td>
							<th>Authorised Person</td>
						
						</tr>
						<tr>
							<td style="border-right: 2px solid #D01E2A;">324556</td>
							<td style="border-right: 2px solid #D01E2A;">25 Janury 2019</td>
							<td>Jamie Macauly</td>
						
						</tr>
						
					
					
					</table>
				</div>
				
				
				<div class="vktitle" style="padding-top:40px;">
					<h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 1</b> upfront payment</h2> </div>
				<div style="padding-bottom: 30px;">
					<table style="background:#EDEBEC;">
						<tr>
							<td>Deposit payment (20%)</td>
							<td style="text-align: right;">$53,800.00</td>
						</tr>
						<tr>
							<td>Payment on installation commencement (70%)</td>
							<td style="text-align: right;">$188,300.00</td>
						</tr>
						<tr>
							<td>On job completion (10%)</td>
							<td style="text-align: right;">$26,900.00</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
							<td><b>Total Payable</b> (excl. GST)</td>
							<td style="text-align: right;"><b>$94,076.00</b></td>
						</tr>
						<tr style="border-bottom: 0px">
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
				<!-- option 2 -->
				<div class="vktitle">
					<h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> OPTION 2 </b>FINANCE THROUGH ENERGY SAVINGS</h2> </div>
				<div style="padding-bottom: px;">
					<table style="background:#EDEBEC;">
						<tr>
							<td>Term (years)</td>
							<td style="text-align: right;">7</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
							<td><b>Monthly payment plan</b> (7 years)</td>
							<td style="text-align: right;"><b>$1,490</b></td>
						</tr>
						<tr style="border-bottom: 0px">
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
				<div class="vaildquote" style="margin-bottom:50px;">
					<h4 style="color:#D01E2A;">This quote is valid till 30 days</h4>
					<div class="comfooter">
						<table class="tablefooter" style="width:100%;">
							<tr>
								<th style="border-right: 1px dashed #D01E2A;">Company Name:</th>
								<th style="padding-left:50px;">Company Address:</td>
							</tr>
							<tr>
								<td style="border-right: 1px dashed #D01E2A;">Illusion Australia Pty Ltd</td>
								<td style="padding-left:50px;">6 Ramage Street, Bayswater VIC 3153</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="fimagee" style="margin-bottom: -66px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
			<div class="footer"></div>
		</div>
</body>

</html>
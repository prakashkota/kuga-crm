<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
<style>
    .font-family{font-family: 'Open Sans', sans-serif;}
    .table-border{border:solid 1px #ccc;}
    .w910{width:910px; }
    body{margin:0px !important;}
</style>
</head>
<body style="padding:0; margin:0">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff; ">
		<tr>
			<td style="font-size:0px;"><img src="<?php echo site_url(''); ?>assets/tender_quotes_files/01.jpg" alt="" height="694" width="910"/></td>
		</tr>
		<tr>
			<td valign="top" style="background: #DE1717;">
				<table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40"></td>
						</tr>
						<tr>
							<td style="font-size: 39px; color: #ffffff; text-transform: uppercase"><strong>YOUR <span style="color: #000000">SOLAR QUOTE</span> </strong></td>
						</tr>
						<tr>
							<td height="8"></td>
						</tr>
						<tr>
							<td style="font-size: 22px; color: #ffffff; text-transform: uppercase">SOLAR ENERGY FOR BETTER TOMORROW</td>
						</tr>
						<tr>
							<td style="border-bottom: solid 1px #ffffff">&nbsp;</td>
						</tr>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td valign="top"><table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td width="435" valign="top"><table width="435" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td width="435" style="color: #ffffff; font-size:23px;"><strong>CUSTOMISED FOR</strong></td>
												</tr>
												<tr>
													<td height="18"></td>
												</tr>
												<tr>
													<td valign="top"><table width="95%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="28%" height="20" style="color: #ffffff; font-size: 18px">Quote Id:</td>
																<td width="72%" style="color: #ffffff; font-size: 18px">#<?php echo $quote['quote_id'] ; ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Name:</td>
																<td style="color: #ffffff; font-size: 18px"><?php echo (!empty($project_name)) ? $project_name : ''; ?></td>
															</tr>
															<tr>
																<td height="20" valign="top" style="color: #ffffff; font-size: 18px">Address:</td>
																<td style="color: #ffffff; font-size: 18px"> <?php echo (isset($quote['address']) && !empty($quote['address'])) ? str_ireplace(", Australia", "", $quote['address']) : ''; ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Phone No.: </td>
																<td style="color: #ffffff; font-size: 18px"><?php echo (isset($customer['contact_no']) && !empty($customer['contact_no'])) ? $customer['contact_no'] : '' ; ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Email:</td>
																<td style="color: #ffffff; font-size: 18px"><?php echo (isset($customer['email']) && !empty($customer['email'])) ? $customer['email'] : ''; ?></td>
															</tr>
														</tbody>
													</table></td>
												</tr>
											</tbody>
										</table></td>
										<td width="375" valign="top"><table width="350" border="0" align="right" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td width="381" style="color: #ffffff; font-size:23px;"><strong>PREPARED BY</strong></td>
												</tr>
												<tr>
													<td height="18"></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $prepared_by['name'] ; ?></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $prepared_by['phone'] ; ?></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $prepared_by['email'] ; ?></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-size:23px;"><strong>DATE PREPARED: <?php echo date('d-m-Y'); ?></strong></td>
						</tr>
						<tr>
							<td height="123"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
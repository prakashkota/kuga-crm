<?php
$terms_and_conditions = array();
if (isset($booking_form->terms_and_conditions)) {
    $terms_and_conditions = json_decode(json_encode($booking_form->terms_and_conditions), true);
}
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td valign="top">
            <table width="830" border="0"  cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="red" style="color:#c32027; font-size:16px; font-family:Calibri, sans-serif; padding-left:40px;">
                        <strong>Key points of our Terms and Conditions</strong>
                    </td>
                </tr>
                <tr>
                    <td  style="padding-top:0px; padding-bottom:10px; color:#333333; font-family:Calibri, sans-serif; font-size:10px; padding-left:40px;">
                        We'd like to highlight to you some key clauses of our Terms and Conditions, which are laid out in full to you on the following pages. 
                    </td>
                </tr>
            </table>
        </td>

    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th  colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <?php if (isset($terms_and_conditions[0])) { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                    <?php } ?>
                                </td>
                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Key Point 1 - Payment</td>
                            </tr>
                        </table>
                    </th>
                </tr>
                <tr>
                    <td width="816" height="20" style="padding:5px; font-family:Calibri, sans-serif; font-size:10px">
                        6.4.2 Post Installation, where a payment for Goods and Services purchased by the Buyer under this agreement, to Kuga Electrical is denied by 
                        the external Finance provider/payment plan provider, on the basis of any issue with the buyer (including but not limited to bad credit rating, 
                        non-payment of repayment amounts) that is not in Kuga Electrical's control, Kuga Electrical will have the right to claim the amount from the  
                        Buyer directly.<br/><br/>
                        6.4.3 In case where Kuga Electrical is denied payments as mentioned in 6.4.2 above and where Kuga Electrical is unable to recover the amount for 
                        Goods and Services purchased by the Buyer under this agreement, the Buyer acknowledges that Kuga Electrical will be entitled to repossession 
                        of the Goods supplied under this agreement. In such events, the Buyer acknowledges that all costs directly attributable to repossession of such 
                        Goods including but not limited to dismantling costs, installers charges, will be borne by the Buyer.
                    </td> 
                </tr>

            </table></td>
    </tr>
    <tr>
        <td height="165">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" >
                <tr>

                    <td width="415" valign="top" style="padding-right:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="405" style="font-family:Calibri, sans-serif; font-size:10px; color:#333333">ALL OF THE ABOBE HAS BEEN EXPLAINED AND I ACCEPT THE CHARGES AS LISTED. I HAVE READ AND UNDERSTOOD THE FULL TERMS AND CONDITIONS ON THE FOLLOWING PAGES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="23" style="font-family:Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised on behalf of:</td>
                            </tr>
                            <tr>
                                <td valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->authorised_on_behalf->company_name; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->position; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->date; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Client Signature:</td>
                                            <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/'. $booking_form->authorised_on_behalf->signature; ?>" />
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>

                        </table> 
                    </td>
                    <td width="415" valign="top" style="padding-left:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" style="font-family:Calibri, sans-serif; font-size:10px; color:#333333">BY SIGNING BELOW YOU ACKNOWLEGE THAT THE CUSTOMER IS AWARE OF THE PROSSIBLE EXTRA CHARGES. INCLUDING ALL ACCESS EQUIPMENT CHARGES AND RE-SCHEDULING FEES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="20" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="116" style="font-family:Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of:</td>
                                            <td width="289" style="font-family:Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">Kuga Electrical</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->position; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->date; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Sales <br />
                                                Consultant Signature:</td>
                                            <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/'. $booking_form->authorised_by_behalf->sales_rep_signature; ?>" />
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table></td>
    </tr>
    <?php if ($type_id == 1) { ?>
    <tr>
        <td height="285">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 2) { ?>
    <tr>
        <td height="321">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 3) { ?>
    <tr>
        <td height="321">&nbsp;</td>
    </tr>
    <?php } ?>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" /></td>
                </tr>
            </table></td>
    </tr>
</table>
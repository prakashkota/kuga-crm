<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Tracking Controller
 * @version 1.0
 */
class Tracking extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Notify");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Product_Model", "product");
        if (!$this->aauth->is_loggedin()) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            } else {
                redirect('admin/login');
            }
        }
    }

    public function manage() {
        $data = array();
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $data['is_sales_rep'] = $data['is_lead_allocation_sales_rep'] = FALSE;

        if ($this->aauth->is_member('Admin')) {
            $users = $this->user->get_users(FALSE, FALSE, FALSE);
            $view_folder = 'sales_rep';
        } else if ($this->components->is_team_leader()) {
            if ($this->components->is_lead_allocation_team_leader()) {
                $la_team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $la_sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $lead_allocation_users = array_merge($la_team_leader, $la_sales_rep);
                $data['lead_allocation_users'] = $lead_allocation_users;
                $team_leader = $this->user->get_users(6, FALSE, FALSE);
                $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                $users = array_merge($team_leader, $sales_rep);
                $data['is_lead_allocation_sales_rep'] = TRUE;
                $view_folder = 'lead_allocation';
            } else {
                $team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $users = array_merge($team_leader, $sales_rep);
                $view_folder = 'sales_rep';
            }
            $data['all_reports_view_permission'] = 1;
        } else if ($this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep()) {
                $la_team_leader = $this->user->get_users(FALSE, $user_id, FALSE);
                $la_sales_rep = $this->user->get_sub_users(FALSE, FALSE, $user_id);
                $lead_allocation_users = array_merge($la_team_leader, $la_sales_rep);
                $data['lead_allocation_users'] = $lead_allocation_users;
                $team_leader = $this->user->get_users(6, FALSE, FALSE);
                $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                $users = array_merge($team_leader, $sales_rep);
                $data['is_lead_allocation_sales_rep'] = TRUE;
                $view_folder = 'lead_allocation';
            } else {
                //For Sarvanis Case we need to show list of all users
                if ($user_id == 29) {
                    //$team_leader = $this->user->get_users(FALSE, 5, FALSE);
                    //$sales_rep = $this->user->get_sub_users(FALSE, FALSE, 5);
                    $team_leader = $this->user->get_users(6, FALSE, FALSE);
                    $sales_rep = $this->user->get_sub_users(7, FALSE, FALSE);
                    $users = array_merge($team_leader, $sales_rep);
                    $view_folder = 'sales_rep';
                } else {
                    $user_id = $this->aauth->get_user_id();
                    $users = $this->user->get_users(FALSE, $user_id, FALSE);
                    $data['is_sales_rep'] = TRUE;
                    $view_folder = 'sales_rep';
                }
            }
        }
        $data['users'] = $users;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $data['all_users'] = $users = $this->user->get_users(FALSE, FALSE, FALSE,'WHERE user.id!=1');
            $data['logged_in_user_data'] = $this->common->fetch_row('tbl_user_locations', '*', array('user_id' => $user_id));
            $data['activity_modal_title'] = 'Add/Schedule Activity';
            $this->load->view('activity/activity_modal', $data);
        $this->load->view('tracking/manage');
    }


}

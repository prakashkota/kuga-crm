<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class GoogleAuth {

    /**
     * Constructor
     */
    public function __construct() {
        require APPPATH . "third_party/google-api-php/autoload.php";
        $this->client = new Google_Client();
        $this->client->setApplicationName('Kuga CRM');
        $this->ci = & get_instance();
        $this->ci->config->load('google');
        $this->config_vars = $this->ci->config->item('google');
        $this->client->setClientId($this->config_vars['client_id']);
        $this->client->setClientSecret($this->config_vars['client_secret']);
        $this->client->setRedirectUri($this->ci->config->base_url() . 'google_oauth_callback');
        $this->client->addScope(Google_Service_Calendar::CALENDAR);
        $this->client->addScope('email');
        $this->client->setAccessType('offline');
        $this->client->setApprovalPrompt('force');
    }
    
    public function loginUrl() {
        return $this->client->createAuthUrl();
    }

    public function getAuthenticate() {
        return $this->client->authenticate();
    }

    public function getAccessToken() {
        return $this->client->getAccessToken();
    }

    public function setAccessToken() {
        return $this->client->setAccessToken();
    }

    public function revokeToken() {
        return $this->client->revokeToken();
    }

    public function client() {
        return $this->client;
    }

    public function isAccessTokenExpired() {
        return $this->client->isAccessTokenExpired();
    }

}

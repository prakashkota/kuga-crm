﻿<?php
ob_start();

session_start();

include_once("application_top.php");

$page = "Add New Product";

$pageId = "newProductAdd";

is_logged_in($pageId); # Check login status for admin

include_once ADMIN_USER_MODULE_PATH . "/common_model.php";

$common_model = new Common();

$lineItem_data = $common_model->fetch_where(TABLE_LINE_ITEM, [], array('id' => $_GET['id']));

if (empty($lineItem_data)) {
    header('location:lineItemAdd.php');
}

if (isset($_POST['submit'])) {
    $update_data = array();
    $update_data = $_POST;
    unset($update_data['submit']);
    if ($lineItem_data[0]['min_price'] != $update_data['min_price']) {
        $update_data['last_updated_price'] = $lineItem_data[0]['min_price'];
    }
    $update_data['updated_at'] = date('Y-m-d H:i:s');
    $status = $common_model->update_data(TABLE_LINE_ITEM, $update_data, array('id' => $_GET['id']));
    $_SESSION['status'] = 'update';
    header('location:lineItemManage.php');
}

include("header.php");


$left_nav_var = 5;
?>


<body>

    <?php include('left_nav.php'); ?>

    <div class="page-wrapper d-block clearfix">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <h3 class="content-header-title mb-0">Admin - Edit Product</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="dashboard.php">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Add Product</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-outline-primary" href="lineItemManage.php"><i class="icon-list"></i> Manage Line Item </a>
                </div>
            </div>
        </div>
        <!-- BEGIN: message  -->	
        <?php message_display_template($message_arr, $message_arr['msg_class'], $message_arr['message']); ?>
        <!-- END: message  -->	
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="form-block d-block clearfix">

                            <form role="form"  action="" class="" method="post" name="lineItemAdd" id="lineItemAdd" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="form-section">Item Details</h4>
                                        <div class="form-group">
                                            <label>Item Name</label>
                                            <input type="text" name="line_item"  class="form-control" placeholder="item Name"  value="<?php echo $lineItem_data[0]['line_item']; ?>" required>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>State</label>
                                            <select name="state" class="form-control"  required>
                                                <?php $lineItem_state = unserialize(product_state);
                                                foreach ($lineItem_state as $key => $value) { ?>
                                                <option value="<?php echo $value; ?>" <?php if($lineItem_data[0]['state'] == $value) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div> 
                                    </div>
                                    <div class="col-md-6" >
                                        <h4 class="form-section">Pricing Details</h4>
                                        <div class="form-group">
                                            <label>Min Price</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control square" placeholder="Min Price"  name="min_price" value="<?php echo $lineItem_data[0]['min_price']; ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Max Price</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control square" placeholder="Max Price"  name="max_price" value="<?php echo $lineItem_data[0]['max_price']; ?>" />
                                            </div>
                                        </div>
                                        <h4 class="form-section">Item Additional Details</h4>
                                        <div class="form-group">
                                            <label>Comments</label>
                                            <textarea type="text" name="comments"  class="form-control"   placeholder="Comments"><?php echo $lineItem_data[0]['comments']; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="buttwrap">
                                    <input type="submit" name="submit"  class="btn" value="SAVE">
                                </div>                                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
    <!-- JQUERY SCRIPTS --> 
    <?php include("footer.php"); ?>
    <script src="../assets/js/product.js"></script>
</body>

</html>


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/payment.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <?php if ($type_id == 1) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_30)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 0 - 39kW</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">No deposit, 100% due upon handover of Certificate of Electricity Safety</td>
                                    <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } else if ($type_id == 2) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_99)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 40kW - 100kW </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Payment (20%)</td>
                                    <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_deposit_30; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Due upon handover of Certificate of Electrical Safety (80%)</td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_deposit_70; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } else if ($type_id == 3) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_100)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 100kW+ </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Payment (20%)</td>
                                    <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_20; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Payment on installation commencement (70%)</td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_70; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Upon providing Certificate of Electrical Safety (10%)</td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_10; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } ?>
                <?php if ($type_id != 1) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_finance)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 2 - Finance Through Energy Ease </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Monthly Payment Plan</td>
                                    <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc Gst:</td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_finance_incGST; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Term (years)</td>
                                    <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_finance_terms; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_energy_plan)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 3 - Energy Plan </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                                        <?php if (isset($booking_form->is_energy_plan_8)) { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" /> 8YR Plan
                                        <?php } else { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" /> 8YR Plan
                                        <?php } ?>
                                    </td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_energy_plan_8_kwh; ?></strong> <span style="float:right;">/kWh</span></td> 
                                </tr>
                                <tr>
                                    <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                                        <?php if (isset($booking_form->is_energy_plan_12)) { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" /> 12YR Plan
                                        <?php } else { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" /> 12YR Plan
                                        <?php } ?>
                                    </td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_energy_plan_12_kwh; ?></strong> <span style="float:right;">/kWh</span></td> 
                                </tr>
                                <tr>
                                    <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                                        <?php if (isset($booking_form->is_energy_plan_15)) { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" /> 15YR Plan
                                        <?php } else { ?>
                                            <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" /> 15YR Plan
                                        <?php } ?>
                                    </td>
                                    <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_energy_plan_15_kwh; ?></strong> <span style="float:right;">/kWh</span></td> 
                                </tr>
                            </table></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <?php if ($type_id == 1) { ?>
    <tr>
        <td height="910">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 2) { ?>
    <tr>
        <td height="590">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 3) { ?>
    <tr>
        <td height="553">&nbsp;</td>
    </tr>
    <?php } ?>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />9</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
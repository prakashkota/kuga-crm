<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Kuga Electrical CRM</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#4c4c4c">
        <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/images/apple-touch-icon-180x180.png" />
        <link rel="manifest" href="<?php echo site_url(); ?>/manifest.json">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

        <!-- STYLE -->
        <?php if (strtolower($this->router->fetch_class()) != 'survey') { ?>
        <link href="<?php echo site_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <?php } ?>

        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/all.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/sidebar-menu.css" type="text/css" media="all">
        <link href="<?php echo site_url(); ?>assets/css/custom.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/style.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/responsive.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/new-style.css" rel="stylesheet" type="text/css">
        
    <link href="<?php echo site_url(); ?>assets/css/commercial.min.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
        <!-- ADDITIONAL -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/toastr.css" />
        
        <!-- SCRIPTS -->
        <script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
        <?php if ($this->components->is_team_leader()) { ?>
            <script src="<?php echo site_url(); ?>assets/js/notify.js?v=<?php echo version; ?>"></script>
            <script> var notify_manager = new notify_manager();</script>
        <?php } else if ($this->components->is_sales_rep()) { ?>
            <script src="<?php echo site_url(); ?>assets/js/notify.js?v=<?php echo version; ?>"></script>
            <script> var notify_manager = new notify_manager();</script>
        <?php } ?>
            
        <script src="<?php echo site_url(); ?>assets/js/toastr.js"></script> 
        <script src="<?php echo site_url(); ?>assets/js/custom.js?v=<?php echo version; ?>"></script>
       
        
        <script>
            var base_url = '<?php echo site_url(); ?>';
        </script>
    </head>


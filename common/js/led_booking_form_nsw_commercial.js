
var led_booking_form_manager = function (options) {
    var self = this;
    this.lead_data = {};
    this.lead_uuid = (options.lead_uuid && options.lead_uuid != '') ? options.lead_uuid : {};
    this.booking_form_uuid = (options.booking_form_uuid && options.booking_form_uuid != '') ? options.booking_form_uuid : '';
    this.site_id = (options.site_id && options.site_id != '') ? options.site_id : '';
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.product_subtotal_excGST = 0;
    this.ae_subtotal_excGST = 0;
    this.total_incGST = 0;
    this.total_excGST = 0;
    this.total_GST = 0;
    this.signature_pad = [];
    this.signature_pad_ids = ['authorised_on_behalf_signature', 'authorised_by_behalf_sales_rep_signature', 'sales_rep_signature','get_nomination_form_details_signature'];
    this.default_product_row_count = 0;
    this.product_types = options.product_types ? JSON.parse(options.product_types) : [];
    this.min_co_payment = 0;
    this.calculator_items = {};
    this.postcode = '';
    $("#authorised_on_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#authorised_by_behalf_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#booked_at").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $('[name="business_details[entity_name]"]').change(function(){
        $('[name="get_nomination_form_details[energy_saver_details][entity_name]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[energy_saver_details][entity_name]"]').change(function(){
        $('[name="business_details[entity_name]"]').val($(this).val());
    });

    $('[name="business_details[abn]"]').change(function(){
        $('[name="get_nomination_form_details[energy_saver_details][abn]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[energy_saver_details][abn]"]').change(function(){
        $('[name="business_details[abn]"]').val($(this).val());
    });

    $('[name="authorised_details[email]"]').change(function(){
        $('[name="get_nomination_form_details[energy_saver_details][email]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[energy_saver_details][email]"]').change(function(){
        $('[name="authorised_details[email]"]').val($(this).val());
    });

    $('[name="authorised_details[contact_no]"]').change(function(){
        $('[name="get_nomination_form_details[energy_saver_details][contact_no]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[energy_saver_details][contact_no]"]').change(function(){
        $('[name="authorised_details[contact_no]"]').val($(this).val());
    });

    $('[name="business_details[address]"]').change(function(){
        $('[name="get_nomination_form_details[certificate_provider_details][address]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[certificate_provider_details][address]"]').change(function(){
        $('[name="business_details[address]"]').val($(this).val());
    });

    $('[name="business_details[description]"]').change(function(){
        $('[name="get_nomination_form_details[certificate_provider_details][description]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[certificate_provider_details][description]"]').change(function(){
        $('[name="business_details[description]"]').val($(this).val());
    });

    $('[name="booking_form[authorised_on_behalf][name]"]').change(function(){
        $('[name="get_nomination_form_details[authorised_details][name]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[authorised_details][name]"]').change(function(){
        $('[name="booking_form[authorised_on_behalf][name]"]').val($(this).val());
    });

    $('[name="booking_form[authorised_on_behalf][date]"]').change(function(){
        $('[name="get_nomination_form_details[authorised_details][date]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[authorised_details][date]"]').change(function(){
        $('[name="booking_form[authorised_on_behalf][date]"]').val($(this).val());
    });

    $('[name="booking_form[authorised_on_behalf][position]"]').change(function(){
        $('[name="get_nomination_form_details[authorised_details][position]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[authorised_details][position]"]').change(function(){
        $('[name="booking_form[authorised_on_behalf][position]"]').val($(this).val());
    });

    $('[name="get_nomination_form_details[energy_saver_details][name]"]').change(function(){
        var name = $(this).val();
        name = name.split(' ');
        var fname = (name.length > 0) ? name[0] : '';
        var lname = (name.length > 1) ? name[1] : '';
        $('#first_name').val(fname);
        $('#last_name').val(lname);
        $('[name="get_nomination_form_details[authorised_details][name]"]').val($(this).val())
    });

    $('[name="booking_form[authorised_by_behalf][date]"]').trigger('change');

    $("input[name='get_nomination_form_details[past_activities][is_nominated]']")[1].setAttribute('checked', 'checked');
    
    $(document).on('change','.item_name',function(){
        var prd_id = $(this).children("option:selected").attr('data-id');
        if(prd_id != null && prd_id != 'null' && prd_id != '' && prd_id != 'undefined' && prd_id != 'undefined'){
            $(this).parent().find('.item_id').val(prd_id);
            $(this).removeClass('is-invalid');
            $(this).parent().removeClass('is-invalid');
            $(this).parent().parent().removeClass('is-invalid');
            $(this).next('.invalid-feedback').remove();
            $(this).parent().children('.invalid-feedback').remove();
        }
    });
    
    $(document).on('change','.item_zone_classification ',function(){
        var zone = $(this).val();
        if(zone == 'Extra'){
            $(this).parent().parent().children('td:nth-child(2)').find('.item_id').removeAttr('required');
        }else{
            $(this).parent().parent().children('td:nth-child(2)').find('.item_id').attr('required','required');
        }
    });
    
    $(document).on('click','#add_more_led_products_row_btn_nsw',function(){
        
        self.default_product_row_count++;
        self.create_led_product_row();
        
    });

    $(document).on('change','input,select',function(){
        var eleval = $(this).val();
        if(eleval != ''){
            $(this).removeClass('is-invalid');
            $(this).parent().removeClass('is-invalid');
            $(this).parent().parent().removeClass('is-invalid');
            $(this).next('.invalid-feedback').remove();
            $(this).parent().children('.invalid-feedback').remove();
            if($(this)[0].hasAttribute('required')){
                $(this).addClass('is-valid');
            }
        }
    });
    
    $(document).on('click','#add_more_led_products_row_btn',function(){
        self.default_product_row_count++;
        $('.product_row_'+self.default_product_row_count).removeClass('hidden');
    });


    if (self.booking_form_uuid != '') {
        self.fetch_led_booking_form_data();
    } else if (self.lead_uuid != '') {
        var data = {};
        data.uuid = self.lead_uuid;
        data.site_id = self.site_id;
        self.fetch_lead_data(data);
    }
    self.handle_items();
    self.handle_cost_centre_change();
    
    $(document).on('change','.type_id',function(){
        var data_id = $(this).attr('data-id');
        $('.item_name_' + data_id).val('');
        $('.item_id_' + data_id).val('');
        $('.lifetime_' + data_id).val('');
        $('.lcp_' + data_id).val('');
    });
    
    //added this code 25-may-2020
    $('body').on('keyup keydown','.item_name',function(){
        var data_id = $(this).attr('data-id');
        var item_type = $('.type_id_' + data_id).val();
    	$('.item_name').autocomplete({
            autoSelect: false,
            autoFocus: false,
            minLength: 2,
            maxItems: 10,
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/booking_form/fetch_products",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term,
                        request: 1,
    					cost_centre_id: $('#cost_centre_id').val(),
                        item_type: self.current_type
                    },
                    success: function(data) {
                        response(data.products);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            search: function(event, ui) {
                $(this).parent().parent().children('td:nth-child(1)').removeClass('is-invalid');
               /* var item_type = $(this).parent().parent().children('td:nth-child(1)').children('select').val();
                self.current_type = item_type;
                if (item_type == '') {
                    this.value = ''
                    $(this).parent().parent().children('td:nth-child(1)').addClass('is-invalid');
                    toastr["error"]("Please select product type first.");
                    return false;
                }*/
            },
            focus: function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.name);
                return false;
            },
            select: function(event, ui) {
                event.preventDefault();
                var product_name = ui.item.name;
                var brand = (ui.item.brand != '' && ui.item.brand != null) ? ui.item.brand : '';
                // var description = (ui.item.description != '' && ui.item.description != null) ? ui.item.description : 'No Description';
                var model = (ui.item.model_no != '' && ui.item.model_no != null) ? ui.item.model_no : 'No Model';
                // var w_kW_name = (ui.item.capacity != '' && ui.item.capacity != null) ? ui.item.capacity : 'No Wattage';
                var item_id = (ui.item.item_id != '' && ui.item.item_id != null) ? ui.item.item_id : '';
                var lifetime = (ui.item.life_time != '' && ui.item.life_time != null) ? ui.item.life_time : '';
                var lcp = (ui.item.lcp != '' && ui.item.lcp != null) ? ui.item.lcp : '';
                
                //var item_crm_id = (ui.item.item_crm_id != '' && ui.item.item_crm_id != null) ? ui.item.item_crm_id : '';
                var sensor = (ui.item.sensor != '' && ui.item.sensor != null) ? parseInt(ui.item.sensor) : 0;
    		    var type_id = (ui.item.type_id != '' && ui.item.type_id != null) ? parseInt(ui.item.type_id) : 8;
    
                var data_id = $(this).attr('data-id');
    
                // $('.item_crm_id_' + data_id).val(item_crm_id);
    		    $('.type_id_' + data_id).val(type_id);
                $('.item_id_' + data_id).val(item_id);
                $('.lifetime_' + data_id).val(lifetime);
                $('.lcp_' + data_id).val(lcp);
                $('.item_name_'  + data_id).val(product_name);
                $('.item_brand_' + data_id).val(brand);
                // $('.item_description_' + data_id).html(description);
                $('.item_model_' + data_id).val(model);
                //$('.item_wattage_' + data_id).val(w_kW_name);
                $('.item_sensor_' + data_id).val(sensor);
    
                return false;
            }
        });
    });

    $(document).on('change','.item_name',function(){
        var name = $(this).val();
        
        if(name.length > 0){
            $(this).next('.item_id').prop('required',true);
        }else{
            $(this).next('.item_id').prop('required',false);
            $(this).next('.item_id').removeAttr('value');
        }
    });
    
    $(document).on('change','.ae_name',function(){
        if($(this).val() == 'Boom Lift' || $(this).val() == 'Site Inspection Required'){
            $('#boom_requirement_page').removeClass('hidden');
            $('#boom_requirement_area_1').attr("required",true);
            $('#boom_reach_up').attr("required",true);
            $('#boom_clearence').attr("required",true);
            $('#boom_reach_access').attr("required",true);
        }else{
           $('#boom_requirement_page').addClass('hidden');
           $('#boom_requirement_area_1').attr("required",false);
           $('#boom_reach_up').attr("required",false);
           $('#boom_clearence').attr("required",false);
           $('#boom_reach_access').attr("required",false);
        }    
    });
};


led_booking_form_manager.prototype.handle_cost_centre_change = function () {
    var self = this;
    $(document).on('change','#cost_centre_id',function(){
        var cost_centre_id = $(this).val();
        if(cost_centre_id == ''){
            toastr['error']('Please Select a Cost Centre Value');
            return false;
        }

        if(self.booking_form_uuid == ''){
            window.location.href = base_url + 'admin/booking_form/add_led_booking_form?deal_ref='+self.lead_uuid+'&site_ref='+self.site_id + '&cost_centre_id='+cost_centre_id;
            return false;
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'admin/booking_form/save_cost_centre',
            datatype: 'json',
            data: {
                'cost_centre_id' : cost_centre_id,
                'booking_form_uuid' : self.booking_form_uuid,
                'lead_uuid' : self.lead_uuid,
                'site_id': self.site_id,
                'type': 'LED'
            },
            success: function (res) {
                if (res.success == true) {
                    window.location.reload();
                }else{
                    toastr['error'](res.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

led_booking_form_manager.prototype.initialize = function () {
    var self = this;

    $('#save_booking_form').click(function () {
        self.save_booking_form();
    });

    $('#generate_booking_form_pdf').click(function () {
        self.save_booking_form(true);
    });

    //Handle Singature Pads
    $('.sign_create').click(function () {
        var id = $(this).attr('data-id');
        if(id == 4){
            var name = $('[name="get_nomination_form_details[authorised_details][name]"]').val();
            var position = $('[name="get_nomination_form_details[authorised_details][position]"]').val();
            var date = $('[name="get_nomination_form_details[authorised_details][date]"]').val();
            if(name == '' || position == '' || date == ''){
                toastr['error']('Please Enter Name, Position, Date First in Order to sign the form');
                return false;
            }
        }
        if(id == 5){
            var name = $('[name="site_assessor_form_details[name2]"]').val();
            var position = $('[name="site_assessor_form_details[position]"]').val();
            var date = $('[name="site_assessor_form_details[date1]"]').val();
            if(name == '' || position == '' || date == ''){
                toastr['error']('Please Enter Name, Position, Date First in Order to sign the form');
                return false;
            }
        }
        $('#booking_form').hide();
        $('.sr-deal_actions').hide();
        $('#signatures').show();
        for (var i = 1; i < 6; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        window.scrollTo({top: 0, behavior: 'smooth'});
        //Resize Canvas for Signature pad because we assume user might have rotated the tablet
        self.resizeCanvas();
    });

    $('.sign_close').click(function () {
        var id = $(this).attr('data-id');
        $('#booking_form').show();
        $('.sr-deal_actions').show();
        $('#signatures').hide();
        for (var i = 1; i < 6; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });

    $('#is_upfront').change(function () {
        $('#is_upfront').attr('checked', 'checked');
        $('#is_finance').removeAttr('checked');

        $('#finance_excGST').val('');
        $('#finance_GST').val('');
        $('#finance_incGST').val('');

        $('#upfront_excGST').val(self.total_excGST);
        $('#upfront_GST').val(self.total_GST);
        $('#upfront_incGST').val(self.total_incGST);
    });

    $('#is_finance').change(function () {
        $('#is_finance').attr('checked', 'checked');
        $('#is_upfront').removeAttr('checked');

        $('#upfront_excGST').val('');
        $('#upfront_GST').val('');
        $('#upfront_incGST').val('');

        $('#finance_excGST').val(self.total_excGST);
        $('#finance_GST').val(self.total_GST);
        $('#finance_incGST').val(self.total_incGST);
    });

    //Check box to copy details from Authroised to Accounts
    $('#same_as_authorised_details_btn').click(function () {
        if ($(this)[0].checked == true) {
            if ($('#first_name').val() != '') {
                $('#acc_first_name').val($('#first_name').val());
            }
            if ($('#last_name').val() != '') {
                $('#acc_last_name').val($('#last_name').val());
            }
            if ($('#contact_no').val() != '') {
                $('#acc_contact_no').val($('#contact_no').val());
            }
            if ($('#landline').val() != '') {
                $('#acc_landline').val($('#landline').val());
            }
            if ($('#email').val() != '') {
                $('#acc_email').val($('#email').val());
            }
        } else {
            if ($('#first_name').val() != '') {
                $('#acc_first_name').val('');
            }
            if ($('#last_name').val() != '') {
                $('#acc_last_name').val('');
            }
            if ($('#contact_no').val() != '') {
                $('#acc_contact_no').val('');
            }
            if ($('#landline').val() != '') {
                $('#acc_landline').val('');
            }
            if ($('#email').val() != '') {
                $('#acc_email').val('');
            }
        }
    });

    self.handle_items();
    self.upload_image();
    self.show_image();
    self.hide_image();
    self.remove_signature_image();
    self.create_signature_pad();
    self.create_led_product_row();
    //self.create_new_signature_pad();
};


led_booking_form_manager.prototype.fetch_lead_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading lead data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                self.lead_data = stat.lead_data;
                self.calculator_items = stat.calculator_items;
                
                self.postcode = self.lead_data.postcode;
                $('#entity_name').val(self.lead_data.customer_company_name).trigger('change');
                $('#address').val(self.lead_data.address).trigger('change');
                $('#postcode').val(self.lead_data.postcode).trigger('change');

                $('#authorised_on_behalf_company_name').val(self.lead_data.customer_company_name);
                $('#authorised_on_behalf_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name).trigger('change');
                $('#authorised_on_behalf_position').val(self.lead_data.position).trigger('change');

                $('[name="get_nomination_form_details[energy_saver_details][name]"]').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);

                $('#first_name').val(self.lead_data.first_name);
                $('#last_name').val(self.lead_data.last_name);
                $('#contact_no').val(self.lead_data.customer_contact_no).trigger('change');
                $('#position').val(self.lead_data.position);
                $('#email').val(self.lead_data.customer_email).trigger('change');

                $("#authorised_by_behalf_name").val(self.lead_data.full_name).trigger('change');
                $("#authorised_by_behalf_position").val('Sales Person').trigger('change');
                $("#authorised_on_behalf_date").val(moment(new Date()).format('DD/MM/YYYY')).trigger('change');
                $("#authorised_by_behalf_date").val(moment(new Date()).format('DD/MM/YYYY')).trigger('change');
                $("#booked_at").val(moment(new Date()).format('DD/MM/YYYY'));

                self.initialize();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


led_booking_form_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

led_booking_form_manager.prototype.handle_items = function () {
    var self = this;
    $(document).on('change','.item_qty,.item_cost_excGST',function () {
        self.product_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        $('table#booking_items tr').each(function () {
            var price = $(this).children().find(".item_cost_excGST").val();
            var qty = $(this).children().find(".item_qty").val();
            var total = price * qty;
            if (total != '' && !isNaN(total)) {
                $(this).children().find(".item_total_cost_excGst").val(total).trigger('change');
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST) + parseFloat(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST).trigger('change');
                $('#total_incGst').val(self.total_incGST);
            } else {
                total = (total == 0 && qty != '') ? total : '';
                $(this).children().find(".item_total_cost_excGst").val(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }
        });
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST).trigger('change');
        $('#total_incGst').val(self.total_incGST).trigger('change');
        $('#total_Gst').val(self.total_GST).trigger('change');
    });

    $('.ae_qty,.ae_cost_excGST').change(function () {
        self.ae_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        for (var i = 0; i < 2; i++) {
            var price = $(".ae_cost_excGST_" + i).val();
            var qty = $(".ae_qty_" + i).val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(".ae_total_cost_excGst_" + i).val(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST) + parseFloat(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            } else {
                $(".ae_total_cost_excGst_" + i).val('');
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }
        }
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST).trigger('change');
        $('#total_incGst').val(self.total_incGST).trigger('change');
        $('#total_Gst').val(self.total_GST).trigger('change');
    });


};

led_booking_form_manager.prototype.upload_image = function () {
    var self = this;
    $('.image_upload').change(function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'led_booking_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

led_booking_form_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/led_booking_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

led_booking_form_manager.prototype.hide_image = function () {
    var self = this;
    $('.image_close').click(function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};




led_booking_form_manager.prototype.create_new_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");
    var canvas4 = document.getElementById("signature_pad_4");
    var canvas5 = document.getElementById("signature_pad_5");


    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = $("#signature_pad_1").jSignature({'UndoButton': true});

    var signaturePad2 = $("#signature_pad_2").jSignature({'UndoButton': true});

    var signaturePad3 = $("#signature_pad_3").jSignature({'UndoButton': true});

    var signaturePad4 = $("#signature_pad_4").jSignature({'UndoButton': true});

    var signaturePad5 = $("#signature_pad_5").jSignature({'UndoButton': true});

    var arr = [signaturePad1, signaturePad2, signaturePad3, signaturePad4, signaturePad5];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        console.log(arr[(id - 1)]);
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        $('#signpad_close_' + id).trigger('click');
        var dataURL = arr[(id - 1)].jSignature('getData', 'image');
        var ele = $('#' + arr1[(id - 1)]);
        self.show_signature_image(ele, dataURL, true);
    });


}

led_booking_form_manager.prototype.create_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");
    var canvas4 = document.getElementById("signature_pad_4");
    var canvas5 = document.getElementById("signature_pad_5");

    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad2 = new SignaturePad(canvas2, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad3 = new SignaturePad(canvas3, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad4 = new SignaturePad(canvas4, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var signaturePad5 = new SignaturePad(canvas5, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1, signaturePad2, signaturePad3, signaturePad4, signaturePad5];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();

        canvas2.width = canvas2.offsetWidth * ratio;
        canvas2.height = canvas2.offsetHeight * ratio;
        canvas2.getContext("2d").scale(ratio, ratio);
        signaturePad2.clear();

        canvas3.width = canvas3.offsetWidth * ratio;
        canvas3.height = canvas3.offsetHeight * ratio;
        canvas3.getContext("2d").scale(ratio, ratio);
        signaturePad3.clear();

        canvas4.width = canvas4.offsetWidth * ratio;
        canvas4.height = canvas4.offsetHeight * ratio;
        canvas4.getContext("2d").scale(ratio, ratio);
        signaturePad4.clear();

        canvas5.width = canvas5.offsetWidth * ratio;
        canvas5.height = canvas5.offsetHeight * ratio;
        canvas5.getContext("2d").scale(ratio, ratio);
        signaturePad5.clear();
    }

    //window.onresize = resizeCanvas;

    window.addEventListener("resize", function (event) {
        resizeCanvas();
    });

    window.addEventListener("orientationchange", function (event) {
        resizeCanvas();
    });

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        console.log(arr[(id - 1)]);
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 1)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 1)].toDataURL();
            var ele = $('#' + arr1[(id - 1)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
}

led_booking_form_manager.prototype.show_signature_image = function (ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.parent().removeClass('is-invalid');
            ele.prev().html('<img style="height:95px;" src="' + image + '" />');
            if(ele[0].name  == 'get_nomination_form_details[authorised_details][signature]'){

                //$('[name="get_nomination_form_details[authorised_details][signature]"]').val(image);
                //$('#get_nomination_form_details_signature').attr('src',image);

                var canvas = document.getElementById('canvas');
                var ctx = canvas.getContext("2d");
                var cimage = new Image();
                cimage.src = image;
                cimage.onload = function() {
                    ctx.drawImage(cimage, 0, 0);
                    console.log(cimage.height);
                    ctx.font = "14pt Calibri";
                    var name = $('[name="get_nomination_form_details[authorised_details][name]"]').val();
                    var position = $('[name="get_nomination_form_details[authorised_details][position]"]').val();
                    var date = $('[name="get_nomination_form_details[authorised_details][date]"]').val();
                    ctx.fillText("Name: " + name, 20, 260);
                    ctx.fillText("Position: "+ position, 20, 280);
                    ctx.fillText("Date: "+ date, 20, 300);
                };

                setTimeout(function(){
                    dataURL = canvas.toDataURL();
                    $('[name="get_nomination_form_details[authorised_details][custom_signature]"]').val(dataURL);
                },1000);
                
            }
            if(ele[0].name  == 'site_assessor_form_details[signature]'){
                //$('[name="site_assessor_form_details[signature]"]').val(image);
                //$('#site_assessor_form_details_signature').attr('src',image);

                var canvas = document.getElementById('canvas');
                var ctx = canvas.getContext("2d");
                var cimage = new Image();
                cimage.src = image;
                cimage.onload = function() {
                    ctx.drawImage(cimage, 0, 0);
                    console.log(cimage.height);
                    ctx.font = "14pt Calibri";
                    var name = $('[name="site_assessor_form_details[name2]"]').val();
                    var position = $('[name="site_assessor_form_details[position]"]').val();
                    var date = $('[name="site_assessor_form_details[date1]"]').val();
                    ctx.fillText("Name: " + name, 20, 260);
                    ctx.fillText("Position: "+ position, 20, 280);
                    ctx.fillText("Date: "+ date, 20, 300);
                };

                setTimeout(function(){
                    dataURL = canvas.toDataURL();
                    $('[name="site_assessor_form_details[custom_signature]"]').val(dataURL);
                },1000);   
            }
        } else {
            ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/led_booking_form_files/' + image + '" />');
            /**if(ele[0].name  == 'booking_form[authorised_on_behalf][signature]'){
                $('[name="get_nomination_form_details[authorised_details][signature]"]').val(image);
                $('#get_nomination_form_details_signature').attr('src',base_url + 'assets/uploads/led_booking_form_files/' + image);
            }
            if(ele[0].name  == 'booking_form[authorised_by_behalf][sales_rep_signature]'){
                $('[name="site_assessor_form_details[signature]"]').val(image);
                $('#site_assessor_form_details_signature').attr('src',base_url + 'assets/uploads/led_booking_form_files/' + image);     
            }*/
        }
    }

    //Signateur Wrapper hide
    //ele.parent().parent().hide();
    //Show Image Wrapper
    //ele.parent().parent().next('div').show();
    //ele.parent().parent().next('div').children('img').attr('src', image);
};

led_booking_form_manager.prototype.remove_signature_image = function () {
    var self = this;
    $('.signature_image_close').click(function () {
        $(this).parent().prev('div').show();
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('img').attr('src', '');
    });
};

led_booking_form_manager.prototype.validate_booking_form = function () {
    var self = this;
    var flag = true;
    var eleArr = [];
    $('.invalid-feedback').remove();
    var elements = document.getElementById("led_booking_form").elements;
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                    console.log($(elements[i]).parent());
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
                eleArr.push($(elements[i]).parent());
            } else {
                $(elements[i]).removeClass('is-invalid');
                $(elements[i]).parent().removeClass('is-invalid');
                $(elements[i]).parent().parent().removeClass('is-invalid');
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'checkbox') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                        eleArr.push($(elements[i]).parent().parent());
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }

    var payment_option_flag = true;
    var is_upfront_checked = document.getElementById('is_upfront').checked;
    var is_finance_checked = false;
    if(document.getElementById('is_finance') != undefined){
     is_finance_checked = document.getElementById('is_finance').checked;
 }
 $('#is_upfront').parent().parent().removeClass('is-invalid');
 $('#upfront_excGST').removeClass('is-invalid');
 $('#upfront_GST').removeClass('is-invalid');
 $('#upfront_incGST').removeClass('is-invalid');
 $('#finance_excGST').removeClass('is-invalid');
 $('#finance_GST').removeClass('is-invalid');
 $('#finance_incGST').removeClass('is-invalid');

 if (is_upfront_checked) {
    var upfront_excGST = document.getElementById('upfront_excGST').value;
    var upfront_GST = document.getElementById('upfront_GST').value;
    var upfront_incGST = document.getElementById('upfront_incGST').value;

    if (upfront_excGST == '') {
        $('#upfront_excGST').addClass('is-invalid');
        $('#upfront_excGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
        payment_option_flag = false;
        eleArr.push($('#upfront_excGST'));
    }

    if (upfront_GST == '') {
        $('#upfront_GST').addClass('is-invalid');
        $('#upfront_GST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
        payment_option_flag = false;
        eleArr.push($('#upfront_GST'));
    }

    if (upfront_incGST == '') {
        $('#upfront_incGST').addClass('is-invalid');
        $('#upfront_incGST').parent().append('<div class="invalid-feedback">Please provide some value. </div>');
        payment_option_flag = false;
        eleArr.push($('#upfront_incGST'));
    }
} else if (is_upfront_checked == false && is_finance_checked == false) {
    $('#is_upfront').parent().parent().addClass('is-invalid');
    payment_option_flag = false;
    eleArr.push($('#is_upfront').parent().parent());
}

flag = (payment_option_flag == false) ? false : flag;

if(eleArr.length > 0){
    $('html, body').animate({
        scrollTop: eleArr[0].offset().top - 70
    }, 2000);
}

return flag;
};

led_booking_form_manager.prototype.save_booking_form = function (download) {
    var self = this;

    /** //Convert Signature to Images
     var signature_pad = self.signature_pad;
     var signature_pad_ids = self.signature_pad_ids;
     for (var i=0; i < signature_pad.length; i++) {
     if (signature_pad[i].isEmpty()) {
     } else {
     var dataURL = signature_pad[i].toDataURL();
     var ele = $('#' + signature_pad_ids[i]);
     self.show_signature_image(ele, dataURL);
     }
 } */

 if (download) {
    var flag = self.validate_booking_form();
    if (!flag) {
        toastr.remove();
        toastr["error"]('Please fill the fields marked in red');
        return false;
    }
}

self.populate_basline_calculator_data();

if(parseFloat($("#total_incGst").val()) < parseFloat(self.min_co_payment)){
   toastr['error']('Booking form cannot be created as Min co Payment has not been met');
        return false;
}

if (self.booking_form_uuid == '') {
    var uuid = self.create_uuid();
    self.booking_form_uuid = uuid;
}

var formData = $('#led_booking_form').serialize();
formData += '&uuid=' + self.booking_form_uuid;
formData += '&lead_id=' + self.lead_data.id;
formData += '&site_id=' + self.site_id;

$.ajax({
    url: base_url + 'admin/booking_form/save_led_booking_form',
    type: 'post',
    data: formData,
    dataType: 'json',
    beforeSend: function () {
        toastr.remove();
        toastr["info"]('Saving Booking Form Data, Please Wait....');
        $("#save_booking_form").attr("disabled", "disabled");
    },
    success: function (response) {
        toastr.remove();
        if (response.success == true) {
            self.lead_id = response.id;
                //Add Quote ref in url
                if (self.booking_form_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/booking_form/edit_led_booking_form/' + self.booking_form_uuid);
                }
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["success"](response.status);
                    setTimeout(function () {
                        window.location.href = base_url + 'admin/lead/edit/' + self.lead_data.uuid;
                    }, 1500);
                }
            } else {
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                } else {
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


led_booking_form_manager.prototype.fetch_led_booking_form_data = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/booking_form/fetch_led_booking_form_data',
        datatype: 'json',
        data: {uuid: self.booking_form_uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var booking_data = stat.booking_data;
                self.calculator_items = stat.calculator_items;
                if (booking_data.simpro_job_id != null && booking_data.simpro_job_id != '') {
                    $('.sr-deal_actions').hide();
                } else if (booking_data.kuga_job_id != null && booking_data.kuga_job_id != '') {
                    //$('.sr-deal_actions').hide();
                    $('#cost_centre_id').attr('disabled','disabled');
                    self.initialize();
                } else {
                    self.initialize();
                }
                
                var product = JSON.parse(booking_data.product);
                for (var key in product) {
                    for (var i = 0; i < product[key].length; i++) {
                        if(i > 0  && key == 'item_name' && product['item_name'][i] != ''){
                            $('#add_more_led_products_row_btn_nsw').click();
                        }
                    }
                }
    
                self.handle_led_booking_form_data(booking_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_booking_form_manager.prototype.handle_led_booking_form_data = function (data) {
    var self = this;

    //Handle Business Details
    var business_details = JSON.parse(data.business_details);
    for (var key in business_details) {
        if (key == 'leased_or_owned' || key == 'bca_hours') {
            $("#" + key).val(business_details[key]);
        } else {
            if ($("#" + key)) {
                $("#" + key).val(business_details[key]).trigger('change');
            } else {
                $("input[name='business_details[" + key + "]']").val(business_details[key]).trigger('change');
            }
        }
        
        self.postcode = business_details['postcode'];
    }

    //Handle Authorised Details
    var authorised_details = JSON.parse(data.authorised_details);
    for (var key in authorised_details) {
        if ($("#" + key)) {
            $("#" + key).val(authorised_details[key]).trigger('change');
        } else {
            $("input[name='authorised_details[" + key + "]']").val(authorised_details[key]).trigger('change');
        }
    }

    //Handle Account Details
    var account_details = JSON.parse(data.account_details);
    for (var key in account_details) {
        if ($("#" + key)) {
            $("#" + key).val(account_details[key]);
        } else {
            $("input[name='account_details[" + key + "]']").val(account_details[key]);
        }
    }

    //Handle Space Type
    var space_type = JSON.parse(data.space_type);
    for (var key in space_type) {
        $("select[name='space_type[" + key + "]']").val(space_type[key]);
    }

    //Handle Ceiling Height
    var ceiling_height = JSON.parse(data.ceiling_height);
    for (var key in ceiling_height) {
        $("input[name='ceiling_height[" + key + "]']").val(ceiling_height[key]);
    }

    //Handle Boom Req
    var boom_req = JSON.parse(data.boom_req);
    for (var key in boom_req) {
        $("input[name='boom_req[" + key + "]']").val(boom_req[key]);
    }

    //Handle Product
    var product = JSON.parse(data.product);
    for (var key in product) {
        for (var i = 0; i < product[key].length; i++) {
            $("." + key + "_" + i).val(product[key][i]);
            
            if(key == 'item_id'){
                $("." + key + "_" + i).attr('required',true);
            }
        }
    }
    $('.item_qty').trigger('change');

    //Handle Access Equipments
    var ae = JSON.parse(data.ae);
    for (var key in ae) {
        for (var i = 0; i < ae[key].length; i++) {
            $("." + key + "_" + i).val(ae[key][i]);
        }
    }
    $('.ae_qty').trigger('change');

    //Handle Booking Form Data
    var booking_form = JSON.parse(data.booking_form);
    for (var key in booking_form) {
        if (key == 'authorised_on_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'signature') {
                    self.show_signature_image($('#authorised_on_behalf_signature'), booking_form[key]['signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]).trigger('change');
                }
            }
        } else if (key == 'authorised_by_behalf') {
            for (var key1 in booking_form[key]) {
                if (key1 == 'sales_rep_signature') {
                    self.show_signature_image($('#authorised_by_behalf_sales_rep_signature'), booking_form[key]['sales_rep_signature']);
                } else {
                    $("#" + key + '_' + key1).val(booking_form[key][key1]);
                }
            }
        } else if (key == 'sales_rep_signature') {
            self.show_signature_image($('#sales_rep_signature'), booking_form[key]);
        } else if (key == 'terms_and_conditions') {
            for (var i = 0; i < 4; i++) {
                if (booking_form[key][i] == 'on') {
                    $('#terms_and_conditions_' + i).attr('checked', 'checked');
                }
            }
        } else if (key == 'is_upfront' || key == 'is_finance' || key == 'is_heers_panel' || key == 'is_heers_batten' || key == 'is_more_products') {
            $("#" + key).attr('checked', 'checked');
        } else {
            $("#" + key).val(booking_form[key]);
        }
    }

    //Handle images
    var booking_form_image = JSON.parse(data.booking_form_image);
    for (var key in booking_form_image) {
        if (booking_form_image[key] != '') {
            $("#" + key).val(booking_form_image[key]);
            self.show_image($("#" + key), booking_form_image[key]);
        }
    }

    //Handle Prev Upgrade
    var prev_upgrade = JSON.parse(data.prev_upgrade);
    for (var key in prev_upgrade) {
        if (key == 'is_occured_at_premise') {
            var is_checked = (prev_upgrade[key] == '1') ? 'yes' : 'no';
            $("#" + key + "_" + is_checked).attr('checked', 'checked');
        }
        $("#" + key).val(prev_upgrade[key]);
    }

    //Handle Customer Check List Some Issue in Ipad so do it with for loop associative key in integer
    var customer_check_list = JSON.parse(data.customer_check_list);
    if (customer_check_list.hasOwnProperty('notes')) {
        $("#customer_check_list_notes").val(customer_check_list['notes']);
    }
    for (var i = 0; i < 17; i++) {
        if ($("#customer_check_list_" + i)) {
            if (customer_check_list[i] == 'on') {
                $("#customer_check_list_" + i).attr('checked', 'checked');
            }
        }
    }

    //Handle GET Nomination Form Details
    var get_nomination_form_details = JSON.parse(data.get_nomination_form_details);
    for (var key in get_nomination_form_details) {
        for (var key1 in get_nomination_form_details[key]) {
            if(key1 == 'is_nominated'){
                if(get_nomination_form_details[key][key1] == '1'){
                    $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']")[0].setAttribute('checked', 'checked');          
                }else{
                    $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']")[1].setAttribute('checked', 'checked');      
                }          
            }else if(key1 == 'signature'){
                $("#get_nomination_form_details_signature").prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/led_booking_form_files/' + get_nomination_form_details[key][key1] + '" />');
                //$("#get_nomination_form_details_signature").attr('src',base_url + 'assets/uploads/led_booking_form_files/' + get_nomination_form_details[key][key1]);
                $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']").val(get_nomination_form_details[key][key1]);   
            }else{
                $("input[name='get_nomination_form_details[" + key + "][" + key1 + "]']").val(get_nomination_form_details[key][key1]);     
            } 
        }
    }


     //Handle Site Assessor Form Details
     var site_assessor_form_details = JSON.parse(data.site_assessor_form_details);
     console.log(site_assessor_form_details);
     for (var key in site_assessor_form_details) {
        if(key == 'is_activities'){
            for (var key1 in site_assessor_form_details[key]) {
                if(site_assessor_form_details[key][key1] == 'on'){
                    $("input[name='site_assessor_form_details[" + key + "][" + key1 + "]']")[0].setAttribute('checked', 'checked');          
                } 
            }
        } else if(key == 'signature'){
            $("#site_assessor_form_details_signature").attr('src',base_url + 'assets/uploads/led_booking_form_files/' + site_assessor_form_details[key]);
            $("[name='site_assessor_form_details[" + key + "]']").val(site_assessor_form_details[key]);   
        }else{
            $("[name='site_assessor_form_details[" + key + "]']").val(site_assessor_form_details[key]);     
        } 
    }

    if(data.cost_centre_id != null){
        $('#cost_centre_id').val(data.cost_centre_id);
    }
};

led_booking_form_manager.prototype.save_booking_form_to_simpro = function () {
    var self = this;
    var url = base_url + 'admin/booking_form/led_booking_form_pdf_download/' + self.booking_form_uuid;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]("Saving Data to Job CRM. Please Wait...");
            $("#save_booking_form").attr("disabled", "disabled");
            $("#generate_booking_form_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function () {
                    window.location.href = base_url + 'admin/lead/edit/' + self.lead_data.uuid;
                }, 1500);
            } else {
                toastr["error"](response.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
            toastr.remove();
            //Getting this weird issue but still boking form was completed save on simpro 
            if (xhr.statusText == "OK") {
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function () {
                    //window.location.href = base_url + 'admin/lead/edit/' + self.lead_data.uuid;
                }, 1500);
            } else {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};

led_booking_form_manager.prototype.resizeCanvas = function () {

    var self = this;

    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");
    var canvas4 = document.getElementById("signature_pad_4");
    var canvas5 = document.getElementById("signature_pad_5");

    var signaturePad1 = self.signature_pad[0];

    var signaturePad2 = self.signature_pad[1];

    var signaturePad3 = self.signature_pad[2];

    var signaturePad4 = self.signature_pad[3];

    var signaturePad5 = self.signature_pad[4];


    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas1.width = canvas1.offsetWidth * ratio;
    canvas1.height = canvas1.offsetHeight * ratio;
    canvas1.getContext("2d").scale(ratio, ratio);
    signaturePad1.clear();

    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    signaturePad2.clear();

    canvas3.width = canvas3.offsetWidth * ratio;
    canvas3.height = canvas3.offsetHeight * ratio;
    canvas3.getContext("2d").scale(ratio, ratio);
    signaturePad3.clear();

    canvas4.width = canvas4.offsetWidth * ratio;
    canvas4.height = canvas4.offsetHeight * ratio;
    canvas4.getContext("2d").scale(ratio, ratio);
    signaturePad4.clear();

    canvas5.width = canvas5.offsetWidth * ratio;
    canvas5.height = canvas5.offsetHeight * ratio;
    canvas5.getContext("2d").scale(ratio, ratio);
    signaturePad5.clear();

};

led_booking_form_manager.prototype.populate_basline_calculator_data = function(){
   
    var self = this;
    var total_mwh_savings_i = 0;
    var space = self.calculator_items['spaceType'];
    var cm_options = self.calculator_items['CM_OLD'];
    
    var postcode = self.postcode;
    var postcodesArray = self.calculator_items.Postcode;
    var result = postcodesArray.filter(function(obj,index) {
      return obj.Postcode == postcode
    });
    
    if(result){
        var rnf = (result[0]) ? result[0].Factor : 1;
    }else{
        var rnf = 1;
    }
        
    for (var i = 0; i <= (self.default_product_row_count); i++) {
       
        //alert($('.item_existing_qty_1').val());
        var am = $('.item_air_con_'+i+' option:selected').attr('data-am'); //1
        
        var space_type = $('.item_space_type_'+i).val();
        var bca_classification = $('.item_bca_type_'+i).val();
        var calculator_regional_site = (result[0]) ? result[0].Regional : 'No';
        
        var applicable_lifetime = 0;
        if(space_type !== undefined && bca_classification !== undefined){
            for(var k in space){
                if(calculator_regional_site == 'No'){
                    if(space_type.trim() == space[k].SpaceType && bca_classification.trim() == space[k].BuildingClassification.trim()){
                        applicable_lifetime = space[k].AllOthers;
                    }
                }else{
                    if(space_type.trim() == space[k].SpaceType && bca_classification.trim() == space[k].BuildingClassification.trim()){
                        applicable_lifetime = space[k].RegionalSite;
                    }
                }
            }    
        }
        var lifetime = applicable_lifetime;
        var eligible_aoh = $('.item_bca_type_'+i+' option:selected').attr('data-aoh');
        eligible_aoh = eligible_aoh.replace(/\D/g,'');
        
        am = parseFloat(am);
        lifetime = parseFloat(lifetime);
        eligible_aoh = parseInt(eligible_aoh);
        
        var plus = $('.item_lamp_and_ballast_type_'+i+' option:selected').attr('data-plus');
        var multiply = $('.item_lamp_and_ballast_type_'+i+' option:selected').attr('data-multiply');
        var nominal_watt = $('.item_existing_nominal_lamp_power_'+i).val();
        
        if(multiply != "" && plus != ""){
            var lcp_old = (parseInt(nominal_watt) * parseFloat(multiply) ) + parseInt(plus);
        }else if(multiply != "" && plus == ""){
            var lcp_old = parseInt(nominal_watt) * parseFloat(multiply);
        }else if(multiply == "" && plus != ""){
            var lcp_old = parseInt(nominal_watt) + parseInt(plus);
        }else if(multiply == "" && plus == ""){
            var lcp_old = nominal_watt;
        }
        
        var cm_old = 0;
        var control_dev = $('.item_control_system_'+i).val();
        var special_occupancy = 'None';
        if(control_dev !== undefined && special_occupancy !== undefined){
            for(var k in cm_options){
                if(control_dev == cm_options[k].ControlSystem && special_occupancy == cm_options[k].SpecialOccupancy){
                    cm_old = cm_options[k].CM;
                }
            }    
        }
        if(cm_old == ""){
            var lcp_low_mode = $('.lcp_low_mode_0').val();
            lcp_low_mode = (lcp_low_mode) ? parseFloat(lcp_low_mode) : 0;
            if(special_occupancy == 'Special Occ 1'){
                cm_old = 0.55+0.45*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 2'){
                cm_old = 0.3+0.7*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 3'){
                cm_new = 0.15+0.85*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 4'){
                cm_old = 0.25+0.75*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }
        }
        var existing_qty = parseInt($('.item_existing_qty_'+i).val());
        
        var pre_upgrade_mwh = lcp_old*cm_old*am*lifetime*eligible_aoh*existing_qty;
        
        
        var lcp = $('.lcp_'+i).val();
        var cm_new = 0;
        var control_dev = $('.control_device_upgrade_lighting_'+i).val();
        if(control_dev !== undefined && special_occupancy !== undefined){
            for(var k in cm_options){
                if(control_dev == cm_options[k].ControlSystem && special_occupancy == cm_options[k].SpecialOccupancy){
                    cm_new = cm_options[k].CM;
                }
            }    
        }
        if(cm_new == ""){
            var lcp_low_mode = $('.lcp_low_mode_0').val();
            lcp_low_mode = (lcp_low_mode) ? parseFloat(lcp_low_mode) : 0;
            if(special_occupancy == 'Special Occ 1'){
                cm_new = 0.55+0.45*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 2'){
                cm_new = 0.3+0.7*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 3'){
                cm_new = 0.15+0.85*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }else if(special_occupancy == 'Special Occ 4'){
                cm_new = 0.25+0.75*(parseFloat(lcp_low_mode)/parseFloat(lcp));
            }
        }
        var upgrade_qty = parseInt($('.item_qty_'+i).val());
        
        var post_upgrade_mwh = lcp*cm_new*am*lifetime*eligible_aoh*upgrade_qty;
    
        pre_upgrade_mwh = (pre_upgrade_mwh/1000000);
        post_upgrade_mwh = (post_upgrade_mwh/1000000);
        
        var mwh_savings = (pre_upgrade_mwh-post_upgrade_mwh)*rnf;
        total_mwh_savings_i = total_mwh_savings_i+parseFloat(mwh_savings);
    }
    self.min_co_payment = (total_mwh_savings_i * 5).toFixed(2);
    
};

led_booking_form_manager.prototype.create_led_product_row = function() {
    var self = this;
    
    var count = self.default_product_row_count;
   var tr = document.createElement('tr');
   
   var td_room_area = document.createElement('td');
   td_room_area.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var room_area_input = document.createElement('input');
   room_area_input.setAttribute('type','text');
   room_area_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   room_area_input.className = 'item_room_area item_room_area_'+count;
   room_area_input.setAttribute('name','product[item_room_area][]');
   if(count == 0){
       room_area_input.setAttribute('required',true);
   }
   td_room_area.appendChild(room_area_input);
   
   var td_ch = document.createElement('td');
   td_ch.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ch_input = document.createElement('input');
   ch_input.setAttribute('type','text');
   ch_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ch_input.className = 'item_ceiling_height item_ceiling_height_'+count;
   ch_input.setAttribute('name','product[item_ceiling_height][]');
   ch_input.setAttribute('data-id',count);
   if(count == 0){
       ch_input.setAttribute('required',true);
   }
   td_ch.appendChild(ch_input);
    
   var td_space_type = document.createElement('td');
   td_space_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var space_type_select = document.createElement('select');
   space_type_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   space_type_select.className = 'item_space_type item_space_type_'+count;
   space_type_select.setAttribute('name','product[item_space_type][]');
   space_type_select.setAttribute('data-id',count);
   if(count == 0){
       space_type_select.setAttribute('required',true);
   }

   var space_type_array = self.calculator_items.Space;
   for(var key in space_type_array){
       var option = document.createElement('option');
       option.innerHTML = space_type_array[key].SpaceTypeLIST;
       option.setAttribute('value',space_type_array[key].SpaceTypeLIST);
       space_type_select.appendChild(option);
   }
   td_space_type.appendChild(space_type_select);

   var td_bca = document.createElement('td');
   td_bca.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var bca_select = document.createElement('select');
   bca_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   bca_select.className = 'item_bca_type item_bca_type_'+count;
   bca_select.setAttribute('name','product[item_bca_type][]');
   bca_select.setAttribute('data-id',count);
   if(count == 0){
       bca_select.setAttribute('required',true);
   }
   
   var bca_classification_array = self.calculator_items.BCA;
   for(var key in bca_classification_array){
       if(bca_classification_array[key].BuildingClassificationList !== undefined){        
           var option = document.createElement('option');
           option.innerHTML = bca_classification_array[key].Description;
           option.setAttribute('value',bca_classification_array[key].Description);
           option.setAttribute('data-aoh',bca_classification_array[key].AnnualOperatingHours);
           bca_select.appendChild(option);
       }        
   }
   td_bca.appendChild(bca_select);

   var td_air_con = document.createElement('td');
   td_air_con.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var ac_select = document.createElement('select');
   ac_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   ac_select.className = 'item_air_con item_air_con_'+count;
   ac_select.setAttribute('name','product[item_air_con][]');
   ac_select.setAttribute('data-id',count);
   if(count == 0){
       ac_select.setAttribute('required',true);
   }
   var air_con = self.calculator_items.BCA;
   for(var key in air_con){
    if(air_con[key].SpaceAirConditioningSystem !== undefined){
       var option = document.createElement('option');
       option.innerHTML = air_con[key].SpaceAirConditioningSystem;
       option.setAttribute('value',air_con[key].SpaceAirConditioningSystem);
       option.setAttribute('data-am',air_con[key].SpaceAirConditioningSystemAM);
       ac_select.appendChild(option);
    }
   }
   td_air_con.appendChild(ac_select);
   
   var td_existing_qty = document.createElement('td');
   td_existing_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var existing_qty_input = document.createElement('input');
   existing_qty_input.setAttribute('type','text');
   existing_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   existing_qty_input.className = 'item_existing_qty item_existing_qty_'+count;
   existing_qty_input.setAttribute('name','product[item_existing_qty][]');
   existing_qty_input.setAttribute('data-id',count);
   if(count == 0){
       existing_qty_input.setAttribute('required',true);
   }
   td_existing_qty.appendChild(existing_qty_input);
   
   var td_lamp_and_ballast = document.createElement('td');
   td_lamp_and_ballast.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var lamp_and_ballast_select = document.createElement('select');
   lamp_and_ballast_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   lamp_and_ballast_select.className = 'item_lamp_and_ballast_type item_lamp_and_ballast_type_'+count;
   lamp_and_ballast_select.setAttribute('name','product[item_lamp_and_ballast_type][]');
   lamp_and_ballast_select.setAttribute('data-id',count);
   if(count == 0){
       lamp_and_ballast_select.setAttribute('required',true);
   }
   var lamp_and_ballast_array = self.calculator_items.LCP_OLD;
   for(var key in lamp_and_ballast_array){
       var option = document.createElement('option');
       option.innerHTML = lamp_and_ballast_array[key].EquipmentClass;
       option.setAttribute('value',lamp_and_ballast_array[key].EquipmentClass);
       option.setAttribute('data-multiply',lamp_and_ballast_array[key].multiply);
        option.setAttribute('data-plus',lamp_and_ballast_array[key].plus_value);
       lamp_and_ballast_select.appendChild(option);
   }
   td_lamp_and_ballast.appendChild(lamp_and_ballast_select);
   
   var td_nominal_lamp_power = document.createElement('td');
   td_nominal_lamp_power.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var nominal_lamp_power_input = document.createElement('input');
   nominal_lamp_power_input.setAttribute('type','text');
   nominal_lamp_power_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   nominal_lamp_power_input.className = 'item_existing_nominal_lamp_power item_existing_nominal_lamp_power_'+count;
   nominal_lamp_power_input.setAttribute('name','product[item_existing_nominal_lamp_power][]');
   nominal_lamp_power_input.setAttribute('data-id',count);
   if(count == 0){
       nominal_lamp_power_input.setAttribute('required',true);
   }
   td_nominal_lamp_power.appendChild(nominal_lamp_power_input);
   
   var td_control_system = document.createElement('td');
   td_control_system.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var cs_select = document.createElement('select');
   cs_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   cs_select.className = 'item_control_system item_control_system_'+count;
   cs_select.setAttribute('name','product[item_control_system][]');
   cs_select.setAttribute('data-id',count);
   if(count == 0){
       cs_select.setAttribute('required',true);
   }
   var control_systems = self.calculator_items.BCA;
   for(var key in control_systems){
       if(control_systems[key].SystemList !== undefined){        
           var option = document.createElement('option');
           option.innerHTML = control_systems[key].SystemList;
           option.setAttribute('value',control_systems[key].SystemList);
           option.setAttribute('data-multiplier',control_systems[key].ControlMultiplierCM);
           cs_select.appendChild(option);
       }
   }
   td_control_system.appendChild(cs_select);
   
   var td_product_type = document.createElement('td');
   td_product_type.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_type_select = document.createElement('select');
   product_type_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_type_select.className = 'type_id type_id_'+count;
   product_type_select.setAttribute('name','product[type_id][]');
   product_type_select.setAttribute('data-id',count);
   if(count == 0){
       product_type_select.setAttribute('required',true);
   }
   var product_types = self.product_types;
   for(var key in product_types){
       var option = document.createElement('option');
       option.innerHTML = product_types[key]['type_name'];
       option.setAttribute('value',product_types[key]['type_id']);
       product_type_select.appendChild(option);
   }
   td_product_type.appendChild(product_type_select);
   
   var td_product_name = document.createElement('td');
   td_product_name.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px; min-width:100px;');
   
   var product_input = document.createElement('input');
   product_input.setAttribute('type','text');
   product_input.setAttribute('style','padding:5px;  min-width:100px; width:100%; height: inherit;');
   product_input.className = 'item_name item_name_'+count;
   product_input.setAttribute('name','product[item_name][]');
   product_input.setAttribute('data-id',count);
   if(count == 0){
       product_input.setAttribute('required',true);
   }
   td_product_name.appendChild(product_input);
   td_product_name.innerHTML += '<input type="hidden" class="item_id item_id_'+count+'" name="product[item_id][]" />';
   td_product_name.innerHTML += '<input type="hidden" class="lifetime lifetime_'+count+'" name="product[lifetime][]" />';
   td_product_name.innerHTML += '<input type="hidden" class="lcp lcp_'+count+'" name="product[lcp][]" />';
   
   var td_control_device_upgrade_lighting = document.createElement('td');
    td_control_device_upgrade_lighting.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');

    var control_device_upgrade_lighting_select = document.createElement('select');
    control_device_upgrade_lighting_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
    control_device_upgrade_lighting_select.className = 'control_device_upgrade_lighting control_device_upgrade_lighting_'+count;
    control_device_upgrade_lighting_select.setAttribute('name','product[control_device_upgrade_lighting][]');
    control_device_upgrade_lighting_select.setAttribute('data-id',count);

    var control_device_upgrade_lighting_array = self.calculator_items.BCA;

    for(var key in control_device_upgrade_lighting_array){
        if(control_device_upgrade_lighting_array[key].SystemList !== undefined){
            var option = document.createElement('option');
            option.innerHTML = control_device_upgrade_lighting_array[key].SystemList;
            option.setAttribute('value',control_device_upgrade_lighting_array[key].SystemList);
            option.setAttribute('data-multiplier',control_device_upgrade_lighting_array[key].ControlMultiplierCM);
            control_device_upgrade_lighting_select.appendChild(option);
        }
    }
    td_control_device_upgrade_lighting.appendChild(control_device_upgrade_lighting_select);

   var td_sensor = document.createElement('td');
   td_sensor.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var sensor_select = document.createElement('select');
   sensor_select.setAttribute('style','padding:5px; width:100%; height: inherit;');
   sensor_select.className = 'item_sensor item_sensor_'+count;
   sensor_select.setAttribute('name','product[item_sensor][]');
   sensor_select.setAttribute('data-id',count);
   if(count == 0){
       sensor_select.setAttribute('required',true);
   }
   var option = document.createElement('option');
   option.innerHTML = 'Please Select';
   option.setAttribute('value','');
   sensor_select.appendChild(option);
       
   var sensors = [{'id': 1 , 'name' : 'Yes'},{'id': 0 , 'name' : 'No'}];
   for(var key in sensors){
       var option = document.createElement('option');
       option.innerHTML = sensors[key]['name'];
       option.setAttribute('value',sensors[key]['id']);
       sensor_select.appendChild(option);
   }
   td_sensor.appendChild(sensor_select);
   
   var td_product_qty = document.createElement('td');
   td_product_qty.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_qty_input = document.createElement('input');
   product_qty_input.setAttribute('type','text');
   product_qty_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_qty_input.className = 'item_qty item_qty_'+count;
   product_qty_input.setAttribute('name','product[item_qty][]');
   product_qty_input.setAttribute('data-id',count);
   if(count == 0){
       product_qty_input.setAttribute('required',true);
   }
   td_product_qty.appendChild(product_qty_input);
   
   
   var td_product_unit_cost = document.createElement('td');
   td_product_unit_cost.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_unit_cost_input = document.createElement('input');
   product_unit_cost_input.setAttribute('type','text');
   product_unit_cost_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_unit_cost_input.className = 'item_cost_excGST item_cost_excGST_'+count;
   product_unit_cost_input.setAttribute('name','product[item_cost_excGST][]');
   product_unit_cost_input.setAttribute('data-id',count);
   if(count == 0){
       product_unit_cost_input.setAttribute('required',true);
   }
   td_product_unit_cost.appendChild(product_unit_cost_input);
   
   
   var td_product_total_cost = document.createElement('td');
   td_product_total_cost.setAttribute('style','padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px; height:40px;');
   
   var product_total_cost_input = document.createElement('input');
   product_total_cost_input.setAttribute('type','text');
   product_total_cost_input.setAttribute('style','padding:5px; width:100%; height: inherit;');
   product_total_cost_input.className = 'item_total_cost_excGst item_total_cost_excGst_'+count;
   product_total_cost_input.setAttribute('name','product[item_total_cost_excGst][]');
   product_total_cost_input.setAttribute('data-id',count);
   if(count == 0){
       product_total_cost_input.setAttribute('required',true);
   }
   td_product_total_cost.appendChild(product_total_cost_input);
   
   
   tr.appendChild(td_room_area);
   tr.appendChild(td_ch);
   tr.appendChild(td_space_type);
   tr.appendChild(td_bca);
   tr.appendChild(td_air_con);
   tr.appendChild(td_existing_qty);
   tr.appendChild(td_lamp_and_ballast);
   tr.appendChild(td_nominal_lamp_power);
   tr.appendChild(td_control_system);
   tr.appendChild(td_product_type);
   tr.appendChild(td_product_name);
   tr.appendChild(td_control_device_upgrade_lighting);
   tr.appendChild(td_sensor);
   tr.appendChild(td_product_qty);
   tr.appendChild(td_product_unit_cost);
   tr.appendChild(td_product_total_cost);
   
   document.getElementById('booking_items_body_nsw').appendChild(tr);
};
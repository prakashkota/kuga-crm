<html>

<head>
    <title>Solar Proposal</title>
    <style>
        .page2-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            width: 910px;
            height: 1300px;
            box-shadow: 1px 1px 3px 1px #333;
            border-collapse: separate;
            margin-bottom: 20px;
        }

        .page2 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page2 .top hr {
            width: 65%;
            height: 1px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page2 .text_color {
            color: #D01E2A;
        }

        .page2 .topimagepagee h2 {
            background-color: #840e12c2;
            font-size: 25px;
            padding-top: 35;
            padding-bottom: 35;
            text-align: center;
            color: #ffffff;
        }

        .page2 .whykuga p {
            margin-left: 48;
            color: #423939;
            margin-right: 48;
            font-size: 20px;
            font-weight: 500;
            text-align: justify;
        }

        .page2 .whykuga h3 {
            margin-left: 48;
            margin-top: 65px;
            color: #D52323;
        }

        .page2 .topimagepage {
            margin-top: 30px;
        }

        .page2 .topimagepagee {
            margin-top: -120px;
            position: absolute;
            width: 910px;
        }

        /** Footer */
        .page2 .footer {
            display: inline-flex;
        }

        .page2 .fimagee img {
            margin-left: 730px;
            margin-top: 85px;
        }

        .page2 .pb {
            padding-bottom: 235px;
        }

        .page2 .zaspan {
            margin-left: 0px;
        }

        .page2 .footer-p p {
            font-size: 10px;
            margin-left: 48;
            margin-top: -23px;
            display: none;
        }

        .page2 .footer-pp p {
            font-size: 10px;
            margin-left: 46px;
            margin-top: -22px;
        }

        .page2 .footer-pp {
            margin-left: 286px;
            margin-top: -36px;
            display: none;
        }

        .page2 .zafooter-icon img {
            width: 25px;
            margin-right: 10px;
            display: none;
        }

        .page2 .zafooter-iconn img {
            width: 25px;
            margin-right: 10px;
        }

        .page2 .zayear {
            font-size: 17px;
        }

        .page2 .redf {
            height: 10px;
            background: red;
            width: 455px;
        }

        .page2 .greyf {
            height: 10px;
            background: #dadada;
            width: 455px;
        }

        .page2 #myVideo {
            right: 0;
            bottom: 0;
            width: 909px;
            object-fit: cover;
            height: 500px;
        }
    </style>
</head>

<body>
    <div class="page2-back-bg page2">
        <div class="top">
            <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="topimagepage">
            <video autoplay muted loop id="myVideo">
                <source src="<?php echo site_url('assets/solar_proposal_pdf_new/KugaLanding-new.mp4'); ?>" type="video/mp4">
            </video>
            <div class="topimagepagee">
                <h2>LEADERS IN COMMERCIAL SOLAR</h2>
            </div>
        </div>
        <div class="whykuga">
            <h3>What to expect when dealing with the team at KUGA</h3>
            <p>We have a strong proven track record over the past 5 years. Our team has
                successfully completed over 5,000 projects Australia wide. Kuga’s purpose is to
                generate savings for businesses at a really healthy return on investment. Our key
                services are Solar Power, Solar Battery Storage, LED Lighting Upgrades, Power
                Factor Correction, Electrical Services and Diesel Generator installations.
            </p>
            <p>When dealing with our team at Kuga, you can expect highly trained individuals who are
                values-driven and understand our products and services to a really high level. As
                our company is a business to business organisation, our reputation is important to us.
                Many of our team have been here since we started way back in 2016. We aim to
                impress our customers as it is our key to gain more prospective customers.
            </p>
            <p>Our industry is very competitive, so we remain best in the market by buying large
                volumes from quality manufacturers and by delivering projects on time and to budget
                with our own internal Kuga Electricians. We then pass on all the benefits to our
                customers by providing a really competitive price. Our purpose is to save businesses
                money.
            </p>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
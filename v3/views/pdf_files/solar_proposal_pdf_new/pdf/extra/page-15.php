<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page15 .back-bg {
		background-color: #fff;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page15 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 654px;
		padding-top: 30px;
	}
	
	.page15 .top hr {
		width: 65%;
		height: 2px;
		margin: -24px 0px 0px 52px;
		background-color: #D01E2A;
	}
	
	.page15 .text_color {
		color: #D01E2A;
	}
	
	.page15 .title {
		margin-top: -50px;
	}
	
	.page15 .title h1 {
		padding-left: 14px;
		margin-left: 48px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
		border-height: 10px;
	}
	
	.page15 .plasting h2 {
		font-size: 16px;
		padding: 12px;
		margin-left: 48px;
		margin-top: 100px;
		background-color: #D01E2A;
		width: 69%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page15 .displayyy {
		display: inline-flex;
	}
	
	.page15 .mt45 {
		margin-top: -45px;
	}
	
	.page15 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page15 .iiimage img {
		width: 384px;
		margin-left: 48px;
		margin-top: 19px;
	}
	
	.page15 .fimagee img {
		margin-left: 650px;
		width: 230;
	}
	</style>
</head>

<body>
	<div class="back-bg page15">
		<div class="top">
			<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
				<hr>
			</div>
			<div class="title">
				<h1>DAILY  <span class="text_color"> AVERAGE PRODUCTION</span></h1>
			</div>
			<div class="displayyy mt45">
				<div class="iiimage plasting">
					<h2>JANUARY - MARCH</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0015.jpg'); ?>"> </div>
		<div class="iiimage plasting">
			<h2>APRIL - JUNE</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/00015.jpg'); ?>"> </div>
	</div>
	<div class="displayyy">
		<div class="iiimage plasting">
			<h2>JUNE - SEPTEMBER</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/000015.jpg'); ?>"> </div>
		<div class="iiimage plasting">
			<h2>OCTOBER - DECEMBER</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0000015.jpg'); ?>"> </div>
	</div>
	<div style="padding-bottom: 100px;"></div>
	<div class="fimagee" style="margin-bottom: -70px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
	<div class="footer"></div>
	</div>
</body>

</html>

</html>
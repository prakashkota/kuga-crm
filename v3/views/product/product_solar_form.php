
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Admin  - Add Solar Product</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" name="productAdd" id="productAdd" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Product Details</h4>
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="text" name="product_name"  class="form-control" placeholder="Product Name" value="<?php echo (!empty($product_data)) ? $product_data['product_name'] : ''; ?>"  required>
                                        <?php echo form_error('product_name', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Type</label>
                                        <select name="type_id" class="form-control"  required>
                                            <?php foreach ($product_types as $key => $value) { ?>
                                                <option value="<?php echo $value['type_id']; ?>" <?php echo (!empty($product_data) && $product_data['type_id'] == $value['type_id']) ? 'selected' : ''; ?>>
                                                    <?php echo $value['type_name']; ?> 
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('type_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Part No.</label>
                                        <input type="text" name="part_no"  class="form-control" placeholder="Part No." value="<?php echo (!empty($product_data)) ? $product_data['part_no'] : ''; ?>"  required>
                                        <?php echo form_error('part_no', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Manufacturer</label>
                                        <input type="text" name="manufacturer"  class="form-control" placeholder="Manufacturer" value="<?php echo (!empty($product_data)) ? $product_data['manufacturer'] : ''; ?>"  required>
                                        <?php echo form_error('manufacturer', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Length</label>
                                                <input type="number" step="0.001" min="0" oninput="this.value=(this.value < 0) ?'' : this.value" name="length"  class="form-control"   placeholder="Length" value="<?php echo (!empty($product_data)) ? $product_data['length'] : ''; ?>">
                                            </div>    
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Width</label>
                                                <input type="number" step="0.001" min="0" oninput="this.value=(this.value < 0) ?'' : this.value" name="width"  class="form-control"   placeholder="Width" value="<?php echo (!empty($product_data)) ? $product_data['width'] : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                <label>Wattage</label>
                                                <input type="number" name="power_wattage"  class="form-control" placeholder="Wattage" value="<?php echo (!empty($product_data)) ? $product_data['power_wattage'] : ''; ?>" required>
                                                <?php echo form_error('power_wattage', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Note</label>
                                        <textarea name="note"  class="form-control" placeholder="Note"  ><?php echo (!empty($product_data)) ? $product_data['note'] : ''; ?></textarea>
                                        <?php echo form_error('note', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Attachment</label>
                                        <div class="custom-file">
                                            <input type="file" name="product_file"  class="custom-file-input" >
                                            <input type="hidden" name="attachment" id="attachment" class="form-control" value="<?php echo (!empty($product_data)) ? $product_data['attachment'] : ''; ?>">
                                            <label class="custom-file-label" for="datasheets"><?php echo (!empty($product_data) && $product_data['attachment'] != '') ? $product_data['attachment'] : 'Choose file'; ?></label>
                                        </div>
                                        <div id="loader"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($product_data) && $product_data['attachment'] != ''){ ?>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <a href="<?php echo site_url('assets/uploads/product_files/'.$product_data['attachment']); ?>" target="__blank">
                                            <img src="<?php echo site_url('assets/uploads/product_files/'.$product_data['attachment']); ?>" style="max-height:711px;" >
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="buttwrap">
                        <input type="submit"   class="btn" value="SAVE">
                    </div>                                            
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('input[name=product_file]').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $('input[name=product_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'product_files');
        form_data.append('do_compress', true);
        $.ajax({
            url: base_url + 'admin/uploader/upload_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                $('#loader').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#attachment').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error:function (res) {
                $('#loader').html('');
                toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
            }
        });
    });
</script>
</body>
</html>


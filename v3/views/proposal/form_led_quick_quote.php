<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<style>
    @media print {
        .no-print {display: none;}
        body {background: transparent;}
        .black-bg{background-color:#010101;}
        .red{color:#c32027}
        .control-label:after {
            content:"*";
            color:red;
        }
    }
    .page-wrapper{ min-height:100%;}

    .sr-deal_actions{
        opacity: 1;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: 0px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }
    .is-invalid{border: 1px solid red;}
    @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 300px;
        }
    }
</style>

<div class="page-wrapper d-block clearfix " >
    <div style="padding:0; margin:0">
        <form id="quick_quote_form">
         <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo site_url('assets/images/led_quote_form.jpg'); ?>" alt="" width="910" height="126" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                        <tbody>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Quote #</td>
                                                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                                                                    <input required="" id="quote_no" name="quote[quote_no]" value="<?php echo $quote_no; ?>" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                                                </td>
                                                            </tr>
                                                        </tbody></table></td>
                                                <td width="415" valign="top" style="padding-left:10px">
                                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Bussiness Name:</td>
                                                                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_company_name" name="quote[customer_company_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody></table></td>
                                            </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Name:</td>
                                                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_name" name="quote[customer_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody></table></td>
                                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Contact No:</td>
                                                                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="customer_contact_no" name="quote[customer_contact_no]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody></table></td>
                                            </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Address:</td>
                                                <td width="708" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_address" name="quote[customer_address]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <?php /**<tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Suburb:</td>
                                                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="customer_suburb" name="quote[customer_suburb]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody></table></td>
                                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Postcode:</td>
                                                                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_postcode" name="quote[customer_postcode]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody></table></td>
                                            </tr>
                                        </tbody></table></td>
                            </tr> */ ?>
                            <tr>
                                <td height="30"></td>
                            </tr>
                            <tr>
                                <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    Work Requested:
                                </td>
                            </tr>
                        </tbody></table></td>
            </tr>
            <td valign="top"></td> </tr>

            <tr>
                <td valign="top">
                    <table width="828" border="1" id="quote_items" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                        <tr> 
                            <th height="40" width="510" colspan="2" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Description of Product/Service</th>
                            <th width="100" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Units</th>
                            <th width="109" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Cost Per Unit</th>
                            <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Cost exc GST</th>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_0" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_0" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_0" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_0" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_1" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_1" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_1" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_1" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_2" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_2" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_2" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_2" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_3" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_3" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_3" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_3" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_4" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_4" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_4" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_4" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_5" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_5" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_5" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_5" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_6" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_6" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_6" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_6" readonly="" type="number" />
                            </td>
                        </tr>
                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_7" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_7" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_7" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_7" readonly="" type="number" />
                            </td>
                        </tr>

                        <tr>
                            <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_name item_name_8" name="quote_product[item_name][]" type="text" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_qty item_qty_8" name="quote_product[item_qty][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_cost_excGST item_cost_excGST_8" name="quote_product[item_cost_excGST][]" type="number" />
                            </td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; width:100%; height: 40px;" class="item_total_cost_excGst item_total_cost_excGst_8" readonly="" type="number" />
                            </td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="510" height="40" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Sales Representative:</td>
                            <td width="109" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>Sub Total exc GST:</strong></td>
                            <td width="154" style="border-top:none; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; height: 40px;" id="sub_total_excGst" readonly="" name="quote[total_excGst]" type="number" required="">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" height="40" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">
                                <input id="rep_name" name="quote[rep_name]" style="padding:5px; width:510px; height: inherit;" type="text" required="" />
                            </td>
                            <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>Total inc GST:</strong></td>
                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                <input style="padding:5px; height: 40px;" id="total_incGst" readonly="" name="quote[total_incGst]" type="number" required="">
                            </td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td height="35" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333333"><strong>Terms &amp; Conditions</strong></td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                    * Payment required by Cash, Cheque or Credit Card or Direct Deposit at the completion of the job.
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                    * Payment is due on the day of the completion of the work.
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                    * All materials remain the property of Kuga Electrical until the full balance is paid. 
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                    * The quote is valid for 14 days from  <input name="quote[quote_expires_at]" id="quote_expires_at" style="padding:5px; height: 30px;" type="text" required="" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/images/bottom_right.jpg'; ?>" width="117" height="56" /></td>
      </tr>
    </table></td>
  </tr>
        </table>
        </form>
    </div>
    <div  class="sr-deal_actions">
        <div class="col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="generate_quote_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save and  Generate Pdf</a>
        </div>
        <div class=" col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="close_quote" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-close"></i> Close</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/quick_quote.js?v=<?php echo version; ?>"></script>
<script>
    var context = {};
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.quote_data = '<?php echo (isset($quote_data) && !empty($quote_data)) ? json_encode($quote_data, JSON_HEX_APOS) : ''; ?>';
    context.quote_items = '<?php echo (isset($quote_items) && !empty($quote_items)) ? json_encode($quote_items, JSON_HEX_APOS) : ''; ?>';
    context.gst = '<?php echo GST ?>';
    var quote_manager = new quick_quote_manager(context);
</script>
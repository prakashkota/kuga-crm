<style>
    .capitalize {
        text-transform:capitalize;
    }   
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .nav.nav-tabs.nav-topline .nav-item a.nav-link.active {
        background: none;
        box-shadow: inset 0 3px 0 #ffd914;
        color: #000;
        border-radius: 0;
        border-top-color: #ffd914;
        border-bottom: none;
    }
    .nav.nav-tabs.nav-topline .nav-item a.nav-link:hover{
        color: #000;
    }
    tfoot{display: block !important;}


    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .dropdown-menu > li > label {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.428571429;
    }
    .deal_view__actions{    
        display: flex;
        box-sizing: border-box;
        align-items: flex-start
    }

    .dt-button-collection button.buttons-columnVisibility:before,
    .dt-button-collection button.buttons-columnVisibility.active span:before {
        display:block;
        position:absolute;
        top:1.2em;
        left:0;
        width:12px;
        height:12px;
        box-sizing:border-box;
    }

    .dt-button-collection button.buttons-columnVisibility:before {
        content:' ';
        margin-top:-6px;
        margin-left:10px;
        border:1px solid black;
        border-radius:3px;
    }

    .dt-button-collection button.buttons-columnVisibility.active span:before {
        content:'\2714';
        margin-top:-11px;
        margin-left:12px;
        text-align:center;
        text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
    }

    .dt-button-collection button.buttons-columnVisibility span {
        margin-left:20px;    
    }

</style>
<div class="page-wrapper d-block clearfix">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="kg-page__heading">
            <div class="kg-page__heading_title">
                Won Leads
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div id="statistics_container">
                <div class="row ">
                <div class="col-12 col-sm-12 col-md-12"> 
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h2 >Leads List</h2>
                                <div class="table-responsive">
                                    <table id="lead_list" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Customer Name</th>
                                                <th>Company</th>
                                                <th>State</th>
                                                <th>Address</th>
                                                <th>Contact Number</th>
                                                <th>Solar System Size (kW)</th>
                                                <th>Battery Size (kWh)</th>
                                                <th>HB</th>
                                                <th>BL</th>
                                                <th>PL</th>
                                                <th>FL</th>
                                                <th>DL</th>
                                                <th>Others</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lead_list_body" >

                                        </tbody>
                                    </table>
                                </div>
                                <div id="table_loader" ></div>
                                <div  id="pagination" class="mt-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div> 

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/pagination/simplePagination.css'); ?>" />
    <script type="text/javascript" src="<?php echo site_url('assets/pagination/jquery.simplePagination.js'); ?>"></script>
    <script src="<?php echo site_url(); ?>common/js/reports/won_leads.js?v=<?php echo version; ?>"></script>
    <script>
        var context = {};
        context.query_params = '<?php echo $query_params; ?>';
        var deal_manager_tool = new deal_manager(context);
    </script>
</body>

</html>

<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Change Password</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                 <?php $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : ''; ?>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                       <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" name="password" id="password"  value="<?php echo (isset($password)) ? $password : '' ; ?>" class="form-control" placeholder="Password"  >
                                 <?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="buttwrap">
                        <input type="submit" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
// echo '<pre>';
// print_r($TotalSavingsArr);
$monthlyPaymentPlanSolar = 0;
if(isset($proposal_finance_data['monthly_payment_plan'])){ 
  //$monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan']  * 12; //* 1.1 Nishan has confirmed to remove this 1.1 multiplication
  //Change Given by anthony on apr 15 2020
  $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'] * $year * 12;
  $annualProduction   =  $AverageDailyProduction * 365;
  for($i = 0; $i < 10; $i++) {
    if($i == 0){
      $annualProductionArr1[$i] = 0 + $annualProduction;
    }else{
      $annualProductionArr1[$i] = ($annualProductionArr1[$i - 1])  - (($annualProductionArr1[$i - 1]) * 0.006); 
    }
    $annualProduction10Yr = array_sum($annualProductionArr1);
  }
  $solar_energy_rate = ($monthlyPaymentPlanSolar/$annualProduction10Yr) * 100;
  $solar_energy_rate = number_format((float) ($solar_energy_rate), '2','.','') . 'c/kWh';
}else{
  $proposal_finance_data['monthly_payment_plan'] = 0;
  $proposal_finance_data['term'] = 0;
}

$Cashflow_Term = $Cashflow_15Yr = 0;//$monthlyPaymentPlanSolar;
$fin = 0;
$TotalInvestment = 0;

$Cashflow_10Yr = 0;
$cashflow = [];
$cashflowSumByYear = [0];
$savingsSum = [];
$cashFlowSum = [];

//echo "<pre>";
//print_r($TotalSavingsArr);
//die;
for ($t = 0; $t < 15; $t++) {
    
  if ($t < $year) {
    $fin = $monthlyPaymentPlanSolar;
    $cashflow[$t] = $TotalSavingsArr[$t] - $fin;
  } else {
    $fin = 0;
    $cashflow[$t] = $TotalSavingsArr[$t] - $fin;
  }
  
  if($t == 0){
    $savingsSum[$t] = 0 + $TotalSavingsArr[$t];
    $cashFlowSum[$t] = $savingsSum[$t] - $monthlyPaymentPlanSolar;
  }else{
    $savingsSum[$t] = $savingsSum[($t - 1)] + $TotalSavingsArr[$t];
    $cashFlowSum[$t] = $savingsSum[$t] - $monthlyPaymentPlanSolar;
  }
}


/**for($i=0;$i<$year;$i++){
    $Cashflow_Term = $Cashflow_Term + $cashflow[$i];
}*/
//echo $savingsSum[($year - 1)] . '-' . $monthlyPaymentPlanSolar;die;
$Cashflow_Term = $savingsSum[($year - 1)] - $monthlyPaymentPlanSolar;

for($i=0;$i<10;$i++){
    $Cashflow_10Yr = $Cashflow_10Yr + $cashflow[$i];
}

if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
    //print_r($annualProduction10Yr);
}
//print_r($savingsSum[($year - 1)]);die;
$Cashflow_15Yr = array_sum($cashflow);
// echo 'cashflow = '.$Cashflow_Term ;
// echo '<br>';

// echo '$Cashflow_10Yr = '.$Cashflow_10Yr ;
// echo '<br>';

// echo '$Cashflow_15Yr = '.$Cashflow_15Yr ;
// echo '<br>';die;    


if ($proposal_data['rate'] == 1) {
  $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 0, '', ',') . ", Distributor Charge: " . number_format((float) ($proposal_data['distributor_charge']), 0, '', ',');
}
if ($proposal_data['rate'] == 2) {
  $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 0, '', ',') . ", Off-Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_off_peak']), 0, '', ',') . ", Distributor Charge: " . number_format((float) ($proposal_data['distributor_charge']), 0, '', ',');
}
if ($proposal_data['rate'] == 3) {
  $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 0, '', ',') . ", Off-Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_off_peak']), 0, '', ',') . ", Shoulder Rate: " . ($proposal_data['solar_cost_per_kwh_shoulder']) . ", Distributor Charge: " . ($proposal_data['distributor_charge']);
}
$price_before_stc_rebate = $proposal_data['price_before_stc_rebate'];
$RateOfReturnFinanced = 0;
if($proposal_data['price_before_stc_rebate'] != 0){
  $RateOfReturnFinanced = $TotalSavingsArr[0] / $price_before_stc_rebate;
}
$RateOfReturnFinanced = $RateOfReturnFinanced * 100;

?>

<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tbody>
    <tr>
      <td>
        <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/top-heading.jpg" alt="">
      </td>
    </tr> 

    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="780" valign="top">
             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding:30px 0 0px 50px;">
               <tr>
                 <td style="padding:0 0 30px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/heading4.jpg" alt=""></td>                     
               </tr>
               <tr>
                 <td style="padding:0 0 30px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/sub-heading.jpg" alt=""></td>
               </tr>

               <tr>
                 <td style="padding-bottom:20px">
                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:20px; padding-right:20px;">
                    <tr>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>RETURN ON INVESTMENT:</strong></td>
                      <!--<td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php //echo number_format((float) ($roi), 0, '', ','); ?></td>-->
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;"><?php echo number_format($roi,1). ($roi>1?' Years':' Year'); ?> </td>
                    </tr>
                    <tr>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>FIRST YEAR SAVINGS:</strong></td>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($savingsSum[0]), 0, '', ','); ?></td>
                    </tr>
                    <tr>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>10 YEAR SAVINGS:</strong></td>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($savingsSum[9]), 0, '', ','); ?></td>
                    </tr>
                    <tr>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>15 YEAR SAVINGS:</strong></td>
                      <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($savingsSum[14]), 0, '', ','); ?></td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
               <td style="padding:0 0 30px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/sub-heading1.jpg" alt=""></td>
             </tr>

             <tr>
               <td style="padding-bottom:20px">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:20px; padding-right:20px;">
                  <tr>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>CASHFLOW OVER FINANCE TERM:</strong></td>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($Cashflow_Term), 0, '', ','); ?></td>
                  </tr>
                  <tr>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>10 YEAR CASHFLOW:</strong></td>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($cashFlowSum[9]), 0, '', ','); ?></td>
                  </tr>
                  <tr>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/arrow.jpg" alt="" style=" float:left; margin:2px 15px 0 0;"> <strong>15 YEAR CASHFLOW:</strong></td>
                    <td style="border-bottom: solid 1px #edb2b5; padding:10px 10px; text-align:right;">$<?php echo number_format((float) ($cashFlowSum[14]), 0, '', ','); ?></td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>
             <td>
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:20px; padding-right:80px;">
                <tr>
                  <?php /** <td width="48%" style="text-align:center;"><span style="display:block; font-size:14px; font-weight:700; padding-bottom:15px;">TOTAL 10 YEAR SAVINGS</span> <strong style="border:solid 2px #000; font-size:18px; display:block; padding:15px 0;">$<?php echo number_format((float) ($savingsSum[0]), 0, '', ','); ?></strong></td> */ ?>
                  <td width="48%" style="text-align:center;"><span style="color:#ce2931;display:block; font-size:14px; font-weight:700; padding-bottom:15px;">10 YEARS SOLAR ENERGY RATE</span> <strong style="border:solid 2px #ce2931; font-size:18px; display:block; padding:15px 0; background:#ce2931; color:#fff;"><?php echo $solar_energy_rate;?></strong></td>
                  <td width="4%">&nbsp;</td>
                  <td width="48%" style="text-align:center;"><span style="display:block; font-size:14px; font-weight:700; padding-bottom:15px;">TOTAL STC FINANCIAL INCENTIVE</span> <strong style="border:solid 2px #000; font-size:18px; display:block; padding:15px 0;">$<?php echo number_format((float) ($proposal_data['stc_rebate_value']), 0, '', ','); ?></strong></td>
                </tr>

                <tr>
                  <td width="48%" style="height:30px;">&nbsp;</td>
                  <td width="4%" style="height:30px;">&nbsp;</td>
                  <td width="48%" style="height:30px;">&nbsp;</td>
                </tr>
                <?php /**
                <tr>
                  <td width="48%" style="text-align:center;"><span style="display:block; font-size:14px; font-weight:700; padding-bottom:15px; color:#ce2931">25 YEARS BENEFITS</span> <strong style="border:solid 2px #ce2931; font-size:18px; display:block; padding:15px 0;color:#ce2931">$<?php echo number_format((float) ($twentyFiveYearsBenefit), 0, '', ','); ?></strong></td>
                  <td width="4%">&nbsp;</td>
                  <td width="48%" style="text-align:center;"><span style="color:#ce2931;display:block; font-size:14px; font-weight:700; padding-bottom:15px;">10 YEARS SOLAR ENERGY RATE</span> <strong style="border:solid 2px #ce2931; font-size:18px; display:block; padding:15px 0; background:#ce2931; color:#fff;"><?php echo $solar_energy_rate;?></strong></td>
                </tr>
                */ ?>
              </table>
            </td>
          </tr>

          <tr>
           <td style="padding:30px 80px 0 0; font-size:15px;">Total revenue includes potential LGC and STC savings, STC and LGC savings are determined by differing market conditions<br /> <br />
             Monthly payment of $<?php echo number_format((float) ($proposal_finance_data['monthly_payment_plan']), 2, '.', ','); ?> excl. Gst with <?php echo $term; ?> years payment terms, calculated with <?php echo (int) $proposal_data['annual_inc_in_ec']; ?>% average annual increase in energy cost from energy provider
           </td>
         </tr>
         
         <tr>
             <td>
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:20px; padding-right:80px;">
                <tr>
                  <td width="48%" style="text-align:center;"><span style="color:#ce2931;display:block; font-size:14px; font-weight:700; padding-bottom:15px;">&nbsp;</span> <strong style="border:solid 2px #fff; font-size:18px; display:block; padding:15px 0; background:#fff; color:#fff;">&nbsp;</strong></td>
                  <td width="4%">&nbsp;</td>
                  <td width="48%" style="text-align:center;"><span style="display:block; font-size:14px; font-weight:700; padding-bottom:15px;">&nbsp;</span> <strong style="border:solid 2px #fff; font-size:18px; display:block; padding:15px 0;">&nbsp;</strong></td>
                </tr>
              </table>
            </td>
          </tr>
         
       </table>
     </td>
     <td valign="top"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/right-img.jpg"></td>
   </tr>
 </table>
</td>
</tr> 

<tr>
  <td>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" width="409" style="padding:20px 50px 0; text-align:left; font-size:13px; color:#666;">WWW.13KUGA.COM.AU | <?php echo $lead_data['company_contact_no']; ?></td>
        <td valign="top"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/bot-img.jpg" alt=""></td>
      </tr>
    </table>
  </td>
</tr>
</tbody>
</table>

var meter_data_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.meter_data = (options.meter_data && options.meter_data != '') ? JSON.parse(options.meter_data) : {};
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.subtotal_excGST = 0;
    this.total_incGST = 0;
     this.signature_pad_ids = ['signature'];
    if (Object.keys(self.meter_data).length > 0) {
        $('#account_name').val(self.meter_data.account_name);
        $('#abn').val(self.meter_data.abn);
        $('#address').val(self.meter_data.address);
        $('#energy_retailer').val(self.meter_data.energy_retailer);
        $('#nmi').val(self.meter_data.nmi);
        $('#rep_name').val(self.meter_data.rep_name);
        $('#from_date').val(self.meter_data.from_date);
        $('#to_date').val(self.meter_data.to_date);
        $('#request_date').val(self.meter_data.request_date);

        $(".datepicker").datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date(),
            setDate : new Date()
        });

        if(self.meter_data.request_date){
            self.show_signature_image($('#signature'), self.meter_data.signature);
        }


    } else if (Object.keys(self.lead_data).length > 0) {
        $('#account_name').val(self.lead_data.customer_company_name);
        $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#customer_contact_no').val(self.lead_data.customer_contact_no);
        $('#address').val(self.lead_data.address);
        $('#customer_postcode').val(self.lead_data.postcode);
        $('#rep_name').val(self.lead_data.full_name);

        $(".datepicker").datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date()
        });
        $(".datepicker").val(moment(new Date()).format('DD/MM/YYYY'));
    }

    $('#save_meter_data').click(function () {
        self.save_meter_data();
    });

    $('#generate_meter_data_pdf').click(function () {
        var flag = self.validate_meter_data();
        if (!flag) {
            toastr["error"]('Please fill the fields marked in red');
            return false;
        }
        if (self.meter_data.uuid != undefined) {
            var url = base_url + 'admin/lead/meter_data_pdf_download/' + self.meter_data.uuid;
            document.getElementById('generate_meter_data_pdf').setAttribute('href', url);
            document.getElementById('generate_meter_data_pdf').click();
        } else {
            toastr["error"]('Please fill data then save.');
            return false;
        }
    });

    //Handle Singature Pads
    $('.sign_create').click(function () {
        var id = $(this).attr('data-id');
        $('#meter_data_form').hide();
        $('.sr-deal_actions').hide();
        $('#signatures').show();
        for (var i = 0; i < 2; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $('#signpad_create_' + id).removeClass('hidden');
        window.scrollTo({top: 0, behavior: 'smooth'});
    });

    $('.sign_close').click(function () {
        var id = $(this).attr('data-id');
        $('#meter_data_form').show();
        $('.sr-deal_actions').show();
        $('#signatures').hide();
        for (var i = 0; i < 2; i++) {
            $('#signpad_create_' + i).addClass('hidden');
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#sign_create_" + id).offset().top
        }, 0);
    });

    self.create_signature_pad();

};

meter_data_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


meter_data_manager.prototype.validate_meter_data = function () {
    var self = this;
    var flag = true;
    var elements = document.getElementById("meter_data_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        $(elements[i]).parent().removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                }
                flag = false;
            }
        }
    }
    return flag;
};

meter_data_manager.prototype.save_meter_data = function () {
    var self = this;

    if (self.meter_data.uuid == undefined) {
        var uuid = self.create_uuid();
        self.meter_data.uuid = uuid;
    }

    var formData = $('#meter_data_form').serialize();
    formData += '&meter_data[uuid]=' + self.meter_data.uuid;
    formData += '&meter_data[lead_id]=' + self.lead_data.id;

    $.ajax({
        url: base_url + 'admin/lead/save_meter_data',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('Saving Form Data, Please Wait....');
            $("#save_meter_data").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.meter_data.id == undefined) {
                    window.history.pushState(self.meter_data, '', base_url + 'admin/lead/edit_meter_data/' + self.meter_data.uuid);
                }
                $("#save_meter_data").removeAttr("disabled");
            } else {
                toastr["error"](response.status);
                $("#save_meter_data").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_meter_data").removeAttr("disabled");
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


meter_data_manager.prototype.create_signature_pad = function () {
    var self = this;
    var canvas1 = document.getElementById("signature_pad_1");

    var clearButton = $(".sign_clear");
    var undoButton = $(".sign_undo");
    var savePNGButton = $(".sign_save");

    var signaturePad1 = new SignaturePad(canvas1, {
        backgroundColor: 'rgb(255, 255, 255)',
        penColor: '#0300FD'
    });

    var arr = [signaturePad1];
    self.signature_pad = arr;
    var arr1 = self.signature_pad_ids;

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();
    }

    //window.onresize = resizeCanvas;
    window.onorientationchange = resizeCanvas;
    resizeCanvas();

    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    clearButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        console.log(arr[(id - 1)]);
        arr[(id - 1)].clear();
    });

    undoButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        var data = arr[(id - 1)].toData();
        if (data) {
            data.pop(); // remove the last dot or line
            arr[(id - 1)].fromData(data);
        }
    });

    savePNGButton.on("click", function (event) {
        var id = this.getAttribute('data-id');
        if (arr[(id - 1)].isEmpty()) {
            alert("Please provide a signature first.");
        } else {
            $('#signpad_close_' + id).trigger('click');
            var dataURL = arr[(id - 1)].toDataURL();
            var ele = $('#' + arr1[(id - 1)]);
            self.show_signature_image(ele, dataURL, true);
        }
    });
};

meter_data_manager.prototype.show_signature_image = function (ele, image, is_data_url) {
    var self = this;
    if (image != '') {
        ele.val(image);
        if (is_data_url) {
            ele.prev().html('<img style="height:95px;" src="' + image + '" />');
        } else {
            ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/meter_data_form_files/' + image + '" />');
        }
    }
};
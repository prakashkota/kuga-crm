<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page14 .back-bg {
		background-color: #FFFFFF;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page14 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 654px;
		padding-top: 30px;
	}
	
	.page14 .top hr {
		width: 65%;
		height: 2px;
		margin: -24px 0px 0px 52px;
		background-color: #D01E2A;
	}
	
	.page14 .text_color {
		color: #D01E2A;
	}
	
	.page14 .title {
		margin-top: -50px;
	}
	
	.page14 .title h1 {
		padding-left: 14px;
		margin-left: 48px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
		border-height: 10px;
	}
	
	.page14 .title h2 {
		padding: 12px;
		margin-left: 48px;
		margin-top: 100px;
		background-color: #D01E2A;
		width: 50%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page14 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page14 .enimage img {
		margin-top: 15px;
		margin-left: 50px;
	}
	
	.page14 .fimagee img {
		margin-left: 650px;
		width: 230;
	}
	</style>
</head>

<body>
	<div class="back-bg page14">
		<div class="top">
			<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
			<hr>
		</div>
		<div class="title">
			<h1>ENERGY <span class="text_color"> CONSUMPTION</span></h1>
			<h2> AVERAGE MONTHLY</h2> 
		</div>
		<div class="enimage"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0014.jpg'); ?>" width="800px"> </div>
		<div class="title">
			<h2>AVERAGE WEEK DAY</h2> 
		</div>
		<div class="enimage"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/00144.jpg'); ?>" width="800px"> </div>
		<div style="padding-bottom: 100px;"></div>
		<div class="fimagee" style="margin-bottom: -67px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
		<div class="footer"></div>
	</div>
</body>

</html>
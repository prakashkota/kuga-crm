<div id="send_proposal_to_colleague_modal" class="modal " role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-800">
                <h5 class="modal-title">Forward To Colleague</h5>
                <button type="button" class="close closemediapop" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <form id="send_proposal_to_colleague_form">

                    <div class="form-group control-group">
                        <label>Name<span style="color:red">*</span></label>
                        <input class="form-control" type="text" name="name" value="" id="name">
                    </div>
                    <div class="form-group control-group">
                        <label>Mobile<span style="color:red">*</span></label>
                        <input class="form-control" type="text" name="mobile" value="" id="mobile">
                    </div>
                    <div class="form-group control-group">
                        <label>Email<span style="color:red">*</span></label>
                        <input class="form-control" type="email" name="email_to" value="" id="email_to">
                    </div>
                    <div class="form-group control-group">
                        <label>Position</label>
                        <input class="form-control" type="text" name="position" value="" id="position">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="send_proposal_to_colleague_send_btn">Send</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

<!-- {ACCESS_CODE} -->
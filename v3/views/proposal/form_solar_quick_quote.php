<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<link rel="stylesheet" type="text/css" id="u0" referrerpolicy="origin" href="https://cdn.tiny.cloud/1/q4hj2x73ddhzsvx7zf9mpc8qda8k4e8upc7jmzi6mm158ej9/tinymce/5.4.1-89/skins/ui/oxide/skin.min.css">
<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=q4hj2x73ddhzsvx7zf9mpc8qda8k4e8upc7jmzi6mm158ej9"></script>
<style>
  @media print {
    .no-print {display: none;}
    body {background: transparent;}
    .black-bg{background-color:#010101;}
    .red{color:#c32027}
    .control-label:after {
      content:"*";
      color:red;
    }
  }
  .page-wrapper{ min-height:100%;}

  .sr-deal_actions{
    opacity: 1;
    position: fixed;
    left: 100px;
    right: 0;
    bottom: 0px;
    height: 67px;
    background-color: #f7f7f7;
    z-index: 100;
    box-shadow: 0 5px 40px rgba(0,0,0,.4);
    transition: opacity .1s ease-out,bottom .1s ease-out .5s;
    display: flex;
  }

  .sr-deal_actions--visible{
    opacity: 1;
    bottom: 0px;
  }
  .sr-deal_actions__options{
    top: 0;
    left: 0;
    bottom: 0;
    width: 100%;
    padding: 10px 10px 10px;
    display: inline-block;
  }

  .sr-deal_actions__options__item{
    position: relative;
    display: inline-block;
    text-align: center;
    height: 100%;
    width: 100%;
    margin-left: 1%;
    overflow: hidden;
    vertical-align: top;
    font: 400 20px/28px Open Sans,sans-serif;
    color: rgba(0,0,0,.5);
    background: #e5e5e5;
    border-radius: 4px;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-clip: padding-box;
  }

  .sr-deal_actions__options__item--header{
    position: relative;
    padding: 0 45px;
    line-height: 45px;
    white-space: nowrap;
    text-overflow: ellipsis;
    color:#fff;
  }
  .is-invalid{border: 1px solid red;}
  @media only screen and (min-width: 1025px){
    .page-wrapper {
      padding: 30px 30px 30px 300px;
    }
  }
</style>

<div class="page-wrapper d-block clearfix " >
  <div style="padding:0; margin:0; margin-bottom:50px;">
    <form id="quick_quote_form">
     <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><img src="<?php echo site_url('assets/images/solar_quote_form_header.jpg'); ?>" alt="" width="910" height="126" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
          <tbody>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                  <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Quote #</td>
                      <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                        <input required="" id="quote_no" name="quote[quote_no]" value="<?php echo $quote_no; ?>" style="padding:5px; width:inherit; height: inherit;" type="text" />
                      </td>
                    </tr>
                  </tbody></table></td>
                  <td width="415" valign="top" style="padding-left:10px">
                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr>
                        <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Sales Person:</td>
                        <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="rep_name" name="quote[rep_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                      </tr>
                    </tbody></table></td>
                  </tr>
                </tbody></table></td>
              </tr>
              <tr>
                <td height="13"></td>
              </tr>
              <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                  <tbody><tr>
                    <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr>
                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Site Contact:</td>
                        <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_name" name="quote[customer_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                      </tr>
                    </tbody></table></td>
                    <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr>
                        <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Valid for:</td>
                        <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="valid_for" name="quote[valid_for]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                      </tr>
                    </tbody></table></td>
                  </tr>
                </tbody></table></td>
              </tr>
              <tr>
                <td height="13"></td>
              </tr>
              <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                  <tbody><tr>
                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Address:</td>
                    <td width="708" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input required="" id="customer_address" name="quote[customer_address]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                  </tr>
                </tbody></table></td>
              </tr>
              <tr>
                <td height="13"></td>
              </tr>
              
              <tr>
                <td height="30"></td>
              </tr>
              <tr>
                <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                  Description
                </td>
              </tr>
            </tbody></table></td>
          </tr>
          <td valign="top"></td> </tr>

          <tr>
            <td valign="top">
              <table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <textarea rows="20" style="width:828px; padding:10px;" name="quote[description]" id="description"></textarea>
              </table>
            </td>
          </tr>
          <tr><td height="10">&nbsp;</td></tr>
          <tr>
            <td valign="top">
            <table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="470" height="40" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">&nbsp</td>
                <td width="150" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>Sub Total exc GST:</strong></td>
                <td width="153" style="border-top:none; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                  <input style="padding:5px; height: 40px;" id="sub_total_excGst" name="quote[total_excGst]" type="number" required="">
                </td>
              </tr>
              <tr>
                <td align="left" height="40" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">&nbsp;</td>
                <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>GST:</strong></td>
                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                  <input style="padding:5px; height: 40px;" id="total_gst"  name="quote[total_gst]" type="number" required="">
                </td>
              </tr>
              <tr>
                <td align="left" height="40" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">&nbsp;</td>
                <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>Total inc GST:</strong></td>
                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                  <input style="padding:5px; height: 40px;" id="total_incGst" name="quote[total_incGst]" type="number" required="">
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
                <img src="<?php echo $this->config->item('live_url').'assets/images/solar_quote_form_pay.jpg'; ?>" width="250" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
          </tr>
        <tr>
            <td>
                <img src="<?php echo $this->config->item('live_url').'assets/images/solar_quote_form_footer.jpg'; ?>"  />
            </td>
        </tr>
        </table>
      </form>
    </div>
    <div  class="sr-deal_actions">
      <div class="col-md-6  sr-deal_actions__options">
        <a href="javascript:void(0);" id="generate_quote_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save and  Generate Pdf</a>
      </div>
      <div class=" col-md-6  sr-deal_actions__options">
        <a href="javascript:void(0);" id="close_quote" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-close"></i> Close</a>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo site_url(); ?>common/js/solar_quick_quote.js?v=<?php echo version; ?>"></script>
  <script>
    var context = {};
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.quote_data = '<?php echo (isset($quote_data) && !empty($quote_data)) ? json_encode($quote_data, JSON_HEX_APOS) : ''; ?>';
    context.quote_items = '<?php echo (isset($quote_items) && !empty($quote_items)) ? json_encode($quote_items, JSON_HEX_APOS) : ''; ?>';
    context.gst = '<?php echo GST ?>';
    var quote_manager = new quick_quote_manager(context);
  </script>
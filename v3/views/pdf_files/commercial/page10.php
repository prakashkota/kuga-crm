<style>
    #containerdd2 {
        height: 400px;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 310px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">

    <tr>
        <td colspan="2" style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/header_img_v3.PNG" alt="" style="display:inline-block" /></td>
    </tr>

    <tr>
        <td colspan="2" style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle;">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/heading_v3_18.PNG" alt="" style="display:inline-block" />
        </td>
    </tr>

    <tr>
        <td style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead1.jpg" alt="" style="display:inline-block" />
        </td>
        <td style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead2.jpg" alt="" style="display:inline-block" />
        </td>
    </tr>
    <tr>
        <td>
            <div id="typical_export_graph"></div>
        </td>
        <td>
            <div id="worst_export_graph"></div>
        </td>
    </tr>
    <tr>
        <td style="padding:80px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead3.jpg" alt="" style="display:inline-block" />
        </td>
        <td style="padding:80px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead4.jpg" alt="" style="display:inline-block" />
        </td>
    </tr>
    <tr>
        <td>
            <div id="high_export_graph"></div>
        </td>
        <td>
            <div id="best_export_graph"></div>
        </td>
    </tr>

    <tr>
        <td colspan="2" style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/footer_img_v3.PNG" alt="" style="display:inline-block" /></td>
    </tr>
</table>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    var graph_data = <?php echo $saving_meter_calculation; ?>;
    var daily_average_production = <?php echo $saving_graph_calculation; ?>;

    $(document).ready(function() {
        console.log(<?php echo $time_of_use; ?>);
        // console.log(graph_data);
        // console.log(daily_average_production);

        show_production(daily_average_production.typical_graph);
        show_production1(daily_average_production.worst_graph);
        show_production2(daily_average_production.high_graph);
        show_production3(daily_average_production.best_graph);
    })




    function show_production(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var typical_export_load_graph_data = d.typical_export_load_graph_data;
        var typical_export_sol_prd_graph_data = d.typical_export_sol_prd_graph_data;
        var typical_export_export_graph_data = d.typical_export_export_graph_data;
        var typical_export_offset_graph_data = d.typical_export_offset_graph_data;

        Highcharts.chart('typical_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },

                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,
                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    console.log(this);
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 3600,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 3,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    // pointStart: Date.UTC(2018, 11, 1, 0, 0, 0),
                    // pointInterval: 3780 * 1000
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        typical_export_load_graph_data[0],
                        typical_export_load_graph_data[1],
                        typical_export_load_graph_data[2],
                        typical_export_load_graph_data[3],
                        typical_export_load_graph_data[4],
                        typical_export_load_graph_data[5],
                        typical_export_load_graph_data[6],
                        typical_export_load_graph_data[7],
                        typical_export_load_graph_data[8],
                        typical_export_load_graph_data[9],
                        typical_export_load_graph_data[10],
                        typical_export_load_graph_data[11],
                        typical_export_load_graph_data[12],
                        typical_export_load_graph_data[13],
                        typical_export_load_graph_data[14],
                        typical_export_load_graph_data[15],
                        typical_export_load_graph_data[16],
                        typical_export_load_graph_data[17],
                        typical_export_load_graph_data[18],
                        typical_export_load_graph_data[19],
                        typical_export_load_graph_data[20],
                        typical_export_load_graph_data[21],
                        typical_export_load_graph_data[22],
                        typical_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                },
                {
                    name: 'Solar Output',
                    data: [
                        typical_export_sol_prd_graph_data[0],
                        typical_export_sol_prd_graph_data[1],
                        typical_export_sol_prd_graph_data[2],
                        typical_export_sol_prd_graph_data[3],
                        typical_export_sol_prd_graph_data[4],
                        typical_export_sol_prd_graph_data[5],
                        typical_export_sol_prd_graph_data[6],
                        typical_export_sol_prd_graph_data[7],
                        typical_export_sol_prd_graph_data[8],
                        typical_export_sol_prd_graph_data[9],
                        typical_export_sol_prd_graph_data[10],
                        typical_export_sol_prd_graph_data[11],
                        typical_export_sol_prd_graph_data[12],
                        typical_export_sol_prd_graph_data[13],
                        typical_export_sol_prd_graph_data[14],
                        typical_export_sol_prd_graph_data[15],
                        typical_export_sol_prd_graph_data[16],
                        typical_export_sol_prd_graph_data[17],
                        typical_export_sol_prd_graph_data[18],
                        typical_export_sol_prd_graph_data[19],
                        typical_export_sol_prd_graph_data[20],
                        typical_export_sol_prd_graph_data[21],
                        typical_export_sol_prd_graph_data[22],
                        typical_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        typical_export_export_graph_data[0],
                        typical_export_export_graph_data[1],
                        typical_export_export_graph_data[2],
                        typical_export_export_graph_data[3],
                        typical_export_export_graph_data[4],
                        typical_export_export_graph_data[5],
                        typical_export_export_graph_data[6],
                        typical_export_export_graph_data[7],
                        typical_export_export_graph_data[8],
                        typical_export_export_graph_data[9],
                        typical_export_export_graph_data[10],
                        typical_export_export_graph_data[11],
                        typical_export_export_graph_data[12],
                        typical_export_export_graph_data[13],
                        typical_export_export_graph_data[14],
                        typical_export_export_graph_data[15],
                        typical_export_export_graph_data[16],
                        typical_export_export_graph_data[17],
                        typical_export_export_graph_data[18],
                        typical_export_export_graph_data[19],
                        typical_export_export_graph_data[20],
                        typical_export_export_graph_data[21],
                        typical_export_export_graph_data[22],
                        typical_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        typical_export_offset_graph_data[0],
                        typical_export_offset_graph_data[1],
                        typical_export_offset_graph_data[2],
                        typical_export_offset_graph_data[3],
                        typical_export_offset_graph_data[4],
                        typical_export_offset_graph_data[5],
                        typical_export_offset_graph_data[6],
                        typical_export_offset_graph_data[7],
                        typical_export_offset_graph_data[8],
                        typical_export_offset_graph_data[9],
                        typical_export_offset_graph_data[10],
                        typical_export_offset_graph_data[11],
                        typical_export_offset_graph_data[12],
                        typical_export_offset_graph_data[13],
                        typical_export_offset_graph_data[14],
                        typical_export_offset_graph_data[15],
                        typical_export_offset_graph_data[16],
                        typical_export_offset_graph_data[17],
                        typical_export_offset_graph_data[18],
                        typical_export_offset_graph_data[19],
                        typical_export_offset_graph_data[20],
                        typical_export_offset_graph_data[21],
                        typical_export_offset_graph_data[22],
                        typical_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });
    }

    function show_production1(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var worst_export_load_graph_data = d.worst_export_load_graph_data;
        var worst_export_sol_prd_graph_data = d.worst_export_sol_prd_graph_data;
        var worst_export_export_graph_data = d.worst_export_export_graph_data;
        var worst_export_offset_graph_data = d.worst_export_offset_graph_data;

        Highcharts.chart('worst_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {

                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    console.log(this);
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        worst_export_load_graph_data[0],
                        worst_export_load_graph_data[1],
                        worst_export_load_graph_data[2],
                        worst_export_load_graph_data[3],
                        worst_export_load_graph_data[4],
                        worst_export_load_graph_data[5],
                        worst_export_load_graph_data[6],
                        worst_export_load_graph_data[7],
                        worst_export_load_graph_data[8],
                        worst_export_load_graph_data[9],
                        worst_export_load_graph_data[10],
                        worst_export_load_graph_data[11],
                        worst_export_load_graph_data[12],
                        worst_export_load_graph_data[13],
                        worst_export_load_graph_data[14],
                        worst_export_load_graph_data[15],
                        worst_export_load_graph_data[16],
                        worst_export_load_graph_data[17],
                        worst_export_load_graph_data[18],
                        worst_export_load_graph_data[19],
                        worst_export_load_graph_data[20],
                        worst_export_load_graph_data[21],
                        worst_export_load_graph_data[22],
                        worst_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        worst_export_sol_prd_graph_data[0],
                        worst_export_sol_prd_graph_data[1],
                        worst_export_sol_prd_graph_data[2],
                        worst_export_sol_prd_graph_data[3],
                        worst_export_sol_prd_graph_data[4],
                        worst_export_sol_prd_graph_data[5],
                        worst_export_sol_prd_graph_data[6],
                        worst_export_sol_prd_graph_data[7],
                        worst_export_sol_prd_graph_data[8],
                        worst_export_sol_prd_graph_data[9],
                        worst_export_sol_prd_graph_data[10],
                        worst_export_sol_prd_graph_data[11],
                        worst_export_sol_prd_graph_data[12],
                        worst_export_sol_prd_graph_data[13],
                        worst_export_sol_prd_graph_data[14],
                        worst_export_sol_prd_graph_data[15],
                        worst_export_sol_prd_graph_data[16],
                        worst_export_sol_prd_graph_data[17],
                        worst_export_sol_prd_graph_data[18],
                        worst_export_sol_prd_graph_data[19],
                        worst_export_sol_prd_graph_data[20],
                        worst_export_sol_prd_graph_data[21],
                        worst_export_sol_prd_graph_data[22],
                        worst_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        worst_export_export_graph_data[0],
                        worst_export_export_graph_data[1],
                        worst_export_export_graph_data[2],
                        worst_export_export_graph_data[3],
                        worst_export_export_graph_data[4],
                        worst_export_export_graph_data[5],
                        worst_export_export_graph_data[6],
                        worst_export_export_graph_data[7],
                        worst_export_export_graph_data[8],
                        worst_export_export_graph_data[9],
                        worst_export_export_graph_data[10],
                        worst_export_export_graph_data[11],
                        worst_export_export_graph_data[12],
                        worst_export_export_graph_data[13],
                        worst_export_export_graph_data[14],
                        worst_export_export_graph_data[15],
                        worst_export_export_graph_data[16],
                        worst_export_export_graph_data[17],
                        worst_export_export_graph_data[18],
                        worst_export_export_graph_data[19],
                        worst_export_export_graph_data[20],
                        worst_export_export_graph_data[21],
                        worst_export_export_graph_data[22],
                        worst_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        worst_export_offset_graph_data[0],
                        worst_export_offset_graph_data[1],
                        worst_export_offset_graph_data[2],
                        worst_export_offset_graph_data[3],
                        worst_export_offset_graph_data[4],
                        worst_export_offset_graph_data[5],
                        worst_export_offset_graph_data[6],
                        worst_export_offset_graph_data[7],
                        worst_export_offset_graph_data[8],
                        worst_export_offset_graph_data[9],
                        worst_export_offset_graph_data[10],
                        worst_export_offset_graph_data[11],
                        worst_export_offset_graph_data[12],
                        worst_export_offset_graph_data[13],
                        worst_export_offset_graph_data[14],
                        worst_export_offset_graph_data[15],
                        worst_export_offset_graph_data[16],
                        worst_export_offset_graph_data[17],
                        worst_export_offset_graph_data[18],
                        worst_export_offset_graph_data[19],
                        worst_export_offset_graph_data[20],
                        worst_export_offset_graph_data[21],
                        worst_export_offset_graph_data[22],
                        worst_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });

    }


    function show_production2(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var high_export_load_graph_data = d.high_export_load_graph_data;
        var high_export_sol_prd_graph_data = d.high_export_sol_prd_graph_data;
        var high_export_export_graph_data = d.high_export_export_graph_data;
        var high_export_offset_graph_data = d.high_export_offset_graph_data;


        Highcharts.chart('high_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    console.log(this);
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        high_export_load_graph_data[0],
                        high_export_load_graph_data[1],
                        high_export_load_graph_data[2],
                        high_export_load_graph_data[3],
                        high_export_load_graph_data[4],
                        high_export_load_graph_data[5],
                        high_export_load_graph_data[6],
                        high_export_load_graph_data[7],
                        high_export_load_graph_data[8],
                        high_export_load_graph_data[9],
                        high_export_load_graph_data[10],
                        high_export_load_graph_data[11],
                        high_export_load_graph_data[12],
                        high_export_load_graph_data[13],
                        high_export_load_graph_data[14],
                        high_export_load_graph_data[15],
                        high_export_load_graph_data[16],
                        high_export_load_graph_data[17],
                        high_export_load_graph_data[18],
                        high_export_load_graph_data[19],
                        high_export_load_graph_data[20],
                        high_export_load_graph_data[21],
                        high_export_load_graph_data[22],
                        high_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        high_export_sol_prd_graph_data[0],
                        high_export_sol_prd_graph_data[1],
                        high_export_sol_prd_graph_data[2],
                        high_export_sol_prd_graph_data[3],
                        high_export_sol_prd_graph_data[4],
                        high_export_sol_prd_graph_data[5],
                        high_export_sol_prd_graph_data[6],
                        high_export_sol_prd_graph_data[7],
                        high_export_sol_prd_graph_data[8],
                        high_export_sol_prd_graph_data[9],
                        high_export_sol_prd_graph_data[10],
                        high_export_sol_prd_graph_data[11],
                        high_export_sol_prd_graph_data[12],
                        high_export_sol_prd_graph_data[13],
                        high_export_sol_prd_graph_data[14],
                        high_export_sol_prd_graph_data[15],
                        high_export_sol_prd_graph_data[16],
                        high_export_sol_prd_graph_data[17],
                        high_export_sol_prd_graph_data[18],
                        high_export_sol_prd_graph_data[19],
                        high_export_sol_prd_graph_data[20],
                        high_export_sol_prd_graph_data[21],
                        high_export_sol_prd_graph_data[22],
                        high_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        high_export_export_graph_data[0],
                        high_export_export_graph_data[1],
                        high_export_export_graph_data[2],
                        high_export_export_graph_data[3],
                        high_export_export_graph_data[4],
                        high_export_export_graph_data[5],
                        high_export_export_graph_data[6],
                        high_export_export_graph_data[7],
                        high_export_export_graph_data[8],
                        high_export_export_graph_data[9],
                        high_export_export_graph_data[10],
                        high_export_export_graph_data[11],
                        high_export_export_graph_data[12],
                        high_export_export_graph_data[13],
                        high_export_export_graph_data[14],
                        high_export_export_graph_data[15],
                        high_export_export_graph_data[16],
                        high_export_export_graph_data[17],
                        high_export_export_graph_data[18],
                        high_export_export_graph_data[19],
                        high_export_export_graph_data[20],
                        high_export_export_graph_data[21],
                        high_export_export_graph_data[22],
                        high_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        high_export_offset_graph_data[0],
                        high_export_offset_graph_data[1],
                        high_export_offset_graph_data[2],
                        high_export_offset_graph_data[3],
                        high_export_offset_graph_data[4],
                        high_export_offset_graph_data[5],
                        high_export_offset_graph_data[6],
                        high_export_offset_graph_data[7],
                        high_export_offset_graph_data[8],
                        high_export_offset_graph_data[9],
                        high_export_offset_graph_data[10],
                        high_export_offset_graph_data[11],
                        high_export_offset_graph_data[12],
                        high_export_offset_graph_data[13],
                        high_export_offset_graph_data[14],
                        high_export_offset_graph_data[15],
                        high_export_offset_graph_data[16],
                        high_export_offset_graph_data[17],
                        high_export_offset_graph_data[18],
                        high_export_offset_graph_data[19],
                        high_export_offset_graph_data[20],
                        high_export_offset_graph_data[21],
                        high_export_offset_graph_data[22],
                        high_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });


    }


    function show_production3(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var best_export_load_graph_data = d.best_export_load_graph_data;
        var best_export_sol_prd_graph_data = d.best_export_sol_prd_graph_data;
        var best_export_export_graph_data = d.best_export_export_graph_data;
        var best_export_offset_graph_data = d.best_export_offset_graph_data;

        Highcharts.chart('best_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    console.log(this);
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        best_export_load_graph_data[0],
                        best_export_load_graph_data[1],
                        best_export_load_graph_data[2],
                        best_export_load_graph_data[3],
                        best_export_load_graph_data[4],
                        best_export_load_graph_data[5],
                        best_export_load_graph_data[6],
                        best_export_load_graph_data[7],
                        best_export_load_graph_data[8],
                        best_export_load_graph_data[9],
                        best_export_load_graph_data[10],
                        best_export_load_graph_data[11],
                        best_export_load_graph_data[12],
                        best_export_load_graph_data[13],
                        best_export_load_graph_data[14],
                        best_export_load_graph_data[15],
                        best_export_load_graph_data[16],
                        best_export_load_graph_data[17],
                        best_export_load_graph_data[18],
                        best_export_load_graph_data[19],
                        best_export_load_graph_data[20],
                        best_export_load_graph_data[21],
                        best_export_load_graph_data[22],
                        best_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        best_export_sol_prd_graph_data[0],
                        best_export_sol_prd_graph_data[1],
                        best_export_sol_prd_graph_data[2],
                        best_export_sol_prd_graph_data[3],
                        best_export_sol_prd_graph_data[4],
                        best_export_sol_prd_graph_data[5],
                        best_export_sol_prd_graph_data[6],
                        best_export_sol_prd_graph_data[7],
                        best_export_sol_prd_graph_data[8],
                        best_export_sol_prd_graph_data[9],
                        best_export_sol_prd_graph_data[10],
                        best_export_sol_prd_graph_data[11],
                        best_export_sol_prd_graph_data[12],
                        best_export_sol_prd_graph_data[13],
                        best_export_sol_prd_graph_data[14],
                        best_export_sol_prd_graph_data[15],
                        best_export_sol_prd_graph_data[16],
                        best_export_sol_prd_graph_data[17],
                        best_export_sol_prd_graph_data[18],
                        best_export_sol_prd_graph_data[19],
                        best_export_sol_prd_graph_data[20],
                        best_export_sol_prd_graph_data[21],
                        best_export_sol_prd_graph_data[22],
                        best_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        best_export_export_graph_data[0],
                        best_export_export_graph_data[1],
                        best_export_export_graph_data[2],
                        best_export_export_graph_data[3],
                        best_export_export_graph_data[4],
                        best_export_export_graph_data[5],
                        best_export_export_graph_data[6],
                        best_export_export_graph_data[7],
                        best_export_export_graph_data[8],
                        best_export_export_graph_data[9],
                        best_export_export_graph_data[10],
                        best_export_export_graph_data[11],
                        best_export_export_graph_data[12],
                        best_export_export_graph_data[13],
                        best_export_export_graph_data[14],
                        best_export_export_graph_data[15],
                        best_export_export_graph_data[16],
                        best_export_export_graph_data[17],
                        best_export_export_graph_data[18],
                        best_export_export_graph_data[19],
                        best_export_export_graph_data[20],
                        best_export_export_graph_data[21],
                        best_export_export_graph_data[22],
                        best_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        best_export_offset_graph_data[0],
                        best_export_offset_graph_data[1],
                        best_export_offset_graph_data[2],
                        best_export_offset_graph_data[3],
                        best_export_offset_graph_data[4],
                        best_export_offset_graph_data[5],
                        best_export_offset_graph_data[6],
                        best_export_offset_graph_data[7],
                        best_export_offset_graph_data[8],
                        best_export_offset_graph_data[9],
                        best_export_offset_graph_data[10],
                        best_export_offset_graph_data[11],
                        best_export_offset_graph_data[12],
                        best_export_offset_graph_data[13],
                        best_export_offset_graph_data[14],
                        best_export_offset_graph_data[15],
                        best_export_offset_graph_data[16],
                        best_export_offset_graph_data[17],
                        best_export_offset_graph_data[18],
                        best_export_offset_graph_data[19],
                        best_export_offset_graph_data[20],
                        best_export_offset_graph_data[21],
                        best_export_offset_graph_data[22],
                        best_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });

    }

    // function show_graph1(avg_load_graph_data) {

    //     // var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
    //     // var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
    //     // console.log(avg_load_graph_data)
    //     var labels = {};

    //     Highcharts.chart('avg_load_area_chart1', {

    //         chart: {

    //             width: 420,

    //             height: 260,

    //         },

    //         credits: {

    //             enabled: false

    //         },

    //         navigation: {

    //             buttonOptions: {

    //                 enabled: false

    //             }

    //         },

    //         title: {

    //             style: {

    //                 display: 'none'

    //             }

    //         },

    //         xAxis: {

    //             title: {

    //                 text: 'Hours',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             type: 'datetime',

    //             //step: 24,

    //             tickInterval: 3780 * 1000,



    //             //minTickInterval: 24,

    //             labels: {

    //                 formatter: function() {

    //                     return Highcharts.dateFormat('%H:%M:%S', this.value);



    //                 },

    //             },

    //         },

    //         yAxis: {

    //             allowDecimals: true,

    //             title: {

    //                 text: 'POWER',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             labels: {

    //                 formatter: function() {

    //                     return this.value + 'kW';

    //                 }

    //             }

    //         },

    //         legend: {

    //             align: 'center',

    //             verticalAlign: 'bottom',

    //             x: 0,

    //             y: 0,

    //             itemStyle: {

    //                 "fontSize": "9px",

    //                 "color": '#666',

    //                 "fontWeight": 'normal'

    //             }

    //         },

    //         plotOptions: {

    //             area: {

    //                 pointStart: 1940,

    //                 marker: {

    //                     enabled: false,

    //                     symbol: 'circle',

    //                     radius: 3,

    //                     states: {

    //                         hover: {

    //                             enabled: true

    //                         }

    //                     }

    //                 }

    //             },

    //             series: {

    //                 pointStart: Date.UTC(2016, 0, 17),

    //                 pointInterval: 3780 * 1000

    //             }

    //         },

    //         series: [

    //             {

    //                 name: 'Jan',

    //                 data: [

    //                     avg_load_graph_data['jan'][0],

    //                     avg_load_graph_data['jan'][1],

    //                     avg_load_graph_data['jan'][2],

    //                     avg_load_graph_data['jan'][3],

    //                     avg_load_graph_data['jan'][4],

    //                     avg_load_graph_data['jan'][5],

    //                     avg_load_graph_data['jan'][6],

    //                     avg_load_graph_data['jan'][7],

    //                     avg_load_graph_data['jan'][8],

    //                     avg_load_graph_data['jan'][9],

    //                     avg_load_graph_data['jan'][10],

    //                     avg_load_graph_data['jan'][11],

    //                     avg_load_graph_data['jan'][12],

    //                     avg_load_graph_data['jan'][13],

    //                     avg_load_graph_data['jan'][14],

    //                     avg_load_graph_data['jan'][15],

    //                     avg_load_graph_data['jan'][16],

    //                     avg_load_graph_data['jan'][17],

    //                     avg_load_graph_data['jan'][18],

    //                     avg_load_graph_data['jan'][19],

    //                     avg_load_graph_data['jan'][20],

    //                     avg_load_graph_data['jan'][21],

    //                     avg_load_graph_data['jan'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Feb',

    //                 data: [

    //                     avg_load_graph_data['feb'][0],

    //                     avg_load_graph_data['feb'][1],

    //                     avg_load_graph_data['feb'][2],

    //                     avg_load_graph_data['feb'][3],

    //                     avg_load_graph_data['feb'][4],

    //                     avg_load_graph_data['feb'][5],

    //                     avg_load_graph_data['feb'][6],

    //                     avg_load_graph_data['feb'][7],

    //                     avg_load_graph_data['feb'][8],

    //                     avg_load_graph_data['feb'][9],

    //                     avg_load_graph_data['feb'][10],

    //                     avg_load_graph_data['feb'][11],

    //                     avg_load_graph_data['feb'][12],

    //                     avg_load_graph_data['feb'][13],

    //                     avg_load_graph_data['feb'][14],

    //                     avg_load_graph_data['feb'][15],

    //                     avg_load_graph_data['feb'][16],

    //                     avg_load_graph_data['feb'][17],

    //                     avg_load_graph_data['feb'][18],

    //                     avg_load_graph_data['feb'][19],

    //                     avg_load_graph_data['feb'][20],

    //                     avg_load_graph_data['feb'][21],

    //                     avg_load_graph_data['feb'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Mar',

    //                 data: [

    //                     avg_load_graph_data['mar'][0],

    //                     avg_load_graph_data['mar'][1],

    //                     avg_load_graph_data['mar'][2],

    //                     avg_load_graph_data['mar'][3],

    //                     avg_load_graph_data['mar'][4],

    //                     avg_load_graph_data['mar'][5],

    //                     avg_load_graph_data['mar'][6],

    //                     avg_load_graph_data['mar'][7],

    //                     avg_load_graph_data['mar'][8],

    //                     avg_load_graph_data['mar'][9],

    //                     avg_load_graph_data['mar'][10],

    //                     avg_load_graph_data['mar'][11],

    //                     avg_load_graph_data['mar'][12],

    //                     avg_load_graph_data['mar'][13],

    //                     avg_load_graph_data['mar'][14],

    //                     avg_load_graph_data['mar'][15],

    //                     avg_load_graph_data['mar'][16],

    //                     avg_load_graph_data['mar'][17],

    //                     avg_load_graph_data['mar'][18],

    //                     avg_load_graph_data['mar'][19],

    //                     avg_load_graph_data['mar'][20],

    //                     avg_load_graph_data['mar'][21],

    //                     avg_load_graph_data['mar'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },
    //         ]

    //     });
    // }

    // function show_graph2(avg_load_graph_data) {

    //     // var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
    //     // var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
    //     // console.log(avg_load_graph_data)
    //     var labels = {};

    //     Highcharts.chart('avg_load_area_chart2', {

    //         chart: {

    //             width: 420,

    //             height: 260,

    //         },

    //         credits: {

    //             enabled: false

    //         },

    //         navigation: {

    //             buttonOptions: {

    //                 enabled: false

    //             }

    //         },

    //         title: {

    //             style: {

    //                 display: 'none'

    //             }

    //         },

    //         xAxis: {

    //             title: {

    //                 text: 'Hours',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             type: 'datetime',

    //             //step: 24,

    //             tickInterval: 3780 * 1000,



    //             //minTickInterval: 24,

    //             labels: {

    //                 formatter: function() {

    //                     return Highcharts.dateFormat('%H:%M:%S', this.value);



    //                 },

    //             },

    //         },

    //         yAxis: {

    //             allowDecimals: true,

    //             title: {

    //                 text: 'POWER',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             labels: {

    //                 formatter: function() {

    //                     return this.value + 'kW';

    //                 }

    //             }

    //         },

    //         legend: {

    //             align: 'center',

    //             verticalAlign: 'bottom',

    //             x: 0,

    //             y: 0,

    //             itemStyle: {

    //                 "fontSize": "9px",

    //                 "color": '#666',

    //                 "fontWeight": 'normal'

    //             }

    //         },

    //         plotOptions: {

    //             area: {

    //                 pointStart: 1940,

    //                 marker: {

    //                     enabled: false,

    //                     symbol: 'circle',

    //                     radius: 3,

    //                     states: {

    //                         hover: {

    //                             enabled: true

    //                         }

    //                     }

    //                 }

    //             },

    //             series: {

    //                 pointStart: Date.UTC(2016, 0, 17),

    //                 pointInterval: 3780 * 1000

    //             }

    //         },

    //         series: [

    //             {

    //                 name: 'Apr',

    //                 data: [

    //                     avg_load_graph_data['apr'][0],

    //                     avg_load_graph_data['apr'][1],

    //                     avg_load_graph_data['apr'][2],

    //                     avg_load_graph_data['apr'][3],

    //                     avg_load_graph_data['apr'][4],

    //                     avg_load_graph_data['apr'][5],

    //                     avg_load_graph_data['apr'][6],

    //                     avg_load_graph_data['apr'][7],

    //                     avg_load_graph_data['apr'][8],

    //                     avg_load_graph_data['apr'][9],

    //                     avg_load_graph_data['apr'][10],

    //                     avg_load_graph_data['apr'][11],

    //                     avg_load_graph_data['apr'][12],

    //                     avg_load_graph_data['apr'][13],

    //                     avg_load_graph_data['apr'][14],

    //                     avg_load_graph_data['apr'][15],

    //                     avg_load_graph_data['apr'][16],

    //                     avg_load_graph_data['apr'][17],

    //                     avg_load_graph_data['apr'][18],

    //                     avg_load_graph_data['apr'][19],

    //                     avg_load_graph_data['apr'][20],

    //                     avg_load_graph_data['apr'][21],

    //                     avg_load_graph_data['apr'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'May',

    //                 data: [

    //                     avg_load_graph_data['may'][0],

    //                     avg_load_graph_data['may'][1],

    //                     avg_load_graph_data['may'][2],

    //                     avg_load_graph_data['may'][3],

    //                     avg_load_graph_data['may'][4],

    //                     avg_load_graph_data['may'][5],

    //                     avg_load_graph_data['may'][6],

    //                     avg_load_graph_data['may'][7],

    //                     avg_load_graph_data['may'][8],

    //                     avg_load_graph_data['may'][9],

    //                     avg_load_graph_data['may'][10],

    //                     avg_load_graph_data['may'][11],

    //                     avg_load_graph_data['may'][12],

    //                     avg_load_graph_data['may'][13],

    //                     avg_load_graph_data['may'][14],

    //                     avg_load_graph_data['may'][15],

    //                     avg_load_graph_data['may'][16],

    //                     avg_load_graph_data['may'][17],

    //                     avg_load_graph_data['may'][18],

    //                     avg_load_graph_data['may'][19],

    //                     avg_load_graph_data['may'][20],

    //                     avg_load_graph_data['may'][21],

    //                     avg_load_graph_data['may'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Jun',

    //                 data: [

    //                     avg_load_graph_data['jun'][0],

    //                     avg_load_graph_data['jun'][1],

    //                     avg_load_graph_data['jun'][2],

    //                     avg_load_graph_data['jun'][3],

    //                     avg_load_graph_data['jun'][4],

    //                     avg_load_graph_data['jun'][5],

    //                     avg_load_graph_data['jun'][6],

    //                     avg_load_graph_data['jun'][7],

    //                     avg_load_graph_data['jun'][8],

    //                     avg_load_graph_data['jun'][9],

    //                     avg_load_graph_data['jun'][10],

    //                     avg_load_graph_data['jun'][11],

    //                     avg_load_graph_data['jun'][12],

    //                     avg_load_graph_data['jun'][13],

    //                     avg_load_graph_data['jun'][14],

    //                     avg_load_graph_data['jun'][15],

    //                     avg_load_graph_data['jun'][16],

    //                     avg_load_graph_data['jun'][17],

    //                     avg_load_graph_data['jun'][18],

    //                     avg_load_graph_data['jun'][19],

    //                     avg_load_graph_data['jun'][20],

    //                     avg_load_graph_data['jun'][21],

    //                     avg_load_graph_data['jun'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },
    //         ]





    //     });
    // }

    // function show_graph3(avg_load_graph_data) {

    //     // var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
    //     // var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
    //     // console.log(avg_load_graph_data)
    //     var labels = {};

    //     Highcharts.chart('avg_load_area_chart3', {

    //         chart: {

    //             width: 420,

    //             height: 260,

    //         },

    //         credits: {

    //             enabled: false

    //         },

    //         navigation: {

    //             buttonOptions: {

    //                 enabled: false

    //             }

    //         },

    //         title: {

    //             style: {

    //                 display: 'none'

    //             }

    //         },

    //         xAxis: {

    //             title: {

    //                 text: 'Hours',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             type: 'datetime',

    //             //step: 24,

    //             tickInterval: 3780 * 1000,



    //             //minTickInterval: 24,

    //             labels: {

    //                 formatter: function() {

    //                     return Highcharts.dateFormat('%H:%M:%S', this.value);



    //                 },

    //             },

    //         },

    //         yAxis: {

    //             allowDecimals: true,

    //             title: {

    //                 text: 'POWER',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             labels: {

    //                 formatter: function() {

    //                     return this.value + 'kW';

    //                 }

    //             }

    //         },

    //         legend: {

    //             align: 'center',

    //             verticalAlign: 'bottom',

    //             x: 0,

    //             y: 0,

    //             itemStyle: {

    //                 "fontSize": "9px",

    //                 "color": '#666',

    //                 "fontWeight": 'normal'

    //             }

    //         },

    //         plotOptions: {

    //             area: {

    //                 pointStart: 1940,

    //                 marker: {

    //                     enabled: false,

    //                     symbol: 'circle',

    //                     radius: 3,

    //                     states: {

    //                         hover: {

    //                             enabled: true

    //                         }

    //                     }

    //                 }

    //             },

    //             series: {

    //                 pointStart: Date.UTC(2016, 0, 17),

    //                 pointInterval: 3780 * 1000

    //             }

    //         },

    //         series: [

    //             {

    //                 name: 'Jul',

    //                 data: [

    //                     avg_load_graph_data['jul'][0],

    //                     avg_load_graph_data['jul'][1],

    //                     avg_load_graph_data['jul'][2],

    //                     avg_load_graph_data['jul'][3],

    //                     avg_load_graph_data['jul'][4],

    //                     avg_load_graph_data['jul'][5],

    //                     avg_load_graph_data['jul'][6],

    //                     avg_load_graph_data['jul'][7],

    //                     avg_load_graph_data['jul'][8],

    //                     avg_load_graph_data['jul'][9],

    //                     avg_load_graph_data['jul'][10],

    //                     avg_load_graph_data['jul'][11],

    //                     avg_load_graph_data['jul'][12],

    //                     avg_load_graph_data['jul'][13],

    //                     avg_load_graph_data['jul'][14],

    //                     avg_load_graph_data['jul'][15],

    //                     avg_load_graph_data['jul'][16],

    //                     avg_load_graph_data['jul'][17],

    //                     avg_load_graph_data['jul'][18],

    //                     avg_load_graph_data['jul'][19],

    //                     avg_load_graph_data['jul'][20],

    //                     avg_load_graph_data['jul'][21],

    //                     avg_load_graph_data['jul'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Aug',

    //                 data: [

    //                     avg_load_graph_data['aug'][0],

    //                     avg_load_graph_data['aug'][1],

    //                     avg_load_graph_data['aug'][2],

    //                     avg_load_graph_data['aug'][3],

    //                     avg_load_graph_data['aug'][4],

    //                     avg_load_graph_data['aug'][5],

    //                     avg_load_graph_data['aug'][6],

    //                     avg_load_graph_data['aug'][7],

    //                     avg_load_graph_data['aug'][8],

    //                     avg_load_graph_data['aug'][9],

    //                     avg_load_graph_data['aug'][10],

    //                     avg_load_graph_data['aug'][11],

    //                     avg_load_graph_data['aug'][12],

    //                     avg_load_graph_data['aug'][13],

    //                     avg_load_graph_data['aug'][14],

    //                     avg_load_graph_data['aug'][15],

    //                     avg_load_graph_data['aug'][16],

    //                     avg_load_graph_data['aug'][17],

    //                     avg_load_graph_data['aug'][18],

    //                     avg_load_graph_data['aug'][19],

    //                     avg_load_graph_data['aug'][20],

    //                     avg_load_graph_data['aug'][21],

    //                     avg_load_graph_data['aug'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Sep',

    //                 data: [

    //                     avg_load_graph_data['sep'][0],

    //                     avg_load_graph_data['sep'][1],

    //                     avg_load_graph_data['sep'][2],

    //                     avg_load_graph_data['sep'][3],

    //                     avg_load_graph_data['sep'][4],

    //                     avg_load_graph_data['sep'][5],

    //                     avg_load_graph_data['sep'][6],

    //                     avg_load_graph_data['sep'][7],

    //                     avg_load_graph_data['sep'][8],

    //                     avg_load_graph_data['sep'][9],

    //                     avg_load_graph_data['sep'][10],

    //                     avg_load_graph_data['sep'][11],

    //                     avg_load_graph_data['sep'][12],

    //                     avg_load_graph_data['sep'][13],

    //                     avg_load_graph_data['sep'][14],

    //                     avg_load_graph_data['sep'][15],

    //                     avg_load_graph_data['sep'][16],

    //                     avg_load_graph_data['sep'][17],

    //                     avg_load_graph_data['sep'][18],

    //                     avg_load_graph_data['sep'][19],

    //                     avg_load_graph_data['sep'][20],

    //                     avg_load_graph_data['sep'][21],

    //                     avg_load_graph_data['sep'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },



    //         ]





    //     });
    // }

    // function show_graph4(avg_load_graph_data) {

    //     // var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
    //     // var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
    //     // console.log(avg_load_graph_data)
    //     var labels = {};

    //     Highcharts.chart('avg_load_area_chart4', {

    //         chart: {


    //             width: 420,

    //             height: 260,

    //         },

    //         credits: {

    //             enabled: false

    //         },

    //         navigation: {

    //             buttonOptions: {

    //                 enabled: false

    //             }

    //         },

    //         title: {

    //             style: {

    //                 display: 'none'

    //             }

    //         },

    //         xAxis: {

    //             title: {

    //                 text: 'Hours',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             type: 'datetime',

    //             //step: 24,

    //             tickInterval: 3780 * 1000,



    //             //minTickInterval: 24,

    //             labels: {

    //                 formatter: function() {

    //                     return Highcharts.dateFormat('%H:%M:%S', this.value);



    //                 },

    //             },

    //         },

    //         yAxis: {

    //             allowDecimals: true,

    //             title: {

    //                 text: 'POWER',

    //                 style: {

    //                     "fontSize": "16px",

    //                     "color": '#000',

    //                     "fontWeight": 'bold'

    //                 }

    //             },

    //             labels: {

    //                 formatter: function() {

    //                     return this.value + 'kW';

    //                 }

    //             }

    //         },

    //         legend: {

    //             align: 'center',

    //             verticalAlign: 'bottom',

    //             x: 0,

    //             y: 0,

    //             itemStyle: {

    //                 "fontSize": "9px",

    //                 "color": '#666',

    //                 "fontWeight": 'normal'

    //             }

    //         },

    //         plotOptions: {

    //             area: {

    //                 pointStart: 1940,

    //                 marker: {

    //                     enabled: false,

    //                     symbol: 'circle',

    //                     radius: 3,

    //                     states: {

    //                         hover: {

    //                             enabled: true

    //                         }

    //                     }

    //                 }

    //             },

    //             series: {

    //                 pointStart: Date.UTC(2016, 0, 17),

    //                 pointInterval: 3780 * 1000

    //             }

    //         },

    //         series: [{

    //                 name: 'Oct',

    //                 data: [

    //                     avg_load_graph_data['oct'][0],

    //                     avg_load_graph_data['oct'][1],

    //                     avg_load_graph_data['oct'][2],

    //                     avg_load_graph_data['oct'][3],

    //                     avg_load_graph_data['oct'][4],

    //                     avg_load_graph_data['oct'][5],

    //                     avg_load_graph_data['oct'][6],

    //                     avg_load_graph_data['oct'][7],

    //                     avg_load_graph_data['oct'][8],

    //                     avg_load_graph_data['oct'][9],

    //                     avg_load_graph_data['oct'][10],

    //                     avg_load_graph_data['oct'][11],

    //                     avg_load_graph_data['oct'][12],

    //                     avg_load_graph_data['oct'][13],

    //                     avg_load_graph_data['oct'][14],

    //                     avg_load_graph_data['oct'][15],

    //                     avg_load_graph_data['oct'][16],

    //                     avg_load_graph_data['oct'][17],

    //                     avg_load_graph_data['oct'][18],

    //                     avg_load_graph_data['oct'][19],

    //                     avg_load_graph_data['oct'][20],

    //                     avg_load_graph_data['oct'][21],

    //                     avg_load_graph_data['oct'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {

    //                 name: 'Nov',

    //                 data: [

    //                     avg_load_graph_data['nov'][0],

    //                     avg_load_graph_data['nov'][1],

    //                     avg_load_graph_data['nov'][2],

    //                     avg_load_graph_data['nov'][3],

    //                     avg_load_graph_data['nov'][4],

    //                     avg_load_graph_data['nov'][5],

    //                     avg_load_graph_data['nov'][6],

    //                     avg_load_graph_data['nov'][7],

    //                     avg_load_graph_data['nov'][8],

    //                     avg_load_graph_data['nov'][9],

    //                     avg_load_graph_data['nov'][10],

    //                     avg_load_graph_data['nov'][11],

    //                     avg_load_graph_data['nov'][12],

    //                     avg_load_graph_data['nov'][13],

    //                     avg_load_graph_data['nov'][14],

    //                     avg_load_graph_data['nov'][15],

    //                     avg_load_graph_data['nov'][16],

    //                     avg_load_graph_data['nov'][17],

    //                     avg_load_graph_data['nov'][18],

    //                     avg_load_graph_data['nov'][19],

    //                     avg_load_graph_data['nov'][20],

    //                     avg_load_graph_data['nov'][21],

    //                     avg_load_graph_data['nov'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //             {



    //                 name: 'Dec',

    //                 data: [

    //                     avg_load_graph_data['dec'][0],

    //                     avg_load_graph_data['dec'][1],

    //                     avg_load_graph_data['dec'][2],

    //                     avg_load_graph_data['dec'][3],

    //                     avg_load_graph_data['dec'][4],

    //                     avg_load_graph_data['dec'][5],

    //                     avg_load_graph_data['dec'][6],

    //                     avg_load_graph_data['dec'][7],

    //                     avg_load_graph_data['dec'][8],

    //                     avg_load_graph_data['dec'][9],

    //                     avg_load_graph_data['dec'][10],

    //                     avg_load_graph_data['dec'][11],

    //                     avg_load_graph_data['dec'][12],

    //                     avg_load_graph_data['dec'][13],

    //                     avg_load_graph_data['dec'][14],

    //                     avg_load_graph_data['dec'][15],

    //                     avg_load_graph_data['dec'][16],

    //                     avg_load_graph_data['dec'][17],

    //                     avg_load_graph_data['dec'][18],

    //                     avg_load_graph_data['dec'][19],

    //                     avg_load_graph_data['dec'][20],

    //                     avg_load_graph_data['dec'][21],

    //                     avg_load_graph_data['dec'][22]

    //                 ],

    //                 zIndex: 1,

    //                 fillOpacity: 0,

    //                 marker: {

    //                     lineWidth: 2,

    //                     enabled: true,

    //                     symbol: 'circle'

    //                 }

    //             },

    //         ]





    //     });
    // }

    // function show_gg() {
    //     Highcharts.chart('containerdd2', {
    //         chart: {
    //             type: 'areaspline'
    //         },
    //         title: {
    //             text: 'Average fruit consumption during one week'
    //         },
    //         legend: {
    //             layout: 'vertical',
    //             align: 'left',
    //             verticalAlign: 'top',
    //             x: 150,
    //             y: 100,
    //             floating: true,
    //             borderWidth: 1,
    //             backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
    //         },
    //         xAxis: {
    //             categories: [
    //                 'Monday',
    //                 'Tuesday',
    //                 'Wednesday',
    //                 'Thursday',
    //                 'Friday',
    //                 'Saturday',
    //                 'Sunday'
    //             ],
    //             plotBands: [{ // visualize the weekend
    //                 from: 4.5,
    //                 to: 6.5,
    //                 color: 'rgba(68, 170, 213, .2)'
    //             }]
    //         },
    //         yAxis: {
    //             title: {
    //                 text: 'Fruit units'
    //             }
    //         },
    //         tooltip: {
    //             shared: true,
    //             valueSuffix: ' units'
    //         },
    //         credits: {
    //             enabled: false
    //         },
    //         plotOptions: {
    //             areaspline: {
    //                 fillOpacity: 0.5
    //             }
    //         },
    //         series: [{
    //             name: 'John',
    //             data: [3, 4, 3, 5, 4, 10, 12]
    //         }, {
    //             name: 'Jane',
    //             data: [1, 3, 4, 3, 3, 5, 4]
    //         }]
    //     });
    // }
</script>
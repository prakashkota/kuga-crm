<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" height="1285">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="100%" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:15px"><strong>Circuit Breaker with highest rating (Need ratings in Amps)</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 270px; padding:5px 0;">
                                    <?php if ($booking_form_image->circuit_breaker != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->circuit_breaker; ?>" height="270" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="800" height="270" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height:50px;">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:15px"><strong>Switchboard legend / schematic</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 270px; padding:5px 0;">
                                    <?php if ($booking_form_image->additional_photos_1 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->additional_photos_1; ?>" height="270" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="800" height="270" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table></td>
    </tr>
   <tr>
        <td height="240">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />6</td>
                </tr>
            </table></td>
    </tr>
</table>

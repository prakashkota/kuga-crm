<html>
<body style="margin:0px;padding:0px;">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/payment.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>

    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <?php if ($type_id == 1) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_30)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 0 - 39kW</td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <?php /**
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">No Deposit</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_0; ?></strong></td>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">50% on booking in the job</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_50_before; ?></strong></td>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">50% on handover of CES</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_50_after; ?></strong></td>
                                </tr> */ ?>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">30% Deposit</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_50_before; ?></strong></td>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">70% on project commencement</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_deposit_50_after; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_30_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>

                     <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_finance)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 2 - Finance for 0 - 39kW </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">Monthly Repayments </td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px">inc GST:</td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_finance_monthly_repayments; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">Term (years)</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_finance_terms; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>

                <?php } else if ($type_id == 2) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_99)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 40kW - 100kW </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">30% Deposit</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_deposit_20; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">70% on project commencement</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_deposit_70; ?></strong></td>
                                </tr>
                                <?php /** <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">10% on handover of CES</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_deposit_10; ?></strong></td>
                                </tr> */ ?>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_99_excGST; ?></strong></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } else if ($type_id == 3) { ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_upfront_100)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 100kW+ </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">30% Deposit</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px"></td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_20; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">70% on project commencement</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_70; ?></strong></td>
                                </tr>
                               <?php /** <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">10% on handover of CES</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_deposit_10; ?></strong></td>
                                </tr> */ ?>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">exc GST:</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_upfront_100_excGST; ?></strong></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } ?>
                <?php if ($type_id != 1) { 
                    $title = ($type_id == 2) ? '40kW - 100kW' : '100kW+';
                    ?>
                    <tr>
                        <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                <tr>
                                    <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="37" align="center">
                                                    <?php if (isset($booking_form->is_finance)) { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                </td>
                                                <td width="779" style="color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:15px">Option 2 - Finance for <?php echo $title; ?> </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="540" height="35" style="font-family:Calibri, sans-serif; font-size:14px">Monthly Repayments</td>
                                    <td width="134" align="right" style="font-family:Calibri, sans-serif; font-size:14px">inc GST:</td>
                                    <td width="148" style="font-family:Calibri, sans-serif; font-size:14px"><strong>$<?php echo $booking_form->is_finance_monthly_repayments; ?></strong></td>
                                </tr>
                                <tr>
                                    <td height="35" style="font-family:Calibri, sans-serif; font-size:14px">Term (years)</td>
                                    <td align="right" style="font-family:Calibri, sans-serif; font-size:14px">&nbsp;</td>
                                    <td style="font-family:Calibri, sans-serif; font-size:14px"><strong><?php echo $booking_form->is_finance_terms; ?></strong></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30">&nbsp;</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <?php if ($type_id == 1) { ?>
    <tr>
        <td height="770">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 2) { ?>
    <tr>
        <td height="737">&nbsp;</td>
    </tr>
    <?php } else if ($type_id == 3) { ?>
    <tr>
        <td height="737">&nbsp;</td>
    </tr>
    <?php } ?>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />11</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
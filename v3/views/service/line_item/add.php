<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;} 
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Franchise - <?php } ?>Add Line Item</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Item Details</h4>
                                    <div class="form-group">
                                        <label>Item Name</label>
                                        <input type="text" name="line_item"  class="form-control" placeholder="Item Name" value="<?php echo (isset($lid_data)) ? $lid_data['line_item'] : ''; ?>"  required>
                                        <?php echo form_error('line_item', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Per Which Product</label>
                                        <select name="per_which_product"  class="form-control">
                                            <?php
                                            $per_which_product = unserialize(per_which_product);
                                            foreach ($per_which_product as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>   
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="buttwrap">
                                <input type="submit"  class="btn" value="SAVE">
                            </div>    
                        </div>
                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Pricing Details</h4>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default">
                                            <table width="100%" border="0">
                                                <thead>
                                                    <tr>
                                                        <td>State</td>
                                                        <td>Qty From</td>
                                                        <td>Qty To</td>
                                                        <td>Price</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="line_items_wrapper">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button id="add_line_items" type="button" class="btn btn-primary" >Add Line Item </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Item Additional Details</h4>
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <textarea type="text" name="comments"  class="form-control"   placeholder="Comments"> <?php echo isset($lid_data) ? $lid_data['comments'] : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


<script>
    var line_item_data = function (options) {
        var self = this;
        this.id = (options.id && options.id != '') ? options.id : '';
        this.states = (options.states && options.states != '') ? options.states : [];
        this.line_items_wrapper = 'line_items_wrapper'
        this.line_items_btn = 'add_line_items';

        if (options.id && options.id != '') {
            self.load_item_data();
        }

        $('form button').on("click", function (e) {
            e.preventDefault();
        });

        self.create_line_items();
        self.remove_line_items();
    };

    line_item_data.prototype.load_item_data = function () {
        var self = this;
        var line_item_id = self.id;
        var form_data = 'line_item_id=' + line_item_id;
        $.ajax({
            url: base_url + 'admin/service/fetch_line_item_data',
            type: 'get',
            data: form_data,
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    for (var i = 0; i < response.line_item_data.length; i++) {
                        self.create_line_items(response.line_item_data[i]);
                    }
                } else {

                }
            }
        });
    };

    line_item_data.prototype.create_line_items = function (data) {
        var self = this;
        var line_items_wrapper = document.getElementById(self.line_items_wrapper);
        var line_items_btn = document.getElementById(self.line_items_btn);
        if (!data) {
            line_items_btn.addEventListener("click", function (e) { //on add input button click
                e.preventDefault();
                var state_select_field = self.create_select_field('state', self.states, 'All');
                $(line_items_wrapper).append('<tr>'
                        + '<td>' + state_select_field + '</td>'
                        + '<td><input type="number" class="form-control" name="line_item_data[qty_from][]" value="1" /></td>'
                        + '<td><input type="number" class="form-control" name="line_item_data[qty_to][]" value="1" /></td>'
                        + '<td><input type="number" class="form-control" name="line_item_data[price][]" step="0.01" value="1" /></td>'
                        + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            });
        } else {
            var state_select_field = self.create_select_field('state', self.states, data.state);
            $(line_items_wrapper).append('<tr>'
                    + '<td>' + state_select_field + '</td>'
                    + '<td><input type="number" class="form-control" name="line_item_data[qty_from][]" value="' + data.qty_from + '" /></td>'
                    + '<td><input type="number" class="form-control" name="line_item_data[qty_to][]" value="' + data.qty_to + '" /></td>'
                    + '<td><input type="number" class="form-control" name="line_item_data[price][]" step="0.01" value="' + data.price + '" /></td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
        }
    };

    line_item_data.prototype.remove_line_items = function () {
        var self = this;
        var line_items_wrapper = document.getElementById(self.line_items_wrapper);
        $(line_items_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
            e.preventDefault();
            $(this).parent().parent('tr').remove();
        });
    };

    line_item_data.prototype.create_select_field = function (key, data, current_value) {
        data = JSON.parse(data);
        var select = '<select name="line_item_data[' + key + '][]" class="form-control ' + key + '">';
        var sel1 = (current_value && current_value == 'All') ? 'selected="selected"' : '';
        select += '<option value="All" ' + sel1 + '>All</option>';
        for (var i = 0; i < data.length; i++) {
            var value = data[i].state_postal;
            var sel = (current_value && current_value == value) ? 'selected="selected"' : '';
            select += '<option value="' + value + '" ' + sel + '>' + value + '</option>';
        }
        return select;
    };

    var context = {};
    context.id = "<?php echo (isset($lid) && $lid != '') ? $lid : '' ?>";
    context.states = '<?php echo (isset($states) && $states != '') ? json_encode($states) : '' ?>';
    var ltd = new line_item_data(context);
</script>
</body>

</html>


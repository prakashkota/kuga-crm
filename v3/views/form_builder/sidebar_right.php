<div class="sidebar-right" style="top:64px !important;">
    <div class="tax-form-cmn dis-block clearfix des"> 
        <strong>Form Elements</strong> 
    </div>
    <div class="scroll-box2 dis-block clearfix">
        <div id="scrollbox2">
            <div id="textElement" class="text-icon cmn-drag-box">Text Input 
                <span class="drag-icon">Drag</span> 
            </div>
            <div id="checkboxElement"  class="chk-icon cmn-drag-box" style="cursor: pointer;">
                Check Box<span class="drag-icon">Drag</span> 
            </div>
            <span class="checkboxspan" style="display:none;">
                <ol class="checkbox map_checkbox" >
                    <input type="text" class="form-control checkbox-label"  name="" placeholder="Label" value="checkbox-group">
                    <li class="ui-sortable-handle">
                        <input type="checkbox" class="option-selected" value="false" name="radio-group-1496836717607-option">
                        <input type="text" class="option-value" value="option-1" name="radio-group-1496836717607-option" placeholder="Value">
                    </li>

                </ol>
                <a class="addnewCheckboxElement btn" title="Add Element">+</a>
            </span>


            <div id="radioElement" class="option-icon cmn-drag-box"> Radio Option <span class="drag-icon">Drag</span></div>            
            <span class="radiospan" style="display:none;">
                <ol class="radiobox map_radiobox" >
                    <input type="text" class="form-control radio-label"  name="" placeholder="Label" value="radio-group">
                    <li class="ui-sortable-handle">
                        <input type="radio" class="option-selected" value="false" name="radio-group-1496836717607-option">
                        <input type="text" class="option-value" value="option-1" name="radio-group-1496836717607-option" placeholder="Value">
                    </li>
                    <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                        <input type="radio" class="option-selected" value="true" name="radio-group-1496836717607-option" checked="true">
                        <input type="text" class="option-value" value="option-2" name="radio-group-1496836717607-option" placeholder="Value">
                    </li>

                </ol>
                <a class="addnewElement btn" title="Remove Element">+</a>
                <!--<a class="addradioElement btn" title="Add Element">Done</a>-->
            </span>
            <div id="selectElement" class="option-icon cmn-drag-box"> Select Option <span class="drag-icon">Drag</span></div>            
            <span class="selectspan" style="display:none;">
                <ol class="selectbox map_radiobox" >
                    <input type="text" class="form-control select-label"  name="" placeholder="Label" value="select-group">
                    <li class="ui-sortable-handle">
                        <input type="radio" class="option-selected" value="false" name="radio-group-1496836717607-option">
                        <input type="text" class="option-value" value="option-1" name="radio-group-1496836717607-option" placeholder="Value">
                    </li>
                    <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                        <input type="radio" class="option-selected" value="true" name="radio-group-1496836717607-option" checked="true">
                        <input type="text" class="option-value" value="option-2" name="radio-group-1496836717607-option" placeholder="Value">
                    </li>

                </ol>
                <a class="add_new_select_option btn" title="Remove Element">+</a>
                <!--<a class="addradioElement btn" title="Add Element">Done</a>-->
            </span>
            <div id="numberElement" class="number-icon cmn-drag-box"> Number Input <span class="drag-icon">Drag</span> </div>
            <div id="emailElement" class="email-icon cmn-drag-box"> Email Address <span class="drag-icon">Drag</span> </div>
            <div class="phone-icon cmn-drag-box"> Phone Number <span class="drag-icon">Drag</span> </div>
            <div id="textareaElement" class="address-icon cmn-drag-box"> Textarea <span class="drag-icon">Drag</span> </div>
            <div id="dateElement" class="watch-icon cmn-drag-box"> Date <span class="drag-icon">Drag</span> </div>

            <div id="fileElement" class="text-icon cmn-drag-box"> File Upload <span class="drag-icon">Drag</span> </div>
            <div id="signature" class=" signature-icon cmn-drag-box "> User Signature <span class="drag-icon">Drag</span> </div>
            

        </div>
    </div>
    <div class="form-element">
        <div class="tax-form-cmn dis-block clearfix"> <strong>Attribute</strong> <a href="javascript:void(0)" class="nav-icon pull-right">Toggle Button</a> </div>
        <div class="element-form clearfix">
            <!--<div class="form-group">
                <label>Max Length</label>
                <input type="text" placeholder="Add Length" class="form-control form-max" name="max_length">
            </div> -->
            <div class="form-group">
                <label>Select Dynamic Mapping Field</label>
                <select  class="" id="mapping_fields_name">
                    <option value="" >Select Dynamic Field</option>
                    <?php
                    $mapping_fields_name = unserialize(mapping_fields_name);
                    foreach ($mapping_fields_name as $key => $value) {
                        ?>
                        <option value="<?php echo $value; ?>" ><?php echo ucwords(str_replace("_", " ", $value)); ?></option>
                    <?php } ?>
                </select>                                                   
            </div>
            
            <div class="form-group" id="chklabel">
                <label>Group Title</label>
                <input type="text" placeholder="Add group title" class="form-control form-group" name="">
            </div>
            <div class="form-group">
                <label>Label</label>
                <input type="text" placeholder="Add Label" class="form-control form-label" name="name">
            </div>
            <div class="form-group chk">
                <input type="checkbox" class="form-control form-required" id="ch1" name="name">
                <label for="ch1">Required</label>
                <div style="display:none" id="read">
                    <input type="checkbox" class="form-control form-readonly" id="ch2" name="read">
                    <label for="ch2">Read Only</label>
                    <input type="text" name="default" id="default" class="form-control default" value="" placeholder="Default Value">
                </div>
            </div>
            <button type="button" onclick="window.form_builder_tool.clone()" class="btn left_btn">Clone</button>
        </div>

        <button type="submit" class="btn save_btn left_btn">Save</button>
        <button type="submit" class="btn delete_btn right_btn">Delete</button>
    </div>  
</div>
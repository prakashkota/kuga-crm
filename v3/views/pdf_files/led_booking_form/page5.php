<?php
$terms_and_conditions = array();
if (isset($booking_form->terms_and_conditions)) {
    $terms_and_conditions = json_decode(json_encode($booking_form->terms_and_conditions),true); 
}
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="red" style="color:#c32027; font-size:16px; font-family: Calibri, sans-serif; padding-left:40px;">
            <strong>Key points of our Terms and Conditions</strong>
        </td>
    </tr>
    <tr>
        <td  style="padding-top:0px; padding-bottom:10px; color:#333333; font-family: Calibri, sans-serif; font-size:10px; padding-left:40px;">
            We'd like to highlight to you some key clauses of our Terms and Conditions, which are laid out in full to you on the following pages. 
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th  colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <?php if (isset($terms_and_conditions[0])) { ?>
                                        <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                    <?php } ?>
                                </td>
                                <td width="779" style="color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:15px">Key Point 1 - Payment</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="20" style="padding:5px; font-family: Calibri, sans-serif; font-size:10px">
                        7.1 You must pay the net price in the installment's and at the the times specified in the Agreement. If the Agreement does not specify and such instalments
                        or times, then you must pay the Net Price no later than one Business Day after installation of the Goods, unless clause 7.2 says otherwise.
                    </td> 
                </tr>

                <tr>
                    <th height="20" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <?php if (isset($terms_and_conditions[1])) { ?>
                                        <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                    <?php } ?>
                                </td>
                                <td width="779" style="color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:15px">Key Point 2 - Variation</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="20" style="padding:5px; font-family: Calibri, sans-serif; font-size:10px">
                        6.1 If, after the Agreement is entered into, we determine that due to:<br/>
                        (a) any special or unusual aspect of the Premises that we could not reasonably have been aware of during our pre-installation site inspection (for
                        example, because that would have required dismantling fixtures or fittings);<br/>
                        (b) any changes having occurred at the Premises since our pre-installation site inspection;<br/>
                        (c) any act, matter or thing you could told us concerning the Premises or the proposed installation of the Goods being revealed to be incorrect, false or misleading;<br/>
                        (d) the state of any electrical wiring at the Premises, including such electrical wiring not being in compliance with all applicable laws and standards; or <br/>
                        (e) technical issues that could not have reasonably been foreseen by us when we entered into the Agreement,<br/>
                        additional work and/or changes not set out in the Agreement are required in order to supply and install the Goods at the Premises, then we 
                        may give you a notice setting out the additional work and/or changes (Variation Notice), whether before or during the Installation of the Goods. If we are entitled
                        to give you a Variation Notice and do so, we will endeavor to give it to you soon as reasonably possible after we make a determination under this clause 6.1.
                        If you accept the Variation Notice in accordance with clause 6.2, that clause and clause 6.3 will apply. Otherwise, clause 6.4 and 6.5 will apply.  
                    </td> 
                </tr>

                <tr>
                    <th height="20" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <?php if (isset($terms_and_conditions[2])) { ?>
                                        <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                    <?php } ?>
                                </td>
                                <td width="779" style="color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:15px">Key Point 3 - Certificates</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="20" style="padding:5px; font-family: Calibri, sans-serif; font-size:10px">
                        9.3(b) replace the existing lighting on a one for one basis with LED lights(being the Goods). You acknowledge that we are required to recycle all old
                        globes and fittings in accordance to VEET and ESS regulations.
                    </td> 
                </tr>

                <tr>
                    <th height="20" colspan="3" align="left" class="black-bg" style="background:#010101;">
                        <table width="816" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="37" align="center">
                                    <?php if (isset($terms_and_conditions[3])) { ?>
                                        <input type="checkbox" class="checkbox_payment" checked="" style="margin-left:10px;" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_payment" style="margin-left:10px;" />
                                    <?php } ?>
                                </td>
                                <td width="779" style="color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:15px">Key Point 4 (NSW Only) - Certificates</td>
                            </tr>
                        </table>

                    </th>
                </tr>
                <tr>
                    <td width="816" height="20" style="padding:5px; font-family: Calibri, sans-serif; font-size:10px">
                        9.4(b) if you are required to make a minimum payment under the rules of the Scheme that applies to the supply and installation of Goods (for example,
                        if the New South Wales Scheme applies), you must make such payment to us as part of the Net Price, and that payment cannot be refunded or reimbursed.
                    </td> 
                </tr>
             
            </table></td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" >
                <tr>
                     
                    <td width="415" valign="top" style="padding-right:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="405" style="font-family: Calibri, sans-serif; font-size:10px; color:#333333">ALL OF THE ABOBE HAS BEEN EXPLAINED AND I ACCEPT THE CHARGES AS LISTED. I HAVE READ AND UNDERSTOOD THE FULL TERMS AND CONDITIONS ON THE FOOLLOWING PAGES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="23" style="font-family: Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised on behalf of:</td>
                            </tr>
                            <tr>
                                <td valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->authorised_on_behalf->company_name; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->position; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_on_behalf->date; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Client Signature:</td>
                                            <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/'. $booking_form->authorised_on_behalf->signature; ?>" />
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>

                        </table> 
                    </td>
                    <td width="415" valign="top" style="padding-left:10px">
                        <table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" style="font-family: Calibri, sans-serif; font-size:10px; color:#333333">BY SIGNING BELOW YOU ACKNOWLEGE THAT THE CUSTOMER IS AWARE OF THE PROSSIBLE EXTRA CHARGES. INCLUDING ALL ACCESS EQUIPMENT CHARGES AND RE-SCHEDULING FEES.</td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td height="20" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="116" style="font-family: Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of:</td>
                                            <td width="289" style="font-family: Calibri, sans-serif; font-size:14px; font-weight:bold; color:#333333">Kuga Electrical</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Position:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->position; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                            <td width="284" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                <strong><?php echo $booking_form->authorised_by_behalf->date; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="121" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333; font-weight:bold">Sales <br />
                                                Consultant Signature:</td>
                                            <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                                                <img width="300" height="60px" src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/'. $booking_form->authorised_by_behalf->sales_rep_signature; ?>" />
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="166">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 4 Bridge Rd, Keysborough VIC 3173</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 6 Turbo Rd, Kings Park, NSW 2148</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 31 Chetwynd St, Loganholme, QLD 4129 </td>
        </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />5</td>
                </tr>
            </table></td>
    </tr>
</table>
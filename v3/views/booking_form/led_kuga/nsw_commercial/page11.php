<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Business Frontage</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Close Up of One Fitting Showing Starter</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="business_frontage" />
                            <input type="hidden" id="business_frontage" name="booking_form_image[business_frontage]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="showing_starter" />
                            <input type="hidden" id="showing_starter" name="booking_form_image[showing_starter]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Area 1</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Area 2</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="area1" />
                            <input type="hidden" id="area1" name="booking_form_image[area1]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                        
                    </td>
                    <td align="center" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="area2" />
                            <input type="hidden" id="area2" name="booking_form_image[area2]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                        
                    </td>
                </tr>
                 <tr>
                     <td align="center" valign="top" style="padding:13px 0">
                         <select class="form-control" name="booking_form[area1_space_type]" id="area1_space_type" >
                            <option value="">Select one</option>
                            <?php
                            if (!empty($space_types)) {
                                foreach ($space_types as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['classification']; ?>" ><?php echo $value['classification']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                     </td>
                     <td align="center" valign="top" style="padding:13px 0">
                         <select class="form-control" name="booking_form[area2_space_type]" id="area2_space_type" >
                            <option value="">Select one</option>
                            <?php
                            if (!empty($space_types)) {
                                foreach ($space_types as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['classification']; ?>" ><?php echo $value['classification']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                     </td>
                 </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
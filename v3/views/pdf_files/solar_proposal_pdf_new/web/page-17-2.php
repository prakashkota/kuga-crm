<?php

// $TotalFinancialIncentives = 0;
// if (isset($financial_summary_data['lgc_pricing'])) {
//     $lgc_pricing = json_decode($financial_summary_data['lgc_pricing']);
//     $TotalFinancialIncentives = array_sum($lgc_pricing);
// }


$STCRebateValue = 0;
if (isset($financial_summary_data['degradation_data'])) {
    $stat = $financial_summary_data['degradation_data'];
}

$FinancialType = "STC";
if (isset($financial_summary_data['type'])) {
    $FinancialType = $financial_summary_data['type'];
}

$SolarEnergyRatewidth = '';
$GraphInvestment = 0;
$TProjectCost = array();
$FinanceTerm = $year;
$monthlyPaymentPlanSolar = $MonthlyPayment = number_format((float) ($proposal_finance_data['monthly_payment_plan']), 0, '', ',');
$AnnualCashflow = number_format((float) ($Cashflow), 0, '', ',');


if (isset($financial_summary_data['type']) && $financial_summary_data['type'] == 'VEEC') {
    $SolarEnergyRatewidth = 'width:40%';

    $TMonthlyPaymentPlan = (array) json_decode($financial_summary_data['monthly_payment_plan']);
    $MonthlyPayment = number_format((float) ($TMonthlyPaymentPlan[1]), 0, '', ',');

    $TProjectCost = (array) json_decode($financial_summary_data['project_cost']);
    $TProjectCost1 = number_format((float) ($TProjectCost[0]), 0, '', ',');
    $TProjectCost2 = number_format((float) ($TProjectCost[1]), 0, '', ',');
    $GraphInvestment = $TProjectCost[1];

    $TPPATerm = (array) json_decode($financial_summary_data['ppa_term']);

    $FinanceTerm = $TPPATerm[1] / 12;
    $monthlyPaymentPlanSolar = 0;
    if (isset($MonthlyPayment)) {
        $monthlyPaymentPlanSolar = $TMonthlyPaymentPlan[1] * 12;
    }

    $MonthlyPayment = number_format((float) ($TMonthlyPaymentPlan[1]), 0, '', ',');
    $AnnualCashflow = number_format((float) ($monthlyPaymentPlanSolar), 0, '', ',');
}

$TenYearsSolarRate = 0;
if (isset($financial_summary_data['degradation_data'])) {
    $stat = $financial_summary_data['degradation_data'];
    $statJson = json_decode($financial_summary_data['degradation_data']);
    $FinalEnergyProvidedBySolar = $statJson->FinalEnergyProvidedBySolar;

    for ($i = 0; $i < 10; $i++) {
        $TenYearsSolarRate = $TenYearsSolarRate + $FinalEnergyProvidedBySolar[$i];
    }

    if ($financial_summary_data['type'] == 'VEEC') {
        $TenYearsSolarRate = ($TProjectCost[1] / $TenYearsSolarRate) * 100;
    } else {
        $TenYearsSolarRate = $TenYearsSolarRate / $proposal_data['price_before_stc_rebate'];
    }
}



$MeterDataPiaYear = 0;
$MeterDataPiaPercentage = 0;
$MeterDataSubsequent = 0;

if (isset($proposal_data['price_annual_increase'])) {
    $price_annual_increase = json_decode($proposal_data['price_annual_increase']);
    $MeterDataPiaYear = $price_annual_increase->MeterDataPiaYear;
    $MeterDataPiaPercentage = $price_annual_increase->MeterDataPiaPercentage;
    $MeterDataSubsequent = $price_annual_increase->MeterDataSubsequent;
}

?>

<html>

<head>
    <title></title>
    <style>
        .page17 .back-bg {
            background-color: white;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page17 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 630px;
            padding-top: 30px;
        }

        .page17 .top hr {
            width: 70%;
            height: 2px;
            margin: -24px 0px 0px;
            background-color: #D01E2A;
        }

        .page17 .text_color {
            color: #D01E2A;
        }

        .page17 .vkcontainer {
            margin: 0 30px;
        }

        .page17 .title h1 {
            padding-left: 14px;
            margin-top: 40px;
            border-left: 5px solid #D01E2A;
        }

        .page17 .vktitle h2 {
            font-weight: 500;
            letter-spacing: 1px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            background: linear-gradient(125deg, #D01E2A 87%, transparent 15%);
        }

        .page17 table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .page17 tr {
            border-bottom: 1px solid #cac8c8;
        }

        .page17 td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page17 .footer {
            display: inline-flex;
        }

        .page17 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page17 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page17 .image img {
            margin-top: -50px;
        }

        .page17 .fimagee img {
            margin-top: 9px;
            margin-left: 730px;
        }

        .page17 .vk-innerbox {
            width: 33%;
            float: left;
            text-align: center;
        }

        .page17 .vk-innerbox h4 {
            margin: 0px;
        }

        .page17 .vkid-section {
            padding-top: 40px;
            margin-bottom: 60px;
        }

        .page17 .vktitle h2 span {
            background: #fff;
            padding: 2px 4px;
            margin-right: 15px;
            color: #fff;
            font-size: 12px;
        }

        .page17 .vksaving-box tr {
            border: none;
        }

        .page17 .rightvkbx h3 {
            font-weight: 600;
            letter-spacing: 3px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            text-align: center;
        }

        .page17 .finacetable tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page17 .finacetable tr th {
            text-align: left;
            font-size: 20px;
        }

        .page17 .finacetable tr td {
            text-align: right;
            font-size: 20px;
        }

        .page17 .finacetable tr th span {
            background: #D01E2A;
            padding: 4px 7px;
            margin-right: 15px;
            color: #D01E2A;
            font-size: 12px;
            border-radius: 50%;
        }

        .page17 .footertext {
            margin: 50px 100px 0;
            padding-bottom: 38px;
        }

        .page17 .vksaving-box {
            margin-left: 100px;
            margin-right: 100px;
        }

        .page17 .vkftrm {
            margin: 0 30px;
        }

        .page17 .customBlock {
            position: relative;
            z-index: 1;
        }
    </style>
</head>

<body>
<div class="back-bg page17">
    <div class="vkcontainer">
        <div class="top vk-header" style="width:100%;">
            <div class="headertext">
                <h2>
                    <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
            </div>
            <hr>
        </div>
        <div class="title">
            <h1 style="text-transform:uppercase;">FINANCIAL <span class="text_color"> SUMMARY<?php if ($financial_summary_data['type'] == 'VEEC') {
                        echo ' - VEEC CLAIM';
                    } ?></span></h1>
        </div>
        <div class="vktitle" style="padding-top:12px;">
            <h2 class="customBlock" style="text-transform:uppercase; font-size:20px;width: 52%;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon1.png'); ?>" style="width:32px;"><b style="vertical-align: super;"> OUTRIGHT PURCHASE</b></h2>
        </div>
        <div class="vkbargraph" style="margin-top: -60px; margin-bottom: -30px;">
            <table>
                <tr style="border:none;">
                    <td>
                        <div id="financial_summery_graph2"></div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="vksaving-box customBlock">
            <table>
                <tr>
                    <th style="width:50%">SAVINGS PER YEAR</th>
                    <th style="width:50%; padding:0px 0px 0px 20px;">RETURN OF INVESTMENT</th>
                </tr>
            </table>
        </div>
        <div class="vksaving-box" style="margin-top:20px;">
            <table>
                <tr>
                    <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                        <h3 style="margin: 0;" id="saving_per_year2"></h3>
                    </td>
                    <td class="rightvkbx" style="padding:0px 15px;">
                        <h3 style="margin: 0;" id="return_of_investment2"></h3>
                    </td>
                </tr>
            </table>
        </div>
        <!-- option 2 -->
        <?php if (count($proposal_finance_data)) { ?>
            <div class="vktitle">
                <h2 style="text-transform:uppercase; font-size:20px;width: 30%;margin-top:30px;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon2.png'); ?>" style="width: 36px;"><b style="vertical-align: super;"> FINANCE </b></h2>
            </div>
            <div class="vkftrm">
                <table class="finacetable">
                    <tr>
                        <th><span>0</span> FINANCE TERMS:</th>
                        <td><?php echo $FinanceTerm; ?></td>
                    </tr>
                    <tr>
                        <th><span>0</span> MONTHLY PAYMENT:</th>
                        <td>$<?php echo $MonthlyPayment; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><span>0</span> ANNUAL CASHFLOW:</th>
                        <td id="annual_cashflow2">$<?php echo $AnnualCashflow; ?></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <div class="vksaving-box" style="margin-top:30px; <?php echo $SolarEnergyRatewidth; ?>">
            <table>
                <tr>
                    <?php if ($financial_summary_data['type'] != 'VEEC') { ?>
                        <th>TOTAL <?php echo $financial_summary_data['type']; ?> FINANCIAL INCENTIVE</th>
                    <?php } ?>
                    <th style="color:#D01E2A">10 YEARS SOLAR ENERGY RATE</th>
                </tr>
            </table>
        </div>
        <div class="vksaving-box" style="margin-top:20px; <?php echo $SolarEnergyRatewidth; ?>">
            <table>
                <tr style="margin-top:15px;">
                    <?php if ($financial_summary_data['type'] != 'VEEC') { ?>
                        <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                            <h3 style="margin: 0;" id="FinalLGCIncentives"></h3>
                        </td>
                    <?php } ?>
                    <td class="rightvkbx" style="padding:0px 15px;">
                        <h3 style="margin: 0;"><?php echo number_format((float)$TenYearsSolarRate, 2, '.', ''); ?>¢/kWh</h3>
                    </td>
                </tr>
            </table>
        </div>
        <div class="footertext" class="footertext" style="padding-bottom: 0; margin-bottom: -16px;">
            <p>Total revenue includes potential <?php echo $FinancialType; ?> savings, <?php echo $FinancialType; ?> savings are determined by differing market conditions</p>
                <p>Savings are calculated with <?php echo $MeterDataPiaPercentage; ?>% average annual increase for first <?php echo $MeterDataPiaYear; ?> Years and <?php echo $MeterDataSubsequent; ?>% subseuenltly in energy cost from energy provider</p>
        </div>
    </div>
    <div class="fimagee">
        <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
    </div>
    <div class="footer">
        <div class="redf">
        </div>
        <div class="greyf">
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    var stat = <?php echo $stat; ?>;
    var FinancialSummeryGraphCount = 0;
    var FinancialSummeryGraphData = [];
    google.load('visualization', '1', {
        packages: ['corechart']
    });
    $(document).ready(function() {

        var data_length = stat.data_length;
        var FinalCummilativeSavings = stat.FinalCummilativeSavings;
        var ttype = stat.type;
        var FinalLGCIncentives = [];
        var FinalLGCIncentivesSum = 0;
        if (ttype == 'LGC') {
            FinalLGCIncentives = stat.FinalLGCValues;
            FinalLGCIncentivesSum = FinalLGCIncentives.reduce((partial_sum, a) => partial_sum + a, 0);
            $("#FinalLGCIncentives").text('$' + dollarUSLocale.format(FinalLGCIncentivesSum.toFixed(0)));
        }
        if (ttype == 'STC') {
            FinalLGCIncentives = stat.FinalLGCValues;
            FinalLGCIncentivesSum = FinalLGCIncentives.reduce((partial_sum, a) => partial_sum + a, 0);
            $("#FinalLGCIncentives").text('$' + dollarUSLocale.format(FinalLGCIncentivesSum.toFixed(0)));
        }
        FinancialSummeryGraphCount = FinalCummilativeSavings.length;
        FinancialSummeryGraphData = FinalCummilativeSavings;
        if (FinancialSummeryGraphCount != 0) {
            google.setOnLoadCallback(drawChart3);
        }

    });

    function drawChart3() {

        var FinancialSummeryGraphArray = [];
        var GraphInvestment = "<?php echo $GraphInvestment; ?>";
        FinancialSummeryGraphArray[0] = ["Year", "", "Investement"];
        for (let index = 0; index < FinancialSummeryGraphCount; index++) {
            FinancialSummeryGraphArray[index + 1] = [(index + 1).toString(), FinancialSummeryGraphData[index], parseFloat(GraphInvestment)];
            if (FinancialSummeryGraphData[index] > parseFloat(GraphInvestment)) {
                break;
            }
        }
        console.log(FinancialSummeryGraphData);
        $("#saving_per_year2").text('$' + dollarUSLocale.format(FinancialSummeryGraphData[0].toFixed(0)));
           // $("#return_of_investment2").text((FinancialSummeryGraphArray.length - 1) + ' Years');
        $("#annual_cashflow2").text('$' + dollarUSLocale.format((FinancialSummeryGraphData[0] - parseFloat(<?php echo $monthlyPaymentPlanSolar; ?>)).toFixed(0)));

        $("#return_of_investment2").text(parseFloat(GraphInvestment /FinancialSummeryGraphData[0]).toFixed(2) + ' Years');
        console.log(FinancialSummeryGraphArray)

        var data = google.visualization.arrayToDataTable(FinancialSummeryGraphArray);
        var options = {
            curveType: 'function',
            titleTextStyle: {
                fontName: 'Arial, sans-serif;',
                italic: false,
                bold: false,
                fontStyle: 'normal',
                fontSize: 12
            },
            bar: {
                groupWidth: "50%"
            },
            legend: {
                position: 'bottom',
                textStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontSize: 14,
                    fontStyle: 'normal'
                }
            },
            hAxis: {
                format: "#",
                titleTextStyle: {
                    fontName: "Arial, sans-serif;",
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: "normal",
                },
                textStyle: {
                    fontName: "Arial, sans-serif;",
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: "normal",
                },
            },
            vAxis: {
                format: "'$'#",
                title: "",
                titleTextStyle: {
                    fontName: "Arial, sans-serif;",
                    italic: false,
                    bold: true,
                    fontSize: 13,
                    fontStyle: "normal",
                },
                textStyle: {
                    fontName: "Arial, sans-serif;",
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: "normal",
                },
            },
            width: 810,
            height: 372,
            legend: {
                position: 'none',
                textStyle: {
                    fontSize: 4,
                }
            },
            seriesType: 'bars',
            series: {
                0: {
                    color: '#A1CB79'
                },
                1: {
                    type: 'line',
                    color: '#000',
                    visibleInLegend: false
                }
            }
        };
        var chart = new google.visualization.ComboChart(document.getElementById('financial_summery_graph2'));
        chart.draw(data, options);
    }
</script>
</body>

</html>
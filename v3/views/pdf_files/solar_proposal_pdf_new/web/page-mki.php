<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page-mki-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 1287px !important;
            max-height: 910px !important;
            width: 1287px !important;
            height: 910px !important;
        }

        .page-mki .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 1044px;
            padding-top: 30px;
        }

        .page-mki .top hr {
            width: 76%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-mki .text_color {
            color: #D01E2A;
        }

        .page-mki .title {
            margin-top: -50px;
        }

        .page-mki .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page-mki .footer {
            display: inline-flex;
        }

        .page-mki .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-mki .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }

        .page-mki .ingBox {
            display: inline-flex;
            padding-top: 100px;
            padding-bottom: 188px;
        }

        .page-mki .iiimage {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 650px;
            margin-left: 48px;
        }

        .page-mki .iibox {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 500px;
            margin-left: 48px;
        }

        .page-mki .fimagee img {
            margin-left: 1120px;
        }
    </style>
</head>

<body>
    <div class="page-mki-back-bg back-bg page-mki">
        <div class="top">
            <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>YOUR SOLAR <span class="text_color"> MOUNT KIT INSTALLATION</span></h1>
        </div>
        <div class="ingBox">
            <div class="iiimage">
                <div class="add-picture2" 
                    data-src="" 
                    style="background-image: url(''); 
                           background-repeat: no-repeat; background-position: 50% 50%; 
                           background-size: 100% 100%; 
                           width: 650px;
                           height: 400px; ">
                </div>
            </div>
            <div class="iibox">
            </div>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
<?php

class Notification_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    public function get_lead_allocation_data($id = FALSE , $franchise_id = FALSE){
        $sql = "SELECT la.note,la.daily_average_usage,la.highbay,la.panel_light,la.flood_light,la.batten_light,la.is_calendar_meeting,laa.scheduled_date,laa.scheduled_time,"
                ."leads.id,leads.user_id as lead_creator_id,leads.uuid,leads.created_at,leads.lead_status,leads.lead_source,leads.cust_id,"
                . "ltu.user_id as franchise_id,"
                ."cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.company_name,cust.customer_contact_no,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_postal as customer_state,"
                . "cat.category_name,"
                . "assinged_ud.full_name as sales_rep_name,assinged_ud.company_contact_no as sales_rep_contact_no,"
                . "creator_ud.full_name as lead_allocator_name,creator_ud.company_contact_no as franchise_contact_no"
                . " FROM tbl_lead_allocations as la";
        $sql .= " LEFT JOIN tbl_leads as leads ON leads.id = la.lead_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_service_category as cat ON cat.id = la.category_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as assinged_ud ON assinged_ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_details as creator_ud ON creator_ud.user_id = leads.user_id";
        $sql .= " LEFT JOIN tbl_activities as laa ON laa.id = la.activity_id";

        if ($id && $franchise_id) {
            $sql .= " WHERE leads.id='" . $id . "' && ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($id) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($franchise_id) {
            $sql .= " WHERE ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }
    
    
    public function get_lead_notification_data($id = FALSE , $franchise_id = FALSE){
        $sql = "SELECT leads.id,leads.user_id as lead_creator_id,leads.uuid,leads.created_at,leads.lead_status,leads.lead_source,leads.cust_id,leads.lead_industry,"
                . "ltu.user_id as franchise_id,"
                ."cust.first_name,cust.last_name,cust.customer_email,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.company_name,cust.customer_contact_no,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,state.state_postal as customer_state,"
                . "assinged_ud.full_name as sales_rep_name,assinged_ud.company_contact_no as sales_rep_contact_no,"
                . "creator_ud.full_name as lead_creator_name,creator_ud.company_contact_no as franchise_contact_no"
                . " FROM tbl_leads as leads";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = leads.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = leads.id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as assinged_ud ON assinged_ud.user_id = user.id";
        $sql .= " LEFT JOIN tbl_user_details as creator_ud ON creator_ud.user_id = leads.user_id";

        if ($id && $franchise_id) {
            $sql .= " WHERE leads.id='" . $id . "' && ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($id) {
            $sql .= " WHERE leads.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else if ($franchise_id) {
            $sql .= " WHERE ltu.user_id='" . $franchise_id . "'";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

    
}

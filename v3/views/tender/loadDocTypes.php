<?php if(count($documentTypes) > 0) {
    foreach($documentTypes as $eachDocument) { ?>
<tr>
    <td> <?php echo $eachDocument['name']; ?> </td>
    <td>
        <?php if($eachDocument['noOfDoc'] <= 0 ) { ?>
    <a href="javascript:void(0);" class="mising-btn"> Missing </a>
<?php } else  {?> <a href="javascript:void(0);" class="upload-btn"> Uploaded </a> <?php } ?> </td>
    <td class="text-left"> <?php echo $eachDocument['noOfDoc']; ?> </td>
    <td>

        <label for="file-upload-<?php echo $eachDocument['id']; ?>" class="custom-file-upload">
            <i class="fa fa-upload" aria-hidden="true"></i> Upload </label>&nbsp;&nbsp;
        <input class="uploadDocuments" id="file-upload-<?php echo $eachDocument['id']; ?>" data-typeId="<?php echo $eachDocument['id']; ?>" type="file" name="<?php echo "doctype_{$eachDocument['id']}"?>" />
        <a href="javascript:void(0)" class="add_solar_category_attachment view-btn filesList" data-typeId="<?php echo $eachDocument['id']; ?>"><i class="fa fa-folder-open"></i> View </a>
    </td>

</tr>
<?php } ?>


    <script>

        $('input[id^=file-upload-]').on('change',function(){
            $('input#docTypeId').val($(this).attr('data-typeId'));
            $('form#uploadTenderDocuments').submit();
        });
        $('.filesList').on('click',function(){
            var docCatId = $(this).attr('data-typeId');
            $.ajax({
                url: '<?php echo site_url('admin/tender/get-documents');?>',
                type: 'POST',
                data: {
                    catId: docCatId,
                },
                success: function(data) {
                    $('#tenderDocumentsList').html(data);
                    $('#tenderDocumentsListModel').modal('show');
                }
            });
        });
        function deleteDocument(docId){
            //$('tr#docRow_' + docId).remove();
            $.ajax({
                url: '<?php echo site_url('admin/tender/delete-document');?>',
                type: 'POST',
                data: {
                    docId: docId,
                },
                success: function(data) {
                    result = JSON.parse(data);
                    if(result.success) {
                        $('tr#docRow_' + docId).remove();
                        loadDocTypes();
                    }
                }
            });
        }
        function downloadDocument(docId){
           window.location.href = '<?php echo base_url('admin/tender/download-document/')?>'+docId;
        }
    </script>
<?php } ?>

<?php

class Proposal_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->edcrm_db = $this->load->database('edcrm_db', TRUE);
    }

    /**
     * fetch led proposal data
     * @param type $lead_id
     * @param type $site_id
     * @return array
     */
    function get_led_proposal_data($lead_id = FALSE, $site_id = FALSE)
    {
        $sql = "SELECT lp.*,"
            . "pf.term,pf.monthly_payment_plan,pf.monthly_saving,pf.monthly_net_cashflow,pf.upfront_payment_options"
            . " FROM tbl_led_proposal as lp";
        $sql .= " LEFT JOIN tbl_proposal_finance as pf ON pf.proposal_id = lp.id AND proposal_type=1";
        if ($lead_id && $site_id) {
            $sql .= " WHERE lp.lead_id='" . $lead_id . "' AND lp.site_id='" . $site_id . "'";
        }
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * fetch solar proposal data
     * @param type $lead_id
     * @param type $site_id
     * @return array
     */
    function get_solar_proposal_data($lead_id = FALSE, $site_id = FALSE, $proposal_id = FALSE, $uuid = FALSE)
    {
        $sql = "SELECT sp.*,cl.id as location_id,cl.address,cl.site_name,"
            . "pf.term,pf.monthly_payment_plan,pf.monthly_saving,pf.monthly_net_cashflow,pf.upfront_payment_options,cust.customer_contact_no,cust.customer_contact_no,cust.first_name,CONCAT(cust.first_name,' ',cust.last_name) as customer_name,cust.customer_email,cl.address as customer_site_address,ud.full_name as sales_person_name,utu.email as sales_person_email,ud.company_name,ud.company_contact_no as sales_person_contact_no"
            . " FROM tbl_solar_proposal as sp";
        $sql .= " LEFT JOIN tbl_proposal_finance as pf ON pf.proposal_id = sp.id AND proposal_type=2";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.id = sp.lead_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = lead.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.id = sp.site_id";
        $sql .= " LEFT JOIN aauth_users as utu ON utu.id = lead.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = lead.user_id";
        if ($lead_id && $site_id) {
            $sql .= " WHERE sp.lead_id='" . $lead_id . "' AND sp.site_id='" . $site_id . "'";
        } else if (isset($proposal_id) && $proposal_id != '') {
            $sql .= " WHERE sp.id='" . $proposal_id . "'";
        } else if (isset($uuid) && $uuid != '') {
            $sql .= " WHERE sp.proposal_uuid='" . $uuid . "'";
        }
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * Fetch Dashboard Report data
     */
    public function get_dashboard_report_data_monthly($month, $year, $user_name)
    {

        //Get Formulas
        $formulas = $this->common->fetch_where('tbl_simpro_sales_formula', '*', NULL);

        $combine_formula_query = '';

        foreach ($formulas as $key => $value) {
            //Replace Dynamic values
            $formula = '';
            $formula = str_replace('{month}', $month, $value['formula']);
            $formula = str_replace('{year}', $year, $formula);
            $formula = str_replace('{user_name}', $user_name, $formula);
            if ($key == (count($formulas) - 1)) {
                $combine_formula_query .= $formula;
            } else {
                $combine_formula_query .= $formula . ',';
            }
        }

        $sql = "SELECT dm.id,"
            . "$combine_formula_query"
            . " FROM tbl_simpro_dump as dm LIMIT 1";

        //echo $sql;die;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_MaxValueAnnualSolarDataByArea($solarAreaId)
    {
        /*
          Adelaide = 1
          Brisbane = 3
          Canberra = 5
          Darwin = 6
          Hobart = 7
          Melbourne = 8
          Perth = 9
          Sydney = 10
         */
        if ($solarAreaId == 1) {
            $dataTable = "adelaide";
        } elseif ($solarAreaId == 3) {
            $dataTable = "brisbane";
        } elseif ($solarAreaId == 5) {
            $dataTable = "canberra";
        } elseif ($solarAreaId == 6) {
            $dataTable = "darwin";
        } elseif ($solarAreaId == 7) {
            $dataTable = "hobart";
        } elseif ($solarAreaId == 8) {
            $dataTable = "melbourne";
        } elseif ($solarAreaId == 9) {
            $dataTable = "perth";
        } elseif ($solarAreaId == 10) {
            $dataTable = "sydney";
        }

        $sql = "SELECT  MAX(" . $dataTable . ") as max_annual_solar FROM tbl_annual_solar_data WHERE 1 ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }


    /** Residential Solar Proposla Functions */

    public function get_proposal_by_uuid($proposal_uuid)
    {
        $sql = "SELECT tp.*,cust.company_name,cust.first_name,cust.last_name,cust.title,cust.customer_email,cust.customer_contact_no,"
            . "site.id as location_id,site.address,site.suburb,site.postcode,site.state_id,"
            . "ud.full_name,ud.company_contact_no,ud.company_name as franchise_company_name,"
            . "ud.company_bsb as franchise_bsb,ud.company_account_name as franchise_account_name,ud.company_account as franchise_account_no,"
            . "user.email,ltu.user_id,"
            . "lead.id as lead_id,lead.lead_source as deal_source"
            . " FROM tbl_proposal as tp";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.id = tp.lead_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = tp.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as site ON site.id = tp.site_id";
        //$sql .= " LEFT JOIN aauth_users as user ON user.id = tp.agent_id";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " WHERE tp.uuid='" . $proposal_uuid . "'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_proposal_user_details_by_uuid($proposal_uuid)
    {
        $sql = "SELECT "
            . "ud.full_name,ud.company_contact_no,ud.company_name as franchise_company_name,"
            . "ud.company_bsb as franchise_bsb,ud.company_account_name as franchise_account_name,ud.company_account as franchise_account_no,"
            . "user.email"
            . " FROM tbl_proposal as tp";
        $sql .= " LEFT JOIN aauth_users as user ON user.id = tp.agent_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = tp.agent_id";
        $sql .= " WHERE tp.uuid='" . $proposal_uuid . "'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function get_proposal_data_by_proposal_id($id)
    {
        $sql = "SELECT prd.*,tpd.item_id,tpd.item_type,tpd.qty,tpd.custom_price,tpd.final_price,prd_type.typeName,"
            . "tpv.state as variant_state,tpv.id as variant_id,tpv.application as variant_application,"
            . "tpv.cost_price as variant_cp,tpv.is_price_set as variant_ips,tpv.amount as variant_amt,tpv.additional_margin as variant_am"
            . " FROM tbl_products as prd";
        $sql .= " LEFT JOIN tbl_proposal_data as tpd ON prd.id = tpd.item_id";
        $sql .= " LEFT JOIN tbl_product_variants as tpv ON tpd.item_variant_id = tpv.id";
        $sql .= " LEFT JOIN tbl_product_type as prd_type ON prd.type_id = prd_type.typeId";
        $sql .= " WHERE tpd.proposal_id='" . $id . "' AND tpd.item_type != 3";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_proposal_line_item_data_by_proposal_id($id)
    {
        $sql = "SELECT lid.*,li.line_item,li.per_which_product,"
            . "tpd.item_id,tpd.item_type,tpd.qty,tpd.custom_price,tpd.final_price"
            . " FROM tbl_service_line_item_data as lid";
        $sql .= " LEFT JOIN tbl_proposal_data as tpd ON lid.id = tpd.item_id";
        $sql .= " LEFT JOIN tbl_service_line_item as li ON li.id = lid.line_item_id";
        $sql .= " WHERE tpd.proposal_id='" . $id . "' AND tpd.item_type = 3";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_proposal_data_by_proposal_id_type($id, $type)
    {
        $sql = "SELECT prd.*,tpd.item_id,tpd.item_type,tpd.qty,tpd.custom_price,tpd.final_price,prd_type.typeName,"
            . "tpv.state as variant_state,tpv.id as variant_id,tpv.application as variant_application,"
            . "tpv.cost_price as variant_cp,tpv.is_price_set as variant_ips,tpv.amount as variant_amt,tpv.additional_margin as variant_am"
            . " FROM tbl_products as prd";
        $sql .= " LEFT JOIN tbl_proposal_data as tpd ON prd.id = tpd.item_id";
        $sql .= " LEFT JOIN tbl_product_variants as tpv ON tpd.item_variant_id = tpv.id";
        $sql .= " LEFT JOIN tbl_product_type as prd_type ON prd.type_id = prd_type.typeId";
        $sql .= " WHERE tpd.proposal_id='" . $id . "' AND tpd.item_type = '" . $type . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}

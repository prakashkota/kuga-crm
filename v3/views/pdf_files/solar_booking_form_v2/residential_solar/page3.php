<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="100%" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <?php $extraHeight = 630; if($cost_centre_id == 8){?>
    <tr>
        <td valign="top">
            <table width="828" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse;">
                <tr>
                    <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Rebate Eligible</td>
                    <td valign="top">
                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td width="415" valign="top" style="padding-right:10px">
                                        <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                                <tr>
                                                    <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_rebate" type="radio" <?php if(isset($business_details->vic_rebate) && $business_details->vic_rebate == '1'){ echo "checked"; } ?> id="vic_rebate_1">
                                                            <label class="form-check-label" for="vic_rebate_1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_rebate" type="radio" <?php if(isset($business_details->vic_rebate) && $business_details->vic_rebate == '0'){ echo "checked"; } ?> id="vic_rebate_0">
                                                            <label class="form-check-label" for="vic_rebate_0">No</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Loan Eligible</td>
                    <td valign="top">
                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td width="415" valign="top" style="padding-right:10px">
                                        <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                                <tr>
                                                    <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_loan" type="radio" <?php if(isset($business_details->vic_loan) && $business_details->vic_loan == '1'){ echo "checked"; } ?> id="vic_loan_1" value="1">
                                                            <label class="form-check-label" for="vic_loan_1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_loan" type="radio" id="vic_loan_0" value="0" <?php if(isset($business_details->vic_loan) && $business_details->vic_loan == '0'){ echo "checked"; } ?>>
                                                            <label class="form-check-label" for="vic_loan_0">No</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php if(isset($business_details->vic_rebate) || isset($business_details->vic_loan)){?>
                    <tr class="expiry_date_tr hidden">
                        <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Expiry Date</td>
                        <td style="padding:5px; border:solid 1px #e4e4e4; height:25px; font-family:Calibri, sans-serif; font-size:13px;"><?= (isset($business_details->expiry_date)) ? $business_details->expiry_date : ''; ?></td>
                    </tr>
                <?php $extraHeight = $extraHeight- 90; }else{ $extraHeight = $extraHeight- 60; } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <?php } ?>
    <tr>
        <td valign="top"><table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
            <tr>
                <th width="300" height="30" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:14px">Prodcut Type</th>
                <th width="336" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px">Description</th>
                <th width="80" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px">Units</th>
                <th width="100" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                <th width="100" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
            </tr>
            <?php  for($i = 0; $i < count($product->product_name); $i++){
                    if(!empty($product->product_name[$i])  && $product->product_name[$i] != " "){
                        $total_cost_excGST = '';
                        if($product->product_qty[$i] != '' && $product->product_cost_excGST[$i] != ''){
                            $total_cost_excGST = $product->product_qty[$i]  * $product->product_cost_excGST[$i];
                        }
                ?>
                <tr>
                    <td height="25" style="padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong><?php echo $product->product_type[$i]; ?></strong></td>
                    <td style="padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong><?php echo $product->product_name[$i]; ?></strong></td>
                    <td style="padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong><?php echo $product->product_qty[$i]; ?></strong></td>
                    <td style="padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong><?php echo $product->product_cost_excGST[$i]; ?></strong></td>
                    <td style="padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong><?php echo $total_cost_excGST; ?></strong></td>
                </tr>
            <?php $extraHeight = $extraHeight - 25;} } ?>
        </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="450" height="30" align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:15px">&nbsp;</td>
                <td width="212" height="30" align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:14px"><strong>System Total (inc GST):</strong></td>
                <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_incGst; ?></strong></td>
            </tr>
            <tr>
                <td align="left" valign="top">&nbsp;</td>
               <td align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:14px"><strong>STC Deduction</strong></td>
               <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->stc_deduction; ?></strong></td>
           </tr>
           <?php if(isset($booking_form->vic_rebate)){ ?>
            <tr>
                <td height="30" style="font-family:Calibri, sans-serif; font-size:15px">&nbsp;</td>
                <td height="30" align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:14px"><strong>Solar VIC Rebate:</strong></td>
                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->vic_rebate; ?></strong></td>
            </tr>
        <?php $extraHeight = $extraHeight - 30; } if($booking_form->vic_loan){ ?>
            <tr>
                <td height="30" style="font-family:Calibri, sans-serif; font-size:15px">&nbsp;</td>
                <td height="30" align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:14px"><strong>Solar VIC Loan:</strong></td>
                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->vic_loan; ?></strong></td>
            </tr>
        <?php $extraHeight = $extraHeight - 30; } ?>
        <tr>
            <td height="30" style="font-family:Calibri, sans-serif; font-size:15px">&nbsp;</td>
            <td height="30" align="right" style="padding:5px; font-family:Calibri, sans-serif; font-size:14px"><strong>Total Payable(inc GST):</strong></td>
            <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_payable; ?></strong></td>
        </tr>
    </table></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
        <tr>
            <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Access details</strong></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="415" valign="top" style="padding-right:10px">
                            <table width="405" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="165" style="font-family:Calibri, sans-serif; font-size:14px;">Roof Height:</td>
                                    <td width="240" style="padding:5px; border:solid 1px #e4e4e4; height:25px; font-family:Calibri, sans-serif; font-size:13px;"><strong><?php echo $booking_form->roof_height; ?></strong></td>
                                </tr>
                            </table>
                        </td>
                        <td width="415" valign="top" style="padding-left:10px">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
        <tr>
            <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="162" style="font-family:Calibri, sans-serif; font-size:14px;">Access Equipments:</td>
                            <td width="665" style="padding:5px; border:solid 1px #e4e4e4; height:25px; font-family:Calibri, sans-serif; font-size:13px;"><strong><?php echo $booking_form->access_equipments; ?></strong></td>
                        </tr>
                    </table></td>
                </tr>
            </table></td>
        </tr>
    </table>
</td>
</tr>
<tr>
    <td height="<?= $extraHeight ?>">&nbsp;</td>
</tr>
<tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
        </tr>
        <tr>
            <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
        </tr>
    </table></td>
    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
    </tr>
    <tr>
        <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
    </tr>
    <tr>
        <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
    </tr>
</table></td>
<td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />4</td>
</tr>
</table></td>
</tr>
</table>

<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; height:100%;}
    .state{width: 70px !important;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Franchise - <?php } ?>Add Product Type</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Product Type Details</h4>
                                    <div class="form-group">
                                        <label>Parent Type</label>
                                        <select name="parent_id" class="form-control">
                                        <option value="">Select Parent Category</option>    
                                        <?php if(isset($parent_types) && !empty($parent_types)){
                                            foreach($parent_types as $type){
                                            ?>
                                        <option value="<?php echo $type['type_id']; ?>"<?php if((isset($parent_id)) && $parent_id = $type['type_id']){ ?>  selected="selected" <?php } ?> ><?php echo $type['type_name']; ?></option>
                                        <?php }} ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Type Name</label>
                                        <input type="text" name="type_name" value="<?php echo (isset($type_name)) ? $type_name : ''; ?>" class="form-control" placeholder="Type Name"   required>
                                        <?php echo form_error('type_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="buttwrap">
                                <input type="submit"  class="btn" value="SAVE">
                            </div> 
                        </div> 
                    </div> 

                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>


<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td align="center" valign="top" height="763">
                <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="910" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/page-1-1.jpg" width="910" height="763" alt=""/></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" height="400">
                <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="445" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/page-1-2.jpg" alt="" width="100%"/></td>
                            <td width="445" valign="top">
                                <table width="445" border="0" align="center" cellpadding="3" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="445" style="border: solid 1px #333333">
                                                <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 20px; color: #333333"><u>Schedule 1</u></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" valign="top">
                                                                <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="70" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">Service Fee: $</td>
                                                                            <td width="40" style="border-bottom: dashed 1px #333333; font-family: Arial, sans-serif; font-size: 11px; color: #333333"><input type="text" style="border: 0px;" value="<?= (isset($service_fee) ? $service_fee : ''); ?>" /></td>
                                                                            <td width="180" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">+ GST per ESC or Minimum charge $</td>
                                                                            <td width="74" style="border-bottom: solid 1px #333333; font-family: Arial, sans-serif; font-size: 11px; color: #333333"><input type="text" style="border: 0px;" value="<?= (isset($gst_min_charge) ? $gst_min_charge : ''); ?>" /></td>
                                                                            <td width="39" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">+ GST</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">Agreed Service Fee and any specific additional services and charges to be noted here( eg, Reflected Ceiling Plans, Product Approval ):</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="25" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="85" height="25" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">The Return will be going to</td>
                                                            <td width="295" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333; border-bottom: solid 1px #333333"><input type="text" style="border: 0px;width:100%;" value="<?= (isset($return_going_to) ? $return_going_to : ''); ?>" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">(If above left blank, the return will be provided to customer)</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="10">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/page-1-3.jpg" width="910" height="51" alt=""/></td>
        </tr>
    </tbody>
</table>
<!--<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">-->
<!--	<tr>-->
<!--		<td style="padding:30px;">-->
<!--			<img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/02.jpg" />-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->
<!--<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">-->
<!--	<tr>-->
<!--		<td style="padding:30px;">-->
<!--			<img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/03.jpg" />-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->
<!--<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">-->
<!--	<tr>-->
<!--		<td style="padding:30px;">-->
<!--			<img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/04.jpg" />-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->
<!--<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">-->
<!--	<tr>-->
<!--		<td style="padding:30px;">-->
<!--			<img src="<?php echo $this->config->item('live_url'); ?>job_crm/assets/nomination_form/nsw_cl/05.jpg" />-->
<!--		</td>-->
<!--	</tr>-->
<!--</table>-->
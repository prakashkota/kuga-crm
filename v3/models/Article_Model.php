<?php

class Article_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * fetch articles
     * @param int $id
     * @param int $cat_id
     * @param int $limit
     * @return array
     */
    
    function get_articles($id = FALSE,$cat_id = FALSE,$limit = 5) {
        $sql = "SELECT a.*,ac.category_name,ac.category_slug"
                . " FROM tbl_articles as a";
        $sql .= " LEFT JOIN tbl_article_categories as ac ON ac.id = a.category_id";
        if($id){
            $sql .= " WHERE a.id='" . $id . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }else if($cat_id){
            $sql .= " WHERE a.category_id='" . $cat_id . "' && a.status=1 ORDER BY a.created_at DESC LIMIT $limit";
            $query = $this->db->query($sql);
            return $query->result_array();
        }else{
             $sql .= " ORDER BY a.created_at DESC LIMIT $limit";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

   /**
     * fetch articles
     * @param string $cat_slug
     * @param string $article_slug
     * @return array
     */
    
    function get_article_by_slug($cat_slug = FALSE,$article_slug = FALSE) {
        $sql = "SELECT a.*,ac.category_name,ac.category_slug"
                . " FROM tbl_articles as a";
        $sql .= " LEFT JOIN tbl_article_categories as ac ON ac.id = a.category_id";
        if($cat_slug && $article_slug){
            $sql .= " WHERE a.article_slug='" . $article_slug . "' && ac.category_slug='" . $cat_slug . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }else{
            return array();
        }
    }
    
    /**
     * search articles using FULL TEXT SEARCH
     * @param string $keyword
     * @return array
     */
    
    function get_articles_by_keyword($keyword) {
        $sql = "SELECT a.*,ac.category_name,ac.category_slug"
                . " FROM tbl_articles as a";
        $sql .= " LEFT JOIN tbl_article_categories as ac ON ac.id = a.category_id";
        if($keyword){
            $sql .= " WHERE MATCH(a.article_title, a.article_description) AGAINST('%".$keyword."%' IN BOOLEAN MODE)";
            $query = $this->db->query($sql);
            return $query->result_array();
        }else{
            return array();
        }
    }

}

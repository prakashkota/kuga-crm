<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Edit Permission</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="example" class="table table-striped">

                                    <thead>

                                        <tr>

                                            <th>Permission</th>

                                            <th>Customer</th>

                                            <th>Product</th>

                                            <th>Services</th>
                                            
                                            <th>Markup</th>

                                            <th>Proposal</th>

                                            <th>Reports</th>

                                            <th>All Reports</th>

                                        </tr>

                                    </thead>



                                    <tbody>

                                        <tr>

                                            <td><strong>Add</strong></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['add_customer']; ?>]" value="1" <?php if ($permissions['add_customer'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['add_product']; ?>]" value="1" <?php if ($permissions['add_product'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['add_services']; ?>]" value="1" <?php if ($permissions['add_services'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['add_markup']; ?>]" value="1" <?php if ($permissions['add_markup'] == 1) { ?> checked <?php } ?> /></td>

                                            <td></td>
                                            
                                            <td></td>
                                            
                                            <td></td>

                                        </tr>

                                        <tr>

                                            <td><strong>Edit</strong></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['edit_customer']; ?>]" value="1" <?php if ($permissions['edit_customer'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['edit_product']; ?>]" value="1" <?php if ($permissions['edit_product'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['edit_services']; ?>]" value="1" <?php if ($permissions['edit_services'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['edit_markup']; ?>]" value="1" <?php if ($permissions['edit_markup'] == 1) { ?> checked <?php } ?> /></td>

                                            <td></td>
                                            
                                            <td></td>
                                            
                                            <td></td>
                                            

                                        </tr>

                                        <tr>

                                            <td><strong>View</strong></td>

                                             <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_customer']; ?>]" value="1" <?php if ($permissions['view_customer'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_product']; ?>]" value="1" <?php if ($permissions['view_product'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_services']; ?>]" value="1" <?php if ($permissions['view_services'] == 1) { ?> checked <?php } ?> /></td>

                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_markup']; ?>]" value="1" <?php if ($permissions['view_markup'] == 1) { ?> checked <?php } ?> /></td>
                                            
                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_all_proposal']; ?>]" value="1" <?php if ($permissions['view_all_proposal'] == 1) { ?> checked <?php } ?> /></td>
                                            
                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_reports']; ?>]" value="1" <?php if ($permissions['view_reports'] == 1) { ?> checked <?php } ?> /></td>
                                            
                                            <td><input type="checkbox" name="permission[<?php echo $permission_ids['view_all_reports']; ?>]" value="1" <?php if ($permissions['view_all_reports'] == 1) { ?> checked <?php } ?> /></td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="buttwrap">
                        <input type="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

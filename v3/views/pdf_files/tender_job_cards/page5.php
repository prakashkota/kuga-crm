<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Job Card</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            
            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=checkbox]:checked:before {
                color: black !important;
            }
        
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        
        </style>
    </head>
<body style="padding:0; margin:0">
<table class="pdf_page hidden" id="page_5" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td height="10">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="180" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">AC Cable length: Inverter to Connection Point</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:15px; padding:5px">
                                                <input value="<?= isset($job_card_details->ac_cable_length) ? $job_card_details->ac_cable_length : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" /> Meters
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Describe AC Cable Route (Inverter to Main Switch Board)</td>
                                        </tr>
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                <textarea style="padding:5px; width:100%; height: 100px;border:solid 1px #ccc;outline:none; box-sizing:border-box;"><?= isset($job_card_details->ac_cable_route) ? $job_card_details->ac_cable_route : '' ?></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Cable Route Photo 1</th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Cable Route Photo 2</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->cable_route_one) && $job_card_details->cable_route_one != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->cable_route_one; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->cable_route_two) && $job_card_details->cable_route_two != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->cable_route_two; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Determine & Describe AC Connection point (MSB)</td>
                                        </tr>
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                <textarea style="padding:5px; width:100%; height: 100px;border:solid 1px #ccc;outline:none; box-sizing:border-box;"><?= isset($job_card_details->ac_connection_point) ? $job_card_details->ac_connection_point : '' ?></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Condition of Switchboard</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if(isset($job_card_details->switch_board_condition)){ ?>
                                                    <input type="radio" <?php if($job_card_details->switch_board_condition == 'good') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Good</span>
                                                    <input type="radio" <?php if($job_card_details->switch_board_condition == 'bad') { echo 'checked=""'; }?>/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">Bad</span>
                                                <?php }else{ ?>
                                                    <input type="radio"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Good</span>
                                                    <input type="radio"/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">Bad</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Switchboard Upgrade</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if(isset($job_card_details->switch_board_upgrade)){ ?>
                                                    <input type="radio" <?php if($job_card_details->switch_board_upgrade == 'good') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Required</span>
                                                    <input type="radio" <?php if($job_card_details->switch_board_upgrade == 'bad') { echo 'checked=""'; }?> /> 
                                                    <span style="margin-right: 20px;font-size: 16px;">Not Required</span>
                                                <?php }else{ ?>
                                                    <input type="radio"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Required</span>
                                                    <input type="radio" /> 
                                                    <span style="margin-right: 20px;font-size: 16px;">Not Required</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="150">&nbsp;</td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td style="position:relative;bottom: -30px;"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
</body>
</html>
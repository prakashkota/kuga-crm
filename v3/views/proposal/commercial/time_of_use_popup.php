<div class="modal fade time_use_model" id="timeUseModel" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="timeUseModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="" id="timeUseModelLabel">Time of Use</h3>

                    <div style="display:block;">
                        <select name="timeOfUseOptions" id="timeOfUseOptions" style="width: 106px;margin-right: 20px;">
                            <option value="1" selected>Standard</option>
                            <option value="2">With Shoulder</option>
                            <option value="3">Custom</option>
                        </select>
                        <button type="button" class="close closemediapop" data-dismiss="modal">×</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <table class="table table-borderd">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Weekdays</th>
                                    <th>Saturday</th>
                                    <th>Sunday</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        12 AM - 1 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time1[]" value="12:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time1" value="00:00" />
                                        <input type="hidden" name="time_of_use_end_time1" value="01:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays1" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays1" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays1" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat1" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat1" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat1" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun1" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun1" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun1" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1 AM - 2 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time2[]" value="01:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time2" value="01:00" />
                                        <input type="hidden" name="time_of_use_end_time2" value="02:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays2" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays2" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays2" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat2" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat2" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat2" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun2" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun2" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun2" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2 AM - 3 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time3[]" value="02:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time3" value="02:00" />
                                        <input type="hidden" name="time_of_use_end_time3" value="03:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays3" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays3" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays3" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat3" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat3" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat3" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun3" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun3" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun3" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3 AM - 4 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time4[]" value="03:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time4" value="03:00" />
                                        <input type="hidden" name="time_of_use_end_time4" value="04:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays4" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays4" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays4" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat4" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat4" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat4" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun4" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun4" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun4" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4 AM - 5 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time5[]" value="04:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time5" value="04:00" />
                                        <input type="hidden" name="time_of_use_end_time5" value="05:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays5" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays5" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays5" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat5" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat5" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat5" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun5" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun5" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun5" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5 AM - 6 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time6[]" value="05:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time6" value="05:00" />
                                        <input type="hidden" name="time_of_use_end_time6" value="06:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays6" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays6" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays6" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat6" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat6" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat6" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun6" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun6" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun6" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6 AM - 7 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time7[]" value="06:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time7" value="06:00" />
                                        <input type="hidden" name="time_of_use_end_time7" value="07:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays7" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays7" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays7" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat7" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat7" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat7" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun7" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun7" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun7" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7 AM - 8 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time8[]" value="07:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time8" value="07:00" />
                                        <input type="hidden" name="time_of_use_end_time8" value="08:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays8" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays8" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays8" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat8" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat8" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat8" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun8" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun8" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun8" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8 AM - 9 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time9[]" value="08:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time9" value="08:00" />
                                        <input type="hidden" name="time_of_use_end_time9" value="09:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays9" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays9" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays9" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat9" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat9" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat9" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun9" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun9" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun9" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9 AM - 10 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time10[]" value="09:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time10" value="09:00" />
                                        <input type="hidden" name="time_of_use_end_time10" value="10:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays10" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays10" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays10" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat10" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat10" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat10" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun10" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun10" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun10" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10 AM - 11 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time11[]" value="10:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time11" value="10:00" />
                                        <input type="hidden" name="time_of_use_end_time11" value="11:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays11" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays11" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays11" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat11" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat11" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat11" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun11" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun11" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun11" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11 AM - 12 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time12[]" value="11:00 AM" /> -->
                                        <input type="hidden" name="time_of_use_start_time12" value="11:00" />
                                        <input type="hidden" name="time_of_use_end_time12" value="12:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays12" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays12" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays12" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat12" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat12" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat12" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun12" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun12" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun12" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12 PM - 1 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time13[]" value="12:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time13" value="12:00" />
                                        <input type="hidden" name="time_of_use_end_time13" value="01:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays13" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays13" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays13" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat13" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat13" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat13" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun13" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun13" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun13" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1 PM - 2 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time14[]" value="01:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time14" value="13:00" />
                                        <input type="hidden" name="time_of_use_end_time14" value="02:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays14" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays14" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays14" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat14" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat14" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat14" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun14" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun14" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun14" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2 PM - 3 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time15[]" value="02:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time15" value="14:00" />
                                        <input type="hidden" name="time_of_use_end_time15" value="03:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays15" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays15" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays15" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat15" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat15" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat15" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun15" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun15" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun15" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3 PM - 4 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time16[]" value="03:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time16" value="15:00" />
                                        <input type="hidden" name="time_of_use_end_time16" value="04:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays16" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays16" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays16" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat16" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat16" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat16" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun16" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun16" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun16" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4 PM - 5 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time17[]" value="04:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time17" value="16:00" />
                                        <input type="hidden" name="time_of_use_end_time17" value="05:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays17" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays17" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays17" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat17" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat17" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat17" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun17" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun17" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun17" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5 PM - 6 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time18[]" value="05:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time18" value="17:00" />
                                        <input type="hidden" name="time_of_use_end_time18" value="06:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays18" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays18" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays18" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat18" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat18" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat18" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun18" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun18" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun18" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6 PM - 7 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time19[]" value="06:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time19" value="18:00" />
                                        <input type="hidden" name="time_of_use_end_time19" value="07:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays19" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays19" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays19" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat19" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat19" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat19" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun19" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun19" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun19" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7 PM - 8 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time20[]" value="07:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time20" value="19:00" />
                                        <input type="hidden" name="time_of_use_end_time20" value="08:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays20" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays20" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays20" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat20" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat20" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat20" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun20" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun20" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun20" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8 PM - 9 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time21[]" value="08:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time21" value="20:00" />
                                        <input type="hidden" name="time_of_use_end_time21" value="09:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays21" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays21" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays21" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat21" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat21" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat21" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun21" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun21" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun21" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9 PM - 10 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time22[]" value="09:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time22" value="21:00" />
                                        <input type="hidden" name="time_of_use_end_time22" value="10:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays22" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays22" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays22" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat22" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat22" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat22" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun22" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun22" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun22" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10 PM - 11 PM
                                        <!-- <input type="hidden" name="time_of_use_start_time23[]" value="10:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time23" value="22:00" />
                                        <input type="hidden" name="time_of_use_end_time23" value="11:00 PM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays23" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays23" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays23" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat23" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat23" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat23" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun23" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun23" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun23" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11 PM - 12 AM
                                        <!-- <input type="hidden" name="time_of_use_start_time24[]" value="11:00 PM" /> -->
                                        <input type="hidden" name="time_of_use_start_time24" value="23:00" />
                                        <input type="hidden" name="time_of_use_end_time24" value="12:00 AM" />
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_weekdays24" value="Peak" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays24" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_weekdays24" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sat24" value="Peak" id="option1_" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat24" value="Shoulder" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sat24" value="Off-Peak" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-toggle  ku-switch-toggle" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="time_of_use_sun24" value="Peak" id="option1_24" autocomplete="off" checked> Peak
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun24" value="Shoulder" id="option2_24" autocomplete="off"> Shoulder
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="time_of_use_sun24" value="Off-Peak" id="option3_24" autocomplete="off"> Off-Peak
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="time_of_use_save">Save</button>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link href="https://solarrun.com.au/solar-saving-calculator-staging/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<?php
    $annualBillSaving = $data['default']['annualBillSaving'];
    $annualEnergyProduction = $data['default']['averageDailyKWhPerMonth1'] + $data['default']['averageDailyKWhPerMonth2'] + $data['default']['averageDailyKWhPerMonth3'] + $data['default']['averageDailyKWhPerMonth4'] + $data['default']['averageDailyKWhPerMonth5'] + $data['default']['averageDailyKWhPerMonth6'] + $data['default']['averageDailyKWhPerMonth7'] + $data['default']['averageDailyKWhPerMonth8'] + $data['default']['averageDailyKWhPerMonth9'] + $data['default']['averageDailyKWhPerMonth10']+ $data['default']['averageDailyKWhPerMonth11'] + $data['default']['averageDailyKWhPerMonth12'];
    $savingsOvrTenYears = $data['default']['Benefit1'] + $data['default']['Benefit2'] + $data['default']['Benefit3'] + $data['default']['Benefit4'] + $data['default']['Benefit5'] + $data['default']['Benefit6'] + $data['default']['Benefit7'] + $data['default']['Benefit8'] + $data['default']['Benefit9'] + $data['default']['Benefit10'];
    
    $sumTotalFive1 = 0;
    $sumTotalFive2 = 0;
    $sumTotalFive3 = 0;
    $sumTotalFive4 = 0;

    $sumTotalSix1 = 0;
    $sumTotalSix2 = 0;
    $sumTotalSix3 = 0;
    $sumTotalSix4 = 0;

    $sumTotalTen1 = 0;
    $sumTotalTen2 = 0;
    $sumTotalTen3 = 0;
    $sumTotalTen4 = 0;

    $sumTotalFive1 = array_sum($data['five_system_size']['best_export_sol_prd_graph_data']);
    $sumTotalFive2 = array_sum($data['five_system_size']['best_export_export_graph_data']);
    $sumTotalFive3 = array_sum($data['five_system_size']['best_export_load_graph_data']);
    $sumTotalFive4 = array_sum($data['five_system_size']['best_export_offset_graph_data']);

    $sumTotalSix1 = array_sum($data['six_system_size']['best_export_sol_prd_graph_data']);
    $sumTotalSix2 = array_sum($data['six_system_size']['best_export_export_graph_data']);
    $sumTotalSix3 = array_sum($data['six_system_size']['best_export_load_graph_data']);
    $sumTotalSix4 = array_sum($data['six_system_size']['best_export_offset_graph_data']);

    $sumTotalTen1 = array_sum($data['ten_system_size']['best_export_sol_prd_graph_data']);
    $sumTotalTen2 = array_sum($data['ten_system_size']['best_export_export_graph_data']);
    $sumTotalTen3 = array_sum($data['ten_system_size']['best_export_load_graph_data']);
    $sumTotalTen4 = array_sum($data['ten_system_size']['best_export_offset_graph_data']);
?>
<style>
  .monthly-solar-production-outer table {
    width: 100% !important;
    margin: 0 auto;
  }
</style>
<div class="savings-calculator">
    <div class="savings-calculator-box estimated-box">
        <div class="bil-tital">
            <h4>Estimated solar saving</h4>
        </div>
        <div class="bil-form clearfix">
            <div class="est-box">
                <strong>Est. Yearly saving</strong>
                <span>$<?php echo number_format($annualBillSaving, 2); ?></span>
            </div>
            <div class="annual-box">
                <ul>
                    <li>
                        <span><i class="anu-icon"><img src="https://kugacrm.com.au/staging/assets/images/annual-icon.png" alt=""/></i> Annual energy production <strong><?php echo number_format($annualEnergyProduction, 2);?>kWh</strong></span>
                    </li>
                    <li>
                        <span><i class="anu-icon"><img src="https://kugacrm.com.au/staging/assets/images/annual-icon1.png" alt=""/></i> Greenhouse gas saved /year <strong>529 tonnes</strong></span>
                    </li>
                    <li>
                        <span><i class="anu-icon"><img src="https://kugacrm.com.au/staging/assets/images/annual-icon2.png" alt=""/></i> Savings over 10 years <strong>$<?php echo number_format($savingsOvrTenYears, 2); ?></strong></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="savings-calculator-box estimated-box">
        <div class="bil-tital">
            <h4>Your solar home daily production</h4> 
        </div>
        <div class="bil-form clearfix">
            <div class="daily-img-box">
                <script type='text/javascript'>
                     google.load('visualization', '1.0', {'packages':['corechart']});
                     google.setOnLoadCallback(drawChart);
                    function drawChart() {
                       var date = '02/03';
                       var data = google.visualization.arrayToDataTable([
                           ['Year', 'Load','Solar Output','Export','Offset Consumption'],
                           [' 0:00', <?php echo $data['default']['best_export_load_graph_data'][0]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][0]; ?>, <?php echo $data['default']['best_export_export_graph_data'][0]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][0]; ?>],
                           [' 1:00', <?php echo $data['default']['best_export_load_graph_data'][1]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][1]; ?>, <?php echo $data['default']['best_export_export_graph_data'][1]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][1]; ?>],
                           [' 2:00', <?php echo $data['default']['best_export_load_graph_data'][2]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][2]; ?>, <?php echo $data['default']['best_export_export_graph_data'][2]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][2]; ?>],
                           [' 3:00', <?php echo $data['default']['best_export_load_graph_data'][3]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][3]; ?>, <?php echo $data['default']['best_export_export_graph_data'][3]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][3]; ?>],
                           [' 4:00', <?php echo $data['default']['best_export_load_graph_data'][4]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][4]; ?>, <?php echo $data['default']['best_export_export_graph_data'][4]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][4]; ?>],
                           [' 5:00', <?php echo $data['default']['best_export_load_graph_data'][5]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][5]; ?>, <?php echo $data['default']['best_export_export_graph_data'][5]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][5]; ?>],
                           [' 6:00', <?php echo $data['default']['best_export_load_graph_data'][6]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][6]; ?>, <?php echo $data['default']['best_export_export_graph_data'][6]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][6]; ?>],
                           [' 7:00', <?php echo $data['default']['best_export_load_graph_data'][7]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][7]; ?>, <?php echo $data['default']['best_export_export_graph_data'][7]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][7]; ?>],
                           [' 8:00', <?php echo $data['default']['best_export_load_graph_data'][8]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][8]; ?>, <?php echo $data['default']['best_export_export_graph_data'][8]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][8]; ?>],
                           [' 9:00', <?php echo $data['default']['best_export_load_graph_data'][9]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][9]; ?>, <?php echo $data['default']['best_export_export_graph_data'][9]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][9]; ?>],
                           [' 10:00', <?php echo $data['default']['best_export_load_graph_data'][10]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][10]; ?>, <?php echo $data['default']['best_export_export_graph_data'][10]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][10]; ?>], 
                           [' 11:00', <?php echo $data['default']['best_export_load_graph_data'][11]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][11]; ?>, <?php echo $data['default']['best_export_export_graph_data'][11]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][11]; ?>],
                           [' 12:00', <?php echo $data['default']['best_export_load_graph_data'][12]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][12]; ?>, <?php echo $data['default']['best_export_export_graph_data'][12]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][12]; ?>],
                           [' 13:00', <?php echo $data['default']['best_export_load_graph_data'][13]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][13]; ?>, <?php echo $data['default']['best_export_export_graph_data'][13]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][13]; ?>],
                           [' 14:00', <?php echo $data['default']['best_export_load_graph_data'][14]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][14]; ?>, <?php echo $data['default']['best_export_export_graph_data'][14]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][14]; ?>],
                           [' 15:00', <?php echo $data['default']['best_export_load_graph_data'][15]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][15]; ?>, <?php echo $data['default']['best_export_export_graph_data'][15]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][15]; ?>],
                           [' 16:00', <?php echo $data['default']['best_export_load_graph_data'][16]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][16]; ?>, <?php echo $data['default']['best_export_export_graph_data'][16]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][16]; ?>],
                           [' 17:00', <?php echo $data['default']['best_export_load_graph_data'][17]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][17]; ?>, <?php echo $data['default']['best_export_export_graph_data'][17]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][17]; ?>],
                           [' 18:00', <?php echo $data['default']['best_export_load_graph_data'][18]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][18]; ?>, <?php echo $data['default']['best_export_export_graph_data'][18]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][18]; ?>],
                           [' 20:00', <?php echo $data['default']['best_export_load_graph_data'][19]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][19]; ?>, <?php echo $data['default']['best_export_export_graph_data'][19]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][19]; ?>],
                           [' 21:00', <?php echo $data['default']['best_export_load_graph_data'][20]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][20]; ?>, <?php echo $data['default']['best_export_export_graph_data'][20]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][20]; ?>],
                           [' 22:00', <?php echo $data['default']['best_export_load_graph_data'][21]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][21]; ?>, <?php echo $data['default']['best_export_export_graph_data'][21]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][21]; ?>],
                           [' 23:00', <?php echo $data['default']['best_export_load_graph_data'][22]; ?>, <?php echo $data['default']['best_export_sol_prd_graph_data'][22]; ?>, <?php echo $data['default']['best_export_export_graph_data'][22]; ?>, <?php echo $data['default']['best_export_offset_graph_data'][22]; ?>]
                          ]);

                        var options = {
                            width: 1000,
                            height: 400,
                            legend: { position: 'none', alignment: 'start' , textStyle: {fontSize: 10}},
                            hAxis:{title: '', titleTextStyle: { color: '#333' }, textPosition: 'none' },
                            vAxis:{gridlines: { color: 'transparent' }, textPosition: 'none' },
                            series: {
                                0: {
                                    color: '#FDBE31',
                                    areaOpacity: 1,
                                    type: 'area',
                                },
                                1: {
                                    color: '#004F60',
                                    areaOpacity: 1,
                                    type: 'area',
                                },
                                2: {
                                    color: '#575757',
                                    type: 'line',
                                    opacity: 1,
                                    pointSize: 9
                                },
                                3: {
                                    color: '#00AF98',
                                    areaOpacity: 1,
                                    type: 'area',
                                }
                            },
                        };
                        var chart = new google.visualization.AreaChart(document.getElementById('best_export_graph'));
                        chart.draw(data, options);
                      }
                  </script>
                  <div id="best_export_graph"></div>
                  <img src="https://kugacrm.com.au/staging/assets/images/daily-prod-legends.jpg" alt="" />
            </div>
            <div class="daily-text-box">
                <h6>What is your daily production calculation?</h6>
                <p>This solar saving calculator takes into account how much energy will be produced by your solar panel system and the typical energy usage of a household similar to your energy profile. Depending on your consumption profile, energy consumption is higher in the morning and peaks of at night when everyone is home. If you stay at home during the day, you can utilise a higher amount of energy produced from solar.</p>
            </div>
        </div>
    </div>
    <br />
    <br />
    <div class="savings-calculator-box estimated-box">
        <div class="bil-tital">
            <h4>System sizes comparison vs your energy usage</h4>
        </div>
        <div class="bil-form clearfix">
            <div class="daily-img-box">
                <div class="row">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <div >
                                        <script type='text/javascript'>
                                             google.load('visualization', '1.0', {'packages':['corechart']});
                                             google.setOnLoadCallback(drawChart);
                                            function drawChart() {
                                               var date = '02/03';
                                               var data = google.visualization.arrayToDataTable([
                                                   ['Year', 'Months',{ role: 'style' }],
                                                   ['Jan', <?php echo $sumTotalFive1; ?>,'#00D6BA'],
                                                   ['Feb', <?php echo $sumTotalFive2; ?>,'#00A994'],
                                                   ['Mar', <?php echo $sumTotalFive3; ?>,'#004B5E'],
                                                   ['Apr', <?php echo $sumTotalFive4; ?>,'#FFC233']
                                                ]);
                                                var options = {
                                                    width: 333,
                                                    height: 400,
                                                    legend: { position: 'none', alignment: 'start', textStyle: { fontSize: 10 } },
                                                    hAxis: { title: '', titleTextStyle: { color: '#333' }, textPosition: 'none' },
                                                    vAxis: { gridlines: { color: 'transparent' }, textPosition: 'none' }                                   
                                                };
                                                var chart = new google.visualization.ColumnChart(document.getElementById('fiveYearComp'));
                                                chart.draw(data, options);
                                              }
                                          </script>
                                          <div id="fiveYearComp"></div>
                                    </div>
                                </td>
                                <td>
                                    <div >
                                        <script type='text/javascript'>
                                             google.load('visualization', '1.0', {'packages':['corechart']});
                                             google.setOnLoadCallback(drawChart);
                                            function drawChart() {
                                               var date = '02/03';
                                               var data = google.visualization.arrayToDataTable([
                                                   ['Year', 'Months',{ role: 'style' }],
                                                   ['Jan', <?php echo $sumTotalSix1; ?>,'#00D6BA'],
                                                   ['Feb', <?php echo $sumTotalSix2; ?>,'#00A994'],
                                                   ['Mar', <?php echo $sumTotalSix3; ?>,'#004B5E'],
                                                   ['Apr', <?php echo $sumTotalSix4; ?>,'#FFC233']
                                                ]);
                                                var options = {
                                                    width: 333,
                                                    height: 400,
                                                    legend: { position: 'none', alignment: 'start', textStyle: { fontSize: 10 } },
                                                    hAxis: { title: '', titleTextStyle: { color: '#333' }, textPosition: 'none' },
                                                    vAxis: { gridlines: { color: 'transparent' }, textPosition: 'none' }
                                                };
                                                var chart = new google.visualization.ColumnChart(document.getElementById('sixYearComp'));
                                                chart.draw(data, options);
                                              }
                                          </script>
                                          <div id="sixYearComp"></div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <script type='text/javascript'>
                                             google.load('visualization', '1.0', {'packages':['corechart']});
                                             google.setOnLoadCallback(drawChart);
                                            function drawChart() {
                                               var date = '02/03';
                                               var data = google.visualization.arrayToDataTable([
                                                   ['Year', 'Months',{ role: 'style' }],
                                                   ['Jan', <?php echo $sumTotalTen1; ?>,'#00D6BA'],
                                                   ['Feb', <?php echo $sumTotalTen2; ?>,'#00A994'],
                                                   ['Mar', <?php echo $sumTotalTen3; ?>,'#004B5E'],
                                                   ['Apr', <?php echo $sumTotalTen4; ?>,'#FFC233']
                                                ]);
                                                var options = {
                                                    width: 333,
                                                    height: 400,
                                                    legend: { position: 'none', alignment: 'start', textStyle: { fontSize: 10 } },
                                                    hAxis: { title: '', titleTextStyle: { color: '#333' }, textPosition: 'none' },
                                                    vAxis: { gridlines: { color: 'transparent' }, textPosition: 'none' }                                    
                                                };
                                                var chart = new google.visualization.ColumnChart(document.getElementById('tenYearComp'));
                                                chart.draw(data, options);
                                              }
                                          </script>
                                          <div id="tenYearComp"></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <img src="https://kugacrm.com.au/staging/assets/images/size-comp-legends.jpg" alt="" />
                </div>
            </div>
            <div class="daily-text-box">
                <h6>Sizing different solar systems</h6>
                <p>When trying to see what size of solar panel system that will fit you best, it is ideal to get the system that can cover the most of your daily energy usage. Finding the sweet spot between a solar panel system that will produce enough energy but not too large that will make unaffordable for you to purchase. The solar saving calculator uses your baseline daily energy consumption measured against several different system sizes.</p>
            </div>
        </div>
    </div>
    <div class="savings-calculator-box estimated-box">
        <div class="bil-tital">
            <h4>Your monthly solar production</h4>
        </div>
        <div class="bil-form clearfix">
            <div class="daily-img-box">
                <script type='text/javascript'>
                     google.load('visualization', '1.0', {'packages':['corechart']});
                     google.setOnLoadCallback(drawChart);
                    function drawChart() {
                       var date = '02/03';
                       var data = google.visualization.arrayToDataTable([
                           ['Year', 'Months'],
                           ['Jan', <?php echo $data['default']['averageDailyKWhPerMonth1']; ?>],
                           ['Feb', <?php echo $data['default']['averageDailyKWhPerMonth2']; ?>],
                           ['Mar', <?php echo $data['default']['averageDailyKWhPerMonth3']; ?>],
                           ['Apr', <?php echo $data['default']['averageDailyKWhPerMonth4']; ?>],
                           ['May', <?php echo $data['default']['averageDailyKWhPerMonth5']; ?>],
                           ['Jun', <?php echo $data['default']['averageDailyKWhPerMonth6']; ?>],
                           ['Jul', <?php echo $data['default']['averageDailyKWhPerMonth7']; ?>],
                           ['Aug', <?php echo $data['default']['averageDailyKWhPerMonth8']; ?>],
                           ['Sep', <?php echo $data['default']['averageDailyKWhPerMonth9']; ?>],
                           ['Oct', <?php echo $data['default']['averageDailyKWhPerMonth10']; ?>],
                           ['Nov', <?php echo $data['default']['averageDailyKWhPerMonth11']; ?>],
                           ['Dec', <?php echo $data['default']['averageDailyKWhPerMonth12']; ?>]
                        ]);

                        var options = {
                            width: 1000,
                            height: 400,
                            legend: { position: 'none', alignment: 'start', textStyle: { fontSize: 10 } },
                            series: {
                                0: {
                                    color: '#f3ae15',
                                    type: 'area',
                                    opacity: 1,
                                    pointSize: 9
                                },
                                1: {
                                    areaOpacity: 1,
                                    type: 'line',
                                }
                            },
                        };
                        var chart = new google.visualization.AreaChart(document.getElementById('monthlySolarGraph'));
                        chart.draw(data, options);
                      }
                  </script>
                  <div id="monthlySolarGraph"></div>
                  <div class="monthly-solar-production-outer">
                        <table width="100%">
                            <tbody>
                                <tr>
                                  <th>Month</th>
                                    <th>Electricity Output(kWh)</th>
                                                                        
                                </tr>
                                <tr class="electricity-output">
                                    <td>Jan</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth1'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Feb</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth2'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Mar</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth3'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Apr</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth4'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>May</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth5'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Jun</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth6'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Jul</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth7'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Aug</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth8'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Sep</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth9'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Oct</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth10'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Nov</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth11'], 2); ?></td>                                    
                                </tr><tr class="electricity-output">
                                    <td>Dec</td>
                                    <td><?php echo number_format($data['default']['averageDailyKWhPerMonth12'], 2); ?></td>                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>

            </div>
            <div class="monthlySolarGraph">
                <ul>
                    <li>
                        <span>Est. Annual Generated Output <strong><?php echo number_format($annualEnergyProduction, 2).'kWh'; ?></strong></span>
                    </li>
                    <li>
                        <span>Savings over 10 years <strong>$<?php echo number_format($savingsOvrTenYears, 2); ?></strong></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table width="828" border="1" bordercolor="#e8e8e8" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:solid 1px #e8e8e8">
      <tr>
        <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">&nbsp;</th>
        <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 1</th>
        <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 2</th>
        <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Area 3</th>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Space Type:</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please Select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please Select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please Select</strong></td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Celling Height:</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>2400mm</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">Zone Classification</th>
        <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Reach Up:</td>
              </tr>
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Ceiling Height)</td>
              </tr>
            </table>
        </th>
        <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Clearance:</td>
              </tr>
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Height of item blocking access)</td>
              </tr>
            </table>
        </th>
        <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff">Reach Across:</td>
              </tr>
              <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff">(Distance from the alsle to light)</td>
              </tr>
            </table>
        </th>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">Boom Requirements:</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
      <tr>
        <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Zone Classificatoin</th>
        <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Description of Upgrade product</th>
        <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
        <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
        <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Office</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Panel - 1200x300mm 5000k</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>76</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">0</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>0</strong></td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Please select</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" colspan="4" style="padding:5px;">
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="55%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Further Products quoted can be found on a separate sheet </td>
            <td width="20%"><img src="checkbox.jpg"/></td>
            <td width="25%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Sub Total exc GST:</td>
          </tr>
        </table></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$0.00</strong></td>
      </tr>
      <tr>
        <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Access Equipment</th>
        <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Detailed Description of Access Equipment</th>
        <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Quantity</th>
        <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
        <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>Less than 4m</strong></td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" colspan="4" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Sub Total exc GST:</td>
        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$0.00</strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
        <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
        <td width="156" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$0.00</strong></td>
      </tr>
      <tr>
        <td align="left" valign="top">
        	<table width="557" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs PANEL</strong> booking form?</td>
                <td width="212"><img src="checkbox.jpg"/></td>
              </tr>
        	</table>
        </td>
        <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
        <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><table width="557" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs BATTEN</strong> booking from?</td>
            <td width="212" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><img src="checkbox.jpg"/></td>
          </tr>
        </table></td>
        <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
        <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px"><strong>$0.00</strong></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>

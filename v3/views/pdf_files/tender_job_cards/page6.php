<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Job Card</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            
            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=checkbox]:checked:before {
                color: black !important;
            }
        
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        
        </style>
    </head>
<body style="padding:0; margin:0">
<table class="pdf_page hidden" id="page_6" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <?php echo isset($job_card_details->first_heading) ? $job_card_details->first_heading : '' ?>
                                </th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">
                                    <?php echo isset($job_card_details->second_heading) ? $job_card_details->second_heading : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <?php if (isset($job_card_details->connection_point_photo) && $job_card_details->connection_point_photo != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->connection_point_photo; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <?php if (isset($job_card_details->main_switch_board) && $job_card_details->main_switch_board != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->main_switch_board; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <?php echo isset($job_card_details->third_heading) ? $job_card_details->third_heading : '' ?>
                                </th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">
                                    <?php echo isset($job_card_details->fourth_heading) ? $job_card_details->fourth_heading : '' ?>
                                </th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->additional_eci_one) && $job_card_details->additional_eci_one != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->additional_eci_one; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->additional_eci_two) && $job_card_details->additional_eci_two != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->additional_eci_two; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="280">&nbsp;</td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td valign="bottom" style="position:relative;bottom:-2px;"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
</body>
</html>
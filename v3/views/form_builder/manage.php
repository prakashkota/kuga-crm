<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
       <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php }else { ?> Franchise - <?php } ?>Manage Form Templates</h1>
    </div>
    <!-- BEGIN: message  -->	
    <?php $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="franchise_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($templates))
                                        foreach ($templates as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo ($key + 1); ?></td>
                                                <td><?php echo $value['title']; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="<?php echo site_url(); ?>admin/form-builder/edit/<?php echo $value['id']; ?>"><i class="icon-pencil"></i> Edit</a>
                                                            <!--<a class="dropdown-item" href="<?php echo site_url(); ?>admin/franchise/change-password/<?php echo $value['user_id']; ?>"><i class="icon-pencil"></i> Change Password</a> -->
                                                            <!--<a class="dropdown-item" href="productTypeManage.php?id=<?php echo $value['id']; ?>&action=delete"><i class="icon-trash"></i> Delete</a>-->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#franchise_list').DataTable();
    });
</script>
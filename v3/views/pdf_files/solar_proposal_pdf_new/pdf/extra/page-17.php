<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page17 .back-bg {
		background-color: white;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page17 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 630px;
		padding-top: 30px;
	}
	
	.page17 .top hr {
		width: 70%;
		height: 2px;
		margin: -24px 0px 0px;
		background-color: #D01E2A;
	}
	
	.page17 .text_color {
		color: #D01E2A;
	}
	
	.page17 .vkcontainer {
		margin: 0 30px;
	}
	
	.page17 .title h1 {
		padding-left: 14px;
		margin-top: 40px;
		border-left: 5px solid #D01E2A;
	}
	
	.page17 .vktitle h2 {
		font-weight: 500;
		letter-spacing: 1px;
		padding: 12px;
		margin-top: 15px;
		background-color: #D01E2A;
		width: 100%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page17 table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}
	
	.page17 tr {
		border-bottom: 1px solid #cac8c8;
	}
	
	.page17 td {
		padding: 15px;
		color: #3D3B3C;
	}
	
	.page17 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page17 .image img {
		margin-top: -50px;
	}
	
	.page17 .fimagee img {
		margin-top: -27px;
		margin-left: 650px;
		width: 230;
	}
	
	.page17 .vk-innerbox {
		width: 33%;
		float: left;
		text-align: center;
	}
	
	.page17 .vk-innerbox h4 {
		margin: 0px;
	}
	
	.page17 .vkid-section {
		padding-top: 40px;
		margin-bottom: 60px;
	}
	
	.page17 .vktitle h2 span {
		background: #fff;
		padding: 2px 4px;
		margin-right: 15px;
		color: #fff;
		font-size: 12px;
	}
	
	.page17 .vksaving-box tr {
		border: none;
	}
	
	.page17 .rightvkbx h3 {
		font-weight: 600;
		letter-spacing: 3px;
		padding: 12px;
		margin-top: 15px;
		background-color: #D01E2A;
		width: 100%;
		color: #ffffff;
		text-align: center;
	}
	
	.page17 .finacetable tr {
		border-bottom: 2px solid #D01E2A;
	}
	
	.page17 .finacetable tr th {
		text-align: left;
		font-size: 20px;
	}
	
	.page17 .finacetable tr td {
		text-align: right;
		font-size: 20px;
	}
	
	.page17 .finacetable tr th span {
		background: #D01E2A;
		padding: 4px 7px;
		margin-right: 15px;
		color: #D01E2A;
		font-size: 12px;
		border-radius: 50%;
	}
	
	.page17 .footertext {
		margin: 50px 100px 0;
		padding-bottom: 38px;
	}
	
	.page17 .vksaving-box {
		margin-left: 100px;
		margin-right: 100px;
	}
	
	.page17 .vkftrm {
		margin: 0 30px;
	}
	</style>
</head>

<body>
	<div class="back-bg page17">
		<div class="vkcontainer">
			<div class="top vk-header" style="width:100%;posi">
				<div class="headertext">
					<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
					</div>
					<hr>
				</div>
				<div class="title">
					<h1 style="text-transform:uppercase;">FINANCIAL  <span class="text_color"> SUMMARY</span></h1>
					
				</div>
				
				<div class="vktitle" style="padding-top:40px;">
					<h2 style="text-transform:uppercase; font-size:20px;width: 52%;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon1.png'); ?>" style="width: 40px;">
					    <b style="vertical-align: super;"> OUTRIGHT PURCHASE</b>
					</h2> </div>
				<div class="vkbargraph">
					<table>
						<tr style="border:none;">
							<td><img src="<?php echo site_url('assets/solar_proposal_pdf_new/img1.png'); ?>" width="100%"></td>
							<td><img src="<?php echo site_url('assets/solar_proposal_pdf_new/img1.png'); ?>" width="100%"></td>
						</tr>
					</table>
				</div>
				<div class="vksaving-box" style="margin-top:30px;">
					<table>
						<tr>
							<th>SAVINGS PER YEAR</th>
							<th>RETURN OF INVESTMENT</th>
						</tr>
					</table>
				</div>
				<div class="vksaving-box" style="margin-top:20px;">
					<table>
						<tr>
							<td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;width: 50%;">
								<h3 style="margin: 0;">$50,700</h3></td>
							<td class="rightvkbx" style="padding:0px 15px;">
								<h3 style="margin: 0;">6.4c/kWh</h3></td>
						</tr>
					</table>
				</div>
				<!-- option 2 -->
				<div class="vktitle">
					<h2 style="text-transform:uppercase; font-size:20px;width: 30%;margin-top:30px;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon2.png'); ?>" style="width: 40px;">
					    <b style="vertical-align: super;"> FINANCE </b>
				    </h2> </div>
				<div class="vkftrm">
					<table class="finacetable">
						<tr>
							<th><span>0</span> FINANCE TERMS:</th>
							<td>$70,123</td>
						</tr>
						<tr>
							<th><span>0</span> MONTHLY PAYMENT:</th>
							<td>$70,123</td>
						</tr>
						<tr>
							<th><span>0</span> ANNUAL CASHFLOW:</th>
							<td>$70,123</td>
						</tr>
					</table>
				</div>
				<div class="vksaving-box" style="margin-top:50px;">
					<table>
						<tr>
							<th>TOTAL STC FINANCIAL INCENTIVE</th>
							<th style="color:#D01E2A">10 YEARS SOLAR ENERGY RATE</th>
						</tr>
					</table>
				</div>
				<div class="vksaving-box" style="margin-top:20px;">
					<table>
						<tr style="margin-top:15px;">
							<td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid; width: 50%;">
								<h3 style="margin: 0;">$50,700</h3></td>
							<td class="rightvkbx" style="padding:0px 15px;">
								<h3 style="margin: 0;">6.4c/kWh</h3></td>
						</tr>
					</table>
				</div>
				<div class="footertext">
					<p>Total revenue includes potential LGC and STC savings, STC and LGC savings are determined by differing market conditions</p>
					<p>Monthly payment of $3,165 with 7 years payment terms, calculated with 5% average annual increase in energy cost from energy provider</p>
				</div>
			</div>
			<div class="fimagee" style="margin-bottom: -66px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
			<div class="footer"></div>
		</div>
</body>

</html>
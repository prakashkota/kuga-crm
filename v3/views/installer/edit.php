<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php }else { ?> Franchise - <?php } ?>Edit Installer</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Account Details</h4>
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" id="email"  class="form-control" placeholder="Email" value="<?php echo (isset($email)) ? $email : ''; ?>"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                        <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Location Details</h4>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label class="control-label">Address</label>
                                                <select class="select2 placecomplete form-control" id="locationstreetAddress"  style="height:48px !important;"></select>
                                                <input type="hidden" id="locationstreetAddressVal" name="user_locations[address]" value="<?php echo (!empty($user_locations)) ? $user_locations['address'] : ''; ?>" />
                                                <?php echo form_error('user_locations[address]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">State</label>
                                                <select name="user_locations[state_id]" id="locationState" class="form-control" required>
                                                    <option value="">Select State</option>
                                                    <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                        <option value="<?php echo $states[$j]['state_id']; ?>" <?php if (!empty($user_locations) && $user_locations['state_id'] == $states[$j]['state_id']) { ?> selected="" <?php } ?>><?php echo $states[$j]['state_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('user_locations[state_id]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">Post Code</label>
                                                <input class="form-control" placeholder="Please Enter Post Code" type="text" name="user_locations[postcode]" value="<?php echo (!empty($user_locations)) ? $user_locations['postcode'] : ''; ?>" id="locationPostCode" value=""  required />
                                                <?php echo form_error('user_locations[postcode]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Warehouse Location Details</h4>
                                    <input type="hidden" name="user_other_locations[id]" value="<?php echo (!empty($user_other_locations)) ? $user_other_locations['id'] : ''; ?>" />
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label >Address</label>
                                                <select class="other_location_placecomplete form-control" id="other_location_address"  style="height:48px !important;"></select>
                                                <input type="hidden" id="other_location_address_val" name="user_other_locations[address]" value="<?php echo (!empty($user_other_locations)) ? $user_other_locations['address'] : ''; ?>" />
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label >State</label>
                                                <select name="user_other_locations[state_id]" id="other_location_state" class="form-control" >
                                                    <option value="">Select State</option>
                                                    <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                        <option value="<?php echo $states[$j]['state_id']; ?>" <?php if (!empty($user_other_locations) && $user_other_locations['state_id'] == $states[$j]['state_id']) { ?> selected="" <?php } ?>><?php echo $states[$j]['state_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label>Post Code</label>
                                                <input class="form-control" placeholder="Please Enter Post Code" type="text" name="user_other_locations[postcode]" value="<?php echo (!empty($user_other_locations)) ? $user_other_locations['postcode'] : ''; ?>" id="other_location_postcode" value=""  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Company Details (Settings)</h4>
                                    
                                    <div class="form-group">
                                        <label>Franchise Name</label>
                                        <input type="text" value="<?php echo (!empty($franchise)) ? $franchise[0]['full_name'] : ''; ?>"  class="form-control" readonly >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Installer Full Name</label>
                                        <input type="text" name="user_details[full_name]" id="full_name"  class="form-control" value="<?php echo (!empty($user_details)) ? $user_details['full_name'] : ''; ?>" placeholder="Installer Full Name"  >
                                        <?php echo form_error('user_details[full_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Comapny Name</label>
                                        <input type="text" name="user_details[company_name]"  value="<?php echo (!empty($user_details)) ? $user_details['company_name'] : ''; ?>" class="form-control" placeholder="Company Name" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Trading Name</label>
                                        <input type="text" name="user_details[company_trading_name]"  value="<?php echo (!empty($user_details)) ? $user_details['company_trading_name'] : ''; ?>" class="form-control" placeholder="Trading Name" >
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" name="user_details[company_contact_no]" id="mobilePhone" value="<?php echo (!empty($user_details)) ? $user_details['company_contact_no'] : ''; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
                                         <?php echo form_error('user_details[company_contact_no]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Secondary Contact No.</label>
                                        <input type="text" name="user_details[company_alt_contact_no]" value="<?php echo (!empty($user_details)) ? $user_details['company_alt_contact_no'] : ''; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea  name="user_details[notes]"  class="form-control" ><?php echo (!empty($user_details)) ? $user_details['notes'] : ''; ?></textarea>
                                    </div>

                                     <div class="form-group">
                                        <label>ABN</label>
                                        <input type="text" name="user_details[company_abn]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_abn'] : ''; ?>" class="form-control" placeholder="ABN" >
                                    </div>

                                     <?php /** <div class="form-group">
                                        <label>BSB</label>
                                        <input type="text" name="user_details[company_bsb]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_bsb'] : ''; ?>" class="form-control" placeholder="Company BSB" >
                                    </div>

                                    <div class="form-group">
                                        <label>Account Number</label>
                                        <input type="text" name="user_details[company_account]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_account'] : ''; ?>" class="form-control" placeholder="Company Account Number" >
                                    </div>

                                    <div class="form-group">
                                        <label>STC Price Asumption</label>
                                        <input type="text" name="user_details[stc_price_assumption]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['stc_price_assumption'] : ''; ?>" class="form-control" placeholder="Stc Price Asumption" >
                                    </div>

                                    <div class="form-group">
                                        <label>Certificate Margin</label>
                                        <input type="text" name="user_details[certificate_margin]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['certificate_margin'] : ''; ?>" class="form-control" placeholder="Certificate Margin" >
                                    </div> **/ ?>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">License Details</h4>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default">
                                            <table width="100%" border="0">
                                                <thead>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Reference</td>
                                                        <td>Expiry Date</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="license_items_wrapper">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button id="add_license_items" type="button" class="btn btn-primary" >Add License </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="buttwrap">
                        <input type="submit" id="addNewCompanyBtn" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry,drawing"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

<!-- Main Location Autocomplete -->
<script>
  $(document).ready(function () {
        

        $('.placecomplete').placecomplete({
            placeServiceResult: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                    }
                                }
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                            }
                        }
                    }
                }
            },
            language: 'fr'
        });
    });
    
    (function ($) {

        if (typeof $.fn.select2 == 'undefined') {
            //alert("ERROR: Placecomplete need Select2 plugin.");
        }
        setTimeout(function () {
            var location_address = "<?php echo (!empty($user_locations)) ? $user_locations['address'] : ''; ?>";
            $('#select2-locationstreetAddress-container').html(location_address);
        }, 2000);


        //Google services
        var ac = null; //Autocomplete
        var ps = null; //Place

        //Google config
        // https://developers.google.com/maps/documentation/javascript/reference#AutocompletionRequest
        var googleAutocompleteOptions = {
            types: ['establishment', 'geocode'],
            componentRestrictions: {country: 'au'},
        };

        //Google init
        window.initGoogleMapsAPI = function () {
            ac = new google.maps.places.AutocompleteService();
            ps = new google.maps.places.PlacesService($('<div/>')[0]); //Google need a mandatory element to pass html result , we do not need this.

        }

        //Google Loading
        if (window.google && google.maps && google.maps.places) {
            window.initGoogleMapsAPI();
        } else {
            $.ajax({
                url: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry,drawing",
                dataType: "script",
                cache: true
            });
        }

        //Google placeservice result map

        var placeServiceResult = function (data, status) {
            var CIVIC = 0, STREET = 1, CITY = 2, SECTEUR = 3, STATE = 4, COUNTRY = 5, ZIPCODE = 6;
            //todo If the result does not have 7 element data map is not the same
            //maybe we will need that html element google put mandatory
            var adrc = data.address_components;
            if (adrc.length != 7)
                return;

            var address = adrc[CIVIC].long_name + ',' + adrc[STREET].long_name + adrc[CIVIC].long_name + ',' + adrc[STREET].long_name

            $('.address input.address').val(adrc[CIVIC].long_name + ' ' + adrc[STREET].long_name);
            $('.address input.city').val(adrc[CITY].long_name);
            $('.address input.state').val(adrc[STATE].long_name);
            $('.address input.country').val(adrc[COUNTRY].long_name);
            $('.address input.zipcode').val(adrc[ZIPCODE].long_name);
        }

        //Select2 default options
        var select2DefaultOptions = {
            closeOnSelect: true,
            debug: false,
            dropdownAutoWidth: false,
            //escapeMarkup: Utils.escapeMarkup,
            language: 'en',
            minimumInputLength: 2,
            maximumInputLength: 0,
            maximumSelectionLength: 0,
            minimumResultsForSearch: 0,
            selectOnClose: false,
            selectOnBlur: true,
            theme: 'default',
            width: '100%',
            placeholder: {
                id: '-1', // the value of the option
                text: 'Search for address'
            },
            ajax: {
                delay: 100
            },
        };

        //jQuery Plugin
        var pluginDefault = {
            placeServiceResult: placeServiceResult
        }
        $.fn.placecomplete = function (options) {
            this.each(function () {
                //Variable by instance
                var $s2 = $(this);
                //Init select2 for $this
                $s2.select2($.extend(true, {
                    ajax: {
                        transport: function (params, success, failure) {

                            // is caching enabled?
                            //TODO(sébastien) ajouter le cache pour google autocomplete
                            if ($s2.data('ajax--cache')) {

                            } else {
                                ac.getPlacePredictions($.extend(googleAutocompleteOptions, params.data), success);
                            }
                        },
                        data: function (params) {
                            return {input: params.term};
                        },
                        processResults: function (data, status) {
                            var response = {results: []}
                            if (data != undefined && data != null) {
                                $.each(data, function (index, item) {
                                    item.text = item.description;
                                    response.results.push(item)
                                });
                            } else {
                                var item = {};
                                item.id = status.term;
                                item.place_id = status.term;
                                item.text = status.term;
                                response.results.push(item)
                            }
                            return response;
                        },
                    }
                }, select2DefaultOptions, options || {}));

                options = $.extend(true, pluginDefault, options);
                $s2.on('select2:select', function (evt) {
                    ps.getDetails({placeId: evt.params.data.place_id}, options.placeServiceResult);
                });
            });
            return this;
        };
    })(jQuery);
</script>

<!-- Other Location Autocomplete -->

<script>
        $('.other_location_placecomplete').placecomplete({
            placeServiceResult: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    //Set Address
                    if ($('#other_location_address_val') != undefined) {
                        $('#other_location_address_val').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            if ($('#other_location_state') != undefined) {
                                var select = document.getElementById("other_location_state");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                    }
                                }
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#other_location_postcode') != undefined) {
                                $('#other_location_postcode').val(address_components[i].long_name);
                            }
                        }
                    }
                }
            },
            language: 'fr'
        });



    (function ($) {

        if (typeof $.fn.select2 == 'undefined') {
            //alert("ERROR: Placecomplete need Select2 plugin.");
        }
        setTimeout(function () {
            var location_address = "<?php echo (!empty($user_other_locations)) ? $user_other_locations['address'] : ''; ?>";
            $('#select2-other_location_address-container').html(location_address);
        }, 2000);


        //Google services
        var ac = null; //Autocomplete
        var ps = null; //Place

        //Google config
        // https://developers.google.com/maps/documentation/javascript/reference#AutocompletionRequest
        var googleAutocompleteOptions = {
            types: ['establishment', 'geocode'],
            componentRestrictions: {country: 'au'},
        };

        //Google init
        window.initGoogleMapsAPI = function () {
            ac = new google.maps.places.AutocompleteService();
            ps = new google.maps.places.PlacesService($('<div/>')[0]); //Google need a mandatory element to pass html result , we do not need this.

        }

        //Google Loading
        if (window.google && google.maps && google.maps.places) {
            window.initGoogleMapsAPI();
        } else {
            $.ajax({
                url: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry,drawing",
                dataType: "script",
                cache: true
            });
        }

        //Google placeservice result map

        var placeServiceResult1 = function (data, status) {
            var CIVIC = 0, STREET = 1, CITY = 2, SECTEUR = 3, STATE = 4, COUNTRY = 5, ZIPCODE = 6;
            //todo If the result does not have 7 element data map is not the same
            //maybe we will need that html element google put mandatory
            var adrc = data.address_components;
            if (adrc.length != 7)
                return;

            var address = adrc[CIVIC].long_name + ',' + adrc[STREET].long_name + adrc[CIVIC].long_name + ',' + adrc[STREET].long_name

            $('.address input.address').val(adrc[CIVIC].long_name + ' ' + adrc[STREET].long_name);
            $('.address input.city').val(adrc[CITY].long_name);
            $('.address input.state').val(adrc[STATE].long_name);
            $('.address input.country').val(adrc[COUNTRY].long_name);
            $('.address input.zipcode').val(adrc[ZIPCODE].long_name);
        }

        //Select2 default options
        var select2DefaultOptions = {
            closeOnSelect: true,
            debug: false,
            dropdownAutoWidth: false,
            //escapeMarkup: Utils.escapeMarkup,
            language: 'en',
            minimumInputLength: 2,
            maximumInputLength: 0,
            maximumSelectionLength: 0,
            minimumResultsForSearch: 0,
            selectOnClose: false,
            selectOnBlur: true,
            theme: 'default',
            width: '100%',
            placeholder: {
                id: '-1', // the value of the option
                text: 'Search for address'
            },
            ajax: {
                delay: 100
            },
        };

        //jQuery Plugin
        var pluginDefault = {
            placeServiceResult: placeServiceResult1
        }
        $.fn.placecomplete = function (options) {
            this.each(function () {
                //Variable by instance
                var $s21 = $(this);
                //Init select2 for $this
                $s21.select2($.extend(true, {
                    ajax: {
                        transport: function (params, success, failure) {

                            // is caching enabled?
                            //TODO(sébastien) ajouter le cache pour google autocomplete
                            if ($s21.data('ajax--cache')) {

                            } else {
                                ac.getPlacePredictions($.extend(googleAutocompleteOptions, params.data), success);
                            }
                        },
                        data: function (params) {
                            return {input: params.term};
                        },
                        processResults: function (data, status) {
                            var response = {results: []}
                            if (data != undefined && data != null) {
                                $.each(data, function (index, item) {
                                    item.text = item.description;
                                    response.results.push(item)
                                });
                            } else {
                                var item = {};
                                item.id = status.term;
                                item.place_id = status.term;
                                item.text = status.term;
                                response.results.push(item)
                            }
                            return response;
                        },
                    }
                }, select2DefaultOptions, options || {}));

                options = $.extend(true, pluginDefault, options);
                $s21.on('select2:select', function (evt) {
                    ps.getDetails({placeId: evt.params.data.place_id}, options.placeServiceResult);
                });
            });
            return this;
        };
    })(jQuery);
</script>

<script>
    var license_item_data = function (options) {
        var self = this;
        this.userid = (options.userid && options.userid != '') ? options.userid : '';
        this.license_items_wrapper = 'license_items_wrapper'
        this.license_items_btn = 'add_license_items';
        
        if (options.userid && options.userid != '') {
            self.load_item_data();
        }

        $('form button').on("click", function (e) {
            e.preventDefault();
        });

        self.create_license_items();
        self.remove_license_items();
    };

    license_item_data.prototype.load_item_data = function () {
        var self = this;
        var userid = self.userid;
        var form_data = 'userid=' + userid;
        $.ajax({
            url: base_url + 'admin/franchise/installer/fetch_license_data',
            type: 'get',
            data: form_data,
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    if(response.license_data.length < 2){
                        var license_data = [];
                        var license_data_obj1 = {};
                        var license_data_obj2 = {};
                        var license_data_obj3 = {};
                        license_data_obj1.lic_expiry_date = ""
                        license_data_obj1.lic_reference = ""
                        license_data_obj1.lic_name = "REC";
                        license_data.push(license_data_obj1);
                        license_data_obj2.lic_expiry_date = ""
                        license_data_obj2.lic_reference = ""
                        license_data_obj2.lic_name = "CEC";
                        license_data.push(license_data_obj2);
                        license_data_obj3.lic_expiry_date = ""
                        license_data_obj3.lic_reference = ""
                        license_data_obj3.lic_name = "A Grade License";
                        license_data.push(license_data_obj3);
                        for (var i = 0; i < license_data.length; i++) {
                            self.create_license_items(license_data[i]);
                        }
                    }
                    for (var i = 0; i < response.license_data.length; i++) {
                        self.create_license_items(response.license_data[i]);
                    }
                } 
            }
        });
    };
    
    license_item_data.prototype.datepicker_script = function (date) {
        var start_date = (date !== undefined && date != '') ? moment(date).format('DD/MM/YYYY') : new Date();
        var script = document.createElement('script');
        script.innerHTML = "$('.datepicker').datepicker({format: 'dd/mm/yyyy', minDate:new Date(),startDate : new Date(),todayHighlight : true});"
                           +"$('.datepicker').datepicker('setDate', '"+start_date+"');";
        return script;
    }
    
    license_item_data.prototype.create_license_items = function (data) {
        var self = this;
        var license_items_wrapper = document.getElementById(self.license_items_wrapper);
        var license_items_btn = document.getElementById(self.license_items_btn);
        if (!data) {
            license_items_btn.addEventListener("click", function (e) { //on add input button click
                e.preventDefault();
                var datepicker_script = self.datepicker_script();
                $(license_items_wrapper).append('<tr>'
                        + '<td><input type="text" class="form-control" name="license_item_data[lic_name][]" value="" /></td>'
                        + '<td><input type="text" class="form-control" name="license_item_data[lic_reference][]" value="" /></td>'
                        + '<td><div class="input-group date" ><input type="text" class="form-control datepicker" name="license_item_data[lic_expiry_date][]" value="" /></div></td>'
                        + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
                license_items_wrapper.appendChild(datepicker_script);
            });
        } else {
            var datepicker_script = self.datepicker_script(data.lic_expiry_date);
            $(license_items_wrapper).append('<tr>'
                    + '<td><input type="text" class="form-control" name="license_item_data[lic_name][]" value="' + data.lic_name + '" /></td>'
                    + '<td><input type="text" class="form-control" name="license_item_data[lic_reference][]" value="' + data.lic_reference + '" /></td>'
                    + '<td><div class="input-group date" ><input type="text" class="form-control datepicker" name="license_item_data[lic_expiry_date][]" value="' + data.lic_expiry_date + '" /></div></td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            license_items_wrapper.appendChild(datepicker_script);
        }
    

    };


    license_item_data.prototype.remove_license_items = function () {
        var self = this;
        var license_items_wrapper = document.getElementById(self.license_items_wrapper);
        $(license_items_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
            e.preventDefault();
            $(this).parent().parent('tr').remove();
        });
    };


    var context = {};
    context.userid = "<?php echo (isset($userid) && $userid != '') ? $userid : '' ?>";
    var ltd = new license_item_data(context);
</script>
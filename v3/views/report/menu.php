<div class="page-wrapper d-block clearfix">
    <div class="row">
        <?php if ($this->aauth->is_member('Admin')) { ?>
            
            <div class="col-sm-3 hidden">
                <a href="<?php echo site_url(); ?>admin/reports?type=simpro" class="tile blue ">
                    <div class="first">
                        <h3 class="title">Simpro Sales Reports</h3>
                        <small>All the sales related reports from simpro data</small>
                    </div>
                    <div class="second">
                        <img src="https://alternative.me/icons/simpro.jpg"/>
                    </div>
                </a>
            </div>

            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=sales" class="tile purple">
                    <div class="first">
                        <h3 class="title">KugaCRM Sales Reports</h3>
                        <small>All the sales related reports</small>
                    </div>
                    <div class="second">
                        <img src="<?php echo site_url(); ?>assets/images/logo.png"/>
                    </div>
                </a>
            </div>
        
           

            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/dashboard" class="tile purple">
                    <div class="first">
                        <h3 class="title">CSM Reports</h3>
                        <small>All CSM related reports</small>
                    </div>
                    <div class="second">
                        <img src="<?php echo site_url(); ?>assets/images/logo.png"/>
                    </div>
                </a>
            </div>

             <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=lead_allocation_report" class="tile purple">
                    <div class="first">
                        <h3 class="title">Lead Alloc. Reports</h3>
                        <small>All the lead allocation related reports</small>
                    </div>
                    <div class="second">
                        <img src="<?php echo site_url(); ?>assets/images/logo.png"/>
                    </div>
                </a>
            </div>
            
            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=facebook" class="tile purple">
                    <div class="first">
                        <h3 class="title">Leads Reports</h3>
                        <small>All the facebook and web lead allocation related reports</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-2x fa-facebook"></i>
                    </div>
                </a>
            </div>

        <?php } else if ($this->components->is_sales_team_leader()) { ?>
            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=sales" class="tile purple">
                    <div class="first">
                        <h3 class="title">Sales Reports</h3>
                        <small>All the sales related reports</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-area-chart fa-3x"></i>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=facebook" class="tile purple">
                    <div class="first">
                        <h3 class="title">Leads Reports</h3>
                        <small>All the facebook and web lead allocation related reports</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-2x fa-facebook"></i>
                    </div>
                </a>
            </div>
        <?php } else if ($this->components->is_lead_allocation_team_leader() || $this->components->is_lead_allocation_rep()) { ?>
            <div class="col-sm-3">
                <a href="<?php echo site_url(); ?>admin/reports?type=lead_allocation_report" class="tile purple">
                    <div class="first">
                        <h3 class="title">Lead Allocation Reports</h3>
                        <small>All the lead allocation related reports</small>
                    </div>
                    <div class="second">
                        <i class="fa fa-area-chart fa-3x"></i>
                    </div>
                </a>
            </div>
          
        <?php  } ?>
    </div>
</div>
<style>
    .tile {
        width: 100%;
        display: inline-block;
        box-sizing: border-box;
        background: #fff;
        padding: 20px;
        margin-bottom: 10px;
        border-radius: 5px;
    }
    .tile .first {
        width: 80%;
        float: left;
    }
    .tile .second {
        width: 20%;
        float: left;
        text-align: center;
    }
    .tile .title {
        margin-top: 0px;
    }
    .tile.purple, .tile.blue, .tile.red, .tile.orange, .tile.green {
        color: #fff !important;
    }
    .tile.purple {
        background: #5133ab;
    }
    .tile.purple:hover {
        background: #3e2784;
    }
    .tile.red {
        background: #ac193d;
    }
    .tile.red:hover {
        background: #7f132d;
    }
    .tile.green {
        background: #00a600;
    }
    .tile.green:hover {
        background: #007300;
    }
    .tile.blue {
        background: #2672ec;
    }
    .tile.blue:hover {
        background: #125acd;
    }
    .tile.orange {
        background: #dc572e;
    }
    .tile.orange:hover {
        background: #b8431f;
    }

</style>
<style>
    @font-face {
        font-family: 'CenturyGothic';
        font-weight: normal;
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
    }

    @font-face {
        font-family: 'CenturyGothic';
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
        src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
        font-weight: bold;
    }

    body {
        font-family: 'CenturyGothic';
        margin: 0;
        padding: 0;
    }
</style>
<table width="910" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <img src="<?php echo site_url('assets/solar_proposal_pdf_new/page-9.jpg'); ?>">
            <div style="position:relative;">
                <h2 style="top: -210px; position: absolute;right: 220px;text-transform:uppercase;"><?php echo implode('&nbsp;',explode(' ',$lead_data['full_name'])); ?></h2>
                <h2 style="top: -180px; position: absolute;right: 220px;text-transform:uppercase; font-weight: 500;">Sales Manager</h2>
            </div>
        </td>
    </tr>
</table>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" />
<style>
    body{
        overflow: hidden;
    }

    .page-wrapper {background-color: #ECEFF1; min-height:100%}
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
    .sr-inactive_user{color:red;}

    table div div {
        overflow: inherit !important;
        white-space: normal !important;
    }

    .page-wrapper{
        background-color: #fff !important;
    }
    .current_date_td{
        background-color: #FFC;
    }

    .table-bordered {
        border: 1px solid #ddd;
    }

    .active {
        color: #545454;
        padding-bottom: 15px;
        border-bottom: 1px solid #7CA06D;
    }
    td {
        height: 90px;
        padding: 5px;
        font-size: 12px;
    }

    .bg-installer{
        background-color: #FFF;
        border-top-color: #FFF !important;
        border-bottom-color: #FFF !important;
        vertical-align: middle !important;
        padding-left: 10px !important;
    }
    th {
        border-right-color: #F1F1F1;
        background-color: #F1F1F1;
    }
    .popover-content {
        padding: 9px 14px;
        font-size: 12px;
        color: #7E7E7E;
    }

    .close_pop {
        position: absolute;
        top: 10px;
        right: 10px;
        cursor:pointer;
    }


</style>

<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Proposal Scheduled Jobs</h1>
        <div class="content-header-right col-md-6 col-12"></div>
    </div>
    <div class="row" >
        <div class="col-12 col-md-12" style="overflow:scroll;" id="scheduled_jobs">

        </div>
    </div>
</div>

<script>
    var job_scheduler = function () {
        var self = this;
        $(document).on("click", "a[data-remote=true]", function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            self.fetch_scheduled_jobs(url);
        });
        self.fetch_scheduled_jobs(base_url + 'admin/franchise/job/fetch_scheduled_jobs?date_type=weekly');
    };

    job_scheduler.prototype.fetch_scheduled_jobs = function (url) {
        $('#scheduled_jobs').html('');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "html",
            success: function (data) {
                $('#scheduled_jobs').html(data);
                $('.event').popover({
                    html: true,
                    content: function () {
                        var data = $(this).attr('data-item');
                        data = JSON.parse(data);
                        var html = '<div class="popover-content">'
                                + '<strong>Name:</strong> '
                                + data.first_name + ' ' + data.last_name
                                + '<br>'
                                + '<strong>Address:</strong> '
                                + data.address
                                + '<div class="close_pop">'
                                + '<i class="fa fa-times fa-lg"></i>'
                                + '</div>'
                                + '</div>';
                        return html;
                    }
                }).on('shown.bs.popover', function (e) {
                    var popover = $(this);
                    $(this).parent().find('div.popover .close_pop').on('click', function (e) {
                        popover.popover('hide');
                    });
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    var job_scheduler_tool = new job_scheduler();

</script>
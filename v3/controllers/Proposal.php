<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**d
 * 
 * @property Proposal Controller
 * @version 1.0
 */
class Proposal extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Components");
        $this->load->library("Email_Manager");
        $this->load->model("Product_Model", "product");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Proposal_Model", "proposal");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(3) != 'led_pdf_download' && $this->uri->segment(3) != 'api_calculate_solar_proposal_cost' && $this->uri->segment(3) != 'api_calculate_solar_proposal_cost_for_react' && $this->uri->segment(3) != 'api_calculate_solar_savings' && $this->uri->segment(3) != 'solar_proposal_pdf_new_download') {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    /** Common Proposal Functions */
    public function manage()
    {
        $data            = array();
        //Restricitng and Fetching of Data based on user logged In group
        $data['user_id'] = $user_id         = $this->aauth->get_user()->id;
        $group           = $this->input->get('group');
        if (isset($group) && $group != '') {
            $data['meta_title'] = 'Manage ' . $this->aauth->is_member($group)[0]->group_name . ' Proposal';
            if (!$this->aauth->is_member($group) && !$this->aauth->is_member('Admin')) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
                redirect($this->redirect_url);
            }
            if ($this->aauth->is_member('Admin')) {
                $product_list = $this->product->get_products_list_by_roles($group);
            } else {
                $product_list = $this->product->get_products_list_by_roles($group, $user_id);
            }
        } else {
            if ($this->aauth->is_member('Admin')) {
                $data['meta_title']   = 'Admin - Manage Proposal';
                $data['user_details'] = $this->user->get_users_list_by_roles(FALSE)[0];
                $data['users']        = $this->user->get_users_list_by_roles(FALSE);
                $cond                 = "";
            } else if ($this->aauth->is_member('Franchise')) {
                $data['meta_title']   = 'Franchise - Manage Proposal';
                $data['user_details'] = $this->user->get_users_list_by_roles(GROUP_FRANCHISE_ID, $user_id)[0];
                $data['users']        = $this->user->get_users_list_by_owner(FALSE, $user_id);
                $all_users            = array_column($data['users'], 'user_id');
                $cond                 = (!empty($all_users)) ? " WHERE `agent_id` IN(" . $user_id . ',' . implode(',', $all_users) . ")" :
                    " WHERE `agent_id` IN(" . $user_id . ")";
            } else {
                $data['meta_title']   = 'Franchise Agents - Manage Proposal';
                $users                = $this->user->get_users_list_by_owner($user_id);
                $data['user_details'] = $users[0];
                $data['users']        = $this->user->get_users_list_by_owner(FALSE, $users[0]['owner_id']);
                $all_users            = array_column($data['users'], 'user_id');
                $cond                 = (!empty($all_users)) ? " WHERE `agent_id` IN(" . $users[0]['owner_id'] . ',' . implode(',', $all_users) . ")" :
                    " WHERE `agent_id` IN(" . $users[0]['owner_id'] . ")";
            }
        }

        $get_param_userid      = $this->input->get('id');
        $data['selected_user'] = (isset($get_param_userid) && $get_param_userid != NULL) ? $get_param_userid : $this->aauth->get_user()->id;
        if (isset($get_param_userid) && $get_param_userid != NULL) {
            $cond          = (isset($get_param_userid) && $get_param_userid != 0) ? " WHERE `agent_id` IN(" . $get_param_userid . ")" : $cond;
            $proposal_list = $this->proposal->get_proposal($cond);
        } else {
            $cond          = " WHERE `agent_id` IN(" . $this->aauth->get_user()->id . ")";
            $proposal_list = $this->proposal->get_proposal($cond);
        }

        $data['proposal_list'] = $proposal_list;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('proposal/manage');
        $this->load->view('partials/footer');
    }

    public function add()
    {
        $data            = array();
        $data['user_id'] = $user_id         = $this->aauth->get_user_id();
        //Check if Lead or Proposal Exists
        $deal_ref        = $this->input->get('deal_ref');
        $type_ref        = $this->input->get('type_ref');
        $site_ref        = $this->input->get('site_ref');

        if (isset($deal_ref) && $deal_ref != '' && isset($type_ref) && $type_ref != '' && isset($site_ref) && $site_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref, FALSE, $site_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/manage'));
            }
            if (!empty($lead_data) && $lead_data['location_id'] == NULL) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, deal found but no location found with referenced id.</div>');
                redirect(site_url('admin/lead/manage'));
            }

            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_id']   = $lead_id           = $lead_data['id'];
            $data['lead_data'] = $lead_data;
            $data['lead_uuid'] = $deal_ref;
            $data['site_id']   = $site_ref;

            switch ($type_ref) {
                case 'led':
                    $proposal_data = $this->proposal->get_led_proposal_data($lead_id, $site_ref);
                    if (empty($proposal_data)) {
                        $insert_data            = array();
                        $insert_data['lead_id'] = $lead_id;
                        $insert_data['site_id'] = $site_ref;
                        $this->common->insert_data('tbl_led_proposal', $insert_data);
                        $proposal_data          = $this->proposal->get_led_proposal_data($lead_id, $site_ref);
                    }
                    unset($proposal_data['calculated_data']);
                    $data['proposal_data']     = $proposal_data;
                    $data['product_types']     = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('parent_id' => LED));
                    $data['space_types']       = $this->common->fetch_where('tbl_led_space_types', '*', array('status' => 1, 'state' => $lead_data['state_postal']));
                    $data['existing_fittings'] = $this->common->fetch_where('tbl_led_existing_products', '*', array('status' => 1));
                    $data['access_equipments'] = $this->common->fetch_where('tbl_led_access_equipments', '*', array('status' => 1, 'state_id' => $lead_data['state_id']));
                    break;
                case 'solar':
                    $proposal_data             = $this->proposal->get_solar_proposal_data($lead_id, $site_ref);
                    $customer_location_data    = $this->common->fetch_row('tbl_customer_locations', '*', array('id' => $site_ref));

                    // In case if meter data in uploaded then we will have to calculate average daily consumption according to sheet
                    if ($proposal_data['is_meter_data'] == 1) {
                        $final_data = $this->read_meter_data_file($proposal_data['meter_data_file'], false);
                        $meter_data = $this->calculate_consumption_profile_data_by_meter_data($proposal_data, $final_data);
                        $sum = 0;
                        foreach ($meter_data['final_data'] as $row) {
                            $sum = $sum + $row[1];
                        }
                        $avgDailyConsumption  = $sum / 365;
                        $proposal_data['monthly_electricity_usage'] = round($avgDailyConsumption);
                    }

                    if (empty($proposal_data)) {
                        $insert_data            = array();
                        $insert_data['lead_id'] = $lead_id;
                        $insert_data['site_id'] = $site_ref;
                        $insert_data['area_id'] = $this->common->fetch_cell('tbl_solar_areas', 'areaId', array('state_id' => $customer_location_data['state_id'], 'areaStatus' => '1'));
                        $this->common->insert_data('tbl_solar_proposal', $insert_data);
                        $proposal_data          = $this->proposal->get_solar_proposal_data($lead_id, $site_ref);
                    }
                    $proposal_data['additional_items']      = ($proposal_data['additional_items'] != NULL) ? json_decode($proposal_data['additional_items']) : NULL;
                    $proposal_data['rate1_data']            = ($proposal_data['rate1_data'] != NULL) ? json_decode($proposal_data['rate1_data']) : NULL;
                    $proposal_data['rate2_data']            = ($proposal_data['rate2_data'] != NULL) ? json_decode($proposal_data['rate2_data']) : NULL;
                    $proposal_data['rate3_data']            = ($proposal_data['rate3_data'] != NULL) ? json_decode($proposal_data['rate3_data']) : NULL;
                    $proposal_data['rate4_data']            = ($proposal_data['rate4_data'] != NULL) ? json_decode($proposal_data['rate4_data']) : NULL;
                    $proposal_data['near_map_data']         = json_decode($proposal_data['near_map_data'], true);
                    $proposal_data['meter_data_replicated'] = ($proposal_data['meter_data_replicated'] != NULL) ? json_decode($proposal_data['meter_data_replicated']) : NULL;
                    $data['proposal_data']                  = $proposal_data;
                    $data['product_types']                  = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('type_id!=' => 1, 'parent_id' => 2));
                    $data['custom_profiles']                = $this->common->fetch_where('tbl_custom_profiles', '*', NULL);
                    $data['postcode_rating']                = $this->common->fetch_cell('tbl_postcodes', 'rating', array('postcode' => $customer_location_data['postcode']));
                    $data['panel_data']                     = $this->common->fetch_where('tbl_solar_proposal_panel_data', '*', array('proposal_id' => $proposal_data['id']));
                    $data['stc_modal']                   = $this->load->view('proposal/form_solar_stc_modal_partial', $data, TRUE);
                    $data['solar_panels']                   = $this->common->fetch_where('tbl_solar_products', '*', ['status' => 1, 'type_id' => 9]);
                    $data['product_mapping']                = $this->common->fetch_where('tbl_mapping_tool_products', '*', NULL);
                    $data['mapping_tool']                   = $this->load->view('partials/mapping_tool', $data, TRUE);
                    $data['new_mapping_tool']               = $this->load->view('partials/near_map', $data, TRUE);
                    break;
                case 'residential_solar':
                    $proposal_data = $this->common->fetch_row('tbl_proposal', 'id,uuid,lead_id,near_map_data', array('lead_id' => $lead_id, 'site_id' => $site_ref));
                    $proposal_data['near_map_data'] = !empty($proposal_data) ? json_decode($proposal_data['near_map_data'], true) : [];
                    $data['proposal_data'] = $proposal_data;
                    $data['stc_price_assumption'] = '';
                    $data['certificate_margin']   = '';
                    $data['product_type']         = $this->common->fetch_where('tbl_product_type', '*', NULL);
                    $data['line_item']            = $this->common->fetch_where('tbl_product_type', '*', NULL);
                    $data['product_mapping'] = $this->common->fetch_where('tbl_mapping_tool_products', '*', NULL);
                    $data['solar_areas']     = $this->common->fetch_where('tbl_solar_areas', '*', NULL);
                    $nearMapProducts      = 'SELECT id,`name`,brand,`length`,width,capacity FROM tbl_products WHERE availability  =1 and `status` = 1 AND type_id = 2';

                    $state = $lead_data['state_postal'];
                    $cond2 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=2";
                    $data['solar_panels'] = $this->product->get_item_data_by_cond($cond2); //$this->db->query($nearMapProducts)->result_array();
                    $data['mapping_tool']    = $this->load->view('partials/mapping_tool', $data, TRUE);
                    $data['new_mapping_tool']  = $this->load->view('partials/near_map_residential_solar', $data, TRUE);
                    $data['stc_modal']                   = $this->load->view('proposal/form_solar_stc_modal_partial', $data, TRUE);
                    break;
            }
        } else {
            redirect(site_url('admin/lead/manage'));
        }

        $data['title']  = ucfirst($type_ref) . ' Proposal';
        $states         = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('proposal/form_' . $type_ref);
        $this->load->view('customer/customer_proposal_site_modal_partial');
        $this->load->view('partials/footer');
    }

    public function calculate_finance()
    {
        if (!empty($this->input->get())) {
            $stat        = FALSE;
            $term        = $this->input->get('term');
            $proposal_id = $this->input->get('proposal_id');
            $type        = $this->input->get('type');
            $amount      = 0;
            if (isset($type) && $type == 2) {
                $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));
                $amount        = $proposal_data['price_before_stc_rebate'];
            } else {
                $calculated_data = $this->calculate_led_proposal_cost($proposal_id);
                $amount          = $calculated_data['total_investment_inGST'];
            }

            $url     = 'https://webapi.energyease.com.au/payment?token=4fRdpuFrSu7a&applicantName=Testing&internalRef=123&financeType=rent&homeBasedBusiness=false&equipmentCode=sol&amount=' . $amount . '&term=' . $term;
            $content = @file_get_contents($url);
            if ($content === FALSE) {
                $data['status']  = 'Looks like something went wrong.';
                $data['success'] = FALSE;
            } else {
                $response                = json_decode($content, true);
                $data['success']         = TRUE;
                $data['monthly_payment'] = $response['Value'];
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function save_finance()
    {
        if (!empty($this->input->post())) {
            $stat                                 = FALSE;
            $proposal_id                          = $this->input->post('proposal_id');
            $proposal_finance_data                = $this->input->post('proposal_finance');
            $proposal_type                        = $proposal_finance_data['proposal_type'];
            $proposal_finance_data['proposal_id'] = $proposal_id;
            if ($proposal_type == '1') {
                //Led Finance
                $calculated_data                               = $this->calculate_led_proposal_cost($proposal_id);
                $total_saving                                  = $calculated_data['total_year_savings'];
                $monthly_saving                                = $total_saving / 12;
                $monthly_payment_plan                          = (isset($proposal_finance_data['monthly_payment_plan']) && $proposal_finance_data['monthly_payment_plan'] != '') ? $proposal_finance_data['monthly_payment_plan'] : 0;
                $monthly_net_cashflow                          = $total_saving - $monthly_payment_plan;
                $proposal_finance_data['monthly_saving']       = number_format((float) $monthly_saving, 2, '.', '');
                $proposal_finance_data['monthly_net_cashflow'] = number_format((float) $monthly_net_cashflow, 2, '.', '');
            }

            $finance_data = $this->common->fetch_row('tbl_proposal_finance', 'pf_id', array('proposal_type' => $proposal_type, 'proposal_id' => $proposal_id));
            if (!empty($finance_data)) {
                $stat = $this->common->update_data('tbl_proposal_finance', ['pf_id' => $finance_data['pf_id']], $proposal_finance_data);
            } else {
                $stat = $this->common->insert_data('tbl_proposal_finance', $proposal_finance_data);
            }
            $data['success'] = TRUE;
            $data['status']  = 'Finance data saved successfully.';
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function delete_finance()
    {
        if (!empty($this->input->post())) {
            $stat                  = FALSE;
            $proposal_id           = $this->input->post('proposal_id');
            $proposal_finance_data = $this->input->post('proposal_finance');
            $proposal_type         = $proposal_finance_data['proposal_type'];
            $stat                  = $this->common->delete_data('tbl_proposal_finance', array('proposal_type' => $proposal_type, 'proposal_id' => $proposal_id));

            if ($stat) {
                $data['success'] = TRUE;
                $data['status']  = 'Finance data deleted successfully.';
            } else {
                $data['success'] = FALSE;
                $data['status']  = 'Looks like something went wrong.';
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    /** Common proposal Function End */

    /** LED Proposal Related Functions */
    public function save_led_proposal()
    {
        if (!empty($this->input->post())) {
            $stat         = FALSE;
            $led_proposal = $this->input->post('led_proposal');
            $proposal_id  = $this->input->post('proposal_id');
            if (isset($proposal_id) && $proposal_id != '') {
                $stat = $this->common->update_data('tbl_led_proposal', ['id' => $proposal_id], $led_proposal);
            }
            if ($stat) {
                $data['status']  = 'Proposal Data Saved successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function save_led_proposal_products()
    {
        if (!empty($this->input->post())) {
            $led_product    = $this->input->post('led_product');
            $led_product_id = $this->input->post('led_product_id');
            if (isset($led_product_id) && $led_product_id != '') {
                $stat = $this->common->update_data('tbl_led_proposal_products', ['id' => $led_product_id], $led_product);
            } else {
                $stat = $this->common->insert_data('tbl_led_proposal_products', $led_product);
            }
            if ($stat) {
                $data['status']       = 'Led Proposal Product saved successfully';
                $data['success']      = TRUE;
                $data['product_data'] = array();
                if (isset($led_product) && $led_product['proposal_id']) {
                    $data['product_data'] = $this->product->get_led_proposal_products($led_product['proposal_id']);
                }
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function delete_led_proposal_products()
    {
        if (!empty($this->input->post())) {
            $stat           = FALSE;
            $led_product_id = $this->input->post('led_product_id');
            $proposal_id    = $this->input->post('proposal_id');
            if (isset($led_product_id) && $led_product_id != '' && isset($proposal_id) && $proposal_id != '') {
                $stat = $this->common->delete_data('tbl_led_proposal_products', ['id' => $led_product_id, 'proposal_id' => $proposal_id]);
            }
            if ($stat) {
                $data['status']  = 'Led Proposal Product deleted successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_led_proposal_products()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $proposal_id          = $this->input->get('proposal_id');
            $data['product_data'] = array();
            $data['product_data'] = $this->product->get_led_proposal_products($proposal_id);
            $data['success']      = TRUE;
        } else {
            $data['success'] = FALSE;
            $data['status']  = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function fetch_led_proposal_additional_items()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $proposal_id          = $this->input->get('proposal_id');
            $data['product_data'] = array();
            $data['product_data'] = $this->product->get_led_proposal_additional_items($proposal_id);
            $data['success']      = TRUE;
        } else {
            $data['success'] = FALSE;
            $data['status']  = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function save_led_proposal_additional_items()
    {
        if (!empty($this->input->post())) {
            $insert_data = $this->input->post();
            $proposal_id = $this->input->post('proposal_id');
            $stat        = $this->common->insert_data('tbl_led_proposal_access_equipments', $insert_data);
            if ($stat) {
                $data['status']       = 'Led Proposal Addtional Items saved successfully';
                $data['success']      = TRUE;
                $data['product_data'] = array();
                $data['product_data'] = $this->product->get_led_proposal_additional_items($proposal_id);
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function delete_led_proposal_additional_items()
    {
        if (!empty($this->input->post())) {
            $stat        = FALSE;
            $item_id     = $this->input->post('item_id');
            $proposal_id = $this->input->post('proposal_id');
            if (isset($item_id) && $item_id != '' && isset($proposal_id) && $proposal_id != '') {
                $stat = $this->common->delete_data('tbl_led_proposal_access_equipments', ['id' => $item_id, 'proposal_id' => $proposal_id]);
            }
            if ($stat) {
                $data['status']  = 'Led Proposal Addtional Items deleted successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_led_proposal_details()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $proposal_id     = $this->input->get('proposal_id');
            $calculated_data = $this->calculate_led_proposal_cost($proposal_id);

            $update_data                              = array();
            $update_data['total_discount_veec_exGST'] = $calculated_data['total_discount_veec_exGST'];
            $update_data['total_project_cost_exGST']  = $calculated_data['total_project_cost_exGST'];
            $update_data['total_investment_exGST']    = $calculated_data['total_investment_exGST'];
            $update_data['calculated_data']           = json_encode($calculated_data);
            $this->common->update_data('tbl_led_proposal', array('id' => $proposal_id), $update_data);

            $proposal_data        = $this->product->get_led_proposal_products_sum_by_product_type($proposal_id);
            $proposal_data['roi'] = $calculated_data['roi'];
            unset($proposal_data['calculated_data']);
            if (!empty($proposal_data)) {
                $data['proposal_data'] = $proposal_data;
                $data['success']       = TRUE;
            } else {
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }

    private function calculate_led_proposal_cost($proposal_id)
    {
        $data                                      = array();
        $npqty_multiply_veec                       = 0;
        $npqty_multiply_price                      = 0;
        $sum_npqty_multiply_veec                   = 0;
        $sum_npqty_multiply_price                  = 0;
        $total_additional_item_cost                = 0;
        $total_discount_veec_exGST                 = 0;
        $total_project_cost_exGST                  = 0;
        $total_investment_exGST                    = 0;
        $total_discount_veec_inGST                 = 0;
        $total_project_cost_inGST                  = 0;
        $total_investment_inGST                    = 0;
        $total_wattage                             = 0;
        $epqty_multiply_total_wattage              = 0;
        $total_existing                            = 0;
        $annual_op_hrs                             = 0;
        $current_kwh_pa                            = 0;
        $npqty_multiply_npwattage                  = 0;
        $wattage                                   = 0;
        $wattage_new                               = 0;
        $new_kwh_pa                                = 0;
        $reduction_pa                              = 0;
        $energy_savings_pa                         = 0;
        $energy_reduction_lighting                 = 0;
        $maintance_savings_pa                      = 0;
        $rated_life_time_for_this_bussiness        = 0;
        $times_old_light_replaced_for_LED_lifetime = 0;
        $lifetime_maintance_cost_p_unit            = 0;
        $total_maintance_cost_for_this_business    = 0;
        $year_savings_10                           = 0;
        $total_savings                             = 0;
        $roi                                       = 0;
        $demand_qty_np                             = 0;
        $demand_veec                               = 0;
        $demand_charges_savings_pa                 = 0;

        //Get Proposal, Proposal Product and Proposal Additional Item Data
        $proposal_data                 = $this->common->fetch_row('tbl_led_proposal', '*', array('id' => $proposal_id));
        $led_proposal_products         = $this->product->get_led_proposal_products($proposal_id);
        $led_proposal_additional_items = $this->common->fetch_where('tbl_led_proposal_access_equipments', '*', array('proposal_id' => $proposal_id));
        $annual_op_hrs                 = ($proposal_data['op_hrs'] * $proposal_data['op_days']) * 52;


        foreach ($led_proposal_products as $product) {

            /** Start Project Cost Calculation */
            $npqty_multiply_veec          = $product['np_qty'] * $product['veec'];
            $npqty_multiply_price         = $product['np_qty'] * $product['price'];
            $sum_npqty_multiply_veec      += $npqty_multiply_veec;
            $sum_npqty_multiply_price     += $npqty_multiply_price;
            /** End Project Cost Calculation */
            /** Start CURRENT kWh P.A. */
            $totalExisting                = 0;
            $total_wattage                = $product['ep_wattage'] + $product['driver_consumption'];
            $epqty_multiply_total_wattage = $product['ep_qty'] * $total_wattage;
            $total_existing               += $epqty_multiply_total_wattage;
            /** End CURRENT kWh P.A. */
            /** Start NEW kWh P.A. */
            $npqty_multiply_npwattage     = $product['np_qty'] * $product['np_wattage'];
            $wattage                      = $wattage + $npqty_multiply_npwattage;
            /** End NEW kWh P.A. */
            /** Start Maintance Savings P.A. */
            if ($annual_op_hrs != 0) {
                $rated_life_time_for_this_bussiness = $product['np_rated_life'] / $annual_op_hrs;
            }
            if ($product['ep_rated_life'] != 0) {
                $times_old_light_replaced_for_LED_lifetime = $product['np_rated_life'] / $product['ep_rated_life'];
            }
            $lifetime_maintance_cost_p_unit         = $times_old_light_replaced_for_LED_lifetime * $product['replacement_cost'];
            $total_maintance_cost_for_this_business = $lifetime_maintance_cost_p_unit * $product['np_qty'];
            $k50                                    = 0;
            if ($rated_life_time_for_this_bussiness > 0) {
                $k50 = round($total_maintance_cost_for_this_business / $rated_life_time_for_this_bussiness);
            }
            $maintance_savings_pa = $maintance_savings_pa + $k50;
            /** End Maintance Savings P.A. */
            /** Start Demand charges Savings P.A.. */
            $demand_qty_np        += $product['np_qty'];
            $demand_veec          += $product['veec'];
            /** End Demand charges Savings P.A.. */
        }

        /** Start Project Cost Calculation */
        foreach ($led_proposal_additional_items as $item) {
            $total_additional_item_cost += ($item['ae_qty'] * $item['ae_cost']);
        }
        $data['total_discount_veec_exGST'] = $total_discount_veec_exGST         = round($sum_npqty_multiply_veec);
        $data['total_project_cost_exGST']  = $total_project_cost_exGST          = round($sum_npqty_multiply_price + $total_discount_veec_exGST + $total_additional_item_cost);
        $data['total_investment_exGST']    = $total_investment_exGST            = round($total_project_cost_exGST - $total_discount_veec_exGST);

        $data['total_discount_veec_inGST'] = $total_discount_veec_inGST         = round($total_discount_veec_exGST * GST);
        $data['total_project_cost_inGST']  = $total_project_cost_inGST          = round($total_project_cost_exGST * GST);
        $data['total_investment_inGST']    = $total_investment_inGST            = round($total_investment_exGST * GST);
        /** End Project Cost Calculation */
        /** Start CURRENT kWh P.A. */
        $total_existing                    = round($total_existing * $annual_op_hrs);
        $current_kwh_pa                    = round($total_existing / 1000);
        $data['current_kwh_pa']            = $current_kwh_pa;
        /** End CURRENT kWh P.A. */
        /** Start NEW kWh P.A. */
        $wattage_new                       = round($wattage * $annual_op_hrs);
        $new_kwh_pa                        = round($wattage_new / 1000);
        $data['new_kwh_pa']                = $new_kwh_pa;
        /** End NEW kWh P.A. */
        /** Start REDUCTION P.A. */
        if ($new_kwh_pa > $current_kwh_pa) {
            $reduction_pa = round($new_kwh_pa - $current_kwh_pa);
        } else {
            $reduction_pa = round($current_kwh_pa - $new_kwh_pa);
        }
        $data['reduction_pa']           = $reduction_pa;
        /** End REDUCTION P.A. */
        /** Start Energy Savings P.A. */
        $data['energy_savings_pa']      = $energy_savings_pa              = round($reduction_pa * $proposal_data['energy_rate']);
        /** End Energy Savings P.A. */
        /** Start 10 YEAR  SAVINGS */
        $YR1                            = round($energy_savings_pa);
        $YR2                            = round($YR1 * 1.05);
        $YR3                            = round($YR2 * 1.05);
        $YR4                            = round($YR3 * 1.05);
        $YR5                            = round($YR4 * 1.05);
        $YR6                            = round($YR5 * 1.05);
        $YR7                            = round($YR6 * 1.05);
        $YR8                            = round($YR7 * 1.05);
        $YR9                            = round($YR8 * 1.05);
        $YR10                           = round($YR9 * 1.05);
        $data['10_year_energy_savings'] = $totalYR10                      = round($YR1 + $YR2 + $YR3 + $YR4 + $YR5 + $YR6 + $YR7 + $YR8 + $YR9 + $YR10);
        /** End 10 YEAR  SAVINGS */
        /** Start ENERGY REDUCTION (Lighting) */
        if ($current_kwh_pa > 0) {
            $energy_reduction_lighting = round($reduction_pa * 100 / $current_kwh_pa);
        }
        $data['energy_reduction_lighting'] = $energy_reduction_lighting;
        /** End ENERGY REDUCTION (Lighting) */
        /** Start Maintance Savings P.A. */
        $data['maintance_savings_pa']      = $maintance_savings_pa              = round($maintance_savings_pa);
        /** End Maintance Savings P.A. */
        /** Start Total Yearly Savings. */
        $data['total_year_savings']        = $TotalYearlySavings                = $energy_savings_pa + $maintance_savings_pa;
        /** End Total Yearly Savings. */
        /** Start 10 YEAR  SAVINGS TABLE Maintance Savings (2% increase). */
        $MaintanceSavings_YR1              = round($maintance_savings_pa);
        $MaintanceSavings_YR2              = round($MaintanceSavings_YR1 * 1.02);
        $MaintanceSavings_YR3              = round($MaintanceSavings_YR2 * 1.02);
        $MaintanceSavings_YR4              = round($MaintanceSavings_YR3 * 1.02);
        $MaintanceSavings_YR5              = round($MaintanceSavings_YR4 * 1.02);
        $MaintanceSavings_YR6              = round($MaintanceSavings_YR5 * 1.02);
        $MaintanceSavings_YR7              = round($MaintanceSavings_YR6 * 1.02);
        $MaintanceSavings_YR8              = round($MaintanceSavings_YR7 * 1.02);
        $MaintanceSavings_YR9              = round($MaintanceSavings_YR8 * 1.02);
        $MaintanceSavings_YR10             = round($MaintanceSavings_YR9 * 1.02);
        $data['10_year_maintance_savings'] = $MaintanceSavings_totalYR10        = round($MaintanceSavings_YR1 + $MaintanceSavings_YR2 + $MaintanceSavings_YR3 + $MaintanceSavings_YR4 + $MaintanceSavings_YR5 + $MaintanceSavings_YR6 + $MaintanceSavings_YR7 + $MaintanceSavings_YR8 + $MaintanceSavings_YR9 + $MaintanceSavings_YR10);
        /** End 10 YEAR  SAVINGS TABLE Maintance Savings (2% increase). */
        /** Start 10 YEAR  SAVINGS. */
        $data['10_year_savings']           = $year_savings_10                   = $totalYR10 + $MaintanceSavings_totalYR10;
        /** End 10 YEAR  SAVINGS. */
        /** Start Total  SAVINGS. */
        $data['TotalSavings_YR1']          = $TotalSavings_YR1                  = $YR1 + $MaintanceSavings_YR1;
        $data['TotalSavings_YR2']          = $TotalSavings_YR2                  = $YR2 + $MaintanceSavings_YR2;
        $data['TotalSavings_YR3']          = $TotalSavings_YR3                  = $YR3 + $MaintanceSavings_YR3;
        $data['TotalSavings_YR4']          = $TotalSavings_YR4                  = $YR4 + $MaintanceSavings_YR4;
        $data['TotalSavings_YR5']          = $TotalSavings_YR5                  = $YR5 + $MaintanceSavings_YR5;
        $data['TotalSavings_YR6']          = $TotalSavings_YR6                  = $YR6 + $MaintanceSavings_YR6;
        $data['TotalSavings_YR7']          = $TotalSavings_YR7                  = $YR7 + $MaintanceSavings_YR7;
        $data['TotalSavings_YR8']          = $TotalSavings_YR8                  = $YR8 + $MaintanceSavings_YR8;
        $data['TotalSavings_YR9']          = $TotalSavings_YR9                  = $YR9 + $MaintanceSavings_YR9;
        $data['TotalSavings_YR10']         = $TotalSavings_YR10                 = $YR10 + $MaintanceSavings_YR10;
        $data['total_savings']             = $total_savings                     = round($TotalSavings_YR1 + $TotalSavings_YR2 + $TotalSavings_YR3 + $TotalSavings_YR4 + $TotalSavings_YR5 + $TotalSavings_YR6 + $TotalSavings_YR7 + $TotalSavings_YR8 + $TotalSavings_YR9 + $TotalSavings_YR10);
        /** End Total  SAVINGS. */
        /** Start 10 Savings Return. */
        $SavingsReturn_YR1                 = $TotalSavings_YR1 - $total_investment_exGST;
        $SavingsReturn_YR2                 = $TotalSavings_YR2 + $SavingsReturn_YR1;
        $SavingsReturn_YR3                 = $TotalSavings_YR3 + $SavingsReturn_YR2;
        $SavingsReturn_YR4                 = $TotalSavings_YR4 + $SavingsReturn_YR3;
        $SavingsReturn_YR5                 = $TotalSavings_YR5 + $SavingsReturn_YR4;
        $SavingsReturn_YR6                 = $TotalSavings_YR6 + $SavingsReturn_YR5;
        $SavingsReturn_YR7                 = $TotalSavings_YR7 + $SavingsReturn_YR6;
        $SavingsReturn_YR8                 = $TotalSavings_YR8 + $SavingsReturn_YR7;
        $SavingsReturn_YR9                 = $TotalSavings_YR9 + $SavingsReturn_YR8;
        $SavingsReturn_YR10                = $TotalSavings_YR10 + $SavingsReturn_YR9;
        /** Start 10 Savings Return. */
        /** Start ROI (months). */
        if ($TotalYearlySavings != 0) {
            $roi = $total_investment_exGST / $TotalYearlySavings;
        }
        $data['roi']                       = $roi                               = 12 * $roi;
        /** End ROI (months). */
        /** Start Demand charges Savings P.A.. */
        $data['demand_charges_savings_pa'] = $demand_charges_savings_pa         = $demand_qty_np - $demand_veec;
        /** End Demand charges Savings P.A.. */
        return $data;
    }


    public function save_dataimg_to_png()
    {
        $dataImg1 =  $this->input->post('dataImg1');
        $chart1 =  $this->input->post('chart1');
        $img = explode(';', $dataImg1);
        if ($dataImg1 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg1));
            $newfilename = $chart1 . '.png';
            file_put_contents('./assets/uploads/pdf_files/' . $newfilename, $image);
        }

        $dataImg2 =  $this->input->post('dataImg2');
        $chart2 =  $this->input->post('chart2');
        $img = explode(';', $dataImg2);
        if ($dataImg2 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg2));
            $newfilename = $chart2 . '.png';
            file_put_contents('./assets/uploads/pdf_files/' . $newfilename, $image);
        }

        $dataImg3 =  $this->input->post('dataImg3');
        $chart3 =  $this->input->post('chart3');
        $img = explode(';', $dataImg3);
        if ($dataImg3 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg3));
            $newfilename = $chart3 . '.png';
            file_put_contents('./assets/uploads/pdf_files/' . $newfilename, $image);
        }

        $dataImg4 =  $this->input->post('dataImg4');
        $chart4 =  $this->input->post('chart4');
        $img = explode(';', $dataImg4);
        if ($dataImg4 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg4));
            $newfilename = $chart4 . '.png';
            file_put_contents('./assets/uploads/pdf_files/' . $newfilename, $image);
        }
    }

    public function led_pdf_download($proposal_id)
    {

        //Check if downloading user is same as creating user
        $user_id        = $this->aauth->get_user_id();
        $lead_id        = $this->common->fetch_cell('tbl_led_proposal', 'lead_id', array('id' => $proposal_id));
        $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
        $is_access      = (($lead_to_userid == $user_id) || $user_id == 1) ? TRUE : FALSE;

        if (!$is_access) {
            //$this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access Not appropriate User to download the proposal.</div>');
            //redirect($this->redirect_url);
            //exit();
        }

        $view = $this->input->get('view');
        if (isset($view) && $view == 'download') {
            $data = array();
            $data = $this->calcualte_led_pdf_data($proposal_id);
            $html = $this->load->view('pdf_files/led/page01', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page1', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page2', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page3', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page4', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page5', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page6', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page7', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page8', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page_calculating_benefit', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page10', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page11', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page12', $data, TRUE);
            $html .= $this->load->view('pdf_files/led/page13', $data, TRUE);

            if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
                echo $html;
                die;
            }

            $customer_name = $data['led_proposal_data']['first_name'] . "_" . $data['led_proposal_data']['last_name'];
            $filename      = "KugaElectrical-LED_Quote_" . $customer_name . "-" . date('Y-m-d');
            $filename    = str_replace(array('.', ',', ' '), '_', $filename);
            $h           = '';
            $f           = '';


            $pdf_options = array(
                "source_type" => 'html',
                "source"      => $html,
                "action"      => 'download',
                "save_directory" => FCPATH . './assets/uploads/pdf_files/',
                "file_name"   => $filename . '.pdf',
                "page_size"   => 'A4',
                "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                "header"      => $h,
                "footer"      => $f
            );
            $this->pdf->phptopdf($pdf_options);
        } else {
            $data = array();
            $data = $this->calcualte_led_pdf_data($proposal_id);
            $this->load->view('pdf_files/led/chart1', $data);
            //redirect(site_url('admin/proposal/led_pdf_dwonload/'.$proposal_id.'?view=download'));
            //exit();
        }
    }

    private function calcualte_led_pdf_data($proposal_id)
    {

        //Get Customer,User,Location,Finance,Proposal(Led Proposal Data), Proposal Product and Proposal Additional Item Data
        $led_proposal_data                        = $this->lead->get_led_proposal_data($proposal_id);
        if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
            //print_r($led_proposal_data);die;
        }
        $led_proposal_data['customer_address']    = str_ireplace(", Australia", "", $led_proposal_data['customer_address']);
        $led_proposal_data['rep_company_address'] = str_ireplace(", Australia", "", $led_proposal_data['rep_company_address']);

        $data['led_proposal_data']             = $led_proposal_data;
        $data['led_proposal_products']         = $led_proposal_products                 = $this->product->get_led_proposal_products($proposal_id);
        $data['led_proposal_additional_items'] = $led_proposal_additional_items         = $this->product->get_led_proposal_additional_items($proposal_id);
        $data['calculated_data']               = $calculated_data                       = $this->calculate_led_proposal_cost($proposal_id);

        $before         = round($calculated_data['current_kwh_pa'] * $led_proposal_data['energy_rate']);
        $data['before'] = round($before);
        $after          = round($calculated_data['new_kwh_pa'] * $led_proposal_data['energy_rate']);
        $data['after']  = round($after);

        //$rebateStatus = $proposal['isFinance'];
        // 10 YEAR SAVINGS TABLE 
        $Term           = $led_proposal_data['term'];
        $financePerYear = $led_proposal_data['monthly_payment_plan'] * 12;

        $Finance_Yr1 = 0;
        $Finance_Yr2 = 0;
        $Finance_Yr3 = 0;
        $Finance_Yr4 = 0;
        $Finance_Yr5 = 0;

        if ($Term == 12) {
            $Finance_Yr1 = $financePerYear;
            $Finance_Yr2 = 0;
            $Finance_Yr3 = 0;
            $Finance_Yr4 = 0;
            $Finance_Yr5 = 0;
        }
        if ($Term == 24) {
            $Finance_Yr1 = $financePerYear;
            $Finance_Yr2 = $financePerYear;
            $Finance_Yr3 = 0;
            $Finance_Yr4 = 0;
            $Finance_Yr5 = 0;
        }
        if ($Term == 36) {
            $Finance_Yr1 = $financePerYear;
            $Finance_Yr2 = $financePerYear;
            $Finance_Yr3 = $financePerYear;
            $Finance_Yr4 = 0;
            $Finance_Yr5 = 0;
        }
        if ($Term == 48) {
            $Finance_Yr1 = $financePerYear;
            $Finance_Yr2 = $financePerYear;
            $Finance_Yr3 = $financePerYear;
            $Finance_Yr4 = $financePerYear;
            $Finance_Yr5 = 0;
        }
        if ($Term == 60) {
            $Finance_Yr1 = $financePerYear;
            $Finance_Yr2 = $financePerYear;
            $Finance_Yr3 = $financePerYear;
            $Finance_Yr4 = $financePerYear;
            $Finance_Yr5 = $financePerYear;
        }

        $data['Finance_Yr1'] = $Finance_Yr1;
        $data['Finance_Yr2'] = $Finance_Yr2;
        $data['Finance_Yr3'] = $Finance_Yr3;
        $data['Finance_Yr4'] = $Finance_Yr4;
        $data['Finance_Yr5'] = $Finance_Yr5;

        $Finance = 0;

        // RETURN ON INVESTMENT
        $returnoninvestment          = ($calculated_data['current_kwh_pa'] - $calculated_data['new_kwh_pa']) * $led_proposal_data['energy_rate'];
        $Firstyearreturnoninvestment = 0;
        if ($calculated_data['total_investment_exGST'] != 0) {
            $Firstyearreturnoninvestment = round($returnoninvestment * 100 / $calculated_data['total_investment_exGST']);
        }
        $data['Firstyearreturnoninvestment'] = $Firstyearreturnoninvestment;

        $Totalreturnoninvestment = round($returnoninvestment - $financePerYear);

        // line graph
        $data['withUpgrade_yer1']    = $withUpgrade_yer1            = $calculated_data['new_kwh_pa'] * $led_proposal_data['energy_rate'];
        $data['withoutUpgrade_yer1'] = $withoutUpgrade_yer1         = $calculated_data['current_kwh_pa'] * $led_proposal_data['energy_rate'];

        $data['withUpgrade_yer2']    = $withUpgrade_yer2            = $withUpgrade_yer1 * 1.1;
        $data['withoutUpgrade_yer2'] = $withoutUpgrade_yer2         = $withoutUpgrade_yer1 * 1.1;

        $data['withUpgrade_yer3']    = $withUpgrade_yer3            = $withUpgrade_yer2 * 1.1;
        $data['withoutUpgrade_yer3'] = $withoutUpgrade_yer3         = $withoutUpgrade_yer2 * 1.1;

        $data['withUpgrade_yer4']    = $withUpgrade_yer4            = $withUpgrade_yer3 * 1.1;
        $data['withoutUpgrade_yer4'] = $withoutUpgrade_yer4         = $withoutUpgrade_yer3 * 1.1;

        $data['withUpgrade_yer5']    = $withUpgrade_yer5            = $withUpgrade_yer4 * 1.1;
        $data['withoutUpgrade_yer5'] = $withoutUpgrade_yer5         = $withoutUpgrade_yer4 * 1.1;

        $data['Noupgradecost_5years']  = $Noupgradecost_5years          = $withoutUpgrade_yer1 + $withoutUpgrade_yer2 + $withoutUpgrade_yer3 + $withoutUpgrade_yer4 + $withoutUpgrade_yer5;
        $data['LEDupgradecost_5years'] = $LEDupgradecost_5years         = $withUpgrade_yer1 + $withUpgrade_yer2 + $withUpgrade_yer3 + $withUpgrade_yer4 + $withUpgrade_yer5;

        if ($Noupgradecost_5years > $LEDupgradecost_5years) {
            $data['Savingsfromupgrade'] = $Savingsfromupgrade         = round($Noupgradecost_5years - $LEDupgradecost_5years);
        } else {
            $data['Savingsfromupgrade'] = $Savingsfromupgrade         = round($LEDupgradecost_5years - $Noupgradecost_5years);
        }


        // Year 1 Data
        $data['OldBill_yer1']    = $OldBill_yer1            = $calculated_data['current_kwh_pa'] * $led_proposal_data['energy_rate'];
        $data['NewBill_yer1']    = $NewBill_yer1            = $calculated_data['new_kwh_pa'] * $led_proposal_data['energy_rate'];
        $data['NoUpgrade_yer1']  = $NoUpgrade_yer1          = $OldBill_yer1;
        $data['LEDUpgrade_yer1'] = $LEDUpgrade_yer1         = $NewBill_yer1 + $calculated_data['total_investment_exGST'];
        $data['OldBill2_yer1']   = $OldBill2_yer1           = abs($OldBill_yer1 - $NewBill_yer1 - $Finance_Yr1);

        // Year 2 Data
        $data['OldBill_yer2']    = $OldBill_yer2            = $OldBill_yer1 * 1.05;
        $data['NewBill_yer2']    = $NewBill_yer2            = $NewBill_yer1 * 1.05;
        $data['NoUpgrade_yer2']  = $NoUpgrade_yer2          = $NoUpgrade_yer1 + $OldBill_yer2;
        $data['LEDUpgrade_yer2'] = $LEDUpgrade_yer2         = $LEDUpgrade_yer1 + $NewBill_yer2;
        $data['OldBill2_yer2']   = $OldBill2_yer2           = abs($OldBill_yer2 - $NewBill_yer2 - $Finance_Yr2);

        // Year 3 Data
        $data['OldBill_yer3']    = $OldBill_yer3            = $OldBill_yer2 * 1.05;
        $data['NewBill_yer3']    = $NewBill_yer3            = $NewBill_yer2 * 1.05;
        $data['NoUpgrade_yer3']  = $NoUpgrade_yer3          = $NoUpgrade_yer2 + $OldBill_yer3;
        $data['LEDUpgrade_yer3'] = $LEDUpgrade_yer3         = $LEDUpgrade_yer2 + $NewBill_yer3;
        $data['OldBill2_yer3']   = $OldBill2_yer3           = abs($OldBill_yer3 - $NewBill_yer3 - $Finance_Yr3);

        // Year 4 Data
        $data['OldBill_yer4']    = $OldBill_yer4            = $OldBill_yer3 * 1.05;
        $data['NewBill_yer4']    = $NewBill_yer4            = $NewBill_yer3 * 1.05;
        $data['NoUpgrade_yer4']  = $NoUpgrade_yer4          = $NoUpgrade_yer3 + $OldBill_yer4;
        $data['LEDUpgrade_yer4'] = $LEDUpgrade_yer4         = $LEDUpgrade_yer3 + $NewBill_yer4;
        $data['OldBill2_yer4']   = $OldBill2_yer4           = abs($OldBill_yer4 - $NewBill_yer4 - $Finance_Yr4);

        // Year 5 Data
        $data['OldBill_yer5']    = $OldBill_yer5            = $OldBill_yer4 * 1.05;
        $data['NewBill_yer5']    = $NewBill_yer5            = $NewBill_yer4 * 1.05;
        $data['NoUpgrade_yer5']  = $NoUpgrade_yer5          = $NoUpgrade_yer4 + $OldBill_yer5;
        $data['LEDUpgrade_yer5'] = $LEDUpgrade_yer5         = $LEDUpgrade_yer4 + $NewBill_yer5;
        $data['OldBill2_yer5']   = $OldBill2_yer5           = abs($OldBill_yer5 - $NewBill_yer5 - $Finance_Yr5);

        // Year 6 Data
        $data['OldBill_yer6']    = $OldBill_yer6            = $OldBill_yer5 * 1.05;
        $data['NewBill_yer6']    = $NewBill_yer6            = $NewBill_yer5 * 1.05;
        $data['NoUpgrade_yer6']  = $NoUpgrade_yer6          = $NoUpgrade_yer5 + $OldBill_yer6;
        $data['LEDUpgrade_yer6'] = $LEDUpgrade_yer6         = $LEDUpgrade_yer1 + $NewBill_yer6;
        $data['OldBill2_yer6']   = $OldBill2_yer6           = abs($OldBill_yer6 - $NewBill_yer6 - $Finance);

        // Year 7 Data
        $data['OldBill_yer7']    = $OldBill_yer7            = $OldBill_yer6 * 1.05;
        $data['NewBill_yer7']    = $NewBill_yer7            = $NewBill_yer6 * 1.05;
        $data['NoUpgrade_yer7']  = $NoUpgrade_yer7          = $NoUpgrade_yer6 + $OldBill_yer7;
        $data['LEDUpgrade_yer7'] = $LEDUpgrade_yer7         = $LEDUpgrade_yer6 + $NewBill_yer7;
        $data['OldBill2_yer7']   = $OldBill2_yer7           = abs($OldBill_yer7 - $NewBill_yer7 - $Finance);

        // Year 8 Data
        $data['OldBill_yer8']    = $OldBill_yer8            = $OldBill_yer7 * 1.05;
        $data['NewBill_yer8']    = $NewBill_yer8            = $NewBill_yer7 * 1.05;
        $data['NoUpgrade_yer8']  = $NoUpgrade_yer8          = $NoUpgrade_yer7 + $OldBill_yer8;
        $data['LEDUpgrade_yer8'] = $LEDUpgrade_yer8         = $LEDUpgrade_yer7 + $NewBill_yer8;
        $data['OldBill2_yer8']   = $OldBill2_yer8           = abs($OldBill_yer8 - $NewBill_yer8 - $Finance);

        // Year 9 Data
        $data['OldBill_yer9']    = $OldBill_yer9            = $OldBill_yer8 * 1.05;
        $data['NewBill_yer9']    = $NewBill_yer9            = $NewBill_yer8 * 1.05;
        $data['NoUpgrade_yer9']  = $NoUpgrade_yer9          = $NoUpgrade_yer8 + $OldBill_yer9;
        $data['LEDUpgrade_yer9'] = $LEDUpgrade_yer9         = $LEDUpgrade_yer8 + $NewBill_yer9;
        $data['OldBill2_yer9']   = $OldBill2_yer9           = abs($OldBill_yer9 - $NewBill_yer9 - $Finance);

        // Year 10 Data
        $data['OldBill_yer10']    = $OldBill_yer10            = $OldBill_yer9 * 1.05;
        $data['NewBill_yer10']    = $NewBill_yer10            = $NewBill_yer9 * 1.05;
        $data['NoUpgrade_yer10']  = $NoUpgrade_yer10          = $NoUpgrade_yer9 + $OldBill_yer10;
        $data['LEDUpgrade_yer10'] = $LEDUpgrade_yer10         = $LEDUpgrade_yer9 + $NewBill_yer10;
        $data['OldBill2_yer10']   = $OldBill2_yer10           = abs($OldBill_yer10 - $NewBill_yer10 - $Finance);


        // Upfront
        $data['Upfront_oldbill_yer1']  = $Upfront_oldbill_yer1          = $OldBill_yer1 - $calculated_data['total_investment_exGST'] - $NewBill_yer1;
        $data['Upfront_oldbill_yer2']  = $Upfront_oldbill_yer2          = $OldBill_yer2 - $NewBill_yer2;
        $data['Upfront_oldbill_yer3']  = $Upfront_oldbill_yer3          = $OldBill_yer3 - $NewBill_yer3;
        $data['Upfront_oldbill_yer4']  = $Upfront_oldbill_yer4          = $OldBill_yer4 - $NewBill_yer4;
        $data['Upfront_oldbill_yer5']  = $Upfront_oldbill_yer5          = $OldBill_yer5 - $NewBill_yer5;
        $data['Upfront_oldbill_yer6']  = $Upfront_oldbill_yer6          = $OldBill_yer6 - $NewBill_yer6;
        $data['Upfront_oldbill_yer7']  = $Upfront_oldbill_yer7          = $OldBill_yer7 - $NewBill_yer7;
        $data['Upfront_oldbill_yer8']  = $Upfront_oldbill_yer8          = $OldBill_yer8 - $NewBill_yer8;
        $data['Upfront_oldbill_yer9']  = $Upfront_oldbill_yer9          = $OldBill_yer9 - $NewBill_yer9;
        $data['Upfront_oldbill_yer10'] = $Upfront_oldbill_yer10         = $OldBill_yer10 - $NewBill_yer10;

        $data['YR1']  = $YR1          = round($calculated_data['energy_savings_pa']);
        $data['YR2']  = $YR2          = round($YR1 * 1.05);
        $data['YR3']  = $YR3          = round($YR2 * 1.05);
        $data['YR4']  = $YR4          = round($YR3 * 1.05);
        $data['YR5']  = $YR5          = round($YR4 * 1.05);
        $data['YR6']  = $YR6          = round($YR5 * 1.05);
        $data['YR7']  = $YR7          = round($YR6 * 1.05);
        $data['YR8']  = $YR8          = round($YR7 * 1.05);
        $data['YR9']  = $YR9          = round($YR8 * 1.05);
        $data['YR10'] = $YR10         = round($YR9 * 1.05);

        $data['MaintanceSavings_YR1']  = $MaintanceSavings_YR1          = round($calculated_data['maintance_savings_pa']);
        $data['MaintanceSavings_YR2']  = $MaintanceSavings_YR2          = round($MaintanceSavings_YR1 * 1.02);
        $data['MaintanceSavings_YR3']  = $MaintanceSavings_YR3          = round($MaintanceSavings_YR2 * 1.02);
        $data['MaintanceSavings_YR4']  = $MaintanceSavings_YR4          = round($MaintanceSavings_YR3 * 1.02);
        $data['MaintanceSavings_YR5']  = $MaintanceSavings_YR5          = round($MaintanceSavings_YR4 * 1.02);
        $data['MaintanceSavings_YR6']  = $MaintanceSavings_YR6          = round($MaintanceSavings_YR5 * 1.02);
        $data['MaintanceSavings_YR7']  = $MaintanceSavings_YR7          = round($MaintanceSavings_YR6 * 1.02);
        $data['MaintanceSavings_YR8']  = $MaintanceSavings_YR8          = round($MaintanceSavings_YR7 * 1.02);
        $data['MaintanceSavings_YR9']  = $MaintanceSavings_YR9          = round($MaintanceSavings_YR8 * 1.02);
        $data['MaintanceSavings_YR10'] = $MaintanceSavings_YR10         = round($MaintanceSavings_YR9 * 1.02);

        $data['TotalSavings_YR1']  = $TotalSavings_YR1          = $YR1 + $MaintanceSavings_YR1;
        $data['TotalSavings_YR2']  = $TotalSavings_YR2          = $YR2 + $MaintanceSavings_YR2;
        $data['TotalSavings_YR3']  = $TotalSavings_YR3          = $YR3 + $MaintanceSavings_YR3;
        $data['TotalSavings_YR4']  = $TotalSavings_YR4          = $YR4 + $MaintanceSavings_YR4;
        $data['TotalSavings_YR5']  = $TotalSavings_YR5          = $YR5 + $MaintanceSavings_YR5;
        $data['TotalSavings_YR6']  = $TotalSavings_YR6          = $YR6 + $MaintanceSavings_YR6;
        $data['TotalSavings_YR7']  = $TotalSavings_YR7          = $YR7 + $MaintanceSavings_YR7;
        $data['TotalSavings_YR8']  = $TotalSavings_YR8          = $YR8 + $MaintanceSavings_YR8;
        $data['TotalSavings_YR9']  = $TotalSavings_YR9          = $YR9 + $MaintanceSavings_YR9;
        $data['TotalSavings_YR10'] = $TotalSavings_YR10         = $YR10 + $MaintanceSavings_YR10;


        $data['OldBill_yer1'] = $projectCost          = $calculated_data['total_discount_veec_exGST'] + $calculated_data['total_investment_exGST'];

        $year = 0;

        if ($led_proposal_data['term'] == 12) {
            $year = 1;
        }
        if ($led_proposal_data['term'] == 24) {
            $year = 2;
        }
        if ($led_proposal_data['term'] == 36) {
            $year = 3;
        }
        if ($led_proposal_data['term'] == 48) {
            $year = 4;
        }
        if ($led_proposal_data['term'] == 60) {
            $year = 5;
        }
        if ($led_proposal_data['term'] == 72) {
            $year = 6;
        }
        if ($led_proposal_data['term'] == 84) {
            $year = 7;
        }

        $YR = round($calculated_data['energy_savings_pa']);

        $monthlyPaymentPlan                   = $led_proposal_data['monthly_payment_plan'] * 12;
        $countt                               = 0;
        $TotalRevenue                         = 0;
        $TotalUpfront                         = 0;
        $TotalCashflow                        = 0;
        $data['year_1_cashflow_on_repayment'] = 0;
        for ($t = 1; $t <= 10; $t++) {
            $countt       = $countt + 1;
            $fin          = 0;
            //$Cashflow = "0";
            $TotalRevenue = $TotalRevenue + $YR;

            if ($t <= $year) {
                $fin      = round($monthlyPaymentPlan);
                $Cashflow = $YR - $fin;
            } else {
                $Cashflow = $YR - $fin;
            }

            $TotalUpfront  = $TotalUpfront + $fin;
            $TotalCashflow = $TotalCashflow + $Cashflow;
            if ($t == 1) {
                $data['year_1_cashflow_on_repayment'] = $year_1_cashflow_on_repayment         = $TotalCashflow;
            }
            $YR = round($YR * 1.05);
        }

        // Total Saving over Lifetime
        $YRLifeTime      = round($calculated_data['energy_savings_pa']);
        $YRLifeTimeTotal = 0;

        for ($z = 1; $z <= 25; $z++) {
            $YRLifeTimeTotal = $YRLifeTimeTotal + $YRLifeTime;
            $YRLifeTime      = round($YRLifeTime * 1.05);
        }

        return $data;
    }

    /** LED Proposal Related Functions END */

    /** SOLAR Proposal Related Functions Start */
    public function save_solar_proposal()
    {

        if (!empty($this->input->post())) {
            $stat             = $stat1    = $stat2        = FALSE;
            $solar_proposal   = $this->input->post('solar_proposal');
            $solar_panel_data = $this->input->post('solar_panel_data');
            $proposal_id      = $this->input->post('proposal_id');
            $financial_selection      = $this->input->post('financial_selection');
            $solar_financial      = $this->input->post('solar_financial');

            //Convert base64img to png
            if (isset($solar_proposal['image'])) {
                $img = explode(';', $solar_proposal['image']);
                if ($solar_proposal['image'] != '' && count($img) > 1) {
                    $image                   = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $solar_proposal['image']));
                    $solar_proposal['image'] = $newfilename             = round(microtime(true)) . '.png';
                    file_put_contents('./assets/uploads/proposal_files/' . $newfilename, $image);
                }
            }

            //Encode Rate1,Rate2,Rate3 Data
            if (isset($solar_proposal['rate1_data'])) {
                $solar_proposal['rate1_data'] = isset($solar_proposal['rate1_data']) ? json_encode($solar_proposal['rate1_data']) : NULL;
            }
            if (isset($solar_proposal['rate2_data'])) {
                $solar_proposal['rate2_data'] = isset($solar_proposal['rate2_data']) ? json_encode($solar_proposal['rate2_data']) : NULL;
            }
            if (isset($solar_proposal['rate3_data'])) {
                $solar_proposal['rate3_data'] = isset($solar_proposal['rate3_data']) ? json_encode($solar_proposal['rate3_data']) : NULL;
            }
            if (isset($solar_proposal['rate4_data'])) {
                $solar_proposal['rate4_data'] = isset($solar_proposal['rate4_data']) ? json_encode($solar_proposal['rate4_data']) : NULL;
            }
            if (isset($proposal_id) && $proposal_id != '') {
                if (isset($solar_proposal['additional_items'])) {
                    $solar_proposal['additional_items'] = json_encode($solar_proposal['additional_items']);
                }

                if (isset($solar_proposal['additional_notes'])) {
                    $notes = [];
                    $notes['additional_notes'] = $solar_proposal['additional_notes'];
                    $notes = $this->components->escape_data($notes);
                    $solar_proposal['additional_notes'] = $notes['additional_notes'];
                }

                $upload_financial_data = [];
                $upload_financial_data['proposal_id'] = $proposal_id;
                $upload_financial_data['type'] = $financial_selection;

                // Saving data for VEEC
                if ($financial_selection == "VEEC") {
                    $solar_proposal['stc'] = 1079.00;
                    $proposal_finance = $this->input->post('proposal_finance');
                    $ppa_checkbox_3 = 1;
                    $ppa_checkbox_4 = 1;

                    if (!isset($proposal_finance['ppa_checkbox_2'])) {
                        $ppa_checkbox_3 = 0;
                    } else {
                        $ppa_checkbox_3 = 1;
                    }

                    if (!isset($proposal_finance['ppa_checkbox_3'])) {
                        $ppa_checkbox_4 = 0;
                    } else {
                        $ppa_checkbox_4 = 1;
                    }


                    $upload_financial_data['ppa_checkbox'] = json_encode(array(
                        $ppa_checkbox_3,
                        $ppa_checkbox_4
                    ));

                    $upload_financial_data['ppa_term'] = json_encode(array(
                        $solar_financial['ppa_term_2'],
                        $solar_financial['ppa_term_3']
                    ));
                    $upload_financial_data['ppa_rate'] = json_encode(array(
                        $solar_financial['ppa_rate_2'],
                        $solar_financial['ppa_rate_3']
                    ));
                    $upload_financial_data['project_cost'] = json_encode(array(
                        $solar_financial['project_cost_2'],
                        $solar_financial['project_cost_3']
                    ));
                    $upload_financial_data['term'] = json_encode(array(
                        $proposal_finance['term_2'],
                        $proposal_finance['term_3']
                    ));
                    $upload_financial_data['upfront_payment_options'] = json_encode(array(
                        $solar_financial['upfront_payment_options_2'],
                        $proposal_finance['upfront_payment_options_3']
                    ));
                    $upload_financial_data['monthly_payment_plan'] = json_encode(array(
                        $proposal_finance['monthly_payment_plan_2'],
                        $proposal_finance['monthly_payment_plan_3']
                    ));
                    $upload_financial_data['veec_discount'] = $solar_financial['veec_discount'];

                    $upfront_payment_deposit = array(
                        array(
                            $solar_financial['veec_upfront_payment_opt_per_2_1'],
                            $solar_financial['veec_upfront_payment_opt_price_2_1']
                        ), array(
                            $solar_financial['veec_upfront_payment_opt_per_2_2'],
                            $solar_financial['veec_upfront_payment_opt_price_2_2']
                        ), array(
                            $solar_financial['veec_upfront_payment_opt_per_2_3'],
                            $solar_financial['veec_upfront_payment_opt_price_2_3']
                        ),
                        array(
                            $solar_financial['veec_upfront_payment_opt_per_3_1'],
                            $solar_financial['veec_upfront_payment_opt_price_3_1']
                        ), array(
                            $solar_financial['veec_upfront_payment_opt_per_3_2'],
                            $solar_financial['veec_upfront_payment_opt_price_3_2']
                        ), array(
                            $solar_financial['veec_upfront_payment_opt_per_3_3'],
                            $solar_financial['veec_upfront_payment_opt_price_3_3']
                        )
                    );
                    $upload_financial_data['upfront_payment_deposit'] = json_encode($upfront_payment_deposit);
                    $upload_financial_data['veec_type'] = implode(',', $this->input->post('veec_type'));
                } else {
                    $proposal_finance = $this->input->post('proposal_finance');

                    if (!isset($proposal_finance['ppa_checkbox'])) {
                        $upload_financial_data['ppa_checkbox'] = 0;
                    } else {
                        $upload_financial_data['ppa_checkbox'] = 1;
                    }

                    $upload_financial_data['ppa_rate'] = $solar_financial['ppa_rate'];
                    $upload_financial_data['ppa_term'] = $solar_financial['ppa_term'];

                    if ($financial_selection == "LGC") {
                        $upfront_payment_deposit = array(
                            array(
                                $solar_financial['lgc_upfront_payment_opt_per_1'],
                                $solar_financial['lgc_upfront_payment_opt_price_1']
                            ), array(
                                $solar_financial['lgc_upfront_payment_opt_per_2'],
                                $solar_financial['lgc_upfront_payment_opt_price_2']
                            ), array(
                                $solar_financial['lgc_upfront_payment_opt_per_3'],
                                $solar_financial['lgc_upfront_payment_opt_price_3']
                            )
                        );
                    } else {
                        $upfront_payment_deposit = array(
                            array(
                                $solar_financial['stc_upfront_payment_opt_per_1'],
                                $solar_financial['stc_upfront_payment_opt_price_1']
                            ), array(
                                $solar_financial['stc_upfront_payment_opt_per_2'],
                                $solar_financial['stc_upfront_payment_opt_price_2']
                            ), array(
                                $solar_financial['stc_upfront_payment_opt_per_3'],
                                $solar_financial['stc_upfront_payment_opt_price_3']
                            )
                        );
                    }
                    $upload_financial_data['upfront_payment_deposit'] = json_encode($upfront_payment_deposit);
                }

                if (isset($solar_financial['lgc_pricing'])) {
                    $upload_financial_data['lgc_pricing'] = json_encode($solar_financial['lgc_pricing']);
                }
                $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
                if (count($financial_summary_data)) {
                    $stat2 = $this->common->update_data('tbl_solar_proposal_financial_summary', ['proposal_id' => $proposal_id], $upload_financial_data);
                } else {
                    $stat2 = $this->common->insert_data('tbl_solar_proposal_financial_summary', $upload_financial_data);
                }


                // print_r($solar_proposal);die;
                $stat = $this->common->update_data('tbl_solar_proposal', ['id' => $proposal_id], $solar_proposal);
                if (isset($solar_panel_data) && !empty($solar_panel_data)) {
                    if (isset($solar_panel_data['panel_orientation'])) {
                        $stat1 = $this->handle_solar_panel_data($proposal_id);
                    }
                }
            }
            if ($stat || $stat1 || $stat2) {
                $data['status']  = 'Proposal Data Saved successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    private function handle_solar_panel_data($proposal_id)
    {
        $solar_panel_update_data = array();
        $solar_panel_insert_data = array();
        $solar_panel_data        = $this->input->post('solar_panel_data');

        //Bulk Update Data
        $solar_panel_existing_data = $this->common->fetch_where('tbl_solar_proposal_panel_data', '*', ['proposal_id' => $proposal_id]);
        $count                     = count($solar_panel_existing_data);

        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                if (isset($solar_panel_data['no_of_panels'][$i])) {
                    $solar_panel_update_data[$i]['no_of_panels']      = $solar_panel_data['no_of_panels'][$i];
                    $solar_panel_update_data[$i]['panel_orientation'] = $solar_panel_data['panel_orientation'][$i];
                    $solar_panel_update_data[$i]['panel_tilt']        = $solar_panel_data['panel_tilt'][$i];
                    $solar_panel_update_data_cond[$i]['id']           = $solar_panel_existing_data[$i]['id'];
                    $stat                                             = $this->common->update_data('tbl_solar_proposal_panel_data', $solar_panel_update_data_cond[$i], $solar_panel_update_data[$i]);
                }
            }
        }

        //Bulk Insert Data
        for ($j = $count, $k = 0; $j < count($solar_panel_data['no_of_panels']); $j++, $k++) {
            $solar_panel_insert_data[$k]['no_of_panels']      = $solar_panel_data['no_of_panels'][$j];
            $solar_panel_insert_data[$k]['panel_orientation'] = $solar_panel_data['panel_orientation'][$j];
            $solar_panel_insert_data[$k]['panel_tilt']        = $solar_panel_data['panel_tilt'][$j];
            $solar_panel_insert_data[$k]['proposal_id']       = $proposal_id;
            $stat                                             = $this->common->insert_data('tbl_solar_proposal_panel_data', $solar_panel_insert_data[$k]);
        }

        //Check if delete Required
        if ($count > count($solar_panel_data['no_of_panels'])) {
            for ($l = count($solar_panel_data['no_of_panels']), $m = 0; $l < $count; $l++, $m++) {
                $solar_panel_cond[$m]['id'] = $solar_panel_existing_data[$l]['id'];
                $stat                       = $this->common->delete_data('tbl_solar_proposal_panel_data', $solar_panel_cond[$m]);
            }
        }

        return $stat;
    }

    public function save_solar_proposal_products()
    {
        if (!empty($this->input->post())) {
            $solar_product    = $this->input->post('solar_product');
            $solar_product_id = $this->input->post('solar_product_id');
            if (isset($solar_product_id) && $solar_product_id != '') {
                $stat = $this->common->update_data('tbl_solar_proposal_products', ['id' => $solar_product_id], $solar_product);
            } else {
                $stat = $this->common->insert_data('tbl_solar_proposal_products', $solar_product);
            }
            if ($stat) {
                $data['status']       = 'Solar Proposal Product saved successfully';
                $data['success']      = TRUE;
                $data['product_data'] = array();
                if (isset($solar_product) && $solar_product['proposal_id']) {
                    $data['product_data'] = $this->product->get_solar_proposal_products($solar_product['proposal_id']);
                }
            } else {
                $data['status']  = 'Looks like nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function delete_solar_proposal_products()
    {
        if (!empty($this->input->post())) {
            $stat             = FALSE;
            $solar_product_id = $this->input->post('solar_product_id');
            $proposal_id      = $this->input->post('proposal_id');
            if (isset($solar_product_id) && $solar_product_id != '' && isset($proposal_id) && $proposal_id != '') {
                $stat = $this->common->delete_data('tbl_solar_proposal_products', ['id' => $solar_product_id, 'proposal_id' => $proposal_id]);
            }
            if ($stat) {
                $data['status']  = 'Solar Proposal Product deleted successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like something went wrong';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_solar_proposal_products()
    {
        $data = array();
        if (!empty($this->input->get())) {
            $proposal_id          = $this->input->get('proposal_id');
            $data['product_data'] = array();
            $data['product_data'] = $this->product->get_solar_proposal_products($proposal_id);
            $data['success']      = TRUE;
        } else {
            $data['success'] = FALSE;
            $data['status']  = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function fetch_solar_proposal_details()
    {
        $data          = array();
        $proposal_data = array();
        if (!empty($this->input->get())) {
            $proposal_id                       = $this->input->get('proposal_id');
            //$calculated_data = $this->calculate_solar_proposal_cost($proposal_id);
            //$proposal_data['calculated_data'] = $calculated_data;
            $proposal_data                     = $this->proposal->get_solar_proposal_data(FALSE, FALSE, $proposal_id);
            $proposal_data['additional_items'] = ($proposal_data['additional_items'] != NULL) ? json_decode($proposal_data['additional_items']) : NULL;
            $proposal_data['rate1_data']       = ($proposal_data['rate1_data'] != NULL) ? json_decode($proposal_data['rate1_data']) : NULL;
            $proposal_data['rate2_data']       = ($proposal_data['rate2_data'] != NULL) ? json_decode($proposal_data['rate2_data']) : NULL;
            $proposal_data['rate3_data']       = ($proposal_data['rate3_data'] != NULL) ? json_decode($proposal_data['rate3_data']) : NULL;
            $proposal_data['rate4_data']       = ($proposal_data['rate4_data'] != NULL) ? json_decode($proposal_data['rate4_data']) : NULL;
            if (!empty($proposal_data)) {
                $data['proposal_data'] = $proposal_data;
                $data['success']       = TRUE;
            } else {
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
        die;
    }

    public function solar_pdf_download($proposal_id)
    {
        $askingFinancialInformation = isset($_GET['api_invoker']) ? true : false;

        try {
            //Check if downloading user is same as creating user
            $user_id        = $this->aauth->get_user_id();
            $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
            $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
            $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
            $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);
            if (!$is_access) {
                //$this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access Not appropriate User to download the proposal.</div>');
                //redirect($this->redirect_url);
                //exit();
            }
            //Check for validation
            $flag          = true;
            $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

            if ($proposal_data['monthly_electricity_bill'] == NULL || $proposal_data['monthly_electricity_bill'] == 0 || $proposal_data['monthly_electricity_bill'] == '') {
                $flag   = false;
                $status = 'Monthly Electricity Bill Cannot be Zero or left blank';
            } else if ($proposal_data['rate'] == NULL || $proposal_data['rate'] == '') {
                $flag   = false;
                $status = 'Electricity Rate Cannot be left blank';
            } else if ($proposal_data['rate'] == NULL || $proposal_data['rate'] == '') {
                $flag   = false;
                $status = 'Electricity Rate Cannot be left blank';
            } else if ($proposal_data['daily_usage_charge'] == NULL || $proposal_data['daily_usage_charge'] == '') {
                $flag   = false;
                $status = 'Daily Usage Charge Cannot be left blank';
            } else if ($proposal_data['feed_in_tariff'] == NULL || $proposal_data['feed_in_tariff'] == '') {
                $flag   = false;
                $status = 'Feed in Tariff Cannot be left blank';
            } else if ((trim($proposal_data['near_map_data']) == NULL || trim($proposal_data['near_map_data']) == '') && $proposal_data['nearmap_toggle'] == 1) {
                $flag   = false;
                $status = 'Panel setup is enabled using nearmap tool, Please draw panels on nearmap tool to download proposal';
            }

            if ($this->input->is_ajax_request() && !$flag && !$askingFinancialInformation) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status'  => $status,
                ));
                die;
            }
            if ($this->input->is_ajax_request() && $flag && !$askingFinancialInformation) {
                echo json_encode(array(
                    'success' => TRUE
                ));
                die;
            }

            if ($askingFinancialInformation && $flag == false) {
                echo json_encode(array(
                    'status'  => $status,
                    'success' => FALSE
                ));
                die;
            }
            $data                 = array();
            $data                 = $this->calculate_solar_proposal_cost($proposal_id);
            $data['lead_data']    = $lead_data;
            $data['product_data'] = array();
            $data['product_data'] = $product_data         = $this->product->get_solar_proposal_products($proposal_id);

            $data['baseurl']            = $baseurl                    = $this->config->item('live_url');
            //$data['baseurl'] = $baseurl = site_url();
            $data['static_images_path'] = $baseurl . 'assets/pdf_images/';
            $data['image_path']         = $baseurl . 'assets/uploads/proposal_files/';

            //Panel ,Inverter Data
            $panel    = array();
            $inverter = array();
            foreach ($product_data as $key => $value) {
                if ($value['type_id'] == 9) {
                    $panel = $value;
                } else if ($value['type_id'] == 11 || $value['type_id'] == 12 || $value['type_id'] == 13 || $value['type_id'] == 14) {
                    $inverter = $value;
                }
            }
            $data['panel']                = $panel;
            $data['inverter']             = $inverter;
            $data['footer_static_number'] = '13 58 42';
            $data['solar_energy_rate'] = 0;
            if ($data['AverageDailyProduction'] > 0) {
                $data['solar_energy_rate'] = number_format((($data['proposal_data']['price_before_stc_rebate'] / 10) / ($data['AverageDailyProduction'] * 12)), '2', '.', '') . 'c/kWh';
            }
            if ($askingFinancialInformation) {
                echo json_encode(array(
                    'data' => $data,
                    'success' => TRUE
                ));
                die;
            }

            $view = $this->input->get('view');
            if (isset($view) && $view == 'html') {
                $html  = $this->load->view('pdf_files/solar/chart_images', $data);
            } else {
                $html                         = $this->load->view('pdf_files/solar/page1', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page2', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page3', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page4', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page5', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page6', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page7', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page8', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page9', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page10', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page11', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page12', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page13', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page14', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page15', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page16', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page17', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page18', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page19', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page20', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page21', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/thank_page', $data, TRUE);

                if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
                    //echo '<pre>';print_r($data);die;
                    //echo $html;die;
                }
                //echo $html;die;
                $customer_name = $lead_data['first_name'] . "_" . $lead_data['last_name'];
                $filename      = "KugaElectrical-SOLAR_Quote_" . $customer_name . "-" . date('Y-m-d');

                $filename    = str_replace(array('.', ',', ' '), '_', $filename);
                $h           = '';
                $f           = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source"      => $html,
                    "action"      => 'download',
                    //"save_directory" => '',
                    "file_name"   => $filename . '.pdf',
                    "page_size"   => 'A4',
                    "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                    "header"      => $h,
                    "footer"      => $f
                );
                $this->pdf->phptopdf($pdf_options);
            }
        } catch (Exception $e) {
            echo 'Looks like some error occured while downloading the proposal. Please contact support.';
            die;
        }
    }

    public function calculate_solar_proposal_cost($proposal_id)
    {
        $data                           = array();
        //Get Proposal, Proposal Product and Proposal Additional Item Data
        $proposal_data                  = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

        // we are giving input for daily consumption rather than monthly so now we need to calculate monty consumption , asked by nishan

        $proposal_data['monthly_electricity_usage'] = ($proposal_data['monthly_electricity_usage'] * 365) / 12;
        $data['proposal_data']  = $proposal_data;



        //print_r($proposal_data);die;
        $data['customer_location_data'] = $customer_location_data         = $this->common->fetch_row('tbl_customer_locations', '*', array('id' => $proposal_data['site_id']));

        //Get Latest Area Id
        $area_data = $this->common->fetch_row('tbl_solar_areas', 'areaId,kwh_day', array('state_id' => $customer_location_data['state_id'], 'areaStatus' => '1'));
        $area_id = $area_data['areaId'];
        $proposal_data['area_id'] = $area_id;

        $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_id), array('area_id' => $area_id));

        $AnnualSolarData               = $this->common->fetch_where('tbl_annual_solar_data_new', '*', NULL);
        $data['getRebate']             = $getRebate                     = $this->common->fetch_row('tbl_rebate', '*', array('rebateId' => $customer_location_data['state_id']));
        $data['proposal_finance_data'] = $proposal_finance_data         = $this->common->fetch_row('tbl_proposal_finance', '*', array('proposal_id' => $proposal_id, 'proposal_type' => 2));
        $additional_items              = json_decode($proposal_data['additional_items'], true);

        if ($additional_items['tiltSystem'] == "1") {
            $tiltSystem = "INCLUDED";
        } else if ($additional_items['tiltSystem'] == "2") {
            $tiltSystem = "NOT INCLUDED";
        } else {
            $tiltSystem = "NOT APPLICABLE";
        }
        $data['tiltSystem'] = $tiltSystem;

        if ($additional_items['switchBoard'] == "1") {
            $switchBoard = "INCLUDED";
        } else if ($additional_items['switchBoard'] == "2") {
            $switchBoard = "NOT INCLUDED";
        } else {
            $switchBoard = "NOT APPLICABLE";
        }
        $data['switchBoard'] = $switchBoard;

        if ($additional_items['gridProtectionEquipment'] == "1") {
            $gridProtectionEquipment = "INCLUDED";
        } else if ($additional_items['gridProtectionEquipment'] == "2") {
            $gridProtectionEquipment = "NOT INCLUDED";
        } else {
            $gridProtectionEquipment = "NOT APPLICABLE";
        }
        $data['gridProtectionEquipment'] = $gridProtectionEquipment;

        if ($additional_items['engineeringReport'] == "1") {
            $engineeringReport = "INCLUDED";
        } else if ($additional_items['engineeringReport'] == "2") {
            $engineeringReport = "NOT INCLUDED";
        } else {
            $engineeringReport = "NOT APPLICABLE";
        }
        $data['engineeringReport'] = $engineeringReport;

        if ($additional_items['networkReport'] == "1") {
            $networkReport = "INCLUDED";
        } else if ($additional_items['networkReport'] == "2") {
            $networkReport = "NOT INCLUDED";
        } else {
            $networkReport = "NOT APPLICABLE";
        }
        $data['networkReport'] = $networkReport;

        if ($additional_items['TVInstallation'] == "1") {
            $TVInstallation = "INCLUDED";
        } else if ($additional_items['TVInstallation'] == "2") {
            $TVInstallation = "NOT INCLUDED";
        } else {
            $TVInstallation = "NOT APPLICABLE";
        }
        $data['TVInstallation'] = $TVInstallation;

        /*
          Adelaide = 1
          Brisbane = 3
          Canberra = 5
          Darwin = 6
          Hobart = 7
          Melbourne = 8
          Perth = 9
          Sydney = 10
         */
        if ($proposal_data['area_id'] == 1) {
            $dataTable = "adelaide";
        } elseif ($proposal_data['area_id'] == 3) {
            $dataTable = "brisbane";
        } elseif ($proposal_data['area_id'] == 5) {
            $dataTable = "canberra";
        } elseif ($proposal_data['area_id'] == 6) {
            $dataTable = "darwin";
        } elseif ($proposal_data['area_id'] == 7) {
            $dataTable = "hobart";
        } elseif ($proposal_data['area_id'] == 8) {
            $dataTable = "melbourne";
        } elseif ($proposal_data['area_id'] == 9) {
            $dataTable = "perth";
        } elseif ($proposal_data['area_id'] == 10) {
            $dataTable = "sydney";
        }

        #############################################################################
        //Consumption profile without Solar split into working / non working days 
        ##############################################################################
        //$data['solar_client_panel_data'] = $panel_data = $this->common->fetch_where('tbl_solar_proposal_panel_data', '*', array('proposal_id' => $proposal_id));

        $panel_data         = json_decode($proposal_data['near_map_data'], true);
        $getOrientationTilt = 0;
        // print_r($panel_data);die;

        if (!empty($panel_data['tiles']) && $proposal_data['nearmap_toggle']) {
            $panel_data['tiles'] = (array) $panel_data['tiles'];
            $no_of_panels        = count($panel_data['tiles']);

            for ($i = 0; $i < count($panel_data['tiles']); $i++) {
                $panel_data['tiles'][$i] = (array) $panel_data['tiles'][$i];
                $orientation             = is_numeric($panel_data['tiles'][$i]['sp_rotation']) ? $panel_data['tiles'][$i]['sp_rotation'] : 0;
                $orientation             = round($orientation / 10) * 10;
                $orientation             = ($orientation > 350) ? $orientation - 360 : $orientation;
                $tilt                    = is_numeric($panel_data['tiles'][$i]['sp_tilt']) ? $panel_data['tiles'][$i]['sp_tilt'] : 0;
                $watt                    = is_numeric($panel_data['tiles'][$i]['sp_watt']) ? $panel_data['tiles'][$i]['sp_watt'] : 0;
                $orientation_tilt        = $this->common->fetch_row('tbl_tilt_and_orientation_2020_data', '*', array('areaId' => $proposal_data['area_id'], 'orientationValue' => $orientation));
                if (!empty($orientation_tilt)) {
                    if ($tilt % 5 == 0) {
                        $getOrientationTilt += $orientation_tilt['tilt' . $tilt];
                        //print_r($getOrientationTilt);die;
                    } else {
                        $x = 5; //Factor of 5 
                        $tilt1                 = (round($tilt) % $x === 0) ? round($tilt) : round(($tilt + $x / 2) / $x) * $x;
                        $tilt2                 = ($tilt1 > 0) ? $tilt1 - 5 : $tilt1;
                        $orientation_tilt_val1 = $orientation_tilt['tilt' . $tilt1];
                        $orientation_tilt_val2 = $orientation_tilt['tilt' . $tilt2];
                        $orientation_tilt_val  = ($orientation_tilt_val1 + $orientation_tilt_val2) / 2;
                        $getOrientationTilt    += $orientation_tilt_val;
                    }
                }
            }

            if (count($panel_data['tiles']) > 0) {
                $getOrientationTilt = $getOrientationTilt / count($panel_data['tiles']);
            }
        } else {

            $data['solar_client_panel_data'] = $panel_data = $this->common->fetch_where('tbl_solar_proposal_panel_data', '*', array('proposal_id' => $proposal_id));
            $tilt = 0;
            $orientation = 0;

            if (!empty($panel_data)) {
                $orientation = $panel_data[0]['panel_orientation'];
                $tilt = $panel_data[0]['panel_tilt'];
            }
            $getOrientationTiltArr = $this->common->fetch_row('tbl_tilt_and_orientation_2020_data', '*', array('areaId' => $proposal_data['area_id'], 'orientationValue' => $orientation));
            $solarTilt = (int) $tilt;
            $getOrientationTilt_new = 0;
            //We Will have to decide here wether we want average or sum
            if (!empty($panel_data)) {
                for ($pd = 1; $pd < count($panel_data); $pd++) {
                    $getOrientationTilt_new = $this->common->fetch_row('tbl_tilt_and_orientation_2020_data', '*', array('areaId' => $proposal_data['area_id'], 'orientationValue' => $orientation = $panel_data[$pd]['panel_orientation']));
                    $tilt_val = (int) $panel_data[$pd]['panel_tilt'];
                    $getOrientationTilt = $getOrientationTiltArr['tilt' . $solarTilt] + $getOrientationTilt_new['tilt' . $tilt_val];
                }
            }
            if (count($panel_data) == 1) {
                $getOrientationTilt = $getOrientationTiltArr['tilt' . $solarTilt];
            }
        }
        $jh                                                               = [];
        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = array();

        if ($proposal_data['is_meter_data']) {
            if ($proposal_data['meter_data_file'] != NULL) {
                $final_data                                                       = $this->read_meter_data_file($proposal_data['meter_data_file'], false);
                $meter_data                                                       = $this->calculate_consumption_profile_data_by_meter_data($proposal_data, $final_data);
                $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = $meter_data['consumption_data'];
                if (count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr) != 8760) {
                    echo "Looks like some issue in meter data can't generate the pdf successfully";
                    die;
                }
                for ($r = 0; $r < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $r++) {
                    $AnnualSolarDataByArea                              = $AnnualSolarData[$r][$dataTable];
                    $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[] = $AnnualSolarDataByArea * $getOrientationTilt * ($proposal_data['total_system_size'] / 100);
                }
            } else {
                $consumption_profile_data                                         = $this->calculate_consumption_profile_data_by_custom_data($proposal_data);
                $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = $consumption_profile_data['consumption_profile'];
                for ($r = 0; $r < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $r++) {
                    $AnnualSolarDataByArea                              = $AnnualSolarData[$r][$dataTable];
                    $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[] = $AnnualSolarDataByArea * $getOrientationTilt * $proposal_data['total_system_size'] / 100;
                }
            }
        } else {
            $consumption_profile_data = $this->calculate_consumption_profile_data_by_custom_data($proposal_data);
            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = $consumption_profile_data['consumption_profile'];
            for ($r = 0; $r < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $r++) {
                $AnnualSolarDataByArea                              = $AnnualSolarData[$r][$dataTable];
                $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[] = $AnnualSolarDataByArea * $getOrientationTilt * $proposal_data['total_system_size'] / 100;
            }
        }

        //echo "<pre>";
        //print_r(($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr));die;
        //echo array_sum($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr);die;
        //print_r(($DataFromAnnualSolarDataXSizeXOrientationXTiltArr));die;
        #############################################################################
        // *** END *** Consumption profile without Solar split into working / non working days 
        ##############################################################################
        ###################################################################
        // Solar consumption
        ################################################################
        $rate                                                               = $proposal_data['rate'] . "rate";
        $costAfterSolarTotal                                                = $costBeforeSolarTotal                                               = $exportTotal                                                        = $exportIncomeTotal                                                  = $SolarConsumptionTotal                                              = 0;
        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal = 0;


        $rate1_data  = ($proposal_data['rate1_data'] != NULL) ? json_decode($proposal_data['rate1_data']) : NULL;
        $rate2_data  = ($proposal_data['rate2_data'] != NULL) ? json_decode($proposal_data['rate2_data']) : NULL;
        $rate3_data  = ($proposal_data['rate3_data'] != NULL) ? json_decode($proposal_data['rate3_data']) : NULL;
        $rate4_data  = ($proposal_data['rate4_data'] != NULL) ? json_decode($proposal_data['rate4_data']) : NULL;
        $rate1_times = $rate2_times = $rate3_times = $rate4_times = array();

        if ($rate1_data != NULL && $rate1_data->start_time != '' && $rate1_data->finish_time) {
            $start = explode(':', $rate1_data->start_time);
            $start = (int) $start[0];
            $end   = explode(':', $rate1_data->finish_time);
            $end   = (int) $end[0];
            for ($i = $start; $i <= $end; $i++) {
                $rate1_times[] = ($i < 10) ? '0' . $i . ':00' : $i . ':00';
            }
        } else {
            $start = explode(':', '00:00');
            $start = (int) $start[0];
            $end   = explode(':', '23:00');
            $end   = (int) $end[0];
            for ($i = $start; $i <= $end; $i++) {
                $rate1_times[] = ($i < 10) ? '0' . $i . ':00' : $i . ':00';
            }
        }



        if ($rate2_data != NULL && $rate2_data->start_time != '' && $rate2_data->finish_time) {
            $start = explode(':', $rate2_data->start_time);
            $start = (int) $start[0];
            $end   = explode(':', $rate2_data->finish_time);
            $end   = (int) $end[0];
            for ($i = $start; $i <= $end; $i++) {
                $rate2_times[] = ($i < 10) ? '0' . $i . ':00' : $i . ':00';
            }
        }

        if ($rate3_data != NULL && $rate3_data->start_time != '' && $rate3_data->finish_time) {
            $start = explode(':', $rate3_data->start_time);
            $start = (int) $start[0];
            $end   = explode(':', $rate3_data->finish_time);
            $end   = (int) $end[0];
            for ($i = $start; $i <= $end; $i++) {
                $rate3_times[] = ($i < 10) ? '0' . $i . ':00' : $i . ':00';
            }
        }

        if ($rate4_data != NULL) {
            if ($rate4_data->start_time != '' && $rate4_data->finish_time) {
                $start = explode(':', $rate4_data->start_time);
                $start = (int) $start[0];
                $end   = explode(':', $rate4_data->finish_time);
                $end   = (int) $end[0];
                for ($i = $start; $i <= $end; $i++) {
                    $rate4_times[] = ($i < 10) ? '0' . $i . ':00' : $i . ':00';
                }
            }
        }

        $meter_data_times = array();
        if ($proposal_data['meter_data_file'] == NULL) {
            for ($i = 0; $i < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $i++) {
                $meter_data_dates     = $AnnualSolarData[$i]['dateAndtime'];
                $time                 = explode(' ', $meter_data_dates);
                $time_break           = explode(':', $time[1]);
                $meter_data_times[$i] = ((int) $time_break[0] < 10) ? (((strlen($time_break[0]) == 2)) ? '' : '0') . $time_break[0] . ':00' : $time_break[0] . ':00';
            }
        }
        //print_r($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[5]);die;
        for ($i = 0; $i < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $i++) {
            if ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i] < $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i]) {
                //$SolarConsumptionArr[] = number_format((float) $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i], 2, '.', '');
                $SolarConsumptionArr[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i];
            } else {
                $SolarConsumptionArr[] = number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '');
            }

            if ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i] > $SolarConsumptionArr[$i]) {
                $ConsumptionAfterSolarArr[] = number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '') - $SolarConsumptionArr[$i];
            } else {
                $ConsumptionAfterSolarArr[] = $SolarConsumptionArr[$i] - number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '');
            }

            if ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i] > $SolarConsumptionArr[$i]) {
                $exportArr[] = number_format((float) $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i] - $SolarConsumptionArr[$i], 2, '.', '');
            } else {
                $exportArr[] = 0;
            }

            $exportTotal = $exportTotal + $exportArr[$i];

            $exportIncomeArr[] = $exportArr[$i] * $proposal_data['feed_in_tariff'];
            $exportIncomeTotal = $exportIncomeTotal + $exportIncomeArr[$i];


            if ($proposal_data['is_meter_data'] && $proposal_data['meter_data_file'] != NULL) {
                $meter_data_dates = $meter_data['consumption_date'];
                $meter_data_times = $meter_data['consumption_time'];
                $day              = date('D', strtotime($meter_data_dates[$i]));
                $conv_date        = $meter_data_dates[$i];
                $conv_date        = date('d/m/2019 H:i', strtotime($conv_date));

                $key = array_search($conv_date, array_column($AnnualSolarData, 'dateAndtime'));

                $keys[] = $key;
                if ($AnnualSolarData[$key][$rate] == "Peak") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_peak'];
                } else if ($AnnualSolarData[$key][$rate] == "Off-Peak") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_off_peak'];
                } else if ($AnnualSolarData[$key][$rate] == "Shoulder") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_shoulder'];
                }
            } else {
                $meter_data_dates = $AnnualSolarData[$i]['dateAndtime'];
                $day              = date('D', strtotime($meter_data_dates));
                if ($AnnualSolarData[$i][$rate] == "Peak") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_peak'];
                } else if ($AnnualSolarData[$i][$rate] == "Off-Peak") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_off_peak'];
                } else if ($AnnualSolarData[$i][$rate] == "Shoulder") {
                    $energyRateArr[] = $proposal_data['solar_cost_per_kwh_shoulder'];
                }
            }

            $energyRateVal = $energyRateArr[$i];

            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysVal = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i];

            $costBeforeSolarArr[] = ($energyRateVal * $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysVal);

            $costAfterSolarArr[] = number_format((float) ($energyRateArr[$i] * $ConsumptionAfterSolarArr[$i]), 2, '.', '');

            $costBeforeSolarTotal = $costBeforeSolarTotal + $costBeforeSolarArr[$i];

            $costAfterSolarTotal = $costAfterSolarTotal + $costAfterSolarArr[$i];

            $SolarConsumptionTotal = $SolarConsumptionTotal + $SolarConsumptionArr[$i];

            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal + $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i];
        }



        //$data['costBeforeSolarArr'] = $costBeforeSolarArr;
        //$data['costAfterSolarArr'] = $costAfterSolarArr;
        //$data['costBeforeSolarTotal'] = $costBeforeSolarTotal;
        //$data['costAfterSolarTotal'] = $costAfterSolarTotal;
        //$data['exportTotal'] = $exportTotal;
        //$data['exportIncomeTotal'] = $exportIncomeTotal;
        // Solar Daily Usage Charge        
        $solarDailyUsageCharge                  = $proposal_data['daily_usage_charge'] * 365;
        $costBeforeSolarTotal_forBefore         = $costBeforeSolarTotal + $solarDailyUsageCharge;
        $costAfterSolarTotal_forAfter           = ($costAfterSolarTotal + $solarDailyUsageCharge) - $exportIncomeTotal;
        //$costAfterSolarTotal_forAfter = ($costAfterSolarTotal + $solarDailyUsageCharge);
        $data['costBeforeSolarTotal_forBefore'] = $costBeforeSolarTotal_forBefore         = $costBeforeSolarTotal_forBefore * 1.1;
        $data['costAfterSolarTotal_forAfter']   = $costAfterSolarTotal_forAfter           = $costAfterSolarTotal_forAfter * 1.1;

        $divi = 0;
        if ($costBeforeSolarTotal_forBefore != 0) {
            $divi = $costAfterSolarTotal_forAfter / $costBeforeSolarTotal_forBefore;
        }
        $minu  = 1 - $divi;
        $multi = $minu * 100;

        $data['annualBillSaving'] = $annualBillSaving         = $multi;
        $data['afterPercentage']  = 100 - $annualBillSaving;

        #############################################################################
        // *** END *** Solar consumption 
        ##############################################################################
        ###################################################################
        // Total System Size/Array Power Rating (recommended)
        ################################################################
        $maxInMeterData = max($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr);
        if ($proposal_data['area_id'] != 0) {
            $GetMaxValueAnnualSolarDataByArea = $this->proposal->get_MaxValueAnnualSolarDataByArea($proposal_data['area_id']);
        }

        $MaxValueAnnualSolarDataByArea = number_format($GetMaxValueAnnualSolarDataByArea['max_annual_solar'], 5);

        $cityMaximum = $MaxValueAnnualSolarDataByArea * 0.95;

        if ($cityMaximum != 0) {
            $kWBasedOnConsumption = $maxInMeterData * 100 / $cityMaximum;
        }
        //$data['maxInMeterData'] = $maxInMeterData;
        //$data['GetMaxValueAnnualSolarDataByArea'] = $GetMaxValueAnnualSolarDataByArea;
        //$data['MaxValueAnnualSolarDataByArea'] = $MaxValueAnnualSolarDataByArea;
        //$data['cityMaximum'] = $cityMaximum;
        //Rounding figure
        //  if($proposal_data['total_system_size'] != 0)
        if ($proposal_data['total_system_size'] <= 0) {
            $roundingFigure = 0;
            $split = explode(".", $kWBasedOnConsumption);
            if ($split[0] <= 5) {
                $roundingFigure = 5;
            } elseif ($split[0] <= 10) {
                $roundingFigure = 10;
            } elseif ($split[0] <= 15) {
                $roundingFigure = 15;
            } elseif ($split[0] <= 20) {
                $roundingFigure = 20;
            } elseif ($split[0] <= 25) {
                $roundingFigure = 25;
            } elseif ($split[0] <= 30) {
                $roundingFigure = 30;
            } elseif ($split[0] <= 40) {
                $roundingFigure = 40;
            } elseif ($split[0] <= 50) {
                $roundingFigure = 50;
            } elseif ($split[0] <= 60) {
                $roundingFigure = 60;
            } elseif ($split[0] <= 70) {
                $roundingFigure = 70;
            } elseif ($split[0] <= 80) {
                $roundingFigure = 80;
            } elseif ($split[0] <= 90) {
                $roundingFigure = 90;
            } elseif ($split[0] <= 100) {
                $roundingFigure = 100;
            }
        } else {
            $roundingFigure = $proposal_data['total_system_size'];
        }
        $data['roundingFigure'] = $roundingFigure;

        //$NumberOfPanels = number_format($roundingFigure/0.27,0);
        $TotalPanelArea         = number_format($roundingFigure * 6.056, 0);
        $data['TotalPanelArea'] = $TotalPanelArea;
        ###################################################################
        // **** END ***** Total System Size/Array Power Rating (recommended)
        ################################################################ 
        ###################################################################
        // Data from Annual Solar Data  x Size X Orientation x Tilt
        ###################################################################
        /*
          Adelaide = 1
          Brisbane = 3
          Canberra = 5
          Darwin = 6
          Hobart = 7
          Melbourne = 8
          Perth = 9
          Sydney = 10
         */
        if ($proposal_data['area_id'] == 1) {
            $dataTable = "adelaide";
        } elseif ($proposal_data['area_id'] == 3) {
            $dataTable = "brisbane";
        } elseif ($proposal_data['area_id'] == 5) {
            $dataTable = "canberra";
        } elseif ($proposal_data['area_id'] == 6) {
            $dataTable = "darwin";
        } elseif ($proposal_data['area_id'] == 7) {
            $dataTable = "hobart";
        } elseif ($proposal_data['area_id'] == 8) {
            $dataTable = "melbourne";
        } elseif ($proposal_data['area_id'] == 9) {
            $dataTable = "perth";
        } elseif ($proposal_data['area_id'] == 10) {
            $dataTable = "sydney";
        }

        //$GetAnnualSolarData = $this->common->fetch_where('tbl_annual_solar_data_new', '*', NULL);
        //$data['GetAnnualSolarData'] = $GetAnnualSolarData;
        ##############################################        
        // STC rebate value   
        ##############################################

        /*         * $GetSTCsCalculationsByPostCode = $this->common->fetch_where('tbl_solar_datastcscalculations', '*', array('startPostcode <=' => $customer_location_data['postcode']));

          $GetSTCsCalculationsByPostCodeCount = count($GetSTCsCalculationsByPostCode);

          $year = "postcodeYear" . date('Y', strtotime($proposal_data['created_at']));

          for ($j = 0; $j <= $GetSTCsCalculationsByPostCodeCount; $j++) {
          if ($j == $GetSTCsCalculationsByPostCodeCount - 1) {
          $GetSTCsCalculationsByPostCodeAndYear = $GetSTCsCalculationsByPostCode[$j][$year];
          }
          } */
        //$STCs = $roundingFigure * $GetSTCsCalculationsByPostCodeAndYear;
        //$STCrebateValue = $STCs * $getRebate['rebate'];
        //$data['GetSTCsCalculationsByPostCode'] = $GetSTCsCalculationsByPostCode;
        //$data['GetSTCsCalculationsByPostCodeAndYear'] = $GetSTCsCalculationsByPostCodeAndYear;
        $STCs                   = $proposal_data['stc'];
        $STCrebateValue         = $proposal_data['stc_rebate_value'];
        $data['STCs']           = $STCs;
        $data['STCrebateValue'] = $STCrebateValue;

        ##############################################        
        // TOTAL SOLAR OFFSET
        ##############################################
        $roiSolarDataByState               = $this->common->fetch_row('tbl_solar_data', '*', array('cityId' => $proposal_data['area_id']));
        $data['averageDailyKWhPerMonth1']  = $averageDailyKWhPerMonth1          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction1'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth2']  = $averageDailyKWhPerMonth2          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction2'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth3']  = $averageDailyKWhPerMonth3          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction3'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth4']  = $averageDailyKWhPerMonth4          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction4'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth5']  = $averageDailyKWhPerMonth5          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction5'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth6']  = $averageDailyKWhPerMonth6          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction6'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth7']  = $averageDailyKWhPerMonth7          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction7'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth8']  = $averageDailyKWhPerMonth8          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction8'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth9']  = $averageDailyKWhPerMonth9          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction9'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth10'] = $averageDailyKWhPerMonth10         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction10'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth11'] = $averageDailyKWhPerMonth11         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction11'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;
        $data['averageDailyKWhPerMonth12'] = $averageDailyKWhPerMonth12         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction12'] * $proposal_data['total_system_size'] / 100) * $getOrientationTilt;

        $EnergyProvidedBySolar = 0;

        //echo "<pre>";
        //print_r($SolarConsumptionTotal);die;
        if (!empty($panel_data['tiles']) && $proposal_data['nearmap_toggle']) {
            $pData = array();
            $pData['area_id'] = $area_data['areaId'];
            $pData['kwh_day'] = $area_data['kwh_day'];
            $pData['total_system_size'] = $proposal_data['total_system_size'];
            $pData['panel_data'] = $panel_data;
            $AverageDailyProduction = $this->calculate_total_solar_production_daily($pData);
        } else {
            $AverageDailyProduction = $proposal_data['total_system_size'] * $area_data['kwh_day'];
        }
        $data['AverageDailyProduction'] = $AverageDailyProduction; //         = ($SolarConsumptionTotal + $exportTotal) / 365;
        //print_r($exportTotal);die;
        if ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal != 0) {
            $EnergyProvidedBySolar = ($SolarConsumptionTotal * 100) / $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal;
        }
        $data['EnergyProvidedBySolar'] = $EnergyProvidedBySolar;
        ##############################################        
        // END TOTAL SOLAR OFFSET
        ##############################################
        ##############################################        
        // Cost Comparison
        ##############################################

        $data['oldBillYer1']  = $oldBillYer1          = $costBeforeSolarTotal;
        $data['oldBillYer2']  = $oldBillYer2          = 1.1 * $oldBillYer1;
        $data['oldBillYer3']  = $oldBillYer3          = 1.1 * $oldBillYer2;
        $data['oldBillYer4']  = $oldBillYer4          = 1.1 * $oldBillYer3;
        $data['oldBillYer5']  = $oldBillYer5          = 1.1 * $oldBillYer4;
        $data['oldBillYer6']  = $oldBillYer6          = 1.1 * $oldBillYer5;
        $data['oldBillYer7']  = $oldBillYer7          = 1.1 * $oldBillYer6;
        $data['oldBillYer8']  = $oldBillYer8          = 1.1 * $oldBillYer7;
        $data['oldBillYer9']  = $oldBillYer9          = 1.1 * $oldBillYer8;
        $data['oldBillYer10'] = $oldBillYer10         = 1.1 * $oldBillYer9;

        $data['newBillYer1']  = $newBillYer1          = $costAfterSolarTotal;
        $data['newBillYer2']  = $newBillYer2          = 1.1 * $newBillYer1;
        $data['newBillYer3']  = $newBillYer3          = 1.1 * $newBillYer2;
        $data['newBillYer4']  = $newBillYer4          = 1.1 * $newBillYer3;
        $data['newBillYer5']  = $newBillYer5          = 1.1 * $newBillYer4;
        $data['newBillYer6']  = $newBillYer6          = 1.1 * $newBillYer5;
        $data['newBillYer7']  = $newBillYer7          = 1.1 * $newBillYer6;
        $data['newBillYer8']  = $newBillYer8          = 1.1 * $newBillYer7;
        $data['newBillYer9']  = $newBillYer9          = 1.1 * $newBillYer8;
        $data['newBillYer10'] = $newBillYer10         = 1.1 * $newBillYer9;



        $Finance_Yr1  = '';
        $Finance_Yr2  = '';
        $Finance_Yr3  = '';
        $Finance_Yr4  = '';
        $Finance_Yr5  = '';
        $Finance_Yr6  = '';
        $Finance_Yr7  = '';
        $Finance_Yr8  = '';
        $Finance_Yr9  = '';
        $Finance_Yr10 = '';

        if (empty($proposal_finance_data)) {
            $proposal_finance_data['term']                    = 0;
            $proposal_finance_data['monthly_payment_plan']    = 0;
            $proposal_finance_data['upfront_payment_options'] = 0;
        }

        $term = 0;
        if ($proposal_finance_data['term'] == 12) {
            $term = 1;
        }
        if ($proposal_finance_data['term'] == 24) {
            $term = 2;
        }
        if ($proposal_finance_data['term'] == 36) {
            $term = 3;
        }
        if ($proposal_finance_data['term'] == 48) {
            $term = 4;
        }
        if ($proposal_finance_data['term'] == 60) {
            $term = 5;
        }
        if ($proposal_finance_data['term'] == 72) {
            $term = 6;
        }
        if ($proposal_finance_data['term'] == 84) {
            $term = 7;
        }

        $data['term'] = $term;

        $Finance_Yr  = array();
        $Finance_Yr2 = array();
        for ($x = 0; $x < 10; $x++) {
            if ($x < $term) {
                $Finance_Yr[$x]  = $proposal_finance_data['monthly_payment_plan'] * 1.1 * 12;
                $Finance_Yr2[$x] = $proposal_finance_data['monthly_payment_plan'] * 1.1 * 12;
            } else {
                $Finance_Yr[$x]  = -1;
                $Finance_Yr2[$x] = -1;
            }
        }

        $data['Finance_Yr']  = $Finance_Yr;
        $data['Finance_Yr2'] = $Finance_Yr2;

        $Oldbill_Newbill1  = $oldBillYer1 - $newBillYer1;
        $Oldbill_Newbill2  = $oldBillYer2 - $newBillYer2;
        $Oldbill_Newbill3  = $oldBillYer3 - $newBillYer3;
        $Oldbill_Newbill4  = $oldBillYer4 - $newBillYer4;
        $Oldbill_Newbill5  = $oldBillYer5 - $newBillYer5;
        $Oldbill_Newbill6  = $oldBillYer6 - $newBillYer6;
        $Oldbill_Newbill7  = $oldBillYer7 - $newBillYer7;
        $Oldbill_Newbill8  = $oldBillYer8 - $newBillYer8;
        $Oldbill_Newbill9  = $oldBillYer9 - $newBillYer9;
        $Oldbill_Newbill10 = $oldBillYer10 - $newBillYer10;

        $data['Benefit1']  = $Benefit1          = abs($Oldbill_Newbill1 - $Finance_Yr2[0]);
        $data['Benefit2']  = $Benefit2          = abs($Oldbill_Newbill2 - $Finance_Yr2[1]);
        $data['Benefit3']  = $Benefit3          = abs($Oldbill_Newbill3 - $Finance_Yr2[2]);
        $data['Benefit4']  = $Benefit4          = abs($Oldbill_Newbill4 - $Finance_Yr2[3]);
        $data['Benefit5']  = $Benefit5          = abs($Oldbill_Newbill5 - $Finance_Yr2[4]);
        $data['Benefit6']  = $Benefit6          = abs($Oldbill_Newbill6 - $Finance_Yr2[5]);
        $data['Benefit7']  = $Benefit7          = abs($Oldbill_Newbill7 - $Finance_Yr2[6]);
        $data['Benefit8']  = $Benefit8          = abs($Oldbill_Newbill8 - $Finance_Yr2[7]);
        $data['Benefit9']  = $Benefit9          = abs($Oldbill_Newbill9 - $Finance_Yr2[8]);
        $data['Benefit10'] = $Benefit10         = abs($Oldbill_Newbill10 - $Finance_Yr2[9]);

        ##############################################        
        // Cost Comparison
        ##############################################

        $Benefit       = $costBeforeSolarTotal - $costAfterSolarTotal;
        $ExportBenefit = $exportTotal * $proposal_data['feed_in_tariff'];


        ##############################################        
        // Solar Revenue
        ##############################################

        $year = 0;
        if ($proposal_finance_data['term'] == 12) {
            $year = 1;
        }
        if ($proposal_finance_data['term'] == 24) {
            $year = 2;
        }
        if ($proposal_finance_data['term'] == 36) {
            $year = 3;
        }
        if ($proposal_finance_data['term'] == 48) {
            $year = 4;
        }
        if ($proposal_finance_data['term'] == 60) {
            $year = 5;
        }
        if ($proposal_finance_data['term'] == 72) {
            $year = 6;
        }
        if ($proposal_finance_data['term'] == 84) {
            $year = 7;
        }
        $data['year'] = $year;

        $upfrontPaymentOptionsText = '';
        if ($proposal_finance_data['upfront_payment_options'] == 1) {
            $upfrontPaymentOptionsText = "Upfront Payment upto 30kW";
        }
        if ($proposal_finance_data['upfront_payment_options'] == 2) {
            $upfrontPaymentOptionsText = "Upfront Payment for 30kW - 100kW";
        }

        if ($proposal_finance_data['upfront_payment_options'] == 3) {
            $upfrontPaymentOptionsText = "Upfront Payment for 100kW & above";
        }
        $data['upfrontPaymentOptionsText'] = $upfrontPaymentOptionsText;


        //Average Load vs Solar Production Graph Data
        $avg_load_graph_data         = $avg_load_sol_prd_graph_data = [];

        if (isset($proposal_data['is_meter_data']) && $proposal_data['is_meter_data'] != 0 && $proposal_data['meter_data_file'] != NULL) {
            $graph_data = $this->calcualte_summer_and_weekend_load_graph_data_from_meter_data(
                $meter_data,
                $DataFromAnnualSolarDataXSizeXOrientationXTiltArr,
                $SolarConsumptionArr
            );
        } else {
            $graph_data = $this->calcualte_summer_and_weekend_load_graph_data_from_custom_data(
                $consumption_profile_data,
                $proposal_data
            );
        }

        //echo "<pre>";
        //print_r($graph_data);die;

        for ($wi = 1442; $wi <= 1465; $wi++) {
            $avg_load_sol_prd_graph_data[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$wi];
        }
        $data['avg_load_graph_data']         = $graph_data['avg_monthly_load_graph_data'];
        $data['avg_load_sol_prd_graph_data'] = $avg_load_sol_prd_graph_data;

        //Weekend Load vs Solar Production Graph Data
        //Weekend Load vs Solar Production Graph Data
        $count                           = 74;
        $weekend_load_graph_data         = $weekend_load_sol_prd_graph_data = [];


        for ($wi = 4322; $wi <= 4345; $wi++) {
            $weekend_load_sol_prd_graph_data[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$wi];
        }

        $data['weekend_load_graph_data']         = $graph_data['weekend_load_graph_data'];
        $data['weekend_load_sol_prd_graph_data'] = $weekend_load_sol_prd_graph_data;


        $typical_export_load_graph_data    = $typical_export_sol_prd_graph_data = $typical_export_export_graph_data  = $typical_export_offset_graph_data  = [];
        if (isset($proposal_data['is_meter_data']) && $proposal_data['is_meter_data'] != 0 && $proposal_data['meter_data_file'] != NULL && FALSE) {
            for ($i = 0; $i <= 23; $i++) {
                $typical_export_load_graph_data[] = ($graph_data['avg_monthly_load_graph_data']['jan'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['feb'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['mar'][$i]) / 3;

                $typical_export_sol_prd_graph_data[] = ($graph_data['avg_monthly_sol_prd_graph_data']['jan'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['feb'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['mar'][$i]) / 3;

                $typical_export_export_graph_data[] = ($graph_data['avg_monthly_export_graph_data']['jan'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['feb'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['mar'][$i]) / 3;

                $typical_export_offset_graph_data[] = ($graph_data['avg_monthly_offset_graph_data']['jan'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['feb'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['mar'][$i]) / 3;
            }
        } else {
            //$a = 24 * 28;
            $a = 24 * 60;
            //echo $AnnualSolarData[$a]['dateAndtime'];die;
            for ($s = $a; $s <= ($a + 23); $s++) {
                $typical_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
            }
            for ($su = $a; $su <= ($a + 23); $su++) {
                $typical_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
            }
            for ($su = $a; $su <= ($a + 23); $su++) {
                $typical_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '')); //abs(number_format((float) ($exportArr[$su]), 2, '.', ''));
            }
            // echo '<pre>';
            // print_r($typical_export_export_graph_data);die;
            for ($su = $a; $su <= ($a + 23); $su++) {
                $typical_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
            }
        }
        $data['typical_export_load_graph_data']    = $typical_export_load_graph_data;
        $data['typical_export_sol_prd_graph_data'] = $typical_export_sol_prd_graph_data;
        $data['typical_export_export_graph_data']  = $typical_export_export_graph_data;
        $data['typical_export_offset_graph_data']  = $typical_export_offset_graph_data;

        //Worst Energy Production Day
        $worst_export_load_graph_data    = $worst_export_sol_prd_graph_data = $worst_export_export_graph_data  = $worst_export_offset_graph_data  = [];

        if (isset($proposal_data['is_meter_data']) && $proposal_data['is_meter_data'] != 0 && $proposal_data['meter_data_file'] != NULL && FALSE) {
            for ($i = 0; $i <= 23; $i++) {
                $worst_export_load_graph_data[] = ($graph_data['avg_monthly_load_graph_data']['apr'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['may'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['jun'][$i]) / 3;

                $worst_export_sol_prd_graph_data[] = ($graph_data['avg_monthly_sol_prd_graph_data']['apr'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['may'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['jun'][$i]) / 3;

                $worst_export_export_graph_data[] = ($graph_data['avg_monthly_export_graph_data']['apr'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['may'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['jun'][$i]) / 3;

                $worst_export_offset_graph_data[] = ($graph_data['avg_monthly_offset_graph_data']['apr'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['may'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['jun'][$i]) / 3;
            }
        } else {
            //$b = 24 * 364;
            $b = 24 * 180;
            //echo $AnnualSolarData[$b]['dateAndtime'];die;
            for ($s = $b; $s <= ($b + 23); $s++) {
                $worst_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
            }
            for ($su = $b; $su <= ($b + 23); $su++) {
                $worst_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
            }
            for ($su = $b; $su <= ($b + 23); $su++) {
                $worst_export_export_graph_data[] =  abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '')); //abs(number_format((float) ($exportArr[$su]), 2, '.', ''));
            }
            // echo '<pre>';
            // echo array_sum($worst_export_export_graph_data);die;
            for ($su = $b; $su <= ($b + 23); $su++) {
                $worst_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
            }
        }
        $data['worst_export_load_graph_data']    = $worst_export_load_graph_data;
        $data['worst_export_sol_prd_graph_data'] = $worst_export_sol_prd_graph_data;
        $data['worst_export_export_graph_data']  = $worst_export_export_graph_data;
        $data['worst_export_offset_graph_data']  = $worst_export_offset_graph_data;

        //High Energy Production Day
        $high_export_load_graph_data    = $high_export_sol_prd_graph_data = $high_export_export_graph_data  = $high_export_offset_graph_data  = [];
        if (isset($proposal_data['is_meter_data']) && $proposal_data['is_meter_data'] != 0 && $proposal_data['meter_data_file'] != NULL && FALSE) {
            for ($i = 0; $i <= 23; $i++) {
                $high_export_load_graph_data[] = ($graph_data['avg_monthly_load_graph_data']['jul'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['aug'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['sep'][$i]) / 3;

                $high_export_sol_prd_graph_data[] = ($graph_data['avg_monthly_sol_prd_graph_data']['jul'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['aug'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['sep'][$i]) / 3;

                $high_export_export_graph_data[] = ($graph_data['avg_monthly_export_graph_data']['jul'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['aug'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['sep'][$i]) / 3;

                $high_export_offset_graph_data[] = ($graph_data['avg_monthly_offset_graph_data']['jul'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['aug'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['sep'][$i]) / 3;
            }
        } else {
            //$c = 24 * 276;
            $c = 24 * 340;
            // echo $AnnualSolarData[$c]['dateAndtime'];die;
            for ($s = $c; $s <= ($c + 23); $s++) {
                $high_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
            }
            for ($su = $c; $su <= ($c + 23); $su++) {
                $high_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
            }
            for ($su = $c; $su <= ($c + 23); $su++) {
                $high_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '')); //abs(number_format((float) ($exportArr[$su]), 2, '.', ''));
            }
            for ($su = $c; $su <= ($c + 23); $su++) {
                $high_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
            }
        }
        $data['high_export_load_graph_data']    = $high_export_load_graph_data;
        $data['high_export_sol_prd_graph_data'] = $high_export_sol_prd_graph_data;
        $data['high_export_export_graph_data']  = $high_export_export_graph_data;
        $data['high_export_offset_graph_data']  = $high_export_offset_graph_data;

        //Best Energy Production Day
        $best_export_load_graph_data    = $best_export_sol_prd_graph_data = $best_export_export_graph_data  = $best_export_offset_graph_data  = [];
        if (isset($proposal_data['is_meter_data']) && $proposal_data['is_meter_data'] != 0 && $proposal_data['meter_data_file'] != NULL && FALSE) {
            for ($i = 0; $i <= 23; $i++) {
                $best_export_load_graph_data[] = ($graph_data['avg_monthly_load_graph_data']['oct'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['nov'][$i] +
                    $graph_data['avg_monthly_load_graph_data']['dec'][$i]) / 3;

                $best_export_sol_prd_graph_data[] = ($graph_data['avg_monthly_sol_prd_graph_data']['oct'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['nov'][$i] +
                    $graph_data['avg_monthly_sol_prd_graph_data']['dec'][$i]) / 3;

                $best_export_export_graph_data[] = ($graph_data['avg_monthly_export_graph_data']['oct'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['nov'][$i] +
                    $graph_data['avg_monthly_export_graph_data']['dec'][$i]) / 3;

                $best_export_offset_graph_data[] = ($graph_data['avg_monthly_offset_graph_data']['oct'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['nov'][$i] +
                    $graph_data['avg_monthly_offset_graph_data']['dec'][$i]) / 3;
            }
        } else {
            //$d = 24 * 53;
            $d = 24 * 182;
            //echo $AnnualSolarData[$d]['dateAndtime'];die;
            for ($s = $d; $s <= ($d + 24); $s++) {
                $best_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
            }
            for ($su = $d; $su <= ($d + 24); $su++) {
                $best_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
            }
            for ($su = $d; $su <= ($d + 24); $su++) {
                $best_export_export_graph_data[] =  abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '')); //abs(number_format((float) ($exportArr[$su]), 2, '.', ''));
            }
            for ($su = $d; $su <= ($d + 24); $su++) {
                $best_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
            }
        }
        $data['best_export_load_graph_data']    = $best_export_load_graph_data;
        $data['best_export_sol_prd_graph_data'] = $best_export_sol_prd_graph_data;
        $data['best_export_export_graph_data']  = $best_export_export_graph_data;
        $data['best_export_offset_graph_data']  = $best_export_offset_graph_data;


        //Summer Day
        $summer_export_load_graph_data    = $summer_export_sol_prd_graph_data = $summer_export_export_graph_data  = $summer_export_offset_graph_data  = [];
        for ($s = 8162; $s <= 8329; $s++) {
            $summer_export_load_graph_data[] = number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_sol_prd_graph_data[] = number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_export_graph_data[] = number_format((float) ($exportArr[$su]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_offset_graph_data[] = number_format((float) ($SolarConsumptionArr[$su]), 2, '.', '');
        }
        $data['summer_export_load_graph_data']    = json_encode($summer_export_load_graph_data);
        $data['summer_export_sol_prd_graph_data'] = json_encode($summer_export_sol_prd_graph_data);
        $data['summer_export_export_graph_data']  = json_encode($summer_export_export_graph_data);
        $data['summer_export_offset_graph_data']  = json_encode($summer_export_offset_graph_data);

        //Winter Day
        $winter_export_load_graph_data    = $winter_export_sol_prd_graph_data = $winter_export_export_graph_data  = $winter_export_offset_graph_data  = [];
        for ($s = 4370; $s <= 4537; $s++) {
            $winter_export_load_graph_data[] = number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_sol_prd_graph_data[] = number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_export_graph_data[] = number_format((float) ($exportArr[$su]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_offset_graph_data[] = number_format((float) ($SolarConsumptionArr[$su]), 2, '.', '');
        }
        $data['winter_export_load_graph_data']    = json_encode($winter_export_load_graph_data);
        $data['winter_export_sol_prd_graph_data'] = json_encode($winter_export_sol_prd_graph_data);
        $data['winter_export_export_graph_data']  = json_encode($winter_export_export_graph_data);
        $data['winter_export_offset_graph_data']  = json_encode($winter_export_offset_graph_data);


        $TotalRevenue           = 0;
        $twentyFiveYearsBenefit = 0;
        for ($z = 0; $z <= 24; $z++) {
            $benefitArr[]       = $Benefit;
            $exportArr[]        = $exportTotal;
            $ExportBenefitArr[] = $ExportBenefit;
            //$TotalSavingsArr[]  = ($Benefit + $ExportBenefit) ;//* 1.1; // nishan told to remove this 1.1 multiplication, he has given the confirmation on 22 oct 19 on skype call 
            //$TotalSavingsArr[] = $newBenefit +$ExportBenefit;
            //Changed fiven by anhtony on apr 14 2020
            if ($z == 0) {
                $TotalSavingsArr[]  = ($benefitArr[0] + $ExportBenefitArr[0]);
            } else {
                $annual_inc_in_ec = 1.05;
                if ($proposal_data['annual_inc_in_ec'] != '' && $proposal_data['annual_inc_in_ec'] != NULL  && $proposal_data['annual_inc_in_ec'] != 0) {
                    $annual_inc_in_ec = 1 + ($proposal_data['annual_inc_in_ec'] / 100);
                }
                //echo $annual_inc_in_ec;die;
                $TotalSavingsArr[]  = ($TotalSavingsArr[($z - 1)]) * $annual_inc_in_ec * 0.994;
            }
            $exportTotal        = $exportTotal * 0.995;
            $Benefit            = $Benefit * 1.1 * 0.995;
            //$newBenefit = $newBenefit*1.1*0.995;
            $ExportBenefit      = $exportTotal * $proposal_data['feed_in_tariff'];
            if ($z < 10) {
                $TotalRevenue = $TotalRevenue + $TotalSavingsArr[$z];
            }
            $twentyFiveYearsBenefit = $twentyFiveYearsBenefit + $TotalSavingsArr[$z];
        }

        $data['benefitArr']             = $benefitArr;
        $data['exportArr']              = $exportArr;
        $data['ExportBenefitArr']       = $ExportBenefitArr;
        $data['TotalSavingsArr']        = $TotalSavingsArr;
        $data['exportTotal']            = $exportTotal;
        $data['Benefit']                = $Benefit;
        $data['ExportBenefit']          = $ExportBenefit;
        $data['TotalRevenue']           = $TotalRevenue;
        $data['twentyFiveYearsBenefit'] = $twentyFiveYearsBenefit;

        //Paybale, Investment, Rebate , ROI
        $data['stc_rebate_value']        = $proposal_data['stc_rebate_value'];
        $data['price_before_stc_rebate'] = $proposal_data['price_before_stc_rebate'];
        $data['total_payable_exGST']     = $proposal_data['total_payable_exGST'];
        /** Start ROI (months). */
        $roi                             = 0;
        if ($TotalSavingsArr[0] != 0) {
            $roi = $proposal_data['price_before_stc_rebate'] / $TotalSavingsArr[0];
        }
        // echo $proposal_data['total_payable_exGST'];die;
        //$data['roi'] = $roi = 12 * $roi;
        $data['roi'] = $roi         = $roi;
        /** End ROI (months). */

        //echo "<pre>";
        //print_r($data);die;
        return $data;
    }

    /*     * private function average_energy_production_graph_data($month_keys,$usage,$solar_prod,$consumption){
      for($month_keys as $key => $value){

      }
      } */

    private function calculate_consumption_profile_data_by_meter_data($proposal_data = [], $meter_data = [])
    {
        $data                 = [];
        //print_r($proposal_data);die;
        //Check if uploaded csv 2-columns or 3-columns
        $column_count         = count($meter_data[0]);
        $full_year_meter_data = [];
        if ($column_count == 2) {
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $meter_data);
            $full_year_meter_data = $final_data['data'];
        } else if ($column_count == 3) {
            $new_meter_data       = $this->convert_three_column_to_two_columns_meter_data($meter_data);
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $new_meter_data);
            $full_year_meter_data = $final_data['data'];
        }



        //echo '<pre>';
        //print_r($full_year_meter_data);die;

        for ($i = 0; $i < count($full_year_meter_data); $i++) {
            $data['consumption_date'][$i] = $full_year_meter_data[$i][0];
            $data['consumption_data'][$i] = $full_year_meter_data[$i][1];
            $data['consumption_time'][$i] = explode(" ", $full_year_meter_data[$i][0])[1];
            $data['final_data'][$i][0]    = $full_year_meter_data[$i][0];
            $data['final_data'][$i][1]    = $full_year_meter_data[$i][1];
            $month                        = (int) date('m', strtotime($full_year_meter_data[$i][0]));
            if ($month <= 3) {
                $data['consumption_keys_1_3'][] = $i;
            } else if ($month > 3 && $month <= 6) {
                $data['consumption_keys_4_6'][] = $i;
            } else if ($month > 6 && $month <= 9) {
                $data['consumption_keys_7_9'][] = $i;
            } else if ($month > 9 && $month <= 12) {
                $data['consumption_keys_10_12'][] = $i;
            }
        }
        $data['missing_months'] = $final_data['missing_months'];

        //echo '<pre>';
        //print_r($data);die;
        return $data;
    }

    private function handle_two_columns_meter_data($proposal_data, $meter_data)
    {


        $is_in_1hr_interval = $is_in_30_mins_interval = $is_in_15_mins_interval = false;

        //Check if DateTime is in 1 hr interval then do nothing
        $dateTime = date('i', strtotime($meter_data[1][0]));

        if ($dateTime == 00) {
            $is_in_1hr_interval = true;
            $minimized_meter_data = $meter_data;
        }


        //Check if DateTime is in 30 mins interval then sum and take mean of 2
        if ($dateTime == 30 && $is_in_1hr_interval == false) {
            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_30 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;
                $minimized_meter_data[$b][0]  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_30) / 2;
                $a = $a + 2;
                $b++;
            }
        }

        //Check if DateTime is in 15 mins interval then sum and take mean of 4
        if ($dateTime == 15  && $is_in_1hr_interval == false  && $is_in_30_mins_interval == false) {
            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_15 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;
                $min_30 = (isset($meter_data[($a + 2)])) ? $meter_data[($a + 2)][1] : 0;
                $min_45 = (isset($meter_data[($a + 3)])) ? $meter_data[($a + 3)][1] : 0;

                $minimized_meter_data[$b][0] = $s  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_15 + $min_30 + $min_45) / 4;
                $a = $a + 4;
                $b++;
            }
        }



        //So first get dates and hours based array meter data
        $meter_data_without_years = [];
        $month_years = [];
        foreach ($minimized_meter_data as $key => $value) {
            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date =  $value[0];
            }

            $break_date = explode(' ', $new_date);
            $break_date_0 = explode('-', $break_date[0]);
            $break_date_1 = explode(':', $break_date[1]);


            if (strlen($break_date_0[0]) > 2) {
                $compiled_date = date('d-m H:i', strtotime($new_date));
            } else {
                $compiled_date = ((strlen($break_date_0[0]) == 2) ? $break_date_0[0] : '0' . $break_date_0[0]) . '-' . $break_date_0[1] . ' ' . ((strlen($break_date_1[0]) == 2) ? $break_date_1[0] : '0' . $break_date_1[0]) . ':' . $break_date_1[1];
            }

            $meter_data_without_years[$key][0] =  $compiled_date;
            $meter_data_without_years[$key][1] = $value[1];


            $month = (int) date('m', strtotime($new_date));
            if (!isset($month_years[$month])) {
                $year = (int) date('Y', strtotime($new_date));
                $month_years[$month] = $year;
            }
        }

        // echo '<pre>';
        // print_r($meter_data_without_years);die;



        //Now loop for each month * no of days * 24hrs
        $final_meter_data['data'] = [];
        $final_meter_data['missing_months'] = [];
        $month = 1;
        $index = 0;
        while ($month <= 12) {
            $count_month_years = count($month_years);
            $prev_yr = ($count_month_years > 0) ? $month_years[$count_month_years - 1] : 2018;
            $year = (isset($month_years[$month])) ? $month_years[$month] : $prev_yr;
            $no_of_days = $this->components->days_in_month($month, $year);
            for ($i = 1; $i <= $no_of_days; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    $d = ($i < 10) ? '0' . $i : $i;
                    $m = ($month < 10) ? '0' . $month : $month;
                    $h = ($j < 10) ? '0' . $j . ':00' : $j . ':00';
                    // if($m==10){
                    // echo $full_date = $d.'-'.$m.' '.$h;
                    // $key = array_search($full_date, array_column($meter_data_without_years, '0'));
                    // print_r(var_dump($key));die;
                    // }
                    $full_date = $d . '-' . $m . ' ' . $h;
                    $key = array_search($full_date, array_column($meter_data_without_years, '0'));
                    if ($key !== false) {
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = $meter_data_without_years[$key][1];
                    } else {
                        if (!in_array($m, $final_meter_data['missing_months'])) {
                            $final_meter_data['missing_months'][] = (int) $m;
                        }
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = 0;
                    }
                    $index++;
                }
            }
            $month++;
        }

        // echo "<pre>";
        // print_r($final_meter_data);die;
        // print_r($proposal_data);die;

        /**if (!empty($proposal_data) && $proposal_data['meter_data_replicated'] != NULL) {
            $meter_data_replicated = json_decode($proposal_data['meter_data_replicated']);
            // echo "<pre>";
            // print_r($meter_data_replicated);
            // die;
            
            if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
                foreach ($meter_data_replicated as $key => $value) {
                    $to = (int) $value->replicated_to;
                    $from = (int) $value->replicated_from;
                    $start = $to_start = $end = $to_end = $j = 0;
                    for($i = 1; $i <= $from; $i++){
                        $no_of_days = $this->components->days_in_month($i,2018);
                        $end += $no_of_days * 24;
                        if($i == ($from - 1)){
                            $start = ($end + 1);
                        }
                    }
                    for($i = 1; $i <= $to; $i++){
                        $no_of_days = $this->components->days_in_month($i,2018);
                        if($i == $to){
                            $to_start = $to_end;
                        }else{
                            $to_end += $no_of_days * 24;
                        }
                    }
                  //  echo '<pre>';
                    $matchMonth  = [];
                    $fromMonth = strlen((string)$from)==1?'0'.$from:$from;
                    foreach($final_meter_data['data'] as $k=>$month){
                        $pattern = "/[0-9][0-9]-".$fromMonth."-[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]/";
                        $toMonth = strlen((string)$to)==1?'0'.$to:$to;
                        if(preg_match($pattern, $month[0])){
                            $e = explode('-',$month[0]);
                            
                            $e[1] = $toMonth;
                            $keyString = implode('-',$e);
                            $matchMonth[$keyString] = $month[1];
                        }
                    }
                    $previousValue = 0;
                    foreach($final_meter_data['data'] as $k=>$month){
                        $pattern = "/[0-9][0-9]-".$toMonth."-[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]/";
                        if(isset($matchMonth[$month[0]])) {
                             if(preg_match($pattern, $month[0])){
                                $previousValue = $final_meter_data['data'][$k][1] = $matchMonth[$month[0]];
                            }
                        } 
                        // else {
                        //     $previousValue = $final_meter_data['data'][$k][1] = $previousValue;
                        // }
                       
                    }
                    // print_r($final_meter_data);die();
                    // $in = 0;
                    // for ($j = $start; $j < $end; $j++) {
                    //     $in = $j-1;
                    //     $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
                    //     $to_start++;
                    // }
                    
                }
            } else {
                foreach ($meter_data_replicated as $key => $value) {
                    $to = (int) $value->replicated_to;
                    $from = (int) $value->replicated_from;
                    $start = $to_start = $end = $to_end = $j = 0;
                    for($i = 1; $i <= $from; $i++){
                        $no_of_days = $this->components->days_in_month($i,2018);
                        $end += $no_of_days * 24;
                        if($i == ($from - 1)){
                            $start = ($end + 1);
                        }
                    }
                    for($i = 1; $i <= $to; $i++){
                        $no_of_days = $this->components->days_in_month($i,2018);
                        if($i == $to){
                            $to_start = $to_end;
                        }else{
                            $to_end += $no_of_days * 24;
                        }
                    }
                        //echo $j;
                        //echo $to_start;die;
                    for ($j = $start; $j < $end; $j++) {
                        $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
                            //echo $final_meter_data['data'][$j][0];
                            //echo $final_meter_data['data'][$to_start][0];
                            //echo $final_meter_data['data'][$to_start][1];die;
                        $to_start++;
                    }
                }
            }
            
        }*/


        if (!empty($proposal_data) && $proposal_data['meter_data_replicated'] != NULL) {
            $meter_data_replicated = json_decode($proposal_data['meter_data_replicated']);
            foreach ($meter_data_replicated as $key => $value) {
                $to = (int) $value->replicated_to;
                $from = (int) $value->replicated_from;

                $start = $to_start = $end = $to_end = $j = 0;
                for ($i = 1; $i <= $from; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    $end += $no_of_days * 24;
                    if ($i == ($from - 1)) {
                        $start = $end; //($end + 1);
                    }
                }
                for ($i = 1; $i <= $to; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    if ($i == $to) {
                        $to_start = $to_end;
                    } else {
                        $to_end += $no_of_days * 24;
                    }
                }

                for ($j = $start; $j < $end; $j++) {
                    $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
                    $to_start++;
                }
            }
        }

        return $final_meter_data;
    }

    public function convert_three_column_to_two_columns_meter_data($meter_data)
    {

        /** Dates in the m/d/y or d-m-y formats are disambiguated by looking at the separator between the various components: if the separator is a slash (/), then the American m/d/y is assumed; whereas if the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed. */
        $two_column_meter_data = [];
        foreach ($meter_data as $key => $value) {
            $time_in_24_hour_format = date("H:i", strtotime($value[1]));
            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date = $value[0];
            }
            $proper_date_format             = date('d-m-Y', strtotime($new_date));
            //echo $proper_date_format;die;
            $col1_col2                      = $proper_date_format . ' ' . $time_in_24_hour_format;
            $two_column_meter_data[$key][0] = $col1_col2;
            $two_column_meter_data[$key][1] = $value[2];
        }
        return $two_column_meter_data;
    }

    private function calculate_consumption_profile_data_by_custom_data($proposal_data = [])
    {
        $Jan = 23;
        $Feb = 21;
        $Mar = 23;
        $Apr = 22;
        $May = 23;
        $Jun = 22;
        $Jul = 23;
        $Aug = 23;
        $Sep = 22;
        $Oct = 23;
        $Nov = 22;
        $Dec = 23;

        // Daily Usage From Monthly usage Calculate
        $DailyUsageFromMonthlyusage_jan = $proposal_data['monthly_electricity_usage'] / $Jan;
        $DailyUsageFromMonthlyusage_feb = $proposal_data['monthly_electricity_usage'] / $Feb;
        $DailyUsageFromMonthlyusage_mar = $proposal_data['monthly_electricity_usage'] / $Mar;
        $DailyUsageFromMonthlyusage_apr = $proposal_data['monthly_electricity_usage'] / $Apr;
        $DailyUsageFromMonthlyusage_may = $proposal_data['monthly_electricity_usage'] / $May;
        $DailyUsageFromMonthlyusage_jun = $proposal_data['monthly_electricity_usage'] / $Jun;
        $DailyUsageFromMonthlyusage_jul = $proposal_data['monthly_electricity_usage'] / $Jul;
        $DailyUsageFromMonthlyusage_aug = $proposal_data['monthly_electricity_usage'] / $Aug;
        $DailyUsageFromMonthlyusage_sep = $proposal_data['monthly_electricity_usage'] / $Sep;
        $DailyUsageFromMonthlyusage_oct = $proposal_data['monthly_electricity_usage'] / $Oct;
        $DailyUsageFromMonthlyusage_nov = $proposal_data['monthly_electricity_usage'] / $Nov;
        $DailyUsageFromMonthlyusage_dec = $proposal_data['monthly_electricity_usage'] / $Dec;


        # Get data by custom profile for all months
        $dataCustomProfileForAllMonthsWorkingDays    = $this->common->fetch_where('tbl_solar_working_days', '*', array('customProfileId' => $proposal_data['custom_profile_id']));
        $dataCustomProfileForAllMonthsNonWorkingDays = $this->common->fetch_where('tbl_solar_non_working_days', '*', array('customProfileId' => $proposal_data['custom_profile_id']));

        $DailyUsagefromDefaultProfile_jan = 0;
        $DailyUsagefromDefaultProfile_feb = 0;
        $DailyUsagefromDefaultProfile_mar = 0;
        $DailyUsagefromDefaultProfile_apr = 0;
        $DailyUsagefromDefaultProfile_may = 0;
        $DailyUsagefromDefaultProfile_jun = 0;
        $DailyUsagefromDefaultProfile_jul = 0;
        $DailyUsagefromDefaultProfile_aug = 0;
        $DailyUsagefromDefaultProfile_sep = 0;
        $DailyUsagefromDefaultProfile_oct = 0;
        $DailyUsagefromDefaultProfile_nov = 0;
        $DailyUsagefromDefaultProfile_dec = 0;


        $RatioOfUsageSelectedDefaultProfileWorkingDays_janArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_febArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_marArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_aprArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_mayArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_junArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_julArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_augArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_sepArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_octArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_novArray = [];
        $RatioOfUsageSelectedDefaultProfileWorkingDays_decArray = [];

        for ($j = 0; $j < count($dataCustomProfileForAllMonthsWorkingDays); $j++) {
            $DailyUsagefromDefaultProfile_jan = $DailyUsagefromDefaultProfile_jan + $dataCustomProfileForAllMonthsWorkingDays[$j]['jan'];
            $DailyUsagefromDefaultProfile_feb = $DailyUsagefromDefaultProfile_feb + $dataCustomProfileForAllMonthsWorkingDays[$j]['feb'];
            $DailyUsagefromDefaultProfile_mar = $DailyUsagefromDefaultProfile_mar + $dataCustomProfileForAllMonthsWorkingDays[$j]['mar'];
            $DailyUsagefromDefaultProfile_apr = $DailyUsagefromDefaultProfile_apr + $dataCustomProfileForAllMonthsWorkingDays[$j]['apr'];
            $DailyUsagefromDefaultProfile_may = $DailyUsagefromDefaultProfile_may + $dataCustomProfileForAllMonthsWorkingDays[$j]['may'];
            $DailyUsagefromDefaultProfile_jun = $DailyUsagefromDefaultProfile_jun + $dataCustomProfileForAllMonthsWorkingDays[$j]['jun'];
            $DailyUsagefromDefaultProfile_jul = $DailyUsagefromDefaultProfile_jul + $dataCustomProfileForAllMonthsWorkingDays[$j]['jul'];
            $DailyUsagefromDefaultProfile_aug = $DailyUsagefromDefaultProfile_aug + $dataCustomProfileForAllMonthsWorkingDays[$j]['aug'];
            $DailyUsagefromDefaultProfile_sep = $DailyUsagefromDefaultProfile_sep + $dataCustomProfileForAllMonthsWorkingDays[$j]['sep'];
            $DailyUsagefromDefaultProfile_oct = $DailyUsagefromDefaultProfile_oct + $dataCustomProfileForAllMonthsWorkingDays[$j]['oct'];
            $DailyUsagefromDefaultProfile_nov = $DailyUsagefromDefaultProfile_nov + $dataCustomProfileForAllMonthsWorkingDays[$j]['nov'];
            $DailyUsagefromDefaultProfile_dec = $DailyUsagefromDefaultProfile_dec + $dataCustomProfileForAllMonthsWorkingDays[$j]['decm'];
        }

        $RatioOfUsage_jan = $RatioOfUsage_feb = $RatioOfUsage_mar = $RatioOfUsage_apr = $RatioOfUsage_may = $RatioOfUsage_jun = 0;
        $RatioOfUsage_jul = $RatioOfUsage_aug = $RatioOfUsage_sep = $RatioOfUsage_oct = $RatioOfUsage_nov = $RatioOfUsage_dec = 0;
        if ($DailyUsageFromMonthlyusage_jan != "" && $DailyUsagefromDefaultProfile_jan != "") {
            $RatioOfUsage_jan = $DailyUsageFromMonthlyusage_jan / $DailyUsagefromDefaultProfile_jan;
        }
        if ($DailyUsageFromMonthlyusage_feb != "" && $DailyUsagefromDefaultProfile_feb != "") {
            $RatioOfUsage_feb = $DailyUsageFromMonthlyusage_feb / $DailyUsagefromDefaultProfile_feb;
        }
        if ($DailyUsageFromMonthlyusage_mar != "" && $DailyUsagefromDefaultProfile_mar != "") {
            $RatioOfUsage_mar = $DailyUsageFromMonthlyusage_mar / $DailyUsagefromDefaultProfile_mar;
        }
        if ($DailyUsageFromMonthlyusage_apr != "" && $DailyUsagefromDefaultProfile_apr != "") {
            $RatioOfUsage_apr = $DailyUsageFromMonthlyusage_apr / $DailyUsagefromDefaultProfile_apr;
        }
        if ($DailyUsageFromMonthlyusage_may != "" && $DailyUsagefromDefaultProfile_may != "") {
            $RatioOfUsage_may = $DailyUsageFromMonthlyusage_may / $DailyUsagefromDefaultProfile_may;
        }
        if ($DailyUsageFromMonthlyusage_jun != "" && $DailyUsagefromDefaultProfile_jun != "") {
            $RatioOfUsage_jun = $DailyUsageFromMonthlyusage_jun / $DailyUsagefromDefaultProfile_jun;
        }
        if ($DailyUsageFromMonthlyusage_jul != "" && $DailyUsagefromDefaultProfile_jul != "") {
            $RatioOfUsage_jul = $DailyUsageFromMonthlyusage_jul / $DailyUsagefromDefaultProfile_jul;
        }
        if ($DailyUsageFromMonthlyusage_aug != "" && $DailyUsagefromDefaultProfile_aug != "") {
            $RatioOfUsage_aug = $DailyUsageFromMonthlyusage_aug / $DailyUsagefromDefaultProfile_aug;
        }
        if ($DailyUsageFromMonthlyusage_sep != "" && $DailyUsagefromDefaultProfile_sep != "") {
            $RatioOfUsage_sep = $DailyUsageFromMonthlyusage_sep / $DailyUsagefromDefaultProfile_sep;
        }
        if ($DailyUsageFromMonthlyusage_oct != "" && $DailyUsagefromDefaultProfile_oct != "") {
            $RatioOfUsage_oct = $DailyUsageFromMonthlyusage_oct / $DailyUsagefromDefaultProfile_oct;
        }
        if ($DailyUsageFromMonthlyusage_nov != "" && $DailyUsagefromDefaultProfile_nov != "") {
            $RatioOfUsage_nov = $DailyUsageFromMonthlyusage_nov / $DailyUsagefromDefaultProfile_nov;
        }
        if ($DailyUsageFromMonthlyusage_dec != "" && $DailyUsagefromDefaultProfile_dec != "") {
            $RatioOfUsage_dec = $DailyUsageFromMonthlyusage_dec / $DailyUsagefromDefaultProfile_dec;
        }


        #Ratio of usage x selected default profile Working Day
        for ($j = 0; $j < count($dataCustomProfileForAllMonthsWorkingDays); $j++) {
            $RatioOfUsageSelectedDefaultProfileWorkingDays_janArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['jan'] * $RatioOfUsage_jan);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_febArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['feb'] * $RatioOfUsage_feb);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_marArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['mar'] * $RatioOfUsage_mar);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_aprArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['apr'] * $RatioOfUsage_apr);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_mayArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['may'] * $RatioOfUsage_may);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_junArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['jun'] * $RatioOfUsage_jun);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_julArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['jul'] * $RatioOfUsage_jul);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_augArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['aug'] * $RatioOfUsage_aug);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_sepArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['sep'] * $RatioOfUsage_sep);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_octArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['oct'] * $RatioOfUsage_oct);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_novArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['nov'] * $RatioOfUsage_nov);
            $RatioOfUsageSelectedDefaultProfileWorkingDays_decArray[] = ($dataCustomProfileForAllMonthsWorkingDays[$j]['decm'] * $RatioOfUsage_dec);
        }

        //echo "<pre>";
        //print_r($RatioOfUsageSelectedDefaultProfileWorkingDays_decArray);die;
        #Ratio of usage x selected default profile Non Working Day
        for ($j = 0; $j < count($dataCustomProfileForAllMonthsNonWorkingDays); $j++) {
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_janArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['jan'] * $RatioOfUsage_jan);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_febArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['feb'] * $RatioOfUsage_feb);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_marArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['mar'] * $RatioOfUsage_mar);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_aprArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['apr'] * $RatioOfUsage_apr);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_mayArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['may'] * $RatioOfUsage_may);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_junArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['jun'] * $RatioOfUsage_jun);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_julArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['jul'] * $RatioOfUsage_jul);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_augArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['aug'] * $RatioOfUsage_aug);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_sepArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['sep'] * $RatioOfUsage_sep);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_octArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['oct'] * $RatioOfUsage_oct);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_novArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['nov'] * $RatioOfUsage_nov);
            $RatioOfUsageSelectedDefaultProfileNonWorkingDays_decArray[] = ($dataCustomProfileForAllMonthsNonWorkingDays[$j]['decm'] * $RatioOfUsage_dec);
        }

        $solarNumberOfWorkingDays = (isset($proposal_data['no_of_working_days'])) ? $proposal_data['no_of_working_days'] : 7;
        $nonWorkDay               = 7 - $solarNumberOfWorkingDays;
        $weekSunday               = 7;
        for ($dayJan = 1; $dayJan <= 365; $dayJan++) {

            if ($nonWorkDay == 1) {
                if ($weekSunday == $dayJan) {
                    $weekSundayArray[] = $weekSunday;
                    $weekSunday        = $weekSunday + 7;
                }
            } else {
                if ($weekSunday == $dayJan) {
                    $weekSundayArray[] = $weekSunday - 1;
                    $weekSundayArray[] = $weekSunday;
                    $weekSunday        = $weekSunday + 7;
                }
            }
        }

        $consumption_time = [];

        for ($days = 1; $days <= 365; $days++) {
            if ($days <= 31) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time3 = 0; $time3 < 24; $time3++) {
                        $consumption_time['jan'][]                                          = ($time3 < 10) ? '0' . $time3 . ':00' : $time3 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_janArray[$time3];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {
                        for ($time = 0; $time < 24; $time++) {
                            $consumption_time['jan'][]                                          = ($time < 10) ? '0' . $time . ':00' : $time . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_janArray[$time];
                        }
                    } else {
                        for ($time2 = 0; $time2 < 24; $time2++) {
                            $consumption_time['jan'][]                                          = ($time2 < 10) ? '0' . $time2 . ':00' : $time2 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_janArray[$time2];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 59) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time4 = 0; $time4 < 24; $time4++) {
                        $consumption_time['feb'][]                                          = ($time4 < 10) ? '0' . $time4 . ':00' : $time4 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_febArray[$time4];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time5 = 0; $time5 < 24; $time5++) {
                            $consumption_time['feb'][]                                          = ($time5 < 10) ? '0' . $time5 . ':00' : $time5 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_febArray[$time5];
                        }
                    } else {
                        for ($time6 = 0; $time6 < 24; $time6++) {
                            $consumption_time['feb'][]                                          = ($time6 < 10) ? '0' . $time6 . ':00' : $time6 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_febArray[$time6];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 90) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time7 = 0; $time7 < 24; $time7++) {
                        $consumption_time['mar'][]                                          = ($time7 < 10) ? '0' . $time7 . ':00' : $time7 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_marArray[$time7];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time8 = 0; $time8 < 24; $time8++) {
                            $consumption_time['mar'][]                                          = ($time8 < 10) ? '0' . $time8 . ':00' : $time8 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_marArray[$time8];
                        }
                    } else {
                        for ($time9 = 0; $time9 < 24; $time9++) {
                            $consumption_time['mar'][]                                          = ($time9 < 10) ? '0' . $time9 . ':00' : $time9 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_marArray[$time9];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 120) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time11 = 0; $time11 < 24; $time11++) {
                        $consumption_time['apr'][]                                          = ($time11 < 10) ? '0' . $time11 . ':00' : $time11 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_aprArray[$time11];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time12 = 0; $time12 < 24; $time12++) {
                            $consumption_time['apr'][]                                          = ($time12 < 10) ? '0' . $time12 . ':00' : $time12 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_aprArray[$time12];
                        }
                    } else {
                        for ($time14 = 0; $time14 < 24; $time14++) {
                            $consumption_time['apr'][]                                          = ($time14 < 10) ? '0' . $time14 . ':00' : $time14 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_aprArray[$time14];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 151) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time15 = 0; $time15 < 24; $time15++) {
                        $consumption_time['may'][]                                          = ($time15 < 10) ? '0' . $time15 . ':00' : $time15 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_mayArray[$time15];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time16 = 0; $time16 < 24; $time16++) {
                            $consumption_time['may'][]                                          = ($time16 < 10) ? '0' . $time16 . ':00' : $time16 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_mayArray[$time16];
                        }
                    } else {
                        for ($time17 = 0; $time17 < 24; $time17++) {
                            $consumption_time['may'][]                                          = ($time17 < 10) ? '0' . $time17 . ':00' : $time17 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_mayArray[$time17];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 181) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time18 = 0; $time18 < 24; $time18++) {
                        $consumption_time['jun'][]                                          = ($time18 < 10) ? '0' . $time18 . ':00' : $time18 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_junArray[$time18];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time19 = 0; $time19 < 24; $time19++) {
                            $consumption_time['jun'][]                                          = ($time19 < 10) ? '0' . $time19 . ':00' : $time19 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_junArray[$time19];
                        }
                    } else {
                        for ($time20 = 0; $time20 < 24; $time20++) {
                            $consumption_time['jun'][]                                          = ($time20 < 10) ? '0' . $time20 . ':00' : $time20 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_junArray[$time20];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 212) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time21 = 0; $time21 < 24; $time21++) {
                        $consumption_time['jul'][]                                          = ($time21 < 10) ? '0' . $time21 . ':00' : $time21 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_julArray[$time21];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time22 = 0; $time22 < 24; $time22++) {
                            $consumption_time['jul'][]                                          = ($time22 < 10) ? '0' . $time22 . ':00' : $time22 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_julArray[$time22];
                        }
                    } else {
                        for ($time23 = 0; $time23 < 24; $time23++) {
                            $consumption_time['jul'][]                                          = ($time23 < 10) ? '0' . $time23 . ':00' : $time23 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_julArray[$time23];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 243) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time24 = 0; $time24 < 24; $time24++) {
                        $consumption_time['aug'][]                                          = ($time24 < 10) ? '0' . $time24 . ':00' : $time24 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_augArray[$time24];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time25 = 0; $time25 < 24; $time25++) {
                            $consumption_time['aug'][]                                          = ($time25 < 10) ? '0' . $time25 . ':00' : $time25 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_augArray[$time25];
                        }
                    } else {
                        for ($time26 = 0; $time26 < 24; $time26++) {
                            $consumption_time['aug'][]                                          = ($time26 < 10) ? '0' . $time26 . ':00' : $time26 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_augArray[$time26];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 273) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time27 = 0; $time27 < 24; $time27++) {
                        $consumption_time['sep'][]                                          = ($time27 < 10) ? '0' . $time27 . ':00' : $time27 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_sepArray[$time27];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time28 = 0; $time28 < 24; $time28++) {
                            $consumption_time['sep'][]                                          = ($time28 < 10) ? '0' . $time28 . ':00' : $time28 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_sepArray[$time28];
                        }
                    } else {
                        for ($time29 = 0; $time29 < 24; $time29++) {
                            $consumption_time['sep'][]                                          = ($time29 < 10) ? '0' . $time29 . ':00' : $time29 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_sepArray[$time29];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 304) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time30 = 0; $time30 < 24; $time30++) {
                        $consumption_time['oct'][]                                          = ($time30 < 10) ? '0' . $time30 . ':00' : $time30 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_octArray[$time30];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time31 = 0; $time31 < 24; $time31++) {
                            $consumption_time['oct'][]                                          = ($time31 < 10) ? '0' . $time31 . ':00' : $time31 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_octArray[$time31];
                        }
                    } else {
                        for ($time32 = 0; $time32 < 24; $time32++) {
                            $consumption_time['oct'][]                                          = ($time32 < 10) ? '0' . $time32 . ':00' : $time32 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_octArray[$time32];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 334) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time33 = 0; $time33 < 24; $time33++) {
                        $consumption_time['nov'][]                                          = ($time33 < 10) ? '0' . $time33 . ':00' : $time33 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_novArray[$time33];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time34 = 0; $time34 < 24; $time34++) {
                            $consumption_time['nov'][]                                          = ($time34 < 10) ? '0' . $time34 . ':00' : $time34 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_novArray[$time34];
                        }
                    } else {
                        for ($time35 = 0; $time35 < 24; $time35++) {
                            $consumption_time['nov'][]                                          = ($time35 < 10) ? '0' . $time35 . ':00' : $time35 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_novArray[$time35];
                        }
                    }
                } // End If
                ## End Month by data
            } elseif ($days <= 365) {
                ## Month by data
                if ($nonWorkDay == 0) {
                    for ($time36 = 0; $time36 < 24; $time36++) {
                        $consumption_time['dec'][]                                          = ($time36 < 10) ? '0' . $time36 . ':00' : $time36 . ':00';
                        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_decArray[$time36];
                    }
                } else {
                    if (in_array($days, $weekSundayArray)) {

                        for ($time37 = 0; $time37 < 24; $time37++) {
                            $consumption_time['dec'][]                                          = ($time37 < 10) ? '0' . $time37 . ':00' : $time37 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileNonWorkingDays_decArray[$time37];
                        }
                    } else {
                        for ($time38 = 0; $time38 < 24; $time38++) {
                            $consumption_time['dec'][]                                          = ($time38 < 10) ? '0' . $time38 . ':00' : $time38 . ':00';
                            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[] = $RatioOfUsageSelectedDefaultProfileWorkingDays_decArray[$time38];
                        }
                    }
                } // End If
                ## End Month by data
            }
            // echo $dataTable;
            // Data from Annual Solar Data  x Size X Orientation x Tilt
        }

        $data['consumption_profile'] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr;
        $data['consumption_time']    = $consumption_time;
        return $data;
    }

    public function manage_meter_data($proposal_id = FALSE)
    {
        $data['proposal_id']            = $proposal_id;
        $proposal_data                  = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));
        $data['is_meter_data']          = $proposal_data['is_meter_data'];
        $data['meter_data_file']        = $proposal_data['meter_data_file'];
        $data['meter_data_graph_dates'] = ($proposal_data['meter_data_graph_dates'] != '' && $proposal_data['meter_data_graph_dates'] != NULL) ? json_decode($proposal_data['meter_data_graph_dates']) : '';
        $this->load->view('partials/header', $data);
        $this->load->view('meter_data/main');
        $this->load->view('partials/footer');
    }

    public function fetch_meter_data_file()
    {
        //phpinfo();
        $data                              = array();
        //$filename = '8419746f625c3084a132cc5bb4c723c2.csv';
        //$filename = '362065a1837cba70617c10f0be2d2189.csv';
        $filename                          = $this->input->post('file_name');
        $final_data                        = $this->read_meter_data_file($filename, false);
        //print_r($final_data);die;
        $proposal_data                     = $this->common->fetch_row('tbl_solar_proposal', '*', array('meter_data_file' => $filename));
        $meter_data                        = $this->calculate_consumption_profile_data_by_meter_data($proposal_data, $final_data);
        $data                              = $this->calcualte_summer_and_weekend_load_graph_data_from_meter_data($meter_data);
        $data['meter_data']                = $meter_data['final_data'];
        $data['meter_data_missing_months'] = $meter_data['missing_months'];

        //echo "<pre>";
        //print_r($data);die;
        echo json_encode($data);
        die;
    }

    private function calcualte_summer_and_weekend_load_graph_data_from_meter_data(
        $meter_data,
        $DataFromAnnualSolarDataXSizeXOrientationXTiltArr = [],
        $SolarConsumptionArr = []
    ) {

        //print_r($DataFromAnnualSolarDataXSizeXOrientationXTiltArr);die;

        $full_year_meter_data       = $meter_data['consumption_data'];
        $full_year_meter_data_dates = $meter_data['consumption_date'];
        $full_year_meter_data_times = $meter_data['consumption_time'];

        //Average Load vs Solar Production Graph Data
        $final_avg_load_graph_data    = $final_avg_sol_prd_graph_data = $final_avg_export_graph_data  = $final_avg_offset_graph_data  = [];
        $avg_load_graph_data          = $avg_sol_prd_graph_data       = $avg_export_prd_graph_data    = $avg_offset_prd_graph_data    = [];
        $is_trading_days              = true;

        foreach ($full_year_meter_data as $key => $value) {

            $month_int = date('m', strtotime($full_year_meter_data_dates[$key]));
            $year      = date('Y', strtotime($full_year_meter_data_dates[$key]));
            $month     = date('M', strtotime($full_year_meter_data_dates[$key]));
            $month     = strtolower($month);
            $hr        = (int) date('H', strtotime($full_year_meter_data_times[$key]));

            if (!isset($avg_load_graph_data[$month][$hr])) {
                $avg_load_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_sol_prd_graph_data[$month][$hr])) {
                $avg_sol_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_export_prd_graph_data[$month][$hr])) {
                $avg_export_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_offset_prd_graph_data[$month][$hr])) {
                $avg_offset_prd_graph_data[$month][$hr] = [];
            }
            // $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
            if ($is_trading_days && $value > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) < 6)) {
                // echo $month."key::".$key."hr::".$hr;print_r($value);
                $load                                     = $sol_prd                                  = 0;
                $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
                $avg_sol_prd_graph_data[$month][$hr][]    = $sol_prd                                  = (isset($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key])) ? $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key] : 0;
                $export                                   = ($sol_prd - $load);
                $avg_export_prd_graph_data[$month][$hr][] = ($export > 0) ? $export : 0;
                $avg_offset_prd_graph_data[$month][$hr][] = (isset($SolarConsumptionArr[$key])) ? $SolarConsumptionArr[$key] : 0;
            } else if (!$is_trading_days && $value[$value] > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) >= 6)) {
                //  echo "else::";echo $month."key::".$key."hr::".$hr;print_r($value);
                $load                                     = $sol_prd                                  = 0;
                $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
                $avg_sol_prd_graph_data[$month][$hr][]    = $sol_prd                                  = (isset($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key])) ? $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key] : 0;
                $export                                   = ($sol_prd - $load);
                $avg_export_prd_graph_data[$month][$hr][] = ($export > 0) ? $export : 0;
                $avg_offset_prd_graph_data[$month][$hr][] = (isset($SolarConsumptionArr[$key])) ? $SolarConsumptionArr[$key] : 0;
            }

            $no_of_days_24hr = $this->components->days_in_month($month_int, $year);

            if ($full_year_meter_data_dates[$key] == $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00') {

                //Load Final
                if (array_sum($avg_load_graph_data[$month][0]) == 0) {
                    for ($i = 0; $i < 24; $i++) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$i] = 0;
                    }
                } else {

                    foreach ($avg_load_graph_data[$month] as $key => $value) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$key] = (count($value) > 0) ? array_sum($value) / count($value) : 0;
                        // echo "<pre>";
                        // echo $key."Hello";print_r(array_sum($value));
                        // print_r(count($value));
                        //print_r($final_avg_load_graph_data[$month][$key]);
                    }
                }


                //Solar Prd Final
                foreach ($avg_sol_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_sol_prd_graph_data[$month])) {
                        $final_avg_sol_prd_graph_data[$month] = [];
                    }
                    $final_avg_sol_prd_graph_data[$month][$key] = (count($avg_sol_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_sol_prd_graph_data[$month][$key]) / count($avg_sol_prd_graph_data[$month][$key]) : 0;
                }

                //Export final
                foreach ($avg_export_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_export_graph_data[$month])) {
                        $final_avg_export_graph_data[$month] = [];
                    }
                    $final_avg_export_graph_data[$month][$key] = (count($avg_export_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_export_prd_graph_data[$month][$key]) / count($avg_export_prd_graph_data[$month][$key]) : 0;
                }

                //Offset Final
                foreach ($avg_offset_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_offset_graph_data[$month])) {
                        $final_avg_offset_graph_data[$month] = [];
                    }
                    $final_avg_offset_graph_data[$month][$key] = (count($avg_offset_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_offset_prd_graph_data[$month][$key]) / count($avg_offset_prd_graph_data[$month][$key]) : 0;
                }
            }
        }
        //print_r($avg_load_graph_data);
        // die();

        //echo "<pre>";
        //print_r($avg_sol_prd_graph_data);die;


        $days   = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $counts = [];

        foreach ($days as $key1 => $value1) {
            for ($w = 0; $w < count($full_year_meter_data); $w++) {
                $day      = date('D', strtotime($full_year_meter_data_dates[$w]));
                $curr_day = strtolower($day);
                $hr       = (int) date('H', strtotime($full_year_meter_data_times[$w]));

                if ($value1 != $curr_day) {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                    continue;
                } else {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                }

                if (isset($counts[$curr_day])) {
                    $counts[$curr_day] += 1;
                } else {
                    $counts[$curr_day] = 1;
                }


                if (!isset($weekend_load_graph_data[$value1])) {
                    $weekend_load_graph_data[$value1] = [];
                }
                if (!isset($weekend_load_graph_data[$value1][$hr])) {
                    $weekend_load_graph_data[$value1][$hr] = 0;
                }
                $weekend_load_graph_data[$value1][$hr] += $full_year_meter_data[$w];
            }
        }


        $data['weekend_load_graph_data']        = $weekend_load_graph_data;
        $data['avg_monthly_load_graph_data']    = $final_avg_load_graph_data;
        $data['avg_monthly_sol_prd_graph_data'] = $final_avg_sol_prd_graph_data;
        $data['avg_monthly_export_graph_data']  = $final_avg_export_graph_data;
        $data['avg_monthly_offset_graph_data']  = $final_avg_offset_graph_data;
        // if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
        //         echo '<pre>';
        //         print_r($data['avg_monthly_load_graph_data']);
        //         echo '</pre>';
        //         die;
        //     }
        return $data;
    }

    private function calcualte_summer_and_weekend_load_graph_data_from_custom_data($meter_data, $proposal_data)
    {

        $meu = $proposal_data['monthly_electricity_usage'];
        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = $meter_data['consumption_profile'];
        $consumption_time                                                 = $meter_data['consumption_time'];
        $combined_consumption_time                                        = [];
        //echo "<pre>";
        //print_r(count($consumption_time['nov'])/24);die;
        //Average Load vs Solar Production Graph Data
        $avg_load_graph_data                                              = $avg_load_sol_prd_graph_data                                      = [];

        $months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];

        //echo (count($month_keys));die;
        $i                    = 1;
        $j                    = 0;
        $consumption_data_key = -1;
        foreach ($months as $key1 => $value1) {
            $k          = 0;
            $month_keys = $consumption_time[$value1];
            foreach ($month_keys as $key => $value) {
                $consumption_data_key        = $consumption_data_key + 1;
                $combined_consumption_time[] = $value;
                //For Time Period '00:00'
                if ($value == '00:00') {
                    if (isset($avg_load_graph_data[$value1][0])) {
                        $avg_load_graph_data[$value1][0] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][0] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }
                //For Time Period '01:00'
                if ($value == '01:00') {
                    if (isset($avg_load_graph_data[$value1][1])) {
                        $avg_load_graph_data[$value1][1] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][1] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '02:00'
                if ($value == '02:00') {
                    if (isset($avg_load_graph_data[$value1][2])) {
                        $avg_load_graph_data[$value1][2] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][2] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '03:00'
                if ($value == '03:00') {
                    if (isset($avg_load_graph_data[$value1][3])) {
                        $avg_load_graph_data[$value1][3] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][3] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '04:00'
                if ($value == '04:00') {
                    if (isset($avg_load_graph_data[$value1][4])) {
                        $avg_load_graph_data[$value1][4] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][4] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '05:00'
                if ($value == '05:00') {
                    if (isset($avg_load_graph_data[$value1][5])) {
                        $avg_load_graph_data[$value1][5] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][5] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '06:00'
                if ($value == '06:00') {
                    if (isset($avg_load_graph_data[$value1][6])) {
                        $avg_load_graph_data[$value1][6] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][6] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '07:00'
                if ($value == '07:00') {
                    if (isset($avg_load_graph_data[$value1][7])) {
                        $avg_load_graph_data[$value1][7] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][7] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '08:00'
                if ($value == '08:00') {
                    if (isset($avg_load_graph_data[$value1][8])) {
                        $avg_load_graph_data[$value1][8] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][8] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '09:00'
                if ($value == '09:00') {
                    if (isset($avg_load_graph_data[$value1][9])) {
                        $avg_load_graph_data[$value1][9] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][9] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '10:00'
                if ($value == '10:00') {
                    if (isset($avg_load_graph_data[$value1][10])) {
                        $avg_load_graph_data[$value1][10] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][10] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '11:00'
                if ($value == '11:00') {
                    if (isset($avg_load_graph_data[$value1][11])) {
                        $avg_load_graph_data[$value1][11] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][11] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '12:00'
                if ($value == '12:00') {
                    if (isset($avg_load_graph_data[$value1][12])) {
                        $avg_load_graph_data[$value1][12] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][12] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '13:00'
                if ($value == '13:00') {
                    if (isset($avg_load_graph_data[$value1][13])) {
                        $avg_load_graph_data[$value1][13] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][13] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '14:00'
                if ($value == '14:00') {
                    if (isset($avg_load_graph_data[$value1][14])) {
                        $avg_load_graph_data[$value1][14] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][14] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '15:00'
                if ($value == '15:00') {
                    if (isset($avg_load_graph_data[$value1][15])) {
                        $avg_load_graph_data[$value1][15] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][15] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '16:00'
                if ($value == '16:00') {
                    if (isset($avg_load_graph_data[$value1][16])) {
                        $avg_load_graph_data[$value1][16] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][16] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '17:00'
                if ($value == '17:00') {
                    if (isset($avg_load_graph_data[$value1][17])) {
                        $avg_load_graph_data[$value1][17] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][17] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '18:00'
                if ($value == '18:00') {
                    if (isset($avg_load_graph_data[$value1][18])) {
                        $avg_load_graph_data[$value1][18] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][18] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '19:00'
                if ($value == '19:00') {
                    if (isset($avg_load_graph_data[$value1][19])) {
                        $avg_load_graph_data[$value1][19] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][19] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }


                //For Time Period '20:00'
                if ($value == '20:00') {
                    if (isset($avg_load_graph_data[$value1][20])) {
                        $avg_load_graph_data[$value1][20] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][20] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '21:00'
                if ($value == '21:00') {
                    if (isset($avg_load_graph_data[$value1][21])) {
                        $avg_load_graph_data[$value1][21] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][21] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }


                //For Time Period '22:00'
                if ($value == '22:00') {
                    if (isset($avg_load_graph_data[$value1][22])) {
                        $avg_load_graph_data[$value1][22] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][22] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }

                //For Time Period '23:00'
                if ($value == '23:00') {
                    if (isset($avg_load_graph_data[$value1][23])) {
                        $avg_load_graph_data[$value1][23] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    } else {
                        $avg_load_graph_data[$value1][23] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$consumption_data_key];
                    }
                }
                $meu = 0;
                if ($k == (count($month_keys) - 1)) {
                    $month_days = count($month_keys) / 24;
                    $total_sum_month = array_sum($avg_load_graph_data[$value1]);
                    foreach ($avg_load_graph_data[$value1] as $key2 => $value2) {
                        if ($meu > 0) {
                            $avg_load_graph_data[$value1][$key2] = ($value2 / $total_sum_month) * $meu;
                        } else {
                            $avg_load_graph_data[$value1][$key2] = ($value2 / $month_days);
                        }
                    }
                }
                $k++;
                $j++;
            }
            $i++;
        }



        //As we dont have dates so we consider index 0 to be monday and proceed
        $weekend_load_graph_data = [];
        $days                    = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $counts                  = [];
        $j                       = 0;
        for ($w = 0; $w < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr) - 1; $w++) {
            $value = $w;

            if ($j >= 0 && $j <= 23) {
                $value1   = $curr_day = $days[0];
            } else if ($j > 23 && $j <= 47) {
                $value1   = $curr_day = $days[1];
            } else if ($j > 47 && $j <= 71) {
                $value1   = $curr_day = $days[2];
            } else if ($j > 71 && $j <= 95) {
                $value1   = $curr_day = $days[3];
            } else if ($j > 95 && $j <= 119) {
                $value1   = $curr_day = $days[4];
            } else if ($j > 119 && $j <= 143) {
                $value1   = $curr_day = $days[5];
            } else if ($j > 143 && $j <= 167) {
                $value1   = $curr_day = $days[6];
            }

            //$day = date('D', strtotime($dates[$w]));
            //$curr_day = strtolower($day);

            if (isset($counts[$curr_day])) {
                $counts[$curr_day] += 1;
            } else {
                $counts[$curr_day] = 1;
            }



            //For Time Period '00:00'
            if ($combined_consumption_time[$value] == '00:00') {
                if (isset($weekend_load_graph_data[$value1][0])) {
                    $weekend_load_graph_data[$value1][0] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][0] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '01:00'
            if ($combined_consumption_time[$value] == '01:00') {
                if (isset($weekend_load_graph_data[$value1][1])) {
                    $weekend_load_graph_data[$value1][1] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][1] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '02:00'
            if ($combined_consumption_time[$value] == '02:00') {
                if (isset($weekend_load_graph_data[$value1][2])) {
                    $weekend_load_graph_data[$value1][2] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][2] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '03:00'
            if ($combined_consumption_time[$value] == '03:00') {
                if (isset($weekend_load_graph_data[$value1][3])) {
                    $weekend_load_graph_data[$value1][3] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][3] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '04:00'
            if ($combined_consumption_time[$value] == '04:00') {
                if (isset($weekend_load_graph_data[$value1][4])) {
                    $weekend_load_graph_data[$value1][4] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][4] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '05:00'
            if ($combined_consumption_time[$value] == '05:00') {
                if (isset($weekend_load_graph_data[$value1][5])) {
                    $weekend_load_graph_data[$value1][5] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][5] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '06:00'
            if ($combined_consumption_time[$value] == '06:00') {
                if (isset($weekend_load_graph_data[$value1][6])) {
                    $weekend_load_graph_data[$value1][6] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][6] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '07:00'
            if ($combined_consumption_time[$value] == '07:00') {
                if (isset($weekend_load_graph_data[$value1][7])) {
                    $weekend_load_graph_data[$value1][7] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][7] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '08:00'
            if ($combined_consumption_time[$value] == '08:00') {
                if (isset($weekend_load_graph_data[$value1][8])) {
                    $weekend_load_graph_data[$value1][8] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][8] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '09:00'
            if ($combined_consumption_time[$value] == '09:00') {
                if (isset($weekend_load_graph_data[$value1][9])) {
                    $weekend_load_graph_data[$value1][9] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][9] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '10:00'
            if ($combined_consumption_time[$value] == '10:00') {
                if (isset($weekend_load_graph_data[$value1][10])) {
                    $weekend_load_graph_data[$value1][10] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][10] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '11:00'
            if ($combined_consumption_time[$value] == '11:00') {
                if (isset($weekend_load_graph_data[$value1][11])) {
                    $weekend_load_graph_data[$value1][11] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][11] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '12:00'
            if ($combined_consumption_time[$value] == '12:00') {
                if (isset($weekend_load_graph_data[$value1][12])) {
                    $weekend_load_graph_data[$value1][12] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][12] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '13:00'
            if ($combined_consumption_time[$value] == '13:00') {
                if (isset($weekend_load_graph_data[$value1][13])) {
                    $weekend_load_graph_data[$value1][13] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][13] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '14:00'
            if ($combined_consumption_time[$value] == '14:00') {
                if (isset($weekend_load_graph_data[$value1][14])) {
                    $weekend_load_graph_data[$value1][14] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][14] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '15:00'
            if ($combined_consumption_time[$value] == '15:00') {
                if (isset($weekend_load_graph_data[$value1][15])) {
                    $weekend_load_graph_data[$value1][15] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][15] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '16:00'
            if ($combined_consumption_time[$value] == '16:00') {
                if (isset($weekend_load_graph_data[$value1][16])) {
                    $weekend_load_graph_data[$value1][16] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][16] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '17:00'
            if ($combined_consumption_time[$value] == '17:00') {
                if (isset($weekend_load_graph_data[$value1][17])) {
                    $weekend_load_graph_data[$value1][17] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][17] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '18:00'
            if ($combined_consumption_time[$value] == '18:00') {
                if (isset($weekend_load_graph_data[$value1][18])) {
                    $weekend_load_graph_data[$value1][18] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][18] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '19:00'
            if ($combined_consumption_time[$value] == '19:00') {
                if (isset($weekend_load_graph_data[$value1][19])) {
                    $weekend_load_graph_data[$value1][19] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][19] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }


            //For Time Period '20:00'
            if ($combined_consumption_time[$value] == '20:00') {
                if (isset($weekend_load_graph_data[$value1][20])) {
                    $weekend_load_graph_data[$value1][20] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][20] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '21:00'
            if ($combined_consumption_time[$value] == '21:00') {
                if (isset($weekend_load_graph_data[$value1][21])) {
                    $weekend_load_graph_data[$value1][21] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][21] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }


            //For Time Period '22:00'
            if ($combined_consumption_time[$value] == '22:00') {
                if (isset($weekend_load_graph_data[$value1][22])) {
                    $weekend_load_graph_data[$value1][22] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][22] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            //For Time Period '23:00'
            if ($combined_consumption_time[$value] == '23:00') {
                if (isset($weekend_load_graph_data[$value1][23])) {
                    $weekend_load_graph_data[$value1][23] += $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                } else {
                    $weekend_load_graph_data[$value1][23] = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$value];
                }
            }

            $j++;
            if ($j == 168) {
                $j = 0;
            }
        }


        foreach ($days as $key => $value) {
            $total_days = $counts[$value];
            $total_sum_week = array_sum($weekend_load_graph_data[$value]);
            foreach ($weekend_load_graph_data[$value] as $key1 => $value1) {
                //$weekend_load_graph_data[$value][$key1] = ($value1 / $total_days) * 24;
                if ($meu > 0) {
                    $weekend_load_graph_data[$value][$key1] = ($value1 / $total_sum_week) * $meu;
                } else {
                    $weekend_load_graph_data[$value][$key1] = ($value1 / $total_days) * 24;
                }
            }
        }



        $data['weekend_load_graph_data']     = $weekend_load_graph_data;
        $data['avg_monthly_load_graph_data'] = $avg_load_graph_data;

        return $data;
    }

    /**private function read_meter_data_file($filename, $columns = true) {
        $filename = __DIR__ . './../../assets/uploads/meter_data_files/' . $filename;
        $file     = fopen($filename, "r");

        ini_set('auto_detect_line_endings', true);
        $count      = 0;
        $final_data = $rows       = [];
        while (($getData    = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($count == 0) {
                $columns1 = $getData;
                if ($columns) {
                    $final_data[] = $columns1;
                }
            } else {
                for ($i = 0; $i < count($getData); $i++) {
                    //$rows1[$i] = str_replace('"', "", $explode_data[$i]);
                    $rows[$i] = ($getData[$i] == '' || $getData[$i] == 'NULL') ? NULL : $getData[$i];
                }
                $final_data[] = $rows;
            }
            $count++;
        }
        //print_r($final_data);die;
        fclose($file);
        return $final_data;
    }*/

    public function manage_simpro_sales_settings()
    {
        $data                   = array();
        $data['formula_fields'] = array(
            ['name' => 'HB - Approved Booking Forms', 'class' => '', 'alias' => 'hb_approved_booking_forms', 'editable' => true],
            ['name' => 'HB - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'hb_submitted_to_ap', 'editable' => true],
            ['name' => 'PL / Batten / Tubes - Approved Booking Forms', 'class' => '', 'alias' => 'p_b_t_approved', 'editable' => true],
            ['name' => 'PL / Batten / Tubes - Installed', 'class' => 'group_last_element', 'alias' => 'p_b_t_installed', 'editable' => true],
            ['name' => 'Floodlights - Approved Booking Forms', 'class' => '', 'alias' => 'floodlight_approved', 'editable' => true],
            ['name' => 'Floodlights - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'floodlight_submitted', 'editable' => true],
            ['name' => 'Solar - Received', 'class' => '', 'alias' => 'solar_received', 'editable' => true],
            ['name' => 'Solar - Approved', 'class' => '', 'alias' => 'solar_approved', 'editable' => true],
            ['name' => 'Solar - Submitted to AP', 'class' => 'group_last_element', 'alias' => 'solar_submitted', 'editable' => true],
            ['name' => 'Solar - Cancelled (Based on received Date)', 'class' => '', 'alias' => 'solar_cancelled', 'editable' => true],
        );
        $data['formula_data']   = $this->common->fetch_where('tbl_simpro_sales_formula', '*', NULL);

        $target_vic_data = $this->common->fetch_where('tbl_simpro_sales_target', '*', array('state_id' => 7));
        foreach ($target_vic_data as $key => $value) {
            $target_vic_data[$key]['target']    = json_decode($target_vic_data[$key]['target']);
            $target_vic_data[$key]['weighting'] = json_decode($target_vic_data[$key]['weighting']);
        }
        $data['target_vic_data'] = $target_vic_data;

        $target_nsw_data = $this->common->fetch_where('tbl_simpro_sales_target', '*', array('state_id' => 2));
        foreach ($target_nsw_data as $key => $value) {
            $target_nsw_data[$key]['target']    = json_decode($target_nsw_data[$key]['target']);
            $target_nsw_data[$key]['weighting'] = json_decode($target_nsw_data[$key]['weighting']);
        }
        $data['target_nsw_data'] = $target_nsw_data;
        $this->load->view('partials/header', $data);
        $this->load->view('settings/sales_dashboard');
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
    }

    public function upload_simpro_sales_dump()
    {
        try {
            if (!isset($_FILES["simpro_dump_csv_file"])) {
                $data['status']  = 'Please Select a CSV File';
                $data['success'] = FALSE;
                return $data;
            }
            //$filename = __DIR__ . './../../Kuga2604.csv';
            $filename = $_FILES["simpro_dump_csv_file"]["tmp_name"];
            if (TRUE) {
                $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
                if (!in_array($_FILES['simpro_dump_csv_file']['type'], $mimes)) {
                    $data['status']  = 'Invalid File type. Please Upload a CSV file.';
                    $data['success'] = FALSE;
                    return $data;
                }
                $file = fopen($filename, "r");

                ini_set('auto_detect_line_endings', true);
                $count = 0;

                $sql           = "SHOW COLUMNS FROM " . $this->db->database . ".tbl_simpro_dump";
                $query         = $this->db->query($sql);
                $response      = $query->result_array();
                $fields        = array_column($response, 'Field');
                unset($fields[0]);
                $fields        = array_values($fields);
                $date_columns  = array(3, 8, 10, 13, 14, 16, 18);
                $columns       = $combined_data = $all_data      = array();

                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if ($count == 0) {
                        $columns = $fields;
                    } else if ($count > 0) {
                        for ($i = 0; $i < count($getData); $i++) {
                            if (in_array($i, $date_columns)) {
                                $date = explode('/', $getData[$i]);
                                if (count($date) >= 3) {
                                    $date[0]  = ($date[0] < 10) ? '0' . $date[0] : $date[0];
                                    $new_date = $date[2] . '-' . $date[1] . '-' . $date[0];
                                } else {
                                    $new_date = $getData[$i];
                                }
                                //$date = str_replace('-','/',$date);
                                //$rows[$i] =  date("Y-m-d H:i:s", strtotime($date));
                                $rows[$i] = $new_date;
                            } else {
                                if ($getData[$i] == '-' || $getData[$i] == '') {
                                    $rows[$i] = NULL;
                                } else {
                                    $rows[$i] = $getData[$i];
                                }
                            }
                        }
                        $combined_data = array_combine($columns, $rows);
                    }
                    $all_data[($count - 1)] = $combined_data;
                    $count++;
                }
                unset($all_data[-1]);
                fclose($file);

                //Empty the table
                $this->db->truncate('tbl_simpro_dump');
                //Insert Batch
                $stat = $this->db->insert_batch('tbl_simpro_dump', $all_data);
                if (!$stat) {
                    $data['status']  = 'CSV File Import Failed';
                    $data['success'] = FALSE;
                } else {
                    $data['status']  = 'CSV File has been successfully Imported';
                    $data['success'] = TRUE;
                }
            }
        } catch (Exception $e) {
            $data['status']  = 'Looks like something wnt wrong while importing the csv';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function save_simpro_sales_settings()
    {
        if (!empty($this->input->post())) {
            $target_vic_data = $this->input->post('target_data_vic');
            $target_nsw_data = $this->input->post('target_data_nsw');
            $formula_data    = $this->input->post('formula');

            if (isset($target_vic_data) && !empty($target_vic_data)) {
                for ($i = 0; $i < count($target_vic_data); $i++) {
                    $target_data = $this->common->fetch_row('tbl_simpro_sales_target', 'id', array('segment' => ($i + 1), 'state_id' => 7));

                    $form_data['segment']           = ($i + 1);
                    $form_data['state_id']          = 7;
                    $form_data['target_start_date'] = $target_vic_data[$i]['target_start_date'];
                    $form_data['target_end_date']   = $target_vic_data[$i]['target_end_date'];
                    $form_data['target']            = json_encode($target_vic_data[$i]['target']);
                    $form_data['weighting']         = json_encode($target_vic_data[$i]['weighting']);
                    if (empty($target_data)) {
                        $stat = $this->common->insert_data('tbl_simpro_sales_target', $form_data);
                    } else {
                        $stat = $this->common->update_data('tbl_simpro_sales_target', array('id' => $target_data['id']), $form_data);
                    }
                }
            }

            if (isset($target_nsw_data) && !empty($target_nsw_data)) {
                for ($i = 0; $i < count($target_nsw_data); $i++) {
                    $target_data                    = $this->common->fetch_row('tbl_simpro_sales_target', 'id', array('segment' => ($i + 1), 'state_id' => 2));
                    $form_data['segment']           = ($i + 1);
                    $form_data['state_id']          = 2;
                    $form_data['target_start_date'] = $target_nsw_data[$i]['target_start_date'];
                    $form_data['target_end_date']   = $target_nsw_data[$i]['target_end_date'];
                    $form_data['target']            = json_encode($target_nsw_data[$i]['target']);
                    $form_data['weighting']         = json_encode($target_nsw_data[$i]['weighting']);
                    if (empty($target_data)) {
                        $stat = $this->common->insert_data('tbl_simpro_sales_target', $form_data);
                    } else {
                        $stat = $this->common->update_data('tbl_simpro_sales_target', array('id' => $target_data['id']), $form_data);
                    }
                }
            }

            if (isset($formula_data) && !empty($formula_data)) {
                foreach ($formula_data as $key => $value) {
                    $exist_formula_data   = $this->common->fetch_row('tbl_simpro_sales_formula', 'id', array('field' => $key));
                    $form_data['formula'] = $value;
                    $form_data['field']   = $key;
                    if (empty($exist_formula_data)) {
                        $stat = $this->common->insert_data('tbl_simpro_sales_formula', $form_data);
                    } else {
                        $stat = $this->common->update_data('tbl_simpro_sales_formula', array('id' => $exist_formula_data['id']), $form_data);
                    }
                }
            }

            $data['status']  = 'Sales Setting Data Saved Successfully';
            $data['success'] = TRUE;
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function api_calculate_solar_proposal_cost($postData = [], $getData = [])
    {

        header('Access-Control-Allow-Origin: *');
        $data = array();

        if (!empty($postData)) {
            $_POST = $postData;
        }
        if (!empty($getData)) {
            $_GET = $getData;
        }

        //Get Proposal, Proposal Product and Proposal Additional Item Data
        $AnnualSolarData       = $this->common->fetch_where('tbl_annual_solar_data_new', '*', NULL);
        $proposal_finance_data = array();

        $postcode = $this->input->get('postcode');
        $area_id  = $this->input->get('area_id');
        if (isset($postcode) && $postcode != '') {
            $state_id  = $this->components->get_region_by_postcode($postcode);
            $area_data = $this->common->fetch_row('tbl_solar_areas', 'areaId', array('state_id' => $state_id));
            if (!empty($area_data)) {
                $proposal_data['area_id'] = $area_data['areaId'];
            }
        } else if (isset($area_id) && $area_id != '') {
            $proposal_data['area_id'] = $area_id;
        } else {
            $proposal_data['area_id'] = 1;
        }

        $proposal_data['total_system_size']         = $this->input->get('total_system_size');
        $proposal_data['feed_in_tariff']            = $this->input->get('feed_in_tariff');
        $proposal_data['monthly_electricity_usage'] = $this->input->get('monthly_electricity_usage');
        $proposal_data['monthly_electricity_usage'] = ($proposal_data['monthly_electricity_usage'] * 365) / 12;
        $proposal_data['daily_usage_charge']        = $this->input->get('daily_usage_charge');
        $proposal_data['custom_profile_id']         = $this->input->get('custom_profile_id');
        $proposal_data['electricity_rate']          = $this->input->get('electricity_rate');


        /*
          Adelaide = 1
          Brisbane = 3
          Canberra = 5
          Darwin = 6
          Hobart = 7
          Melbourne = 8
          Perth = 9
          Sydney = 10
         */

        if ($proposal_data['area_id'] == 1) {
            $dataTable = "adelaide";
        } elseif ($proposal_data['area_id'] == 3) {
            $dataTable = "brisbane";
        } elseif ($proposal_data['area_id'] == 5) {
            $dataTable = "canberra";
        } elseif ($proposal_data['area_id'] == 6) {
            $dataTable = "darwin";
        } elseif ($proposal_data['area_id'] == 7) {
            $dataTable = "hobart";
        } elseif ($proposal_data['area_id'] == 8) {
            $dataTable = "melbourne";
        } elseif ($proposal_data['area_id'] == 9) {
            $dataTable = "perth";
        } elseif ($proposal_data['area_id'] == 10) {
            $dataTable = "sydney";
        }

        #############################################################################
        //Consumption profile without Solar split into working / non working days 
        ##############################################################################

        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = array();
        $consumption_profile_data                                         = $this->calculate_consumption_profile_data_by_custom_data($proposal_data);
        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr = $consumption_profile_data['consumption_profile'];
        for ($r = 0; $r < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $r++) {
            $AnnualSolarDataByArea                              = $AnnualSolarData[$r][$dataTable];
            $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[] = $AnnualSolarDataByArea * $proposal_data['total_system_size'] / 100;
        }


        #############################################################################
        // *** END *** Consumption profile without Solar split into working / non working days 
        ##############################################################################
        ###################################################################
        // Solar consumption
        ################################################################
        $costAfterSolarTotal                                                = $costBeforeSolarTotal                                               = $exportTotal                                                        = $exportIncomeTotal                                                  = $SolarConsumptionTotal                                              = 0;
        $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal = 0;


        for ($i = 0; $i < count($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr); $i++) {
            if ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i] > $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i]) {
                $SolarConsumptionArr[] = number_format((float) $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i], 2, '.', '');
            } else {
                $SolarConsumptionArr[] = number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '');
            }

            if ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i] > $SolarConsumptionArr[$i]) {
                $ConsumptionAfterSolarArr[] = number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '') - $SolarConsumptionArr[$i];
            } else {
                $ConsumptionAfterSolarArr[] = $SolarConsumptionArr[$i] - number_format((float) $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i], 2, '.', '');
            }

            $exportArr[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$i] - $SolarConsumptionArr[$i];

            $exportTotal       = $exportTotal + $exportArr[$i];
            $exportIncomeArr[] = $exportArr[$i] * $proposal_data['feed_in_tariff'];
            $exportIncomeTotal = $exportIncomeTotal + $exportIncomeArr[$i];

            $energyRateArr[] = $proposal_data['electricity_rate'];

            $energyRateVal = $energyRateArr[$i];

            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysVal = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i];

            $costBeforeSolarArr[] = ($energyRateVal * $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysVal);

            $costAfterSolarArr[] = number_format((float) ($energyRateArr[$i] * $ConsumptionAfterSolarArr[$i]), 2, '.', '');

            $costBeforeSolarTotal = $costBeforeSolarTotal + $costBeforeSolarArr[$i];

            $costAfterSolarTotal = $costAfterSolarTotal + $costAfterSolarArr[$i];

            $SolarConsumptionTotal = $SolarConsumptionTotal + $SolarConsumptionArr[$i];

            $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal = $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal + $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$i];
        }


        //$data['costBeforeSolarArr'] = $costBeforeSolarArr;
        //$data['costAfterSolarArr'] = $costAfterSolarArr;
        //$data['costBeforeSolarTotal'] = $costBeforeSolarTotal;
        //$data['costAfterSolarTotal'] = $costAfterSolarTotal;
        //$data['exportTotal'] = $exportTotal;
        //$data['exportIncomeTotal'] = $exportIncomeTotal;
        // Solar Daily Usage Charge        
        $solarDailyUsageCharge                  = $proposal_data['daily_usage_charge'] * 365;
        $costBeforeSolarTotal_forBefore         = $costBeforeSolarTotal + $solarDailyUsageCharge;
        $costAfterSolarTotal_forAfter           = ($costAfterSolarTotal + $solarDailyUsageCharge) - $exportIncomeTotal;
        $data['costBeforeSolarTotal_forBefore'] = $costBeforeSolarTotal_forBefore         = $costBeforeSolarTotal_forBefore * 1.1;
        $data['costAfterSolarTotal_forAfter']   = $costAfterSolarTotal_forAfter           = $costAfterSolarTotal_forAfter * 1.1;
        $divi                                   = 0;
        if ($costBeforeSolarTotal_forBefore != 0) {
            $divi = $costAfterSolarTotal_forAfter / $costBeforeSolarTotal_forBefore;
        }
        $minu  = 1 - $divi;
        $multi = $minu * 100;

        $data['annualBillSaving'] = $annualBillSaving         = $multi;
        $data['afterPercentage']  = 100 - $annualBillSaving;

        #############################################################################
        // *** END *** Solar consumption 
        ##############################################################################
        ###################################################################
        // Total System Size/Array Power Rating (recommended)
        ################################################################
        $maxInMeterData = max($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr);
        if ($proposal_data['area_id'] != 0) {
            $GetMaxValueAnnualSolarDataByArea = $this->proposal->get_MaxValueAnnualSolarDataByArea($proposal_data['area_id']);
        }

        $MaxValueAnnualSolarDataByArea = number_format($GetMaxValueAnnualSolarDataByArea['max_annual_solar'], 5);

        $cityMaximum = $MaxValueAnnualSolarDataByArea * 0.95;

        if ($cityMaximum != 0) {
            $kWBasedOnConsumption = $maxInMeterData * 100 / $cityMaximum;
        }
        //$data['maxInMeterData'] = $maxInMeterData;
        //$data['GetMaxValueAnnualSolarDataByArea'] = $GetMaxValueAnnualSolarDataByArea;
        //$data['MaxValueAnnualSolarDataByArea'] = $MaxValueAnnualSolarDataByArea;
        //$data['cityMaximum'] = $cityMaximum;
        //Rounding figure
        //  if($proposal_data['total_system_size'] != 0)
        if ($proposal_data['total_system_size'] == 0) {
            $split = explode(".", $kWBasedOnConsumption);
            if ($split[0] <= 5) {
                $roundingFigure = 5;
            } elseif ($split[0] <= 10) {
                $roundingFigure = 10;
            } elseif ($split[0] <= 15) {
                $roundingFigure = 15;
            } elseif ($split[0] <= 20) {
                $roundingFigure = 20;
            } elseif ($split[0] <= 25) {
                $roundingFigure = 25;
            } elseif ($split[0] <= 30) {
                $roundingFigure = 30;
            } elseif ($split[0] <= 40) {
                $roundingFigure = 40;
            } elseif ($split[0] <= 50) {
                $roundingFigure = 50;
            } elseif ($split[0] <= 60) {
                $roundingFigure = 60;
            } elseif ($split[0] <= 70) {
                $roundingFigure = 70;
            } elseif ($split[0] <= 80) {
                $roundingFigure = 80;
            } elseif ($split[0] <= 90) {
                $roundingFigure = 90;
            } elseif ($split[0] <= 100) {
                $roundingFigure = 100;
            }
        } else {
            $roundingFigure = $proposal_data['total_system_size'];
        }
        $data['roundingFigure'] = $roundingFigure;

        //$NumberOfPanels = number_format($roundingFigure/0.27,0);
        $TotalPanelArea         = number_format($roundingFigure * 6.056, 0);
        $data['TotalPanelArea'] = $TotalPanelArea;
        ###################################################################
        // **** END ***** Total System Size/Array Power Rating (recommended)
        ################################################################ 
        ###################################################################
        // Data from Annual Solar Data  x Size X Orientation x Tilt
        ###################################################################
        /*
          Adelaide = 1
          Brisbane = 3
          Canberra = 5
          Darwin = 6
          Hobart = 7
          Melbourne = 8
          Perth = 9
          Sydney = 10
         */
        if ($proposal_data['area_id'] == 1) {
            $dataTable = "adelaide";
        } elseif ($proposal_data['area_id'] == 3) {
            $dataTable = "brisbane";
        } elseif ($proposal_data['area_id'] == 5) {
            $dataTable = "canberra";
        } elseif ($proposal_data['area_id'] == 6) {
            $dataTable = "darwin";
        } elseif ($proposal_data['area_id'] == 7) {
            $dataTable = "hobart";
        } elseif ($proposal_data['area_id'] == 8) {
            $dataTable = "melbourne";
        } elseif ($proposal_data['area_id'] == 9) {
            $dataTable = "perth";
        } elseif ($proposal_data['area_id'] == 10) {
            $dataTable = "sydney";
        }

        //$GetAnnualSolarData = $this->common->fetch_where('tbl_annual_solar_data_new', '*', NULL);
        //$data['GetAnnualSolarData'] = $GetAnnualSolarData;
        ##############################################        
        // STC rebate value   
        ##############################################

        /*         * $GetSTCsCalculationsByPostCode = $this->common->fetch_where('tbl_solar_datastcscalculations', '*', array('startPostcode <=' => $customer_location_data['postcode']));

          $GetSTCsCalculationsByPostCodeCount = count($GetSTCsCalculationsByPostCode);

          $year = "postcodeYear" . date('Y', strtotime($proposal_data['created_at']));

          for ($j = 0; $j <= $GetSTCsCalculationsByPostCodeCount; $j++) {
          if ($j == $GetSTCsCalculationsByPostCodeCount - 1) {
          $GetSTCsCalculationsByPostCodeAndYear = $GetSTCsCalculationsByPostCode[$j][$year];
          }
          } */
        //$STCs = $roundingFigure * $GetSTCsCalculationsByPostCodeAndYear;
        //$STCrebateValue = $STCs * $getRebate['rebate'];
        //$data['GetSTCsCalculationsByPostCode'] = $GetSTCsCalculationsByPostCode;
        //$data['GetSTCsCalculationsByPostCodeAndYear'] = $GetSTCsCalculationsByPostCodeAndYear;
        $STCs                   = 0;
        $STCrebateValue         = 0;
        $data['STCs']           = $STCs;
        $data['STCrebateValue'] = $STCrebateValue;

        ##############################################        
        // TOTAL SOLAR OFFSET
        ##############################################
        $roiSolarDataByState               = $this->common->fetch_row('tbl_solar_data', '*', array('cityId' => $proposal_data['area_id']));
        $data['averageDailyKWhPerMonth1']  = $averageDailyKWhPerMonth1          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction1'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth2']  = $averageDailyKWhPerMonth2          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction2'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth3']  = $averageDailyKWhPerMonth3          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction3'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth4']  = $averageDailyKWhPerMonth4          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction4'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth5']  = $averageDailyKWhPerMonth5          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction5'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth6']  = $averageDailyKWhPerMonth6          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction6'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth7']  = $averageDailyKWhPerMonth7          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction7'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth8']  = $averageDailyKWhPerMonth8          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction8'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth9']  = $averageDailyKWhPerMonth9          = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction9'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth10'] = $averageDailyKWhPerMonth10         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction10'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth11'] = $averageDailyKWhPerMonth11         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction11'] * $proposal_data['total_system_size'] / 100);
        $data['averageDailyKWhPerMonth12'] = $averageDailyKWhPerMonth12         = ($roiSolarDataByState['averageDailykWHperMonthSolarProduction12'] * $proposal_data['total_system_size'] / 100);

        $EnergyProvidedBySolar = 0;

        $data['AverageDailyProduction'] = $AverageDailyProduction         = ($SolarConsumptionTotal + $exportTotal) / 365;
        //print_r($exportTotal);die;
        if ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal != 0) {
            $EnergyProvidedBySolar = ($SolarConsumptionTotal * 100) / $ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysTotal;
        }
        $data['EnergyProvidedBySolar'] = $EnergyProvidedBySolar;
        ##############################################        
        // END TOTAL SOLAR OFFSET
        ##############################################
        ##############################################        
        // Cost Comparison
        ##############################################

        $data['oldBillYer1']  = $oldBillYer1          = $costBeforeSolarTotal;
        $data['oldBillYer2']  = $oldBillYer2          = 1.1 * $oldBillYer1;
        $data['oldBillYer3']  = $oldBillYer3          = 1.1 * $oldBillYer2;
        $data['oldBillYer4']  = $oldBillYer4          = 1.1 * $oldBillYer3;
        $data['oldBillYer5']  = $oldBillYer5          = 1.1 * $oldBillYer4;
        $data['oldBillYer6']  = $oldBillYer6          = 1.1 * $oldBillYer5;
        $data['oldBillYer7']  = $oldBillYer7          = 1.1 * $oldBillYer6;
        $data['oldBillYer8']  = $oldBillYer8          = 1.1 * $oldBillYer7;
        $data['oldBillYer9']  = $oldBillYer9          = 1.1 * $oldBillYer8;
        $data['oldBillYer10'] = $oldBillYer10         = 1.1 * $oldBillYer9;

        $data['newBillYer1']  = $newBillYer1          = $costAfterSolarTotal;
        $data['newBillYer2']  = $newBillYer2          = 1.1 * $newBillYer1;
        $data['newBillYer3']  = $newBillYer3          = 1.1 * $newBillYer2;
        $data['newBillYer4']  = $newBillYer4          = 1.1 * $newBillYer3;
        $data['newBillYer5']  = $newBillYer5          = 1.1 * $newBillYer4;
        $data['newBillYer6']  = $newBillYer6          = 1.1 * $newBillYer5;
        $data['newBillYer7']  = $newBillYer7          = 1.1 * $newBillYer6;
        $data['newBillYer8']  = $newBillYer8          = 1.1 * $newBillYer7;
        $data['newBillYer9']  = $newBillYer9          = 1.1 * $newBillYer8;
        $data['newBillYer10'] = $newBillYer10         = 1.1 * $newBillYer9;



        $Finance_Yr1  = '';
        $Finance_Yr2  = '';
        $Finance_Yr3  = '';
        $Finance_Yr4  = '';
        $Finance_Yr5  = '';
        $Finance_Yr6  = '';
        $Finance_Yr7  = '';
        $Finance_Yr8  = '';
        $Finance_Yr9  = '';
        $Finance_Yr10 = '';

        if (empty($proposal_finance_data)) {
            $proposal_finance_data['term']                    = 0;
            $proposal_finance_data['monthly_payment_plan']    = 0;
            $proposal_finance_data['upfront_payment_options'] = 0;
        }

        $term = 0;
        if ($proposal_finance_data['term'] == 12) {
            $term = 1;
        }
        if ($proposal_finance_data['term'] == 24) {
            $term = 2;
        }
        if ($proposal_finance_data['term'] == 36) {
            $term = 3;
        }
        if ($proposal_finance_data['term'] == 48) {
            $term = 4;
        }
        if ($proposal_finance_data['term'] == 60) {
            $term = 5;
        }
        if ($proposal_finance_data['term'] == 72) {
            $term = 6;
        }
        if ($proposal_finance_data['term'] == 84) {
            $term = 7;
        }
        $data['term'] = $term;

        $Oldbill_Newbill1  = $oldBillYer1 - $newBillYer1;
        $Oldbill_Newbill2  = $oldBillYer2 - $newBillYer2;
        $Oldbill_Newbill3  = $oldBillYer3 - $newBillYer3;
        $Oldbill_Newbill4  = $oldBillYer4 - $newBillYer4;
        $Oldbill_Newbill5  = $oldBillYer5 - $newBillYer5;
        $Oldbill_Newbill6  = $oldBillYer6 - $newBillYer6;
        $Oldbill_Newbill7  = $oldBillYer7 - $newBillYer7;
        $Oldbill_Newbill8  = $oldBillYer8 - $newBillYer8;
        $Oldbill_Newbill9  = $oldBillYer9 - $newBillYer9;
        $Oldbill_Newbill10 = $oldBillYer10 - $newBillYer10;

        $data['Benefit1']  = $Benefit1          = $Oldbill_Newbill1;
        $data['Benefit2']  = $Benefit2          = $Oldbill_Newbill2;
        $data['Benefit3']  = $Benefit3          = $Oldbill_Newbill3;
        $data['Benefit4']  = $Benefit4          = $Oldbill_Newbill4;
        $data['Benefit5']  = $Benefit5          = $Oldbill_Newbill5;
        $data['Benefit6']  = $Benefit6          = $Oldbill_Newbill6;
        $data['Benefit7']  = $Benefit7          = $Oldbill_Newbill7;
        $data['Benefit8']  = $Benefit8          = $Oldbill_Newbill8;
        $data['Benefit9']  = $Benefit9          = $Oldbill_Newbill9;
        $data['Benefit10'] = $Benefit10         = $Oldbill_Newbill10;

        ##############################################        
        // Cost Comparison
        ##############################################

        $Benefit       = $costBeforeSolarTotal - $costAfterSolarTotal;
        $ExportBenefit = $exportTotal * $proposal_data['feed_in_tariff'];

        $TotalRevenue           = 0;
        $twentyFiveYearsBenefit = 0;
        for ($z = 0; $z <= 24; $z++) {
            $benefitArr[]       = $Benefit;
            $exportArr[]        = $exportTotal;
            $ExportBenefitArr[] = $ExportBenefit;
            $TotalSavingsArr[]  = ($Benefit + $ExportBenefit) * 1.1;
            //$TotalSavingsArr[] = $newBenefit +$ExportBenefit;
            $exportTotal        = $exportTotal * 0.995;
            $Benefit            = $Benefit * 1.1 * 0.995;
            //$newBenefit = $newBenefit*1.1*0.995;
            $ExportBenefit      = $exportTotal * $proposal_data['feed_in_tariff'];
            if ($z < 10) {
                $TotalRevenue = $TotalRevenue + $TotalSavingsArr[$z];
            }
            if ($z < 12) {
                $annualBenefit = $TotalRevenue + $TotalSavingsArr[$z];
            }
            $twentyFiveYearsBenefit = $twentyFiveYearsBenefit + $TotalSavingsArr[$z];
        }
        $data['benefitArr']             = $benefitArr;
        $data['exportArr']              = $exportArr;
        $data['ExportBenefitArr']       = $ExportBenefitArr;
        $data['TotalSavingsArr']        = $TotalSavingsArr;
        $data['exportTotal']            = $exportTotal;
        $data['Benefit']                = $Benefit;
        $data['ExportBenefit']          = $ExportBenefit;
        $data['TotalRevenue']           = $TotalRevenue;
        $data['twentyFiveYearsBenefit'] = $twentyFiveYearsBenefit;
        $data['annualBenefit']          = $annualBenefit;

        $graph_data = $this->calcualte_summer_and_weekend_load_graph_data_from_custom_data($consumption_profile_data, $proposal_data);


        for ($wi = 1442; $wi <= 1465; $wi++) {
            $avg_load_sol_prd_graph_data[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$wi];
        }
        $data['avg_load_graph_data']         = $graph_data['avg_monthly_load_graph_data'];
        $data['avg_load_sol_prd_graph_data'] = $avg_load_sol_prd_graph_data;

        //Weekend Load vs Solar Production Graph Data
        //Weekend Load vs Solar Production Graph Data
        $count                           = 74;
        $weekend_load_graph_data         = $weekend_load_sol_prd_graph_data = [];


        for ($wi = 4322; $wi <= 4345; $wi++) {
            $weekend_load_sol_prd_graph_data[] = $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$wi];
        }

        $data['weekend_load_graph_data']         = $graph_data['weekend_load_graph_data'];
        $data['weekend_load_sol_prd_graph_data'] = $weekend_load_sol_prd_graph_data;


        //Typical Energy Production Day
        $typical_export_load_graph_data    = $typical_export_sol_prd_graph_data = $typical_export_export_graph_data  = $typical_export_offset_graph_data  = [];
        $a                                 = 24 * 28;
        for ($s = $a; $s <= ($a + 23); $s++) {
            $typical_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
        }
        for ($su = $a; $su <= ($a + 23); $su++) {
            $typical_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $a; $su <= ($a + 23); $su++) {
            $typical_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $a; $su <= ($a + 23); $su++) {
            $typical_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
        }
        $data['typical_export_load_graph_data']    = $typical_export_load_graph_data;
        $data['typical_export_sol_prd_graph_data'] = $typical_export_sol_prd_graph_data;
        $data['typical_export_export_graph_data']  = $typical_export_export_graph_data;
        $data['typical_export_offset_graph_data']  = $typical_export_offset_graph_data;

        //Worst Energy Production Day
        $worst_export_load_graph_data    = $worst_export_sol_prd_graph_data = $worst_export_export_graph_data  = $worst_export_offset_graph_data  = [];
        $b                               = 24 * 364;
        for ($s = $b; $s <= ($b + 23); $s++) {
            $worst_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
        }
        for ($su = $b; $su <= ($b + 23); $su++) {
            $worst_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $b; $su <= ($b + 23); $su++) {
            $worst_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $b; $su <= ($b + 23); $su++) {
            $worst_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
        }
        $data['worst_export_load_graph_data']    = $worst_export_load_graph_data;
        $data['worst_export_sol_prd_graph_data'] = $worst_export_sol_prd_graph_data;
        $data['worst_export_export_graph_data']  = $worst_export_export_graph_data;
        $data['worst_export_offset_graph_data']  = $worst_export_offset_graph_data;

        //High Energy Production Day
        $high_export_load_graph_data    = $high_export_sol_prd_graph_data = $high_export_export_graph_data  = $high_export_offset_graph_data  = [];
        $c                              = 24 * 276;
        for ($s = $c; $s <= ($c + 23); $s++) {
            $high_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
        }
        for ($su = $c; $su <= ($c + 23); $su++) {
            $high_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $c; $su <= ($c + 23); $su++) {
            $high_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $c; $su <= ($c + 23); $su++) {
            $high_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
        }
        $data['high_export_load_graph_data']    = $high_export_load_graph_data;
        $data['high_export_sol_prd_graph_data'] = $high_export_sol_prd_graph_data;
        $data['high_export_export_graph_data']  = $high_export_export_graph_data;
        $data['high_export_offset_graph_data']  = $high_export_offset_graph_data;

        //Best Energy Production Day
        $best_export_load_graph_data    = $best_export_sol_prd_graph_data = $best_export_export_graph_data  = $best_export_offset_graph_data  = [];
        $d                              = 24 * 53;
        for ($s = $d; $s <= ($d + 24); $s++) {
            $best_export_load_graph_data[] = abs(number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', ''));
        }
        for ($su = $d; $su <= ($d + 24); $su++) {
            $best_export_sol_prd_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $d; $su <= ($d + 24); $su++) {
            $best_export_export_graph_data[] = abs(number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', ''));
        }
        for ($su = $d; $su <= ($d + 24); $su++) {
            $best_export_offset_graph_data[] = abs(number_format((float) ($SolarConsumptionArr[$su]), 2, '.', ''));
        }
        $data['best_export_load_graph_data']    = $best_export_load_graph_data;
        $data['best_export_sol_prd_graph_data'] = $best_export_sol_prd_graph_data;
        $data['best_export_export_graph_data']  = $best_export_export_graph_data;
        $data['best_export_offset_graph_data']  = $best_export_offset_graph_data;


        //Summer Day
        $summer_export_load_graph_data    = $summer_export_sol_prd_graph_data = $summer_export_export_graph_data  = $summer_export_offset_graph_data  = [];
        for ($s = 8162; $s <= 8329; $s++) {
            $summer_export_load_graph_data[] = number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_sol_prd_graph_data[] = number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_export_graph_data[] = number_format((float) ($exportArr[$su]), 2, '.', '');
        }
        for ($su = 8162; $su <= 8329; $su++) {
            $summer_export_offset_graph_data[] = number_format((float) ($SolarConsumptionArr[$su]), 2, '.', '');
        }
        $data['summer_export_load_graph_data']    = json_encode($summer_export_load_graph_data);
        $data['summer_export_sol_prd_graph_data'] = json_encode($summer_export_sol_prd_graph_data);
        $data['summer_export_export_graph_data']  = json_encode($summer_export_export_graph_data);
        $data['summer_export_offset_graph_data']  = json_encode($summer_export_offset_graph_data);

        //Winter Day
        $winter_export_load_graph_data    = $winter_export_sol_prd_graph_data = $winter_export_export_graph_data  = $winter_export_offset_graph_data  = [];
        for ($s = 4370; $s <= 4537; $s++) {
            $winter_export_load_graph_data[] = number_format((float) ($ConsumptionProfileWithoutSolarSplitIntoWorking_nonworkingDaysArr[$s]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_sol_prd_graph_data[] = number_format((float) ($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$su]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_export_graph_data[] = number_format((float) ($exportArr[$su]), 2, '.', '');
        }
        for ($su = 4370; $su <= 4537; $su++) {
            $winter_export_offset_graph_data[] = number_format((float) ($SolarConsumptionArr[$su]), 2, '.', '');
        }
        $data['winter_export_load_graph_data']    = json_encode($winter_export_load_graph_data);
        $data['winter_export_sol_prd_graph_data'] = json_encode($winter_export_sol_prd_graph_data);
        $data['winter_export_export_graph_data']  = json_encode($winter_export_export_graph_data);
        $data['winter_export_offset_graph_data']  = json_encode($winter_export_offset_graph_data);

        if (!empty($getData)) {
            return $data;
        } else {
            echo json_encode($data);
            die;
        }
    }

    public function api_calculate_solar_proposal_cost_for_react()
    {
        $postData = $this->input->post();
        $getData  = $this->input->get();
        $interval = ['default' => null, 'five_system_size' => 5, 'six_system_size' => 6, 'ten_system_size' => 10];
        $response = [];
        try {
            foreach ($interval as $k => $total_system_size) {
                if ($total_system_size) {
                    $getData['total_system_size'] = $total_system_size;
                }
                $response[$k] = $this->api_calculate_solar_proposal_cost($postData, $getData);
            }
            //$postForUser['user_email'] = $getData['user_email'];
            $finalResponse             = ['get_data' => $response, 'post_data' => []];
            $encodedData               = json_encode($finalResponse);
            $response['success'] = true;
            //$this->generateSolarCalculatorPdf($finalResponse);
        } catch (Exception $e) {
            $response['success'] = false;
        }
        echo json_encode($response);
        die;
    }

    public function api_calculate_solar_savings()
    {
        $response = [];
        $response = $this->api_calculate_solar_proposal_cost();
    }

    public function generateSolarCalculatorPdf($postData)
    {
        //        $data;
        //        $postData     = json_decode(file_get_contents('php://input'), true);
        $data['data'] = $postData['get_data'];
        $post         = $postData['post_data'];

        $html        = $this->load->view('solar_calculator/generate_pdf', $data, True);
        $filename    = "Solar_Calculator_" . date('Y-m-d-h:i:sa');
        $filename    = str_replace(array('.', ',', ' '), '_', $filename);
        $h           = '';
        $f           = '';
        $pdf_options = array(
            "source_type"    => 'html',
            "source"         => $html,
            "action"         => 'save',
            "save_directory" => __DIR__ . '/../../assets/uploads/solar_saving_calculator_files',
            "file_name"      => $filename . '.pdf',
            "page_size"      => 'A4',
            "margin"         => array("right" => "10", "left" => '10', "top" => "10", "bottom" => "10"),
            "header"         => $h,
            "footer"         => $f
        );

        $this->pdf->phptopdf($pdf_options);
        $filePath = site_url('assets/uploads/solar_saving_calculator_files/' . $filename . '.pdf');

        /* Mailing Function Start */
        //$to        = $postData['post_data']['user_email'];
        //$dataArray = ["user_email" => $to, "file_path" => $filePath];
        //$dataArray = ["user_email" => 'asdfawe@yopmail.com', "file_path" => $filePath];
        //$this->email_manager->send_solar_calculated_pdf_email_to_user($dataArray);
    }

    public function read_meter_data_file($filename)
    {
        $filetype = '';
        $ext = explode('.', $filename);
        $file = FCPATH . '/assets/uploads/meter_data_files/' . $filename;

        switch (strtolower($ext[1])) {
            case 'xls':
                $filetype = 'Xls';
                break;
            case 'xlsx':
                $filetype = 'Xlsx';
                break;
            case 'csv':
                $filetype = 'Csv';
                break;
        }

        if ($filetype == '') {
            echo json_encode(array(
                'status' => 'Meter Data File Extension not supported.',
                'success' => FALSE
            ));
            die;
        }

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($filetype);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($file);
        $worksheet = $spreadsheet->getActiveSheet();

        $i = $j = 0;
        $date_time_column = $kWh_column =  '';
        $is_sheet_column_datetime = false;
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as  $cell) {
                if ($i ==  0) {
                    //Detect Datetime and Kwh columns
                    if ($date_time_column == '') {
                        if (preg_match('/datetime/', strtolower($cell->getValue()))) {
                            $date_time_column = $j;
                            //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell);
                        } else if (preg_match('/date\/time/', strtolower($cell->getValue()))) {
                            $date_time_column = $j;
                            //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell);
                        } else if (preg_match('/time/', strtolower($cell->getValue()))) {
                            $date_time_column = $j;
                            //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell);
                        }
                    }

                    if ($kWh_column == '') {
                        if (preg_match('/kwh/', strtolower($cell->getValue()))) {
                            $kWh_column = $j;
                        } else if (preg_match('/kw/', strtolower($cell->getValue()))) {
                            $kWh_column = $j;
                        } else if (preg_match('/e kwh at meter/', strtolower($cell->getValue()))) {
                            $kWh_column = $j;
                        }
                    }
                    $j++;
                }
                $cells[] = $cell->getValue();
            }


            //If Column not present return false;
            if (($date_time_column == '' || $kWh_column == '') && $i == 0 && $date_time_column != 0) {
                echo json_encode(array(
                    'status' => 'Either Date/Time or kWh column is not found on the uploaded file.',
                    'success' => FALSE
                ));
                die;
            }


            //Maintain 2 column table
            //Also check if date is not having dots
            if ($i > 0) {
                $dot_position = strpos($cells[$date_time_column], '.');
                $datetime = ($dot_position != FALSE && $dot_position != 5) ? str_replace('.', '-', $cells[$date_time_column]) : $cells[$date_time_column];
                if (is_numeric($datetime)) {
                    $is_sheet_column_datetime = TRUE;
                }

                $new_datetime = [];
                if ($is_sheet_column_datetime) {
                    $new_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datetime);
                }
                $new_datetime = !empty($new_datetime) ? $new_datetime->format("d-m-Y H:i:s") : $datetime;
                $rows[($i - 1)][0] = $new_datetime;
                $rows[($i - 1)][1] = $cells[$kWh_column];
            }
            $i++;
        }
        return $rows;
    }

    public function calculate_total_solar_production_daily($proposal_data)
    {
        $data                         = array();
        $total_solar_production_daily = 0;
        $kwh_day                      = (isset($proposal_data['kwh_day']) && $proposal_data['kwh_day'] != '') ? $proposal_data['kwh_day'] : 0;
        $system_size                  = (isset($proposal_data['total_system_size']) && $proposal_data['total_system_size'] != '') ? $proposal_data['total_system_size'] : 0;
        $panel_data                   = (isset($proposal_data['panel_data']) && $proposal_data['panel_data'] != '') ? (array) ($proposal_data['panel_data']) : [];
        //We Will have to decide here wether we want average or sum
        if (!empty($panel_data['tiles'])) {
            $panel_data['tiles']    = (array) $panel_data['tiles'];
            //echo "<pre>";
            //print_r($panel_data);die;
            $no_of_panels           = count($panel_data['tiles']);
            //echo $no_of_panels;die;
            $individual_system_size = ($system_size > 0 && $no_of_panels > 0) ? $system_size / $no_of_panels : 0;
            //echo $individual_system_size;die;
            for ($i = 0; $i < count($panel_data['tiles']); $i++) {
                $panel_data['tiles'][$i] = (array) $panel_data['tiles'][$i];
                $orientation             = is_numeric($panel_data['tiles'][$i]['sp_rotation']) ? $panel_data['tiles'][$i]['sp_rotation'] : 0;
                $orientation             = round($orientation / 10) * 10;
                $orientation             = ($orientation > 350) ? $orientation - 360 : $orientation;
                $tilt                    = is_numeric($panel_data['tiles'][$i]['sp_tilt']) ? $panel_data['tiles'][$i]['sp_tilt'] : 0;
                $watt                    = is_numeric($panel_data['tiles'][$i]['sp_watt']) ? $panel_data['tiles'][$i]['sp_watt'] : 0;
                $orientation_tilt        = $this->common->fetch_row('tbl_tilt_and_orientation_2020_data', '*', array('areaId' => $proposal_data['area_id'], 'orientationValue' => $orientation));
                if (!empty($orientation_tilt)) {
                    if ($tilt % 5 == 0) {
                        $orientation_tilt_val = isset($orientation_tilt['tilt' . $tilt]) ? $orientation_tilt['tilt' . $tilt] : 0;
                        $total_solar_production_daily += $individual_system_size * $orientation_tilt_val * $kwh_day;
                        //$total_solar_production_dailys[] = $orientation_tilt['tilt' . $tilt];
                    } else {
                        $x = 5; //Factor of 5 
                        $tilt1                 = (round($tilt) % $x === 0) ? round($tilt) : round(($tilt + $x / 2) / $x) * $x;
                        $tilt2                 = ($tilt1 > 0) ? $tilt1 - 5 : $tilt1;
                        $orientation_tilt_val1        = isset($orientation_tilt['tilt' . $tilt1]) ? $orientation_tilt['tilt' . $tilt1] : 0;
                        $orientation_tilt_val2        = isset($orientation_tilt['tilt' . $tilt2]) ? $orientation_tilt['tilt' . $tilt2] : 0;
                        $orientation_tilt_val         = ($orientation_tilt_val1 + $orientation_tilt_val2) / 2;
                        $total_solar_production_daily += $individual_system_size * $orientation_tilt_val * $kwh_day;
                        //$total_solar_production_dailys[] = $orientation_tilt_val;
                    }
                }
            }
        }
        //print_r($total_solar_production_dailys);die;
        return $total_solar_production_daily;
    }

    public function save_dataimg_to_png1()
    {
        $dataImg1 =  $this->input->post('dataImg1');
        $filename1 =  $this->input->post('filename1');
        $img = explode(';', $dataImg1);
        if ($dataImg1 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg1));
            $newfilename = $filename1 . '.png';
            file_put_contents('./assets/uploads/commercial_solar_pdf_file_images/' . $newfilename, $image);
        }

        $dataImg2 =  $this->input->post('dataImg2');
        $filename2 =  $this->input->post('filename2');
        $img = explode(';', $dataImg2);
        if ($dataImg2 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg2));
            $newfilename = $filename2 . '.png';
            file_put_contents('./assets/uploads/commercial_solar_pdf_file_images/' . $newfilename, $image);
        }
    }

    public function inverter_layout()
    {
        $data = array();
        $data['inverters'] = $this->common->fetch_where('tbl_products', '*', ['status' => 1, 'type_id' => 1, 'ted_crm_id IS NOT NULL' => NULL]);
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/inverter_layout');
        $this->load->view('partials/footer');
    }

    public function solar_proposal_pdf_new()
    {
        $data = array();
        $this->load->view('pdf_files/solar_proposal_pdf_new/web/main');
    }

    public function solar_proposal_pdf_new_download()
    {
        $data = array();
        $view = $this->input->get('view');
        if (isset($view) && $view == 'final_html') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-1', $data, TRUE);
            $html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-2', $data, TRUE);
            $html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-3', $data, TRUE);
            $html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-4', $data, TRUE);
            $html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-8', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page1') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/page-1', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page4') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/page-4', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page8') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/page-8', $data, TRUE);
            echo $html;
            die;
        } else {
            //Page 1
            $filename  = "page1.jpg";
            $upload_path_img = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename;
            $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1295 https://kugacrm.com.au/admin/proposal/solar_proposal_pdf_new_download?view=page1 ' . $upload_path_img;
            exec($cmd);

            //Page 4
            $filename  = "page4.jpg";
            $upload_path_img = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename;
            $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1295 https://kugacrm.com.au/admin/proposal/solar_proposal_pdf_new_download?view=page4 ' . $upload_path_img;
            exec($cmd);

            //Page 8
            $filename  = "page8.jpg";
            $upload_path_img = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename;
            $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1295 https://kugacrm.com.au/admin/proposal/solar_proposal_pdf_new_download?view=page8 ' . $upload_path_img;
            exec($cmd);

            $filename  = "SOLAR_PROPOSAL.pdf";
            $upload_path_img = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm  https://kugacrm.com.au/admin/proposal/solar_proposal_pdf_new_download?view=final_html ' . $upload_path_img;
            exec($cmd);
            echo site_url('assets/uploads/solar_proposal_pdf_new/' . $filename);
            die;
        }
    }
}

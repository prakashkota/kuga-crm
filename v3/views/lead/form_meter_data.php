<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<link href="https://fonts.googleapis.com/css?family=Mr+De+Haviland&display=swap" rel="stylesheet">

<style>
    @media print {
        .no-print {display: none;}
        body {background: transparent;}
        .black-bg{background-color:#010101;}
        .red{color:#c32027}
        .control-label:after {
            content:"*";
            color:red;
        }
    }
    .page-wrapper{ min-height:100%;}

    .sr-deal_actions{
        opacity: 1;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: 0px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }
    .is-invalid{border: 1px solid red;}
    @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 300px;
        }
    }

     .signature-pad {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        font-size: 10px;
       width: 100%;
       height:100%;
        max-height: 460px;
        border: 1px solid #e8e8e8;
        background-color: #fff;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
        border-radius: 4px;
        padding: 16px;
    }

    .signature-pad::before,
    .signature-pad::after {
        position: absolute;
        z-index: -1;
        content: "";
        width: 40%;
        height: 10px;
        bottom: 10px;
        background: transparent;
        box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
    }

    .signature-pad::before {
        left: 20px;
        -webkit-transform: skew(-3deg) rotate(-3deg);
        transform: skew(-3deg) rotate(-3deg);
    }

    .signature-pad::after {
        right: 20px;
        -webkit-transform: skew(3deg) rotate(3deg);
        transform: skew(3deg) rotate(3deg);
    }

    .signature-pad--body {
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #f4f4f4;
    }

    .signature-pad--body
    canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
    }

    .signature-pad--footer {
        color: #C3C3C3;
        text-align: center;
        font-size: 1.2em;
        margin-top: 8px;
    }

    .signature-pad--actions {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-top: 8px;
    }

    #github img {
        border: 0;
    }

    @media (max-width: 940px) {
        #github img {
            width: 90px;
            height: 90px;
        }
        .sr-deal_actions{
            opacity: 1;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            height: 67px;
            background-color: #f7f7f7;
            z-index: 100;
            box-shadow: 0 5px 40px rgba(0,0,0,.4);
            transition: opacity .1s ease-out,bottom .1s ease-out .5s;
            display: flex;
        }


    }

    /** IPAD */
    @media only screen 
    and (min-width: 1024px) 
    and (max-height: 1366px) 
    and (-webkit-min-device-pixel-ratio: 1.5) {
        .page-wrapper {
            padding: 30px 30px 30px 105px;
        }
    }

    .signature_image_close,.image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: -18px;
        right: -18px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .signature_image_container,.image_container{ 
        margin: 20px 20px 20px 20px;
        overflow: visible;
        box-shadow: 5px 5px 2px #888888;
        position: relative;
    }

    .is-invalid{border: 1px solid red;}
    #signatures{    
        height: 400px;
    }
    .sign_close,.sign_undo{display: none;}
    
    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
</style>

<div class="page-wrapper d-block clearfix " >
    <div style="padding:0; margin:0">
        <form id="meter_data_form">
         <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo site_url('assets/images/meter_data.jpg'); ?>" alt="" width="910" height="126" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                        <tbody>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                            <td >To whom it may concern,</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            <td >I have sought permission for the owner of the below electricity account to retrieve interval data and latest electricity bill issued to the customer.</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="315" valign="top" style="padding-right:10px">
                                                    <table width="310" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Pleae supply 30 minutes meter data (1 year) from</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="515" valign="top" style="padding-left:10px">
                                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                 <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input class="datepicker" id="from_date" name="meter_data[from_date]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                                 <td width="283" style="padding-left:5px; padding-right:5px;"> to </td>
                                                                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input class="datepicker" id="to_date" name="meter_data[to_date]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                             <tr>
                            <td>Please send your latest bill to 
                                <a href="#" style="text-decoration: underline;"><?php echo $user['email']; ?></a>
                            </td>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <td>
                                I/We undersigned hereby provide permission for Kuga Electrical (ABN:39 616 409 584) to obtain access to our electricity consumption data for the below premises.
                            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Electricity Account Name:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                                                    <input required="" id="account_name" name="meter_data[account_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>ABN/ACN:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="abn" type="number" min="1" name="meter_data[abn]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Address:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                                                    <input required="" id="address" name="meter_data[address]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>NMI:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="nmi" name="meter_data[nmi]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Energy Retailer:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="energy_retailer" name="meter_data[energy_retailer]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Signature:</strong></td>
                                            </tr>
                                           
                                             <tr>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                             <tr>
                                                <td align="center" style="">
                                            <div class="signature-pad">
                                                <div class="signature-pad--body">
                                                    <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_1" data-id="1" class="sign_create"></div>
                                                    <input required="" id="signature" name="meter_data[signature]" type="hidden" />
                                                </div>
                                            </div>
                                        <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                            <img src="" />
                                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                        </div>
                                    </td>
                                
                                                </tr>
                                            </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="13"></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Name:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="rep_name" name="meter_data[rep_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Date:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="request_date" class="datepicker" name="meter_data[request_date]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td height="35" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333333">Regards,
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:30px; padding-bottom:10px; padding-left:10px;">
                                    <p style="font-size:70px; font-family: 'Mr De Haviland', cursive;"><?php echo $user['full_name']; ?></p>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                    <?php echo $user['full_name']; ?> <br/>
                                    Commercial Sales Manager<br/>
                                    Kuga Electrical
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">4 Bridge Road, Keysborough VIC 3173</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">6 Turbo Road, Kings Park NSW 2148</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/images/bottom_right.jpg'; ?>" width="117" height="56" /></td>
      </tr>
    </table></td>
  </tr>
        </table>
        </form>
    </div>
    <div  class="sr-deal_actions">
        <div class="col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="generate_meter_data_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save and Generate Pdf</a>
        </div>
        <div class=" col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="close_meter_data" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-close"></i> Close</a>
        </div>
    </div>


<div id="signatures" >
        <div class="signature-pad hidden" id="signpad_create_1">
            <div class="signature-pad--body">
                <canvas width="664" height="200" style="touch-action: none; border: 1px solid black;" id="signature_pad_1"></canvas>
            </div>
            <div class="signature-pad--footer">
                <div class="signature-pad--actions">
                    <div>
                        <button type="button" class="btn button sign_clear" data-action="clear" data-id="1">Clear</button>
                        <button type="button" class="btn button sign_undo" data-action="undo" data-id="1">Undo</button>
                    </div>
                    <div>
                        <button type="button" class="btn button sign_close" data-id="1" id="signpad_close_1">Close</button>
                        <button type="button" class="btn button sign_save" data-action="save-png" data-id="1">Save</button>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/signpad.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/meter_data.js?v=<?php echo version; ?>"></script>
<script>
    var context = {};
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.meter_data = '<?php echo (isset($meter_data) && !empty($meter_data)) ? json_encode($meter_data, JSON_HEX_APOS) : ''; ?>';
    context.gst = '<?php echo GST; ?>';
    new meter_data_manager(context);
</script>

var stage_reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.filters = '';

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#lead_list_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById('start_date').value = start.format('YYYY-MM-DD');
        document.getElementById('end_date').value = end.format('YYYY-MM-DD');
        var data = $('#lead_list_filters').serialize() + '&view=pipeline';
        self.fetch_stage_list(data);
    }

    $('#lead_list_range').daterangepicker({
        numberOfMonths: 6,
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#lead_list_filters :input').change(function () {
        var data = $('#lead_list_filters').serialize() + '&view=pipeline';
        self.fetch_stage_list(data);
    });

    if (self.user_group != '1' && self.all_reports_view_permission == '0') {
        $('#reporting_container').hide();
        $('#error_container').removeClass('hidden');
        $('.page-wrapper').attr('style', 'height:100% !important;');
    } else {
        var data = 'view=pipeline';
        self.fetch_stage_list(data);
    }
}

stage_reporting.prototype.create_stage_list = function (data) {
    var self = this;
    var div_list = document.createElement('div');
    div_list.className = "list";
    var div_list_header = document.createElement('header');
    div_list_header.innerHTML = data.stage_name;
    var div_list_ul = document.createElement('div');
    div_list_ul.className = "connectedSortable stage_list";
    div_list_ul.setAttribute('id', 'stage_' + data.id);
    div_list_ul.setAttribute('data-id', data.id);
    div_list_ul.setAttribute('data-name', data.stage_name);
    //var div_list_li = document.createElement('li');
    //div_list_li.setAttribute('onclick', 'loadNoteDetails(' + JSON.stringify(data) + ');');
    //div_list_li.innerHTML = data.notes_point;
    var div_list_footer = document.createElement('footer');
    //var addCardAction = document.createElement('a');
    //addCardAction.setAttribute('onclick', 'addNewTabPoint("' + uid + '","' + notes_tab + '")');
    //addCardAction.setAttribute('href', 'javascript:void(0);');
    //addCardAction.innerHTML = "Add a point..";
    //div_list_footer.appendChild(addCardAction);
    div_list.appendChild(div_list_header);
    document.querySelector('#stages_data').appendChild(div_list_ul);
    //div_list.appendChild(div_list_footer);
    return div_list;
}

stage_reporting.prototype.create_stage_list_item = function (data) {
    var self = this;
    var stage_list_ele;
    var stage = (data.lead_stage == '' || data.lead_stage == null || data.lead_stage == 'null') ? '1' : data.lead_stage;
    if (stage !== '' || stage !== null || stage !== 'null') {
        stage_list_ele = document.querySelector('#stage_' + stage);
    } else {
        return false;
    }

    if (typeof (stage_list_ele) != 'undefined' && stage_list_ele != null) {
        //var color = (data.activity_last_created_days != null && data.activity_last_created_days >= 5) ? 'bg-danger text-white' : '';
        var color = (data.is_viewed == '0') ? 'bg-danger text-white' : '';
        color = (data.deal_won_lost != null && data.deal_won_lost != '') ? '' : color;
        var div_list_item = document.createElement('div');
        div_list_item.setAttribute('id', data.uuid);
        div_list_item.setAttribute('proposal_id', data.proposal_id);
        div_list_item.className = "stage_list__item ui-state-default " + color;
        //div_list_li.setAttribute('onclick', 'loadNoteDetails("edit","' + itemCounter + '");');
        var div_list_item_link = document.createElement('a');
        div_list_item_link.href = base_url + 'admin/proposal/add?deal_ref=' + data.uuid;
        div_list_item_link.setAttribute('style', 'text-decoration:none; color: inherit;');
        div_list_item_link.innerHTML = '<i class="fas fa-chevron-circle-right pull-right"></i>';

        var div_list_item_heading = document.createElement('h2');
        var fname = (data.first_name != null && data.first_name != '') ? data.first_name : '';
        var lname = (data.last_name != null && data.last_name != '') ? data.last_name : '';
        var custname = (data.company_name != null && data.company_name != '' && data.company_name != '0') ? data.company_name : fname + ' ' + lname;

        div_list_item_heading.innerHTML = custname;

        if (self.user_group == '1') {
            div_list_item_heading.appendChild(div_list_item_link);
        } else if (self.user_id == data.franchise_id) {
            div_list_item_heading.appendChild(div_list_item_link);
        }

        var div_list_item_body = document.createElement('span');
        div_list_item_body.innerHTML = '$' + data.quote_total_inGst + ' - ' + data.system_size + 'kW, ' + data.customer_state + '|' + data.customer_postcode;

        var input = document.createElement('input');
        //input.setAttribute('name', 'notes_tab[' + itemCounter + ']');
        //input.className = 'card_' +  uid + '_tab_name';
        input.setAttribute('style', 'display:none;');
        //input.value = notes_tab;
        div_list_item.appendChild(div_list_item_heading);
        div_list_item.appendChild(div_list_item_body);
        div_list_item.appendChild(input);

        stage_list_ele.appendChild(div_list_item);
    }
}

stage_reporting.prototype.fetch_stage_list = function (data) {
    var self = this;
    $.ajax({
        url: base_url + "admin/lead/fetch_lead_stage_data",
        type: 'get',
        dataType: "json",
        data: data,
        beforeSend: function () {
            document.getElementById('stages_data').innerHTML = '';
            document.getElementById('stages_data').appendChild(createLoader('Fetching lead information data....'));
        },
        success: function (data) {
            document.getElementById('stages').innerHTML = '';
            document.getElementById('stages_data').innerHTML = '';
            if (data.proposal_stages.length > 0) {
                for (var i = 0; i < data.proposal_stages.length; i++) {
                    var list = self.create_stage_list(data.proposal_stages[i]);
                    document.querySelector('#stages').appendChild(list);
                }
                for (var i = 0; i < data.proposal_data_by_stages.length; i++) {
                    if (data.proposal_data_by_stages[i].length > 0) {
                        for (var j = 0; j < data.proposal_data_by_stages[i].length; j++) {
                            self.create_stage_list_item(data.proposal_data_by_stages[i][j]);
                        }
                    }
                }

                /**Make Header Fixed on Scroll Vertical
                 $('.lists').scroll(function(){
                 var sticky = $('.list > header');
                 var scroll = $('.lists').scrollTop();
                 if (scroll >= 100){
                 sticky.addClass('fixed');
                 }else{ 
                 sticky.removeClass('fixed');
                 }
                 });
                 **/

                var currentlyScrolling = false;
                var SCROLL_AREA_WIDTH = 150; // Distance from container left and bottom right.
                //
                //Set max height for container to be able to scroll
                var screenHeight = $(window).height() - 110;
                $('.lists_data__hasScrollbar').attr('style', 'max-height:' + screenHeight + 'px; min-height:' + (screenHeight - 5) + 'px;');
                
       
                $("div.connectedSortable").sortable({
                    scroll: true,
                    connectWith: ".connectedSortable",
                    cancel: ".disable-sort-item",
                    tolerance: 'pointer',
                    placeholder: 'sortablePlaceholder',
                    forcePlaceholderSize: false,
                    forceHelperSize: false,
                    start: function (event, ui) {

                        $('.sr-deal_actions').addClass('sr-deal_actions--visible');
                    },
                    stop: function (event, ui) {
                        $('.sr-deal_actions').removeClass('sr-deal_actions--visible');
                    },
                    receive: function (event, ui) {
                        var receiver = ui.item.closest('div.stage_list');
                        if (receiver.attr('data-id') == undefined) {
                            self.update_proposal_won_lost(event, ui);
                        } else {
                            self.update_proposal_deal_stage(event, ui);
                        }
                    },
                    sort: function (event, ui) {

                        if (currentlyScrolling) {
                            return;
                        }

                        var continerWidth = $('.lists').width();

                        var mouseXPosition = event.clientX;

                        if ((mouseXPosition - SCROLL_AREA_WIDTH) < SCROLL_AREA_WIDTH) {
                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "-=" + continerWidth / 4 + "px" // Scroll up half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });

                        } else if (mouseXPosition > (continerWidth - SCROLL_AREA_WIDTH)) {

                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "+=" + continerWidth / 4 + "px" // Scroll down half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });
                        }
                    }
                }).disableSelection();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

stage_reporting.prototype.update_proposal_deal_stage = function (event, ui) {
    var self = this;

    var receiver_id = ui.item.closest('div.stage_list').attr('data-id');
    var current_item = $(ui.item);
    var current_item_id = $(ui.item).attr('id');

    var data = {};
    data.deal_stage = receiver_id;
    data.uuid = current_item_id;
    data.action = 'update_proposal';

    $.ajax({
        url: base_url + "admin/lead/update_lead_stage",
        type: 'post',
        dataType: "json",
        data: data,
        beforeSend: function () {
            current_item.removeClass('bg-danger');
            current_item.removeClass('text-white');
            current_item.addClass('disable-sort-item');
            current_item.addClass('bg-disabled');
        },
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                var new_stage_name = ui.item.closest('ul').attr('data-name');
                var prev_stage_name = ui.sender.attr('data-name');
                var date = moment(new Date()).format("DD-MM-YYYY");
                var activity_data = "activity_action=Stage Change&activity[activity_type]=Note&activity[note]=Changed from " + prev_stage_name + " to " + new_stage_name + " on " + date + '&uuid=' + current_item_id + '&action=save_activity';
                self.save_activity_handler(activity_data);
            } else {
                toastr["error"](data.status);
            }
            if (data.success == false) {
                $(ui.sender).sortable('cancel');
            }
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $(ui.sender).sortable('cancel');
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        }
    });
};

stage_reporting.prototype.update_proposal_won_lost = function (event, ui) {
    var self = this;

    var receiver_status = ui.item.closest('div.connectedSortable').attr('data-status');
    var current_item = $(ui.item);
    var current_item_id = $(ui.item).attr('proposal_id');

    if (current_item_id == null || current_item_id == 'null') {
        toastr["error"]('No Quote Link to selected lead.');
        $(ui.sender).sortable('cancel');
        return false;
    }

    var data = {};
    data.deal_won_lost = receiver_status;
    data.uuid = current_item_id;
    data.action = 'update_proposal';

    $.ajax({
        url: base_url + "admin/proposal/update",
        type: 'post',
        dataType: "json",
        data: data,
        beforeSend: function () {
            $(ui.sender).sortable('cancel');
            current_item.addClass('disable-sort-item');
            current_item.addClass('bg-disabled');
            current_item.removeClass('bg-danger');
            current_item.removeClass('text-white');
        },
        success: function (data) {
            if (data.success == true) {
                current_item.removeClass('bg-disabled');
                (receiver_status == 'won') ? current_item.addClass('bg-success') : current_item.addClass('bg-danger');
                setTimeout(function () {
                    current_item.addClass('hidden');
                }, 1000);
                toastr["success"](data.status);
                var date = moment(new Date()).format("DD-MM-YYYY");
                var activity_data = "activity_action=Added Activity&activity[activity_type]=Note&activity[note]=Job " + receiver_status + " on " + date + '&uuid=' + current_item_id + '&action=save_activity';
                self.save_activity_handler(activity_data);
            } else {
                current_item.removeClass('hidden');
                current_item.removeClass('disable-sort-item');
                current_item.removeClass('bg-disabled');
                toastr["error"](data.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $(ui.sender).sortable('cancel');
            current_item.removeClass('hidden');
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        }
    });
};

stage_reporting.prototype.save_activity_handler = function (data) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/save_proposal_activity',
        datatype: 'json',
        data: data,
        success: function (stat) {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            //toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};





<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_02.jpg'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <?php /**
                <tr>
                    <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:15px"><strong>Additional Notes</strong></td>
                </tr>
                <tr>
                    <td height="100" valign="top" style="color:#333333; font-family: Calibri, sans-serif; font-size:14px; border:solid 1px #e4e4e4; padding:5px">
                        <strong><?php echo $booking_form->additional_notes; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="35" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Other Kuga Electrical representative involved, if applicable. Place their name here.</td>
                </tr>
                <tr>
                    <td style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family: Calibri, sans-serif; font-size:14px; color:#333333">
                        <strong> <?php echo $booking_form->other_rep_name; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                */ ?>
                <tr>
                    <td height="35" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333"><strong>Call the office on 1300 005 842 to arrange temporary booking date and time.</strong></td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">If you can’t arrange a booking. put today’s date and the reason why you can’t make the booking in the “Scheduler who booked the job:” field</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="402" height="30" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Date and Time of Temporary Booking:</td>
                                <td width="23" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">&nbsp;</td>
                                <td width="405" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Scheduler who booked the job:</td>
                            </tr>
                            <tr>
                                <td style="border:solid 1px #e4e4e4; height:35px; font-family: Calibri, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->booked_at; ?></strong>
                                </td>
                                <td></td>
                                <td valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family: Calibri, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->booked_by; ?></strong>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <?php /*
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="163" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Pipedrive Deal ID.</td>
                                <td width="667" valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family: Calibri, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->pipedrive_id; ?></strong>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                */ ?>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">By signing this form you acknowledge that all information is tru and correct. You also accept responsibility for any and all costs incurred by providing inaccurate information.</td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family: Calibri, sans-serif; font-size:15px; color:#333333"><strong>KUGA ELECTRICAL SALES REPRESENTATIVE SIGNATURE</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                        <img width="800" height="200" src="<?php echo $this->config->item('live_url') . 'assets/uploads/led_booking_form_files/'. $booking_form->sales_rep_signature; ?>" />
                    </td>
                </tr>
                <tr>
                    <td height="35" align="center" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Plesae Sign Here</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="535">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />17</td>
                </tr>
            </table></td>
    </tr>
</table>
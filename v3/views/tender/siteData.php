
<div class="upload-sec box"> 
				<div class="row"> 
					<div class="col-12">
                        <?php if(count(@$systemOverviewDetails['optionId']) <= 3) {?>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" id='addMoreOption' data-siteId="<?php echo $siteId; ?>"> <i class="fa fa-plus"></i> Add Option </button>
                        </div>
                        <?php } ?>
						<div class="table-responsive upload-table">
							<form id="imageUploadForm" method="post" action="<?php echo site_url("admin/tender/uploadsiteimages/{$siteId}"); ?>" enctype="multipart/form-data">
							<table> 
								<tr>
									<th > Type </th>
                                    <?php if(count(@$systemOverviewDetails['optionId']) ==1){?>
                                        <th>  </th>
                                        <th> Actions </th>
                                        <th>  </th>
                                    <?php } else if(count(@$systemOverviewDetails['optionId']) ==2){?>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  Actions</th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                    <?php } else if(count(@$systemOverviewDetails['optionId']) ==3){?>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th> Actions </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                    <?php }
                                    else if(count(@$systemOverviewDetails['optionId']) ==4){?>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th> Actions </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                        <th>  </th>
                                    <?php }?>

								</tr>
								<?php
							
								foreach($siteTypes as $k=>$eachType) { ?>

<tr>
									<td> <?php echo $eachType; ?> </td>
                                    <?php $paddingRight= 300;
                                    $padDesc =300/count(@$siteDetails[$k]); ?>
									<?php foreach(@$siteDetails[$k] as $optionId=>$filename) {

                                        ?>
                                        <td class="optionEmpty<?php echo $optionId; ?>"></td>
                                        <td class="optionEmpty<?php echo $optionId; ?>"></td>
									<td id="<?php echo "{$k}-{$optionId}" ?>Td" style="position: relative;padding-left: 170px;">
                                        <a href="javascript:void(0)" style="width: 100px;"  <?php if(!empty($filename)) {?> class="addEffect" <?php } ?>>
                                        <img  onclick="javascript:openImg('<?php echo "{$k}-{$optionId}" ?>ImageContainer');" id="<?php echo "{$k}-{$optionId}" ?>ImageContainer" src="<?php echo site_url($siteUploadPath.$filename)?>"
												class="img-responsive" style="<?php if(empty($filename)) {?> display: none;<?php } ?>width: 100px;" />
									<div id="<?php echo "{$k}-{$optionId}" ?>-file-upload-div" <?php if(!empty($filename)) {?> style="position: absolute;top: 50%; color: #DB1E30;" <?php } ?>>
                                        &nbsp;<i onclick="javascript:openImg('<?php echo "{$k}-{$optionId}" ?>ImageContainer');" style="color: #DB1E30;<?php if(empty($filename)) {?> display: none;<?php } ?>" id="<?php echo "{$k}-{$optionId}" ?>-file-eye"  class="fa fa-eye" ></i>
                                        &nbsp;<label for="<?php echo "{$k}-{$optionId}" ?>-file-upload" class="custom-file-upload">
                                            <i <?php if(!empty($filename)) {?> style="color: #DB1E30;" <?php } ?> class="fa fa-upload" aria-hidden="true"></i><?php if(empty($filename)) {?> Upload <?php } ?></label>&nbsp;&nbsp;<i onclick="javascript:deleteSiteType('<?php echo $k ?>',<?php echo $optionId; ?>,<?php echo $siteId ?>);" style="color: #DB1E30;<?php if(empty($filename)) {?> display: none;<?php } ?>" id="<?php echo "{$k}-{$optionId}" ?>-file-trash"  class="fa fa-trash" ></i> &nbsp;
										<input class="uploadSiteImages" id="<?php echo "{$k}-{$optionId}" ?>-file-upload" type="file" name="<?php echo "{$k}_{$optionId}" ?>" />
									</div>
                                        </a>
									</td>
									<?php
                                        $paddingRight -= $padDesc;
                                    } ?>
								</tr>

								<?php
								}
								?>
								
								
							</table>
							</form>
						</div>
					</div>					
				</div>
			</div>
<form id="systemOverView" >
			<div class="sytem-table box"> 
				<div class="system-title">
					<h3> System Overview</h3>
				</div>
				<div class="table-responsive">

<?php $optionsCount = count(@$systemOverviewDetails['optionId']);  ?>
					<table class="tableoption">
						<tr>
							<th class="empty" width='25%'> </th>
							<?php
                            if(count(@$systemOverviewDetails['optionId'])>0){
								foreach(@$systemOverviewDetails['optionId'] as $eachOption){
								?>
							<th class="optionEmpty<?php echo $eachOption; ?>"></th>
							<th class="optionEmpty<?php echo $eachOption; ?>"></th>
							<th class="bg-grey optionData<?php echo $eachOption; ?>"> option<?php echo $eachOption; ?> </th>
							
							<?php } } ?>						
						</tr>
						<?php $overViewArray = array('systemSize'=>'System Size',
						'panels'=>'Panels',
						'inverter'=>'Inverter',
						'roofType'=>'Roof Type');
						$overViewPanelArray = array('flush'=>'Flush','tilt'=>'Tilt');
						
						foreach($overViewArray as $eachView=>$eachValue) {?>
						<tr>
							<td> <?php echo $eachValue; ?> </td>
							<?php if(count(@$systemOverviewDetails[$eachView])>0){
								foreach(@$systemOverviewDetails[$eachView] as $systemOverViewOption=>$systemOverViewValue) {
								?>
							<td class="optionCellEmpty<?php echo $systemOverViewOption; ?>"></td>
                                    <td class="optionCellEmpty<?php echo $systemOverViewOption; ?>"></td>
							<td class="optionCellData<?php echo $systemOverViewOption; ?>">
								<?php if($eachView != 'roofType'){?>
								<input type='text' class="trackChanges form-control" value="<?php echo @$systemOverViewValue; ?>" name="systemOverView<?php echo '['.$systemOverViewOption.']['.$eachView.']'; ?>" /> </td>
									<?php } else {?>
										<select id="roofTypeOne" name="systemOverView<?php echo '['.$systemOverViewOption.']['.$eachView.']'; ?>" class="trackChanges form-control">
									<?php
									
									foreach($roofOptions as $k=>$v){
										echo "<option ".(($k==@$systemOverViewValue)?'selected':'')." value='{$k}'>{$v}</option>";
									}
									?>
								</select> 
							<?php } } } ?>
							
						</tr>
						<?php } ?>
						

						<tr> 
							<td colspan="<?php echo ($optionsCount*3); ?>"  class="empty">
								<h4> Panel Layout</h4>
							</td>
						</tr>
						<?php
						foreach($overViewPanelArray as $eachView=>$eachValue) {?>
						<tr>
							<td> <?php echo $eachValue; ?> </td>
							<?php if(count(@$systemOverviewDetails[$eachView])>0){
								foreach(@$systemOverviewDetails[$eachView] as $systemOverViewOption=>$systemOverViewValue) {
								?>
							<td class="optionDataEmpty<?php echo $systemOverViewOption; ?>"></td>
							<td class="optionDataEmpty<?php echo $systemOverViewOption; ?>"></td>
							<td class="optionDataCell<?php echo $systemOverViewOption; ?>">
								<?php 
								if(!isset($total[$systemOverViewOption])){
									$total[$systemOverViewOption] = array();
								}
								array_push($total[$systemOverViewOption],$systemOverViewValue); ?>
								<input id="overViewPanel-<?php echo $systemOverViewOption.'-'.$eachView;?>" data-optionId="<?php echo $systemOverViewOption; ?>" type='number' class="trackChanges form-control" value="<?php echo @$systemOverViewValue; ?>" name="systemOverView<?php echo '['.$systemOverViewOption.']['.$eachView.']'; ?>" /> </td>
									 
							<?php  } } ?>
							
						</tr>
						<?php } ?>
						<tr> 
							<td> Total Panels </td> 
							<?php foreach($total as $eachOption=>$optionValues) { ?>
							<td class="optionDataEmpty<?php echo $eachOption; ?>"></td>
							<td class="optionDataEmpty<?php echo $eachOption; ?>"></td>
							<td class="optionDataCell<?php echo $eachOption; ?>"><span id="optionTotal_<?php echo $eachOption; ?>"><?php echo array_sum($optionValues); ?></span></td>
							<?php } ?>
						</tr>
						<tr > 
							<td colspan="<?php echo ($optionsCount*3); ?>" class="empty">  </td>
						</tr>
						<tr> 
							<td> Price exc GST (inc site specific costs)</td>
							<?php foreach($total as $eachOption=>$optionValues) { ?>
							<td class="optionDataEmpty<?php echo $eachOption; ?>"> </td>
							<td class="optionDataEmpty<?php echo $eachOption; ?>"> </td>
							<td class="optionDataCell<?php echo $eachOption; ?>"><input class="trackChanges form-control" type="text" name="systemOverView[<?php echo $eachOption; ?>][priceExcGst]"  value="<?php echo @$systemOverviewDetails['priceExcGst'][$eachOption]; ?>" /> <span class="dollar-btn">$</span> </td>
							<?php } ?>						
						</tr>						
					</table>

			
				</div>
			</div> 

			<div class="sytem-table box">
				<div class="system-title">
					<h3> Site specific system costs</h3>
                    <a hreg="javascript:void(0);" class="add-btn" data-siteId="<?php echo $siteId; ?>" id="addCostType"> <i class="fa fa-plus"></i> Cost Type </a>
				</div>
				<div class="table-responsive pb-3">

					<table class="tableoption">
						<tr>
							<th class="bg-grey" width='25%'>Type</th>
                            <?php if(count(@$systemcostTypes)>0){foreach (@$systemcostTypes as $typeId => $typeName) {
                            foreach ($systemcostDetails[$typeId] as $k => $v) { ?>
							<th class="optionEmpty<?php echo $k; ?> bg-grey">Unit Price (option<?php echo $k; ?>)</th>
							<th class="optionEmpty<?php echo $k; ?> bg-grey">Quantity (option<?php echo $k; ?>)</th>
							<th class="optionData<?php echo $k; ?> bg-grey">
							<i data-optId="<?php echo $k; ?>" id="arrow<?php echo $k; ?>" class="showHideColoums fa fa-backward" title="click to show columns"></i>Total Price (option<?php echo $k; ?>)</th>
                            <?php }
                            break;
                            } } ?>
						</tr>
						<?php $grandTotal = array();
                        foreach(@$systemcostTypes as $typeId=>$typeName) { ?>
						<tr>
							<td><?php echo @$typeName; ?></td>
							<?php foreach($systemcostDetails[$typeId] as $k=>$v) { ?>
							<td class="optionDataEmpty<?php echo $k; ?>"><input class="trackChanges form-control" id="trackChange-unitprice-<?php echo $typeId.$k; ?>" data-optionId="<?php echo $k; ?>" data-typeId="<?php echo $typeId; ?>" type="number" step="0.01" value="<?php echo @$v['unitprice']; ?>" name="systemCost[<?php echo $typeId; ?>][<?php echo $k; ?>][unitprice]" /></td>
							<td class="optionDataEmpty<?php echo $k; ?>"> <input class="trackChanges form-control" id="trackChange-quantity-<?php echo $typeId.$k; ?>" data-optionId="<?php echo $k; ?>" data-typeId="<?php echo $typeId; ?>" type="number" step="0.01" value="<?php echo @$v['quantity']; ?>" name="systemCost[<?php echo $typeId; ?>][<?php echo $k; ?>][quantity]" /> </td>
							<td class="optionDataCell<?php echo $k; ?>" ><?php
							$total= @$v['unitprice']*@$v['quantity']; 
							$grandTotal[$k][$typeId]=$total;
							
							echo "<span class='typeTotal{$k}' id='total-{$typeId}{$k}'>{$total}</span>";
							?></td>
							<?php } ?>	
							
						</tr>
						<?php } if(count($grandTotal) > 0 ) {  ?>

						<tr>
							<td>Total </td>	
							<?php foreach(@$grandTotal as $opt=>$typeTotal) { ?>
							<td class="optionDataEmpty<?php echo $opt; ?>">&nbsp;</td>
							<td class="optionDataEmpty<?php echo $opt; ?>">&nbsp;</td>
							<td class="optionDataCell<?php echo $opt; ?>">
							<strong><span id="totalCost<?php echo $opt; ?>"><?php echo array_sum($typeTotal); ?></span>	
							</td>
							<?php } } ?>
						</tr>

						

					</table>
				</div>
			</div>
							</form>
				<script src='<?php echo site_url(); ?>assets/js/custom2.js' type='text/javascript'></script>


			<script>
				$(document).ready(function(){
					$('.trackChanges').on('change',function(){
						$.ajax({
							url: '<?php echo site_url('admin/tender/save-site-information/').$siteId; ?>',
							type: 'POST',
							data:$('form#systemOverView').serialize(),
							dataType: 'json',
							success: function(data) {
								if(data.success) {
                                    toastr.success('Site information saved successfully.');
								}
							}
						});
					});
				
					$('input.uploadSiteImages').on('change',function(){
						$('#imageUploadForm').submit();
					});
					$('#imageUploadForm').on('submit',(function(e) {
						e.preventDefault();
						var formData = new FormData(this);
						$.ajax({
							type:'POST',
							url: $(this).attr('action'),
							data:formData,
							cache:false,
							contentType: false,
							processData: false,
							success:function(result){
								var data = jQuery.parseJSON(result);
								if(data.success){
                                    toastr.success('Image uploaded successfully');
									var idStartString = data.type+'-'+data.optionId;
									$('img#'+idStartString+'ImageContainer').attr('src','<?php echo site_url($siteUploadPath)?>'+data.filename).show();
									$('i#'+idStartString+'-file-trash').show();
									$('i#'+idStartString+'-file-eye').show();
									$('div#'+idStartString+'-file-upload-div label').html('<i style="color: #DB1E30" class="fa fa-upload" aria-hidden="true"></i>');
									$('div#'+idStartString+'-file-upload-div').css({'position': 'absolute','top': '50%', 'color': '#DB1E30;'});
									$('td#'+idStartString+'Td a').addClass('addEffect');
								}else{
                                    toastr.error(data.errors.replace(/<\/?[^>]+(>|$)/g, ""));
                                }
							},
							error: function(result){
								console.log("error");
								console.log(data);
							}
						});
						$('input.uploadSiteImages').val('');
					}));
                    $('[id^=trackChange]').on('keyup',function(){
                        var totalPrice = 0
                        var rows=3;
                        var typeTotalPrice = 0;
                        var typeId = $(this).attr('data-typeId');
                        var optionId = $(this).attr('data-optionId');
                        var unitPrice = $('input#trackChange-unitprice-'+typeId+''+optionId).val();
                        var quantity = $('input#trackChange-quantity-'+typeId+''+optionId).val();
                        $('span#total-'+typeId+''+optionId).html(unitPrice*quantity);
                        var grandTotal = 0;
                        $('span.typeTotal'+optionId).each(function(){
                            grandTotal += parseFloat($(this).html());
                        });
                        $('span#totalCost'+optionId).html(grandTotal);

                    });
                    $('i.showHideColoums').on('click',function(){
                        var optId = $(this).attr('data-optId');
                        var id="arrow"+optId;
                        if($('#'+id).hasClass('fa-backward')) {
                            $('i.showHideColoums').removeClass('fa-forward').addClass('fa-backward').attr('title', 'click to show columns');

                            if ($('#' + id).hasClass('fa-backward')) {
                                $('#' + id).removeClass('fa-backward').addClass('fa-forward');
                                $('#' + id).attr('title', 'click to hide columns');
                                $("th[class^='optionEmpty'], td[class^='optionDataEmpty'],td[class^='optionCellEmpty']").hide();
                                $("th.optionEmpty" + optId).show().css({'border':'2px solid red !important'});
                                $("td.optionDataEmpty" + optId).show().css({'border':'2px solid red !important'});
                                $("td.optionCellEmpty" + optId).show().css({'border':'2px solid red !important'});
                            }
                        } else {
                            $('#' + id).removeClass('fa-forward').addClass('fa-backward');
                            $('#' + id).attr('title', 'click to show columns');
                            $("th[class^='optionEmpty'], td[class^='optionDataEmpty'],td[class^='optionCellEmpty']").hide().css({'border':''});
                        }
                    });

                    function expandCollapse(optionId){
                        $("th[class^='optionEmpty'], td[class^='optionDataEmpty'],td[class^='optionCellEmpty']").hide();
                        $(".optionEmpty"+optionId+", td.optionDataEmpty"+optionId+",td.optionCellEmpty"+optionId).show();

                    }

					$('input[id^=overViewPanel]').on('keyup',function(){
						var optionId = $(this).attr('data-optionId');
						var overViewPanelTotal = 0;
						$('input[id^=overViewPanel-'+optionId+'-]').each(function(){
							overViewPanelTotal += parseFloat($(this).val());
						});
						$('span#optionTotal_'+optionId).html(overViewPanelTotal);

					});
				
				});

                function deleteSiteType(sType,optionId,siteId){
                    $.ajax({
                        url: '<?php echo site_url('admin/tender/deleteSiteType')?>',
                        type: 'POST',
                        data: {
                            sType: sType,
                            optionId: optionId,
                            siteId:siteId,
                        },
                        dataType: 'json',
                        success: function(result) {
                            if(result.success){
                                var idStartString = sType+'-'+optionId;
                                $('img#'+idStartString+'ImageContainer').attr('src','').hide();
                                $('i#'+idStartString+'-file-trash').hide();
                                $('i#'+idStartString+'-file-eye').hide();
                                $('div#'+idStartString+'-file-upload-div label').html('<i class="fa fa-upload" aria-hidden="true"></i>Upload');
                                $('div#'+idStartString+'-file-upload-div').css({'position': '','bottom': '0px', 'color': ''});
                                $('td#'+idStartString+'Td a').removeClass('addEffect');
                                toastr.success('Image deleted successfully');
                            }

                        }
                    });
                }
                function openImg(imageId){
                    $('#imgModal').modal('show');
                    $('#imgFullSize').attr('src',$('img#'+imageId).attr('src'));
                }



            </script>
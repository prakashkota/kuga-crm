
var lead_allocation_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data) ? JSON.parse(options.lead_data) : {};
    this.temp_lead_data = {};
    this.lead_id = '';
    this.postcode = '';
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.user_type = (options.user_type && options.user_type != '') ? options.user_type : '';
    //this.lead_to_userids = (options.lead_to_userids !== '') ? JSON.parse(options.lead_to_userids) : [];
    this.follow = false;
    
    
    if (Object.keys(self.lead_data).length > 0) {
        if(self.lead_data.user_id != null){
            $('#current_sales_rep_assigned').show();
            $('#current_sales_rep_assigned_name').html('<i class="fa fa-user mt-1 mr-2"></i> ' + self.lead_data.franchise);
        }
    }

    $('#category_id').on('change', function () {
        //self.fetch_sub_category_item_data();
    });

    $('#la_site_id').change(function () {
        var site = $(this).val();
        if (site != 'Please Select Site') {
            var data = $('#la_site_id option:selected').attr('data-item');
            data = JSON.parse(data);
            self.postcode = data.postcode;
            self.fetch_franchise_list(self.lead_data.assigned_userid);
            if (self.user_group == '1') {
                var data = {};
                data.postcode = self.postcode;
                data.userid = self.lead_data.assigned_userid;
            }
        }else{
            $('#businessId').html('<option> Please Select Site First </option>');
            $('#franchise_contact_name').html('');
            $('#franchise_name').html('');
            self.lead_to_userids = [];
        }
    });
    
    $('#la_cust_contact_id').change(function () {
        var customer = $(this).val();
        if (customer != 'Please Select Contact Name') {
            var customer_data = JSON.parse($('#la_cust_contact_id option:selected').attr('data-item'));
            $('#la_contact_email').val(customer_data.customer_email);
            $('#la_contact_no').val(customer_data.customer_contact_no);
        } else {
            $('#la_contact_email').val('');
            $('#la_contact_no').val('');
        }
    });

    /**$('#customer_name,#customer_lastname').change(function () {
        var data = {};
        data.firstname = document.getElementById('customer_name').value;
        data.lastname = document.getElementById('customer_lastname').value;
        self.validate_customer(data);
    });**/

    $('#customer_email').change(function () {
        if ($(this).val().length > 5) {
            var flag = self.validateEmail($(this).val());
            if (!flag) {
                $('#customer_email').addClass('is-invalid');
                $('#lead_actions').addClass('hidden');
                toastr["error"]('Invalid Email Entered');
            } else {
                $('#customer_email').removeClass('is-invalid');
                $('#lead_actions').removeClass('hidden');
            }
        }
    });

    $('#lead_btn_finish').click(function () {
        self.save_lead_allocation();
    });

    $('#lead_btn_follow').click(function () {
        self.follow = true;
        self.save_lead_allocation();
    });

    $('#lead_btn_reset').click(function () {
        self.lead_data = {};
        self.lead_id = '';
        self.postcode = '';
        self.lead_to_userids = [];
        self.follow = false;
        document.getElementById("lead_allocation_form").reset();
        document.getElementById("businessId").innerHTML = '<option>Enter Customer Postcode First</option>';
        document.getElementById("franchise_name").innerHTML = '';
        document.getElementById("franchise_contact_name").innerHTML = '';
        $('#lead_btn_follow').addClass('hidden');
        $('#lead_btn_finish').removeClass('hidden');
    });

    $('#confirm_customer_yes').click(function () {
        var data = self.temp_lead_data;
        $('#customer_name').val(data.first_name);
        self.lead_data = data;
        self.lead_to_userids = [data.assigned_userid];
        self.populate_lead_data();
        self.temp_lead_data = {};
        $('#confirm_customer_modal').modal('hide');
        $('#locationPostCode').trigger('change');
    });

    $('#confirm_customer_no').click(function () {
        self.temp_lead_data = {};
        $('#confirm_customer_modal').modal('hide');
    });

    $('#is_calendar_meeting').click(function(){
        $this = $(this);
        if ($(this).is(':checked')) {
            $('#calendar_meeting_option').addClass('hidden');
            $('#is_calendar_meeting').val(0);
        } else {
            $('#calendar_meeting_option').removeClass('hidden');
            $('#is_calendar_meeting').val(1);
            $('#is_calendar_meeting_value').val(1);
        }
        
    });

    tinymce.init({
        selector:'#la_note',
        branding: false,
        menubar: false,
        statusbar: false,
        plugins: "lists,link,fullscreen",
        toolbar: 'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
        mode: "textareas",
        mobile: {
            theme: 'silver'
        },
        entity_encoding : "raw"
    });

    self.manage_franchise_select();
};

lead_allocation_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_allocation_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

lead_allocation_manager.prototype.fetch_sub_category_item_data = function (category_id, sub_category_id) {
    var self = this;
    category_id = (category_id != undefined && category_id != '') ? category_id : $('#category_id').val();
    sub_category_id = (sub_category_id != undefined && sub_category_id != '') ? sub_category_id : '';
    var form_data = 'category_id=' + category_id;
    $.ajax({
        url: base_url + 'admin/service/category_item_data',
        type: 'get',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            var sub_category = document.getElementById('sub_category_id');
            sub_category.innerHTML = '';
            if (response.success == true) {
                for (var i = 0; i < response.category_item_data.length; i++) {
                    var option = document.createElement('option');
                    option.setAttribute('value', response.category_item_data[i].id);
                    option.innerHTML = response.category_item_data[i].category_name;
                    if (sub_category_id == response.category_item_data[i].id) {
                        option.setAttribute('selected', 'selected');
                    }
                    sub_category.appendChild(option)
                }
            } else {
                var option = document.createElement('option');
                option.setAttribute('value', '');
                option.innerHTML = 'No Sub Category Found';
                sub_category.appendChild(option)
            }
        }
    });
};

lead_allocation_manager.prototype.autopopulate_franchise_list = function (data, selected) {
    var self = this;
    var option;
    var franchise_select = document.getElementById('businessId');
    franchise_select.innerHTML = '';
    if (data.franchise_list.length > 0) {
        var franchise_list = data.franchise_list;
        for (var i = 0; i < franchise_list.length; i++) {
            option = document.createElement('option');
            option.setAttribute('value', data.franchise_list[i].user_id);
            option.innerHTML = data.franchise_list[i].full_name + ' (' + data.franchise_list[i].email + ')';
            if (selected == data.franchise_list[i].user_id) {
                option.setAttribute('selected', 'selected');
            }
            if ((selected == '' || selected == null) && data.selected_franchise.length > 0) {
                if (data.selected_franchise[0].user_id == data.franchise_list[i].user_id) {
                    option.setAttribute('selected', 'selected');
                }
            }
            option.setAttribute('data-item', JSON.stringify(data.franchise_list[i]));
            franchise_select.appendChild(option);
        }
    } else {
        var default_franchise = data.default_franchise;
        option = document.createElement('option');
        option.setAttribute('value', default_franchise.user_id);
        option.innerHTML = default_franchise.full_name + ' (' + default_franchise.email + ')';
        option.setAttribute('selected', 'selected');
        option.setAttribute('data-item', JSON.stringify(default_franchise));
        franchise_select.appendChild(option);
    }
    $('#businessId').trigger('change');
};

lead_allocation_manager.prototype.fetch_franchise_list = function (userid) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/assign_user_by_postcode',
        type: 'get',
        data: {uuid: self.lead_data.uuid, postcode: self.postcode,userid:userid},
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.autopopulate_franchise_list(response, userid);
            }
        }
    });
};

lead_allocation_manager.prototype.manage_franchise_select = function () {
    var self = this;
    $('#businessId').change(function () {
        var data = $(this).find('option:selected').attr('data-item');
        if (data != undefined) {
            data = JSON.parse(data);
            $('#franchise_contact_name').html(data.full_name);
            $('#franchise_name').html(data.company_name);
            self.lead_to_userids = [$(this).find('option:selected').val()];
        }
    });
}

lead_allocation_manager.prototype.validate_lead_data = function () {
    var self = this;
    var flag = true;
    var la_site_id = $('#la_site_id').val();
    var la_cust_contact_id = $('#la_cust_contact_id').val();
    var category_id = $('#category_id').val();
    //var sub_category_id = $('#sub_category_id').val();
    var businessId = $('#businessId').val();
    var is_calendar_meeting = $('#is_calendar_meeting').val();
    
    //Remove invalid
    $('#la_site_id').removeClass('is-invalid');
    $('#la_cust_contact_id').removeClass('is-invalid');
    $('#category_id').removeClass('is-invalid');
    $('#sub_category_id').removeClass('is-invalid');
    $('#businessId').removeClass('is-invalid');
    $('#la_scheduled_date_value').removeClass('is-invalid');
    $('#la_scheduled_time_value').removeClass('is-invalid');
    $('#la_scheduled_duration_value').removeClass('is-invalid');

    if (la_site_id == 'Please Select Site') {
        flag = false;
        $('#la_site_id').addClass('is-invalid');
    }

    if (la_cust_contact_id == 'Please Select Contact Name') {
        flag = false;
        $('#la_cust_contact_id').addClass('is-invalid');
    }
    if (category_id == '') {
        flag = false;
        $('#category_id').addClass('is-invalid');
    }
    /**if (sub_category_id == '') {
        flag = false;
        $('#sub_category_id').addClass('is-invalid');
    }*/
    if (businessId == '') {
        flag = false;
        $('#businessId').addClass('is-invalid');
    }

    if(is_calendar_meeting == '1'){
        var la_scheduled_date_value = $('#la_scheduled_date_value').val();
        var la_scheduled_time_value = $('#la_scheduled_time_value').val();
        var la_scheduled_duration_value = $('#la_scheduled_duration_value').val();


        if (la_scheduled_date_value == '') {
            $('#la_scheduled_date_value').addClass('is-invalid');
            flag = false;
        }
        if (la_scheduled_time_value == '') {
            $('#la_scheduled_time_value').addClass('is-invalid');
            flag = false;
        }
        if (la_scheduled_duration_value == '') {
            $('#la_scheduled_duration_value').addClass('is-invalid');
            flag = false;
        }

    }
    
    return flag;
};

lead_allocation_manager.prototype.save_lead_allocation = function () {
    var self = this;
    
    var flag = self.validate_lead_data();

    if (!flag) {
        toastr.clear();
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }

    var follow = (self.follow) ? true : false;
    var data = $('#lead_allocation_form').serialize();
    data += '&follow=' +  follow;
    
    $.ajax({
        url: base_url + 'admin/lead/save_lead_allocation',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('Assigning Lead to specified sales rep....');
            $("#lead_btn_finish").attr("disabled", "disabled");
            $("#lead_btn_follow").attr("disabled", "disabled");
            $("#lead_btn_reset").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                self.save_calendar_meeting();
            } else {
                toastr["error"](response.status);
                $("#lead_btn_finish").removeAttr("disabled");
                $("#lead_btn_follow").removeAttr("disabled");
                $("#lead_btn_reset").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#lead_btn_finish").removeAttr("disabled");
            $("#lead_btn_follow").removeAttr("disabled");
            $("#lead_btn_reset").removeAttr("disabled");
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_allocation_manager.prototype.save_calendar_meeting = function () {
    var self = this;
    var lead_id = $('#lead_allocation_lead_id').val();
    var businessId = $('#businessId').val();

    var is_calendar_meeting = $('#is_calendar_meeting').val();

    var la_scheduled_date_value  = la_scheduled_time_value = la_scheduled_duration_value = '';
    var activity_action = 'Add Activity';
    var activity_type = 'Note';
    if(is_calendar_meeting == '1'){
         la_scheduled_date_value = $('#la_scheduled_date_value').val();
         la_scheduled_time_value = $('#la_scheduled_time_value').val();
         la_scheduled_duration_value = $('#la_scheduled_duration_value').val();
         activity_action = 'Scheduled Activity';
         activity_type = 'Schedule Meeting';
    }
    var activity_note = '';

    var sales_rep_name = $('#franchise_contact_name').html();
    var category_name = $('#category_id option:selected').text();
    var daily_average_usage = $('#la_daily_average_usage').val();
    var highbay = $('#la_highbay').val();
    var panel_light = $('#la_panel_light').val();
    var flood_light = $('#la_flood_light').val();
    var batten_light = $('#la_batten_light').val();
    
    var editor_data = tinyMCE.get('la_note').getContent().trim();
    var note = encodeURIComponent(editor_data);

    var customer_name = (self.lead_data.first_name != '' && self.lead_data.first_name != null) ? self.lead_data.first_name + ' ' + self.lead_data.last_name : self.lead_data.customer_company_name;
    var customer_no = self.lead_data.customer_contact_no;

    activity_note = "Sales Manager allocated to: " +sales_rep_name+ "<br/>";

    activity_note += "Customer Contact Name: " +customer_name+ "<br/>";

    activity_note += "Customer Contact No.: <a href='tel:"+customer_no+"'>" +customer_no+ "</a><br/><br/>";

    activity_note += "Service category: " + category_name + "<br/>";
            
    if(daily_average_usage != ''){
        activity_note += "Daily Average Usage: " + daily_average_usage + "<br/>";
    }
    if(highbay != ''){
        activity_note += "Highbay: " + highbay + "<br/>";
    }
    if(panel_light != ''){
        activity_note += "Panel Light: " + panel_light + "<br/>";
    }
    if(flood_light != ''){
        activity_note += "Flood Light: " + flood_light + "<br/>";
    }
    if(batten_light != ''){
        activity_note += "Battens: " + batten_light + "<br/>";
    }

    if(note != ''){
        activity_note += "<br/>Note: " +note;
    }

    var data = '';
    data += "activity[activity_title]=NEW CALL CENTRE LEAD";
    data += '&lead_id=' + lead_id;
    data += '&userid=' + businessId;
    data += '&activity_action='+activity_action+'&activity[id]=&activity[activity_type]='+activity_type+'&activity[activity_note]='+activity_note+'&activity[scheduled_date]='+la_scheduled_date_value+'&activity[scheduled_time]='+la_scheduled_time_value+'&activity[scheduled_duration]='+la_scheduled_duration_value;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/activity/save',
        datatype: 'json',
        data: data,
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success == true) {
                toastr["success"]('Call Centre Activity Created Successfully.');
                setTimeout(function () {
                    window.location.reload();
                }, 2000); 
            } else {
                toastr["error"]('Looks like something went wrong in creating activity.');
                setTimeout(function () {
                    window.location.reload();
                }, 2000); 
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
             setTimeout(function () {
                window.location.reload();
            }, 2000); 
        }
    });
};

lead_allocation_manager.prototype.validate_customer = function (data) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/check_unique_customer',
        type: 'post',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.success == false) {
                if ((self.userid != response.customer_data.user_id) && self.user_group != '1' && self.user_group != '7') {
                    $('#lead_actions').addClass('hidden');
                    toastr["error"](response.status);
                    self.lead_data = {};
                    self.lead_id = '';
                    self.postcode = '';
                    self.lead_to_userids = [];
                    self.follow = false;
                    document.getElementById("lead_form").reset();
                    document.getElementById("select2-customer_address-container").innerHTML = 'Search for address';
                    document.getElementById("businessId").innerHTML = '<option>Enter Customer Postcode First</option>';
                    document.getElementById("franchise_name").innerHTML = '';
                    document.getElementById("franchise_contact_name").innerHTML = '';
                    $('#lead_btn_follow').addClass('hidden');
                    $('#lead_btn_finish').removeClass('hidden');
                } else {
                    var data = response.customer_data;
                    self.temp_lead_data = data;
                    var html = 'Customer already exists:';
                    html += '<br/><br/><b>Name:</b> ' + data.first_name + ' ' + data.last_name;
                    html += '<br/><br/><b>Address:</b> ' + data.address;
                    html += '<br/><br/><b>Phone:</b> ' + data.customer_contact_no;
                    html += '<br/><br/><b>Email:</b> ' + data.customer_email;
                    if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                        html += '<br/><br/><b>Franchise:</b> '+ data.full_name; 
                    }
                    if(data.company_name != '' && data.company_name != 'null' && data.company_name != null){
                        html += '<br/><br/><b>Franchise Name:</b> '+ data.company_name;
                    }
                    if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                        html += '<br/><br/><b>Franchise Contact Number:</b> '+ data.company_contact_no; 
                    }

                    if (data.full_name != '' && data.full_name != 'null' && data.full_name != null) {
                        html += '<br/><br/>Note: Customer has been allocated to Franchisee ' + data.full_name + ' (' + data.franchise_address + ')';
                    }
                    $('#confirm_customer_modal_body').html('');
                    $('#confirm_customer_modal_body').html(html);
                    $('#confirm_customer_modal').modal('show');
                    return false;
                }
            } else {
                $('#lead_actions').removeClass('hidden');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_actions').removeClass('hidden');
        }
    });
};

lead_allocation_manager.prototype.fetch_lead_count_by_franchise = function (data) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/lead/fetch_lead_count_by_franchise',
        type: 'get',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#franchise_lead_details_table_body').html(createLoader('Please wait while data is being loaded'));
        },
        success: function (response) {
            $('#franchise_lead_details_table_body').html('');
            $('#franchise_lead_details_table').DataTable().clear();
            $('#franchise_lead_details_table').DataTable().destroy();
            if (response.lead_data.length > 0) {
                var lead_data = response.lead_data;
                for (var i = 0; i < lead_data.length; i++) {
                    var tr = document.createElement('tr');
                    var td1 = document.createElement('td');
                    var td2 = document.createElement('td');
                    var td3 = document.createElement('td');
                    td1.innerHTML = lead_data[i].company_full_name;
                    td2.innerHTML = lead_data[i].lead_count;
                    td3.innerHTML = lead_data[i].last_allocated_at;
                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    tr.appendChild(td3);
                    document.getElementById('franchise_lead_details_table_body').appendChild(tr);
                }
                $('#franchise_lead_details_table').DataTable({
                    "order": [[1, "desc"]],
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": true,
                });
            }
        }
    });
};


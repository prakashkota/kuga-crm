
<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01-wide.jpg'; ?>" alt="" width="100%" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
     <tr>
        <td><a href="javascript:void(0);" id="add_more_led_products_row_btn_nsw" class="btn btn-kuga mb-2 pull-right"><i class="fa fa-plus"></i> Add More</a></td>
    </tr>
     <tr>
      <td>
         <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="booking_items">
            <tr>
               <th colspan="5" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">&nbsp;</th>
               <th colspan="4" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Existing Lighting</th>
               <th colspan="3" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Upgraded Lighting</th>
               <th colspan="1" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">&nbsp;</th>
               <th colspan="1" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">&nbsp;</th>
               <th colspan="1" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">&nbsp;</th>
               <th colspan="1" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">&nbsp;</th>
            </tr>
            <tr>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Area Name</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Celling Height</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Space Type</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">BCA Classification</th>
                <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Air-con?</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Qty</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Lamp and ballast combination</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Nominal Lamp Power</th>
              <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Control Device</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Product Types</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Product Modal</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Control Device</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Sensor</th>
               <th style="width:150px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Qty</th>
              
               <th style="width:40px; background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center;">Unit Cost exc GST</th>
               <th style="width:40px; background:#000; border:1px solid #e8e8e8e8e8e8; color:#fff; text-align:center;">Total Cost exc GST</th>
            </tr>
            <tbody id="booking_items_body_nsw">
                
            </tbody>
         </table>
      </td>
   </tr>
  
    <tr>
        <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
               
                    <td height="40" colspan="4" style="padding:5px;">
                        <table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="55%" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Further Products quoted can be found on a separate sheet </td>
                                <td width="20%"><input id="" name="booking_form[is_more_products]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                <td width="25%" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Sub Total exc GST:</td>
                            </tr>
                        </table></td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input id="product_total_excGst" name="product[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
               
                <tr>
                    <th width="154" height="40" class="black-bg" colspan="2" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Access Equipment</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Detailed Description of Access Equipment</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Quantity</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php for ($i = 0; $i < 2; $i++) {
                    $required = ($i == 0) ? '' : '';
                    ?>
                    <tr>
                        <td colspan="2" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="ae_name ae_name_<?php echo $i; ?>" name="ae[ae_name][]" style="padding:5px; width:100%; height: inherit;" type="text" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <select class="ae_description ae_description_<?php echo $i; ?>" name="ae[ae_description][]"  style="padding:5px; width:100%; height: inherit;" <?php echo $required; ?>>
                                <option value="">Please Select</option>
                                <?php
                                $ae = unserialize(BF_AE);
                                if (!empty($ae)) {
                                    foreach ($ae as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_qty ae_qty_<?php echo $i; ?>" name="ae[ae_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_cost_excGST ae_cost_excGST_<?php echo $i; ?>" name="ae[ae_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="ae_total_cost_excGst ae_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="1" type="number" <?php echo $required; ?> />
                        </td>
                    </tr>   
                <?php } ?>
                <tr style="display: none;">
                    <td height="40" colspan="4" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Sub Total exc GST:</td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input id="ae_total_excGst" name="ae[total_excGst]"  style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_excGst" name="booking_form[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" >
                        <table width="557" border="0" cellspacing="0" cellpadding="0" style="display: none;">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs PANEL</strong> booking form?</td>
                                <td width="212">
                                    <input id="is_heers_panel" name="booking_form[is_heers_panel]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_Gst" name="booking_form[total_Gst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                        <table width="557" border="0" cellspacing="0" cellpadding="0" style="display: none;">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs BATTEN</strong> booking from?</td>
                                <td width="212" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <input id="is_heers_batten" name="booking_form[is_heers_batten]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" />
                                </td>
                            </tr>
                        </table></td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span><i class="fa fa-trash"></i> Delete</span>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this item?
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-ok">Delete</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade confirmation-modal" id="confirm-request" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span><i class="fa fa-user"></i> Confirm Settings Change</span>
            </div>
            <div class="modal-body">
                Are you sure you want to allow the request change for franchise?
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-ok">Confirm</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade confirmation-modal" id="confirm-unban" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span><i class="fa fa-user"></i> Confirm User Unban</span>
            </div>
            <div class="modal-body">
                Are you sure you want to unban this user?
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-ok">Confirm</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade confirmation-modal" id="confirm-ban" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span><i class="fa fa-user"></i> Confirm User Ban</span>
            </div>
            <div class="modal-body">
                Are you sure you want to ban this user?
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-ok">Confirm</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="electricity_bill_upload_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span> Upload Electricity Bill to Connected Job on Job CRM</span>
            </div>
            <div class="modal-body" id="electricity_bill_upload_modal_body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="overlay-roadblock">
    <div class="overlay-roadblock__content">
        <i class="fa fa-warning"></i> Please allow browser location to procced furthur. <a href="https://kugacrm.com.au/assets/images/popup-block-help-1.png" target="__blank">(Learn More)</a>
    </div>
</div>

<script src="<?php echo site_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/sidebar-menu.js"></script>

<script>
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('<?php echo site_url(); ?>service-worker.js').then(function(registration) {
                console.log('Registered!');
            }, function(err) {
              console.log('ServiceWorker registration failed: ', err);
            }).catch(function(err) {
              console.log(err);
            });

            navigator.serviceWorker.ready.then(function(registration) {
              if (!registration.pushManager) {
                alert('No push notifications support.');
                return false;
              }
              registration.pushManager.subscribe({
                userVisibleOnly: true 
              }).then(function (subscription) {
              console.log('Subscribed.');
              }).catch(function (error) {
              console.log('Subscription error: ', error);
              });
            });
        });
      } else {
        console.log('service worker is not supported');
    }

    
    var deferredPrompt;
    
    window.addEventListener('beforeinstallprompt', function (e) {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
    
      //showAddToHomeScreen();
    
    });
    
    

</script>

</body>
</html>
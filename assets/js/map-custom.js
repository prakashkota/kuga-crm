var fadeOut = 650;
const debug = !1;
$(document).ready(function() {
    recalc_height()
});
$(window).resize(function() {
    recalc_height()
});

function recalc_height() {
    let header_top = parseInt($('.header-top').outerHeight());
    let hf_height = parseInt($('.header-calc').outerHeight()) + header_top;
    if (debug) console.log($('.header-calc').outerHeight() + " : " + $('.footer-calc').outerHeight());
    $('.footer-calc').css({
        'max-height': header_top + 'px'
    });
    $('.mask').css({
        'height': 'calc(100% - ' + hf_height + 'px)'
    });
    $('#map').css({
        'height': 'calc(100% - ' + hf_height + 'px)'
    })
}

function measure(lat1, long1, lat2, long2) {
    lat1 = deg2rad(lat1);
    lat2 = deg2rad(lat2);
    long1 = deg2rad(long1);
    long2 = deg2rad(long2);
    dLon = long2 - long1;
    y = Math.sin(dLon) * Math.cos(lat2);
    x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
    brng = Math.atan2(y, x);
    brng = brng * 90 / 3.1415926535898;
    brng = fmod(brng + 90, 90);
    return brng
}

function deg2rad(angle) {
    return angle * 0.017453292519943295
}

function fmod(a, b) {
    return a % b
}

function resetAll() {
    location.reload()
}

function removeMask() {
    $("#mask").animate({
        opacity: 0
    }, fadeOut, function() {
        $(this).css("display", "none")
    });
    $('#reset-all').css("display", "block");
    $('#mapping_tool_actions').show();
    $('.cardDiv').css("display", "block");
    $('.map-leftpanel').css("display", "block");
    $('.header-calc').addClass('map_adjust')
}

function togglePacCard(action) {}
$('#toggle-pac-card').click(function() {
    togglePacCard("none")
});

function createDetailTable() {
    var table = document.createElement('table');
    table.className = "table table-bordered text-center";
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');
    var s_no_col = document.createElement('td');
    s_no_col.innerHTML = '<b>Section</b>';
    var no_of_panels_col = document.createElement('td');
    no_of_panels_col.innerHTML = '<b>No. of panels</b>';
    var angle = document.createElement('td');
    angle.innerHTML = '<b>Angle</b>';
    tr.appendChild(s_no_col);
    tr.appendChild(no_of_panels_col);
    tr.appendChild(angle);
    thead.appendChild(tr);
    table.appendChild(thead);
    return table
}

function createDetailTableRow(data) {
    var row = document.createElement('tr');
    var s_no_col = document.createElement('td');
    s_no_col.innerHTML = data.id;
    var no_of_panels_col = document.createElement('td');
    no_of_panels_col.innerHTML = data.no_of_panels;
    var angle = document.createElement('td');
    angle.innerHTML = data.angle;
    row.appendChild(s_no_col);
    row.appendChild(no_of_panels_col);
    row.appendChild(angle);
    return row
}

function createDetailTableTotalPanelsRow(total_panels) {
    var row = document.createElement('tr');
    var total_panels_col = document.createElement('td');
    total_panels_col.innerHTML = '<b>Total Panels</b>';
    var total_panels_count_col = document.createElement('td');
    total_panels_count_col.innerHTML = total_panels;
    row.appendChild(total_panels_col);
    row.appendChild(total_panels_count_col);
    return row
}

function calculateNow() {
    var totalPanels = 0;
    var totalDirectionPanels = {
        N: 0,
        S: 0,
        E: 0,
        W: 0
    };
    var sectionBreakdown = "";
    var table = createDetailTable();
    var tbody = document.createElement('tbody');
    $('#accordion .sub-panel-num').each(function() {
        let $id = $(this).attr('id').split('_')[2];
        let $dir = $('[name=roof_facing_' + $id + ']:checked').val();
        if (typeof $dir == "undefined") $dir = 'N';
        totalDirectionPanels[$dir] += parseInt($(this).text());
        totalPanels += parseInt($(this).text());
        var angle;
        angle = (Math.round(anglesArray[$id])) + '&deg;';
        var data = {};
        data.id = $id;
        data.no_of_panels = parseInt($(this).text());
        data.angle = angle;
        var row = createDetailTableRow(data);
        tbody.appendChild(row)
    });
    var total_panels_row = createDetailTableTotalPanelsRow(totalPanels);
    tbody.appendChild(total_panels_row);
    table.appendChild(tbody);
    document.getElementById('cntPanel').innerHTML = '';
    document.getElementById('cntPanel').appendChild(table);
    $('#accordion .sub-panel-type').each(function() {
        sectionBreakdown += parseInt($(this).text())
    })
    $('#accordion .sub-panel-facing').each(function() {
        sectionBreakdown += parseInt($(this).text())
    })
    $('#input_5_5').val(totalDirectionPanels.N);
    $('#input_5_10').val(totalDirectionPanels.S);
    $('#input_5_9').val(totalDirectionPanels.E);
    $('#input_5_8').val(totalDirectionPanels.W);
    $('#input_5_6').val($('#query-address').val());
    $('#input_5_7').val(sectionBreakdown)
}
var marker;
var map;
var melbourne = {
    lat: -37.81,
    lng: 144.96
};
var qld = {
    lat: -20.91,
    lng: 142.70
};
var section_id = 0;
var sectionSolarPanels = [];
var drawingManager;
var drawingRemoveManager;
var polyShapes = [];
var spherical;
var next_row_start_point;
var solarPanels = [];
var roofPanels = [];
var solarPanelFillColor = '#000000';
var solarPanelFillOpacity = 0.7;
var solarPanelStrokeWeight = 0.5;
var solarPanelStrokeColor = '#000000';
var solarPanelRowNum = 150;
var solarPanelColNum = 150;
var solarPanel_roof_edge_gap = 0.30;
var polygonArray = [];
var polygonArray1 = [];
var anglesArray = [];
var all_panels_instance_array = [];
var all_panels_removed_instance_array = [];
var panel_mode = 'landscape';

function setPanelMode(val) {
    panel_mode = val;
    $('#panel_mode_modal').modal('hide');
    return !0
}

function initMap() {
    if (window.near_map_api_key != undefined && window.near_map_api_key != '') {
        var apikey = window.near_map_api_key;

        function degreesToRadians(deg) {
            return deg * (Math.PI / 180)
        }

        function radiansToDegrees(rad) {
            return rad / (Math.PI / 180)
        }

        function regionForCoordinate(x, y, zoom) {
            var x_z1 = x / Math.pow(2, (zoom - 1));
            if (x_z1 < 1) {
                return 'us'
            } else {
                return 'au'
            }
        }

        function rotateTile(coord, zoom, heading) {
            var numTiles = 1 << zoom;
            var x, y;
            switch (heading) {
                case 0:
                    x = coord.x;
                    y = coord.y;
                    break;
                case 90:
                    x = numTiles - (coord.y + 1);
                    y = coord.x;
                    break;
                case 180:
                    x = numTiles - (coord.x + 1);
                    y = numTiles - (coord.y + 1);
                    break;
                case 270:
                    x = coord.y;
                    y = numTiles - (coord.x + 1);
                    break
            }
            return new google.maps.Point(x, y)
        }

        function MercatorProjection(worldWidth, worldHeight) {
            this.pixelOrigin = new google.maps.Point(worldWidth / 2, worldHeight / 2);
            this.pixelsPerLonDegree = worldWidth / 360;
            this.pixelsPerLatRadian = worldHeight / (2 * Math.PI)
        }
        MercatorProjection.prototype.fromLatLngToPoint = function(latlng, opt_point) {
            var point = opt_point || new google.maps.Point(0, 0);
            var origin = this.pixelOrigin;
            var lat = latlng.lat();
            var lng = latlng.lng();
            point.x = origin.x + lng * this.pixelsPerLonDegree;
            var siny = Math.sin(degreesToRadians(lat));
            point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -this.pixelsPerLatRadian;
            return point
        };
        MercatorProjection.prototype.fromPointToLatLng = function(point, noWrap) {
            var origin = this.pixelOrigin;
            var lng = (point.x - origin.x) / this.pixelsPerLonDegree;
            var latRadians = (point.y - origin.y) / -this.pixelsPerLatRadian;
            var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
            return new google.maps.LatLng(lat, lng)
        };
        const projVertical = new MercatorProjection(256, 256);

        function vertToWest(ll) {
            var pt = projVertical.fromLatLngToPoint(ll);
            pt = new google.maps.Point(pt.y, pt.x);
            ll = projVertical.fromPointToLatLng(pt);
            return new google.maps.LatLng(ll.lat(), -ll.lng())
        }

        function westToVert(ll) {
            ll = new google.maps.LatLng(ll.lat(), -ll.lng());
            var pt = projVertical.fromLatLngToPoint(ll);
            pt = new google.maps.Point(pt.y, pt.x);
            return projVertical.fromPointToLatLng(pt)
        }

        function vertToEast(ll) {
            var pt = projVertical.fromLatLngToPoint(ll);
            pt = new google.maps.Point(pt.y, pt.x);
            ll = projVertical.fromPointToLatLng(pt);
            return new google.maps.LatLng(-ll.lat(), ll.lng())
        }

        function eastToVert(ll) {
            ll = new google.maps.LatLng(-ll.lat(), ll.lng());
            var pt = projVertical.fromLatLngToPoint(ll);
            pt = new google.maps.Point(pt.y, pt.x);
            return projVertical.fromPointToLatLng(pt)
        }

        function fakeLatLng(mapTypeId, ll) {
            if (mapTypeId === 'W') {
                ll = vertToWest(ll)
            } else if (mapTypeId === 'E') {
                ll = vertToEast(ll)
            } else if (mapTypeId === 'S') {
                ll = new google.maps.LatLng(-ll.lat(), -ll.lng())
            }
            return ll
        }

        function realLatLng(mapTypeId, ll) {
            if (mapTypeId === 'W') {
                ll = westToVert(ll)
            } else if (mapTypeId === 'E') {
                ll = eastToVert(ll)
            } else if (mapTypeId === 'S') {
                ll = new google.maps.LatLng(-ll.lat(), -ll.lng())
            }
            return ll
        }

        function registerProjectionWorkaround(map) {
            var currMapType = map.getMapTypeId();
            map.addListener('maptypeid_changed', function() {
                var center = realLatLng(currMapType, map.getCenter());
                currMapType = map.getMapTypeId();
                map.setCenter(fakeLatLng(currMapType, center))
            })
        }

        function createMapType(tileWidth, tileHeight, heading, name) {
            var maptype = new google.maps.ImageMapType({
                name: name,
                tileSize: new google.maps.Size(tileWidth, tileHeight),
                isPng: !0,
                minZoom: 1,
                maxZoom: 24,
                getTileUrl: function(coord, zoom) {
                    coord = rotateTile(coord, zoom, heading);
                    var x = coord.x;
                    var y = coord.y;
                    var url = 'http://' + regionForCoordinate(x, y, zoom) + '0.nearmap.com/maps/hl=en' + '&x=' + x + '&y=' + y + '&z=' + zoom + '&nml=' + name + '&httpauth=false&version=2' + '&apikey=' + apikey;
                    return url
                }
            });
            maptype.projection = new MercatorProjection(tileWidth, tileHeight);
            return maptype
        }

        function createBookmarkControl(map) {
            var bookmarkControlDiv = document.createElement('div');
            var bookmarkControl = new BookmarkControl(bookmarkControlDiv, map);
            bookmarkControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(bookmarkControlDiv)
        }

        function BookmarkControl(controlDiv, map) {
            function createBookmarkLink(label, point) {
                var bookmarkLink = document.createElement('span');
                bookmarkLink.className = 'bookmark-link';
                bookmarkLink.innerHTML = label;
                bookmarkLink.addEventListener('click', function() {
                    map.setCenter(fakeLatLng(map.getMapTypeId(), point))
                });
                return bookmarkLink
            }
        }
        if (apikey === undefined || apikey.length === 0) {
            alert('Please provide your Nearmap API Key');
            return
        }
        var map_types = [createMapType(256, 256, 0, 'Vert'), createMapType(256, 181, 0, 'N'), createMapType(256, 181, 90, 'E'), createMapType(256, 181, 180, 'S'), createMapType(256, 181, 270, 'W'), ];
        mapOptions = {
            zoom: 15,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'Vert', 'N', 'E', 'S', 'W'],
                streetViewControl: !1
            },
            disableDefaultUI: !0,
            center: melbourne,
            mapTypeControl: !1,
            streetViewControl: !1,
            fullscreenControl: !1,
            rotateControl: !1,
        };
        spherical = google.maps.geometry.spherical;
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        for (var i = 0; i < map_types.length; i++) {
            var mt = map_types[i];
            map.mapTypes.set(mt.name, mt)
        }
        registerProjectionWorkaround(map);
        map.setMapTypeId('Vert');
        createBookmarkControl(map)
    } else {
        mapOptions = {
            zoom: 1,
            disableDefaultUI: !0,
            center: melbourne,
            mapTypeId: 'satellite',
            mapTypeControl: !1,
            streetViewControl: !1,
            fullscreenControl: !1,
            rotateControl: !1,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                mapTypeIds: ['satellite'],
                streetViewControl: !1
            }
        };
        spherical = google.maps.geometry.spherical;
        map = new google.maps.Map(document.getElementById('map'), mapOptions)
    }
    maxZoomService = new google.maps.MaxZoomService();
    map.addListener('click', showMaxZoom);
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: !1,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT,
            drawingModes: ['rectangle']
        },
        polygonOptions: {
            fillColor: '#000000',
            fillOpacity: 0.02,
            strokeWeight: 2,
            strokeColor: '#ff0000',
            clickable: !0,
            editable: !0,
            draggable: !1,
            zIndex: 1
        }
    });
    drawingRemoveManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: !1,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT,
            drawingModes: [google.maps.drawing.OverlayType.MARKER, google.maps.drawing.OverlayType.CIRCLE, google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.POLYLINE, google.maps.drawing.OverlayType.RECTANGLE]
        },
        polygonOptions: {
            fillColor: '#000000',
            fillOpacity: 0.01,
            strokeWeight: 2.5,
            strokeColor: '#00ff00',
            clickable: !1,
            editable: !1,
            draggable: !1,
            zIndex: 3
        }
    });
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        var newShape = event.overlay;
        newShape.type = event.type;
        marker.setMap(null);
        if (event.type == google.maps.drawing.OverlayType.POLYGON) {
            console.log("hi");
            $('#pac-card').css("display", "block");
            $('#remove-multi-roof-panels-card').removeClass("dltLine");
            $('.remove-multi-button').removeAttr("disabled");
            completedDrawing(event);
            $('#pac-card').css("display", "block");
            $('#tour-card > div:last-of-type').removeClass('tour_card_1').addClass('tour_card_3');
            $('#tour-card p').text('Deselect Panels');
            $('#add-roof-title').text('Add Another Roof Section');
            togglePacCard('show')
        } else {}
    });
    google.maps.event.addListener(drawingManager, "polygoncomplete", function(polygon) {
        polygon.setEditable(!0);
        polygon.setDraggable(!0);
        polygon.dragInProgress = !1;
        google.maps.event.addListener(polygon, 'dragstart', function() {
            polygon.dragInProgress = !0
        });
        google.maps.event.addListener(polygon, 'dragend', function() {
            roofLocationChanged(polygon)
        });
        google.maps.event.addListener(polygon.getPath(), "insert_at", function() {
            roofPathChanged(polygon)
        });
        google.maps.event.addListener(polygon.getPath(), "remove_at", function() {
            roofPathChanged(polygon)
        });
        google.maps.event.addListener(polygon.getPath(), "set_at", function() {
            if (!polygon.dragInProgress) {
                roofPathChanged(polygon)
            }
        });
        while (polygonArray.length > 0) {
            polygonArray.pop()
        }
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
            polygonArray.push(polygon.getPath().getAt(i).toUrlValue(6))
        }
        var resPoly1 = polygonArray[0].split(",");
        var lat1 = (resPoly1[0]);
        var lon1 = (resPoly1[1]);
        var resPoly2 = polygonArray[1].split(",");
        var lat2 = (resPoly2[0]);
        var lon2 = (resPoly2[1]);
        var deg = measure(lat1, lon1, lat2, lon2);
        anglesArray[section_id] = deg
    });

    function showMaxZoom(e) {
        maxZoomService.getMaxZoomAtLatLng(e.latLng, function(response) {
            if (response.status !== 'OK') {
                console.log('Error in MaxZoomService')
            } else {
                console.log('The maximum zoom at this location is: ' + response.zoom)
            }
        })
    }

    function roofLocationChanged(polygon) {
        polygon.dragInProgress = !0;
        setTimeout(function() {
            var polygon_remove_btn_id = polygon.id + 1;
            document.getElementById("num_panels_" + polygon_remove_btn_id).innerHTML = 0;
            for (var i = 0, len = roofPanels[polygon_remove_btn_id].length; i < len; i++) {
                for (var j = 0; j < roofPanels[polygon_remove_btn_id][i].length; j++) {
                    roofPanels[polygon_remove_btn_id][i][j].setMap(null);
                    removeFromArray(all_panels_instance_array, roofPanels[polygon_remove_btn_id][i][j])
                }
            }
            var temp_section_id = section_id;
            section_id = polygon_remove_btn_id;
            completedDrawing(polygon, !0);
            setTimeout(function() {
                section_id = temp_section_id
            }, 2000)
        }, 500);
        polygon.dragInProgress = !1
    }

    function roofPathChanged(polygon) {
        setTimeout(function() {
            var polygon_remove_btn_id = polygon.id + 1;
            document.getElementById("num_panels_" + polygon_remove_btn_id).innerHTML = 0;
            for (var i = 0, len = roofPanels[polygon_remove_btn_id].length; i < len; i++) {
                for (var j = 0; j < roofPanels[polygon_remove_btn_id][i].length; j++) {
                    roofPanels[polygon_remove_btn_id][i][j].setMap(null);
                    removeFromArray(all_panels_instance_array, roofPanels[polygon_remove_btn_id][i][j])
                }
            }
            var temp_section_id = section_id;
            section_id = polygon_remove_btn_id;
            completedDrawing(polygon, !0);
            setTimeout(function() {
                section_id = temp_section_id
            }, 2000)
        }, 500)
    }
    google.maps.event.addListener(drawingRemoveManager, 'overlaycomplete', function(event) {
        var newShape = event.overlay;
        newShape.type = event.type;
        marker.setMap(null);
        setTimeout(function() {
            polygonArray1 = [];
            newShape.setMap(null)
        }, 1000);
        if (event.type == google.maps.drawing.OverlayType.POLYGON) {
            if (drawingRemoveManager.getDrawingMode()) {
                drawingRemoveManager.setDrawingMode(null)
            }
        } else {}
    });
    google.maps.event.addListener(drawingRemoveManager, "polygoncomplete", function(polygon) {
        while (polygonArray.length > 0) {
            polygonArray.pop()
        }
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
            polygonArray.push(polygon.getPath().getAt(i).toUrlValue(7))
        }
        polygonArray1.push(polygon);
        setTimeout(function() {
            var pointsInside = 0;
            var pointsOutside = 0;
            for (var i = 0; i < all_panels_instance_array.length; i++) {
                var points = all_panels_instance_array[i].getPath().getArray().map(function(x) {
                    return x
                });
                polygonArray1.forEach(function(polygon) {
                    for (var j = 0; j < points.length; j++) {
                        if (google.maps.geometry.poly.containsLocation(points[j], polygon)) {
                            var new_sec_id = all_panels_instance_array[i].id;
                            togglePanel(all_panels_instance_array[i], new_sec_id, !1)
                        }
                    }
                })
            }
        }, 500)
    });
    $(".draw-button").click(function() {
        var mode = $(this).attr('data-id');
        $('.tour-card').addClass('show_card');
        $('.tour-card button.close').click(function() {
            $('#tour-card').addClass('take_tour')
        });
        $('.tour-card button.tour').click(function() {
            $('#tour-card').removeClass('take_tour')
        });
        togglePacCard('hide');
        setTimeout(function() {}, 2000);
        var polygonOptions = drawingManager.get('polygonOptions');
        polygonOptions.id = section_id;
        drawingManager.set('polygonOptions', polygonOptions);
        drawingManager.setMap(map);
        solarPanels = [];
        drawingManager.setDrawingMode('polygon');
        $('.panel-group h4 a').each(function() {
            $(this).addClass('collapsed').prop('aria-expanded', 'false');
            let section_num = $(this).attr('href').split('_')[1];
            $('#collapse_' + section_num).removeClass('in').prop('aria-expanded', 'false')
        });
        $('#panel_mode_modal').modal('show')
    });
    $(".remove-multi-button").click(function() {
        var mode = $(this).attr('data-id');
        drawingRemoveManager.setMap(map);
        drawingRemoveManager.setDrawingMode(mode)
    });
    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input2');
    var types = document.getElementById('type-selector');
    var strictBounds = document.getElementById('strict-bounds-selector');
    var addSectionContainer = document.getElementById('add-roof-panels-card');
    var tourCard = document.getElementById('tour-card');
    var compass = document.getElementById('compass');
    $('#search-address').prop('disabled', !0);
    $('#pac-input2').on('input', function(e) {
        $('#search-address').prop('disabled', !0)
    });
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(tourCard)
}

function rotate90() {
    var heading = map.getHeading() || 0;
    map.setHeading(heading + 90)
}
var firstPoint;
var secondPoint;
var thirdPoint;
var heading1;
var heading1_invers;
var heading2;
var numberOfPanels;

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

function completedDrawing(event, is_changed_event = !1) {
    if (debug) console.log('Completed polygon...');
    if (debug) console.log('Turn off drawing mode...');
    if (drawingManager.getDrawingMode()) {
        drawingManager.setDrawingMode(null)
    }
    if (debug) console.log('Starting the solar panel calculations...');
    var group;
    var flag = !1;
    if (is_changed_event) {
        event.overlay = event;
        group = event.overlay
    } else {
        group = event.overlay
    }
    for (var i = 0; i < group.getPath().getArray().length; i++) {
        var node = group.getPath().getArray()[i];
        if (debug) console.log(node.lat() + "," + node.lng());
        if (i == 0) {
            flag = !0;
            firstPoint = new google.maps.LatLng(node.lat(), node.lng())
        } else if (i == 1) {
            secondPoint = new google.maps.LatLng(node.lat(), node.lng());
            heading1 = spherical.computeHeading(firstPoint, secondPoint);
            heading1_invers = spherical.computeHeading(secondPoint, firstPoint)
        } else if (i == 2) {
            thirdPoint = new google.maps.LatLng(node.lat(), node.lng());
            heading2 = spherical.computeHeading(secondPoint, thirdPoint)
        }
    }
    google.maps.Polygon.prototype.getBoundsPolygon = function() {
        var bounds = new google.maps.LatLngBounds();
        var paths = this.getPaths();
        var path;
        for (var i = 0; i < paths.getLength(); i++) {
            path = paths.getAt(i);
            for (var ii = 0; ii < path.getLength(); ii++) {
                bounds.extend(path.getAt(ii))
            }
        }
        return bounds
    }
    google.maps.Polygon.prototype.getLongestSideHeadingPolygon = function() {
        var longest_LatLng_A;
        var longest_LatLng_B;
        var longest_LatLng_A_Temp;
        var longest_LatLng_B_Temp;
        var longest_LatLng_Last_Temp;
        var SideDistTemp;
        var longestSideDist = 0;
        var longestSideHeading;
        var longestSideHeading_Inverse;
        var final_p1;
        var final_p2;
        var group2 = event.overlay;
        for (var i = 0; i < group2.getPath().getArray().length; i++) {
            var xy = group2.getPath().getArray()[i];
            if (i == 0) {
                longest_LatLng_A_Temp = new google.maps.LatLng(xy.lat(), xy.lng())
            } else if (i == 1) {
                longest_LatLng_B_Temp = new google.maps.LatLng(xy.lat(), xy.lng());
                longest_LatLng_Last_Temp = longest_LatLng_B_Temp;
                SideDistTemp = google.maps.geometry.spherical.computeDistanceBetween(longest_LatLng_A_Temp, longest_LatLng_B_Temp);
                if (SideDistTemp > longestSideDist) {
                    longestSideDist = SideDistTemp;
                    longestSideHeading = google.maps.geometry.spherical.computeHeading(longest_LatLng_A_Temp, longest_LatLng_B_Temp);
                    longestSideHeading_Inverse = google.maps.geometry.spherical.computeHeading(longest_LatLng_B_Temp, longest_LatLng_A_Temp);
                    final_p1 = longest_LatLng_A_Temp;
                    final_p2 = longest_LatLng_B_Temp
                }
            } else if (group2.getPath().getArray().length == (i + 1)) {
                longest_LatLng_A_Temp = new google.maps.LatLng(group2.getPath().getArray()[i].lat(), group2.getPath().getArray()[i].lng());
                longest_LatLng_B_Temp = new google.maps.LatLng(group2.getPath().getArray()[0].lat(), group2.getPath().getArray()[0].lng());
                SideDistTemp = google.maps.geometry.spherical.computeDistanceBetween(longest_LatLng_A_Temp, longest_LatLng_B_Temp);
                if (SideDistTemp > longestSideDist) {
                    longestSideDist = SideDistTemp;
                    longestSideHeading = google.maps.geometry.spherical.computeHeading(longest_LatLng_A_Temp, longest_LatLng_B_Temp);
                    longestSideHeading_Inverse = google.maps.geometry.spherical.computeHeading(longest_LatLng_B_Temp, longest_LatLng_A_Temp);
                    final_p1 = longest_LatLng_A_Temp;
                    final_p2 = longest_LatLng_B_Temp
                }
            } else {
                longest_LatLng_B_Temp = new google.maps.LatLng(xy.lat(), xy.lng());
                SideDistTemp = google.maps.geometry.spherical.computeDistanceBetween(longest_LatLng_Last_Temp, longest_LatLng_B_Temp);
                if (SideDistTemp > longestSideDist) {
                    longestSideDist = SideDistTemp;
                    longestSideHeading = google.maps.geometry.spherical.computeHeading(longest_LatLng_Last_Temp, longest_LatLng_B_Temp);
                    longestSideHeading_Inverse = google.maps.geometry.spherical.computeHeading(longest_LatLng_B_Temp, longest_LatLng_Last_Temp);
                    final_p1 = longest_LatLng_Last_Temp;
                    final_p2 = longest_LatLng_B_Temp
                }
                longest_LatLng_Last_Temp = longest_LatLng_B_Temp
            }
        }
        var mid_latLng = google.maps.geometry.spherical.computeOffset(final_p1, google.maps.geometry.spherical.computeDistanceBetween(final_p1, final_p2), google.maps.geometry.spherical.computeHeading(final_p1, final_p2));
        var m_offset_1 = spherical.computeOffset(mid_latLng, 0.3, (longestSideHeading + 90));
        var m_offset_2 = spherical.computeOffset(mid_latLng, 0.3, (longestSideHeading - 90));
        if (debug) console.log("P1: " + final_p1);
        if (debug) console.log("P2: " + final_p2);
        if (debug) console.log("M1: " + mid_latLng);
        if (debug) console.log("O1: " + m_offset_1);
        if (debug) console.log("O2: " + m_offset_2);
        var obj_Headings = {
            normal: longestSideHeading,
            inverse: longestSideHeading_Inverse,
            mid_point_M1: mid_latLng,
            mid_point_o1: m_offset_1,
            mid_point_o2: m_offset_2
        };
        return obj_Headings
    }
    var roofArea = new google.maps.Polygon({
        paths: event.overlay.getPath()
    });
    var roofBounds = new google.maps.LatLngBounds();
    roofBounds = roofArea.getBoundsPolygon();
    var longestSideheadings = roofArea.getLongestSideHeadingPolygon();
    heading1 = longestSideheadings.normal;
    heading1_invers = longestSideheadings.inverse;
    if (!is_changed_event) {
        var newShape = event.overlay;
        newShape.type = event.type;
        buildSection(newShape)
    }
    var NE = roofBounds.getNorthEast();
    var SW = roofBounds.getSouthWest();
    var NW = new google.maps.LatLng(NE.lat(), SW.lng());
    var SE = new google.maps.LatLng(SW.lat(), NE.lng());
    var distance1 = distance(NE.lat(), NE.lng(), SE.lat(), SE.lng());
    var distance2 = distance(SE.lat(), SE.lng(), SW.lat(), SW.lng());
    var start_rec = NW;
    var increasePercentage = 1.10;
    var pointSouthWest = SW;
    var pointNorthEast = NE;
    var latAdjustment = (pointNorthEast.lat() - pointSouthWest.lat()) * (increasePercentage - 1);
    var lngAdjustment = (pointNorthEast.lng() - pointSouthWest.lng()) * (increasePercentage - 1);
    var newPointNorthEast = new google.maps.LatLng(pointNorthEast.lat() + latAdjustment, pointNorthEast.lng() + lngAdjustment);
    var newPointSouthWest = new google.maps.LatLng(pointSouthWest.lat() - latAdjustment, pointSouthWest.lng() - lngAdjustment);
    M = longestSideheadings.mid_point_M1;
    if (google.maps.geometry.poly.containsLocation(longestSideheadings.mid_point_o1, roofArea) == !0) {
        if (debug) console.log("mid_point_o1: IN");
        M = longestSideheadings.mid_point_o1
    } else {
        if (debug) console.log("mid_point_o1: OUT")
    }
    if (google.maps.geometry.poly.containsLocation(longestSideheadings.mid_point_o2, roofArea) == !0) {
        if (debug) console.log("mid_point_o2: IN");
        M = longestSideheadings.mid_point_o2
    } else {
        if (debug) console.log("mid_point_o2: OUT")
    }
    M = longestSideheadings.mid_point_M1;
    var countPanelRows = 0;
    flag_row_stop = !1;
    roofPanels[section_id] = [];
    NW_extended = M;
    var is_square = !1;
    if (panel_mode == 'landscape') {
        is_square = (distance1 > distance2) ? !1 : !0
    } else {
        is_square = (distance1 > distance2) ? !0 : !1
    }
    drawSolarPanels(NW_extended, roofArea, is_square);
    while (flag_row_stop == !1) {
        drawSolarPanels(next_row_start_point, roofArea, is_square);
        countPanelRows = countPanelRows + 1;
        if ((countPanelRows + 1) > solarPanelColNum) {
            flag_row_stop = !0
        }
    }
    countPanelRows = 0;
    flag_row_stop = !1;
    drawSolarPanelsInvers(NW_extended, roofArea, is_square);
    while (flag_row_stop == !1) {
        drawSolarPanelsInvers(next_row_start_point, roofArea, is_square);
        countPanelRows = countPanelRows + 1;
        if ((countPanelRows + 1) > solarPanelColNum) {
            flag_row_stop = !0
        }
    }
    countPanelRows = 0;
    flag_row_stop = !1;
    drawSolarPanelsInvers__(NW_extended, roofArea, is_square);
    while (flag_row_stop == !1) {
        drawSolarPanelsInvers__(next_row_start_point, roofArea, is_square);
        countPanelRows = countPanelRows + 1;
        if ((countPanelRows + 1) > solarPanelColNum) {
            flag_row_stop = !0
        }
    }
    countPanelRows = 0;
    flag_row_stop = !1;
    drawSolarPanelsInvers_(NW_extended, roofArea, is_square);
    while (flag_row_stop == !1) {
        drawSolarPanelsInvers_(next_row_start_point, roofArea, is_square);
        countPanelRows = countPanelRows + 1;
        if ((countPanelRows + 1) > solarPanelColNum) {
            flag_row_stop = !0
        }
    }
    polyShapes.push(group);
    var roof_area = google.maps.geometry.spherical.computeArea(event.overlay.getPath()).toFixed(2);
    if (debug) console.log('Computed Area: ' + roof_area + ' m/squ');
    var roof_area_formatted = roof_area + ' m<sup>2</sup>';
    document.getElementById("roof-area_" + section_id).innerHTML = roof_area_formatted
}

function setClickEvent(sp_temp, new_section_id) {
    new google.maps.event.addListener(sp_temp, 'click', function(event) {
        togglePanel(this, new_section_id)
    })
}

function togglePanel(obj, new_sec_id, visible = !0) {
    var color = $('#features').val();
    var dseltColor = color.split("|");
    if (obj.get('fillColor') == 'white') {
        if (visible) {
            var temp_num_panels2 = parseInt(document.getElementById("num_panels_" + new_sec_id).innerHTML);
            temp_num_panels2 = (temp_num_panels2 + 1);
            document.getElementById("num_panels_" + new_sec_id).innerHTML = temp_num_panels2;
            obj.setOptions({
                strokeWeight: 0.2,
                fillColor: '#000000',
                fillOpacity: 0.7
            })
        }
    } else {
        var temp_num_panels3 = parseInt(document.getElementById("num_panels_" + new_sec_id).innerHTML);
        temp_num_panels3 = (temp_num_panels3 - 1);
        document.getElementById("num_panels_" + new_sec_id).innerHTML = temp_num_panels3;
        obj.setOptions({
            strokeWeight: 0.0,
            fillColor: 'white',
            fillOpacity: 0.0
        })
    }
}

function drawSolarPanels(startPoint, roofAreaPolygon, is_square) {
    var heading = heading1;
    var features1 = $('#features').val();
    var res1 = features1.split("|");
    var solarPanel_length = parseFloat(res1[0]);
    var solarPanel_width = parseFloat(res1[1]);
    var solarPanel_gap = parseFloat(res1[2]);
    if (!is_square) {
        solarPanel_width = parseFloat(res1[1]);
        solarPanel_length = parseFloat(res1[0])
    }
    var nP_1 = spherical.computeOffset(startPoint, solarPanel_width + solarPanel_gap, heading);
    var nP_2 = spherical.computeOffset(nP_1, solarPanel_length, heading - 90);
    var nP_3 = spherical.computeOffset(nP_2, solarPanel_width + solarPanel_gap, heading - 90 - 90);
    var nextRowStart;
    var xnP_1;
    var xnP_2;
    var xnP_3;
    var solarPanel_OnRoof = !1;
    var flag_stop = !1;
    var countPanels = 0;
    next_row_start_point = spherical.computeOffset(startPoint, solarPanel_length + solarPanel_gap, heading - 90);
    solarPanels = [];
    while (flag_stop == !1) {
        solarPanel_OnRoof = !1;
        solarPanel_OnRoof_1 = !1;
        solarPanel_OnRoof_2 = !1;
        solarPanel_OnRoof_3 = !1;
        solarPanel_OnRoof_4 = !1;
        startPoint = spherical.computeOffset(startPoint, solarPanel_gap, heading);
        xnP_1 = spherical.computeOffset(startPoint, solarPanel_width, heading);
        xnP_2 = spherical.computeOffset(xnP_1, solarPanel_length, heading - 90);
        xnP_3 = spherical.computeOffset(xnP_2, solarPanel_width, heading - 90 - 90);
        if (google.maps.geometry.poly.containsLocation(startPoint, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_1 = !0
        } else {
            solarPanel_OnRoof_1 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_1, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_2 = !0
        } else {
            solarPanel_OnRoof_2 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_2, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_3 = !0
        } else {
            solarPanel_OnRoof_3 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_3, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_4 = !0
        } else {
            solarPanel_OnRoof_4 = !1
        }
        solarPanel_OnRoof = !0;
        if (solarPanel_OnRoof_1 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_2 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_3 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_4 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof) {
            var solar_panel_temp = new google.maps.Polygon({
                path: [startPoint, xnP_1, xnP_2, xnP_3],
                id: section_id,
                geodesic: !0,
                fillOpacity: solarPanelFillOpacity,
                strokeWeight: solarPanelStrokeWeight,
                strokeColor: solarPanelStrokeColor,
                zIndex: 2,
                map: map
            });
            solarPanels.push(solar_panel_temp);
            all_panels_instance_array.push(solar_panel_temp);
            var temp_num_panels1 = parseInt(document.getElementById("num_panels_" + section_id).innerHTML);
            temp_num_panels1 = (temp_num_panels1 + 1);
            document.getElementById("num_panels_" + section_id).innerHTML = temp_num_panels1;
            setClickEvent(solar_panel_temp, section_id)
        }
        startPoint = xnP_1;
        countPanels = countPanels + 1;
        if ((countPanels + 1) > solarPanelRowNum) {
            flag_stop = !0
        }
    }
    if (solarPanels.length > 0) {
        roofPanels[section_id].push(solarPanels)
    }
}

function drawSolarPanelsInvers(startPoint, roofAreaPolygon, is_square) {
    var heading = heading1;
    var features1 = $('#features').val();
    var res1 = features1.split("|");
    var solarPanel_length = parseFloat(res1[0]);
    var solarPanel_width = parseFloat(res1[1]);
    var solarPanel_gap = parseFloat(res1[2]);
    if (!is_square) {
        solarPanel_width = parseFloat(res1[1]);
        solarPanel_length = parseFloat(res1[0])
    }
    var nP_1 = spherical.computeOffset(startPoint, solarPanel_width + solarPanel_gap, heading);
    var nP_2 = spherical.computeOffset(nP_1, solarPanel_length, heading + 90);
    var nP_3 = spherical.computeOffset(nP_2, solarPanel_width + solarPanel_gap, heading + 90 + 90);
    var nextRowStart;
    var xnP_1;
    var xnP_2;
    var xnP_3;
    var solarPanel_OnRoof = !1;
    var flag_stop = !1;
    var countPanels = 0;
    next_row_start_point = spherical.computeOffset(startPoint, solarPanel_length + solarPanel_gap, heading + 90);
    solarPanels = [];
    while (flag_stop == !1) {
        solarPanel_OnRoof = !1;
        solarPanel_OnRoof_1 = !1;
        solarPanel_OnRoof_2 = !1;
        solarPanel_OnRoof_3 = !1;
        solarPanel_OnRoof_4 = !1;
        startPoint = spherical.computeOffset(startPoint, solarPanel_gap, heading);
        xnP_1 = spherical.computeOffset(startPoint, solarPanel_width, heading);
        xnP_2 = spherical.computeOffset(xnP_1, solarPanel_length, heading + 90);
        xnP_3 = spherical.computeOffset(xnP_2, solarPanel_width, heading + 90 + 90);
        if (google.maps.geometry.poly.containsLocation(startPoint, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_1 = !0
        } else {
            solarPanel_OnRoof_1 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_1, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_2 = !0
        } else {
            solarPanel_OnRoof_2 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_2, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_3 = !0
        } else {
            solarPanel_OnRoof_3 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_3, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_4 = !0
        } else {
            solarPanel_OnRoof_4 = !1
        }
        solarPanel_OnRoof = !0;
        if (solarPanel_OnRoof_1 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_2 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_3 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_4 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof) {
            var solar_panel_temp = new google.maps.Polygon({
                path: [startPoint, xnP_1, xnP_2, xnP_3],
                id: section_id,
                geodesic: !0,
                fillOpacity: solarPanelFillOpacity,
                strokeWeight: solarPanelStrokeWeight,
                strokeColor: solarPanelStrokeColor,
                zIndex: 2,
                map: map
            });
            solarPanels.push(solar_panel_temp);
            all_panels_instance_array.push(solar_panel_temp);
            var temp_num_panels1 = parseInt(document.getElementById("num_panels_" + section_id).innerHTML);
            temp_num_panels1 = (temp_num_panels1 + 1);
            document.getElementById("num_panels_" + section_id).innerHTML = temp_num_panels1;
            setClickEvent(solar_panel_temp, section_id)
        }
        startPoint = xnP_1;
        countPanels = countPanels + 1;
        if ((countPanels + 1) > solarPanelRowNum) {
            flag_stop = !0
        }
    }
    if (solarPanels.length > 0) {
        roofPanels[section_id].push(solarPanels)
    }
}
var count = 0;

function drawSolarPanelsInvers_(startPoint, roofAreaPolygon, is_square) {
    var heading = heading1_invers;
    var features1 = $('#features').val();
    var res1 = features1.split("|");
    var solarPanel_length = parseFloat(res1[0]);
    var solarPanel_width = parseFloat(res1[1]);
    var solarPanel_gap = parseFloat(res1[2]);
    if (!is_square) {
        solarPanel_width = parseFloat(res1[0]);
        solarPanel_length = parseFloat(res1[1])
    }
    var nP_1 = spherical.computeOffset(startPoint, solarPanel_width + solarPanel_gap, heading);
    var nP_2 = spherical.computeOffset(nP_1, solarPanel_length, heading + 90);
    var nP_3 = spherical.computeOffset(nP_2, solarPanel_width + solarPanel_gap, heading + 90 + 90);
    var nextRowStart;
    var xnP_1;
    var xnP_2;
    var xnP_3;
    var solarPanel_OnRoof = !1;
    var flag_stop = !1;
    var countPanels = 0;
    next_row_start_point = spherical.computeOffset(startPoint, solarPanel_length + solarPanel_gap, heading + 90);
    solarPanels = [];
    while (flag_stop == !1) {
        solarPanel_OnRoof = !1;
        solarPanel_OnRoof_1 = !1;
        solarPanel_OnRoof_2 = !1;
        solarPanel_OnRoof_3 = !1;
        solarPanel_OnRoof_4 = !1;
        startPoint = spherical.computeOffset(startPoint, solarPanel_gap, heading);
        xnP_1 = spherical.computeOffset(startPoint, solarPanel_width, heading);
        xnP_2 = spherical.computeOffset(xnP_1, solarPanel_length, heading + 90);
        xnP_3 = spherical.computeOffset(xnP_2, solarPanel_width, heading + 90 + 90);
        if (google.maps.geometry.poly.containsLocation(startPoint, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_1 = !0
        } else {
            solarPanel_OnRoof_1 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_1, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_2 = !0
        } else {
            solarPanel_OnRoof_2 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_2, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_3 = !0
        } else {
            solarPanel_OnRoof_3 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_3, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_4 = !0
        } else {
            solarPanel_OnRoof_4 = !1
        }
        solarPanel_OnRoof = !0;
        if (solarPanel_OnRoof_1 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_2 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_3 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_4 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof) {
            var solar_panel_temp = new google.maps.Polygon({
                path: [startPoint, xnP_1, xnP_2, xnP_3],
                id: section_id,
                geodesic: !0,
                fillOpacity: solarPanelFillOpacity,
                strokeWeight: solarPanelStrokeWeight,
                strokeColor: solarPanelStrokeColor,
                zIndex: 2,
                map: map
            });
            solarPanels.push(solar_panel_temp);
            all_panels_instance_array.push(solar_panel_temp);
            var temp_num_panels1 = parseInt(document.getElementById("num_panels_" + section_id).innerHTML);
            temp_num_panels1 = (temp_num_panels1 + 1);
            document.getElementById("num_panels_" + section_id).innerHTML = temp_num_panels1;
            setClickEvent(solar_panel_temp, section_id)
        }
        startPoint = xnP_1;
        countPanels = countPanels + 1;
        if ((countPanels + 1) > solarPanelRowNum) {
            flag_stop = !0
        }
    }
    if (solarPanels.length > 0) {
        roofPanels[section_id].push(solarPanels)
    }
}

function drawSolarPanelsInvers__(startPoint, roofAreaPolygon, is_square) {
    var heading = heading1_invers;
    var features1 = $('#features').val();
    var res1 = features1.split("|");
    var solarPanel_length = parseFloat(res1[0]);
    var solarPanel_width = parseFloat(res1[1]);
    var solarPanel_gap = parseFloat(res1[2]);
    if (!is_square) {
        solarPanel_width = parseFloat(res1[0]);
        solarPanel_length = parseFloat(res1[1])
    }
    var nP_1 = spherical.computeOffset(startPoint, solarPanel_width + solarPanel_gap, heading);
    var nP_2 = spherical.computeOffset(nP_1, solarPanel_length, heading - 90);
    var nP_3 = spherical.computeOffset(nP_2, solarPanel_width + solarPanel_gap, heading - 90 - 90);
    var nextRowStart;
    var xnP_1;
    var xnP_2;
    var xnP_3;
    var solarPanel_OnRoof = !1;
    var flag_stop = !1;
    var countPanels = 0;
    next_row_start_point = spherical.computeOffset(startPoint, solarPanel_length + solarPanel_gap, heading - 90);
    solarPanels = [];
    while (flag_stop == !1) {
        solarPanel_OnRoof = !1;
        solarPanel_OnRoof_1 = !1;
        solarPanel_OnRoof_2 = !1;
        solarPanel_OnRoof_3 = !1;
        solarPanel_OnRoof_4 = !1;
        startPoint = spherical.computeOffset(startPoint, solarPanel_gap, heading);
        xnP_1 = spherical.computeOffset(startPoint, solarPanel_width, heading);
        xnP_2 = spherical.computeOffset(xnP_1, solarPanel_length, heading - 90);
        xnP_3 = spherical.computeOffset(xnP_2, solarPanel_width, heading - 90 - 90);
        if (google.maps.geometry.poly.containsLocation(startPoint, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_1 = !0
        } else {
            solarPanel_OnRoof_1 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_1, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_2 = !0
        } else {
            solarPanel_OnRoof_2 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_2, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_3 = !0
        } else {
            solarPanel_OnRoof_3 = !1
        }
        if (google.maps.geometry.poly.containsLocation(xnP_3, roofAreaPolygon) == !0) {
            solarPanel_OnRoof_4 = !0
        } else {
            solarPanel_OnRoof_4 = !1
        }
        solarPanel_OnRoof = !0;
        if (solarPanel_OnRoof_1 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_2 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_3 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof_4 == !1) {
            solarPanel_OnRoof = !1
        }
        if (solarPanel_OnRoof) {
            var solar_panel_temp = new google.maps.Polygon({
                path: [startPoint, xnP_1, xnP_2, xnP_3],
                id: section_id,
                geodesic: !0,
                fillOpacity: solarPanelFillOpacity,
                strokeWeight: solarPanelStrokeWeight,
                strokeColor: solarPanelStrokeColor,
                zIndex: 2,
                map: map
            });
            solarPanels.push(solar_panel_temp);
            all_panels_instance_array.push(solar_panel_temp);
            var temp_num_panels1 = parseInt(document.getElementById("num_panels_" + section_id).innerHTML);
            temp_num_panels1 = (temp_num_panels1 + 1);
            document.getElementById("num_panels_" + section_id).innerHTML = temp_num_panels1;
            setClickEvent(solar_panel_temp, section_id)
        }
        startPoint = xnP_1;
        countPanels = countPanels + 1;
        if ((countPanels + 1) > solarPanelRowNum) {
            flag_stop = !0
        }
    }
    if (solarPanels.length > 0) {
        roofPanels[section_id].push(solarPanels)
    }
}

function buildSection(poly_) {
    section_id++;
    var sectionAccordionHTML = '<div class="panel panel-default">\n' + '<div class="panel-heading solar-diagram-title">\n' + '<h4 class="panel-title">\n' + '<a data-toggle="collapse" data-parent="#accordion" href="#collapse_' + section_id + '">Section ' + section_id + '</a>\n' + '<i class="fa fa-times-circle remove-btn" id="remove-btn-' + section_id + '" data-id="' + section_id + '" data-type="delete" aria-hidden="true"></i>' + '</h4>\n' + '<h4 class="panel-title">\n' + '<a data-toggle="collapse" data-parent="#accordion" href="#collapse_' + section_id + '">Clear Section Border ' + section_id + '</a>\n' + '<i class="fa fa-times-circle remove-btn" id="clear-border-' + section_id + '" data-id="' + section_id + '" data-type="clsborder" aria-hidden="true">				                     </i>' + '</h4>\n' + '</div>\n' + '<table style="display:none;">' + '<tr>' + '<td>Solar Panels: </td>' + '<td style="padding-left: 10px;"><span class="sub-panel-num" id="num_panels_' + section_id + '">0</span></td>' + '</tr>' + '<tr>' + '<td>Roof Area: </td>' + '<td style="padding-left: 10px;"><span class="sub-panel-area" id="roof-area_' + section_id + '"></span></td>' + '</tr>' + '</table>' + '<div id="collapse_' + section_id + '" class="panel-collapse collapse in" style="display:none;">\n' + '<div class="panel-body">\n' + '<div>' + '<div id="roof_type_' + section_id + '">' + '<p>Roof Type</p>' + '<label class="radio-inline">' + '<input type="radio" class="roof-type" onclick="roofTypeFunc(' + section_id + ', this)" name="roof_type' + section_id + '" id="inlineRadio1" value="flat" checked> Flat' + '</label>' + '<label class="radio-inline">' + '<input type="radio" class="roof-type" onclick="roofTypeFunc(' + section_id + ', this)" name="roof_type' + section_id + '" id="inlineRadio2" value="angled"> Angled' + '</label>' + '<input id="roof-type-val-' + section_id + '" type="hidden" value="flat">' + '</div>' + '<div class="roof_facing" style="display:none;">' + '<p>Roof Facing</p>' + '<label class="radio-inline">' + '<input type="radio" class="roof-facing" onclick="roofFacingFunc(' + section_id + ', this)" name="roof_facing_' + section_id + '" id="inlineRadio1" value="N"> N' + '</label>' + '<label class="radio-inline">' + '<input type="radio" class="roof-facing" onclick="roofFacingFunc(' + section_id + ', this)" name="roof_facing_' + section_id + '" id="inlineRadio2" value="E"> E' + '</label>' + '<label class="radio-inline">' + '<input type="radio" class="roof-facing" onclick="roofFacingFunc(' + section_id + ', this)" name="roof_facing_' + section_id + '" id="inlineRadio2" value="S"> S' + '</label>' + '<label class="radio-inline">' + '<input type="radio" class="roof-facing" onclick="roofFacingFunc(' + section_id + ', this)" name="roof_facing_' + section_id + '" id="inlineRadio3" value="W"> W' + '</label>' + '<input id="roof-facing-val-' + section_id + '" type="hidden" value="">' + '</div>' + '<p id="roof_note_' + section_id + '" class="roof_note">' + 'As your roof is South facing, you will require a custom design to provide an accurate return.' + '</p>' + '</div>' + '</div>\n' + '</div>\n' + '</div>';
    var sections_container = $('.section-container');
    $(sections_container).append(sectionAccordionHTML);
    $('#remove-btn-' + section_id).click(function() {
        buttonActions(this, poly_)
    });
    $('#clear-border-' + section_id).click(function() {
        borderAction(this, poly_)
    })
}

function removeFromArray(arr) {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1)
        }
    }
    return arr
}

function buttonActions(obj, poly_) {
    if ($(obj).attr('data-type') == 'delete') {
        $(obj).parent().parent().parent().remove();
        poly_.setMap(null);
        var data_id_ = $(obj).attr('data-id');
        for (var i = 0, len = roofPanels[data_id_].length; i < len; i++) {
            for (var j = 0; j < roofPanels[data_id_][i].length; j++) {
                roofPanels[data_id_][i][j].setMap(null);
                removeFromArray(all_panels_instance_array, roofPanels[data_id_][i][j])
            }
        }
        if (!$('.panel-group h4 a').length) {
            $('#add-roof-title').text('Add Roof Section');
            $('#remove-multi-roof-panels-card').addClass("dltLine");
            $('.remove-multi-button').attr("disabled", "disabled");
            togglePacCard('hide');
            section_id = 0
        }
    } else {
        alert($(obj).attr('data-id'))
    }
}

function borderAction(obj, polybod_) {
    if ($(obj).attr('data-type') == 'clsborder') {
        $(obj).parent().remove();
        var data_id_ = $(obj).attr('data-id');
        for (var i = 0, len = roofPanels[data_id_].length; i < len; i++) {
            for (var j = 0; j < roofPanels[data_id_][i].length; j++) {
                polybod_.setOptions({
                    strokeWeight: 0
                });
                polybod_.setOptions({
                    clickable: !1
                });
                polybod_.setOptions({
                    editable: !1
                });
                polybod_.setOptions({
                    draggable: !1
                })
            }
        }
    }
}

function roofTypeFunc(sectionID_, obj_) {
    $('#roof-type-val-' + sectionID_).val(obj_.value);
    $parent = $("input[name='roof_facing_" + sectionID_ + "']").first().parent().parent();
    if (obj_.value == "angled") {
        $parent.css({
            'display': 'block'
        })
    } else {
        $parent.css({
            'display': 'none'
        })
    }
}

function roofFacingFunc(sectionID_, obj_) {
    $name = $(obj_).attr('name');
    $id = $name.split("_")[2];
    $val = $(obj_).val();
    $('#roof-facing-val-' + sectionID_).val($val);
    $('#roof_note_' + $id).css('display', (($val == 'S') ? 'block' : 'none'))
}
var getCanvas;

function transform(div) {
    if (div != undefined) {
        var transform = $(div).css("transform");
        var comp = transform.split(",");
        var mapleft = parseFloat(comp[4]);
        var maptop = parseFloat(comp[5]);
        $(div).css({
            "transform": "none",
            "left": mapleft,
            "top": maptop,
        })
    }
}

function transformBack(div) {
    if (div != undefined) {
        var transform = $(div).css("transform")
        $(div).css({
            left: 0,
            top: 0,
            "transform": transform
        })
    }
}
$('#save_image_locally').click(function() {
    $(".tour").hide();
    transform($('.gm-style>div:first>div:first>div:last>div')[0]);
    transform($('.gm-style>div:first>div:first>div:first>div:first>div')[0]);
    transform($('.gm-style>div:first>div:first>div:nth-child(2)>div:first>div')[0]);
    $(".gm-style-pbt").css({
        "transform": "none",
        "display": "none"
    });
    var width = document.getElementById('map').offsetWidth;
    var height = document.getElementById('map').offsetHeight;
    html2canvas($('#map'), {
        useCORS: !0,
        width: width,
        height: height,
        onrendered: function(canvas) {
            $("#shImg").append(canvas);
            getCanvas = canvas;
            transformBack($('.gm-style>div:first>div:first>div:last>div')[0]);
            transformBack($('.gm-style>div:first>div:first>div:first>div:first>div')[0]);
            transformBack($('.gm-style>div:first>div:first>div:nth-child(2)>div:first>div')[0]);
            var imgageData = getCanvas.toDataURL("image/png");
            console.log(imgageData);
            var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            $('#image').val(imgageData);
            if ($('#proposal_no_image') != undefined) {
                $('#proposal_no_image').attr('style', 'display:none !important;');
                $('#proposal_image').attr('style', 'display:block !important;');
                $('#proposal_image > .add-picture').css('background-image', 'url("' + imgageData + '")');
                $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
                $('#proposal_image > .add-picture').css('background-position', '50% 50%');
                $('#proposal_image > .add-picture').css('background-size', '108% 103%');
                $("#tab_image_upload_btn").click()
            }
        }
    });
    $("#btn-Convert-Html2Image").on('click', function() {
        var imgageData = getCanvas.toDataURL("image/png");
        var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
        $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData)
    })
});
$('.close').click(function() {
    $(".tour").show()
});

function googleMapApp() {
    var self = this
}
googleMapApp.prototype.setAddress = function(data) {
    marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });
    $('#search-address').prop('disabled', !0);
    marker.setVisible(!1);
    var place = data;
    if (!place.geometry) {
        window.alert("Please select the address from the list");
        return
    }
    if (place.geometry.viewport) {
        $('#search-address').prop('disabled', !1);
        map.fitBounds(place.geometry.viewport);
        map.setZoom(30)
    } else {
        $('#search-address').prop('disabled', !1);
        map.setCenter(place.geometry.location);
        map.setZoom(30)
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(!0);
    var address = '';
    if (place.address_components) {
        address = [(place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')].join(' ')
    }
    marker.setTitle(address);
    $('#query-address').val(address)
}
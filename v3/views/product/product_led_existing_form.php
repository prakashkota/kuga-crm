
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Admin  - Add Existing LED Product</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" name="productAdd" id="productAdd" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Product Details</h4>
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="text" name="ep_name"  class="form-control" placeholder="Product Name" value="<?php echo (!empty($product_data)) ? $product_data['ep_name'] : ''; ?>"  required>
                                        <?php echo form_error('name', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Type</label>
                                        <select name="type_id" class="form-control"  required>
                                            <?php foreach ($product_types as $key => $value) { ?>
                                                <option value="<?php echo $value['type_id']; ?>" <?php echo (!empty($product_data) && $product_data['type_id'] == $value['type_id']) ? 'selected' : ''; ?>>
                                                    <?php echo $value['type_name']; ?> 
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('type_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>State</label>
                                        <select name="state" class="form-control"  required>
                                            <?php foreach ($states as $key => $value) { ?>
                                                <option value="<?php echo $value; ?>" <?php echo (!empty($product_data) && $product_data['state'] == $value) ? 'selected' : ''; ?>>
                                                    <?php echo $value; ?> 
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('state', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>


                                    <div class="form-group">
                                        <label>Wattage</label>
                                        <input type="number" name="ep_wattage"  class="form-control" placeholder="Wattage" value="<?php echo (!empty($product_data)) ? $product_data['ep_wattage'] : ''; ?>" required>
                                        <?php echo form_error('ep_wattage', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Rated Life</label>
                                        <input type="number" name="ep_rated_life"  class="form-control" placeholder="Rated Life" value="<?php echo (!empty($product_data)) ? $product_data['ep_rated_life'] : ''; ?>" required>
                                        <?php echo form_error('ep_rated_life', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Cost Details</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Globe Cost</label>
                                                <input type="number" step="0.01" name="globe_cost"  class="form-control"   placeholder="Globe Cost" value="<?php echo (!empty($product_data)) ? $product_data['globe_cost'] : ''; ?>">
                                            </div>    
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Driver Consumption</label>
                                                <input type="number" step="0.01" name="driver_consumption"  class="form-control"   placeholder="Driver Consumption" value="<?php echo (!empty($product_data)) ? $product_data['driver_consumption'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Replacement Time (minutes)</label>
                                                <input type="number"  name="replacement_time"  class="form-control"   placeholder="Replacement Time" value="<?php echo (!empty($product_data)) ? $product_data['replacement_time'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Labour Rate (per hour)</label>
                                                <input type="number"  name="labour_rate"  class="form-control"   placeholder="Labour Rate" value="<?php echo (!empty($product_data)) ? $product_data['labour_rate'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Cost Per Minute</label>
                                                <input type="number" step="0.01" name="cost_per_minute"  class="form-control"   placeholder="Cost Per Minute" value="<?php echo (!empty($product_data)) ? $product_data['cost_per_minute'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Replacement Cost</label>
                                                <input type="number" step="0.01" name="replacement_cost"  class="form-control"   placeholder="Replacement Cost" value="<?php echo (!empty($product_data)) ? $product_data['replacement_cost'] : ''; ?>">
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="buttwrap" style="float:right;">
                        <input type="submit"   class="btn" value="SAVE">
                    </div>                                            
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>


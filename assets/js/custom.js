$(document).ready(function () {
    $('.menu-toggle-icon').click(function () {
        $('.sidebar-menu').toggleClass('active');
        $('.page-wrapper').toggleClass('body-space');
    });
    $.sidebarMenu($('.sidebar-menu'))
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
    $('.confirmation-modal').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

});

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function createLoader(text) {
    var loader = document.createElement('div');
    loader.className = "loader";
    var loader_text = document.createElement('div');
    loader_text.className = "text-center";
    loader_text.innerHTML = text;
    var div = document.createElement('div');
    div.className = "col-md-12";
    div.appendChild(loader);
    div.appendChild(loader_text);
    return div;
}

function createSpinner(text,width,height,color) {
    width = (width) ? 'width:'+width+'px;' : 'width:30px;';
    height = (height) ? 'height:'+height+'px;' : 'height:30px;';
    color = (color) ? 'border-top: 5px solid '+color+';' : 'border-top: 5px solid #555;';
    var loader = document.createElement('div');
    loader.className = "custom_spinner";
    loader.setAttribute('style',width+height+color);
    var loader_text = document.createElement('div');
    loader_text.className = "text-center";
    loader_text.innerHTML = text;
    var div = document.createElement('div');
    div.className = "col-md-12";
    div.appendChild(loader);
    div.appendChild(loader_text);
    return div;
}

function createPlaceHolder(image,col){
    var palceholder_card = document.createElement('div');
    palceholder_card.className = "placeholder__card card";
    
    var palceholder_card_image = document.createElement('div');
    palceholder_card_image.className = "image loading";
    
    var palceholder_card_bars = document.createElement('div');
    palceholder_card_bars.className = "bars";
    
    var palceholder_card_bar1 = document.createElement('div');
    palceholder_card_bar1.className = "bar bar1 loading";
    
    var palceholder_card_bar2 = document.createElement('div');
    palceholder_card_bar2.className = "bar bar2 loading";
    
    
    palceholder_card_bars.appendChild(palceholder_card_bar1);
    palceholder_card_bars.appendChild(palceholder_card_bar2);
    palceholder_card_bars.appendChild(palceholder_card_bar2);
    
    if(image){
        palceholder_card.appendChild(palceholder_card_image);
    }
    palceholder_card.appendChild(palceholder_card_bars);
    if(col != ''){
      var div_col = document.createElement('div');
      div_col.className = col;
      div_col.appendChild(palceholder_card);
      return div_col;
    }else{
     return palceholder_card;
    }
}
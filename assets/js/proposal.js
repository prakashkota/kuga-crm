

function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var proposal_calculator = function (options) {
    
    var self = this;
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.proposal_id = (options.proposal_id && options.proposal_id != '') ? options.proposal_id : self.uuid();
    this.lead_id = (options.lead_id && options.lead_id != '') ? options.lead_id : '';
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.user_type = options.user_type;
    this.proposal_image = '';
    this.cust_id = '';
    this.system_size = 0;
    this.no_of_panels = 0;
    this.prd_panel = 0;
    this.prd_inverter = 0;
    this.prd_battery = 0;
    this.prd_panel_price = 0;
    this.prd_inverter_price = 0;
    this.prd_battery_price = 0;
    this.system_total = 0;
    this.prev_system_total = 0;
    this.prev_total_cost = 0;
    this.custom_quote_total_inGst = 0;
    this.stored_custom_quote_total_inGst = 0;
    this.racking_price = 0;
    this.kliplock_price = 0;
    this.additional_cost_item_price = 0;
    this.line_item_price = 0;
    this.total_cost = 0;
    this.original = {};
    this.total_cost_incGst = 0;
    this.postcode_rating = 0;
    this.stc_deduction = 0;
    this.stc_price_assumption = options.stc_price_assumption;
    this.certificate_margin = options.certificate_margin;
    this.company_markup_price = 0;
    this.company_markup = 0;
    this.additional_margin = 0;
    this.line_item_data = [];
    this.activity_action = '';
    this.deal_won_lost = '';
    this.prev_deal_won_lost = '';
    this.deal_stage = '';
    this.prev_deal_stage = '';
    this.deal_stage_name = '';
    this.prev_deal_stage_name = '';
    this.proposal_stages = options.proposal_stages;
    this.additional_cost_items_wrapper = 'additional_cost_items_wrapper'
    this.additional_cost_items_btn = 'add_additional_cost_items';
    this.line_items_wrapper = 'line_items_wrapper'
    this.line_items_btn = 'add_line_items';
    this.product_type_selector = 'additional_product_type';
    this.line_item_selector = 'line_items';
    this.postcode = '';

    if (self.user_type != '1' && self.user_type != '4') {
        document.getElementById('line_items_div').setAttribute('class', 'hidden');
    }

    if (Object.keys(self.lead_data).length > 0 ) {
        self.cust_id = self.lead_data.cust_id;
        $('#cust_id').val(self.lead_data.cust_id);
        if (self.lead_data.site_id == 0 || self.lead_data.site_id === '0') {
            $('#site_id').val(self.lead_data.location_id);
        } else {
            $('#site_id').val(self.lead_data.site_id);
        }
        $('#companyName').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#dd_cname').html(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('input[name=customer_phone').val(self.lead_data.customer_contact_no);
        $('input[name=customer_address').val(self.lead_data.address);
        $('input[name=customer_postcode').val(self.lead_data.postcode);
        setTimeout(function () {
            $('#customer_postcode').trigger('change');
            self.fetch_system_item();
        }, 1000);
        $('#addNewCompany').addClass('hidden');
        $('#editCompany').removeClass('hidden');
        //Disable Lead Source If it is Allocated lead not self generated
        if(self.lead_data.lead_source != '' && self.lead_data.lead_source != null){
            $('#lead_source').val(self.lead_data.lead_source); 
        }
        
        if(self.lead_data.user_id != self.userid){
            $('#lead_source').attr('disabled','dsiabled');
        }else{
            //Remove first 2 options from Lead Source If it is self generated
            $('#lead_source').find('option').get(1).remove();
            $('#lead_source').find('option').get(1).remove();
        }
        
        if (self.lead_data.lead_stage != '' && self.lead_data.lead_stage != null) {
            self.deal_stage = self.lead_data.lead_stage;
            self.create_deal_stages();//Show Proceed To Job Management Button
            self.toogle_job_manage_btn();
        }
    
    }else{
        //Remove first 2 options from Lead Source If it is self generated
        $('#lead_source').find('option').get(1).remove();
        $('#lead_source').find('option').get(1).remove();
        $('#lead_source').val('Referral');
    }

    if (options.proposal_id && options.proposal_id != '') {
        $.isLoading({
            text: "Loading"
        });
        setTimeout(function () {
            self.load_proposal();
        }, 1000);
    }


    $('#prd_panel,#prd_inverter,#prd_battery').on('change', function () {
        self.calculate_racking_price();
        self.calculate_sum();
        self.create_quote_total();
    });

    $('.roof_type_no_of_panels').on('change', function () {
        self.calculate_racking_price();
        self.fetch_line_item_data();
    });


    $('#customer_postcode').on('change', function () {
        self.fetch_postcode_rating(($(this).val()));
    });

    /** $('#system_size').on('change', function () {
     self.system_size = parseFloat($(this).val());
     self.calculate_sum();
     self.create_quote_total();
     }); */

    $('#prd_panel,#system_size').change(function () {
        self.fetch_line_item_data();
    });

    $('#save_proposal').click(function () {
        self.save_proposal();
    });

    $('form button').on("click", function (e) {
        e.preventDefault();
    });

    $('#addNewCompany').click(function () {
        $('#add_new_company_modal').modal('show');
        $('#company_cust_id').val('');
    });

    $('#editCompany').click(function () {
        var cust_data;
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            cust_data = $('#companyName').attr('data-item');
            cust_data = JSON.parse(cust_data);
        }
        //console.log(cust_data);
        //Set Cust Data in Modal
        $('#company_cust_id').val(cust_data.cust_id);
        $('#locationPostCode').val(cust_data.postcode);
        $('#locationState').val(cust_data.state_id).trigger('change');
        $('#select2-locationstreetAddress-container').html(cust_data.address);
        $('#locationstreetAddressVal').val(cust_data.address);
        $('#contactMobilePhone').val(cust_data.customer_contact_no);
        $('#contactEmailId').val(cust_data.customer_email);
        $('#firstname').val(cust_data.first_name);
        $('#lastname').val(cust_data.last_name);
        $('#cust_title').val(cust_data.title);
        $('#companyNameForm').val(cust_data.company_name);
        $('#add_new_company_modal_title').html('Edit Customer');
        $('#add_new_company_modal').modal('show');
    });

    $('#create_pdf').click(function (e) {
        self.save_proposal(true);
    });

    $('#add_activity').click(function () {
        var cust_data;
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            cust_data = $('#companyName').attr('data-item');
            cust_data = JSON.parse(cust_data);
        }
        
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#mark_as_completed').addClass('hidden');
        $('.datepicker').show();
        self.activity_action = "Scheduled Activity";
        
        $('#activityLocationPostCode').val(cust_data.postcode);
        $('#select2-activityAddress-container').html(cust_data.address);
        $('#activityLocationAddressVal').val(cust_data.address);
        $('#activityLocationState').val(cust_data.state_id).trigger('change');
        $('#activity_modal').modal('show');
    });

   /** $('#schedule_activity').click(function () {
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#is_scheduled').val(1);
        $('#scheduled_date').removeClass('hidden');
        $('#scheduled_time').removeClass('hidden');
        $('#scheduled_duration').removeClass('hidden');
        $('.datepicker').show();
        self.activity_action = "Scheduled Activity";
        $('#activity_modal').modal('show');
    }); */

    $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    $('.datepicker').datepicker({
        autoclose: true,
    }).on('changeDate', function () {
        $('.datepicker').hide();
    });

    $('.sr-show_inactive_users').click(function () {
        $('.sr-inactive_user').addClass('hidden');
        if ($(this).prop("checked") == true) {
            $('.sr-inactive_user').removeClass('hidden');
        }
    });

    self.fetch_products();
    //self.autocomplete_customer();
    $('#contactEmailId').change(function () {
        var data = {};
        data.email = document.getElementById('contactEmailId').value;
        self.validate_customer(data);
    });

    $('#contactMobilePhone').change(function () {
        var data = {};
        data.contact_no = document.getElementById('contactMobilePhone').value;
        self.validate_customer(data);
    });

    $('#locationstreetAddressVal').change(function () {
        var data = {};
        data.address = document.getElementById('locationstreetAddressVal').value;
        self.validate_customer(data);
    });

    $('#locationPostCode').change(function () {
        self.postcode = document.getElementById('locationPostCode').value;
        //self.fetch_franchise_list();
    });

    $('#scheduled_time input').timepicker({
        'minTime': '12:00am',
        'maxTime': '11:00pm',
        'showDuration': true
    });
    
    $('#scheduled_duration input').timepicker({
        'minTime': '0:30',
        'maxTime': '08:30',
        'timeFormat': 'H:i'
    });
    
    self.save_customer();
    self.create_additional_items();
    self.remove_additional_items();
    //self.create_line_items();
    self.remove_line_items();
    self.upload_proposal_image();
    self.show_image();
    self.hide_image();
    //Deal
    self.hanlde_deal_won_lost();
    self.create_deal_stages();
    
};

proposal_calculator.prototype.create_pdf = function () {
    var self = this;
    var url = base_url + 'admin/proposal/download/' + self.proposal_id;
    document.getElementById('create_pdf_download').setAttribute('href', url);
    document.getElementById('create_pdf_download').click();
};

proposal_calculator.prototype.uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

proposal_calculator.prototype.autocomplete_customer = function () {
    var self = this;
    
    $('#companyName').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url + "admin/customer/fetch_customers_by_keyword",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term,
                    request: 1
                },
                success: function (data) {
                    response(data.customers);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },
        select: function (event, ui) {
            $(this).val(ui.item.name);
            $('#dd_cname').html(ui.item.name);
            var userid = ui.item.value;
            $.ajax({
                url: base_url + 'admin/customer/fetch_customer_location',
                type: 'post',
                data: {
                    cust_id: userid
                },
                dataType: 'json',
                success: function (response) {
                    $('#cust_id').val(userid);
                    self.cust_id = userid;
                    $('#site_id').val(response.location.location_id);
                    $('input[name=customer_phone').val(response.location.customer_contact_no);
                    $('input[name=customer_address').val(response.location.address);
                    $('input[name=customer_postcode').val(response.location.postcode).trigger('change');
                    $('.placecomplete').val(response.location.address).trigger('change.select2');
                    //Populate Panel,Inverter,Battery data accordingly
                    self.fetch_system_item();
                    self.calculate_sum();
                    self.create_quote_total();
                    //Convert Add New Customer to Edit customer
                    $('#addNewCompany').addClass('hidden');
                    $('#editCompany').removeClass('hidden');
                    $('#companyName').attr('data-item', JSON.stringify(response.location));
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
            return false;
        }
    });

};

proposal_calculator.prototype.validate_customer = function (data) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/customer/check_unique_customer',
        data: data,
        datatype: 'json',
        beforeSend: function () {
            $("#addNewCompanyBtn").attr("disabled", "disabled");
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == false) {
                $('#checkcompany').html('<div style="color:#fe0000">' + stat.status + '</div>');
                $("#addNewCompanyBtn").attr("disabled", "disabled");
            } else {
                $('#checkcompany').html('');
                $("#addNewCompanyBtn").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.save_customer = function () {
    var self = this;
    $(document).on('click', '#addNewCompanyBtn', function () {
        var company_cust_id = document.getElementById('company_cust_id').value;
        var flag = self.validate_lead_data();
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var customerAddForm = $('#customerAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer',
                datatype: 'json',
                data: customerAddForm + '&action=add',
                beforeSend: function () {
                    if(company_cust_id != '' && company_cust_id != null && company_cust_id != undefined){
                        toastr["info"]('Updating Customer please wait....');
                    }else{
                        toastr["info"]('Creating Customer into the system....');
                    }
                    $('#customer_loader').html(createLoader('Please wait while data is being saved'));
                    $("#addNewCompanyBtn").attr("disabled", "disabled");
                },
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        var new_company_name = $('#companyNameForm').val();
                        $('#companyName').val(new_company_name);
                        $('#dd_cname').html(new_company_name);
                        self.lead_data.cust_id = stat.customer_id;
                        self.lead_data.site_id = stat.location.location_id;
                        self.lead_data.uuid = self.uuid();
                        $('#cust_id').val(stat.customer_id);
                        $('#site_id').val(stat.location.location_id);
                        //Check if it is Add Customer Or Edit Customer
                        if(company_cust_id != '' && company_cust_id != null && company_cust_id != undefined){
                            toastr["success"](stat.status);
                            $('#add_new_company_modal').modal('hide');
                            $("#addNewCompanyBtn").removeAttr("disabled");
                            $('#customer_loader').html('');
                        }else{
                            //Save Lead Details
                            setTimeout(function () {
                                self.save_lead_details();
                            }, 1500);
                        }
                        //Fetch System Item
                        setTimeout(function () {
                            $('#cust_id').val(stat.customer_id);
                            self.cust_id = stat.customer_id;
                            $('#site_id').val(stat.location.location_id);
                            $('input[name=customer_phone').val(stat.location.customer_contact_no);
                            $('input[name=customer_address').val(stat.location.address);
                            $('input[name=customer_postcode').val(stat.location.postcode).trigger('change');
                            $('#companyName').removeAttr('data-item');
                            $('#companyName').attr('data-item', JSON.stringify(stat.location));
                            self.fetch_system_item();
                        }, 2000);
                    }else{
                       toastr["error"](stat.status);
                       $("#addNewCompanyBtn").removeAttr("disabled");
                       $('#customer_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#addNewCompanyBtn").removeAttr("disabled");
                    $('#customer_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};

proposal_calculator.prototype.save_proposal_validate = function () {
    var self = this;
    var flag = true;
    $('#prd_panel').removeClass('is-invalid');
    //$('#prd_inverter').removeClass('is-invalid');
    $('#system_size').removeClass('is-invalid');
    $('#companyName').removeClass('is-invalid');
    $('#customer_postcode').removeClass('is-invalid');
    var cust_id = $('#cust_id').val();
    var site_id = $('#site_id').val();
    if (self.prd_panel_price == 0 || self.prd_panel_price == null) {
        flag = false;
        $('#prd_panel').addClass('is-invalid');
    }
    if (self.prd_inverter_price == 0 || self.prd_inverter_price == null) {
        //flag = false;
        //$('#prd_inverter').addClass('is-invalid');
    }
    if (self.system_size == 0 || self.system_size == null) {
        flag = false;
        $('#system_size').addClass('is-invalid');
    }
    if (cust_id == '' || site_id == '') {
        flag = false;
        $('#companyName').addClass('is-invalid');
        $('#customer_postcode').addClass('is-invalid');
    }

    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
    }
    return flag;
};

proposal_calculator.prototype.load_proposal = function () {
    var self = this;
    var uuid = self.proposal_id;
    var form_data = 'uuid=' + uuid + '&action=load';
    $('#loader').html(createLoader('Please wait while data is being loaded'));
    $.ajax({
        url: base_url + 'admin/proposal/load',
        type: 'post',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            $.isLoading("hide");
            $('#loader').html('');
            $('#save_proposal').removeAttr('disabled');
            if (response.success == true) {
                self.cust_id = response.proposal.cust_id;
                self.fetch_system_item(response);
            }
        }
    });
};

proposal_calculator.prototype.handle_proposal_data = function (data) {
    var self = this;
    //handle Customer Data
    $('#cust_id').val(data.proposal.cust_id);
    var agent_id = (data.proposal.agent_id != '' && data.proposal.agent_id != null) ? data.proposal.agent_id : '';
    $('#agent_id').val(agent_id);
    $('#site_id').val(data.proposal.site_id);
    $('#companyName').val(data.proposal.first_name + ' ' + data.proposal.last_name);
    $('#dd_cname').html(data.proposal.first_name + ' ' + data.proposal.last_name);
    $('input[name=customer_phone').val(data.proposal.customer_contact_no);
    $('input[name=customer_address').val(data.proposal.address);
    $('input[name=customer_postcode').val(data.proposal.postcode).trigger('change');
    //Convert Add New Customer to Edit customer
    $('#addNewCompany').addClass('hidden');
    $('#editCompany').removeClass('hidden');
    $('#companyName').attr('data-item', JSON.stringify(data.proposal));

    $('.placecomplete').val(data.proposal.address).change();

    //handle Image
    if (data.proposal.image != '') {
        /**document.getElementById('mapping_tool').setAttribute('style', 'display:none !important;');
         document.getElementById('mapping_tool_image').setAttribute('style', 'display:block !important;');
         $('.add-picture').css({'background': 'url("' + data.proposal.image + '") no-repeat',
         'background-size': '100% 100%',
         'height': '100%'
         });*/
        self.proposal_image = data.proposal.image;
        $('#image').val(self.proposal_image);
        self.show_image();
    }
    self.stored_custom_quote_total_inGst = parseFloat(data.proposal.quote_total_inGst).toFixed(2);
    //$('#system_size').val(data.proposal.system_size).trigger('change');
    $('#no_of_stories').val(data.proposal.no_of_stories).trigger('change');
    $('#phase').val(data.proposal.phase).trigger('change');
    $('#comments').val(data.proposal.comments).trigger('change');
    if (data.proposal.deal_won_lost != '' && data.proposal.deal_won_lost != null) {
        self.deal_won_lost = data.proposal.deal_won_lost;
        $('.sr-deal_btn__' + data.proposal.deal_won_lost).click();
    }
    /**if (data.proposal.deal_stage != '' && data.proposal.deal_stage != null) {
        self.deal_stage = data.proposal.deal_stage;
        self.create_deal_stages();//Show Proceed To Job Management Button
        self.toogle_job_manage_btn();
    }

    if (data.proposal.lead_source != '' && data.proposal.lead_source != null) {
        $('#lead_source').val(data.proposal.lead_source);
    } else {
        $('#lead_source').val(data.proposal.deal_source);
    }*/
};

proposal_calculator.prototype.toogle_job_manage_btn = function () {
    var self = this;
    if (self.deal_stage == '7' || self.deal_won_lost == 'won') {
        $('#proceed_to_job_management').removeClass('hidden');
    }
}

proposal_calculator.prototype.handle_proposal_item_data = function (data) {
    var self = this;
    //Add Panel,Inverter,Battery and additional data
    var proposal_data = data.proposal_data;
    for (var i = 0; i < proposal_data.length; i++) {
        if (proposal_data[i]['type_id'] == '2' && proposal_data[i]['item_type'] == '1') { //Panel
            $('#prd_panel').val(proposal_data[i]['item_id']).trigger('change');
            //self.prd_panel = proposal_data[i]['item_id'];
        } else if (proposal_data[i]['type_id'] == '1' && proposal_data[i]['item_type'] == '1') { //Inverter
            //self.prd_inverter = proposal_data[i]['item_id'];
            $('#prd_inverter').val(proposal_data[i]['item_id']).trigger('change');
        } else if (proposal_data[i]['type_id'] == '3' && proposal_data[i]['item_type'] == '1') { //Battery
            //self.prd_battery = proposal_data[i]['item_id'];
            $('#prd_battery').val(proposal_data[i]['item_id']).trigger('change');
        } else {
            self.create_additional_items(proposal_data[i]);
        }
    }

    var rt_nfp = data.proposal.roof_type_no_of_panels.split(';');
    $('.roof_type_no_of_panels').each(function (index) {
        $(this).val(rt_nfp[index])
    });


    var proposal_line_item_data = data.proposal_line_item_data;
    self.line_item_data = proposal_line_item_data;
    for (var i = 0; i < proposal_line_item_data.length; i++) {
        self.create_line_items(proposal_line_item_data[i]);
    }
    self.calculate_racking_price();
    self.fetch_line_item_data();
};

proposal_calculator.prototype.calculate_racking_price = function () {
    var self = this;
    var prd_panel = document.getElementById('prd_panel');
    if (prd_panel.value == '' || prd_panel.value == null
            || prd_panel.value == 'Please Fill Customer Details First' || prd_panel.value == 'Select Panel') {
        return false;
    }
    $.ajax({
        url: base_url + 'admin/product/fetch_racking_item_data',
        type: 'post',
        data: $('form').serialize() + '&action=fetch_racking_item_data',
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                if (response.racking_data.length > 0) {
                    var racking_price = 0;
                    for (var i = 0; i < response.racking_data.length; i++) {
                        var data = response.racking_data[i];
                        var is_tile_tin = data.name.search("Tin/Tile");
                        var is_tilt = data.name.search("Tilt");
                        var roof_types = document.getElementsByClassName('roof_type_no_of_panels');
                        var tile_tin_nfp = parseInt(roof_types[0].value) + parseInt(roof_types[1].value);
                        var tilt_nfp = parseInt(roof_types[2].value);

                        if (is_tile_tin != -1) {
                            racking_price = parseFloat(racking_price) + parseFloat(data.variant_cp) * tile_tin_nfp;
                        } else if (is_tilt != -1) {
                            racking_price = parseFloat(racking_price) + (parseFloat(data.variant_cp) * tilt_nfp);
                        }
                    }

                    self.racking_price = (racking_price && !isNaN(racking_price)) ? racking_price.toFixed(2) : parseFloat(0).toFixed(2);
                    self.calculate_sum();
                    self.create_quote_total();
                } else {
                    self.racking_price = parseFloat(0).toFixed(2);
                    self.calculate_sum();
                    self.create_quote_total();
                }
            }
            setTimeout(function () {
                if (self.stored_custom_quote_total_inGst > 0 && isFinite(self.stored_custom_quote_total_inGst)) {
                    self.custom_quote_total_inGst = parseFloat(self.stored_custom_quote_total_inGst).toFixed(2);
                    $('#custom_quote_total_inGst').val(self.stored_custom_quote_total_inGst);
                    self.calculate_sum();
                    self.create_quote_total();
                    self.stored_custom_quote_total_inGst = 0;
                }
            }, 1000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.save_proposal = function (create_pdf) {
    var self = this;
    self.calculate_sum();
    self.create_quote_total();
    var flag = self.save_proposal_validate();
    if (flag) {
        var prd_panel = document.getElementById('prd_panel');
        var prd_panel_data = null;
        if (prd_panel.value != '' && prd_panel.value != undefined) {
            prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
        }
        var panel_variant_id = prd_panel_data.variant_id;

        var prd_inverter = document.getElementById('prd_inverter');
        var prd_inverter_data = null
        if (prd_inverter.value != '' && prd_inverter.value != undefined) {
            prd_inverter_data = JSON.parse(prd_inverter.options[prd_inverter.selectedIndex].getAttribute('data-item'));
        }
        var inverter_variant_id = (prd_inverter_data != null) ? prd_inverter_data.variant_id : '';

        var prd_battery = document.getElementById('prd_battery');
        var prd_battery_data = null;
        if (prd_battery.value != '' && prd_battery.value != undefined) {
            prd_battery_data = JSON.parse(prd_battery.options[prd_battery.selectedIndex].getAttribute('data-item'));
        }
        var battery_variant_id = (prd_battery_data != null) ? prd_battery_data.variant_id : '';

        $('#save_proposal').attr('disabled', 'disabled');
        var uuid = self.proposal_id;
        var lead_id = self.lead_id;
        var form_data = $('form').serialize() + '&lead_id=' + lead_id + '&uuid=' + uuid + '&action=save&panel_variant_id=' + panel_variant_id + '&inverter_variant_id=' + inverter_variant_id + '&battery_variant_id=' + battery_variant_id;
        $('#loader').html(createLoader('Please wait while data is being saved'));
        
        $.ajax({
            url: base_url + 'admin/proposal/save',
            type: 'post',
            data: form_data,
            dataType: 'json',
            success: function (response) {
                $('#loader').html('');
                $('#save_proposal').removeAttr('disabled');
                if (response.success == true) {
                    toastr["success"]('Proposal Saved Successfully.');

                    if (self.prev_deal_won_lost != self.deal_won_lost) {
                        self.prev_deal_won_lost = self.deal_won_lost;
                        var date = moment(new Date()).format("DD-MM-YYYY");
                        var activity_data = "activity_action=Added Activity&activity[activity_type]=Note&activity[activity_note]=Job " + self.deal_won_lost + " on " + date;
                        window.activity_manager_tool.save_activity_handler(activity_data);
                    }
                    if (self.prev_deal_stage != '' && (self.prev_deal_stage != self.deal_stage)) {
                        self.prev_deal_stage = self.deal_stage;
                        var new_stage_name = self.deal_stage_name;
                        var prev_stage_name = (self.deal_stage) ? self.prev_deal_stage_name : 'None';
                        var date = moment(new Date()).format("DD-MM-YYYY");
                        var activity_data = "activity_action=Stage Change&activity[activity_type]=Note&activity[activity_note]=Changed from " + prev_stage_name + " to " + new_stage_name + " on " + date;
                        self.update_deal_stage(activity_data);
                    }
                    self.toogle_job_manage_btn();
                    if (create_pdf) {
                        self.create_pdf();
                    }
                } else {
                    toastr["error"]('Error! Saving the proposal.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

proposal_calculator.prototype.fetch_postcode_rating = function (postcode) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/fetch_postcode',
        type: 'get',
        data: {
            postcode: postcode
        },
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.postcode_rating = response.postcode.rating;
                if (self.system_size < 100) {
                    var dt = new Date();
                    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 12;
                    var system_size = (self.system_size) ? self.system_size : 1;
                    var stc_price_assumption = (self.stc_price_assumption) ? self.stc_price_assumption : 1;
                    var no_of_stc = (parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size));
                    no_of_stc = Math.floor(no_of_stc);
                    self.stc_deduction = (parseFloat(no_of_stc) * parseFloat(stc_price_assumption)).toFixed(2);
                    self.calculate_sum();
                    self.create_quote_total();
                }
            } else {
                toastr["error"]('Error! While Fetching Postcode Rating.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.fetch_system_item = function (data) {
    var self = this;
    var cust_id = self.cust_id;
    $.ajax({
        url: base_url + 'admin/product/fetch_item_data',
        type: 'get',
        data: {
            cust_id: cust_id,
            action: 'fetch_item_data'
        },
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.handle_item_data(response.inverter, 1, 'prd_inverter');
                self.handle_item_data(response.panel, 2, 'prd_panel');
                self.handle_item_data(response.battery, 3, 'prd_battery');
                if (data) {
                    self.handle_proposal_data(data);
                    self.handle_proposal_item_data(data);
                }
            } else {
                toastr["error"]('Error! While fetching product data.');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.handle_item_data = function (data, item_type, item_container) {
    var self = this;
    var option;
    item_container = document.getElementById(item_container);
    item_container.innerHTML = '';
    if (data.length > 0) {
        var item_name = (item_type == 1) ? 'Inverter' : (item_type == 2) ? 'Panel' : 'Battery';
        option = document.createElement('option');
        option.innerHTML = 'Select ' + item_name;
        item_container.appendChild(option);
        for (var i = 0; i < data.length; i++) {
            option = self.create_product_select_option(data[i]);
            item_container.appendChild(option);
        }
    } else {
        option = document.createElement('option');
        option.innerHTML = 'No Item Found with choosen Location';
        item_container.appendChild(option);
    }
};

proposal_calculator.prototype.fetch_products = function () {
    var self = this;
    var product_type_selector = document.getElementById(self.product_type_selector);
    product_type_selector.addEventListener("change", function () {
        var cust_id = self.cust_id;
        $.ajax({
            url: base_url + 'admin/product/fetch_additional_item_data',
            type: 'get',
            data: {
                type_id: this.value,
                cust_id: cust_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    var option;
                    document.getElementById('additional_product').innerHTML = '';
                    if (response.products.length > 0) {
                        for (var i = 0; i < response.products.length; i++) {
                            option = self.create_product_select_option(response.products[i]);
                            document.getElementById('additional_product').appendChild(option);
                        }
                    } else {
                        option = document.createElement('option');
                        option.innerHTML = 'No Item Found with Selected Type and Choosen Location';
                        document.getElementById('additional_product').appendChild(option);
                    }
                } else {
                    toastr["error"]('Error! While fetching product data.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

proposal_calculator.prototype.fetch_line_item_data = function () {
    var self = this;

    //Quickly claculate no of panels that will be rquired.
    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }

    var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.size)) : 0;
    //Get System Size
    var system_size = self.calculate_system_size();
    system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);

    var no_of_panels = (system_size * 1000) / prd_panel_size;
    self.no_of_panels = Math.floor(no_of_panels);
    var no_of_panels = (self.no_of_panels > 0 && isFinite(self.no_of_panels)) ? self.no_of_panels : 0;
    if (no_of_panels > 0) {
        $.ajax({
            url: base_url + 'admin/service/fetch_line_item_and_markup',
            type: 'POST',
            data: {
                prd_panel_data: prd_panel_data,
                no_of_panels: no_of_panels,
                action: 'fetch_line_item_data'
            },
            dataType: 'json',
            success: function (response) {
                document.getElementById(self.line_items_wrapper).innerHTML = '';
                if (response.success == true) {
                    if (response.line_item_data.length > 0) {
                        self.line_item_data = response.line_item_data;
                        for (var i = 0; i < response.line_item_data.length; i++) {
                            self.create_line_items(response.line_item_data[i]);
                        }
                    }
                    self.company_markup_price = parseFloat(0).toFixed(3);
                    if (response.company_markup.length > 0) {
                        //(Select the Company Markup that matches the state and number of panels) 
                        //and add the additional margins for all the products
                        var company_markup_price = parseFloat(response.company_markup[0].price).toFixed(3);
                        self.company_markup_price = company_markup_price;
                    }
                    self.calculate_sum();
                    self.create_quote_total();
                } else {
                    toastr["error"]('Error! While fetching line items data.');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

/** proposal_calculator.prototype.fetch_line_item_data = function () {
 var self = this;
 var line_item_selector = document.getElementById(self.line_item_selector);
 line_item_selector.addEventListener("change", function () {
 $.ajax({
 url: 'ajaxProducts.php',
 type: 'POST',
 data: {line_item_id: this.value},
 dataType: 'json',
 success: function (response) {
 if (response.success == true) {
 var option;
 document.getElementById('line_items_data').innerHTML = '';
 if (response.line_item_data.length > 0) {
 for (var i = 0; i < response.line_item_data.length; i++) {
 option = self.create_line_item_data_select_option(response.line_item_data[i]);
 document.getElementById('line_items_data').appendChild(option);
 }
 } else {
 option = document.createElement('option');
 option.innerHTML = 'No Line item Data Found';
 document.getElementById('line_items_data').appendChild(option);
 }
 } else {
 toastr["error"]('Error! While fetching line items data.');
 }
 }
 });
 });
 };*/

proposal_calculator.prototype.create_product_select_option = function (data, selected) {
    var option = document.createElement('option');
    var value = data.id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    if (data.id = selected) {
        option.setAttribute('selected', 'selected');
    }
    //option.innerHTML = data.name + ' [' + data.variant_state + '] ' + '[' + data.variant_application + ']';
    option.innerHTML = data.name;
    return option;
};

proposal_calculator.prototype.create_line_item_data_select_option = function (data) {
    var option = document.createElement('option');
    var value = data.id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    option.innerHTML = data.state + ' (' + data.qty_from + ' - ' + data.qty_to + ') [$' + data.price + ']';
    return option;
};

proposal_calculator.prototype.create_additional_items = function (data) {
    var self = this;
    var additional_cost_items_wrapper = document.getElementById(self.additional_cost_items_wrapper);
    var additional_cost_items_btn = document.getElementById(self.additional_cost_items_btn);
    if (!data) {
        additional_cost_items_btn.addEventListener("click", function (e) { //on add input button click
            var additional_product_type_name = $("#additional_product_type option:selected").html();
            var additional_product_data_item = $("#additional_product option:selected").attr('data-item');
            var additional_product_data = JSON.parse(additional_product_data_item);
            e.preventDefault();
            $(additional_cost_items_wrapper).append('<tr>' +
                    '<td>' + additional_product_type_name + '<input type="hidden" name="additional_cost_item[]"  value="' + additional_product_data.id + '" /></td>' +
                    '<td>' + additional_product_data.name + ' [' + additional_product_data.variant_state + '] ' + '[' + additional_product_data.variant_application + ']' + '<input type="hidden" name="additional_cost_item_variant[]" value="' + additional_product_data.variant_id + '" /></td>' +
                    '<td>$' + additional_product_data.variant_cp + '</td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_qty" name="additional_cost_item_qty[]" value="1" /></td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_price" name="additional_cost_item_price[]" value="' + additional_product_data.variant_cp + '" step="0.01" /></td>' +
                    "<td><input type='text' min='1' oninput='this.value = Math.abs(this.value)'  class='form-control additional_cost_item_total' name='additional_cost_item_total[]'   data-item='" + additional_product_data_item + "'  value='" + additional_product_data.variant_cp + "' readonly /></td>" +
                    '<td><a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            self.calculate_sum();
            self.create_quote_total();

            $('input[name="additional_cost_item_price[]"],input[name="additional_cost_item_qty[]"]').on('change', function () {
                $('.additional_cost_item_total').each(function () {
                    var price = $(this).closest('tr').find('.additional_cost_item_price').val();
                    var qty = $(this).closest('tr').find('.additional_cost_item_qty').val();
                    var total = (parseFloat(price) * parseFloat(qty)).toFixed(2);
                    $(this).val(total);
                });
                self.calculate_sum();
                self.create_quote_total();
            });
        });
    } else {
        var data_item = JSON.stringify(data);
        $(additional_cost_items_wrapper).append('<tr>' +
                '<td>' + data.typeName + '<input type="hidden" name="additional_cost_item[]" value="' + data.item_id + '" /></td>' +
                '<td>' + data.name + ' [' + data.variant_state + '] ' + '[' + data.variant_application + ']' + '<input type="hidden" name="additional_cost_item_variant[]" value="' + data.variant_id + '" /></td>' +
                '<td>$' + data.variant_cp + '</td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_qty" name="additional_cost_item_qty[]" value="' + data.qty + '" /></td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control additional_cost_item_price" name="additional_cost_item_price[]" value="' + data.custom_price + '" step="0.01" /></td>' +
                "<td><input type='text' min='1' oninput='this.value = Math.abs(this.value)'  class='form-control additional_cost_item_total' name='additional_cost_item_total[]'   data-item='" + data_item + "'  value='" + data.final_price + "' readonly /></td>" +
                '<td><a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');

        self.calculate_sum();
        self.create_quote_total();
        $('input[name="additional_cost_item_price[]"],input[name="additional_cost_item_qty[]"]').on('change', function () {
            $('.additional_cost_item_total').each(function () {
                var price = $(this).closest('tr').find('.additional_cost_item_price').val();
                var qty = $(this).closest('tr').find('.additional_cost_item_qty').val();
                var total = (parseFloat(price) * parseFloat(qty)).toFixed(2);
                $(this).val(total);
            });
            self.calculate_sum();
            self.create_quote_total();
        });
    }
};

proposal_calculator.prototype.remove_additional_items = function () {
    var self = this;
    var additional_cost_items_wrapper = document.getElementById(self.additional_cost_items_wrapper);
    $(additional_cost_items_wrapper).on("click", ".remove_field", function (e) { //user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
        self.calculate_sum();
        self.create_quote_total();
    });
};

//Line Items

proposal_calculator.prototype.create_line_items = function (data) {
    var self = this;
    var line_items_wrapper = document.getElementById(self.line_items_wrapper);
    var line_items_btn = document.getElementById(self.line_items_btn);
    if (!data) {
        line_items_btn.addEventListener("click", function (e) { //on add input button click
            var line_item_data = $("#line_items option:selected").attr('data-item');
            var line_item_data_data = $("#line_items_data option:selected").attr('data-item');
            line_item_data = JSON.parse(line_item_data);
            line_item_data_data = JSON.parse(line_item_data_data);
            e.preventDefault();
            $(line_items_wrapper).append('<tr>' +
                    '<td>' + line_item_data.line_item + '(' + line_item_data.per_which_product + ')<input type="hidden" name="line_item[]" value="' + line_item_data_data.id + '" /></td>' +
                    '<td>' + line_item_data_data.state + '</td>' +
                    '<td>' + line_item_data_data.qty_from + ' - ' + line_item_data_data.qty_to + '</td>' +
                    '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control" name="line_item_price[]" step="0.01" value="' + line_item_data_data.price + '" /></td>' +
                    '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            self.calculate_sum();
            self.create_quote_total();
            $('input[name="line_item_price[]"]').on('change', function () {
                self.calculate_sum();
                self.create_quote_total();
            });
        });
    } else {
        var price = (data.custom_price) ? data.custom_price : data.price;
        /**  If it is per System then the price will be multiplied by 1
         If it is per kilowatt then the price will be multiplied by the number of kilowatts
         If it is per panel then the price will be mulitplied by the number of pane
         */
        price = (data.per_which_product == 'Panel') ? (price * self.no_of_panels) : (data.per_which_product == 'kiloWatts') ? (price * self.system_size) : price;

        $(line_items_wrapper).append('<tr>' +
                '<td>' + data.line_item + '(' + data.per_which_product + ')<input type="hidden" name="line_item[]" value="' + data.id + '" /></td>' +
                '<td>' + data.state + '</td>' +
                '<td>' + data.qty_from + ' - ' + data.qty_to + '</td>' +
                '<td><input type="number" min="1" oninput="this.value = Math.abs(this.value)" class="form-control" name="line_item_price[]" step="0.01" value="' + price + '" readonly /></td>' +
                '<td><!--<a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a>--></td>');
        self.calculate_sum();
        self.create_quote_total();
        $('input[name="line_item_price[]"]').on('change', function () {
            self.calculate_sum();
            self.create_quote_total();
        });
    }
};

proposal_calculator.prototype.remove_line_items = function () {
    var self = this;
    var line_items_wrapper = document.getElementById(self.line_items_wrapper);
    $(line_items_wrapper).on("click", ".remove_lt_field", function (e) { //user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
        self.calculate_sum();
        self.create_quote_total();
    });
};

proposal_calculator.prototype.calculate_system_size = function () {
    var self = this;

    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }

    var prd_panel_watt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.size) : 0;
    //Get System Size
    var rt_price = 0;
    var rt_nfp = document.getElementsByClassName('roof_type_no_of_panels');
    for (var i = 0; i < rt_nfp.length; i++) {
        rt_price = parseFloat(rt_price) + parseFloat(rt_nfp[i].value);

    }
    rt_price = parseFloat(rt_price) * parseFloat(prd_panel_watt / 1000);

    var system_size = (rt_price == 0 || isNaN(rt_price)) ? parseFloat(0).toFixed(2) : parseFloat(rt_price).toFixed(2);

    self.system_size = system_size;
    document.getElementById('system_size').value = system_size;

    return system_size;
};

proposal_calculator.prototype.calculate_sum = function () {
    var self = this;
    var additional_margin = 0;

    //Get panel price
    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }

    //Get System Size
    var system_size = self.calculate_system_size();

    system_size = ((system_size) == 0 || isNaN(system_size)) ? 0 : parseFloat(system_size);

    var prd_panel_ips = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseInt(prd_panel_data.variant_ips) : 0;
    var prd_panel_amt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_amt) : 0;

    var prd_panel_price = 0;
    if (prd_panel_ips == 0) {
        prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.variant_cp) : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_panel_price = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_amt) : 0;
    }

    var prd_panel_size = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(Math.floor(prd_panel_data.size)) : 0;

    (prd_panel_data !== null && prd_panel_data !== undefined && (self.system_size == 0 || isNaN(self.system_size))) ? $('#prd_panel_help').slideDown() : $('#prd_panel_help').slideUp();
    var no_of_panels = (system_size * 1000) / prd_panel_size;
    document.getElementById('no_of_panels').innerHTML = isFinite(Math.floor(no_of_panels)) ? "<i class='fa fa-info-circle'></i> " + Math.floor(no_of_panels) + " panels will be required." : "";
    self.no_of_panels = Math.floor(no_of_panels);
    var final_panel_price = self.no_of_panels * prd_panel_price; //Multiply 1000 because system size is in w and panel in kW
    self.prd_panel_price = (final_panel_price && isFinite(final_panel_price)) ? parseFloat(final_panel_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(additional_margin) + (parseFloat(self.no_of_panels) * parseFloat(prd_panel_data.variant_am)) : parseFloat(0).toFixed(2);

    //Get Inverter Price
    var prd_inverter = document.getElementById('prd_inverter');
    var prd_inverter_data = null;
    if (prd_inverter.value != '' && prd_inverter.value != undefined) {
        prd_inverter_data = JSON.parse(prd_inverter.options[prd_inverter.selectedIndex].getAttribute('data-item'));
    }
    var prd_inverter_ips = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseInt(prd_inverter_data.variant_ips) : 0;
    var prd_inverter_amt = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_data.variant_amt) : 0;
    var prd_inverter_price = 0;
    if (prd_inverter_ips == 0) {
        prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? prd_inverter_data.variant_cp : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_inverter_price = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(prd_inverter_amt) : 0;
    }
    self.prd_inverter_price = (prd_inverter_price && isFinite(prd_inverter_price)) ? parseFloat(prd_inverter_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_inverter_data !== null && prd_inverter_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_inverter_data.variant_am) : parseFloat(0).toFixed(2);

    //Get battery price
    var prd_battery = document.getElementById('prd_battery');
    var prd_battery_data = null;
    if (prd_battery.value != '' && prd_battery.value != undefined) {
        prd_battery_data = JSON.parse(prd_battery.options[prd_battery.selectedIndex].getAttribute('data-item'));
    }
    var prd_battery_ips = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseInt(prd_battery_data.variant_ips) : 0;
    var prd_battery_amt = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_data.variant_amt) : 0;

    var prd_battery_price = 0;
    if (prd_battery_ips == 0) {
        prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? prd_battery_data.variant_cp : 0;
    } else {
        //Take amount value if Is Amount Set to Yes(1)
        prd_battery_price = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(prd_battery_amt) : 0;
    }
    self.prd_battery_price = (prd_battery_price && isFinite(prd_battery_price)) ? parseFloat(prd_battery_price).toFixed(2) : parseFloat(0).toFixed(2);
    additional_margin = (prd_battery_data !== null && prd_battery_data !== undefined) ? parseFloat(additional_margin) + parseFloat(prd_battery_data.variant_am) : parseFloat(0).toFixed(2);

    //Get Additional item_price
    var additional_cost_item = document.getElementsByName("additional_cost_item_total[]");
    var additional_cost_item_price = 0;

    for (var i = 0; i < additional_cost_item.length; i++) {
        var additional_item_data = additional_cost_item[i].getAttribute('data-item');
        additional_item_data = JSON.parse(additional_item_data);
        var additional_item_ips = (additional_item_data !== null && additional_item_data !== undefined) ? parseInt(additional_item_data.variant_ips) : 0;
        var additional_item_amt = (additional_item_data !== null && additional_item_data !== undefined) ? parseFloat(additional_item_data.variant_amt) : 0;

        if (additional_item_ips == 0) {
            additional_cost_item_price += parseFloat(additional_cost_item[i].value);
        } else {
            additional_cost_item_price += parseFloat(additional_item_amt);
        }

        additional_margin = (additional_cost_item[i] !== null && additional_cost_item[i] !== undefined) ? parseFloat(additional_margin) + parseFloat(additional_item_data.variant_am) : parseFloat(0).toFixed(2);
    }

    additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);

    self.additional_cost_item_price = (additional_cost_item_price && isFinite(additional_cost_item_price)) ? parseFloat(additional_cost_item_price).toFixed(2) : parseFloat(0).toFixed(2);
    self.additional_margin = (additional_margin && additional_margin != 0 && isFinite(additional_margin)) ? parseFloat(additional_margin).toFixed(2) : parseFloat(0).toFixed(2);

    //Get Line item_price
    var line_item = document.getElementsByName("line_item_price[]");
    var line_item_price = 0;

    for (var i = 0; i < line_item.length; i++) {
        no_of_panels = (no_of_panels && no_of_panels != 0 && isFinite(no_of_panels)) ? self.no_of_panels : 0;
        var current_item_price = parseFloat(line_item[i].value);
        line_item_price += current_item_price;
    }

    line_item_price = (line_item_price && isFinite(line_item_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
            parseFloat(line_item_price).toFixed(2) : parseFloat(0).toFixed(2);

    self.line_item_price = parseFloat(line_item_price).toFixed(2);

    //Get Racking Price
    var racking_price = (self.prd_panel_price > 0 && isFinite(prd_panel_price) && self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ?
            parseFloat(self.racking_price) : parseFloat(0).toFixed(2);

    var kliplock_price = (no_of_panels > 0) ? 2 * self.no_of_panels * parseFloat(3.90).toFixed(2) : parseFloat(0).toFixed(2);
    self.kliplock_price = parseFloat(kliplock_price).toFixed(2);

    //Calcaulte System Total
    var system_total = parseFloat(self.prd_panel_price) + parseFloat(self.prd_inverter_price) +
            parseFloat(self.prd_battery_price) + parseFloat(racking_price);

    /**console.log("Panels: " + self.prd_panel_price);
     console.log("Inverter: " + self.prd_inverter_price);
     console.log("Battery: " + self.prd_battery_price);
     console.log("Racking: " + racking_price);
     console.log("Additional: " + self.additional_cost_item_price);
     */
    var total_cost_before_markup = parseFloat(system_total) + parseFloat(self.additional_cost_item_price) + parseFloat(self.line_item_price);

    //console.log(self.line_item_price);
    //console.log("Total Without Markup: " +total_cost_before_markup);

    var certificate_margin = parseFloat(self.company_markup_price);

    var company_markup = (total_cost_before_markup > 0 && isFinite(total_cost_before_markup) && certificate_margin > 0 && isFinite(certificate_margin)) ? ((total_cost_before_markup / certificate_margin) - total_cost_before_markup) : parseFloat(0).toFixed(2);

    company_markup = (self.additional_margin > 0 && isFinite(self.additional_margin)) ? parseFloat(company_markup) + parseFloat(self.additional_margin) : parseFloat(company_markup);


    //var company_markup = (self.system_size != 0 && !isNaN(self.system_size) && self.company_markup_price && isFinite(self.company_markup_price)) ? (parseFloat(self.company_markup_price) * no_of_panels) + parseFloat(self.additional_margin) : parseFloat(0).toFixed(2);

    self.company_markup = (company_markup && isFinite(company_markup)) ? parseFloat(company_markup).toFixed(3) : parseFloat(0).toFixed(2);

    var total_cost = parseFloat(total_cost_before_markup) + parseFloat(self.company_markup);

    self.system_total = (total_cost && isFinite(total_cost)) ? total_cost.toFixed(2) : parseFloat(0).toFixed(2);

    //Recalculate no of STC deduction
    var dt = new Date();
    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 12;
    var stc_price_assumption = (self.stc_price_assumption) ? self.stc_price_assumption : 33.50;
    var no_of_stc = (parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size));
    no_of_stc = Math.floor(no_of_stc);

    self.stc_deduction = (parseFloat(no_of_stc) * parseFloat(stc_price_assumption)).toFixed(2);

    self.total_cost = (self.stc_deduction && total_cost > 0.00) ? (total_cost - self.stc_deduction).toFixed(2) : (total_cost).toFixed(2);

    self.total_cost_incGst = (self.total_cost * 1.1).toFixed(2);

    //Maintain Original Values
    self.original.system_total = self.system_total;
    self.original.stc_deduction = self.stc_deduction;
    self.original.total_cost = self.total_cost;
    self.original.total_cost_incGst = self.total_cost_incGst;

    if (self.prev_total_cost != self.total_cost_incGst) {
        self.custom_quote_total_inGst = (self.total_cost_incGst && isFinite(self.total_cost_incGst)) ? parseFloat(self.total_cost_incGst).toFixed(2) : parseFloat(0).toFixed(2);
    }

    if (self.custom_quote_total_inGst != self.total_cost_incGst) {
        //Adjust system total and exGst
        var newexGst = (parseFloat(self.custom_quote_total_inGst) / 1.1).toFixed(2);
        self.total_cost = newexGst;
        total_cost = (self.stc_deduction) ? (parseFloat(newexGst) + parseFloat(self.stc_deduction)).toFixed(2) : newexGst;
        self.system_total = (total_cost && isFinite(total_cost)) ? parseFloat(total_cost).toFixed(2) : parseFloat(0).toFixed(2);
    }

    self.prev_system_total = (self.total_cost_incGst && isFinite(self.total_cost_incGst)) ? parseFloat(self.total_cost_incGst).toFixed(2) : parseFloat(0).toFixed(2);
    self.prev_total_cost = (self.total_cost_incGst && isFinite(self.total_cost_incGst)) ? parseFloat(self.total_cost_incGst).toFixed(2) : parseFloat(0).toFixed(2);

};

proposal_calculator.prototype.create_quote_total = function () {
    var self = this;
    //System Cost Row
    var system_total_row = document.createElement('tr');
    var system_total = document.createElement('td');
    system_total.innerHTML = 'System Total';
    var system_total_value = document.createElement('td');
    system_total_value.innerHTML = '<input type="hidden" class="form-control" name="proposal[system_total]" value="' + self.system_total + '" step="0.01" /><span class="total-price">$' + toCommas(self.system_total) + '</span>';
    system_total_row.appendChild(system_total);
    system_total_row.appendChild(system_total_value);

    //Additonalo Cost Row
    var additional_total_row = document.createElement('tr');
    var additional_total = document.createElement('td');
    additional_total.innerHTML = 'Additional Total';
    var additional_total_value = document.createElement('td');
    additional_total_value.innerHTML = '<span class="total-price">$' + toCommas(self.additional_cost_item_price) + '</span>';
    additional_total_row.appendChild(additional_total);
    additional_total_row.appendChild(additional_total_value);

    //STC Deduction
    var stc_deduction = (self.system_total > 0) ? self.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_row = document.createElement('tr');
    var stc_deduction_col = document.createElement('td');
    stc_deduction_col.innerHTML = 'STC Deduction';
    var stc_deduction_value = document.createElement('td');
    stc_deduction_value.innerHTML = '<input type="hidden" name="proposal[stc_deduction]" value="' + stc_deduction + '" /><span class="total-price" style="background: indianred;">$' + toCommas(stc_deduction) + '</span>';
    stc_deduction_row.appendChild(stc_deduction_col);
    stc_deduction_row.appendChild(stc_deduction_value);

    //Quote Total ex GST
    var quote_total_exGst_row = document.createElement('tr');
    var quote_total_exGst = document.createElement('td');
    quote_total_exGst.innerHTML = 'Quote Total (ex GST)';
    var quote_total_exGst_value = document.createElement('td');
    quote_total_exGst_value.innerHTML = '<input type="hidden" name="proposal[quote_total_exGst]" value="' + self.total_cost + '" /><span class="total-price">$' + toCommas(self.total_cost) + ' </span>';
    quote_total_exGst_row.appendChild(quote_total_exGst);
    quote_total_exGst_row.appendChild(quote_total_exGst_value);

    //Quote Total inc GST
    var quote_total_inGst_row = document.createElement('tr');
    var quote_total_inGst = document.createElement('td');
    quote_total_inGst.innerHTML = 'Quote Total (inc GST)';
    var quote_total_inGst_value = document.createElement('td');
    quote_total_inGst_value.innerHTML = '<span class="total-price">$' + toCommas(self.total_cost_incGst) + '</span>';
    quote_total_inGst_row.appendChild(quote_total_inGst);
    quote_total_inGst_row.appendChild(quote_total_inGst_value);

    //System Cost Row Custom Price
    var price = (self.custom_quote_total_inGst > 0 && isFinite(self.custom_quote_total_inGst)) ? parseFloat(self.custom_quote_total_inGst).toFixed(2) : self.total_cost_incGst;
    var custom_price_row = document.createElement('tr');
    var custom_price = document.createElement('td');
    custom_price.setAttribute('id', 'quote_total_text');
    custom_price.innerHTML = 'Quote Total (inc GST)';
    if (self.custom_quote_total_inGst != self.total_cost_incGst) {
        custom_price.innerHTML = 'Custom Quote Total (inc GST)';
    }
    var custom_price_input = document.createElement('td');
    custom_price_input.innerHTML = ' <span style="display:inline-flex"> <input type="number" name="proposal[quote_total_inGst]" class="form-control" value="' + price + '" step="0.01"  id="custom_quote_total_inGst" /><a style="margin-left:10px; margin-top: 10px;" href="javascript:void(0);" id="show_total_quote_original"><i class="fa fa-info-circle"></i></a></span>';
    custom_price_row.appendChild(custom_price);
    custom_price_row.appendChild(custom_price_input);

    document.getElementById('total_quote_wrapper').innerHTML = '';
    document.getElementById('total_quote_wrapper').appendChild(system_total_row);
    //document.getElementById('total_quote_wrapper').appendChild(additional_total_row);
    document.getElementById('total_quote_wrapper').appendChild(stc_deduction_row);
    document.getElementById('total_quote_wrapper').appendChild(quote_total_exGst_row);
    //document.getElementById('total_quote_wrapper').appendChild(quote_total_inGst_row);
    document.getElementById('total_quote_wrapper').appendChild(custom_price_row);
    self.create_quote_items();

    $('#custom_quote_total_inGst').change(function () {
        self.custom_quote_total_inGst = $(this).val();
        self.calculate_sum();
        self.create_quote_total();
    })

    self.create_quote_total_original();

    $('#show_total_quote_original').click(function () {
        $('#total_quote_original').slideToggle();
    });

    //Set Deal Details
    $('#dd_system_size').html(self.system_size + ' kW');
    $('#dd_deal_value').html('$' + price);

    /** console.log("----------------");
     console.log("Panels: " + self.prd_panel_price);
     console.log("Inverter: " + self.prd_inverter_price);
     console.log("Battery: " + self.prd_battery_price);
     console.log("Racking: " + self.racking_price);
     console.log("KlipLock Price: " + self.kliplock_price);
     console.log("Additional: " + self.additional_cost_item_price);
     console.log("Company Markup: " + self.company_markup);
     console.log("System Total: " + self.total_cost);
     */
};

proposal_calculator.prototype.create_quote_total_original = function () {
    var self = this;
    var system_total_row_org = document.createElement('tr');
    var system_total_org = document.createElement('td');
    system_total_org.innerHTML = 'System Total';
    var system_total_value_org = document.createElement('td');
    system_total_value_org.innerHTML = '<span class="total-price">$' + self.original.system_total + '</span>';
    system_total_row_org.appendChild(system_total_org);
    system_total_row_org.appendChild(system_total_value_org);


    //STC Deduction
    var stc_deduction = (self.system_total > 0) ? self.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_org = (self.original.system_total > 0) ? self.original.stc_deduction : parseFloat(0).toFixed(2);
    var stc_deduction_row_org = document.createElement('tr');
    var stc_deduction_col_org = document.createElement('td');
    stc_deduction_col_org.innerHTML = 'STC Deduction';
    var stc_deduction_value_org = document.createElement('td');
    stc_deduction_value_org.innerHTML = '<span class="total-price" style="background: indianred;">$' + toCommas(stc_deduction) + '</span>';
    stc_deduction_row_org.appendChild(stc_deduction_col_org);
    stc_deduction_row_org.appendChild(stc_deduction_value_org);

    //Quote Total ex GST
    var quote_total_exGst_row_org = document.createElement('tr');
    var quote_total_exGst_org = document.createElement('td');
    quote_total_exGst_org.innerHTML = 'Quote Total (ex GST)';
    var quote_total_exGst_value_org = document.createElement('td');
    quote_total_exGst_value_org.innerHTML = '<span class="total-price">$' + toCommas(self.original.total_cost) + ' </span>';
    quote_total_exGst_row_org.appendChild(quote_total_exGst_org);
    quote_total_exGst_row_org.appendChild(quote_total_exGst_value_org);

    //Quote Total inc GST
    var quote_total_inGst_row_org = document.createElement('tr');
    var quote_total_inGst_org = document.createElement('td');
    quote_total_inGst_org.innerHTML = 'Quote Total (inc GST)';
    var quote_total_inGst_value_org = document.createElement('td');
    quote_total_inGst_value_org.innerHTML = '<span class="total-price">$' + toCommas(self.original.total_cost_incGst) + '</span>';
    quote_total_inGst_row_org.appendChild(quote_total_inGst_org);
    quote_total_inGst_row_org.appendChild(quote_total_inGst_value_org);

    var price = (self.custom_quote_total_inGst > 0 && isFinite(self.custom_quote_total_inGst)) ? parseFloat(self.custom_quote_total_inGst).toFixed(2) : self.total_cost_incGst;
    var gst_row = document.createElement('tr');
    var gst_col = document.createElement('td');
    gst_col.innerHTML = 'GST (Current)';
    var gst_col_value = document.createElement('td');
    var diff = parseFloat(price) - parseFloat(self.total_cost);
    diff = Math.floor(diff.toFixed(2));

    gst_col_value.innerHTML = '<span class="total-price">$ ' + toCommas(diff) + '</span>';
    gst_row.appendChild(gst_col);
    gst_row.appendChild(gst_col_value);

    var gst_row_org = document.createElement('tr');
    var gst_col_org = document.createElement('td');
    gst_col_org.innerHTML = 'GST (Orignal)';
    var gst_col_value_org = document.createElement('td');
    var diff1 = parseFloat(self.original.total_cost_incGst) - parseFloat(self.original.total_cost);
    diff1 = Math.floor(diff1.toFixed(2));

    gst_col_value_org.innerHTML = '<span class="total-price">$ ' + toCommas(diff1) + '</span>';
    gst_row_org.appendChild(gst_col_org);
    gst_row_org.appendChild(gst_col_value_org);

    document.getElementById('total_quote_original_wrapper').innerHTML = '';
    document.getElementById('total_quote_original_wrapper').appendChild(system_total_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(stc_deduction_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(quote_total_exGst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(quote_total_inGst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(gst_row_org);
    document.getElementById('total_quote_original_wrapper').appendChild(gst_row);

};

proposal_calculator.prototype.create_quote_item_row = function (name, price) {
    var row = document.createElement('tr');
    var column = document.createElement('td');
    column.innerHTML = name;
    var column_price = document.createElement('td');
    price = (price && price != '') ? '$' + price : '';
    column_price.innerHTML = toCommas(price) + '</span>';
    row.appendChild(column);
    row.appendChild(column_price);
    return row;
};

proposal_calculator.prototype.create_quote_items = function () {
    var self = this;
    if (self.user_type != '1' && self.user_type != '4') {
        document.getElementById('line_items_div').setAttribute('class', 'hidden');
        return false;
    }
    //System Item Price Row
    var panel_price_row = self.create_quote_item_row('Panels', self.prd_panel_price);
    var inverter_price_row = self.create_quote_item_row('Inverters', self.prd_inverter_price);
    var racking_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? self.racking_price : parseFloat(0).toFixed(2);
    var racking_price_row = self.create_quote_item_row('Racking', racking_price);
    var kliplock_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? self.kliplock_price : parseFloat(0).toFixed(2);
    var kliplock_price_row = self.create_quote_item_row('KlipLock', kliplock_price);
    var additional_items_price_row = self.create_quote_item_row('Additional Items', self.additional_cost_item_price);

    $('#quote_items').removeClass('hidden');
    document.getElementById('quote_items_wrapper').innerHTML = '';
    document.getElementById('quote_items_wrapper').appendChild(panel_price_row);
    document.getElementById('quote_items_wrapper').appendChild(inverter_price_row);
    document.getElementById('quote_items_wrapper').appendChild(racking_price_row);
    document.getElementById('quote_items_wrapper').appendChild(kliplock_price_row);
    document.getElementById('quote_items_wrapper').appendChild(additional_items_price_row);
    document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row('', ''));

    //Line Item Data
    var line_item_data = self.line_item_data;
    for (var i = 0; i < line_item_data.length; i++) {
        var line_item_price = parseFloat(line_item_data[i].price).toFixed(2);
        line_item_price = (line_item_data[i].per_which_product == 'Panel') ? (line_item_price * self.no_of_panels) : (line_item_data[i].per_which_product == 'kiloWatts') ? (line_item_price * self.system_size) : line_item_price;
        line_item_price = (self.prd_panel_price > 0 && isFinite(self.prd_panel_price)) ? parseFloat(line_item_price).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row(line_item_data[i].line_item, line_item_price));
    }

    //Total and Company Markup
    var total_price_row = self.create_quote_item_row('Total (exGst)', self.total_cost);
    var company_markup_price_row = self.create_quote_item_row('Company Markup', self.company_markup);
    document.getElementById('quote_items_wrapper').appendChild(self.create_quote_item_row('', ''));
    document.getElementById('quote_items_wrapper').appendChild(total_price_row);
    document.getElementById('quote_items_wrapper').appendChild(company_markup_price_row);
};

proposal_calculator.prototype.show_image = function () {
    var self = this;
    var image = self.proposal_image;

    if (image != '') {
        $('#proposal_no_image').attr('style', 'display:none !important;');
        $('#proposal_image').attr('style', 'display:block !important;');
        $('#proposal_image > .add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/proposal_files/' + image + '")');
        $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
        $('#proposal_image > .add-picture').css('background-position', '50% 50%');
        $('#proposal_image > .add-picture').css('background-size', '100% 100%');
    }
};

proposal_calculator.prototype.hide_image = function () {
    $('#proposal_image_close').click(function () {
        //document.getElementById('mapping_tool').setAttribute('style', 'display:block !important;');
        //document.getElementById('mapping_tool_image').setAttribute('style', 'display:none !important;');
        $('#proposal_no_image').attr('style', 'display:block !important;');
        $('#proposal_image').attr('style', 'display:none !important;');
        $('#proposal_image > .add-picture').css('background-image', '');
        $('#proposal_image > .add-picture').css('background-repeat', '');
        $('#proposal_image > .add-picture').css('background-position', '');
        $('#proposal_image > .add-picture').css('background-size', '');
    });
};

proposal_calculator.prototype.upload_proposal_image = function () {
    var self = this;
    $('#proposal_image_upload').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'proposal_files');
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/proposal/upload_image', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="proposal[image]"').val(res.file_name);
                    self.proposal_image = res.file_name;
                    self.show_image();
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

proposal_calculator.prototype.hanlde_deal_won_lost = function () {
    var self = this;
    $('.sr-deal_btn').click(function () {
        $('.sr-deal_btn').removeClass('sr-deal_btn--won');
        $('.sr-deal_btn').removeClass('sr-deal_btn--lost');
        var deal_value = $(this).attr('data-value');
        if (self.prev_deal_won_lost == deal_value) {
            self.prev_deal_won_lost = '';
            self.deal_won_lost = '';
            $('#proposal_deal_won_lost').val('');
            $(this).removeClass('sr-deal_btn--' + deal_value);
        } else {
            $('#proposal_deal_won_lost').val(deal_value);
            $(this).addClass('sr-deal_btn--' + deal_value);
        }
        self.prev_deal_won_lost = self.deal_won_lost;
        self.deal_won_lost = deal_value;
    });
};

proposal_calculator.prototype.create_deal_stages = function () {
    var self = this;
    var proposal_stages = JSON.parse(self.proposal_stages);
    var ul = document.createElement('ul');
    ul.className = "steps";
    for (var i = 0; i < proposal_stages.length; i++) {
        var li = document.createElement('li');
        if (self.deal_stage != '') {
            if (proposal_stages[i].id < self.deal_stage) {
                li.className = "done";
            } else if (proposal_stages[i].id == self.deal_stage) {
                li.className = "active";
                self.deal_stage_name = proposal_stages[i].stage_name;
            } else {
                li.className = "undone";
            }
        } else {
            if (i == 0) {
                li.className = "active";
            } else {
                li.className = "undone";
            }
        }
        li.innerHTML = '<a href="javascript:void(0);" class="deal_stage" data-id="' + proposal_stages[i].id + '" data-name="' + proposal_stages[i].stage_name + '">' + proposal_stages[i].stage_name + '</a>';
        ul.appendChild(li);
    }
    if (self.deal_stage == '') {
        self.deal_stage = proposal_stages[0].id;
        self.deal_stage_name = proposal_stages[0].stage_name;
        document.getElementById('proposal_deal_stage').value = proposal_stages[0].id;
    }

    document.getElementById('deal_stages').innerHTML = '';
    document.getElementById('deal_stages').appendChild(ul);
    self.change_deal_stage();
};

proposal_calculator.prototype.change_deal_stage = function () {
    var self = this;
    $('.deal_stage').click(function () {
        $('#deal_stage_help').show();
        var id = $(this).attr('data-id');
        self.prev_deal_stage_name = self.deal_stage_name;
        self.deal_stage_name = $(this).attr('data-name');
        self.prev_deal_stage = self.deal_stage;
        self.deal_stage = id;
        document.getElementById('proposal_deal_stage').value = id;
        self.create_deal_stages();
    });
};

proposal_calculator.prototype.validate_lead_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('#locationPostCode').val();
    var locationState = $('#locationState').val();
    var locationstreetAddress = $('#locationstreetAddressVal').val();
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var contactMobilePhone = $('#contactMobilePhone').val();
    var businessId = $('#businessId').val();
    $('#companyNameForm').val(firstname + ' ' + lastname);
    var companyNameForm = $('#companyNameForm').val();
    var customer_email = $('#contactEmailId').val();

    //Remove invalid
    $('#locationPostCode').removeClass('is-invalid');
    $('#locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');
    $('#contactMobilePhone').removeClass('is-invalid');
    $('#firstname').removeClass('is-invalid');
    $('#companyNameForm').removeClass('is-invalid');
    $('#businessId').removeClass('is-invalid');
    $('#contactEmailId').removeClass('is-invalid');

    if (locationPostCode == '') {
        flag = false;
        $('#locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('#locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }
    if (companyNameForm == '') {
        flag = false;
        $('#companyNameForm').addClass('is-invalid');
    }
    if (contactMobilePhone == '') {
        flag = false;
        $('#contactMobilePhone').addClass('is-invalid');
    }
    if (firstname == '') {
        flag = false;
        $('#firstname').addClass('is-invalid');
    }
    if (businessId == '') {
        flag = false;
        $('#businessId').addClass('is-invalid');
    }
    if (customer_email == '') {
        flag = false;
        $('#contactEmailId').addClass('is-invalid');
    }

    return flag;
};

proposal_calculator.prototype.fetch_franchise_list = function (userid) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/franchise_auto_assign',
        type: 'get',
        data: {postcode: self.postcode,userid:userid},
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.autopopulate_franchise_list(response, userid);
            }
        }
    });
};

proposal_calculator.prototype.autopopulate_franchise_list = function (data, selected) {
    var self = this;
    var option;
    var franchise_select = document.getElementById('businessId');
    franchise_select.innerHTML = '';
    if (data.franchise_list.length > 0) {
        var franchise_list = data.franchise_list;
        for (var i = 0; i < franchise_list.length; i++) {
            option = document.createElement('option');
            option.setAttribute('value', data.franchise_list[i].user_id);
            option.innerHTML = data.franchise_list[i].full_name + ' (' + data.franchise_list[i].email + ')';
            if (selected == data.franchise_list[i].user_id) {
                option.setAttribute('selected', 'selected');
            }
            if ((selected == '' || selected == null) && data.selected_franchise.length > 0) {
                if (data.selected_franchise[0].user_id == data.franchise_list[i].user_id) {
                    option.setAttribute('selected', 'selected');
                }
            }
            option.setAttribute('data-item', JSON.stringify(data.franchise_list[i]));
            franchise_select.appendChild(option);
        }
    } else {
        var default_franchise = data.default_franchise;
        option = document.createElement('option');
        option.setAttribute('value', default_franchise.user_id);
        option.innerHTML = default_franchise.full_name + ' (' + default_franchise.email + ')';
        option.setAttribute('selected', 'selected');
        option.setAttribute('data-item', JSON.stringify(default_franchise));
        franchise_select.appendChild(option);
    }
    $('#businessId').trigger('change');
};

proposal_calculator.prototype.save_lead_details = function () {
    var self = this;

    if (self.lead_data.cust_id == '' || self.lead_data.cust_id == undefined) {
        toastr["error"]('Customer not Exists in the system');
        return false;
    }

    if (self.lead_data.uuid == '') {
        var uuid = self.uuid();
        self.lead_data.uuid = uuid;
    }
    
    var lead_source = document.getElementById('lead_source').value;
    var formData = 'lead[uuid]='+self.lead_data.uuid+'&lead[user_id]='+self.userid+'&lead[cust_id]='+self.lead_data.cust_id+'&lead[site_id]='+self.lead_data.site_id + '&lead[lead_source]='+lead_source;
     $.ajax({
        url: base_url + 'admin/lead/save_lead_details',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            //toastr["info"]('Creating Lead into the system....');
            $("#addNewCompanyBtn").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                self.lead_id = response.id;
                //Add Deal ref in url
                window.history.pushState(self.lead_data, '', '?deal_ref=' + self.lead_data.uuid);
                //toastr["success"](response.status);
                setTimeout(function () {
                    self.assign_franchise();
                }, 1000);
            } else {
                toastr["error"](response.status);
                $("#addNewCompanyBtn").removeAttr("disabled");
                $('#customer_loader').html('');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#addNewCompanyBtn").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.assign_franchise = function () {
    var self = this;
    var data = {};
    data.user_ids = [$('#businessId').find('option:selected').val()];
    data.lead_id = self.lead_id;
    data.follow = false;
    data.self_lead = true;
    $.ajax({
        url: base_url + 'admin/lead/assign_lead_to_franchise',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            //toastr["info"]('3. Assigning Lead to specified franchise....');
            $("#addNewCompanyBtn").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                $('#add_new_company_modal').modal('hide');
                toastr["success"]("Customer created succesfully with self lead.");
                window.activity_manager_tool.fetch_activity_data();
                $('#customer_loader').html('');
            } else {
                toastr["error"](response.status);
                $('#customer_loader').html('');
            }
            $("#addNewCompanyBtn").removeAttr("disabled");
            $('#customer_loader').html('');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#addNewCompanyBtn").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

proposal_calculator.prototype.update_deal_stage = function (activity_data) {
    var self = this;
    
    var data = {};
    data.deal_stage = self.deal_stage;
    data.uuid = self.lead_data.uuid;

    $.ajax({
        url: base_url + "admin/lead/update_lead_stage",
        type: 'post',
        dataType: "json",
        data: data,
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                window.activity_manager_tool.save_activity_handler(activity_data);
                $('#deal_stage_help').hide();
            } else {
                toastr["error"](data.status);
            }
           
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
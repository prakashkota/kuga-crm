<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;}
    .mask{    margin-left: -15px;}
    #proposal_image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 30px;
        right: 2px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }


    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #000 !important;
        opacity: 1; /* Firefox */
    }

    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }

    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }

    .remove_field{
        color:red;
    }

    .monthly-event-list{
        top:22.5em !important ;
        height:calc(100% - 25.5em) !important;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }
    .steps li {
        display: inline-block;
        margin-bottom: 3px;
    }
    .steps li a, .steps li p {
        background: #e5f4fd;
        padding: 8px 20px;
        color: #0077bf;
        display: block;
        font-size: 14px;
        font-weight: bold;
        position: relative;
        text-indent: 12px;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li a:before, .steps li p:before {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #fff;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        left: 0;
        top: 50%;
        width: 0;
        margin-top: -18px;
    }
    .steps li a:after, .steps li p:after {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #e5f4fd;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        /*right: -12px;*/
        left:100%;
        top: 50%;
        width: 0;
        margin-top: -18px;
        z-index: 1;
    }
    .steps li.active a, .steps li.active p {
        background: #5cb85c;
        color: #fff;
    }
    .steps li.active a:after, .steps li.active p:after {
        border-left: 12px solid #5cb85c;
    }
    .steps li.undone a, .steps li.undone p {
        background: #eee;
        color: #333;
    }
    .steps li.undone a:after, .steps li.undone p:after {
        border-left: 12px solid #eee;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }

    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }

    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }

    .sr-no_wrap{
        white-space: nowrap;
    }

    .sr-inactive_user{
        color:red;
    }

    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    .card_body__title h2{
        display:inline-flex;
        padding-bottom: 10px;
    }

    .form-block{margin-bottom: 0px !important;}

    .kg-activity{
        border-left-color: #f0ad4e;
        border: 1px solid #f0ad4e;
        border-left-width: .25rem;
        border-radius: .25rem;
    }

    .kg-activity__description{
        background-color:#ffffe0;
        margin: -12px;
        padding: 12px;
    }

    .card_row_1{
        min-height: 429px;
    }

    .sr-switch {
        font-size: 1rem;
        position: relative;
    }
    .sr-switch input {
        position: absolute;
        height: 1px;
        width: 1px;
        background: none;
        border: 0;
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        overflow: hidden;
        padding: 0;
    }
    .sr-switch input + label {
        position: relative;
        min-width: calc(calc(2.375rem * .8) * 2);
        border-radius: calc(2.375rem * .8);
        height: calc(2.375rem * .8);
        line-height: calc(2.375rem * .8);
        display: inline-block;
        cursor: pointer;
        outline: none;
        user-select: none;
        vertical-align: middle;
        text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
    }
    .sr-switch input + label::before,
    .sr-switch input + label::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: calc(calc(2.375rem * .8) * 2);
        bottom: 0;
        display: block;
    }
    .sr-switch input + label::before {
        right: 0;
        background-color: #dee2e6;
        border-radius: calc(2.375rem * .8);
        transition: 0.2s all;
    }
    .sr-switch input + label::after {
        top: 2px;
        left: 2px;
        width: calc(calc(2.375rem * .8) - calc(2px * 2));
        height: calc(calc(2.375rem * .8) - calc(2px * 2));
        border-radius: 50%;
        background-color: white;
        transition: 0.2s all;
    }
    .sr-switch input:checked + label::before {
        background-color: #08d;
    }
    .sr-switch input:checked + label::after {
        margin-left: calc(2.375rem * .8);
    }
    .switch input:focus + label::before {
        outline: none;
        box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
    }
    .sr-switch input:disabled + label {
        color: #868e96;
        cursor: not-allowed;
    }
    .sr-switch input:disabled + label::before {
        background-color: #e9ecef;
    }
    .switch.switch-sm {
        font-size: 0.875rem;
    }
    .sr-switch.switch-sm input + label {
        min-width: calc(calc(1.9375rem * .8) * 2);
        height: calc(1.9375rem * .8);
        line-height: calc(1.9375rem * .8);
        text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
    }
    .sr-switch.switch-sm input + label::before {
        width: calc(calc(1.9375rem * .8) * 2);
    }
    .sr-switch.switch-sm input + label::after {
        width: calc(calc(1.9375rem * .8) - calc(2px * 2));
        height: calc(calc(1.9375rem * .8) - calc(2px * 2));
    }
    .sr-switch.switch-sm input:checked + label::after {
        margin-left: calc(1.9375rem * .8);
    }
    .sr-switch + .sr-switch {
        margin-left: 1rem;
    }
    .tbl-as-inpt tr {
        width: 100%;
        display: block;
    }
   .tbl-as-inpt.table-default table tr td {
    display: block;
    width: 100%;
    padding: 0 !important;
    border: none;
    background: none;
    font-size: 15px;
}
.tbl-as-inpt.table-default table tr td .total-price {

    padding: 0;
    font-size: 12px;
    background-color: #ECEFF1;
    border: solid 1px #000;
    padding: 10px 8px;
    margin: 6px 0 15px 0;
    display: block;

}
    
    @media only screen and (min-width: 992px){
        .modal-content{width:900px;}
        .modal-dialog{max-width:800px;}
    }
    
    @media only screen and (max-width: 991px){
        .modal-content{width:360px;}
    }
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/lead/add?deal_ref=' . $lead_data['uuid']) ?>"><i class="icon-arrow-left"></i> Back to Deal </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Customer Details</h2>
                                            <a style="margin-left:20px;" id="customer_location_edit_btn" href="javascript:void(0);" ><i class="fa fa-pencil"></i> Edit Customer</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group" style="margin-bottom:10px;">
                                               <label>Site Name</label>
                                               <input type="text"  id="site_name" placeholder="Site Name" class="form-control gray-bg" readonly="">
                                           </div>
                                           <div class="form-group" style="margin-bottom:10px;">
                                            <label>Site Address</label>
                                            <input type="text"  id="site_address" placeholder="Address" class="form-control gray-bg" readonly="">
                                        </div>
                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label>Customer Name</label>
                                            <input type="text" id="customer_name" placeholder="Customer Name" class="form-control gray-bg" readonly>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label>Customer Contact Number</label>
                                            <input type="tel" id="customer_contact_no" placeholder="Cusotmer Contact No." class="form-control gray-bg" readonly>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label>Postcode</label>
                                            <input type="number" id="site_postcode" placeholder="Postcode" class="form-control gray-bg" >
                                            <small class="form-text text-danger" id="site_postcode_warning"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4">
                            <div class="card card_row_1">
                                <div class="card-body">
                                    <div class="card_body__title">
                                        <h2>Proposal Details</h2>  
                                    </div>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default tbl-as-inpt">
                                            <table width="100%" border="0">
                                                <tbody id="proposal_cost_details_wrapper">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="btn-block text-center">
                                        <button id="manage_finance_proposal_btn" class="btn btn-primary hidden"><i class="fa fa-money"></i> Manage Finance</button>
                                        <a id="download_solar_proposal" href="javascript:void(0);" class="btn btn-primary" ><i class="fa fa-file-pdf-o"></i> Download Proposal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                         <div class="col-12 col-sm-12 col-md-4">
                            <div class="card card_row_1">
                                <div class="card-body">
                                    <div class="card_body__title">
                                        <h2>Financial Summary</h2>  
                                    </div>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default  tbl-as-inpt">
                                            <table width="100%" border="0">
                                                <tbody id="proposal_financial_summery">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-body"  id="mapping_tool_actions" style="display: none;">
                            <div class="map-leftpanel" style="overflow-x: hidden;" >
                                <div class="row">
                                    <div class="col-md-5">

                                        <div id="add-roof-panels-card" style=" text-align:center;">
                                            <div class="addBtn">
                                                <button data-toggle="tooltip" data-placement="bottom" title="Click this to draw section to add roof panel." type="button" class=" btn-danger draw-button" data-id="polygon" ><i class="fa fa-plus"></i></button>
                                            </div>
                                            <div id="remove-multi-roof-panels-card" class="addBtn dltLine" style="display:block;">
                                                <button data-container="body" disabled="disabled" data-toggle="tooltip" title="Click this to draw section to remove roof panel." data-placement="bottom" type="button" class=" btn-success remove-multi-button" data-id="polygon" ><i class="fa fa-trash"></i></button>
                                            </div>
                                            <p class="add-more">Add more roof section above</p>
                                        </div>
                                    </div>
                                    <div class="col-md-2"> 
                                        <div class="roof-panels-middle">Or</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="cardDiv" id="pac-card">
                                            <div>
                                                <div id="type-selector" class="pac-controls">
                                                    <input type="radio" name="type" id="changetype-all" checked="checked">
                                                    <label for="changetype-all">All</label>

                                                    <input type="radio" name="type" id="changetype-establishment">
                                                    <label for="changetype-establishment">Establishments</label>

                                                    <input type="radio" name="type" id="changetype-address">
                                                    <label for="changetype-address">Addresses</label>

                                                    <input type="radio" name="type" id="changetype-geocode">
                                                    <label for="changetype-geocode">Geocodes</label>
                                                </div>
                                                <div id="strict-bounds-selector" class="pac-controls">
                                                    <input type="checkbox" id="use-strict-bounds" value="">
                                                    <label for="use-strict-bounds">Strict Bounds</label>
                                                </div>
                                            </div>

                                            <div class="calculate-container">
                                                <input type="hidden" id="query-address" value="" />
                                                <button class="btn btn-calculate" type="button" data-toggle="modal" data-target="#calculate-modal" id="reset-all" style="display: block;">Calculate Now</button>
                                                <br/>
                                                <button class="btn btn-calculate" type="button" id="save_image_locally" style="display: block;">Create Image</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-group section-container" id="accordion"></div>         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="card"style="padding: 0;background: transparent;box-shadow: none;">
                        <div class="card-body" style="padding: 0;margin-bottom: 0px;">
                            <ul class="nav nav-tabs justify-content-left" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#tab_image_upload" id="tab_image_upload_btn" role="tab" data-toggle="tab">
                                    Upload Image</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab_near_map" id="tab_near_map_btn" role="tab" data-toggle="tab">Near Map</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab_mapping_tool" id="tab_mapping_tool_btn" role="tab" data-toggle="tab">Mapping Tool</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active show" id="tab_image_upload">
                                    <div class="form-block d-block clearfix">
                                        <div class="form-block d-block clearfix" id="proposal_no_image" >
                                            <div class="add-picture">
                                                <span>Add picture</span>
                                                <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                <input type="file"  id="proposal_image_upload" />
                                            </div>
                                        </div>
                                        <div class="form-block d-block clearfix" id="proposal_image" style="display:none !important; ">
                                            <div class="add-picture"></div>
                                            <a id="proposal_image_close" href="javascript:void(0);" ></a>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade in" id="tab_near_map">
                                    <?php echo $new_mapping_tool; ?>
                                </div>

                                <div role="tabpanel" class="tab-pane fade in" id="tab_mapping_tool">
                                    <div class="form-block d-block clearfix">
                                        <div class="form-block d-block clearfix"  id="mapping_tool">
                                            <?php echo $mapping_tool; ?>
                                        </div>
                                        <div class="form-block d-block clearfix" id="mapping_tool_image" style="display:none !important; ">
                                            <div class="add-picture"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card_body__title">
                                <h2>Customer Billing Information</h2>
                            </div>
                            <div class="form-block d-block clearfix">
                                <form role="form" action="" enctype="multipart/form-data" method="post" id="solar_energy_form">
                                    <input type="hidden" id="image" name="solar_proposal[image]" />
                                    <input type="hidden" name="solar_proposal[near_map_data]" id="near-map-exported-data"/>
                                    <input type="hidden" name="solar_proposal[total_system_size]" id="total_system_size">
                                    <input type="hidden" name="solar_panel_data[no_of_panels][0]" class="no_of_panels" >
                                    <input type="hidden" name="solar_proposal[total_panel_area]" id="total_panel_area" />
                                    <input type="hidden" name="solar_proposal[nearmap_toggle]" id="nearmap_toggle_val" />
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3" id="mtdt-cls">
                                            <div class="form-group control-group ">
                                                <label>Meter data?<small id="mtdt-msg" class="text-info"> (Avg daily consumption will be calculated automatically in case of custom meter data. Refresh the page once the file is uploaded.)</small></label>
                                                <br/>
                                                <span class="sr-switch">
                                                    <label for="is_meter_data">Yes</label>
                                                    <input type="checkbox" class="sr-switch" id="is_meter_data" checked="" />
                                                    <label for="is_meter_data">No</label>
                                                </span>
                                                <input type="hidden" name="solar_proposal[is_meter_data]" id="is_meter_data_val" />
                                                <script>
                                                    $(document).ready(function(){
                                                        if($('#is_meter_data').prop('checked')==false){
                                                            $('#mtdt-msg').show();
                                                            $('#mtdt-cls').removeClass('col-md-4').addClass('col-md-12');
                                                        }else{
                                                            $('#mtdt-msg').hide();
                                                            $('#mtdt-cls').removeClass('col-md-12').addClass('col-md-4');
                                                        }
                                                        $(document).on('change','#is_meter_data',function(){
                                                            var a = $(this).prop('checked');
                                                            if(a==false){
                                                                $('#mtdt-msg').show();
                                                                $('#mtdt-cls').removeClass('col-md-4').addClass('col-md-12');
                                                            }else{
                                                                $('#mtdt-msg').hide();
                                                                $('#mtdt-cls').removeClass('col-md-12').addClass('col-md-4');
                                                            }
                                                        })
                                                    });
                                                </script>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <div class="form-group control-group">
                                                <label>Number of Working Days</label>
                                                <select class="form-control" name="solar_proposal[no_of_working_days]" id="no_of_working_days">
                                                    <option value="">Select one</option>
                                                    <?php
                                                    $nwd = unserialize(NO_OF_WORKING_DAYS);
                                                    if (!empty($nwd)) {
                                                        foreach ($nwd as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value; ?>" ><?php echo $value; ?> Days</option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="form-group control-group">
                                                <label>Custom Profile</label>
                                                <select class="form-control" name="solar_proposal[custom_profile_id]" id="custom_profile_id">
                                                    <option value="">Select one</option>
                                                    <?php
                                                    if (!empty($custom_profiles)) {
                                                        foreach ($custom_profiles as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value['customProfileId']; ?>"><?php echo $value['customProfileName']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="monthly_electricity_bill">Monthly Electricity Bill (AUD / Month)</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                </div>
                                                <input type="number" name="solar_proposal[monthly_electricity_bill]" class="form-control" id="monthly_electricity_bill" placeholder="Please Enter Monthly Electricity Bill (AUD / Month)"  min="0" step="0.0001" oninput="validity.valid||(value='');" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3 ">
                                            <!--<label for="monthly_electricity_usage">Monthly Electricity Usage (kWh / Month)</label>-->
                                            <label for="monthly_electricity_usage">Average Daily Consumption(kWh)</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[monthly_electricity_usage]" class="form-control" id="monthly_electricity_usage" placeholder="Please Enter Average Daily Consumption(kWh)" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <div class="form-group control-group">
                                                <label>Rate</label>
                                                <select class="form-control" name="solar_proposal[rate]" id="rate" required>
                                                    <option value="">Select one</option>
                                                    <?php
                                                    $rates = unserialize(RATE);
                                                    if (!empty($rates)) {
                                                        foreach ($rates as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value; ?>" ><?php echo $value; ?> Rate</option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-6 mb-3">
                                            <div class="form-group control-group">
                                                <label>% average annual increase in energy cost</label>
                                                <input type="number" name="solar_proposal[annual_inc_in_ec]" class="form-control" id="annual_inc_in_ec" min="0" oninput="validity.valid||(value='');" value="5" required="">
                                            </div>
                                        </div> 
                                    </div>


                                    <div class="form-row hidden" id="rate1">
                                        <div class="col-sm-6 col-md-6 mb-3">
                                            <label>Cost per kWh (peak)</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[solar_cost_per_kwh_peak]" class="form-control" id="solar_cost_per_kwh_peak" min="0" step="0.0001" oninput="validity.valid||(value='');" placeholder="Peak Rate" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row hidden" id="rate2">
                                        <div class="col-sm-6 col-md-6 mb-3">
                                            <label>Cost per kWh (Off-peak)</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[solar_cost_per_kwh_off_peak]" class="form-control" min="0" step="0.0001" oninput="validity.valid||(value='');" id="solar_cost_per_kwh_off_peak" placeholder="Off-Peak Rate" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row hidden" id="rate3">
                                        <div class="col-sm-6 col-md-6 mb-3">
                                            <label>Cost per kWh (Shoulder)</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[solar_cost_per_kwh_shoulder]" class="form-control" min="0" step="0.0001" oninput="validity.valid||(value='');" id="solar_cost_per_kwh_shoulder" placeholder="Shoulder Rate" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row hidden" id="rate4">
                                        <div class="col-md-2 mb-3">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[rate4_data][peak]" class="form-control" min="0" step="0.0001" oninput="validity.valid||(value='');" id="rate4_peak" placeholder="Rate" >
                                            </div>
                                        </div>
                                        <div class="col-md-2 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[rate4_data][usage]" class="form-control" min="0" oninput="validity.valid||(value='');" id="rate4_usage" placeholder="Usage" >
                                            </div>
                                        </div>
                                        <div class="col-md-2 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="time" name="solar_proposal[rate4_data][start_time]" class="form-control rate_st" id="rate4_start_time" placeholder="Start Time">
                                            </div>
                                        </div>
                                        <div class="col-md-2 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="time" name="solar_proposal[rate4_data][finish_time]" class="form-control rate_ft" id="rate4_finish_time" placeholder="Finish Time">
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <select class="form-control rate_days_of_week" name="solar_proposal[rate4_data][days_of_week]" id="rate4_days_of_week">
                                                    <option value="">Select Days Of Week</option>
                                                    <option value="flat_rate" >Flat Rate</option>
                                                    <option value="peak_weekday" >Peak Mon-Fri (Weekday)</option>
                                                    <option value="shoulder_weekday" >Shoulder Mon-Fri (Weekday)</option>
                                                    <option value="off_peak_weekday" >Off Peak Mon-Fri (Weekday)</option>
                                                    <option value="off_peak_weekend" >Off Peak Sta-Sun (Weekend)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="daily_usage_charge">Daily Usage Charge</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                </div>
                                                <input type="number" name="solar_proposal[daily_usage_charge]" class="form-control" id="daily_usage_charge" placeholder="Please Enter Daily usage charge" min="0" step="0.0001" oninput="validity.valid||(value='');"  required="">
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="feed_in_tariff">Feed in tariff</label>
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                </div>
                                                <input type="number" name="solar_proposal[feed_in_tariff]" class="form-control" id="feed_in_tariff" placeholder="Please Enter Feed in tariff" min="0" step="0.0001" oninput="validity.valid||(value='');"  required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row mb-2">
                                        <span class="col-sm-3">Demand Charge?</span>
                                        <div class="col-sm-4">
                                            <span class="sr-switch">
                                                <label for="is_demand_charge">Yes</label>
                                                <input type="checkbox" class="sr-switch" id="is_demand_charge" checked="" />
                                                <label for="is_demand_charge">No</label>
                                            </span>
                                            <input type="hidden" name="solar_proposal[is_demand_charge]" id="is_demand_charge_val" />
                                        </div>
                                    </div>
                                    <!--<div class="form-row mb-2">-->
                                    <!--    <div class="col-sm-12">-->
                                    <!--        <span>Additional Notes</span>-->
                                            <textarea row="3" id="additional_notes" style="display:none;" name="solar_proposal[additional_notes]" class="form-control"></textarea>
                                    <!--    </div>-->
                                    <!--</div>-->

                                    <div class="form-row hidden" id="demand_charge_container">
                                        <div class="col-md-4 mb-3">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[demand_charge]" class="form-control" id="demand_charge" min="0" step="0.01" oninput="validity.valid||(value='');" placeholder="Demand Charge">
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" name="solar_proposal[demand_charge_kVA]" class="form-control" id="demand_charge_kVA" min="0" step="0.01" oninput="validity.valid||(value='');" placeholder="kVA">
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 ">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <input type="number" readonly="" class="form-control" id="total_demand_charge" min="0" step="0.01" oninput="validity.valid||(value='');" placeholder="Total Demand Charge">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-4 mb-3 hidden">
                                            <img id="solarCustomProfileIdThumb" class="custom-profile__thumbnail" src="" width="150">
                                        </div>

                                    </div>

                                    <div class="btn-block mt-2">
                                        <input type="button" id="save_proposal_btn" value="Update" class="btn btn-primary">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="card" style="min-height: 545px;">
                        <div class="card-body ">
                            <div class="card_body__title">
                                <h2>Add Product</h2>
                            </div>
                            <div class="form-block d-block clearfix">
                                <form role="form" action="#" method="post" id="solar_products_form">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <label>Product Type</label>
                                                <select class="form-control" id="type_id" required>
                                                    <option value="">Select one</option>
                                                    <?php
                                                    if (!empty($product_types)) {
                                                        foreach ($product_types as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value['type_id']; ?>"><?php echo $value['type_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <label>Item</label>
                                                <select class="form-control" name="solar_product[prd_id]" name="prd_id" id="prd_id" required>
                                                    <option value="">Please Select Product Type</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <label class="required-label">Qty</label>
                                                <input class="form-control" placeholder="Please Enter Qty" type="number" name="solar_product[prd_qty]" id="prd_qty" min="1" oninput="validity.valid||(value='');" value="" required="">

                                            </div>
                                        </div>
                                    </div>

                                    <input type="button" id="add_product_btn" value="Add Product" class="btn btn-primary">
                                    <input type="button" id="update_product_btn" value="Update Product" class="btn btn-primary hidden">
                                    <input type="button" id="cancel_product_btn" value="Cancel" class="btn btn-primary hidden">
                                    <input type="hidden" id="solar_product_id" name="solar_product_id">
                                </form>
                            </div>

                            <div class="form-block d-block clearfix mt-5">
                                <div class="table-responsive"  >
                                    <table width="100%" border="0" class="table table-striped" id="solar_proposal_products_table" >
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Type</th>
                                                <th>Qty</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="solar_proposal_products_table_body">

                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                            <div class="form-row mt-5">
                                <div class="col-sm-12">
                                    <label for="feed_in_tariff">Additional Notes</label>
                                    <textarea row="3" name="solar_proposal[additional_notes]" onkeyup="document.getElementById('additional_notes').value = this.value" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                         <form role="form" action="" method="post" name="solarSystemSummary" id="solarSystemSummary">
                            <div class="card" style="min-height: 560px;">
                                <div class="card-body row">
                                        <div class="col-sm-12 mb-5">
                                             <div class="card_body__title">
                                                <h2>Panel setup via</h2>
                                            </div>
                                            <span class="sr-switch">
                                                <label for="nearmap_toggle">Uploading Image</label>
                                                <input type="checkbox" class="sr-switch" id="nearmap_toggle"/>
                                                <label for="nearmap_toggle">Using Near Maps on CRM</label>
                                            </span>
                                        </div>
                                    <div class="col-md-6" id="manual-panel-stats">
                                            <div class="card_body__title">
                                                <h2>Panel Setup</h2>
                                            </div>
                                            <div class="form-block d-block clearfix">
                                                <div class="form-group hidden">
                                                    <label for="total_system_size">Panel Wattage </label>
                                                    <input class="form-control" placeholder="Please Enter Total System Size/Array Power Rating (recommended)" type="number" name="solar_proposal[total_system_size]" id="total_system_size" value="" >
                                                </div>

                                                <div class="form-group hidden">
                                                    <label for="total_panel_area">Total Panel Area</label>
                                                    <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                        <input type="number" name="solar_proposal[total_panel_area]" placeholder="Please Enter Total Panel Area" min="1" oninput="validity.valid||(value='');" value="" class="form-control" id="total_panel_area" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><b>m<sup>2</sup></b></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="font-size:18px; border-bottom:1px solid black; margin-bottom:5px;">Panel Details - Area1</div>
                                                <div class="form-group">
                                                    <label>Number of Panels</label>
                                                    <input class="form-control no_of_panels" placeholder="Please Enter Number of Panels" id="js-val-nop" type="number" name="solar_panel_data[no_of_panels][0]" value="" min="1" oninput="validity.valid||(value='');" required="">
                                                </div>

                                                <div class="form-group">
                                                    <label class="required-label">Panel Orientation</label>
                                                    <select name="solar_panel_data[panel_orientation][0]" class="form-control" required="">
                                                        <option value="0" selected="">0</option>
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="30">30</option>
                                                        <option value="40">40</option>
                                                        <option value="50">50</option>
                                                        <option value="60">60</option>
                                                        <option value="70">70</option>
                                                        <option value="80">80</option>
                                                        <option value="90">90</option>
                                                        <option value="100">100</option>
                                                        <option value="110">110</option>
                                                        <option value="120">120</option>
                                                        <option value="130">130</option>
                                                        <option value="140">140</option>
                                                        <option value="150">150</option>
                                                        <option value="160">160</option>
                                                        <option value="170">170</option>
                                                        <option value="180">180</option>
                                                        <option value="190">190</option>
                                                        <option value="200">200</option>
                                                        <option value="210">210</option>
                                                        <option value="220">220</option>
                                                        <option value="230">230</option>
                                                        <option value="240">240</option>
                                                        <option value="250">250</option>
                                                        <option value="260">260</option>
                                                        <option value="270">270</option>
                                                        <option value="280">280</option>
                                                        <option value="290">290</option>
                                                        <option value="300">300</option>
                                                        <option value="310">310</option>
                                                        <option value="320">320</option>
                                                        <option value="330">330</option>
                                                        <option value="340">340</option>
                                                        <option value="350">350</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="required-label">Panel Tilt</label>
                                                    <select name="solar_panel_data[panel_tilt][0]" class="form-control" required="">
                                                        <option value="0" selected="">0</option>
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="30">30</option>
                                                        <option value="40">40</option>
                                                        <option value="50">50</option>
                                                        <option value="60">60</option>
                                                        <option value="70">70</option>
                                                        <option value="80">80</option>
                                                        <option value="90">90</option>
                                                    </select>
                                                </div>
                                                <div id="panels_area_wrapper">
                                                </div>
                                                <div class="buttwrap">
                                                    <button id="add_panel_area" type="button" class="btn btn-primary">Add Panel Area </button>
                                                    <input type="button" id="save_proposal_additional_btn" value="Update" class="btn btn-primary pull-right">
                                                </div>
                                            </div>
                                    </div>
                                    <!-- solar panel manual tilt and rotation end -->
                                    <div class="col-md-6" id="inclusion-stats">
                                        <div class="card_body__title">
                                            <h2>Inclusions / Exclusions</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group">
                                                <label class="required-label">Tilt System</label>
                                                <select name="solar_proposal[additional_items][tiltSystem]" id="tiltSystem" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-label">Additional Switchboard</label>
                                                <select name="solar_proposal[additional_items][switchBoard]" id="switchBoard" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>                
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-label">Grid Protection Equipment</label>
                                                <select name="solar_proposal[additional_items][gridProtectionEquipment]" id="gridProtectionEquipment" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>                
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-label">Engineering Report</label>
                                                <select name="solar_proposal[additional_items][engineeringReport]" id="engineeringReport" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>                
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-label">Network Report</label>
                                                <select name="solar_proposal[additional_items][networkReport]" id="networkReport" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>               
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-label">TV Installation</label>
                                                <select name="solar_proposal[additional_items][TVInstallation]" id="TVInstallation" class="form-control additional_items" required="">
                                                    <option value="0">Not Applicable</option>
                                                    <option value="1">Included</option>
                                                    <option value="2">Not Included</option>              
                                                </select>
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card" style="min-height: 599px;">
                            <div class="card-body">
                                <div class="card_body__title">
                                    <h2>Payment Summary</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="" method="post" name="solarPayments" id="solarPayments">

                                        <div class="row hidden">
                                           <div class="col-12">
                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>STC</b></span>
                                                </div>
                                                <input type="number" name="solar_proposal[stc]" class="form-control" id="stc" readonly="" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Total Project Cost exc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input type="number" name="solar_proposal[total_payable_exGST]" class="form-control" id="total_payable_exGST" placeholder="Please Enter Total Payable exc. GST" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Total Project Cost inc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input type="number"  class="form-control" id="total_payable_inGST" placeholder="Please Enter Total Payable inc. GST" required="" readonly="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="required-label">STC Rebate exc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="solar_proposal[stc_rebate_value]" id="stc_rebate_value" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="required-label">STC Rebate inc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input type="number" class="form-control" placeholder="Please Enter STC rebate value"  id="stc_rebate_value_inGST" value="" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Net Payable exc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input type="number" name="solar_proposal[price_before_stc_rebate]" class="form-control" id="price_before_stc_rebate" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Net Payable inc GST</label>
                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                    </div>
                                                    <input type="number" class="form-control" id="price_before_stc_rebate_inGST" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="card" id="solar_proposal_finance_container" >
                                <div class="">
                                    <div class="card_body__title">
                                        <h2>Finance</h2>  
                                    </div>
                                    <div class="form-block d-block clearfix">
                                        <form role="form" action="" method="post" id="proposal_finance_form">
                                            <div class="form-row">
                                                <input type="hidden" name="proposal_finance[proposal_type]" value="2" />
                                                <div class="col-md-6 mb-3">
                                                    <label for="term">Term (Months)</label>
                                                    <select class="form-control" name="proposal_finance[term]" id="term" required="">
                                                        <option value="">Select one</option>
                                                        <?php
                                                        $term_months = unserialize(TERM_MONTHS);
                                                        foreach ($term_months as $key => $value) {
                                                            ?>
                                                            <?php if($key == 0){ ?>
                                                                <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                            <?php }else { ?>
                                                                <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label for="monthly_payment_plan">Monthly Payment Plan</label>
                                                        <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                            </div>
                                                            <input type="number" name="proposal_finance[monthly_payment_plan]" class="form-control" id="monthly_payment_plan"   required="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-12 mb-3">
                                                    <div class="form-group">
                                                        <label for="monthly_payment_plan">Upfront Payment Option</label>
                                                        <select class="form-control" name="proposal_finance[upfront_payment_options]" id="upfront_payment_options">
                                                            <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option> 
                                                            <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                            <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3" id="finance_loader"></div>
                                                <div class="btn-block mt-5 text-center">
                                                    <input type="button" id="save_proposal_finance_btn" value="Update"  class="btn btn-primary">
                                                    <input type="button" id="delete_proposal_finance_btn" value="Remove"  class="btn btn-primary hidden">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
</div>
<?= $stc_modal ?>
<script src="<?php echo site_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>assets/js/isLoading.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="<?php echo site_url(); ?>common/js/solar_proposal.js?v=<?php echo version; ?>" type="text/javascript"></script>
<script>

    $(document).ready(function () {
        $('.locationstreetAddress').placecomplete2({
            placeServiceResult2: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('.locationstreetAddressVal') != undefined) {
                        $('.locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('.locationState') != undefined) {
                                var select = document.querySelector(".locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.querySelector(".locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    }
                                }

                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('.locationPostCode') != undefined) {
                                $('.locationPostCode').val(address_components[i].long_name);
                                $('.locationPostCode').trigger('change');
                            }
                        }
                    }
                } else{
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'fr'
        });

    });

var context = {};
context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
context.proposal_data = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? json_encode($proposal_data, JSON_HEX_APOS) : ''; ?>';
context.panel_data = '<?php echo (isset($panel_data) && !empty($panel_data)) ? json_encode($panel_data, JSON_HEX_APOS) : ''; ?>';
context.lead_uuid = '<?php echo (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : ''; ?>';
context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
context.postcode_rating = '<?php echo (isset($postcode_rating) && $postcode_rating != '') ? $postcode_rating : 0; ?>';
var solar_proposal_manager = new solar_proposal_manager(context);
</script>
</body>
</html>


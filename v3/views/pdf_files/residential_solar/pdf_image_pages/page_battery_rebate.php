<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
<style>
    .font-family{font-family: 'Open Sans', sans-serif;}
    .table-border{border:solid 1px #ccc;}
    .w910{width:910px; }
    body{margin:0px !important;}
</style>
</head>
<body style="padding:0; margin:0">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
		<tr>
			<td valign="top" style="border: 0; font: 0">
				<table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td  align="center" valign="top"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/p8.jpg" width="840" alt=""/></td>
						</tr>
						<tr>
							<td height="80"></td>
						</tr>
						<tr>
							<td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="381" align="left" valign="middle"><table width="125" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td width="37" align="left"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/phone.jpg" width="25" height="25" alt=""/></td>
													<td width="41" align="center" style="font-size:20px;"><strong>13</strong></td>
													<td width="35" align="center" style="font-size:20px;"><strong>58</strong></td>
													<td width="37" align="center" style="font-size:20px;"><strong>42</strong></td>
												</tr>
											</tbody>
										</table></td>
										<td width="405" align="right" valign="top"><img src="<?php echo site_url(''); ?>assets/images/logo.svg" width="163" height="58" alt=""/></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td height="30"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
<table class="pdf_page hidden" id="page_3" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>Site Details</strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="400" valign="top">
                                    <table width="390" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="60" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Access:</td>
                                            <td width="180" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:15px; padding:5px">
                                                <input id="project_name" name="job_card[roof_access]" style="padding:5px; width:inherit; height: inherit;" type="text" /> Meters
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is forklift available on Site?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input type="radio" id="is_forklift_yes" value="yes" name="job_card[is_forklift]"/>
                                                <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                <input type="radio" id="is_forklift_no" value="no" name="job_card[is_forklift]"/> 
                                                <span style="margin-right: 20px;font-size: 16px;">No</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Notes</td>
                                        </tr>
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                <textarea id="access_note" name="job_card[access_note]" style="padding:5px; width:100%; height: 100px;"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Roof Access Photo 1</th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Roof Access Photo 2</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="roof_access_one" />
                                        <input type="hidden" id="roof_access_one" name="job_card[roof_access_one]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="roof_access_two" />
                                        <input type="hidden" id="roof_access_two" name="job_card[roof_access_two]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="text-align: center;background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Roof Condition Photo 1</th>
                                <th width="412" class="black-bg" style="text-align: center;background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Roof Condition Photo 2</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="roof_condition_one" />
                                        <input type="hidden" id="roof_condition_one" name="job_card[roof_condition_one]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="roof_condition_two" />
                                        <input type="hidden" id="roof_condition_two" name="job_card[roof_condition_two]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
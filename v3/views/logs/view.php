<?php
$priority           = [1 => 'Business Critical', 2 => 'Important', 3 => 'Normal', 4 => 'Low'];
$priorityClass      = [1 => 'badge-danger', 2 => 'badge-warning', 3 => 'badge-primary', 4 => 'badge-secondary'];
$runningStatus      = ['Not Started', 'Running', 'In Review', 'Completed'];
$runningStatusClass = ['badge-secondary', 'badge-warning', 'badge-info', 'badge-success'];
?> 
<div class="page-wrapper d-block clearfix badge-">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><b><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?> <?php echo 'L-' . $issue['id']; ?>: <?php echo $issue['status'] == 3 ? '<strike>' . $issue['title'] . '</strike>' : $issue['title'] ?></b></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/page/log-issue/manage') ?>"><i class="icon-arrow-left"></i> Back to Manage Issues </a>
            </div>
        </div>
    </div>
    <p>
        <span class="pull-right">Created on <?php echo date('d/m/Y h:i A', strtotime($issue['created_at'])); ?></span>
        <span class="badge badge-pill <?php echo $priorityClass[$issue['priority']]; ?>"><?php echo $priority[$issue['priority']]; ?></span>
    </p>
    <p><?php echo $issue['description'] ?></p>
    <?php if ($issue['image']) { ?>
        <p>
            <?php
            $extension = strtolower(pathinfo($issue['image'], PATHINFO_EXTENSION));

            if (in_array($extension, ['jpeg', 'jpg', 'png', 'gif'])) {
                ?>
                <i class="fa fa-file"></i>
                <a target="_blank" href="<?php echo site_url('assets/uploads/issue_files/' . $issue['image']); ?>">
                    <img src="<?php echo site_url('assets/uploads/issue_files/' . $issue['image']); ?>" style="height:100px;width:100px;">
                </a> 
            <?php } else {
                ?>
                <a target="_blank" href="<?php echo site_url('assets/uploads/issue_files/' . $issue['image']); ?>"><?php echo $issue['image']; ?></a> 
            <?php } ?>
        </p>
    <?php } ?>
    <?php if ($issue['url']) { ?>
        <p><b>Url: </b><a href="<?php echo $issue['url'] ?>" target="_blank"><?php echo $issue['url'] ?></a></p>
    <?php } ?>
    <?php if ($issue['user_id']) { ?>
        <p><b>Franchise: </b><?php echo $issue['full_name'] ?></p>
    <?php } ?>
    <?php if ($issue['cust_id']) { ?>
        <p><b>Customer: </b><?php echo $issue['first_name'] . ' ' . $issue['last_name'] ?></p>
    <?php } ?>
    <?php if ($issue['additional_data']) { ?>
        <p><b>Additional Data: </b><?php echo $issue['additional_data'] ?></p>
    <?php } ?>
    <p><b>Current Status: </b>
        <span class="js-status-container">
            <span class="badge badge-pill <?php echo $runningStatusClass[$issue['status']]; ?>"><?php echo $runningStatus[$issue['status']]; ?></span>
        </span>            
    </p>
    <ul class="timeline" id="time-line-thread"><div class="alert alert-info text-center"><i class="fa fa-spin fa-gear"></i> Loading thread...</div></ul>
    <div class="review-form">
        <h3>Add Comment</h3>
        <form id="review-form-handle">
            <textarea id="mail-editor" name="message"></textarea>        
            <div class="row  mt-5">            
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Current status</label>
                        <select class="select2" name="status" id="curent-issue-status" name="status">
                            <?php foreach ($runningStatus as $k => $rs) { ?>
                                <option <?php echo $issue['status'] == $k ? 'selected="selected"' : ''; ?> value="<?php echo $k ?>"><?php echo $rs ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Attach file with comment</label>
                        <input type="file" id="attachment" class="rf-input"/>
                        <input type="hidden" name="file" id="attachment-file"/>
                        <input type="hidden" id="issue_id" name="issue_id" value="<?php echo $issue['id'] ?>"/>
                    </div>

                </div>        
            </div>        
            <div class="row">            
                <div class="col-md-8">
                    <div class="form-group">
                        <button type="submit" id="js-submit" class="btn btn-info">Add Comment</button>
                        <a id="js-ldr" class="btn btn-info" style="display:none;"><i class="fa fa-gear fa-spin"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include 'script.php'; ?>
<?php include 'timeline.php'; ?>

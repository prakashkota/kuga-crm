<style>
    #inverter_layout_canvas {
        width: 100%;
        height: 600px;
        position: relative;
        overflow: hidden;
    }

    #tool-bar1 {
        background: rgba(0, 0, 0, .8);
        bottom: 0px;
        width: 50px;
        padding-top: 8px;
        z-index: 0;
        position: absolute;
        left: 0px;
        top: 0px;
    }

    #tool-bar1 ul {
        margin: 0;
        padding: 0;
    }

    #tool-bar1 ul li {
        display: block;
        list-style: none;
        text-align: center;
    }

    #tool-bar1 ul li a {
        color: #929292;
        padding: 15px 0px;
        display: block;
    }

    #tool-bar1 ul li a.active,
    #tool-bar1 ul li a:hover {
        color: #fff;
    }

    #tool-bar1 .tool-bar1-window {
        position: absolute;
        top: 15px;
        width: 350px;
        background: rgba(0, 0, 0, .8);
        left: 65px;
        padding: 15px;
        color: #fff;
        border-radius: 5px;
    }

    #tool-bar1 .tool-bar1-window input {
        color: #000;
    }

    .preview {
        width: 1400px;
    }

    .preview img {
        width: 100%;
    }

    .tool-overlay {
        display: none;
        position: fixed;
        z-index: 1;
        background: rgba(0, 0, 0, .7);
        color: #fff;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
    }

    .tool-overlay div {
        position: absolute;
        top: 50%;
        text-align: center;
        width: 100%;
    }

    .orientation {
        opacity: 0;
        position: absolute;
    }

    .rotate-by-90 {
        display: flex;
        justify-content: space-between;
    }

    .counter-inpt {
        width: 68px !important;
        margin: 0;
    }

    .rotate-by-90.rd-btn {
        justify-content: end;
    }

    .rotate-by-90.rd-btn label {
        margin: 0 0 0 6px;
        cursor: pointer;
    }

    .portrait {
        position: relative;
    }

    .portrait::after {
        background: #000;
        width: 14px;
        height: 22px;
        content: "";
        position: absolute;
        top: 4px;
        left: 0;
        right: 0;
        margin: 0 auto;
        border-radius: 2px;
    }

    .landscape {
        position: relative;
    }

    .landscape::after {
        background: #000;
        width: 22px;
        height: 14px;
        content: "";
        position: absolute;
        top: 8px;
        left: 0;
        right: 0;
        margin: 0 auto;
        border-radius: 2px;
    }

    .rotate-by-90.rd-btn label {
        background: #bdbdbd !important;
    }

    .rotate-by-90.rd-btn label.active {
        background: #b92625 !important;
        background: #b92625 !important;
        background: #b92625 !important;
        background: #b92625 !important;
    }

    #rt-landscape,#rt-portrait{margin: 10px;}

    /* 16 April 2019 SJ */
    .tool-bar1-window .form-control{ height: 30px; padding: 5px 10px; font-size: 12px; line-height: 1.5; border-radius: 3px; margin: 0; width:100%;}
    .tool-bar1-window label { color: #fff; font-size: 14px;}
    .tool-bar1-window .input-group{ float: none; width: 100%; }
    #inverter_layout_canvas .input-group .input-group-btn{ position: absolute; right: 0; top: 0;}
    #inverter_layout_canvas .input-group .input-group-btn .btn{ -webkit-border-radius:3px; border-radius:3px; }
    .tool-bar1-window .btn{ height: 30px; line-height: 30px; padding: 0 4px; -webkit-transition: none; transition: none; -webkit-border-radius: 3px; border-radius: 3px;}
    #inverter_layout_canvas .rotate-by-90 label{ cursor: pointer; width: 30px; padding: 0; height: 30px; line-height: 32px; text-align: center; color: #000; -webkit-border-radius: 3px; border-radius: 3px;
        background:#b92625;
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b92625',endColorstr='#ffb51d',GradientType=0);}
        #inverter_layout_canvas .btn:hover, #inverter_layout_canvas .input-group .input-group-btn .btn:hover, #inverter_layout_canvas .rotate-by-90 label:hover{ color: #fff; background: #b92625;}
        #inverter_layout_canvas .rotate-by-90 i { font-size: 12px;  margin: 7px; margin-top:10px;}
        .btn-inset button {
            position: absolute;
            right: -30px;
            top: 0;
            border-radius: 0px 3px 3px 0px !important;
            bottom: -30px;
            z-index: 2;
        }
        .select-typ {
            position: relative;
            right: -18px;
            width: 110px;
            text-align: center;
        }
        .full_screen .card{height:100%;}
        .canvas-container{min-height: 600px !important;}

        .myMenu{
            position:absolute;     
            display: none;
            width: 120px;
            z-index: 2;
        }

        .myMenu ul{
            list-style: none;
            margin:0;
            padding:0;
        }

        .myMenu li{
            width: 140px;
            background: #fff;
            border:1px solid #ccc;                
            color:#b5b5b5; 
        }

        .myMenu a{
            color:#3d7cb9;
            text-decoration: none;
            display:block;
            padding:5px 10px;
        }

        .myMenu a:hover{
            background-color:#3d7cb9;
            color:#fff;                
        }
        a#full_screen{
            background-color: #fff;
            cursor: pointer;
            border-bottom: 1px solid #ccc;
            width: 35px;
            height: 35px;
            line-height: 35px;
            display: block;
            text-align: center;
            text-decoration: none;
            color: black;
            right: 25px;
            color: #000;
            top: 32px;
            z-index: 1;
            position: absolute;
            background-clip: padding-box;
        }

    </style>

    <div class="row" id="il_tool_section">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="card">
                <div class="card-body">

                    <a href="javascript:void(0)" class="tl" id="full_screen">
                        <i class="fa fa-expand"></i>
                    </a>


                    <div class="myMenu" id="myMenu">
                        <ul>
                            <li><a href="javascript:void(0);" class="tl" id="add_text">Add Text</a></li>
                            <li><a href="javascript:void(0);" class="tl" id="add_measuring_line">Add Line</a></li>
                            <li><a href="javascript:void(0);" class="tl" id="add_connection_line">Add Cable Tray</a></li>
                        </ul>
                    </div>

                    <canvas id="inverter_layout_canvas" width="1280" height="600" style="border:1px solid #ccc; margin-left:40px;"></canvas>
                    <div id="tool-bar1">
                        <ul>

                            <li>
                                <a href="javascript:void(0)" class="tl" id="select_tool">
                                    <i class="fa fa-mouse-pointer "></i>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)" data-id="inverter_tool_section" class="tl" title="Add Inverter">
                                    IN
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)" data-id="np_tool_section" class="tl" >
                                    NP
                                </a>
                            </li>

                            <!--
                            <li>
                                <a href="javascript:void(0)" class="tl" id="add_connection_line">
                                    CR
                                </a>
                            </li>

                            
                            <li>
                                <a href="javascript:void(0)" class="tl" id="add_measuring_line">
                                    <i class="fas fa-arrows-alt-h"></i> L
                                </a>
                            </li>
                        -->

                            <!--<li>
                                <a href="javascript:void(0)" class="tl" id="add_text">
                                    TXT
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)" class="tl" id="full_screen">
                                    <i class="fa fa-expand"></i>
                                </a>
                            </li>
                        -->

                        <li>
                            <a href="javascript:void(0)" class="tl" id="capture_image">
                                <i class="fa fa-picture-o"></i>
                            </a>
                            <a href="javascript:void(0)" class="d-none" id="capture_image_link" target="__blank"></a>
                        </li>

                        <li>
                            <a href="javascript:void(0);" class="d-none" title="Download Image" target="_blank" id="download_snapshot_link">
                                <i class="fa fa-download"></i>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" class="tl" id="save_inverter_design_btn">
                                <i class="fa fa-save"></i>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" class="tl" id="add_inverter_notes_btn">
                                <i class="fa fa-sticky-note-o"></i>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" class="tl" id="delete_il_all_inverter">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </li>


                        <li style="margin-top:250px;" class="d-none"><a href="javascript:void(0)" class="" id="zoomIn"><i class="fas fa-2x fa-plus"></i></a></li>
                        <li class="d-none"><a href="javascript:void(0)" class="" id="zoomOut"><i class="fas fa-2x fa-minus"></i></a></li>
                    </ul>
                    <div class="tool-bar1-window d-none">

                        <div class="tl-2-content tl-content d-none" id="inverter_tool_section">
                            <div class="form-group">
                                <select class="form-control" id="il_inverter_list">
                                    <?php foreach ($inverters as $row) { ?>
                                        <option 
                                        data-dimension="<?php echo $row['length']; ?>,<?php echo $row['width']; ?>" 
                                        data-capacity="<?php echo $row['capacity']; ?>"
                                        data-image="<?php echo $row['inverter_design_image']; ?>" 
                                        value="<?php echo $row['id']; ?>">
                                        <?php echo $row['name']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="input-group input-group-sm btn-inset d-none">
                                        <input id="no_of_il_inverter" type="number" class="form-control "  placeholder="Enter no of inverters to add" min="1" value="1" />
                                    </div>
                                    <button id="add_il_inverter" type="button" class="btn btn-info btn-flat"><i class="fa fa-plus"></i> Add Inverter</button>
                                </div>
                            </div>

                            <div class="col-sm-3  ml-4">
                                <div class="form-group">
                                    <button id="add_il_inverter_cage" class="btn btn-xs  btn-info" ><i class="fa fa-plus"></i> Add Cage</button>
                                </div>
                            </div>

                            <div class="col-sm-3  ml-2 d-none">
                                <div class="form-group">
                                   <!-- <button id="delete_il_all_inverter" class="btn btn-xs  btn-info" ><i class="fa fa-trash"></i> Delete All</button> -->
                               </div>
                           </div>
                       </div>
                       <div class="row">
                        <div class="col-sm-6">
                            <label>Enter inverter length in mm</label>
                            <div class="form-group">
                                <input id="il_inverter_length" type="number" class="form-control"  placeholder="Enter inverter length in mm" min="1" value="1000" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label>Enter inverter height in mm</label>
                            <div class="form-group">
                                <input id="il_inverter_height" type="number" class="form-control"  placeholder="Enter inverter height in mm" min="1" value="650" />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label>Enter disatnce between inverters in mm</label>
                            <div class="form-group">
                                <input id="il_inverter_distance" type="number" class="form-control"  placeholder="Enter disatnce between inv. in mm" min="1" value="850" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="javascript:void(0);" class="btn btn-xs  btn-info tl">Close</a>
                        </div>
                    </div>
                </div>

                <div class="tl-2-content tl-content d-none" id="np_tool_section">
                   <div class="row">
                    <div class="col-sm-12">
                        <label>Enter network protection to inverter distance in mm</label>
                        <div class="form-group">
                            <input id="il_np_to_inv_distance" type="number" class="form-control"  placeholder="Enter network protection to inverter distance in mm" min="1" value="400" />
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label>Enter network protection length in mm</label>
                        <div class="form-group">
                            <input id="il_np_length" type="number" class="form-control"  placeholder="Enter network protection length in mm" min="1" value="650" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Enter network protection height in mm</label>
                        <div class="form-group">
                            <input id="il_np_height" type="number" class="form-control"  placeholder="Enter network protection height in mm" min="1" value="1000" />
                        </div>
                    </div>

                    <div class="col-sm-3  ml-3">
                        <div class="form-group">
                            <button id="add_network_protection" class="btn btn-xs  btn-info" ><i class="fa fa-plus"></i> Add Network Protection</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="inverter_notes_modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title pb-1">Notes for Inverter Design</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="col-12 col-sm-12 col-md-12">
               <div class="form-block d-block clearfix">
                  <textarea id="inverter_notes"></textarea>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save_inverter_notes_btn">Save changes</button>
    </div>
</div>
</div>
</div>

<img id="il_image" />
<script src="https://unpkg.com/fabric@latest/dist/fabric.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
<script src="<?php echo site_url(''); ?>common/js/inverter_layout.js?v=<?php echo time(); ?>"></script>
<script type="text/javascript">
    var context = {};
    context.proposal_id = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? $proposal_data['id'] : '';?>';
    context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
    context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
    var inverter_layout = new inverter_layout_manager(context);
</script>
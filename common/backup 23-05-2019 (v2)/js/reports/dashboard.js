var dashboard_reporting_manager = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.target_data = {};
    
    if (self.user_group != '1' && self.user_group != '6' && self.user_group != '7' && self.all_reports_view_permission == '0') {
        $('#statistics_container').hide();
        $('#error_container').removeClass('hidden');
        $('#reporting_actions').addClass('hidden');
        $('.page-wrapper').attr('style', 'height:100% !important;');
        return false;
    }
    
    $(function () {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('start_date').value = start.format('YYYY-MM-DD');
            document.getElementById('end_date').value = end.format('YYYY-MM-DD');
            self.initialize();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });

    $('.sr-show_inactive_users').click(function () {
        $('.sr-inactive_user').addClass('hidden');
        if ($(this).prop("checked") == true) {
            $('.sr-inactive_user').removeClass('hidden');
        }
    });

    $('#userid').on('change', function () {
        self.initialize();
    });

    self.mark_as_completed_activity();
};

dashboard_reporting_manager.prototype.initialize = function () {
    var self = this;
    document.getElementById('statistics').innerHTML = '';
    document.getElementById('conversion_table_body').innerHTML = '';
    document.getElementById('activity_statistics').innerHTML = '';
    document.getElementById('activity_devision').innerHTML = '';
    document.getElementById('scheduled_activities_table_body').innerHTML = '';
    //document.getElementById('statistics').appendChild(createLoader('Fetching statistics information....'));
    //document.getElementById('conversion_table_body').appendChild(createLoader('Fetching conversion information....'));
    //document.getElementById('activity_statistics').appendChild(createLoader('Fetching activity information....'));
    //document.getElementById('activity_devision').appendChild(createLoader('Fetching activity devision information....'));
    //document.getElementById('scheduled_activities_table_body').appendChild(createLoader('Fetching scheduled activity information....'));

    document.getElementById('statistics').appendChild(createPlaceHolder(true, 'col-4'));
    document.getElementById('statistics').appendChild(createPlaceHolder(true, 'col-4'));
    document.getElementById('statistics').appendChild(createPlaceHolder(true, 'col-4'));

    document.getElementById('conversion_table_body').appendChild(createPlaceHolder());
    document.getElementById('activity_statistics').appendChild(createPlaceHolder());
    document.getElementById('activity_devision').appendChild(createPlaceHolder());
    document.getElementById('scheduled_activities_table_body').appendChild(createPlaceHolder());

    self.fetch_statistics_data();
    self.fetch_activity_data();
    self.fetch_lead_stage_proposal_count();
    
    if (self.user_group == '1' || self.user_group == '6' || self.user_group == '7') {
        $('#sales_target_container').removeClass('hidden');
        if (self.user_group == '1'){
            var userid = $('#userid').val();
            if(userid == '' || userid == null){
                $('#sales_target_container').addClass('hidden');
            }
        }
        self.fetch_target_data();
    }
};

dashboard_reporting_manager.prototype.create_statisitics_card = function (data) {
    var card = document.createElement('div');
    card.className = 'card';

    var card_body = document.createElement('div');
    card_body.className = 'card-body';

    var card_body_title = document.createElement('div');
    card_body_title.className = "text-center";
    card_body_title.innerHTML = "<h3>" + data.title + "</h3>"

    card_body.appendChild(card_body_title);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'No. Sales: <span class="text-success"> ' + data.deals_data[0].no_of_sales + '</span>';
    card_body.appendChild(no_of_sales);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'Total Revenue: <span class="text-success"> $' + parseFloat(data.deals_data[0].total_revenue).toFixed(2);
    +'</span>';
    card_body.appendChild(no_of_sales);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'Total kW\'s: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_kws).toFixed(2) + ' kW</span>';
    //card_body.appendChild(no_of_sales);


    var highbay = document.createElement('div');
    highbay.className = "text-muted";
    highbay.innerHTML = 'Highbay: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_highbay) + '</span>';
    card_body.appendChild(highbay);

    var panel_light = document.createElement('div');
    panel_light.className = "text-muted";
    panel_light.innerHTML = 'Panel: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_panel_light) + '</span>';
    card_body.appendChild(panel_light);

    var battens = document.createElement('div');
    battens.className = "text-muted";
    battens.innerHTML = 'Battens: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_batten_light) + '</span>';
    card_body.appendChild(battens);

    var flood_light = document.createElement('div');
    flood_light.className = "text-muted";
    flood_light.innerHTML = 'Floodlight: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_flood_light) + '</span>';
    card_body.appendChild(flood_light);

    var down_light = document.createElement('div');
    down_light.className = "text-muted";
    down_light.innerHTML = 'Downlight: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_down_light) + '</span>';
    card_body.appendChild(down_light);

    var other = document.createElement('div');
    other.className = "text-muted";
    other.innerHTML = 'Other: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_other) + '</span>';
    card_body.appendChild(other);

    var card_container = document.createElement('div');
    card_container.className = "col-xl-4 col-lg-4 col-12";

    card.appendChild(card_body);
    card_container.appendChild(card);

    return card_container;
};

dashboard_reporting_manager.prototype.create_conversion_row = function (data) {
    var no_of_new_quotes = data.stats[0].deals_data[0].no_of_sales;
    var no_of_won_quotes = data.stats[1].deals_data[0].no_of_sales;
    var no_of_lost_quotes = data.stats[2].deals_data[0].no_of_sales;
    var total_quotes = parseInt(no_of_new_quotes) + parseInt(no_of_won_quotes) + parseInt(no_of_lost_quotes);

    var tr = document.createElement('tr');
    var td1 = document.createElement('td');
    td1.innerHTML = total_quotes;

    var td2 = document.createElement('td');
    td2.innerHTML = no_of_won_quotes;

    var td3 = document.createElement('td');
    td3.className = 'text-center';
    var conv_rate = (total_quotes > 0) ? (parseFloat(no_of_won_quotes / total_quotes) * 100).toFixed(2) : 0;
    var conv_rate_class = (conv_rate > 70) ? 'bg-success' : 'bg-warning';
    td3.innerHTML = conv_rate + '%';

    var progress = document.createElement('div');
    progress.className = 'progress progress-sm mt-1 mb-0';
    var progress_bar = document.createElement('div');
    progress_bar.className = 'progress-bar ' + conv_rate_class;
    progress_bar.setAttribute('style', 'width:' + conv_rate + '%');

    progress.appendChild(progress_bar);
    td3.appendChild(progress);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);

    return tr;

};

dashboard_reporting_manager.prototype.fetch_statistics_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_statistics_data';

    $.ajax({
        url: base_url + "admin/reports/dashboard/fetch_statistics_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('statistics').innerHTML = '';
            document.getElementById('conversion_table_body').innerHTML = '';
            if (data.stats.length > 0) {
                for (var i = 0; i < data.stats.length; i++) {
                    var stats = self.create_statisitics_card(data.stats[i]);
                    document.getElementById('statistics').appendChild(stats);
                }
                //Create Conversion table row
                var row = self.create_conversion_row(data);
                document.getElementById('conversion_table_body').appendChild(row);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('conversion_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

/** Proposal summary Functions */
dashboard_reporting_manager.prototype.create_proposal_count_list_group = function (count, id) {
    var list_group = document.createElement('ul');
    list_group.className = "list-group";

    var list_group_item = document.createElement('li');
    list_group_item.className = "list-group-item d-flex justify-content-between align-items-center";
    list_group_item.innerHTML = 'Proposals in progress <span class="badge badge-primary badge-pill">' + count + '</span>';

    list_group.appendChild(list_group_item);

    document.getElementById(id).innerHTML = '';
    document.getElementById(id).appendChild(list_group);
};

dashboard_reporting_manager.prototype.create_led_statistics_table_row = function (data) {
    var tr_head = document.createElement('tr');
    var tr_body = document.createElement('tr');

    var stages = data.lead_stages;
    for (var key in stages) {
        if (key != 4 && key < 10) {
            var td = document.createElement('td');
            td.className = 'text-center';
            td.setAttribute('data-stage-id', stages[key].id);
            td.innerHTML = stages[key].stage_name;
            tr_head.appendChild(td);
        }
    }

    var i = 1;
    var stat = data.led_statistics;
    for (var key in stat) {
        if (i !== 5) {
            var td = document.createElement('td');
            td.className = 'text-center';
            td.innerHTML = stat[key];
            tr_body.appendChild(td);
        }
        i++;
    }

    document.getElementById('led_statistics_table_head').appendChild(tr_head);
    document.getElementById('led_statistics_table_body').appendChild(tr_body);
};

dashboard_reporting_manager.prototype.create_solar_statistics_table_row = function (data) {
    var tr_head = document.createElement('tr');
    var tr_body = document.createElement('tr');

    var stages = data.lead_stages;
    for (var key in stages) {
        if (key != 4 && key < 10) {
            var td = document.createElement('td');
            td.className = 'text-center';
            td.setAttribute('data-stage-id', stages[key].id);
            td.innerHTML = stages[key].stage_name;
            tr_head.appendChild(td);
        }
    }

    var i = 1;
    var stat = data.solar_statistics;
    for (var key in stat) {
        if (i !== 5) {
            var td = document.createElement('td');
            td.className = 'text-center';
            td.innerHTML = stat[key];
            tr_body.appendChild(td);
        }
        i++;
    }

    document.getElementById('solar_statistics_table_head').appendChild(tr_head);
    document.getElementById('solar_statistics_table_body').appendChild(tr_body);
};

dashboard_reporting_manager.prototype.fetch_lead_stage_proposal_count = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_activity_data';

    $.ajax({
        url: base_url + "admin/reports/dashboard/fetch_lead_stage_proposal_count?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('led_statistics_table_body').innerHTML = '';
            document.getElementById('led_statistics_table_head').innerHTML = '';
            document.getElementById('solar_statistics_table_head').innerHTML = '';
            document.getElementById('solar_statistics_table_body').innerHTML = '';
            if (data.led_statistics) {
                self.create_led_statistics_table_row(data);
            }
            if (data.led_prg_proposal_count) {
                self.create_proposal_count_list_group(data.led_prg_proposal_count, 'led_general_stats');
            }
            if (data.solar_statistics) {
                self.create_solar_statistics_table_row(data);
            }
            if (data.solar_prg_proposal_count) {
                self.create_proposal_count_list_group(data.solar_prg_proposal_count, 'solar_general_stats');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('led_statistics_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

/** Activity Reporting Functions */

dashboard_reporting_manager.prototype.fetch_activity_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_activity_data';

    $.ajax({
        url: base_url + "admin/reports/dashboard/fetch_activity_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('activity_statistics').innerHTML = '';
            document.getElementById('activity_devision').innerHTML = '';
            document.getElementById('scheduled_activities_table_body').innerHTML = '';


            google.charts.load('current', {'packages': ['corechart']});

            if (data.activity.length > 0) {
                google.charts.setOnLoadCallback(function () {
                    self.create_activity_chart(data.activity);
                });

                google.charts.setOnLoadCallback(function () {
                    self.create_activity_devision_chart(data.activity);
                });
            }
            //Create Scheduled table rows
            if (data.scheduled_activity.length > 0) {
                if ($('#scheduled_activities_table')) {
                    $('#scheduled_activities_table').DataTable().destroy();
                    document.getElementById('scheduled_activities_table_body').innerHTML = '';
                }
                for (var i = 0; i < data.scheduled_activity.length; i++) {
                    var row = self.create_scheduled_activities_data(data.scheduled_activity[i]);
                    document.getElementById('scheduled_activities_table_body').appendChild(row);
                }
                $('#scheduled_activities_table').DataTable({
                    "order": [
                        [0, "asc"]
                    ],
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": true,
                });
            } else {
                document.getElementById('scheduled_activities_table_body').innerHTML = '<tr><td colspan="5" class="text-center"> No Scheduled Activiites Found </td></tr>';
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('activity_statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('scheduled_activities_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

dashboard_reporting_manager.prototype.create_activity_chart = function (report_data) {
    var list = [];
    list.push(["Date", "Note", "Visit", "Email", "Phone"]);
    for (var i = 0; i < report_data.length; i++) {
        list[(i + 1)] = [report_data[i].created_at, parseInt(report_data[i].note_count), parseInt(report_data[i].visit_count)
                    , parseInt(report_data[i].email_count), parseInt(report_data[i].phone_count)];
    }


    var data = google.visualization.arrayToDataTable(list, false);
    var options = {
        title: 'Actitivty Statistics'
    };
    var chart = new google.visualization.LineChart(document.getElementById('activity_statistics'));
    chart.draw(data, options);
}

dashboard_reporting_manager.prototype.create_activity_devision_chart = function (report_data) {
    var list = [];
    list.push(["Activity", "Count"]);
    for (var i = 0; i < report_data.length; i++) {
        list[(i + 1)] = [report_data[i].acitivity_type + " (" + report_data[i].count + ")", parseInt(report_data[i].count)];
    }
    var note_count = 0;
    var visit_count = 0;
    var email_count = 0;
    var phone_count = 0;


    for (var i = 0; i < report_data.length; i++) {
        note_count += parseInt(report_data[i].note_count);
        visit_count += parseInt(report_data[i].visit_count);
        email_count += parseInt(report_data[i].email_count);
        phone_count += parseInt(report_data[i].phone_count);
    }

    list[1] = ['Note', parseInt(note_count)];
    list[2] = ['Visit', parseInt(visit_count)];
    list[3] = ['Email', parseInt(email_count)];
    list[4] = ['Phone', parseInt(phone_count)];

    var data = google.visualization.arrayToDataTable(list, false);
    var options = {
        title: 'Actitivty Division'
    };
    var chart = new google.visualization.PieChart(document.getElementById('activity_devision'));
    chart.draw(data, options);
};

dashboard_reporting_manager.prototype.create_scheduled_activities_data = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.first_name + ' ' + data.last_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.full_name;

    var td3 = document.createElement('td');
    td3.innerHTML = (data.scheduled_at != null) ? data.scheduled_at : '-';

    var td4 = document.createElement('td');
    td4.innerHTML = data.activity_type;


    if (data.days_left != null) {
        var badge_class = (data.status == 1) ? 'success' : ((data.days_left < 0) ? 'danger' : ((data.days_left > 0) ? 'warning' : 'info'));
        var badge_text = (data.status == 1) ? 'Completed' : ((data.days_left < 0) ? 'Past' : ((data.days_left > 0) ? 'Upcoming' : 'Today'));
    } else {
        var badge_class = 'info';
        var badge_text = 'Not Scheduled';
    }

    var badge = document.createElement('span');
    badge.className = 'badge badge-default badge-' + badge_class;
    badge.innerHTML = badge_text;

    var td5 = document.createElement('td');
    td5.appendChild(badge);

    var td6 = document.createElement('td');
    td6.className = "text-center";
    var mark_as_completed_btn = '<a href="javascript:void(0);" data-id="' + data.id + '" data-userid="' + data.user_id + '"  class="mark_as_completed_activity"> <i class="fa fa-check text-success"></i></a>';
    mark_as_completed_btn = (data.days_left < 0 && data.status == 0) ? mark_as_completed_btn : ((data.days_left == 0 && data.status == 0) ? mark_as_completed_btn : '-');
    td6.innerHTML = mark_as_completed_btn;

    var td7 = document.createElement('td');
    td7.className = "text-center";
    var url = base_url + 'admin/proposal/edit/' + data.uuid;
    td7.innerHTML = ((data.user_id == self.user_id) || self.user_group == 1) ? '<a href="' + url + '" target="__blank"> <i class="fa fa-pencil"></i></a>' : '-';


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    //tr.appendChild(td6);
    //tr.appendChild(td7);

    return tr;
};

dashboard_reporting_manager.prototype.mark_as_completed_activity = function () {
    var self = this;
    $(document).on('click', '.mark_as_completed_activity', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        var userid = $this.attr('data-userid');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/proposal/mark_as_completed_proposal_activity',
            datatype: 'json',
            data: 'id=' + id + '&userid=' + userid + '&action=mark_as_completed',
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    var prev_td = $this.parent().closest('td').prev('td');
                    prev_td.html('<span class="badge badge-default badge-success">Completed</span>');
                    $this.parent().closest('td').html('-');
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

dashboard_reporting_manager.prototype.get_past_months = function (data) {
    var self = this;
    var months = [];
    var start = parseInt(data.month_year_range.start);
    var end = parseInt(data.month_year_range.end);

    for (var i = start; i < end; i++) {
        months.push(moment().subtract(i, 'months').format('MMM YYYY'));
    }
    return months;
};

dashboard_reporting_manager.prototype.create_rep_target_table = function (data) {
    var self = this;

    var table = document.createElement('table');
    table.className = 'table_target';
    table.setAttribute('id', 'target_table');

    var thead = document.createElement('thead');

    var months = self.get_past_months(data);

    var theads_array = [''];

    var tr = document.createElement('tr');
    for (var i = 0; i < theads_array.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = theads_array[i];
        tr.appendChild(th);
        thead.appendChild(tr);
    }


    for (var i = (months.length - 1); i >= 0; i--) {
        var th = document.createElement('th');
        th.innerHTML = months[i];
        th.className = 'css-month';
        tr.appendChild(th);
        thead.appendChild(tr);
    }

    table.appendChild(thead);
    return table;
};

dashboard_reporting_manager.prototype.create_rep_target_table_body_column1 = function (data, tbody) {
    var self = this;
    var items_data = data.items;
    var statistics_data = data.statistics;

    for (var i = 0; i < items_data.length; i++) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.innerHTML = items_data[i].name;
        td.className = items_data[i].class + ' item_title';
        tr.className = items_data[i].alias;
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
};

dashboard_reporting_manager.prototype.handle_color = function (per) {
    per = parseInt(per);
    if (per >= 0 && per < 80) {
        return 'red';
    } else if (per >= 80 && per <= 99) {
        return 'yellow';
    } else if (per > 100) {
        return 'green';
    }
};

dashboard_reporting_manager.prototype.create_rep_target_table_body_column = function (data) {
    var self = this;
    var items_data = data.items;
    var months = data.months;
    var years = data.years;
    var statistics_data = data.statistics;
    var target_data = data.target_data;

    var target = [0, 0, 0, 0];
    var weighting = [0, 0, 0, 0];
    
    for (var j = 0, k = 23; j < statistics_data.length; j++, k--) {
        for (var i = 0; i < items_data.length; i++) {
            var tr = document.querySelector('.' + items_data[i].alias);
            var td = document.createElement('td');
            if (items_data[i]['editable']) {
                var value = (statistics_data[k][items_data[i].alias] != undefined) ? statistics_data[k][items_data[i].alias] : 0;
                var input = document.createElement('input');
                input.setAttribute('name', 'report_data[' + items_data[i].alias + ']');
                input.setAttribute('data-m', months[k]);
                input.setAttribute('data-y', years[k]);
                input.setAttribute('type', 'number');
                input.setAttribute('min', 0);
                input.setAttribute('value', value);
                input.className = 'target_input';
                td.appendChild(input);
            } else {
                var value = (statistics_data[k][items_data[i].alias] != undefined) ? statistics_data[k][items_data[i].alias] : 0;
                if (items_data[i].alias == 'percentage_to_target') {
                    if (j >= parseInt(target_data[0].target_start_date)  && j <= parseInt(target_data[0].target_end_date)) {
                        target = target_data[0].target;
                        weighting = target_data[0].weighting;
                    } else if (j >= parseInt(target_data[1].target_start_date)  && j <= parseInt(target_data[1].target_end_date)) {
                        target = target_data[1].target;
                        weighting = target_data[1].weighting;

                    } else if (j >= parseInt(target_data[2].target_start_date)  && j <= parseInt(target_data[2].target_end_date)) {
                        target = target_data[2].target;
                        weighting = target_data[2].weighting;
                    }
                    target['hb_submitted_to_ap'] = parseFloat(target['hb_submitted_to_ap']);
                    target['p_b_t_installed'] = parseFloat(target['p_b_t_installed']);
                    target['floodlight_submitted'] = parseFloat(target['floodlight_submitted']);
                    target['solar_approved'] = parseFloat(target['solar_approved']);
                    weighting['hb_submitted_to_ap'] = parseFloat(weighting['hb_submitted_to_ap']);
                    weighting['p_b_t_installed'] = parseFloat(weighting['p_b_t_installed']);
                    weighting['floodlight_submitted'] = parseFloat(weighting['floodlight_submitted']);
                    weighting['solar_approved'] = parseFloat(weighting['solar_approved']);
                    
                    var hb_submitted_to_ap = (statistics_data[k]['hb_submitted_to_ap'] == 0) ? 0 : parseFloat(statistics_data[k]['hb_submitted_to_ap']);
                    var aggregate_hb_submitted_to_ap = (target['hb_submitted_to_ap'] == 0) ? 0 : (hb_submitted_to_ap /target['hb_submitted_to_ap']) * weighting['hb_submitted_to_ap'];
                    
                    var p_b_t_installed = (statistics_data[k]['p_b_t_installed'] == 0) ? 0 : parseFloat(statistics_data[k]['p_b_t_installed']);
                    var aggregate_p_b_t_installed = (target['p_b_t_installed'] == 0) ? 0 : (p_b_t_installed /target['p_b_t_installed']) * weighting['p_b_t_installed'];
                    
                    var floodlight_submitted = (statistics_data[k]['floodlight_submitted'] == 0) ? 0 : parseFloat(statistics_data[k]['floodlight_submitted']);
                    var aggregate_floodlight_submitted = (target['floodlight_submitted'] == 0) ? 0 : (floodlight_submitted / target['floodlight_submitted']) * weighting['floodlight_submitted'];
                    
                    var solar_approved = (statistics_data[k]['solar_approved'] == 0) ? 0 : parseFloat(statistics_data[k]['solar_approved']);
                    var aggregate_solar_approved = (target['solar_approved'] == 0) ? 0 : (solar_approved /target['solar_approved']) * weighting['solar_approved'];
                    /**if(j == 7){
                        console.log('HB-' + statistics_data[k]['hb_submitted_to_ap']);
                        console.log('PBT-' + statistics_data[k]['p_b_t_installed']);
                        console.log('FL-' + statistics_data[k]['floodlight_submitted']);
                        console.log('S-' + statistics_data[k]['solar_approved']);
                        console.log(target['floodlight_submitted']);
                        console.log(weighting['floodlight_submitted']);
                        console.log(aggregate_hb_submitted_to_ap);
                        console.log(aggregate_p_b_t_installed);
                        console.log(aggregate_floodlight_submitted);
                        console.log(aggregate_solar_approved);
                    }*/
                    var total = parseFloat(aggregate_hb_submitted_to_ap) + parseFloat(aggregate_p_b_t_installed) + parseFloat(aggregate_floodlight_submitted) + parseFloat(aggregate_solar_approved);
                    td.innerHTML = Math.round(total * 100) + '%';
                    td.className = (Math.round(total * 100) == 0) ? '' : items_data[i].class + ' kg-tbl_col--' + self.handle_color(Math.round(total * 100));
                } else {
                    td.innerHTML = (value == 0) ? '' : Math.round(value);
                    td.className = items_data[i].class;
                }
            }

            tr.appendChild(td);
        }
    }

};

dashboard_reporting_manager.prototype.fetch_target_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/reports/dashboard/fetch_target_data',
        data:form_data,
        datatype: 'json',
        beforeSend: function () {
            document.getElementById('target_table_container').innerHTML = '';
            document.getElementById('target_table_container').appendChild(createPlaceHolder());
            document.getElementById('target_table_container').appendChild(createPlaceHolder());
            document.getElementById('target_table_container').appendChild(createPlaceHolder());
        },
        success: function (stat) {
            document.getElementById('target_table_container').innerHTML = '';
            stat = JSON.parse(stat);
            var target_table = self.create_rep_target_table(stat);
            var tbody = document.createElement('tbody');
            self.create_rep_target_table_body_column1(stat, tbody);
            target_table.appendChild(tbody);
            document.getElementById('target_table_container').appendChild(target_table);
            self.create_rep_target_table_body_column(stat, tbody);
            self.save_target_data();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

dashboard_reporting_manager.prototype.save_target_data = function () {
    var self = this;
    $('.target_input').change(function () {
        $this = $(this);
        var data = {};
        data[$this.attr('name')] = $this.val();
        data['report_data[year]'] = $this.attr('data-y');
        data['report_data[month]'] = $this.attr('data-m');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/reports/dashboard/save_target_data',
            data: data,
            datatype: 'json',
            beforeSend: function () {
                document.getElementById('target_table_container').innerHTML = '';
                document.getElementById('target_table_container').appendChild(createPlaceHolder());
                document.getElementById('target_table_container').appendChild(createPlaceHolder());
                document.getElementById('target_table_container').appendChild(createPlaceHolder());
            },
            success: function (stat) {
                document.getElementById('target_table_container').innerHTML = '';
                stat = JSON.parse(stat);
                var target_table = self.create_rep_target_table();
                var tbody = document.createElement('tbody');
                self.create_rep_target_table_body_column1(stat, tbody);
                target_table.appendChild(tbody);
                document.getElementById('target_table_container').appendChild(target_table);
                self.create_rep_target_table_body_column(stat, tbody);
                self.save_target_data();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });
};


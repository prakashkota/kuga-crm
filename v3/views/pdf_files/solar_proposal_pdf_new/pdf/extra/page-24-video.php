<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page24 .back-bg {
		background-color: #FFFFFF;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page24 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 610px;
		padding-top: 30px;
	}
	
	.page24 .top hr {
		width: 70%;
		height: 2px;
		margin: -24px 0px 0px 0px;
		background-color: #D01E2A;
	}
	
	.page24 .text_color {
		color: #D01E2A;
	}
	
	.page24 .title {
		margin-top: -50px;
		margin-bottom: : 30px;
	}
	
	.page24 .title h1 {
		padding-left: 14px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
		border-height: 10px;
	}
	
	.page24 .col-4 {
		max-width: 32.5%;
		display: inline-block;
	}
	
	.page24 .imagesbox {
		margin-bottom: 30px;
	}
	
	.page24 .imagesbox img {
		width: 100%;
	}
	
	.page24 .imagesbox h2 {
		font-size: 16px;
		line-height: 1.3;
	}
	
	.page24 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page24 .images img {
		width: 100%;
	}
	
	.page24 .imagese img {
		margin-left: 650px;
		width: 230;
	}
	
	.page24 .socialbox .boxfrist p {
		display: inline-block;
		margin-left: 15px;
		vertical-align: super;
	}
	
	.page24 .socialbox .boxfrist img {
		display: inline-block;
		width: 90px;
	}
	
	.page24 .paddsec {
		padding: 0 40px;
	}
	
	.page24 .imagesbox iframe {
		border: 0;
	}
	</style>
</head>

<body>
	<div class="back-bg page24">
		<div class="paddsec">
			<div class="top">
				<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
				<hr>
			</div>
			<div class="title">
				<h1>MEDIA</h1>
				
			</div>
			
			<div class="banntext">
				<h2>See our work and installation on Youtube</h2> </div>
			<div class="imagessec">
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/jdt36OuhyfY"> </iframe>
						<h2>75kW Solar PV system - Senghork Foods</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/6nJZpJHsrgk"> </iframe>
						<h2>100kW Solar PV system - DeFries Industries</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/vP0kNLpUK2g"> </iframe>
						<h2>Technical Challanges Higlight - DeFies Industries</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/tc3kdH3FXQM"> </iframe>
						<h2>LED Upgrade - Autobarn Day 1 Saving</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/n1P_Wub40LA"> </iframe>
						<h2>LED Upgrade - AMC Equestrian CEnter</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/y_SHfzapCGk"> </iframe>
						<h2>LED Upgrade - Patterson Cheney Toyota multi site</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/M-afg87EvSs"> </iframe>
						<h2>LED Upgrade - Collingwood FC Holden Centre </h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/yZuUUzxSyQE"> </iframe>
						<h2>40kW Solar PV System - Quipworx Tullamarine</h2> </div>
				</div>
				<div class="col-4">
					<div class="imagesbox">
						<iframe width="265" height="160" src="https://www.youtube.com/embed/0aryAHoiUbQ"> </iframe>
						<h2>LED Upgrad - Aussie DIsposals Multi Site</h2> </div>
				</div>
			</div>
			<div class="socialmed">
				<h2> Follow us on social</h2>
				<p>We regularly post new contents on our social media channels, follow us for the latest news in the industry.</p>
			</div>
			<div class="socialbox">
				<div class="boxfrist">
					<a href="https://www.youtube.com/channel/UCNvIeQncB97SLR8rlnXLZQA"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/younew.jpg'); ?>"></a>
					<p>Search: "Youtub Kuga Electrical"</p>
				</div>
				<div class="boxfrist">
					<a href="https://www.facebook.com/13kuga"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/fac.jpg'); ?>"></a>
					<p>Search: "Facebook Kuga Electrical"</p>
				</div>
				<div class="boxfrist">
					<a href="https://www.linkedin.com/company/kuga-electrical/?originalSubdomain=au"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/linkd.jpg'); ?>"></a>
					<p>Search: "Linkedin Kuga Electrical"</p>
				</div>
			</div>
		</div>
		<div class="imagese" style="margin-bottom: -66px;">
			<a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a>
		</div>
		<div class="footer"></div>
	</div>
</body>

</html>
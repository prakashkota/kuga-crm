<?php
$o = $t = array();
$tilt = $orientation = '';
$panel_id = '';
$panel_model_type = '';
$inverter_model_type = '';

if (count($mappingPanelObjects)) {
  for ($i = 0; $i < count($mappingPanelObjects); $i++) {

    $object_properties = json_decode($mappingPanelObjects[$i]['object_properties']);
    $panel_id = $object_properties->sp_panel_id;
    if (!in_array($object_properties->sp_rotation, $o)) {
      $o[] = $object_properties->sp_rotation;
    }
    if (!in_array($object_properties->sp_tilt, $t)) {
      $t[] = $object_properties->sp_tilt;
    }
  }


  $o = empty($o) ? array(0) : $o;
  $t = empty($t) ? array(0) : $t;
  $orientation = implode('&deg;/ ', $o);
  $tilt = implode('&deg;/ ', $t);

  for ($i = 0; $i < count($solar_panels); $i++) {
    if ($solar_panels[$i]['id'] === $panel_id) {
      $panel_model_type = $solar_panels[$i]['name'];
      break;
    }
  }
}
?>
<style>
  @font-face {
    font-family: 'CenturyGothic';
    font-weight: normal;
    src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
    src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
    url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
    url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
  }
  @font-face {
    font-family: 'CenturyGothic';
    src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
    src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
    url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
    url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
    font-weight: bold;
  }
  body{
    font-family: 'CenturyGothic';
    margin: 0;
    padding: 0;
  }

  .page5  .top h2 {
   font-size: 16px;
   color: #221E1F;
   padding-left: 1044px;
 }
 .page8  .top hr {
   width: 74%;
   height: 1px;
   margin: -24px 0px 0px 52px;
   background-color: #D01E2A;
 }
 .page8  .text_color {
   color: #D01E2A;
 }
 .page8  .title h1 {
   padding-left: 14px;
   margin-left: 70px;
   margin-top: 30px;
   border-left: 5px solid #D01E2A;
 }
 .page8  .title h2 {
   padding: 10px;
   margin-left: 70px;
   margin-top: 50px;
   background-color: #D01E2A;
   width: 47%;
   color: #ffffff;
   font-size: 18px;
 }
 .page8  .item_summary_table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 83%;
   margin-left: 70px;
 }
 .page8 .item_summary_table tr {
   border-bottom: 2px solid #D01E2A;
 }
 .page8  .item_summary_table th {
   text-align: left;
   padding-left: 50px;
 }
 .page8  .item_summary_table td {
   padding: 15px;
   color: #3D3B3C;
 }
 .page8  .footer {
   display:inline-flex;
 }
 .page8  .image img {
   margin-top: 0px;
   margin-left: 23px;
 }
 .page8  .fimagee img {
  margin-left: 1100px;
  background: #fff;
}
.page8  .bb-none {
  border-bottom: none !important;
}

.page5 .redf {
  height: 10px;
  width: 850px;
  background: red;
}
.page5 .greyf {
  height: 10px;
  width: 440px;
  background: #dcdcdc;
}
</style>
<table border="0" cellpadding="0" cellspacing="0" class="page8 page5" width="100%">
  <tr><td style="height:40px;">&nbsp;</td></tr>
  <tr>
    <td valign="top" class="top">
      <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
      <hr>
    </td>
  </tr>
  <tr style="height: 40px;">
   <td class="title" valign="top">
    <h1>YOUR SOLAR  <span class="text_color"> MOUNT KIT INSTALLATION</span></h1>
  </td>
</tr>
<tr style="height: 450px;" valign="top">
  <td style="padding-left: 70px;" valign="top">
    <table>
      <tr>
        <td valign="top" style="padding-right: 40px;">
          <img src="<?php echo $mapping_tool['snapshot2']; ?>" width="800" height="600"/>
        </td>
        <td valign="top" style="border: 1px solid #c5c5c5; padding: 12px; margin-left:30px;" width="300px">
          <?php echo $mapping_tool['notes']; ?>
        </td>
      </tr>
    </table>
  </td>
</tr>

<tr style="height: 145px;">
  <td class="title" valign="top">
   &nbsp;
 </td>
</tr>

<tr>
  <td style="padding-bottom:10px;">
    <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="https://kugacrm.com.au/assets/solar_proposal_pdf_new/logo-transparent.png" width="150px"></a> </div>
  </td>
</tr>

<tr>
  <td>
    <table>
      <tr>
        <td>
          <div class="redf"></div>
        </td>
        <td>
          <div class="greyf"></div>
        </td>
      </tr>
    </table>
  </td>
</tr>
</table>
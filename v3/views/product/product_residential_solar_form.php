<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Franchise - <?php } ?>Add Product</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" name="productAdd" id="productAdd" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card" style="min-height: 481px;">
                                <div class="card-body">
                                    <h4 class="form-section">Product Details</h4>
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label>Product Name</label>
                                                <input type="text" name="name"  class="form-control" placeholder="Product Name" value="<?php echo (!empty($product_data)) ? $product_data['name'] : ''; ?>"  required readonly="">
                                                <?php echo form_error('name', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label>Product Type</label>
                                                <select name="type_id" class="form-control" id="type_id" required>
                                                    <?php foreach ($product_type as $key => $value) { ?>
                                                        <option value="<?php echo $value['typeId']; ?>" <?php echo (!empty($product_data) && $product_data['type_id'] == $value['typeId']) ? 'selected' : ''; ?>><?php echo $value['typeName']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('type_id', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label>Brand</label>
                                                <input type="text" name="brand"  class="form-control" placeholder="Brand" value="<?php echo (!empty($product_data)) ? $product_data['brand'] : ''; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Description</label>
                                                <input type="text" name="description"  class="form-control" placeholder="Description" value="<?php echo (!empty($product_data)) ? $product_data['description'] : ''; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Model Number</label>
                                                <input type="text" name="model_no"  class="form-control" placeholder="Model Number" value="<?php echo (!empty($product_data)) ? $product_data['model_no'] : ''; ?>">
                                            </div>

                                        </div>


                                        <div class="col-md-6">

                                            <div class="form-group hidden">
                                                <label>Length</label>
                                                <input type="number" step="0.001" min="0" oninput="this.value=(this.value < 0) ?'' : this.value" name="length"  class="form-control"   placeholder="Length" value="<?php echo (!empty($product_data)) ? $product_data['length'] : ''; ?>">
                                            </div> 

                                            <div class="form-group hidden">
                                                <label>Width</label>
                                                <input type="number" step="0.001" min="0" oninput="this.value=(this.value < 0) ?'' : this.value" name="width"  class="form-control"   placeholder="Width" value="<?php echo (!empty($product_data)) ? $product_data['width'] : ''; ?>">
                                            </div>

                                            <div class="form-group ">
                                                <label>Amount of Phases</label>
                                                <select name="amount_phase"  class="form-control">
                                                    <option value="" >Select Phase</option>
                                                    <?php $inverter_phases = unserialize(INVERTER_PHASES);
                                                    foreach ($inverter_phases as $key => $value) { ?>
                                                        <option value="<?php echo $key; ?>" <?php echo (!empty($product_data) && $product_data['amount_phase'] == $key) ? 'selected' : ''; ?>><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Wattage</label>
                                                <input type="number" step="0.001" min="0" oninput="this.value=(this.value < 0) ?'' : this.value" name="capacity"  class="form-control"   placeholder="Wattage" value="<?php echo (!empty($product_data)) ? $product_data['capacity'] : ''; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Product Warranty</label>
                                                <input type="number"  min="0"  name="product_warranty"  class="form-control"   placeholder="Product Warranty" value="<?php echo (!empty($product_data)) ? $product_data['product_warranty'] : ''; ?>">
                                            </div>

                                            <div class="form-group hidden">
                                                <label>Performance Warranty</label>
                                                <input type="number"  min="0"  name="performance_warranty"  class="form-control"   placeholder="Performance Warranty" value="<?php echo (!empty($product_data)) ? $product_data['performance_warranty'] : ''; ?>">
                                            </div>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>



                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Product Additional Details</h4>
                                    <div class="row">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <textarea type="text" name="comments"  class="form-control"   placeholder="Comments"><?php echo (!empty($product_data)) ? $product_data['comments'] : ''; ?></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control">Available</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <div class="d-inline-block custom-control custom-radio mr-1">
                                                    <input type="radio" name="availability" class="custom-control-input" checked="" id="av_yes" value="1" <?php if (!empty($product_data) && $product_data['availability'] == 1) { ?> checked="checked" <?php } ?>>
                                                    <label class="custom-control-label" for="av_yes">Yes</label>
                                                </div>
                                                <div class="d-inline-block custom-control custom-radio">
                                                    <input type="radio" name="availability" class="custom-control-input" id="av_no" value="0" <?php if (!empty($product_data) && $product_data['availability'] == 0) { ?> checked="checked" <?php } ?>>
                                                    <label class="custom-control-label" for="av_no">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="datasheets_container">
                                     <label class="col-md-4 label-control">DataSheets</label>
                                     <div class="col-md-12">
                                        <div class="custom-file">
                                            <input type="file" name="product_file"  class="custom-file-input" >
                                            <input type="hidden" name="datasheets" id="datasheets" class="form-control" value="<?php echo (!empty($product_data)) ? $product_data['datasheets'] : ''; ?>">
                                            <label class="custom-file-label" for="datasheets"><?php echo (!empty($product_data) && $product_data['datasheets'] != '') ? $product_data['datasheets'] : 'Choose file'; ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label>SpecSheet Attachment <span class="text-muted small bold">(Size Required : 910 width * 1287 height)</span></label>
                                    <div class="custom-file">
                                        <input type="file" name="attachment_file"  class="custom-file-input" >
                                        <input type="hidden" name="attachment" id="attachment" class="form-control" value="<?php echo (!empty($product_data)) ? $product_data['attachment'] : ''; ?>">
                                        <label class="custom-file-label attachment_file" for="attachment_file"><?php echo (!empty($product_data) && $product_data['attachment'] != '') ? $product_data['attachment'] : 'Choose file'; ?></label>
                                    </div>
                                </div>
                                <div id="loader"></div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!--Variants -->
                                <h4 class="form-section">Product Variants</h4>
                                <div class="form-block d-block clearfix" style="margin:0 auto;">
                                    <div class="table-default">
                                        <table width="100%" border="0">
                                            <thead>
                                                <tr>
                                                    <td>State</td>
                                                    <td>Application</td>
                                                    <td>Cost Price</td>
                                                    <td>Is Amount Set?</td>
                                                    <td>Amount</td>
                                                    <td>Additional Margin</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody id="product_variants_wrapper">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button id="add_product_variants" type="button" class="btn btn-primary" > <i class="fa fa-plus"></i> Add Variant </button>
                                </div>
                                <!-- Variants End -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttwrap" style="float:right;">
                    <input type="submit"   class="btn" value="SAVE">
                </div>                                            
            </form>

        </div>
    </div>
    <?php if(!empty($product_data) && $product_data['attachment'] != ''){ ?>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                     <h4 class="form-section">Spec Sheet</h4>
                    <a href="<?php echo site_url('assets/uploads/product_files/'.$product_data['attachment']); ?>" target="__blank">
                        <img src="<?php echo site_url('assets/uploads/product_files/'.$product_data['attachment']); ?>" style="max-height:711px;" >
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</div>
<script>

    $(document).ready(function () {

    $('input[name=product_file]').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $('input[name=product_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: base_url + 'admin/product/upload_datasheet',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#datasheets').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
        $('#loader').html('');
    });

    $("#type_id").change(function () {
        $("input[name=length]").parent().addClass('hidden');
        $("input[name=width]").parent().addClass('hidden');
        $("select[name=amount_phase]").parent().addClass('hidden');
        $("input[name=performance_warranty]").parent().addClass('hidden');
        $("input[name=capacity]").parent().addClass('hidden');
        $("input[name=product_warranty]").parent().addClass('hidden');
        $("#datasheets_container").addClass('hidden');
        if ($(this).val() == '2') {
            $("input[name=length]").parent().removeClass('hidden');
            $("input[name=width]").parent().removeClass('hidden');
            $("input[name=performance_warranty]").parent().removeClass('hidden');
        } else if ($(this).val() == '1') {
            $("select[name=amount_phase]").parent().removeClass('hidden');
        }
        var allowed_types = [1,2,3];
        if(allowed_types.includes(parseInt($(this).val()))){
            $("input[name=capacity]").parent().removeClass('hidden');
            $("input[name=product_warranty]").parent().removeClass('hidden');
            $("#datasheets_container").removeClass('hidden');
        }
    });

    $('input[name=is_price_set]').click(function () {
        if ($(this).val() == 1) {
            $('#product_amount_row').show();
        } else {
            $('#product_amount_row').hide();
        }
    });

    if ($('#product_list').length > 0) {
        $('#product_list').DataTable({
            "order": [[1, "asc"], [2, "asc"], [3, "asc"]],
        });
    }

    $('#product_csv_file_import').click(function () {
        var file_data = $('input[name=product_csv_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('product_csv_file', file_data);
        form_data.append('action', 'import_product_csv');
        import_csv(form_data);
    });

    $('#product_variant_csv_file_import').click(function () {
        var file_data = $('input[name=product_variant_csv_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('product_variant_csv_file', file_data);
        form_data.append('action', 'import_product_variant_csv');
        import_csv(form_data);
    });

    $('input[name=brand],input[name=description],input[name=model_no],input[name=capacity]').change(function () { 
        var brand = $('input[name=brand]').val();
        var description = $('input[name=description]').val();
        var model_no = $('input[name=model_no]').val();
        var capacity = $('input[name=capacity]').val();
        var product_name = brand + ' ' + description + ' ' + model_no;
        $('input[name=name]').val(product_name);
    });

    function import_csv(form_data) {
        $.ajax({
            url: base_url + 'admin/product/import_csv',
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                $('.loader1').html(createLoader('Importing file, please wait..'));
            },
            success: function (res) {
                $('.loader1').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
    }
    
    $('input[name=attachment_file]').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $('input[name=attachment_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'product_files');
        form_data.append('do_compress', true);
        $.ajax({
            url: base_url + 'admin/uploader/upload_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                $('#loader').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#attachment').val(res.file_name);
                    $('.attachment_file').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error:function (res) {
                $('#loader').html('');
                toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
            }
        });
    });
    
});

//Product Variant Related

var product_variants = function (options) {
    var self = this;
    this.id = (options.id && options.id != '') ? options.id : '';
    this.product_state = (options.product_state && options.product_state != '') ? options.product_state : {};
    this.product_application = (options.product_application && options.product_application != '') ? options.product_application : {};
    this.product_variants_wrapper = 'product_variants_wrapper'
    this.product_variants_btn = 'add_product_variants';

    if (options.id && options.id != '') {
        self.load_variant_data();
    } else {
        setTimeout(function () {
            var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
            var state_select_field = self.create_select_field('state', self.product_state, null);
            var application_select_field = self.create_select_field('application', self.product_application, null);
            var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="0.00">';
            var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="0.00">';
            var amount_set_field = self.create_amount_set_field(null);
            var readonly = 'readonly';
            var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" value="0.00" ' + readonly + '>';
            $(product_variants_wrapper).append('<tr>'
                    + '<td>' + state_select_field + '</td>'
                    + '<td>' + application_select_field + '</td>'
                    + '<td>' + cost_price_field + '</td>'
                    + '<td>' + amount_set_field + '</td>'
                    + '<td>' + amount_field + '</td>'
                    + '<td>' + adn_margin_field + '</td>'
                    + '<td></td>');
        }, 500);
    }

    $('form button').on("click", function (e) {
        e.preventDefault();
    });

    setTimeout(function(){
        $("#type_id").trigger('change');
    },100);
    

    self.create_product_variants();
    self.remove_product_variants();

};

product_variants.prototype.load_variant_data = function () {
    var self = this;
    var product_id = self.id;
    var form_data = 'product_id=' + product_id;
    $.ajax({
        url: base_url + 'admin/product/fetch_product_variant_data',
        type: 'get',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                if (response.product_variant_data.length == 0) {
                    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
                    var state_select_field = self.create_select_field('state', self.product_state);
                    var application_select_field = self.create_select_field('application', self.product_application);
                    var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01"  name="product_variant[cost_price][]">';
                    var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]">';
                    var amount_set_field = self.create_amount_set_field();
                    var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" readonly>';
                    $(product_variants_wrapper).append('<tr>'
                            + '<td>' + state_select_field + '</td>'
                            + '<td>' + application_select_field + '</td>'
                            + '<td>' + cost_price_field + '</td>'
                            + '<td>' + amount_set_field + '</td>'
                            + '<td>' + amount_field + '</td>'
                            + '<td>' + adn_margin_field + '</td>'
                            + '<td></td>');
                    $('.is_price_set').on('change', function () {
                        $(this).closest('tr').find('input.amount').removeAttr('readonly');
                        if ($(this).val() == 0) {
                            $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                        }
                    });
                }
                for (var i = 0; i < response.product_variant_data.length; i++) {
                    if (i == 0) {
                        var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
                        var state_select_field = self.create_select_field('state', self.product_state, response.product_variant_data[i].state);
                        var application_select_field = self.create_select_field('application', self.product_application, response.product_variant_data[i].application);
                        var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="' + response.product_variant_data[i].cost_price + '">';
                        var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="' + response.product_variant_data[i].additional_margin + '">';
                        var amount_set_field = self.create_amount_set_field(response.product_variant_data[i].is_price_set);
                        var readonly = (response.product_variant_data[i].is_price_set == 0) ? 'readonly' : '';
                        var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" value="' + response.product_variant_data[i].amount + '" ' + readonly + '>';
                        $(product_variants_wrapper).append('<tr>'
                                + '<td>' + state_select_field + '</td>'
                                + '<td>' + application_select_field + '</td>'
                                + '<td>' + cost_price_field + '</td>'
                                + '<td>' + amount_set_field + '</td>'
                                + '<td>' + amount_field + '</td>'
                                + '<td>' + adn_margin_field + '</td>'
                                + '<td></td>');
                    } else {
                        self.create_product_variants(response.product_variant_data[i]);
                    }
                    $('.is_price_set').on('change', function () {
                        $(this).closest('tr').find('input.amount').removeAttr('readonly');
                        if ($(this).val() == 0) {
                            $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                        }
                    });
                }
            }
        }
    });
};

product_variants.prototype.create_product_variants = function (data) {
    var self = this;
    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
    var product_variants_btn = document.getElementById(self.product_variants_btn);
    if (!data) {
        product_variants_btn.addEventListener("click", function (e) { //on add input button click
            e.preventDefault();
            var state_select_field = self.create_select_field('state', self.product_state);
            var application_select_field = self.create_select_field('application', self.product_application);
            var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" >';
            var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01" name="product_variant[additional_margin][]">';
            var amount_set_field = self.create_amount_set_field();
            var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" readonly>';
            $(product_variants_wrapper).append('<tr>'
                    + '<td>' + state_select_field + '</td>'
                    + '<td>' + application_select_field + '</td>'
                    + '<td>' + cost_price_field + '</td>'
                    + '<td>' + amount_set_field + '</td>'
                    + '<td>' + amount_field + '</td>'
                    + '<td>' + adn_margin_field + '</td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            $('.is_price_set').on('change', function () {
                $(this).closest('tr').find('input.amount').removeAttr('readonly');
                if ($(this).val() == 0) {
                    $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                }
            });
        });
    } else {
        var state_select_field = self.create_select_field('state', self.product_state, data.state);
        var application_select_field = self.create_select_field('application', self.product_application, data.application);
        var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="' + data.cost_price + '">';
        var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="' + data.additional_margin + '">';
        var amount_set_field = self.create_amount_set_field(data.is_price_set);
        var readonly = (data.is_price_set == 0) ? 'readonly' : '';
        var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01" name="product_variant[amount][]" value="' + data.amount + '" ' + readonly + '>';
        $(product_variants_wrapper).append('<tr>'
                + '<td>' + state_select_field + '</td>'
                + '<td>' + application_select_field + '</td>'
                + '<td>' + cost_price_field + '</td>'
                + '<td>' + amount_set_field + '</td>'
                + '<td>' + amount_field + '</td>'
                + '<td>' + adn_margin_field + '</td>'
                + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
    }
};


product_variants.prototype.remove_product_variants = function () {
    var self = this;
    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
    $(product_variants_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
    });
};

product_variants.prototype.create_select_field = function (key, data, current_value) {
    var self = this;
    data = JSON.parse(data);
    var select = '<select name="product_variant[' + key + '][]" class="form-control">';
    for (var i = 0; i < data.length; i++) {
        var value = data[i];
        var sel = (current_value && current_value == value) ? 'selected="selected"' : '';
        select += '<option value="' + value + '" ' + sel + '>' + value + '</option>';
    }
    return select;
};

product_variants.prototype.create_amount_set_field = function (value) {
    var select = '<select name="product_variant[is_price_set][]" class="form-control is_price_set">';
    var selected1 = (value && value == 1) ? 'selected="selected"' : '';
    var selected0 = (value && value == 0) ? 'selected="selected"' : '';
    if (value = '' || value == null) {
        selected0 = 'selected="selected"';
    }
    select += '<option value="1" ' + selected1 + '>Yes</option>';
    select += '<option value="0" ' + selected0 + '>No</option>';
    return select;
};

    var context = {};
    context.id = "<?php echo (isset($pid) && $pid != '') ? $pid : '' ?>";
    context.product_state = '<?php echo json_encode($states); ?>';
    context.product_application = '<?php echo json_encode($product_application); ?>';
    var ltd = new product_variants(context);
</script>
</body>
</html>


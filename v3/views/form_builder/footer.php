<input type="hidden" id="counter" />
<div class="loader" id="loader" style="display: none;"><img src="./img/loader.gif" alt=""/></div>
<script src="<?php echo site_url('assets/form_builder/js/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/form_builder/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/form_builder/js/enscroll-0.6.2.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/form_builder/vendor/plugins/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/form_builder/js/custom.js'); ?>"></script>
<script src="<?php echo site_url('assets/form_builder/js/jquery-ui.min.js'); ?>"></script>

<script src="<?php echo site_url(); ?>assets/js/toastr.js"></script>

<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    function createLoader(text) {
        var loader = document.createElement('div');
        loader.className = "loader";
        var loader_text = document.createElement('div');
        loader_text.className = "text-center";
        loader_text.innerHTML = text;
        var div = document.createElement('div');
        div.className = "col-md-12";
        div.appendChild(loader);
        div.appendChild(loader_text);
        return div;
    }
</script>
<?php if (isset($proposal_id) && $proposal_id != '') { ?>
    <script src="<?php echo site_url('assets/js/form_builder_franchise.js?v=' . version); ?>"></script>
    <script src="http://szimek.github.io/signature_pad/js/signature_pad.umd.js"></script>
    <script>
        var context = {};
        context.template_id = '<?php echo isset($id) ? $id : 1; ?>';
        context.proposal_id = '<?php echo isset($proposal_id) ? $proposal_id : ''; ?>';
        var form_builder_tool = new form_builder(context);
    </script>
<?php } else { ?>
    <script src="<?php echo site_url('assets/js/form_builder.js?v=' . version); ?>"></script>
    <script>
        var context = {};
        context.template_id = '<?php echo isset($id) ? $id : 1; ?>';
        var form_builder_tool = new form_builder(context);
    </script>
<?php } ?>
<script>
  
    </script>
</body>
</html>

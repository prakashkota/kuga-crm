<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
	</style>
</head>

<body style="padding:0; margin:0">
   
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial/page1',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page2',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page3',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page4',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page5',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page6',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page7',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page8',$nomination_form_data); ?>
    <?php $this->load->view('pdf_files/led_kuga/nomination_form/nsw_commercial//page9',$nomination_form_data); ?>

</body>
</html>
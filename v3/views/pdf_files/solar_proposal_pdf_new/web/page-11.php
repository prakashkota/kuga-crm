<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page11{
		background-color: #FFFFFF;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 1287px !important;
		max-height: 910px !important;
		width: 1287px !important;
		height: 910px !important;
	}
	
	.page11 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 1044px;
		padding-top: 30px;
	}
	
	.page11 .top hr {
		width: 76%;
		height: 2px;
		margin: -24px 0px 0px 52px;
		background-color: #D01E2A;
	}
	
	.page11 .text_color {
		color: #D01E2A;
	}
	
	.page11 .title {
		margin-top: -50px;
	}
	
	.page11 .title h1 {
		padding-left: 14px;
		margin-left: 48px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
		border-height: 10px;
	}
	
	.page11 .plasting h2 {
		padding: 12px;
		margin-left: 48px;
		margin-top: 50px;
		font-size: 18px;
		background-color: #D01E2A;
		width: 43%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page11 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page11 .iiimage img {
		width: 92%;
		height: 200px;
		margin-left: 48px;
		margin-top: 19px;
	}
	
	.page11 .displayy {
		display: inline-flex;
	}
	
	.page11 .fimagee img {
		margin-left: 1068px;
		width: 180px;
	}
	</style>
</head>

<body>
	<div class="back-bg page11">
		<div class="top">
			<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
				<hr>
			</div>
			
			<div class="title">
				<h1>PANEL <span class="text_color"> INSTALLATION</span></h1>
			</div>
			<div class="">
				<div class="iiimage plasting">
					<h2>PANEL INSTALLATION</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0011.jpg'); ?>"> </div>
		<div class="iiimage plasting">
			<h2>COMMENTS ON INSTALLATION</h2> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0011.jpg'); ?>"> </div>
	</div>
	<div style="padding-bottom: 46px;"> </div>
	<div class="fimagee" style="margin-bottom: -66px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
	<div class="footer"></div>
	</div>
</body>

</html>
    <?php
    $o = $t = $s = array();
    $tilt = $orientation = '';
    $panel_id = '';
    $panel_model_type = '';
    $inverter_model_type = '';
    $battery_model_type = '';

    $numberOfPanel = 0;
    $panelCapacity = 0;

    if (count($mappingPanelObjects)) {
        $numberOfPanel = count($mappingPanelObjects);
        for ($i = 0; $i < count($mappingPanelObjects); $i++) {

            $object_properties = json_decode($mappingPanelObjects[$i]['object_properties']);
            $panel_id = $object_properties->sp_panel_id;
            if (!in_array($object_properties->sp_rotation, $o)) {
                $o[] = $object_properties->sp_rotation;
            }
            if (!in_array($object_properties->sp_tilt, $t)) {
                $t[] = $object_properties->sp_tilt;
            }
        }

        $o = empty($o) ? array(0) : $o;
        $t = empty($t) ? array(0) : $t;
        $orientation = implode('&deg;, ', $o);
        $tilt = implode('&deg;, ', $t);
    }

    // if (count($mappingPanelObjects)) {
    //     for ($i = 0; $i < count($mappingPanelObjects); $i++) {

    //         $object_properties = json_decode($mappingPanelObjects[$i]['object_properties']);
    //         $panel_id = $object_properties->sp_panel_id;
    //         if (!in_array($object_properties->sp_rotation, $o)) {
    //             $o[] = $object_properties->sp_rotation;
    //         }
    //         if (!in_array($object_properties->sp_tilt, $t)) {
    //             $t[] = $object_properties->sp_tilt;
    //         }
    //     }

    //     $o = empty($o) ? array(0) : $o;
    //     $t = empty($t) ? array(0) : $t;
    //     $orientation = implode('&deg;/ ', $o);
    //     $tilt = implode('&deg;/ ', $t);
    // }


    if ($proposal_data['system_details'] != null && $proposal_data['system_details'] != "") {
        $SystemDetails = json_decode($proposal_data['system_details']);
        if (!empty($SystemDetails)) {
            $panel_model_type = (isset($SystemDetails->system_pricing_panel) && $SystemDetails->system_pricing_panel != '') ? json_decode($SystemDetails->system_pricing_panel)->name : '';
            $inverter_model_type = (isset($SystemDetails->system_pricing_inverter) && $SystemDetails->system_pricing_inverter != '') ? json_decode($SystemDetails->system_pricing_inverter)->name : '';
            $battery_model_type = (isset($SystemDetails->system_pricing_battery) && $SystemDetails->system_pricing_battery != '') ? json_decode($SystemDetails->system_pricing_battery)->name : '';
            $panelCapacity = (isset($SystemDetails->system_pricing_panel) && $SystemDetails->system_pricing_panel != '') ? json_decode($SystemDetails->system_pricing_panel)->capacity : 0;
            
            $system_pricing_inverter_val = $SystemDetails->system_pricing_inverter_val;
        }
    }
    

    if ($proposal_data['is_production_file'] == 1 || $proposal_data['is_production_file'] == 2) {
        if (isset($proposal_data['pvwatts_data'])) {
            $d_pvwatts_data = json_decode($proposal_data['pvwatts_data']);
            for ($i = 0; $i < count($d_pvwatts_data); $i++) {
                if (!in_array($d_pvwatts_data[$i]->pv_azimuth, $o)) {
                    $o[$i] = $d_pvwatts_data[$i]->pv_azimuth;
                }
                if (!in_array($d_pvwatts_data[$i]->pv_tilt, $t)) {
                    $t[$i] = $d_pvwatts_data[$i]->pv_tilt;
                }

                if (!in_array($d_pvwatts_data[$i]->pv_system_size, $s)) {
                    $s[$i] = $d_pvwatts_data[$i]->pv_system_size;
                }
            }

            $o = empty($o) ? array(0) : $o;
            $t = empty($t) ? array(0) : $t;
            $orientation = implode('&deg;, ', $o);
            $tilt = implode('&deg;, ', $t);

            // $numberOfPanel = (int)(array_sum($s) * 1000 / $panelCapacity);
            if($panelCapacity > 0){
                $numberOfPanel = (int)($proposal_data['total_system_size'] * 1000 / $panelCapacity);
            }else{
                $numberOfPanel = 0;
            }
        }
    }
    $height = 215;
    ?>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }

        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }

        body {
            font-family: 'CenturyGothic';
            margin: 0;
            padding: 0;
        }

        .page8 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
        }

        .page8 .top hr {
            width: 65%;
            height: 1px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page8 .text_color {
            color: #D01E2A;
        }

        .page8 .title h1 {
            padding-left: 14px;
            margin-left: 70px;
            margin-top: 30px;
            border-left: 5px solid #D01E2A;
        }

        .page8 .title h2 {
            padding: 10px;
            margin-left: 70px;
            margin-top: 50px;
            background-color: #D01E2A;
            width: 47%;
            color: #ffffff;
            font-size: 18px;
        }

        .page8 .item_summary_table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 83%;
            margin-left: 70px;
        }

        .page8 .item_summary_table tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page8 .item_summary_table th {
            text-align: left;
            padding-left: 50px;
        }

        .page8 .item_summary_table td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page8 .footer {
            display: inline-flex;
        }

        .page8 .image img {
            margin-top: 0px;
            margin-left: 23px;
        }

        .page8 .fimagee img {
            margin-left: 730px;
        }

        .page8 .bb-none {
            border-bottom: none !important;
        }

        .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .greyf {
            height: 10px;
            width: 455px;
            background: #dcdcdc;
        }
    </style>


    <?php
    /** Manage Data Here */
    $mapping_tool = empty($mapping_tool) ? [] : $mapping_tool;
    $mapping_tool['snapshot1'] = isset($mapping_tool['snapshot1']) && $mapping_tool['snapshot1'] != NULL ? $mapping_tool['snapshot1'] : $proposal_data['panel_layout_image'];
    $mapping_tool['snapshot2'] = isset($mapping_tool['snapshot2']) && $mapping_tool['snapshot2'] != NULL ? $mapping_tool['snapshot2'] : $proposal_data['panel_layout_image'];
    $mapping_tool['notes'] = isset($mapping_tool['notes']) && $mapping_tool['notes'] != NULL ? $mapping_tool['notes'] : '';
    ?>

    <table width="910" border="0" cellpadding="0" cellspacing="0" class="page8">
        <tr>
            <td style="height:40px;">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top" class="top">
                <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
                <hr>
            </td>
        </tr>
        <tr style="height: 40px;">
            <td class="title" valign="top">
                <h1>SITE <span class="text_color"> PLAN</span></h1>
                <h2><?php echo $lead_data['customer_company_name']; ?></h2>
            </td>
        </tr>
        <tr style="height: 450px;" valign="top">
            <td valign="top">
                <img src="<?php echo $mapping_tool['snapshot1']; ?>" style="background-image: url('<?php echo $mapping_tool['snapshot1']; ?>'); 
                       background-repeat: no-repeat; background-position: 50% 50%; 
                       background-size: 100% 100%; 
                       width: 750;
                       height: 450px;
                       margin-left: 70px; 
                       margin-bottom: 30px;" />
            </td>
        </tr>
        <tr style="height: 40px;">
            <td class="title" valign="top">
                <h2><?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></h2>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
                    <tr>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Manufacturer power rating for array</td>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo ($proposal_data['total_system_size']); ?> kw</td>
                    </tr>
                    <tr>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Number of Panels</td>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $numberOfPanel; ?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Orientation</td>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $orientation . '&deg; '; ?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Tilt</td>
                        <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $tilt . '&deg; '; ?></td>
                    </tr>
                    <?php if (!empty($panel_model_type)) { ?>
                        <tr>
                            <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Type/Model</td>
                            <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $panel_model_type; ?></td>
                        </tr>
                    <?php } ?>
                    <?php if (!empty($inverter_model_type) && empty($battery_model_type)) { ?>
                        <tr>
                            <td style="padding:10px 30px; font-weight:600;">Inverter Type/Model</td>
                            <td style="padding:10px 30px;"><?php echo $inverter_model_type; ?></td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Inverter Type/Model</td>
                            <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $system_pricing_inverter_val.' X '.$inverter_model_type; ?></td>
                        </tr>
                    <?php } ?>
                    <?php if (!empty($battery_model_type)) {
                        $height = 200;
                    ?>
                        <tr>
                            <td style="padding:10px 30px; font-weight:600;">Battery Type/Model</td>
                            <td style="padding:10px 30px;"><?php echo $battery_model_type; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
        <tr style="height: <?php echo $height; ?>px;">
            <td class="title" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr style="position:fixed; bottom:0px; top: 1210px;">
            <td class="ruga">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/footer.png') ?>">
            </td>
        </tr>
    </table>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property TeamLeader Controller
 * @version 1.0
 */
class TeamLeader extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("Email_Manager", 'email_manager');
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }
    
    /**
     * Manage Team Leader
     * @param int group_id
     * @return view/list
     */
    public function manage($group_id = FALSE) {
        
        if (!$this->aauth->is_member('Admin') && !$group_id) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $team_types = TEAM_LEADER_TYPES;
        
        //If not a team leader redirect to dashboard
        if(!array_key_exists($group_id, $team_types)){
           $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
           redirect('admin/dashboard'); 
        }
        
        $users = $this->user->get_users($group_id,FALSE,FALSE);
        $data['users'] = $users;
        $data['title'] = 'Manage '.$team_types[$group_id];
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_leader/manage');
        $this->load->view('partials/footer');
    }
    
     /**
     * Add Team Leader
     *  Take user details, user location details and create new user
     * @return view/form
     */
    public function add() {
        
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('user_details[full_name]', 'Team Leader Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Company Contact No', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[aauth_users.email]');
            $this->form_validation->set_rules('user_group', 'Team Leader Type', 'required');
            $this->form_validation->set_rules('users[password]', 'Pssword', 'required');
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');

            if ($this->form_validation->run() != FALSE) {
                //Start Trancsaction
                $this->db->trans_begin();
                $user_group = $this->input->post('user_group');
                $userdata = $this->input->post('users');
                $userdata['email'] = $this->input->post('email');
                //$username = $this->aauth->generate_username($user_details['full_name'], 10);
                $user_id = $this->aauth->create_user($userdata['email'], $userdata['password']);
                if ($user_id) {
                    //Add user to Specific Group
                    $this->aauth->add_member($user_id, $user_group);
                    //Insert Data to Details Table
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $user_id;
                    $this->common->insert_data('tbl_user_details', $user_details);
                    //Insert Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    //Populate Working Location Postcodes in comma seperated string
                    if (isset($user_locations['workarea_postcodes'])) {
                        $user_locations['workarea_postcodes'] = implode(',', $user_locations['workarea_postcodes']);
                    }
                    if (isset($user_locations['workarea_postcodes_secondary'])) {
                        $user_locations['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
                    }

                    $user_locations['user_id'] = $user_id;
                    $this->common->insert_data('tbl_user_locations', $user_locations);
                    
                    //Check Transaction status and take necessary action
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Looks like something went wrong while creating the user.</div>');
                        redirect('admin/team-leader/manage/'.$user_group);
                    } else {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('message', '<div class="alert alert-success"> Team Leader with Email: <strong>' . $userdata['email'] . '</strong> and Password: <strong>' . $userdata['password'] . '</strong> created successfuly.</div>');
                        redirect('admin/team-leader/manage/'.$user_group);
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
                $data['user_group'] = $this->input->post('user_group');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['team_types'] = TEAM_LEADER_TYPES;
        $data['title'] = 'Add Team Leader';
        $data['show_password_field'] = TRUE;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_leader/form');
        $this->load->view('partials/footer');
    }
    
    /**
     * Edit Team Leader
     * Take user details, user location details and update if required
     * @return view/form
     */
    public function edit($id) {
        if ($this->aauth->get_user_id() != $id && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this franchise.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $team_leaders = $this->user->get_users(FALSE,$id,FALSE);

        if (empty($team_leaders)) {
            redirect('/admin/franchise/manage');
        }

        $data['email'] = $team_leaders[0]['email'];
        $data['users'] = $team_leaders[0];
        $data['user_group'] = $team_leaders[0]['group_id'];
        $data['user_locations'] = $team_leaders[0];
        $data['user_details'] = $team_leaders[0];
        

        if (!empty($this->input->post())) {
            if ($this->input->post('email') != $team_leaders[0]['email']) {
                $is_unique = '|is_unique[aauth_users.email]';
            } else {
                $is_unique = '';
            }
            $this->form_validation->set_rules('user_details[full_name]', 'Franchise Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Company Contact No', 'required');
            $this->form_validation->set_rules('user_group', 'Team Leader Type', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email' . $is_unique);
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');

            if ($this->form_validation->run() != FALSE) {
                $user_group = $this->input->post('user_group');
                $email = $this->input->post('email');
                $stat = TRUE;
                if ($this->input->post('email') != $team_leaders[0]['email']) {
                    $stat = $this->aauth->update_user($id, $email);
                }

                if ($stat) {
                    //Update user to Specific Group
                    if($team_leaders[0]['group_id'] != $user_group){
                        $this->aauth->remove_member($id, $team_leaders[0]['group_id']);
                        $this->aauth->add_member($id, $user_group);
                    }
                    //Update Data to Details Table
                    $user_details = $this->input->post('user_details');
                    $this->common->update_data('tbl_user_details', array('user_id' => $id), $user_details);
                    //Update Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    //Populate Working Location Postcodes in comma seperated string
                    if (isset($user_locations['workarea_postcodes'])) {
                        $user_locations['workarea_postcodes'] = implode(',', $user_locations['workarea_postcodes']);
                    }
                    if (isset($user_locations['workarea_postcodes_secondary'])) {
                        $user_locations['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
                    }
                    $user_locations['user_id'] = $id;
                    $this->common->insert_or_update('tbl_user_locations', array('user_id' => $id), $user_locations);
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Team Leader with Email: <strong>' . $email . '</strong> updated successfuly.</div>');
                    redirect('admin/team-leader/manage/'.$user_group);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
                $data['user_group'] = $this->input->post('user_group');
            }
        }

        $data['workarea_postcodes'] = array();
        $data['workarea_postcodes_secondary'] = array();
        if ($team_leaders[0]['workarea_postcodes'] != '' && $team_leaders[0]['workarea_postcodes'] != NULL) {
            $workarea_postcodes = explode(',', $team_leaders[0]['workarea_postcodes']);
            $data['workarea_postcodes'] = $workarea_postcodes;
        }
        if ($team_leaders[0]['workarea_postcodes_secondary'] != '' && $team_leaders[0]['workarea_postcodes_secondary'] != NULL) {
            $workarea_postcodes_secondary = explode(',', $team_leaders[0]['workarea_postcodes_secondary']);
            $data['workarea_postcodes_secondary'] = $workarea_postcodes_secondary;
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['team_types'] = TEAM_LEADER_TYPES;
        $data['title'] = 'Edit Team Leader';
        $data['show_password_field'] = FALSE;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_leader/form');
        $this->load->view('partials/footer');
    }
    
    /**
     * Function to update team leader settings by himself
     * @return view/form
     */
    public function manage_settings() {
        $user_id = $this->aauth->get_user_id();
        
        if (!$this->aauth->is_member('Lead Allocation Team Leader') && !$this->aauth->is_member('Sales Team Leader') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this franchise.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        $team_leaders = $this->user->get_users(FALSE,$user_id,FALSE);
        
        if (empty($team_leaders)) {
            redirect('/admin/dashboard');
        }

        $data['user_details'] = $team_leaders[0];
        $data['user_locations'] = $user_locations = $this->input->post('user_locations');

        if (!empty($this->input->post())) {
            //Update Data to Details Table
            $user_details = $this->input->post('user_details');
            $stat = $this->common->update_data('tbl_user_details', array('user_id' => $user_id), $user_details);
            //Update Data to Location Table
            //Populate Working Location Postcodes in comma seperated string
            $user_locations_update = array();
            $user_locations_update['workarea_postcodes_secondary'] = implode(',', $user_locations['workarea_postcodes_secondary']);
            $stat1 = $this->common->update_data('tbl_user_locations', array('user_id' => $user_id), $user_locations_update);
            if ($stat || $stat1) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"> Settings updated successfuly.</div>');
                redirect('admin/settings/team-leader/manage');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the settings. </div>');
            }
        }
        
        $data['workarea_postcodes_secondary'] = array();
        if ($team_leaders[0]['workarea_postcodes_secondary'] != '' && $team_leaders[0]['workarea_postcodes_secondary'] != NULL) {
            $workarea_postcodes_secondary = explode(',', $team_leaders[0]['workarea_postcodes_secondary']);
            $data['workarea_postcodes_secondary'] = $workarea_postcodes_secondary;
        }
      
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('team_leader/manage_settings');
        $this->load->view('partials/footer');
    }

    /**
     * Fetch postcodes
     * @return view/form
     */
    public function fetch_postcodes() {
        $data['results'] = array();
        if (!empty($this->input->get('q'))) {
            $postcode = $this->input->get('q');
            $postcodes = $this->common->fetch_where('tbl_postcodes', '*', array('postcode' => $postcode));
            if (!empty($postcodes)) {
                $data['results'][0] = array('id' => $postcodes[0]['id'], 'text' => $postcodes[0]['postcode']);
            }
        }
        echo json_encode($data);
        die;
    }

}

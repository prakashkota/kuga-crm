<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder@latest/dist/Control.Geocoder.css">
<link rel="stylesheet" href="<?php echo site_url(); ?>assets/map-tool/map.css?v=<?php echo time(); ?>">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//unpkg.com/leaflet-gesture-handling/dist/leaflet-gesture-handling.min.css">
<div id="real_map_container">
    <input type="hidden" id="panel_objects" />
    <div id="real_map"></div> <a href="javascript:void(0);" class="leaflet-bar leaflet-control  left-panel-toggle" id="mtool_left_sidebar_toogle"><i class="fa-regular fa-arrow-right-arrow-left"></i></a>
    <!-- Left Side Design Options -->
    <div class="leaflet-bar leaflet-control designer__sidebar" id="mtool_left_sidebar">
        <!-- Search Address -->
        <div class="solar_tool_address_search_toolbar">
            <div class="input-group mb-3 w-100">
                <input class="form-control form-control-dark" type="text" placeholder="Search For Address" aria-label="Search For Address" id="solar_tool_address_search">
                <div class="input-group-append d-none">
                    <button class="btn btn-outline-secondary" href="javascript:void(0);" id="save_bounded_area" type="button">Save</button>
                </div>
                <div class="input-group-append d-none">
                    <button class="btn btn-outline-secondary" href="javascript:void(0);" id="remove_bounded_area" type="button">Reset</button>
                </div>
            </div>
        </div>
        <div class="ltoolbar d-none">
            <div class="toolbar-btn" id="tool_select">
                <div class="icon icon--18 icon--withsub">
                    <svg height="18" viewBox="0 0 18 18" width="18" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.4,1.1l0,15.8l0,0L8,12.2l6.6,0.2l0,0L3.4,1.1z"></path>
                        <title> Select tool</title>
                    </svg>
                </div>
            </div>
            <div class="divider"></div>
            <div class="toolbar-btn  toolbar-btn--selected d-none" id="tool_hand">
                <div class="icon icon--18 icon--72pc">
                    <svg height="18" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg">
                        <path d="M23 5.5V20c0 2.2-1.8 4-4 4h-7.3c-1.08 0-2.1-.43-2.85-1.19L1 14.83s1.26-1.23 1.3-1.25c.22-.19.49-.29.79-.29.22 0 .42.06.6.16.04.01 4.31 2.46 4.31 2.46V4c0-.83.67-1.5 1.5-1.5S11 3.17 11 4v7h1V1.5c0-.83.67-1.5 1.5-1.5S15 .67 15 1.5V11h1V2.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5V11h1V5.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5z"></path>
                    </svg>
                </div>
            </div>
            <div class="divider"></div>
            <div class="toolbar-btn" id="tool_manage_panel">
                <div class="icon icon--18 icon--72pc">
                    <svg height="22" width="22" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path fill="none" d="M0,0h24v24H0V0z"></path>
                        <path d="M9,19h3v-2H9V3h10v9h2V3c0-1.1-0.9-2-2-2H9C7.9,1,7,1.9,7,3v14C7,18.1,7.9,19,9,19z"></path>
                        <path d="M12,21H5V7H3v14c0,1.1,0.9,2,2,2h7V21z"></path>
                        <polygon points="17,17 14,17 14,19 17,19 17,22 19,22 19,19 22,19 22,17 19,17 19,14 17,14"></polygon>
                        <title>Manage Panels</title>
                    </svg>
                </div>
            </div>
            <div class="toolbar-btn d-none" id="tool_eraser">
                <div class="icon icon--18 icon--72pc">
                    <svg height="18px" viewBox="0 0 24 24" width="18px" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.14,3C14.63,3 14.12,3.2 13.73,3.59L2.59,14.73C1.81,15.5 1.81,16.77 2.59,17.56L5.03,20H12.69L21.41,11.27C22.2,10.5 22.2,9.23 21.41,8.44L16.56,3.59C16.17,3.2 15.65,3 15.14,3M17,18L15,20H22V18"></path>
                    </svg>
                </div>
            </div>
            <div class="divider d-none"></div>
            <div class="toolbar-btn d-none">
                <div class="icon icon--18 icon--72pc">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18" height="18" viewBox="0 0 18 18" xml:space="preserve">
                        <g>
                            <path d="M4.4,3.5l7.8,7.9L8,11.3l-0.4,0l-0.3,0.3l-2.9,3L4.4,3.5 M3.4,1.1l0,15l0.7,0.3l3.9-4l5.6,0.2l0.3-0.7L3.4,1.1L3.4,1.1z" class="st1"></path>
                        </g>
                        <g>
                            <polygon points="16,15 14,15 14,13 13,13 13,15 11,15 11,16 13,16 13,18 14,18 14,16 16,16     " class="st1"></polygon>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="toolbar-btn d-none">
                <div class="icon icon--18 icon--72pc">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18" height="18" viewBox="0 0 18 18" xml:space="preserve">
                        <g>
                            <path d="M4.4,3.5l7.8,7.9L8,11.3l-0.4,0l-0.3,0.3l-2.9,3L4.4,3.5 M3.4,1.1l0,15l0.7,0.3l3.9-4l5.6,0.2l0.3-0.7L3.4,1.1L3.4,1.1z" class="st1"></path>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="divider"></div>
            <div class="toolbar-btn d-none">
                <div class="icon icon--18 icon--72pc">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-5 14h-2V9h-2V7h4v10z"></path>
                    </svg>
                </div>
            </div>
            <div class="toolbar-btn" id="tool_annotations">
                <div class="icon icon--18 icon--72pc">
                    <svg xmlns="http://www.w3.org/2000/svg" height="22" width="22" viewBox="0 0 24 24">
                        <path d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M12 2l-5.5 9h11z"></path>
                        <circle cx="17.5" cy="17.5" r="4.5"></circle>
                        <path d="M3 13.5h8v8H3z"></path>
                        <title>Select Annotations</title>
                    </svg>
                </div>
            </div>
            <div class="toolbar-btn d-none">
                <div class="icon icon--18 icon--72pc">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                        <path d="M22.17 9.17c0-3.87-3.13-7-7-7s-7 3.13-7 7c0 3.47 2.52 6.34 5.83 6.89V20H6v-3h1v-4c0-.55-.45-1-1-1H3c-.55 0-1 .45-1 1v4h1v5h16v-2h-3v-3.88c3.47-.41 6.17-3.36 6.17-6.95zM4.5 11c.83 0 1.5-.67 1.5-1.5S5.33 8 4.5 8 3 8.67 3 9.5 3.67 11 4.5 11z"></path>
                    </svg>
                </div>
            </div>
            <div class="divider"></div>
            <div class="toolbar-btn d-none" id="tool_measuring_tape"> <a class="leaflet-control-simpleMapScreenshoter-btn">
                    <img src='<?php echo site_url("assets/map-tool/measuring-tape.png"); ?>' style="width: 22px;height: 22px;" />
                </a> </div>
            <div class="divider"></div>
            <div class="toolbar-btn" id="snapshot_btn1"> <a class="leaflet-control-simpleMapScreenshoter-btn">
                    <img title="Capture Snapshot" src='<?php echo site_url("assets/map-tool/snapshot.png"); ?>' style="width: 22px;height: 22px;" />
                </a> </div>
            <div class="divider"></div>
            <div class="toolbar-btn d-none" id="snapshot_btn2"> <a class="leaflet-control-simpleMapScreenshoter-btn">
                    <img src='<?php echo site_url("assets/map-tool/snapshot2.png"); ?>' style="width: 25px;height: 25px;" />
                </a> </div>
            <div class="toolbar-btn loader d-none" id="object_loader"></div>
        </div>
        <!-- Panel Tool -->
        <div id="sidebar_right_panel" class="d-none">
            <div class="summary-charts">
                <div class="summary-charts__chart text-center">
                    <div class="col-sm-12" style="text-align: left !important;">
                        <label id="mtool_selected" style="font-size: 11px; font-weight: 700;"></label>
                    </div>
                    <div class="col-sm-12" style="text-align: left !important;">
                        <label id="mtool_watt_capacity" style="font-size: 11px; font-weight: 700;"></label>
                    </div>
                </div>
            </div>
            <div class="p-2" style="display: block;">
                <label style="font-size: 10px;background: #C20000; padding: 6px;color: #fff;width: 100%;">Select Panel</label>
                <div class="form-group mb-1">
                    <select class="form-control" id="mtool_panel_list">
                        <?php foreach ($solar_panels as $row) { ?>
                            <option data-dimension="<?php echo $row['length']; ?>,<?php echo $row['width']; ?>" data-capacity="<?php echo $row['capacity']; ?>" data-degradation="<?php echo $row['panel_degradation_per']; ?>" data-efficiency="<?php echo $row['inverter_efficiency']; ?>" value="<?php echo $row['id']; ?>">
                                <?php echo $row['name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="panel-library__list mb-2">
                    <div>
                        <?php foreach ($solar_panels as $row) { ?>
                            <div class="panel-library-item pl-0 d-none" id="panel_list_item_<?php echo $row['id']; ?>">
                                <div class="panel-library-item__wrapper">
                                    <div class="panel-library-item__object"> <img crossorigin="anonymous" src="https://static.getpylon.com/images/panels/lg_neon_2_60_48.jpg" draggable="false" class="panel-library-item__img"> </div>
                                </div>
                                <div class="panel-library-item__details">
                                    <div class="panel-library-item__name"> <span><?php echo $row['brand']; ?></span> <span><?php echo $row['description']; ?></span> </div>
                                    <div class="panel-library-item__power"> <span><?php echo $row['capacity']; ?></span> <span class="panel-library-item__code"><?php echo $row['model_no']; ?></span> </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="input-group input-group-sm btn-inset">
                                <input id="mtool_no_of_panels" type="number" class="form-control" placeholder="Enter no of panels to add" min="1" value="1">
                                <button id="mtool_add_panel" type="button" class="btn btn-info btn-flat">ADD</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 pl-2">
                        <div class="form-group">
                            <button id="mtool_select_toggle" class="btn btn-xs  btn-info select-typ" title="Select All">SELECT ALL</button>
                        </div>
                    </div>
                    <div class="col-sm-2 pl-3">
                        <div class="form-group">
                            <button style="width:30px;" id="mtool_delete_panel" class="btn btn-xs btn-info" title="Delete"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group input-group-sm">
                            <label>Rotation</label>
                            <div class="rotate-by-90">
                                <label for="mtool_rt_left"> <i class="fas fa-undo" title="Rotate Left">
                                        <input type="radio" id="mtool_rt_left" class="mtool_orientation" name="mtool_orientation" value="landscape">
                                    </i> </label>
                                <input id="mtool_individual_rotation" type="number" class="counter-inpt form-control" placeholder="Rotaion" value="0" min="0" max="361">
                                <label for="mtool_rt_right"> <i class="fas fa-redo" title="Rotate Right">
                                        <input type="radio" id="mtool_rt_right" class="mtool_orientation" name="mtool_orientation" value="portrait">
                                    </i> </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group input-group-sm">
                            <label>Tilt</label>
                            <input style="min-width: 70px;" id="mtool_tilt" type="number" class="form-control" placeholder="Tilt" value="" min="0" max="91">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label title="Panel Mounting">Mounting</label>
                        <div class="rotate-by-90 rd-btn" style="min-width: 0px !important;">
                            <label for="mtool_landscape" class="landscape" title="Landscape">
                                <input type="radio" id="mtool_landscape" class="mtool_mounting" name="mtool_mounting" value="landscape"> </label>
                            <label for="mtool_portrait" title="Portrait" class="portrait active">
                                <input type="radio" id="mtool_portrait" class="mtool_mounting" name="mtool_mounting" value="portrait" checked="checked"> </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="display:none;">
                        <div class="form-group">
                            <label>Global Rotation</label>
                            <input id="mtool_rotation" type="number" class="form-control" placeholder="Rotaion" min="0" max="360" value="0">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label id="selected"></label>
                    </div>
                    <div class="col-sm-12">
                        <label id="watt_capacity"></label>
                    </div>
                </div>
            </div>
        </div>
        <!-- Annotation Tool -->
        <div id="sidebar_right_annotation" class="d-none">
            <div class="p-2" style="display: block;">
                <label style="font-size: 10px;background: #C20000; padding: 6px;color: #fff;width: 100%;">Select Annotation</label>
                <div class="form-group mb-1">
                    <select class="form-control" id="annotation_list">
                        <?php $annotations = array(
                            array('id' => 1, 'name' => 'Inverter', 'icon' => 'ic_inverter_96px.png', 'type' => 'icon'),
                            array('id' => 2, 'name' => 'Meter Box', 'icon' => 'ic_meter_box_96px.png', 'type' => 'icon'),
                            array('id' => 3, 'name' => 'Distribution Box', 'icon' => 'ic_distribution_box_96px.png', 'type' => 'icon'),
                            array('id' => 4, 'name' => 'Main SwitchBoard', 'icon' => 'ic_main_switchboard_96px.png', 'type' => 'icon'),
                            array('id' => 5, 'name' => 'DC Rooftop Isolator', 'icon' => 'ic_isolator_96px.png', 'type' => 'icon'),
                            array('id' => 6, 'name' => 'Optimiser', 'icon' => 'ic_optimiser_96px.png', 'type' => 'icon'),
                            array('id' => 7, 'name' => 'Battery', 'icon' => 'ic_battery_96px.png', 'type' => 'icon'),
                            array('id' => 8, 'name' => 'Solar Regulator', 'icon' => 'ic_solar_regulator_96px.png', 'type' => 'icon'),
                            array('id' => 9, 'name' => 'Generator', 'icon' => 'ic_generator_96px.png', 'type' => 'icon'),
                            array('id' => 10, 'name' => 'Ladder Access Point', 'icon' => 'ic_ladder_access_96px.png', 'type' => 'icon'),
                            array('id' => 11, 'name' => 'Arrest Roof Anchor Point', 'icon' => 'ic_anchor_96px.png', 'type' => 'icon'),
                            array('id' => 12, 'name' => 'Edge Protection', 'icon' => 'ic_edge_protection_96px.png', 'type' => 'icon'),
                            array('id' => 13, 'name' => 'Scissor Lift Access', 'icon' => 'scissor_lift_access_96px.png', 'type' => 'icon'),
                            array('id' => 14, 'name' => 'Cable Tray', 'color' => '#808080', 'type' => 'line', 'width' => 10, 'icon' => 'cable_tray_96px.png'),
                            array('id' => 15, 'name' => 'Walk Way', 'color' =>  '#FFFF00', 'type' => 'line', 'width' => 20, 'icon' => 'walk_way_96px.png'),
                            array('id' => 16, 'name' => 'Sky Light protection', 'color' => '#0000FF', 'type' => 'polygon', 'icon' => 'sky_light_protection_96px.png'),
                            array('id' => 17, 'name' => 'Work Zone', 'color' => '#FFA500', 'type' => 'polygon', 'icon' => 'work_zone_96px.png'),
                            array('id' => 18, 'name' => 'Measuring Tape', 'color' => '#FFA500', 'type' => 'measuring-tape', 'icon' => 'measuring-tape.png'),
                        );
                        foreach ($annotations as $k => $v) { ?>
                            <option value="<?php echo $v['id']; ?>" data-name="<?php echo $v['name']; ?>" data-icon="<?php echo isset($v['icon']) ? site_url('assets/map-tool/annotations/' . $v['icon']) : ''; ?>" data-color="<?php echo isset($v['color']) ? $v['color'] : ''; ?>" data-type="<?php echo isset($v['type']) ? $v['type'] : ''; ?>" data-width="<?php echo isset($v['width']) ? $v['width'] : ''; ?>">
                                <?php echo $v['name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <button style="width: 100px;" id="add-annotation" type="button" class="btn btn-info btn-flat mr-2">ADD</button>
                        </div>
                    </div>
                </div>
                <div style="display: block;">
                    <label style="font-size: 10px;background: #C20000; padding: 6px;color: #fff;width: 100%;">Existing Annotations</label>
                    <div class="panel-library__list mb-2" id="exisiting_annotations"> </div>
                </div>
            </div>
        </div>

        <?php /**
      <div class="leaflet-bar leaflet-control" style="border: none;" id="tool_panel_design_notes">
         <div class="toolbar-btn">
            <a class="leaflet-control-simpleMapScreenshoter-btn" title="Notes"> 
               <i class="fa fa-sticky-note-o" style="font-size: 20px;margin-top: 5px;"></i> </a>
         </div>
      </div>
         */ ?>
        <button class="btn btn-outline-secondary ml-3 d-none" id="tool_panel_design_notes">
            <i class="fa fa-sticky-note-o"></i> Add Notes
        </button>

        <div class="d-none" id="nm_imagery_datepicker" style="top: 0px;padding: 10px;">
            <label>Nearmap Imagery Timeline</label>
            <select id="nm_imagery_datepicker_select" class="form-control">

            </select>
        </div>

        <button class="btn btn-outline-secondary ml-3 d-none" id="tool_select_reset_btn">
            <i class="fa fa-close"></i> Remove Select Tool
        </button>

    </div>
    <!-- Image Download Without Annotations Icon -->
    <div class="leaflet-bar leaflet-control d-none" style="border: none;" id="tool_download_snapshot1">
        <div class="toolbar-btn">
            <a href="javascript:void(0);" title="Download Panel Image" target="_blank" class="icon icon--24 icon--72pc" id="tool_download_snapshot1_link">
                <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                    <path fill="none" d="M0,0h24v24H0V0z"></path>
                    <path d="M13,5H5C3.9,5,3,5.9,3,7v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2v-8h-6V5z M5,19l3-4l2,3l3-4l4,5H5z"></path>
                    <path d="M20.9,4.8h-1.7V2.3h-2.5v2.5h-1.7L18,7.7L20.9,4.8z M15.1,8.5v0.8h5.8V8.5H15.1z"></path>
                </svg>
            </a>
        </div>
    </div>
    <!-- Image Download With Annotations Icon -->
    <div class="leaflet-bar leaflet-control d-none" style="border: none;" id="tool_download_snapshot2">
        <div class="toolbar-btn">
            <a href="javascript:void(0);" title="Download Annotation Image" target="_blank" class="icon icon--24 icon--72pc" id="tool_download_snapshot2_link">
                <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10,6l-4.4,7.2h8.8L10,6z"></path>
                    <circle cx="14.4" cy="18.4" r="3.6"></circle>
                    <path d="M2.8,15.2h6.4v6.4H2.8V15.2z"></path>
                    <path d="M20.9,4.8h-1.7V2.3h-2.5v2.5h-1.7L18,7.7L20.9,4.8z M15.1,8.5v0.8h5.8V8.5H15.1z"></path>
                </svg>
            </a>
        </div>
    </div>

    <!-- Image Download With Annotations Icon -->
    <div class="leaflet-bar leaflet-control d-none" style="border: none;" id="tool_nm_imagery_datepicker">
        <div class="toolbar-btn">
            <a href="javascript:void(0);" title="NearMap Imagery Dates" class="icon icon--24 icon--72pc" id="tool_nm_imagery_datepicker_btn" style="text-decoration: none;color: rgb(0, 0, 0);margin-right: 70px;width: 105px !important;outline: none;">
                10 Sep 2019
            </a>
        </div>
    </div>


</div>

<img id="image" src="" class="d-none" />
<div id="loaderContainer" style="height:400px;">
    <div class="loader" style="margin-top: 200px;"></div>
    <p class="text-center mt-2">Loading Mapping Tool. Please Wait...</p>
</div>

<canvas id="annotation_text_canvas" class="d-none" style="width: 600px;height: 600px;position: relative;overflow: hidden;"></canvas>

<div class="modal fade" id="panel_notes_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pb-1">Notes for Panel Design</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <textarea id="panel_notes"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save_panel_notes_btn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script src='<?php echo site_url(); ?>assets/js/jquery-ui.min.js' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://unpkg.com/fabric@latest/dist/fabric.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"></script>
<!-- Leaflet Related -->
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<script src="https://unpkg.com/leaflet.gridlayer.googlemutant@latest/dist/Leaflet.GoogleMutant.js"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/Leaflet.Draw.js?v=<?php echo time(); ?>"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/leaflet.contextmenu.min.js?v=<?php echo time(); ?>"></script>
<script src="https://unpkg.com/leaflet-control-geocoder@latest/dist/Control.Geocoder.js"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/L.Path.Transform.js?v=<?php echo time(); ?>"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/leaflet.geometryutil.js?v=<?php echo time(); ?>"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/L.Path.Drag.js?v=<?php echo time(); ?>"></script>
<script src="<?php echo site_url(); ?>assets/map-tool/Control.FullScreen.js?v=<?php echo time(); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/@turf/turf@6/turf.min.js"></script>
<script src="https://cdn.rawgit.com/bjornharrtell/jsts/gh-pages/1.1.2/jsts.min.js"></script>
<script src="//unpkg.com/leaflet-gesture-handling"></script>
<script src="https://publiclab.github.io/Leaflet.DistortableImage/dist/leaflet.distortableimage.js"></script>
<script src="<?php echo site_url(); ?>common/js/solar-tool.js?v=<?php echo time(); ?>"></script>
<script type="text/javascript">
    var context = {};
    context.map_container_id = 'real_map';
    context.section = 'COMMERCIAL_SOLAR_PROPOSAL';
    context.proposal_id = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? $proposal_data['id'] : ''; ?>';
    context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
    context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
    context.annotationList = '<?php echo (isset($annotations) && !empty($annotations)) ? json_encode($annotations) : ''; ?>';
    var stool = new solar_tool(context);
</script>
</body>

</html>
<html>
<body style="margin:0px;padding:0px;">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
  <tbody>
    <tr>
      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/header.jpg" width="910" height="127" alt=""/></td>
    </tr>
    <tr>
      <td height="25"></td>
    </tr>
    <tr>
      <td valign="top"><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="40" style="color: #922a27; font-size:20px;"><strong>Section B - Maintenance Schedule</strong></td>
          </tr>
          <tr>
            <td style="border-top: solid 1px #969697; height:10px"></td>
          </tr>
          <tr>
            <td height="25" style="font-size:12px; color:#000000;">To ensure that the lighting system (lamps and fittings) continues to operate correctly for its full lifetime, maintenance of the lighting equipment should be conducted. <strong>Kuga Electrical Pty Ltd</strong> encourages you to follow this maintenance schedule:</td>
          </tr>
          
          <tr>
            <td height="15"></td>
          </tr>
          <tr>
            <td style="font-size: 12px; color: #000000"><strong>Recommended maintenance schedule</strong></td>
          </tr>
          <tr>
            <td height="15"></td>
          </tr>
          <tr>
            <td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="160" height="30" align="center" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; font-size: 12px"><strong>Area</strong></td>
                  <td width="160" height="30" align="center" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; font-size: 12px"><strong>Lamp/Fitting Type</strong></td>
                  <td width="160" height="30" align="center" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; font-size: 12px"><strong>Assumed hours of  use<br />
                    per year</strong></td>
                  <td width="160" height="30" align="center" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; font-size: 12px"><strong>Recommended lamp  replacement interval</strong></td>
                  <td width="160" height="30" align="center" style="border-bottom: solid 1px #969697; font-size: 12px"><strong>Cleaning schedule<br />
                    (e.g yearly)</strong></td>
                </tr>
                <?php $maintenance_schedule_items = json_decode(json_encode($maintenance_schedule_items),true); ?>
                <?php for($i=0; $i < 12; $i++){ ?>
                <tr>
                  <td height="32" style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; padding-left:10px;">
                       <?php echo isset($maintenance_schedule_items['area'][$i]) ? $maintenance_schedule_items['area'][$i] : ''; ?>
                  </td>
                  <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; padding-left:10px;">
                      <?php echo isset($maintenance_schedule_items['area'][$i]) ? $maintenance_schedule_items['fitting_type'][$i] : ''; ?>
                  </td>
                  <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; padding-left:10px;">
                      <?php echo isset($maintenance_schedule_items['area'][$i]) ? $maintenance_schedule_items['hours'][$i] : ''; ?>
                  </td>
                  <td style="border-bottom: solid 1px #969697; border-right: solid 1px #969697; padding-left:10px;">
                       <?php echo isset($maintenance_schedule_items['area'][$i]) ? $maintenance_schedule_items['recommended_lamp'][$i] : ''; ?>
                  </td>
                  <td style="border-bottom: solid 1px #969697; padding-left:10px; ">
                        <?php echo isset($maintenance_schedule_items['area'][$i]) ? $maintenance_schedule_items['cleaning_schedule'][$i] : ''; ?>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
            </table></td>
          </tr>
          <tr>
            <td height="490"></td>
          </tr>
          </tbody>
      </table></td>
    </tr>
    <tr>
      <td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/nsw_cl/bottom.jpg" width="910" height="102" alt=""/></td>
    </tr>
  </tbody>
</table>
</body>
</html>
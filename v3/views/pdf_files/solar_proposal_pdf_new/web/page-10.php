<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .mt54 {
            margin-top: -54px;
        }

        .page-sp-back-bg {
            background-color: #fff;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page-sp .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page-sp .top hr {
            width: 65%;
            height: 1px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-sp .text_color {
            color: #D01E2A;
        }

        .page-sp .title {
            display: inline-flex;
            margin-top: -50px;
            margin-left: 70px;
            width: 100%;
        }

        .page-sp .title h1 {
            padding-left: 14px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;

        }

        .page-sp .solorproduction {
            width: 50%;
        }

        .page-sp .solorproduction h2 {
            padding: 4px;
            margin-top: 88px;
            font-size: 16px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            background: linear-gradient(105deg, #D01E2A 87%, transparent 15%);
            letter-spacing: 2px;
            display: flex;
            align-items: center;
        }

        .page-sp .solorproductionavg {
            width: 60%;
            margin-right: 200px;
        }

        .page-sp .solorproductionavg h2 {
            padding: 12.5px;
            margin-top: 88px;
            font-size: 16px;
            width: 100%;
            color: #373536;
            border: 2px solid #d01e2a;
            background: linear-gradient(300deg, #ffffff 87%, transparent 15%);
        }

        .page-sp table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 80%;
            margin-left: 46px;
        }

        .page-sp tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page-sp td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page-sp .sectio-image img {
            margin-top: 25px;
            margin-left: 85px;
        }

        .page-sp .fimagee img {
            margin-left: 730px;
            margin-top: 125px;
        }

        .page-sp .sub_titlee h6 {
            font-size: 17px;
            margin-left: 48px;
        }

        .page-sp .firstBoxs {
            display: inline-flex;
            width: 85%;
            margin: 40px 70px 0;
        }

        .page-sp .sub_titlee {
            background-color: #dddddd;
            margin-right: 30px;
        }

        .page-sp .sub_titlee1 {
            margin-right: 30px;
            border: 2px solid #D01E2A;
        }

        .page-sp .sub_titlee1 p {
            margin: 9px 0;
            /* font-size: 18px; */
            color: #D01E2A;
            font-weight: bold;
            text-align: center;
        }



        .page-sp .sub_titlee2 {
            margin-right: 30px;

        }

        .page-sp .sub_titlee2 .sub_titlee2-inner {
            border: 2px solid #D01E2A;
            padding: 0 16px;
        }

        .page-sp .sub_titlee2 p {
            margin: 9px 0;
            /* font-size: 18px; */
            color: #D01E2A;
            font-weight: bold;
            text-align: center;
        }

        .page-sp .sub_titlee p {
            /* color: #D01E2A; */
            font-weight: bold;
            text-align: center;
            margin: 9px 0;
        }

        .page-sp .box {
            background-color: #dddddd;
            padding: 9px 0;
            font-weight: bold;
            text-align: center;
            width: 236px;
            max-width: 236px;
        }

        .page-sp .box1 {
            /* border: 2px solid #D01E2A; */
            padding: 9px 0;
            font-weight: bold;
            text-align: center;
            width: 232px;
            max-width: 232px;
        }

        .page-sp .box2 {
            border: 2px solid #F2B1B5;
            width: 234px;
            height: 60px;
        }

        .page-sp .tittle {
            display: inline-flex;
            margin-top: -50px;
            margin-left: 20px;
        }

        .page-sp .solorproductionn {
            margin-right: 30px;
            margin-bottom: 30px;
            margin-top: 30px;
        }

        .page-sp .solorproductionn h2 {
            font-size: 16px;
            color: #241E20;
            text-align: center;
        }

        .page-sp .footer {
            display: inline-flex;
        }

        .page-sp .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page-sp .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page-sp .f20 p {
            font-size: 100px;
            margin: 0;
            margin-left: 19px;
            color: #D01E2A;
            margin-right: 42px;
        }

        .page-sp .f20 {
            border-right: 2px solid #F2B1B5;
            display: flex;
            align-items: center;

        }

        .page-sp .f20image {
            border-right: 2px solid #F2B1B5;

        }

        .page-sp .f20image img {
            margin-left: 42px;
            margin-right: 42px;

        }

        .page-sp .f20imagee img {
            margin-left: 42px;
            margin-right: 42px;

        }

        .page-sp .f20imagee p {
            text-align: center;
            font-size: 50px;
            margin: 0;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class=".page-sp-back-bg back-bg page-sp">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>SYSTEM <span class="text_color"> PERFORMANCE</span></h1>
        </div>
        <div class="title">
            <div class="solorproduction">
                <h2>

                    <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g id="Icon">
                            <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                            <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                            <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                        </g>
                    </svg>
                    <!-- <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/solar-panel.png'); ?>" width="18px">  -->
                    &nbsp &nbsp SOLAR PRODUCTION
                </h2>
            </div>
            <div class="solorproductionavg">
                <h2>SOLAR PRODUCTION (AVG MONTHLY)</h2>
            </div>
        </div>

        <div class="sectio-image">
            <div id="avg_kw_per_day_chart"></div>
            <!-- <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0010.jpg'); ?>" width="650px"> -->
        </div>



        <div class="firstBoxs" style="margin-top:0">
            <div class="sub_titlee">
                <p>Bill Before Solar</p>
                <div class="box" id="final_electricity_calculation">5989</div>
            </div>
            <div class="sub_titlee">
                <p>Bill After Solar</p>
                <div class="box" id="bill_after_solar"></div>
            </div>
            <div class="sub_titlee">
                <p>%of Electricity Bill Saving</p>
                <div class="box" id="bill_saving"></div>
            </div>
        </div>


        <div class="firstBoxs">
            <div class="sub_titlee1">
                <p>Annual Solar Production</p>
                <div class="box1" id="annual_solar_production"></div>
            </div>
            <div class="sub_titlee1">
                <p>Offset Consumption</p>
                <div class="box1" id="offset_consumption"></div>
            </div>
            <div class="sub_titlee1">
                <p>Export To Grid</p>
                <div class="box1" id="export_to_grid"></div>
            </div>
        </div>

        <div class="firstBoxs">
            <div class="sub_titlee2" style="margin-left:104px;">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-1.jpg'); ?>" style="margin-left: 90px; margin-bottom: 15px; width:80px;">
                <div class="sub_titlee2-inner">
                    <p>% of Energy Provided by Solar</p>
                    <div class="box1" id="percantage_engery_provided_solar"></div>
                </div>
            </div>
            <!-- <div class="sub_titlee1">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-2.jpg'); ?>" style="margin-left: 70px; margin-bottom: 20px; width: 70px;">
                <div class="box1"></div>
                <p>% of Bill Reduction</p>
            </div> -->
            <div class="sub_titlee2">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-3.jpg'); ?>" style="margin-left: 90px; margin-bottom: 22px; width: 78px;">
                <div class="sub_titlee2-inner">
                    <p>Reduces Carbon Footprint by</p>
                    <div class="box1" id="reduces_carbon"></div>
                </div>
            </div>
        </div>


        <div class="fimagee mt54"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="footer">
                <div class="redf">
                </div>
                <div class="greyf">
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script>
        var saving_meter_calculation = <?php echo $saving_meter_calculation; ?>;
        var system_performance = <?php echo $system_performance; ?>;
        var dollarUSLocale = Intl.NumberFormat('en-US');

        google.load('visualization', '1', {
            packages: ['corechart']
        });

        var pvwatts_data_result = <?php echo $pvwatts_data_result; ?>;
        var pvSt_month1 = 0;
        var pvSt_month2 = 0;
        var pvSt_month3 = 0;
        var pvSt_month4 = 0;
        var pvSt_month5 = 0;
        var pvSt_month6 = 0;
        var pvSt_month7 = 0;
        var pvSt_month8 = 0;
        var pvSt_month9 = 0;
        var pvSt_month10 = 0;
        var pvSt_month11 = 0;
        var pvSt_month12 = 0;
        var pvtotal_production = 0;
        var pvSt_avg_prev = 0;
        var pvSt_avg = 0;




        $(document).ready(function() {
            $("#final_electricity_calculation").text('$' + dollarUSLocale.format(system_performance.FinalLoadChargesWithRates.toFixed(0)));
            $("#annual_solar_production").text(dollarUSLocale.format(system_performance.AnnualSolarProduction.toFixed(0)) + ' kWh');
            $("#offset_consumption").text(dollarUSLocale.format(saving_meter_calculation.offset_consumption.toFixed(0)) + ' kWh');
            $("#percantage_engery_provided_solar").text(system_performance.FinalPercantageEnergyProvidedBySolar.toFixed(2) + '%');
            $("#bill_after_solar").text('$' + dollarUSLocale.format(system_performance.FinalBillAfterSolar.toFixed(0)));
            $("#export_to_grid").text(system_performance.FinalPercantageOfExport.toFixed(2) + '%');

            $("#bill_saving").text((((system_performance.FinalLoadChargesWithRates.toFixed(2) - system_performance.FinalBillAfterSolar.toFixed(2)) * 100) / system_performance.FinalLoadChargesWithRates.toFixed(2)).toFixed(2) + '%');

            $("#reduces_carbon").text(dollarUSLocale.format(system_performance.FinalReducesCarbon.toFixed(0)) + ' kg');

            console.log(system_performance);

            getPvwattCalcAPI();
        })

        function getPvwattCalcAPI() {

            console.log(pvwatts_data_result.outputs);
            var pv_output = pvwatts_data_result;

            pvSt_month1 = pvSt_month1 + pv_output.outputs.ac_monthly[0];
            pvSt_month2 = pvSt_month2 + pv_output.outputs.ac_monthly[1];
            pvSt_month3 = pvSt_month3 + pv_output.outputs.ac_monthly[2];
            pvSt_month4 = pvSt_month4 + pv_output.outputs.ac_monthly[3];
            pvSt_month5 = pvSt_month5 + pv_output.outputs.ac_monthly[4];
            pvSt_month6 = pvSt_month6 + pv_output.outputs.ac_monthly[5];
            pvSt_month7 = pvSt_month7 + pv_output.outputs.ac_monthly[6];
            pvSt_month8 = pvSt_month8 + pv_output.outputs.ac_monthly[7];
            pvSt_month9 = pvSt_month9 + pv_output.outputs.ac_monthly[8];
            pvSt_month10 = pvSt_month10 + pv_output.outputs.ac_monthly[9];
            pvSt_month11 = pvSt_month11 + pv_output.outputs.ac_monthly[10];
            pvSt_month12 = pvSt_month12 + pv_output.outputs.ac_monthly[11];

            // pvSt_avg = pvSt_avg_prev + (pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12);

            pvSt_avg = pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12
            pvSt_avg = Math.round(pvSt_avg / 12);
            pvSt_avg_prev = pvSt_avg_prev + pvSt_avg;
            pvSt_avg = pvSt_avg_prev;




            //  parseInt(pvtotal_production) + 
            pvtotal_production = parseInt(pv_output.outputs.ac_annual);
            // console.log(pvSt_month1)
            // $("#pv_json_data").html(pvwatts_data_html);

            // $("#pvtotal_production span").text(Math.round(pvtotal_production));

            google.setOnLoadCallback(drawChart);
        }

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Year", "kWh", "Average"],
                ["JAN", Math.round(self.pvSt_month1), self.pvSt_avg],
                ["FEB", Math.round(self.pvSt_month2), self.pvSt_avg],
                ["MAR", Math.round(self.pvSt_month3), self.pvSt_avg],
                ["APR", Math.round(self.pvSt_month4), self.pvSt_avg],
                ["MAY", Math.round(self.pvSt_month5), self.pvSt_avg],
                ["JUN", Math.round(self.pvSt_month6), self.pvSt_avg],
                ["JUL", Math.round(self.pvSt_month7), self.pvSt_avg],
                ["AUG", Math.round(self.pvSt_month8), self.pvSt_avg],
                ["SEP", Math.round(self.pvSt_month9), self.pvSt_avg],
                ["OCT", Math.round(self.pvSt_month10), self.pvSt_avg],
                ["NOV", Math.round(self.pvSt_month11), self.pvSt_avg],
                ["DEC", Math.round(self.pvSt_month12), self.pvSt_avg],
            ]);
            var options = {
                curveType: 'function',
                titleTextStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontStyle: 'normal',
                    fontSize: 12
                },
                bar: {
                    groupWidth: "88%"
                },
                legend: {
                    position: 'bottom',
                    textStyle: {
                        fontName: 'Arial, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 14,
                        fontStyle: 'normal'
                    }
                },
                hAxis: {
                    format: "#'$'",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                vAxis: {
                    format: "#'kWh'",
                    title: "",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                width: 910,
                height: 380,
                legend: {
                    position: 'none',
                    textStyle: {
                        fontSize: 4,
                    }
                },
                seriesType: 'bars',
                series: {
                    0: {
                        color: '#A1CB79'
                    },
                    1: {
                        type: 'line',
                        color: '#000',
                        visibleInLegend: false
                    }
                }
            };
            var chart = new google.visualization.ComboChart(document.getElementById('avg_kw_per_day_chart'));
            chart.draw(data, options);
        }
    </script>
</body>

</html>
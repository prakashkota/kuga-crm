
var lead_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data) ? JSON.parse(options.lead_data) : {};
    this.temp_lead_data = {};
    this.lead_id = '';
    this.postcode = '';
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.user_group = (options.user_group && options.user_group != '') ? options.user_group : '';
    this.lead_to_userids = (options.lead_to_userids !== '') ? JSON.parse(options.lead_to_userids) : [];
    this.follow = false;


    $('#category_id').on('change', function () {
        self.fetch_sub_category_item_data();
    });

    $('#locationPostCode').change(function () {
        self.postcode = document.getElementById('locationPostCode').value;
        self.fetch_franchise_list(self.lead_data.assigned_userid);
        if (self.user_group == '1') {
            var data = {};
            data.postcode = self.postcode;
            data.userid = self.lead_data.assigned_userid;
            self.fetch_lead_count_by_franchise(data);
        }
    });

    /**$('#customer_name,#customer_lastname').change(function () {
        var data = {};
        data.firstname = document.getElementById('customer_name').value;
        data.lastname = document.getElementById('customer_lastname').value;
        self.validate_customer(data);
    });**/

    $('#customer_email').change(function () {
        var data = {};
        data.email = document.getElementById('customer_email').value;
        self.validate_customer(data);
    });

    $('#contactMobilePhone').change(function () {
        var data = {};
        data.contact_no = document.getElementById('contactMobilePhone').value;
        self.validate_customer(data);
    });
    
    $('#locationstreetAddressVal').change(function () {
        var data = {};
        data.address = document.getElementById('locationstreetAddressVal').value;
        self.validate_customer(data);
    });

    $('#customer_email').change(function () {
        if ($(this).val().length > 5) {
            var flag = self.validateEmail($(this).val());
            if (!flag) {
                $('#customer_email').addClass('is-invalid');
                $('#lead_actions').addClass('hidden');
                toastr["error"]('Invalid Email Entered');
            } else {
                $('#customer_email').removeClass('is-invalid');
                $('#lead_actions').removeClass('hidden');
            }
        }
    });

    $('#lead_btn_finish').click(function () {
        self.save_customer();
    });

    $('#lead_btn_follow').click(function () {
        self.follow = true;
        self.save_lead_details();
    });

    $('#lead_btn_reset').click(function () {
        self.lead_data = {};
        self.lead_id = '';
        self.postcode = '';
        self.lead_to_userids = [];
        self.follow = false;
        document.getElementById("lead_form").reset();
        document.getElementById("select2-customer_address-container").innerHTML = 'Search for address';
        document.getElementById("businessId").innerHTML = '<option>Enter Customer Postcode First</option>';
        document.getElementById("franchise_name").innerHTML = '';
        document.getElementById("franchise_contact_name").innerHTML = '';
        $('#lead_btn_follow').addClass('hidden');
        $('#lead_btn_finish').removeClass('hidden');
    });

    $('#confirm_customer_yes').click(function () {
        var data = self.temp_lead_data;
        $('#customer_name').val(data.first_name);
        self.lead_data = data;
        self.lead_to_userids = [data.assigned_userid];
        self.populate_lead_data();
        self.temp_lead_data = {};
        $('#confirm_customer_modal').modal('hide');
        $('#locationPostCode').trigger('change');
    });

    $('#confirm_customer_no').click(function () {
        self.temp_lead_data = {};
        $('#confirm_customer_modal').modal('hide');
    });

    //if (self.user_group == '1') {
        self.fetch_lead_count_by_franchise();
    //}
    //self.autocomplete_customer();
    self.manage_franchise_select();

};

lead_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

lead_manager.prototype.populate_lead_data = function () {
    var self = this;
    document.getElementById('cust_id').value = self.lead_data.cust_id;
    document.getElementById('site_id').value = self.lead_data.location_id;
    document.getElementById('cust_title').value = self.lead_data.title;
    document.getElementById('customer_name').value = self.lead_data.first_name;
    document.getElementById('customer_lastname').value = self.lead_data.last_name;
    document.getElementById('customer_email').value = self.lead_data.customer_email;
    document.getElementById('contactMobilePhone').value = self.lead_data.customer_contact_no;
    document.getElementById('select2-customer_address-container').innerHTML = self.lead_data.address;
    document.getElementById('locationstreetAddressVal').value = self.lead_data.address;
    document.getElementById('locationPostCode').value = self.lead_data.postcode;
    document.getElementById('locationState').value = self.lead_data.state_id;
    document.getElementById('category_id').value = self.lead_data.category_id;
    document.getElementById('sub_category_id').value = self.lead_data.sub_category_id;
    document.getElementById('uuid').value = (self.lead_data.uuid != undefined && self.lead_data.uuid != null) ? self.lead_data.uuid : self.create_uuid();
    //document.getElementById('lead_description').value = (self.lead_data.description != undefined && self.lead_data.description != null) ? self.lead_data.description : '';
    document.getElementById('lead_note').value = (self.lead_data.note != undefined && self.lead_data.note != null) ? self.lead_data.note : '';
    document.getElementById('roof_type').value = (self.lead_data.roof_type != undefined && self.lead_data.roof_type != null) ? self.lead_data.roof_type : '';
    document.getElementById('no_of_stories').value = (self.lead_data.no_of_stories != undefined && self.lead_data.no_of_stories != null) ? self.lead_data.no_of_stories : 0;
    self.fetch_sub_category_item_data(self.lead_data.category_id, self.lead_data.sub_category_id);
    self.fetch_franchise_list(self.lead_data.assigned_userid);
    if (self.lead_data.uuid != undefined && self.lead_data.uuid != null && self.lead_data.uuid != '') {
        $('#lead_btn_follow').removeClass('hidden');
        $('#lead_btn_finish').addClass('hidden');
        toastr["success"]('Lead associated with the selected customer found. Proceed for follow-up.');
    } else {
        toastr["error"]('No Lead found with the selected customer.');
    }
    self.lead_id = (self.lead_data.lead_id != undefined && self.lead_data.lead_id != null) ? self.lead_data.lead_id : '';
};

lead_manager.prototype.autocomplete_customer = function () {
    var self = this;
    $(document).on('keydown', '#customer_name', function () {
        var id = this.id;
        $('#customer_name').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: base_url + "admin/customer/fetch_customers_by_keyword",
                    type: 'post',
                    dataType: "json",
                    data: {search: request.term, request: 1},
                    success: function (data) {
                        response(data.customers);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            select: function (event, ui) {
                var data = ui.item.data;

                self.temp_lead_data = data;
                var html = 'Customer already exists:';
                html += '<br/><br/><b>Name:</b> ' + data.first_name + ' ' + data.last_name;
                html += '<br/><br/><b>Address:</b> ' + data.address;
                html += '<br/><br/><b>Phone:</b> ' + data.customer_contact_no;
                html += '<br/><br/><b>Email:</b> ' + data.customer_email;
                if (data.full_name != '' && data.full_name != 'null' && data.full_name != null) {
                    html += '<br/><br/>Note: Customer has been allocated to Franchisee ' + data.full_name + ' (' + data.franchise_address + ')';
                }
                $('#confirm_customer_modal_body').html('');
                $('#confirm_customer_modal_body').html(html);
                $('#confirm_customer_modal').modal('show');

            }
        });
    });
};

lead_manager.prototype.fetch_sub_category_item_data = function (category_id, sub_category_id) {
    var self = this;
    category_id = (category_id != undefined && category_id != '') ? category_id : $('#category_id').val();
    sub_category_id = (sub_category_id != undefined && sub_category_id != '') ? sub_category_id : '';
    var form_data = 'category_id=' + category_id;
    $.ajax({
        url: base_url + 'admin/service/category_item_data',
        type: 'get',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            var sub_category = document.getElementById('sub_category_id');
            sub_category.innerHTML = '';
            if (response.success == true) {
                for (var i = 0; i < response.category_item_data.length; i++) {
                    var option = document.createElement('option');
                    option.setAttribute('value', response.category_item_data[i].id);
                    option.innerHTML = response.category_item_data[i].category_name;
                    if (sub_category_id == response.category_item_data[i].id) {
                        option.setAttribute('selected', 'selected');
                    }
                    sub_category.appendChild(option)
                }
            } else {
                var option = document.createElement('option');
                option.setAttribute('value', '');
                option.innerHTML = 'No Sub Category Found';
                sub_category.appendChild(option)
            }
        }
    });
};

lead_manager.prototype.autopopulate_franchise_list = function (data, selected) {
    var self = this;
    var option;
    var franchise_select = document.getElementById('businessId');
    franchise_select.innerHTML = '';
    if (data.franchise_list.length > 0) {
        var franchise_list = data.franchise_list;
        for (var i = 0; i < franchise_list.length; i++) {
            option = document.createElement('option');
            option.setAttribute('value', data.franchise_list[i].user_id);
            option.innerHTML = data.franchise_list[i].full_name + ' (' + data.franchise_list[i].email + ')';
            if (selected == data.franchise_list[i].user_id) {
                option.setAttribute('selected', 'selected');
            }
            if ((selected == '' || selected == null) && data.selected_franchise.length > 0) {
                if (data.selected_franchise[0].user_id == data.franchise_list[i].user_id) {
                    option.setAttribute('selected', 'selected');
                }
            }
            option.setAttribute('data-item', JSON.stringify(data.franchise_list[i]));
            franchise_select.appendChild(option);
        }
    } else {
        var default_franchise = data.default_franchise;
        option = document.createElement('option');
        option.setAttribute('value', default_franchise.user_id);
        option.innerHTML = default_franchise.full_name + ' (' + default_franchise.email + ')';
        option.setAttribute('selected', 'selected');
        option.setAttribute('data-item', JSON.stringify(default_franchise));
        franchise_select.appendChild(option);
    }
    $('#businessId').trigger('change');
};

lead_manager.prototype.fetch_franchise_list = function (userid) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/franchise_auto_assign',
        type: 'get',
        data: {uuid: self.lead_data.uuid, postcode: self.postcode,userid:userid},
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.autopopulate_franchise_list(response, userid);
            }
        }
    });
};

lead_manager.prototype.manage_franchise_select = function () {
    var self = this;
    $('#businessId').change(function () {
        var data = $(this).find('option:selected').attr('data-item');
        if (data != undefined) {
            data = JSON.parse(data);
            $('#franchise_contact_name').html(data.full_name);
            $('#franchise_name').html(data.company_name);
            self.lead_to_userids = [$(this).find('option:selected').val()];
        }
    });
}

lead_manager.prototype.validate_lead_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('#locationPostCode').val();
    var locationState = $('#locationState').val();
    var locationstreetAddress = $('#locationstreetAddressVal').val();
    var firstname = $('#customer_name').val();
    var lastname = $('#customer_lastname').val();
    var contactMobilePhone = $('#contactMobilePhone').val();
    var businessId = $('#businessId').val();
    $('#companyNameForm').val(firstname + ' ' + lastname);
    var companyNameForm = $('#companyNameForm').val();
    var category_id = $('#category_id').val();
    var sub_category_id = $('#sub_category_id').val();
    var customer_email = $('#customer_email').val();
    //Remove invalid
    $('#locationPostCode').removeClass('is-invalid');
    $('#locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');
    $('#contactMobilePhone').removeClass('is-invalid');
    $('#customer_name').removeClass('is-invalid');
    $('#companyNameForm').removeClass('is-invalid');
    $('#businessId').removeClass('is-invalid');
    $('#category_id').removeClass('is-invalid');
    $('#sub_category_id').removeClass('is-invalid');
    $('#customer_email').removeClass('is-invalid');

    if (locationPostCode == '') {
        flag = false;
        $('#locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('#locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }
    if (companyNameForm == '') {
        flag = false;
        $('#companyNameForm').addClass('is-invalid');
    }
    if (contactMobilePhone == '') {
        flag = false;
        $('#contactMobilePhone').addClass('is-invalid');
    }
    if (firstname == '') {
        flag = false;
        $('#customer_name').addClass('is-invalid');
    }
    if (businessId == '') {
        flag = false;
        $('#businessId').addClass('is-invalid');
    }
    if (category_id == '') {
        flag = false;
        $('#category_id').addClass('is-invalid');
    }
    if (sub_category_id == '') {
        flag = false;
        $('#sub_category_id').addClass('is-invalid');
    }
    if (customer_email == '') {
        flag = false;
        $('#customer_email').addClass('is-invalid');
    }

    return flag;
};

lead_manager.prototype.save_customer = function () {
    var self = this;
    if (self.lead_data.cust_id != '' && self.lead_data.cust_id != undefined && self.lead_data.cust_id != 0) {
        self.save_lead_details();
        return false;
    }
    var flag = self.validate_lead_data();

    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
    }

    if (flag) {
        var lead_form = $('#lead_form').serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/customer/save_customer',
            datatype: 'json',
            data: lead_form + '&action=add',
            beforeSend: function () {
                toastr["info"]('1. Adding Customer into the system....');
                $("#lead_btn_finish").attr("disabled", "disabled");
                $("#lead_btn_reset").attr("disabled", "disabled");
            },
            success: function (stat) {
                var stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    self.lead_data.cust_id = stat.customer_id;
                    self.lead_data.site_id = stat.location.location_id;
                    self.lead_data.uuid = self.create_uuid();
                    $('#uuid').val(self.lead_data.uuid);
                    $('#cust_id').val(stat.customer_id);
                    $('#site_id').val(stat.location.location_id);
                    setTimeout(function () {
                        self.save_lead_details();
                    }, 1500);
                } else {
                    toastr["error"](stat.status);
                    $("#lead_btn_finish").removeAttr("disabled");
                    $("#lead_btn_reset").removeAttr("disabled");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#lead_btn_finish").removeAttr("disabled");
                $("#lead_btn_reset").removeAttr("disabled");
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

lead_manager.prototype.save_lead_details = function () {
    var self = this;
    var flag = self.validate_lead_data();

    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }

    if (self.lead_data.cust_id == '' || self.lead_data.cust_id == undefined) {
        toastr["error"]('Customer not Exists in the system');
        return false;
    }

    if (self.lead_data.uuid == '') {
        var uuid = self.create_uuid();
        document.getElementById('uuid').value = uuid;
        self.lead_data.uuid = uuid;
    }

    var form_data = $('#lead_form').serialize();
    $.ajax({
        url: base_url + 'admin/lead/save_lead_details',
        type: 'post',
        data: form_data,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('2. Creating Lead into the system....');
            $("#lead_btn_finish").attr("disabled", "disabled");
            $("#lead_btn_follow").attr("disabled", "disabled");
            $("#lead_btn_reset").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                self.lead_id = response.id;
                toastr["success"](response.status);
                setTimeout(function () {
                    self.assign_franchise();
                }, 1000);
            } else {
                toastr["error"](response.status);
                $("#lead_btn_finish").removeAttr("disabled");
                $("#lead_btn_follow").removeAttr("disabled");
                $("#lead_btn_reset").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#lead_btn_finish").removeAttr("disabled");
            $("#lead_btn_follow").removeAttr("disabled");
            $("#lead_btn_reset").removeAttr("disabled");
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.assign_franchise = function () {
    var self = this;
    var data = {};
    data.user_ids = (self.lead_to_userids && self.lead_to_userids.length > 0) ? self.lead_to_userids : [$('#businessId').find('option:selected').val()];
    data.lead_id = self.lead_id;
    data.follow = (self.follow) ? true : false;
    $.ajax({
        url: base_url + 'admin/lead/assign_lead_to_franchise',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('3. Assigning Lead to specified franchise....');
            $("#lead_btn_finish").attr("disabled", "disabled");
            $("#lead_btn_follow").attr("disabled", "disabled");
            $("#lead_btn_reset").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function () {
                    window.location.href = base_url + 'admin/lead/manage';
                }, 2000);
            } else {
                toastr["error"](response.status);
                $("#lead_btn_finish").removeAttr("disabled");
                $("#lead_btn_follow").removeAttr("disabled");
                $("#lead_btn_reset").removeAttr("disabled");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#lead_btn_finish").removeAttr("disabled");
            $("#lead_btn_follow").removeAttr("disabled");
            $("#lead_btn_reset").removeAttr("disabled");
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.validate_customer = function (data) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/customer/check_unique_customer',
        type: 'post',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.success == false) {
                if ((self.userid != response.customer_data.user_id) && self.user_group != '1' && self.user_group != '7') {
                    $('#lead_actions').addClass('hidden');
                    toastr["error"](response.status);
                    self.lead_data = {};
                    self.lead_id = '';
                    self.postcode = '';
                    self.lead_to_userids = [];
                    self.follow = false;
                    document.getElementById("lead_form").reset();
                    document.getElementById("select2-customer_address-container").innerHTML = 'Search for address';
                    document.getElementById("businessId").innerHTML = '<option>Enter Customer Postcode First</option>';
                    document.getElementById("franchise_name").innerHTML = '';
                    document.getElementById("franchise_contact_name").innerHTML = '';
                    $('#lead_btn_follow').addClass('hidden');
                    $('#lead_btn_finish').removeClass('hidden');
                } else {
                    var data = response.customer_data;
                    self.temp_lead_data = data;
                    var html = 'Customer already exists:';
                    html += '<br/><br/><b>Name:</b> ' + data.first_name + ' ' + data.last_name;
                    html += '<br/><br/><b>Address:</b> ' + data.address;
                    html += '<br/><br/><b>Phone:</b> ' + data.customer_contact_no;
                    html += '<br/><br/><b>Email:</b> ' + data.customer_email;
                    if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                        html += '<br/><br/><b>Franchise:</b> '+ data.full_name; 
                    }
                    if(data.company_name != '' && data.company_name != 'null' && data.company_name != null){
                        html += '<br/><br/><b>Franchise Name:</b> '+ data.company_name;
                    }
                    if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                        html += '<br/><br/><b>Franchise Contact Number:</b> '+ data.company_contact_no; 
                    }
                   
                    if (data.full_name != '' && data.full_name != 'null' && data.full_name != null) {
                        html += '<br/><br/>Note: Customer has been allocated to Franchisee ' + data.full_name + ' (' + data.franchise_address + ')';
                    }
                    $('#confirm_customer_modal_body').html('');
                    $('#confirm_customer_modal_body').html(html);
                    $('#confirm_customer_modal').modal('show');
                    return false;
                }
            } else {
                $('#lead_actions').removeClass('hidden');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_actions').removeClass('hidden');
        }
    });
};

lead_manager.prototype.fetch_lead_count_by_franchise = function (data) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/lead/fetch_lead_count_by_franchise',
        type: 'get',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#franchise_lead_details_table_body').html(createLoader('Please wait while data is being loaded'));
        },
        success: function (response) {
            $('#franchise_lead_details_table_body').html('');
            $('#franchise_lead_details_table').DataTable().clear();
            $('#franchise_lead_details_table').DataTable().destroy();
            if (response.lead_data.length > 0) {
                var lead_data = response.lead_data;
                for (var i = 0; i < lead_data.length; i++) {
                    var tr = document.createElement('tr');
                    var td1 = document.createElement('td');
                    var td2 = document.createElement('td');
                    var td3 = document.createElement('td');
                    td1.innerHTML = lead_data[i].company_full_name;
                    td2.innerHTML = lead_data[i].lead_count;
                    td3.innerHTML = lead_data[i].last_allocated_at;
                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    tr.appendChild(td3);
                    document.getElementById('franchise_lead_details_table_body').appendChild(tr);
                }
                $('#franchise_lead_details_table').DataTable({
                    "order": [[1, "desc"]],
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": true,
                });
            }
        }
    });
};


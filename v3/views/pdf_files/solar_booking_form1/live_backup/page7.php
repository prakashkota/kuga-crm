<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Additional Photos</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <?php if ($booking_form_image->additional_photos_2 != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->additional_photos_2; ?>" width="800" height="250" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="800" height="250" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" height="250">
                                    <?php echo $booking_form->notes; ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="163" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Pipedrive Deal ID.</td>
                                <td width="667" valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <strong><?php echo $booking_form->pipedrive_id; ?></strong>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="147">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" /></td>
                </tr>
            </table></td>
    </tr>
</table>
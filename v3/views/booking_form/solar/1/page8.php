<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/embedded-generation.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">To:</td>
                                <td width="350" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                    <input required="" id="to" name="booking_form[to]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="10" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">I,</td>
                                <td width="350" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                    <input required="" id="i" name="booking_form[i]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                </td>
                                <td width="450" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                    nominate Kuga Electrical for anywork related to the embedded generation
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td width="220" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Connection process of Company,</td>
                                <td width="610" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                    <input required="" id="company_name" name="booking_form[company_name]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">at the address</td>
                                <td width="720" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                    <input required="" id="company_address" name="booking_form[company_address]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">NMI:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="nmi" name="booking_form[nmi]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Meter Number:</td>
                                                        <td width="251" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="meter_no" name="booking_form[meter_no]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Regards,</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style=" padding:5px; height: 180px; width:100%;">
                        <div class="signature-pad">
                            <div class="signature-pad--body">
                                <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_3" data-id="3" class="sign_create"></div>
                                <input required="" id="signature" name="booking_form[signature]" type="hidden" />
                            </div>
                        </div>
                        <div class="signature-pad-image signature_image_container" style="display:none !important;">
                            <img src="" />
                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="35" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Plesae Sign Here</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Name:</td>
                                            <td width="325" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="customer_name" name="booking_form[customer_name]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                            <td width="325" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="customer_position" name="booking_form[customer_position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Company:</td>
                                            <td width="750" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="customer_company_name" name="booking_form[customer_company_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>

            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/solar_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
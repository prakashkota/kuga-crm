<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <title>Solar Booking Form</title>
        <style>
             @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            .form-check.form-check-inline {
                display: inherit;
            }
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 20px;
                color: transparent !important;
                background: #fff;
                width: 20px;
                height: 20px;
                border: 2px solid #a2a2a2;
                margin-right: 5px;
                border-radius: 3px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        </style>
    </head>
    <body style="padding:0; margin:0">
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page1'); ?>    
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page2'); ?>
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page4_1'); ?> 
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page3'); ?>    
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page4'); ?>
        <?php //$this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page5'); ?>    
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page6'); ?>
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page6_1'); ?>
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page7'); ?>    
        <?php //$this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page8'); ?>    
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page9'); ?>    
        <?php //$this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page10'); ?>    
        <?php $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page11'); ?>
    </body>
</html>
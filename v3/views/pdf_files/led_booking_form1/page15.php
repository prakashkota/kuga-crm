<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_02.jpg'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Details of Previous Upgrade ( If Any )</strong></td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                    <td height="35" style="padding:5px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">How long has the current business occupied the premise?</td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;" width="676" valign="top"><strong><?php echo $prev_upgrade->how_long_premise; ?></strong></td>
                </tr>
                <tr>
                    <td height="30" style="padding:5px; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">Has a previous upgrade under an Energy Efficiency Scheme occurred at this premise?</td>
                </tr>
                <tr>
                    <td style="padding:5px;"><table width="150" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="33">
                                    <?php if (isset($prev_upgrade->is_occured_at_premise) && $prev_upgrade->is_occured_at_premise == '1') { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light" />
                                    <?php } ?>
                                </td>
                                <td width="58" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">YES</td>
                                <td width="32">
                                     <?php if (isset($prev_upgrade->is_occured_at_premise) && $prev_upgrade->is_occured_at_premise == '0') { ?>
                                        <input type="checkbox" class="checkbox_light" checked=""  />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light" />
                                    <?php } ?>
                                </td>
                                <td width="27" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px">NO</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="13"></td>
                </tr>
                <tr>
                    <td height="23" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>If a previous upgrade has occurred:</strong></td>
                </tr>
                <tr>
                    <td height="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Which areas were previously upgraded?</td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;">
                        <strong><?php echo $prev_upgrade->which_areas_upgraded; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td height="13"></td>
                </tr>
                <tr>
                    <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">What type of upgrade occurred? (Tubes, High, Bays, etc...)</span></td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;">
                        <strong><?php echo $prev_upgrade->type_of_upgrade; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td height="13"></td>
                </tr>
                <tr>
                    <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">How many fittings were upgraded?</span></td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                        <strong><?php echo $prev_upgrade->no_of_fittings; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td height="13"></td>
                </tr>
                <tr>
                    <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">When did the previous upgrade occur? (Approximation to month and year is acceptable)</span></td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;">
                        <strong><?php echo $prev_upgrade->occur_at; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td height="13"></td>
                </tr>
                <tr>
                    <td height="30"><span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Which installation company carried out the previous upgrade? (If known)</span></td>
                </tr>
                <tr>
                    <td style="border:solid 1px #e4e4e4; height:70px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333;">
                        <strong><?php echo $prev_upgrade->which_company_upgraded; ?></strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333"><strong>****Make sure that if there has been a previous upgrade that the details are recorded on the RCP****</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table></td>
    </tr>
    <tr>
    <td height="210">&nbsp;</td>
  </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />15</td>
                </tr>
            </table></td>
    </tr>
</table>
<?php
  $subtotal = $gst = $total_payable = 0;
  $height = 600;
  $productcount = 0;
  $tableCount = 0;
?>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
<style>
    .font-family{font-family: 'Open Sans', sans-serif;}
    .table-border{border:solid 1px #ccc;}
    .w910{width:910px; }
    body{margin:0px !important;}
</style>
</head>
<body style="padding:0; margin:0">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
  <tr>
    <td valign="top" style="border: 0; font: 0"><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2" height="30"></td>
              </tr>
              <tr>
                <td width="58" align="left" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/header_lines.jpg" width="45" height="40" alt=""/></td>
                <td width="822" style="font-size: 25px; color: #000000; text-transform: uppercase"><strong><span style="color: #DE1717">YOUR</span> SOLAR SYSTEM AND INCLUSIONS </strong></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="50"></td>
        </tr>
        <tr>
          <td valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td width="440" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td style="color:#DE1717; font-size:16px"><strong>Invoice to</strong></td>
                    </tr>
                    <tr>
                      <td height="15"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 15px; color: #000000"><strong><?php echo $customer['name']; ?></strong></td>
                    </tr>
                    <tr>
                      <td height="20" style="font-size: 14px; color: #000000"><?php echo str_ireplace(", Australia", "", $quote['address']); ?></td>
                    </tr>
                  </tbody>
                </table></td>
                <td width="440" align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td height="40" align="right" style="font-size: 20px; border-bottom: solid 2px #DE1717"><strong>YOUR QUOTE</strong></td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td width="22"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/calender.jpg" width="16" height="16" alt=""/></td>
                            <td width="300" style="font-size: 15px; color: #000000"><strong>DATE:</strong> <?php echo date('d-m-Y'); ?></td>
                            <td width="22" align="right"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/quote.jpg" width="16" height="16" alt=""/></td>
                            <td width="122" align="right" style="font-size: 15px; color: #000000"><strong>QUOTE ID:</strong> <?php echo $quote['quote_id']; ?></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td height="18"></td>
                    </tr>
                  </tbody>
                </table></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="25"></td>
        </tr>
        <tr>
          <td align="left" valign="top">
            <?php if(!empty($products['quote_details'])){  foreach($products['quote_details'] as $detail){ ?>
                <?php if($detail['title'] != ''){ ?>
                <span style="font-size:20px; color: #333333; text-transform: uppercase"><strong><?= $detail['title']; ?></strong></span>
                <table width="840" border="1" bordercolor="#999999" cellspacing="0" cellpadding="7" style="border-collapse:collapse; border:solid 1px #999999">
                    <tbody>
                      <tr>
                        <th width="650" align="left" style="font-size:16px; color: #ffffff; background: #DE1717; text-transform: uppercase"><strong>Product</strong></th>
                        <th align="right" style="font-size:16px; color: #ffffff; background: #DE1717;  text-transform: uppercase"><strong>Price</strong></th>
                      </tr>
    
                      <?php if(!empty($detail['product_details'])){ foreach($detail['product_details']['product_type'] as $key => $product){ ?>
                        <tr>
                          <td width="650" align="left" style="font-size:16px; color: #333333;">
                            <?= $detail['product_details']['item_name'][$key].', '.$detail['product_details']['item_capacity'][$key].', '.$detail['product_details']['item_description'][$key] ?>
                              
                            </td>
                          <td align="right" style="font-size:16px; color: #333333;">$<?= $detail['product_details']['item_price'][$key] ?></td>
                        </tr>
                      <?php $subtotal = $subtotal + $detail['product_details']['item_price'][$key]; $productcount++; } } ?>
                    </tbody>
                </table>
                <?php $tableCount++; } ?>
              <br>
            <?php  } } ?>
          </td>
        </tr>
        <tr>
          <td height="20"></td>
        </tr>

        <?php

          if($subtotal > 0){
            $gst = ((10/100)*$subtotal);
          }

          $total_payable = $subtotal + $gst;
        ?>
        <tr>
          <td height="110" align="right" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="7">
            <tbody>
              <tr>
                <td width="278" style="background: #dcdcdb; text-transform: uppercase; color: #333333; font-size:16px; text-align: right"><strong>SUB TOTAL exc GST</strong></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo number_format($subtotal,2,'.',''); ?></strong></td>
              </tr>
              <tr>
                <td colspan="2" style="padding: 1px !important;height: 10px;"></td>
              </tr>
              <tr>
                <td width="278" style="background: #dcdcdb; text-transform: uppercase; color: #333333; font-size:16px; text-align: right"><strong>GST</strong></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo number_format($gst,2,'.',''); ?></strong></td>
              </tr>
              <tr>
                <td colspan="2" style="padding: 1px !important;height: 10px;"></td>
              </tr>
              <tr>
                <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>TOTAL PAYABLE</strong> <span style="font-size: 14px">(GST INCLUSIVE)</span></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$ <?php echo number_format($total_payable,2,'.','');; ?> </strong></td>
              </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td align="left" valign="top" style="padding-bottom: 5px"><table width="840" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="842" style="color: #333333; font-size: 16px;text-transform:uppercase;"><strong>Inclusions</strong></td>
          </tr>
          <tr>
            <td width="842" height="100" align="top" style="color: #333333;font-size: 16px;border: 1px solid #c3c3c3;padding: 10px;vertical-align: top;"><?php echo !empty($products['inclusions']) ? $products['inclusions'] : ''; ?></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>
</td>
</tr>
</table>
<table>
    <?php
        
        if($tableCount > 1){
            $height = $height -( ($productcount * 35) + ($tableCount * 200));
        }else{
            $height = $height - ( ($productcount * 35) + ($tableCount * 110));
        }
        if($height < 0){
            $height = 60;
        }
    ?>
    <tr>
      <td height="<?= $height ?>"></td>
    </tr>
    <tr>
        <td align="bottom" style="position:relative;bottom: -10px;">
            <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/footer.jpg" width="100%" alt=""/>
        </td>
    </tr>
</table>
</body>
</html>
<link href="<?php echo site_url(); ?>assets/css/mapping_tool.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
<div class="col-12 " style="height: 677px; padding:0px;overflow: hidden;">
    <div id="mask" class="mask" style="position: absolute;">
        <div class="container-fluid">
            <div class="maskblock">
                <div class="maskblockinn">
                    <div class="splash">
                        <div class="form-group pac-input1">
                            <select class="form-control" style="width:85%;" name="features" id="features">
                                <?php
                                foreach ($product_mapping as $val) {
                                    $lgtwd = explode('x', strtolower($val['lenght_width']));
                                    ?>
                                    <option value="<?php echo $lgtwd[0] . '|' . $lgtwd[1] . '|' . $val['solarPanel_gap'] . '|' . $val['solarPanel_roof_edge_gap'] . '|' . $val['deselect_color_status']; ?>"><?php echo $val['product_name'] . '(' . $val['lenght_width'] . ')'; ?></option>
                                <?php } ?>                          
                            </select> 
                        </div>
                        <div class="location-selector clearfix"> 
                            <div class="input-group">
                            <!-- <input id="pac-input2" data-tap-disabled="true" type="text" class="form-control" placeholder="123 Example Street, Somewhere" style="-webkit-text-fill-color:#000; -webkit-transform: translateZ(0px);">
                                -->
                                <select class="select2 placecomplete form-control" id="mapping_tool_placecomplete" style="height:48px !important;"></select>
                            </div>
                            <div class="input-group-append">
                                <button class=" btn-search" type="button" onClick="removeMask()" id="search-address" style="    position: absolute; margin:0px 0px 0px 5px;"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </div>        

                    </div>
                    <p>Access to this system is restricted to employees of Kuga Electrical and representatives who have written authority. </p>				
                </div>
            </div>

        </div>  	
    </div>
    <div id="map"></div>
</div>


<!-- Modal -->

<div class="modal" id="calculate-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Roof Mapping Details</h4>
                <button type="button" class="close" onclick="closeModal('calculate-modal');">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="cntPanel" style="padding-top:10px;"></div>
                <div id="NthAngel"></div>
            </div>
        </div>
    </div>
</div>

<!-- For Creating Image -->
<div class="modal" id="create-img-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Roof Mapping Image</h4>
                <button type="button" class="close" onclick="closeModal('create-img-modal');">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <a id="btn-Convert-Html2Image" href="#">Download</a>
                <div id="shImg"></div>
            </div>
        </div>
    </div>
</div> 
<!-- End of Image -->

<div class="modal fade" id="panel_mode_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog newModal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Please Select Panel Mode</h5>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-danger" onclick="setPanelMode('landscape');">Landscape</button>
                <button type="button" class="btn btn-danger" onclick="setPanelMode('potrait');">Potrait</button>
            </div>
        </div>
    </div>
</div>

<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<script>
    var near_map_api_key = '<?php echo isset($near_map_api_key) ? $near_map_api_key : ''; ?>';
</script>
<script type="text/javascript" src="<?php echo site_url(); ?>assets/js/map-custom.js?v=<?php echo version; ?>"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/Highly-Customizable-jQuery-Toast-Message-Plugin-Toastr/build/toastr.css" />
<script src="https://www.jqueryscript.net/demo/Highly-Customizable-jQuery-Toast-Message-Plugin-Toastr/toastr.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing&callback=initMap"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bjornharrtell/jsts/gh-pages/1.1.2/jsts.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script>
    function closeModal(id) {
        $('#' + id).modal('hide');
    }

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        
        var google_map_app = new googleMapApp();
        
        $('#mapping_tool_placecomplete').placecomplete({
            placeServiceResult: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    console.log("hi1");
                    google_map_app.setAddress(data);
                    $('#show_something').html(data.adr_address);
                    var address_components = data.address_components;

                }
            },
            language: 'fr'
        });
        
        $('#locationstreetAddress').placecomplete1({
            placeServiceResult1: function (data, status) {
                 console.log("hi2");
                if (status != 'INVALID_REQUEST') {

                    $('#show_something').html(data.adr_address);

                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                    }
                                }
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                                $('#locationPostCode').trigger('change');
                            }
                        }
                    }
                }
            },
            language: 'fr'
        });
     
    });
</script>


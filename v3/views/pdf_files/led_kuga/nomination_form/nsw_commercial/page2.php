<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/header.jpg" width="910" height="127" alt=""/></td>
			</tr>
			<tr>
				<td height="45"></td>
			</tr>
			<tr>
				<td><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 1 - Accredited Certificate Provider Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="400" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ACP (company)</td>
												</tr>
												<tr>
													<td><input readonly="" type="text" name="input" value="Kuga Electrical Pty Ltd" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td width="400" valign="top"><table align="right" width="370" border="0"  cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact Name</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="Louie Latumbo" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ABN</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="39 616 409 584" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact email</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="l.latumbo@13kuga.com.au" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">RESA accreditation</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact phone</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 2 - Original Energy Saver Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="400" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Name</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][entity_name]" type="text"  value="<?php echo $energy_saver_details->entity_name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td width="400" valign="top"><table align="right" width="370" border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Phone Number</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][contact_no]" type="text"  value="<?php echo $energy_saver_details->contact_no; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Email</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][email]" type="text"  value="<?php echo $energy_saver_details->email; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ABN Number</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][abn]" type="text"  value="<?php echo $energy_saver_details->abn; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 3 - Implementation Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.1 - Implementation Sites</td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; height:25px">Address</td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][address]" type="text"  value="<?php echo $certificate_provider_details->address; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
									<tr>
										<td height="7"></td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][address1]" type="text"  value="<?php echo $certificate_provider_details->address1; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="13"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.2 - Energy saving activities implemented</td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; height:25px">Description</td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][description]" type="text"  value="<?php echo $certificate_provider_details->description; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="13"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.3 - Past activities implemented at the site(s)</td>
						</tr>
						<tr>
							<td valign="top">
								<table width="800" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:25px" width="701">Have you or, to the best of your knowledge, has any other person, previously nominated an energy saver for this activity
											or activities at this site(s)?</td>
											<td width="99">
												<table width="99" border="0" cellspacing="0" cellpadding="0">
													<tbody>
														<tr>
															<td width="33" height="30" style="font-size:12px; color:#6a6a6a;">
															<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '1'){ ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } else {  ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } ?>
															</td>
															<td width="66" style="font-size:12px; color:#6a6a6a;">Yes</td>
														</tr>
														<tr>
															<td style="font-size:12px; color:#6a6a6a;">
																<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '0'){ ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } else {  ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } ?>
															</td>
															<td style="font-size:12px; color:#6a6a6a;">No</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>

							</td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="25" style="font-size:12px; color:#6a6a6a;">If yes, please provide a reason for the new nomination form</td>
						</tr>
						<tr>
							<td><input type="text"  name="get_nomination_form_details[past_activities][reason]" value="<?php echo $past_activities->reason; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
						</tr>
						<tr>
							<td height="90"></td>
						</tr>
					</tbody>
				</table></td>
			</tr>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/bottom.jpg" width="910" height="102" alt=""/></td>
			</tr>
		</tbody>
	</table>
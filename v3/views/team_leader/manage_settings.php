<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    
    .sr-switch {
        font-size: 1rem;
        position: relative;
    }
    .sr-switch input {
        position: absolute;
        height: 1px;
        width: 1px;
        background: none;
        border: 0;
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        overflow: hidden;
        padding: 0;
    }
    .sr-switch input + label {
        position: relative;
        min-width: calc(calc(2.375rem * .8) * 2);
        border-radius: calc(2.375rem * .8);
        height: calc(2.375rem * .8);
        line-height: calc(2.375rem * .8);
        display: inline-block;
        cursor: pointer;
        outline: none;
        user-select: none;
        vertical-align: middle;
        text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
    }
    .sr-switch input + label::before,
    .sr-switch input + label::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: calc(calc(2.375rem * .8) * 2);
        bottom: 0;
        display: block;
    }
    .sr-switch input + label::before {
        right: 0;
        background-color: #dee2e6;
        border-radius: calc(2.375rem * .8);
        transition: 0.2s all;
    }
    .sr-switch input + label::after {
        top: 2px;
        left: 2px;
        width: calc(calc(2.375rem * .8) - calc(2px * 2));
        height: calc(calc(2.375rem * .8) - calc(2px * 2));
        border-radius: 50%;
        background-color: white;
        transition: 0.2s all;
    }
    .sr-switch input:checked + label::before {
        background-color: #08d;
    }
    .sr-switch input:checked + label::after {
        margin-left: calc(2.375rem * .8);
    }
    .switch input:focus + label::before {
        outline: none;
        box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
    }
    .sr-switch input:disabled + label {
        color: #868e96;
        cursor: not-allowed;
    }
    .sr-switch input:disabled + label::before {
        background-color: #e9ecef;
    }
    .switch.switch-sm {
        font-size: 0.875rem;
    }
    .sr-switch.switch-sm input + label {
        min-width: calc(calc(1.9375rem * .8) * 2);
        height: calc(1.9375rem * .8);
        line-height: calc(1.9375rem * .8);
        text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
    }
    .sr-switch.switch-sm input + label::before {
        width: calc(calc(1.9375rem * .8) * 2);
    }
    .sr-switch.switch-sm input + label::after {
        width: calc(calc(1.9375rem * .8) - calc(2px * 2));
        height: calc(calc(1.9375rem * .8) - calc(2px * 2));
    }
    .sr-switch.switch-sm input:checked + label::after {
        margin-left: calc(1.9375rem * .8);
    }
    .sr-switch + .sr-switch {
        margin-left: 1rem;
    }

</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Edit Team Leader Settings</h1>
        <span class="sr-switch">
            <input type="checkbox" class="sr-switch" onchange="changeAvailability(this);" id="availability" <?php echo (!empty($user_details) && $user_details['availability'] == 1) ? 'checked="checked"' : ''; ?>>
            <label for="availability">Availability</label>
        </span>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <form role="form"  action="" method="post"  enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Company Details (Settings) 
                                        <span style="display:inline-flex; font-size:14px; float:right;">
                                            <a href="javascript:void(0);" id="request_detail_change_btn">Request Detail Change</a>
                                        </span>
                                    </h4>
                                    <div class="form-group">
                                        <label class="control-label">Team Leader Name</label>
                                        <input type="text" id="full_name"  class="form-control" value="<?php echo (!empty($user_details)) ? $user_details['full_name'] : ''; ?>" placeholder="Team Leader Name"  readonly>
                                        <?php echo form_error('user_details[full_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" id="company_name" value="<?php echo (!empty($user_details)) ? $user_details['company_name'] : ''; ?>"  class="form-control" placeholder="Company Name" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text"  id="mobilePhone" value="<?php echo (!empty($user_details)) ? $user_details['company_contact_no'] : ''; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>ABN</label>
                                        <input type="text"  id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_abn'] : ''; ?>" class="form-control" placeholder="ABN" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Account Name</label>
                                        <input type="text" name="user_details[company_account_name]"  value="<?php echo (!empty($user_details)) ? $user_details['company_account_name'] : ''; ?>" class="form-control" placeholder="Company Account Name" >
                                    </div>

                                    <div class="form-group">
                                        <label>BSB</label>
                                        <input type="text" name="user_details[company_bsb]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_bsb'] : ''; ?>" class="form-control" placeholder="Company BSB" >
                                    </div>

                                    <div class="form-group">
                                        <label>Account Number</label>
                                        <input type="text" name="user_details[company_account]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_account'] : ''; ?>" class="form-control" placeholder="Company Account Number" >
                                    </div>

                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Working Location Details</h4>

                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label class="">Secondary Postcodes</label>
                                                <select class="postcodes1 form-control" name="user_locations[workarea_postcodes_secondary][]" style="height:48px !important;" multiple="multiple"></select>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>

                            <div class="buttwrap">
                                <input type="submit" id="addNewCompanyBtn" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                            </div> 
                        </form>
                    </div>



                    <div class="col-md-6" >
                        <form role="form"  action="<?php echo site_url('admin/franchise/request_settings_change'); ?>" method="post"  enctype="multipart/form-data" class="hidden" id="request_detail_change_form">
                            <div class="card">
                                <div class="card-body ">
                                    <h4 class="form-section">Company Details (Request Change Settings)
                                        <span style="display:inline-flex; font-size:14px; float:right;">
                                            <a href="javascript:void(0);" id="request_detail_change_hide_btn">Hide</a>
                                        </span>
                                    </h4>
                                    <div class="form-group">
                                        <label class="control-label">Team Leader Name</label>
                                        <input type="text" name="user_details[full_name]" id="full_name"  class="form-control" value="<?php echo (!empty($user_details)) ? $user_details['full_name'] : ''; ?>" placeholder="Team Leader Name"  >
                                        <?php echo form_error('user_details[full_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" name="user_details[company_name]" id="company_name" value="<?php echo (!empty($user_details)) ? $user_details['company_name'] : ''; ?>"  class="form-control" placeholder="Company Name" >
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" name="user_details[company_contact_no]" id="mobilePhone" value="<?php echo (!empty($user_details)) ? $user_details['company_contact_no'] : ''; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
                                    </div>

                                    <div class="form-group">
                                        <label>ABN</label>
                                        <input type="text" name="user_details[company_abn]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_abn'] : ''; ?>" class="form-control" placeholder="ABN" >
                                    </div>

                                </div>
                            </div>
                            <div class="buttwrap">
                                <input type="submit" data-loading-text="Saving..." class="btn" value="Submit" >
                            </div> 
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script>
                $(document).ready(function () {
                    $('#request_detail_change_btn').click(function () {
                        $('#request_detail_change_form').removeClass('hidden');
                    });
                    $('#request_detail_change_hide_btn').click(function () {
                        $('#request_detail_change_form').addClass('hidden');
                    });

                    var workarea_postcodes_secondary = JSON.parse('<?php echo json_encode($workarea_postcodes_secondary); ?>');
                    var initialPropertyOptions1 = [];

                    if (workarea_postcodes_secondary.length > 0) {
                        for (var i = 0; i < workarea_postcodes_secondary.length; i++) {
                            var obj = {};
                            obj.id = workarea_postcodes_secondary[i];
                            obj.text = workarea_postcodes_secondary[i];
                            obj.selected = true;
                            initialPropertyOptions1.push(obj);
                        }
                    }

                    $(".postcodes1").select2({
                        data: initialPropertyOptions1,
                        ajax: {
                            url: base_url + "admin/fetch_postcodes",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page
                                };
                            },
                            processResults: function (data, params) {
                                return {
                                    results: data.results,
                                };
                            },
                            cache: true
                        },
                        placeholder: 'Search for postcodes',
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work
                        minimumInputLength: 1,
                        templateResult: formatResults,
                    });


                    function formatResults(data) {
                        if (data.loading) {
                            return data.text;
                        }
                        return data.text;
                    }


                });


                function changeAvailability(self) {
                    var availability = self.checked;
                    var data = {availability:availability};
                    $.ajax({
                        url: base_url + "admin/change_availability_status",
                        type: 'post',
                        data:data,
                        dataType: "json",
                        success: function (data) {
                            if(data.success == true){
                                toastr["success"](data.status);
                            }else{
                                toastr["error"](data.status);  
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
</script>
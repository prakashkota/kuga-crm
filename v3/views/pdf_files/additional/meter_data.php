<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quick Quote</title>

<style>
@media print {
.no-print {display: none;}
body {background: transparent;}
.red{color:#c32027}
.black-bg {background-color:#010101}
.dark-bg{background-color:#282828}
</style>
<link href="https://fonts.googleapis.com/css?family=Mr+De+Haviland&display=swap" rel="stylesheet">
</head>

<body style="padding:0; margin:0">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/images/meter_data.jpg'; ?>" alt="" width="910" height="126" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
            <tr>
                <td valign="top">
                  <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                        <tbody>
                            <tr>
                              <td>
                            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" >
                            <tbody>
                            <tr>
                            <td >To whom it may concern,</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            <td >I have sought permission for the owner of the below electricity account to retrieve interval data and latest electricity bill issued to the customer.</td>
                            </tr>
                            </tbody>
                            </table>
                          </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="330" valign="top" style="padding-right:10px">
                                                    <table width="320" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                <td width="300" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Pleae supply 30 minutes meter data (1 year) from</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="500" valign="top" style="padding-left:10px">
                                                    <table width="390" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                                 <td width="220" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input class="datepicker" id="from_date" name="meter_data[from_date]" value="<?php echo (isset($meter_data)) ? $meter_data['from_date'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                                                 <td style="padding-left:15px; padding-right:5px;"> to </td>
                                                                <td  width="200" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input class="datepicker" id="to_date" name="meter_data[to_date]" value="<?php echo (isset($meter_data)) ? $meter_data['to_date'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                             <tr>
                            <td>Please send your latest bill to 
                                <a href="#" style="text-decoration: underline;"><?php echo $user['email']; ?></a>
                            </td>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <td>
                                I/We undersigned hereby provide permission for Kuga Electrical (ABN:39 616 409 584) to obtain access to our electricity consumption data for the below premises.
                            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Electricity Account Name:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="account_name" name="meter_data[account_name]" value="<?php echo (isset($meter_data)) ? $meter_data['account_name'] : ''; ?>"  style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>ABN/ACN:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="abn" name="meter_data[abn]" value="<?php echo (isset($meter_data)) ? $meter_data['abn'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Address:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="address" name="meter_data[address]" value="<?php echo (isset($meter_data)) ? $meter_data['address'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>NMI:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="nmi" name="meter_data[nmi]"  value="<?php echo (isset($meter_data)) ? $meter_data['nmi'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Energy Retailer:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="energy_retailer" name="meter_data[energy_retailer]" value="<?php echo (isset($meter_data)) ? $meter_data['energy_retailer'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Signature:</strong></td>
                                            </tr>
                                           
                                             <tr>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                               <td align="center" style="border:solid 1px #e8e8e8; padding:5px;">
                                  <img width="400" height="80" src="<?php echo $this->config->item('live_url') . 'assets/uploads/meter_data_form_files/'. $meter_data['signature']; ?>" />
                              </td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Name:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="rep_name" name="meter_data[rep_name]" value="<?php echo (isset($meter_data)) ? $meter_data['rep_name'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Date:</strong></td>
                                                <td width="628" valign="top" style="border:solid 1px #e8e8e8; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input id="request_date" class="datepicker" name="meter_data[request_date]" value="<?php echo (isset($meter_data)) ? $meter_data['request_date'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;" type="text" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
           
             </tbody>
                    </table>
                    </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                  <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td height="35" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333333">Regards,
                                </td>
                            </tr>
                            <tr>
                                 <td style="padding-left:10px; font-size:60px; font-family: 'Mr De Haviland', cursive;">
                                    <?php echo $user['full_name']; ?>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                                   <?php echo $user['full_name']; ?>  <br/>
                                    Commercial Sales Manager<br/>
                                    Kuga Electrical
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr height="30">
                 <td>&nbsp;</td>
             </tr>
   <tr>
    <td height="90" class="bg-bottom" style="background:#ededed">
      <table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">4 Bridge Road, Keysborough VIC 3173</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">6 Turbo Road, Kings Park NSW 2148</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/images/bottom_right.jpg'; ?>" width="117" height="56" /></td>
      </tr>
    </table>
  </td>
  </tr>
             </table>
</body>
</html>

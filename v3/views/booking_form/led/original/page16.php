<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101;color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Check List</strong></td>
      </tr>
      <tr>
        <td height="30" class="black-bg" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Please write all relevant conversations with customers in the Note Section:</strong></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">1.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Have you explained the disclaimers?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">2.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Have you had pricing discussions with the customer. If so what was discussed?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">3.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Have you shown Samples to the client?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">3a.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Do any need to be installed?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">4.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Access issues? including the provisioning of equipment and fees associaated with the supply of faulty or dangerous<br />
              units by customer.</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">5.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Scissor lift/Boom lift accessibility?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">6.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Has the customer agreed to clear access? Be specific, what areas?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">7.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Are all the fittings to be upgraded working?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">8.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Are there any globes missing?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">8a.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Have you asked the customer to replace any missing globes?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
       <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">9.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Have you explained the difference between Electronic and Magnetic ballast and how this may affect pricing?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">10.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">What are the access times?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">11.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Are there induction requirements?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">12.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Is there someone else we need to organise this with? i.e. Centre Management</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px"><table width="900" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
            <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">13.</td>
            <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Are areas vacant at certain times?</td>
          </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <tr>
    <td><table width="900" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
        <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">14.</td>
        <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">is the customer aware that the payment is required on day of completion?</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <tr>
    <td><table width="900" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="49"><img src="list_check.jpg" width="37" height="37" /></td>
        <td width="26" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">15.</td>
        <td width="755" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">If there has been a previous upgrade, have the details been recorded on the RCP?</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
      </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Parking will be made available<br />
    Office Hours 8am - 5pm Mon-Fri</strong></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <tr>
    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>There are a few 600 x 300 twin Fluor fittings not to be upgraded<br />
    $0 cost to customer for upgrading 76 fittings to 1200 x 300 panel lights</strong></td>
  </tr>
  <tr>
    <td height="50">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>
<?php include 'header.php'; ?>
<div class="rating-section-outer">
    <div class="alert alert-danger error-message-form" style="display: none;"></div>
    <div class="rating-section-inst clearfix">        
        <!--<a href="" class="solar-logo"><img src="<?php //echo site_url();     ?>assets/images/logo.png" alt="logo"/></a>-->
        <form id="survey">
            <div class="top-rating clearfix">
                <div class="hmt-rating"><h3>Rating</h3></div>
                <div class="hmt-rating">
                    <div class="star-ratings">
                        <div class="star-ratings-top" style="width:<?php echo $rate * 20; ?>%;"></div>
                        <div class="star-ratings-bottom"></div>
                    </div>
                </div>
                <div class="hmt-rating"><h4><?php echo $rate_message[$rate]; ?></h4></div>
                <input type="hidden" value="<?php echo $rate; ?>" class="star-rating-input" name="rating">
                <input type="hidden" value="<?php echo $lead_id; ?>" name="lead_id">
                <input type="hidden" value="<?php echo $customer_id; ?>" name="customer_id">
            </div>
            <div class="qst-box clearfix">
                <ul>
                    <?php
                    // print_r($questions );
                    foreach ($questions as $i => $question) {
                        ?>
                        <li class="clearfix">
                            <h4><?php echo $question['question'] ?></h4>
                            <input type="hidden" value="<?php echo $question['question']; ?>" name="questions[<?php echo $i; ?>]">
                            <div class="rlt-qst clearfix">
                                <?php
                                $options = json_decode($question['options'], true);
                                foreach ($options as $o => $option) {
                                    ?>            
                                    <div class="check-one">
                                        <?php if ($i == 0) { ?>
                                            <h5><?php echo $option['option_text'] ?></h5>
                                        <?php } ?>
                                        <label class="custom-radio-btn">
                                            <input type="radio" value="<?php echo $option['option_value']; ?>" name="answers[<?php echo $i; ?>]">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                <?php } ?>                            
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="comment-box">
                <h4>Comments about your experience:</h4>
                <textarea class="comments-typ" name="comment"></textarea>
                <input type="submit" class="done-btn not-loader" value="Submit">
                <button class="done-btn ldr" style="display: none;"><i class="fa fa-gear fa-spin"></i></button>
            </div>
        </form>
    </div>
</div>
<?php include 'footer.php'; ?>

<script>
    $(document).ready(function () {
        $(document).on('submit', '#survey', function (e) {
            $('.error-message-form').slideUp().text();
            e.preventDefault();
            var formData = $(this).serialize();
            $('.ldr').show();
            $('.not-loader').hide();
            $.ajax({
                url: '<?php echo site_url(); ?>survey/submitFeedback',
                method: 'post',
                data: formData,
                success: function (res) {
                    $('.ldr').hide();
                    $('.not-loader').show();
                    var output = JSON.parse(res);
                    if (output.status) {
                        window.location.href = '<?php echo site_url(); ?>survey/thankyou';
                    } else {
                        $('.error-message-form').text(output.message).slideDown();
                    }
                }
            });
        });
        var fullWidth = $('.star-ratings-bottom').innerWidth();
        $(document).on('click mousemove', '.star-ratings-bottom,.star-ratings-top', function (e) {
            var x = e.pageX - $(this).offset().left;
            var ratingPercentage = ((x / fullWidth) * 100);
            ratingPercentage = ratingPercentage.toFixed(0);
            var message = '';
            if (ratingPercentage <= 20) {
                ratingPercentage = 20;
                message = 'Bad';
            } else if (ratingPercentage <= 40) {
                ratingPercentage = 40;
                message = 'Poor';
            } else if (ratingPercentage <= 60) {
                ratingPercentage = 60;
                message = 'OK but some issues';
            } else if (ratingPercentage <= 80) {
                ratingPercentage = 80;
                message = 'Good';
            } else if (ratingPercentage <= 100) {
                ratingPercentage = 100;
                message = 'Excellent';
            }
            $(this).closest('.top-rating').find('.star-rating-input').val(ratingPercentage / 20);
            $(this).closest('.top-rating').find('.hmt-rating h4').text(message);
            $(this).parent().find('.star-ratings-top').css({"width": ratingPercentage + "%"});
        });
    });
</script>
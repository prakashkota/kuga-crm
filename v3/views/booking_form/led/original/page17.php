<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Additional Notes</strong></td>
      </tr>
      <tr>
        <td height="100" valign="top" style="color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px; border:solid 1px #e4e4e4; padding:5px">Note:</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Other Kuga Electrical representative involved, if applicable. Place their name here.</td>
      </tr>
      <tr>
        <td style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">&nbsp;</td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333"><strong>Call the office on 1300 005 842 to arrange temporary booking date and time.</strong></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">If you can’t arrange a booking. put today’s date and the reason why you can’t make the booking in the “Scheduler who booked the job:” field</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="402" height="30" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Date and Time of Temporary Booking:</td>
            <td width="23" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">&nbsp;</td>
            <td width="405" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Scheduler who booked the job:</td>
          </tr>
          <tr>
            <td style="border:solid 1px #e4e4e4; height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">&nbsp;</td>
            <td></td>
            <td valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333"><strong>Pls book for ASAP</strong></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="163" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Pipedrive Deal ID.</td>
            <td width="667" valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333"><strong>Tlogic</strong></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">By signing this form you acknowledge that all information is tru and correct. You also accept responsibility for any and all costs incurred by providing inaccurate information.</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333"><strong>KUGA ELECTRICAL SALES REPRESENTATIVE SIGNATURE</strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center" style="border:solid 1px #e4e4e4; padding:5px;"><img src="signature.jpg" width="477" height="167" /></td>
      </tr>
      <tr>
        <td height="35" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Plesae Sign Here</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
  </tr>
</table>

</div>
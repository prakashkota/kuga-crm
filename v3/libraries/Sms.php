<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms
{

    /**
     * Constructor
     */
    public function __construct()
    {
        // get main CI object
        $this->lt = &get_instance();
    }

    public function send_sms($communication_data = [], $new = TRUE)
    {
        $type = (isset($new) && $new == TRUE) ? 'NEW LEAD' : "FOLLOW UP";
        #remove 0 as first character from mobile no
        $f_mobile = $communication_data['sales_rep_contact_no'];
        //$c_mobile = $communication_data['customer_contact_no'];
        if (preg_match("/^[0][0-9]+$/", $f_mobile)) {
            $f_mobile = substr($f_mobile, 1);
        }

        $franchise_text = $customer_text = '';
        require_once(APPPATH . 'libraries/BurstSMS_APIClient2.php');
        $api = new transmitsmsAPI(SMS_API_KEY, SMS_API_SECRET);

        //Removinng Australia on any occurence from franchise and customer address
        //$f_address = str_ireplace(",Australia", "_",$communication_data['franchise_address']);
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        #send sms to franchisee
        $franchise_text = $type . " {CATEGORY_NAME}: {CUSTOMER_COMPANY_NAME} {CUSTOMER_ADDRESS} {CUSTOMER_NAME} .\r\n"
            . "PH: {CUSTOMER_MOBILE}. Pls call in the next 2hrs.";
        $franchise_text = str_replace("{CATEGORY_NAME}", $communication_data['category_name'], $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_COMPANY_NAME}", $communication_data['company_name'], $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_ADDRESS}", $c_address, $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $franchise_text);
        $franchise_text = str_replace("{CUSTOMER_NOTE}", ($communication_data['note'] == '') ? '-' : $communication_data['note'], $franchise_text);
        $f_message = $franchise_text;
        $result = $api->sendSms($f_message, "61{$f_mobile}", "SolarRun");

        if ($result->error->code == 'SUCCESS') {
            #send sms to customer
            //$customer_text = "{FRANCHISE_NAME} from {FRANCHISE_COMPANY_NAME} will be in touch within the next 2 business hours. {FRANCHISE_CONTACT_NO}. Thankyou.";
            //$customer_text = str_replace("{FRANCHISE_COMPANY_NAME}", $communication_data['franchise_company_name'], $customer_text);
            //$customer_text = str_replace("{FRANCHISE_NAME}", $communication_data['franchise_name'], $customer_text);
            //$customer_text = str_replace("{FRANCHISE_CONTACT_NO}", $communication_data['franchise_contact_no'], $customer_text);
            //$c_message = $customer_text;
            //$api->sendSms($c_message, "61{$c_mobile}", "SolarRun");
            return true;
        } else {
            return false;
        }
    }

    public function send_custom_sms($message = '', $to = '', $smsSenderName = 'Kuga')
    {
        #remove 0 as first character from mobile no
        if (preg_match("/^[0][0-9]+$/", $to)) {
            $to = substr($to, 1);
        }
        require_once(APPPATH . 'libraries/BurstSMS_APIClient2.php');
        $api = new transmitsmsAPI(SMS_API_KEY, SMS_API_SECRET);
        $api->sendSms($message, "61{$to}", $smsSenderName);
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Pripedrive Migration Controller
 * @version 1.0
 */
class Pipedrive_Migration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('Components');
        $this->load->library('Pipedrive');
        if (!$this->aauth->is_loggedin()) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            } else {
                redirect('admin/login');
            }
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function getDeals() {
        $insert_data = $update_data = array();
        $deals_data = $this->pipedrive->get_deals();
        $i = 0;
        foreach ($all_data as $key => $value) {
            //First name and Last name
            $name = explode(' ', $value['person_name']);
            $fname = (isset($name[0])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[0])) : NULL;
            $lname = (isset($name[1])) ? preg_replace("/[^A-Za-z0-9?![:space:]]/", '', trim($name[1])) : NULL;
            if ($value['org_name'] !== '') {
                //Cust Data
                $cust_data[$i]['first_name'] = $fname;
                $cust_data[$i]['last_name'] = $lname;
                $cust_data[$i]['company_name'] = $company_name[] = $value['org_name'];
                //$cust_data[$i]['customer_email'] = $all_data[$key]['customer_email'];
                //$cust_data[$i]['customer_contact_no'] = $all_data[$key]['customer_contact_no'];
                $cust_data[$i]['created_at'] = date('Y-m-d H:i:s', strtotime($value['add_time']));
                $cust_data[$i]['is_processed'] = 1;
                //Lead Data
                $stage_id = 1;
                $lead_status = 'open';
                $lead_status = ($value['lead_status'] != '') ? strtolower($value['status']) : $lead_status;
                if (trim($value['lead_stage']) == 'Prospect') {
                    $stage_id = 1;
                } else if (trim($value['lead_stage']) == 'Follow Up') {
                    $stage_id = 2;
                } else if (trim($value['lead_stage']) == 'New Lead') {
                    $stage_id = 3;
                } else if (trim($value['lead_stage']) == 'Appointment Booked') {
                    $stage_id = 4;
                } else if (trim($value['lead_stage']) == 'Need Proposal') {
                    $stage_id = 5;
                } else if (trim($value['lead_stage']) == 'Proposal Rework') {
                    $stage_id = 6;
                } else if (trim($value['lead_stage']) == 'Proposal Delivered') {
                    $stage_id = 7;
                } else if (trim($value['lead_stage']) == 'Decision Due') {
                    $stage_id = 8;
                } else if (trim($value['lead_stage']) == 'Closable Deal') {
                    $stage_id = 9;
                } else if (trim($value['lead_stage']) == 'Gone Cold') {
                    $stage_id = 10;
                } else if (trim($value['lead_stage']) == 'Won') {
                    $stage_id = 11;
                    $lead_status = 'won';
                } else if (trim($value['lead_stage']) == 'Lost') {
                    $stage_id = 12;
                    $lead_status = 'lost';
                } else if (trim($value['lead_stage']) == 'Proposal Ready') {
                    $stage_id = 13;
                }

                $lead_data[$i]['lead_stage'] = $stage_id;
                $lead_data[$i]['lead_status'] = $lead_status;
                $lead_data[$i]['created_at'] = date('Y-m-d H:i:s', strtotime($value['add_time']));
                $lead_data[$i]['updated_at'] = (isset($value['update_time']) && $value['update_time'] != NULL && $value['update_time'] != '') ? date('Y-m-d H:i:s', strtotime($value['update_time'])) : date('Y-m-d H:i:s', strtotime($value['add_time']));
                $lead_data[$i]['user_id'] = $value['owner_name'];
                $lead_data[$i]['lead_value'] = $value['value'];
                $lead_data[$i]['lead_type'] = $value['lead_type'];
                $lead_data[$i]['status'] = $value['status'];

                //Cust Loc Data
                $cust_loc_data[$i]['address'] = $value['org_id']['address'];
                if (stripos($cust_loc_data[$i]['address'], 'NSW') !== false) {
                    $cust_loc_data[$i]['state_id'] = 2;
                } else if (stripos($cust_loc_data[$i]['address'], 'New South Wales') !== false) {
                    $cust_loc_data[$i]['state_id'] = 2;
                } else {
                    $cust_loc_data[$i]['state_id'] = 7;
                }

            }
            $i++;
        }
    }

    public function getFiles() {
         ini_set('max_execution_time','-1'); 
        $pd_api_data = $this->common->fetch_row('tbl_pipedrive_api_data', '*', array('api' => 'files'));
        $payload = ['start' => $pd_api_data['processed_count'],'limit' => 500];
        $i = 0;
        $deals_files_data = $this->pipedrive->get_files($payload);
        $additional_data = $deals_files_data['additional_data']['pagination'];
        //echo "<pre>";
        //print_r($deals_files_data['data']);die;
        foreach($deals_files_data['data'] as $key => $value){
            $pd_lead_data = $pd_file_data = [];
            $is_cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name' => $value['org_name']));
            $is_pd_org_data = $this->common->fetch_row('tbl_pipedrive_leads_data', '*', array('org_name' => $value['org_name']));
            $kuga_cust_id = NULL;
            if(empty($is_pd_org_data)){
                if(!empty($is_cust_data)){
                  $kuga_cust_id = $is_cust_data['id'];   
                }
                $pd_lead_data['pd_deal_id'] = $value['deal_id'];
                $pd_lead_data['kuga_cust_id'] = $kuga_cust_id;
                $pd_lead_data['org_name'] = $value['org_name'];
                $this->common->insert_data('tbl_pipedrive_leads_data',$pd_lead_data);
                $pd_lead_id = $this->common->insert_id();
            }else{
                $pd_lead_id = $is_pd_org_data['id'];
            }
            //$pd_file_data['pd_deal_id'] = $value['deal_id'];
            //$pd_file_data['pd_lead_id'] = $pd_lead_id;
            //$pd_file_data['pd_file_id'] = $value['id'];
            $pd_file_data['file_data'] = json_encode($value);
            $this->common->update_data('tbl_pipedrive_files',array('pd_file_id' => $value['id']),$pd_file_data);
            
            $this->common->update_data('tbl_pipedrive_api_data',array('api' => 'files'),array('processed_count' => $additional_data['next_start']));
            
        }
        $i = $i + 500;
        sleep(3);
        if($i < 18500){
            $this->getFiles();
        }
    }
    
    public function downloadFiles(){
        $files_data = $this->common->fetch_where('tbl_pipedrive_files', '*',array('is_downloaded' => 0));
        foreach($files_data as $key => $value){
            $file_data = json_decode($value['file_data']);
            $data = $this->pipedrive->download_files([],$file_data);
            if($data['error'] == FALSE){
                file_put_contents(FCPATH . 'assets/uploads/pipedrive_files/'.$file_data->file_name,$data['file']);
                $this->common->update_data('tbl_pipedrive_files',array('pd_file_id' => $value['pd_file_id']),array('file_name' => $file_data->file_name,'is_downloaded' => 1));
            }
            sleep(2);
        }
    }
    
    public function updatePipedriveCustomer(){
        $pd_org_data = $this->common->fetch_where('tbl_pipedrive_leads_data', '*',NULL);
        //echo "<pre>";
        //print_r($pd_org_data);die;
        foreach($pd_org_data as $key => $value){
            $is_cust_data = $this->common->fetch_row('tbl_customers', '*', array('company_name LIKE' => '%'. $value['org_name'].'%'));
            //echo $this->db->last_query();die;
            if(!empty($is_cust_data)){
                $this->common->update_data('tbl_pipedrive_leads_data',array('id' => $value['id']),array('kuga_cust_id' => $is_cust_data['id']));
            }
        }
    }

}

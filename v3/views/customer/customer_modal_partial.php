<div class="modal" id="add_customer_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_customer_modal_title">Add New Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post" name="customerAdd" id="customerAdd" enctype="multipart/form-data">
                            <input type="hidden" name="cust_id" id="company_cust_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="checkcompany"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Business Name</label>
                                        <input type="text" id="bussiness_name" name="company[company_name]" class="form-control" placeholder="Business Name"  required >
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" name="company[customer_contact_no]" id="contactMobilePhone"  class="form-control" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <select name="company[title]" id="cust_title" class="form-control"  required>
                                            <?php
                                            $cust_title = unserialize(cust_title);
                                            foreach ($cust_title as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>   
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="company[first_name]" id="firstname"  class="form-control" placeholder="First Name"  required>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="company[last_name]" id="lastname"  class="form-control" placeholder="Last Name"  required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="company[customer_email]" id="contactEmailId"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text" id="position" name="company[position]" class="form-control" placeholder="Position"  required >
                                    </div>
                                </div>
                            </div>

                            <?php /**
                              <div class="row">
                              <div class="col-md-6">
                              <h4 class="form-section">Site Details</h4>
                              </div>
                              </div>

                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                              <label>First Name</label>
                              <input type="text" name="company[first_name]" id="firstname"  class="form-control" placeholder="First Name"  required>
                              </div>
                              </div>
                              <div class="col-md-6">
                              <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" name="company[last_name]" id="lastname"  class="form-control" placeholder="Last Name"  required>
                              </div>
                              </div>
                              </div>

                              <div class="row">
                              <div class="col-md-12" >
                              <div class="form-group control-group">
                              <label>Customer Address</label>
                              <select class="select2 placecomplete form-control" id="locationstreetAddress"  style="height:48px !important;"></select>
                              <input type="hidden" id="locationstreetAddressVal" name="site[address]" />
                              </div>
                              </div>
                              </div>
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group control-group">
                              <label>State</label>
                              <select name="site[state_id]" id="locationState" class="form-control" required>
                              <option value="">Select State</option>
                              <?php for ($j = 0; $j < count($states); $j++) { ?>
                              <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                              <?php } ?>
                              </select>

                              </div>
                              </div>
                              <div class="col-md-6">
                              <div class="form-group control-group">
                              <label>Post Code</label>
                              <input class="form-control" placeholder="Please Enter Post Code" type="text" name="site[postcode]" id="locationPostCode" value=""  required />

                              </div>
                              </div>
                              </div>
                             * *
                             */ ?>
                            <div class="buttwrap" style="display:inline-flex;">
                                <input type="button" id="save_customer" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                                <div id="customer_loader"></div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td height="35" style="color:#000000; font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>Photo of Front of Electricity Bill</strong></td>
            </tr>
            <tr>
                <td>
                    <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                    <tr>
                        <td align="center" valign="top" >
                            <div class="add-picture" style="height: 1000px;">
                                <span>Add picture</span>
                                <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                <input type="file" class="image_upload" data-id="electricity_bill_front" />
                                <input type="hidden" id="electricity_bill_front" name="booking_form_image[electricity_bill_front]" />
                            </div>
                            <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                <div class="add-picture" style="height: 1000px;"></div>
                                <a class="image_close" href="javscript:void(0);"  ></a>
                            </div>
                        </td>

                    </tr>
                </table></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td style="height: 20px;">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>

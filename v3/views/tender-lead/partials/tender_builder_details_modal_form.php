<div class="modal" id="add_tender_builder_contact_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_tender_builder_contact_modal_title">Add New Builder</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post" name="customerContactAdd" id="customerContactAdd" enctype="multipart/form-data">
                            <input type="hidden" name="company[lead_id]" id="contact_lead_id">
                            <input type="hidden" name="contact_id" id="contact_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="checkcompany"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Company Name</label>
                                        <input type="text" name="company[company_name]" id="contact_company_name"  class="form-control" placeholder="Company Name"  required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Name</label>
                                        <input type="text" name="company[name]" id="contact_name"  class="form-control" placeholder="Name"  required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="company[email]" id="contact_email"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" name="company[contact_no]" id="contact_contact_no"  class="form-control" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required></div>
                                </div>
                            </div>

                            <div class="buttwrap" style="display:inline-flex;">
                                <input type="button" id="save_tender_lead_contact" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                                <div id="tender_lead_contact_loader"></div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Booking_Form Controller
 * @version 1.0
 */
class Booking_Form extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Notify");
        $this->load->library("Components");
        $this->load->library("Simpro");
        $this->load->library("Email_Manager");
        $this->load->library('Exif_Manager');
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Product_Model", "product");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Job_Model", "job");
        if (!$this->aauth->is_loggedin()) {
            //redirect('admin/login');
        }
        $this->views_folder = array(
            6 => 'led', 
            83 => 'led_kuga' ,
        );
    }

    /** LED Booking FORM */
    public function add_led_booking_form($uuid = FALSE) {
        $data = array();
        $user_id = $this->aauth->get_user_id();
        $deal_ref = $this->input->get('deal_ref');
        $site_ref = $this->input->get('site_ref');
        $cost_centre_id = $this->input->get('cost_centre_id');
        
        if ($this->aauth->is_member('Admin')) {
            $data['users'] =  $this->user->get_users(FALSE, FALSE, FALSE);
        } else if ($this->components->is_team_leader()) {
            $owner_id = $this->aauth->get_user_id();
            $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
        } else if ($this->components->is_sales_rep()) {
            $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
            $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
        }
        $data['calculator_items'] = json_decode(file_get_contents(APPPATH . './../assets/calculator/vic.json'),true);
        
        if (isset($deal_ref) && $deal_ref != '' && isset($site_ref) && $site_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref, FALSE, $site_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            if(isset($cost_centre_id) && $cost_centre_id != ''){
                $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('lead_id' => $lead_data['id'], 'site_id' => $site_ref, 'cost_centre_id' => $cost_centre_id));
            }else{
                $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('lead_id' => $lead_data['id'], 'site_id' => $site_ref));
            }
            if (!empty($booking_data)) {
                //redirect(site_url('admin/booking_form/edit_led_booking_form/' . $booking_data['uuid']));
            }
            
            $data['lead_uuid'] = $lead_data['uuid'];
            $data['site_id'] = $site_ref;
            $data['cost_centre_id'] = $cost_centre_id;
            $data['product_types'] = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('parent_id' => LED));
            $data['space_types'] = $this->common->fetch_where('tbl_led_space_types', '*', array('status' => 1, 'state' => 'NSW'));
            $data['existing_fittings'] = $this->common->fetch_where('tbl_led_existing_products', '*', array('status' => 1));
            $data['access_equipments'] = $this->common->fetch_where('tbl_led_access_equipments', '*', array('status' => 1));
            $data['led_products'] = $this->common->fetch_where('tbl_led_new_products', 'np_id,type_id,np_name', array('status' => 1));
            $data['show_db_products'] = TRUE;
            $cond = 'WHERE user.id NOT IN (3,4,23,24) AND user_group.group_id IN(1,6,7) AND user.banned=0 AND user.id='.$user_id;
            $data['logged_in_sales_rep'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
            
            //Now check what ap company is assigned to the cost centre
            $cost_centre_data = $this->job->fetch_row('tbl_cost_centres', '*', array('ccid' => $data['cost_centre_id']));
            $data['ap_company_id'] = $ap_company = !empty($cost_centre_data) ? $cost_centre_data['ap_company_id'] : 6;
            $cc_folder = isset($this->views_folder[$ap_company]) ? $this->views_folder[$ap_company] : 'led';
            
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            if($data['cost_centre_id'] == 2){
                $this->load->view('booking_form/'.$cc_folder.'/heers/main');
            }else if($data['cost_centre_id'] == 3){
                $this->load->view('booking_form/'.$cc_folder.'/nsw_commercial/main');
            }else{
                $this->load->view('booking_form/'.$cc_folder.'/main');
            }
        } else if ($uuid) {
            $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
            if (empty($booking_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/add'));
            }
            
            $lead_uuid = $this->common->fetch_cell('tbl_leads','uuid',array('id' => $booking_data['lead_id']));
            $data['lead_uuid'] = $lead_uuid;
            $data['site_id'] = $booking_data['site_id'];
            $data['product_types'] = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('parent_id' => LED));
            $data['space_types'] = $this->common->fetch_where('tbl_led_space_types', '*', array('status' => 1, 'state' => 'NSW'));
            $data['existing_fittings'] = $this->common->fetch_where('tbl_led_existing_products', '*', array('status' => 1));
            $data['access_equipments'] = $this->common->fetch_where('tbl_led_access_equipments', '*', array('status' => 1));
            $data['led_products'] = $this->common->fetch_where('tbl_led_new_products', 'np_id,type_id,np_name', array('status' => 1));
            $data['show_db_products'] = ($booking_data['id'] > 385) ? TRUE : FALSE;
            $data['booking_form_uuid'] = $uuid;
            $data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0 AND user.id='.$user_id;
            $data['logged_in_sales_rep'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
            
            //Now check what ap company is assigned to the cost centre
            $data['ap_company_id'] = $ap_company = !empty($booking_data) ? $booking_data['ap_company_id'] : 6;
            // $cc_folder = isset($this->views_folder[$ap_company]) ? $this->views_folder[$ap_company] : 'led';
            $cc_folder = isset($this->views_folder[$ap_company]) ? $this->views_folder[$ap_company] : 'led';
            
            //echo $cc_folder;die;
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            // if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
            //     echo '<pre>';
            //     print_r($cc_folder);
            //     echo '</pre>';
            //     die;
            // }
            if($data['cost_centre_id'] == 2){
                $this->load->view('booking_form/'.$cc_folder.'/heers/main');
            }else if($data['cost_centre_id'] == 3){
                $this->load->view('booking_form/'.$cc_folder.'/nsw_commercial/main');
            }else{
                $this->load->view('booking_form/'.$cc_folder.'/main');
            }
        } else {
            redirect('admin/dashboard');
        }
    }
    
    
     public function fetch_products() {
        $data = array();
		
        if (!empty($this->input->post())) {
            $search = $this->input->post('search');
            $search = trim($search);
			$cost_centre_id = $this->input->post('cost_centre_id');
            $product_type_id = $this->input->post('product_type_id');
           
			$cond_crm = " WHERE tp.name LIKE '%$search%' AND tp.cost_centre_id =".$cost_centre_id . " AND tp.status=1"; 
			if(isset($product_type_id) && $product_type_id != ''){
			   $cond_crm .= " AND pt.type_id='$product_type_id'";
			}
			$ed_products =  [];
			$own_products = $this->job->get_products_for_po('tp.name as label',$cond_crm);
			$data['products'] = array_merge($ed_products,$own_products);
          
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
        die;
    }
	
	

    public function save_led_booking_form($uuid = FALSE) {
        $data = array();
        if (!empty($this->input->post())) {
            $is_booking_exists = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $this->input->post('uuid')));
            $form_data = array();
            $form_data['business_details'] = json_encode($this->input->post('business_details'));
            $form_data['authorised_details'] = json_encode($this->input->post('authorised_details'));
            $form_data['account_details'] = json_encode($this->input->post('account_details'));
            $form_data['space_type'] = json_encode($this->input->post('space_type'));
            $form_data['ceiling_height'] = json_encode($this->input->post('ceiling_height'));
            $form_data['boom_req'] = json_encode($this->input->post('boom_req'));
            $form_data['product'] = json_encode($this->input->post('product'));
            $form_data['ae'] = json_encode($this->input->post('ae'));
            $form_data['booking_form_image'] = json_encode($this->input->post('booking_form_image'));
            $form_data['prev_upgrade'] = json_encode($this->input->post('prev_upgrade'));
            $form_data['customer_check_list'] = json_encode($this->input->post('customer_check_list'));

            $geo_tag_data = $this->input->post('geo_tag_data');
            if(isset($geo_tag_data) && !empty($geo_tag_data)){
                $latitude =  $geo_tag_data['latitude'];
                $longitude =  $geo_tag_data['longitude'];
                $altitude =  $geo_tag_data['altitude'];
                $altitude = number_format((float)$altitude, 2, '.', '');
            }

            //Convert Base64 signatures to png
            $booking_form = $this->input->post('booking_form');
            if (isset($booking_form['authorised_on_behalf']['signature'])) {
                $img = explode(';', $booking_form['authorised_on_behalf']['signature']);
                if ($booking_form['authorised_on_behalf']['signature'] != '' && count($img) > 1) {
                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_on_behalf']['signature']));
                    $booking_form['authorised_on_behalf']['signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);
                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }


            if (isset($booking_form['authorised_by_behalf']['sales_rep_signature'])) {
                $img = explode(';', $booking_form['authorised_by_behalf']['sales_rep_signature']);
                if ($booking_form['authorised_by_behalf']['sales_rep_signature'] != '' && count($img) > 1) {
                    /**$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_by_behalf']['sales_rep_signature']));
                    $booking_form['authorised_by_behalf']['sales_rep_signature'] = $newfilename = $this->components->uuid() . '.jpg';
                    file_put_contents('./assets/uploads/led_booking_form_files/' . $newfilename, $image);*/
                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_by_behalf']['sales_rep_signature']));
                    $booking_form['authorised_by_behalf']['sales_rep_signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            if (isset($booking_form['sales_rep_signature'])) {
                $img = explode(';', $booking_form['sales_rep_signature']);
                if ($booking_form['sales_rep_signature'] != '' && count($img) > 1) {
                    /**$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['sales_rep_signature']));
                    $booking_form['sales_rep_signature'] = $newfilename = $this->components->uuid() . '.jpg';
                    file_put_contents('./assets/uploads/led_booking_form_files/' . $newfilename, $image);*/

                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['sales_rep_signature']));
                    $booking_form['sales_rep_signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            $form_data['booking_form'] = json_encode($booking_form);

            $get_nomination_form_details = $this->input->post('get_nomination_form_details');
            $get_nomination_form_authorised_details = $get_nomination_form_details['authorised_details'];
            if (isset($get_nomination_form_authorised_details['signature'])) {
                $img = explode(';', $get_nomination_form_authorised_details['signature']);
                if ($get_nomination_form_authorised_details['signature'] != '' && count($img) > 1) {
                    /**$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $get_nomination_form_authorised_details['signature']));
                    $get_nomination_form_details['authorised_details']['signature'] = $newfilename = $this->components->uuid() . '.jpg';
                    file_put_contents('./assets/uploads/led_booking_form_files/' . $newfilename, $image);*/

                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $get_nomination_form_authorised_details['signature']));
                    $get_nomination_form_details['authorised_details']['signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }


            if (isset($get_nomination_form_authorised_details['custom_signature'])) {
                $img = explode(';', $get_nomination_form_authorised_details['custom_signature']);
                if ($get_nomination_form_authorised_details['custom_signature'] != '' && count($img) > 1) {
                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $get_nomination_form_authorised_details['custom_signature']));
                    $get_nomination_form_details['authorised_details']['custom_signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }
            
            
            $get_nomination_form_certificate_provider_details = $get_nomination_form_details['certificate_provider_details'];
            if (isset($get_nomination_form_certificate_provider_details['signature'])) {
                $img = explode(';', $get_nomination_form_certificate_provider_details['signature']);
                if ($get_nomination_form_certificate_provider_details['signature'] != '' && count($img) > 1) {

                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $get_nomination_form_certificate_provider_details['signature']));
                    $get_nomination_form_details['certificate_provider_details']['signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0);
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            $get_nomination_form_energy_saver_details = $get_nomination_form_details['energy_saver_details'];
            if (isset($get_nomination_form_energy_saver_details['signature'])) {
                $img = explode(';', $get_nomination_form_energy_saver_details['signature']);
                if ($get_nomination_form_energy_saver_details['signature'] != '' && count($img) > 1) {

                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $get_nomination_form_energy_saver_details['signature']));
                    $get_nomination_form_details['energy_saver_details']['signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0);
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            $site_assessor_form_details = $this->input->post('site_assessor_form_details');
            if (isset($site_assessor_form_details['signature'])) {
                $img = explode(';', $site_assessor_form_details['signature']);
                if ($site_assessor_form_details['signature'] != '' && count($img) > 1) {
                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $site_assessor_form_details['signature']));
                    $site_assessor_form_details['signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            if (isset($site_assessor_form_details['custom_signature'])) {
                $img = explode(';', $site_assessor_form_details['custom_signature']);
                if ($site_assessor_form_details['custom_signature'] != '' && count($img) > 1) {
                    $newfilename = $this->components->uuid() . '.jpg';
                    $imageName = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $site_assessor_form_details['custom_signature']));
                    $site_assessor_form_details['custom_signature'] = $newfilename;
                    $source = imagecreatefromstring($image);
                    $rotate = imagerotate($source, 0, 0); // if want to rotate the image
                    $imageSave = imagejpeg($rotate,$imageName,100);
                    imagedestroy($source);

                    if(isset($geo_tag_data) && !empty($geo_tag_data)){
                        $this->exif_manager->addGpsInfo($imageName, $imageName, '','', '', $longitude, $latitude,$altitude, date('Y-m-d H:i:s'));
                    }
                }
            }

            $form_data['get_nomination_form_details'] = json_encode($get_nomination_form_details);
            $form_data['site_assessor_form_details'] = json_encode($site_assessor_form_details);
            
            $cost_center_id = $this->input->post('cost_centre_id');
            
            if(isset($cost_center_id) && $cost_center_id!=''){
                $form_data['cost_centre_id'] = $cost_center_id;
            }
            $form_data['geo_tag_data'] = json_encode($geo_tag_data);
            $ap_company_id = $this->input->post('ap_company_id');
            if(isset($ap_company_id) && $ap_company_id != ''){
                $form_data['ap_company_id'] = $ap_company_id;
            }
            if (empty($is_booking_exists)) {
                $insert_data = array();
                $insert_data = $form_data;
                $insert_data['uuid'] = $this->input->post('uuid');
                $insert_data['lead_id'] = $this->input->post('lead_id');
                $insert_data['site_id'] = $this->input->post('site_id');
                $insert_data['user_id'] = $this->aauth->get_user_id();
                $stat = $this->common->insert_data('tbl_led_booking_form', $insert_data);
                $quote_id = $this->common->insert_id();
            } else {
                $booking_id = $is_booking_exists['id'];
                $update_data = array();
                $update_data = $form_data;
                $stat = $this->common->update_data('tbl_led_booking_form', array('id' => $booking_id), $update_data);
            }
            if ($stat) {
                $data['status'] = 'Booking Form Data Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to Update';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_led_booking_form_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $this->input->get('uuid')));
            $data['booking_data'] = $booking_data;
            $data['calculator_items'] = json_decode(file_get_contents(APPPATH . '../assets/calculator/nsw_commercial.json'));
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function led_booking_form_pdf_download($uuid) {
        $data = array();
        $customer_payload = $site_payload = $site_contact_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
        $simpro_customer_response = $simpro_site_response = $simpro_contact_response = $simpro_job_response = $section_response = $cost_center_response = array();

        $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        $cost_centre_id = $booking_data['cost_centre_id'];

        try {

            //Create Pdf first if it fails then dont procced with booking form
            $form_data = array();
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['account_details'] = json_decode($booking_data['account_details']);
            $form_data['space_type'] = json_decode($booking_data['space_type']);
            $form_data['ceiling_height'] = json_decode($booking_data['ceiling_height']);
            $form_data['boom_req'] = json_decode($booking_data['boom_req']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['ae'] = json_decode($booking_data['ae']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            $form_data['prev_upgrade'] = json_decode($booking_data['prev_upgrade']);
            $form_data['customer_check_list'] = json_decode($booking_data['customer_check_list']);
            $form_data['form_ap_company_id'] = $booking_data['ap_company_id'];
            
            
            //Check if booking form is a job in job crm and its status is > 15
            if($booking_data['kuga_job_id'] != NULL && $booking_data['kuga_job_id'] != ''){
                $job_status = $this->job->get_job_status($booking_data['kuga_job_id']);
                if($job_status['status_id'] > 15){
                    $data['success'] = FALSE;
                    $data['status'] = 'Cannot update booking form because the associated job is having the job status '. $job_status['status_name'];
                    echo json_encode($data);
                    die;
                }
            }
            
            
            $view = $this->input->get('view');
            if(isset($view) && $view == 'page3'){
                if($cost_centre_id == 3){
                     $html = $this->load->view('pdf_files/led_booking_form/page3_new_nsw', $form_data, TRUE);
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/page3_new', $form_data, TRUE);
                }
                echo $html;die;
            }else if(isset($view) && $view == 'page1_2'){
                $html = $this->load->view('pdf_files/led_booking_form/main1', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_4_10'){
                $html = $this->load->view('pdf_files/led_booking_form/main2', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_11'){
                $html = $this->load->view('pdf_files/led_booking_form/page11', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_11_v2'){
                if($form_data['ae']->ae_name[0] == 'Boom Lift' || $form_data['ae']->ae_name[1] == 'Boom Lift'){
                    $html = $this->load->view('pdf_files/led_booking_form/page_11_v2', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_12'){
                $html = $this->load->view('pdf_files/led_booking_form/page12', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_13'){
                $html = $this->load->view('pdf_files/led_booking_form/page13', $form_data, TRUE);
                // $html += $this->load->view('pdf_files/led_booking_form/page13_1', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_14'){
                $html = $this->load->view('pdf_files/led_booking_form/page14', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_14_1'){
                $html = $this->load->view('pdf_files/led_booking_form/page14_1', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page_all'){
                if($cost_centre_id == 3){
                   $html = $this->load->view('pdf_files/led_booking_form/main3_nsw', $form_data, TRUE);
                }else{
                   $html = $this->load->view('pdf_files/led_booking_form/main3', $form_data, TRUE); 
                }
                echo $html;die;
            }
            
            if($cost_centre_id == 1){

                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A4  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                exec($cmd);
                
                //Build page 11 in landscape mode
                $filename4 = 'page_11_'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11 '. $upload_path_img4;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                $upload_path_img4_v2 = "";
                if($form_data['ae']->ae_name[0] == 'Boom Lift' || $form_data['ae']->ae_name[1] == 'Boom Lift' || $form_data['ae']->ae_name[0] == 'Site Inspection Required' || $form_data['ae']->ae_name[1] == 'Site Inspection Required'){
                    //Build page 11 in landscape mode
                    $filename4_v2 = 'page_11_v2_'.$uuid;
                    $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                    exec($cmd);
                    //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                }
                
                // if($form_data['ae']->ae_name[0] == 'Site Inspection Required' || $form_data['ae']->ae_name[1] == 'Site Inspection Required'){
                //     //Build page 11 in landscape mode
                //     $filename4_v2 = 'page_11_v2_'.$uuid;
                //     $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                //     $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                //     exec($cmd);
                //     //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                // }
                
                //Build page 12 in landscape mode
                $filename5 = 'page_12_'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_12 '. $upload_path_img5;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf');die;
                
                //Build page 13 in landscape mode
                $filename6 = 'page_13_'.$uuid;
                $upload_path_img6 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_13 '. $upload_path_img6;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7 = 'page_14_'.$uuid;
                $upload_path_img7 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14 '. $upload_path_img7;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7_1 = 'page_14_1'.$uuid;
                $upload_path_img7_1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7_1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14_1 '. $upload_path_img7_1;
                exec($cmd);
                
                //Build page 14 in landscape mode
                $filename8 = 'page_all_'.$uuid;
                $upload_path_img8 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_all '. $upload_path_img8;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf');die;
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/'.$filename;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '.$upload_path_img4_v2.' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6 .' '. $upload_path_img7.' '.$upload_path_img7_1 .' '. $upload_path_img8;
                exec($cmd);
            }else if($cost_centre_id == 3){
            
                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A4  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf');die;
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                exec($cmd);
                
                //Build page 11 in landscape mode
                $filename4 = 'page_11_'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11 '. $upload_path_img4;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                $upload_path_img4_v2 = "";
                if($form_data['ae']->ae_name[0] == 'Boom Lift' || $form_data['ae']->ae_name[1] == 'Boom Lift'){
                    //Build page 11 in landscape mode
                    $filename4_v2 = 'page_11_v2_'.$uuid;
                    $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                    exec($cmd);
                    //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                }
                
                //Build page 12 in landscape mode
                $filename5 = 'page_12_'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_12 '. $upload_path_img5;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf');die;
                
                //Build page 13 in landscape mode
                $filename6 = 'page_13_'.$uuid;
                $upload_path_img6 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_13 '. $upload_path_img6;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7 = 'page_14_'.$uuid;
                $upload_path_img7 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14 '. $upload_path_img7;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $upload_path_img7_1 = "";
                // $filename7_1 = 'page_14_1'.$uuid;
                // $upload_path_img7_1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7_1 . '.pdf';
                // $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14_1 '. $upload_path_img7_1;
                // exec($cmd);
                
                //Build page 14 in landscape mode
                $filename8 = 'page_all_'.$uuid;
                $upload_path_img8 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_all '. $upload_path_img8;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf');die;
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/'.$filename;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '.$upload_path_img4_v2.' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6 .' '. $upload_path_img7.' '.$upload_path_img8;
                exec($cmd);
            }else{
                
                if($cost_centre_id == 2){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_heers/main', $form_data, TRUE);
                } else if($cost_centre_id == 3){
                        $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main', $form_data, TRUE);
                }
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                
                $h = '';
                $f = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source" => $html,
                    "action" => 'save',
                    "save_directory" => __DIR__ . '/../../assets/uploads/led_booking_form_files',
                    "file_name" => $filename,
                    "page_size" => 'A4',
                    "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                    "header" => $h,
                    "footer" => $f
                );
                set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                    if (0 === error_reporting()) { return false; }
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                });
                $this->pdf->phptopdf($pdf_options);
            }
            
            
            
            if($cost_centre_id == 1){

                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A2  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                exec($cmd);
                
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/mail_copy_pdf/'.$filename1;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '. $upload_path_img4;
                exec($cmd);
            }else if($cost_centre_id == 3){
            
                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A4  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf');die;
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                exec($cmd);
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/mail_copy_pdf/'.$filename1;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '.$upload_path_img4_v2.' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6 .' '. $upload_path_img7.' '.$upload_path_img8;
                exec($cmd);
            }else{
                //Booking Form Pdf Version Form Mail With Hidden Office Files
                if($cost_centre_id == 2){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_heers/main_mail_copy', $form_data, TRUE);
                } else if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main_mail_copy', $form_data, TRUE);
                }
                
                
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename1 = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                
                $h = '';
                $f = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source" => $html,
                    "action" => 'save',
                    "save_directory" => __DIR__ . '/../../assets/uploads/led_booking_form_files/mail_copy_pdf',
                    "file_name" => $filename1,
                    "page_size" => 'A4',
                    "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                    "header" => $h,
                    "footer" => $f
                );
                set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                    if (0 === error_reporting()) { return false; }
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                });
                $this->pdf->phptopdf($pdf_options);
            }
            
            // echo $html;die;
            //Decode Data
            $business_details = json_decode($booking_data['business_details']);
            $authorised_details = json_decode($booking_data['authorised_details']);
            $space_type = json_decode($booking_data['space_type']);
            $ceiling_height = json_decode($booking_data['ceiling_height']);
            $product = json_decode($booking_data['product']);
            $ae = json_decode($booking_data['ae']);
            $booking_form = json_decode($booking_data['booking_form']);
            $booking_form_image = json_decode($booking_data['booking_form_image']);
            $get_nomination_form_details = json_decode($booking_data['get_nomination_form_details']);
            $site_assessor_form_details = json_decode($booking_data['site_assessor_form_details']); 
            
            $filename = str_replace('.pdf','',$filename);
            $pdf_url = str_replace(' ', '%20', $filename);
            $pdf_url = site_url('assets/uploads/led_booking_form_files/' . $pdf_url . '.pdf');
            $b64_file = base64_encode(file_get_contents($pdf_url));

            
            $filename_new = $filename . '.pdf';
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
            //echo FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf';
            //exit;
            if (!file_exists(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
            
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_led_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));

            
            //Save to Job CRM
            $customer_check_list = json_decode($booking_data['customer_check_list']);
            $booking_form->notes = isset($customer_check_list->notes) ? $customer_check_list->notes : '';
            
            $job_crm_data = [];
            $job_crm_data['kuga_job_id'] = $booking_data['kuga_job_id'];
            $job_crm_data['booking_form_uuid'] = $uuid;
            $job_crm_data['booking_form_id'] = $booking_data['id'];
            $job_crm_data['booking_form_site_id'] = isset($booking_data['site_id']) ? $booking_data['site_id'] : NULL;
            $job_crm_data['business_details'] = $business_details;
            $job_crm_data['authorised_details'] = $authorised_details;
            $job_crm_data['booking_form'] = $booking_form;
            $job_crm_data['booking_form_image'] = $booking_form_image;
            $job_crm_data['filename'] = $filename;
            $job_crm_data['user_id'] = $lead_data['user_id'];
            $job_crm_data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $job_crm_data['ap_company_id'] = $ap_company_id = $booking_data['ap_company_id'];
            $job_crm_data['get_nomination_form_details'] = $get_nomination_form_details;
            $job_crm_data['site_assessor_form_details'] = $site_assessor_form_details;
            $job_crm_data['product_data'] = $product;
            $job_id = $this->create_job_in_job_crm('LED',$lead_data['cust_id'],$job_crm_data);
        
            if($job_id != null){
                
                //Make Entry For Signatures
                if($get_nomination_form_details != '' && $get_nomination_form_details != NULL){
                    $get_nomination_form_authorised_details = $get_nomination_form_details->authorised_details;
                    
                    $newfilename = $get_nomination_form_authorised_details->signature;
                    $signature_data = array();
                    $signature_data['job_id'] = $job_id;
                    $signature_data['user_id'] =  1;
                    $signature_data['section'] = 'NOMINATION_FORM';
                    $signature_data['signature_main_key'] = 'authorised_details';
                    $signature_data['signature_key'] = 'signature';
                    $signature_data['signature_base64'] = NULL;
                    $signature_data['signature_value'] = $newfilename;
                    $signature_data['signature_sys_path'] = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $signature_data['signature_url'] = site_url() . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $signature_data['geo_tag_data'] = NULL;
                    $this->job->insert_data('tbl_job_form_signatures',$signature_data);
                }
                
                if($site_assessor_form_details != '' && $site_assessor_form_details != NULL){
                    $newfilename = $site_assessor_form_details->signature;
                    $signature_data = array();
                    $signature_data['job_id'] = $job_id;
                    $signature_data['user_id'] =  1;
                    $signature_data['section'] = 'SITE_ASSESSOR_FORM';
                    $signature_data['signature_main_key'] = NULL;
                    $signature_data['signature_key'] = 'signature';
                    $signature_data['signature_base64'] = NULL;
                    $signature_data['signature_value'] = $newfilename;
                    $signature_data['signature_sys_path'] = FCPATH . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $signature_data['signature_url'] = site_url() . 'assets/uploads/led_booking_form_files/' . $newfilename;
                    $signature_data['geo_tag_data'] = NULL;
                    $this->job->insert_data('tbl_job_form_signatures',$signature_data);
                }
                
                $this->common->update_data('tbl_led_booking_form', array('uuid' => $uuid), array('kuga_job_id' => $job_id));
                
                $custom_fields = array();
                $kW = '';
                if(isset($product->product_capacity)){
                    $product->product_capacity = json_decode(json_encode($product->product_capacity), true);
                }
                if(!empty($product) && (isset($product->product_capacity[1]) && $product->product_capacity[1] != '')){
                    $pcapacity = (float) $product->product_capacity[1];
                    $pqty = (int) $product->product_qty[1];
                    $kW = ($pcapacity * $pqty)/1000;
                }else {
                    if(!empty($product) && (isset($product->product_name[0]) && $product->product_name[0] != '')){
                        $kW = preg_replace("/[^0-9.]/","",$product->product_name[0]);
                    }
                }
                if($kW != '') {
                    $custom_fields[2] = $kW;
                }
                
                //Battery kWh
                $kWh = '';
                if(!empty($product) && (isset($product->product_capacity[3]) && $product->product_capacity[3] != '')){
                    $pcapacity = (float) $product->product_capacity[3];
                    $pqty = (int) $product->product_qty[3];
                    $kWh = ($pcapacity * $pqty);
                }
                if($kWh != '') {
                    $custom_fields[3] = $kWh;
                }
                $this->create_job_custom_fields_in_job_crm($job_id,$custom_fields);

                //Send Success Mail
                $customer_payload['CompanyName'] = $business_details->entity_name;
                $customer_payload['Phone'] = $authorised_details->contact_no;
                $customer_payload['Email'] = $authorised_details->email;
            
                $user_id = $this->aauth->get_user_id();
                $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
                $owner_data = $this->user->get_users(FALSE,$owner_id,FALSE,FALSE);
                $sales_rep_data = $this->user->get_users(FALSE,$user_id,FALSE,FALSE);
                $communication_data = array();
                $communication_data['customer'] = $customer_payload;
                $communication_data['type'] = 'LED';
                $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"]. '/assets/uploads/led_booking_form_files/' . $filename . '.pdf';
                $to_sales_rep = $bcc_team_leader = '';
                if(!empty($owner_data)){
                  $communication_data['team_leader']['name'] = $owner_data[0]['full_name'];
                  $communication_data['team_leader']['email'] = $bcc_team_leader = $owner_data[0]['email'];
              }
              if(!empty($sales_rep_data)){
                  $communication_data['sales_rep']['name'] =  $sales_rep_data[0]['full_name'];
                  $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
                  //$this->email_manager->send_booking_form_success_email($communication_data,$to_sales_rep,$bcc_team_leader);
              }
              $communication_data['cost_centre_id'] = $booking_data['cost_centre_id'];
              $communication_data['job_id'] = $job_id;
              $this->add_booking_form_mail_to_queue($communication_data,1);
              $data['success'] = TRUE;
              $data['status'] = 'Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.';
          }

      }catch (Exception $e) {
        $status = $e->getMessage();
        $data['success'] = FALSE;
        $data['status'] = $status;
    } 

    echo json_encode($data);
    die;
}


public function led_booking_form_upload_pdf() {
    $data = array();
    $uuid = $this->input->post('uuid');
    $customer_payload = $site_payload = $site_contact_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_contact_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
    if (empty($booking_data)) {
        $data['status'] = 'Booking Form pdf cant be created, no deal found with reference id.';
        $data['success'] = FALSE;
        echo json_encode($data);
        die;
    }


    /**try {
            //Decode Data
        $business_details = json_decode($booking_data['business_details']);
        $filename = $booking_data['pdf_file'];
        $simpro_job_id = $booking_data['simpro_job_id'];

            //if (!file_exists(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename)) {
        $form_data = array();
        $form_data['business_details'] = json_decode($booking_data['business_details']);
        $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
        $form_data['account_details'] = json_decode($booking_data['account_details']);
        $form_data['space_type'] = json_decode($booking_data['space_type']);
        $form_data['ceiling_height'] = json_decode($booking_data['ceiling_height']);
        $form_data['boom_req'] = json_decode($booking_data['boom_req']);
        $form_data['product'] = json_decode($booking_data['product']);
        $form_data['ae'] = json_decode($booking_data['ae']);
        $form_data['booking_form'] = json_decode($booking_data['booking_form']);
        $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
        $form_data['prev_upgrade'] = json_decode($booking_data['prev_upgrade']);
        $form_data['customer_check_list'] = json_decode($booking_data['customer_check_list']);
        $html = $this->load->view('pdf_files/led_booking_form/main', $form_data, TRUE);

        $filename = md5(time());
        $h = '';
        $f = '';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'save',
            "save_directory" => __DIR__ . '/../../assets/uploads/led_booking_form_files',
            "file_name" => $filename . '.pdf',
            "page_size" => 'A4',
            "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            "header" => $h,
            "footer" => $f,
            "ssl" => true
        );
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            if (0 === error_reporting()) { return false; }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
        $this->pdf->phptopdf($pdf_options);
                //echo $html;die;

                //Update File name and size in DB
        $filesize = $this->getFileSize(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf');
        $this->common->update_data('tbl_led_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));

        $filename = $filename . '.pdf';
            //}

            //Get File
        $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/led_booking_form_files/' . $filename)));
        if (isset($business_details->entity_name) && $business_details->entity_name != '') {
            $filename_new = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his') . '.pdf';
        } else {
            $filename_new = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $lead_data['full_name'] . '-' . date('m-d-Y_his') . '.pdf';
        }

        $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);

        
    } catch (ErrorException $e) {
        $status = 'Look like something went wrong while conversion of booking form. Please try again later or contact support if issue  still persists.';
        $data['success'] = FALSE;
        $data['status'] = $status;
        echo json_encode($data);
        die;
    } catch (Exception $e) {
        $response = $e->getResponse();
        $error = $response->getBody()->getContents();
        $error = json_decode($error, true);
        $status = $e->getMessage();
        if (!empty($error['errors'])) {
            $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
            $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
            $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
        }
        $data['success'] = FALSE;
        $data['status'] = $status;
        echo json_encode($data);
        die;
    }*/
    
        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        $cost_centre_id = $booking_data['cost_centre_id'];

        try {

            //Create Pdf first if it fails then dont procced with booking form
            $form_data = array();
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['account_details'] = json_decode($booking_data['account_details']);
            $form_data['space_type'] = json_decode($booking_data['space_type']);
            $form_data['ceiling_height'] = json_decode($booking_data['ceiling_height']);
            $form_data['boom_req'] = json_decode($booking_data['boom_req']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['ae'] = json_decode($booking_data['ae']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            $form_data['prev_upgrade'] = json_decode($booking_data['prev_upgrade']);
            $form_data['customer_check_list'] = json_decode($booking_data['customer_check_list']);
            
            if($cost_centre_id == 2){
                $html = $this->load->view('pdf_files/led_booking_form/nsw_heers/main', $form_data, TRUE);
            } else if($cost_centre_id == 3){
                $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main', $form_data, TRUE);
            } else{
                $html = $this->load->view('pdf_files/led_booking_form/main', $form_data, TRUE);
            }
        
            
            $business_details = json_decode($booking_data['business_details']);
            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
            } else {
                $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
            }
            
            $h = '';
            $f = '';
            $pdf_options = array(
                "source_type" => 'html',
                "source" => $html,
                "action" => 'save',
                "save_directory" => __DIR__ . '/../../assets/uploads/led_booking_form_files',
                "file_name" => $filename,
                "page_size" => 'A4',
                "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                "header" => $h,
                "footer" => $f
            );
            set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                if (0 === error_reporting()) { return false; }
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            });
            $this->pdf->phptopdf($pdf_options);
            
            
            $filename = str_replace('.pdf','',$filename);
            $pdf_url = str_replace(' ', '%20', $filename);
            $pdf_url = site_url('assets/uploads/led_booking_form_files/' . $pdf_url . '.pdf');
            $b64_file = base64_encode(file_get_contents($pdf_url));

            
            $filename_new = $filename . '.pdf';
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
            //echo FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf';
            //exit;
            if (!file_exists(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
            
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_led_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
            
            $data['filename'] = $pdf_url;
            $data['status'] = 'Pdf Generated';
            $data['success'] = TRUE;
            
        }catch(Exception $e){
            $data['status'] = 'Looks like some issue in generating pdf';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
}


public function led_booking_form_generate_draft_pdf($uuid) {
    $data = array();

    $customer_payload = $site_payload = $site_contact_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_contact_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
    if (empty($booking_data)) {
        $data['status'] = 'Booking Form pdf cant be created, no deal found with reference id.';
        $data['success'] = FALSE;
        echo json_encode($data);
        die;
    }

        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        $cost_centre_id = $booking_data['cost_centre_id'];
        
        try {

            //Create Pdf first if it fails then dont procced with booking form
            $form_data = array();
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['account_details'] = json_decode($booking_data['account_details']);
            $form_data['space_type'] = json_decode($booking_data['space_type']);
            $form_data['ceiling_height'] = json_decode($booking_data['ceiling_height']);
            $form_data['boom_req'] = json_decode($booking_data['boom_req']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['ae'] = json_decode($booking_data['ae']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            $form_data['prev_upgrade'] = json_decode($booking_data['prev_upgrade']);
            $form_data['customer_check_list'] = json_decode($booking_data['customer_check_list']);
            $form_data['form_ap_company_id'] = $booking_data['ap_company_id'];
            $form_data['calculator_items'] = json_decode(file_get_contents(APPPATH . './../assets/calculator/vic.json'),true);
            
            $view = $this->input->get('view');
            if(isset($view) && $view == 'page3'){
                $product = json_decode($booking_data['product']);
                
                if(isset($product->item_name) && !empty($product->item_name)){
                    $itemname = $product->item_name;
                    $product->item_name = array_filter($itemname);
                    $product_count = count($product->item_name);
    
                    if($product_count > 2){
                        $form_data['product_chunks'] = [];
                        $html = '';
                        $productData = array();
                        for($i = 0; $i < $product_count; $i++){
                            $total_cost_excGST = '';
                            if ($product->item_qty[$i] != '' && $product->item_cost_excGST[$i] != '') {
                                $total_cost_excGST = (int) $product->item_qty[$i] * (float) $product->item_cost_excGST[$i];
                            }
                            
                            if($product->item_name[$i] != ''){
                                $typeData = $this->common->fetch_row('tbl_product_types','*',['type_id'=>$product->type_id[$i]]);
                                $anual_op_hrs = 0;
                                for($idx=0;$idx<count($form_data['calculator_items']['bca_types']);$idx++){
                                    if($form_data['calculator_items']['bca_types'][$idx]['type_of_space'] == $product->item_bca_type[$i]){
                                        $anual_op_hrs = $form_data['calculator_items']['bca_types'][$idx]['op_hrs_per_yr'];        
                                    }
                                }
                                if($cost_centre_id == 3){
                                    $form_data['product_chunks'][] = array(
                                        'item_room_area' => $product->item_room_area[$i],
                                        'item_ceiling_height' => $product->item_ceiling_height[$i],
                                        'item_space_type' => $product->item_space_type[$i],
                                        'item_bca_type' => $product->item_bca_type[$i],
                                        'item_air_con' => $product->item_air_con[$i],
                                        'item_existing_qty' => $product->item_existing_qty[$i],
                                        'item_lamp_and_ballast_type' => $product->item_lamp_and_ballast_type[$i],
                                        'item_existing_nominal_lamp_power' => $product->item_existing_nominal_lamp_power[$i],
                                        'item_control_system' => $product->item_control_system[$i],
                                        'type_name' => $typeData['type_name'],
                                        'item_name'  => $product->item_name[$i],
                                        'control_device_upgrade_lighting'  => $product->control_device_upgrade_lighting[$i],
                                        'item_sensor' => $product->item_sensor[$i],
                                        'item_qty' => $product->item_qty[$i],
                                        'item_cost_excGST' => $product->item_cost_excGST[$i],
                                        'total_cost_excGST' => $total_cost_excGST,
                                    );
                                }else{
                                    $form_data['product_chunks'][] = array(
                                        'item_name'  => $product->item_name[$i],
                                        'item_room_area' => $product->item_room_area[$i],
                                        'item_ceiling_height' => $product->item_ceiling_height[$i],
                                        'item_bca_type' => $product->item_bca_type[$i],
                                        'item_lamp_type' => $product->item_lamp_type[$i],
                                        'item_nom_watts' => $product->item_nom_watts[$i],
                                        'item_control_system' => $product->item_control_system[$i],
                                        'item_air_con' => $product->item_air_con[$i],
                                        'item_existing_no_of_lamps' => $product->item_existing_no_of_lamps[$i],
                                        'item_activity_type' => $product->item_activity_type[$i],
                                        'item_name' => $product->item_name[$i],
                                        'item_sensor' => $product->item_sensor[$i],
                                        'item_qty' => $product->item_qty[$i],
                                        'item_cost_excGST' => $product->item_cost_excGST[$i],
                                        'anual_op_hrs' => $anual_op_hrs,
                                        'type_name' => $typeData['type_name'],
                                        'total_cost_excGST' => $total_cost_excGST,
                                    ); 
                                }
                                
                                if($i == ($product_count-1)){
                                    $form_data['product']->is_end = true;
                                }else{
                                    $form_data['product']->is_end = 0;
                                }
                                if($i == ($product_count-1)){
                                    if($cost_centre_id == 3){
                                        $html .= $this->load->view('pdf_files/led_booking_form/nsw_commercial/page3_new_dynamic_nsw', $form_data, TRUE);     
                                    }else{
                                        $html .= $this->load->view('pdf_files/led_booking_form/page3_new_dynamic', $form_data, TRUE); 
                                    }
                                    
                                }else if((($i+1) % 2) == 0){
                                    if($cost_centre_id == 3){
                                        $html .= $this->load->view('pdf_files/led_booking_form/nsw_commercial/page3_new_dynamic_nsw', $form_data, TRUE);     
                                    }else{
                                        $html .= $this->load->view('pdf_files/led_booking_form/page3_new_dynamic', $form_data, TRUE); 
                                    }
                                    $form_data['product_chunks'] = [];
                                }
                            }
                        }
     
                        echo $html;die;
                    }else{
                        if($cost_centre_id == 3){
                            $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page3_new_nsw', $form_data, TRUE);
                        }else{
                            $html = $this->load->view('pdf_files/led_booking_form/page3_new', $form_data, TRUE);
                        }
                        
                        echo $html;die;
                    }
                }else{
                    if($cost_centre_id == 3){
                        $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page3_new_nsw', $form_data, TRUE);
                    }else{
                        $html = $this->load->view('pdf_files/led_booking_form/page3_new', $form_data, TRUE);
                    }
                    echo $html;die;
                }
                
            }else if(isset($view) && $view == 'page1_2'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main1', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/main1', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_4_10'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main2', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/main2', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_11'){
                
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page11', $form_data, TRUE);
                    echo $html;die;
                }else{ 
                    $html = $this->load->view('pdf_files/led_booking_form/page11', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_11_v2'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page_11_v2', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/page_11_v2', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_12'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page12', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/page12', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_13'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page13', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/page13', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_14'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page14', $form_data, TRUE);
                    echo $html;die;
                }else{ 
                    $html = $this->load->view('pdf_files/led_booking_form/page14', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_14_1'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/page14_1', $form_data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/page14_1', $form_data, TRUE);
                    echo $html;die;
                }
            }else if(isset($view) && $view == 'page_all'){
                if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main3_nsw', $form_data, TRUE);
                }else{
                    $html = $this->load->view('pdf_files/led_booking_form/main3', $form_data, TRUE);
                }
                echo $html;die;
            }
            
            
            
            if($cost_centre_id == 1){
                
                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //echo site_url() . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';die;
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A2  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                // echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf');die;
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                exec($cmd);
                
                //Build page 11 in landscape mode
                $filename4 = 'page_11_'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11 '. $upload_path_img4;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                $upload_path_img4_v2 = "";
                if($form_data['ae']->ae_name[0] == 'Boom Lift' || $form_data['ae']->ae_name[1] == 'Boom Lift' || $form_data['ae']->ae_name[0] == 'Site Inspection Required' || $form_data['ae']->ae_name[1] == 'Site Inspection Required'){
                    //Build page 11 in landscape mode
                    $filename4_v2 = 'page_11_v2_'.$uuid;
                    $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                    exec($cmd."2>&1",$output);
                    //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                }
                
                // if($form_data['ae']->ae_name[0] == 'Site Inspection Required' || $form_data['ae']->ae_name[1] == 'Site Inspection Required'){
                //     //Build page 11 in landscape mode
                //     $filename4_v2 = 'page_11_v2_'.$uuid;
                //     $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                //     $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                //     exec($cmd);
                //     //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                // }
                
                //Build page 12 in landscape mode
                $filename5 = 'page_12_'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_12 '. $upload_path_img5;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf');die;
                
                //Build page 13 in landscape mode
                $filename6 = 'page_13_'.$uuid;
                $upload_path_img6 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_13 '. $upload_path_img6;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7 = 'page_14_'.$uuid;
                $upload_path_img7 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14 '. $upload_path_img7;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7_1 = 'page_14_1'.$uuid;
                $upload_path_img7_1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7_1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14_1 '. $upload_path_img7_1;
                exec($cmd);
                
                //Build page 14 in landscape mode
                $filename8 = 'page_all_'.$uuid;
                $upload_path_img8 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_all '. $upload_path_img8;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf');die;
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/'.$filename;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '.$upload_path_img4_v2.' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6 .' '. $upload_path_img7.' '.$upload_path_img7_1 .' '. $upload_path_img8;
                exec($cmd);
                //echo site_url() . 'assets/uploads/led_booking_form_files/'.$filename;die;
            }elseif($cost_centre_id == 3){

                //Build page 1 and 2 in 1 pdf
                $filename1 = 'page1_2_'.$uuid;
            
                $upload_path_img1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename1 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page1_2 '. $upload_path_img1;
                exec($cmd);
                
                //Build page 3 in landscape mode
                $filename2 = 'page3_'.$uuid;
                $upload_path_img2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -O landscape --page-size A2  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page3 '. $upload_path_img2;
                exec($cmd);
                // echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename2 . '.pdf');die;
                
                //Build page 4
                $filename3 = 'page_4_10_'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_4_10 '. $upload_path_img3;
                
                exec($cmd);
                
                //Build page 11 in landscape mode
                $filename4 = 'page_11_'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11 '. $upload_path_img4;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                $upload_path_img4_v2 = "";
                if($form_data['ae']->ae_name[0] == 'Boom Lift' || $form_data['ae']->ae_name[1] == 'Boom Lift'){
                    //Build page 11 in landscape mode
                    $filename4_v2 = 'page_11_v2_'.$uuid;
                    $upload_path_img4_v2 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename4_v2 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_11_v2 '. $upload_path_img4_v2;
                    exec($cmd);
                    //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename4 . '.pdf');die;
                }
                
                //Build page 12 in landscape mode
                $filename5 = 'page_12_'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf';
                
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_12 '. $upload_path_img5;
                //echo "<pre>";print_r($cmd);die;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename5 . '.pdf');die;
                
                //Build page 13 in landscape mode
                $filename6 = 'page_13_'.$uuid;
                $upload_path_img6 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_13 '. $upload_path_img6;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename6 . '.pdf');die;
                
                //Build page 14 in landscape mode
                $filename7 = 'page_14_'.$uuid;
                $upload_path_img7 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14 '. $upload_path_img7;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename7 . '.pdf');die;
                
                $upload_path_img7_1 = "";
                //Build page 14 in landscape mode
                // $filename7_1 = 'page_14_1'.$uuid;
                // $upload_path_img7_1 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename7_1 . '.pdf';
                // $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin//booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_14_1 '. $upload_path_img7_1;
                // exec($cmd);
                
                // //Build page 14 in landscape mode
                $filename8 = 'page_all_'.$uuid;
                $upload_path_img8 = FCPATH . 'assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/led_booking_form_generate_draft_pdf/'.$uuid.'?view=page_all '. $upload_path_img8;
                exec($cmd);
                //echo site_url('assets/uploads/led_booking_form_files/pages/'.$filename8 . '.pdf');die;
                
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
                $final_upload_path = FCPATH . 'assets/uploads/led_booking_form_files/'.$filename;
                $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2.' '.$upload_path_img3.' '.$upload_path_img4.' '.$upload_path_img5.' '.$upload_path_img6.' '.$upload_path_img7.' '.$upload_path_img7_1.' '.$upload_path_img8;
                exec($cmd);
            }else{
                // die($cost_centre_id);
                if($cost_centre_id == 2){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_heers/main', $form_data, TRUE);
                } else if($cost_centre_id == 3){
                    $html = $this->load->view('pdf_files/led_booking_form/nsw_commercial/main', $form_data, TRUE);
                }
        
                $business_details = json_decode($booking_data['business_details']);
                if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                    $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICAL_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                } else {
                    $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                    $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                    $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                    $filename = 'KUGA_ELECTRICA_LED_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
                }
            
                $h = '';
                $f = '';
                // echo $html;die;
                $pdf_options = array(
                    "source_type" => 'html',
                    "source" => $html,
                    "action" => 'save',
                    "save_directory" => __DIR__ . '/../../assets/uploads/led_booking_form_files',
                    "file_name" => $filename,
                    "page_size" => 'A4',
                    "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                    "header" => $h,
                    "footer" => $f
                );
                set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                    if (0 === error_reporting()) { return false; }
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                });
                $this->pdf->phptopdf($pdf_options);
            
            }
            
            $filename = str_replace('.pdf','',$filename);
            $pdf_url = str_replace(' ', '%20', $filename);
            $pdf_url = site_url('assets/uploads/led_booking_form_files/' . $pdf_url . '.pdf');
            $b64_file = base64_encode(file_get_contents($pdf_url));

            
            $filename_new = $filename . '.pdf';
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
            //echo FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf';
            //exit;
            if (!file_exists(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
        
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/led_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_led_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
            
            redirect($pdf_url);
            exit();
        }catch(Exception $e){
            $data['status'] = 'Looks like some issue in generating pdf';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
}

/** SOLAR Booking FORM */
public function manage_solar_booking_form($uuid = FALSE) {
    $data = array();
    $deal_ref = $this->input->get('deal_ref');
    $site_ref = $this->input->get('site_ref');
    $cost_centre_id = $this->input->get('cost_centre_id');

    $data['deal_ref'] = $deal_ref;
    $data['site_ref'] = $site_ref;
    $data['cost_centre_id'] = $cost_centre_id;
    if (isset($deal_ref) && $deal_ref != '' && isset($site_ref) && $site_ref != '') {
        $lead_data = $this->lead->get_leads($deal_ref);
        if (empty($lead_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $this->load->view('booking_form/solar/main');
    } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
        redirect(site_url('admin/lead/add'));
    }
}

/** Residential Solar Booking FORM */
public function manage_residential_solar_booking_form($uuid = FALSE) {
    $data = array();
    $deal_ref = $this->input->get('deal_ref');
    $site_ref = $this->input->get('site_ref');
    $cost_centre_id = $this->input->get('cost_centre_id');

    $data['deal_ref'] = $deal_ref;
    $data['site_ref'] = $site_ref;
    $data['cost_centre_id'] = $cost_centre_id;
    if (isset($deal_ref) && $deal_ref != '' && isset($site_ref) && $site_ref != '') {
        $lead_data = $this->lead->get_leads($deal_ref);
        if (empty($lead_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        $this->load->view('booking_form/solar/residentialMain');
    } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
        redirect(site_url('admin/lead/add'));
    }
}

public function add_solar_booking_form($uuid = FALSE) {
    $data = array();
    $deal_ref = $this->input->get('deal_ref');
    $type_ref = $this->input->get('type_ref');
    $site_ref = $this->input->get('site_ref');
    $cost_centre_id = $this->input->get('cost_centre_id');

    if ($this->aauth->is_member('Admin')) {
        $data['users'] =  $this->user->get_users(FALSE, FALSE, FALSE);
    } else if ($this->components->is_team_leader()) {
        $owner_id = $this->aauth->get_user_id();
        $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
        $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
    } else if ($this->components->is_sales_rep()) {
        $user_id = $this->aauth->get_user_id();
        $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
        $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
        $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
    }
    if (isset($deal_ref) && $deal_ref != '' && isset($type_ref) && $type_ref < 5 && isset($site_ref) && $site_ref != '') {
        $lead_data = $this->lead->get_leads($deal_ref, FALSE, $site_ref);
        if (empty($lead_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('lead_id' => $lead_data['id'], 'site_id' => $site_ref, 'type_id' => $type_ref));
        if (!empty($booking_data)) {
            //redirect(site_url('admin/booking_form/edit_solar_booking_form/' . $booking_data['uuid']));
        }

        $data['lead_uuid'] = $lead_data['uuid'];
        $data['site_id'] = $site_ref;
        $data['type_id'] = $type_ref;
        $data['cost_centre_id'] = $cost_centre_id;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        if($type_ref == 4){			
			$this->load->view('booking_form/solar/3/residential_solar/main');
		}else{
			$this->load->view('booking_form/solar/3/main');
		}
    } else if ($uuid) {
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $lead_uuid = $this->common->fetch_cell('tbl_leads','uuid',array('id' => $booking_data['lead_id']));
        $data['lead_uuid'] = $lead_uuid;
        $data['site_id'] = $booking_data['site_id'];
        $data['booking_form_uuid'] = $uuid;
        $data['type_id'] = $booking_data['type_id'];
        $data['cost_centre_id'] = $booking_data['cost_centre_id'];
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        if($data['type_id'] == 4){			
			$this->load->view('booking_form/solar/3/residential_solar/main');
		}else{
			$this->load->view('booking_form/solar/3/main');
		}
    } else {
        redirect('admin/dashboard');
    }
}


public function add_solar_booking_form_v1($uuid = FALSE) {
    $data = array();
    $deal_ref = $this->input->get('deal_ref');
    $type_ref = $this->input->get('type_ref');
    $site_ref = $this->input->get('site_ref');
    $cost_centre_id = $this->input->get('cost_centre_id');

    if ($this->aauth->is_member('Admin')) {
        $data['users'] =  $this->user->get_users(FALSE, FALSE, FALSE);
    } else if ($this->components->is_team_leader()) {
        $owner_id = $this->aauth->get_user_id();
        $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
        $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
    } else if ($this->components->is_sales_rep()) {
        $user_id = $this->aauth->get_user_id();
        $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
        $cond = 'WHERE user.id NOT IN (1,3,4,23,24) AND user_group.group_id IN(6,7) AND user.banned=0';
            //$data['users'] = $this->user->get_users(FALSE, FALSE, $owner_id);
        $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$cond);
    }
    if (isset($deal_ref) && $deal_ref != '' && isset($type_ref) && $type_ref < 5 && isset($site_ref) && $site_ref != '') {
        $lead_data = $this->lead->get_leads($deal_ref, FALSE, $site_ref);
        if (empty($lead_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('lead_id' => $lead_data['id'], 'site_id' => $site_ref, 'type_id' => $type_ref));
        if (!empty($booking_data)) {
            //redirect(site_url('admin/booking_form/edit_solar_booking_form/' . $booking_data['uuid']));
        }

        $data['lead_uuid'] = $lead_data['uuid'];
        $data['site_id'] = $site_ref;
        $data['type_id'] = $type_ref;
        $data['cost_centre_id'] = $cost_centre_id;

        $customer_data = $this->customer->get_customer_by_site_id($site_ref);
        $state = $customer_data['state_postal'];

        $cond1 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=1";
        $cond2 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=2";
        $cond3 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=3";
        $inverter = $this->product->get_item_data_by_cond($cond1);
        $panel = $this->product->get_item_data_by_cond($cond2);
        $battery = $this->product->get_item_data_by_cond($cond3);
        $data['inverter'] = $inverter;
        $data['panel'] = $panel;
        $data['battery'] = $battery;

        $this->load->view('partials/header', $data);
        $this->load->view('booking_form/solar/3/residential_solar/stc_rebate_modal');
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        if($type_ref == 4){			
			$this->load->view('booking_form/solar/3/residential_solar/main');
		}else{
			$this->load->view('booking_form/solar/3/main');
		}
    } else if ($uuid) {
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking form creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $lead_uuid = $this->common->fetch_cell('tbl_leads','uuid',array('id' => $booking_data['lead_id']));
        $data['lead_uuid'] = $lead_uuid;
        $data['site_id'] = $booking_data['site_id'];
        $data['booking_form_uuid'] = $uuid;
        $data['type_id'] = $booking_data['type_id'];
        $data['cost_centre_id'] = $booking_data['cost_centre_id'];
        
        $lead_data = $this->lead->get_leads($lead_uuid, FALSE, $booking_data['site_id']);
        
        // if($_SERVER['REMOTE_ADDR'] == '111.93.41.194'){
        //     pr($lead_data,1);
        // }
        $data['postcode'] = $lead_data['postcode'];
        $customer_data = $this->customer->get_customer_by_site_id($site_ref);
        $state = $customer_data['state_postal'];

        $cond1 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=1";
        $cond2 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=2";
        $cond3 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=3";
        $inverter = $this->product->get_item_data_by_cond($cond1);
        $panel = $this->product->get_item_data_by_cond($cond2);
        $battery = $this->product->get_item_data_by_cond($cond3);
        $data['inverter'] = $inverter;
        $data['panel'] = $panel;
        $data['battery'] = $battery;

        
        $this->load->view('partials/header', $data);
        $this->load->view('booking_form/solar/3/residential_solar/stc_rebate_modal');
        $this->load->view('partials/sidebar');
        $this->load->view('partials/footer');
        if($data['type_id'] == 4){			
			$this->load->view('booking_form/solar/3/residential_solar/main');
		}else{
			$this->load->view('booking_form/solar/3/main');
		}
    } else {
        redirect('admin/dashboard');
    }
}

public function save_solar_booking_form($uuid = FALSE) {
    $data = array();
    if (!empty($this->input->post())) {
        $is_booking_exists = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $this->input->post('uuid')));
        $form_data = array();
        /*$form_data['business_details'] = json_encode($this->input->post('business_details'));
        $form_data['authorised_details'] = json_encode($this->input->post('authorised_details'));*/
		
		$business_details = $this->input->post('business_details');
        $authorised_details = $this->input->post('authorised_details');
        if(!isset($business_details['entity_name'])){
            $business_details['entity_name'] = $authorised_details['first_name'] . ' ' . $authorised_details['last_name'];
        }
        if(!isset($business_details['abn'])){
            $business_details['abn'] = '';
        }
        if(!isset($business_details['trading_name'])){
            $business_details['trading_name'] = '';
        }
        if(!isset($business_details['leased_or_owned'])){
            $business_details['leased_or_owned'] = 'Owner';
        }
        if(!isset($business_details['address'])){
            $business_details['address'] = $authorised_details['address'];
        }
        /*if(!isset($business_details['suburb'])){
            $business_details['suburb'] = $authorised_details['suburb'];
        }
        if(!isset($business_details['postcode'])){
            $business_details['postcode'] = $authorised_details['postcode'];
        }*/
        
        $form_data['business_details'] = json_encode($business_details);
        $form_data['authorised_details'] = json_encode($authorised_details);
        $form_data['electricity_bill'] = json_encode($this->input->post('electricity_bill'));
        $form_data['product'] = json_encode($this->input->post('product'));
        $form_data['booking_form_image'] = json_encode($this->input->post('booking_form_image'));

            //Convert Base64 signatures to png
        $booking_form = $this->input->post('booking_form');
        if (isset($booking_form['authorised_on_behalf']['signature'])) {
            $img = explode(';', $booking_form['authorised_on_behalf']['signature']);
            if ($booking_form['authorised_on_behalf']['signature'] != '' && count($img) > 1) {
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_on_behalf']['signature']));
                $newfilename = $this->components->uuid() . '.png';
                file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename, $image);
                $booking_form['authorised_on_behalf']['signature'] = $newfilename;
            }
        }

        if (isset($booking_form['authorised_by_behalf']['sales_rep_signature'])) {
            $img = explode(';', $booking_form['authorised_by_behalf']['sales_rep_signature']);
            if ($booking_form['authorised_by_behalf']['sales_rep_signature'] != '' && count($img) > 1) {
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['authorised_by_behalf']['sales_rep_signature']));
                $newfilename1 = $this->components->uuid() . '.png';
                file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename1, $image);
                $booking_form['authorised_by_behalf']['sales_rep_signature'] = $newfilename1;
            }
        }

        if (isset($booking_form['signature'])) {
            $img = explode(';', $booking_form['signature']);
            if ($booking_form['signature'] != '' && count($img) > 1) {
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $booking_form['signature']));
                $newfilename2 = $this->components->uuid() . '.png';
                file_put_contents('./assets/uploads/solar_booking_form_files/' . $newfilename2, $image);
                $booking_form['signature'] = $newfilename2;
            }
        }

        $form_data['booking_form'] = json_encode($booking_form);
        $form_data['cost_centre_id'] = $this->input->post('cost_centre_id');

        if (empty($is_booking_exists)) {
            $insert_data = array();
            $insert_data = $form_data;
            $insert_data['uuid'] = $this->input->post('uuid');
            $insert_data['lead_id'] = $this->input->post('lead_id');
            $insert_data['site_id'] = $this->input->post('site_id');
            $insert_data['type_id'] = $this->input->post('type_id');
            $insert_data['user_id'] = $this->aauth->get_user_id();
            $stat = $this->common->insert_data('tbl_solar_booking_form', $insert_data);
            $quote_id = $this->common->insert_id();
        } else {
            $booking_id = $is_booking_exists['id'];
            
            $form_data['updated_at'] = date('Y-m-d H:i:s');
            //print_r($form_data);die;
            $stat = $this->common->update_data('tbl_solar_booking_form', array('id' => $booking_id), $form_data);
        }
        if ($stat) {
            $data['status'] = 'Booking Form Data Saved Successfully.';
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Nothing to update.';
            $data['success'] = FALSE;
        }
    } else {
        $data['status'] = 'Invalid Request';
        $data['success'] = FALSE;
    }
    echo json_encode($data);
    die;
}

public function fetch_solar_booking_form_data() {
    $data = array();
    if (!empty($this->input->get())) {
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $this->input->get('uuid')));
        
        $lead_uuid = $this->common->fetch_cell('tbl_leads','uuid',array('id' => $booking_data['lead_id']));
        $lead_data = $this->lead->get_leads($lead_uuid, FALSE, $booking_data['site_id']);
        
        $booking_data['postcode'] = $lead_data['postcode'];
        $data['booking_data'] = $booking_data;
        // $data['postcode'] = $lead_data['postcode'];
        $data['success'] = TRUE;
    } else {
        $data['status'] = 'Invalid Request';
        $data['success'] = FALSE;
    }
    echo json_encode($data);
    die;
}

public function solar_booking_form_pdf_download($uuid) {
    $data = array();
    $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
    if (empty($booking_data)) {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
        redirect(site_url('admin/lead/add'));
    }
    $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
    $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);

    try {

        $form_data = array();
        $form_data['type_id'] = $booking_data['type_id'];
        $form_data['business_details'] = json_decode($booking_data['business_details']);
        $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
        $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
        $form_data['product'] = json_decode($booking_data['product']);
        $form_data['booking_form'] = json_decode($booking_data['booking_form']);
        $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
        $form_data['cost_centre_id'] =$booking_data['cost_centre_id'];
        
        if($form_data['type_id'] == 4){			
			$html = $this->load->view('pdf_files/solar_booking_form/residential_solar/main', $form_data, TRUE); //residential booking form
		}else{
			$html = $this->load->view('pdf_files/solar_booking_form/main', $form_data, TRUE);
		}
	
        $filename = md5(time());
        $h = '';
        $f = '';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'save',
            "save_directory" => __DIR__ . '/../../assets/uploads/solar_booking_form_files',
            "file_name" => $filename . '.pdf',
            "page_size" => 'A4',
            "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            "header" => $h,
            "footer" => $f
        );
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            if (0 === error_reporting()) { return false; }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
        $this->pdf->phptopdf($pdf_options);
        

        //Decode Data
        $business_details = json_decode($booking_data['business_details']);
        $authorised_details = json_decode($booking_data['authorised_details']);
        $electricity_bill = json_decode($booking_data['electricity_bill']);
        $product = json_decode($booking_data['product']);
        $booking_form = json_decode($booking_data['booking_form']);
        $booking_form_image = json_decode($booking_data['booking_form_image']);


        //Get File
        $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
        $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
        if (isset($business_details->entity_name) && $business_details->entity_name != '') {
            $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
        } else {
            $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
        }

        $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);


        if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
            $data['success'] = FALSE;
            $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
            echo json_encode($data);
            die;
        }

            //Update File name and size in DB
        $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
        $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));


            //Save to Job CRM
        $job_crm_data = [];
        $job_crm_data['kuga_job_id'] = $booking_data['kuga_job_id'];
        $job_crm_data['booking_form_id'] = $booking_data['id'];
        $job_crm_data['booking_form_site_id'] = isset($booking_data['site_id']) ? $booking_data['site_id'] : NULL;
        $job_crm_data['business_details'] = $business_details;
        $job_crm_data['authorised_details'] = $authorised_details;
        $job_crm_data['booking_form'] = $booking_form;
        $job_crm_data['booking_form_image'] = $booking_form_image;
        $job_crm_data['filename'] = $filename;
        $job_crm_data['user_id'] = $lead_data['user_id'];
        $job_crm_data['cost_centre_id'] = $booking_data['cost_centre_id'];
        $job_crm_data['electricity_bill'] = $electricity_bill;
        $job_id = $this->create_job_in_job_crm('Solar',$lead_data['cust_id'],$job_crm_data);
        if($job_id != null){
            $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('kuga_job_id' => $job_id));
            
            $custom_fields = array();
            $kW = '';
            if(isset($product->product_capacity)){
                $product->product_capacity = json_decode(json_encode($product->product_capacity), true);
            }
            if(!empty($product) && (isset($product->product_capacity[1]) && $product->product_capacity[1] != '')){
                $pcapacity = (float) $product->product_capacity[1];
                $pqty = (int) $product->product_qty[1];
                $kW = ($pcapacity * $pqty)/1000;
            }else {
                if(!empty($product) && (isset($product->product_name[0]) && $product->product_name[0] != '')){
                    $kW = preg_replace("/[^0-9.]/","",$product->product_name[0]);
                }
            }
            if($kW != '') {
                $custom_fields[2] = $kW;
            }
            
            //Battery kWh
            $kWh = '';
            if(!empty($product) && (isset($product->product_capacity[3]) && $product->product_capacity[3] != '')){
                $pcapacity = (float) $product->product_capacity[3];
                $pqty = (int) $product->product_qty[3];
                $kWh = ($pcapacity * $pqty);
            }
            if($kWh != '') {
                $custom_fields[3] = $kWh;
            }
            $this->create_job_custom_fields_in_job_crm($job_id,$custom_fields);

            //Send Success Mail
            $customer_payload['CompanyName'] = $business_details->entity_name;
            $customer_payload['Phone'] = $authorised_details->contact_no;
            $customer_payload['Email'] = $authorised_details->email;
                
            $user_id = $this->aauth->get_user_id();
            $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
            $owner_data = $this->user->get_users(FALSE,$owner_id,FALSE,FALSE);
            $sales_rep_data = $this->user->get_users(FALSE,$user_id,FALSE,FALSE);
            $communication_data = array();
            $communication_data['customer'] = $customer_payload;
            $communication_data['type'] = 'Solar';
            $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"]. '/assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
            $to_sales_rep = $bcc_team_leader = '';
            if(!empty($owner_data)){
              $communication_data['team_leader']['name'] = $owner_data[0]['full_name'];
              $communication_data['team_leader']['email'] = $bcc_team_leader = $owner_data[0]['email'];
          }
          if(!empty($sales_rep_data)){
              $communication_data['sales_rep']['name'] =  $sales_rep_data[0]['full_name'];
              $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
              //$this->email_manager->send_booking_form_success_email($communication_data,$to_sales_rep,$bcc_team_leader);
          }
            $communication_data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $communication_data['job_id'] = $job_id;
            $this->add_booking_form_mail_to_queue($communication_data,1);
          $data['success'] = TRUE;
          $data['status'] = 'Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.';
      }

  } catch (ErrorException $e) {
    $status = $e->getMessage();
    $data['success'] = FALSE;
    $data['status'] = $status;
} catch (Exception $e) {
    $response = $e->getResponse();
    $error = $response->getBody()->getContents();
    $error = json_decode($error, true);
    $status = $e->getMessage();
    if (!empty($error['errors'])) {
        $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
        $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
        $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
    }
    $data['success'] = FALSE;
    $data['status'] = $status;
    echo json_encode($data);
    die;
}

echo json_encode($data);
die;
}

    public function solar_booking_form_pdf_download_v1($uuid) {
        $data = array();
        $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
        $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();
    
        $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        
        $view = $this->input->get('view');
        try {
    
            $form_data = array();
            $form_data['type_id'] = $booking_data['type_id'];
            $form_data['business_details'] = json_decode($booking_data['business_details']);
            $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
            $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
            $form_data['product'] = json_decode($booking_data['product']);
            $form_data['booking_form'] = json_decode($booking_data['booking_form']);
            $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
            $form_data['cost_centre_id'] =$booking_data['cost_centre_id'];
            
    		if(isset($view) && $view == 'final_html'){
    		    if($form_data['type_id'] == 4){			
    			    $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/main', $form_data, TRUE); //residential booking form
        		}else{
        			$html = $this->load->view('pdf_files/solar_booking_form_v2/main', $form_data, TRUE);
        		}
        		echo $html;die;
            }else if(isset($view) && $view == 'page9'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page9', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v2/page9', $form_data, TRUE);
            	}
        	    echo $html;die;
            }else if(isset($view) && $view == 'page10'){
                if($form_data['type_id'] == 4){			
        		     $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page10', $form_data, TRUE); //residential booking form
        		}else{
            	    $html = $this->load->view('pdf_files/solar_booking_form_v2/page10', $form_data, TRUE);
            	}
            	echo $html;die;
            }else if(isset($view) && $view == 'final_pdf'){
                $filename = md5(time());
                $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                exec($cmd);
                //echo site_url() . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
                //die;
            }else {
                if($form_data['type_id'] == 4){
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                    //echo site_url() . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
                    //die;
                }else{
                    //Page 9
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page9.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page9 '. $upload_path_img;
                    exec($cmd);
                    
                    $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page10.png';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page10 '. $upload_path_img;
                    exec($cmd);
                    
                    $filename = md5(time());
                    $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                    exec($cmd);
                }
            }
            
            
            //Decode Data
            $business_details = json_decode($booking_data['business_details']);
            $authorised_details = json_decode($booking_data['authorised_details']);
            $electricity_bill = json_decode($booking_data['electricity_bill']);
            $product = json_decode($booking_data['product']);
            $booking_form = json_decode($booking_data['booking_form']);
            $booking_form_image = json_decode($booking_data['booking_form_image']);
    
    
            //Get File
            $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
            $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
            } else {
                $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
            }
    
            $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);
    
    
            if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
            
            
            //Update File name and size in DB
            $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
            $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
    
    
                //Save to Job CRM
            $job_crm_data = [];
            $job_crm_data['kuga_job_id'] = $booking_data['kuga_job_id'];
            $job_crm_data['booking_form_id'] = $booking_data['id'];
            $job_crm_data['booking_form_site_id'] = isset($booking_data['site_id']) ? $booking_data['site_id'] : NULL;
            $job_crm_data['business_details'] = $business_details;
            $job_crm_data['authorised_details'] = $authorised_details;
            $job_crm_data['booking_form'] = $booking_form;
            $job_crm_data['booking_form_image'] = $booking_form_image;
            $job_crm_data['filename'] = $filename;
            $job_crm_data['user_id'] = $lead_data['user_id'];
            $job_crm_data['cost_centre_id'] = $booking_data['cost_centre_id'];
            $job_crm_data['electricity_bill'] = $electricity_bill;
            $job_crm_data['type_id'] = $form_data['type_id'];
            $job_crm_data['product'] = $product;
            
            
            $job_id = $this->create_job_in_job_crm('Solar',$lead_data['cust_id'],$job_crm_data);
            if($job_id != null){
                $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('kuga_job_id' => $job_id));
                $custom_fields = array();
                $kW = '';
                if(isset($product->product_capacity)){
                    $product->product_capacity = json_decode(json_encode($product->product_capacity), true);
                }
                if(!empty($product) && (isset($product->product_capacity) && $product->product_capacity[1] != '')){
                    $pcapacity = (float) $product->product_capacity[1];
                    $pqty = (int) $product->product_qty[1];
                    $kW = ($pcapacity * $pqty)/1000;
                }else {
                    if(!empty($product) && (isset($product->product_name[0]) && $product->product_name[0] != '')){
                        $kW = preg_replace("/[^0-9.]/","",$product->product_name[0]);
                    }
                }
                if($kW != '') {
                    $custom_fields[2] = $kW;
                }
                
                //Battery kWh
                $kWh = '';
                if(!empty($product) && (isset($product->product_capacity[3]) && $product->product_capacity[3] != '')){
                    $pcapacity = (float) $product->product_capacity[3];
                    $pqty = (int) $product->product_qty[3];
                    $kWh = ($pcapacity * $pqty);
                }
                if($kWh != '') {
                    $custom_fields[3] = $kWh;
                }
                $this->create_job_custom_fields_in_job_crm($job_id,$custom_fields);
    
                //Send Success Mail
                $customer_payload['CompanyName'] = $business_details->entity_name;
                $customer_payload['Phone'] = $authorised_details->contact_no;
                $customer_payload['Email'] = $authorised_details->email;
                    
                $user_id = $this->aauth->get_user_id();
                $owner_id = $this->common->fetch_cell('aauth_user_to_user', 'owner_id', array('user_id' => $user_id));
                $owner_data = $this->user->get_users(FALSE,$owner_id,FALSE,FALSE);
                $sales_rep_data = $this->user->get_users(FALSE,$user_id,FALSE,FALSE);
                $communication_data = array();
                $communication_data['customer'] = $customer_payload;
                $communication_data['type'] = 'Solar';
                $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"]. '/assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
                $to_sales_rep = $bcc_team_leader = '';
                if(!empty($owner_data)){
                  $communication_data['team_leader']['name'] = $owner_data[0]['full_name'];
                  $communication_data['team_leader']['email'] = $bcc_team_leader = $owner_data[0]['email'];
                }
                if(!empty($sales_rep_data)){
                    $communication_data['sales_rep']['name'] =  $sales_rep_data[0]['full_name'];
                    $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
                    //$this->email_manager->send_booking_form_success_email($communication_data,$to_sales_rep,$bcc_team_leader);
                }
                $communication_data['cost_centre_id'] = $booking_data['cost_centre_id'];
                $communication_data['job_id'] = $job_id;
                $this->add_booking_form_mail_to_queue($communication_data,1);
                $data['success'] = TRUE;
                $data['status'] = 'Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.';
            }

        } catch (ErrorException $e) {
            $status = $e->getMessage();
            $data['success'] = FALSE;
            $data['status'] = $status;
        } catch (Exception $e) {
            $response = $e->getResponse();
            $error = $response->getBody()->getContents();
            $error = json_decode($error, true);
            $status = $e->getMessage();
            if (!empty($error['errors'])) {
                $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
                $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
                $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
            }
            $data['success'] = FALSE;
            $data['status'] = $status;
            echo json_encode($data);
            die;
        }

        echo json_encode($data);
        die;
    }

public function solar_booking_form_generate_draft_pdf($uuid) {
    $data = array();
    $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
    if (empty($booking_data)) {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
        redirect(site_url('admin/lead/add'));
    }
    
    $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
    $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);

    try {

        $form_data = array();
        $form_data['type_id'] = $booking_data['type_id'];
        $form_data['business_details'] = json_decode($booking_data['business_details']);
        $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
        $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
        $form_data['product'] = json_decode($booking_data['product']);
        $form_data['booking_form'] = json_decode($booking_data['booking_form']);
        $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
        $form_data['cost_centre_id'] =$booking_data['cost_centre_id'];
        
        if($form_data['type_id'] == 4){			
			$html = $this->load->view('pdf_files/solar_booking_form/residential_solar/main', $form_data, TRUE); //residential booking form
		}else{
			$html = $this->load->view('pdf_files/solar_booking_form/main', $form_data, TRUE);
		}
	    //echo $html;die;
        $filename = md5(time());
        $h = '';
        $f = '';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'save',
            "save_directory" => __DIR__ . '/../../assets/uploads/solar_booking_form_files',
            "file_name" => $filename . '.pdf',
            "page_size" => 'A4',
            "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            "header" => $h,
            "footer" => $f
        );
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            if (0 === error_reporting()) { return false; }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
        $this->pdf->phptopdf($pdf_options);
        

        //Decode Data
        $business_details = json_decode($booking_data['business_details']);
        $authorised_details = json_decode($booking_data['authorised_details']);
        $electricity_bill = json_decode($booking_data['electricity_bill']);
        $product = json_decode($booking_data['product']);
        $booking_form = json_decode($booking_data['booking_form']);
        $booking_form_image = json_decode($booking_data['booking_form_image']);


        //Get File
        $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
        $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
        if (isset($business_details->entity_name) && $business_details->entity_name != '') {
            $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
        } else {
            $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
        }

        $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);


        if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
            $data['success'] = FALSE;
            $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
            echo json_encode($data);
            die;
        }

        //Update File name and size in DB
        $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
        $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
        redirect(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf'));
        exit();
        
        
    }catch(Exception $e){
            $data['status'] = 'Looks like some issue in generating pdf. Error:'.json_encode($e->getMessage());
            $data['success'] = FALSE;
    }
    echo json_encode($data);
    die;
}


public function solar_booking_form_generate_draft_pdf_v1($uuid) {
    $data = array();
    $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));
    if (empty($booking_data)) {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
        redirect(site_url('admin/lead/add'));
    }
    
    $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
    $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);

    $view = $this->input->get('view');
    try {

        $form_data = array();
        $form_data['uuid'] = $booking_data['uuid'];
        $form_data['type_id'] = $booking_data['type_id'];
        $form_data['business_details'] = json_decode($booking_data['business_details']);
        $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
        $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
        $form_data['product'] = json_decode($booking_data['product']);
        $form_data['booking_form'] = json_decode($booking_data['booking_form']);
        $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
        $form_data['cost_centre_id'] =$booking_data['cost_centre_id'];
        
        if(isset($view) && $view == 'final_html'){
            if($form_data['type_id'] == 4){			
    		     $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/main', $form_data, TRUE); //residential booking form
    		}else{
        	    $html = $this->load->view('pdf_files/solar_booking_form_v2/main', $form_data, TRUE);
        	}
        	echo $html;die;
        }else if(isset($view) && $view == 'page9'){
            if($form_data['type_id'] == 4){			
    		     $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page9', $form_data, TRUE); //residential booking form
    		}else{
        	    $html = $this->load->view('pdf_files/solar_booking_form_v2/page9', $form_data, TRUE);
        	}
        	echo $html;die;
        }else if(isset($view) && $view == 'page10'){
            if($form_data['type_id'] == 4){			
    		     $html = $this->load->view('pdf_files/solar_booking_form_v2/residential_solar/page10', $form_data, TRUE); //residential booking form
    		}else{
        	    $html = $this->load->view('pdf_files/solar_booking_form_v2/page10', $form_data, TRUE);
        	}
        	echo $html;die;
        }else if(isset($view) && $view == 'final_pdf'){
            $filename = md5(time());
            $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
            exec($cmd);
            //echo site_url() . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
            //die;
        }else {
            if($form_data['type_id'] == 4){
                $filename = md5(time());
                $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                exec($cmd);
                //echo site_url() . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf';
                //die;
            }else{
                //Page 9
                $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page9.png';
                $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page9 '. $upload_path_img;
                exec($cmd);
                
                $upload_path_img = FCPATH .'assets/uploads/solar_booking_form_files/pages/'.$uuid.'_page10.png';
                $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 --quality 80 https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=page10 '. $upload_path_img;
                exec($cmd);
                
                $filename = md5(time());
                $upload_path_img = FCPATH . 'assets/uploads/solar_booking_form_files/'.$filename . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/solar_booking_form_generate_draft_pdf/'.$uuid.'?view=final_html '. $upload_path_img;
                exec($cmd);
            }
        }
        

        //Decode Data
        $business_details = json_decode($booking_data['business_details']);
        $authorised_details = json_decode($booking_data['authorised_details']);
        $electricity_bill = json_decode($booking_data['electricity_bill']);
        $product = json_decode($booking_data['product']);
        $booking_form = json_decode($booking_data['booking_form']);
        $booking_form_image = json_decode($booking_data['booking_form_image']);


        //Get File
        $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf')));
        $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
        if (isset($business_details->entity_name) && $business_details->entity_name != '') {
            $bn = $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $bn . '.pdf';
        } else {
            $bn = $lead_data['full_name'] . '-' . date('m-d-Y_his');
            $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
            $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement' . $bn  . '.pdf';
        }

        $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);


        if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf')) {
            $data['success'] = FALSE;
            $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
            echo json_encode($data);
            die;
        }

        //Update File name and size in DB
        $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
        $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));
        redirect(site_url('assets/uploads/solar_booking_form_files/' . $filename . '.pdf'));
        exit();
        
        
    }catch(Exception $e){
            $data['status'] = 'Looks like some issue in generating pdf. Error:'.json_encode($e->getMessage());
            $data['success'] = FALSE;
    }
    echo json_encode($data);
    die;
}

public function solar_booking_form_upload_pdf() {
    $data = array();
    $uuid = $this->input->post('uuid');
    $customer_payload = $site_payload = $job_payload = $attachment_payload = $section_payload = $cost_center_payload = array();
    $simpro_customer_response = $simpro_site_response = $simpro_job_response = $section_response = $cost_center_response = array();

    $booking_data = $this->common->fetch_row('tbl_solar_booking_form', '*', array('uuid' => $uuid));

    if (empty($booking_data)) {
        $data['status'] = 'Booking Form pdf cant be created, no deal found with reference id.';
        $data['success'] = FALSE;
        echo json_encode($data);
        die;
    }
    $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);

    try {
            //Decode Data
        $business_details = json_decode($booking_data['business_details']);
        $filename = $booking_data['pdf_file'];
        $simpro_job_id = $booking_data['simpro_job_id'];

            //if (!file_exists(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename)) {
        $form_data = array();
        $form_data['type_id'] = $booking_data['type_id'];
        $form_data['business_details'] = json_decode($booking_data['business_details']);
        $form_data['authorised_details'] = json_decode($booking_data['authorised_details']);
        $form_data['electricity_bill'] = json_decode($booking_data['electricity_bill']);
        $form_data['product'] = json_decode($booking_data['product']);
        $form_data['booking_form'] = json_decode($booking_data['booking_form']);
        $form_data['booking_form_image'] = json_decode($booking_data['booking_form_image']);
        $html = $this->load->view('pdf_files/solar_booking_form/main', $form_data, TRUE);
        $filename = md5(time());
        $h = '';
        $f = '';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'save',
            "save_directory" => __DIR__ . '/../../assets/uploads/solar_booking_form_files',
            "file_name" => $filename . '.pdf',
            "page_size" => 'A4',
            "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            "header" => $h,
            "footer" => $f
        );
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            if (0 === error_reporting()) { return false; }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
        $this->pdf->phptopdf($pdf_options);

                //Update File name and size in DB
        $filesize = $this->getFileSize(FCPATH . 'assets/uploads/solar_booking_form_files/' . $filename . '.pdf');
        $this->common->update_data('tbl_solar_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename . '.pdf','pdf_file_size' => $filesize));

        $filename = $filename . '.pdf';
            //}

            //Get File
        $b64_file = base64_encode(file_get_contents(site_url('assets/uploads/solar_booking_form_files/' . $filename)));
        $size_name = ($booking_data['type_id'] == 1) ? '0 to 39kW' : (($booking_data['type_id'] == 2) ?  '39kW to 100kW' : '100kW+');
        if (isset($business_details->entity_name) && $business_details->entity_name != '') {
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.' Solar_Upgrade_Booking_Form_Agreement_' . $business_details->entity_name . '-' . $lead_data['full_name'] . '-' . date('m-d-Y_his') . '.pdf';
        } else {
            $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Solar_Upgrade_Booking_Form_Agreement_' . $lead_data['full_name'] . '-' . date('m-d-Y_his') . '.pdf';
        }

        $filename_new = preg_replace('/[^A-Za-z0-9 ._&\-]/', '',$filename_new);


    } catch (ErrorException $e) {
        $status = 'Look like something went wrong while conversion of booking form. Please try again later or contact support if issue still persists.';
        $data['success'] = FALSE;
        $data['status'] = $status;
    } catch (Exception $e) {
        $response = $e->getResponse();
        $error = $response->getBody()->getContents();
        $error = json_decode($error, true);
        $status = $e->getMessage();
        if (!empty($error['errors'])) {
            $status = (isset($error['errors'][0]['path'])) ? 'simPRO Api Error ' . $error['errors'][0]['path'] . ' ' : '';
            $status .= (isset($error['errors'][0]['message'])) ? $error['errors'][0]['message'] . ' ' : '';
            $status .= (isset($error['errors'][0]['value'])) ? ' it can not be ' . $error['errors'][0]['value'] : '';
        }
        $data['success'] = FALSE;
        $data['status'] = $status;
        echo json_encode($data);
        die;
    }

    echo json_encode($data);
    die;
}

function getFileSize($path) {
    $size = filesize($path);
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

private function create_job_in_job_crm($job_type,$cust_id,$data){
    $booking_form_kuga_job_id = isset($data['kuga_job_id']) ? $data['kuga_job_id'] : NULL;
    $booking_form_uuid = isset($data['booking_form_uuid']) ? $data['booking_form_uuid'] : NULL;
    $business_details = isset($data['business_details']) ? $data['business_details'] : NULL;
    $authorised_details = isset($data['authorised_details']) ? $data['authorised_details'] : NULL;
    $booking_form_data = isset($data['booking_form']) ? $data['booking_form'] : NULL;
    $booking_form_image = isset($data['booking_form_image']) ? $data['booking_form_image'] : NULL;
    $get_nomination_form_details = isset($data['get_nomination_form_details']) ? $data['get_nomination_form_details'] : NULL;
    $site_assessor_form_details = isset($data['site_assessor_form_details']) ? $data['site_assessor_form_details'] : NULL;
    $filename = isset($data['filename']) ? $data['filename'] : '';
    $cost_centre_id =  (isset($data['cost_centre_id']) && ($data['cost_centre_id'] != NULL && $data['cost_centre_id'] != 0)) ? $data['cost_centre_id'] : 1;
    $product_data =  (isset($data['product_data'])) ? $data['product_data'] : NULL;
    $ap_company_id =  (isset($data['ap_company_id']) && ($data['ap_company_id'] != NULL && $data['ap_company_id'] != '')) ? $data['ap_company_id'] : 6;
    $booking_form_site_id = isset($data['booking_form_site_id']) ? $data['booking_form_site_id'] : NULL;
    $electricity_bill = isset($data['electricity_bill']) ? $data['electricity_bill'] : NULL;
    
    $type_id = isset($data['type_id']) ? $data['type_id'] : NULL;
    $crm_cust_id = $this->job->insert_or_update_customer($cust_id,$data['booking_form_id']);    
    
    if($crm_cust_id){
        $cust_loc_id = $this->job->insert_or_update_customer_location($cust_id,$crm_cust_id,$booking_form_site_id);
        
        
        $job_data = array();
        $job_data['uuid'] = $this->components->uuid();
        $job_data['company_id'] = 1;
        $job_data['cust_id'] = $crm_cust_id;
        $job_data['booking_form_id'] = $data['booking_form_id'];
        $job_data['cost_centre_id'] = $cost_centre_id;
        $job_data['ap_company_id'] = $ap_company_id;
        $job_data['assigned_to'] = isset($data['user_id']) ? $data['user_id'] : '';
        $job_data['business_details'] = json_encode($business_details);
        $job_data['authorised_details'] = json_encode($authorised_details);
        $job_data['booking_form_data'] = json_encode($booking_form_data);
        $job_data['booking_form_image'] = json_encode($booking_form_image);
        $job_data['get_nomination_form_details'] = json_encode($get_nomination_form_details);
        $job_data['site_assessor_form_details'] = json_encode($site_assessor_form_details);
        $job_data['job_type'] = $job_type;
        
        
            
        if($filename != ''){
            $folder = ($job_type == 'LED') ?  'led_booking_form_files' : 'solar_booking_form_files';
            $job_data['booking_form_file'] = site_url('assets/uploads/'.$folder.'/' . $filename . '.pdf');
        }
        
        // if($job_type == 'LED'){
        //     $important_notes = isset($data['customer_check_list']) ? $data['customer_check_list']->notes : NULL;
        // }else{
            $important_notes = isset($booking_form_data->notes) ? $booking_form_data->notes : NULL;
        // }
        $job_data['important_notes'] = $important_notes;
        if($booking_form_kuga_job_id != NULL ){
            $job_id = $this->job->insert_or_update_job($data['booking_form_id'],$job_data);

            if($job_id != ''){
                $this->job->insert_booking_form_activity_note($job_id, $job_data);
            }

            if($job_id != '' && !empty($product_data) && $job_type == 'LED'){
                $this->job->insert_or_update_job_products($job_id,$job_type,$product_data);
            }
            
            return $job_id;
        }
        
        // if($booking_form_kuga_job_id != NULL){
        //     $job_id = $this->job->insert_or_update_job($data['booking_form_id'],$job_data);

        //     if($job_id != ''){
        //         $this->job->insert_booking_form_activity_note($job_id, $job_data);
        //     }

        //     if($job_id != '' && !empty($product_data) && $job_type == 'LED'){
        //         $this->job->insert_or_update_job_products($job_id,$job_type,$product_data);
        //     }
            
        //     return $job_id;
        // }
        
        //$nfilename = 'KUGA ELECTRICAL-Nomination Form_Dotsquares-04-07-2020_045654.pdf';
        //$safilename = 'KUGA ELECTRICAL-Site Assessor Form_Dotsquares-04-07-2020_045756.pdf';

        $nfilename = $safilename = NULL;

        if($cost_centre_id == 2){
            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . date('m-d-Y_his') ;
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
            
                $nfilename = 'KUGA_ELECTRICAL_Nomination Form_' . $bn . '.pdf';
                $safilename = 'KUGA_ELECTRICAL_Site Assessor Form_' . $bn . '.pdf';
            } else {
                
                $nfilename = 'KUGA_ELECTRICAL_Nomination Form_' . date('m-d-Y_his') . '.pdf';
                $safilename = 'KUGA_ELECTRICAL_Site Assessor Form_' . date('m-d-Y_his') . '.pdf';
            }
            
            if($get_nomination_form_details != NULL){
                $nomination_form_data = array();
                $get_nomination_form_details = json_encode($get_nomination_form_details);
                $nomination_form_data = json_decode($get_nomination_form_details);
                
                $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
                
                if($cc_folder != '' && $cc_folder != 'led'){
                    $html = $this->load->view('pdf_files/'.$cc_folder.'/nomination_form/page1', $nomination_form_data, TRUE);
                }else{
                    // $html = $this->load->view('pdf_files/nomination_form/page1', $nomination_form_data, TRUE);
                    $html = $this->load->view('pdf_files/nomination_form/page1_v2', $nomination_form_data, TRUE);
                }

                $h = '';
                $f = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source" => $html,
                    "action" => 'save',
                    "save_directory" => __DIR__ . '/../../assets/uploads/nomination_form_files',
                    "file_name" => $nfilename,
                    "page_size" => 'A4',
                    "margin" => array("right" => "5", "left" => '5', "top" => "0", "bottom" => "0"),
                    "header" => $h,
                    "footer" => $f
                );
                set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                    if (0 === error_reporting()) { return false; }
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                });
                $this->pdf->phptopdf($pdf_options);

                $job_data['get_nomination_form_file'] = site_url('assets/uploads/nomination_form_files/' . $nfilename);
                $this->common->update_data('tbl_led_booking_form', array('id' => $data['booking_form_id']), array('get_nomination_form_file' =>  $nfilename));
            }

            if($site_assessor_form_details != NULL){
                $site_assessor_form_data = array();
                $site_assessor_form_details = json_encode($site_assessor_form_details);
                $site_assessor_form_data = json_decode($site_assessor_form_details,true);
                
                $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
                if($cc_folder != '' && $cc_folder != 'led'){
                    $html = $this->load->view('pdf_files/'.$cc_folder.'/site_assessor_form/page1', $site_assessor_form_data, TRUE);
                }else{
                    $html = $this->load->view('pdf_files/site_assessor_form/page1_v2', $site_assessor_form_data, TRUE);
                    // $html = $this->load->view('pdf_files/site_assessor_form/page1', $site_assessor_form_data, TRUE);
                }

                $h = '';
                $f = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source" => $html,
                    "action" => 'save',
                    "save_directory" => __DIR__ . '/../../assets/uploads/site_assessor_form_files',
                    "file_name" => $safilename,
                    "page_size" => 'A4',
                    "margin" => array("right" => "5", "left" => '5', "top" => "0", "bottom" => "0"),
                    "header" => $h,
                    "footer" => $f
                );
                set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                    if (0 === error_reporting()) { return false; }
                    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                });
                $this->pdf->phptopdf($pdf_options);

                $job_data['site_assessor_form_file'] = site_url('assets/uploads/site_assessor_form_files/' . $safilename);
                $this->common->update_data('tbl_led_booking_form', array('id' => $data['booking_form_id']), array('site_assessor_form_file' =>  $safilename));
            }
        } else if($cost_centre_id == 3){

            if (isset($business_details->entity_name) && $business_details->entity_name != '') {
                $bn = $business_details->entity_name . '-' . date('m-d-Y_his') ;
                $bn = str_replace(array('.', ',', ' ','/','&',"'"), '_', $bn);
                $bn = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_',$bn);
                $nfilename = 'KUGA_ELECTRICAL_Nomination_Form_' . $bn . '.pdf';
            } else {
                $nfilename = 'KUGA_ELECTRICAL_Nomination_Form_' . date('m-d-Y_his') . '.pdf';
            }

            if($get_nomination_form_details != NULL){
                $nomination_form_data = array();
                $get_nomination_form_details = json_encode($get_nomination_form_details);
                $nomination_form_data = json_decode($get_nomination_form_details);
                
                // $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
                $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
                if($cc_folder != '' && $cc_folder != 'led'){
                    $upload_path_img = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/page5_'.$data['booking_form_id'].'.jpg';
                    $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nf_page5 '. $upload_path_img;
                    exec($cmd);
                    $html = $this->load->view('pdf_files/'.$cc_folder.'/nomination_form/nsw_commercial/main', ['nomination_form_data' => $nomination_form_data,'lead_id' => $data['booking_form_id']], TRUE);
                
                    $h = '';
                    $f = '';
                    $pdf_options = array(
                        "source_type" => 'html',
                        "source" => $html,
                        "action" => 'save',
                        "save_directory" => __DIR__ . '/../../assets/uploads/nomination_form_files',
                        "file_name" => $nfilename,
                        "page_size" => 'A4',
                        "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                        "header" => $h,
                        "footer" => $f
                    );
                    set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
                        if (0 === error_reporting()) { return false; }
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    });
                    $this->pdf->phptopdf($pdf_options);
                    
                }else{
                    //     $html = $this->load->view('pdf_files/nomination_form/nsw_commercial/page1', $nomination_form_data, TRUE);
                    
                    $filename1 = 'page1_'.$booking_form_uuid;
                    $upload_path_img1 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename1 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page1 '. $upload_path_img1;
                    exec($cmd);
                
                    $filename2 = 'page2_'.$booking_form_uuid;
                    $upload_path_img2 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename2 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page2 '. $upload_path_img2;
                    exec($cmd);
                    
                    $filename3 = 'page3_'.$booking_form_uuid;
                    $upload_path_img3 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename3 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page3 '. $upload_path_img3;
                    exec($cmd);
                    
                    $filename4 = 'page4_'.$booking_form_uuid;
                    $upload_path_img4 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename4 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page4 '. $upload_path_img4;
                    exec($cmd);
                    
                    $filename5 = 'page5_'.$booking_form_uuid;
                    $upload_path_img5 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename5 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page5 '. $upload_path_img5;
                    exec($cmd);
                    
                    $filename6 = 'page6_'.$booking_form_uuid;
                    $upload_path_img6 = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/'.$filename6 . '.pdf';
                    $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$booking_form_uuid.'?view=nsw_c_page6 '. $upload_path_img6;
                    exec($cmd);
                    
                    
                    $final_upload_path = FCPATH . 'assets/uploads/nomination_form_files/'.$nfilename;
                    $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '. $upload_path_img4.' '. $upload_path_img5 .' '. $upload_path_img6;
                    exec($cmd);
                }

                $job_data['get_nomination_form_file'] = site_url('assets/uploads/nomination_form_files/' . $nfilename);
                $this->common->update_data('tbl_led_booking_form', array('id' => $data['booking_form_id']), array('get_nomination_form_file' =>  $nfilename));
            }
        }
        
        
        
        
        $job_id = $this->job->insert_or_update_job($data['booking_form_id'],$job_data);

        if($job_id != ''){
            $this->job->insert_booking_form_activity_note($job_id, $job_data);
        }

        if($job_id != '' && !empty($product_data) && $job_type == 'LED'){
            $this->job->insert_or_update_job_products($job_id,$job_type,$product_data);
        }

        if($job_id != '' && $cost_centre_id == 2){
            $this->job->insert_or_update_heers_job_submission_docs($job_id,$booking_form_image);
        }
        
        if($job_id != '' && $cost_centre_id == 1){
            $this->job->insert_or_update_job_submission_docs($job_id,$booking_form_image);
        }
        if($job_id != '' && $job_type == 'Solar'){
            $this->job->insert_or_update_solar_bill($job_id,$booking_form_image,$electricity_bill);
        }
    
        if($job_type == 'Solar'){
            $product = $data['product'];
            $this->job->insert_or_update_solar_attachments($job_id,$product,$job_data['uuid'] );
        }
        $this->send_booking_form_mail_to_john($job_id,$job_data,$filename,$nfilename,$safilename);

        return $job_id;
    }

    return null;
}

private function send_booking_form_mail_to_john($job_id,$job_data,$bf_filename,$nfilename,$safilename){
    
    if($_SERVER['REMOTE_ADDR'] == '111.93.41.194'){
        return true;
    }

    $business_details = json_decode($job_data['business_details']);
    $authorised_details = json_decode($job_data['authorised_details']);

    $customer_payload = array();
    $customer_payload['CompanyName'] = $business_details->entity_name;
    $customer_payload['Phone'] = $authorised_details->contact_no;
    $customer_payload['Email'] = $authorised_details->email;

    $user_id = $this->aauth->get_user_id();
    $sales_rep_data = $this->user->get_users(FALSE, $user_id, FALSE, FALSE);

    $communication_data = array();
    $communication_data['customer'] = $customer_payload;
    $communication_data['type'] = $job_data['job_type'];
    $communication_data['job_details'] = $this->job->get_job_details_from_job_crm($job_id);

    if($bf_filename != ''){
        $folder = ($job_data['job_type'] == 'LED') ?  'led_booking_form_files' : 'solar_booking_form_files';
            //$bf_filename = str_replace(' ', '%20', $bf_filename);
        $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/'.$folder.'/' . $bf_filename . '.pdf';
    }
    if($nfilename != ''){
            //$nfilename = str_replace(' ', '%20', $nfilename);
        $communication_data['attachment1'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/nomination_form_files/' . $nfilename;
    }
    if($safilename != ''){
            //$safilename = str_replace(' ', '%20', $safilename);
        $communication_data['attachment2'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/site_assessor_form_files/' . $safilename;
    }

    $communication_data['sales_rep'] = array();
    if (!empty($sales_rep_data)) {
        $communication_data['sales_rep']['name'] = $sales_rep_data[0]['full_name'];
        $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
    }
    //$this->email_manager->send_booking_form_success_email_to_john($communication_data);
    $communication_data['cost_centre_id'] = isset($job_data['cost_centre_id']) ? $job_data['cost_centre_id'] : NULL;
    $communication_data['job_id'] = $job_id;
    $this->add_booking_form_mail_to_queue($communication_data,2);

}

private function create_job_custom_fields_in_job_crm($job_id,$data){
    foreach($data as $key => $value){ 
        $table_data = array();
        $table_data['job_id'] = $job_id;
        $table_data['custom_field_id'] = $key;
        $table_data['custom_field_value'] = $value;

        $cond = array('job_id' => $job_id,'custom_field_id' => $key);
        $stat = $this->job->insert_or_update('tbl_job_custom_field_data',$cond,$table_data);
    }
}

public function save_cost_centre(){
    $data = array();
    $cost_centre_id = $this->input->post('cost_centre_id');
    $booking_form_uuid = $this->input->post('booking_form_uuid');
    $lead_uuid = $this->input->post('lead_uuid');
    $site_id = $this->input->post('site_id');
    $type = $this->input->post('type');
    $stat = FALSE;
    if(!empty($this->input->post())){

        $update_data = array();
        $update_data['cost_centre_id'] = $cost_centre_id;
        $tbl = (isset($type) && $type == 'LED') ?  'tbl_led_booking_form' : 'tbl_solar_booking_form';
        if(isset($booking_form_uuid) && $booking_form_uuid != ''){
            $update_cond = array('uuid' => $booking_form_uuid);
            $stat = $this->common->update_data($tbl,$update_cond,$update_data);
        }else{
            $lead_id = $this->common->fetch_cell('tbl_leads','id',array('uuid' => $lead_uuid));
            if($lead_id != ''){
                $update_cond = array('lead_id' => $lead_id,'site_id' => $site_id);
                $stat = $this->common->update_data($tbl,$update_cond,$update_data);
            }
        }

        if($stat){
            $data['status'] = 'Cost Centre Updated Successfully';
            $data['success'] = TRUE;
        }else{
            $data['status'] = 'Nothing to update';
            $data['success'] = FALSE;
        }
    }else{
        $data['status'] = 'Invalid Request';
        $data['success'] = FALSE;
    }
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode($data);
}

    public function add_booking_form_mail_to_queue($communication_data,$type = 1){
        if($_SERVER['REMOTE_ADDR'] == '111.93.41.194' || $_SERVER['REMOTE_ADDR'] == '103.121.234.222' || $_SERVER['REMOTE_ADDR'] == '5.62.61.90'){
            return true;
        }
        //return true;
        $insert_data = array();
        $insert_data['payload'] = json_encode($communication_data);
        $insert_data['notification_type'] = $type;
        $this->job->insert_data('tbl_job_mail_queue',$insert_data);
    }

    public function test_form_pdf($uuid){
        
        $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        $cost_centre_id = $booking_data['cost_centre_id'];


        $business_details = json_decode($booking_data['business_details']);
        $authorised_details = json_decode($booking_data['authorised_details']);
        $space_type = json_decode($booking_data['space_type']);
        $ceiling_height = json_decode($booking_data['ceiling_height']);
        $product = json_decode($booking_data['product']);
        $ae = json_decode($booking_data['ae']);
        $booking_form = json_decode($booking_data['booking_form']);
        $booking_form_image = json_decode($booking_data['booking_form_image']);
        $get_nomination_form_details = json_decode($booking_data['get_nomination_form_details']);
        $site_assessor_form_details = json_decode($booking_data['site_assessor_form_details']); 
        $ap_company_id = $booking_data['ap_company_id'];   
        $nomination_form_data = array();
        $get_nomination_form_details = json_encode($get_nomination_form_details);
        
        $nomination_form_data = json_decode($get_nomination_form_details);
                    
        $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
        if($cc_folder != '' && $cc_folder != 'led'){
            
            $upload_path_img = FCPATH . 'assets/uploads/nomination_form_files/pdf_pages/page5_'.$booking_data['lead_id'].'.jpg';
            $cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1287 https://kugacrm.com.au/admin/booking_form/convert_html_tom_image/'.$uuid.'?view=nf_page5 '. $upload_path_img;
            exec($cmd);
            
            $html = $this->load->view('pdf_files/'.$cc_folder.'/nomination_form/nsw_commercial/main', ['nomination_form_data' => $nomination_form_data,'lead_id' => $booking_data['lead_id']], TRUE);
        }else{
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial/page1', $nomination_form_data, TRUE);
        }
        $nfilename = 'KUGA_ELECTRICAL_Nomination_Form_' . date('m-d-Y_his') . '.pdf';
        //echo $html;die;
        $h = '';
        $f = '';
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'download',
            "save_directory" => __DIR__ . '/../../assets/uploads/nomination_form_files',
            "file_name" => $nfilename,
            "page_size" => 'A4',
            "margin" => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            "header" => $h,
            "footer" => $f
        );
        
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
        if (0 === error_reporting()) { return false; }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
        $this->pdf->phptopdf($pdf_options);
    }
    
    public function convert_html_tom_image($uuid){
        $booking_data = $this->common->fetch_row('tbl_led_booking_form', '*', array('uuid' => $uuid));
        if (empty($booking_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Booking Form pdf cant be created, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/add'));
        }
        $booking_site_data = $this->customer->get_customer_location($booking_data['site_id']);
        $lead_data = $this->lead->get_leads(FALSE, $booking_data['lead_id'], FALSE);
        $cost_centre_id = $booking_data['cost_centre_id'];


        $business_details = json_decode($booking_data['business_details']);
        $authorised_details = json_decode($booking_data['authorised_details']);
        $space_type = json_decode($booking_data['space_type']);
        $ceiling_height = json_decode($booking_data['ceiling_height']);
        $product = json_decode($booking_data['product']);
        $ae = json_decode($booking_data['ae']);
        $booking_form = json_decode($booking_data['booking_form']);
        $booking_form_image = json_decode($booking_data['booking_form_image']);
        $get_nomination_form_details = json_decode($booking_data['get_nomination_form_details']);
        $site_assessor_form_details = json_decode($booking_data['site_assessor_form_details']); 
        $ap_company_id = $booking_data['ap_company_id'];   
        $nomination_form_data = array();
        $get_nomination_form_details = json_encode($get_nomination_form_details);
        $nomination_form_data = json_decode($get_nomination_form_details);
        $view = $this->input->get('view');
        
        $cc_folder = isset($this->views_folder[$ap_company_id]) ? $this->views_folder[$ap_company_id] : '';
        if(isset($view) && $view == 'nf_page5'){
            echo $html = $this->load->view('pdf_files/'.$cc_folder.'/nomination_form/nsw_commercial/page5_image', $nomination_form_data, TRUE);
            die;
        }else if(isset($view) && $view == 'nsw_c_page1'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page1', $nomination_form_data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'nsw_c_page2'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_0', $nomination_form_data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'nsw_c_page3'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_1', $nomination_form_data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'nsw_c_page4'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_2', $nomination_form_data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'nsw_c_page5'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_3', $nomination_form_data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'nsw_c_page6'){
            $html = $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_4', $nomination_form_data, TRUE);
            echo $html;die;
        }
    } 

}

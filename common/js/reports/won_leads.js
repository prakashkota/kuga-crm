var deal_manager = function (options) {
    var self = this;
    this.filters = '';
    this.loading = false;
    this.page = 1;
    this.per_page = 100;
    this.pagination = false;
    this.query_params = options.query_params;
    self.fetch_won_lead_data();
}

deal_manager.prototype.create_deal_stage_table_element = function (ele, value) {
    var table_ele = document.createElement(ele);
    if (value) {
        table_ele.innerHTML = value;
    }
    return table_ele;
};

deal_manager.prototype.create_deal_stage_table = function (data) {
    var self = this;

    var table_body = document.getElementById('lead_list_body');
    if (data.lead_data.length > 0) {
        for (var i = 0; i < data.lead_data.length; i++) {
            var tr = self.create_deal_stage_table_element('tr');
                    
            var date_hack = data.lead_data[i].created_at.split('/');
            var date_hack_String = '<span class="hidden">' + date_hack[2] + date_hack[1] + date_hack[0] + '</span>';
                
            var td_state = self.create_deal_stage_table_element('td',data.lead_data[i].customer_state_postal);
            var td_deal_kw = self.create_deal_stage_table_element('td', (data.lead_data[i].total_solar) ? data.lead_data[i].total_solar : '-');
            
            var td_created_at = self.create_deal_stage_table_element('td', date_hack_String + data.lead_data[i].created_at);
                    
            var fname = (data.lead_data[i].first_name != null) ? data.lead_data[i].first_name : '';
            var lname = (data.lead_data[i].last_name != null) ? data.lead_data[i].last_name : '';
            var td_cname = self.create_deal_stage_table_element('td', fname + ' ' + lname);
                    
            var td_phone = self.create_deal_stage_table_element('td', data.lead_data[i].customer_contact_no);
            var td_email = self.create_deal_stage_table_element('td', data.lead_data[i].customer_email);
                    
            var customer_name = '<a target="__blank" href="' + base_url + 'admin/lead/add?deal_ref=' + data.lead_data[i].uuid + '">' + data.lead_data[i].company_name + '</a>' + '<span class="hidden">' + data.lead_data[i].customer_name + '</span>';
            var td_company_name = self.create_deal_stage_table_element('td', customer_name);
                    
            var td_address = self.create_deal_stage_table_element('td', data.lead_data[i].customer_address);
            var td_bill = self.create_deal_stage_table_element('td', data.lead_data[i].current_monthly_electricity_bill);
                    
            var td_assigned_to = self.create_deal_stage_table_element('td', data.lead_data[i].created_by);
            
            
            var td_sno = self.create_deal_stage_table_element('td',(i+1));
            tr.appendChild(td_sno);
            tr.appendChild(td_cname);
            tr.appendChild(td_company_name);
            tr.appendChild(td_state);
            tr.appendChild(td_address);
            tr.appendChild(td_phone);
            var item_counts = [
                'total_system_size',
                'total_battery_size',
                'total_highbay',
                'total_batten_light',
                'total_panel_light',
                'total_flood_light',
                'total_down_light',
                'total_other'
                ];
            for(var key in item_counts){
                var ic = (data.lead_data[i][item_counts[key]] != null) ? data.lead_data[i][item_counts[key]] : '-';
                var td_ic = self.create_deal_stage_table_element('td', ic);
                tr.appendChild(td_ic);
            }
            table_body.appendChild(tr);       
        }
    }
};

deal_manager.prototype.fetch_won_lead_data = function (data) {
    var self = this;
    
    $.ajax({
        url: base_url + 'admin/reports/fetch_won_lead_data?'+self.query_params,
        type: 'get',
        dataType: 'json',
        beforeSend: function(){
            $('#table_loader').html(createLoader('Please wait while data is being fetched.......'));
        },
        success: function (response) {
            if (response.lead_data != undefined) {
                $('#table_loader').html('');
                self.create_deal_stage_table(response);
            } else {
                if (response.hasOwnProperty('authenticated')) {
                    toastr['error'](response.status);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    toastr["error"]('Something went wrong please try again.');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
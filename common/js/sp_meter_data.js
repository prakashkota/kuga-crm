var sp_meter_data_manager = function (options) {
	var self = this;
	this.proposal_data =
		options.proposal_data && options.proposal_data != ''
			? JSON.parse(options.proposal_data)
			: {};
	this.meter_data_replicated = null;
	this.excel_table = null;
	this.meter_data = [];

	$(document).on('click', '#meter_data_column_mapping_btn', function () {
		$('#meter_data_column_mapping').modal('show');
		self.fetch_file_data();
	});

	var $el1 = $('#meterDataFileURL');
	$el1.fileinput({
		showUpload: false,
		showPreview: false,
		dropZoneEnabled: false,
		maxFileCount: 10,
		// autoProcessQueue: false,
		uploadUrl: base_url + 'admin/meter_data/upload_file',
		uploadAsync: false,
		browseOnZoneClick: true,
		uploadExtraData: function () {
			// myDropzone.processQueue();
			// callback example
			var out = { proposal_id: self.proposal_data.id };
			return out;
		},
	}).on('filebatchselected', function (event, data, previewId, index) {
		$el1.fileinput('upload');
	});

	$('#meterDataFileURL').on(
		'filebatchuploadsuccess',
		function (event, data, previewId, index) {
			toastr['success'](data.response.status);
			$('#meter_data_actions').removeClass('d-none');
			$('#meter_data_file_download_btn').attr('href', data.response.file);
			$('#predefined_choose_file_outer').hide();
		}
	);

	$('#meterDataFileURL').on(
		'filebatchuploaderror',
		function (event, data, previewId, index) {
			toastr['error'](data.response.status);
		}
	);

	$(document).on('click', '#saving_column_mapping_btn', function () {
		var meter_data_replicated = [];
		var meter_data_month_sum = [];
		var missing_months = self.meter_data.meter_data_missing_months;

		if (missing_months.length > 0) {
			var flag = true;
			for (var i = 0; i < 12; i++) {
				var columnSum = self.excel_table
					.getColumnData(i)
					.reduce(function (pv, cv) {
						return parseFloat(pv) + parseFloat(cv);
					}, 0);
				var month = missing_months[j];
				if (i != month - 1) {
					meter_data_month_sum.push(columnSum);
				} else {
					meter_data_month_sum.push(0);
				}
			}

			for (var j in missing_months) {
				var key = parseInt(missing_months[j] - 1);
				var columnSumMissing = self.excel_table
					.getColumnData(key)
					.reduce(function (pv, cv) {
						return parseFloat(pv) + parseFloat(cv);
					}, 0);
				for (var i = 0; i < meter_data_month_sum.length; i++) {
					if (columnSumMissing == 0) {
						toastr['error'](
							'Meter Data for month ' +
								missing_months[j] +
								' is missing.'
						);
						return false;
					}
				}
			}
		}

		console.log(meter_data_month_sum);

		var data = $('#meter_data_column_mapping_form').serialize();
		data += '&proposal_id=' + self.proposal_data.id;
		if (self.meter_data_replicated != '') {
			data +=
				'&meter_data_replicated=' +
				JSON.stringify(self.meter_data_replicated);
		}
		self.save_meter_data_column_mapping(data);
	});
};

sp_meter_data_manager.prototype.fetch_file_data = function () {
	var self = this;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/meter_data/fetch_file_data',
		datatype: 'json',
		data: { proposal_id: self.proposal_data.id },
		beforeSend: function () {
			$('#meter_data_column_mapping_table').html('');
			document
				.getElementById('meter_data_column_mapping_table')
				.appendChild(createLoader('Loading Data.'));
			$('#data_extrapolation_table').html('');
			document
				.getElementById('data_extrapolation_table')
				.appendChild(createLoader('Loading Data.'));
		},
		success: function (stat) {
			self.meter_data = stat;

			// console.log(self.meter_data);

			$('#data_extrapolation_table').html('');
			$('#meter_data_column_mapping_table').html('');

			if (stat.meter_data_missing_months.length > 0) {
				self.handle_data_extrapolation(stat);
			}

			for (var key in stat.column_mapping_rows) {
				if (key == 0) {
					self.create_column_redefine_table_head(
						stat.column_mapping_rows[key],
						stat.column_mapping
					);
				} else {
					self.create_column_redefine_table_rows(
						stat.column_mapping_rows[key]
					);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$('#data_extrapolation_table').html(
				'Looks like something went wrong'
			);
			$('#meter_data_column_mapping_table').html(
				'Looks like something went wrong'
			);
			toastr.remove();
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

sp_meter_data_manager.prototype.create_column_redefine_table_head = function (
	row,
	column_mapping
) {
	var self = this;

	var thead = document.createElement('thead');
	var tr1 = document.createElement('tr');
	for (var key in row) {
		var th = document.createElement('th');
		th.innerHTML = row[key];
		th.setAttribute('style', 'width:150px !important;');
		tr1.appendChild(th);
	}
	thead.appendChild(tr1);

	var tr2 = document.createElement('tr');
	var select_options = [
		'Select',
		'N/A',
		'Date',
		'DateTime',
		'Time',
		'W',
		'kW',
		'kwh',
		'kVa',
	];
	for (var key in row) {
		var select = document.createElement('select');
		for (var key1 in select_options) {
			var option = document.createElement('option');
			option.innerHTML = select_options[key1];
			if (select_options[key1] == 'Select') {
				option.setAttribute('value', '');
			} else {
				option.setAttribute('value', select_options[key1]);
			}
			if (column_mapping != null) {
				if (column_mapping[key] == select_options[key1]) {
					option.setAttribute('selected', 'selected');
				}
			}
			select.appendChild(option);
			select.className = 'form-control';
			select.setAttribute('style', 'height: 28px;');
			select.setAttribute('name', 'column_mapping[' + key + ']');
		}
		var th = document.createElement('th');
		th.appendChild(select);
		th.setAttribute('style', 'width:150px !important;');
		tr2.appendChild(th);
	}
	thead.appendChild(tr2);
	document
		.getElementById('meter_data_column_mapping_table')
		.appendChild(thead);
};

sp_meter_data_manager.prototype.create_column_redefine_table_rows = function (
	row
) {
	var self = this;
	var tr = document.createElement('tr');
	for (var key in row) {
		var td = document.createElement('td');
		td.innerHTML = row[key];
		td.setAttribute(
			'style',
			'width:150px !important; text-overflow: ellipsis;overflow: hidden; white-space: nowrap;'
		);
		tr.appendChild(td);
	}
	document.getElementById('meter_data_column_mapping_table').appendChild(tr);
};

sp_meter_data_manager.prototype.save_meter_data_column_mapping = function (
	data
) {
	var self = this;
	$.ajax({
		type: 'POST',
		url: base_url + 'admin/meter_data/save_meter_data_column_mapping',
		datatype: 'json',
		data: data,
		beforeSend: function () {
			$('#saving_column_mapping_btn').attr('disabled', 'disabled');
		},
		success: function (stat) {
			$('#saving_column_mapping_btn').removeAttr('disabled');
			toastr['success'](stat.status);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$('#saving_column_mapping_btn').removeAttr('disabled');
			toastr.remove();
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

sp_meter_data_manager.prototype.handle_data_extrapolation = function (data) {
	var self = this;

	self.meter_data_replicated = [];
	var final_data = [];

	var months = [
		'jan',
		'feb',
		'mar',
		'apr',
		'may',
		'jun',
		'jul',
		'aug',
		'sep',
		'oct',
		'nov',
		'dec',
	];
	for (var i = 0; i < 24; i++) {
		final_data[i] = [];
		for (var j = 0; j < months.length; j++) {
			final_data[i].push(
				parseFloat(
					data.avg_monthly_load_graph_data[months[j]][i].toFixed(2)
				)
			);
		}
	}

	jexcel.destroy(document.getElementById('data_extrapolation_table'), false);
	var copyFrom = [];
	var source = 0;
	var destination = 0;
	self.excel_table = jexcel(
		document.getElementById('data_extrapolation_table'),
		{
			data: final_data,
			columns: [
				{
					type: 'text',
					title: 'Jan',
					width: 60,
				},
				{
					type: 'text',
					title: 'Feb',
					width: 60,
				},
				{
					type: 'text',
					title: 'Mar',
					width: 60,
				},
				{
					type: 'text',
					title: 'Apr',
					width: 60,
				},
				{
					type: 'text',
					title: 'May',
					width: 60,
				},
				{
					type: 'text',
					title: 'Jun',
					width: 60,
				},
				{
					type: 'text',
					title: 'Jul',
					width: 60,
				},
				{
					type: 'text',
					title: 'Aug',
					width: 60,
				},
				{
					type: 'text',
					title: 'Sep',
					width: 60,
				},
				{
					type: 'text',
					title: 'Oct',
					width: 60,
				},
				{
					type: 'text',
					title: 'Nov',
					width: 60,
				},
				{
					type: 'text',
					title: 'Dec',
					width: 60,
				},
			],
			onselection: function (obj, col, val) {
				if (copyFrom.indexOf(col) == -1) {
					copyFrom.push(col);
				}
			},
			onpaste: function (obj, cell, val) {
				source = copyFrom[copyFrom.length - 2] || 0;
				destination = cell[0]['col'];
				self.meter_data_replicated.push({
					replicated_from: parseInt(source) + 1,
					replicated_to: parseInt(destination) + 1,
				});
				copyFrom = [];
			},
		}
	);
};

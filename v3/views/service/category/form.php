<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;} 
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Franchise - <?php } ?>Add Category</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Category Details</h4>
                                    <div class="form-group">
                                        <label>Category Name</label>
                                        <input type="text" name="category_name"  class="form-control" placeholder="Category Name" value="<?php echo (isset($category_name)) ? $category_name : ''; ?>"  required>
                                        <?php echo form_error('category_name', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="buttwrap">
                                <input type="submit"  class="btn" value="SAVE">
                            </div>    
                        </div>
                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Sub Category Details</h4>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default">
                                            <table width="100%" border="0">
                                                <thead>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Status</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="category_items_wrapper">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button id="add_category_items" type="button" class="btn btn-primary" >Add Sub Category</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


<script>
    var category_item_data = function (options) {
        var self = this;
        this.id = (options.id && options.id != '') ? options.id : '';
        this.category_items_wrapper = 'category_items_wrapper'
        this.category_items_btn = 'add_category_items';

        if (options.id && options.id != '') {
            self.load_item_data();
        }

        $('form button').on("click", function (e) {
            e.preventDefault();
        });

        self.create_category_items();
        self.remove_category_items();
    };

    category_item_data.prototype.load_item_data = function () {
        var self = this;
        var category_item_id = self.id;
        var form_data = 'category_id=' + category_item_id;
        $.ajax({
            url: base_url + 'admin/service/category_item_data',
            type: 'get',
            data: form_data,
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    for (var i = 0; i < response.category_item_data.length; i++) {
                        self.create_category_items(response.category_item_data[i]);
                    }
                } 
            }
        });
    };

    category_item_data.prototype.create_category_items = function (data) {
        var self = this;
        var category_items_wrapper = document.getElementById(self.category_items_wrapper);
        var category_items_btn = document.getElementById(self.category_items_btn);
        if (!data) {
            category_items_btn.addEventListener("click", function (e) { //on add input button click
                e.preventDefault();
                var status_set_field = self.create_status_set_field();
                console.log(status_set_field);
                $(category_items_wrapper).append('<tr>'
                        + '<td><input type="text" class="form-control" name="category_item_data[category_name][]" value="" required /></td>'
                        + '<td>' + status_set_field + '</td>'
                        + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            });
        } else {
            var status_set_field = self.create_status_set_field(data.status);
            $(category_items_wrapper).append('<tr>'
                    + '<td><input type="text" class="form-control" name="category_item_data[category_name][]" value="' + data.category_name + '" required /></td>'
                    + '<td>' + status_set_field + '</td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
        }
    };

    category_item_data.prototype.remove_category_items = function () {
        var self = this;
        var category_items_wrapper = document.getElementById(self.category_items_wrapper);
        $(category_items_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
            e.preventDefault();
            $(this).parent().parent('tr').remove();
        });
    };


    category_item_data.prototype.create_status_set_field = function (value) {
        var select = '<select name="category_item_data[status][]" class="form-control">';
        var selected1 = (value && value == 1) ? 'selected="selected"' : '';
        var selected0 = (value && value == 0) ? 'selected="selected"' : '';
        if (value = '' || value == null) {
            selected1 = 'selected="selected"';
        }
        select += '<option value="1" ' + selected1 + '>Active</option>';
        select += '<option value="0" ' + selected0 + '>In-Active</option>';
        return select;
    };

    var context = {};
    context.id = "<?php echo (isset($id) && $id != '') ? $id : '' ?>";
    var ltd = new category_item_data(context);
</script>
</body>

</html>


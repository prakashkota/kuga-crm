
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Admin  - Add Product</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" name="productAdd" id="productAdd" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Product Details</h4>
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="text" name="np_name"  class="form-control" placeholder="Product Name" value="<?php echo (!empty($product_data)) ? $product_data['np_name'] : ''; ?>"  required>
                                        <?php echo form_error('name', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Type</label>
                                        <select name="type_id" class="form-control"  required>
                                            <?php  foreach ($product_types as $key => $value) {  ?>
                                                <option value="<?php echo $value['type_id']; ?>" <?php echo (!empty($product_data) && $product_data['type_id'] == $value['type_id']) ? 'selected' : ''; ?>>
                                                    <?php echo $value['type_name']; ?> 
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('type_id', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>State</label>
                                        <select name="state" class="form-control"  required>
                                            <?php  foreach ($states as $key => $value) {  ?>
                                                <option value="<?php echo $value; ?>" <?php echo (!empty($product_data) && $product_data['state'] == $value) ? 'selected' : ''; ?>>
                                                    <?php echo $value; ?> 
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('state', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label>Wattage</label>
                                        <input type="number" name="np_wattage"  class="form-control" placeholder="Wattage" value="<?php echo (!empty($product_data)) ? $product_data['np_wattage'] : ''; ?>" required>
                                        <?php echo form_error('np_wattage', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Rated Life</label>
                                        <input type="number" name="np_rated_life"  class="form-control" placeholder="Rated Life" value="<?php echo (!empty($product_data)) ? $product_data['np_rated_life'] : ''; ?>" required>
                                        <?php echo form_error('np_rated_life', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Rebate Details</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Rebate 2000</label>
                                                <input type="number" step="0.01" name="rebate_2000"  class="form-control"   placeholder="Rebate" value="<?php echo (!empty($product_data)) ? $product_data['rebate_2000'] : ''; ?>">
                                            </div>    
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Rebate 3000</label>
                                                <input type="number" step="0.01" name="rebate_3000"  class="form-control"   placeholder="Rebate" value="<?php echo (!empty($product_data)) ? $product_data['rebate_3000'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Rebate 5000</label>
                                                <input type="number" step="0.01" name="rebate_5000"  class="form-control"   placeholder="Rebate" value="<?php echo (!empty($product_data)) ? $product_data['rebate_5000'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Rebate 5000 A</label>
                                                <input type="number" step="0.01" name="rebate_5000_a"  class="form-control"   placeholder="Rebate" value="<?php echo (!empty($product_data)) ? $product_data['rebate_5000_a'] : ''; ?>">
                                            </div> 
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Rebate 5000 C</label>
                                                <input type="number" step="0.01" name="rebate_5000_c"  class="form-control"   placeholder="Rebate" value="<?php echo (!empty($product_data)) ? $product_data['rebate_5000_c'] : ''; ?>">
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="buttwrap" style="float:right;">
                        <input type="submit"   class="btn" value="SAVE">
                    </div>                                            
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>


<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page-spl-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 1287px !important;
            max-height: 910px !important;
            width: 1287px !important;
            height: 910px !important;
        }

        .page-spl .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 1044px;
            padding-top: 30px;
        }

        .page-spl .top hr {
            width: 76%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-spl .text_color {
            color: #D01E2A;
        }

        .page-spl .title {
            margin-top: -50px;
        }

        .page-spl .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page-spl .footer {
            margin-top: -3px;
            display: inline-flex;
        }

        .page-spl .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-spl .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }

        .page-spl .ingBox {
            display: inline-flex;
            padding-top: 100px;
            padding-bottom: 188px;
        }

        .page-spl .iiimage {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 650px;
            margin-left: 48px;
        }

        .page-spl .iibox {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 438px;
            margin-left: 48px;
        }

        .page-spl .fimagee img {
            margin-left: 1120px;
        }

        #panel_notes_html ul {
            margin-left: 18px;
        }
    </style>
</head>

<?php
/** Manage Data Here */
$mapping_tool = empty($mapping_tool) ? [] : $mapping_tool;
$mapping_tool['snapshot1'] = isset($mapping_tool['snapshot1']) && $mapping_tool['snapshot1'] != NULL ? $mapping_tool['snapshot1'] : $proposal_data['panel_layout_image'];
$mapping_tool['snapshot2'] = isset($mapping_tool['snapshot2']) && $mapping_tool['snapshot2'] != NULL ? $mapping_tool['snapshot2'] : $proposal_data['panel_layout_image'];
$mapping_tool['notes'] = isset($mapping_tool['notes']) && $mapping_tool['notes'] != NULL ? $mapping_tool['notes'] : $proposal_data['additional_notes'];
?>

<body>
    <div class="page-spl-back-bg back-bg page-spl">
        <div class="top">
            <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>YOUR SOLAR <span class="text_color"> PANEL LAYOUT</span></h1>
        </div>

        <div class="ingBox">
            <div class="iiimage">
                <div class="add-picture1" data-src="<?php echo $mapping_tool['snapshot2']; ?>" style="background-image: url('<?php echo $mapping_tool['snapshot2']; ?>'); 
                           background-repeat: no-repeat; background-position: 50% 50%; 
                           background-size: 100% 100%; 
                           width: 650px;
                           height: 400px; ">
                </div>
            </div>
            <div class="iibox" style="padding-right:20px;padding-left:20px;">
                <a href="javascript:void(0);" class="btn" id="tool_panel_design_notes" style="margin-top: -36px;padding-left: 92%;"><i class="fa fa-edit"></i> Edit</a>
                <span id="panel_notes_html"><?php echo $mapping_tool['notes']; ?></span>
            </div>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>

    <div class="modal fade" id="panel_notes_modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title pb-1">Notes for Panel Design</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="form-block d-block clearfix">
                            <textarea id="panel_notes"><?php echo $mapping_tool['notes']; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save_panel_notes_btn">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var panel_notes_html = $('#panel_notes_html').html();

        $('#tool_panel_design_notes').on('click', function() {
            CKEDITOR.replace(
                document.querySelector('#panel_notes'), {
                    toolbar: [{
                            name: 'basicstyles',
                            groups: ['basicstyles', 'cleanup'],
                            items: ['Bold', 'Italic', 'Underline']
                        },
                        {
                            name: 'paragraph',
                            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                            items: ['NumberedList', 'BulletedList', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
                        },
                        {
                            name: 'styles',
                            items: ['Styles', 'Format', 'Font', 'FontSize']
                        },
                    ]
                });
            $('#panel_notes_modal').modal('show');
        });

        $(document).on('click', '#save_panel_notes_btn', function() {
            var editor_data = CKEDITOR.instances.panel_notes.getData();
            var data = {};
            data.key = 'notes';
            data.value = editor_data;
            data.proposal_id = '<?php echo $proposal_data["id"]; ?>';
            data.tool_id = '<?php echo isset($mapping_tool["id"]) ? $mapping_tool["id"] : ""; ?>';

            $.ajax({
                url: base_url + 'admin/solar-tool/save_mapping_data',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(data) {
                    toastr['success'](data.status);
                    $('#panel_notes_html').html(editor_data);
                    $('#panel_notes_modal').modal('hide');
                },
                error: function(data) {
                    toastr['success']('Data Saving Failed');
                },
            });
        });
    </script>

</body>

</html>
<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table id="booking_items" width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Product Type</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">System</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_0" name="product[product_type][]" value="System Size" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_0" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_0" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_0" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_0" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_1" name="product[product_type][]" value="Solar Panels" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                        <input  class="product_capacity product_capacity_1" name="product[product_capacity][1]" value="" type="hidden" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_1" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_1" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_1" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_1" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_2" name="product[product_type][]" value="Inverters" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                        <input  class="product_capacity product_capacity_2" name="product[product_capacity][2]" value="" type="hidden" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_2" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_2" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_2" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_2" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_3" name="product[product_type][]" value="PV Mounting Kit" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_3" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_3" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_3" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_3" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_4" name="product[product_type][]" value="" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_4" name="product[product_name][]" value="" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_4" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_4" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_4" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_5" name="product[product_type][]" value="Extra:" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_5" name="product[product_name][]" value="" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_5" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_5" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_5" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

				<tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_6" name="product[product_type][]" value="" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_6" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_6" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_6" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_6" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
				
                <?php for ($i = 7; $i < 12; $i++) { ?>
                    <tr>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="product_type product_type_<?php echo $i; ?>" name="product[product_type][]" style="padding:5px; width:100%; height: inherit;" type="text" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_name product_name_<?php echo $i; ?>" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_qty product_qty_<?php echo $i; ?>" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_cost_excGST product_cost_excGST_<?php echo $i; ?>" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_total_cost_excGst product_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                    </tr>   
                <?php } ?>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_excGst" name="booking_form[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px"><!0-- Is there a forklift on site? --></td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_Gst" name="booking_form[total_Gst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                        <table width="557" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="345">
                                    <!-- <input id="is_forklift_on_site_yes" name="booking_form[is_forklift_on_site]" value="1" style="padding:5px; width:20px; height: inherit;" type="radio"> YES
                                    <input id="is_forklift_on_site_no" name="booking_form[is_forklift_on_site]" value="0" style="padding:5px; width:20px; height: inherit;" type="radio"> NO -->
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Array Details</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Height:</td>
                                            <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="roof_height" name="booking_form[roof_height]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Type:</td>
                                            <td width="251" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <select class="form-control" name="booking_form[roof_type]" id="roof_type" required>
                                                    <option value="">Select one</option>
                                                    <?php
                                                    $roof_types = unserialize(BF_ROOF_TYPE);
                                                    if (!empty($roof_types)) {
                                                        foreach ($roof_types as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Equipments:</td>
                                            <td width="665" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="access_equipments" name="booking_form[access_equipments]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>

            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>

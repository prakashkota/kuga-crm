
var activity_manager = function (options) {
    var self = this;
    this.view = options.view;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : '';
    this.is_sales_rep = options.is_sales_rep;
    this.activity_action = '';
    this.activity_trigger = 2;  //1:Add 2:Schedule
    self.initialize();
    self.save_activity();
    self.upload_activity_file();
    self.delete_activity();

};

activity_manager.prototype.initialize = function () {
    var self = this;
    var minDate = moment().add(1, 'd').toDate();
    var defaultDate = moment().add(1, 'M').toDate();
    $.fn.datepicker.defaults.format = "dd-mm-yyyy";

    $('.datepicker').datepicker({
        autoclose: true,
        useCurrent: false,
        startDate: minDate,
    }).on('changeDate', function () {
        $('.datepicker').hide();
    });

    $('#scheduled_time input').timepicker({
        'minTime': '12:00am',
        'maxTime': '11:00pm',
        'showDuration': true
    });

    $('#scheduled_duration input').timepicker({
        'minTime': '0:30',
        'maxTime': '08:30',
        'timeFormat': 'H:i'
    });

    self.create_attendees_select2([]);

    $('#activity_type').change(function () {
        var activity_type = $(this).val();
        $('#activity_location').addClass('hidden');

        if (activity_type === 'Visit' && self.activity_trigger == 2) {
            $('#activity_location').removeClass('hidden');
        }

        if (activity_type === 'Note') {
            $('#scheduler').addClass('hidden');
            $('#attendees').addClass('hidden');
        } else {
            if (self.activity_trigger == 2) {
                $('#scheduler').removeClass('hidden');
                $('#attendees').removeClass('hidden');

                $('#scheduled_time input').timepicker({
                    'minTime': '12:00am',
                    'maxTime': '11:00pm',
                    'showDuration': true
                });

                $('#scheduled_duration input').timepicker({
                    'minTime': '0:30',
                    'maxTime': '08:30',
                    'timeFormat': 'H:i'
                });
            }
        }

    });

    if (self.is_sales_rep == '') {
        $('#userid').change(function () {
            if (self.view == 'list') {
                document.getElementById('activity_table_body').innerHTML = '';
                //document.getElementById('activity_table_body').appendChild(createLoader('Fetching activity data....'));
                document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
                document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
            } else if (self.view == 'calendar') {
                $('#calendar').fullCalendar('destroy');
                document.getElementById('calendar').innerHTML = '';
                //document.getElementById('calendar').appendChild(createLoader('Fetching activity data....'));
                document.getElementById('calendar').appendChild(createPlaceHolder(false));
                document.getElementById('calendar').appendChild(createPlaceHolder(false));
            }
            var userid = $(this).val();
            var data = 'user_id=' + userid;
            self.fetch_activity_data(data);
        });
    }

    if (self.view == 'list') {
        document.getElementById('activity_table_body').innerHTML = '';
        //document.getElementById('activity_table_body').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
        document.getElementById('activity_table_body').appendChild(createPlaceHolder(false));
        self.fetch_activity_data();
    } else if (self.view == 'calendar') {
        $('#calendar').fullCalendar('destroy');
        document.getElementById('calendar').innerHTML = '';
        //document.getElementById('calendar').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('calendar').appendChild(createPlaceHolder(false));
        document.getElementById('calendar').appendChild(createPlaceHolder(false));
        self.fetch_activity_data();
    } else if (self.view == 'proposal') {
        document.getElementById('all_activity_table_body').innerHTML = '';
        //document.getElementById('all_activity_table_body').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('all_activity_table_body').appendChild(createPlaceHolder(false));
        document.getElementById('all_activity_table_body').appendChild(createPlaceHolder(false));
        var form_data = 'lead_id=' + self.lead_data.id;
        self.fetch_activity_data(form_data);
    } else if (self.view == 'lead') {
        document.getElementById('all_activity_container').innerHTML = '';
        //document.getElementById('all_activity_container').appendChild(createLoader('Fetching activity data....'));
        document.getElementById('all_activity_container').appendChild(createPlaceHolder(false));
        document.getElementById('all_activity_container').appendChild(createPlaceHolder(false));
        var form_data = 'lead_id=' + self.lead_data.id;
        self.fetch_activity_data(form_data);
    }

};

activity_manager.prototype.create_attendees_select2 = function (data) {
    var self = this;
    $(".attendees").select2({
        data: data,
        dropdownParent: $('#activity_modal'),
        placeholder: 'Enter Emails',
        minimumInputLength: 5,
        tags: true,
        // automatically creates tag when user hit space or comma:
        tokenSeparators: [",", " "],
        createTag: function (term, data) {
            var value = term.term;
            if (self.validateEmail(value)) {
                return {
                    id: value,
                    text: value
                };
            }
            return null;
        }
    });
};

activity_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

activity_manager.prototype.floatToTime = function (num, other_format) {
    var sign = num >= 0 ? 1 : -1;
    var min = 1 / 60;

    // Get positive value of num
    num = num * sign;

    // Separate the int from the decimal part
    var intpart = Math.floor(num);
    var decpart = num - intpart;

    // Round to nearest minute
    decpart = min * Math.round(decpart / min);

    var minutes = Math.floor(decpart * 60);

    // Sign result
    sign = sign == 1 ? '' : '-';

    // pad() adds a leading zero if needed
    // return sign + intpart + 'h' + pad(minutes, 2);
    var duration = '';
    var other_duration = '';
    if (intpart != 0) {
        duration += intpart + 'h';
        other_duration += '0' + intpart;
    }
    if (minutes != 0) {
        duration += minutes + 'm';
        other_duration += (minutes > 10) ? ':' + minutes : ':0' + minutes;
    }
    if (intpart == 0 && minutes == 0) {
        duration += '-';
    }
    if (intpart != 0 && minutes == 0) {
        other_duration = '0' + intpart + ':00';
    } else if (intpart == 0 && minutes != 0) {
        other_duration = (minutes > 10) ? '00:' + minutes : '00:0' + minutes;
    }

    if (intpart == 0 && minutes == 0) {
        duration += '-';
    }
    if (other_format == true) {
        return other_duration;
    } else {
        return sign + duration;
    }

}

activity_manager.prototype.create_activity_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    /**var mark_as_completed_checkbox = document.createElement('input');
     mark_as_completed_checkbox.setAttribute('type', 'checkbox');
     mark_as_completed_checkbox.setAttribute('class', 'event_edit');
     mark_as_completed_checkbox.setAttribute('data-item', JSON.stringify(data));
     */

    var mark_as_completed_checkbox = document.createElement('i');
    mark_as_completed_checkbox.setAttribute('class', 'fa fa-pencil event_edit');
    mark_as_completed_checkbox.setAttribute('style', 'cursor:pointer;');
    mark_as_completed_checkbox.setAttribute('data-item', JSON.stringify(data));


    var td1 = document.createElement('td');
    td1.appendChild(mark_as_completed_checkbox);

    var td2 = document.createElement('td');
    td2.innerHTML = (data.scheduled_date != null) ? data.scheduled_date : '-';

    var td3 = document.createElement('td');
    td3.innerHTML = (data.scheduled_time != null) ? data.scheduled_time : '-';

    var td4 = document.createElement('td');
    td4.innerHTML = (data.scheduled_duration != null) ? self.floatToTime(data.scheduled_duration) : '-';

    var td5 = document.createElement('td');
    td5.innerHTML = data.activity_type;

    var td6 = document.createElement('td');
    td6.innerHTML = data.owner_name;

    var td7 = document.createElement('td');
    td7.innerHTML = data.first_name + ' ' + data.last_name;

    var td8 = document.createElement('td');
    td8.innerHTML = data.customer_address;

    var td9 = document.createElement('td');
    td9.innerHTML = data.customer_contact_no;

    var td10 = document.createElement('td');
    td10.innerHTML = (data.status == 1) ? "<i class='fa fa-check-circle' style='color:green; font-size:18px; text-align:center;'></i>" : "<i class='fa fa-times-circle' style='color:red; font-size:18px; text-align:center;'></i>";

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);

    return tr;
};

activity_manager.prototype.create_activity_card = function (data) {
    var self = this;

    var activity_icon = '';
    if (data.activity_type == 'Phone') {
        activity_icon = 'phone';
    } else if (data.activity_type == 'Visit') {
        activity_icon = 'building';
    } else if (data.activity_type == 'Email') {
        activity_icon = 'envelope';
    } else if (data.activity_type == 'Note') {
        activity_icon = 'pencil';
    }

    var actiivity_card = document.createElement('div');
    actiivity_card.className = "card kg-activity__item kg-activity__item--default card_" + data.id;

    var actiivity_card_body = document.createElement('div');
    actiivity_card_body.className = "card-body";

    var actiivity_card_title = document.createElement('div');
    actiivity_card_title.className = "kg-activity__item__title";
    actiivity_card_title.innerHTML = "<span class='mr-2'><i class='fa fa-" + activity_icon + "'></i></span>" + data.activity_type + ' - ' + data.activity_title;

    var activity_card_attachment = document.createElement('a');
    activity_card_attachment.className = "ml-3";
    activity_card_attachment.setAttribute('href',base_url+ 'assets/uploads/activity_files/' + data.attachment);
    activity_card_attachment.setAttribute('target','__blank');
    activity_card_attachment.innerHTML = "<i class='fa fa-paperclip'></i></span>";
    
    var actiivity_card_action = document.createElement('a');
    actiivity_card_action.className = "kg-activity__item__action";
    actiivity_card_action.setAttribute('href', '#');
    actiivity_card_action.setAttribute('role', 'button');
    actiivity_card_action.setAttribute('data-toggle', 'dropdown');
    actiivity_card_action.setAttribute('aria-haspopup', 'true');
    actiivity_card_action.setAttribute('aria-expanded', 'false');

    var actiivity_card_action_dropdown = document.createElement('div');
    actiivity_card_action_dropdown.className = "dropdown-menu";

    var actiivity_card_action_dropdown_item_edit = document.createElement('a');
    actiivity_card_action_dropdown_item_edit.className = "dropdown-item";
    actiivity_card_action_dropdown_item_edit.innerHTML = 'Edit';

    var actiivity_card_action_dropdown_item_delete = document.createElement('a');
    actiivity_card_action_dropdown_item_delete.className = "dropdown-item activity_delete";
    actiivity_card_action_dropdown_item_delete.innerHTML = 'Delete';
    actiivity_card_action_dropdown_item_delete.setAttribute('data-id', data.id);
    actiivity_card_action_dropdown_item_delete.setAttribute('href', 'javascript:void(0)');

    //actiivity_card_action_dropdown.appendChild(actiivity_card_action_dropdown_item_edit);
    actiivity_card_action_dropdown.appendChild(actiivity_card_action_dropdown_item_delete);

    var actiivity_card_subtitle = document.createElement('div');
    actiivity_card_subtitle.className = 'card-subtitle mb-4';

    var actiivity_card_subtitle_item_date = document.createElement('span');
    actiivity_card_subtitle_item_date.className = 'text-muted';
    actiivity_card_subtitle_item_date.innerHTML = moment(data.created_at_calendar).format('LLL') + ' - ' + data.first_name + ' ' + data.last_name;

    var actiivity_card_subtitle_item_user = document.createElement('span');
    actiivity_card_subtitle_item_user.className = 'text-muted ml-3';
    actiivity_card_subtitle_item_user.innerHTML = '<i class="fa fa-user"></i> ' + data.owner_name;

    var actiivity_card_subtitle_item_site = document.createElement('span');
    actiivity_card_subtitle_item_site.className = 'text-muted ml-3';
    actiivity_card_subtitle_item_site.innerHTML = '<i class="fa fa-building"></i> ' + data.customer_address;

    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_date);
    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_user);
    actiivity_card_subtitle.appendChild(actiivity_card_subtitle_item_site);

    var actiivity_card_description = document.createElement('p');
    actiivity_card_description.className = 'kg-activity__item__description';
    actiivity_card_description.innerHTML = data.activity_note;

    actiivity_card_body.appendChild(actiivity_card_title);
    if(data.attachment != null && data.attachment != ''){
        actiivity_card_body.appendChild(activity_card_attachment);
    }
    actiivity_card_body.appendChild(actiivity_card_action);
    actiivity_card_body.appendChild(actiivity_card_action_dropdown);
    actiivity_card_body.appendChild(actiivity_card_subtitle);
    actiivity_card_body.appendChild(actiivity_card_description);

    actiivity_card.appendChild(actiivity_card_body);

    return actiivity_card;
};

activity_manager.prototype.fetch_activity_data = function (form_data) {
    var self = this;
    $.ajax({
        url: base_url + "admin/activity/fetch?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (stat) {
            if (stat.success == true) {
                if (stat.activities.length > 0) {
                    if (self.view == 'list') {
                        document.getElementById('activity_table_body').innerHTML = '';
                        self.handle_list_view(stat);
                    } else if (self.view == 'calendar') {
                        document.getElementById('calendar').innerHTML = '';
                        self.handle_calendar_view(stat);
                    } else if (self.view == 'lead') {
                        document.getElementById('all_activity_container').innerHTML = '';
                        self.handle_lead_view(stat);
                    }
                } else {
                    if (self.view == 'list') {
                        document.getElementById('activity_table_body').innerHTML = '<tr><td class="text-center" colspan="10">No Activity Data Found</td></tr>';
                    } else if (self.view == 'calendar') {
                        document.getElementById('calendar').innerHTML = '<div class="alert alert-danger text-center">No Activity Data Found</div>';
                    } else if (self.view == 'lead') {
                        document.getElementById('all_activity_container').innerHTML = '<div class="alert alert-danger text-center">No Activity Data Found</div>';
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (self.view == 'list') {
                document.getElementById('activity_table_body').innerHTML = 'Looks Like Something Went Wrong';
            } else if (self.view == 'calendar') {
                document.getElementById('calendar').innerHTML = 'Looks Like Something Went Wrong';
            } else if (self.view == 'calendar') {
                document.getElementById('all_activity_container').innerHTML = 'Looks Like Something Went Wrong';
            }

        }
    });
};

activity_manager.prototype.handle_list_view = function (stat) {
    var self = this;
    if ($('#activity_table')) {
        $('#activity_table').DataTable().clear();
        $('#activity_table').DataTable().destroy();
    }
    for (var i = 0; i < stat.activities.length; i++) {
        var stats = self.create_activity_table_row(stat.activities[i]);
        document.getElementById('activity_table_body').appendChild(stats);
    }
    var table = $('#activity_table').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "order": [
            [0, "asc"]
        ],
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": true,
        paging: false,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Columns',
                postfixButtons: ['colvisRestore'],
            }
        ]
    });
    $('.event_edit').click(function () {
        var activity_data = $(this).attr('data-item');
        activity_data = JSON.parse(activity_data);
        self.edit_activity(activity_data);
    });
    $('[data-toggle="tooltip"]').tooltip();
};

activity_manager.prototype.handle_color = function (lead_stage) {
    if(lead_stage > 0 && lead_stage < 3){
        return 'yellow';
    }else if(lead_stage > 2 && lead_stage < 8){
        return 'grey';
    }else if(lead_stage > 7 && lead_stage < 10){
        return 'amber';
    }else if(lead_stage == 10){
        return 'blue';
    }else if(lead_stage == 11){
        return 'green';
    }else if(lead_stage == 12){
        return 'red';
    }
};

activity_manager.prototype.handle_calendar_view = function (stat) {
    var self = this;
    var events = [];
    for (var i = 0; i < stat.activities.length; i++) {
        var event_obj = {};
        event_obj.title = ' ';
        event_obj.title += (stat.activities[i].activity_title != '') ? stat.activities[i].activity_type : stat.activities[i].activity_type;
        event_obj.title += ' - ' + stat.activities[i].first_name + ' ' + stat.activities[i].last_name;
        event_obj.className = 'event_' + stat.activities[i].activity_type;
        event_obj.className += ' calendar_grid-item--'+self.handle_color(parseInt(stat.activities[i].lead_stage));
        
        var schedule_date = stat.activities[i].scheduled_date_calendar;
        var schedule_time = stat.activities[i].scheduled_time_calendar;
        var schedule_duration = stat.activities[i].scheduled_duration;

        if (schedule_date != '' && schedule_date != null && schedule_time != '' && schedule_time != null && schedule_duration != '' && schedule_duration != null) {
            var start_date = stat.activities[i].scheduled_date_calendar;
            var start_time = stat.activities[i].scheduled_time_calendar;
            event_obj.start = moment(start_date).format('YYYY-MM-DD') + 'T' + moment(start_time, 'HH:mm:ss').format('HH:mm');
            var end_date = start_date;
            event_obj.end = end_date + 'T' + moment(start_time, 'HH:mm:ss').add(parseFloat(stat.activities[i].scheduled_duration), 'hours').format('HH:mm');
        } else {
            event_obj.start = moment(stat.activities[i].created_at_calendar).format('YYYY-MM-DD HH:mm:ss');
        }

        if (stat.activities[i].activity_type == 'Phone') {
            event_obj.icon = 'phone';
        } else if (stat.activities[i].activity_type == 'Visit') {
            event_obj.icon = 'building';
        } else if (stat.activities[i].activity_type == 'Email') {
            event_obj.icon = 'envelope';
        } else if (stat.activities[i].activity_type == 'Note') {
            event_obj.icon = 'pencil';
        }
        event_obj.data = stat.activities[i];
        event_obj.is_completed = stat.activities[i].status;
        event_obj.is_google_synced = (stat.activities[i].gcal_event_id && stat.activities[i].gcal_event_id != null) ? true : false;
        events.push(event_obj);
    }

    $('#calendar').fullCalendar({
        height: 'auto',
        defaultView: 'eventWeekDay',
        groupByResource: true,
        customButtons: {
            eventPhone: {
                text: 'Phone',
                click: function () {
                    $('.event_Phone').toggle();
                }
            },
            eventVisit: {
                text: 'Site Visit',
                click: function () {
                    $('.event_Visit').toggle();
                }
            },
            eventEmail: {
                text: 'Email',
                click: function () {
                    $('.event_Email').toggle();
                }
            }
        },
        header: {
            left: 'eventPhone,eventVisit,eventEmail',
            center: 'title',
            right: 'prev,next,agendaDay,eventWeekDay'
        },
        views: {
            eventWeekDay: {
                type: 'agenda',
                duration: {days: 7},
                eventLimit: 6
            }
        },
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        navLinks: false,
        events: events,
        eventRender: function (event, element) {
            if (event.icon) {
                element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i> ");
            }

            if (event.is_google_synced) {
                element.find(".fc-time").append('<i class="fa fa-google" aria-hidden="true" style="float:right; margin-left:5px;"></i>');
            }

            if (event.is_completed == '1') {
                element.find(".fc-time").append("<span style='float:right;'> <i class='fa fa-check-circle' style='color:green; font-size:12px;'></i> </span>");
            }
        },
        eventClick: function (event, jsEvent, view) {
            var activity_data = event.data;
            self.edit_activity(activity_data);
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
}

activity_manager.prototype.handle_lead_view = function (data) {
    var self = this;
    document.getElementById('all_activity_container').innerHTML = "";
    for (var i = 0; i < data.activities.length; i++) {
        var activity_card = self.create_activity_card(data.activities[i]);
        document.getElementById('all_activity_container').appendChild(activity_card);
    }
    $('.kg-activity__item').click(function (e) {
        if (e.originalEvent.detail === 2) {
            $(this).find('div > p.kg-activity__item__description').slideToggle();
        }
    });
};

activity_manager.prototype.save_activity = function () {
    var self = this;
    $(document).on('click', '#addActivity', function () {
        var flag = true;
        var activity_title = $('#activity_title').val();
        var activity_type = $('#activity_type').val();
        $('#activity_title').removeClass('is-invalid');
        $('#activity_type').removeClass('is-invalid');
        $('#scheduled_date_value').removeClass('is-invalid');
        $('#scheduled_time_value').removeClass('is-invalid');
        $('#scheduled_duration_value').removeClass('is-invalid');

        if (activity_title == '') {
            $('#activity_title').addClass('is-invalid');
            flag = false;
        }

        if (activity_type == '') {
            $('#activity_type').addClass('is-invalid');
            flag = false;
        }

        if (activity_type != 'Note' && self.activity_trigger == 2) {
            var scheduled_date_value = $('#scheduled_date_value').val();
            var scheduled_time_value = $('#scheduled_time_value').val();
            var scheduled_duration_value = $('#scheduled_duration_value').val();

            if (scheduled_date_value == '') {
                $('#scheduled_date_value').addClass('is-invalid');
                flag = false;
            }
            if (scheduled_time_value == '') {
                $('#scheduled_time_value').addClass('is-invalid');
                flag = false;
            }
            if (scheduled_duration_value == '') {
                $('#scheduled_duration_value').addClass('is-invalid');
                flag = false;
            }

        }

        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            $("#addActivity").attr("disabled", "disabled");
            var activity_add_form_data = $('#activity_add_form').serialize();
            self.save_activity_handler(activity_add_form_data);
            $("#addActivity").removeAttr("disabled");
        }
    });
};

activity_manager.prototype.save_activity_handler = function (data) {
    var self = this;
    if (self.lead_data !== '') {
        data += '&lead_id=' + self.lead_data.id;
    }
    if (self.activity_action !== '') {
        data += '&activity_action=' + self.activity_action;
    }
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/activity/save',
        datatype: 'json',
        data: data + '&activity_action=' + self.activity_action + '&action=save_activity',
        beforeSend: function () {
            toastr["info"]('Activity is being saved. Please wait...');
        },
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#activity_add_form').trigger("reset");
                $('.custom-file-label').html('Choose attachment file');
                $('#activity_modal').modal('hide');

                if (stat.gcal_success != undefined && stat.gcal_success == false) {
                    toastr["error"]('Looks like their was some issue in syncronizing the event with Google Calendar');
                }

                self.initialize();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

activity_manager.prototype.edit_activity = function (activity_data) {
    var self = this;
    $('#activity_add_form').trigger("reset");
    if (activity_data.status == '1') {
        $('#status').attr('checked', 'checked');
    }
    var deal_link = base_url + '/admin/lead/add?deal_ref=' + activity_data.uuid;
    $('#deal_link').attr('href', deal_link);

    //Append Activity Data to inputs
    $('#activity_title').val(activity_data.activity_title);
    $('#activity_type').val(activity_data.activity_type);
    $('#activity_note').val(activity_data.activity_note);
    $('#activity_id').val(activity_data.id);
    $('#activity_file_value').val(activity_data.attachment);
    if (activity_data.attachment != '' && activity_data.attachment != null) {
        $('.custom-file-label').html(activity_data.attachment);
    } else {
        $('.custom-file-label').html('Choose attachment file');
    }

    if (activity_data.scheduled_date_calendar != '' && activity_data.scheduled_date_calendar != null) {
        $('#scheduled_date_value').val(moment(activity_data.scheduled_date_calendar).format('DD-MM-YYYY'));
    }
    if (activity_data.scheduled_time != '' && activity_data.scheduled_time != null) {
        $('#scheduled_time_value').timepicker('setTime', activity_data.scheduled_time);
    }
    if (activity_data.scheduled_duration != '' && activity_data.scheduled_duration != null) {
        var time = self.floatToTime(activity_data.scheduled_duration, true);
        $('#scheduled_duration_value').timepicker('setTime', time);
    }

    //Append Location data to customer location default if activity location data is null
    if (activity_data.address != 'null' && activity_data.address != '') {
        $('#activityLocationPostCode').val(activity_data.customer_postcode);
        $('#activityLocationState').val(activity_data.customer_state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.customer_address);
        $('#activityLocationAddressVal').val(activity_data.customer_address);
    } else {
        $('#activityLocationPostCode').val(activity_data.postcode);
        $('#activityLocationState').val(activity_data.state_id).trigger('change');
        $('#select2-activityAddress-container').html(activity_data.address);
        $('#activityLocationAddressVal').val(activity_data.address);
    }

    //Get GCAL EVENT DATA
    var gcal_event_data = JSON.parse(activity_data.gcal_event_data);

    //If gcal_event_data empty or null open and dont proceed furthur
    if (gcal_event_data === '' || gcal_event_data === null) {
        //Set Send Updates to false
        document.getElementById("sendUpdates").checked = false;
        document.getElementById("event_reminders_duration").value = '';
        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2();

        $('#activity_modal').modal('show');
        return false;
    }

    //Append Attendees if any
    if (gcal_event_data.event_data.attendees !== undefined) {
        var attendees = [];
        if (gcal_event_data.event_data.attendees.length > 0) {
            for (var i = 0; i < gcal_event_data.event_data.attendees.length; i++) {
                var obj = {};
                obj.id = gcal_event_data.event_data.attendees[i].email;
                obj.text = gcal_event_data.event_data.attendees[i].email;
                obj.selected = true;
                attendees.push(obj);
            }
        }

        //Destroy and empty select2 and create again
        $(".attendees").select2('destroy').empty();
        self.create_attendees_select2(attendees);
    }

    //Check SendUpates if enabled
    if (gcal_event_data.event_optional_data.sendUpdates !== undefined && gcal_event_data.event_optional_data.sendUpdates == 'all') {
        document.getElementById("sendUpdates").checked = true;
    }

    //Check if Reminder Time set
    if (gcal_event_data.event_data.reminders !== undefined && gcal_event_data.event_data.reminders.overrides !== undefined) {
        document.getElementById("event_reminders_duration").value = gcal_event_data.event_data.reminders.overrides[0].minutes;
    }


    $('#activity_modal').modal('show');
};

activity_manager.prototype.edit_activity_proposal = function () {
    var self = this;
    $(document).on('click', '.activity_edit', function () {
        if (self.user_type != '1' && self.user_type != '4') {
            toastr["error"]('Users should not be able to edit activities (only admins)');
            return false;
        }
        var $this = $(this);
        var activity_data = JSON.parse($this.attr('data-item'));
        $('#activity_add_form').trigger("reset");
        $('#activity_type').val(activity_data.activity_type);
        $('#activity_note').val(activity_data.note);
        $('#activity_id').val(activity_data.id);
        $('#is_scheduled').val(0);
        $('.datepicker').hide();
        $('#scheduler').addClass('hidden');
        $('#activity_file_value').val('');
        $('.custom-file-label').html("Choose attachment file");
        self.activity_action = "Added Activity";
        if (activity_data.attachment != '' && activity_data.attachment != null) {
            $('#activity_file_value').val(activity_data.attachment);
            $('.custom-file-label').html(activity_data.attachment);

        }
        if (activity_data.is_scheduled == 1) {
            $('#is_scheduled').val(1);
            $('#scheduler').removeClass('hidden');
            $('.datepicker').datepicker("setDate", activity_data.scheduled_at);
            $('.datepicker').show();
            self.activity_action = "Scheduled Activity";
        }
        $('#activity_modal').modal('show');
    });
};

activity_manager.prototype.upload_activity_file = function () {
    var self = this;
    $('#activity_file').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'activity_files');
        toastr["info"]("Uploading file please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_activity_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                toastr["info"]('File is being uploaded. Please wait...');
            },
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="activity[attachment]"').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

activity_manager.prototype.delete_activity = function () {
    var self = this;
    $(document).on('click', '.activity_delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/activity/delete',
            datatype: 'json',
            data: 'id=' + id + '&action=delete_activity',
            beforeSend: function () {
                toastr["info"]('Activity deletion is in progress. Please wait...');
            },
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $('.card_' + id).remove();
                    if (stat.gcal_success != undefined && stat.gcal_success == false) {
                        toastr["error"]('Looks like their was some issue in deleting the event with Google Calendar, please try to delete it manually.');
                    }
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};
<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Photo of main cable coming to MSB either above or below with rating / size if possible</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="additional_photos_3" />
                                        <input type="hidden" id="additional_photos_3" name="booking_form_image[additional_photos_3]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Close Up Meter Photo</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="additional_photos_4" />
                                        <input type="hidden" id="additional_photos_4" name="booking_form_image[additional_photos_4]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/solar_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
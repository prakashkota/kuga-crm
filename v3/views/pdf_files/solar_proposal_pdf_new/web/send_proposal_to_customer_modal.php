<div id="send_proposal_to_customer_modal" class="modal " role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-800">
                <h5 class="modal-title">Send To Customer</h5>
                <button type="button" class="close closemediapop" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <form id="send_proposal_to_customer_form">

                    <div class="form-group control-group">
                        <label>To</label>
                        <input class="form-control" type="email" name="email_to" value="<?php echo $proposal_data['customer_email']; ?>" id="email_to">
                    </div>
                    <!--<div class="form-group control-group">-->
                    <!--    <label>CC Emails (Please enter comma seperated email addresses)</label>-->
                    <!--    <input class="form-control" type="email" name="cc_emails" value="">-->
                    <!--</div>-->
                    <div class="form-group control-group">
                        <label>Subject</label>
                        <input class="form-control" type="text" name="email_subject" value="Here's your <?php echo $proposal_data['total_system_size']; ?>kW Proposal for <?php echo $proposal_data['customer_site_address']; ?>" id="email_subject">
                    </div>
                    <div class="form-group control-group">
                        <label>Mobile</label>
                        <input class="form-control" type="text" name="mobile" value="<?php echo $proposal_data['customer_contact_no']; ?>" id="mobile">
                    </div>

                    <div class="form-group control-group">
                        <label>Message</label>
                        <textarea rows="10" class="form-control" id="invoice_mail_body" name="email_body" aria-hidden="true">
                            Hi <?php echo $proposal_data['first_name']; ?>,<br/><br/>
                            
                            Here's your <?php echo $proposal_data['total_system_size']; ?>kW Proposal for <?php echo $proposal_data['customer_site_address']; ?>.<br/><br/>

                            {CLICK_HERE} to access the proposal, Access code is sent to {MOBILE_NUMBER}. <br/><br/>
                            {FORWORD_COLLEAGUE} <br/><br/>
                            
                            Best Regards, <br/> 
                            <?php echo $lead_data['full_name']; ?> <br/>
                            
                            <img width="150" src="<?php echo site_url() . 'assets/images/Kuga-Logo-C-01.png'; ?>" alt="Logo"  /> <br/>
                            <br/><br/>

                            <strong style="color:#D52323">M:</strong> <?php echo $lead_data['company_contact_no']; ?> <br/>
                            <strong style="color:#D52323">P:</strong> 13 KUGA (13 58 42) <br/>
                            <strong style="color:#D52323">W:</strong> <a href="https://kugacrm.com.au/">www.kugacrm.com.au</a> <br/>
                            <strong style="color:#D52323">A:</strong> 4 Bridge Road, Keysborough, VIC <br/> 6 Turbo Road, Kings Park, NSW <br/> 31 Chetwynd Street, Loganholme, QLD <br/>110 -112 Grand Junction Road, Blair Athol, SA <br/>
                        </textarea>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="send_proposal_to_customer_send_btn">Send</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

<!-- {ACCESS_CODE} -->
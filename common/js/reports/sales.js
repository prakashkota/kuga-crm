var sales_reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.start_date = '';
    this.end_date = '';
    this.user_selections = [];
    this.user_selection_init = false;
    this.localStorage = window.localStorage;

    $(function () {

        var start = (self.localStorage.getItem('start_date')) ? moment(self.localStorage.getItem('start_date')) : moment().subtract(29, 'days');
        var end = (self.localStorage.getItem('end_date')) ? moment(self.localStorage.getItem('end_date')) : moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('start_date').value = start.format('YYYY-MM-DD');
            document.getElementById('end_date').value = end.format('YYYY-MM-DD');
            self.start_date = start.format('d/M/Y');
            self.end_date = end.format('d/M/Y');
            self.localStorage.setItem('start_date', start);
            self.localStorage.setItem('end_date', end);
            self.initialize();
        }
        
        var currentDate = moment();
        var weekStart = currentDate.clone().startOf('isoWeek');
        var weekEnd = currentDate.clone().endOf('isoWeek');
        
        var thisWeek = [];
        var lastWeek = [];
        thisWeek.push(moment(weekStart).add(0, 'days'));
        thisWeek.push(moment(weekStart).add(6, 'days'));
        lastWeek.push(moment(weekStart).add(-7, 'days'));
        lastWeek.push(moment(weekStart).add(-1, 'days'));
        
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All': [moment('2016-01-01'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': thisWeek,
                'Last Week': lastWeek,
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        
        
        $('[data-toggle="tooltip"]').tooltip();

        $('.sr-show_inactive_users').click(function () {
            $('.sr-inactive_user').addClass('hidden');
            if ($(this).prop("checked") == true) {
                $('.sr-inactive_user').removeClass('hidden');
            }
        });

        if(self.localStorage.getItem('state_id')){
            $('#state_id').val(self.localStorage.getItem('state_id'));
        }

        $('#state_id').on('change', function () {
            self.localStorage.setItem('state_id', $(this).val());
            self.initialize();
        });


        $('#userid').multiselect({
            onChange: function (option, checked) {
                var elementId = option[0].value;
                if (checked) {
                    if(self.user_selections.indexOf(elementId) == -1){
                        self.user_selections.push(elementId);
                    }
                } else {
                    var index = self.user_selections.indexOf(elementId);
                    if (index > -1) {
                        self.user_selections.splice(index, 1);
                    }
                }
                if(self.user_selection_init){
                    self.localStorage.setItem('user_selections', JSON.stringify(self.user_selections));
                    self.initialize();
                }
            }
        });


        var localStorage_UserSelections = self.localStorage.getItem('user_selections');
        localStorage_UserSelections = (localStorage_UserSelections != '' && localStorage_UserSelections != null) ? JSON.parse(localStorage_UserSelections) : [];

        var checkboxes = $('ul.multiselect-container li input[type=checkbox]');
        checkboxes.each(function (index,element) {
            if (localStorage_UserSelections != '' && localStorage_UserSelections.length > 0) {
                if(localStorage_UserSelections.includes($(this).val()) != false){
                 $(this).prop('checked', true).trigger('change');
             }
             if(index == ((checkboxes.length) - 1)){
                self.user_selection_init = true;
            }
        }
        });

        if (localStorage_UserSelections || localStorage_UserSelections.length == 0) {
            self.user_selection_init = true;
        }

        cb(start, end);

    });
    
    $(document).on('click','.view_won_leads',function(){
        var user_id = $(this).attr('data-user_id');
        var form_data = '?type=won_leads'+ '&' + $('#reporting_actions').serialize() + '&user_id='+user_id;
        window.open(base_url + 'admin/reports'+form_data,"_blank");
        return false;
    });

};

sales_reporting.prototype.initialize = function () {
    var self = this;
    self.fetch_sales_rep_leads_performance_data();
    self.fetch_sales_rep_performance_data();
    self.fetch_sales_rep_closable_leads_data();
    self.fetch_sales_rep_lead_stage_data();
    self.fetch_sales_rep_activity_data();
}

sales_reporting.prototype.fetch_sales_rep_performance_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_performance_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_leads_performance_report_table_body').innerHTML = '';
            document.getElementById('sales_statistics_chart_container').innerHTML = '';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            document.getElementById('sales_statistics_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_performance_report_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_leads_performance_report_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_statistics_chart_container').innerHTML = '';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = '';
            if (data.sales_stats.length > 0) {
                google.charts.load('current', {'packages': ['corechart', 'bar']});
                google.charts.setOnLoadCallback(function () {
                    self.create_sales_statistics_chart(data.sales_stats);
                });

                var total_leads = 0;
                var total_led_proposals = 0;
                var total_solar_proposals = 0;
                var total_proposals = 0;
                var total_won_leads = 0;
                var total_kws = 0;
                var total_conv_rate = 0;
                var total_highbays = 0;
                var total_flood_light = 0;
                var total_panel_light = 0;
                var total_batten_light = 0;

                for (var i = 0; i < data.sales_stats.length; i++) {
                    var row = self.create_sales_performance_report_data(data.sales_stats[i]);
                    document.getElementById('sales_rep_performance_report_table_body').appendChild(row);


                    var led_proposals = data.sales_stats[i].total_led_proposal;
                    var solar_proposals = data.sales_stats[i].total_solar_proposal;
                    var proposals_count = parseInt(led_proposals) + parseInt(solar_proposals);
                    var leads_count = data.sales_stats[i].total_leads;
                    var won_leads_count = data.sales_stats[i].total_won_leads;
                    var kws_count = data.sales_stats[i].total_kws;
                    var highbay_count = data.sales_stats[i].total_highbays;
                    var flood_light_count = data.sales_stats[i].total_flood_light;
                    var panel_light_count = data.sales_stats[i].total_panel_light;
                    var batten_light_count = data.sales_stats[i].total_batten_light;
                    var conv_rate = 0;

                    if (leads_count != 0 && won_leads_count != 0) {
                        conv_rate = (won_leads_count / leads_count) * 100;
                        conv_rate = conv_rate.toFixed(2);
                        total_conv_rate = parseFloat(total_conv_rate) + parseFloat(conv_rate);
                        total_conv_rate = parseFloat(total_conv_rate).toFixed(2);
                    }

                    total_leads = parseInt(total_leads) + parseInt(leads_count);
                    total_led_proposals = parseInt(total_led_proposals) + parseInt(led_proposals);
                    total_solar_proposals = parseInt(total_solar_proposals) + parseInt(solar_proposals);
                    total_proposals = parseInt(total_proposals) + parseInt(proposals_count);
                    total_won_leads = parseInt(total_won_leads) + parseInt(won_leads_count);
                    total_kws = parseFloat(total_kws) + parseFloat(kws_count);
                    total_kws = parseFloat(total_kws).toFixed(2);
                    total_highbays = parseInt(total_highbays) + parseInt(highbay_count);
                    total_flood_light = parseInt(total_flood_light) + parseInt(flood_light_count);
                    total_panel_light = parseInt(total_panel_light) + parseInt(panel_light_count);
                    total_batten_light = parseInt(total_batten_light) + parseInt(batten_light_count);

                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var td5 = document.createElement('td');
                var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                var td9 = document.createElement('td');
                var td10 = document.createElement('td');
                var td11 = document.createElement('td');
                //var td3 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '2');
                tr_total.appendChild(td1);
                td2.innerHTML = total_proposals;
                td3.innerHTML = total_led_proposals;
                td4.innerHTML = total_solar_proposals;
                var avg_conv = parseFloat(total_won_leads / total_leads) * 100;
                avg_conv = avg_conv.toFixed(2);
                avg_conv = (isNaN(avg_conv)) ? '0.00' : avg_conv;
                td5.innerHTML = avg_conv + '%';
                td6.innerHTML = total_won_leads;
                td7.innerHTML = total_kws + ' kW';
                td8.innerHTML = total_highbays;
                td9.innerHTML = total_flood_light;
                td10.innerHTML = total_panel_light;
                td11.innerHTML = total_batten_light;
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                tr_total.appendChild(td5);
                tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                tr_total.appendChild(td9);
                tr_total.appendChild(td10);
                tr_total.appendChild(td11);
                document.getElementById('sales_rep_performance_report_table_body').appendChild(tr_total);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_statistics_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

sales_reporting.prototype.create_sales_statistics_chart = function (stat_data) {
    var total_stats = [];
    total_stats.push(['Sales Rep', 'kW Sold', 'Highbay Sold']);
    for (var i = 0; i < stat_data.length; i++) {
        total_stats.push([stat_data[i].full_name, parseFloat(stat_data[i].total_kws), parseFloat(stat_data[i].total_highbays)]);
    }
    var data = google.visualization.arrayToDataTable(total_stats);
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        vAxis: {format:'#', gridlines: {count: 2},}
    };

    var chart = new google.charts.Bar(document.getElementById('sales_statistics_chart_container'));
    chart.draw(data, google.charts.Bar.convertOptions(options));

};

sales_reporting.prototype.create_sales_performance_report_data = function (data) {

    var led_proposals = data.total_led_proposal;
    var solar_proposals = data.total_solar_proposal;
    var total_proposals = parseInt(led_proposals) + parseInt(solar_proposals);
    var total_leads = data.total_leads;
    var total_won_leads = data.total_won_leads;
    var conv_rate = 0;
    if (total_leads != 0 && total_won_leads != 0) {
        var conv = (total_won_leads / total_leads) * 100;
        conv = conv.toFixed(2);
        conv_rate = conv + '%';
    }

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.full_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state;

    var td3 = document.createElement('td');
    td3.innerHTML = total_proposals;

    var td4 = document.createElement('td');
    td4.innerHTML = led_proposals;

    var td5 = document.createElement('td');
    td5.innerHTML = solar_proposals;

    var td6 = document.createElement('td');
    td6.innerHTML = conv_rate;

    var td7 = document.createElement('td');
    td7.innerHTML = total_won_leads;

    var td8 = document.createElement('td');
    td8.innerHTML = data.total_kws + ' kW';

    var td9 = document.createElement('td');
    td9.innerHTML = data.total_highbays;

    var td10 = document.createElement('td');
    td10.innerHTML = data.total_flood_light;

    var td11 = document.createElement('td');
    td11.innerHTML = data.total_panel_light;

    var td12 = document.createElement('td');
    td12.innerHTML = data.total_batten_light;


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);
    tr.appendChild(td11);
    tr.appendChild(td12);

    return tr;
};

/** Closable Lead Data Functions */

sales_reporting.prototype.create_closeable_leads_statistics_chart = function (stat_data) {
    var total_stats = [];
    total_stats.push(['Sales Rep', 'kW Sold', 'Highbay Sold']);
    for (var i = 0; i < stat_data.length; i++) {
        total_stats.push([stat_data[i].full_name, parseFloat(stat_data[i].total_kws), parseFloat(stat_data[i].total_highbays)]);
    }
    var data = google.visualization.arrayToDataTable(total_stats);
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        vAxis: {format:'#', gridlines: {count: 2},}
        
    };
    var chart = new google.charts.Bar(document.getElementById('sales_rep_closable_lead_chart_container'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
};

sales_reporting.prototype.create_closeable_leads_table_row = function (data) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.full_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state;

    var td3 = document.createElement('td');
    td3.innerHTML = data.total_leads;

    var td4 = document.createElement('td');
    td4.innerHTML = data.total_led_leads;

    var td5 = document.createElement('td');
    td5.innerHTML = data.total_solar_leads;

    //var td6 = document.createElement('td');
    //td6.innerHTML = conv_rate;

    //var td7 = document.createElement('td');
    //td7.innerHTML = total_won_leads;

    var td8 = document.createElement('td');
    td8.innerHTML = data.total_kws + ' kW';

    var td9 = document.createElement('td');
    td9.innerHTML = data.total_highbays;

    var td10 = document.createElement('td');
    td10.innerHTML = data.total_flood_light;

    var td11 = document.createElement('td');
    td11.innerHTML = data.total_panel_light;

    var td12 = document.createElement('td');
    td12.innerHTML = data.total_batten_light;


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    //tr.appendChild(td6);
    //tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);
    tr.appendChild(td11);
    tr.appendChild(td12);

    return tr;
};


sales_reporting.prototype.fetch_sales_rep_closable_leads_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_closable_leads_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_closable_lead_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = '';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = '';
            if (data.sales_stats.length > 0) {
                google.charts.load('current', {'packages': ['corechart', 'bar']});
                google.charts.setOnLoadCallback(function () {
                    self.create_closeable_leads_statistics_chart(data.sales_stats);
                });
                var columns1 = ['Sales Rep', 'State' , 'Total Deals', 'Solar Deals', 'LED Deals', 'kW', 'Highbays', 'Floodlights', 'Panel Lights', 'Batten Lights' ];
                var lead_stage_table = self.create_sales_rep_table('sales_rep_closable_lead_table', columns1);
                var total_led_leads = 0;
                var total_solar_leads = 0;
                var total_proposals = 0;
                var total_leads = 0;
                var total_kws = 0;
                var total_highbays = 0;
                var total_flood_light = 0;
                var total_panel_light = 0;
                var total_batten_light = 0;
                
                var tbody = document.createElement('tbody');
                for (var i = 0; i < data.sales_stats.length; i++) {
                    var row = self.create_closeable_leads_table_row(data.sales_stats[i]);
                    tbody.appendChild(row);

                    var led_leads = data.sales_stats[i].total_led_leads;
                    var solar_leads = data.sales_stats[i].total_solar_leads;
                    var leads_count = data.sales_stats[i].total_leads;
                    var kws_count = data.sales_stats[i].total_kws;
                    var highbay_count = data.sales_stats[i].total_highbays;
                    var flood_light_count = data.sales_stats[i].total_flood_light;
                    var panel_light_count = data.sales_stats[i].total_panel_light;
                    var batten_light_count = data.sales_stats[i].total_batten_light;

                    total_led_leads = parseInt(total_led_leads) + parseInt(led_leads);
                    total_solar_leads = parseInt(total_solar_leads) + parseInt(solar_leads);
                    //total_proposals = parseInt(total_proposals) + parseInt(proposals_count);
                    total_leads = parseInt(total_leads) + parseInt(leads_count);
                    total_kws = parseFloat(total_kws) + parseFloat(kws_count);
                    total_kws = parseFloat(total_kws).toFixed(2);
                    total_highbays = parseInt(total_highbays) + parseInt(highbay_count);
                    total_flood_light = parseInt(total_flood_light) + parseInt(flood_light_count);
                    total_panel_light = parseInt(total_panel_light) + parseInt(panel_light_count);
                    total_batten_light = parseInt(total_batten_light) + parseInt(batten_light_count);

                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                //var td5 = document.createElement('td');
                //var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                var td9 = document.createElement('td');
                var td10 = document.createElement('td');
                var td11 = document.createElement('td');
                //var td3 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '2');
                tr_total.appendChild(td1);
                td2.innerHTML = total_leads;
                td3.innerHTML = total_led_leads;
                td4.innerHTML = total_solar_leads;
                //td6.innerHTML = total_won_leads;
                td7.innerHTML = total_kws + ' kW';
                td8.innerHTML = total_highbays;
                td9.innerHTML = total_flood_light;
                td10.innerHTML = total_panel_light;
                td11.innerHTML = total_batten_light;
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                //tr_total.appendChild(td5);
                //tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                tr_total.appendChild(td9);
                tr_total.appendChild(td10);
                tr_total.appendChild(td11);
                tbody.appendChild(tr_total);
                lead_stage_table.appendChild(tbody);
                document.getElementById('sales_rep_closable_lead_table_container').appendChild(lead_stage_table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_closable_lead_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_closable_lead_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Lead Stage Data Functions */

sales_reporting.prototype.create_sales_rep_lead_stage_table_row = function (stages, data, lead_stage_table) {
    var tbody = document.createElement('tbody');
    var total_counts = [];
  
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);
        for (var key in stages) {
            if (stages[key].id != 11 && stages[key].id != 12) {
                var td = document.createElement('td');
                var stage = 'stage' + stages[key].order_no + '_count';
                if (total_counts[key]) {
                    total_counts[key] = parseInt(total_counts[key]) + parseInt(data[i][stage]);
                } else {
                    total_counts[key] = data[i][stage];
                }
                td.innerHTML = data[i][stage];
                tr.appendChild(td);
            }
        }
        tbody.appendChild(tr);
    }

    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.setAttribute('colspan', '2');
    td.innerHTML = '<b>Total</b>';
    tr.appendChild(td);
    for (var key in total_counts) {
        var td = document.createElement('td');
        td.innerHTML = total_counts[key];
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
    lead_stage_table.appendChild(tbody);
};

sales_reporting.prototype.create_sales_rep_nd_vs_cd_table_row = function (stages, data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_counts = [];
    var chart_data = [];
    chart_data.push(['Sales Rep', 'New Leads', 'New Closable Leads']);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);
        
        for (var key in stages) {
            if (stages[key].id == 3 || stages[key].id == 9) {
                var td = document.createElement('td');
                if(stages[key].id == 3){
                    var stage = 'stage' + (parseInt(key) + 1) + '_1_count';
                }else{
                    var stage = 'stage' + (parseInt(key) + 1) + '_count';
                }
                if (total_counts[key]) {
                    total_counts[key] = parseInt(total_counts[key]) + parseInt(data[i][stage]);
                } else {
                    total_counts[key] = data[i][stage];
                }
                td.innerHTML = data[i][stage];
                tr.appendChild(td);
            }
        }

        chart_data.push([data[i].full_name, parseInt(data[i].stage4_1_count), parseInt(data[i].stage9_count)]);
        tbody.appendChild(tr);
    }

    var tr = document.createElement('tr');
    var td = document.createElement('td');
    td.setAttribute('colspan', '2');
    td.innerHTML = '<b>Total</b>';
    tr.appendChild(td);
    for (var key in total_counts) {
        var td = document.createElement('td');
        td.innerHTML = total_counts[key];
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
    table.appendChild(tbody);
    
    
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            gridlines: {count: 2},
            format: '#'
        }
    };


    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_nd_vs_cd_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.create_sales_rep_completed_site_visits_table_row = function (stages, data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_visit_counts = [];
    var chart_data = [];
    var chart_data_items = ['Sales Rep'];
    for(var key in stages){
        chart_data_items.push(stages[key].stage_name);
    }
    chart_data.push(chart_data_items);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);

        var chart_data_item_counts = [data[i].full_name];

        for (var key in stages) {
            var td = document.createElement('td');
            var stage_visit = 'stage' + stages[key].order_no + '_visit_count';
            if (total_visit_counts[i]) {
                total_visit_counts[i] = parseInt(total_visit_counts[i]) + parseInt(data[i][stage_visit]);
            } else {
                total_visit_counts[i] = parseInt(data[i][stage_visit]);
            }
            td.innerHTML = data[i][stage_visit];
            tr.appendChild(td);
            chart_data_item_counts.push(data[i][stage_visit])
        }
        var td_total = document.createElement('td');
        td_total.innerHTML = total_visit_counts[i];
        tr.appendChild(td_total);
        
        tbody.appendChild(tr);
        chart_data.push(chart_data_item_counts);
    }

    table.appendChild(tbody);
    
    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            gridlines: {count: 2},
            format: '#'
        }
    };

    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_completed_site_visits_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.fetch_sales_rep_lead_stage_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_lead_stage_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            document.getElementById('sales_rep_lead_stage_data_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_nd_vs_cd_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_nd_vs_cd_table_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_completed_site_visits_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_completed_site_visits_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = '';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = '';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = '';
            if (data.sales_stats.length > 0) {
                var columns1 = ['Sales Rep', 'State'];
                var columns2 = ['Sales Rep', 'State'];
                var columns3 = ['Sales Rep', 'State'];
                var stages = data.lead_stages;
                for (var key in stages) {
                    if (stages[key].id != 11 && stages[key].id != 12) {
                        columns1.push(stages[key].stage_name);
                    }
                    //New Deals vs Closable Deals
                    if (stages[key].id == 3 || stages[key].id == 9) {
                        columns2.push(stages[key].stage_name);
                    }
                    columns3.push(stages[key].stage_name);
                }

                var lead_stage_table = self.create_sales_rep_table('sales_rep_lead_stage_data_table', columns1);
                self.create_sales_rep_lead_stage_table_row(stages, data.sales_stats, lead_stage_table);
                document.getElementById('sales_rep_lead_stage_data_container').appendChild(lead_stage_table);

                //New Deals vs Closable Deals
                var nd_vs_cd_table = self.create_sales_rep_table('sales_rep_nd_vs_cd_table', columns2);
                self.create_sales_rep_nd_vs_cd_table_row(stages, data.sales_stats, nd_vs_cd_table);
                document.getElementById('sales_rep_nd_vs_cd_table_container').appendChild(nd_vs_cd_table);

                //Completed Site Visits
                columns3.push('Total');
                var completed_stie_visits_table = self.create_sales_rep_table('sales_rep_completed_site_visits_table', columns3);
                self.create_sales_rep_completed_site_visits_table_row(stages, data.sales_stats, completed_stie_visits_table);
                document.getElementById('sales_rep_completed_site_visits_table_container').appendChild(completed_stie_visits_table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_lead_stage_data_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_nd_vs_cd_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_nd_vs_cd_table_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_completed_site_visits_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_completed_site_visits_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Activity Data Functions */

sales_reporting.prototype.create_sales_rep_activity_table_row = function (data, table) {
    var self = this;
    var min,max = 0;
    var tbody = document.createElement('tbody');
    var total_counts = [];
    var chart_data = [];
    chart_data.push(['Sales Rep', 'Site Visits', 'Phone Calls','Schedule Meeting','Emails', 'Notes']);
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        td2.innerHTML = data[i].state;
        tr.appendChild(td1);
        tr.appendChild(td2);

        var keys = ['visit_count', 'phone_count','meeting_count' ,'email_count', 'note_count'];
        for (var key in keys) {
            var td = document.createElement('td');
            var stage = keys[key];
            if (total_counts[i]) {
                total_counts[i] = parseInt(total_counts[i]) + parseInt(data[i][stage]);
            } else {
                total_counts[i] = parseInt(data[i][stage]);
            }
            td.innerHTML = data[i][stage];
            tr.appendChild(td);
        }
        var td_total = document.createElement('td');
        td_total.innerHTML = total_counts[i];
        tr.appendChild(td_total);
        
        tbody.appendChild(tr);
        chart_data.push([data[i].full_name, parseInt(data[i].visit_count), parseInt(data[i].phone_count),parseInt(data[i].meeting_count),parseInt(data[i].email_count), parseInt(data[i].note_count)]);
    }

    table.appendChild(tbody);

    var options = {
        height: 300,
        legend: {position: 'bottom', maxLines: 3, textStyle: {fontSize: 10}},
        bar: {groupWidth: '50%'},
        isStacked: true,
        bars: 'horizontal',
        hAxis: {
            minValue: min,
            maxValue: max,
            format: '#'
        }
    };
    
    google.charts.load('current', {'packages': ['corechart', 'bar']});
    google.charts.setOnLoadCallback(function () {
        self.create_sales_rep_bar_chart('sales_rep_activity_chart_container', chart_data , options);
    });
};

sales_reporting.prototype.fetch_sales_rep_activity_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_activity_data?" + form_data,
        type: 'get',
        dataType: "json",
        beforeSend:function(){
            document.getElementById('sales_rep_activity_chart_container').innerHTML = '';
            document.getElementById('sales_rep_activity_table_container').innerHTML = '';
            document.getElementById('sales_rep_activity_chart_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_activity_table_container').appendChild(createPlaceHolder(false));
            document.getElementById('sales_rep_activity_table_container').appendChild(createPlaceHolder(false));
        },
        success: function (data) {
            document.getElementById('sales_rep_activity_chart_container').innerHTML = '';
            document.getElementById('sales_rep_activity_table_container').innerHTML = '';
            if (data.activity_stats.length > 0) {
                var columns = ['Sales Rep', 'State', 'Site Visits', 'Phone Calls','Schedule Meeting','Emails', 'Notes', 'Total'];
                var table = self.create_sales_rep_table('sales_rep_activity_table', columns);
                self.create_sales_rep_activity_table_row(data.activity_stats, table);
                document.getElementById('sales_rep_activity_table_container').appendChild(table);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_rep_activity_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_activity_table_container').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

/** Common Functions */

sales_reporting.prototype.create_sales_rep_bar_chart = function (container, chart_data , options) {
    var data = google.visualization.arrayToDataTable(chart_data);
    var chart = new google.charts.Bar(document.getElementById(container));
    chart.draw(data, google.charts.Bar.convertOptions(options));

};

sales_reporting.prototype.create_sales_rep_table = function (id, columns) {
    var table = document.createElement('table');
    table.className = 'table';
    table.setAttribute('id', id);

    var table_head = document.createElement('thead');
    var table_head_row = document.createElement('tr');

    for (var i = 0; i < columns.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = columns[i];
        table_head_row.appendChild(th);
    }

    table_head.appendChild(table_head_row);
    table.appendChild(table_head);

    return table;
};

sales_reporting.prototype.format_number = function (number) {
    return Number(parseInt(number)).toLocaleString('en');
};

sales_reporting.prototype.fetch_sales_rep_leads_performance_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize() + '&report_type=lead_statistics';
    $.ajax({
        url: base_url + "admin/reports/fetch_sales_rep_performance_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('sales_rep_leads_performance_report_table_body').innerHTML = '';
            if (data.sales_stats.length > 0) {
                
                var total_prospect = 0;
                var total_leads = 0;
                var total_sg_leads = 0;
                var total_cc_leads = 0;
                var total_mic_leads = 0;
                var total_fb_leads = 0
                var total_web_leads = 0
                var total_op_leads = 0;
                var total_led_proposals = 0;
                var total_solar_proposals = 0;
                var total_proposals = 0;
                var total_won_leads = 0;
                var total_conv_rate = 0;

                for (var i = 0; i < data.sales_stats.length; i++) {
                    var row = self.create_sales_lead_performance_report_data(data.sales_stats[i]);
                    document.getElementById('sales_rep_leads_performance_report_table_body').appendChild(row);

                    var prospect_count = data.sales_stats[i].total_prospect_count;
                    var leads_count = data.sales_stats[i].total_leads;
                    var sg_leads = data.sales_stats[i].total_self_generated_leads_count;
                    var cc_count = data.sales_stats[i].total_leads_allocation_count;
                    var mic_leads = data.sales_stats[i].total_mitc_leads_count;
                    var fb_leads = data.sales_stats[i].total_fb_leads_count;
                    var web_leads = data.sales_stats[i].total_web_leads_count;
                    var op_leads = data.sales_stats[i].total_other_partner_leads_count;
                    var led_proposals = data.sales_stats[i].total_led_proposal;
                    var solar_proposals = data.sales_stats[i].total_solar_proposal;
                    var quick_quotes = data.sales_stats[i].total_quick_quote;
                    var proposals_count = parseInt(led_proposals) + parseInt(solar_proposals) + parseInt(quick_quotes);
                    var leads_count = data.sales_stats[i].total_leads;
                    var won_leads_count = data.sales_stats[i].total_won_leads;
                    
                    total_prospect = parseInt(total_prospect) + parseInt(prospect_count);
                    total_leads = parseInt(total_leads) + parseInt(leads_count);
                    total_sg_leads = parseInt(total_sg_leads) + parseInt(sg_leads);
                    total_cc_leads = parseInt(total_cc_leads) + parseInt(cc_count);
                    total_mic_leads = parseInt(total_mic_leads) + parseInt(mic_leads);
                    total_fb_leads = parseInt(total_fb_leads) + parseInt(fb_leads);
                    total_web_leads = parseInt(total_web_leads) + parseInt(web_leads);
                    total_op_leads = parseInt(total_op_leads) + parseInt(op_leads);
                    total_led_proposals = parseInt(total_led_proposals) + parseInt(led_proposals);
                    total_solar_proposals = parseInt(total_solar_proposals) + parseInt(solar_proposals);
                    total_proposals = parseInt(total_proposals) + parseInt(proposals_count);
                    total_won_leads = parseInt(total_won_leads) + parseInt(won_leads_count);

                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td_prospect = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var td5 = document.createElement('td');
                var td_fb = document.createElement('td');
                var td_web = document.createElement('td');
                var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                var td9 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '2');
                
                td_prospect.innerHTML = total_prospect;
                td2.innerHTML = total_leads;
                td3.innerHTML = total_sg_leads;
                td4.innerHTML = total_cc_leads;
                td5.innerHTML = total_mic_leads;
                td_fb.innerHTML = total_fb_leads;
                td_web.innerHTML = total_web_leads;
                td6.innerHTML = total_op_leads;
                td7.innerHTML = parseInt(total_led_proposals) + parseInt(total_solar_proposals);
                td8.innerHTML = '<a href="javascript:void(0);" class="view_won_leads" data-user_id="0" target="__blank">'+total_won_leads+'</a>';
                var avg_conv = parseFloat(total_won_leads / total_leads)  * 100;
                avg_conv = avg_conv.toFixed(2);
                avg_conv = (isNaN(avg_conv)) ? '0.00' : avg_conv;
                td9.innerHTML = avg_conv + '%';

                tr_total.appendChild(td1);
                tr_total.appendChild(td_prospect);
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                tr_total.appendChild(td5);
                tr_total.appendChild(td_fb);
                tr_total.appendChild(td_web);
                tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                tr_total.appendChild(td9);
                document.getElementById('sales_rep_leads_performance_report_table_body').appendChild(tr_total);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('sales_statistics_chart_container').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('sales_rep_performance_report_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};

sales_reporting.prototype.create_sales_lead_performance_report_data = function (data) {

    var led_proposals = data.total_led_proposal;
    var solar_proposals = data.total_solar_proposal;
    var quick_quotes = data.total_quick_quote;
    var total_proposals = parseInt(led_proposals) + parseInt(solar_proposals) + parseInt(quick_quotes);
    var total_leads = data.total_leads;
    var total_won_leads = data.total_won_leads;
    var conv_rate = 0;
    if (total_leads != 0 && total_won_leads != 0) {
        var conv = (total_won_leads / total_leads) * 100;
        conv = conv.toFixed(2);
        conv_rate = conv + '%';
    }

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.full_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state;
    
    var td_prospect = document.createElement('td');
    td_prospect.innerHTML = data.total_prospect_count;
    
    var td3 = document.createElement('td');
    td3.innerHTML = total_leads;

    var td4 = document.createElement('td');
    td4.innerHTML = data.total_self_generated_leads_count;

    var td5 = document.createElement('td');
    td5.innerHTML = data.total_leads_allocation_count;

    var td6 = document.createElement('td');
    td6.innerHTML = data.total_mitc_leads_count;
    
    var td_fb = document.createElement('td');
    td_fb.innerHTML = data.total_fb_leads_count;
    
    var td_web = document.createElement('td');
    td_web.innerHTML = data.total_web_leads_count;

    var td7 = document.createElement('td');
    td7.innerHTML = data.total_other_partner_leads_count;

    var td8 = document.createElement('td');
    td8.innerHTML = parseInt(led_proposals) + parseInt(solar_proposals);

    var td9 = document.createElement('td');
    var url = base_url+'report/won-leads?ids='+data.won_leads_ids;
    td9.innerHTML = '<a href="javascript:void(0);" class="view_won_leads" data-user_id="'+data.user_id+'" target="__blank">'+total_won_leads+'</a>';

    var td10 = document.createElement('td');
    td10.innerHTML = conv_rate;


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td_prospect);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td_fb);
    tr.appendChild(td_web);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);

    return tr;
};

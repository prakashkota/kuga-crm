<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />

<style>
    
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }
    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }
    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
    .remove_field{
        color:red;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }
    .steps li {
        display: inline-block;
        margin: 0 5px 5px 0;
        max-width: 189px;
        width: 100%;
        text-align: center;
    }
    .steps li a, .steps li p {
        background: #231f20;
        padding: 4px 20px 4px;
        color: #ffffff;
        display: block;
        font-size: 13px;
        font-weight: bold;
        position: relative;
        border-radius: 0 10px 10px 10px;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li.active a, .steps li.active p {
        background: #b92625;
        color: #fff;
    }
    .steps li.undone a, .steps li.undone p {
        background: #d1d2d4;
        color: #ffffff;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }
    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }
    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }
    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }
    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }
    .sr-no_wrap{
        white-space: nowrap;
    }
    .sr-inactive_user{
        color:red;
    }
    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    .form-block{margin-bottom: 0px !important;}
    .card_row_1{
        min-height: 330px;
    }

    .kg-activity__item{
        border: 1px solid;
        border-radius: 20px;
    }
    .kg-activity__item--default{
        border-color: #f0ad4e !important;
        box-shadow: inset 4px 0 0 0 #fff !important;
        border-color: rgba(255, 0, 0, 0.5) !important;
        /**color:rgba(255, 0, 0, 0.5) !important;*/
    }
    .kg-activity__item--green{
        border-color: green !important;
        box-shadow: inset 0px 0 0 0 #008000 !important;
    }
    .kg-activity__item--amber{
        border-color: rgba(255, 191, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }
    .kg-activity__item--pink{
        border-color: rgba(255, 192, 203, 0.24) !important;
        background-color: rgba(255, 192, 203, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }
    .kg-activity__item--red{
        border-color: rgba(255, 0, 0, 1) !important;
        box-shadow: inset 0px 0 0 0 #ff0000 !important;
    }
    .kg-activity__item--blue{
        border-color: rgba(49, 122, 226, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }
    .kg-activity__item__title{
        display:inline-flex;
        margin-bottom: 10px;
        font-size: 16px;
        font-weight: 500;
        margin: 0;
        padding: 0 0 15px;
        line-height: 1.2;
        color: inherit;
        font-family: 'Poppins', sans-serif;
        position: relative;
        cursor:pointer;
    }

    .kg-activity__item__title--green{
        color:rgba(0,128,0, 0.5) !important;
    }


    .kg-activity__item__description{
        /**background-color:#ffffe0;*/
        margin: 0 -13px -13px -13px;
        padding: 12px;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
        background-color:#b92625;
        color: #ffffff;
    }
    
    .kg-activity__item__description p{
        color: #ffffff;
    }

    .kg-activity__item__action{
        float:right;
        margin: 0;
        padding: 0;
    }
    .kg-activity__item__action:after {
        content: '\2807';
        font-size: 1.5em;
        color: #2e2e2e;
    }

    .kg-activity__attachment_item{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }


    @media only screen and (max-width: 991px){
        .kg-btn_block{
            display:inline-grid;
        }
        .kg-btn_block a{
            margin:5px;
        }
    }

    @media only screen and (min-width: 991px){
        .btn{
           padding: 0 15px 0px 15px;
min-width: 160px;
margin-bottom: 1.25rem;
        }
    }

    @media only screen and (max-width:767px){
      .steps{margin:0 -5px;}
      .steps li, #deal_stages .steps li{max-width: calc(50% - 10px); margin: 0 5px 10px; display: inline-block;}
  }

  @media only screen and (max-width:479px){
      .steps li, #deal_stages .steps li{max-width: 100%;}
  }
  
  p{padding-bottom: 0px !important;}
  
  .btn-group-outer{padding-bottom:0;}
  .btn-group-outer .kg-btn_block{display:flex; flex-wrap:wrap;}

</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <!--<a class="btn btn-outline-primary hidden" id="proceed_to_job_management" href="<?php echo site_url('admin/franchise/job/schedule') ?>"><i class="icon-arrow-right"></i> Proceed to Job Management </a> -->
                <a class="btn btn-outline-primary <?php if (isset($is_lead_allocation_sales_rep) &&  $is_lead_allocation_sales_rep != TRUE) { ?> hidden <?php } ?>" href="javascript:void(0);" id="set_appointment_btn"><i class="fa fa-calendar-alt"></i> Set Appointment </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-block d-block clearfix">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <h1 id="dd_company_name" style="margin:0px;">Business Name</h1>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <label>Deal Value:</label>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group" id="dd_deal_value">

                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>

                                        <?php $class = (isset($is_lead_allocation_sales_rep) && $is_lead_allocation_sales_rep != TRUE) ? 'col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4' : 'col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4'; ?>
                                        <div class="col-md-8">  
                                            <div class="row">
                                                <div class="<?php echo $class; ?>">
                                                    <div class="form-group" >
                                                        <label>
                                                            Sales Manager: 
                                                            <!--<label class="form-check-label ml-5">
                                                                <input type="checkbox" class="form-check-input sr-show_inactive_users" value="1">
                                                                <span style="font-size:10px;"> Inactive users </span>
                                                            </label> -->
                                                        </label>
                                                        <select name="lead[user_id]" id="user_id">
                                                            <?php foreach ($users as $key => $value) { ?>
                                                                <option class="<?php if ($value['banned'] == 1) { ?> hidden sr-inactive_user<?php } ?>" value="<?php echo $value['user_id']; ?>" <?php if ($user_id == $value['user_id']) { ?> selected="" <?php } ?>><?php echo $value['full_name']; ?> <?php echo (isset($value['group_id']) && $value['group_id'] == 5) ? '(Agent)' : ''; ?> </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="<?php echo $class; ?>">
                                                    <div class="form-group">
                                                        <label>Call Center:</label>
                                                        <select id="la_user_id" disabled="">
                                                            <option value="">None</option>
                                                            <?php
                                                            if(isset($lead_allocation_users)){
                                                                foreach ($lead_allocation_users as $key => $value) {
                                                                    ?>
                                                                    <option class="<?php if ($value['banned'] == 1) { ?> hidden<?php } ?>" value="<?php echo $value['user_id']; ?>" <?php if ($user_id == $value['user_id']) { ?> selected="" <?php } ?>><?php echo $value['full_name']; ?>  </option>
                                                                <?php }} ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="<?php echo $class; ?>">
                                                        <div class="form-group">
                                                            <label>Lead Source:</label>
                                                            <select name="lead[lead_source]" class="" id="lead_source">
                                                                <?php
                                                                $lead_source = unserialize(lead_source);
                                                                foreach ($lead_source as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="<?php echo $class; ?>">
                                                        <div class="form-group">
                                                            <label>Type:</label>
                                                            <select name="lead[lead_type]" class="" id="lead_type">
                                                                <?php
                                                                $lead_type = unserialize(LEAD_TYPE);
                                                                foreach ($lead_type as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="<?php echo $class; ?>">
                                                        <div class="form-group">
                                                            <label>Segment:</label>
                                                            <select name="lead[lead_segment]" class="" id="lead_segment">
                                                                <?php
                                                                $lead_segment = unserialize(LEAD_SEGMENT);
                                                                foreach ($lead_segment as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="<?php echo $class; ?>">
                                                        <div class="form-group">
                                                            <label>Industry:</label>
                                                            <select name="lead[lead_industry]" class="" id="lead_industry">
                                                                <?php
                                                                $lead_industry = unserialize(LEAD_INDSUTRY);
                                                                foreach ($lead_industry as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        <!--<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 hidden" id="lead_attachments">
                                            <div class="form-block d-block clearfix">
                                                <div class="dropzone" id="lead_dropzone">
                                                    <div class="dz-message needsclick">
                                                        Drop files here or click to upload.<br>
                                                    </div>
                                                </div>
                                                <div class="btn-block mt-2">
                                                    <input type="button" id="save_meter_data_btn" onclick="save_solar_proposal();" value="Save" class="btn btn-primary hidden">
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12">
                                            <div id="deal_stages">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>






                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Customer Details</h2>
                                            <a style="margin-left:20px;" id="add_new_customer_btn" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                            <a class="hidden" style="margin-left:20px;" id="edit_customer_btn" href="javascript:void(0);" ><i class="fa fa-pencil"></i> Edit Customer</a>
                                        </div>

                                        <div class="form-block d-block clearfix">
                                            <input type="hidden"  id="cust_id">
                                            <div class="form-group">
                                                <input type="text"  id="company_name" placeholder="Business Name" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group">
                                                <input type="text"  id="customer_name" placeholder="Contact Name" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" id="customer_position" placeholder="Position" class="form-control gray-bg" readonly>
                                            </div>
                                            <div class="form-group" >
                                                <input type="tel" style="cursor:pointer;" id="customer_phone" placeholder="Phone Number" class="form-control gray-bg" readonly>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" id="customer_email" placeholder="Email" class="form-control gray-bg" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-8">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Additional Contacts</h2>
                                            <a style="margin-left:20px;" class="add_new_customer_contact_btn" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="table-responsive"  >
                                                <table width="100%" border="0" class="table table-striped" id="customer_contact_table" >
                                                    <thead>
                                                        <tr>
                                                            <th>Contact Name</th>
                                                            <th>Position</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="customer_contact_table_body">

                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- LED SIZE BLOCK START
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Lead Size</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="row">
                                                <label class="col-sm-1 col-form-label">Solar</label>
                                                <div class="col-sm-11">
                                                    <div class="row">
                                                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <input type="number" class="form-control"  ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Size" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><b>kW</b></span>
                                                                    </div>
                                                                </div>
                                                                <small class="form-text text-muted" id="no_of_panels"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" class="form-control"    ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Value" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-sm-1 col-form-label">LED</label>
                                                <div class="col-sm-11">
                                                    <div class="row">
                                                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" class="form-control"    ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Value" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-block d-block clearfix">
                                                        <h2>Additional Cost Items</h2>
                                                        <div class="table-default">
                                                            <table class="table table-striped" width="100%" border="0">
                                                                <thead>
                                                                    <tr>
                                                                        <td>Type</td>
                                                                        <td>Product</td>
                                                                        <td>Price</td>
                                                                        <td>Qty</td>
                                                                        <td>Custom Price</td>
                                                                        <td>Total</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="additional_cost_items_wrapper">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
                                                    <div class="form-block d-block clearfix">
                                                        <h2>Add Additional Costs Items</h2>
                                                        <table width="100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <select id="additional_product_type" class="form-control"  required>
                                                                            <option value="">Select Product Type </option>
                                          </select>
                                                                    </td>
                                                                    <td>
                                                                        <select id="additional_product" class="form-control"  required>
                                                                            <option value="" >Please Select Type First</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0);" id="add_additional_cost_items" style="margin-left:10px;"><i class="fa fa-plus"></i></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                LED SIZE BLOCK END -->


                <!-- PROPOSAL TOTALS BLOCK START -->
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Proposal Totals</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="row">
                                                <label class="col-sm-2 col-md-1 col-form-label">Solar</label>
                                                <div class="col-sm-10 col-md-11">
                                                    <div class="row">
                                                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input readonly="" id="total_system_value" type="number" class="form-control"    ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Value" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <input id="total_system_size" type="number" class="form-control"  ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Size" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><b>kW</b></span>
                                                                    </div>
                                                                </div>
                                                                <small class="form-text text-muted" id="no_of_panels"></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <label class="col-sm-2 col-md-1 col-form-label">Battery</label>
                                                <div class="col-sm-10 col-md-11">
                                                    <div class="row">
                                                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <input id="total_battery_size" type="number" class="form-control"  ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="Battery Size" />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><b>kW</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-sm-2 col-md-1 col-form-label">LED</label>
                                                <div class="col-sm-10 col-md-11">
                                                    <div class="row">
                                                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                                            <div class="form-group">
                                                                <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input readonly="" id="total_led" type="number" class="form-control"  placeholder="System Value" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-row">
                                                        <?php
                                                        foreach ($product_types as $key => $value) {
                                                            $id = strtolower($value['type_name']);
                                                            $id = 'total_' . str_replace(' ', '_', $id);
                                                            ?>
                                                            <div class="col-sm-2 col-md-2 mb-3">
                                                                <label for="<?php echo $id; ?>"><?php echo $value['type_name']; ?></label>
                                                                <input type="number" class="form-control item_count" id="<?php echo $id; ?>" placeholder="<?php echo $value['type_name']; ?>" oninput="this.value = Math.abs(this.value)">
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--<div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <div class="form-block d-block clearfix">
                                                        <h2>Additional Cost Items</h2>
                                                        <div class="table-default">
                                                            <table class="table table-striped" width="100%" border="0">
                                                                <thead>
                                                                    <tr>
                                                                        <td>Type</td>
                                                                        <td>Product</td>
                                                                        <td>Price</td>
                                                                        <td>Qty</td>
                                                                        <td>Custom Price</td>
                                                                        <td>Total</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="additional_cost_items_wrapper">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PROPOSAL TOTALS BLOCK END -->

                <!-- CUSTOMER SITE BLOCK START -->
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="kg-activity__item__title">
                                            <h2>Customer Locations</h2>
                                            <a style="margin-left:20px;" class="add_new_customer_location_btn" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="table-responsive"  >
                                                <table width="100%" border="0" class="table table-striped" id="customer_location_table" >
                                                    <thead>
                                                        <tr>
                                                            <th>Address</th>
                                                            <th class="text-center">LED Proposal</th>
                                                            <th class="text-center">Commercial Solar Proposal</th>
                                                            <th class="text-center">Residential Solar Proposal</th>
                                                            <th class="text-center">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="customer_location_table_body">

                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CUSTOMER SITE BLOCK END -->


                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h2>Attachments</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="lead_forms_data_table" >
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Address</th>
                                                    <th>Date Created</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lead_forms_data_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="pipedrive_files_section">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h2>Pipedrive Files  
                                        <!--<small>(<i class="fa fa-info-circle"></i> Pipedrive File Migration is still in progress some files might be seen later.)</small>-->
                                        <a href="javascript:void(0);" id="fetch_pipedrive_files" class="btn-primary btn-lg pull-right"><i class="fa fa-paperclip"></i> Files</a>
                                    </h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <div class="table-responsive"  id="pipedrive_files_container">
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               <?php /** <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h2>Quick Quotes</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="lead_quote_table" >
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Address</th>
                                                    <th>Quote #</th>
                                                    <th>Quote Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lead_quote_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h2>Booking Forms</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <div class="table-responsive"  >
                                        <table width="100%" border="0" class="table table-striped" id="lead_booking_form_table" >
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Address</th>
                                                    <th>Type</th>
                                                    <th>Booking Form Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lead_booking_form_table_body">

                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                */ ?>


                <!-- ACTIVITY BLOCK START -->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card">
                            <div class="card-body btn-group-outer">
                                <div class="btn-block kg-btn_block">
                                    <a href="javascript:void(0);" id="add_activity" class="btn mrg-r10">Add Activity</a>
                                    <a href="javascript:void(0);" id="schedule_activity" class="btn btn-primary btn-sm mrg-r10">Schedule Activity</a>
                                    <a href="javascript:void(0);" id="add_quick_quote" class="btn btn-primary btn-sm mrg-r10">Quick Quote</a>
                                    <a href="javascript:void(0);" id="add_solar_quick_quote" class="btn btn-primary btn-sm mrg-r10">Solar Quote</a>
                                    <a href="javascript:void(0);" id="add_job_card" class="btn btn-primary btn-sm mrg-r10">Job Card</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card">
                            <div class="card-body btn-group-outer">
                                <div class="btn-block kg-btn_block">
                                    <a href="javascript:void(0);" id="save_proposal" class="btn mrg-r10 hidden">Proposal Request</a>
                                    <a href="javascript:void(0);" id="led_booking_form_btn" class="btn mrg-r10">LED Booking Form</a>
                                    <a href="javascript:void(0);" id="solar_booking_form_btn" class="btn mrg-r10">Commercial Solar Booking Form</a>
                                    <a href="javascript:void(0);" id="add_meter_data" class="btn btn-primary btn-sm mrg-r10">Request Meter Data</a>
                                    <a href="javascript:void(0);" id="residential_solar_booking_form_btn" class="btn mrg-r10">Residential Solar Booking Form</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 



                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12" id="all_activity_container">

                    </div>
                </div>

            </div>
        </div>
        <!-- ACTIVITY BLOCK START -->
    </div>
</div>



<script src='<?php echo site_url(); ?>assets/js/jquery-ui.min.js' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/lead.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/activity.js?v=<?php echo version; ?>' type='text/javascript'></script>

<script type='text/javascript'>

    var ac_context = {};
    ac_context.view = 'lead';
    ac_context.lead_data = '<?php echo (isset($lead_data) && !(empty($lead_data))) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    var activity_manager_tool = new activity_manager(ac_context);

    function closeModal(id) {
        $('#' + id).modal('hide');
    }

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#locationstreetAddress').placecomplete1({
            placeServiceResult1: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    
                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    

                    var address_components = data.address_components;
                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    console.log(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.getElementById("locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("locationState").appendChild(option);
                                    }
                                }
                               
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                                $('#locationPostCode').trigger('change');
                            }
                        }
                    }
                } else {
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'en'
        });

    });


    var context = {};
    context.user_type = '<?php echo isset($user_group) ? $user_group : ''; ?>';
    context.lead_stages = '<?php echo (isset($lead_stages) && !empty($lead_stages)) ? json_encode($lead_stages) : ''; ?>';
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    context.is_lead_allocation_sales_rep = '<?php echo $is_lead_allocation_sales_rep; ?>';
    var lead_manager = new lead_manager(context);
</script>
<?php if (isset($is_lead_allocation_sales_rep) && $is_lead_allocation_sales_rep == 1) { ?>
    <script src='<?php echo site_url(); ?>common/js/lead_allocation.js?v=<?php echo version; ?>' type='text/javascript'></script>
    <script>
        var context = {};
        context.user_type = '<?php echo isset($user_group) ? $user_group : ''; ?>';
        context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
        context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS ) : ''; ?>';
        var lead_allocation_manager = new lead_allocation_manager(context);
    </script>
<?php } ?>

</body>
</html>


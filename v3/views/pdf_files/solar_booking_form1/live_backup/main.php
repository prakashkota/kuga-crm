<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Solar Booking Form</title>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            </style>
        </head>
        <body style="padding:0; margin:0">
            <?php $this->load->view('pdf_files/solar_booking_form/page1'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page2'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page3'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page4'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page5'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page6'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page7'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page8'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page9'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page10'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form/page11'); ?>
        </body>
    </html>
<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page-4-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 1287px !important;
            max-height: 910px !important;
            width: 1287px !important;
            min-height: 510px !important;
        }

        .page-4 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 1044px;
            padding-top: 30px;
        }

        .page-4 .top hr {
            width: 76%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-4 .text_color {
            color: #D01E2A;
        }

        .page-4 .title {
            display: block;
            float: left;
            width: 100%;
        }

        .page-4 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            font-size: 2.8rem;
            /* border-left: 5px solid #D01E2A; */
            font-weight: 500;
            margin-top: 60px;
        }

        .page-4 .title h2 {
            padding-left: 14px;
            margin-left: 48px;
            border-right: none;
            font-weight: 500;
            margin-top: -22px;
            font-size: 2.6rem;
            margin-bottom: 60px;
        }


        .page-4 .footer {
            margin-top: -3px;
            display: inline-flex;
        }

        .page-4 .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-4 .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }

        .page-4 .ingBox {
            display: block;
            padding-top: 100px;
            padding-bottom: 138px;
        }

        .page-4 .iiimage {
            float: left;
            min-height: 500px;
            border: 1px solid #f4f4f4;
            width: 370px;
            margin-left: 0px;
            background: #f4f4f4;
        }

        .page-4 .iibox {
            float: left;
            min-height: 500px;
            border: 1px solid #f4f4f4;
            width: 903px;
            margin-left: 12px;
            background: #f4f4f4;
            box-sizing: border-box;
            padding: 40px 30px 10px 30px !important;
        }

        .page-4 .fimagee img {
            margin-left: 1120px;
        }


        .page-4 .page-4-button {
            display: inline-block;
            width: 50%;
            float: left;
        }

        .page-4 .page-4-button {
            display: block;
            width: 30%;
            float: left;
        }
        .page-4-button a.download{
            margin-top: 5px;
        }
        .page-4-links{
            margin-top: 20px;
            width: 60%;
            float: right;
        }
        .page-4-description{
            margin-top: 100px;
        }
    </style>
</head>

<?php
$inverter_design['snapshot'] = '';
$inverter_design['notes'] = '';
?>

<body>
    <div class="page-4-back-bg back-bg page-4">
        <div class="top">
            <!-- <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA
            </h2>
            <hr> -->
        </div>
        <div class="title">
            <h1><span class="text_color">YOUR BEST OPTION FOR THE INVERTER IS </span></h1>
            <h2>SUNPOWER SPR-P3-480-UPP 480W</h2>
        </div>
        <div class="ingBox">
            <div class="iiimage">
                <div class="add-picture1" data-src="<?php echo !empty($inverter_design) ? $inverter_design['snapshot'] : ''; ?>" style="background-image: url('<?php echo !empty($inverter_design) ? $inverter_design['snapshot'] : ''; ?>'); 
                           background-repeat: no-repeat; background-position: 50% 50%; 
                           background-size: 100% 100%; 
                           width: 650px;
                           height: 400px; ">
                    <img style="padding-left: 90px;" src="<?php echo site_url('assets/download_pack/solar_power_logo.PNG')?>"/><br/>
                    <img style="padding-left: 28px;" src="<?php echo site_url('assets/download_pack/solar_panel.PNG')?>"/>
                </div>
            </div>
            <div class="iibox" style="padding-right: 20px;
    padding-left: 20px;">
                <div class="page-4-description">
                    <p>We selected the Sunpower SPR-P3-480-UPP 480W for your project as it is engineered to meet the
                        unique needs of large-scale solar power plants. By exposing more active surface area across more
                        mono PERC cells, Performance 3 UPP panels optimize power density, while lowering system costs.</p>
                    <p>Engineered to stand up to environmental stresses such as shading, daily temperature swings and
                        high humidity, which is the typical condition in your area. The SunPower Performance 3 UPP is a
                        high power panel uniquely suited for power plant EPCs and developers looking to maximize energy
                        production.</p>
                    <p>it is also proven working best with the Sungrow inverter SG110CX.</p>
                </div>
                <div class="page-4-links">
                    <div class="page-4-button">
                        <strong>Warranty</strong>
                        <br>
                        <br>
                        <a class="download">DOWNLOAD</a>
                    </div>
                    <div class="page-4-button">
                        <strong>Data Sheet</strong>
                        <br>
                        <br>
                        <a class="download">DOWNLOAD</a>
                    </div>
                    <div class="page-4-button">
                        <strong>Product Video</strong>
                        <br>
                        <br>
                        <a class="download">watch</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
<script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!--<script type="text/javascript" src="https://unpkg.com/canvg@3.0.4/lib/umd.js"></script>-->
<style>
    .overlay-roadblock{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 10;
		background-color: rgba(0,0,0,0.5);
		display: none;
	}

	.overlay-roadblock__content{
		width: auto;
		height: 80px;
		position: fixed;
		top: 50%; 
		left: 50%;
		margin-top: -100px;
		margin-left: -150px;
		margin-right: 25px;
		background-color: #fff;
		border-radius: 5px;
		text-align: center;
		z-index: 11;
		word-break: break-all;
		padding:20px;
	}

	.loader {
	    border: 5px solid #f3f3f3;
	    -webkit-animation: spin 1s linear infinite;
	    animation: spin 1s linear infinite;
	    border-top: 5px solid #555;
	    border-radius: 50%;
	    width: 50px;
	    height: 50px;
	    margin: 0 auto;
	    text-align:center;
	}

	@keyframes spin {
        0% { 
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }
    
        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
	}
</style>
<form action="<?php echo site_url('admin/product/residential_solar/save_chartimg_to_png'); ?>" method="POST" id="chartForm">
    <input type="hidden"  id="pId" value="<?php echo $proposal['id']; ?>" />
    <input type="hidden"  id="uuid" value="<?php echo $proposal['uuid']; ?>" />
    <input type="hidden"  name="chart1" value="chart1_<?php echo $proposal['id']; ?>" />
    <input type="hidden" id="dataImg1" name="dataImg1" value="" />
</form>

<div id="charts">
    <div id="chart"></div>
</div>

<div class="overlay-roadblock">
    <div class="overlay-roadblock__content" id="overlay_content">
       
    </div>
</div>


<?php
if ($lead_type != 3 && !empty($savings_calculation_data)) {
    $annual_prod = (isset($saving_calculation_details['solar_annual_production'])  && $saving_calculation_details['solar_annual_production'] != '') ?  (float) $saving_calculation_details['solar_annual_production'] : 0;  
    $avg_prod = $annual_prod / 12;
    $avg_prod = round((float) $avg_prod);

    $usage_savings = (isset($saving_calculation_details['usage_savings']) && $saving_calculation_details['usage_savings'] != '') ?  round((float) $saving_calculation_details['usage_savings']): 0;

    $sc_export_credit = (isset($saving_calculation_details['export_credit']) && $saving_calculation_details['export_credit'] != '') ?  round((float) $saving_calculation_details['export_credit']): 0;

    $sc_total_savings = (isset($saving_calculation_details['total_savings']) && $saving_calculation_details['total_savings'] != '') ?  round((float) $saving_calculation_details['total_savings']): 0;

    $sc_annual_savings = (isset($saving_calculation_details['annual_savings']) &&  $saving_calculation_details['annual_savings'] != '') ?  round((float) $saving_calculation_details['annual_savings']): 0;

    $total_system_size = (isset($saving_calculation_details['total_system_size']) &&  $saving_calculation_details['total_system_size'] != '') ?  (float) $saving_calculation_details['total_system_size'] :  0;

    $kwh_day = (isset($saving_calculation_details['kwh_day']) && $saving_calculation_details['kwh_day'] != '') ?  (float) $saving_calculation_details['kwh_day'] :  0;

    $exportTotal = (isset($saving_calculation_details['exportTotal']) && $saving_calculation_details['exportTotal'] != '') ?  (float) $saving_calculation_details['exportTotal'] :  0;

    $TotalSavingsArr = (isset($saving_calculation_details['TotalSavingsArr'])  && $saving_calculation_details['TotalSavingsArr'] != '') ?  (float) $saving_calculation_details['TotalSavingsArr'] :  0;

    $feed_in_tariff = (isset($saving_calculation_details['feed_in_tariff'])  && $saving_calculation_details['feed_in_tariff'] != '') ?  number_format((float) $saving_calculation_details['feed_in_tariff'], 2, '.', '') :  0;

    $export = (isset($saving_calculation_details['export'])  && $saving_calculation_details['export'] != '') ?  $saving_calculation_details['export'] :  0;

    $savings_calculation_data['TotalSavingsArr'];
    $AverageDailyProduction = $total_system_size * $kwh_day;
    $AverageDailyProduction = round((float) $AverageDailyProduction);
    $exportTotal = number_format((float) $exportTotal, 2, '.', '');
    $exportTotal = round((float) $exportTotal);


    $electricity_rate = 0;
    if (isset($saving_calculation_details['electricity_rate'])) {
        $electricity_rate = $saving_calculation_details['electricity_rate'];
    } else if (isset($saving_calculation_details['flat_rate']) && $saving_calculation_details['flat_rate'] > 0) {
        $electricity_rate = $saving_calculation_details['flat_rate'];
    } else if (isset($saving_calculation_details['tr_rate'])) {
        $saving_calculation_details['tr_rate'] = (array) $saving_calculation_details['tr_rate'];
        $electricity_rate = array_sum($saving_calculation_details['tr_rate']);
    }
    $electricity_rate = number_format((float) $electricity_rate, 2, '.', '');

    $width = (strlen($AverageDailyProduction) > 1) ? 413 : 402;
    
    $near_map_data = (isset($proposal['near_map_data'])) ? $proposal['near_map_data'] : [];
    $tilts = [];
    if (!empty($near_map_data)) {
        $near_map_data = json_decode($near_map_data,true);
        if(!empty($near_map_data['tiles'])){
            $near_map_data['tiles']    = $near_map_data['tiles'];
            $no_of_panel_tiles  = count($near_map_data['tiles']);
            for ($i = 0; $i < $no_of_panel_tiles; $i++) {
                $tilt = is_numeric($near_map_data['tiles'][$i]['sp_tilt']) ? $near_map_data['tiles'][$i]['sp_tilt'] . '&#176;' : 0 . '&#176;';
                if(!in_array($tilt , $tilts)){
                    $tilts[] = $tilt;
                }
            }
        }
    }

    $tilt_text = '';
    if(!empty($tilts)){
        $tilt_text = "of " . implode('/',$tilts);
    }
    
    //As in Change on 04-08-2020 via skype feedback by anthony
    $estd_daily_otp = ($annual_prod > 0) ? $annual_prod/365 : 0;
    $estd_daily_otp = round((float) $estd_daily_otp);
    
    ?>
    <script type="text/javascript" src="https://www.google.com/jsapi" ></script>
    <script type='text/javascript'>
        google.load('visualization', '1', {packages: ['corechart']});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Year', 'kWh', 'Average'],
                ['JAN', <?php echo $savings_calculation_data['averageDailyKWhPerMonth1']; ?>,<?php echo $estd_daily_otp; ?>],
                ['FEB', <?php echo $savings_calculation_data['averageDailyKWhPerMonth2']; ?>,<?php echo $estd_daily_otp; ?>],
                ['MAR', <?php echo $savings_calculation_data['averageDailyKWhPerMonth3']; ?>,<?php echo $estd_daily_otp; ?>],
                ['APR', <?php echo $savings_calculation_data['averageDailyKWhPerMonth4']; ?>,<?php echo $estd_daily_otp; ?>],
                ['MAY', <?php echo $savings_calculation_data['averageDailyKWhPerMonth5']; ?>,<?php echo $estd_daily_otp; ?>],
                ['JUN', <?php echo $savings_calculation_data['averageDailyKWhPerMonth6']; ?>,<?php echo $estd_daily_otp; ?>],
                ['JUL', <?php echo $savings_calculation_data['averageDailyKWhPerMonth7']; ?>,<?php echo $estd_daily_otp; ?>],
                ['AUG', <?php echo $savings_calculation_data['averageDailyKWhPerMonth8']; ?>,<?php echo $estd_daily_otp; ?>],
                ['SEP', <?php echo $savings_calculation_data['averageDailyKWhPerMonth9']; ?>,<?php echo $estd_daily_otp; ?>],
                ['OCT', <?php echo $savings_calculation_data['averageDailyKWhPerMonth10']; ?>,<?php echo $estd_daily_otp; ?>],
                ['NOV', <?php echo $savings_calculation_data['averageDailyKWhPerMonth11']; ?>,<?php echo $estd_daily_otp; ?>],
                ['DEC', <?php echo $savings_calculation_data['averageDailyKWhPerMonth12']; ?>,<?php echo $estd_daily_otp; ?>],
            ]);
            var options = {
                curveType: 'function',
                titleTextStyle: {
                    fontName: 'Helvetica, sans-serif;',
                    italic: false,
                    bold: false,
                    fontStyle: 'normal', //or bold, italic, etc.
                    fontSize: 12
                },
                legend: {position: 'bottom',
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 14,
                        fontStyle: 'normal'
                    }
                },
                hAxis: {
                    format: '#\'$\'',
                    titleTextStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 12,
                        fontStyle: 'normal'
                    },
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: 'normal'
                    }
                },
                vAxis: {
                    format: '#\'kWh\'',
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: 'normal'
                    }
                },
                width: 900,
                height: 330,
                legend: {position: 'none', textStyle: {fontSize: 4, }},
                seriesType: 'bars',
                series: {
                    0: {color: '#DE1717'},
                    1: {
                        type: 'line', color: '#000',
                        visibleInLegend: false
                    }
                }
            };
            /**var view = new google.visualization.DataView(data);
             view.setColumns([0, 1,
             {calc: "stringify",
             sourceColumn: 1,
             type: "string",
             role: "annotation"},
             2]);*/
  
		
		$('.overlay-roadblock').show();
		$('#overlay_content').html('<div class="loader"></div>Processing Images for Pdf Creation Please Wait.....');

  var my_div = document.getElementById('chart');
    var my_chart = new google.visualization.ColumnChart(my_div);

    google.visualization.events.addListener(my_chart, 'ready', function () {
      document.getElementById('dataImg1').value = my_chart.getImageURI();
      $.ajax({
          type: 'POST',
          url: '<?php echo site_url(); ?>admin/product/residential_solar/save_chartimg_to_png',
          datatype: 'json',
          data: $('#chartForm').serialize(),
          success: function (stat) {
            //$('#charts').html('');
            var uuid = $('#uuid').val();
            $('.overlay-roadblock').show();
			$('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
            setTimeout(function(){
                window.location.href = '<?php echo site_url(); ?>admin/product/residential_solar/proposal_pdf_download/'+ uuid + '?view=download';
                //window.open('https://kugacrm.com.au/admin/proposal/led_pdf_download/'+ pid + '?view=download');
                //window.close();
            },300);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            
          }
        });
    });
    my_chart.draw(data,options);
}

   </script>
<?php } ?>
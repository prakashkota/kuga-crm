<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
		input[type=checkbox] {
			-moz-appearance:none;
			-webkit-appearance:none;
			-o-appearance:none;
			outline: none;
			content: none;	
		}

		input[type=checkbox]:before {
			font-family: "FontAwesome";
			content: "\f00c";
			font-size: 15px;
			color: transparent !important;
			background: #fff;
			display: block;
			width: 15px;
			height: 15px;
			border: 2px solid black;
			margin-right: 5px;
		}

		input[type=checkbox]:checked:before {
			color: black !important;
		}

		input[type=radio] {
			-moz-appearance:none;
			-webkit-appearance:none;
			-o-appearance:none;
			outline: none;
			content: none;	
		}

		input[type=radio]:before {
			font-family: "FontAwesome";
			content: "\f00c";
			font-size: 15px;
			color: transparent !important;
			background: #fff;
			display: block;
			width: 15px;
			height: 15px;
			border: 2px solid black;
			margin-right: 5px;
		}

		input[type=radio]:checked:before {
			color: black !important;
		}
		
	</style>
</head>
<body>
    <?php $this->load->view('pdf_files/nomination_form/nsw_commercial_v2/page_nomination_form_new'); ?>
</body>
</html>
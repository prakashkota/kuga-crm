$(document).ready(function(){
    $(".site-table .arrow-btn1").click(function(){
        $(".site-table").toggleClass("site-table-show1");   
        $(".site-table").removeClass("site-table-show2"); 
        $(".tableoption-2").toggleClass("tableoption-show1");  
        $(".tableoption-2").removeClass("tableoption-show2");
		if($(".site-table").hasClass('site-table-show1')){
			$(".site-table th:nth-child(4) a").html('<i class="fa fa-backward" title="click to hide columns"></i>');
		} else {
			$(".site-table th:nth-child(4) a").html('<i class="fa fa-forward" title="click to expand"></i>');
		}
         
    });
    $(".site-table .arrow-btn2").click(function(){
        $(".site-table").removeClass("site-table-show1");   
        $(".site-table").toggleClass("site-table-show2");
        $(".tableoption-2").toggleClass("tableoption-show2");
        $(".tableoption-2").removeClass("tableoption-show1");

    });
});

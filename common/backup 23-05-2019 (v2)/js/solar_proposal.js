var meter_data_window;

var solar_proposal_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.proposal_data = (options.proposal_data && options.proposal_data != '') ? JSON.parse(options.proposal_data) : {};
    this.panel_data = (options.panel_data && options.panel_data != '') ? JSON.parse(options.panel_data) : {};
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.postcode_rating = parseFloat(options.postcode_rating);
    this.cust_id = '';
    this.proposal_image = '';
    this.solar_panel_area_count = 1;
    this.gst = 1.1;
    this.total_panels = 0;
    this.total_panels_by_area = 0;
    this.total_system_size = 0;

    $('#rate').change(function () {
        var rate = $(this).val();
        if (rate == '1') {
            $('#rate1').removeClass('hidden');
            $('#rate2').addClass('hidden');
            $('#rate3').addClass('hidden');
            $('#rate4').addClass('hidden');
        } else if (rate == '2') {
            $('#rate1').removeClass('hidden');
            $('#rate2').removeClass('hidden');
            $('#rate3').addClass('hidden');
            $('#rate4').addClass('hidden');
        } else if (rate == '3') {
            $('#rate1').removeClass('hidden');
            $('#rate2').removeClass('hidden');
            $('#rate3').removeClass('hidden');
        } else if (rate == '4') {
            $('#rate1').removeClass('hidden');
            $('#rate2').removeClass('hidden');
            $('#rate3').removeClass('hidden');
            $('#rate4').removeClass('hidden');
        }
    });

    $('#custom_profile_id').change(function () {
        var profile_id = $(this).val();
        var thumb_ele = document.getElementById('solarCustomProfileIdThumb');
        if (profile_id) {
            thumb_ele.setAttribute('class', 'custom-profile__thumbnail');
            thumb_ele.setAttribute('src', base_url + 'assets/images/ROI-graphics-' + profile_id + '.jpg');
        } else {
            thumb_ele.setAttribute('class', 'hidden custom-profile__thumbnail');
        }
    });

    self.add_panel_area();

    $('#type_id').change(function () {
        self.fetch_solar_product_by_type();
    });

    $('#add_product_btn').click(function () {
        self.add_proposal_product();
    });

    $('#update_product_btn').click(function () {
        self.add_proposal_product();
    });

    $('#cancel_product_btn').click(function () {
        document.getElementById('solar_products_form').reset();
        $(this).addClass('hidden');
        $('#update_product_btn').addClass('hidden');
        $('#add_product_btn').removeClass('hidden');
        document.getElementById('solar_product_id').value = '';
    });

    $('#save_proposal_btn').click(function () {
        self.save_solar_proposal();
    });

    $('#save_proposal_additional_btn').click(function () {
        self.save_solar_system_summary();
    });

    $('.additional_items').change(function () {
        self.save_solar_system_summary();
    });

    $('#price_before_stc_rebate').change(function () {
        self.calculate_payment_summary();
        self.save_solar_rebate();
    });

    $('.stc_calculate_inputs').change(function () {
        self.calculate_stc_rebate(true);
    });
    
    $('#save_stc_btn').click(function () {
       self.calculate_payment_summary(true);
    });

    $('#manage_finance_proposal_btn').click(function () {
        $(this).hide();
        $('#solar_proposal_finance_container').slideDown();
    });

    $('#term').change(function () {
        self.calculate_proposal_finance();
    });

    $('#save_proposal_finance_btn').click(function () {
        self.save_proposal_finance();
    });

    $('#delete_proposal_finance_btn').click(function () {
        self.delete_proposal_finance();
    });

    $('#tab_image_upload_btn').click(function () {
        $('#mapping_tool_actions').hide();
    });

    $('#reset-all').click(function () {
        calculateNow();
    });

    $('#is_meter_data').click(function () {
        $this = $(this);
        if ($(this).is(':checked')) {
            $('#no_of_working_days').parent().removeClass('hidden');
            $('#custom_profile_id').parent().removeClass('hidden');
            $('#is_meter_data_val').val(0);
        } else {
            $('#no_of_working_days').parent().addClass('hidden');
            $('#custom_profile_id').parent().addClass('hidden');
            $('#no_of_working_days').val('');
            $('#custom_profile_id').val('');
            $('#is_meter_data_val').val(1);
            self.handle_meter_data();
        }
    });

    $('#is_demand_charge').click(function () {
        $this = $(this);
        if ($(this).is(':checked')) {
            $('#demand_charge_container').addClass('hidden');
            $('#is_demand_charge_val').val(0);
        } else {
            $('#demand_charge_container').removeClass('hidden');
            $('#is_demand_charge_val').val(1);
        }
    });

    $('#demand_charge,#demand_charge_kVA').change(function () {
        var dc = $('#demand_charge').val();
        var demand_charge_kVA = $('#demand_charge_kVA').val();
        if (!isNaN(dc) && !isNaN(demand_charge_kVA)) {
            var tc = dc * demand_charge_kVA;
            $('#total_demand_charge').val(tc);
        }
    });

    $(document.body).delegate('.no_of_panels','change',function () {
        var total_panels_count = 0;
        var panels_count = document.getElementsByClassName('no_of_panels');
        for (var i = 0; i < panels_count.length; i++) {
            total_panels_count = parseInt(total_panels_count) + parseInt(panels_count[i].value);
            console.log(panels_count[i].value);
        }
        self.total_panels_by_area = total_panels_count;
    });
    
    $('.rate_usage').change(function () {
        var total_usage = 0;
        var usage = document.getElementsByClassName('rate_usage');
        for (var i = 0; i < usage.length; i++) {
            if(!isNaN(parseInt(usage[i].value))){
                total_usage = parseInt(total_usage) + parseInt(usage[i].value);
            }
        }
        document.getElementById('monthly_electricity_usage').value = total_usage;
    });
    
    $('.rate_days_of_week').change(function () {
        $this = $(this);
        if($this.val() == 'flat_rate'){
            $this.parent().parent().prev('div').prev('div').find('.rate_st').val('00:00');
            $this.parent().parent().prev('div').find('.rate_ft').val('23:59');
        }
    });
    
    $('#download_solar_proposal').click(function () {
        var flag = self.validate_solar_proposal_pdf_data();
        if (self.total_panels_by_area != self.total_panels) {
            toastr.remove();
            toastr["error"]('No of panels in panel area does not match the panel count by product.');
            return false;
        } else if (!flag) {
            toastr.remove();
            toastr["error"]('Proposal cannot be generated some required data is missing.');
            return false;
        } else {
            var url = base_url + 'admin/proposal/solar_pdf_download/' + self.proposal_data.id;
            document.getElementById('download_solar_proposal').setAttribute('href', url);
            document.getElementById('download_solar_proposal').click();
        }

    });

    /**$('#stc').click(function () {
        var dt = new Date();
        $('#stc_calculation_modal').modal('show');
        $('#stc_calculate_system_size').val(self.total_system_size);
        $('#stc_calculate_postcode').val(self.lead_data.postcode);
        $('#stc_calculate_year').val(dt.getFullYear());
    });*/
    
    $('#stc_rebate_value').click(function () {
        var dt = new Date();
        $('#stc_calculation_modal').modal('show');
        $('#stc_calculate_system_size').val(self.total_system_size);
        $('#stc_calculate_postcode').val(self.lead_data.postcode);
        $('#stc_calculate_year').val(dt.getFullYear());
    });
    

    if (Object.keys(self.lead_data).length > 0) {
        self.cust_id = self.lead_data.cust_id;
        //Populate Customer Details
        $('#site_name').val(self.lead_data.site_name);
        $('#site_address').val(self.lead_data.address);
        $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#customer_contact_no').val(self.lead_data.customer_contact_no);
        $('#site_postcode').val(self.lead_data.postcode);
        if(self.lead_data.postcode == '0' || self.lead_data.postcode == ''){
            $('#site_postcode_warning').html('<i class="fa fa-warning"></i> Please provide postcode');
        }
        //Populdate Proposal Data
        for (var key in self.proposal_data) {
            if (key === 'additional_items') {
                var additional_items = self.proposal_data[key];
                for (var item_key in additional_items) {
                    $("#" + item_key).val(additional_items[item_key]);
                }
            } else if (key === 'image') {
                if (self.proposal_data[key] != '' && self.proposal_data[key] != null) {
                    self.proposal_image = self.proposal_data[key];
                    $('#image').val(self.proposal_image);
                    self.show_image();
                }
            } else if (key == 'total_payable_exGST') {
                $("#total_payable_exGST").val(parseFloat(self.proposal_data[key]).toFixed(2));
                $("#total_payable_inGST").val(parseFloat(self.proposal_data[key] * self.gst).toFixed(2));
            } else if (key == 'stc_rebate_value') {
                $("#stc_rebate_value").val(self.proposal_data[key]);
                $("#stc_rebate_value_inGST").val(parseFloat(self.proposal_data[key] * self.gst).toFixed(2));
            } else if (key == 'price_before_stc_rebate') {
                var PriceBeforeSTCRebate = (parseFloat(self.proposal_data[key]) + Number($("#stc_rebate_value").val())).toFixed(2);
                $("#price_before_stc_rebate").val(PriceBeforeSTCRebate);
                $("#price_before_stc_rebate_inGST").val(parseFloat(PriceBeforeSTCRebate * self.gst).toFixed(2));
            } else if (key === 'rate1_data') {
                for (var key1 in self.proposal_data['rate1_data']) {
                    $("[name='solar_proposal[rate1_data][" + key1 + "]']").val(self.proposal_data['rate1_data'][key1]).trigger('change');
                }
            } else if (key === 'rate2_data') {
                for (var key1 in self.proposal_data['rate2_data']) {
                    $("[name='solar_proposal[rate2_data][" + key1 + "]']").val(self.proposal_data['rate2_data'][key1]).trigger('change');
                }
            } else if (key === 'rate3_data') {
                for (var key1 in self.proposal_data['rate3_data']) {
                    $("[name='solar_proposal[rate3_data][" + key1 + "]']").val(self.proposal_data['rate3_data'][key1]).trigger('change');
                }
            } else if (key === 'rate4_data') {
                for (var key1 in self.proposal_data['rate4_data']) {
                    $("[name='solar_proposal[rate4_data][" + key1 + "]']").val(self.proposal_data['rate4_data'][key1]).trigger('change');
                }
            } else if (key === 'is_meter_data') {
                $("[name='solar_proposal[" + key + "]']").val(self.proposal_data[key]).trigger('change');
                if (self.proposal_data['is_meter_data'] == '1') {
                    $('#is_meter_data').click();
                    if (meter_data_window) {
                        meter_data_window.close();
                    }
                }
            } else if (key === 'is_demand_charge') {
                $("[name='solar_proposal[" + key + "]']").val(self.proposal_data[key]).trigger('change');
                if (self.proposal_data['is_demand_charge'] == '1') {
                    $('#is_demand_charge').click();
                }
            } else {
                if(key == 'no_of_working_days' || key == 'custom_profile_id'){
                    if(self.proposal_data[key] != '' && self.proposal_data[key] != '0' && self.proposal_data[key] != null){
                        $("[name='solar_proposal[" + key + "]']").val(self.proposal_data[key]).trigger('change');
                    }
                }else{
                    if(self.proposal_data[key] != '' && self.proposal_data[key] != null){
                        $("[name='solar_proposal[" + key + "]']").val(self.proposal_data[key]).trigger('change');
                    }
                }
            }
        }

        //Set Price Assumption
        var no_of_stc = $('#stc').val();
        no_of_stc = parseFloat(no_of_stc).toFixed(2);

        var stc_rebate_value = $("#stc_rebate_value").val();
        stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

        var price_assumption = (stc_rebate_value / no_of_stc)
        price_assumption = parseFloat(price_assumption).toFixed(2);

        $('#stc_calculate_price').val(price_assumption);

        $('#rate').trigger('change');

        //Populdate Finance Details
        $('#term').val(self.proposal_data.term);
        $('#monthly_payment_plan').val(self.proposal_data.monthly_payment_plan);
        if (self.proposal_data.term != null) {
            $('#delete_proposal_finance_btn').removeClass('hidden');
        }

        //Populdate Proposal Data
        self.solar_panel_area_count = self.panel_data.length;
        for (var i = 0; i < self.panel_data.length; i++) {
            if (i > 0) {
                $("#add_panel_area").click();
            }
            $("[name='solar_panel_data[no_of_panels][" + i + "]']").val(parseInt(self.panel_data[i]['no_of_panels']));
            $("[name='solar_panel_data[panel_orientation][" + i + "]']").val(parseInt(self.panel_data[i]['panel_orientation']));
            $("[name='solar_panel_data[panel_tilt][" + i + "]']").val(parseInt(self.panel_data[i]['panel_tilt']));
        }
        self.fetch_solar_proposal_details();
    }


    self.fetch_proposal_products();
    self.upload_proposal_image();
    self.show_image();
    self.hide_image();
    self.edit_customer_location();
    self.save_customer_location();
    self.save_customer_postcode();
    self.save_customer();
};

/** Common Functions **/

solar_proposal_manager.prototype.toCommas = function (value) {
    console.log(value);
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

solar_proposal_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

solar_proposal_manager.prototype.space_type_by_hours = function (item_hours) {
    $("#hours").val(item_hours);
    $('#lifetime').attr('disabsolar', 'disabsolar');
    $('#lifetime').val('');
    if (parseInt(item_hours) >= 5000) {
        $('#lifetime').removeAttr('disabsolar');
    }
    $("#hours").val(item_hours);
};

solar_proposal_manager.prototype.fetch_rebate_value = function () {
    if ($('#prd_id option:selected').val() != '') {
        var rebate = 0;
        var item_data = $('#prd_id option:selected').attr('data-item');
        item_data = JSON.parse(item_data);
        var hours = document.getElementById('hours').value;
        hours = parseInt(hours);
        if (hours == 2000) {
            rebate = item_data['rebate_2000'];
        } else if (hours == 3000) {
            rebate = item_data['rebate_3000'];
        } else if (hours == 5000) {
            var item_data_id = $('#space_type option:selected').val();
            var item_data_text = $('#space_type option:selected').text();
            if (item_data_id == '8' || item_data_text === '5000hrs (lifetime A)') {
                rebate = item_data['rebate_5000_a'];
            } else if (item_data_id == '7' || item_data_text === '5000hrs (lifetime C)') {
                rebate = item_data['rebate_5000_c'];
            } else {
                rebate = item_data['rebate_5000'];
            }
        }
        document.getElementById('veec').value = rebate;
    }
};

solar_proposal_manager.prototype.create_solar_proposal_details = function (data) {
    var self = this;
    var tr = document.createElement('tr');
    var total_project_cost_row = document.createElement('tr');
    var total_project_cost = document.createElement('td');
    total_project_cost.innerHTML = 'Total Project Cost (exGST)';
    var total_project_cost_value = document.createElement('td');
    total_project_cost_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.total_payable_exGST) + '</span>';
    total_project_cost_row.appendChild(total_project_cost);
    total_project_cost_row.appendChild(total_project_cost_value);

    var total_rebate_cost_row = document.createElement('tr');
    var total_rebate_cost = document.createElement('td');
    total_rebate_cost.innerHTML = 'Total Rebate Cost (exGST)';
    var total_rebate_cost_value = document.createElement('td');
    total_rebate_cost_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.stc_rebate_value) + '</span>';
    total_rebate_cost_row.appendChild(total_rebate_cost);
    total_rebate_cost_row.appendChild(total_rebate_cost_value);

    var total_invesment_row = document.createElement('tr');
    var total_invesment = document.createElement('td');
    total_invesment.innerHTML = 'Net Payable (exGST)';
    var total_invesment_value = document.createElement('td');
    total_invesment_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.price_before_stc_rebate) + '</span> </a>';
    total_invesment_row.appendChild(total_invesment);
    total_invesment_row.appendChild(total_invesment_value);

    var total_system_size = document.createElement('tr');
    var system_size = document.createElement('td');
    system_size.innerHTML = 'System Size';
    var total_system_size_value = document.createElement('td');
    total_system_size_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_system_size) + ' kW</span>';
    total_system_size.appendChild(system_size);
    total_system_size.appendChild(total_system_size_value);

    var td2 = document.createElement('tr');
    //td2.appendChild(additional_item_cost_row);
    td2.appendChild(total_project_cost_row);
    td2.appendChild(total_rebate_cost_row);
    td2.appendChild(total_invesment_row);
    td2.appendChild(total_system_size);

    tr.appendChild(td2);

    document.getElementById('proposal_cost_details_wrapper').innerHTML = '';
    document.getElementById('proposal_cost_details_wrapper').appendChild(td2);
};

solar_proposal_manager.prototype.fetch_solar_proposal_details = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_solar_proposal_details',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        beforeSend: function () {
            //document.getElementById('proposal_cost_details_wrapper').appendChild(createLoader('Fetching Proposal Details....'));
            document.getElementById('proposal_cost_details_wrapper').appendChild(createPlaceHolder(false));
            document.getElementById('proposal_cost_details_wrapper').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                self.create_solar_proposal_details(stat.proposal_data);
                $('[data-toggle="tooltip"]').tooltip();
                //$('#total_payable_exGST').trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.calculate_proposal_finance = function () {
    var self = this;
    var term = document.getElementById('term').value;
    if (term == '') {
        toastr["error"]('Please Select term.');
        return false;
    }
    var data = {};
    data.proposal_id = self.proposal_data.id;
    data.term = term;
    data.type = 2;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/calculate_finance',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').appendChild(createLoader('Calcualting Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                document.getElementById('monthly_payment_plan').value = stat.monthly_payment;
            } else {
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.save_proposal_finance = function () {
    var self = this;
    var term = document.getElementById('term').value;
    var data = '';
    data = $('#proposal_finance_form').serialize();
    data += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/save_finance',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').appendChild(createLoader('Saving Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success) {
                toastr["success"](stat.status);
                $('#delete_proposal_finance_btn').removeClass('hidden');
            } else {
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.delete_proposal_finance = function () {
    var self = this;
    var data = '';
    data = $('#proposal_finance_form').serialize();
    data += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/delete_finance',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').appendChild(createLoader('Deleting Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success) {
                toastr["success"](stat.status);
                $('#delete_proposal_finance_btn').addClass('hidden');
                document.getElementById('proposal_finance_form').reset();
            } else {
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#save_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            $('#delete_proposal_finance_btn').removeAttr('disabsolar', 'disabsolar');
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


solar_proposal_manager.prototype.add_panel_area = function () {
    var self = this;
    var max_fields = 20; //maximum input boxes allowed
    var wrapper = $("#panels_area_wrapper"); //Fields wrapper
    var add_button = $("#add_panel_area"); //Add button ID
    var count = self.solar_panel_area_count;
    var x = (count > 0) ? count : 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) {
            $(wrapper).append(self.create_panel_area(x));
            x++;
        }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove field
        e.preventDefault();
        $(this).parent().parent('div').remove();
        x--;
        $('.no_of_panels').trigger('change');
    });
};


solar_proposal_manager.prototype.create_panel_area = function (x) {
    var panel_area = '<div><div style="font-size:18px; border-bottom:1px solid black; margin-bottom:5px;">Panel Details - Area' + (x + 1) + '<a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></div>'
            + '<div class="form-group">'
            + '<label>Number of Panels</label>'
            + '<input class="form-control no_of_panels" placeholder="Please Enter Number of Panels" type="number" name="solar_panel_data[no_of_panels][' + x + ']" value="" min="1" required="">'
            + '</div>'

            + '<div class="form-group">'
            + '<label class="required-label">Panel Orientation</label>'
            + '<select name="solar_panel_data[panel_orientation][' + x + ']"  class="form-control" required="">'
            + '<option value="0">0</option>'
            + '<option value="10">10</option>'
            + '<option value="20" >20</option>'
            + '<option value="30">30</option>'
            + '<option value="40">40</option>'
            + '<option value="50">50</option>'
            + '<option value="60">60</option>'
            + '<option value="70">70</option>'
            + '<option value="80">80</option>'
            + '<option value="90">90</option>'
            + '<option value="100">100</option>'
            + '<option value="110">110</option>'
            + '<option value="120">120</option>'
            + '<option value="130">130</option>'
            + '<option value="140">140</option>'
            + '<option value="150">150</option>'
            + '<option value="160">160</option>'
            + '<option value="170">170</option>'
            + '<option value="180">180</option>'
            + '<option value="190">190</option>'
            + '<option value="200">200</option>'
            + '<option value="210">210</option>'
            + '<option value="220">220</option>'
            + '<option value="230">230</option>'
            + '<option value="240">240</option>'
            + '<option value="250">250</option>'
            + '<option value="260">260</option>'
            + '<option value="270">270</option>'
            + '<option value="280">280</option>'
            + '<option value="290">290</option>'
            + '<option value="300">300</option>'
            + '<option value="310">310</option>'
            + '<option value="320">320</option>'
            + '<option value="330">330</option>'
            + '<option value="340">340</option>'
            + '<option value="350">350</option>'
            + '</select>'

            + '</div>'
            + '<div class="form-group">'
            + '<label>Panel Tilt</label>'
            + '<select name="solar_panel_data[panel_tilt][' + x + ']"  class="form-control" required="">'
            + '<option value="0">0</option>'
            + '<option value="10" selected="">10</option>'
            + '<option value="20">20</option>'
            + '<option value="30">30</option>'
            + '<option value="40">40</option>'
            + '<option value="50">50</option>'
            + '<option value="60">60</option>'
            + '<option value="70">70</option>'
            + '<option value="80">80</option>'
            + '<option value="90">90</option>'
            + '</select>'
            + '</div></div>';
    return panel_area;
}

/** ENERGY USAGE RELATED FUNCTIONS **/

solar_proposal_manager.prototype.validate_solar_proposal_pdf_data = function () {
    var self = this;
    var flag = true;
    
    var elements = document.getElementById('solar_energy_form').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    
    var elements1 = document.getElementById('solarSystemSummary').elements;
    for (var i = 0; i < elements1.length; i++) {
        $(elements1[i]).removeClass('is-invalid');
        if (elements1[i].hasAttribute('required')) {
            if (elements1[i].value == '') {
                $(elements1[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    
    var is_meter_data = $('#is_meter_data').val();
    var no_of_working_days = $('#no_of_working_days').val();
    var custom_profile_id = $('#custom_profile_id').val();
    $('#no_of_working_days').removeClass('is-invalid');
    $('#custom_profile_id').removeClass('is-invalid');
    if(is_meter_data != '1'){
        if(no_of_working_days == ''){
            $('#no_of_working_days').addClass('is-invalid');
            flag = false;
        }
        if(custom_profile_id == ''){
            $('#custom_profile_id').addClass('is-invalid');
            flag = false;
        }
    }
    
    return flag;
};

solar_proposal_manager.prototype.validate_solar_proposal_data = function (form) {
    var flag = true;
    var elements = document.getElementById(form).elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    
    if(form == 'solar_energy_form'){
        var is_meter_data = $('#is_meter_data').val();
        var no_of_working_days = $('#no_of_working_days').val();
        var custom_profile_id = $('#custom_profile_id').val();
        $('#no_of_working_days').removeClass('is-invalid');
        $('#custom_profile_id').removeClass('is-invalid');
        if(is_meter_data != '1'){
            if(no_of_working_days == ''){
                $('#no_of_working_days').addClass('is-invalid');
                flag = false;
            }
            if(custom_profile_id == ''){
                $('#custom_profile_id').addClass('is-invalid');
                flag = false;
            }
        }
    }
    
    return flag;
};

solar_proposal_manager.prototype.save_solar_proposal = function () {
    var self = this;
    var flag = self.validate_solar_proposal_data('solar_energy_form');
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#solar_energy_form').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_solar_proposal',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend:function(){
            toastr["info"]('Saving Proposal Data Please Wait....');
        },
        success: function (stat) {
            toastr.remove();
            if (stat.success == true) {
                toastr["success"](stat.status);
                self.fetch_solar_proposal_details();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.save_solar_system_summary = function () {
    var self = this;
    var flag = self.validate_solar_proposal_data('solarSystemSummary');
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#solarSystemSummary').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_solar_proposal',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            toastr.remove();
            if (stat.success == true) {
                toastr["success"](stat.status);
                self.fetch_solar_proposal_details();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.save_solar_rebate = function (is_save) {
    var self = this;
    var flag = self.validate_solar_proposal_data('solarPayments');
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#solarPayments').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_solar_proposal',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                if(is_save){
                    $('#stc_calculation_modal').modal('hide');
                }
                self.fetch_solar_proposal_details();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


/** LED PRODUCT RELATED FUNCTIONS **/

solar_proposal_manager.prototype.fetch_solar_product_by_type = function (id) {
    var self = this;
    var type_id = $('#type_id').val();
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/product/fetch_solar_product_by_type',
        datatype: 'json',
        data: {type_id: type_id, state: self.lead_data.state_postal},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var option;
                document.getElementById('prd_id').innerHTML = '';
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        option = self.create_product_select_option(stat.product_data[i], id);
                        document.getElementById('prd_id').appendChild(option);
                    }
                } else {
                    option = document.createElement('option');
                    option.setAttribute('value', '');
                    option.innerHTML = 'No Item Found with Selected Product Type';
                    document.getElementById('prd_id').appendChild(option);
                }
                $('#prd_id').trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.create_product_select_option = function (data, selected) {
    var option = document.createElement('option');
    var value = data.prd_id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    if (data.prd_id == selected) {
        option.setAttribute('selected', 'selected');
    }
    option.innerHTML = data.product_name;
    return option;
};

solar_proposal_manager.prototype.validate_solar_proposal_product_data = function () {
    var self = this;
    var flag = true;
    var space_type = $('#space_type').val();
    var elements = document.getElementById("solar_products_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

solar_proposal_manager.prototype.add_proposal_product = function () {
    var self = this;
    var flag = self.validate_solar_proposal_product_data();
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#solar_products_form').serialize();
    formData += '&solar_product[proposal_id]=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_solar_proposal_products',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#solar_proposal_products_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_prodcut_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#solar_proposal_products_table_body');
                        tbody.appendChild(row);
                    }
                    self.calculate_system_size();
                    self.edit_proposal_product();
                    self.delete_proposal_product();
                    var prd_typ_id = $('#type_id').val();
                    prd_typ_id = parseInt(prd_typ_id);
                    if (prd_typ_id == 9) {
                        //self.save_solar_rebate();
                        //self.save_solar_system_summary();
                    }
                } else {
                    $('#solar_proposal_products_table_body').html('<tr><td colspan="4" class="text-center">No product data found.</td></tr>');
                }
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.create_proposal_prodcut_table_row = function (data) {
    var tr = document.createElement('tr');
    var td_array = ['product_name', 'type_name', 'prd_qty'];
    for (var i = 0; i < td_array.length; i++) {
        var td = document.createElement('td');
        td.innerHTML = data[td_array[i]];
        tr.appendChild(td);
    }
    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm solar_proposal_product_edit_btn";
    action_edit_btn.setAttribute('data-id', data.id);
    action_edit_btn.setAttribute('data-item', JSON.stringify(data));
    action_edit_btn.setAttribute('href', 'javascript:void(0);');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    var action_delete_btn = document.createElement('a');
    action_delete_btn.className = "btn-danger btn-sm ml-2 solar_proposal_product_delete_btn";
    action_delete_btn.setAttribute('data-id', data.id);
    action_delete_btn.setAttribute('href', 'javascript:void(0);');
    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

    td_action.appendChild(action_edit_btn);
    td_action.appendChild(action_delete_btn);
    tr.appendChild(td_action);

    return tr;
};

solar_proposal_manager.prototype.fetch_proposal_products = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_solar_proposal_products',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#solar_proposal_products_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_prodcut_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#solar_proposal_products_table_body');
                        tbody.appendChild(row);
                    }
                    self.calculate_system_size();
                    self.edit_proposal_product();
                    self.delete_proposal_product();
                } else {
                    $('.no_of_panels').trigger('change');
                    $('#solar_proposal_products_table_body').html('<tr><td colspan="4" class="text-center">No product data found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.edit_proposal_product = function () {
    var self = this;
    $('.solar_proposal_product_edit_btn').click(function () {
        var id = $(this).attr('data-id');
        document.getElementById('solar_product_id').value = id;

        var data = $(this).attr('data-item');
        data = JSON.parse(data);

        $('#add_product_btn').addClass('hidden');
        $('#cancel_product_btn').removeClass('hidden');
        $('#update_product_btn').removeClass('hidden');

        //Set space Type
        $('#space_type').val(data['space_type_id']);
        $('#space_type').trigger('change');

        //Populate data in form
        var elements = document.getElementById("solar_energy_form").elements;

        for (var key in data) {
            $('#' + key).val(data[key]);
        }

        if (parseInt(data.type_id) > 9 && parseInt(data.type_id) < 14) {
            $('#type_id').val(10);
        }

        //Fetch and Set New Prorduct
        self.fetch_solar_product_by_type(data['prd_id']);
        //window.scrollTo({top: 10, behavior: 'smooth'});
    });
};

solar_proposal_manager.prototype.delete_proposal_product = function () {
    var self = this;
    $('.solar_proposal_product_delete_btn').click(function () {
        $this = $(this);
        var id = $(this).attr('data-id');
        $('#cancel_product_btn').click();
        $.ajax({
            url: base_url + 'admin/proposal/delete_solar_proposal_products',
            type: 'post',
            data: {
                solar_product_id: id,
                proposal_id: self.proposal_data.id
            },
            dataType: 'json',
            success: function (stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                    self.calculate_system_size();
                    //self.save_solar_rebate();
                    //self.save_solar_system_summary();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};


/** ADDITIONAL ITEM RELATED FUNCTIONS **/

solar_proposal_manager.prototype.create_proposal_additional_item_table_row = function (data) {
    var tr = document.createElement('tr');
    var td_array = ['ae_name', 'ae_qty', 'ae_cost'];
    for (var i = 0; i < td_array.length; i++) {
        var td = document.createElement('td');
        td.innerHTML = data[td_array[i]];
        tr.appendChild(td);
    }
    var td_action = document.createElement('td');

    var action_delete_btn = document.createElement('a');
    action_delete_btn.className = "btn-danger btn-sm ml-2 solar_proposal_additional_item_delete_btn";
    action_delete_btn.setAttribute('data-id', data.id);
    action_delete_btn.setAttribute('href', 'javascript:void(0);');
    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

    td_action.appendChild(action_delete_btn);
    tr.appendChild(td_action);

    return tr;
};

solar_proposal_manager.prototype.fetch_proposal_additional_items = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_solar_proposal_additional_items',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#solar_proposal_additional_item_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_additional_item_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#solar_proposal_additional_item_table_body');
                        tbody.appendChild(row);
                    }
                    self.delete_proposal_additional_item();
                } else {
                    $('#solar_proposal_additional_item_table_body').html('<tr><td colspan="4" class="text-center">No additional item found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.validate_solar_proposal_additional_item_data = function () {
    var flag = true;
    var elements = document.getElementById("add_additional_item_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

solar_proposal_manager.prototype.add_proposal_additional_item = function () {
    var self = this;
    var flag = self.validate_solar_proposal_additional_item_data();
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#add_additional_item_form').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_solar_proposal_additional_items',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#solar_proposal_additional_item_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_additional_item_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#solar_proposal_additional_item_table_body');
                        tbody.appendChild(row);
                    }
                    self.delete_proposal_additional_item();
                } else {
                    $('#solar_proposal_additional_item_table_body').html('<tr><td colspan="4" class="text-center">No additional item found.</td></tr>');
                }
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

solar_proposal_manager.prototype.delete_proposal_additional_item = function () {
    var self = this;
    $('.solar_proposal_additional_item_delete_btn').click(function () {
        $this = $(this);
        var id = $(this).attr('data-id');
        $.ajax({
            url: base_url + 'admin/proposal/delete_solar_proposal_additional_items',
            type: 'post',
            data: {
                item_id: id,
                proposal_id: self.proposal_data.id
            },
            dataType: 'json',
            success: function (stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

solar_proposal_manager.prototype.show_image = function () {
    var self = this;
    var image = self.proposal_image;
    if (image != '') {
        $('#proposal_no_image').attr('style', 'display:none !important;');
        $('#proposal_image').attr('style', 'display:block !important;');
        $('#proposal_image > .add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/proposal_files/' + image + '")');
        $('#proposal_image > .add-picture').css('background-repeat', 'no-repeat');
        $('#proposal_image > .add-picture').css('background-position', '50% 50%');
        $('#proposal_image > .add-picture').css('background-size', '100% 100%');
    }
};

solar_proposal_manager.prototype.hide_image = function () {
    $('#proposal_image_close').click(function () {
        $('#proposal_no_image').attr('style', 'display:block !important;');
        $('#proposal_image').attr('style', 'display:none !important;');
        $('#proposal_image > .add-picture').css('background-image', '');
        $('#proposal_image > .add-picture').css('background-repeat', '');
        $('#proposal_image > .add-picture').css('background-position', '');
        $('#proposal_image > .add-picture').css('background-size', '');
        $('#mapping_tool_actions').show();
    });
};

solar_proposal_manager.prototype.upload_proposal_image = function () {
    var self = this;
    $('#proposal_image_upload').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'proposal_files');
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="solar_proposal[image]"').val(res.file_name);
                    self.proposal_image = res.file_name;
                    self.show_image();
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

solar_proposal_manager.prototype.handle_meter_data = function () {
    var self = this;
    var url = base_url + 'admin/proposal/meter_data/' + self.proposal_data.id;
    var y = window.top.outerHeight / 2 + window.top.screenY - (500 / 2);
    var x = window.top.outerWidth / 2 + window.top.screenX - (400 / 2);
    meter_data_window = window.open('', 'Manage Meter Data', 'width=800px, height=500px, top=' + y + ', left=' + x);
    meter_data_window.location.href = url;
}


solar_proposal_manager.prototype.calculate_system_size = function () {
    var self = this;
    var total_system_size = 0;
    var total_panels = 0;
    var products = document.getElementsByClassName('solar_proposal_product_edit_btn');
    for (var i = 0; i < products.length; i++) {
        var data = JSON.parse(products[i].getAttribute('data-item'));
        var prd_type = parseInt(data.type_id);
        if (prd_type == 9) {
            var prd_qty = parseInt(data.prd_qty);
            var prd_power_wattage = parseFloat(data.power_wattage / 1000);
            var system_size = prd_qty * prd_power_wattage;
            total_system_size = parseFloat(total_system_size) + parseFloat(system_size);
            total_panels = parseFloat(total_panels) + parseFloat(prd_qty);
        }
    }
    total_system_size = parseFloat(total_system_size).toFixed(2);
    document.getElementById('total_system_size').value = total_system_size;
    //SET Upfront option
    if(total_system_size <= 39 ){
        $('#upfront_payment_options').val(1);
    }else if(total_system_size > 39  && total_system_size <= 100){
        $('#upfront_payment_options').val(2);
    }else{
        $('#upfront_payment_options').val(3);
    }
    //$("[name='solar_panel_data[no_of_panels][0]']").val(total_panels);
    $('.no_of_panels').trigger('change');
    self.total_panels = total_panels;
    self.total_system_size = total_system_size;
    self.calculate_stc_rebate();
};

solar_proposal_manager.prototype.calculate_stc_rebate = function (is_save) {
    var self = this;
    var dt = new Date();
    var year_val = (dt.getYear() && dt.getFullYear() == '2018') ? 13 : 12;
    var system_size = $('#total_system_size').val();
    if (isNaN(system_size)) {
        toastr["error"]('Please provide Panel Wattage/System Size in order to calculate payment summary');
        return false;
    }
    var no_of_stc = parseFloat(self.postcode_rating) * parseFloat(year_val) * parseFloat(system_size);
    no_of_stc = Math.floor(no_of_stc);

    var price_assumption = $('#stc_calculate_price').val();
    price_assumption = parseFloat(price_assumption).toFixed(2);

    var stc_rebate_value = no_of_stc * parseFloat(price_assumption);
    stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

    $('#stc').val(no_of_stc);
    $('#stc_rebate_value').val(stc_rebate_value);
    $('#stc_rebate_value_inGST').val(parseFloat(stc_rebate_value * self.gst).toFixed(2));
    //Modal Values
    $('#stc_calculate_stc').val(no_of_stc);
    $('#stc_calculate_rebate').val(stc_rebate_value);
};

solar_proposal_manager.prototype.calculate_payment_summary = function (is_save) {
    var self = this;

    var net_payable = $('#price_before_stc_rebate').val();
    net_payable = parseFloat(net_payable).toFixed(2);

    var stc_rebate_value = $('#stc_rebate_value').val();
    stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

    var total_payable = parseFloat(stc_rebate_value) + parseFloat(net_payable);
    total_payable = parseFloat(total_payable).toFixed(2);

    $("#total_payable_exGST").val(total_payable);
    $("#total_payable_inGST").val(parseFloat(total_payable * self.gst).toFixed(2));
    $("#price_before_stc_rebate_inGST").val(parseFloat(net_payable * self.gst).toFixed(2));

    var context = {};
    context.total_payable_exGST = total_payable;
    context.stc_rebate_value = stc_rebate_value;
    context.price_before_stc_rebate = net_payable;
    context.total_system_size = self.total_system_size;

    self.create_solar_proposal_details(context);

    if (is_save) {
        self.save_solar_rebate(is_save);
    }
};

solar_proposal_manager.prototype.edit_customer_location = function () {
    var self = this;
    $('#customer_location_edit_btn').click(function () {
        $('#site_id').val(self.proposal_data.site_id);
        $('#locationLatitude').val(self.lead_data.latitude);
        $('#locationLongitude').val(self.lead_data.longitude);
        $('.locationstreetAddressVal').val(self.lead_data.address);
        $('.locationPostCode').val(self.lead_data.postcode);
        $('.locationState').val(self.lead_data.state_id);
        $('#select2-cust_locationstreetAddress-container').html(self.lead_data.address);
        

        $('#company_cust_id').val(self.lead_data.cust_id);
        $('#site_name').val(self.lead_data.site_name);
        $('#site_address').val(self.lead_data.address);
        $('#contact_firstname').val(self.lead_data.first_name);
        $('#contact_lastname').val(self.lead_data.last_name);
        $('#contact_phone').val(self.lead_data.customer_contact_no);
        $('#contact_email').val(self.lead_data.customer_email);
        $('#contact_position').val(self.lead_data.position);

        $('#add_customer_location_modal_title').html('Edit Customer Location');
        $('#add_customer_location_modal').modal('show');
    });
}

solar_proposal_manager.prototype.validate_customer_location_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('.locationPostCode').val();
    var locationState = $('.locationState').val();
    var locationstreetAddress = $('.locationstreetAddressVal').val();

    //Remove invalid
    $('.locationPostCode').removeClass('is-invalid');
    $('.locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');


    if (locationPostCode == '') {
        flag = false;
        $('.locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('.locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }

    return flag;
};

solar_proposal_manager.prototype.save_customer_location = function () {
    var self = this;
    $(document).on('click', '#save_customer_location', function () {
        var cust_id = self.lead_data.cust_id;
        var site_id = document.getElementById('site_id').value;
        var flag = self.validate_customer_location_data();
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var locationAddForm = $('#locationAdd').serialize();
            locationAddForm += '&cust_id=' + cust_id;
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer_location',
                datatype: 'json',
                data: locationAddForm,
                beforeSend: function () {
                    if (site_id != '' && site_id != null && site_id != undefined) {
                        toastr["info"]('Updating Customer Location please wait....');
                    }
                    $('#customer_location_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer_location").attr("disabled", "disabled");
                },
                success: function (stat) {
                    toastr.remove();
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        self.lead_data.address = stat.location.address;
                        self.lead_data.postcode = stat.location.postcode;
                        self.lead_data.state_id = stat.location.state_id;
                        if(self.lead_data.postcode == '0' || self.lead_data.postcode == ''){
                            $('#site_postcode_warning').html('<i class="fa fa-warning"></i> Please provide postcode');
                        }else{
                            $('#site_postcode_warning').html('');
                        }
                        $('#site_address').val(self.lead_data.address);
                        $("#save_customer_location").removeAttr("disabled");
                        $('#add_customer_location_modal').modal('hide');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer_location").removeAttr("disabled");
                        $('#customer_location_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer_location").removeAttr("disabled");
                    $('#customer_location_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};


solar_proposal_manager.prototype.save_customer_postcode = function () {
    var self = this;
    $(document).on('change', '#site_postcode', function () {
        var flag = true;
        var cust_id = self.lead_data.cust_id;
        var site_id = self.proposal_data.site_id;
        var site_postcode = document.getElementById('site_postcode').value;
        if(site_postcode == ''){
            toastr["error"]('Error ! Postcode can not be left blank.');
            flag = false;
            return flag;
        }
        if (flag) {
            var data = 'site[postcode]='+site_postcode+'&site[address]='+self.lead_data.address+'&site[state_id]='+self.lead_data.state_id+'&cust_id=' + cust_id + '&site_id='+site_id;;
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer_location',
                datatype: 'json',
                data: data,
                beforeSend: function () {
                    if (site_id != '' && site_id != null && site_id != undefined) {
                        toastr["info"]('Updating Customer Location please wait....');
                    }
                    $("#site_postcode").attr("readonly", "readonly");
                },
                success: function (stat) {
                    toastr.remove();
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        self.lead_data.postcode = stat.location.postcode;
                        self.postcode_rating = stat.location.rating;
                        if(self.lead_data.postcode == '0' || self.lead_data.postcode == ''){
                            $('#site_postcode_warning').html('<i class="fa fa-warning"></i> Please provide postcode');
                        }else{
                            $('#site_postcode_warning').html('');
                        }
                        $("#site_postcode").removeAttr("readonly");
                    } else {
                        toastr["error"](stat.status);
                        $("#site_postcode").removeAttr("readonly");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#site_postcode").removeAttr("readonly");
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};


solar_proposal_manager.prototype.validate_customer_data = function () {
    var self = this;
    var flag = true;
    var firstname = $('#contact_firstname').val();
    var lastname = $('#contact_lastname').val();
    var contactMobilePhone = $('#contact_phone').val();
    var customer_email = $('#contact_email').val();

    //Remove invalid
    $('#contact_phone').removeClass('is-invalid');
    $('#contact_firstname').removeClass('is-invalid');
    $('#contact_email').removeClass('is-invalid');

    if (contactMobilePhone == '') {
        flag = false;
        $('#contact_phone').addClass('is-invalid');
    }
    
    if (firstname == '') {
        flag = false;
        $('#contact_firstname').addClass('is-invalid');
    }
   
    if (customer_email == '') {
        flag = false;
        $('#contact_email').addClass('is-invalid');
    }

    return flag;
};

solar_proposal_manager.prototype.save_customer = function () {
    var self = this;
    $(document).on('click', '#save_customer', function () {
        var cust_id = document.getElementById('company_cust_id').value;
        var flag = self.validate_customer_data();
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var customerAddForm = $('#locationAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer',
                datatype: 'json',
                data: customerAddForm + '&action=add',
                beforeSend: function () {
                    if (cust_id != '' && cust_id != null && cust_id != undefined) {
                        toastr["info"]('Updating Customer please wait....');
                    } 
                    //$('#customer_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer").attr("disabled", "disabled");
                },
                success: function (stat) {
                    toastr.remove();
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        //Populate Data from customer Modal to Front View
                        $('#customer_name').val($('#contact_firstname').val() + ' ' + $('#contact_lastname').val());
                        $('#customer_contact_no').val($('#contact_phone').val());
                        
                        self.lead_data.first_name = $('#contact_firstname').val();
                        self.lead_data.last_name = $('#contact_lastname').val();
                        self.lead_data.customer_contact_no = $('#contact_phone').val();
                        self.lead_data.customer_email = $('#contact_email').val();
                        self.lead_data.position = $('#contact_position').val();
                        
                        $("#save_customer").removeAttr("disabled");
                        $('#customer_loader').html('');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer").removeAttr("disabled");
                        $('#customer_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer").removeAttr("disabled");
                    $('#customer_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};
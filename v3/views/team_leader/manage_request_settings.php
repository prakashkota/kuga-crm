<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Admin - Manage Request Settings</h1>
    </div>
    <!-- BEGIN: message  -->	
    <?php $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Franchise Name</th>
                                        <th>Company Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($franchises))
                                        foreach ($franchises as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $value['full_name']; ?></td>
                                                <td><?php echo $value['company_name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['company_contact_no']; ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" data-href="<?php echo site_url('admin/franchise/confirm_request_settings_change/'.$value['id']); ?>"  data-toggle="modal" data-target="#confirm-request"><i class="icon-user"></i> Confirm Request</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#list').DataTable();
    });
</script>
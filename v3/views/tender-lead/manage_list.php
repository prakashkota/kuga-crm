<style>
    .capitalize {
        text-transform:capitalize;
    }   
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .nav.nav-tabs.nav-topline .nav-item a.nav-link.active {
        background: none;
        box-shadow: inset 0 3px 0 #ffd914;
        color: #000;
        border-radius: 0;
        border-top-color: #ffd914;
        border-bottom: none;
    }
    .nav.nav-tabs.nav-topline .nav-item a.nav-link:hover{
        color: #000;
    }
    tfoot{display: block !important;}


    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .dropdown-menu > li > label {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.428571429;
    }
    .deal_view__actions{    
        display: flex;
        box-sizing: border-box;
        align-items: flex-start
    }

    .dt-button-collection button.buttons-columnVisibility:before,
    .dt-button-collection button.buttons-columnVisibility.active span:before {
        display:block;
        position:absolute;
        top:1.2em;
        left:0;
        width:12px;
        height:12px;
        box-sizing:border-box;
    }

    .dt-button-collection button.buttons-columnVisibility:before {
        content:' ';
        margin-top:-6px;
        margin-left:10px;
        border:1px solid black;
        border-radius:3px;
    }

    .dt-button-collection button.buttons-columnVisibility.active span:before {
        content:'\2714';
        margin-top:-11px;
        margin-left:12px;
        text-align:center;
        text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
    }

    .dt-button-collection button.buttons-columnVisibility span {
        margin-left:20px;    
    }

</style>
<div class="page-wrapper d-block clearfix">
    <form id="lead_list_filters">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="kg-page__heading">
                <div class="kg-page__heading_title">
                    Manage Deal (List View)
                </div>   
                <?php if (isset($is_lead_allocation_sales_rep) && $is_lead_allocation_sales_rep == TRUE) { ?>
                    <div class="kg-page__heading_action_left hidden">
                        <select class="form-control" name="filter[custom][la_userid]" id="la_userid" style="width:150px;">
                            <option value="">Select LA User</option>
                            <option value="All">All</option>
                            <?php foreach ($lead_allocation_users as $key => $value) { ?>
                                <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>" <?php if ($value['user_id'] == $user_id) { ?> selected="" <?php } ?> value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="kg-page__heading_action_left hidden">
                        <select class="form-control" name="filter[custom][userid]" id="userid"  style="width:150px;">
                            <option value="">Select Sales User</option>
                            <option value="All">All</option>
                            <?php foreach ($users as $key => $value) { ?>
                                <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="kg-page__heading_action_left hidden">
                        <select class="form-control" name="filter[custom][state_id]" id="state_id" style="width:110px;">
                            <option value="">Select State</option>
                            <?php foreach ($states as $key => $value) { ?>
                                <option  value="<?php echo $value['state_id']; ?>"><?php echo $value['state_postal']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?> 
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="columns columns-right btn-group pull-right">
                    <div class="ml-2">
                        <a class="btn btn-default" style="margin-left:5px;" href="<?php echo site_url('admin/tender-lead/manage?view=pipeline'); ?>">
                            <i class="fa fa-th"></i>
                        </a>
                    </div>

                    <div class="ml-2 hidden">
                        <button class="btn btn-default" type="button" id="lead_list_range" >
                            <i class="fa fa-calendar"></i> &nbsp; <span></span> <i class="fa fa-caret-down" ></i>
                        </button>
                        <input type="hidden" name="filter[start_date]" id="start_date" value="" />
                        <input type="hidden" name="filter[end_date]" id="end_date" value="" />
                    </div>

                    <div class="ml-2 hidden">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-filter"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-height:100px; height:300px; overflow-y: scroll;">
                            <li><label><strong>Lead Type</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_type">Reset</a>)</label></li>
                            <?php $lead_type = unserialize(LEAD_TYPE);
                                  foreach ($lead_type as $key => $value) { ?>
                            <li><label><input class="lead_type" type="radio" name="filter[custom][lead_type]" value="<?php echo $key; ?>"> <?php echo $value; ?></label></li>
                            <?php } ?>
                            <li><label><strong>Lead Source</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_source">Reset</a>)</label></li>
                            <?php $lead_source = unserialize(lead_source);
                                  foreach ($lead_source as $key => $value) { ?>
                            <li><label><input class="lead_source" type="radio" name="filter[custom][lead_source]" value="<?php echo $value; ?>"> <?php echo $value; ?></label></li>
                            <?php } ?>
                            <li><label><strong>Lead Stage</strong> (<a href="javascript:void(0);" class="reset_filter" data-id="lead_stage">Reset</a>)</label></li>
                            <?php foreach ($lead_stages as $key => $value) { ?>
                            <li><label><input class="lead_stage" type="radio" name="filter[custom][lead_stage]" value="<?php echo $value['id']; ?>"> <?php echo $value['stage_name']; ?></label></li>
                            <?php } ?>
                        </ul>
                    </div>  
                    
                    <div class="ml-2 hidden">
                        <a href="javascript:void(0);" class="btn btn-default" id="export_leads">Export</a>
                    </div>

                </div>
            </div>

        </div>
    </form>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12"> 
                    <div class="card">
                        <h2 class="card-header">Leads List</h2>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="lead_list" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Closing Date</th>
                                                <th>Deal Name</th>
                                                <th>Site Address</th>
                                                <th>Solar System Size</th>
                                                <th>Award Date</th>
                                                <th>Date Created</th>
                                                <th>Deal Stage</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lead_list_body" >

                                        </tbody>
                                    </table>
                                </div>
                                <div id="table_loader" ></div>
                                <div  id="pagination" class="mt-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/pagination/simplePagination.css'); ?>" />
    <script type="text/javascript" src="<?php echo site_url('assets/pagination/jquery.simplePagination.js'); ?>"></script>

    <script>

        var deal_manager = function () {
            var self = this;
            this.filters = '';
            this.loading = false;
            this.page = 1;
            this.per_page = 100;
            this.pagination = false;
            this.keyword = '';

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#lead_list_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                document.getElementById('start_date').value = start.format('YYYY-MM-DD');
                document.getElementById('end_date').value = end.format('YYYY-MM-DD');
                self.pagination = false;
                $('#pagination').pagination('destroy');
                var data = $('#lead_list_filters').serialize();
                self.fetch_lead_stage_data(data);
            }

            var currentDate = moment();
            var weekStart = currentDate.clone().startOf('isoWeek');
            var weekEnd = currentDate.clone().endOf('isoWeek');
            
            var thisWeek = [];
            var lastWeek = [];
            thisWeek.push(moment(weekStart).add(0, 'days'));
            thisWeek.push(moment(weekStart).add(6, 'days'));
            lastWeek.push(moment(weekStart).add(-7, 'days'));
            lastWeek.push(moment(weekStart).add(-1, 'days'));

            $('#lead_list_range').daterangepicker({
                numberOfMonths: 6,
                startDate: start,
                endDate: end,
                ranges: {
                    'All': [moment('2016-01-01'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': thisWeek,
                    'Last Week': lastWeek,
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            self.fetch_lead_stage_data();

            $('#lead_list_filters :input').change(function () {
                self.pagination = false;
                $('#pagination').pagination('destroy');
                var data = $('#lead_list_filters').serialize();
                self.fetch_lead_stage_data(data);
            });

            $('#export_leads').click(function () {
                var data = $('#lead_list_filters').serialize();
                var url = base_url + 'admin/lead/export_csv?action=export_lead_csv&' + data;
                window.open(url, '_blank');
            });
            
            $('.reset_filter').click(function () {
                var filter_type = $(this).attr('data-id');
                $("." + filter_type).prop("checked", false);
                $("." + filter_type).trigger("change");
            });

        }


        deal_manager.prototype.create_deal_stage_table_element = function (ele, value) {
            var table_ele = document.createElement(ele);
            if (value) {
                table_ele.innerHTML = value;
            }
            return table_ele;
        };

        deal_manager.prototype.create_deal_stage_table = function (data) {
            var self = this;
            self.loading = false;
            if (data.lead_data.length == 0) {
                self.loading = true;
            }
            var table_body = document.getElementById('lead_list_body');
            if (data.lead_data.length > 0) {
                for (var i = 0; i < data.lead_data.length; i++) {
                    var tr = self.create_deal_stage_table_element('tr');
                    var stage = (data.lead_data[i].lead_stage == '' || data.lead_data[i].lead_stage == null || data.lead_data[i].lead_stage == 'null') ? '1' : data.lead_data[i].lead_stage;
                    var color = '';
                    if (data.lead_data[i].closing_date != null && parseInt(stage) == 1) {
                        var now = moment().format("YYYY-MM-DD");
                        var end = moment(data.lead_data[i].closing_date).format("YYYY-MM-DD");
                        
                        now = moment(now, "YYYY-MM-DD");
                        end = moment(end, "YYYY-MM-DD");
              
                        var diff = end.diff(now, 'days');
                        if(diff == 0){
                            color = 'bg-danger text-white';
                        }else if(diff == 1){
                             color = 'bg-amber text-white';
                        }
                    }else if(data.lead_data[i].award_date != null && parseInt(stage) == 2){
                        var now = moment().format("YYYY-MM-DD");
                        var end = moment(data.lead_data[i].award_date).format("YYYY-MM-DD");
                            
                        now = moment(now, "YYYY-MM-DD");
                        end = moment(end, "YYYY-MM-DD");
                  
                        var diff = end.diff(now, 'days');
                        if(diff > 0){
                            color = 'bg-danger text-white';
                        }
                    }
                    
                    var td_closing_date = self.create_deal_stage_table_element('td', data.lead_data[i].closing_date);
                    
                    var project_name = '<a target="__blank" href="' + base_url + 'admin/lead/add?deal_ref=' + data.lead_data[i].uuid + '">' + data.lead_data[i].project_name + '</a>' + '<span class="hidden">' + data.lead_data[i].customer_name + '</span>';
                    var td_project_name = self.create_deal_stage_table_element('td', project_name);
                    td_project_name.className = color;
                    
                    var td_address = self.create_deal_stage_table_element('td', data.lead_data[i].address);
                    
                    var td_solar_size = self.create_deal_stage_table_element('td', data.lead_data[i].solar_system_size);
                    var td_awd = self.create_deal_stage_table_element('td', data.lead_data[i].award_date);
                    
                    var date_hack = data.lead_data[i].created_at.split('/');
                    var date_hack_String = '<span class="hidden">' + date_hack[2] + date_hack[1] + date_hack[0] + '</span>';
                    var td_created_at = self.create_deal_stage_table_element('td', date_hack_String + data.lead_data[i].created_at);
                    
                    var td_stage = self.create_deal_stage_table_element('td', (data.lead_data[i].deal_stage_name) ? data.lead_data[i].deal_stage_name : '-');
                   
                    tr.appendChild(td_closing_date);
                    tr.appendChild(td_project_name);
                    tr.appendChild(td_address);
                    tr.appendChild(td_solar_size);
                    tr.appendChild(td_awd);
                    tr.appendChild(td_created_at);
                    tr.appendChild(td_stage);
                    table_body.appendChild(tr);
                }
            }
        };


        deal_manager.prototype.fetch_lead_stage_data = function (data) {
            var self = this;
            var stage_id = self.current_stage_id;
            $('#table_loader').html(createLoader('Please wait while data is being fetched.......'));
            $.ajax({
                url: base_url + 'admin/tender-lead/fetch_lead_stage_data',
                type: 'get',
                data: data + '&limit=' + self.per_page,
                dataType: 'json',
                success: function (response) {
                    if (response.success == true) {
                        if ($('#lead_list')) {
                            $('#lead_list').DataTable().clear();
                            $('#lead_list').DataTable().destroy();
                        }
                        $('#table_loader').html('');
                        self.create_deal_stage_table(response);

                        var table = $('#lead_list').DataTable({
                            orderCellsTop: true,
                            fixedHeader: true,
                            columnDefs: [
                                {type: 'date-uk', targets: 0}
                            ],
                            "order1": [
                                [0, "desc"]
                            ],
                            "bInfo": false,
                            "bLengthChange": false,
                            "bFilter": true,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'colvis',
                                    text: 'Columns',
                                    postfixButtons: ['colvisRestore'],
                                }
                            ]
                        });

                        if (self.pagination == false) {
                            $(function () {
                                $('#pagination').pagination({
                                    items: response.meta.total,
                                    itemsOnPage: self.per_page,
                                    cssStyle: "light-theme",
                                    onPageClick: function (pageNumber) {
                                        var data = $('#lead_list_filters').serialize();
                                        self.page = pageNumber;
                                        data += '&page=' + self.page;
                                        self.fetch_lead_stage_data(data);
                                    }
                                });
                            });
                            self.pagination = true;
                        }


                        //Overriding Search
                        var search_input = '<label>Search:<input type="search" id="search_leads" class="" placeholder="" aria-controls="lead_list"></label>';

                        document.getElementById("lead_list_filter").innerHTML = search_input;
                        var table = $('#lead_list').dataTable().api();

                        if (self.keyword != '') {
                            document.getElementById("search_leads").value = self.keyword;
                        }

                        $('#search_leads').change(function () {
                            var search_val = $(this).val();
                            if (search_val == '') {
                                self.pagination = false;
                            } else {
                                $('#pagination').pagination('destroy');
                            }
                            self.keyword = search_val;
                            var data = 'keyword=' + encodeURIComponent(search_val);
                            data += '&page=1';
                            self.fetch_lead_stage_data(data);
                        });


                        /** $(window).on("scroll", function () {
                         if(!self.loading){
                         var scrollHeight = $(document).height();
                         var scrollPosition = $(window).height() + $(window).scrollTop();
                         if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                         self.loading = true;
                         var data = $('#lead_list_filters').serialize();
                         self.page = self.page + 1;
                         data += '&page='+self.page+'&limit='+self.limit;
                         self.fetch_lead_stage_data(data);
                         }
                         }
                         }); */
                    } else {
                        if (response.hasOwnProperty('authenticated')) {
                            toastr['error'](response.status);
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        } else {
                            toastr["error"]('Something went wrong please try again.');
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        deal_manager.prototype.export_lead_data = function (data) {
            var self = this;
            $.ajax({
                url: base_url + 'admin/lead/export_csv',
                type: 'get',
                data: data,
                dataType: 'json',
                success: function (response) {


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        //In Prgoress
        /**
         var table = $('#lead_list').DataTable({
         orderCellsTop: true,
         fixedHeader: true,
         'processing': true,
         'serverSide': true,
         'serverMethod': 'post',
         ajax: base_url + 'admin/lead/fetch_lead_stage_data',
         columns: [
         {data: "deal_created"},
         {data: "owner_name"},
         {data: "company_name"},
         {data: "lead_stage"},
         {data: "lead_status"}
         ],
         "bInfo": false,
         "bLengthChange": false,
         "bFilter": true,
         paging: false,
         dom: 'Bfrtip',
         buttons: [
         {
         extend: 'colvis',
         text: 'Columns',
         postfixButtons: ['colvisRestore'],
         }
         ]
         });
         */

        var context = {};
        var deal_manager_tool = new deal_manager(context);
    </script>
</body>

</html>

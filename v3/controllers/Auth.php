<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Auth Controller
 * @version 1.0
 */
class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Role_Model", "role");
        $this->load->model("User_Model", "user");
    }

    public function index() {
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        } else {
            redirect('admin/dashboard');
        }
    }

    public function dashboard() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $data['user_id'] = $user_id = $this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        
        //Redirect User Group Lead Creator Only to Deal Create page.
        if($user_group == 8){
            redirect(site_url('admin/lead/add'));
        }

        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $data['is_sales_rep'] = FALSE;
        
        if ($this->aauth->is_member('Admin')) {
            $user_cond = " WHERE user_group.group_id IN(6,7)";
            $data['users'] = $this->user->get_users(FALSE, FALSE, FALSE,$user_cond);
        } else if ($this->components->is_team_leader()) {
            $user_id = $this->aauth->get_user_id();
            $data['users'] = $this->user->get_users(FALSE, FALSE, $user_id);
            if (!$this->components->is_lead_allocation_team_leader()) {
                $data['all_reports_view_permission'] = 1;
            }
        } else if ($this->components->is_sales_rep()) {
            $user_id = $this->aauth->get_user_id();
            $data['users'] = $this->user->get_users(FALSE, $user_id, FALSE);
            $data['is_sales_rep'] = TRUE;
        }
        
        $logged_in_userdata = $this->common->fetch_row('tbl_user_details','logged_in_count,id',array('user_id' => $user_id));
        if($logged_in_userdata['logged_in_count'] == 0 && $logged_in_userdata['id'] > 19){
            $this->common->update_data('tbl_user_details',array('user_id' => $user_id),array('logged_in_count' => 1));
            redirect('admin/settings/change-password');
        }
        if($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2){
            redirect('admin/tender/manage?view=pipeline');
        }

        $data['coordinates'] = $this->session->userdata('coordinates');
        $data['activity_types'] = unserialize(activity_type);

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('dashboard/main');
        $this->load->view('partials/footer');
    }

    public function login() {
        $data = array();
        if ($this->aauth->is_loggedin()) {
            redirect('admin/dashboard');
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() != FALSE) {
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                if ($this->aauth->login($email, $password)) {
                    $this->session->unset_userdata('coordinates');
                    redirect('admin/dashboard');
                } else {
                    $data['email'] = $this->input->post('email');
                    $errors = $this->aauth->get_errors_array();
                    if (!empty($errors)) {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"> ' . $errors[0] . '</div>');
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"> Invalid login Credentials. </div>');
                    }
                }
            }
        }
        $this->load->view('partials/header', $data);
        $this->load->view('auth/login');
        $this->load->view('partials/footer');
    }
    
    public function create_job_crm_login(){
        
        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            header('Content-Type: application/json; charset=UTF-8');
            echo json_encode($data);
        }
        
        $data = array();
        $user_id = $this->input->post('user_id');
        $job_crm_password = $this->input->post('job_crm_password');
        
        if (!empty($this->input->post()) && isset($job_crm_password) && $job_crm_password != '') {
            $this->load->model("Job_Model", "job");
            $response = $this->job->insert_or_update_job_crm_user($user_id,$job_crm_password);
            if($response === 'Email Exists'){
                $data['status'] = 'User with same email already exists on JOB CRM.';
                $data['success'] = FALSE;
            }else{
                $data['status'] = 'User Created Successfully';
                $data['success'] = TRUE;
            }
        }else{
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
    }
    
    
    public function login_ajax() {
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() != FALSE) {
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                if ($this->aauth->login($email, $password)) {
                    $this->session->unset_userdata('coordinates');
                    $data['is_superuser'] = FALSE;
                    if($email == 'superadmin@kugacrm.com.au'){
                        $data['is_superuser'] = TRUE;
                    }
                    $data['status'] = 'Login Successful, Redirecting to dashboard..';
                    $data['success'] = TRUE;
                } else {
                    $errors = $this->aauth->get_errors_array();
                    if (!empty($errors)) {
                        $data['status'] = $errors[0];
                    } else {
                        $data['status'] = 'Invalid login Credentials.';
                    }
                    $data['success'] = FALSE;
                }
            }else{
                $data['status'] = 'Unable to login check for error messages';
                $data['success'] = FALSE;
                $data['errors'] = $this->form_validation->error_array();
            }
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
    }

    public function logout() {
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->aauth->logout();
        $this->session->set_flashdata('message', '<div class="alert alert-success"> You have been successfully logged out.</div>');
        redirect('admin/login');
    }

    public function change_password($id = NULL) {
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        if (!$this->aauth->is_member('Admin') && $id != NULL && $id != 1) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to change the password. </div>');
            redirect('admin/login');
        }
        
        $user_id = ($id != NULL) ? $id : $this->aauth->get_user_id();
        $message = ($id != NULL) ? 'User account password' : 'Your account password';
        $redirect_uri = ($id != NULL) ? 'admin/settings/change-password/'.$id : 'admin/settings/change-password';
      
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() != FALSE) {
                $pwd = $this->input->post('password');
                $stat = $this->aauth->update_user($user_id, FALSE, $pwd, FALSE);
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> '.$message.' updated successfuly with new Password: <strong>' . $pwd . '</strong></div>');
                    redirect($redirect_uri);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the password. </div>');
                }
            } else {
                $data['password'] = $this->input->post('password');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('auth/change_password');
        $this->load->view('partials/footer');
    }

    public function manage_roles() {
        $data['roles'] = $this->common->fetch_where('aauth_groups', '*', array('id !=' => 1, 'status' => 1));
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('auth/manage_roles');
        $this->load->view('partials/footer');
    }

    public function roles_manage_permission($id) {
        $permission = $this->role->get_permission_by_roles($id);
        $data['permissions'] = $permissions = array_column($permission, 'allowed', 'name');
        $data['permission_ids'] = $permissions_ids = array_column($permission, 'id', 'name');

        if (!empty($this->input->post())) {
            $new_permissions = $this->input->post('permission');
            foreach ($permissions_ids as $key => $value) {
                if (array_key_exists($value, $new_permissions)) {
                    $perm_exists = $this->common->fetch_where('aauth_perm_to_group', '*', array('perm_id' => $value, 'group_id' => $id));
                    if (count($perm_exists) == 0) {
                        $this->common->insert_data('aauth_perm_to_group', array('perm_id' => $value, 'group_id' => $id));
                    }
                } else {
                    $this->common->delete_data('aauth_perm_to_group', array('perm_id' => $value, 'group_id' => $id));
                }
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success"> Permission Updated Successfully.</div>');
            redirect('admin/roles/manage');
        }
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('auth/roles_manage_permission');
        $this->load->view('partials/footer');
    }

    public function ban_unban_user($id = FALSE, $status = 1) {
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        
        /**if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect($this->agent->referrer());
        }*/

        if (isset($id) && $id != '' && $id != 1) {
            if ($status == 1) {
                $stat = $this->aauth->unban_user($id);
            } else {
                $stat = $this->aauth->ban_user($id);
            }
            if ($stat) {
                if ($status == 1) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success">Franchise has been successfully Un-banned.</div>');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-success">Franchise has been successfully banned.</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
            }
            redirect($this->agent->referrer());
        }
    }
    
    public function delete_user() {
        $data = array();
        if (!$this->aauth->is_member('Admin')) {
            $data['success'] = FALSE;
            $data['status'] = 'Unauthorized Access. Only admin is allowed to perform this action.';
            echo json_encode($data);
            die;
        }
        
        if(!empty($this->input->post())){
            $userid = $this->input->post('userid');
            $assign_user = $this->input->post('assign_user');
            if ($userid == '' || $userid == NULL) {
                $data['success'] = FALSE;
                $data['status'] = 'Invalid User. User Not Found.';
                echo json_encode($data);
                die;
            }
            
            //Start Trancsaction
            $this->db->trans_begin();
            
            //If Assign User is blank then Delete Leads and related data else Update
            if($assign_user != '' && $assign_user != NULL){
                $lead_ids = $this->common->fetch_where('tbl_lead_to_user','lead_id',array('user_id' => $userid));
                $lead_ids = array_column($lead_ids,'lead_id');
                //UPDATE USER ACTIVITY
                $this->db->where('user_id', $userid);
                $this->db->update('tbl_activities', array('user_id' => $assign_user));
                //UPDATE USER LEAD
                if(!empty($lead_ids)){
                    $this->db->where_in('lead_id', $lead_ids);
                    $this->db->update('tbl_lead_to_user', array('user_id' => $assign_user));
                }
                $this->db->where('user_id', $userid);
                $this->db->update('tbl_leads', array('user_id' => $assign_user));

            }else{
                $lead_ids = $this->common->fetch_where('tbl_lead_to_user','lead_id',array('user_id' => $userid));
                $lead_ids = array_column($lead_ids,'lead_id');
                
                if(!empty($lead_ids)){
                    $cust_ids = $this->common->fetch_where_in('tbl_leads','cust_id','id',$lead_ids);
                    $cust_ids = array_column($cust_ids,'cust_id');
                    $led_proposal_ids = $this->common->fetch_where_in('tbl_led_proposal','id','lead_id',$lead_ids);
                    $led_proposal_ids = array_column($led_proposal_ids,'id');
                    $solar_proposal_ids = $this->common->fetch_where_in('tbl_solar_proposal','id','lead_id',$lead_ids);
                    $solar_proposal_ids = array_column($solar_proposal_ids,'id');
                    
                    //DELETE ACTIVITY
                    $this->common->delete_batch('tbl_activities','lead_id',$lead_ids);
                    $this->db->where('user_id', $userid);
                    $this->db->delete('tbl_activities');
                    //DELETE ISSUES AND ISSUE MESSAGES
                    $this->db->where('user_id', $userid);
                    $this->db->delete('tbl_issues');
                    $this->db->where('posted_by_user_id', $userid);
                    $this->db->delete('tbl_issues_messages');
                    //DELTE LED PROPOSAL
                    $this->common->delete_batch('tbl_led_proposal_products','proposal_id',$led_proposal_ids);
                    $this->common->delete_batch('tbl_led_proposal_access_equipments','proposal_id',$led_proposal_ids);
                    $this->common->delete_batch('tbl_led_proposal','id',$led_proposal_ids);
                    //DELTE SOLAR PROPOSAL
                    $this->common->delete_batch('tbl_solar_proposal_panel_data','proposal_id',$solar_proposal_ids);
                    $this->common->delete_batch('tbl_solar_proposal_products','proposal_id',$solar_proposal_ids);
                    $this->common->delete_batch('tbl_solar_proposal','id',$solar_proposal_ids);
                    //DELETE LEAD
                    $this->common->delete_batch('tbl_lead_to_user','lead_id',$lead_ids);
                    $this->common->delete_batch('tbl_leads','id',$lead_ids);
                    //DELETE CUSTOMER
                    $this->common->delete_batch('tbl_customer_locations','cust_id' , $cust_ids);
                    $this->common->delete_batch('tbl_customer_contacts','cust_id' , $cust_ids);
                    $this->common->delete_batch('tbl_customers','id',$cust_ids);
                }
            }
            
            //Delete USER
            $this->common->delete_data('google_service_accounts',array('user_id' => $userid));
            $this->common->delete_data('tbl_user_details',array('user_id' => $userid));
            $this->common->delete_data('tbl_user_locations',array('user_id' => $userid));
            $this->common->delete_data('aauth_user_to_user',array('user_id' => $userid));
            $this->common->delete_data('aauth_user_to_user',array('user_id' => $userid));
            $this->common->delete_data('aauth_user_to_group',array('user_id' => $userid));
            $this->common->delete_data('aauth_users',array('id' => $userid));
            
            //Check Transaction status and take necessary action
            if ($this->db->trans_status() === FALSE) {
               $this->db->trans_rollback();
               $data['success'] = FALSE;
               $data['status'] = 'Looks like something went wrong. While trying to perform the action.';
            } else {
                $this->db->trans_commit();
                $data['success'] = TRUE;
                $data['status'] = 'Action Successfully Completed.';
            }
        }
        echo json_encode($data);
        die;
    }

    public function forgot_password() {
        if ($this->aauth->is_loggedin()) {
            redirect('admin/dashboard');
        }
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            if ($this->form_validation->run() != FALSE) {
                $email = $this->input->post('email');
                $user_data = $this->common->fetch_where('aauth_users', '*', array('email' => $email));
                if (!empty($user_data)) {
                    $this->aauth->remind_password($email);
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Please check your email a password reset link has been sent.</div>');
                    redirect('admin/login');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> User with the speicifed email is not found in the system. </div>');
                }
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('auth/forgot_password');
        $this->load->view('partials/footer');
    }

    public function reset_password($code) {
        if ($this->aauth->is_loggedin()) {
            redirect('admin/dashboard');
        }
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
            if ($this->form_validation->run() != FALSE) {
                $password = $this->input->post('password');
                $stat = $this->aauth->reset_password($code, $password);
                if ($stat == TRUE) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> The password has been successfully reset.</div>');
                    redirect('admin/login');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Invalid or Expired password reset link.</div>');
                    redirect('account/forgot-password');
                }
            }
        }
        $this->load->view('partials/header', $data);
        $this->load->view('auth/reset_password');
        $this->load->view('partials/footer');
    }

    public function change_availability_status() {
        $data = array();
        if (!$this->aauth->is_loggedin()) {
            $data['status'] = 'Unauthorized Access';
            $data['success'] = FALSE;
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('availability', 'Availability', 'required');
            if ($this->form_validation->run() != FALSE) {
                $availability = $this->input->post('availability');
                $availability = (isset($availability) && $availability == 'true') ? 1 : 0;
                $userid = $this->aauth->get_user_id();
                $stat = $this->common->update_data('tbl_user_details', array('user_id' => $userid), array('availability' => $availability));
                if ($stat == TRUE) {
                    $data['status'] = 'User Settings Updated Successfully';
                    $data['success'] = TRUE;
                } else {
                    $data['status'] = 'User Settings Update Failed';
                    $data['success'] = FALSE;
                }
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function google_calendar() {
        $data = array();
        $userid = $this->aauth->get_user_id();
        $is_account_exists = $this->common->num_rows('google_service_accounts', array('service_id' => 1, 'user_id' => $userid, 'status' => 1));
        $data['is_google_account_connected'] = (isset($is_account_exists) && $is_account_exists > 0) ? 'true' : 'false';
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('settings/google_calendar');
        $this->load->view('partials/footer');
    }

    public function changelog() {
        $this->load->view('partials/header');
        $this->load->view('partials/changelog');
        $this->load->view('partials/footer');
    }

    public function set_current_latlng(){
        //Testing lat: -37.89754400 lng: 145.14004200
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $coordinates = array('lat' => $lat, 'lng' => $lng);
        $this->session->set_userdata('coordinates',$coordinates);
        echo json_encode(array('success' => TRUE));
        die;
    }

}

<style>
    .page-wrapper {background-color: #ECEFF1; min-height: 100%;}
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
    .sr-inactive_user{color:red;}
    .tooltip {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 12px;
        font-style: normal;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: left;
        text-align: start;
        text-decoration: none;
        text-shadow: none;
        text-transform: none;
        letter-spacing: normal;
        word-break: normal;
        word-spacing: normal;
        word-wrap: normal;
        white-space: normal;

    }
</style>
<body>
    <!--pagewrapper-->
    <div class="page-wrapper d-block clearfix">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Lead Allocation Reporting</h1>
            <div class="content-header-right col-md-6 col-12 ">
                <form id="reporting_actions" >

                    <?php if ($this->aauth->is_member('Admin')) { ?>
                        <div class="btn-group col-md-7" role="group" >
                            <div id="reportrange" >
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                            <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <div class="float-md-right col-md-5">
                            <select class="form-control" name="state_id" id="state_id">
                                <option value="">Select State</option>
                                <?php $states = unserialize(STATE_FILTER);
                                foreach ($states as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
                            </select>
                        </div>
                        <div  class="float-md-right col-md-5 hidden" style="display:inline-flex;">
                            <select class="" name="userid" id="userid">
                                <option value="">Select User</option>
                                <?php foreach ($users as $key => $value) { ?>
                                    <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
    <?php } ?>
                            </select>
                            <div class="form-check-inline" style="margin-left:5px;">
                                <input type="checkbox" class="form-check-input sr-show_inactive_users" value="1">
                                <i class="fa fa-user-times"></i>
                            </div>
                        </div>
                    <?php } else if ($all_reports_view_permission) { ?>
                        <div class="btn-group col-md-8" role="group" >
                            <div id="reportrange" >
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                            <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <div  class="float-md-right col-md-4" >
                            <select class="" name="userid" id="userid">
                                <option value="">Select User</option>
                                <?php foreach ($users as $key => $value) { ?>
                                    <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                    <?php } ?>
                            </select>  
                        </div>
<?php } else { ?>
                        <div class="btn-group float-md-right" role="group" >
                            <div id="reportrange" >
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                            <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
<?php } ?>
                </form>
            </div>
        </div>


        <div class="container hidden" id="error_container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-template">
                        <div class="error-details">
                            Sorry, you don't have permissions to access reports!
                        </div>
                        <div class="error-actions">
                            <a href="proposalManage.php" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-pencil"></span>
                                Manage proposal </a><a href="mailto:service@solarrun.com.au" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="statistics_container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-body">
                                <h2>
                                    Sales Statistics
                                    <a href="#" data-toggle="tooltip" title="Statistics for leads and conversion rate." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                </h2>
                                <div id="sales_statistics_chart_container"></div>
                                <div  class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Lead Allocation Rep</th>
                                                <th>State</th>
                                                <th>Total Deals</th>
                                                <th>Conversion Rate</th>
                                                <th>Won Deals</th>
                                                <th>kWs Sold</th>
                                                <th>Highbays Sold</th>
                                                <th>Floodlights Sold</th>
                                                <th>Panels Sold</th>
                                                <th>Battens Sold</th>
                                            </tr>
                                        </thead>
                                        <tbody id="sales_rep_performance_report_table_body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

     

                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-body">
                                <h2>
                                    Activity  Statistics (Productivity)
                                    <a href="#" data-toggle="tooltip" title="Productivity Statistics for lead allocation memeber." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                </h2>
                                <div  class="table-responsive" id="sales_rep_activity_chart_container"></div>
                                <div  class="table-responsive mt-5" id="sales_rep_activity_table_container">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script src="<?php echo site_url(); ?>common/js/reports/lead_allocation.js?v=<?php echo version; ?>"></script>

    <script type="text/javascript">
        var context = {};
        context.user_id = '<?php echo $user_id; ?>';
        context.user_group = '<?php echo $user_group; ?>';
        context.all_reports_view_permission = '<?php echo $all_reports_view_permission; ?>';
        var lead_allocation_reporting_tool = new lead_allocation_reporting(context);

    </script>
</body>
</html>


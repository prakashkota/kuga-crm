<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
    i#deleteFile{
        color: #b92625 !important;
        cursor: pointer !important;
    }
</style>
<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Screenshot of Pricing Tool</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="additional_photos_2" />
                                        <input type="hidden" required id="additional_photos_2" name="booking_form_image[additional_photos_2]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top">
                                    <textarea id="notes" name="booking_form[notes]" style="padding:5px; width:100%; height: 350px;" ></textarea>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="163" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Pricing Sheet</td>
                                <td width="667" valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
                                    <input type="file" class="image_upload" data-id="solarfiles"  id="upload" hidden/>
                                    <label for="upload"  class="btn">Upload</label>
                                    <input type="hidden" id="solarfiles" name="booking_form_image[solarfiles]" />
                                    <span id="solarfilesName"></span>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/solar_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
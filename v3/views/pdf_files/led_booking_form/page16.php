<?php $customer_check_list = json_decode(json_encode($customer_check_list),true); ?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_02.jpg'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101;color:#ffffff; font-family: Calibri, sans-serif; font-size:15px"><strong>Check List</strong></td>
                </tr>
                <tr>
                    <td height="30" class="black-bg" style="color:#333333; font-family: Calibri, sans-serif; font-size:15px"><strong>Please write all relevant conversations with customers in the Note Section:</strong></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[0])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">1.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Have you explained the disclaimers?</td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[1])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">2.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Have you had pricing discussions with the customer. If so what was discussed?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[2])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">3.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Have you shown Samples to the client?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[3])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">3a.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Do any need to be installed?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[4])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">4.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Access issues? including the provisioning of equipment and fees associaated with the supply of faulty or dangerous<br />
                                    units by customer.</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[5])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">5.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Scissor lift/Boom lift accessibility?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[6])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">6.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Has the customer agreed to clear access? Be specific, what areas?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[7])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">7.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Are all the fittings to be upgraded working?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[8])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">8.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Are there any globes missing?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[9])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">8a.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Have you asked the customer to replace any missing globes?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[10])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">9.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Have you explained the difference between Electronic and Magnetic ballast and how this may affect pricing?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[11])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">10.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">What are the access times?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[12])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">11.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Are there induction requirements?</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[13])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">12.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Is there someone else we need to organise this with? i.e. Centre Management</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td style="color:#333333; font-family: Calibri, sans-serif; font-size:14px"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="49">
                                    <?php if (isset($customer_check_list[14])) { ?>
                                        <input type="checkbox" class="checkbox_light" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox_light"  />
                                    <?php } ?>
                                </td>
                                <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">13.</td>
                                <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">Are areas vacant at certain times?</td>
                            </tr>
                        </table></td>
                </tr>

    <tr>
        <td height="10"></td>
    </tr>
    <tr>
        <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="49">
                        <?php if (isset($customer_check_list[15])) { ?>
                            <input type="checkbox" class="checkbox_light" checked="" />
                        <?php } else { ?>
                            <input type="checkbox" class="checkbox_light"  />
                        <?php } ?>
                    </td>
                    <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">14.</td>
                    <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">is the customer aware that the payment is required on day of completion?</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="10"></td>
    </tr>
    <tr>
        <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="49">
                        <?php if (isset($customer_check_list[16])) { ?>
                            <input type="checkbox" class="checkbox_light" checked="" />
                        <?php } else { ?>
                            <input type="checkbox" class="checkbox_light"  />
                        <?php } ?>
                    </td>
                    <td width="26" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">15.</td>
                    <td width="755" style="font-family: Calibri, sans-serif; font-size:14px; color:#333333">If there has been a previous upgrade, have the details been recorded on the RCP?</td>
                </tr>
            </table></td>
    </tr>
             
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; font-size:15px"><strong>Important Notes</strong></td>
    </tr>
    <tr>
        <td valign="top" height="150" style="color:#333333; font-family: Calibri, sans-serif; font-size:14px; border:solid 1px #e4e4e4; padding:5px">
            <strong>
            <?php echo $customer_check_list['notes'];  ?>
            </strong>
        </td>
    </tr>
       </table></td>
    </tr>
    
     <tr>
    <td height="45">&nbsp;</td>
  </tr>

    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 4 Bridge Rd, Keysborough VIC 3173</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 6 Turbo Rd, Kings Park, NSW 2148</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 31 Chetwynd St, Loganholme, QLD 4129 </td>
        </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />16</td>
                </tr>
            </table></td>
    </tr>
</table>
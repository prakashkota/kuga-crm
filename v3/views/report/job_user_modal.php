<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>
<style type="text/css">
  #columnSelector > .btn-group{height: 43px;}
 .modal-xl {max-width: 90%;}
</style>
<div id="job_list_modal" class="modal fade"  role="dialog" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Jobs</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body job_section">
                <div class="tbl-block">
                    <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                      <table class="table table-bordered">
                        <thead style="background:#dee2e6; color:#000;">
                          <tr>
                            <th class="tbl_col_header_0">Status</th>
                            <th class="tbl_col_header_1">Sub-Status</th>
                            <th class="tbl_col_header_2">Cost Centre</th>
                            <th class="tbl_col_header_3">Job #</th>
                            <th class="tbl_col_header_4">Business Name</th>
                            <th class="tbl_col_header_16">Sales Person</th>
                			<th class="tbl_col_header_17">Date Set to Procesing </th>
                			<th class="tbl_col_header_18">Date Completed</th>
                            <th class="tbl_col_header_6">Electricians</th>
                            <th class="tbl_col_header_19">Invoice Count</th>
                            <th class="tbl_col_header_7">kW</th>
                            <th class="tbl_col_header_8">HB</th>
                            <th class="tbl_col_header_9">FLs</th>
                            <th class="tbl_col_header_10">PLs</th>
                            <th class="tbl_col_header_11">B</th>
                            <th class="tbl_col_header_12">DLs</th>
                            <th class="tbl_col_header_13">Other</th>
                            <th class="tbl_col_header_14">Certificates</th>
                            <th class="tbl_col_header_15">Certificate Value</th>
                            <th>Total Certificate Value</th>
                          </tr>
                        </thead>
                        <tbody id="job_table_body">
                
                        </tbody>
                      </table>
                    </div>
                  </div>
                <div id="pagination"></div>
                <div class="tbl-block mt-4">
                  <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                    <table class="table table-bordered">
                      <thead style="background:#dee2e6; color:#000;">
                        <tr>
                          <th>Cost Centre</th>
                          <th>kW</th>
                          <th>HB</th>
                          <th>FLs</th>
                          <th>PLs</th>
                          <th>B</th>
                          <th>DLs</th>
                          <th>Other</th>
                          <th>Certificates</th>
                          <th>Certificate Value</th>
                          <th>Total Certificate Value</th>
                        </tr>
                      </thead>
                      <tbody id="job_product_count_table_body">
                
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="job_invoice_count_details_modal" class="modal fade"  role="dialog" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invoice Linked Details</h5>
                <div class="ml-4">
                    <a href="javascript:void(0);" target="__blank" id="modal_link_job_invoice_section" class="btn btn-kuga"><i class="fa fa-external-link"></i> Go to Job Invoice Section</a>
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body job_section">
                <div class="form-block d-block clearfix additional-contact2">
                    <div class="table-responsive">
                        <table width="100%" bordercolor="#707070" border="1" class="table table-striped" style="border-collapse:collapse; border:solid 1px #707070">
                            <thead>
                                <tr>
                                    <th>Xero Invoice #</th>
                                    <th>User</th>
                                    <th>To</th>
                                    <th>Ref</th>
                                    <th>Date Invoiced</th>
                                    <th>Due Date</th>
                                    <th>Paid Inc. GST</th>
                                    <th>Due Inc. GST</th>
                                    <th>Status</th>
                                    <th>Sent</th>
                                </tr>
                            </thead>
                            <tbody id="invoice_count_table_body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



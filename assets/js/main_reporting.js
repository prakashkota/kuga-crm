var reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.start_date = '';
    this.end_date = '';

    if (self.user_group != '1' && self.all_reports_view_permission == '0') {
        $('#statistics_container').hide();
        $('#error_container').removeClass('hidden');
        $('#reporting_actions').addClass('hidden');
        $('.page-wrapper').attr('style', 'height:100% !important;');
    } else {
        $(function () {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                document.getElementById('start_date').value = start.format('YYYY-MM-DD');
                document.getElementById('end_date').value = end.format('YYYY-MM-DD');
                self.start_date = start.format('d/M/Y');
                self.end_date = end.format('d/M/Y');
                self.re_initialize();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
            self.fetch_franchise_performance_chart_data();

        });

        $('.sr-show_inactive_users').click(function () {
            $('.sr-inactive_user').addClass('hidden');
            if ($(this).prop("checked") == true) {
                $('.sr-inactive_user').removeClass('hidden');
            }
        });

        $('#userid').on('change', function () {
            self.re_initialize();
        });
    }

};


reporting.prototype.initialize = function () {
    var self = this;
    document.getElementById('franchise_performance_report_table_body').innerHTML = '';
    document.getElementById('franchise_performance_report_table_body').appendChild(createLoader('Fetching franchisee nformation....'));
    self.fetch_franchise_performance_data();
    self.fetch_franchise_performance_chart_data();
}

reporting.prototype.re_initialize = function () {
    var self = this;
    document.getElementById('franchise_performance_report_table_body').innerHTML = '';
    document.getElementById('franchise_performance_report_table_body').appendChild(createLoader('Fetching franchisee nformation....'));
    self.fetch_franchise_performance_data();
}


reporting.prototype.fetch_franchise_performance_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_franchise_performance_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('franchise_performance_report_table_body').innerHTML = '';
            if (data.stats.length > 0) {
                var total_leads = 0;
                var total_allocated_leads = 0;
                var total_self_generated_leads = 0;
                var total_conv_rate = 0;
                var total_won_leads = 0;
                var total_revenue = 0;
                var total_kws = 0;
                for (var i = 0; i < data.stats.length; i++) {
                    if (data.stats[i]['franchise_email']) {
                        var stats = self.create_franchise_performance_report_data(data.stats[i]);
                        document.getElementById('franchise_performance_report_table_body').appendChild(stats);

                        var self_generated_leads = data.stats[i].self_generated;
                        var allocated_leads = data.stats[i].allocated_leads;
                        var won_allocated_leads = data.stats[i].won_allocated_leads;
                        var won_self_generated_leads = data.stats[i].won_self_generated_leads;
                        var won_total_leads = parseInt(won_allocated_leads) + parseInt(won_self_generated_leads);
                        var conv_rate = 0;
                        total_self_generated_leads = parseInt(total_self_generated_leads) + parseInt(self_generated_leads);
                        total_allocated_leads = parseInt(total_allocated_leads) + parseInt(allocated_leads);
                        var sum_of_leads = parseInt(self_generated_leads) + parseInt(allocated_leads);
                        total_leads = parseInt(total_leads) + parseInt(sum_of_leads);
                        if (sum_of_leads != 0 && won_total_leads != 0) {
                            var conv = (won_total_leads / sum_of_leads);
                            conv_rate = conv.toFixed(2);
                            total_conv_rate = parseFloat(total_conv_rate) + parseFloat(conv_rate);
                            total_conv_rate = parseFloat(total_conv_rate).toFixed(2);
                        }
                        total_won_leads = parseInt(total_won_leads) + parseInt(won_total_leads);
                        total_revenue = parseFloat(total_revenue) + parseFloat(data.stats[i].total_revenue);
                        total_revenue = parseFloat(total_revenue).toFixed(2);
                        total_kws = parseFloat(total_kws) + parseFloat(data.stats[i].total_kws);
                        total_kws = parseFloat(total_kws).toFixed(2);
                    }
                }

                //For Total
                var tr_total = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var td5 = document.createElement('td');
                var td6 = document.createElement('td');
                var td7 = document.createElement('td');
                var td8 = document.createElement('td');
                //var td3 = document.createElement('td');
                td1.innerHTML = '<b>Total</b>';
                td1.setAttribute('colspan', '3');
                tr_total.appendChild(td1);
                td2.innerHTML = total_leads;
                td3.innerHTML = total_allocated_leads;
                td4.innerHTML = total_self_generated_leads;
                var avg_conv = parseFloat(total_conv_rate/data.franchise_count)  * 100;
                avg_conv = avg_conv.toFixed(2);
                td5.innerHTML =  avg_conv + '%';
                td6.innerHTML = total_won_leads;
                td7.innerHTML = '$' + total_revenue;
                td8.innerHTML = total_kws + ' kW';
                tr_total.appendChild(td2);
                tr_total.appendChild(td3);
                tr_total.appendChild(td4);
                tr_total.appendChild(td5);
                tr_total.appendChild(td6);
                tr_total.appendChild(td7);
                tr_total.appendChild(td8);
                document.getElementById('franchise_performance_report_table_body').appendChild(tr_total);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('franchise_performance_report_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

reporting.prototype.create_franchise_performance_report_data = function (data) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.franchise_company_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.state_postal;

    var td3 = document.createElement('td');
    td3.innerHTML = data.franchise_name;

    var self_generated_leads = data.self_generated;
    var allocated_leads = data.allocated_leads;
    var total_leads = parseInt(self_generated_leads) + parseInt(allocated_leads);

    var td4 = document.createElement('td');
    td4.innerHTML = total_leads;

    var td5 = document.createElement('td');
    td5.innerHTML = allocated_leads;

    var td6 = document.createElement('td');
    td6.innerHTML = self_generated_leads;

    var won_allocated_leads = data.won_allocated_leads;
    var won_self_generated_leads = data.won_self_generated_leads;
    var won_total_leads = parseInt(won_allocated_leads) + parseInt(won_self_generated_leads);

    var conv_rate = 0;
    if (total_leads != 0 && won_total_leads != 0) {
        var conv = (won_total_leads / total_leads) * 100;
        conv = conv.toFixed(2);
        conv_rate = conv + '%';
    }

    var td7 = document.createElement('td');
    td7.innerHTML = conv_rate;

    var td8 = document.createElement('td');
    td8.innerHTML = won_total_leads;

    var td9 = document.createElement('td');
    td9.innerHTML = '$' + data.total_revenue;

    var td10 = document.createElement('td');
    td10.innerHTML = data.total_kws + ' kW';

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);
    tr.appendChild(td7);
    tr.appendChild(td8);
    tr.appendChild(td9);
    tr.appendChild(td10);

    return tr;
};

reporting.prototype.fetch_franchise_performance_chart_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    $.ajax({
        url: base_url + "admin/reports/fetch_franchise_performance_chart_data?",
        type: 'get',
        dataType: "json",
        beforeSend: function () {
            document.getElementById('franchise_kw_sold_statistics').appendChild(createLoader('Preparing Chart Information....'));
            document.getElementById('franchise_conv_statistics').appendChild(createLoader('Preparing Chart Information....'));
            document.getElementById('franchise_lead_statistics').appendChild(createLoader('Preparing Chart Information....'));
        },
        success: function (data) {
            google.charts.load('current', {'packages': ['line', 'bar']});
            google.charts.setOnLoadCallback(function () {
                self.create_performance_chart(data);
            });

            var kw_table_obj = self.create_franchise_chart_table();
            var kw_table = self.create_franchise_chart_table_body(kw_table_obj, data, data.kw_stats, data.kw_stats_table);
            document.getElementById('franchise_kw_sold_statistics_table_container').appendChild(kw_table);

            var conv_table_obj = self.create_franchise_chart_table();
            var conv_table = self.create_franchise_chart_conversion_table_body(conv_table_obj, data, data.conversion_stats, data.conversion_stats_table);
            document.getElementById('franchise_conv_statistics_table_container').appendChild(conv_table);

            var lead_table_obj = self.create_franchise_chart_table();
            var lead_table = self.create_franchise_chart_table_body(lead_table_obj, data, data.lead_stats, data.lead_stats_table);
            document.getElementById('franchise_lead_statistics_table_container').appendChild(lead_table);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('franchise_kw_sold_statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('franchise_conv_statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('franchise_lead_statistics').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

reporting.prototype.create_performance_chart = function (report_data) {
    var self = this;
    var lead_list = [];
    var kws_list = [];
    var conv_list = [];
    var list_0 = ["Date"];
    var conv_list_0 = ["Date", "Average"];
    var dateTicks1 = [];
    var dateTicks2 = [];

    for (var i = 0; i < report_data.franchises.length; i++) {
        list_0.push(report_data.franchises[i].company_name);
        conv_list_0.push(report_data.franchises[i].company_name);
    }
    lead_list[0] = list_0;
    kws_list[0] = list_0;
    conv_list[0] = conv_list_0;

    /** Prepare Lead Statistics and kw sold Data */
    for (var i = 0; i < report_data.lead_stats.length; i++) {
        var stat_0 = new Date(report_data.lead_stats[i][0]);
        dateTicks1.push(stat_0);
        report_data.lead_stats[i][0] = stat_0;
        report_data.kw_stats[i][0] = stat_0;
        lead_list[(i + 1)] = report_data.lead_stats[i];
        kws_list[(i + 1)] = report_data.kw_stats[i];
    }

    /** Create kw sold Chart */
    var kws_data = google.visualization.arrayToDataTable(kws_list);
    var kws_chart_options = {
        hAxis: {
            format: 'MMM yyyy',
            ticks: dateTicks1,
            scaleType: 'continuous'
        },
        legend: {position: 'left', textStyle: {fontSize: 12}},
    };

    var kws_chart = new google.charts.Line(document.getElementById('franchise_kw_sold_statistics'));
    kws_chart.draw(kws_data, google.charts.Line.convertOptions(kws_chart_options));

    /** Create lead  Chart */
    var lead_data = google.visualization.arrayToDataTable(lead_list);
    var lead_chart_options = {
        hAxis: {
            format: 'MMM yyyy',
            ticks: dateTicks1,
            scaleType: 'continuous'
        },
        legend: {position: 'left', textStyle: {fontSize: 12}},
    };

    var lead_chart = new google.charts.Line(document.getElementById('franchise_lead_statistics'));
    lead_chart.draw(lead_data, google.charts.Line.convertOptions(lead_chart_options));

    /** Prepare Conversion Statistics Data */
    var conversion_stats_data = report_data.conversion_stats;
    for (var i = 0; i < conversion_stats_data.length; i++) {
        var stat_0 = new Date(conversion_stats_data[i][0]);
        dateTicks2.push(stat_0);
        var conv_stats = [];
        conv_stats[0] = stat_0;
        var sum = 0;
        for (var j = 1; j < conversion_stats_data[i].length; j++) {
            sum += parseInt(conversion_stats_data[i][j]);
            conv_stats[(j + 1)] = (conversion_stats_data[i][j]) * 100;
        }
        var avg = (sum / conversion_stats_data[i].length) * 100;
        avg = avg.toFixed(2);
        avg = parseFloat(avg);
        conv_stats[1] = avg;
        conv_list[(i + 1)] = conv_stats;
    }


    /** Create Conversion  Chart */
    var conv_data = google.visualization.arrayToDataTable(conv_list);
    var conv_chart_options = {
        hAxis: {
            format: 'MMM yyyy',
            ticks: dateTicks2,
            scaleType: 'continuous'
        },
        vAxis: {
            minValue: 0,
            maxValue: 100,
            format: '#\'%\''
        },
        seriesType: 'line',
        series: {0: {type: 'bars'}},
        legend: {position: 'left', textStyle: {fontSize: 12}},
    };

    var conv_chart = new google.charts.Line(document.getElementById('franchise_conv_statistics'));
    conv_chart.draw(conv_data, google.charts.Line.convertOptions(conv_chart_options));
};

reporting.prototype.get_past_months = function (monthsRequired) {
    var self = this;
    var months = [];
    for (var i = 0; i < monthsRequired; i++) {
        months.push(moment().subtract(i, 'months').format('MMM YYYY'));
    }
    return months;
};

reporting.prototype.create_franchise_chart_table = function () {
    var self = this;

    var table = document.createElement('table');
    table.className = 'table table-stripped';

    var thead = document.createElement('thead');

    var months = self.get_past_months(12);

    var theads_array = ['Franchise', 'State'];

    var tr = document.createElement('tr');
    for (var i = 0; i < theads_array.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = theads_array[i];
        tr.appendChild(th);
        thead.appendChild(tr);
    }


    for (var i = (months.length - 1); i >= 0; i--) {
        var th = document.createElement('th');
        th.innerHTML = months[i];
        tr.appendChild(th);
        thead.appendChild(tr);
    }

    table.appendChild(thead);
    return table;
};

reporting.prototype.create_franchise_chart_table_body = function (table, data, chart_data, table_data) {
    var self = this;
    var tbody = document.createElement('tbody');
    for (var i = 0; i < table_data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        var td3 = document.createElement('td');

        if (data.franchises[i]) {
            td1.innerHTML = data.franchises[i].company_name;
            td2.innerHTML = data.franchises[i].state_postal;
            //td3.innerHTML = data.franchises[i].full_name;
        } else {
            td1.innerHTML = '-';
            td2.innerHTML = '-';
            //td3.innerHTML = '-';
        }
        tr.appendChild(td1);
        tr.appendChild(td2);
        //tr.appendChild(td3);

        for (var j = (table_data[i].length - 1); j >= 0; j--) {
            var td = document.createElement('td');
            td.innerHTML = self.format_number(table_data[i][j]);
            tr.appendChild(td);
            tbody.appendChild(tr);
        }
    }

    //For Total
    var tr_total = document.createElement('tr');
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    //var td3 = document.createElement('td');
    td1.innerHTML = '<b>Total</b>';
    td2.innerHTML = '-';
    //td3.innerHTML = '-';
    tr_total.appendChild(td1);
    tr_total.appendChild(td2);
    //tr_total.appendChild(td3);
    for (var i = (chart_data.length - 1); i >= 0; i--) {
        var td = document.createElement('td');
        var sum = 0;
        for (var j = 1; j < chart_data[i].length; j++) {
            sum += parseFloat(chart_data[i][j]);
        }
        td.innerHTML = self.format_number(sum);
        tr_total.appendChild(td);
    }
    tbody.appendChild(tr_total);
    table.appendChild(tbody);

    return table;
}

reporting.prototype.create_franchise_chart_conversion_table_body = function (table, data, chart_data, table_data) {
    var self = this;
    var tbody = document.createElement('tbody');
    for (var i = 0; i < table_data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        var td3 = document.createElement('td');

        if (data.franchises[i]) {
            td1.innerHTML = data.franchises[i].company_name;
            td2.innerHTML = data.franchises[i].state_postal;
            //td3.innerHTML = data.franchises[i].full_name;
        } else {
            td1.innerHTML = '-';
            td2.innerHTML = '-';
            //td3.innerHTML = '-';
        }
        tr.appendChild(td1);
        tr.appendChild(td2);
        //tr.appendChild(td3);
        for (var j = (table_data[i].length - 1); j >= 0; j--) {
            var td = document.createElement('td');
            var conv = (table_data[i][j] * 100);
            conv = conv.toFixed(2);
            conv = self.format_number(conv);
            td.innerHTML = conv + '%';
            tr.appendChild(td);
            tbody.appendChild(tr);
        }
    }

    //For Total
    var tr_total = document.createElement('tr');
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    //var td3 = document.createElement('td');
    td1.innerHTML = '<b>Total</b>';
    td2.innerHTML = '-';
    //td3.innerHTML = '-';
    tr_total.appendChild(td1);
    tr_total.appendChild(td2);
    //tr_total.appendChild(td3);
   
    for (var i = (chart_data.length - 1); i >= 0; i--) {
        var td = document.createElement('td');
        var sum = 0;
        for (var j = 1; j < chart_data[i].length; j++) {
            sum = parseFloat(sum) + parseFloat(chart_data[i][j]);
        }
        var avg = (sum / (chart_data[i].length - 1)) * 100;
        //avg = avg.toFixed(2);
        avg = self.format_number(avg);
        td.innerHTML = avg + '%';
        tr_total.appendChild(td);
    }
    tbody.appendChild(tr_total);
    table.appendChild(tbody);

    return table;
};

reporting.prototype.format_number = function (number) {
    return Number(parseInt(number)).toLocaleString('en');
};
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Business Details</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Company Name:</td>
                                            <td width="283" valign="top" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;"><strong><?php echo $business_details->entity_name; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" ><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">ABN/ACN:</td>
                                            <td width="283" valign="top" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;"><strong><?php echo $business_details->abn; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Trading Name:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->trading_name; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" ><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Leased or Owned:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->leased_or_owned; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Street Address:</td>
                                <td width="708" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->address; ?></strong></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <?php /*
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Suburb:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->suburb; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Postcode:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->postcode; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <?php */ ?>
                <?php if(isset($business_details->wifi_username)){ ?>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Wifi Username:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->wifi_username; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Wifi Password:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->wifi_password; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">IT Dept. Email:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->it_dept_email; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">IT Dept. Phone:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->it_dept_phone; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Firewall:</td>
                                            <td width="283" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if (isset($business_details->is_firewall) && $business_details->is_firewall == '1') { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                    <label class="form-check-label" for="is_firewall_1">YES</label>
                                                 <?php if (isset($business_details->is_firewall) && $business_details->is_firewall == '0') { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/non_tick_small.jpg'; ?>" width="19" height="20" />
                                                    <?php } ?>
                                                    <label class="form-check-label" for="is_firewall_1">NO</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">&nbsp;</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <?php } ?>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Authorised Details</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">First Name:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->first_name; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Last Name:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->last_name; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Position:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->position; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Email:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->email; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Mobile Number:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->contact_no; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Other Number:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->other_contact_no; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Electricity Bill Information</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Calibri, sans-serif; font-size:14px;">Retailer:</td>
                                            <td width="240" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_retailer; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="154" style="font-family:Calibri, sans-serif; font-size:14px;">NMI:</td>
                                            <td width="251" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_nmi; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Calibri, sans-serif; font-size:14px;">Distributor:</td>
                                            <td width="240" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_distributor; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="154" style="font-family:Calibri, sans-serif; font-size:14px;">Meter Number:</td>
                                            <td width="251" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_meter_no; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Calibri, sans-serif; font-size:14px;">Account Name:</td>
                                            <td width="240" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_acc_name; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="154" style="font-family:Calibri, sans-serif; font-size:14px;">Account Number:</td>
                                            <td width="251" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_acc_no; ?></strong></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="50">&nbsp;</td>
    </tr>
    <tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />2</td>
      </tr>
    </table></td>
  </tr>
</table>

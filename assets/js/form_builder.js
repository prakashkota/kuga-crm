var form_builder = function(options) {
    var self = this;
    this.template_id = options.template_id;
    this.field_counter = 0;
    document.onreadystatechange = function() {
        if (document.readyState == "interactive") {
            //$("#loader").show();
            $("#page-container").find('img').each(function() {
                $(this).attr('src', base_url + "assets/form_builder/uploads/"+self.template_id+"/" + $(this).attr('src'));
            });
        }
    };

    $(".save_btn").on('click', function() {
        self.save_template_fields();
    });

    $(".delete_btn").on('click', function() {
        self.delete_template_fields();
    });

    $("#radioElement,#selectElement").on('click', function() {
        $(this).next().toggle('slow');
    });

    $("#checkboxElement").on('click', function() {
        $(this).next().slideToggle();
    });

    if (self.template_id != '') {
        self.load_template();
    }

}

form_builder.prototype.hoverdiv = function(e, divid, type) {
    var div = document.getElementById(divid);
    if (type == 'show') {
        $("#" + divid).addClass('changeCursor');
        div.style.opacity = 1;
    } else {
        $("#" + divid).removeClass('changeCursor');
        div.style.opacity = 0;
    }
    return false;
};

form_builder.prototype.initialize = function() {
    var self = this;

    function trigger_click() {
        self.trigger_save_template_fields();
    }

    $.getScript(base_url + "assets/form_builder/js/jquery-ui.min.js").done(function(script, textStatus) {

        $("#loader").hide();

        $("#page-container").css('overflow', 'hidden');
        //$("#pf1").css('margin','0px');

        var wcount = 0;
        $("#page-container div[id^='pf']").each(function() {
            wcount = parseInt(wcount + $(this).height());
        });
        $("#page-container").height(wcount)

        $("input,textarea,div.signData,select").removeClass('focused');

        // Slide up and down 
        $("#element-style").click(function() {
            $(this).parent().parent().find('.element-form-slide').slideUp();
        });

        $(".form-max").keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
            $(".focused").attr('maxlength', $(this).val());
        });


        $(".form-label").keyup(function() {
            $(".focused").attr('label', $(this).val());
        });

        $("#chklabel .form-group").keyup(function() {
            $("input[name='" + $(this).attr('name') + "']").attr('attr-label', $(this).val());
        });

        $(".form-required").change(function() {
            //console.log($(".focused").attr("type"));
            if ($(this).is(":checked")) {
                $(".focused").attr('required', true);
                $(".focused").addClass('required');
            } else {
                $(".focused").attr('required', false);
                $(".focused").removeClass('required');
            }
        });

        $(".form-readonly").change(function() {
            //console.log($(".focused").attr("type"));
            if ($(this).is(":checked")) {
                $(".focused").attr('readonly', true);
                $(".focused").addClass('readonly');
            } else {
                $(".focused").attr('readonly', false);
                $(".focused").removeClass('readonly');
            }
        });

        $(".pull-right").click(function() {
            $(".form-element").hide('slow');
            $(".scroll-box2").show('slow');
        });

        $("#form-element").click(function() {
            $(".form-element").show('slow');
            $(".scroll-box2").hide('slow');
        });

        $(document).on('click', '.addnewCheckboxElement', function() {
            $(".checkbox").append("<li class='ui-sortable-handle'><input type='checkbox' class='option-selected' value='true' name='radio-group-1496836717607-option'><input type='text' class='option-value' value='' placeholder='Value'><a class='removenewElement btn' title='Remove Element'>×</a></li>");
            $(".removenewElement").on('click', function() {
                $(this).parent().remove();
            });
        });

        $("#scrollbox2 div").hover(function() {
            $("#scrollbox2 div").removeClass('active');
            $(this).addClass('active');
        });

        /* Element dragable functioality */
        $("#scrollbox2 div").draggable({
            helper: "clone",
            revert: "invalid",
            refreshPositions: true,
            drag: function(event, ui) {
                $("#scrollbox2").css('overflow', 'inherit');
                ui.helper.addClass("draggable");
                $("input,textarea,div.signData,select").removeClass('focused');
                dragElement = $(this).attr('id');
                dragLabelElement = $(this).attr('title');
                dragValElement = $(this).attr('val');
            },
            stop: function(event, ui) {
                ui.helper.removeClass("draggable");
                $("#scrollbox2").css('overflow', 'hidden');
            }
        });

        /* Here we can drop the element */
        $(".pf").droppable({
            accept: '#scrollbox2 div',
            drop: function(event, ui) {
                var attribute = '';
                var $newPosX = (100 * parseFloat((ui.offset.left - $(this).offset().left) / parseFloat($("#" + $(event.target).attr('id')).width()))) + "%";
                var $newPosY = (100 * parseFloat((ui.offset.top - $(this).offset().top) / parseFloat($("#" + $(event.target).attr('id')).height()))) + "%";
                var uival = (ui.offset.top - $(this).offset().top);
                switch (dragElement) {
                    case "textElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;min-height:32px;"><input type="text" class="css-class-name" data-page-id=' + $(event.target).attr('data-page-no') + '></div>';
                        $("#read").css("display", "block");
                        break;
                    case "counterTextElement":
                        attribute = '<div class="form-input-group counter form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;max-height:32px; min-height:32px;"><input type="text" class="css-class-name counter" style="max-height:32px;" data-page-id=' + $(event.target).attr('data-page-no') + '></div>';
                        break;
                    case "dateElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;max-height:32px; min-height:32px;"><input type="text" class="css-class-name date" style="max-height:32px;" data-page-id=' + $(event.target).attr('data-page-no') + ' readonly></div>';
                        break;
                    case "counterDateElement":
                        attribute = '<div class="form-input-group form-resizable counter" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;max-height:32px; min-height:32px;"><input type="text" class="css-class-name date counter" style="max-height:32px;" data-page-id=' + $(event.target).attr('data-page-no') + ' readonly></div>';
                        break;
                    case "fileElement":
                        var div_id = $("#counter").val();
                        var new_id = parseInt(div_id) + 1;
                        $("#counter").val(new_id);
                        attribute = '<div id="imageBox-' + new_id + '" class="form-input-group form-resizable imageBox" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:100px;width:100px;min-width:50px; min-height:50px;" data-toggle="image" data-gallery="thumb-image"><img src="" style="display:none;"><input type="hidden" class="css-class-name image_box" style="height:100%;width:100%;display:none;" data-page-id=' + $(event.target).attr('data-page-no') + '><span class="image_area" style="width:100%; height:100%;border:1px solid #dbdfe6;top:0px;position:absolute;background:#fff;"><span style="    display: block;position:absolute;left:0;top:50%;width: 100%;height:auto;transform:translateY(-50%);-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%);text-align:center;">No Image</span></span></div>';
                        break;
                    case "counterFileElement":
                        var div_id = $("#counter").val();
                        var new_id = parseInt(div_id) + 1;
                        $("#counter").val(new_id);
                        attribute = '<div id="imageBox-' + new_id + '" class="form-input-group counter form-resizable imageBox" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:100px;width:100px;min-width:50px; min-height:50px;" data-toggle="image" data-gallery="thumb-image"><img src="" style="display:none;"><input type="hidden" class="css-class-name image_box counter" style="height:100%;width:100%;display:none;" data-page-id=' + $(event.target).attr('data-page-no') + '><span class="image_area" style="width:100%; height:100%;border:1px solid #dbdfe6;top:0px;position:absolute;background:#fff;"><span style="    display: block;position:absolute;left:0;top:50%;width: 100%;height:auto;transform:translateY(-50%);-moz-transform:translateY(-50%);-webkit-transform:translateY(-50%);text-align:center;">No Image</span></span></div>';
                        break;
                    case "catElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:15px;max-height:32px; min-height:15px;"><input type="text" class="css-class-name" label="' + dragLabelElement + '" style="max-height:32px;" value=' + dragValElement + ' data-page-id=' + $(event.target).attr('data-page-no') + '></div>';
                        break;
                    case "checkboxElement":
                        var i = 1;
                        $(".checkbox li").each(function() {
                            var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                            var $newPosYX = parseInt($newPosYY + i) + "%";
                            var radioLabel = $('.checkbox-label').val();
                            attribute += '<div class="form-input-group mycheckbox" style="position:absolute; left:' + $newPosX + '; top:' + $newPosYX + '; height:13px; line-height:13px;"><input type="checkbox" class="css-class-check checkfocused" style="height:13px; line-height:13px; margin:0;" value="' + $(this).find("input[type=text]").val() + '" attr-label="' + radioLabel + '" data-page-id=' + $(event.target).attr('data-page-no') + '  ></div>';
                            i = i + 2;
                        });
                        break;
                    case "counterCheckboxElement":
                        var i = 1;
                        $(".counterCheckbox li").each(function() {
                            var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                            var $newPosYX = parseInt($newPosYY + i) + "%";
                            var radioLabel = $('.counter-checkbox-label').val();
                            attribute += '<div class="form-input-group mycheckbox counter" style="position:absolute; left:' + $newPosX + '; top:' + $newPosYX + '; height:13px; line-height:13px;"><input type="checkbox" class="css-class-check checkfocused counter" style="height:13px; line-height:13px; margin:0;" value="' + $(this).find("input[type=text]").val() + '" attr-label="' + radioLabel + '" data-page-id=' + $(event.target).attr('data-page-no') + '  ></div>';
                            i = i + 2;
                        });
                        break;
                    case "radioElement":
                        var i = 1;
                        $(".radiobox li").each(function() {
                            var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                            var $newPosYX = parseInt($newPosYY + i) + "%";
                            var checkLabel = $('.radio-label').val();
                            attribute += '<div class="form-input-group checkboxattr" style="position:absolute; left:' + $newPosX + '; top:' + $newPosYX + '; height:13px; line-height:13px;"><input type="radio" class="css-class-radio myfocused" name="custom" style="height:13px; line-height:13px; margin:0;" value="' + $(this).find("input[type=text]").val() + '" attr-label="' + checkLabel + '" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                            i = i + 2;
                        });
                        break;
                    case "counterRadioElement":
                        var i = 1;
                        $(".counterRadiobox li").each(function() {
                            var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                            var $newPosYX = parseInt($newPosYY + i) + "%";
                            var checkLabel = $('.counter-radio-label').val();
                            attribute += '<div class="form-input-group counter checkboxattr" style="position:absolute; left:' + $newPosX + '; top:' + $newPosYX + '; height:13px; line-height:13px;"><input type="radio" class="css-class-radio myfocused counter" name="custom" style="height:13px; line-height:13px; margin:0;" value="' + $(this).find("input[type=text]").val() + '" attr-label="' + checkLabel + '" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                            i = i + 2;
                        });
                        break;
                    case "selectElement":
                        var i = 1;
                        var selectLabel = $('.select-label').val();
                        var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                        var $newPosYX = parseInt($newPosYY + i) + "%";
                        attribute = '<div class="form-input-group counter form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px; min-height:32px;"><select type="select" class="select_element" name="custom-select" attr-label="' + selectLabel + '" data-page-id=' + $(event.target).attr('data-page-no') + '>';
                        $(".selectbox li").each(function() {
                            var $newPosYY = (100 * parseFloat(uival / parseFloat($("#" + $(event.target).attr('id')).height())));
                            var $newPosYX = parseInt($newPosYY + i) + "%";

                            attribute += '<option class="css-class-radio myfocused counter"  style="height:13px; line-height:13px; margin:0;" value="' + $(this).find("input[type=text]").val() + '">' + $(this).find("input[type=text]").val() + '</option>';
                            i = i + 2;
                        });
                        attribute += '</select></div>';
                        break;
                    case "numberElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;  min-height:32px;"><input type="number" class="css-class-name" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                    case "counterNumberElement":
                        attribute = '<div class="form-input-group counter form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;  min-height:32px;"><input type="number" class="css-class-name counter" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                    case "emailElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px;  min-height:32px;"><input type="email" class="css-class-name" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                    case "counterEmailElement":
                        attribute = '<div class="form-input-group counter form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:32px; min-height:32px;"><input type="email" class="css-class-name counter" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                    case "textareaElement":
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:62px; min-height:40px;"><textarea class="css-class-name" data-page-id=' + $(event.target).attr('data-page-no') + '></textarea></div>';
                        break;
                    case "counterTextareaElement":
                        attribute = '<div class="form-input-group form-resizable counter" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:62px; min-height:40px;"><textarea class="css-class-name counter" data-page-id=' + $(event.target).attr('data-page-no') + '></textarea></div>';
                        break;
                    case "signature":
                        attribute = '<div><div class="form-input-group form-resizable signData" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; min-height:32px;" data-page-id=' + $(event.target).attr('data-page-no') + '  ></div></div>';
                        break;
                    case "counterSignature":
                        attribute = '<div class="form-input-group form-resizable signData" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:15px; max-height:32px; min-height:15px;"><div class="css-class-name signData counterSign" data-page-id=' + $(event.target).attr('data-page-no') + ' style="height:35px; max-height:50px; min-height:35px; width:100px; max-width:200px; min-width:150px;" ></div></div>';
                        break;
                    case "counterPhone":
                        attribute = '<div class="form-input-group form-resizable counter" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:15px; max-height:32px; min-height:15px;"><input type="text" class="css-class-name counter" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                    default:
                        attribute = '<div class="form-input-group form-resizable" style="position:absolute; left:' + $newPosX + '; top:' + $newPosY + '; height:15px; max-height:32px; min-height:15px;"><input type="text" class="css-class-name" data-page-id=' + $(event.target).attr('data-page-no') + ' ></div>';
                        break;
                }

                $("#page-container div").removeClass('checkboxattr').removeClass('mycheckbox');
                $("#" + $(event.target).attr('id') + " .pc").append(attribute);
                $("input, textarea, div.signData,select").removeClass('focused');

                $("#" + $(event.target).attr('id') + " .pc div:last").find('input').addClass('focused');
                $("#" + $(event.target).attr('id') + " .pc div:last").find('img').addClass('focused');
                $("#" + $(event.target).attr('id') + " .pc div:last").find('textarea').addClass('focused');
                //$("#" + $(event.target).attr('id') + " .pc div:last").find('div.signData').addClass('focused');
                //$("#" + $(event.target).attr('id') + " .pc div:last").find('div').hasClass('signData');
                if ($("#" + $(event.target).attr('id') + " .pc div:last").hasClass('signData')) {
                    $("#" + $(event.target).attr('id') + " .pc div:last").addClass('focused')
                }
                $("#" + $(event.target).attr('id') + " .pc div:last").find('select').addClass('focused');
                $(".scroll-box2").toggle();
                $(".form-element").toggle();


                // This is element Resizable function 
                $('#' + $(event.target).attr('id') + '  div.form-resizable').resizable({
                    // containment: "#page-container",
                    resize: function(event, ui) {
                        var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                        var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                        $(this).css("left", l);
                        $(this).css("top", t);
                    },
                    stop: function() {
                        self.trigger_save_template_fields();
                    }
                });



                // This is element dragable function 
                $("#" + $(event.target).attr('id') + " div.form-input-group").draggable({
                    start: function(event, ui) {
                        $(this).data('preventBehaviour', true);
                    },
                    drag: function(event, ui) {
                        var property = $('#page-container').css('transform');
                        var values = property.split('(')[1];
                        if (values !== undefined) {
                            values = values.split(')')[0];
                            values = values.split(',');
                            var zoomScale = values[0];
                            console.log('zoomScale' + zoomScale);
                            var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
                            var newLeft = ui.originalPosition.left + changeLeft / zoomScale; // adjust new left by our zoomScale
                            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
                            var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale
                            console.log((newLeft / $(this).parent().width()) * 100);
                            ui.position.left = newLeft;
                            ui.position.top = newTop;
                            //                        ui.position.left = (newLeft/$(this).parent().width())*100+'%';
                            //                        ui.position.top = (newTop/$(this).parent().height())*100+'%';

                        } else {
                            var l = (100 * parseFloat(($(this).position().left) / parseFloat($(this).parent().width()))) + "%";
                            var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                            $(this).css("left", l);
                            $(this).css("top", t);
                            if ($(this).children('div').hasClass('signData')) {
                                $(this).children('div.signData').addClass('focused');
                            }
                        }
                    },
                    stop: function(event, ui) {
                        var property = $('#page-container').css('transform');
                        var values = property.split('(')[1];
                        if (values !== undefined) {
                            values = values.split(')')[0];
                            values = values.split(',');
                            var zoomScale = values[0];
                            var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
                            var newLeft = ui.originalPosition.left + changeLeft / zoomScale; // adjust new left by our zoomScale
                            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
                            var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale
                            $(this).css("left", parseFloat((newLeft / $(this).parent().width()) * 100 * zoomScale) + '%');
                            $(this).css("top", parseFloat((newTop / $(this).parent().height()) * 100 * zoomScale) + '%');
                        } else {
                            var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                            var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                            $(this).css("left", l);
                            $(this).css("top", t);
                        }
                    }

                });

                // Mouse down element drag function
                $("#" + $(event.target).attr('id') + " div.form-input-group :input").on('mousedown', function(e) {
                    var mdown = document.createEvent("MouseEvents");
                    mdown.initMouseEvent("mousedown", false, true, window, 0, e.screenX, e.screenY, e.clientX, e.clientY, true, false, false, true, 0, null);
                    $(this).closest('#page-container div.form-input-group')[0].dispatchEvent(mdown);
                }).on('click', function(e) {
                    var $draggable = $(this).closest('#page-container div.form-input-group');
                    if ($draggable.data("preventBehaviour")) {
                        e.preventDefault();
                        $draggable.data("preventBehaviour", false)
                    }
                });

                $(".radiospan").hide('slow');
                $(".counterRadiospan").hide('slow');
                $(".checkboxspan").hide('slow');
                $(".counterCheckboxspan").hide('slow');
                $(".selectspan").hide('slow');
                //saveFields();
                $(".save_btn").trigger("click");
                setTimeout(function() {
                    $(".checkboxspan").html("<ol class='checkbox'><input type='text' value='checkbox-group' placeholder='Label' name='' class='form-control checkbox-label'><li class='ui-sortable-handle'><input type='checkbox' name='radio-group-1496836717607-option' value='false' class='option-selected'><input type='text' placeholder='Value' name='radio-group-1496836717607-option' value='option-1' class='option-value'></li></ol><a title='Add Element' class='addnewCheckboxElement btn'>+</a>");
                    $(".form-element .form-label").val($("#page-container input.focused , #page-container textarea.focused, #page-container div.focused").attr('label'));
                    $("#chklabel .form-group").keyup(function() {
                        $("input[name='" + $(this).attr('name') + "']").attr('attr-label', $(this).val());
                    });
                }, 500);

                setTimeout(function() {
                    $(".counterCheckboxspan").html("<ol class='counterCheckbox'><input type='text' value='checkbox-group' placeholder='Label' name='' class='form-control checkbox-label'><li class='ui-sortable-handle'><input type='checkbox' name='radio-group-1496836717607-option' value='false' class='option-selected'><input type='text' placeholder='Value' name='radio-group-1496836717607-option' value='option-1' class='option-value'></li></ol><a title='Add Element' class='addnewCheckboxElement btn'>+</a>");
                    $(".form-element .form-label").val($("#page-container input.focused , #page-container textarea.focused, #page-container div.focused").attr('label'));
                    $("#chklabel .form-group").keyup(function() {
                        $("input[name='" + $(this).attr('name') + "']").attr('attr-label', $(this).val());
                    });
                }, 500);

                // Element focus function active focus element
                $("#page-container input, #page-container textarea, #page-container div.focused").click(function() {
                    $("#page-container").find("textarea,input,div,select").removeClass('focused');
                    $(this).addClass('focused');
                    $(".form-element").show();
                    $(".scroll-box2").hide();
                    $(".form-element .form-title").val($(this).attr('title'));
                    $(".form-element .form-label").val($(this).attr('label'));
                    $(".form-element .form-max").val($(this).attr('maxlength'));
                     $("#mapping_fields_name").val($(this).attr('name'));
                   
                    if ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio') {
                        $(".element-form .form-group:first").hide();
                    } else {
                        $(".element-form .form-group:first").show();
                        $("#chklabel").hide();
                    }
                    if ($(this).attr('required') == 'required') {
                        $(".form-element .form-required").prop('checked', true);
                    } else {
                        $(".form-element .form-required").prop('checked', false);
                    }

                    if ($(this).attr('attr-label') == undefined) {
                        $("#chklabel").hide();
                    } else {
                        $("#chklabel").show();
                        $("#chklabel").find('input').val($(this).attr('attr-label'));
                        $("#chklabel").find('input').attr('name', $(this).attr('name'));
                    }

                });

            }

        });
        // var zoomScale = 1.4;
        /* Drag and drop and resize on load */
        $('.pf  div.form-resizable').resizable({
            //containment: "#page-container",
            resize: function(event, ui) {
                var property = $('#page-container').css('transform');
                var values = property.split('(')[1];
                if (values !== undefined) {
                    values = values.split(')')[0];
                    values = values.split(',');
                    var zoomScale = values[0];

                    // console.log(zoomScale);

                    var newLeft = ((100 * parseFloat(($(this).position().left / zoomScale) / parseFloat($(this).parent().width())))) + "%";
                    var newTop = ((100 * parseFloat(($(this).position().top / zoomScale) / $(this).parent().height()))) + "%";
                    // console.log('parseInt(newTop)'+parseInt(newTop));
                    // console.log(parseInt(newTop)/zoomScale);
                    $(this).css("left", (parseInt(newLeft) / zoomScale) + "%");
                    $(this).css("top", (parseInt(newTop) / zoomScale) + "%");
                } else {

                    var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                    var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                    $(this).css("left", l);
                    $(this).css("top", t);
                    var w = $(this).width();
                    var h = $(this).height();
                    $("#page-container").find("textarea,input,select").removeClass('focused');
                    $(this).find('textarea,input,select').addClass('focused');
                }
            },
            stop: function() {
                self.trigger_save_template_fields();
            }
        });

        $(".pf div.form-input-group,.clone").draggable({
            //cursorAt: {left: 0, top: 0, right: 0, bottom: 0},
            start: function(event, ui) {
                $(this).data('preventBehaviour', true);
                ui.position.left = 0;
                ui.position.top = 0;
            },
            drag: function(event, ui) {

                var property = $('#page-container').css('transform');
                var values = property.split('(')[1];
                if (values !== undefined) {
                    values = values.split(')')[0];
                    values = values.split(',');
                    var zoomScale = values[0];
                    console.log('zoomScale' + zoomScale);
                    var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
                    var newLeft = ui.originalPosition.left + changeLeft / zoomScale; // adjust new left by our zoomScale
                    var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
                    var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale
                    console.log((newLeft / $(this).parent().width()) * 100);
                    ui.position.left = newLeft;
                    ui.position.top = newTop;
                } else {
                    var l = (100 * parseFloat(($(this).position().left) / parseFloat($(this).parent().width()))) + "%";
                    var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                    $(this).css("left", l);
                    $(this).css("top", t);
                    if ($(this).children('div').hasClass('signData')) {
                        $(this).children('div.signData').addClass('focused');
                    }
                    //Custom temporary Ptach
                    $(this).find('textarea,input,select').addClass('focused');      
                }
            },
            stop: function(event, ui) {
                var property = $('#page-container').css('transform');
                var values = property.split('(')[1];
                if (values !== undefined) {
                    values = values.split(')')[0];
                    values = values.split(',');
                    var zoomScale = values[0];
                    var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
                    var newLeft = ui.originalPosition.left + changeLeft / zoomScale; // adjust new left by our zoomScale
                    var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
                    var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale
                    $(this).css("left", parseFloat((newLeft / $(this).parent().width()) * 100 * zoomScale) + '%');
                    $(this).css("top", parseFloat((newTop / $(this).parent().height()) * 100 * zoomScale) + '%');
                } else {
                    var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                    var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                    $(this).css("left", l);
                    $(this).css("top", t);
                }

                self.trigger_save_template_fields();
                if ($(this).children('div').hasClass('signData')) {
                    //$(this).children('div.signData').removeClass('focused');
                }
            }
        });


        $(document.body).on('mousedown', '.pc div.form-input-group :input', function(e) {
            // console.log("dsfg");
            // alert("sfdg");
            var mdown = document.createEvent("MouseEvents");
            mdown.initMouseEvent("mousedown", false, true, window, 0, e.screenX, e.screenY, e.clientX, e.clientY, true, false, false, true, 0, null);
            $(this).closest('.pc div.form-input-group')[0].dispatchEvent(mdown);
        }).on('click', function(e) {
            // console.log($(this).find('#page-container input.focused').attr('type'));
            if ($(this).find('#page-container input.focused').attr('type') == "text") {
                $("#read").css("display", "block");
            } else {
                $("#read").css("display", "none");
            }
            var $draggable = $(this).closest('.pc div.form-input-group');
            if ($draggable.data("preventBehaviour")) {
                e.preventDefault();
                $draggable.data("preventBehaviour", false)
            }
        });

    });

    // Focus on load 
    $(document.body).on('mousedown', ".pc input, .pc textarea, .pc div.signData , .pc div select", function() {
        // console.log("click");
        $(".des").html("");
        if ($(this).hasClass("signData") && !$(this).hasClass("counterSign")) {
            $(".des").html("<strong>Form Elements</strong> User Signature");
        } else if ($(this).hasClass("counterSign")) {
            $(".des").html("<strong>Form Elements</strong> Counter Signature");
        } else {
            $(".des").html("<strong>Form Elements</strong>");
        }
        $("#page-container").find("textarea,input,div,img,select").removeClass('focused');
        //console.log($(this))
        $(this).addClass('focused');
        $(".form-element").show();
        $(".form-element .form-title").val($(this).attr('title'));
        $(".form-element .form-label").val($(this).attr('label'));
        $(".form-element .form-max").val($(this).attr('maxlength'));
        $(".form-element .default").val($(this).attr('value'));
        $("#mapping_fields_name").val($(this).attr('name'));
        if ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio') {
            $(".element-form .form-group:first").hide();
        } else {
            $(".element-form .form-group:first").show();
        }

        if ($(this).attr('required') == 'required') {
            $(".form-required").prop('checked', true);
        } else {
            $(".form-required").prop('checked', false)
        }

        if ($(this).attr('readonly') == 'readonly') {
            $(".form-readonly").prop('checked', true);
        } else {
            $(".form-readonly").prop('checked', false)
        }

        if ($(this).attr('attr-label') == undefined) {
            $("#chklabel").hide();
        } else {
            $("#chklabel").show();
            $("#chklabel").find('input').val($(this).attr('attr-label'));
            $("#chklabel").find('input').attr('name', $(this).attr('name'));
        }

        $(".scroll-box2").hide();
    });


    /* Add radio button element */

    $(".addradioElement").on('click', function() {

        var attribute = '';
        var newPosX = '';
        var i = 0;
        $("div").removeClass('checkboxattr');
        $("input,textarea,select").removeClass('myfocused');
        $(".radiobox li").each(function() {
            newPosX = parseInt(50 + i);
            attribute += '<div class="form-input-group checkboxattr" style="position:absolute; left:100px; top:' + newPosX + 'px; height:25px;"><input type="radio" class="css-class-radio myfocused" name="custom" value=' + $(this).find('.option-value').val() + ' /></div>';
            i = i + 20;
        })

        $("#page-container").append(attribute);
        $("input,textarea,div.signData,select").removeClass('focused');
        $("#page-container  div:last").find('input').addClass('focused');

        $(".scroll-box2").toggle();
        $(".form-element").toggle();

        $("#page-container div.form-input-group").draggable({
            start: function(event, ui) {
                $(this).data('preventBehaviour', true);
            },
            drag: function(event, ui) {
                var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                $(this).css("left", l);
                $(this).css("top", t);
            },
            stop: function() {
                var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
                var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
                $(this).css("left", l);
                $(this).css("top", t);
            }
        });

        $("#page-container div.form-input-group :input").on('mousedown', function(e) {
            var mdown = document.createEvent("MouseEvents");
            mdown.initMouseEvent("mousedown", false, true, window, 0, e.screenX, e.screenY, e.clientX, e.clientY, true, false, false, true, 0, null);
            $(this).closest('#page-container div.form-input-group')[0].dispatchEvent(mdown);
        }).on('click', function(e) {
            var $draggable = $(this).closest('#page-container div.form-input-group');
            if ($draggable.data("preventBehaviour")) {
                e.preventDefault();
                $draggable.data("preventBehaviour", false)
            }
        });

        setTimeout(function() {
            //saveFields();
            $(".save_btn").trigger("click");
            $(".radiospan").toggle();
        }, 500);

        setTimeout(function() {
            //saveFields();
            $(".save_btn").trigger("click");
            $(".counterRadiospan").toggle();
        }, 500);

        $("#page-container input, #page-container textarea,#page-container select, #page-container div.focused").click(function() {
            $("#page-container").find("textarea,input,div,select").removeClass('focused');
            $(this).addClass('focused');
            $(".form-element").show();
            $(".scroll-box2").hide();

            if ($(this).attr('required') == 'required') {
                $(".form-element .form-required").prop('checked', true);
            } else {
                $(".form-element .form-required").prop('checked', false);
            }
            $(".form-element .form-title").val($(this).attr('title'));
            $(".form-element .form-label").val($(this).attr('label'));
            $(".form-element .form-max").val($(this).attr('maxlength'));
            $("#mapping_fields_name").val($(this).attr('name'));
        });

    });

    $(".addnewElement").on('click', function() {
        $(".radiobox").append("<li class='ui-sortable-handle'><input type='radio' class='option-selected' value='true' name='radio-group-1496836717607-option'><input type='text' class='option-value' value='' placeholder='Value'><a class='removenewElement btn' title='Remove Element'>×</a></li>");
        $(".removenewElement").on('click', function() {
            $(this).parent().remove();
        });
    });

    $(".add_new_select_option").on('click', function() {
        $(".selectbox").append("<li class='ui-sortable-handle'><input type='radio' class='option-selected' value='true' name='selec-group-1496836717607-option'><input type='text' class='option-value' value='' placeholder='Value'><a class='remove_new_select_option btn' title='Remove Option'>×</a></li>");
        $(".remove_new_select_option").on('click', function() {
            $(this).parent().remove();
        });
    });

    $("#mapping_fields_name").on('click', function() {
        if ($(this).val() != '') {
            $('input.focused').attr('name', $(this).val());
        }
    });

};


form_builder.prototype.save_template_fields = function() {
    var self = this;
    var required = 0;
    var readonly = 0;
    var readOnlyValue = 0;
    var TypeAttr = $('input.focused').attr('type');
    var TitleAttr = $('input.focused').attr('title');
    var MaxAttr = $('input.focused').attr('maxlength');
    var NameAttr = $('input.focused').attr('name');
    var LabelAttr = $('input.focused').attr('label');
    var requiredAttr = $('input.focused').attr('required');
    var readonlyAttr = $('input.focused').attr('readonly');
    var ValueAttr = $('input.focused').attr('value');
    if (requiredAttr == 'required') {
        required = 1;
    }
    if (readonlyAttr == 'readonly') {
        readonly = 1;
        readOnlyValue = $('#default').val();
    }
    //console.log(TypeAttr);
    var styleAttr = $('#page-container input.focused').parent();
    var attrName = parseInt(1) + parseInt(self.field_counter);

    if (typeof TypeAttr !== 'undefined') {
        var groupTitle = '';
        if (TypeAttr == 'radio') {
            TypeAttr = 'radio-group';
            LabelAttr = $('.radio-label').val();
            groupTitle = $("#chklabel").find('input').val();
        } else if (TypeAttr == 'checkbox') {
            TypeAttr = 'checkbox-group';
            LabelAttr = $('.checkbox-label').val();
            groupTitle = $("#chklabel").find('input').val();
        }

        console.log(LabelAttr);
        if (typeof LabelAttr == 'undefined') {
            LabelAttr = 'label' + attrName;
            $('input.focused').attr('label', 'label' + attrName);
        }

        if (typeof NameAttr === 'undefined' || NameAttr == '') {
            NameAttr = 'attr' + attrName;
        }


        var pageId = $('input.focused').attr('data-page-id');
        var post_data = {
            id: $('#page-container input.focused').attr('id'),
            groupTitle: groupTitle,
            length: MaxAttr,
            Type: TypeAttr,
            Required: required,
            Readonly: readonly,
            readOnlyValue: readOnlyValue,
            Label: LabelAttr,
            Title: TitleAttr,
            templateId: self.template_id,
            name: NameAttr,
            ValueAttr: ValueAttr,
            pageId: pageId
        };
        post_data.html_contain = styleAttr[0].outerHTML;
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: post_data,
            url: base_url + 'admin/form-builder/save_template_fields/',
            success: function(stat) {
                stat = JSON.parse(stat);
                var data = stat.id;
                $(".element-form").find('input').val('');
                $('#page-container div input.focused').attr('id', data);
                $('#page-container div img.focused').attr('id', data);
                self.field_counter++;
                if (TypeAttr == 'radio-group') {
                    $('#page-container div input.myfocused').attr('Attrid', data);
                    $(".checkboxattr").each(function() {
                        $(this).find('input.myfocused').attr('label', $(this).find('input.myfocused').val());
                        $(this).find('input.myfocused').attr('name', 'attr' + attrName);
                        var styleRadioAttr = $(this).find('input.myfocused').parent();
                        var LabelAttr = 'label' + attrName;
                        var obj = $(this);
                        post_data.html_contain = styleRadioAttr[0].outerHTML;
                        post_data.Label = $(this).find('input.myfocused').val();
                        post_data.value = $(this).find('input.myfocused').val();
                        post_data.templateAttrId = $("#page-container div").find('input.myfocused').attr('Attrid');
                        $.ajax({
                            type: 'POST',
                            data: post_data,
                            datatype: 'json',
                            url: base_url + 'admin/form-builder/save_template_radio_field_options/',
                            beforeSend: function(xhr) {

                            },
                            success: function(data) {
                                data = JSON.parse(data);
                                if (data.success) {
                                    obj.find('input.myfocused').attr('id', data.id);
                                    $('#page-container div input.myfocused').attr('Attrid', post_data.templateAttrId);
                                }
                            }
                        });
                    });
                }
                if (TypeAttr == 'checkbox-group') {
                    $('#page-container div input.checkfocused').attr('Attrid', data);
                    $(".mycheckbox").each(function() {
                        $(this).find('input.checkfocused').attr('label', $(this).find('input.checkfocused').val());
                        $(this).find('input.checkfocused').attr('name', 'attr' + attrName);
                        var styleRadioAttr = $(this).find('input.checkfocused').parent();
                        var LabelAttr = 'label' + attrName;
                        var obj = $(this);
                        post_data.html_contain = styleRadioAttr[0].outerHTML;
                        post_data.Label = $(this).find('input.checkfocused').val();
                        post_data.value = $(this).find('input.checkfocused').val();
                        post_data.templateAttrId = $("#page-container div").find('input.checkfocused').attr('Attrid');
                        $.ajax({
                            type: 'POST',
                            data: post_data,
                            datatype: 'json',
                            url: base_url + 'admin/form-builder/save_template_radio_field_options/',
                            beforeSend: function(xhr) {

                            },
                            success: function(data) {
                                data = JSON.parse(data);
                                obj.find('input.checkfocused').attr('id', data.id);
                                $('#page-container div input.checkfocused').attr('Attrid', post_data.templateAttrId);
                            }
                        });
                    });
                }
            }
        });

    } else if ($("div").hasClass("focused") && $("div").hasClass("signData")) {
        var TypeAttr = 'signature';
        var TitleAttr = $('div.focused').attr('title');
        var MaxAttr = $('div.focused').attr('maxlength');
        var NameAttr = $('div.focused').attr('name');
        var LabelAttr = $('div.focused').attr('label');
        var requiredAttr = $('div.focused').attr('required');
        var styleAttr = $('div.focused').parent();
        var pageId = $('div.focused').attr('data-page-id');
        var post_data = {
            id: $('#page-container div.focused').attr('id'),
            groupTitle: groupTitle,
            length: MaxAttr,
            Type: TypeAttr,
            Required: required,
            Readonly: readonly,
            readOnlyValue: readOnlyValue,
            Label: LabelAttr,
            Title: TitleAttr,
            templateId: self.template_id,
            name: NameAttr,
            ValueAttr: ValueAttr,
            pageId: pageId
        };
        if (requiredAttr == 'required') {
            required = 1;
        };
        if (typeof NameAttr === 'undefined' || NameAttr == '') {
            NameAttr = 'attr' + attrName;
        }
        post_data.html_contain = styleAttr[0].outerHTML;
        $.ajax({
            type: 'POST',
            data: post_data,
            datatype: 'json',
            url: base_url + 'admin/form-builder/save_template_fields/',
            beforeSend: function(xhr) {

            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.success){
                    self.field_counter++;
                    $('#page-container div div.focused:last').attr('id', data.id);
                }
            }
        });
    } else if ($("select").hasClass("focused") && $("select").hasClass("select_element")) {
        var select_ele = $("select.focused");
        var groupTitle = '';
        TypeAttr = 'select-group';
        LabelAttr = $('.radio-label').val();
        groupTitle = $("#chklabel").find('input').val();
        console.log(LabelAttr);
        if (typeof LabelAttr == 'undefined') {
            LabelAttr = 'label' + attrName;
            select_ele.attr('label', 'label' + attrName);
        }

        NameAttr = select_ele.attr('name');
        if (typeof NameAttr === 'undefined' || NameAttr == '') {
            NameAttr = 'custom-select' + attrName;
        }

        var styleAttr = select_ele.parent();
        var pageId = select_ele.attr('data-page-id');
        var post_data = {
            id: select_ele.attr('id'),
            groupTitle: groupTitle,
            Type: TypeAttr,
            Required: required,
            Readonly: readonly,
            readOnlyValue: readOnlyValue,
            Label: LabelAttr,
            Title: TitleAttr,
            templateId: self.template_id,
            name: NameAttr,
            ValueAttr: ValueAttr,
            pageId: pageId
        };
        post_data.html_contain = styleAttr[0].outerHTML;
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: post_data,
            url: base_url + 'admin/form-builder/save_template_fields/',
            success: function(stat) {
                stat = JSON.parse(stat);
                var data = stat.id;
                $(".element-form").find('input').val('');
                select_ele.attr('id', data);
                self.field_counter++;
                select_ele.attr('Attrid', data);
                /**for (var i = 0; i < select_ele.length; i++){
                    console.log(select_ele);
                    var option = select_ele.options[i];
                    post_data.html_contain = styleRadioAttr[0].outerHTML;
                        post_data.Label = option.value;
                        post_data.value = option.value;
                        post_data.templateAttrId = select_ele.attr('Attrid');
                        $.ajax({
                            type: 'POST',
                            data: post_data,
                            datatype: 'json',
                            url: base_url + 'admin/form-builder/save_template_radio_field_options/',
                            beforeSend: function (xhr) {

                            },
                            success: function (data) {
                                data = JSON.parse(data);
                                if(data.success){
                                    option.attr('id', data.id);
                                    option.attr('Attrid', post_data.templateAttrId);
                                }
                            }
                        });
                }*/
            }

        });
    } else {
        var TypeAttr = 'textarea';
        var TitleAttr = $('textarea.focused').attr('title');
        var LabelAttr = $('textarea.focused').attr('label');
        var requiredAttr = $('textarea.focused').attr('required');
        var pageId = $('textarea.focused').attr('data-page-id');

        if (typeof LabelAttr == 'undefined') {
            LabelAttr = 'label' + attrName;
            $('textarea.focused').attr('label', 'label' + attrName);
        }
        var styleAttr = $('#page-container textarea.focused').parent();
        post_data.html_contain = styleAttr[0].outerHTML;

        $.ajax({
            type: 'POST',
            data: post_data,
            datatype: 'json',
            url: base_url + 'admin/form-builder/save_template_fields/' + id,
            beforeSend: function(xhr) {

            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.success){
                    $(".element-form").find('input').val('');
                    $('#page-container div textarea.focused').attr('id', data.id);
                    self.field_counter++;
                }
            }
        });
    }
};

form_builder.prototype.delete_template_fields = function() {
    var self = this;
    var defaultMsg = 'Are you sure want to delete this Attribute !';
    swal({
            title: "Are you sure?",
            text: defaultMsg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                var TypeAttr = $('input.focused').attr('type');
                if (typeof TypeAttr !== 'undefined') {
                    var deleteId = $('#page-container input.focused').attr('id');
                    var TypeAttr = $('#page-container input.focused').attr('type');
                } else if ($("div").hasClass("signData") && $("div").hasClass("focused")) {
                    var deleteId = $('#page-container div.signData.focused').attr('id');
                    var TypeAttr = 'signature';
                } else {
                    var deleteId = $('#page-container textarea.focused').attr('id');
                    var TypeAttr = 'textarea';
                }
                $('#page-container input.focused,#page-container textarea.focused,#page-container div.focused').parent('div').remove();
                $.ajax({
                    type: 'POST',
                    datatype: 'json',
                    data: {
                        id: deleteId,
                        type: TypeAttr,
                        template_id: self.template_id
                    },
                    url: base_url + 'admin/form-builder/delete_template_fields',
                    beforeSend: function(xhr) {

                    },
                    success: function(stat) {
                        stat = JSON.parse(stat);
                        if (stat.success) {
                            $('#page-container input.focused,#page-container textarea.focused,#page-container div.focused').parent('div').remove();
                            $(".form-element").hide('slow');
                            $(".scroll-box2").show('slow');
                        } else {
                            alert('Element doesnot deleted');
                        }
                    }
                });
            } else {
                return false;
            }
        });
};


form_builder.prototype.load_template = function() {
    var self = this;
    $.ajax({
        type: 'get',
        datatype: 'json',
        url: base_url + 'admin/form-builder/fetch_template_fields?template_id=' + self.template_id,
        beforeSend: function(xhr) {

        },
        success: function(stat) {
            stat = JSON.parse(stat);
            if (stat.success) {
                self.handle_template_fields(stat);
            }
        }
    });
};


form_builder.prototype.handle_template_fields = function(data) {
    var self = this;
    var attributes = data.template_attributes;
    self.field_counter = (attributes.length > 0) ? attributes.length : 0;
    $("#counter").val(self.field_counter);
    for (var i = 0; i < attributes.length; i++) {
        var page_no = (attributes[i].page_no) ? attributes[i].page_no : 1;
        //Radio and Checkbox
        if (attributes[i].field_type == 'radio-group' || attributes[i].field_type == 'checkbox-group') {
            var attribute_options = data.template_attribute_options[i];
            for (var j = 0; j < attribute_options.length; j++) {
                $("#pf" + page_no + " .pc").append(attribute_options[j].html_contain);
                $("#pf" + page_no + " .pc").children().last().find('div').remove();
                $("#pf" + page_no + " .pc").children().last().find('input').attr('id', attribute_options[j].id);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('Attrid', attribute_options[j].template_attribute_id);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('attr-label', attribute_options[j].field_label);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('name', attribute_options[j].field_name);
                $("#pf" + page_no + " .pc").children().last().find('input').attr('value', attribute_options[j].value);
            }
        } else if (attributes[i].field_type == 'signature') {
            $("#pf" + page_no + " .pc").append(attributes[i].html_contain);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('id', attributes[i].id);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('name', attributes[i].field_name);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('title', attributes[i].field_title);
            $("#pf" + page_no + " .pc").children().last().find('div.signData').attr('label', attributes[i].field_label);
        } else {
            $("#pf" + page_no + " .pc").append(attributes[i].html_contain);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea').attr('id', attributes[i].id);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea').attr('name', attributes[i].field_name);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea').attr('title', attributes[i].field_title);
            $("#pf" + page_no + " .pc").children().last().find('input,textarea').attr('label', attributes[i].field_label);
            if (attributes[i].is_readonly === true) {
                $("#pf" + page_no + " .pc").children().last().find('input,textarea').attr('value', attributes[i].value);
            } else {
                $("#pf" + page_no + " .pc").children().last().find('input,textarea').removeAttr('value');
            }
        }
    }
    self.initialize();

    $('div.form-resizable').resizable({
        // containment: "#page-container",
        resize: function(event, ui) {
            var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + "%";
            var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + "%";
            $(this).css("left", l);
            $(this).css("top", t);
        },
        stop: function() {
            self.trigger_save_template_fields();
        }
    });
    $('div.form-resizable').resizable("destroy");

};


form_builder.prototype.trigger_save_template_fields = function(data) {
    var self = this;
    $(".des").html("");
    $(".des").html("<strong>Form Elements</strong>");
    var required = 0;
    var readonly = 0;
    var readOnlyValue = 0;
    var TypeAttr = $('input.focused').attr('type');
    var TitleAttr = $('input.focused').attr('title');
    var MaxAttr = $('input.focused').attr('maxlength');
    var NameAttr = $('input.focused').attr('name');
    var LabelAttr = $('input.focused').attr('label');
    var requiredAttr = $('input.focused').attr('required');
    var readonlyAttr = $('input.focused').attr('readonly');
    if (requiredAttr == 'required') {
        required = 1;
    }
    if (readonlyAttr == 'readonly') {
        readonly = 1;
        readOnlyValue = $('#default').val();
    }
    //console.log(TypeAttr);
    var styleAttr = $('#page-container input.focused').parent();
    if (typeof TypeAttr !== 'undefined') {
        var groupTitle = '';
        if (TypeAttr == 'radio') {
            TypeAttr = 'radio-group';
            BaseUrl = base_url + 'admin/form-builder/save_template_radio_field_options';
            groupTitle = $("#chklabel").find('input').val();
        } else if (TypeAttr == 'checkbox') {
            TypeAttr = 'checkbox-group';
            BaseUrl = base_url + 'admin/form-builder/save_template_radio_field_options';
            groupTitle = $("#chklabel").find('input').val();
        } else {
            BaseUrl = base_url + 'admin/form-builder/save_template_fields';
        }
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: {
                id: $('#page-container input.focused').attr('id'),
                groupTitle: groupTitle,
                name: NameAttr,
                length: MaxAttr,
                Type: TypeAttr,
                Required: required,
                Readonly: readonly,
                readOnlyValue: readOnlyValue,
                html_contain: styleAttr[0].outerHTML,
                Label: LabelAttr,
                Title: TitleAttr,
                templateId: self.template_id
            },
            url: BaseUrl,
            success: function(data) {
                data = JSON.parse(data);
                if (data.success) {
                    $(".element-form").find('input').val('');
                    $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
                    $(".form-element #tempmsg").fadeOut(3000, function() {
                        $(this).remove();
                    });
                }

                $(".form-element").hide('slow');
                $(".scroll-box2").show('slow');
            }
        });

        $(".element-form").find('input').val('');
        $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
        $(".form-element #tempmsg").fadeOut(3000, function() {
            $(this).remove();
        });
        $(".form-element").hide('slow');
        $(".scroll-box2").show('slow');
    } else if ($("div").hasClass("signData") && $("div").hasClass("focused")) {
        var TypeAttr = 'signature';
        var TitleAttr = $('div.focused').attr('title');
        var MaxAttr = $('div.focused').attr('maxlength');
        var NameAttr = $('div.focused').attr('name');
        var LabelAttr = $('div.focused').attr('label');
        var requiredAttr = $('div.focused').attr('required');
        var styleAttr = $('div.focused').parent();

        if (requiredAttr == 'required') {
            required = 1;
        }
        
        $(".element-form").find('input').val('');
        $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
        $(".form-element #tempmsg").fadeOut(3000, function() {
            $(this).remove();
        });
        $(".form-element").hide('slow');
        $(".scroll-box2").show('slow');
        console.log($('div.signData.focused'));
        console.log($('div.signData.focused').attr('id'));
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: {
                id: $('div.signData.focused').attr('id'),
                name: NameAttr,
                length: MaxAttr,
                Type: TypeAttr,
                Required: required,
                html_contain: styleAttr[0].outerHTML,
                Label: LabelAttr,
                Title: TitleAttr,
                templateId: self.template_id
            },
            url: base_url + 'admin/form-builder/save_template_fields',
            success: function(data) {
                data = JSON.parse(data);
                if (data.success) {
                    $("#page-container").find("div.focused").removeClass('focused');
                    $(".element-form").find('input').val('');
                    $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
                    $(".form-element #tempmsg").fadeOut(3000, function() {
                        $(this).remove();
                    });
                }
                $(".form-element").hide('slow');
                $(".scroll-box2").show('slow');
            }
        });
    } else if ($("select").hasClass("select_element") && $("select").hasClass("focused")) {
        var select_ele = $("select.focused");
        var groupTitle = '';
        TypeAttr = 'select-group';
        LabelAttr = $('.radio-label').val();
        groupTitle = $("#chklabel").find('input').val();
        console.log(LabelAttr);
        if (typeof LabelAttr == 'undefined') {
            LabelAttr = 'label' + attrName;
            select_ele.attr('label', 'label' + attrName);
        }

        NameAttr = select_ele.attr('name');
        if (typeof NameAttr === 'undefined' || NameAttr == '') {
            NameAttr = 'custom-select' + attrName;
        }

        var styleAttr = select_ele.parent();
        var pageId = select_ele.attr('data-page-id');
        var post_data = {
            id: select_ele.attr('id'),
            groupTitle: groupTitle,
            Type: TypeAttr,
            Required: required,
            Readonly: readonly,
            readOnlyValue: readOnlyValue,
            Label: LabelAttr,
            Title: TitleAttr,
            templateId: self.template_id,
            name: NameAttr,
            pageId: pageId
        };
        post_data.html_contain = styleAttr[0].outerHTML;
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: post_data,
            url: base_url + 'admin/form-builder/save_template_fields/',
            success: function(stat) {
                stat = JSON.parse(stat);
                var data = stat.id;
                $("#page-container").find("div.focused").removeClass('focused');
                $(".element-form").find('input').val('');
                $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
                $(".form-element #tempmsg").fadeOut(3000, function() {
                    $(this).remove();
                });
                $(".form-element").hide('slow');
                $(".scroll-box2").show('slow');
                /**for (var i = 0; i < select_ele.length; i++){
                    console.log(select_ele);
                    var option = select_ele.options[i];
                    post_data.html_contain = styleRadioAttr[0].outerHTML;
                        post_data.Label = option.value;
                        post_data.value = option.value;
                        post_data.templateAttrId = select_ele.attr('Attrid');
                        $.ajax({
                            type: 'POST',
                            data: post_data,
                            datatype: 'json',
                            url: base_url + 'admin/form-builder/save_template_radio_field_options/',
                            beforeSend: function (xhr) {

                            },
                            success: function (data) {
                                data = JSON.parse(data);
                                if(data.success){
                                    option.attr('id', data.id);
                                    option.attr('Attrid', post_data.templateAttrId);
                                }
                            }
                        });
                }*/
            }

        });
    } else {
        var TypeAttr = 'textarea';
        var TitleAttr = $('textarea.focused').attr('title');
        var MaxAttr = $('textarea.focused').attr('maxlength');
        var NameAttr = $('textarea.focused').attr('name');
        var LabelAttr = $('textarea.focused').attr('label');
        var requiredAttr = $('textarea.focused').attr('required');
        var styleAttr = $('#page-container textarea.focused').parent();
        if (requiredAttr == 'required') {
            required = 1;
        }
        $.ajax({
            type: 'POST',
            datatype: 'json',
            data: {
                id: $('#page-container textarea.focused').attr('id'),
                name: NameAttr,
                length: MaxAttr,
                Type: TypeAttr,
                Required: required,
                html_contain: styleAttr[0].outerHTML,
                Label: LabelAttr,
                Title: TitleAttr,
                templateId: self.template_id
            },
            url: base_url + 'admin/form-builder/save_template_fields',
            success: function(data) {
                data = JSON.parse(data);
                if (data.success) {
                    $(".element-form").find('input').val('');
                    $(".form-element").append('<p id="tempmsg" style="color: green; position: absolute; left: 15px; bottom: 80px;"> Attribute has been saved successfully.</p>');
                    $(".form-element #tempmsg").fadeOut(3000, function() {
                        $(this).remove();
                    });
                }
                $(".form-element").hide('slow');
                $(".scroll-box2").show('slow');
            }
        });
    }

};


form_builder.prototype.clone = function(data) {
    var self = this;
    var clone = $(".focused").parent().clone();
    clone.find("textarea,input,div,img,select").removeClass('focused');
    clone.addClass('clone');
    var attrName = parseInt(2 + self.field_counter);
    var attributeName = 'attr' + attrName;

    left = $(clone).css('left');
    y = $(clone).css('top');
    newLeft = parseFloat(left) + 5;
    newTop = parseFloat(y) + 3;
    //console.log(newTop);
    $(clone).css('left', newLeft + '%');
    $(clone).css('top', newTop + '%');

    clone.find("textarea,input,div,img,select").removeAttr('id');
    clone.find("textarea,input,div,img,select").removeAttr('name');
    clone.find("textarea,input,div,img,select").attr('name', attributeName);
    clone.appendTo($(".focused").parent().parent()).draggable({
        stop: function(event, ui) {
            $(".save_btn").trigger("click");
        }
    });
    self.field_counter++;
};
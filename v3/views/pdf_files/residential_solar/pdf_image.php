<style>
	.font-family{font-family: 'Open Sans', sans-serif;}
	.table-border{border:solid 1px #ccc;}
	.w910{width:910px; }
	body{margin:0px !important; overflow-y: hidden;}

	.overlay-roadblock{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 10;
		background-color: rgba(0,0,0,0.5);
		display: none;
	}

	.overlay-roadblock__content{
		width: auto;
		height: 80px;
		position: fixed;
		top: 50%; 
		left: 50%;
		margin-top: -100px;
		margin-left: -150px;
		margin-right: 25px;
		background-color: #fff;
		border-radius: 5px;
		text-align: center;
		z-index: 11;
		word-break: break-all;
		padding:20px;
	}

	.loader {
	    border: 5px solid #f3f3f3;
	    -webkit-animation: spin 1s linear infinite;
	    animation: spin 1s linear infinite;
	    border-top: 5px solid #555;
	    border-radius: 50%;
	    width: 50px;
	    height: 50px;
	    margin: 0 auto;
	    text-align:center;
	}

	@keyframes spin {
    0% { 
        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}
</style>

<?php
$system_cost = number_format($proposal['system_total'], 0, '.', ',');
$stc_deduction = number_format($proposal['stc_deduction'], 0, '.', ',');
if ($proposal['quote_total_inGst'] != NULL) {
  $total_payable = number_format($proposal['quote_total_inGst'], 0, '.', ',');
  $gst = $proposal['quote_total_inGst'] - $proposal['quote_total_exGst'];
} else {
  $total_payable = number_format((($proposal['system_total'] * 1.1) - $proposal['stc_deduction']), 0, '.', ',');
  $gst = (($proposal['system_total'] * 1.1) - $proposal['stc_deduction']) - $proposal['quote_total_exGst'];
}

$count = 0;
if (!empty($panel_data)) {
    //$height = $height - 30;
  $count = 1;
}
if (!empty($inverter_data)) {
    //$height = $height - 30;
  $count += 1;
}
if (!empty($battery_data)) {
    //$height = $height - 30;
  $count += 1;
}
if (!empty($proposal_additional_data)) {
    //$height = $height - (count($proposal_additional_data) * 30);
}

$count += count($proposal_additional_data);
$new_count = 6 - $count;

$height = 90;
if($stc_deduction > 0){
  $height = $height - 30;  
}
if($system_cost > 0){
  $height = $height - 30;  
}
$color = 'white';
$ncolor = '#f0f0f0';

$gst = number_format($gst, 0, '.', ',');
$is_vic_rebate = $proposal['is_vic_rebate'];
$vic_rebate = number_format($proposal['vic_rebate'], 0, '.', ',');

if(!$is_vic_rebate){
  $height = $height + 30;  
}else{
  $height = $height - 10;   
}

?>
<form  method="POST" id="chartForm1">
	<input type="hidden"  name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg1" name="dataImg1" value="" />
	<input type="hidden"  id="filename1" name="filename1" value="page2" />
</form>

<form  method="POST" id="chartForm2">
	<input type="hidden"  name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg2" name="dataImg2" value="" />
	<input type="hidden"  id="filename2" name="filename2" value="page3" />
</form>

<form  method="POST" id="chartForm3">
	<input type="hidden"  name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg3" name="dataImg3" value="" />
	<input type="hidden" id="filename3" name="filename3" value="page_solar_rebate" />
</form>

<form  method="POST" id="chartForm4">
	<input type="hidden"   name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg4" name="dataImg4" value="" />
	<input type="hidden"  id="filename4" name="filename4" value="page_battery_rebate" />
</form>

<form  method="POST" id="chartForm5">
	<input type="hidden"  name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg5" name="dataImg5" value="" />
	<input type="hidden"  id="filename5" name="filename5" value="page1" />
</form>

<form  method="POST" id="chartForm6">
	<input type="hidden" name="pId" value="<?php echo $proposal['id']; ?>" />
	<input type="hidden" id="dataImg6" name="dataImg6" value="" />
	<input type="hidden" id="filename6" name="filename6" value="page4" />
</form>

<div id="page1" class="w910" style="display: none;">
	<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff; ">
		<tr>
			<td style="font-size:0px;"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/main-img.jpg" alt=""/></td>
		</tr>
		<tr>
			<td valign="top" style="background: #DE1717;">
				<table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40"></td>
						</tr>
						<tr>
							<td style="font-size: 39px; color: #ffffff; text-transform: uppercase"><strong>YOUR <span style="color: #000000">SOLAR PROPOSAL</span> </strong></td>
						</tr>
						<tr>
							<td height="8"></td>
						</tr>
						<tr>
							<td style="font-size: 22px; color: #ffffff; text-transform: uppercase">SOLAR ENERGY FOR BETTER TOMORROW</td>
						</tr>
						<tr>
							<td style="border-bottom: solid 1px #ffffff">&nbsp;</td>
						</tr>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td valign="top"><table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td width="435" valign="top"><table width="435" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td width="435" style="color: #ffffff; font-size:23px;"><strong>CUSTOMISED FOR</strong></td>
												</tr>
												<tr>
													<td height="18"></td>
												</tr>
												<tr>
													<td valign="top"><table width="95%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="28%" height="20" style="color: #ffffff; font-size: 18px">Quote Id:</td>
																<td width="72%" style="color: #ffffff; font-size: 18px">#<?php echo $proposal['id'] ; ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Name:</td>
																<td style="color: #ffffff; font-size: 18px"><?php echo $customer_data['first_name'] . ' ' . $customer_data['last_name']; ?></td>
															</tr>
															<tr>
																<td height="20" valign="top" style="color: #ffffff; font-size: 18px">Address:</td>
																<td style="color: #ffffff; font-size: 18px"> <?php echo str_ireplace(", Australia", "", $customer_data['address']); ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Phone No.: </td>
																<td style="color: #ffffff; font-size: 18px"><?php echo $customer_data['customer_contact_no'] ; ?></td>
															</tr>
															<tr>
																<td height="20" style="color: #ffffff; font-size: 18px">Email:</td>
																<td style="color: #ffffff; font-size: 18px"><?php echo $customer_data['customer_email'] ; ?></td>
															</tr>
														</tbody>
													</table></td>
												</tr>
											</tbody>
										</table></td>
										<td width="375" valign="top"><table width="350" border="0" align="right" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td width="381" style="color: #ffffff; font-size:23px;"><strong>PREPARED BY</strong></td>
												</tr>
												<tr>
													<td height="18"></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $proposal['full_name'] ; ?></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $proposal['company_contact_no'] ; ?></td>
												</tr>
												<tr>
													<td height="20" style="font-size: 18px; color: #ffffff"><?php echo $proposal['email'] ; ?></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-size:23px;"><strong>DATE PREPARED: <?php echo date('d-m-Y'); ?></strong></td>
						</tr>
						<tr>
							<td height="123"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
</div>


<div id="page2" class="w910">
	
	<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #e6e6e6">
		<tr>
			<td valign="top" style="border: 0; font: 0"><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td colspan="2" height="20"></td>
								</tr>
								<tr>
									<td width="58" align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/header_lines.png" width="45" height="40" alt=""/></td>
									<td width="822" style="font-size: 25px; color: #000000; text-transform: uppercase"><strong><span style="color: #DE1717">Next </span>Steps</strong></td>
								</tr>
							</tbody>
						</table></td>
					</tr>
					<tr>
						<td height="20"></td>
					</tr>
					<tr>
						<td align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/p6.jpg" width="835" height="1063" alt=""/></td>
					</tr>
					<tr>
						<td height="55"></td>
					</tr>
					<tr>
						<td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td width="381" align="left" valign="middle"><table width="125" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td width="37" align="left"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/phone.png" width="25" height="25" alt=""/></td>
												<td width="41" align="center" style="font-size:20px;"><strong>13</strong></td>
												<td width="35" align="center" style="font-size:20px;"><strong>58</strong></td>
												<td width="37" align="center" style="font-size:20px;"><strong>42</strong></td>
											</tr>
										</tbody>
									</table></td>
									<td width="405" align="right" valign="top"><img src="<?php echo site_url('');; ?>assets/images/logo.svg" width="163" alt=""/></td>
								</tr>
							</tbody>
						</table></td>
					</tr>
					<tr>
						<td height="30"></td>
					</tr>
				</tbody>
			</table></td>
		</tr>
	</table>
</div>

<div id="page3" class="w910" style="display: none;">

	<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #e6e6e6">
		<tr>
			<td valign="top" style="border: 0; font: 0"><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td colspan="2" height="20"></td>
								</tr>
								<tr>
									<td width="58" align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/header_lines.png" width="45" height="40" alt=""/></td>
									<td width="822" style="font-size: 25px; color: #000000; text-transform: uppercase"><strong><span style="color: #DE1717">Important </span>things to know</strong></td>
								</tr>
							</tbody>
						</table></td>
					</tr>
					<tr>
						<td height="50"></td>
					</tr>
					<tr>
						<td height="200" align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/p7.jpg" width="840" alt=""/></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="20" >&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" style="border: 0; font: 0"><table width="910" border="0" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="420" valign="top"><table width="420" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td width="420" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/bottom_top-7.jpg" width="420" height="165" alt=""/></td>
							</tr>
							<tr>
								<td height="73" align="right" valign="top" style="background: #252324">
								    <table width="335" border="0" align="right" cellpadding="0" cellspacing="0">
    									<tbody>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['full_name'] ; ?></td>
    										</tr>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['company_contact_no'] ; ?></td>
    										</tr>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['email'] ; ?></td>
    										</tr>
    									</tbody>
								    </table>
								</td>
							</tr>
							<tr>
								<td valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/bottom-7.jpg" width="420" height="27" alt=""/></td>
							</tr>
						</tbody>
					</table></td>
					<td width="490" align="right" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/right-7.jpg" width="490" height="264" alt=""/></td>
				</tr>
			</tbody>
		</table></td>
	</tr>
	<tr>
		<td height="125" >&nbsp;</td>
	</tr>
</table>
</div>

<div id="page4" class="w910" style="display: none;">
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
		<tr>
			<td valign="top" style="border: 0; font: 0">
				<table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td  align="center" valign="top"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/p5.jpg" width="840" alt=""/></td>
						</tr>
						<tr>
							<td height="80"></td>
						</tr>
						<tr>
							<td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="381" align="left" valign="middle"><table width="125" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td width="37" align="left"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/phone.jpg" width="25" height="25" alt=""/></td>
													<td width="41" align="center" style="font-size:20px;"><strong>13</strong></td>
													<td width="35" align="center" style="font-size:20px;"><strong>58</strong></td>
													<td width="37" align="center" style="font-size:20px;"><strong>42</strong></td>
												</tr>
											</tbody>
										</table></td>
										<td width="405" align="right" valign="top"><img src="<?php echo site_url(''); ?>assets/images/logo.svg" width="163"  alt=""/></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td height="20"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
</div>


<div id="page5" class="w910" style="display: none;">
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
		<tr>
			<td valign="top" style="border: 0; font: 0">
				<table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td  align="center" valign="top"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/p8.jpg" width="840" alt=""/></td>
						</tr>
						<tr>
							<td height="80"></td>
						</tr>
						<tr>
							<td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="381" align="left" valign="middle"><table width="125" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td width="37" align="left"><img src="<?php echo site_url(''); ?>assets/pdf_images/residential_solar/phone.jpg" width="25" height="25" alt=""/></td>
													<td width="41" align="center" style="font-size:20px;"><strong>13</strong></td>
													<td width="35" align="center" style="font-size:20px;"><strong>58</strong></td>
													<td width="37" align="center" style="font-size:20px;"><strong>42</strong></td>
												</tr>
											</tbody>
										</table></td>
										<td width="405" align="right" valign="top"><img src="<?php echo site_url(''); ?>assets/images/logo.svg" width="163"  alt=""/></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td height="20"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
</div>

<div id="page6" class="w910" style="display: none;">
    <table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
  <tr>
    <td valign="top" style="border: 0; font: 0"><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2" height="20"></td>
              </tr>
              <tr>
                <td width="58" align="left" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/header_lines.jpg" width="45" height="40" alt=""/></td>
                <td width="822" style="font-size: 25px; color: #000000; text-transform: uppercase"><strong><span style="color: #DE1717">YOUR</span> SOLAR SYSTEM AND INCLUSIONS </strong></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="50"></td>
        </tr>
        <tr>
          <td valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td width="440" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td style="color:#DE1717; font-size:16px"><strong>Invoice to</strong></td>
                    </tr>
                    <tr>
                      <td height="15"></td>
                    </tr>
                    <tr>
                      <td style="font-size: 15px; color: #000000"><strong><?php echo $customer_data['first_name'] . ' ' . $customer_data['last_name']; ?></strong></td>
                    </tr>
                    <tr>
                      <td height="20" style="font-size: 14px; color: #000000"><?php echo str_ireplace(", Australia", "", $customer_data['address']); ?></td>
                    </tr>
                  </tbody>
                </table></td>
                <td width="440" align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td height="40" align="right" style="font-size: 20px; border-bottom: solid 2px #DE1717"><strong>YOUR QUOTE</strong></td>
                    </tr>
                    <tr>
                      <td height="5"></td>
                    </tr>
                    <tr>
                      <td><table width="370" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td width="22"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/calender.jpg" width="16" height="16" alt=""/></td>
                            <td width="350" style="font-size: 15px; color: #000000"><strong>DATE:</strong> <?php echo date('d-m-Y'); ?></td>
                            <td width="22" align="right"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/quote.jpg" width="16" height="16" alt=""/></td>
                            <td width="122" align="right" style="font-size: 15px; color: #000000"><strong>QUOTE ID:</strong> <?php echo $proposal['id']; ?></td>
                          </tr>
                        </tbody>
                      </table></td>
                    </tr>
                    <tr>
                      <td height="18"></td>
                    </tr>
                  </tbody>
                </table></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="25"></td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="840" border="1" bordercolor="#999999" cellspacing="0" cellpadding="7" style="border-collapse:collapse; border:solid 1px #999999">
            <tbody>
              <tr>
                <th width="424" align="left" style="font-size:16px; color: #ffffff; background: #DE1717; text-transform: uppercase"><strong>KUGA SYSTEM</strong></th>
                <th width="422" align="right" style="font-size:16px; color: #ffffff; background: #DE1717;  text-transform: uppercase"><strong>QTY</strong></th>
              </tr>
              <?php if (!empty($panel_data)) {?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $panel_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong><?php echo $no_of_panels; ?></strong></td>
                </tr>
              <?php } ?>
              <?php if (!empty($inverter_data)) { ?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $inverter_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong>1</strong></td>
                </tr>
              <?php } ?>
              <tr>
                <td style="font-size:14px; color: #000000"><strong>Mounting Systems</strong></td>
                <td align="right" style="font-size:14px; color: #000000"><strong>1 pack</strong></td>
              </tr>
              <?php if (!empty($battery_data)) { ?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $battery_data['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong>1</strong></td>
                </tr>
              <?php } ?>
              <?php foreach ($proposal_additional_data as $key => $value) {?>
                <tr>
                  <td style="font-size:14px; color: #000000"><strong><?php echo $value['name']; ?></strong></td>
                  <td align="right" style="font-size:14px; color: #000000"><strong><?php echo$value['qty']; ?></strong></td>
                </tr>
              <?php } ?>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="20"></td>
        </tr>
        <tr>
          <td height="110" align="right" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="7">
            <tbody>

              <?php if($system_cost > 0){ ?>
                <tr>
                 <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>SYSTEM COST</strong></td>
                 <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $system_cost; ?></strong></td>
               </tr>
             <?php } ?>
             <?php if($stc_deduction > 0){ ?>
              <tr>
                <td colspan="2" style="padding: 1px !important;"></td>
              </tr>
              <tr>
                <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>STC INCENTIVE</strong></td>
                <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>($<?php echo $stc_deduction; ?>)</strong></td>
              </tr>
            <?php } ?>
            <?php if ($is_vic_rebate) { ?>
             <tr>
              <td colspan="2" style="padding: 1px !important;"></td>
            </tr>
            <tr>
              <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>SOLAR VICTORIA REBATE</strong></td>
              <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$<?php echo $vic_rebate; ?></strong></td>
            </tr>
          <?php } ?>
          <tr>
            <td colspan="2" style="padding: 1px !important;"></td>
          </tr>
          <tr>
            <td width="278" style="background: #000000; text-transform: uppercase; color: #ffffff; font-size:16px; text-align: right"><strong>TOTAL PAYABLE</strong> <span style="font-size: 14px">(GST INCLUSIVE)</span></td>
            <td width="120" align="center" style="background: #DE1717; color: #ffffff; border-left:solid 1px #ffffff"><strong>$ <?php echo $total_payable; ?> </strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td align="left" valign="top" style="border-bottom: solid 1px #000000; padding-bottom: 5px"><table width="840" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="38"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/services.jpg" width="24" height="24" alt=""/></td>
            <td width="842" style="color: #DE1717; font-size: 16px"><strong>Services</strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td height="18"></td>
    </tr>
    <tr>
      <td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="440" height="27" style="font-size: 14px; color: #000000;"><strong>Cable Reticulation</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
          <tr>
            <td height="27" style="font-size: 14px; color: #000000;"><strong>Fixing, Support, Drilling, Waterproofing</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
          <tr>
            <td height="27" style="font-size: 14px; color: #000000;"><strong>Installation</strong></td>
            <td width="440" align="right" style="color: #DE1717; font-size: 14px;"><strong><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/included.png" alt=""/></strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td height="50" align="left" valign="top">&nbsp;</td>
    </tr>
    <?php for($i = 0; $i < $new_count; $i++) { ?>
        <tr>
            <td height="40" align="left" valign="top">&nbsp;</td>
        </tr>
    <?php } ?>
    <tr>
      <td align="left" valign="top" style="border-bottom: solid 1px #000000; padding-bottom: 5px"><table width="840" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="38"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/warranty.jpg" width="24" height="24" alt=""/></td>
            <td width="842" style="color: #DE1717; font-size: 16px"><strong>Services</strong></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td  height="<?php $height; ?>"></td>
    </tr>
    <tr>
      <td height="27" align="left" style="font-size: 14px; color: #000000;"><strong>Please visit the link below for the Warranty Infromation:</strong></td>
    </tr>
    <tr>
      <td height="5"></td>
    </tr>
    <tr>
      <td height="25" align="left" style="color: #DE1717; font-size: 14px"><strong>https://www.13kuga.com.au/warranty-information/</strong></td>
    </tr>
    <tr>
      <td height="250" align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
      <td align="left" valign="top"><table width="840" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="381" align="left" valign="middle"><table width="125" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="37" align="left"><img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/phone.jpg" width="25" height="25" alt=""/></td>
                  <td width="41" align="center" style="font-size:20px;"><strong>13</strong></td>
                  <td width="35" align="center" style="font-size:20px;"><strong>58</strong></td>
                  <td width="37" align="center" style="font-size:20px;"><strong>42</strong></td>
                </tr>
              </tbody>
            </table></td>
            <td width="405" align="right" valign="top"><img src="<?php echo $this->config->item('live_url'); ?>assets/images/logo.svg" width="163" alt=""/></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
        <td height="50"></td>
	</tr>
  </tbody>
</table></td>
</tr>
</table>
</div>

<div class="overlay-roadblock">
    <div class="overlay-roadblock__content" id="overlay_content">
       
    </div>
</div>

<div id="canvas_container">
	<canvas id="canvas" width="910"></canvas>
</div>

<script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script>
	var base_url = '<?php echo site_url(); ?>';
</script>

<script>
    var fileConverted = [false,false,false,false,false,false];
    
	$(window).load(function() {
		$('.overlay-roadblock').show();
		$('#overlay_content').html('<div class="loader"></div>Processing Images for Pdf Creation Please Wait it will take approx a min...');

		setTimeout(function(){
			$('#page2').show();
			var box = document.querySelector('#page2');
			var width = box.offsetWidth;
			var height = box.offsetHeight;

			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')

			html2canvas(document.querySelector("#page2"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg1').val(dataimg);
			});
            
		},2000);

		setTimeout(function(){
			$('#page2').hide();
			$('#page3').show();
			var box = document.querySelector('#page3');
			var width = box.offsetWidth;
			var height = box.offsetHeight;
			
			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')
			
			html2canvas(document.querySelector("#page3"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg2').val(dataimg);
			});
			
		},4500);


		setTimeout(function(){
			$('#page2').hide();
			$('#page3').hide();
			$('#page4').show();
			var box = document.querySelector('#page4');
			var width = box.offsetWidth;
			var height = box.offsetHeight;
			
			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')
			
			html2canvas(document.querySelector("#page4"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg3').val(dataimg);
			});
			
		},6500);

		setTimeout(function(){
			$('#page2').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page5').show();
			var box = document.querySelector('#page5');
			var width = box.offsetWidth;
			var height = box.offsetHeight;
			
			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')
			
			html2canvas(document.querySelector("#page5"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg4').val(dataimg);
			});
			
		},8500);

		setTimeout(function(){
			$('#page2').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page5').hide();
			$('#page1').show();

			var box = document.querySelector('#page1');
			var width = box.offsetWidth;
			var height = box.offsetHeight;

			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')

			html2canvas(document.querySelector("#page1"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg5').val(dataimg);
			});

		},10500);
		
		
		setTimeout(function(){
			$('#page2').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page5').hide();
			$('#page1').hide();
			$('#page6').show();

			var box = document.querySelector('#page6');
			var width = box.offsetWidth;
			var height = box.offsetHeight;

			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')

			html2canvas(document.querySelector("#page6"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg6').val(dataimg);
				create_image_to_pdf(0);
			});

		},12500);
		
		
	

		/**setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/product/residential_solar/save_dataimg_to_png',
				datatype: 'json',
				data: $('#chartForm').serialize(),
				success: function (stat) {
					$('.overlay-roadblock').show();
					$('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
					var puuid = "<?php echo $proposal['uuid']; ?>";
					//window.location.href = base_url + "admin/product/residential_solar/proposal_pdf_download/" + puuid + '?view=download';
				},
				error: function (xhr, ajaxOptions, thrownError) {

				}
			});
		},9500);*/
	});
	
	function create_image_to_pdf(key){
	        var dkey = 'dataImg'+(key+1);
	        var fkey = 'filename'+(key+1);
	        $.ajax({
				type: 'POST',
				url: base_url + 'admin/product/residential_solar/save_dataimg_to_png',
				datatype: 'json',
				//data: 'pId='+$('#pId').val()+'&'+dkey+'='+$('#'+dkey).val() +'&'+fkey +'=' +$('#'+fkey).val(),
				data: $('#chartForm'+(key+1)).serialize() + '&key=' + (key+1),
				beforeSend: function(){
				    $('#overlay_content').html('<div class="loader"></div>Converting Page '+ (key + 1) + ' of 6');
				},
				success: function (stat) {
				    if(key < 6){
				        create_image_to_pdf((key+1));
				    }
				    fileConverted[key] = true;
				    var is_all_converted = true;
				    for(var key1 in fileConverted){
				        if(fileConverted[key1] == false){
				            is_all_converted = false;
				        }
				    }
				    if(is_all_converted){
    					$('.overlay-roadblock').show();
    					$('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
    					var puuid = "<?php echo $proposal['uuid']; ?>";
    					window.location.href = base_url + "admin/product/residential_solar/proposal_pdf_download/" + puuid + '?view=download';
				    }
				},
				error: function (xhr, ajaxOptions, thrownError) {

				}
			});
	    }
</script>
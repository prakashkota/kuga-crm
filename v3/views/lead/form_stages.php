<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Stage Details</h4>
                                    <div class="form-group">
                                        <label class="control-label">Stage Name</label>
                                        <input type="text" name="stage_name"  class="form-control" placeholder="Stage Name" value="<?php echo (isset($stage_name)) ? $stage_name : ''; ?>"  required>
                                        <?php echo form_error('stage_name', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Order No</label>
                                        <input type="number" name="order_no"  class="form-control" placeholder="Order Number" value="<?php echo (isset($order_no)) ? $order_no : ''; ?>"  required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="buttwrap">
                        <input type="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

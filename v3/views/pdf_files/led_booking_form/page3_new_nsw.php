<?php
$space_type = json_decode(json_encode($space_type), true);
$ceiling_height = json_decode(json_encode($ceiling_height), true);
$defaultHeight = 700;
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding:0; margin:0;">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01-wide.jpg'; ?>" alt="" width="100%" height="100" /></td>
    </tr>
    <tr>
        <td height="10">&nbsp;</td>
    </tr>
    <!--<tr>-->
    <!--    <td valign="top">-->
    <!--        <table width="100%" border="1" bordercolor="#e8e8e8" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:solid 1px #e8e8e8">-->
    <!--            <tr>-->
    <!--                <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">&nbsp;</th>-->
    <!--                <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Area 1</th>-->
    <!--                <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Area 2</th>-->
    <!--                <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Area 3</th>-->
    <!--            </tr>-->
    <!--            <tr>-->
    <!--                <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;">Space Type:</td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($space_type[0])) ? $space_type[0] : ''; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($space_type[1])) ? $space_type[1] : ''; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($space_type[2])) ? $space_type[2] : ''; ?></strong></td>-->
    <!--            </tr>-->
    <!--            <tr>-->
    <!--                <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;">Celling Height:</td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($ceiling_height[0])) ? $ceiling_height[0] : ''; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($ceiling_height[1])) ? $ceiling_height[1] : ''; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo (isset($ceiling_height[2])) ? $ceiling_height[2] : ''; ?></strong></td>-->
    <!--            </tr>-->
    <!--            <tr>-->
    <!--                <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">Zone Classification</th>-->
    <!--                <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">-->
    <!--                    <table width="100%" border="0" cellspacing="0" cellpadding="0">-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">Reach Up:</td>-->
    <!--                        </tr>-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Ceiling Height)</td>-->
    <!--                        </tr>-->
    <!--                    </table>-->
    <!--                </th>-->
    <!--                <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">-->
    <!--                    <table width="100%" border="0" cellspacing="0" cellpadding="0">-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">Clearance:</td>-->
    <!--                        </tr>-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Height of item blocking access)</td>-->
    <!--                        </tr>-->
    <!--                    </table>-->
    <!--                </th>-->
    <!--                <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">-->
    <!--                    <table width="100%" border="0" cellspacing="0" cellpadding="0">-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">Reach Across:</td>-->
    <!--                        </tr>-->
    <!--                        <tr>-->
    <!--                            <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Distance from the alsle to light)</td>-->
    <!--                        </tr>-->
    <!--                    </table>-->
    <!--                </th>-->
    <!--            </tr>-->
    <!--            <tr>-->
    <!--                <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;">Boom Requirements:</td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $boom_req->reach_up; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $boom_req->clearance; ?></strong></td>-->
    <!--                <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $boom_req->reach_across; ?></strong></td>-->
    <!--            </tr>-->
    <!--        </table></td>-->
    <!--</tr>-->
    <!--<tr>-->
    <!--    <td height="30" valign="top">&nbsp;</td>-->
    <!--</tr>-->
    <tr>
        <td valign="top">
            
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                   <th colspan="4" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">&nbsp;</th>
                   <th  colspan="6" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Existing Lighting (Baseline)</th>
                   <th  colspan="6" style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Upgraded Lighting (Baseline)</th>
                </tr>
                <tr>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Area Name</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Celling Height</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Space Type</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">BCA Classification</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Air-con?</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Qty</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Lamp &amp; Ballast Combination</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Nominal Lamp Power</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Control Device</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Product Type</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Product Model</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Control Device</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Sensor</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Qty</th>
                   <th style=" background:#000; border:1px solid #e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Unit Cost exc GST</th>
                   <th style=" background:#000; border:1px solid #e8e8e8e8e8e8; color:#fff; text-align:center; font-size:12px;padding: 2px 0px;">Total Cost exc GST</th>
                </tr>
                <tbody>
                    <?php
                        $height = 130;
                    for ($i = 0; $i < count($product->item_name); $i++) {
                        $total_cost_excGST = '';
                        if ($product->item_qty[$i] != '' && $product->item_cost_excGST[$i] != '') {
                            $total_cost_excGST = $product->item_qty[$i] * $product->item_cost_excGST[$i];
                        }
                        if($product->item_name[$i] != ''){
                            $typeData = $this->common->fetch_row('tbl_product_types','*',['type_id'=>$product->type_id[$i]]);
                            $anual_op_hrs = 0;
                            for($idx=0;$idx<count($calculator_items['bca_types']);$idx++){
                                if($calculator_items['bca_types'][$idx]['type_of_space'] == $product->item_bca_type[$i]){
                                    $anual_op_hrs = $calculator_items['bca_types'][$idx]['op_hrs_per_yr'];        
                                }
                            }
                    ?>
                   <tr>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_room_area[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_ceiling_height[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_space_type[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_bca_type[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_air_con[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_existing_qty[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_lamp_and_ballast_type[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_existing_nominal_lamp_power[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_control_system[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $typeData['type_name']; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_name[$i]; ?></strong></td>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->control_device_upgrade_lighting[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_sensor[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_qty[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $product->item_cost_excGST[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php }} ?> 
                <tr>
                    <th height="40" colspan="6" style="text-align: right; padding-right: 10px;">Further Products quoted can be found on a separate sheet: </th>
                    <th colspan="4" style="text-align: left; padding-left: 10px;">
                       <?php if (isset($booking_form->is_more_products)) { ?>
                            <input type="checkbox" checked="" style="margin-left:10px;" />
                        <?php } else { ?>
                            <input type="checkbox" style="margin-left:10px;" />
                        <?php } ?>
                    </th>
                    <th colspan="5" style="text-align: right; padding-right: 10px;">Sub Total exc GST:</th>
                    <th>$<?php echo $product->total_excGst; ?></th>
                </tr>
                
                <tr>
                    <th colspan="4" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:12px;">Access Equipment</th>
                    <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Detailed Description of Access Equipment</th>
                    <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Quantity</th>
                    <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Unit Cost exc GST</th>
                    <th colspan="3" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:12px;">Total Cost exc GST</th>
                </tr>
                <?php
                for ($i = 0; $i < count($ae->ae_name); $i++) {
                    $total_cost_excGST = '';
                    if ($ae->ae_qty[$i] != '' && $ae->ae_cost_excGST[$i] != '') {
                        $total_cost_excGST = $ae->ae_qty[$i] * $ae->ae_cost_excGST[$i];
                    }
                    ?>
                    <tr>
                        <td colspan="4" height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_name[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_description[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_qty[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;"><strong><?php echo $ae->ae_cost_excGST[$i]; ?></strong></td>
                        <td colspan="3" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="13" height="30" colspan="4" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:12px;">Sub Total exc GST:</td>
                    <td colspan="2"  style="padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $ae->total_excGst; ?></strong></td>
                </tr>
                
                
                </tbody>
            </table></td>
    </tr>
        <tr>
        <td valign="top">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="560" height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="147" height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="147" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_excGst; ?></strong></td>
                </tr>
                <tr>
                    <td width="560" height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_Gst; ?></strong></td>
                </tr>
                <tr>
                    <td width="560" height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="30" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:12px; text-align:right;"><strong>$<?php echo $booking_form->total_incGst; ?></strong></td>
                </tr> 
                </table>
                </td>
                </tr>
                
                <?php
                    $itemname = $product->item_name;
                    $product->item_name = array_filter($itemname);
                    $defaultHeight = (count($product->item_name) > 1 ) ? 45 : 160;
                ?>
    <tr>
        <td height="<?= $defaultHeight ?>">&nbsp;</td>
    </tr>
    <tr>
        <td height="70" class="bg-bottom" style="background:#ededed"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:12px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 4 Bridge Rd, Keysborough VIC 3173</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 6 Turbo Rd, Kings Park, NSW 2148</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif"> 31 Chetwynd St, Loganholme, QLD 4129 </td>
                            </tr>
                             
                        </table></td>
                    <td align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" /></td>
                </tr>
            </table></td>
    </tr>
</table>

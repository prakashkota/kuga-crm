<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property TenderQuotes Controller
 * @version 1.0
 */
class Tender extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $models = array(
                        'Tender_Model' => 'tender',
                        'TenderSites_Model' => 'sites',
                        'TenderSitesType_Model' => 'sitetype',
                        'TenderSitesSystemoverview_Model' => 'systemoverview',
                        'TenderSitesSystemcost_Model' => 'systemcost',
                        'TenderDocumentTypes_Model' => 'documenttypes', 
                        'TenderDocuments_Model' => 'documents',
                        'Product_Model' => 'product',
                        'TenderJob_Model' => 'job',
                    );
        foreach ($models as $file => $object_name) {
            $this->load->model($file, $object_name); 
        }
    
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }
    public function add($id=0) {
        if (!$this->aauth->is_member('Admin') && !($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2)) {
            redirect('admin/dashboard');
        }

        
        $data = array();
        $team_types = TEAM_REP_TYPES;
        
        //If not a team leader redirect to dashboard
        /*if(!array_key_exists($group_id, $team_types)){
           $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
           redirect('admin/dashboard'); 
        }*/
        
        $logged_in_user_grp_id = $this->aauth->get_user_groups()[0]->id;
       // if($logged_in_user_grp_id != 1){
            $userid = $this->aauth->get_user_id();
            $users = $this->user->get_sub_users(FALSE,FALSE,$userid);
        //}
        $cond = 'id='.$id;
        if($logged_in_user_grp_id == 7){
            $cond = 'id='.$id." AND repId='{$userid}'";
        }
        $data['admin'] = $this->user->get_users(FALSE,$this->session->userdata('id'),FALSE);
        $data['users'] = $users;
        $data['title'] = 'Manage ';
        $tenderData['statusTypes'] = TENDER_STATUS_TYPES;
        $tender = array();
        $id= (int)$id;
        if((int)$id>0){
            $tender=$this->common->fetch_where('tbl_tender','*',$cond);
            if(count($tender)>0){
                $tenderData['data'] = $tender[0];
                $tenderSites=$this->common->fetch_where('tbl_tender_sites','*','tenderId='.(int)$tenderData['data']['id']);
                $tenderData['data']['sites'] = $tenderSites;


                $tenderDocumentTypes = $this->db->query("select dcTy.id,dcTy.name,count(doc.id) as noOfDoc from tbl_tender_document_types as dcTy
    LEFT JOIN tbl_tender_documents as doc ON doc.documentTypeId = dcTy.id WHERE dcTy.tenderId={$tenderData['data']['id']} GROUP BY dcTy.id");

                $tenderData['data']['documentTypes'] = $tenderDocumentTypes->result_array();
            } else {
                redirect('/admin/tender/add/0');
            }
        } else {
            $tenderData['data'] = array('id'=>0,'name'=>'','status'=>'1','client'=>'','contactPerson'=>'','mobile'=>'','email'=>'','contractNumber'=>'');
            $tenderData['data']['sites'] = array();
            $tenderData['data']['documentTypes'] = array();
        }
    
       
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['tender'] = $tender;
        $data['activity_modal_title'] = 'Add/Schedule Activity';
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('activity/activity_modal');
        $this->load->view('tender/add',$tenderData);
        $this->load->view('partials/footer');
    }
    public function saveTender(){
        if (!$this->aauth->is_member('Admin') && !($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2)) {
            redirect('admin/dashboard');
        }
        if (!empty($this->input->post())) {
            if($this->aauth->is_member('Admin')){
                $repId = $this->input->post('repId');
            }else{
                $repId = $this->session->userdata('id');
            }
            $error = $tenderData = array();
            $this->form_validation->set_rules('name', 'Tender Name', 'required');
            $this->form_validation->set_rules('mobile', 'Tender Mobile', 'required');
            $this->form_validation->set_rules('email', 'Tender Email', 'required|valid_email');
            $this->form_validation->set_rules('contractNumber', 'Contract Number', 'required');
             $id = $this->input->post('tenderId');
            if ($this->form_validation->run() != FALSE){
                $tenderData = array(
                    'name'=>$this->input->post('name'),
                    'email'=>$this->input->post('email'),
                    'mobile'=>$this->input->post('mobile'),
                    'client'=>$this->input->post('client'),
                    'contactPerson'=>$this->input->post('contactPerson'),
                    'contractNumber'=>$this->input->post('contractNumber')
                );
                
                if($this->input->post('tenderId') == 0){
                    $tenderData['created_by']=$this->session->userdata('id');
                    $tenderData['repId']=$this->session->userdata('id');
                    $this->common->insert_data('tbl_tender',$tenderData);
                    $id = $this->common->insert_id();
                   
                }else {
                    
                    $this->common->update_data('tbl_tender', 'id='.$id,$tenderData);
                }
                $dataSave = true;
            }else {
                $dataSave = false;
                $error = $this->form_validation->error_array();
            }
            echo json_encode(array('success'=>$dataSave,'id'=>$id,'errors'=>$error,'tenderData'=>$tenderData));
            exit;
        }   
      
    }
    public function saveSite(){
        if (!empty($this->input->post())) {
            $error = array();
            $this->form_validation->set_rules('name', 'Site Name', 'required');
            $this->form_validation->set_rules('tenderId', 'Please save tender details', 'required');
            $this->form_validation->set_rules('address', 'Site Addrerss', 'required');
            $id = $this->input->post('siteId');
            if ($this->form_validation->run() != FALSE){
                $tenderSiteData = array(
                    'name'=>$this->input->post('name'),
                    'tenderId'=>$this->input->post('tenderId'),
                    'address'=>$this->input->post('address'),
                );
                if($this->input->post('siteId') == 0){
                    $this->common->insert_data('tbl_tender_sites',$tenderSiteData);
                    $id = $this->common->insert_id();
                    $siteType=array(
                        array('siteId'=>$id,'optionId'=>1,'type'=>'pl','filename'=>''),
                        array('siteId'=>$id,'optionId'=>1,'type'=>'il','filename'=>''),
                        array('siteId'=>$id,'optionId'=>1,'type'=>'mu','filename'=>'')
                    );
                    foreach($siteType as $eachType){
                          $this->common->insert_data('tbl_tender_site_type',$eachType);
                    }
                    $overView= array('siteId'=>$id,'optionId'=>1);
                    $this->common->insert_data('tbl_tender_sites_systemoverview',$overView);
                }else {
                    $this->common->update_data('tbl_tender_sites', array('id' => $id),$tenderSiteData);
                }
                $dataSave = true;
            }else {
                $dataSave = false;
                $error = $this->form_validation->error_array();
            }
            echo json_encode(array('success'=>$dataSave,'id'=>$id,'errors'=>$error));
            exit;
        }
    }
    public function loadoptions(){
        $data['siteTypes'] = SITE_TYPES ;
        $data['siteUploadPath'] = SITE_UPLOAD_PATH;
        $data['roofOptions'] = ROOF_OPTIONS;
        
        if(!empty($this->input->post())){
            $siteId = $this->input->post('siteId');
            $data['siteId'] = $siteId;
            $siteTypes = $this->common->fetch_where('tbl_tender_site_type','*',"siteId='{$siteId}'");
            $siteDetails = array();
            foreach($siteTypes as $eachSiteType){
                $siteDetails[$eachSiteType['type']][$eachSiteType['optionId']]=$eachSiteType['filename'];
            }
            $data['siteDetails'] = $siteDetails;

            $systemCostTypes = $this->common->fetch_where('tbl_site_cost_types','*',"siteId='{$siteId}'");
            $systemcostTypes = array();
            foreach($systemCostTypes as $eachCostType) {
                $systemcostTypes[$eachCostType['id']] = $eachCostType['name'];
            }

            $systemcost = $this->common->fetch_where('tbl_tender_sites_systemcost','*',"siteId='{$siteId}'");
            $systemCostDetails = array();
            foreach($systemcost as $eachSiteCostType){
                foreach($eachSiteCostType as $k=>$v){
                    $systemCostDetails[$eachSiteCostType['typeId']][$eachSiteCostType['optionId']][$k]=$v;
                }
            }
            $data['systemcostDetails'] = $systemCostDetails;
            $data['systemcostTypes'] = $systemcostTypes;
            $systemoverview = $this->common->fetch_where('tbl_tender_sites_systemoverview','*',"siteId='{$siteId}'");
            $systemOverviewDetails = array();
            foreach($systemoverview as $eachSiteOverView){
                foreach($eachSiteOverView as $fieldName=>$fieldValue){
                    $systemOverviewDetails[$fieldName][$eachSiteOverView['optionId']]=$fieldValue;
                }
            }

            $data['systemOverviewDetails'] = $systemOverviewDetails;       
        }

        $this->load->view('tender/siteData',$data);
    }
    public function uploadsiteimages($id) {
        $config['upload_path']=SITE_UPLOAD_PATH;
		$config['allowed_types']='jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload',$config);
        $i=1;
        $insertData = array();
        $fileuploaded = false;
        while($i<=TENDER_OPTIONS){
            foreach(array_keys(SITE_TYPES) as $eachType){
                 $fieldName = "{$eachType}_{$i}";
                 if(!empty($_FILES[$fieldName]['name'])) {
                     if ($this->upload->do_upload($fieldName)) {
                         $insertData['type'] = $eachType;
                         $insertData['siteId'] = $id;
                         $insertData['optionId'] = $i;
                         $data = $this->upload->data();
                         $insertData['filename'] = $data['file_name'];
                         $fileuploaded = true;
                         break 2;
                     } else {
                         $id = 0;
                         $dataSave = false;
                         $error = $this->upload->display_errors();
                     }
                 }
            }
            $i++;    
        }
        if($fileuploaded){
            $this->common->update_data('tbl_tender_site_type',"siteId='{$id}' AND type='{$insertData['type']}' AND optionId={$insertData['optionId']}",$insertData);
            $insertData['success'] = true;
        } else {
            $insertData['success'] = false;
            $insertData['errors'] = $error;
        }
		echo json_encode($insertData);
		exit;
       
    }
    public function deleteSite(){
		$data = $this->input->post();
        //Start Trancsaction
        $this->db->trans_begin();
		$siteId = $data['siteId'];
        $deleteTables = array(
        'tbl_tender_site_type',
        'tbl_tender_sites_systemoverview',
        'tbl_tender_sites_systemcost');
        $siteTypes = $this->common->fetch_where('tbl_tender_site_type','*',
        "siteId={$siteId}");
        foreach($siteTypes as $eachSiteType){
            unlink(SITE_UPLOAD_PATH.$eachSiteType['filename']);
        }
        foreach($deleteTables as $eachTable){
            $this->common->delete_data($eachTable,"siteId='{$siteId}'");
        }
        $this->common->delete_data('tbl_tender_sites',"id='{$siteId}'");
        //Check Transaction status and take necessary action
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $status['success'] = FALSE;
            $status['status'] = 'Looks like something went wrong. While trying to perform the action.';
        } else {
            $this->db->trans_commit();
            $status['success'] = TRUE;
            $status['status'] = 'Action Successfully Completed.';
        }
		echo json_encode ($status); exit;
	}
    public function deleteSiteType(){
        $siteId=$this->input->post('siteId');
        $sType=$this->input->post('sType');
        $optionId=$this->input->post('optionId');
		$result = $this->common->fetch_where('tbl_tender_site_type','*',
        "type='{$sType}' AND siteId={$siteId} AND optionId={$optionId}");
		unlink(SITE_UPLOAD_PATH.$result[0]['filename']);
		$this->common->update_data('tbl_tender_site_type',"id={$result[0]['id']}",array('filename'=>''));
		echo json_encode(array('success'=>true));
		exit;
	}
    public function saveSiteInformation($id){

        if($this->input->post()){
            $siteId=(int)$id;
            $systemCost = $this->input->post('systemCost');
            $systemOverView = $this->input->post('systemOverView');
            foreach($systemOverView as $optionId=>$SysOVoptionData){
                $changedRows1 =$this->common->update_data('tbl_tender_sites_systemoverview',"siteId={$siteId} AND optionId='{$optionId}'" ,$SysOVoptionData);
            }
           
            foreach($systemCost as $typeId=>$eachCostDetails){
                foreach($eachCostDetails as $optionId=>$optionData){
                    $siteCostData=array('siteId'=>$siteId,'optionId'=>$optionId,'typeId'=>$typeId,'unitprice'=>$optionData['unitprice'],'quantity'=>$optionData['quantity']);
                    $changedRows[] = $this->common->update_data('tbl_tender_sites_systemcost',"typeId='{$typeId}' AND siteId='{$siteId}' AND optionId='{$optionId}'",$siteCostData);
                } 
            }
            if(($changedRows1+array_sum($changedRows)) > 0){
                echo json_encode(array('success'=>true));
                exit;
            } else {
                echo json_encode(array('success'=>false));
                exit;
            }
        }

    }
    public function saveTenderDocCat(){

        if (!empty($this->input->post())) {
            $error = $tenderData = array();
            $this->form_validation->set_rules('dcName', 'Document Type', 'required');
            $id = 0;
            $documentTypes = array();
            if ($this->form_validation->run() != FALSE){
                $dataSave = true;
                $data['name'] = $this->input->post('dcName');
                $data['tenderId'] = $this->input->post('tenderId');
                $this->common->insert_data('tbl_tender_document_types',$data);
                $tenderDocumentTypes = $this->db->query("select dcTy.id,dcTy.name,count(doc.id) as noOfDoc from tbl_tender_document_types as dcTy
    LEFT JOIN tbl_tender_documents as doc ON doc.documentTypeId = dcTy.id WHERE dcTy.tenderId={$data['tenderId']} GROUP BY dcTy.id");
                $documentTypes = $tenderDocumentTypes->result_array();
            } else {
                $dataSave = false;
                $error = $this->form_validation->error_array();
            }
            echo json_encode(array('success'=>$dataSave,'id'=>$id,'errors'=>$error,'documentTypes'=>$documentTypes));
            exit;
        }
    }
    public function loadTenderDocumentTypes(){
        $data['documentTypes'] = array();
        if(!empty($this->input->post())){
            $tenderId = $this->input->post('tenderId');
            $tenderDocumentTypes = $this->db->query("select dcTy.id,dcTy.name,count(doc.id) as noOfDoc from tbl_tender_document_types as dcTy
    LEFT JOIN tbl_tender_documents as doc ON doc.documentTypeId = dcTy.id WHERE dcTy.tenderId=? GROUP BY dcTy.id",array($tenderId));
            $data['documentTypes'] = $tenderDocumentTypes->result_array();
        }
        $this->load->view('tender/loadDocTypes',$data);
    }
    public  function uploadTenderDocuments(){
        if(!empty($this->input->post())){
            $config['upload_path']=TENDER_SITE_UPLOAD_PATH;
            $config['allowed_types']='jpg|png|jpeg|pdf';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload',$config);
            $docTypeId = $this->input->post('docTypeId');
            $fileName = "doctype_{$docTypeId}";
            $error = array();
            if($this->upload->do_upload($fileName)){
                $data = $this->upload->data();
                $insertData['documentTypeId'] = $docTypeId;
                $insertData['filename'] = $data['file_name'];
                $this->common->insert_data('tbl_tender_documents',$insertData);
                $id = $this->common->insert_id();
                $dataSave = true;
            } else {
                $id = 0;
                $dataSave = false;
                $error  = $this->upload->display_errors();
            }
            echo json_encode(array('success'=>$dataSave,'id'=>$id,'errors'=>$error));
            exit;

        }
    }
    public function downloadZip($id){
        $this->load->library('zip');
        $tender=$this->common->fetch_where('tbl_tender','*','id='.(int)$id);
        $tenderDocumentTypes = $this->db->query("select dcTy.id,dcTy.name,doc.filename from tbl_tender_document_types as dcTy
    LEFT JOIN tbl_tender_documents as doc ON doc.documentTypeId = dcTy.id WHERE dcTy.tenderId=? ORDER BY dcTy.id ASC",array($tender[0]['id']));
        $documentTypes = $tenderDocumentTypes->result_array();
        if(count($documentTypes)>0) {
            $i=1;
            foreach ($documentTypes as $row) {
                if($i!=1 && $holdId!=$row['id']){
                    $i=1;
                }
                $fnameArray = explode('.', $row['filename']);
                $newName = $row['name']."_{$i}.".end($fnameArray);
                $this->zip->read_file(TENDER_SITE_UPLOAD_PATH.$row['filename'],false,$newName);
                $holdId=$row['id'];
                $i++;
            }
            $this->zip->download("{$tender['0']['name']}-Attachments.zip");
        }
        exit;
    }
    public function addSiteOption(){
        $dataSave=false;
        if($this->input->post('siteId') > 0){
            $id = $this->input->post('siteId');
            $siteOptionsResult = $this->db->query("select optionId from tbl_tender_site_type  WHERE siteId='{$id}' GROUP BY optionId ORDER BY optionId DESC LIMIT 0,1");
            $siteOptions = $siteOptionsResult->result_array();

            if($siteOptions[0]['optionId'] < TENDER_OPTIONS ) {
                $optionId = $siteOptions[0]['optionId']+1;
                $siteType=array(
                    array('siteId'=>$id,'optionId'=>$optionId,'type'=>'pl','filename'=>''),
                    array('siteId'=>$id,'optionId'=>$optionId,'type'=>'il','filename'=>''),
                    array('siteId'=>$id,'optionId'=>$optionId,'type'=>'mu','filename'=>'')
                );
                foreach ($siteType as $eachType) {
                    $this->common->insert_data('tbl_tender_site_type', $eachType);
                }
                $overView = array('siteId' => $id, 'optionId' => $optionId);
                $this->common->insert_data('tbl_tender_sites_systemoverview', $overView);
                $siteCostTypeResultObj = $this->db->query("SELECT * FROM tbl_site_cost_types WHERE siteId='{$id}'");
                $siteCostTypeResult = $siteCostTypeResultObj->result_array();
                if(count($siteCostTypeResult) > 0 ) {
                    foreach ($siteCostTypeResult as $eachCostType) {
                        $siteCostType = array('siteId' => $id, 'optionId' => $optionId, 'typeId' => $eachCostType['id']);
                        $this->common->insert_data('tbl_tender_sites_systemcost', $siteCostType);
                    }
                }
                $dataSave=true;
            }
        }
        echo json_encode(array('success'=>$dataSave)); exit;
    }
    public function manage() {
        redirect('admin/tender/manage-tender?view=pipeline');
        if (!$this->aauth->is_member('Admin') && !($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2)) {
            redirect('admin/dashboard');
        }


        if($this->aauth->is_member('Admin')){
            $repIdCondition = "repId > 0 ";
        } else if($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2){
            $repIdCondition = "repId ='{$this->aauth->get_user_id()}'";
        }


        $data = array();


       $tenderData = $this->common->fetch_where('tbl_tender','*',$repIdCondition);

        $data['admin'] = $this->user->get_users(FALSE,1,FALSE);
        $data['tender'] = $tenderData;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('tender/manage');
        $this->load->view('partials/footer');
    }
    public function delete(){
        $data = $this->input->post();
        //Start Trancsaction
        $this->db->trans_begin();
        $tenderId = $data['tenderId'];
        $tenderSites=$this->common->fetch_where('tbl_tender_sites','*','id='.(int)$tenderId);
        foreach($tenderSites as $eachSite) {
            $siteId = $eachSite['id'];
            $deleteTables = array(
                'tbl_tender_site_type',
                'tbl_tender_sites_systemoverview',
                'tbl_tender_sites_systemcost');
            $siteTypes = $this->common->fetch_where('tbl_tender_site_type', '*',
                "siteId={$siteId}");
            foreach ($siteTypes as $eachSiteType) {
                @unlink(SITE_UPLOAD_PATH . $eachSiteType['filename']);
            }
            foreach ($deleteTables as $eachTable) {
                $this->common->delete_data($eachTable, "siteId='{$siteId}'");
            }
            $this->common->delete_data('tbl_tender_sites', "id='{$siteId}'");
        }
        $this->common->delete_data('tbl_tender', "id='{$tenderId}'");
        $tenderDocumentCats = $this->common->fetch_where('tbl_tender_document_types','*','tenderId='.(int)$tenderId);
        $this->common->delete_data('tbl_tender_document_types', 'tenderId='.(int)$tenderId);
        if(count($tenderDocumentCats) > 0) {
            foreach($tenderDocumentCats as $eachDocumentCat){
                $tDoc = $this->common->fetch_where('tbl_tender_documents', '*', 'documentTypeId=' . (int)$eachDocumentCat['id']);
                foreach($tDoc as $eachDoc){
                    @unlink(TENDER_SITE_UPLOAD_PATH . $eachDoc['filename']);
                }
                $this->common->delete_data('tbl_tender_documents', 'documentTypeId='.(int)$eachDocumentCat['id']);
            }
        }
        $this->common->delete_data('tbl_tender_document_types', "tenderId='{$tenderId}'");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $status['success'] = FALSE;
            $status['status'] = 'Looks like something went wrong. While trying to perform the action.';
        } else {
            $this->db->trans_commit();
            $status['success'] = TRUE;
            $status['status'] = 'Action Successfully Completed.';
        }
        echo json_encode($status); exit;
    }
    public function getTenderDocuments(){
        if($this->input->post()){
            $catId = $this->input->post('catId');
            if($catId > 0 ){
                $data['tenderDocuments'] = $this->common->fetch_where('tbl_tender_documents','*','documentTypeId='.(int)$catId);
                $this->load->view('tender/tender-documents',$data);
            }
        }
    }
    public function deleteTenderDocument(){
        if($this->input->post()){
            $docId = $this->input->post('docId');
            if($docId > 0 ){
                $this->db->trans_begin();
                $tDoc = $this->common->fetch_where('tbl_tender_documents','*','id='.(int)$docId);
                @unlink(TENDER_SITE_UPLOAD_PATH . $tDoc[0]['filename']);
                $this->common->delete_data('tbl_tender_documents', "id='{$tDoc[0]['id']}'");
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $status['success'] = FALSE;
            $status['status'] = 'Looks like something went wrong. While trying to perform the action.';
        } else {
            $this->db->trans_commit();
            $status['success'] = TRUE;
            $status['status'] = 'Action Successfully Completed.';
        }
        echo json_encode($status); exit;
    }
    public function downloadTenderDocument($id){
        if((int)$id > 0){
             $docId = (int)$id;
            if($docId > 0 ) {
                $tDoc = $this->common->fetch_where('tbl_tender_documents', '*', 'id=' . (int)$docId);

                if(file_exists(TENDER_SITE_UPLOAD_PATH . $tDoc[0]['filename'])) {
                    $this->load->helper('download');
                    $file_download = file_get_contents(TENDER_SITE_UPLOAD_PATH . $tDoc[0]['filename']);
                    force_download($tDoc[0]['filename'],$file_download);
                }
            }
        }
        exit;
    }

    public function manageTender() {
        //$this->load->library("Email_Manager", 'email_manager');
        //$this->email_manager->send_test_mail();
        $data = array();
        $data['user_id'] = ($this->input->get('user_id'))?$this->input->get('user_id'):$this->aauth->get_user_id();
        $data['user_group'] = $user_group = $this->aauth->get_user_groups()[0]->id;
        $all_reports_view_permission = $this->aauth->is_group_allowed(15, $user_group);
        $data['all_reports_view_permission'] = $all_reports_view_permission = ($all_reports_view_permission != NULL) ? $all_reports_view_permission : 0;
        $data['is_sales_rep'] = $data['is_lead_allocation_sales_rep'] = FALSE;

        if ($this->aauth->is_member('Admin')) {
            $users = $this->user->get_users(FALSE, FALSE, FALSE);
        } else if($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2) {
            $users = $this->user->get_users(FALSE, $this->aauth->get_user_id(), FALSE);
        }else {
                redirect('admin/dashboard');
        }
        $view = $this->input->get('view');
        $view = (isset($view) && $view == 'list') ? $view : 'pipeline';
        $data['users'] = $users;
        $data['tenderStatus'] = TENDER_STATUS_TYPES;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view("tender/manage_{$view}");
        $this->load->view('partials/footer');

    }

    private function handle_tender_data_filters() {
        $user_id = $this->aauth->get_user_id();
        $parsed_filter_options = '';
        $filter = $this->input->get('filter');

        $view = $this->input->get('view');

        if (isset($filter) && !empty($filter)) {
            if (isset($filter['start_date']) && $filter['start_date'] != '') {
                $start_date = new DateTime($filter['start_date']);
                $end_date = new DateTime($filter['end_date']);
                $end_date->add(new DateInterval('PT23H59M59S'));
                if(isset($view) && $view == 'pipeline'){
                    if($start_date != '2016-01-01'){
                        $parsed_filter_options .= ' AND created_at BETWEEN "' . $start_date->format('Y-m-d H:i:s') . '" AND "' . $end_date->format('Y-m-d H:i:s') . '"';
                    }
                }else{
                    $parsed_filter_options .= ' AND created_at BETWEEN "' . $start_date->format('Y-m-d H:i:s') . '" AND "' . $end_date->format('Y-m-d H:i:s') . '"';
                }
            }
            if (isset($filter['custom']) && !empty($filter['custom'])) {
                foreach ($filter['custom'] as $key => $value) {
                    switch ($key) {
                        case 'userid':
                            if ($this->aauth->is_member('Admin')) {

                                if ( $value != 'All') {
                                    $is_filter_sales_user = TRUE;
                                    $parsed_filter_options .= " AND repId IN(" . $value . ")";
                                }
                            }
                            break;
                        default:
                            if ($value != '') {
                                $parsed_filter_options .= ' AND ' . $key . '=' . $value;
                            }
                            break;
                    }
                }
            }
        }

        if ($this->components->is_team_leader()) {
            $make_it_cheaper_profile = 23;
            if ($this->components->is_lead_allocation_team_leader() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND repId IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                //For Make it cheaper profile
                if (!$is_source_make_it_cheaper) {
                    $parsed_filter_options .= " AND repId IN(" . $user_id .")";
                }

            }
        } else if ($this->components->is_sales_rep()) {
            if ($this->components->is_lead_allocation_rep() && $is_filter_la_user == FALSE) {
                $user_id = $this->aauth->get_user_id();
                $parsed_filter_options .= " AND leads.user_id IN(" . $user_id . ")";
            } else if ($is_filter_sales_user == FALSE) {
                $parsed_filter_options .= " AND repId IN(" . $user_id . ")";
            }
        }

        return $parsed_filter_options;
    }

    public function fetch_status_data(){
        $data = array();
        //Pagination Params
        $limit = $this->input->get('limit');
        $limit = (isset($limit) && $limit != '') ? (int) $limit : 300;
        $page = $this->input->get('page');
        $page = (isset($page) && $page != '') ? $page : 1;
        $offset = ($page - 1) * $limit;
        //Pagination End

        $status_id = $this->input->get('status_id');
        $status_id = (isset($status_id) && $status_id != '') ? $status_id : FALSE;
        $filters = '';
        $filters = $this->handle_tender_data_filters();
        $is_filter = ($filters != '') ? TRUE : FALSE;

        $view = $this->input->get('view');
        $view = (isset($view) && $view != '') ? $view : 'list';
        $user_id = $this->aauth->get_user_id();
        $custom_filter_params = $this->input->get('custom');
        $user_cond = "";

        //Having Filter
        $having = FALSE;

        if($page == 1){
            //Unset Lead Total Count
            $this->session->unset_userdata('kg_tender_total_count');
            $this->session->unset_userdata('kg_map_lead_total_count');
        }
        $view = 'pipeline';
        if($this->input->get('view') == 'list'){
            $view = 'list';
        }
        $data['tender_status'] = array();
        foreach(TENDER_STATUS_TYPES as $k=>$v) {
            array_push($data['tender_status'], array('id' => $k, 'name' => $v));
        }
        if ($view == 'pipeline') {
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);

            if (isset($keyword) && $keyword != '') {
                $search_by_project = 'tbl_tender.name';
                $filters .= (isset($keyword) && $keyword != '') ?
                    " AND  $search_by_project LIKE '%$keyword%'" . $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }

            $tender_total_count = $this->session->userdata('kg_tender_total_count');
            $total =  (!empty($tender_total_count))?$tender_total_count:$this->tender->get_tender_data_by_status_count_for_pipeline($status_id,$filters);
            $this->session->set_userdata('kg_tender_total_count',$total);
            if (isset($page) && $page == 1) {
                $tender_status = $data['tender_status'];
                foreach ($tender_status as $key => $value) {
                    $tender_status[$key]['count'] = $this->tender->get_tender_data_by_status_count_for_pipeline($value['id'],$filters);
                    $tender_data_by_status[$key] = $this->tender->get_tender_data_by_status_for_pipeline($value['id'], $filters, false, $offset, $limit, $is_filter,$user_id);
                }
                $data['tender_status'] = $tender_status;
                $data['tender_data_by_status'] = $tender_data_by_status;
            } else {
                $tender_data_by_status = $this->tender->get_tender_data_by_status_for_pipeline(FALSE, $filters, $having, $offset, $limit, $is_filter,$user_id);
                $data['tender_data_by_status'] = $tender_data_by_status;
            }

            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        }
        else if ($view == 'list') {
            //Keyword Filter
            $keyword = $this->input->get('keyword');
            $keyword = str_replace("@", " ", $keyword);
            $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keyword);
            if (isset($keyword) && $keyword != '') {
                $search_by_project = 'name';
                $filters = (isset($keyword) && $keyword != '') ?
                    " AND (name LIKE '%$keyword%' OR email LIKE '%$keyword%' OR contactPerson LIKE '%$keyword%') ". $user_cond : '';
                $offset = $limit = FALSE;
                $is_filter = TRUE;
            }

            $tender_total_count = $this->session->userdata('kg_tender_total_count');
            $total = (isset($tender_total_count) && $tender_total_count != '') ? $tender_total_count : $this->tender->get_tender_data_by_status_count_for_pipeline(false, $filters);
            $this->session->set_userdata('kg_tender_total_count',$total);

            $data['tender_data'] = $this->tender->get_tender_data_by_status_for_pipeline(false, $filters, $having, $offset, $limit, $is_filter, $user_id);
            if ((count($data['tender_data']) < 100) && ($total < 100)) {
                //So if Total leads with first page is less than 100 than bring all the data without limit.
                $offset = $limit = FALSE;
                $data['tender_data'] = $this->tender->get_tender_data_by_status_for_pipeline(false, $filters, $having, $offset, $limit, $is_filter, $user_id);
            }

            if ($limit != FALSE) {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = $limit;
                $data['meta']['total_pages'] = ceil($total / $limit);
            } else {
                $data['meta']['total'] = $total;
                $data['meta']['curr_page'] = $page;
                $data['meta']['per_page'] = 300;
                $data['meta']['total_pages'] = $total;
            }
        }
        $data['success'] = TRUE;
        echo json_encode($data,true); exit;
    }

    public function bookingForm($uuid = false){
        if (!$this->aauth->is_member('Admin') && !($this->components->is_sales_rep() && $this->session->userdata('profileId') == 2)) {
            redirect('admin/dashboard');
        } else {
            $users = $this->user->get_users(FALSE, FALSE, FALSE);
        }

        if($uuid == false){
            $data['users'] = $users;
            if(empty($this->input->get('centerId')) || empty($this->input->get('siteId')) || empty($this->input->get('tenderId'))) {
                redirect('admin/dashboard');
            }
            $data['centerId'] = $this->input->get('centerId');
            $data['siteId'] = $this->input->get('siteId');
            $data['tenderId'] = $this->input->get('tenderId');
            $data['uuid'] = '';
        } else {
            $is_booking_exists = $this->common->fetch_row('tbl_tender_booking_form', '*', array('uuid' => $uuid));
            if(empty($is_booking_exists)){
                redirect('admin/dashboard');
            }
            $data['centerId'] = $is_booking_exists['costCentreId'];
            $data['siteId'] = $is_booking_exists['siteId'];
            $data['tenderId'] = $is_booking_exists['tenderId'];
            $data['uuid'] = $is_booking_exists['uuid'];
        }

        $data['tenderStatus'] = TENDER_STATUS_TYPES;

        $data['sites'] = $this->common->fetch_where('tbl_tender_sites','*',"id={$data['siteId']}");
        $this->common->db = $this->load->database('jobcrm_db', TRUE);
        $inverter =   $this->common->fetch_where('tbl_warehouse_products','*' ,'type_id=1 AND availability=1');
        $panel =   $this->common->fetch_where('tbl_warehouse_products','*' ,'type_id=2 AND availability=1');
        $battery =   $this->common->fetch_where('tbl_warehouse_products','*' ,'type_id=3 AND availability=1');
        $data['inverter'] = $inverter;
        $data['panel'] = $panel;
        $data['battery'] = $battery;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('tender/bookingform/tender-bookingform');
        $this->load->view('partials/footer');

    }


    public function save_booking_form($uuid = FALSE) {
        $data = array();
        if (!empty($this->input->post())) {
            $is_booking_exists = $this->common->fetch_row('tbl_tender_booking_form', '*', array('uuid' => $this->input->post('uuid')));
            $form_data = array();
            $form_data['tenderId'] = $this->input->post('tenderId');
            $form_data['uuid'] = $this->input->post('uuid');
            $form_data['siteId'] = $this->input->post('siteId');
            $form_data['costCentreId'] = $this->input->post('cost_centre_id');
            $form_data['business_details'] = json_encode($this->input->post('business_details'));
            $form_data['contacts'] = json_encode($this->input->post('contacts'));
            $form_data['contractInformation'] = json_encode($this->input->post('contractInformation'));
            $form_data['electricity_bill'] = json_encode($this->input->post('electricity_bill'));
            $form_data['product'] = json_encode($this->input->post('product'));
            $form_data['notes'] = json_encode($this->input->post('notes'));
            $form_data['booking_form_image'] = json_encode($this->input->post('booking_form_image'));
            $form_data['booking_form'] = json_encode($this->input->post('booking_form'));
            if (empty($is_booking_exists)) {
                $form_data['user_id'] = $this->aauth->get_user_id();
                $this->common->insert_data('tbl_tender_booking_form', $form_data);
                $stat = $this->common->insert_id();
            } else {
                $booking_id = $is_booking_exists['id'];
               // $update_data = $form_data;
                $stat = $this->common->update_data('tbl_tender_booking_form', array('id' => $booking_id), $form_data);
            }
            if ($stat) {
                $data['status'] = 'Booking Form Data Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to Update';
                $data['success'] = FALSE;
            }
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function fetch_booking_form_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $booking_data = $this->common->fetch_row('tbl_tender_booking_form', '*', array('uuid' => $this->input->get('uuid')));
            $data['booking_data'] = $booking_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

   public function bookingFormDownload($uuid){
        if($uuid){
            $booking_data = $this->common->fetch_row('tbl_tender_booking_form', '*', array('uuid' => $uuid));
            foreach($booking_data as $k=>$eachData){
                if(in_array($k,array('business_details','more','contacts','contractInformation','electricity_bill','product','booking_form_image','booking_form','notes'))){
                    $booking_data[$k]=json_decode($eachData,true);
                    $$k=$eachData;
                }
            }
            $html = $this->load->view('tender/bookingform/tender-bookingform-pdf',$booking_data,true);
            $filename = md5(time());
            $constructor = array(
                'mode' => '',
                'format' => 'A4',
                'default_font_size' => 0,
                'default_font' => '',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 25,
                'margin_bottom' => 15,
                'margin_header' => 0,
                'margin_footer' => 0,
                'orientation' => 'P',
            );
            $mpdf = new Mpdf\Mpdf($constructor);
            $header = "<img src='".site_url('assets/tender_booking_form/header_02.png')."' />";
            $mpdf->SetHTMLHeader($header);
            $mpdf->SetHTMLFooter("<img src='".site_url('assets/tender_booking_form/bottom.png')."' />");
           // $mpdf->_setAutoHeaderHeight($header);
            $mpdf->WriteHTML($html);
            $mpdf->Output(FCPATH.'assets/uploads/tender_booking_form_files/' . $filename . '.pdf','F');
            $filesize = $this->getFileSize(FCPATH.'assets/uploads/tender_booking_form_files/' . $filename . '.pdf');
            $filename = $filename . '.pdf';
            //Get File
            //$b64_file = base64_encode(file_get_contents(__DIR__.'/../../assets/uploads/tender_booking_form_files/' . $filename));
            $size_name = $booking_data['product']['systemSize']['product_name'].'kW';
            if (isset($business_details->company_name) && $business_details->company_name != '') {
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.' Solar_Upgrade_Booking_Form_Agreement_' . $business_details->company_name . '-' . date('m-d-Y_his') . '.pdf';
            } else {
                $filename_new = 'KUGA_ELECTRICAL_'.$size_name.'_Tender_Booking_Form_Agreement_' . date('m-d-Y_his') . '.pdf';
            }
            $directory = FCPATH.'assets/uploads/tender_booking_form_files/';
            rename( $directory. $filename,$directory.$filename_new);
            $this->common->update_data('tbl_tender_booking_form', array('uuid' => $uuid), array('pdf_file' => $filename_new,'pdf_file_size' => $filesize));
            $data['status'] = 'PDF Generated';
            $data['success'] = TRUE;
           $jobId= $this->create_tenderJob_in_job_crm('TENDER',$booking_data);
            if($jobId > 0 ){
                $this->common->update_data('tbl_tender_booking_form', array('uuid' => $uuid), array('kuga_job_id' => $jobId));
                $data['status'] = 'PDF Generated';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Invalid Request';
                $data['success'] = FALSE;
            }
            //$data['booking_data'] = $booking_data;
        }
        else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
       echo json_encode($data);
       die;

   }
    function getFileSize($path) {
        $size = filesize($path);
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }
    public function GetBookingForms(){
        $data['success'] = false;
        $data['html'] = '';
        if($this->input->post()){
            $tenderId = (int)$this->input->post('tenderId');
            if($tenderId >0){
                $data['success'] = true;
                $tenderBookingForms = $this->db->query("SELECT TBF.*,TS.name,TS.address 
                                                            FROM tbl_tender_booking_form as TBF 
                                                                JOIN tbl_tender_sites as TS ON TS.id = TBF.siteId 
                                                            WHERE TBF.tenderId=? ORDER BY created_at DESC",$tenderId);

                $data['html']=$this->load->view('tender/bookingform/BookingFormsList',array('tenderBookingForms'=>$tenderBookingForms->result_array()),true);
            }
        }
        echo json_encode($data); exit;
    }


    private function create_tenderJob_in_job_crm($job_type,$data){
        $booking_form_kuga_job_id = isset($data['kuga_job_id']) ? $data['kuga_job_id'] : NULL;
        $booking_form_uuid = isset($data['uuid']) ? $data['uuid'] : NULL;
        $business_details = isset($data['business_details']) ? $data['business_details'] : NULL;
        $contacts = isset($data['contacts']) ? $data['contacts'] : NULL;
        $booking_form_data = isset($data['booking_form']) ? $data['booking_form'] : NULL;
        $booking_form_image_data = isset($data['booking_form_image']) ? $data['booking_form_image'] : NULL;
        $booking_form_image = array();
       foreach($booking_form_image_data as $k=>$eachBooking_form_image){
           if($k!='more'){
               if($eachBooking_form_image!='') {
                   $booking_form_image[$k] = $eachBooking_form_image;
               }
           } else {
               foreach($eachBooking_form_image as $m=>$eachimage){
                   if(!empty($eachimage['image1'])) {
                       $booking_form_image['additional_image_'.$m.'_image1'] = $eachimage['image1'];
                   }
                   if(!empty($eachimage['image2'])) {
                       $booking_form_image['additional_image_'.$m.'_image2'] = $eachimage['image2'];
                   }
               }
           }
       }
        $filename = isset($data['pdf_file']) ? $data['pdf_file'] : '';
        $cost_centre_id =  (isset($data['costCentreId']) && ($data['costCentreId'] != NULL && $data['costCentreId'] != 0)) ? $data['costCentreId'] : 1;
        $product_data =  (isset($data['product'])) ? $data['product'] : NULL;
        $booking_form_site_id = isset($data['siteId']) ? $data['siteId'] : NULL;
        $electricity_bill = isset($data['electricity_bill']) ? $data['electricity_bill'] : NULL;
        $crm_cust_id = $this->job->insert_or_update_customer($data['tenderId'],$data['id'],$data);
        if($crm_cust_id){
            $cust_loc_id = $this->job->insert_or_update_customer_location($data['tenderId'],$crm_cust_id,$booking_form_site_id);
            $job_data = array();
            $job_data['uuid'] = $this->components->uuid();
            $job_data['company_id'] = 1;
            $job_data['cust_id'] = $crm_cust_id;
            $job_data['booking_form_id'] = $data['id'];
            $job_data[ 'cost_centre_id'] = $cost_centre_id;
            $job_data['assigned_to'] = isset($data['user_id']) ? $data['user_id'] : '';
            $business_details['entity_name'] = $business_details['company_name'];
            unset($business_details['company_name']);
            $job_data['business_details'] = json_encode($business_details);
            $job_data['authorised_details'] = json_encode($contacts['projectManager']);
            $job_data['booking_form_data'] = json_encode($booking_form_data);
            $job_data['booking_form_image'] = json_encode($booking_form_image);
            $job_data['important_notes'] = $data['notes']['acRun'];

            $job_data['job_type'] = $job_type;
            if($data['pdf_file'] != ''){
                $job_data['booking_form_file'] = site_url('assets/uploads/tender_booking_form_files/' . $data['pdf_file']);
            }

            $job_id = $this->job->insert_or_update_job($data['id'],$job_data);
            unset($contacts['projectManager']);
            if($crm_cust_id){
                $this->job->insert_booking_form_additional_contacts($crm_cust_id, $contacts);
            }

            if($job_id != ''){
                $this->job->insert_booking_form_activity_note($job_id, $job_data);
            }

           // $this->send_booking_form_mail_to_john($job_id,$job_data,$filename,$nfilename,$safilename);

            return $job_id;
        }

        return null;
    }

    private function send_booking_form_mail_to_john($job_id,$job_data,$bf_filename,$nfilename,$safilename){

        if($_SERVER['REMOTE_ADDR'] == '111.93.41.194'){
            return true;
        }

        $business_details = json_decode($job_data['business_details']);
        $authorised_details = json_decode($job_data['authorised_details']);

        $customer_payload = array();
        $customer_payload['CompanyName'] = $business_details->entity_name;
        $customer_payload['Phone'] = $authorised_details->contact_no;
        $customer_payload['Email'] = $authorised_details->email;

        $user_id = $this->aauth->get_user_id();
        $sales_rep_data = $this->user->get_users(FALSE, $user_id, FALSE, FALSE);

        $communication_data = array();
        $communication_data['customer'] = $customer_payload;
        $communication_data['type'] = $job_data['job_type'];
        $communication_data['job_details'] = $this->job->get_job_details_from_job_crm($job_id);

        if($bf_filename != ''){
            $folder = ($job_data['job_type'] == 'LED') ?  'led_booking_form_files' : 'solar_booking_form_files';
            //$bf_filename = str_replace(' ', '%20', $bf_filename);
            $communication_data['attachment'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/'.$folder.'/' . $bf_filename . '.pdf';
        }
        if($nfilename != ''){
            //$nfilename = str_replace(' ', '%20', $nfilename);
            $communication_data['attachment1'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/nomination_form_files/' . $nfilename;
        }
        if($safilename != ''){
            //$safilename = str_replace(' ', '%20', $safilename);
            $communication_data['attachment2'] = $_SERVER["DOCUMENT_ROOT"] . '/assets/uploads/site_assessor_form_files/' . $safilename;
        }

        $communication_data['sales_rep'] = array();
        if (!empty($sales_rep_data)) {
            $communication_data['sales_rep']['name'] = $sales_rep_data[0]['full_name'];
            $communication_data['sales_rep']['email'] = $to_sales_rep = $sales_rep_data[0]['email'];
        }
        //$this->email_manager->send_booking_form_success_email_to_john($communication_data);
        $communication_data['cost_centre_id'] = isset($job_data['cost_centre_id']) ? $job_data['cost_centre_id'] : NULL;
        $communication_data['job_id'] = $job_id;
        $this->add_booking_form_mail_to_queue($communication_data,2);

    }

    private function create_job_custom_fields_in_job_crm($job_id,$data){
        foreach($data as $key => $value){
            $table_data = array();
            $table_data['job_id'] = $job_id;
            $table_data['custom_field_id'] = $key;
            $table_data['custom_field_value'] = $value;

            $cond = array('job_id' => $job_id,'custom_field_id' => $key);
            $stat = $this->job->insert_or_update('tbl_job_custom_field_data',$cond,$table_data);
        }
    }

    public function updateStatus(){
        $data['success'] = false;
        if($this->input->post()){
            if($this->input->post('statusId') && $this->input->post('tenderId')){
                $tenderData['status']=$this->input->post('statusId');
                $effectedRow = $this->common->update_data('tbl_tender', 'id='.(int)$this->input->post('tenderId'),$tenderData);
                if($effectedRow > 0){
                    $data['success'] = true;
                    $data['message'] = 'Tender status updated successfully.';
                }else{
                    $data['message'] = 'Nothing to update.';
                }
            }else{
                $data['message'] = 'Invalid Request.';
            }
        } else {
            $data['message'] = 'Invalid Request.';
        }
        echo json_encode($data); exit;
    }

    public function saveCostCat(){
        if (!empty($this->input->post())) {
            $error = $tenderData = array();
            $this->form_validation->set_rules('ctName', 'Cost Type', 'required');
            $this->form_validation->set_rules('siteId', 'Invalid Request', 'required');
            $id = 0;
            $costTypes = array();
            if ($this->form_validation->run() != FALSE){

                $data['name'] = $this->input->post('ctName');
                $data['siteId'] = $siteId = (int)$this->input->post('siteId');
                $this->common->insert_data('tbl_site_cost_types',$data);
                $id = $this->common->insert_id();
                $siteOptionsResult = $this->db->query("select optionId from tbl_tender_site_type  WHERE siteId='{$siteId}' GROUP BY optionId ORDER BY optionId DESC LIMIT 0,1");
                $siteOptions = $siteOptionsResult->result_array();
                $currentOption = $siteOptions[0]['optionId'];
                $i=1;
                while($i<=$currentOption){
                        $siteCostType = array('siteId' => $siteId, 'optionId' => $i, 'typeId' => $id);
                        $this->common->insert_data('tbl_tender_sites_systemcost', $siteCostType);

                    $i++;
                }
                $dataSave = true;
            } else {
                $dataSave = false;
                $error = $this->form_validation->error_array();
            }
            echo json_encode(array('success'=>$dataSave,'id'=>$id,'errors'=>$error,));
            exit;
        }
    }



}
?>

var lead_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? parseInt(options.userid) : '';
    this.user_group = (options.user_group && options.user_group != '') ? options.user_group : '';
    this.is_sales_rep = (options.is_sales_rep && options.is_sales_rep != '') ? options.is_sales_rep : '';
    this.is_lead_allocation_sales_rep = (options.is_lead_allocation_sales_rep && options.is_lead_allocation_sales_rep != '') ? options.is_lead_allocation_sales_rep : '';
    this.lead_stages = options.lead_stages;
    this.cust_id = '';
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.cust_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.cust_location_data = [];
    this.cust_contact_data = [];
    this.deal_stage = '';
    this.modal_return = '';

    if (Object.keys(self.lead_data).length > 0) {
        self.cust_id = self.lead_data.cust_id;
        
        $('#user_id').val(self.lead_data.user_id);
        $('#cust_id').val(self.lead_data.cust_id);
        $('#company_name').val(self.lead_data.customer_company_name);
        $('#dd_company_name').html(self.lead_data.customer_company_name);
        $('#company_cust_id').val(self.lead_data.cust_id);
        if(self.lead_data.first_name != '' && self.lead_data.first_name != null){
            $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        }
        $('#customer_phone').val(self.lead_data.customer_contact_no);
        if(self.lead_data.customer_email != '' && self.lead_data.customer_email != null){
            $('#customer_email').val(self.lead_data.customer_email);
        }
        if(self.lead_data.position != '' && self.lead_data.position != null){
            $('#customer_position').val(self.lead_data.customer_email);
        }
        $('#add_new_customer_btn').addClass('hidden');
        $('#edit_customer_btn').removeClass('hidden');
        //Disable Lead Source If it is Allocated lead not self generated
        if (self.lead_data.lead_source != '' && self.lead_data.lead_source != null) {
            $('#lead_source').val(self.lead_data.lead_source);
        }

        //Set Lead Type and Segment , Indsutry
        $('#lead_type').val(self.lead_data.lead_type);
        $('#lead_segment').val(self.lead_data.lead_segment);
        $('#lead_industry').val(self.lead_data.lead_industry);

        if (self.lead_data.user_id != self.userid) {
            $('#lead_source').attr('disabled', 'dsiabled');
        } else {
            //Remove first 2 options from Lead Source If it is self generated
            //$('#lead_source').find('option').get(1).remove();
            //$('#lead_source').find('option').get(1).remove();
        }

        if (self.lead_data.lead_stage != '' && self.lead_data.lead_stage != null) {
            self.deal_stage = self.lead_data.lead_stage;
            self.create_deal_stages();
        }

        $('#customer_phone').click(function () {
            if ($(this).val() != '') {
                $('#make_call').remove();
                $('body').append('<a href="tel:' + $(this).val() + '" id="make_call" style="display:none;">Call ' + $(this).val() + '</a>');
                setTimeout(function () {
                    document.getElementById('make_call').click();
                }, 500);
                setTimeout(function () {
                    document.getElementById('add_activity').click();
                    $('#activity_title').val('Outgoing Call');
                }, 2500);

            }
        });

        //Remove Hidden Class from Attachment Dropzone
        $('#lead_attachments').removeClass('hidden');

        self.fetch_customer_location();
        self.fetch_customer_contacts();
        self.fetch_proposal_total_details();
        //self.fetch_lead_quotes();
        //self.fetch_lead_booking_forms();
        //self.fetch_attachments();
        self.fetch_lead_form_data();
    } else {
        if (self.is_lead_allocation_sales_rep == true){
            $('#lead_source').val('Call Centre Lead');    
        }
    }

    //Handle Lead Data Save
    $('#user_id,#lead_source,#lead_type,#lead_industry,#lead_segment').change(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        self.save_lead_details();
    });

    //Handle Customer Modal
    $('#add_new_customer_btn').click(function () {
        var is_allowed = self.check_ownership_rules(false,'add_customer');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        $('#add_customer_modal').modal('show');
        $('#cust_id').val('');
    });

    $('#edit_customer_btn').click(function () {
        var is_allowed = self.check_ownership_rules(false,'add_customer');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var cust_data = self.cust_data;
        //Set Cust Data in Modal
        $('#company_cust_id').val(cust_data.cust_id);
        $('#contactMobilePhone').val(cust_data.customer_contact_no);
        $('#contactEmailId').val(cust_data.customer_email);
        $('#firstname').val(cust_data.first_name);
        $('#lastname').val(cust_data.last_name);
        $('#cust_title').val(cust_data.title);
        $('#bussiness_name').val(cust_data.customer_company_name);
        $('#position').val(cust_data.position);
        $('#add_customer_modal_title').html('Edit Customer');
        $('#add_customer_modal').modal('show');
    });

    //Handle Cusotmer Location Modal
    $('.add_new_customer_location_btn').click(function () {
        var data_return = $(this).attr('data-return');
        self.modal_return = (data_return) ? JSON.parse(data_return) : '';

        var is_allowed = self.check_ownership_rules(false,'add_site');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            $('#lead_allocation_appointment_modal').modal('hide');
            $('#site_id').val('');
            document.querySelector('#locationAdd').reset();
            $('#select2-locationstreetAddress-container').html('Search for Address');
            $('#add_customer_location_modal').modal('show');
            $('#add_customer_location_modal_title').html('Add New Customer Location');
            self.fetch_customer_and_contacts(null, '#site_cust_contact_id');
        } else {
            toastr["error"]('Oops, Please create customer first then add location.');
        }
    });

    $('#add_new_customer_location_btn_1').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            $('#booking_form_choose_customer_location_modal').modal('hide');
            $('#site_id').val('');
            document.querySelector('#locationAdd').reset();
            $('#select2-locationstreetAddress-container').html('Search for Address');
            $('#add_customer_location_modal').modal('show');
            $('#add_customer_location_modal_title').html('Add New Customer Location');
            self.fetch_customer_and_contacts(null, '#site_cust_contact_id');
        } else {
            toastr["error"]('Oops, Please create customer first then add location.');
        }
    });


    //Handle Cusotmer Contact Modal
    $('.add_new_customer_contact_btn').click(function () {
        var is_allowed = self.check_ownership_rules(false,'add_customer');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            $('#lead_allocation_appointment_modal').modal('hide');
            document.getElementById('customerContactAdd').reset();
            $('#add_customer_contact_modal_title').html('Add New Customer Contact');
            $('#cust_contact_id').val('');
            $('#contact_cust_id').val(self.cust_data.cust_id);
            $('#add_customer_contact_modal').modal('show');
        } else {
            toastr["error"]('Oops, Please create customer first then add additional contacts.');
        }
    });

    $('#site_cust_contact_id').change(function () {
        var is_allowed = self.check_ownership_rules(false,'add_customer');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var customer = $(this).val();
        if (customer != 'Please Select Contact Name') {
            var customer_data = JSON.parse($('#site_cust_contact_id option:selected').attr('data-item'));
            $('#locationContactPerson').val(customer_data.first_name + ' ' + customer_data.last_name);
            $('#locationContactEmail').val(customer_data.customer_email);
            $('#locationContactPhone').val(customer_data.customer_contact_no);
        } else {
            $('#locationContactPerson').val('');
            $('#locationContactEmail').val('');
            $('#locationContactPhone').val('');
        }
    });

    //Quick Quote
    $('#add_quick_quote').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            var url = base_url + 'admin/lead/add_quick_quote?deal_ref=' + self.lead_data.uuid;
            window.open(url, '_blank');
        } else {
            toastr["error"]('Oops, Please create customer first then proceed to create a quote.');
        }
    });

    //Meter data
    $('#add_meter_data').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            var url = base_url + 'admin/lead/add_meter_data?deal_ref=' + self.lead_data.uuid;
            window.open(url, '_blank');
        } else {
            toastr["error"]('Oops, Please create customer first then proceed to request a meter data.');
        }
    });

    
    //LED Booking Form
    $('#led_booking_form_btn').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            var url = base_url + 'admin/booking_form/add_led_booking_form?deal_ref=' + self.lead_data.uuid;
            //window.open(url, '_blank');
            $('.customer_booking_form_location_create_btn').attr('data-url', url);
            $('#booking_form_choose_customer_location_modal').modal('show');
        } else {
            toastr["error"]('Oops, Please create customer first then proceed to create a booking form.');
        }
    });

    //SOLAR Booking Form
    $('#solar_booking_form_btn').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        if (Object.keys(self.cust_data).length > 0) {
            var url = base_url + 'admin/booking_form/manage_solar_booking_form?deal_ref=' + self.lead_data.uuid;
            //window.open(url, '_blank');
            $('.customer_booking_form_location_create_btn').attr('data-url', url);
            $('#booking_form_choose_customer_location_modal').modal('show');
        } else {
            toastr["error"]('Oops, Please create customer first then proceed to create a booking form.');
        }
    });

    //Activity Handler
    $('#add_activity').click(function () {
        //Make Activity Attachment Blank
        document.getElementById('activity_attachments').innerHTML = '';
        $('.dz-preview').remove();
        $('.dropzone').removeClass('dz-started');

        var is_allowed = self.check_ownership_rules(false,'add_activity');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var cust_data;
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            if (Object.keys(self.cust_data).length > 0) {
                cust_data = self.cust_data;
            } else {
                toastr["error"]('Oops, Please create customer first then add activity.');
                return false;
            }
        }

        $('#scheduler').addClass('hidden');
        $('#attendees').addClass('hidden');

        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#mark_as_completed').addClass('hidden');
        $('.datepicker').hide();
        self.activity_action = "Add Activity";
        window.activity_manager_tool.activity_trigger = 1;

        $('#activity_modal_body_left').removeClass('col-md-6');
        $('#activity_modal_body_left').addClass('col-md-12');

        $('#activityLocationPostCode').val(cust_data.postcode);
        $('#select2-activityAddress-container').html(cust_data.address);
        $('#activityLocationAddressVal').val(cust_data.address);
        $('#activityLocationState').val(cust_data.state_id).trigger('change');
        $('#activity_modal').modal('show');
    });

    $('#schedule_activity').click(function () {
        //Make Activity Attachment Blank
        document.getElementById('activity_attachments').innerHTML = '';
        $('.dz-preview').remove();
        $('.dropzone').removeClass('dz-started');

        var is_allowed = self.check_ownership_rules(false,'schedule_activity');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var cust_data;
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            if (Object.keys(self.cust_data).length > 0) {
                cust_data = self.cust_data;
            } else {
                toastr["error"]('Oops, Please create customer first then schedule activity.');
                return false;
            }
        }

        $('#scheduler').removeClass('hidden');
        $('#attendees').removeClass('hidden');

        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#mark_as_completed').addClass('hidden');
        $('.datepicker').show();
        self.activity_action = "Scheduled Activity";
        window.activity_manager_tool.activity_trigger = 2;

        $('#activity_modal_body_left').addClass('col-md-6');
        $('#activity_modal_body_left').removeClass('col-md-12');

        $('#activityLocationPostCode').val(cust_data.postcode);
        $('#select2-activityAddress-container').html(cust_data.address);
        $('#activityLocationAddressVal').val(cust_data.address);
        $('#activityLocationState').val(cust_data.state_id).trigger('change');
        $('#activity_modal').modal('show');
    });

    $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    $('.datepicker').datepicker({
        autoclose: true,
    }).on('changeDate', function () {
        $('.datepicker').hide();
    });

    //Attach Functions
    if (options.lead_stages != '') {
        document.getElementById('deal_stages').appendChild(createPlaceHolder(false));
        self.create_deal_stages();
    }

    //Set Appointment Lead Allocation
    $('#set_appointment_btn').click(function () {
        var cust_data = {};
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            if (Object.keys(self.cust_data).length > 0) {
                cust_data = self.cust_data;
            } 
        }
        
        var customer_company_name = cust_data.customer_company_name;
        var lead_id = self.lead_data.id;
        if (!cust_data.hasOwnProperty('customer_company_name')) {
            customer_company_name = $('#dd_company_name').html();
            lead_id = self.lead_id;
        }
        
        document.querySelector('#lead_allocation_lead_id').value = lead_id;
        var lead_date = (cust_data.created_at != null && cust_data.created_at != '') ? moment(cust_data.la_date_of_lead).format('LLL') : moment().format('LLL');
        document.querySelector('#la_date_of_lead').innerHTML = lead_date;
        document.querySelector('#la_business_name').innerHTML = customer_company_name;
        $('#lead_allocation_appointment_modal').modal('show');
        self.fetch_customer_and_contacts(null, '#la_cust_contact_id');
        var location_data = self.cust_location_data;
        var select = document.querySelector('#la_site_id');
        select.innerHTML = '';
        var option = document.createElement('option');
        option.innerHTML = 'Please Select Site';
        select.appendChild(option);
        for (var key in location_data) {
            var option = document.createElement('option');
            option.value = location_data[key].id;
            var name = (location_data[key].site_name == '') ? location_data[key].address : location_data[key].site_name + ' ('+ location_data[key].address +')';
            option.innerHTML = name;
            option.setAttribute('data-item', JSON.stringify(location_data[key]));
            select.appendChild(option);
        }
        $('#la_site_id').val('Please Select Site');
    });


    //self.handle_dropzone();
    self.save_customer();
    self.save_customer_location();
    self.save_customer_contact();
    self.check_ownership_rules();
};

/** Common Functions **/

lead_manager.prototype.check_ownership_rules = function (msg,action) {
    var self = this;
    var lead_stage = (self.deal_stage == '') ? parseInt(self.lead_data.lead_stage) : parseInt(self.deal_stage);
    var is_sales_rep = parseInt(self.is_sales_rep);
    var lead_user = parseInt(self.lead_data.user_id);

    
    if (!self.lead_data.hasOwnProperty('id')) {
        return true;
    }
    
    //If User is Lead Allocation Member Stop all his privelages except mentioned
    if (self.lead_data.hasOwnProperty('id') && self.is_lead_allocation_sales_rep == true && 
        action != 'add_site' && action != 'add_customer' && action != 'schedule_activity' && action != 'add_activity' && action != 'modify_deal_stage') {
        return false;
}

if ((lead_stage == 1 || lead_stage == 2)) {
    return true;
} else if (!is_sales_rep) {
    if (self.userid == lead_user) {
        $('#user_id').attr('disabled', 'dsiabled');
    }
    return true;
} else if (self.userid == lead_user) {
    return true;
} else {
    if (msg) {
        toastr.remove();
        toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
    }
    $('#user_id').attr('disabled', 'dsiabled');
    return false;
}
}

lead_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};


lead_manager.prototype.fetch_attachments = function(){
    var self = this;

    var cust_data = {};
    if (Object.keys(self.lead_data).length > 0) {
        cust_data = self.lead_data;
    } else {
        if (Object.keys(self.cust_data).length > 0) {
            cust_data = self.cust_data;
        } 
    }


    var lead_id = self.lead_data.id;
    if (!cust_data.hasOwnProperty('customer_company_name')) {
        lead_id = self.lead_id;
    }

    $.ajax({
        type: 'GET',
        url: base_url + 'admin/uploader/fetch_lead_attachments',
        datatype: 'json',
        data: {lead_id: lead_id},
        beforeSend:function(){
            document.getElementById('lead_attachments_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var tbody = document.getElementById('lead_attachments_table_body');
            tbody.innerHTML = '';
            var stat = JSON.parse(stat);
            if(stat.attachments.length > 0){
                for(var i=0; i < stat.attachments.length;i++){
                    var tr =  document.createElement('tr');
                    var td_file = document.createElement('td');
                    td_file.innerHTML = '<a href="'+ base_url + 'assets/uploads/lead_files/'+stat.attachments[i].lead_id+'/'+stat.attachments[i].file_name +'" target="_blank">'+stat.attachments[i].file_name + '</a>';
                    var td_date = document.createElement('td');
                    td_date.innerHTML = moment(stat.attachments[i].created_at).format('LLL');
                    var td_action = document.createElement('td');
                    var action_delete_btn = document.createElement('a');
                    action_delete_btn.className = "btn-danger btn-sm ml-2 attachment_delete_btn";
                    action_delete_btn.setAttribute('data-id', stat.attachments[i].id);
                    action_delete_btn.setAttribute('href', 'javascript:void(0);');
                    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';
                    td_action.appendChild(action_delete_btn);

                    tr.appendChild(td_file);
                    tr.appendChild(td_date);
                    tr.appendChild(td_action);
                    tbody.appendChild(tr);
                }
                self.delete_attachments();
            } else {
             tbody.innerHTML ='<tr><td colspan="3" class="text-center">No attachments found.</td></tr>';
         }
     },
     error: function (xhr, ajaxOptions, thrownError) {
     }
 });
}

lead_manager.prototype.delete_attachments = function(){
    var self = this;
    
    $('.attachment_delete_btn').click(function(){
        var is_allowed = self.check_ownership_rules(false);
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }

        $this = $(this);
        var id = $(this).attr('data-id');
        var data = {};
        data.id = id;

        $.ajax({
            type: 'POST',
            url: base_url + 'admin/uploader/delete_lead_attachments',
            datatype: 'json',
            data: data,
            beforeSend: function () {
                toastr["info"]('Deleting Attachment from the system....');
                $this.attr("disabled", "disabled");
            },
            success: function (stat) {
                var stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                } else {
                    toastr["error"](stat.status);
                    $this.removeAttr("disabled");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $this.removeAttr("disabled");
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

lead_manager.prototype.create_deal_stages = function () {
    var self = this;
    var proposal_stages = JSON.parse(self.lead_stages);

    var ul = document.createElement('ul');
    ul.className = "steps";
    for (var i = 0; i < proposal_stages.length; i++) {
        var li = document.createElement('li');
        if (self.deal_stage != '') {
            if (parseInt(proposal_stages[i].id) < parseInt(self.deal_stage)) {
                li.className = "done";
            } else if (parseInt(proposal_stages[i].id) == parseInt(self.deal_stage)) {
                li.className = "active";
                self.deal_stage_name = proposal_stages[i].stage_name;
            } else {
                li.className = "undone";
            }
        } else {
            if (i == 0) {
                li.className = "active";
            } else {
                li.className = "undone";
            }
        }
        li.innerHTML = '<a href="javascript:void(0);" class="deal_stage" data-id="' + proposal_stages[i].id + '" data-name="' + proposal_stages[i].stage_name + '">' + proposal_stages[i].stage_name + '</a>';
        ul.appendChild(li);
    }
    if (self.deal_stage == '') {
        self.deal_stage = proposal_stages[0].id;
        self.deal_stage_name = proposal_stages[0].stage_name;
    }

    document.getElementById('deal_stages').innerHTML = '';
    document.getElementById('deal_stages').appendChild(ul);
    self.change_deal_stage();
};

lead_manager.prototype.change_deal_stage = function () {
    var self = this;
    $('.deal_stage').click(function () {
        var is_allowed = self.check_ownership_rules(false,'modify_deal_stage');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var id = $(this).attr('data-id');
        self.prev_deal_stage_name = self.deal_stage_name;
        self.deal_stage_name = $(this).attr('data-name');
        self.prev_deal_stage = self.deal_stage;
        self.deal_stage = id;
        self.create_deal_stages();
        self.save_lead_details();
    });
};

lead_manager.prototype.fetch_proposal_total_details = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_proposal_totals',
        datatype: 'json',
        data: {deal_ref: self.lead_data.uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                for (var key in stat.proposal_totals) {
                    document.getElementById(key).value = stat.proposal_totals[key];
                }
                for (var key in stat.solar_proposal_totals) {
                    document.getElementById(key).value = stat.solar_proposal_totals[key];
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.create_lead_quotes_table_row = function (data) {
    var self = this;
    var tr = document.createElement('tr');

    var td_quote_date = document.createElement('td');
    td_quote_date.innerHTML = moment(data.created_at).format('LLL');

    var td_quote_address = document.createElement('td');
    td_quote_address.innerHTML = data.customer_address;


    var td_quote_no = document.createElement('td');
    td_quote_no.innerHTML = data.quote_no;

    var td_quote_total = document.createElement('td');
    td_quote_total.innerHTML = '$' + data.total_incGST;
    var td_action = document.createElement('td');



    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm";
    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        action_edit_btn.setAttribute('href', base_url + 'admin/lead/edit_quick_quote/' + data.uuid);
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }
    action_edit_btn.setAttribute('target', '__blank');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Manage';

    td_action.appendChild(action_edit_btn);

    tr.appendChild(td_quote_date);
    tr.appendChild(td_quote_address);
    tr.appendChild(td_quote_no);
    tr.appendChild(td_quote_total);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.fetch_lead_quotes = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_quotes',
        datatype: 'json',
        data: {lead_id: self.lead_data.id},
        beforeSend: function () {
            //$('#customer_location_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('lead_quote_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('lead_quote_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#lead_quote_table_body').html('');
                if (stat.quote_data.length > 0) {
                    for (var i = 0; i < stat.quote_data.length; i++) {
                        var row = self.create_lead_quotes_table_row(stat.quote_data[i]);
                        var tbody = document.querySelector('#lead_quote_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    $('#lead_quote_table_body').html('<tr><td colspan="5" class="text-center">No quotes data found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_quote_table_body').html('<tr><td colspan="3" class="text-center">No quotes data found.</td></tr>');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

/** Customer Save and Lead Save Related Functions **/

lead_manager.prototype.validate_lead_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('#locationPostCode').val();
    var locationState = $('#locationState').val();
    var locationstreetAddress = $('#locationstreetAddressVal').val();
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    var contactMobilePhone = $('#contactMobilePhone').val();
    var bussiness_name = $('#bussiness_name').val();
    var customer_email = $('#contactEmailId').val();

    //Remove invalid
    $('#locationPostCode').removeClass('is-invalid');
    $('#locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');
    $('#contactMobilePhone').removeClass('is-invalid');
    $('#firstname').removeClass('is-invalid');
    $('#bussiness_name').removeClass('is-invalid');
    $('#businessId').removeClass('is-invalid');
    $('#contactEmailId').removeClass('is-invalid');

    /**
     if (locationPostCode == '') {
     flag = false;
     $('#locationPostCode').addClass('is-invalid');
     }
     if (locationState == '') {
     flag = false;
     $('#locationState').addClass('is-invalid');
     }
     if (locationstreetAddress == '') {
     flag = false;
     $('.select2-container').addClass('form-control is-invalid');
     }
     */
     if (bussiness_name == '') {
        flag = false;
        $('#bussiness_name').addClass('is-invalid');
    }
    if (contactMobilePhone == '') {
        flag = false;
        $('#contactMobilePhone').addClass('is-invalid');
    }
    if (firstname == '') {
        //flag = false;
        //$('#firstname').addClass('is-invalid');
    }

    if (customer_email == '') {
        //flag = false;
        //$('#contactEmailId').addClass('is-invalid');
    }

    return flag;
};

lead_manager.prototype.save_customer = function () {
    var self = this;
    $(document).on('click', '#save_customer', function () {
        toastr.remove();
        var cust_id = document.getElementById('company_cust_id').value;
        var flag = self.validate_lead_data();
        if (!flag) {
            toastr.clear();
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var customerAddForm = $('#customerAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer',
                datatype: 'json',
                data: customerAddForm + '&action=add',
                beforeSend: function () {
                    if (cust_id != '' && cust_id != null && cust_id != undefined) {
                        toastr["info"]('Updating Customer please wait....');
                    } else {
                        toastr["info"]('Creating Customer into the system....');
                    }
                    $('#customer_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer").attr("disabled", "disabled");
                },
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        //Populate Data from customer Modal to Front View
                        var new_company_name = $('#bussiness_name').val();
                        $('#company_name').val(new_company_name);
                        $('#company_name').removeAttr('data-item');
                        //$('#company_name').attr('data-item', JSON.stringify(stat.location));
                        $('#dd_company_name').html(new_company_name);
                        $('#cust_id').val(stat.customer_id);
                        $('#customer_name').val($('#firstname').val() + ' ' + $('#lastname').val());
                        $('#customer_position').val($('#position').val());
                        $('#customer_phone').val($('#contactMobilePhone').val());
                        $('#customer_email').val($('#contactEmailId').val());

                        self.cust_data.cust_id = stat.customer_id;
                        self.cust_data.customer_contact_no = $('#contactMobilePhone').val();
                        self.cust_data.customer_email = $('#contactEmailId').val();
                        self.cust_data.first_name = $('#firstname').val();
                        self.cust_data.last_name = $('#lastname').val();
                        self.cust_data.title = $('#cust_title').val();
                        self.cust_data.customer_company_name = new_company_name;
                        self.cust_data.position = $('#position').val();
                        self.cust_data.postcode = $('#locationPostCode').val();
                        self.cust_data.address = $('#locationstreetAddressVal').val();

                        //Populate some data in lead data obj
                        self.lead_data.cust_id = stat.customer_id;
                        self.lead_data.uuid = self.create_uuid();

                        //Check if it is Add Customer Or Edit Customer
                        if (cust_id != '' && cust_id != null && cust_id != undefined) {
                            $('#add_customer_modal').modal('hide');
                            $("#save_customer").removeAttr("disabled");
                            $('#customer_loader').html('');
                        } else {
                            //Save Lead Details
                            setTimeout(function () {
                                self.save_lead_details();
                            }, 1500);
                        }
                        $('#add_new_customer_btn').addClass('hidden');
                        $('#edit_customer_btn').removeClass('hidden');

                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer").removeAttr("disabled");
                        $('#customer_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer").removeAttr("disabled");
                    $('#customer_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
}
});
};

lead_manager.prototype.save_lead_details = function () {
    var self = this;

    if (self.lead_data.cust_id == '' || self.lead_data.cust_id == undefined) {
        toastr["error"]('Customer not Exists in the system');
        return false;
    }

    if (self.lead_data.uuid == '') {
        var uuid = self.create_uuid();
        self.lead_data.uuid = uuid;
    }

    var lead_to_user = document.getElementById('user_id').value;
    var lead_source = document.getElementById('lead_source').value;
    var lead_type = document.getElementById('lead_type').value;
    var lead_segment = document.getElementById('lead_segment').value;
    var lead_industry = document.getElementById('lead_industry').value;

    var formData = 'lead[uuid]=' + self.lead_data.uuid +
    '&lead[user_id]=' + self.userid +
    '&lead[cust_id]=' + self.lead_data.cust_id +
    '&lead[lead_source]=' + lead_source +
    '&lead[lead_stage]=' + self.deal_stage +
    '&lead[lead_type]=' + lead_type +
    '&lead[lead_segment]=' + lead_segment +
    '&lead[lead_industry]=' + lead_industry +
    '&lead_to_user=' + lead_to_user;

    $.ajax({
        url: base_url + 'admin/lead/save_lead_details',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            //toastr["info"]('Creating Lead into the system....');
            $("#save_customer").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                self.lead_id = response.id;
                var lead_data = {};
                lead_data.id = self.lead_id;
                if (window.hasOwnProperty('lead_allocation_manager')) {
                    window.lead_allocation_manager.lead_data.uuid = self.lead_data.uuid;
                }
                if (document.querySelector('#lead_allocation_lead_id')) {
                    document.querySelector('#lead_allocation_lead_id').value = self.lead_id;
                }
                //console.log(lead_data);
                window.activity_manager_tool.lead_data = lead_data;
                //console.log(window.activity_manager_tool.lead_data);

                //Add Deal ref in url
                window.history.pushState(self.lead_data, '', '?deal_ref=' + self.lead_data.uuid);
                $('#add_customer_modal').modal('hide');
                $("#save_customer").removeAttr("disabled");
                $('#customer_loader').html('');
            } else {
                toastr["error"](response.status);
                $("#save_customer").removeAttr("disabled");
                $('#customer_loader').html('');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


/** Customer Site Related Functions **/

lead_manager.prototype.create_customer_locations_table_row = function (data) {
    var self = this;
    var is_allowed = self.check_ownership_rules();

    var tr = document.createElement('tr');
    var td_site_name = document.createElement('td');
    td_site_name.innerHTML = data.site_name;
    var td_address = document.createElement('td');
    td_address.innerHTML = data.address;
    //var td_state = document.createElement('td');
    //td_state.innerHTML = data.state_name;
    //var td_postcode = document.createElement('td');
    //td_postcode.innerHTML = data.postcode;
    var td_site_contact = document.createElement('td');
    td_site_contact.innerHTML = data.site_contact_no;
    var td_led_proposal = document.createElement('td');
    var td_solar_proposal = document.createElement('td');
    var td_action = document.createElement('td');

    var led_proposal_add_btn = document.createElement('a');
    led_proposal_add_btn.className = "btn-primary btn-sm";
    var lead_proposal_url = base_url + 'admin/proposal/add?deal_ref=' + self.lead_data.uuid + '&type_ref=led' + '&site_ref=' + data.id;
    if (is_allowed) {
        led_proposal_add_btn.setAttribute('href', lead_proposal_url);
    } else {
        led_proposal_add_btn.setAttribute('href', 'javascript:void(0)');
        led_proposal_add_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }
    led_proposal_add_btn.innerHTML = '<i class="fa fa-plus"></i> Add';

    var led_proposal_edit_btn = document.createElement('a');
    led_proposal_edit_btn.className = "btn-primary btn-sm ml-2";
    led_proposal_edit_btn.setAttribute('href', '#');
    led_proposal_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    td_led_proposal.appendChild(led_proposal_add_btn);
    //td_led_proposal.appendChild(led_proposal_edit_btn);

    var solar_proposal_add_btn = document.createElement('a');
    solar_proposal_add_btn.className = "btn-success btn-sm";
    var solar_proposal_url = base_url + 'admin/proposal/add?deal_ref=' + self.lead_data.uuid + '&type_ref=solar' + '&site_ref=' + data.id;
    if (is_allowed) {
        solar_proposal_add_btn.setAttribute('href', solar_proposal_url);
    } else {
        solar_proposal_add_btn.setAttribute('href', 'javascript:void(0)');
        solar_proposal_add_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    solar_proposal_add_btn.innerHTML = '<i class="fa fa-plus"></i> Add';

    var solar_proposal_edit_btn = document.createElement('a');
    solar_proposal_edit_btn.className = "btn-success btn-sm ml-2";
    solar_proposal_edit_btn.setAttribute('href', '#');
    solar_proposal_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    td_solar_proposal.appendChild(solar_proposal_add_btn);
    //td_solar_proposal.appendChild(solar_proposal_edit_btn);

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm customer_location_edit_btn";
    action_edit_btn.setAttribute('data-id', data.id);
    action_edit_btn.setAttribute('href', 'javascript:void(0);');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    var action_delete_btn = document.createElement('a');
    action_delete_btn.className = "btn-danger btn-sm ml-2 customer_location_delete_btn";
    action_delete_btn.setAttribute('data-id', data.id);
    action_delete_btn.setAttribute('href', '#');
    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

    td_action.appendChild(action_edit_btn);
    td_action.appendChild(action_delete_btn);

    //tr.appendChild(td_site_name);
    tr.appendChild(td_address);
    //tr.appendChild(td_state);
    //tr.appendChild(td_postcode);
    //tr.appendChild(td_site_contact);
    tr.appendChild(td_led_proposal);
    tr.appendChild(td_solar_proposal);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.validate_customer_location_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('#locationPostCode').val();
    var locationState = $('#locationState').val();
    var locationstreetAddress = $('#locationstreetAddressVal').val();
    var locationTitle = $('#locationTitle').val();
    var locationContactPerson = $('#locationContactPerson').val();
    var locationContactEmail = $('#locationContactEmail').val();
    var locationContactPhone = $('#locationContactPhone').val();

    //Remove invalid
    $('#locationPostCode').removeClass('is-invalid');
    $('#locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');
    $('#locationContactPhone').removeClass('is-invalid');
    $('#locationTitle').removeClass('is-invalid');
    $('#locationContactEmail').removeClass('is-invalid');


    if (locationPostCode == '') {
        flag = false;
        $('#locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('#locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }
    if (locationTitle == '' && self.is_lead_allocation_sales_rep != true ) {
        //flag = false;
        //$('#locationTitle').addClass('is-invalid');
    }
    if (locationContactPerson == '' && self.is_lead_allocation_sales_rep != true ) {
        //flag = false;
        //$('#locationContactPerson').addClass('is-invalid');
    }
    if (locationContactEmail == '' && self.is_lead_allocation_sales_rep != true ) {
        //flag = false;
        //$('#locationContactEmail').addClass('is-invalid');
    }
    if (locationContactPhone == '' && self.is_lead_allocation_sales_rep != true ) {
        //flag = false;
        //$('#locationContactPhone').addClass('is-invalid');
    }


    return flag;
};

lead_manager.prototype.save_customer_location = function () {
    var self = this;
    $(document).on('click', '#save_customer_location', function () {
        var cust_id = document.getElementById('cust_id').value;
        var site_id = document.getElementById('site_id').value;
        var flag = self.validate_customer_location_data();
        if (!flag) {
            toastr.clear();
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var locationAddForm = $('#locationAdd').serialize();
            locationAddForm += '&cust_id=' + cust_id;
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer_location',
                datatype: 'json',
                data: locationAddForm,
                beforeSend: function () {
                    if (site_id != '' && site_id != null && site_id != undefined) {
                        toastr["info"]('Updating Customer Location please wait....');
                    } else {
                        toastr["info"]('Creating Customer Location into the system....');
                    }
                    $('#customer_location_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer_location").attr("disabled", "disabled");
                },
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        self.cust_location_data[stat.site_id] = stat.location;

                        //Populate Site in Table
                        $('#customer_location_table_body').html('');
                        $('#customer_booking_form_location_table_body').html('');
                        var location_data = self.cust_location_data;
                        for (var key in location_data) {
                            if (Object.keys(location_data[key]).length > 0) {
                                if (location_data[key].address == '' || location_data[key].address == null || location_data[key].address == 'null') {
                                    continue;
                                }
                                var row = self.create_customer_locations_table_row(location_data[key]);
                                var tbody = document.querySelector('#customer_location_table_body');
                                tbody.appendChild(row);
                                //Booking Form Table
                                var row1 = self.create_booking_form_customer_locations_table_row(location_data[key]);
                                var tbody1 = document.querySelector('#customer_booking_form_location_table_body');
                                tbody1.appendChild(row1);
                            }
                        }
                        self.create_booking_form();
                        self.edit_customer_location();
                        self.delete_customer_location();
                        $("#save_customer_location").removeAttr("disabled");
                        $('#add_customer_location_modal').modal('hide');
                        if(self.modal_return != ''){
                            setTimeout(function(){
                                $('#'+self.modal_return.id).click();
                            },500);
                        }
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer_location").removeAttr("disabled");
                        $('#customer_location_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer_location").removeAttr("disabled");
                    $('#customer_location_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};

lead_manager.prototype.edit_customer_location = function () {
    var self = this;
    $('.customer_location_edit_btn').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var site_id = $(this).attr('data-id');
        var location_data = self.cust_location_data[site_id];
        var cust_id = (location_data.cust_contact_id != null) ? location_data.cust_contact_id : location_data.cust_id;
        self.fetch_customer_and_contacts(cust_id, '#site_cust_contact_id');
        //Populate Cust location data in Modal
        $('#site_id').val(location_data.id);
        $('#locationLatitude').val(location_data.latitude);
        $('#locationLongitude').val(location_data.longitude);
        $('#locationTitle').val(location_data.site_name);
        $('#locationContactPerson').val(location_data.site_contact_name);
        $('#locationContactEmail').val(location_data.site_contact_email);
        $('#locationContactPhone').val(location_data.site_contact_no);
        $('#locationstreetAddressVal').val(location_data.address);
        $('#locationPostCode').val(location_data.postcode);
        $('#locationState').val(location_data.state_id);
        $('#select2-locationstreetAddress-container').html(location_data.address);

        $('#add_customer_location_modal_title').html('Edit Customer Location');
        $('#add_customer_location_modal').modal('show');
    });
}

lead_manager.prototype.delete_customer_location = function () {
    var self = this;
    $('.customer_location_delete_btn').click(function () {
        var is_allowed = self.check_ownership_rules();
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        $this = $(this);
        var site_id = $(this).attr('data-id');
        var data = {};
        data.site_id = site_id;
        data.lead_id = self.lead_data.id;
        data.cust_id = self.cust_data.id;
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/customer/delete_customer_location',
            datatype: 'json',
            data: data,
            beforeSend: function () {
                toastr["info"]('Deleting Customer Location and Connected Data from the system....');
                $this.attr("disabled", "disabled");
            },
            success: function (stat) {
                var stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                    self.fetch_proposal_total_details();
                } else {
                    toastr["error"](stat.status);
                    $this.removeAttr("disabled");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $this.removeAttr("disabled");
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });
}

lead_manager.prototype.fetch_customer_location = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/customer/fetch_customer_location',
        datatype: 'json',
        data: {cust_id: self.cust_data.cust_id},
        beforeSend: function () {
            //$('#customer_location_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('customer_location_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('customer_location_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#customer_location_table_body').html('');
                if (stat.location.length > 0) {
                    for (var i = 0; i < stat.location.length; i++) {
                        if (stat.location[i].address == '' || stat.location[i].address == null || stat.location[i].address == 'null') {
                            continue;
                        }
                        self.cust_location_data[stat.location[i].id] = stat.location[i];
                        var row = self.create_customer_locations_table_row(stat.location[i]);
                        var tbody = document.querySelector('#customer_location_table_body');
                        tbody.appendChild(row);
                        //Booking Form Table
                        var row1 = self.create_booking_form_customer_locations_table_row(stat.location[i]);
                        var tbody1 = document.querySelector('#customer_booking_form_location_table_body');
                        tbody1.appendChild(row1);
                    }
                    self.create_booking_form();
                    self.edit_customer_location();
                    self.delete_customer_location();
                } else {
                    $('#customer_location_table_body').html('<tr><td colspan="8" class="text-center">No location data found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#customer_location_table_body').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


/** Customer Contact Save and Edit Related Functions **/

lead_manager.prototype.create_customer_contacts_table_row = function (data) {
    var tr = document.createElement('tr');
    var td_contact_name = document.createElement('td');
    td_contact_name.innerHTML = data.first_name + ' ' + data.last_name;
    var td_position = document.createElement('td');
    td_position.innerHTML = data.position;
    var td_phone = document.createElement('td');
    td_phone.innerHTML = data.customer_contact_no;
    var td_email = document.createElement('td');
    td_email.innerHTML = data.customer_email;
    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm customer_contact_edit_btn";
    action_edit_btn.setAttribute('data-id', data.id);
    action_edit_btn.setAttribute('href', 'javascript:void(0);');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    td_action.appendChild(action_edit_btn);

    tr.appendChild(td_contact_name);
    tr.appendChild(td_position);
    tr.appendChild(td_phone);
    tr.appendChild(td_email);
    tr.appendChild(td_action);

    return tr;
}

lead_manager.prototype.validate_customer_contact_data = function () {
    var self = this;
    var flag = true;

    var contact_firstname = $('#contact_firstname').val();
    var contact_phone = $('#contact_phone').val();
    var contact_email = $('#contact_email').val();
    var contact_bussiness_name = $('#contact_bussiness_name').val();
    var contact_position = $('#contact_position').val();

    //Remove invalid
    $('#contact_firstname').removeClass('is-invalid');
    $('#contact_phone').removeClass('is-invalid');
    $('#contact_email').removeClass('is-invalid');
    $('#contact_bussiness_name').removeClass('is-invalid');
    $('#contact_position').removeClass('is-invalid');

    if (contact_firstname == '') {
        flag = false;
        $('#contact_firstname').addClass('is-invalid');
    }
    if (contact_phone == '') {
        //flag = false;
        //$('#contact_phone').addClass('is-invalid');
    }
    if (contact_email == '') {
        //flag = false;
        //$('#contact_email').addClass('is-invalid');
    }
    if (contact_bussiness_name == '') {
        flag = false;
        $('#contact_bussiness_name').addClass('is-invalid');
    }
    if (contact_position == '') {
        flag = false;
        $('#contact_position').addClass('is-invalid');
    }

    return flag;
};

lead_manager.prototype.save_customer_contact = function () {
    var self = this;
    $(document).on('click', '#save_customer_contact', function () {
        var cust_contact_id = $('#cust_contact_id').val();
        var flag = self.validate_customer_contact_data();
        if (!flag) {
            toastr.clear();
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var customerContactAddForm = $('#customerContactAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer_contact',
                datatype: 'json',
                data: customerContactAddForm,
                beforeSend: function () {
                    if (cust_contact_id != '' && cust_contact_id != null && cust_contact_id != undefined) {
                        toastr["info"]('Updating Customer Contact please wait....');
                    } else {
                        toastr["info"]('Creating Customer Contact into the system....');
                    }
                    $('#customer_contact_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer_contact").attr("disabled", "disabled");
                },
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        self.cust_contact_data[stat.cust_contact_id] = stat.contact;
                        $('#customer_contact_table_body').html('');
                        var contact_data = self.cust_contact_data;
                        for (var key in contact_data) {
                            if (Object.keys(contact_data[key]).length > 0) {
                                var row = self.create_customer_contacts_table_row(contact_data[key]);
                                var tbody = document.querySelector('#customer_contact_table_body');
                                tbody.appendChild(row);
                            }
                        }
                        self.edit_customer_contact();
                        $("#add_customer_contact_modal").modal("hide");
                        $("#save_customer_contact").removeAttr("disabled");
                        $('#customer_contact_loader').html('');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer_contact").removeAttr("disabled");
                        $('#customer_contact_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer_contact").removeAttr("disabled");
                    $('#customer_contact_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};

lead_manager.prototype.edit_customer_contact = function () {
    var self = this;
    $('.customer_contact_edit_btn').click(function () {
        var contact_id = $(this).attr('data-id');
        var data = self.cust_contact_data[contact_id];

        //Populate Cust Contact data in Modal
        $('#contact_cust_id').val(data.cust_id);
        $('#cust_contact_id').val(contact_id);
        $('#contact_firstname').val(data.first_name);
        $('#contact_firstname').val(data.first_name);
        $('#contact_lastname').val(data.last_name);
        $('#contact_phone').val(data.customer_contact_no);
        $('#contact_email').val(data.customer_email);
        $('#contact_bussiness_name').val(data.company_name);
        $('#contact_position').val(data.position);

        $('#add_customer_contact_modal_title').html('Edit Customer Contact');
        $('#add_customer_contact_modal').modal('show');
    });
}

lead_manager.prototype.fetch_customer_contacts = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/customer/fetch_customer_contacts',
        datatype: 'json',
        data: {cust_id: self.cust_data.cust_id},
        beforeSend: function () {
            //$('#customer_contact_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('customer_contact_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('customer_contact_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#customer_contact_table_body').html('');
                if (stat.contact.length > 0) {
                    for (var i = 0; i < stat.contact.length; i++) {
                        self.cust_contact_data[stat.contact[i].id] = stat.contact[i];
                        var row = self.create_customer_contacts_table_row(stat.contact[i]);
                        var tbody = document.querySelector('#customer_contact_table_body');
                        tbody.appendChild(row);
                    }
                    self.edit_customer_contact();
                } else {
                    $('#customer_contact_table_body').html('<tr><td colspan="8" class="text-center">No additional contacts found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#customer_contact_table_body').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.fetch_customer_and_contacts = function (selected, element) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/customer/fetch_customer_and_contacts',
        datatype: 'json',
        data: {cust_id: self.cust_data.cust_id},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var select = document.querySelector(element);
                select.innerHTML = '';
                var option = document.createElement('option');
                option.innerHTML = 'Please Select Contact Name';
                select.appendChild(option);

                if (stat.contact.length > 0) {
                    for (var i = 0; i < stat.contact.length; i++) {
                        var option = document.createElement('option');
                        option.value = stat.contact[i].id;
                        var name = (stat.contact[i].first_name == '') ? stat.contact[i].company_name : stat.contact[i].first_name + ' ' + stat.contact[i].last_name;
                        option.innerHTML = name;
                        option.setAttribute('data-item', JSON.stringify(stat.contact[i]));
                        select.appendChild(option);
                    }
                    if (selected == '' || selected == null) {
                        $('#site_cust_contact_id').val('Please Select Contact Name');
                    } else {
                        $('#site_cust_contact_id').val(selected);
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //$('#customer_contact_table_body').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


lead_manager.prototype.create_booking_form_customer_locations_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td_site_name = document.createElement('td');
    td_site_name.innerHTML = data.site_name;

    var td_address = document.createElement('td');
    td_address.innerHTML = data.address;

    var td_site_contact = document.createElement('td');
    td_site_contact.innerHTML = data.site_contact_no;
    var td_action = document.createElement('td');

    var action_create_btn = document.createElement('a');
    action_create_btn.className = "btn-info btn-sm customer_booking_form_location_create_btn";
    action_create_btn.setAttribute('data-id', data.id);
    action_create_btn.setAttribute('data-url', data.id);
    action_create_btn.setAttribute('href', 'javascript:void(0);');
    action_create_btn.innerHTML = '<i class="fa fa-plus"></i> Add';

    td_action.appendChild(action_create_btn);

    //tr.appendChild(td_site_name);
    tr.appendChild(td_address);
    //tr.appendChild(td_site_contact);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.create_booking_form = function (data) {
    $('.customer_booking_form_location_create_btn').click(function () {
        var site_id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        url += '&site_ref=' + site_id;
        window.open(url, '_blank');
    });
};

lead_manager.prototype.fetch_lead_booking_forms = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_booking_forms_data',
        datatype: 'json',
        data: {lead_id: self.lead_data.id},
        beforeSend: function () {
            //$('#customer_location_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('lead_booking_form_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('lead_booking_form_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#lead_booking_form_table_body').html('');
                if (stat.booking_form_data.length > 0) {
                    for (var i = 0; i < stat.booking_form_data.length; i++) {
                        var row = self.create_booking_form_table_row(stat.booking_form_data[i]);
                        var tbody = document.querySelector('#lead_booking_form_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    $('#lead_booking_form_table_body').html('<tr><td colspan="5" class="text-center">No booking form data found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_booking_form_table_body').html('<tr><td colspan="5" class="text-center">No booking form data found.</td></tr>');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.create_booking_form_table_row = function (data) {

    var self = this;

    var tr = document.createElement('tr');

    var td_site = document.createElement('td');
    td_site.innerHTML = data.address;

    var td_bf_type = document.createElement('td');
    if (data.type == 'LED') {
        td_bf_type.innerHTML = data.type;
    } else {
        td_bf_type.innerHTML = data.type + '(' + data.type_name + ')';
    }

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_total = document.createElement('td');
    var booking_from_data = (data.booking_form != null) ? JSON.parse(data.booking_form) : '';
    var total_cost = (booking_from_data != '') ? '$' + booking_from_data.total_excGst : '-';
    td_total.innerHTML = total_cost;

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        if (data.type == 'LED') {
            action_edit_btn.setAttribute('href', base_url + 'admin/booking_form/edit_led_booking_form/' + data.uuid);
        } else {
            action_edit_btn.setAttribute('href', base_url + 'admin/booking_form/edit_solar_booking_form/' + data.uuid);
        }
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Manage';

    td_action.appendChild(action_edit_btn);

    tr.appendChild(td_date);
    tr.appendChild(td_site);
    tr.appendChild(td_bf_type);
    tr.appendChild(td_total);
    tr.appendChild(td_action);

    return tr;
};


lead_manager.prototype.fetch_lead_form_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_form_data',
        datatype: 'json',
        data: {lead_id: self.lead_data.id},
        beforeSend: function () {
            //$('#customer_location_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('lead_forms_data_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('lead_forms_data_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#lead_forms_data_table_body').html('');
                //Handle Booking Form Rows
                if (stat.booking_form_data.length > 0) {
                    if(stat.booking_form_data.length == 1){
                        if(stat.booking_form_data[0].type == 'Solar'){
                            //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Booking Form - LED</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                        }
                    }
                    for (var i = 0; i < stat.booking_form_data.length; i++) {
                        var row = self.create_lead_booking_form_table_row(stat.booking_form_data[i]);
                        var tbody = document.querySelector('#lead_forms_data_table_body');
                        tbody.appendChild(row);
                    }
                    if(stat.booking_form_data.length == 1){
                        if(stat.booking_form_data[0].type == 'LED'){
                            //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Booking Form - Solar</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                        }
                    }
                } else {
                    //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Booking Form - LED</td><td colspan="3" class="text-center">No form data found.</td></tr><tr><td>Booking Form - Solar</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                }

                //Handle Meter Data Rows
                if (stat.meter_data.length > 0) {
                    for (var i = 0; i < stat.meter_data.length; i++) {
                        var row = self.create_meter_data_form_table_row(stat.meter_data[i]);
                        var tbody = document.querySelector('#lead_forms_data_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Meter Data Request</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                }

                //Handle Quick Quote Rows
                if (stat.quote_data.length > 0) {
                    for (var i = 0; i < stat.quote_data.length; i++) {
                        var row = self.create_quote_data_form_table_row(stat.quote_data[i]);
                        var tbody = document.querySelector('#lead_forms_data_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Quick Quote</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                }

                if ((stat.booking_form_data.length == 0) && (stat.meter_data.length == 0) && (stat.quote_data.length == 0)) {
                    document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td colspan="4" class="text-center">No form data found.</td></tr>';
                }
            }
            self.upload_simpro_attachment();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_forms_data_table_body').html('<tr><td colspan="5" class="text-center">No form data found.</td></tr>');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.create_lead_booking_form_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td_site = document.createElement('td');
    td_site.innerHTML = data.address;

    var td_bf_type = document.createElement('td');
    if (data.type == 'LED') {
        td_bf_type.innerHTML = 'Booking Form - ' + data.type;
    } else {
        td_bf_type.innerHTML = 'Booking Form - ' + data.type + '(' + data.type_name + ')';
    }

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = (data.simpro_job_id != null && data.simpro_job_id != '') ? "btn-info btn-sm" : "btn-danger btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        if (data.type == 'LED') {
            action_edit_btn.setAttribute('href', base_url + 'admin/booking_form/edit_led_booking_form/' + data.uuid);
        } else {
            action_edit_btn.setAttribute('href', base_url + 'admin/booking_form/edit_solar_booking_form/' + data.uuid);
        }
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    if (data.simpro_job_id != null && data.simpro_job_id != '') {
        action_edit_btn.innerHTML = '<i class="fa fa-eye"></i> View';
    } else {
        action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Complete Draft';
    }

    var action_btn_group = document.createElement('div');
    action_btn_group.className = 'btn-group';

    var action_btn_group_btn = document.createElement('button');
    action_btn_group_btn.setAttribute('type', 'button');
    action_btn_group_btn.className = 'btn btn-info dropdown-toggle mr-1 mb-1';
    action_btn_group_btn.setAttribute('data-toggle', 'dropdown');
    action_btn_group_btn.setAttribute('aria-haspopup', 'true');
    action_btn_group_btn.setAttribute('aria-expanded', 'false');
    action_btn_group_btn.innerHTML = 'File';

    var action_btn_group_dropdown_menu = document.createElement('div');
    action_btn_group_dropdown_menu.className = 'dropdown-menu';

    if (data.simpro_job_id != null && data.simpro_job_id != '') {
        var action_upload_btn = document.createElement('a');
        action_upload_btn.className = "btn-warning btn-sm mr-2 upload_simpro_attachment_btn";
        action_upload_btn.setAttribute('href', 'javascript:void(0);');
        action_upload_btn.setAttribute('data-id', data.uuid);
        action_upload_btn.setAttribute('data-type', data.type);
        action_upload_btn.innerHTML = '<i class="fa fa-upload" aria-hidden="true"></i> Upload';

        var action_download_btn = document.createElement('a');
        action_download_btn.className = "btn-primary btn-sm mr-2";
        if (data.type == 'LED') {
            action_download_btn.setAttribute('href', base_url + 'assets/uploads/led_booking_form_files/' + data.pdf_file);
        }else{
            action_download_btn.setAttribute('href', base_url + 'assets/uploads/solar_booking_form_files/' + data.pdf_file);
        }
        action_download_btn.setAttribute('target', '__blank');
        action_download_btn.innerHTML = '<i class="fa fa-download" aria-hidden="true"></i> Download';
        //action_btn_group_dropdown_menu.appendChild(action_upload_btn);
        //action_btn_group_dropdown_menu.appendChild(action_download_btn);
        //action_btn_group_dropdown_menu.appendChild(action_download_btn);
        if (data.simpro_attachment_id == null || data.simpro_attachment_id == '') {
            td_action.appendChild(action_upload_btn);
        }
        if (data.pdf_file != null && data.pdf_file != '') {
            td_action.appendChild(action_download_btn);
        }
    }

    //action_btn_group.appendChild(action_btn_group_btn);
    //action_btn_group.appendChild(action_btn_group_dropdown_menu);

    //td_action.appendChild(action_btn_group);
    td_action.appendChild(action_edit_btn);
    tr.appendChild(td_bf_type);
    tr.appendChild(td_site);
    tr.appendChild(td_date);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.create_meter_data_form_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td_site = document.createElement('td');
    td_site.innerHTML = data.address;

    var td_bf_type = document.createElement('td');
    td_bf_type.innerHTML = 'Meter Data Request';

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        action_edit_btn.setAttribute('href', base_url + 'admin/lead/edit_meter_data/' + data.uuid);
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> View/Edit';

    td_action.appendChild(action_edit_btn);

    tr.appendChild(td_bf_type);
    tr.appendChild(td_site);
    tr.appendChild(td_date);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.create_quote_data_form_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td_site = document.createElement('td');
    td_site.innerHTML = data.customer_address;

    var td_bf_type = document.createElement('td');
    td_bf_type.innerHTML = 'Quick Quote';

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        action_edit_btn.setAttribute('href', base_url + 'admin/lead/edit_quick_quote/' + data.uuid);
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> View/Edit';

    td_action.appendChild(action_edit_btn);

    tr.appendChild(td_bf_type);
    tr.appendChild(td_site);
    tr.appendChild(td_date);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.upload_simpro_attachment = function () {
    var self = this;
    $('.upload_simpro_attachment_btn').click(function () {
        $this = $(this);
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var url = '';
        url = (type == "LED") ? 'admin/booking_form/led_booking_form_upload_pdf' : 'admin/booking_form/solar_booking_form_upload_pdf';
        $.ajax({
            type: 'POST',
            url: base_url + url,
            datatype: 'json',
            data: {uuid : id},
            beforeSend: function () {
                toastr["info"]('Uploading Booking form to simPRO....');
                $this.attr("disabled", "disabled");
            },
            success: function (stat) {
                var stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.remove();
                } else {
                    toastr["error"](stat.status);
                    $this.removeAttr("disabled");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $this.removeAttr("disabled");
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};


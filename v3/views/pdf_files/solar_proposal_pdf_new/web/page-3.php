<html>

<head>
    <title>Solar Proposal</title>
    <style>
        .page3-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            width: 910px;
            height: 1300px;
            box-shadow: 1px 1px 3px 1px #333;
            border-collapse: separate;
            margin-bottom: 20px;
        }

        .page3 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page3 .top hr {
            width: 65%;
            height: 1px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page3 .text_color {
            color: #D01E2A;
        }

        .page3 .topimagepagee h2 {
            background-color: #840e12c2;
            font-size: 25px;
            padding-top: 35;
            padding-bottom: 35;
            text-align: center;
            color: #ffffff;
        }

        .page3 .whykuga p {
            margin-left: 48;
            color: #423939;
            margin-right: 48;
            font-size: 20px;
            font-weight: 500;
            text-align: justify;
        }

        .page3 .whykuga h3 {
            margin-left: 48;
            margin-top: 65px;
            color: #D52323;
        }

        .page3 .topimagepage {
            margin-top: 30px;
        }

        .page3 .topimagepagee {
            margin-top: -120px;
            position: absolute;
            width: 910px;
        }

        /** Footer */
        .page3 .footer {
            display: inline-flex;
        }

        .page3 .fimagee img {
            margin-left: 730px;
            margin-top: 58px;
        }

        .page3 .pb {
            padding-bottom: 235px;
        }

        .page3 .zaspan {
            margin-left: 0px;
        }

        .page3 .footer-p p {
            font-size: 10px;
            margin-left: 48;
            margin-top: -23px;
            display: none;
        }

        .page3 .footer-pp p {
            font-size: 10px;
            margin-left: 46px;
            margin-top: -22px;
        }

        .page3 .footer-pp {
            margin-left: 286px;
            margin-top: -36px;
            display: none;
        }

        .page3 .zafooter-icon img {
            width: 25px;
            margin-right: 10px;
            display: none;
        }

        .page3 .zafooter-iconn img {
            width: 25px;
            margin-right: 10px;
        }

        .page3 .zayear {
            font-size: 17px;
        }

        .page3 .redf {
            height: 10px;
            background: red;
            width: 455px;
        }

        .page3 .greyf {
            height: 10px;
            background: #dadada;
            width: 455px;
        }

        .page3 #myVideo {
            right: 0;
            bottom: 0;
            width: 909px;
            object-fit: cover;
            height: 500px;
        }
    </style>
</head>

<body>
    <div class="page3-back-bg page3">
        <div class="top">
            <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="topimagepage">
            <video autoplay muted loop id="myVideo">
                <source src="<?php echo site_url('assets/solar_proposal_pdf_new/KugaLanding-new-2.mp4'); ?>" type="video/mp4">
            </video>
            <div class="topimagepagee">
                <h2>5 REASONS WHY BUSINESSES CHOOSE KUGA</h2>
            </div>
        </div>
        <div class="whykuga">
            <img src="<?php echo site_url('assets/solar_proposal_pdf_new/kuge_proposal_5_reason.PNG'); ?>" />
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
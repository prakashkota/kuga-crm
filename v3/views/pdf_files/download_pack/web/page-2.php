<html>

<head>
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page-2-back-bg {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 1287px !important;
            max-height: 910px !important;
            width: 1287px !important;
        }

        .page-2 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 1044px;
            padding-top: 30px;
        }

        .page-2 .top hr {
            width: 76%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page-2 .text_color {
            color: #D01E2A;
        }

        .page-2 .title {
            padding-top: 12px;
        }

        .page-2 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page-2 .title h2 {
            padding-left: 14px;
            margin-left: 48px;
            border-right: none;
            font-weight: normal;
        }
        .page-2 .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-2 .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }

        .page-2 .ingBox {
            display: inline-flex;
            padding-left: 50px;
            width: 90%;
        }
        .ingBox table > thead > tr > th:first-child,
        .ingBox table > tbody > tr > th:first-child{
            width: 400px !important;
        }


        .ingBox table {
            border-collapse: collapse !important;
        }
        .ingBox table tbody tr td,
        .ingBox table thead tr th {
            border: 1px dotted grey !important;
        }
        .ingBox table thead tr:first-child th {
            border-top: 0 !important;
        }
        .ingBox table thead tr th {
            height: 30px !important;
        }
        .ingBox table tbody tr td {
            height: 100px !important;
            text-align: center;
        }
        .ingBox table tbody tr:last-child td {
            border-bottom: 0 !important;
        }
        .ingBox table tbody tr td:first-child,
        .ingBox table thead tr th:first-child {
            border-left: 0 !important;
        }
        .ingBox table tbody tr td:last-child,
        .ingBox table thead tr th:last-child {
            border-right: 0 !important;
        }

        .ingBox table > thead > tr > th:not(:first-child),
        .ingBox table > tbody > tr > td:not(:first-child){
            width: 200px !important;
        }
        .ingBox table > thead > tr > th{
            font-size: 12px;
            font-weight: 900;
            color:grey;
        }
        .ingBox table > tbody > tr:nth-child(even) {background: #fff;}
        .ingBox table > tbody > tr:nth-child(odd) {background: #f4f4f4;}

        .page-2 .iiimage {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 650px;
            margin-left: 48px;
        }

        .page-2 .iibox {
            height: 400;
            border: 1px solid #c5c5c5;
            width: 500px;
            margin-left: 48px;
        }
        a.download{
            background-color: #555555;
            padding: 3px 10px;
            color: white;
            border-radius: 10px;
            font-size: 12px;
            font-weight: 700;
            text-transform: uppercase;
        }
        a.download:hover{
            background-color: #C20000;
        }
        .page-2 .footer {
            margin-top: -3px;
            display: inline-flex;
        }

        .page-2 .redf {
            width: 643px;
            height: 10px;
            background: red;
        }

        .page-2 .greyf {
            width: 643px;
            height: 10px;
            background: #EDEBEC;
        }
        .page-2 .fimagee img {
            margin-left: 1120px;
        }

    </style>
</head>

<?php
$inverter_design['snapshot'] = '';
$inverter_design['notes'] = '';
?>

<body>
    <div class="page-2-back-bg back-bg page-2">
        <div class="top">
            <!-- <h2>
                <span class="text_color">SOLAR PROPOSAL</span> | KUGA
            </h2>
            <hr> -->
        </div>
        <div class="title">
            <h1>WE’VE SELECTED THE FOLLOWING PRODUCTS <span class="text_color">FOR YOUR PROJECT </span></h1>
        </div>
        <div class="ingBox">
            <table width="100%">
                <thead>
                    <tr>
                        <th colspan="2">PRODUCT</th>
                        <th>WARRANTY</th>
                        <th>DATASHEET</th>
                        <th>PRODUCT VIDEO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr><tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr><tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Download&nbsp;</a></td>
                        <td><a  class="download">&nbsp;Watch&nbsp;</a></td>
                    </tr>


                </tbody>
            </table>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
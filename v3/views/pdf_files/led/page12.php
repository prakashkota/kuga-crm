<?php $html = "
<tr>
    <td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Get solar for your business
    </td>
  </tr>

<tr>
<td align=\"left\" valign=\"top\">
<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\">Powering you with massive cost saving</td>
</tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;\">
Kuga Electrical has helped many businesses put solar power on their roof. Our commercial solar solutions are designed to create savings for your business and will help you manage your ongoing overheads. By producing and consuming your own solar energy, your savings can create positive cash flow and help you re-invest your savings into your business.
</td>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\">
        <img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/re_invest.jpg\" style=\"width:68%;  display:block;\">
</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\">Secure your STC rebate</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px;\">
The best time to get your solar power system is now. Maximize the value of your solar power system by installing your system as STC rebate decreases every year. Put more savings in your pocket when you lock in your system this year and enjoy the lower energy cost with power generated from your own solar on the roof.
</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; color:#000; line-height:20px; font-weight:bold;\">Our Partners</td>
</tr>
<tr>
<td align=\"center\" valign=\"top\">
        <img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/backed_by.jpg\" style=\"width:98%;  display:block;\">
</td>
</tr>

</tr>
</tbody>
</table>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
<tr height=\"72px\">
    <td>&nbsp;</td>
  </tr>
<tr>
<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/solarpdfimages/footer-pic1.jpg\" alt=\"\" title=\"\" style=\"   width:100%; display:block;\" /></td>
</tr>

";
echo $html;	
	?>
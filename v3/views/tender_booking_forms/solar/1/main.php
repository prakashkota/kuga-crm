<style>
     @media print {
        .no-print {display: none;}
        body {background: transparent;}
        .red{color:#c32027}
        .black-bg {background-color:#010101}
        .dark-bg{background-color:#282828}
        .page-wrapper{ min-height:100%;}
    }
    @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 150px;
        }
    }
    table,tr,td{
        margin : auto !important;
    }
    
    .sr-deal_actions{
        opacity: 1;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: 0px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }

    .pdf_page{
        width: 910px;
        box-shadow: 1px 1px 3px 1px #333;
        border-collapse: separate;
        padding:5px;
    }
    .add-picture{
        padding: 180px 0 !important;
    }



    .signature-pad {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        font-size: 10px;
        width: 100%;
        height: 100%;
        max-height: 460px;
        border: 1px solid #e8e8e8;
        background-color: #fff;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
        border-radius: 4px;
        padding: 16px;
    }

    .signature-pad::before,
    .signature-pad::after {
        position: absolute;
        z-index: -1;
        content: "";
        width: 40%;
        height: 10px;
        bottom: 10px;
        background: transparent;
        box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
    }

    .signature-pad::before {
        left: 20px;
        -webkit-transform: skew(-3deg) rotate(-3deg);
        transform: skew(-3deg) rotate(-3deg);
    }

    .signature-pad::after {
        right: 20px;
        -webkit-transform: skew(3deg) rotate(3deg);
        transform: skew(3deg) rotate(3deg);
    }

    .signature-pad--body {
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #f4f4f4;
    }

    .signature-pad--body
    canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
    }

    .signature-pad--footer {
        color: #C3C3C3;
        text-align: center;
        font-size: 1.2em;
        margin-top: 8px;
    }

    .signature-pad--actions {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-top: 8px;
    }

    #github img {
        border: 0;
    }

    @media (max-width: 990px) {
        #github img {
            width: 90px;
            height: 90px;
        }
        .sr-deal_actions{
            opacity: 1;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            height: 67px;
            background-color: #f7f7f7;
            z-index: 100;
            box-shadow: 0 5px 40px rgba(0,0,0,.4);
            transition: opacity .1s ease-out,bottom .1s ease-out .5s;
            display: flex;
        }
        
        .page-wrapper {
            padding: 60px 0px 0px;
        }
        
    }
    
    /** IPAD */
    @media only screen 
    and (min-width: 1024px) 
    and (max-height: 1366px) 
    and (-webkit-min-device-pixel-ratio: 1.5) {
        .page-wrapper {
            padding: 30px 30px 30px 105px;
        }
    }
    
    .signature_image_close,.image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: -18px;
        right: -18px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .signature_image_container,.image_container{ 
        margin: 20px 20px 20px 20px;
        overflow: visible;
        box-shadow: 5px 5px 2px #888888;
        position: relative;
    }
    
    .is-invalid{border: 1px solid red;}
     #signatures{    
        height: 400px;
    }
    .sign_close,.sign_undo{display: none;}
    
    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<div class="page-wrapper d-block clearfix " >
    <div style="padding:0; margin:0" id="booking_form">
        <form id="solar_booking_form" enctype="multipart/form-data">
            <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom: 10px !important;">
                <tbody>
                    <tr>
                        <td align="left" valign="top" style="display: flex;">
                           <label>Cost Centre</label>
                           <?php $opArr = [
                                    ['id' => '' , 'name' => 'Select Cost Centre'],
                                    ['id' => '4' , 'name' => 'VIC - Solar'],
                                    ['id' => '5' , 'name' => 'NSW - Solar'],
                                ];
                            ?>
                            <select class="form-control ml-3" style="width: 200px;" name="cost_centre_id" id="cost_centre_id">
                                <?php foreach($opArr as $key => $value){ ?>
                                    <option <?php if($value['id'] == $cost_centre_id) {?> selected="" <?php } ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                <?php } ?>
                            </select>
                            <!--<input type="hidden" name="cost_centre_id" id="cost_centre_id"  /> -->
                        </td>
                    </tr>
                </tbody>
            </table>
            <input name="type_id" type="hidden" value="<?php echo $type_id; ?>" />
            <?php $this->load->view('tender_booking_forms/solar/1/page1'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page2'); ?><br/>
            <?php $this->load->view('tender_booking_forms/solar/1/page4_1'); ?><br/>
            <?php $this->load->view('tender_booking_forms/solar/1/page3'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page4'); ?><br/>  
            <?php $this->load->view('tender_booking_forms/solar/1/page5'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page6'); ?><br/>
            <?php $this->load->view('tender_booking_forms/solar/1/page6_1'); ?><br/> 
            <?php $this->load->view('tender_booking_forms/solar/1/page7'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page8'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page9_'.$type_id); ?><br/>  
            <?php $this->load->view('tender_booking_forms/solar/1/page10'); ?><br/>    
            <?php $this->load->view('tender_booking_forms/solar/1/page11'); ?> 
        </form>
    </div>
    <div  class="sr-deal_actions">
        <div class="col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="save_booking_form" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save as Draft</a>
        </div>
        <div class=" col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="generate_booking_form_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-file-pdf-o"></i> Generate Booking Form</a>
            <a href="javascript:void(0);" id="generate_booking_form_pdf_link" class="hidden" ></a>
        </div>
    </div>
    
    <div id="signatures">
        
        <div class="signature-pad hidden" id="signpad_create_1">
            <div class="signature-pad--body">
                <canvas width="664" height="200" style="touch-action: none; border: 1px solid black;" id="signature_pad_1"></canvas>
            </div>
            <div class="signature-pad--footer">
                <div class="signature-pad--actions">
                    <div>
                        <button type="button" class="btn button sign_clear" data-action="clear" data-id="1">Clear</button>
                        <button type="button" class="btn button sign_undo" data-action="undo" data-id="1">Undo</button>
                    </div>
                    <div>
                        <button type="button" class="btn button sign_close" data-id="1" id="signpad_close_1">Close</button>
                        <button type="button" class="btn button sign_save" data-action="save-png" data-id="1">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="signature-pad hidden" id="signpad_create_2" style="margin-top:-400px;">
            <div class="signature-pad--body">
                <canvas width="664" height="200" style="touch-action: none; border: 1px solid black;" id="signature_pad_2"></canvas>
            </div>
            <div class="signature-pad--footer">
                <div class="signature-pad--actions">
                    <div>
                        <button type="button" class="btn button sign_clear" data-action="clear" data-id="2">Clear</button>
                        <button type="button" class="btn button sign_undo" data-action="undo" data-id="2">Undo</button>
                    </div>
                    <div>
                        <button type="button" class="btn button sign_close" data-id="2" id="signpad_close_2">Close</button>
                        <button type="button" class="btn button sign_save" data-action="save-png" data-id="2">Save</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="signature-pad hidden" id="signpad_create_3" style="margin-top:-400px;">
            <div class="signature-pad--body">
                <canvas width="664" height="200" style="touch-action: none; border: 1px solid black;" id="signature_pad_3"></canvas>
            </div>
            <div class="signature-pad--footer">
                <div class="signature-pad--actions">
                    <div>
                        <button type="button" class="btn button sign_clear" data-action="clear" data-id="3">Clear</button>
                        <button type="button" class="btn button sign_undo" data-action="undo" data-id="3">Undo</button>
                    </div>
                    <div>
                        <button type="button" class="btn button sign_close"  id="signpad_close_3" data-id="3">Close</button>
                        <button type="button" class="btn button sign_save" data-action="save-png" data-id="3">Save</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/signpad.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/tender_solar_booking_form.js?v=<?php echo version; ?>"></script>
<script>
    var context = {};
    context.lead_uuid = '<?php echo (isset($lead_uuid) && !empty($lead_uuid)) ? $lead_uuid : ''; ?>';
    context.lead_id = '<?php echo (isset($lead_id) && !empty($lead_id)) ? $lead_id : ''; ?>';
    context.site_id = '<?php echo (isset($site_id)) ? $site_id : ''; ?>';
    context.booking_form_uuid = '<?php echo (isset($booking_form_uuid)) ? $booking_form_uuid : ''; ?>';
    context.gst = '<?php echo GST ?>';
    var tender_solar_booking_form = new tender_solar_booking_form(context);
</script>

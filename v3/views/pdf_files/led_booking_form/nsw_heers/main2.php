<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>LED Booking Form</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }


            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none;	
            }

            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                display: block;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }

            input[type=checkbox]:checked:before {
                color: black !important;
            }
            
            .checkbox_light:before {
                border: 1px solid #e4e4e4 !important;
            }
            
            .checkbox_payment:before {
                font-size: 15px !important;
                width: 15px !important;
                height: 15px !important;
                border: none !important;
            }
            
        </style>
    </head>
    <body style="padding:0; margin:0">
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page4'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page5'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page6'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page7'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page8'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page9'); ?>
        <?php $this->load->view('pdf_files/led_booking_form/nsw_heers/page10'); ?>
    </body>
</html>
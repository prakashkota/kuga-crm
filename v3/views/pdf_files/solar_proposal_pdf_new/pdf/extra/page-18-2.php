<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page18  .back-bg {
		background-color: white;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page18  .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 630px;
		padding-top: 30px;
	}
	
	.page18  .top hr {
		width: 70%;
		height: 2px;
		margin: -24px 0px 0px;
		background-color: #D01E2A;
	}
	
	.page18  .text_color {
		color: #D01E2A;
	}
	
	.page18  .vkcontainer {
		margin: 0 30px;
	}
	
	.page18  .title h1 {
		padding-left: 14px;
		margin-top: 40px;
		border-left: 5px solid #D01E2A;
	}
	
	.page18  .vktitle h2 {
		font-weight: 500;
		letter-spacing: 3px;
		padding: 12px;
		margin-top: 15px;
		background-color: #D01E2A;
		width: 100%;
		color: #ffffff;
		background: linear-gradient( 125deg, #D01E2A 87%, transparent 15%);
	}
	
	.page18  table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}
	
	.page18  tr {
		border-bottom: 1px solid #cac8c8;
	}
	
	.page18  td {
		padding: 15px;
		color: #3D3B3C;
	}
	
	.page18  .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.image img {
		margin-top: -50px;
	}
	
	.page18  .fimagee img {
		margin-left: 650px;
		width: 230;
	}
	
	.page18  .vk-innerbox {
		width: 33%;
		float: left;
		text-align: center;
	}
	
	.page18  .vk-innerbox h4 {
		margin: 0px;
	}
	
	.page18  .vkid-section {
		padding-top: 40px;
		margin-bottom: 60px;
	}
	
	.page18  .vktitle h2 span {
		background: #fff;
		padding: 2px 4px;
		margin-right: 15px;
		color: #fff;
		font-size: 12px;
	}
	
	.headertable tr {
		border: none;
		text-align: center;
	}
	
	.page18  .headertable td {
		width: 33.33%;
	}
	
	.page18  table.headertable {
		margin-top: 75px;
	}
	
	.page18  .tablefooter tr {
		border: none;
	}
	
	.page18  .tablefooter tr th {
		text-align: left;
		padding-left: 20px;
		color: #D01E2A;
	}
	
	.page18  .tablefooter td {
		width: 50%;
		padding-left: 20px;
	}
	
	.page18  .comfooter {
		border-top: 1px dashed #D01E2A;
		border-bottom: 1px dashed #D01E2A;
		padding: 30px 0;
		margin-top: 70px;
	}
	
	.page18  .installvk td {
		width: 50%;
	}
	
	.page18  .installvk tr {
		border: none;
	}
	
	.page18  .installvk p {
		font-size: 17px;
		line-height: 22px;
	}
	
	.page18 .installvk .vktitle h2 {
		letter-spacing: 1px;
	}
	</style>
</head>

<body>
	<div class="back-bg page18">
		<div class="vkcontainer">
			<div class="top vk-header" style="width:100%;posi">
				<div class="headertext">
					<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
					</div>
					<hr>
				</div>
				<div class="title">
					<h1 style="text-transform:uppercase;">RECENT <span class="text_color"> INSTALLS</span></h1>
					
				</div>
				
				
				<div class="installvk" style="margin-top:40px;">
					<table>
						<tr>
							<td style="border: 1px solid #999;">
							<iframe height="250px" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
							
							
							</td>
							<td style="padding-top:0px;"><div class="vktitle" style="">
									<h2 style=" font-size:19px;margin-top:0px;"><b> Extrusions Australia PTY LTD</b> </h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et </p>
				</div>
				</td>
				</tr>
				</table>
			</div>
			<div class="installvk" style="margin-top:40px;">
				<table>
					<tr>
						<td style="border: 1px solid #999;">
							<iframe height="250px" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
						</td>
						<td style="padding-top:0px;">
							<div class="vktitle" style="">
								<h2 style=" font-size:19px;margin-top:0px;"><b>Medline </b> </h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et </p>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="installvk" style="margin-top:40px;">
				<table>
					<tr>
						<td style="border: 1px solid #999;">
							<iframe height="250px" width="100%" allowfullscreen="true" src="https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"></iframe>
						</td>
						<td style="padding-top:0px;">
							<div class="vktitle" style="">
								<h2 style=" font-size:19px;margin-top:0px;"><b>Della Rosa Fine Foods </b> </h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et </p>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="fimagee" style="margin-bottom: -66px;">
			<a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a>
		</div>
		<div class="footer"></div>
	</div>
</body>

</html>
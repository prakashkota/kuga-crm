
var survey_reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.start_date = '';
    this.end_date = '';
    this.apply_date_filter = true;

    if (self.user_group != '1' && self.all_reports_view_permission == '0') {
        $('#statistics_container').hide();
        $('#error_container').removeClass('hidden');
        $('#reporting_actions').addClass('hidden');
        $('.page-wrapper').attr('style', 'height:100% !important;');
    } else {
        $(function () {
            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end, applyDateFilter) {
                if (typeof applyDateFilter == 'undefined') {
                    applyDateFilter = true;
                }
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                document.getElementById('start_date').value = start.format('YYYY-MM-DD');
                document.getElementById('end_date').value = end.format('YYYY-MM-DD');
                self.start_date = start.format('d/M/Y');
                self.end_date = end.format('d/M/Y');
                if (applyDateFilter) {
                    self.apply_date_filter = true;
                } else {
                    self.apply_date_filter = false;
                }
                self.re_initialize();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, false);
//            self.fetch_franchise_with_rating();

        });

        $('.sr-show_inactive_users').click(function () {
            $('.sr-inactive_user').addClass('hidden');
            if ($(this).prop("checked") == true) {
                $('.sr-inactive_user').removeClass('hidden');
            }
        });

        $('#userid').on('change', function () {
            self.re_initialize();
        });
        $(document).on('click', '.load-franchise-review', function () {
            var franchiseId = this.getAttribute('data-id');
            self.fetch_franchise_reviews(franchiseId);
        });
    }

};
survey_reporting.prototype.fetch_franchise_with_rating = function () {
    var self = this;
    var form_data = '';
    if (self.apply_date_filter) {
        form_data = $('#reporting_actions').serialize();
    }
    var userId = $('#userid').val();
    form_data += 'userid=' + userId;

    $.ajax({
        url: base_url + "admin/reports/fetch-franchise-list?",
        type: 'get',
        dataType: "json",
        data: form_data,
        beforeSend: function () {
            document.getElementById('franchise_list').appendChild(createLoader('Fetching Franchise list....'));
        },
        success: function (data) {
            if (data.status) {
                self.handle_franchise_list(data.data);
            } else {
                document.getElementById('franchise_list').innerHTML = data.message;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('franchise_list').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}
survey_reporting.prototype.fetch_franchise_reviews = function (franchiseId) {

    var self = this;
    var form_data = '';
    if (self.apply_date_filter) {
        form_data = $('#reporting_actions').serialize();
    }
    var userId = $('#userid').val();
    if (typeof franchiseId == 'undefined') {
        form_data += 'userid=' + userId;
    } else {
        form_data += 'userid=' + franchiseId;
    }



    $.ajax({
        url: base_url + "admin/reports/fetch-franchise-reviews?",
        type: 'get',
        dataType: "json",
        data: form_data,
        beforeSend: function () {
            document.getElementById('reviews_list').appendChild(createLoader('Fetching review list....'));
        },
        success: function (data) {
            if (data.status) {
                self.handle_franchise_reviews(data.data);
            } else {
                document.getElementById('reviews_list').innerHTML = data.message;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('reviews_list').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}


survey_reporting.prototype.re_initialize = function () {
    var self = this;
    document.getElementById('franchise_list').innerHTML = '';
    document.getElementById('reviews_list').innerHTML = '';
    self.fetch_franchise_with_rating();
    self.fetch_franchise_reviews();
}


survey_reporting.prototype.handle_franchise_list = function (data) {
    var self = this;
    if ($('#franchise_tbl')) {
        $('#franchise_tbl').DataTable().destroy();
    }
    $('#franchise_tbl').show();
    document.getElementById('franchise_list').innerHTML = "";
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        td1.innerHTML = data[i].company_name;
        var td2 = document.createElement('td');
        td2.innerHTML = data[i].franchise_name;
        var td3 = document.createElement('td');
        var ratingPercentage = data[i].avg_rating * 20;
        var ratingStars = '<div class="star-ratings" style="margin:0;"><div class="star-ratings-top" style="width:' + ratingPercentage + '%"></div><div class="star-ratings-bottom"></div></div>'
        td3.innerHTML = ratingStars + ' (' + data[i].total_reviews + ' Reviews)';
        var td5 = document.createElement('td');
        td5.innerHTML = data[i].total_reviewlt4;
        var td6 = document.createElement('td');
        td6.innerHTML = '<a href="javascript:void(0)" data-id="' + data[i].user_id + '" class="text-info load-franchise-review">View reviews</a>';

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td5);
        tr.appendChild(td6);

        document.getElementById('franchise_list').appendChild(tr);
    }

    $('#franchise_tbl').DataTable({
        "order": [
            [3, "asc"]
        ],
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": true,
    });

}
survey_reporting.prototype.handle_franchise_reviews = function (data) {
    var self = this;
    if ($('#reviews_tbl')) {
        $('#reviews_tbl').DataTable().destroy();
    }
    $('#reviews_tbl').show();
    document.getElementById('reviews_list').innerHTML = "";
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        td1.innerHTML = data[i].company_name;
        var td2 = document.createElement('td');
        td2.innerHTML = data[i].franchise_name;
        var td3 = document.createElement('td');
        var ratingPercentage = data[i].rating * 20;
        var ratingStars = '<div class="star-ratings" style="margin:0;"><div class="star-ratings-top" style="width:' + ratingPercentage + '%"></div><div class="star-ratings-bottom"></div></div>'
        td3.innerHTML = ratingStars;
        var td7 = document.createElement('td');
        td7.innerHTML = '<small>' + data[i].comment.substr(0, 100) + '...<a href="javascript:void(0)" data-id="'+data[i].survey_id+'" class="load-complete-review text-info">More</a></small>';
        var td8 = document.createElement('td');
        td8.innerHTML = data[i].status;
        var td9 = document.createElement('td');
        td9.innerHTML = data[i].created_at;
        var td10 = document.createElement('td');
        td10.innerHTML = '<a href="javascript:void(0)" data-id="'+data[i].survey_id+'" class="load-complete-review text-info">View</a>';

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td7);
        tr.appendChild(td8);
        tr.appendChild(td9);
        tr.appendChild(td10);

        document.getElementById('reviews_list').appendChild(tr);
    }

    $('#reviews_tbl').DataTable({
        "order": [
            [3, "asc"]
        ],
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": true,
    });

}
<style>
    ul li {list-style:circle;}
</style>
<h3 class="mt-5 text-center">Changelog</h3>
<div class="container" >
    <div class="page-wrapper">
        <h2>Version: 0.0.0.9 (30/11/2018):</h2>
        <div class="row">
            <div class="col-md-12">
                <h3>General:</h3>
                <ul>
                    <li> Quote Date Migrated to Franchisor <span class="text-success">(Completed)</span>  </li>
                    <li> Task 43 : Fixes and Feedback : Points Above General Questions Completed i.e. Fields readonly and request <span class="text-success">(Completed)</span>  </li>
                    <li> All latest changes of franchisor is live on /franchisor </li> 
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.8 (29/11/2018):</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Task 40 : Manage Proposal Scheduling Stages Added <span class="text-success">(Completed)</span>  </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li> Task 40 : Scheduling Jobs <span class="text-danger">(Under Development)</span> <br/> Ref:<a href="https://solarrunapp.com.au/staging/franchisor/admin/franchise/job/schedule"> https://solarrunapp.com.au/staging/franchisor/admin/franchise/job/schedule </a> </li>
                </ul>
            </div>
            
        </div>
        <br/>
        <h2>Version: 0.0.0.7 (28/11/2018):</h2>
        <div class="row">
            <div class="col-md-12">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Phone Lead Allocation page in /Franchisor at Franchisor level Added <span class="text-success">(Completed)</span> <br/> Ref:<a href="https://solarrunapp.com.au/staging/franchisor/admin/search/manage"> https://solarrunapp.com.au/staging/franchisor/admin/search/manage </a> </li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.6 (27/11/2018):</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Lead Management Module Completed with Activity/Note <span class="text-success">(Completed)</span></li>
                    <li> Lead Management Module Added Mail Functionality when a franchise is assigned <span class="text-success">(Completed)</span></li>
                </ul>
            </div>
             <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li> Lead Management Module Completed and Added with Activity/Note and Status Update <span class="text-success">(Completed)</span></li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.5.3 (26/11/2018):</h2>
        <div class="row">
            <div class="col-md-12">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Near Map Api Management Franchise wise added. </li>
                    <li> Lead creation module completed </li>
                    <li> Lead assign to franchise module completed. </li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.5.2 (24/11/2018):</h2>
        <div class="row">
            <div class="col-md-12">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Lead creation module added </li>
                    <li> Lead assign to franchise. <span class="text-danger">(Under Development)</span></li>
                    <li> Service Category Module Added. </li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.5.1 (Upcoming):</h2>
        <div class="row">
            <div class="col-md-12">
                <h3>Franchisor:</h3>
                <ul>
                    <li> Lead is received from the web site <span class="text-danger">(Upcoming)</span></li>
                    <li> Lead is automatically allocated to a Franchisee base on existing code. <span class="text-danger">(Upcoming)</span></li>
                    <li> Franchisee allocates lead to an agent. <span class="text-danger">(Upcoming)</span></li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.5 (22/11/2018): <span class="text-success"><a href="https://projects.dotsquares.com/files/view/project/9581/file/421861/Migrating%20functionality%20to%20franchisor.docx">(Roadmap Shared Doc Finished) </a></span></h2>
        <div class="row">
            <div class="col-md-4">
                <h3>Franchisor:</h3>
                <ul>
                    <li>Franchisor Proposal Module Added <span class="text-success">(Completed)</span></li>
                    <li>Franchisor Proposal Pdf Generation <span class="text-success">(Completed)</span></li>
                    <li>Franchisor Report Module Added </li>
                    <li>Franchisor Dashboard Module Added </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>Franchise:</h3>
                <ul>
                    <li>Franchise Proposal Module Added <span class="text-success">(Completed)</span></li>
                    <li>Franchise Proposal Pdf Generation <span class="text-success">(Completed)</span></li>
                    <li>Franchise Report Module Added </li>
                    <li>Franchise Dashboard Module Added </li>
                    <li>Franchise Settings Module Added </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>Franchise Agent:</h3>
                <ul>
                    <li>Franchise Agent General Module Added </li>
                    <li>Franchise Agent Customer Module Added </li>
                    <li>Franchise Agent Proposal Module Added <span class="text-success">(Completed)</span></li>
                    <li>Franchise Agent Dashboard Module Added </li>
                    <li>Franchise Agent Settings Module Added </li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.4 (21/11/2018):</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li>Franchisor Proposal Module Added <span class="text-warning">(Beta might need few more test's) </span></li>
                    <li>Franchisor Proposal Pdf Generation <span class="text-warning">(Under Development) </span></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li>Franchise Proposal Module Added <span class="text-warning">(Beta might need few more test's)</span></li>
                    <li>Franchise Proposal Pdf Generation <span class="text-warning">(Under Development) </span></li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.3 (20/11/2018):</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li>Franchisor Product Module Added</li>
                    <li>Franchisor Product Type Module Added</li>
                    <li>Franchisor Proposal Module Started <span class="text-warning">(Under Development) </span></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li>Franchise Proposal Module Started <span class="text-warning">(Under Development) </span></li>
                    <li>Franchise Agent Module Removed Certain Fields in Form</li>
                </ul>
            </div>
        </div>
        <br/>
        <h2>Version: 0.0.0.2 (19/11/2018):</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li>Franchisor Customer Module Added</li>
                    <li>Franchisor Services Module Added</li>
                    <li>Franchisor Markup Module Added</li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li>Franchise User(Agent) Module Added</li>
                    <li>Franchise Customer Module Added</li>
                    <li>Franchise Services Module Added</li>
                    <li>Franchise Markup Module Added</li>
                </ul>
            </div>
        </div>
         <br/>
        <h2>Version: 0.0.0.1 (16/11/2018):</h2>    
        <div class="row">
            <div class="col-md-6">
                <h3>Franchisor:</h3>
                <ul>
                    <li>Franchisor Franchise Module Added</li>
                    <li>Franchisor Role Module Added</li>
                    <li>Franchisor General Module Added</li>
                </ul>
            </div>
            <div class="col-md-6">
                <h3>Franchise:</h3>
                <ul>
                    <li>Franchise General Module Added</li>
                </ul>
            </div>
        </div>

    </div>
</div>
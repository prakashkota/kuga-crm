<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
	}
	
	.page9 .back-bg {
		background-color: #FFFFFF;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page9  .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 654px;
		padding-top: 30px;
	}
	
	.page9  .top hr {
		width: 65%;
		height: 1px;
		margin: -24px 0px 0px 52px;
		background-color: #D01E2A;
	}
	
	.page9  .text_color {
		color: #D01E2A;
	}
	
	.page9  .title {
		margin-top: -50px;
	}
	
	.page9  .title h1 {
		padding-left: 14px;
		margin-left: 70px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
	}
	
	.page9  .subtitle h5 {
		font-size: 18px;
		margin-left: 70px;
		color: #D01E2A;
		margin-bottom: 0px;
	}
	
	.page9  ul.b {
		list-style-type: square;
		color: #7D7D7D;
		padding-bottom: 45px;
	}
	
	.page9  ul li {
		line-height: 2.5;
		margin-left: 48px;
		font-size: 18px;
		color: black;
	}
	
	.page9  .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: 30px;
	}
	
	.page9  .image img {
		margin-top: 40px;
		width: 910px;
	}
	
	.page9  .imagee {
		margin-bottom: -90px;
	}
	
	.page9  .imagee img {
		margin-left: 613px;
		width: 250px;
		margin-top: -18px;
	}
	</style>
</head>

<body>
	<div class="back-bg page9">
		<div class="top">
			<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
				<hr>
			</div>
			<div class="title">
				<h1> TURNKEY  <span class="text_color"> PROJECT</span></h1>
				
			</div>
			<div class="image">
				<img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0009.jpg'); ?>">
			</div>
			
			<div class="subtitle">
				<h5>Warranties</h5>
				<ul class="b">
				  <li>10 years product warranty on Inverters</li>
				  <li>25 years warranty on PV modules performance</li>
				  <li>12 years warranty on Pv panels</li>
				  <li>10 years warranty on all workmanship</li>
				</ul>
			</div>
			<div class="imagee">
				<a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a>
			</div>
			<div class="footer"></div>
		</div>
		
	</body>
</html>

var lead_calendar_manager = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.is_lead_allocation_sales_rep = (options.is_lead_allocation_sales_rep) ? options.is_lead_allocation_sales_rep : '';
    this.filters = '';
    this.initial_limit = 30;
    this.limit = 100;
    this.page = 1;
    this.loading = false;

    self.fetch_lead_data();

}

lead_calendar_manager.prototype.fetch_lead_data = function(){
    var self = this;
     $('#calendar').fullCalendar({
        height: 'auto',
        header: {
            center: 'title',
            right: 'prev,next,agendaDay,eventWeekDay'
        },
        views: {
            eventWeekDay: {
                type: 'agenda',
                duration: {days: 7},
                eventLimit: 6
            }
        },
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        navLinks: false,
        eventClick: function (event, jsEvent, view) {
            var lead_data = event.data;
            var url = base_url + 'admin/lead/edit/' + lead_data.uuid;
            var redirectWindow = window.open(url, '_blank');
            redirectWindow.location;
        },
       events: function(start, end, timezone, callback) {
        $.ajax({
            url: base_url + "admin/lead/fetch_lead_stage_data",
            type: 'get',
            dataType: 'json',
            data:{
               'view' : 'calendar',
               'start_date': start.format(),
               'end_date': end.format()
            },
            beforeSend: function(){
                $('#loader').html(createSpinner(''));
            },
            success: function(res){
                $('#loader').html('');
                var data = res.lead_data;
                var events = [];
                for (var i = 0; i < data.length; i++) {
                    var event_obj = {};
                    event_obj.title = ' ';
                    event_obj.title += data[i].company_name;
                    //event_obj.className = 'event_' + data[i].activity_type;
                    event_obj.className += ' calendar_grid-item--' + self.handle_color(parseInt(data[i].lead_stage));

                    event_obj.start = moment(data[i].created_at).format('YYYY-MM-DD HH:mm:ss');

                    event_obj.data = data[i];
                    //event_obj.is_completed = data[i].status;
                    //event_obj.is_google_synced = (data[i].gcal_event_id && data[i].gcal_event_id != null) ? true : false;
                    events.push(event_obj);
                }
                toastr['info'](events.length + ' leads found.');
                callback(events);
            },
            error: function() {
              toastr['error']('There was an error while fetching lead data!');
            }
        });
        }
    });
};

lead_calendar_manager.prototype.handle_color = function (lead_stage) {
    if (lead_stage > 0 && lead_stage < 3) {
        return 'yellow';
    } else if (lead_stage > 2 && lead_stage < 8) {
        return 'grey';
    } else if (lead_stage > 7 && lead_stage < 10) {
        return 'amber';
    } else if (lead_stage == 10) {
        return 'blue';
    } else if (lead_stage == 11) {
        return 'green';
    } else if (lead_stage == 12) {
        return 'red';
    }
};

lead_calendar_manager.prototype.handle_calendar_view = function (data) {
    var self = this;
    var events = [];
    for (var i = 0; i < data.length; i++) {
        var event_obj = {};
        event_obj.title = ' ';
        event_obj.title += data[i].company_name;
        //event_obj.className = 'event_' + data[i].activity_type;
        event_obj.className += ' calendar_grid-item--' + self.handle_color(parseInt(data[i].lead_stage));

        event_obj.start = moment(data[i].created_at).format('YYYY-MM-DD HH:mm:ss');

        event_obj.data = data[i];
        //event_obj.is_completed = data[i].status;
        //event_obj.is_google_synced = (data[i].gcal_event_id && data[i].gcal_event_id != null) ? true : false;
        events.push(event_obj);
    }

    $('#calendar').fullCalendar({

        height: 'auto',
        defaultView: 'eventWeekDay',
        groupByResource: true,
        customButtons: {
            eventPhone: {
                text: 'Phone',
                click: function () {
                    $('.event_Phone').toggle();
                }
            },
            eventVisit: {
                text: 'Site Visit',
                click: function () {
                    $('.event_Visit').toggle();
                }
            },
            eventEmail: {
                text: 'Email',
                click: function () {
                    $('.event_Email').toggle();
                }
            }
        },
        header: {
            left: 'eventPhone,eventVisit,eventEmail',
            center: 'title',
            right: 'prev,next,agendaDay,eventWeekDay'
        },
        views: {
            eventWeekDay: {
                type: 'agenda',
                duration: {days: 7},
                eventLimit: 6
            }
        },
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        navLinks: false,
        events: events,
        eventRender: function (event, element) {
            if (event.icon) {
                //element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i> ");
            }

            if (event.is_google_synced) {
                element.find(".fc-time").append('<i class="fa fa-google" aria-hidden="true" style="float:right; margin-left:5px;"></i>');
            }

            if (event.is_completed == '1') {
                element.find(".fc-time").append("<span style='float:right;'> <i class='fa fa-check-circle' style='color:green; font-size:12px;'></i> </span>");
            }
        },
        eventClick: function (event, jsEvent, view) {
            var activity_data = event.data;
            console.log(event);
        },
    });
    $('[data-toggle="tooltip"]').tooltip();
}
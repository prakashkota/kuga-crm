<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Product Controller
 * @version 1.0
 */
class SolarProduct extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("components");
        $this->load->library("pdf");
        $this->load->model("Product_Model", "product");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Proposal_Model", "proposal");
        if (!$this->aauth->is_loggedin()) {
            //redirect('admin/login'); proposal_draft_pdf_download
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function manage() {
        $data = array();
        
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restricted Access.</div>');
            redirect('admin/dashboard');
        }
        
        //Restricitng and Fetching of Data based on user logged In group
        $group = $this->input->get('group');
        if (isset($group) && $group != '') {
            $data['meta_title'] = 'Manage Franchise Product';
            if (!$this->aauth->is_member($group) && !$this->aauth->is_member('Admin')) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
                redirect($this->redirect_url);
            }
            if ($this->aauth->is_member('Admin')) {
                $product_list = $this->product->get_products_list_by_roles(FALSE);
            } else {
                $product_list = $this->product->get_products_list_by_roles($group, $this->aauth->get_user()->id);
            }
        } else {
            $data['meta_title'] = 'Manage Product';
            if ($this->aauth->is_member('Admin')) {
                $product_list = $this->product->get_products_list_by_roles(FALSE);
            } else if (!$this->aauth->is_member('Admin')) {
                $product_list = $this->product->get_products_list_by_roles(FALSE);
                //$group_id = $this->aauth->get_user_groups()[0]->group_id;
                //$product_list = $this->product->get_products_list_by_roles($group_id, $this->aauth->get_user()->id);
            }
        }
        
        $data['product_types'] = $this->common->fetch_where('tbl_product_type','*',array('status' => '1'));
        $data['product_list'] = $product_list;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_residential_solar_manage');
        $this->load->view('partials/footer');
    }

    public function add() {

        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restricted Access.</div>');
            redirect('admin/dashboard');
        }
        
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                unset($insert_data['product_variant']);
                $insert_data['user_id'] = $this->aauth->get_user()->id;
                $insert_data['created_at'] = date('Y-m-d H:i:s');

                //Insert Product Item
                $this->common->insert_data('tbl_products', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $variant_insert_data = array();
                    $variant_data = $this->input->post('product_variant');
                    if (!empty($variant_data)) {
                        foreach ($variant_data['state'] as $key => $value) {
                            $variant_insert_data[$key]['product_id'] = $id;
                            $variant_insert_data[$key]['state'] = $variant_data['state'][$key];
                            $variant_insert_data[$key]['application'] = $variant_data['application'][$key];
                            $variant_insert_data[$key]['cost_price'] = $variant_data['cost_price'][$key];
                            $variant_insert_data[$key]['is_price_set'] = $variant_data['is_price_set'][$key];
                            $variant_insert_data[$key]['amount'] = $variant_data['amount'][$key];
                            $variant_insert_data[$key]['additional_margin'] = $variant_data['additional_margin'][$key];
                        }
                        $this->common->insert_batch('tbl_product_variants', $variant_insert_data);
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product created successfuly.</div>');
                    redirect('admin/product/residential_solar/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_type'] = $this->common->fetch_where('tbl_product_type', '*', NULL);
        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_residential_solar_form');
        $this->load->view('partials/footer');
    }

    public function edit($id) {

        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restricted Access.</div>');
            redirect('admin/dashboard');
        }
        
        //Restricitng and Fetching of Data based on user logged In group   
        $product_data = $this->common->fetch_where('tbl_products', '*', array('id' => $id));

        if (empty($product_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Product found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        /**if ($this->aauth->get_user()->id != $product_data[0]['user_id'] && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }*/


        $data = array();

        if (!empty($product_data)) {
            $data['product_data'] = $product_data[0];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('name', 'Product Name', 'required');
            $this->form_validation->set_rules('type_id', 'Product Type', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                unset($update_data['product_variant']);
                $update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Product Item
                $this->common->update_data('tbl_products', ['id' => $id], $update_data);

                if ($id) {
                    $product_variant_update_data = array();
                    $product_variant_data = $this->input->post('product_variant');
                    //Bulk Update Data
                    $pvid_item_data = $this->common->fetch_where('tbl_product_variants', '*', ['product_id' => $id]);
                    $count = count($pvid_item_data);
                    if ($count > 0) {
                        for ($i = 0; $i < $count; $i++) {
                            $product_variant_update_data[$i]['state'] = $product_variant_data['state'][$i];
                            $product_variant_update_data[$i]['application'] = $product_variant_data['application'][$i];
                            $product_variant_update_data[$i]['cost_price'] = $product_variant_data['cost_price'][$i];
                            $product_variant_update_data[$i]['is_price_set'] = $product_variant_data['is_price_set'][$i];
                            $product_variant_update_data[$i]['amount'] = $product_variant_data['amount'][$i];
                            $product_variant_update_data[$i]['additional_margin'] = $product_variant_data['additional_margin'][$i];
                            if ($pvid_item_data[$i]['cost_price'] != $product_variant_data['cost_price'][$i]) {
                                $product_variant_update_data[$i]['last_updated_price'] = $pvid_item_data[$i]['cost_price'];
                            }
                            $product_variant_update_data_cond[$i]['id'] = $pvid_item_data[$i]['id'];
                            $this->common->update_data('tbl_product_variants', $product_variant_update_data_cond[$i], $product_variant_update_data[$i]);
                        }
                    }

                    //Bulk Insert Data
                    for ($j = $count, $k = 0; $j < count($product_variant_data['state']); $j++, $k++) {
                        $product_variant_insert_data[$k]['product_id'] = $id;
                        $product_variant_insert_data[$k]['state'] = $product_variant_data['state'][$j];
                        $product_variant_insert_data[$k]['application'] = $product_variant_data['application'][$j];
                        $product_variant_insert_data[$k]['cost_price'] = $product_variant_data['cost_price'][$j];
                        $product_variant_insert_data[$k]['is_price_set'] = $product_variant_data['is_price_set'][$j];
                        $product_variant_insert_data[$k]['amount'] = $product_variant_data['amount'][$j];
                        $product_variant_insert_data[$k]['additional_margin'] = $product_variant_data['additional_margin'][$j];
                        $stat = $this->common->insert_data('tbl_product_variants', $product_variant_insert_data[$k]);
                    }

                    //Check if delete Required on less Variant data thean already stored
                    if ($count > count($product_variant_data['state'])) {
                        for ($l = count($product_variant_data['state']), $m = 0; $l < $count; $l++, $m++) {
                            $product_variant_cond[$m]['id'] = $pvid_item_data[$l]['id'];
                            $this->common->delete_data('tbl_product_variants', $product_variant_cond[$m]);
                        }
                    }

                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product updated successfuly.</div>');
                    redirect('admin/product/residential_solar/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['product_data'] = $this->input->post();
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['product_type'] = $this->common->fetch_where('tbl_product_type', '*', NULL);
        $data['states'] = array_column($states, 'state_postal');
        $data['product_application'] = unserialize(product_application);
        $data['pid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_residential_solar_form');
        $this->load->view('partials/footer');
    }
    
    public function delete($id = FALSE) {
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Only Admin is allowed to delete products.</div>');
            redirect('admin/dashboard');
        }

        if ($id) {
            $products = $this->common->fetch_where('tbl_products', '*', array('id' => $id));
            if (empty($products)) {
                redirect('admin/dashboard');
            } else {
                $stat = $this->common->update_data('tbl_products',array('id' => $id),array('status' => 0));
                if ($stat) {
                    if($stat == -1){
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Product cannot be deleted it is linked to some proposal.</div>');
                    }else{
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Product and Its Variants Deleted Successfuly.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
            redirect('admin/product/residential_solar/manage');
        }
    }

    public function fetch_product_variant_data() {
        $data = array();
        $data['success'] = FALSE;
        $id = $this->input->get('product_id');
        if (isset($id) && $id != '') {
            $product_variant_data = $this->common->fetch_where('tbl_product_variants', '*', array('product_id' => $id));
            $data['product_variant_data'] = $product_variant_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function import_csv() {
        $data = array();

        if (!$this->aauth->is_member('Admin')) {
            $data['success'] = FALSE;
            $data['status'] = 'Only Admin is allowed to import porduct csv';
            echo json_encode($data);
            die;
        }

        if (!empty($this->input->post())) {
            $action = $this->input->post('action');
            switch ($action) {
                case 'import_product_csv':
                $data = $this->handle_product_csv_import();
                break;
                case 'import_product_variant_csv':
                $data = $this->handle_product_variant_csv_import();
                break;
                default :
                $data['success'] = FALSE;
                $data['status'] = 'Invalid Request';
                break;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function export_csv() {
        $data = array();

        if (!$this->aauth->is_member('Admin')) {
            $data['success'] = FALSE;
            $data['status'] = 'Only Admin is allowed to export porduct csv';
            echo json_encode($data);
            die;
        }

        $action = $this->input->get('action');
        if ($action == 'export_product_csv') {

            $products = $this->common->fetch_where('tbl_products', '*', NULL);
            $this->components->download_send_headers("product_export_" . date("Y-m-d") . ".csv");
            //Filter Some Columns to be not included 

            $new_products = array();
            foreach ($products as $key => $product) {
                unset($product['user_id'], $product['datasheets'], $product['last_updated_price'], $product['created_at'], $product['updated_at'], $product['size'], $product['status']);
                $new_products[$key] = $product;
            }
            echo $this->components->array2csv($new_products);
            die();
        } else if ('export_product_variant_csv') {
            $products = $this->common->fetch_where('tbl_product_variants', '*', NULL);
            $this->components->download_send_headers("product_variant_export_" . date("Y-m-d") . ".csv");
            //Filter Some Columns to be not included 
            $new_products = array();
            foreach ($products as $key => $product) {
                unset($product['last_updated_price']);
                $new_products[$key] = $product;
            }
            echo $this->components->array2csv($new_products);
            die();
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    private function handle_product_csv_import() {
        try {
            if (!isset($_FILES["product_csv_file"])) {
                $data['status'] = 'Please Select a CSV File';
                $data['success'] = FALSE;
                return $data;
            }
            $filename = $_FILES["product_csv_file"]["tmp_name"];
            if ($_FILES["product_csv_file"]["size"] > 0) {
                $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
                if (!in_array($_FILES['product_csv_file']['type'], $mimes)) {
                    $data['status'] = 'Invalid FIle type. Please Upload a CSV file.';
                    $data['success'] = FALSE;
                    return $data;
                }
                $file = fopen($filename, "r");
                ini_set('auto_detect_line_endings', true);
                $count = 0;
                $columns = [];
                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if ($count == 0) {
                        /*                         * $columns = explode(';', $getData[0]);
                          for ($i = 0; $i < count($columns); $i++) {
                          $columns1[$i] = str_replace('"', "", $columns[$i]);
                      } */
                      $columns1 = $getData;
                  } else {
                    /*                         * $explode_data = explode(';', $getData[0]); */
                    $user_id = $this->aauth->get_user()->id;
                    for ($i = 0; $i < count($getData); $i++) {
                            //$rows1[$i] = str_replace('"', "", $explode_data[$i]);
                        $rows[$i] = ($getData[$i] == '' || $getData[$i] == 'NULL') ? NULL : $getData[$i];
                    };
                        //First Check if product already present by name if , yes then update
                    $combined_data = array_combine($columns1, $rows);
                    $product = ($combined_data['id'] != '') ? $this->common->fetch_where('tbl_products', '*', ['id' => $combined_data['id']]) : [];
                    if (!empty($product)) {
                            //Here we only update already exisitng product.
                        $product_update_data = $combined_data;
                        $product_update_data['updated_at'] = date('Y-m-d H:i:s');
                        $result = $this->common->update_data('tbl_products', ['id' => $product[0]['id']], $product_update_data);
                    } else {
                            //Insert Product
                        $product_insert_data = $combined_data;
                            //$product_insert_data['cost_price'] = floatval(str_replace(',', '', $combined_data['cost_price']));
                            //$product_insert_data['amount'] = floatval(str_replace(',', '', $combined_data['amount']));
                        $product_insert_data['created_at'] = date('Y-m-d H:i:s');
                        $product_insert_data['user_id'] = $user_id;
                        $result = $this->common->insert_data('tbl_products', $product_insert_data);
                    }
                }
                $count++;
            }
            fclose($file);
            if (!isset($result)) {
                $data['status'] = 'CSV File Import Failed';
                $data['success'] = FALSE;
            } else {
                $data['status'] = 'CSV File has been successfully Imported';
                $data['success'] = TRUE;
            }
            return $data;
        }
    } catch (Exception $e) {
        $data['status'] = 'Looks like something wnt wrong while importing the csv';
        $data['success'] = FALSE;
        return $data;
    }
}

private function handle_product_variant_csv_import() {
    try {
        if (!isset($_FILES["product_variant_csv_file"])) {
            $data['status'] = 'Please Select a CSV File';
            $data['success'] = FALSE;
            return $data;
        }
        $filename = $_FILES["product_variant_csv_file"]["tmp_name"];
        if ($_FILES["product_variant_csv_file"]["size"] > 0) {
            $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
            if (!in_array($_FILES['product_variant_csv_file']['type'], $mimes)) {
                $data['status'] = 'Invalid FIle type. Please Upload a CSV file.';
                $data['success'] = FALSE;
                echo json_encode($data);
                die;
            }
            $file = fopen($filename, "r");
            ini_set('auto_detect_line_endings', true);
            $count = 0;
            $columns = [];
            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                if ($count == 0) {
                    $columns1 = $getData;
                } else {
                    /*                         * $explode_data = explode(';', $getData[0]); */
                    for ($i = 0; $i < count($getData); $i++) {
                            //$rows1[$i] = str_replace('"', "", $explode_data[$i]);
                        $rows[$i] = ($getData[$i] == '' || $getData[$i] == 'NULL') ? NULL : $getData[$i];
                    };

                        //First Check if product already present by name if , yes then update
                    $combined_data = array_combine($columns1, $rows);
                    if ($combined_data['product_id'] != '' && $combined_data['state'] != '' && $combined_data['id'] != '') {
                        $product_variant_update_data = $combined_data;
                        unset($product_variant_update_data['id']);
                        $product_variant_update_data['cost_price'] = floatval(str_replace(',', '', $combined_data['cost_price']));
                        $product_variant_update_data['is_price_set'] = $combined_data['is_price_set'];
                        $product_variant_update_data['amount'] = floatval(str_replace(',', '', $combined_data['amount']));
                        $product_variant_update_data['additional_margin'] = floatval(str_replace(',', '', $combined_data['additional_margin']));
                        $result = $this->common->update_data('tbl_product_variants', ['id' => $combined_data['id']], $product_variant_update_data);
                    } else if ($combined_data['product_id'] != '' && $combined_data['state'] != '' && $combined_data['id'] == '') {
                            //Insert into variant
                        $product_variant_insert_data = $combined_data;
                        $product_variant_insert_data['product_id'] = $combined_data['product_id'];
                        $product_variant_insert_data['cost_price'] = floatval(str_replace(',', '', $combined_data['cost_price']));
                        $product_variant_insert_data['is_price_set'] = $combined_data['is_price_set'];
                        $product_variant_insert_data['amount'] = floatval(str_replace(',', '', $combined_data['amount']));
                        $product_variant_insert_data['additional_margin'] = floatval(str_replace(',', '', $combined_data['additional_margin']));
                        $result = $this->common->insert_data('tbl_product_variants', $product_variant_insert_data);
                    }
                }
                $count++;
            }
            fclose($file);
            if (!isset($result)) {
                $data['status'] = 'CSV File Import Failed';
                $data['success'] = FALSE;
            } else {
                $data['status'] = 'CSV File has been successfully Imported';
                $data['success'] = TRUE;
            }
            return $data;
        }
    } catch (Exception $e) {
        $data['status'] = 'Looks like something wnt wrong while importing the csv';
        $data['success'] = FALSE;
        return $data;
    }
}

    /**
     * Product Type Related Functions 
     * */
    public function manage_product_type() {
        $data = array();
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect($this->redirect_url);
        }

        $product_type_list = $this->common->fetch_where('tbl_product_type', '*', NULL);
        $data['product_type_list'] = $product_type_list;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_manage');
        $this->load->view('partials/footer');
    }

    public function add_product_type() {

        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('typeName', 'Product Type Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data['typeName'] = $this->input->post('typeName');
                $insert_data['user_id'] = $this->aauth->get_user()->id;

                //Insert Product Type Item
                $this->common->insert_data('tbl_product_type', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product created successfuly.</div>');
                    redirect('admin/product-type/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the product. </div>');
                }
            } else {
                $data['typeName'] = $this->input->post('typeName');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_form');
        $this->load->view('partials/footer');
    }

    public function edit_product_type($id) {

        $product_type_data = $this->common->fetch_where('tbl_product_type', '*', array('typeId' => $id));

        if (empty($product_type_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Data found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        if ($this->aauth->get_user()->id != $product_type_data[0]['user_id'] && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }


        $data = array();

        if (!empty($product_type_data)) {
            $data['typeName'] = $product_type_data[0]['typeName'];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('typeName', 'Product Type Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data['typeName'] = $this->input->post('typeName');
                $stat = $this->common->update_data('tbl_product_type', array('typeId' => $id), $update_data);
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Product type updated successfuly.</div>');
                    redirect('admin/product-type/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the data. </div>');
                }
            } else {
                $data['typeName'] = $this->input->post('typeName');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('product/product_type_form');
        $this->load->view('partials/footer');
    }

    public function fetch_item_data() {
        $data = array();
        $data['inverter'] = array();
        $data['panel'] = array();
        $data['battery'] = array();
        if (!empty($this->input->get())) {
            $cust_id = $this->input->get('cust_id');
            $customer_data = $this->customer->get_customer_by_cond($cust_id, FALSE,FALSE);
            if (!empty($customer_data)) {
                $data['success'] = TRUE;
                $state = $customer_data['state_postal'];
                $cond1 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=1";
                $cond2 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=2";
                $cond3 = " WHERE tpv.state IN ('All','" . $state . "') AND prd.type_id=3";
                $inverter = $this->product->get_item_data_by_cond($cond1);
                $panel = $this->product->get_item_data_by_cond($cond2);
                $battery = $this->product->get_item_data_by_cond($cond3);
                $data['inverter'] = $inverter;
                $data['panel'] = $panel;
                $data['battery'] = $battery;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function fetch_racking_item_data() {
        $data = array();
        $data['racking_data'] = array();
        $data['success'] = TRUE;
        if (!empty($this->input->post())) {
            $data['racking_data'] = $this->product->get_racking_item_data($this->input->post());
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }
    
    public function fetch_additional_item_data() {
        $data = array();
        $data['products'] = array();
        if (!empty($this->input->get())) {
            $data['success'] = TRUE;
            $type_id = $this->input->get('type_id');
            $cust_id = $this->input->get('cust_id');
            $customer_data = $this->customer->get_customer_by_cond($cust_id, FALSE);
            if (!empty($customer_data)) {
                $state = $customer_data['state_postal'];
                $cond = " WHERE prd.type_id='" . $type_id . "' AND tpv.state IN ('All','" . $state . "')";
                $products = $this->product->get_item_data_by_cond($cond);
                $data['products'] = $products;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }


    public function fetch_postcode() {
        if (!empty($this->input->get())) {
            $postcode = $this->input->get('postcode');
            $postcodes = $this->common->fetch_where('tbl_postcodes', '*', array('postcode' => $postcode));
            $data['success'] = TRUE;
            $data['postcode'] = (!empty($postcodes)) ? $postcodes[0] : [];
            $data['solar_area'] = '';
            if(!empty($data['postcode'])){
                $region = $this->components->get_region_by_postcode($data['postcode']['postcode']);
                $data['solar_area'] = $this->common->fetch_cell('tbl_solar_areas', 'areaId', array('state_id' => $region));
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }


    private function get_savings_calculation_data_from_kugacrm($saving_caluclation_data) {
        $ch                      = curl_init();
        $saving_caluclation_data = (array) json_decode($saving_caluclation_data);

        if (empty($saving_caluclation_data)) {
            return array();
        }

        $electricity_rate = 0;
        if (isset($saving_caluclation_data['electricity_rate'])) {
            $electricity_rate = $saving_caluclation_data['electricity_rate'];
        } else if (isset($saving_caluclation_data['flat_rate']) && $saving_caluclation_data['flat_rate'] > 0) {
            $electricity_rate = $saving_caluclation_data['flat_rate'];
        } else if (isset($saving_caluclation_data['tr_rate'])) {
            $saving_caluclation_data['tr_rate'] = (array) $saving_caluclation_data['tr_rate'];
            $electricity_rate                   = array_sum($saving_caluclation_data['tr_rate']);
        }

        if (!isset($saving_caluclation_data['total_system_size'])) {
            return array();
        }
        if (!isset($saving_caluclation_data['feed_in_tariff'])) {
            return array();
        }
        if (!isset($saving_caluclation_data['export'])) {
            return array();
        }
        if ($electricity_rate == 0) {
            return array();
        }

        $total_system_size = (isset($saving_caluclation_data['total_system_size']) && $saving_caluclation_data['total_system_size'] > 0) ? $saving_caluclation_data['total_system_size'] : 0;
        $feed_in_tariff    = (isset($saving_caluclation_data['feed_in_tariff']) && $saving_caluclation_data['feed_in_tariff'] > 0) ? $saving_caluclation_data['feed_in_tariff'] : 0;
        $export            = (isset($saving_caluclation_data['export']) && $saving_caluclation_data['export'] > 0) ? $saving_caluclation_data['export'] : 0;

        $post_data = array(
            'total_system_size' => $total_system_size,
            'electricity_rate'  => $electricity_rate,
            'feed_in_tariff'    => $feed_in_tariff,
            'export'            => $saving_caluclation_data['export'],
            'custom_profile_id' => 1,
        );

        if (isset($saving_caluclation_data['postcode'])) {
            $post_data['postcode'] = $saving_caluclation_data['postcode'];
        } else if (isset($saving_caluclation_data['sc_sb_area'])) {
            $post_data['area_id'] = $saving_caluclation_data['sc_sb_area'];
        }
        //print_r($post_data);die;
        curl_setopt($ch, CURLOPT_URL, "https://kugacrm.com.au/admin/proposal/api_calculate_solar_savings?" . http_build_query($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $content = trim(curl_exec($ch));
        curl_close($ch);

        return (array) json_decode($content);
    }

    public function get_savings_calculation_data() {
        $data = array();
        if (!empty($this->input->post())) {
            $sc_type           = $this->input->post('sc_type');
            $lead_id           = $this->input->post('lead_id');
            $system_size       = $this->input->post('total_system_size');
            $system_size       = (isset($system_size) && $system_size != '') ? $system_size : 0;
            $postcode          = $this->input->post('postcode');
            $feed_in_tariff    = $this->input->post('feed_in_tariff');
            $percentage_export = $this->input->post('export');
            $electricity_rate  = $this->input->post('electricity_rate');
            $tariff_type       = $this->input->post('tariff_type');
            $flat_rate         = $this->input->post('flat_rate');
            $battery_size      = $this->input->post('battery_size');
            $battery_size      = (isset($battery_size) && $battery_size != '') ? $battery_size : 0;
            $tr_rate           = $this->input->post('tr_rate');
            $er_rate           = $this->input->post('er_rate');
            $panel_data        = $this->input->post('panel_data');


            if (isset($postcode) && $postcode == '') {
                $data['success'] = FALSE;
                echo json_encode($data);
                die;
            }

            if (!isset($panel_data)) {
                $panel_data = $this->common->fetch_cell('tbl_proposal', 'near_map_data', ['lead_id' => $lead_id]);
            }
            
            

            if (isset($electricity_rate) && $electricity_rate != '') {
                $proposal_electricity_rate = $electricity_rate;
            } else if (isset($flat_rate) && $flat_rate > 0) {
                $proposal_electricity_rate = $flat_rate;
            } else if (isset($tr_rate) && $tr_rate > 0) {
                $tr_rate                   = (array) $tr_rate;
                $proposal_electricity_rate = array_sum($tr_rate);
            }


            if ($sc_type == '1') {
                $state_id                           = $this->components->get_region_by_postcode($postcode);
                $solar_area_data                    = $this->common->fetch_row('tbl_solar_areas', 'areaId,kwh_day', array('state_id' => $state_id));
                $kwh_day                            = $solar_area_data['kwh_day'];
                $data['kwh_day']                    = (float) $kwh_day;
                //$data['solar_annual_production'] = $solar_annual_production = (int) ($system_size * $kwh_day * 365);
                $proposal_data                      = [];
                $proposal_data                      = $this->input->post();
                $proposal_data['area_id']           = $solar_area_data['areaId'];
                $proposal_data['electricity_rate']  = $electricity_rate;
                $proposal_data['custom_profile_id'] = 3;
                $proposal_data['panel_data']        = $panel_data;
                $proposal_data['kwh_day']           = $kwh_day;
                if (!empty($panel_data)) {
                    $panel_data = (array) json_decode($panel_data);
                    if(!empty($panel_data['tiles'])){
                        $total_solar_production_daily = $this->calculate_total_solar_production_daily($proposal_data);
                    }else{
                        $total_solar_production_daily = (int) ($system_size * $kwh_day);
                    }
                } else {
                    $total_solar_production_daily = (int) ($system_size * $kwh_day);
                }
                $data['solar_annual_production'] = $solar_annual_production         = (int) ($total_solar_production_daily * 365);
				
				$shading = 0;
				if(isset($panel_data['shading'])){
					$shading = $panel_data['shading'];
				}
				if(isset($shading) && $shading > 0){
					$shading_ratio = 100 - $shading;
					$shading_ratio = $shading_ratio / 100;
					if($shading_ratio > 0){
						$data['solar_annual_production'] = $solar_annual_production = $data['solar_annual_production'] * $shading_ratio;
					}
				}
				
				$data['shading']	= $shading;
				
                $data['usage_savings']           = $usage_savings                   = (float) ($solar_annual_production / 12) * (1 - ((float) ($percentage_export) / 100)) * (float) ($electricity_rate);
                $data['export_credit']           = $export_credit                   = (float) ($solar_annual_production / 12) * ((float) ($percentage_export) / 100) * (float) ($feed_in_tariff);
                $data['total_savings']           = $total_savings                   = (float) ($usage_savings) + (float) ($export_credit);
                $data['annual_savings']          = number_format((float) ($total_savings) * 12, 2, '.', '');
            } else if ($sc_type == '2') {
                $sc_sb_area                         = $this->input->post('sc_sb_area');
                $kwh_day                            = $this->common->fetch_cell('tbl_solar_areas', 'kwh_day', array('areaId' => $sc_sb_area));
                $data['kwh_day']                    = (float) $kwh_day;
                //$data['solar_annual_production'] = $solar_annual_production = (int) ($system_size * $kwh_day * 365);
                $proposal_data                      = [];
                $proposal_data                      = $this->input->post();
                $proposal_data['area_id']           = $sc_sb_area;
                $proposal_data['electricity_rate']  = $electricity_rate;
                $proposal_data['custom_profile_id'] = 3;
                $proposal_data['panel_data']        = $panel_data;
                $proposal_data['kwh_day']           = $kwh_day;
                if (!empty($panel_data)) {
                    $panel_data = (array) json_decode($panel_data);
                    if(!empty($panel_data['tiles'])){
                        $total_solar_production_daily = $this->calculate_total_solar_production_daily($proposal_data);
                    }else{
                        $total_solar_production_daily = (int) ($system_size * $kwh_day);
                    }
                } else {
                    $total_solar_production_daily = (int) ($system_size * $kwh_day);
                }
                $data['solar_annual_production'] = $solar_annual_production         = (int) ($total_solar_production_daily * 365);
				
				
                if ($tariff_type == '1') {
                    //Flat Rate
                    $ms_ap = ($solar_annual_production / 12);
                    $ms_ap = (float) ($ms_ap);
                    $ms_us = (float) ($ms_ap) * (float) (1 - ((float) ($percentage_export) / 100)) * (float) ($flat_rate);
                    $ms_us = (float) ($ms_us);
                    $ms_ec = (float) ($ms_ap) * ((float) ($percentage_export) / 100) * (float) ($feed_in_tariff);
                    $ms_ec = (float) ($ms_ec);
                    $ms_ts = (float) ($ms_us) + (float) ($ms_ec);
                    $ms_ts = (float) ($ms_ts);
                    $ms_as = (float) ($ms_ts) * 12;
                    $ms_as = (float) ($ms_as);

                    $ms_ap_export       = ($ms_ap * (float) ($percentage_export) / 100);
                    $battery_size_month = ($battery_size * (float) (365 / 12));
                    $bs_ap              = ($ms_ap_export >= $battery_size_month) ? $battery_size_month : $ms_ap_export;
                    $bs_ap              = (float) ($bs_ap);
                    $bs_us              = (float) ($bs_ap) * (float) ($flat_rate);
                    $bs_us              = (float) ($bs_us);
                    $bs_ec              = 0;
                    $bs_ts              = (float) ($bs_us) + (float) ($bs_ec);
                    $bs_ts              = (float) ($bs_ts);
                    $bs_as              = (float) ($bs_ts) * 12;
                    $bs_as              = (float) ($bs_as);


                    $usage_savings          = ((float) ($ms_ap) * ((float) ($percentage_export) / 100) <= (float) ($bs_ap)) ? (float) ($ms_ap) : (float) ($ms_ap) * (float) (1 - ((float) ($percentage_export) / 100)) + (float) ($bs_ap);
                    $data['usage_savings']  = $usage_savings          = (float) ($usage_savings) * (float) ($flat_rate);
                    $sc_export_credit       = ((((float) ($ms_ap) * ((float) ($percentage_export) / 100)) - (float) ($bs_ap)) > 0) ? (((float) ($ms_ap) * ((float) ($percentage_export) / 100)) - (float) ($bs_ap)) : 0;
                    $data['export_credit']  = $sc_export_credit       = (float) ($sc_export_credit) * (float) ($feed_in_tariff);
                    $data['total_savings']  = $sc_total_savings       = (float) ($usage_savings) + (float) ($sc_export_credit);
                    $data['annual_savings'] = $sc_annual_savings      = (float) ($sc_total_savings) * 12;
                } else {
                    //TOU
                    $ms_ap               = ($solar_annual_production / 12);
                    $ms_ap               = (float) ($ms_ap);
                    $data['ms_ap']       = $ms_ap;
                    $total_rate_peak     = (float) ($tr_rate['peak'] / 100) * (float) ($er_rate['peak']);
                    $total_rate_off_peak = (float) ($tr_rate['off_peak'] / 100) * (float) ($er_rate['off_peak']);
                    $total_rate_shoulder = (float) ($tr_rate['shoulder'] / 100) * (float) ($er_rate['shoulder']);
                    $partial_ms_us       = (float) ($ms_ap) * (float) (1 - ((float) ($percentage_export) / 100));
                    $data['ms_us']       = $ms_us               = ((float) ($partial_ms_us) * (float) ($total_rate_peak)) + ((float) ($partial_ms_us) * (float) ($total_rate_off_peak)) + ((float) ($partial_ms_us) * (float) ($total_rate_shoulder));
                    $data['ms_ec']       = $ms_ec               = (float) ($ms_ap) * ((float) ($percentage_export) / 100) * (float) ($feed_in_tariff);
                    $data['ms_ts']       = $ms_ts               = (float) ($ms_us) + (float) ($ms_ec);
                    $data['ms_as']       = $ms_as               = (float) ($ms_ts) * 12;

                    $ms_ap_export       = ($ms_ap * (float) ($percentage_export) / 100);
                    $battery_size_month = ($battery_size * (float) (365 / 12));
                    $data['bs_ap']      = $bs_ap              = ($ms_ap_export >= $battery_size_month) ? $battery_size_month : $ms_ap_export;
                    $data['bs_us']      = $bs_us              = ((float) ($bs_ap) * (float) ($total_rate_peak)) + ((float) ($bs_ap) * (float) ($total_rate_off_peak));
                    $data['bs_ec']      = $bs_ec              = 0;
                    $data['bs_ts']      = $bs_ts              = (float) ($bs_us) + (float) ($bs_ec);
                    $data['bs_as']      = $bs_as              = (float) ($bs_ts) * 12;

                    $us1                    = ($ms_ap_export <= $bs_ap) ? $ms_ap * $total_rate_peak : ($partial_ms_us + $bs_ap) * $total_rate_peak;
                    $us2                    = ($ms_ap_export <= $bs_ap) ? $ms_ap * $total_rate_off_peak : ($partial_ms_us + $bs_ap) * $total_rate_off_peak;
                    $us3                    = ($ms_ap_export <= $bs_ap) ? $ms_ap * $total_rate_shoulder : ($partial_ms_us + $bs_ap) * $total_rate_shoulder;
                    $usage_savings          = (float) $us1 + (float) $us2 + (float) $us3;
                    $data['usage_savings']  = $usage_savings          = (float) ($usage_savings);
                    $sc_export_credit       = ((((float) ($ms_ap) * ((float) ($percentage_export) / 100)) - (float) ($bs_ap)) > 0) ? (((float) ($ms_ap) * ((float) ($percentage_export) / 100)) - (float) ($bs_ap)) : 0;
                    $data['export_credit']  = $sc_export_credit       = (float) ($sc_export_credit) * (float) ($feed_in_tariff);
                    $data['total_savings']  = $sc_total_savings       = (float) ($usage_savings) + (float) ($sc_export_credit);
                    $data['annual_savings'] = $sc_annual_savings      = (float) ($sc_total_savings) * 12;
                }
            }

            //Update data in proposal
            $post_data = $this->input->post();
            if (isset($post_data['panel_data'])) {
                unset($post_data['panel_data']);
                $update_data['near_map_data'] = json_encode($panel_data);
            }
            $sc_data                                   = array_merge($post_data, $data);
            $update_data['saving_calculation_details'] = json_encode($sc_data);

            $this->common->update_data('tbl_proposal', ['lead_id' => $lead_id], $update_data);
            $data['success'] = TRUE;
        } else {
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function calculate_total_solar_production_daily($proposal_data) {
        $data                         = array();
        $total_solar_production_daily = 0;
        $kwh_day                      = (isset($proposal_data['kwh_day']) && $proposal_data['kwh_day'] != '') ? $proposal_data['kwh_day'] : 0;
        $system_size                  = (isset($proposal_data['total_system_size']) && $proposal_data['total_system_size'] != '') ? $proposal_data['total_system_size'] : 0;
        $panel_data                   = (isset($proposal_data['panel_data']) && $proposal_data['panel_data'] != '') ? (array) json_decode($proposal_data['panel_data']) : [];
        //We Will have to decide here wether we want average or sum
        if (!empty($panel_data['tiles'])) {
            $panel_data['tiles']    = (array) $panel_data['tiles'];
            //echo "<pre>";
            //print_r($panel_data);die;
            $no_of_panels           = count($panel_data['tiles']);
            //echo $no_of_panels;die;
            $individual_system_size = ($system_size > 0 && $no_of_panels > 0) ? $system_size / $no_of_panels : 0;
            //echo $individual_system_size;die;
            for ($i = 0; $i < count($panel_data['tiles']); $i++) {
                $panel_data['tiles'][$i] = (array) $panel_data['tiles'][$i];
                $orientation             = is_numeric($panel_data['tiles'][$i]['sp_rotation']) ? $panel_data['tiles'][$i]['sp_rotation'] : 0;
                $orientation             = round($orientation / 10) * 10;
                $orientation             = ($orientation > 350) ? $orientation - 360 : $orientation;
                $tilt                    = is_numeric($panel_data['tiles'][$i]['sp_tilt']) ? $panel_data['tiles'][$i]['sp_tilt'] : 0;
                $watt                    = is_numeric($panel_data['tiles'][$i]['sp_watt']) ? $panel_data['tiles'][$i]['sp_watt'] : 0;
                $orientation_tilt        = $this->common->fetch_row('tbl_tilt_and_orientation_2020_data', '*', array('areaId' => $proposal_data['area_id'], 'orientationValue' => $orientation));
                if (!empty($orientation_tilt)) {
                    if ($tilt % 5 == 0) {
                        $orientation_tilt_val = isset($orientation_tilt['tilt' . $tilt]) ? $orientation_tilt['tilt' . $tilt] : 0;
                        $total_solar_production_daily += $individual_system_size * $orientation_tilt_val * $kwh_day;
                        //$total_solar_production_dailys[] = $orientation_tilt['tilt' . $tilt];
                    } else {
                        $x = 5; //Factor of 5 
                        $tilt1                 = (round($tilt) % $x === 0) ? round($tilt) : round(($tilt + $x/2 )/$x)*$x;
                        $tilt2                 = ($tilt1 > 0) ? $tilt1 - 5 : $tilt1;
                        $orientation_tilt_val1        = isset($orientation_tilt['tilt' . $tilt1]) ? $orientation_tilt['tilt' . $tilt1] : 0;
                        $orientation_tilt_val2        = isset($orientation_tilt['tilt' . $tilt2]) ? $orientation_tilt['tilt' . $tilt2] : 0;
                        $orientation_tilt_val         = ($orientation_tilt_val1 + $orientation_tilt_val2) / 2;
                        $total_solar_production_daily += $individual_system_size * $orientation_tilt_val * $kwh_day;
                        //$total_solar_production_dailys[] = $orientation_tilt_val;
                    }
                }
            }
        }
        //print_r($total_solar_production_dailys);die;
        return $total_solar_production_daily;
    }


    public function proposal_save() {
        $data = array();
        if (!empty($this->input->post())) {
            $proposal_item_data         = array();
            $proposal_uuid              = $this->input->post('uuid');
            $lead_id                    = $this->input->post('lead_id');
            $prd_panel                  = $this->input->post('prd_panel');
            $proposal_data              = $this->input->post('proposal');
            $saving_calculation_details = $this->input->post('saving_calculation_details');

            $proposal = $this->common->fetch_where('tbl_proposal', '*', ['uuid' => $proposal_uuid]);
            $agent_id = $this->aauth->get_user_id();

            if (!empty($proposal)) {
                //Convert base64img to png
                $img = explode(';', $proposal_data['image']);
                if ($proposal_data['image'] != '' && count($img) > 1) {
                    $image                  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $proposal_data['image']));
                    $newfilename            = $proposal_data['image'] = round(microtime(true)) . '.png';
                    file_put_contents('./assets/uploads/proposal_files/' . $newfilename, $image);
                    
                    //Resize and compress iamge
                    $this->load->library('image_lib');
                    $configer = array(
                        'image_library' => 'gd2',
                        'source_image' => FCPATH . 'assets/uploads/proposal_files/'.$newfilename,
                        'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                        'maintain_ratio' => FALSE,
                        'quality' => '100%', //tell CI to reduce the image quality and affect the image size
                        'width' => 650, //new size of image
                        'height' => 435, //new size of image
                    );

                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                }
                //update Proposal First
                unset($proposal_data['near_map_data']);
                $proposal_update                           = array();
                $proposal_update                           = $proposal_data;
                $proposal_update['updated_at']             = date('Y-m-d H:i:s');
                if(isset($proposal_data['roof_type'])){
                    $proposal_update['roof_type'] = implode(';', $proposal_data['roof_type']);
                }
                if(isset($proposal_data['roof_type_no_of_panels'])){
                    $proposal_update['roof_type_no_of_panels'] = implode(';', $proposal_data['roof_type_no_of_panels']);
                }
                $proposal_update['agent_id']               = (isset($agent_id) && $agent_id != '' ) ? $agent_id : ((isset($proposal[0]['agent_id']) && $proposal[0]['agent_id'] != '' && $proposal[0]['agent_id'] != 0) ? $proposal[0]['agent_id'] : $this->aauth->get_user()->id);
                $stat                                      = $this->common->update_data('tbl_proposal', ['uuid' => $proposal_uuid], $proposal_update);

                //Delete old item data
                $this->common->delete_data('tbl_proposal_data', ['proposal_id' => $proposal[0]['id']]);

                //Insert Proposal Item Data
                $stat1 = $this->handle_proposal_item_data($proposal[0]['id']);


                if ($stat && $stat1) {
                    $data['success'] = TRUE;
                    $data['status']  = 'Proposal Saved Successfully';
                } else {
                    $data['success'] = FALSE;
                    $data['status']  = 'Looks like something went wrong while saving the proposal';
                }
            } else {
                //Convert base64img to png
                $img = explode(';', $proposal_data['image']);
                if ($proposal_data['image'] != '' && count($img) > 1) {
                    $image                  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $proposal_data['image']));
                    $newfilename            = $proposal_data['image'] = round(microtime(true)) . '.png';
                    file_put_contents('./assets/uploads/proposal_files/' . $newfilename, $image);
                    
                    //Resize and compress iamge
                    $this->load->library('image_lib');
                    $configer = array(
                        'image_library' => 'gd2',
                        'source_image' => FCPATH . 'assets/uploads/proposal_files/'.$newfilename,
                        'create_thumb' => FALSE, //tell the CI do not create thumbnail on image
                        'maintain_ratio' => FALSE,
                        'quality' => '100%', //tell CI to reduce the image quality and affect the image size
                        'width' => 650, //new size of image
                        'height' => 435, //new size of image
                    );

                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                }
                //Create Proposal First
                unset($proposal_data['near_map_data']);
                $proposal_insert                           = array();
                $proposal_insert                           = $proposal_data;
                $proposal_insert['uuid']                   = $proposal_uuid;
                $proposal_insert['lead_id']                = $lead_id;
                $proposal_insert['created_at']             = date('Y-m-d H:i:s');
                if(isset($proposal_data['roof_type'])){
                    $proposal_insert['roof_type']              = implode(';', $proposal_data['roof_type']);
                }
                if(isset($proposal_data['roof_type_no_of_panels'])){
                    $proposal_insert['roof_type_no_of_panels'] = implode(';', $proposal_data['roof_type_no_of_panels']);
                }
                $proposal_insert['agent_id']               = (isset($agent_id) && $agent_id != '' ) ? $agent_id : $this->aauth->get_user()->id;
                $this->common->insert_data('tbl_proposal', $proposal_insert);
                $insert_id                                 = $this->common->insert_id();
                //Insert Proposal Item Data
                $stat                                      = $this->handle_proposal_item_data($insert_id);

                if ($stat) {
                    $data['success'] = TRUE;
                    $data['status']  = 'Proposal Saved Successfully';
                } else {
                    $data['success'] = FALSE;
                    $data['status']  = 'Looks like something went wrong while saving the proposal';
                }
            }
        } else {
            $data['success'] = FALSE;
            $data['status']  = 'Looks like something went wrong while saving the proposal';
        }
        echo json_encode($data);
        die;
    }

    public function handle_proposal_item_data($insert_id) {
        //Gather System Item Data
        $proposal_item_data                            = array();
        $count                                         = 0;
        $proposal_item_data[$count]['item_id']         = $this->input->post('prd_panel');
        $proposal_item_data[$count]['item_variant_id'] = $this->input->post('panel_variant_id');
        $proposal_item_data[$count]['proposal_id']     = $insert_id;
        $proposal_item_data[$count]['item_type']       = 1;
        $proposal_item_data[$count]['qty']             = 1;
        $proposal_item_data[$count]['custom_price']    = NULL;
        $proposal_item_data[$count]['final_price']     = NULL;
        $proposal_item_data[$count]['created_at']      = date('Y-m-d H:i:s');

        $count++;
        if ($this->input->post('prd_inverter') && $this->input->post('prd_inverter') != '' && $this->input->post('inverter_variant_id') != '') {
            $proposal_item_data[$count]['item_id']         = $this->input->post('prd_inverter');
            $proposal_item_data[$count]['item_variant_id'] = $this->input->post('inverter_variant_id');
            $proposal_item_data[$count]['proposal_id']     = $insert_id;
            $proposal_item_data[$count]['item_type']       = 1;
            $proposal_item_data[$count]['qty']             = 1;
            $proposal_item_data[$count]['custom_price']    = NULL;
            $proposal_item_data[$count]['final_price']     = NULL;
            $proposal_item_data[$count]['created_at']      = date('Y-m-d H:i:s');
            $count++;
        }


        if ($this->input->post('prd_battery') && $this->input->post('prd_battery') != '' && $this->input->post('battery_variant_id') != '') {
            $proposal_item_data[$count]['item_id']         = $this->input->post('prd_battery');
            $proposal_item_data[$count]['item_variant_id'] = $this->input->post('battery_variant_id');
            $proposal_item_data[$count]['proposal_id']     = $insert_id;
            $proposal_item_data[$count]['item_type']       = 1;
            $proposal_item_data[$count]['qty']             = 1;
            $proposal_item_data[$count]['custom_price']    = NULL;
            $proposal_item_data[$count]['final_price']     = NULL;
            $proposal_item_data[$count]['created_at']      = date('Y-m-d H:i:s');
            $count++;
        }

        //Gather Additional Item Data
        $additional_cost_item = $this->input->post('additional_cost_item');
        if (!empty($additional_cost_item)) {
            foreach ($additional_cost_item as $key => $value) {
                $proposal_item_data[$count]['item_id']         = $value;
                $proposal_item_data[$count]['item_variant_id'] = $this->input->post('additional_cost_item_variant')[$key];
                $proposal_item_data[$count]['proposal_id']     = $insert_id;
                $proposal_item_data[$count]['item_type']       = 2;
                $proposal_item_data[$count]['qty']             = $this->input->post('additional_cost_item_qty')[$key];
                $proposal_item_data[$count]['custom_price']    = $this->input->post('additional_cost_item_price')[$key];
                $proposal_item_data[$count]['final_price']     = $this->input->post('additional_cost_item_total')[$key];
                $proposal_item_data[$count]['created_at']      = date('Y-m-d H:i:s');
                $count++;
            }
        }

        //Gather Line Item Data
        $line_item = $this->input->post('line_item');
        if (!empty($line_item)) {
            foreach ($line_item as $key => $value) {
                $proposal_item_data[$count]['item_id']      = $value;
                $proposal_item_data[$count]['proposal_id']  = $insert_id;
                $proposal_item_data[$count]['item_type']    = 3;
                $proposal_item_data[$count]['qty']          = 1;
                $proposal_item_data[$count]['custom_price'] = $this->input->post('line_item_price')[$key];
                $proposal_item_data[$count]['final_price']  = $this->input->post('line_item_price')[$key];
                $proposal_item_data[$count]['created_at']   = date('Y-m-d H:i:s');
                $count++;
            }
        }

        $stat = FALSE;
        foreach ($proposal_item_data as $key => $value) {
            if($proposal_item_data[$key]['item_id']  != NULL && $proposal_item_data[$key]['item_id']  != ''){
                $stat = $this->common->insert_data('tbl_proposal_data', $proposal_item_data[$key]);
            }
        }
        return $stat;
    }

    public function proposal_load() {
        $data = array();
        if (!empty($this->input->post())) {
            $proposal_uuid      = $this->input->post('uuid');
            //Proposal
            $proposal           = $this->proposal->get_proposal_by_uuid($proposal_uuid);
            //Proposal Data
            $proposal_data      = $this->proposal->get_proposal_data_by_proposal_id($proposal['id']);
            //Line Item Data
            $proposal_line_data = $this->proposal->get_proposal_line_item_data_by_proposal_id($proposal['id']);

            $data['success']                        = TRUE;
            $proposal['saving_calculation_details'] = json_decode($proposal['saving_calculation_details']);
            $data['proposal']                       = $proposal;
            $data['proposal_data']                  = $proposal_data;
            $data['proposal_line_item_data']        = $proposal_line_data;
        } else {
            $data['success'] = FALSE;
            $data['success'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function proposal_pdf_download($id) {
        //Proposal
        $proposal = $this->proposal->get_proposal_by_uuid($id);

        //If Proposal Not Linked to lead then catch user and find its fracnhise
        //Find Fracnhise from propsal agent_id
        $owner = $this->common->fetch_where('aauth_user_to_user', 'owner_id', array('user_id' => $proposal['agent_id']));
        
        
            
        if (!empty($owner)) {
            //If already franchise get directly
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
            if(empty($franchise_data)){
                $franchise_data = $this->user->get_owner_list($owner[0]['owner_id']);
            }
        } else {
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
        }
       
        
        if (!empty($franchise_data)) {
            $franchise_data                     = $franchise_data[0];
            $proposal['full_name']              = $franchise_data['full_name'];
            $proposal['company_contact_no']     = $franchise_data['company_contact_no'];
            $proposal['franchise_company_name'] = $franchise_data['company_name'];
            $proposal['franchise_bsb']          = $franchise_data['company_bsb'];
            $proposal['franchise_account_name'] = $franchise_data['company_account_name'];
            $proposal['franchise_account_no']   = $franchise_data['company_account'];
            
            if($proposal['user_id'] != $franchise_data['user_id']){
                $proposal_user_details = $this->proposal->get_proposal_user_details_by_uuid($id);
                $proposal = array_merge($proposal,$proposal_user_details);
            }
        }
        
    
        $data['proposal']  = $proposal;
        //Customer Data
        $data['customer_data']  = $proposal;

        //echo "<pre>";
        //print_r($proposal);die;
        //Proposal Product Data
        $data['proposal_product_data'] = $proposal_product_data  = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 1);
        //Proposal Additional Data
        $data['proposal_additional_data'] = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 2);
        //Line Item Data
        $data['proposal_line_data']       = $this->proposal->get_proposal_line_item_data_by_proposal_id($proposal['id']);

        $data['saving_calculation_details'] = (array) json_decode($proposal['saving_calculation_details']);

        $savings_calculation_data = array();

        $savings_calculation_data = $this->get_savings_calculation_data_from_kugacrm($proposal['saving_calculation_details']);

        $data['savings_calculation_data'] = $savings_calculation_data;
		
		if(isset($data['saving_calculation_details']['postcode']) && !empty($data['saving_calculation_details']['postcode'])){
		    $postcodes = $this->common->fetch_where('tbl_postcodes', '*', array('postcode' => $data['saving_calculation_details']['postcode']));
    		if(!empty($postcodes) && $postcodes[0]['rating'] !=''){
    	        $data['saving_calculation_details']['postcode_rating'] = $postcodes[0]['rating']; 
    		}
		}
        
        $data['panel_data']    = array();
        $data['inverter_data'] = array();
        $data['battery_data']  = array();
        $inverter_attachment = $battery_attachment = $panel_attachment = '';
        foreach ($proposal_product_data as $key => $value) {
            if ($value['type_id'] == 1) {
                $data['inverter_data'] = $value;
                $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $value['id'], 'media_cat_id' => 1]);
                if(!empty($attachment)){
                    $inverter_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                    $inverter_file = 'inverter_sheet_'.$id;
                    $upload_path_img13 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$inverter_file . '.pdf';
                    file_put_contents($upload_path_img13, file_get_contents($inverter_attachment));
                }
            } else if ($value['type_id'] == 2) {
                $data['panel_data'] = $value;
                $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $value['id'], 'media_cat_id' => 1]);
                if(!empty($attachment)){
                    $panel_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                    
                    $panle_file = 'panel_sheet_'.$id;
                    $upload_path_img12 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$panle_file . '.pdf';
                    file_put_contents($upload_path_img12, file_get_contents($panel_attachment));
                }
            } else if ($value['type_id'] == 3) {
                $data['battery_data'] = $value;
                $attachment = $this->common->fetch_row('tbl_product_attachments','base_url,file_name',['product_id' => $value['id'], 'media_cat_id' => 1]);
                if(!empty($attachment)){
                    $battery_attachment = $attachment['base_url'].'large/'.$attachment['file_name'];
                    $battery_file = 'battery_sheet_'.$id;
                    $upload_path_img14 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$battery_file . '.pdf';
                    file_put_contents($upload_path_img14, file_get_contents($battery_attachment));
                }
            }
        }
        
        $no_of_panels = 0;
        if (!empty($data['panel_data']) && $data['panel_data']['capacity'] > 0) {
            $no_of_panels = ($proposal['system_size']) / ($data['panel_data']['capacity'] / 1000);
        };
        $data['no_of_panels'] = round($no_of_panels);

        $data['dir']       = $dir               = ($proposal['state_id'] == 7) ? 'vic' : 'non_vic';
        $data['lead_type'] = $lead_type         = $proposal['lead_type'];

        if ($dir == 'vic' && isset($data['proposal']['franchise_company_name'])) {
            $data['proposal']['franchise_company_name'] = 'Solar Run';
        }
        
        $view = $this->input->get('view');
        $page_name = $this->input->get('page_name');

		if($lead_type != 3 && !empty($savings_calculation_data) && isset($view) && $view == 'html'){
            $this->load->view('pdf_files/residential_solar/chart', $data);
		}else{
		    
		  //  $filename1 = 'page0_'.$id;
    //         $upload_path_img1 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename1 . '.pdf';
    //         $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page0 '. $upload_path_img1;
    //         exec($cmd);
                
		    
            //13/2/21
            $filename2 = 'page1_'.$id;
            $upload_path_img2 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename2 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page1 '. $upload_path_img2;
            exec($cmd);
            
            $upload_path_img3 = $upload_path_img4 = $upload_path_img5 = $upload_path_img7 = $upload_path_img8 = $upload_path_img9 = '';
            if ($lead_type != 3) {
                
                $filename3 = 'page2_'.$id;
                $upload_path_img3 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page2_new '. $upload_path_img3;
                exec($cmd);
                
                $filename4 = 'page3_'.$id;
                $upload_path_img4 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page_3_v2 '. $upload_path_img4;
                exec($cmd);
                
                
                
                $filename5 = 'page4_'.$id;
                $upload_path_img5 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page3 '. $upload_path_img5;
                exec($cmd);
            }

			//13-2-21
			
			$filename6 = 'page5_'.$id;
            $upload_path_img6 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename6 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page4_new '. $upload_path_img6;
            exec($cmd);
                
            
			
			if ($lead_type != 3 && !empty($savings_calculation_data)) {
			    
			    $filename7 = 'page6_'.$id;
                $upload_path_img7 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename7 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page7 '. $upload_path_img7;
                exec($cmd);
			}
            
            if ($dir == 'vic') {
				//13-2-21
				$filename8 = 'page7_'.$id;
                $upload_path_img8 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename8 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page_solar_rebate_new '. $upload_path_img8;
                exec($cmd);
            }
            
            if ($dir == 'vic' && $lead_type == 3) {
                
                $filename9 = 'page8_'.$id;
                $upload_path_img9 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename9 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page_battery_rebate_new '. $upload_path_img9;
                exec($cmd);
            }

			//13-2-21
            $filename10 = 'page9_'.$id;
            $upload_path_img10 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename10 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page_steps_new '. $upload_path_img10;
            exec($cmd);
            
            
            $filename11 = 'page10_'.$id;
            $upload_path_img11 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename11 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page_imp_new '. $upload_path_img11;
            exec($cmd);
            
            // // First part of the pdf
            // $filename1 = 'page1_imp_new'.$id;
            // $upload_path_img1 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename1 . '.pdf';
            // $h             = '';
            // $f             = '';
            // $pdf_options   = array(
            //     "source_type" => 'html',
            //     "source"      => $html,
            //     "action"      => 'save',
            //     "save_directory" => __DIR__ . '/../../assets/uploads/proposal_pdf_files/pages',
            //     "file_name"   => $filename1 . '.pdf',
            //     "page_size"   => 'A4',
            //     "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
            //     "header"      => $h,
            //     "footer"      => $f);
            // $this->pdf->phptopdf($pdf_options);
            
            // $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=page1 '. $upload_path_img1;
            // exec($cmd);
            
            
            $filename12 = 'page11_'.$id;
            $upload_path_img15 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename12 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/product/residential_solar/proposal_draft_pdf_download/'.$id.'?view=last_page '. $upload_path_img15;
            exec($cmd);
            
            
            
            // Last part of the pdf
		  //  $filename2 = 'last_'.$id;
    //         $upload_path_img2 = FCPATH . 'assets/uploads/proposal_pdf_files/pages/'.$filename2 . '.pdf';
    //         $h             = '';
    //         $f             = '';
    //         $pdf_options   = array(
    //             "source_type" => 'html',
    //             "source"      => $html,
    //             "action"      => 'save',
    //             "save_directory" => __DIR__ . '/../../assets/uploads/proposal_pdf_files/pages',
    //             "file_name"   => $filename2 . '.pdf',
    //             "page_size"   => 'A4',
    //             "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
    //             "header"      => $h,
    //             "footer"      => $f);
    //         $this->pdf->phptopdf($pdf_options);
            
            // Create PDF final name
            $customer_name = $proposal['first_name'] . "_" . $proposal['last_name'];
            $filename      = "Kuga_Electrical-Residential_Solar_Quote_Form_" . $customer_name . "-" . date('Y-m-d');
            $filename      = str_replace(array('.', ',', ' '), '_', $filename);
            
            $finalFilepath = $upload_path_img2.' '.$upload_path_img3.' '.$upload_path_img4.' '.$upload_path_img5.' '.$upload_path_img6.' '.$upload_path_img7.' '.$upload_path_img8.' '.$upload_path_img9.' '.$upload_path_img10.' '.$upload_path_img11;
            
            if(!empty($panel_attachment)){
                // Panel Sheet
                $finalFilepath = $finalFilepath .' '. $upload_path_img12;
            }
            if(!empty($inverter_attachment)){
                // Inverter Sheet
                $finalFilepath = $finalFilepath .' '. $upload_path_img13;
            }
            
            if(!empty($battery_attachment)){
                // Battery Sheet
                $finalFilepath = $finalFilepath .' '. $upload_path_img14;
            }
            $finalFilepath = $finalFilepath .' '. $upload_path_img15;
            
            // Generate final pdf
            $final_upload_path = FCPATH . 'assets/uploads/proposal_pdf_files/'.$filename. '.pdf';
            $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $finalFilepath;
            exec($cmd);

            $pdf_url = str_replace(' ', '%20', $filename);
            $pdf_url = site_url('assets/uploads/proposal_pdf_files/' . $pdf_url . '.pdf');
            $b64_file = base64_encode(file_get_contents($pdf_url));
            
            if (!file_exists(FCPATH . 'assets/uploads/proposal_pdf_files/' . $filename . '.pdf')) {
                $data['success'] = FALSE;
                $data['status'] = '!Oops looks like some issue in converting booking form to pdf. Please try again. If issue still persits please contact support.';
                echo json_encode($data);
                die;
            }
            
            $filename = $filename. '.pdf';
            
            header('Content-Type: application/pdf');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=".$filename);
            readfile($pdf_url);
            // redirect($pdf_url);
            exit();
        }
    }
    
    public function generate_draft_pdf($id){
        //Proposal
        $proposal = $this->proposal->get_proposal_by_uuid($id);
        //If Proposal Not Linked to lead then catch user and find its fracnhise
        //Find Fracnhise from propsal agent_id
        $owner = $this->common->fetch_where('aauth_user_to_user', 'owner_id', array('user_id' => $proposal['agent_id']));
        
        
            
        if (!empty($owner)) {
            //If already franchise get directly
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
            if(empty($franchise_data)){
                $franchise_data = $this->user->get_owner_list($owner[0]['owner_id']);
            }
        } else {
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
        }
       
        
        if (!empty($franchise_data)) {
            $franchise_data                     = $franchise_data[0];
            $proposal['full_name']              = $franchise_data['full_name'];
            $proposal['company_contact_no']     = $franchise_data['company_contact_no'];
            $proposal['franchise_company_name'] = $franchise_data['company_name'];
            $proposal['franchise_bsb']          = $franchise_data['company_bsb'];
            $proposal['franchise_account_name'] = $franchise_data['company_account_name'];
            $proposal['franchise_account_no']   = $franchise_data['company_account'];
            
            if($proposal['user_id'] != $franchise_data['user_id']){
                $proposal_user_details = $this->proposal->get_proposal_user_details_by_uuid($id);
                $proposal = array_merge($proposal,$proposal_user_details);
            }
        }
        
    
        $data['proposal']  = $proposal;
        //Customer Data
        $data['customer_data']  = $proposal;

        //echo "<pre>";
        //print_r($proposal);die;
        //Proposal Product Data
        $data['proposal_product_data'] = $proposal_product_data  = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 1);
        //Proposal Additional Data
        $data['proposal_additional_data'] = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 2);
        //Line Item Data
        $data['proposal_line_data']       = $this->proposal->get_proposal_line_item_data_by_proposal_id($proposal['id']);

        $data['saving_calculation_details'] = (array) json_decode($proposal['saving_calculation_details']);

        $savings_calculation_data = array();

        $savings_calculation_data = $this->get_savings_calculation_data_from_kugacrm($proposal['saving_calculation_details']);

        $data['savings_calculation_data'] = $savings_calculation_data;
		
		if(isset($data['saving_calculation_details']['postcode']) && !empty($data['saving_calculation_details']['postcode'])){
		    $postcodes = $this->common->fetch_where('tbl_postcodes', '*', array('postcode' => $data['saving_calculation_details']['postcode']));
    		if(!empty($postcodes) && $postcodes[0]['rating'] !=''){
    	        $data['saving_calculation_details']['postcode_rating'] = $postcodes[0]['rating']; 
    		}
		}
        
        $data['panel_data']    = array();
        $data['inverter_data'] = array();
        $data['battery_data']  = array();
        $inverter_attachment = $battery_attachment = $panel_attachment = '';
        foreach ($proposal_product_data as $key => $value) {
            if ($value['type_id'] == 1) {
                $data['inverter_data'] = $value;
            } else if ($value['type_id'] == 2) {
                $data['panel_data'] = $value;
            } else if ($value['type_id'] == 3) {
                $data['battery_data'] = $value;
            }
        }
        
        $no_of_panels = 0;
        if (!empty($data['panel_data']) && $data['panel_data']['capacity'] > 0) {
            $no_of_panels = ($proposal['system_size']) / ($data['panel_data']['capacity'] / 1000);
        };
        $data['no_of_panels'] = round($no_of_panels);

        $data['dir']       = $dir               = ($proposal['state_id'] == 7) ? 'vic' : 'non_vic';
        $data['lead_type'] = $lead_type         = $proposal['lead_type'];

        if ($dir == 'vic' && isset($data['proposal']['franchise_company_name'])) {
            $data['proposal']['franchise_company_name'] = 'Solar Run';
        }
        
        $view = $this->input->get('view');
        
        if(isset($view) && $view == 'page0'){
            $html = $this->load->view('pdf_files/residential_solar/' . $dir . '/page0', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page1'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page1', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page2_new'){
            $html = $this->load->view('pdf_files/residential_solar/' . $dir . '/page2_new', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page_3_v2'){
            $html = $this->load->view('pdf_files/residential_solar/' . $dir . '/page_3_v2', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page3'){
            $html = $this->load->view('pdf_files/residential_solar/' . $dir . '/page3', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page4_new'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page4_new', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page7'){
            $html = $this->load->view('pdf_files/residential_solar/' . $dir . '/page7', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page_solar_rebate_new'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page_solar_rebate_new', $data, TRUE);
            echo $html;die;
            
        }else if(isset($view) && $view == 'page_battery_rebate_new'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page_battery_rebate_new', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page_steps_new'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page_steps_new', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'page_imp_new'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/page_imp_new', $data, TRUE);
            echo $html;die;
        }else if(isset($view) && $view == 'last_page'){
            $html = $this->load->view('pdf_files/residential_solar/pdf_image_pages/last_page', $data, TRUE);
            echo $html;die;
        }
        
        
    }
    
    public function save_dataimg_to_png(){
        

        $pId =  $this->input->post('pId');
        $key =  $this->input->post('key');
        $upload_path = FCPATH .'/assets/uploads/residential_solar_pdf_file_images/'.$pId;
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, TRUE);
        }
        
        $dataImg1 =  $this->input->post('dataImg1');
        $filename1 =  $this->input->post('filename1');
        $img = explode(';',$dataImg1);
        if ($dataImg1 != '' && count($img) > 1 && $key == 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg1));
            $newfilename = $filename1 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename1,$upload_path);
        }
        
        

        $dataImg2 =  $this->input->post('dataImg2');
        $filename2 =  $this->input->post('filename2');
        $img = explode(';',$dataImg2);
        if ($dataImg2 != '' && count($img) > 1 && $key == 2) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg2));
            $newfilename = $filename2 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename2,$upload_path);
        }
        
        
        
        $dataImg3 =  $this->input->post('dataImg3');
        $filename3 =  $this->input->post('filename3');
        $img = explode(';',$dataImg3);
        if ($dataImg3 != '' && count($img) > 1 && $key == 3) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg3));
            $newfilename = $filename3 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename3,$upload_path);
        }
        
        
        
        $dataImg4 =  $this->input->post('dataImg4');
        $filename4 =  $this->input->post('filename4');
        $img = explode(';',$dataImg4);
        if ($dataImg4 != '' && count($img) > 1 && $key == 4) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg4));
            $newfilename = $filename4 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename4,$upload_path);
        }
        
        
        $dataImg5 =  $this->input->post('dataImg5');
        $filename5 =  $this->input->post('filename5');
        $img = explode(';',$dataImg5);
        if ($dataImg5 != '' && count($img) > 1 && $key == 5) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg5));
            $newfilename = $filename5 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename5,$upload_path);
        }
        
        
        
        $dataImg6 =  $this->input->post('dataImg6');
        $filename6 =  $this->input->post('filename6');
        $img = explode(';',$dataImg6);
        if ($dataImg6 != '' && count($img) > 1 && $key == 6) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg6));
            $newfilename = $filename6 . '.png';
            file_put_contents($upload_path .'/'. $newfilename, $image);
            
            $file = $upload_path .'/'. $newfilename;
            $this->convert_image_to_pdf_dimension1($file,$filename6,$upload_path);
        }
        
        
    }

    public function proposal_to_image($id){
        //Proposal
        $proposal = $this->proposal->get_proposal_by_uuid($id);

        //If Proposal Not Linked to lead then catch user and find its fracnhise
        //Find Fracnhise from propsal agent_id
        $owner = $this->common->fetch_where('aauth_user_to_user', 'owner_id', array('user_id' => $proposal['agent_id']));
        
        
            
        if (!empty($owner)) {
            //If already franchise get directly
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
            if(empty($franchise_data)){
                $franchise_data = $this->user->get_owner_list($owner[0]['owner_id']);
            }
        } else {
            $franchise_data = $this->common->fetch_where('tbl_user_details', '*', array('user_id' => $proposal['agent_id']));
        }
       
        
        if (!empty($franchise_data)) {
            $franchise_data                     = $franchise_data[0];
            $proposal['full_name']              = $franchise_data['full_name'];
            $proposal['company_contact_no']     = $franchise_data['company_contact_no'];
            $proposal['franchise_company_name'] = $franchise_data['company_name'];
            $proposal['franchise_bsb']          = $franchise_data['company_bsb'];
            $proposal['franchise_account_name'] = $franchise_data['company_account_name'];
            $proposal['franchise_account_no']   = $franchise_data['company_account'];
            
            if($proposal['user_id'] != $franchise_data['user_id']){
                $proposal_user_details = $this->proposal->get_proposal_user_details_by_uuid($id);
                $proposal = array_merge($proposal,$proposal_user_details);
            }
        }
        
    
        $data['proposal']  = $proposal;
        //Customer Data
        $data['customer_data']  = $proposal;

        //echo "<pre>";
        //print_r($proposal);die;
        //Proposal Product Data
        $data['proposal_product_data'] = $proposal_product_data  = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 1);
        //Proposal Additional Data
        $data['proposal_additional_data'] = $this->proposal->get_proposal_data_by_proposal_id_type($proposal['id'], 2);
        //Line Item Data
        $data['proposal_line_data']       = $this->proposal->get_proposal_line_item_data_by_proposal_id($proposal['id']);

        $data['saving_calculation_details'] = (array) json_decode($proposal['saving_calculation_details']);

        $savings_calculation_data = array();

        $savings_calculation_data = $this->get_savings_calculation_data_from_kugacrm($proposal['saving_calculation_details']);

        $data['savings_calculation_data'] = $savings_calculation_data;
        
        $data['panel_data']    = array();
        $data['inverter_data'] = array();
        $data['battery_data']  = array();
        foreach ($proposal_product_data as $key => $value) {
            if ($value['type_id'] == 1) {
                $data['inverter_data'] = $value;
            } else if ($value['type_id'] == 2) {
                $data['panel_data'] = $value;
            } else if ($value['type_id'] == 3) {
                $data['battery_data'] = $value;
            }
        }
        
        $no_of_panels = 0;
        if (!empty($data['panel_data']) && $data['panel_data']['capacity'] > 0) {
            $no_of_panels = ($proposal['system_size']) / ($data['panel_data']['capacity'] / 1000);
        };
        $data['no_of_panels'] = round($no_of_panels);

        $data['dir']       = $dir               = ($proposal['state_id'] == 7) ? 'vic' : 'non_vic';
        $data['lead_type'] = $lead_type         = $proposal['lead_type'];

        if ($dir == 'vic' && isset($data['proposal']['franchise_company_name'])) {
            $data['proposal']['franchise_company_name'] = 'Solar Run';
        }
        
        $view = $this->input->get('view');
        $this->load->view('pdf_files/residential_solar/pdf_image', $data);
    }
    
    private function convert_image_to_pdf_dimension($file,$filename,$upload_path){
        $data['page4_file'] = $file;
        $dst_x = 0;   // X-coordinate of destination point
        $dst_y = 0;   // Y-coordinate of destination point
        $src_x = 185; // Crop Start X position in original image
        $src_y = 0; // Crop Srart Y position in original image
        $dst_w = 910; // Thumb width
        $dst_h = 1286; // Thumb height
        $src_w = 910; // Crop end X position in original image
        $src_h = 1286; // Crop end Y position in original image
                    
        // Creating an image with true colors having thumb dimensions (to merge with the original image)
        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
        // Get original image
        $img = file_get_contents($file);
        $src_image = imagecreatefromstring($img);
        // Cropping
        imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
        // Saving
        imagejpeg($dst_image, $upload_path.'/'.$filename.'.jpg',99);
    }
    
    private function convert_image_to_pdf_dimension1($file,$filename,$upload_path){
        $data['page4_file'] = $file;
        $dst_x = 0;   // X-coordinate of destination point
        $dst_y = 0;   // Y-coordinate of destination point
        $src_x = 0; // Crop Start X position in original image
        $src_y = 0; // Crop Srart Y position in original image
        $dst_w = 910; // Thumb width
        $dst_h = 1286; // Thumb height
        $src_w = 910; // Crop end X position in original image
        $src_h = 1286; // Crop end Y position in original image
                    
        // Creating an image with true colors having thumb dimensions (to merge with the original image)
        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
        // Get original image
        $img = file_get_contents($file);
        $src_image = imagecreatefromstring($img);
        // Cropping
        imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
        // Saving
        imagejpeg($dst_image, $upload_path.'/'.$filename.'.jpg',99);
    }

	public function save_chartimg_to_png(){		
        $dataImg1 =  $this->input->post('dataImg1');
        $chart1 =  $this->input->post('chart1');
        $img = explode(';',$dataImg1);
        if ($dataImg1 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg1));
            $newfilename = $chart1 . '.png';
            file_put_contents('./assets/uploads/pdf_files/' . $newfilename, $image);
        }
    }
    
    
    public function sync_ted_product_to_kuga_sale_crm(){
        
        $this->edcrm_db = $this->load->database('edcrm_db', TRUE);
        
        $this->edcrm_db->select('prd.*, prdprvar_sr.availability as availability, prdprvar_sr.sell_price as sell_price');
        $this->edcrm_db->from('tbl_products  prd');
        $this->edcrm_db->join('tbl_product_pricing_variants  prdprvar_sr','prd.id = prdprvar_sr.product_id AND prdprvar_sr.company = 2','left');
        $ted_products = $this->edcrm_db->get()->result_array();
        if(!empty($ted_products)){
            
            $this->db->trans_begin();
                $ted_products_types = array(1 => 1, 2 => 2, 3 => 3, 4 => 19, 5 => 17, 6 => 6, 8 => 4, 9 => 5, 10 => 7);
                $applications = array('Residential' => 1, 'Commercial' => 2, 'Both' => 3);
                
                $sleep_counter = 0;
                for($i = 0; $i < count($ted_products); $i++){
                    
                    if($ted_products[$i]['type_id'] == NULL || $ted_products[$i]['type_id'] == 0 || $ted_products[$i]['type_id'] == ''){
                        continue;    
                    }
                    
                    $ted_products[$i]['type_id'] = array_search($ted_products[$i]['type_id'], $ted_products_types);
                    $sale_crm_prd_id = $sell_price = 0;
                    $sale_crm_prd = $this->common->fetch_row('tbl_products','id,ted_crm_id',array('ted_crm_id' => $ted_products[$i]['id']));
                    if(!empty($sale_crm_prd)){
                        $update_data = $ted_products[$i];
                        $sell_price = $update_data['sell_price'];
                        $sale_crm_prd_id = $sale_crm_prd['id'];
                        
                        unset($update_data['id']);
                        unset($update_data['crm_availability_sr']);
                        unset($update_data['crm_availability_je']);
                        unset($update_data['crm_availability_kg']);
                        unset($update_data['kuga_item_id']);
                        unset($update_data['datasheets1']);
                        unset($update_data['sell_price']);
                        
                        $update_data['user_id']  =1;
                        $this->common->update_data('tbl_products',array('id' => $sale_crm_prd['id']),$update_data);
                        
                        $this->edcrm_db->select('st.state_postal as postal');
                        $this->edcrm_db->from('tbl_state st');
                        $this->edcrm_db->join('tbl_product_variants tpv','tpv.state = st.state_id','left');
                        $this->edcrm_db->where('tpv.product_id',$ted_products[$i]['id']);
                        $variants = $this->edcrm_db->get()->result_array();
                        
                        $applicationName = array_search($update_data['application'], $applications);
                        
                        foreach($variants as $variant){
                            $data = [
                                'product_id' => $sale_crm_prd['id'],
                                'application' => $applicationName,
                                'state' => $variant['postal'],
                                'cost_price' => $update_data['cost_price'],
                            ]; 
                            
                            $variantData = $this->common->fetch_row('tbl_product_variants','id',['product_id' => $sale_crm_prd['id'],'state' => $variant['postal']]);
                                
                            if(!empty($variantData)){
                                $this->common->update_data('tbl_product_variants',array('id' => $variantData['id']),$data);
                            }else{
                                $this->common->insert_data('tbl_product_variants',$data);
                            }
                        }
                            
                    }else{
                        $sale_crm_prd = $this->common->fetch_row('tbl_products','id,name',array('name' => $ted_products[$i]['name']));
                        if(!empty($sale_crm_prd)){
                            
                            $sale_crm_prd_id = $sale_crm_prd['id'];
                            $update_data = $ted_products[$i];
                            $update_data['ted_crm_id'] = $update_data['id'];
                            $sell_price = $update_data['sell_price'];
                            
                            unset($update_data['id']);
                            unset($update_data['crm_availability_sr']);
                            unset($update_data['crm_availability_je']);
                            unset($update_data['crm_availability_kg']);
                            unset($update_data['kuga_item_id']);
                            unset($update_data['datasheets1']);
                            unset($update_data['sell_price']);
                        
                            $update_data['user_id']  =1;
                            $this->common->update_data('tbl_products',array('id' => $sale_crm_prd['id']),$update_data);
                            
                            $this->edcrm_db->select('st.state_postal as postal');
                            $this->edcrm_db->from('tbl_state st');
                            $this->edcrm_db->join('tbl_product_variants tpv','tpv.state = st.state_id','left');
                            $this->edcrm_db->where('tpv.product_id',$ted_products[$i]['id']);
                            $variants = $this->edcrm_db->get()->result_array();
                            
                            $applicationName = array_search($update_data['application'], $applications);
                        
                            foreach($variants as $variant){
                                $data = [
                                    'product_id' => $sale_crm_prd_id,
                                    'application' => $applicationName,
                                    'state' => $variant['postal'],
                                    'cost_price' => $update_data['cost_price'],
                                ]; 
                                
                                $variantData = $this->common->fetch_row('tbl_product_variants','id',['product_id' => $sale_crm_prd['id'],'state' => $variant['postal']]);
                                
                                if(!empty($variantData)){
                                    $this->common->update_data('tbl_product_variants',array('id' => $variantData['id']),$data);
                                }else{
                                    $this->common->insert_data('tbl_product_variants',$data);
                                }
                            }
                            
                            
                        }else{
                        
                            $insert_data = $ted_products[$i];
                            $insert_data['ted_crm_id'] = $insert_data['id'];
                            $sell_price = $insert_data['sell_price'];
                            unset($insert_data['id']);
                            unset($insert_data['crm_availability_sr']);
                            unset($insert_data['crm_availability_je']);
                            unset($insert_data['crm_availability_kg']);
                            unset($insert_data['kuga_item_id']);
                            unset($insert_data['datasheets1']);
                            unset($insert_data['sell_price']);
                        
                            $insert_data['user_id']  =1;
                            $this->common->insert_data('tbl_products',$insert_data);
                            $sale_crm_prd_id = $this->common->insert_id();
                            $this->edcrm_db->select('st.state_postal as postal');
                            $this->edcrm_db->from('tbl_state st');
                            $this->edcrm_db->join('tbl_product_variants tpv','tpv.state = st.state_id','left');
                            $this->edcrm_db->where('tpv.product_id',$ted_products[$i]['id']);
                            $variants = $this->edcrm_db->get()->result_array();
                            
                            $applicationName = array_search($update_data['application'], $applications);
                        
                            foreach($variants as $variant){
                                $data = [
                                    'product_id' => $sale_crm_prd_id,
                                    'application' => $applicationName,
                                    'state' => $variant['postal'],
                                    'cost_price' => $update_data['cost_price'],
                                ];
                                
                                $variantData = $this->common->fetch_row('tbl_product_variants','id',['product_id' => $sale_crm_prd['id'],'state' => $variant['postal']]);
                                
                                if(!empty($variantData)){
                                    $this->common->update_data('tbl_product_variants',array('id' => $variantData['id']),$data);
                                }else{
                                    $this->common->insert_data('tbl_product_variants',$data);
                                }
                            }
                        }
                    }
                    
                    $states = $this->common->fetch_where('tbl_state');
                    $applicationName = array_search($ted_products[$i]['application'], $applications);
                    foreach($states as $state){
                        $data = [
                            'product_id' => $sale_crm_prd_id,
                            'application' => $applicationName,
                            'state' => $state['state_postal'],
                            'cost_price' => (isset($sell_price) && !empty($sell_price)) ? $sell_price : $ted_products[$i]['cost_price'],
                        ]; 
                        
                        $variantData = $this->common->fetch_row('tbl_product_variants','id',['product_id' => $sale_crm_prd_id,'state' => $state['state_postal']]);
                            
                        if(!empty($variantData)){
                            $this->common->update_data('tbl_product_variants',array('id' => $variantData['id']),$data);
                        }else{
                            $this->common->insert_data('tbl_product_variants',$data);
                        }
                    }
                    
                    $this->edcrm_db->where('product_id',$ted_products[$i]['id']);
                    $attachments = $this->edcrm_db->get('tbl_product_attachments')->result_array();
                    foreach($attachments as $attachment){
                        
                        $attachment['product_id'] = $sale_crm_prd_id;
                        
                        $attach = $this->common->fetch_row('tbl_product_attachments','media_id',['product_id' => $attachment['product_id'], 'media_cat_id' => $attachment['media_cat_id']]);
                    
                        if(!empty($attach)){
                            $this->common->update_data('tbl_product_attachments',['media_id' => $attach['media_id']], $attachment);
                        }else{
                            $this->common->insert_data('tbl_product_attachments',$attachment);
                        }
                    }
                    
                    $sleep_counter++;
                    if($sleep_counter == 100){
                        $sleep_counter = 0;
                        sleep(5);
                    }
                }
                
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $stat = FALSE;
            } else {
                $this->db->trans_commit();
                $stat = TRUE;
            }
            
            if($stat){
                echo "Product Syncying successfull";
                echo "<br>";
            }else{
                echo "Product Syncying failed!!";
                echo "<br>";
            }
        }
    }
}

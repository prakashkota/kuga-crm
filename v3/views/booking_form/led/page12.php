<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35" style="color:#000000; font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>Additional Space Type Photographs</strong></td>
                </tr>
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td width="412" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Photo 1</strong></td>
                                <td width="412" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px"><strong>Photo 2</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="space_type_photo_1" />
                                        <input type="hidden" id="space_type_photo_1" name="booking_form_image[space_type_photo_1]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                                <td align="center" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="space_type_photo_2" />
                                        <input type="hidden" id="space_type_photo_2" name="booking_form_image[space_type_photo_2]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td width="412" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Supporting BCA Evidence</strong></td>
                                <td width="412" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px"><strong>Supporting BCA Evidence</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="bca_evidence_1" />
                                        <input type="hidden" id="bca_evidence_1" name="booking_form_image[bca_evidence_1]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                                <td align="center" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="bca_evidence_2" />
                                        <input type="hidden" id="bca_evidence_2" name="booking_form_image[bca_evidence_2]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>

<style>
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .table_target {
        border: 1px solid grey;
        width:100%;
        font-family: arial, sans-serif;
        border-collapse: collapse;
    }
    .table_target td {
        border: 1px solid grey;
        border-bottom: 1px solid #fff;
        position: relative;
    }

    .table_target td input{
        width: 100%;
    }

    .table_target td textarea{
        width: 100%;
        height:100px;
    }

    .item_title{
        font-size: 14px;
        padding-left: 5px;
        width:50%;
    }   

    .table_target td, .table_target th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    .table_target th
    {
        background-color:black;
        color:white;
    }

</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Manage Sales Dashboard Settings</h1>
    </div>
    <div class="form-block d-block clearfix">
        <?php
        $message = $this->session->flashdata('message');
        echo (isset($message) && $message != NULL) ? $message : '';
        ?>
        <form role="form"  action="" class="" method="post"  enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Upload simPRO Dump
                            <div class="float-md-right">
                                <a href="javascript:void(0);" id="upload_simpro_dump_file" class="btn">Upload</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dump File (.csv)</label>
                                        <div class="custom-file">
                                            <input type="file" name="simpro_dump_file">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="loader" style="display: flex;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form role="form"  action="" class="" method="post"  id="target_data_vic_form"  enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Manage Percentage Target (VIC)
                            <div class="float-md-right">
                                <a href="javascript:void(0);" id="save_vic_target_data" class="btn">Save</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_0" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_vic[0][target_start_date]" id="daterange_0_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_vic[0][target_end_date]" id="daterange_0_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_vic[0][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_vic[0][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_vic[0][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_vic[0][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_vic[0][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_vic[0][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_vic[0][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_vic[0][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_1" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_vic[1][target_start_date]" id="daterange_1_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_vic[1][target_end_date]" id="daterange_1_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_vic[1][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_vic[1][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_vic[1][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_vic[1][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_vic[1][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_vic[1][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_vic[1][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_vic[1][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_2" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_vic[2][target_start_date]" id="daterange_2_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_vic[2][target_end_date]" id="daterange_2_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_vic[2][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_vic[2][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_vic[2][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_vic[2][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_vic[2][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_vic[2][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_vic[2][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_vic[2][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <form role="form"  action="" class="" method="post" id="target_data_nsw_form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Manage Percentage Target (NSW)
                            <div class="float-md-right">
                                <a href="javascript:void(0);" id="save_nsw_target_data" class="btn">Save</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_3" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_nsw[0][target_start_date]" id="daterange_3_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_nsw[0][target_end_date]" id="daterange_3_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_nsw[0][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_nsw[0][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_nsw[0][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_nsw[0][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_nsw[0][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_nsw[0][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_nsw[0][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_nsw[0][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_4" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_nsw[1][target_start_date]" id="daterange_4_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_nsw[1][target_end_date]" id="daterange_4_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_nsw[1][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_nsw[1][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_nsw[1][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_nsw[1][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_nsw[1][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_nsw[1][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_nsw[1][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_nsw[1][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="text-center">
                                        <div class="btn-group float-md-right" role="group" >
                                            <div id="daterange_5" >
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="target_data_nsw[2][target_start_date]" id="daterange_5_sd" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                                            <input type="hidden" name="target_data_nsw[2][target_end_date]" id="daterange_5_ed" value="<?php echo date('Y-m-d'); ?>" />
                                        </div>
                                    </div>
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Targets</th>
                                                    <th>Weighting</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="item_title">HB - Submitted to Ap</td>
                                                    <td><input name="target_data_nsw[2][target][hb_submitted_to_ap]" type="number" /></td>
                                                    <td><input name="target_data_nsw[2][weighting][hb_submitted_to_ap]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">PL / Batten / Tubes - Installed</td>
                                                    <td><input name="target_data_nsw[2][target][p_b_t_installed]" type="number" /></td>
                                                    <td><input name="target_data_nsw[2][weighting][p_b_t_installed]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Floodlights - Submitted to AP </td>
                                                    <td><input name="target_data_nsw[2][target][floodlight_submitted]" type="number" /></td>
                                                    <td><input name="target_data_nsw[2][weighting][floodlight_submitted]" type="number" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title">Solar Approved</td>
                                                    <td><input name="target_data_nsw[2][target][solar_approved]" type="number" /></td>
                                                    <td><input name="target_data_nsw[2][weighting][solar_approved]" type="number" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>



        <form role="form"  action="" class="" method="post" id="sales_formula_form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Manage Sales Report Formula (in Sql)
                            <div class="float-md-right">
                                <a href="javascript:void(0);" id="save_formulas" class="btn">Save</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div  class="table-responsive">
                                        <table class="table table_target" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Formula</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($formula_fields as $key => $value) { ?>
                                                    <tr>
                                                        <td class="item_title"><?php echo $value['name']; ?></td>
                                                        <td><textarea name="formula[<?php echo $value['alias']; ?>]"><?php echo $formula_data[$key]['formula']; ?></textarea></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>

    $(function () {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb1(start, end) {
            $('#daterange_0 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_0_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_0_ed').value = end.format('YYYY-MM-DD');
        }

        function cb2(start, end) {
            $('#daterange_1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_1_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_1_ed').value = end.format('YYYY-MM-DD');
        }

        function cb3(start, end) {
            $('#daterange_2 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_2_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_2_ed').value = end.format('YYYY-MM-DD');
        }

        function cb4(start, end) {
            $('#daterange_3 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_3_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_3_ed').value = end.format('YYYY-MM-DD');
        }

        function cb5(start, end) {
            $('#daterange_4 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_4_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_4_ed').value = end.format('YYYY-MM-DD');
        }

        function cb6(start, end) {
            $('#daterange_5 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            document.getElementById('daterange_5_sd').value = start.format('YYYY-MM-DD');
            document.getElementById('daterange_5_ed').value = end.format('YYYY-MM-DD');
        }

        $('#daterange_0').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb1);

        $('#daterange_1').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb2);

        $('#daterange_2').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb3);


        $('#daterange_3').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb4);

        $('#daterange_4').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb5);

        $('#daterange_5').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb6);

        cb1(start, end);
        cb2(start, end);
        cb3(start, end);
        cb4(start, end);
        cb5(start, end);
        cb6(start, end);

    });


    $('#upload_simpro_dump_file').click(function () {
        if ($('input[name=simpro_dump_file]').val() == '') {
            toastr["error"]('Please Select a file to upload');
            return false;
        }
        var file_data = $('input[name=simpro_dump_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('simpro_dump_csv_file', file_data);
        import_csv(form_data);
    });

    function import_csv(form_data) {
        $.ajax({
            url: base_url + 'admin/settings/upload_simpro_sales_dump',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                $('#loader').html(createLoader('Uploading simPRO Dump, please wait..'));
                $('#upload_simpro_dump_file').attr('disabled', 'disabled');
            },
            success: function (res) {
                $('#upload_simpro_dump_file').removeAttr('disabled');
                $('#loader').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
    }

    $('#save_vic_target_data').click(function () {
        var form_data = $('#target_data_vic_form').serialize();
        save_sales_data(form_data);
    });

    $('#save_nsw_target_data').click(function () {
        var form_data = $('#target_data_nsw_form').serialize();
        save_sales_data(form_data);
    });

    $('#save_formulas').click(function () {
        var form_data = $('#sales_formula_form').serialize();
        save_sales_data(form_data);
    });

    function save_sales_data(form_data) {
        $.ajax({
            url: base_url + 'admin/settings/simpro_sales/save',
            dataType: 'json',
            cache: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                toastr["success"]('Saving Data Please Wait....');
            },
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
    }

    var target_vic_data = '<?php echo (isset($target_vic_data) && !empty($target_vic_data)) ? json_encode($target_vic_data, JSON_HEX_APOS) : ''; ?>';
    var target_nsw_data = '<?php echo (isset($target_nsw_data) && !empty($target_nsw_data)) ? json_encode($target_nsw_data, JSON_HEX_APOS) : ''; ?>';

    setTimeout(function () {
        target_vic_data = JSON.parse(target_vic_data);
        for (var i = 0; i < target_vic_data.length; i++) {
            $('#daterange_'+i+' span').html(moment(target_vic_data[i]['target_start_date']).format('MMMM D, YYYY') + ' - ' + moment(target_vic_data[i]['target_end_date']).format('MMMM D, YYYY'));
            $("input[name='target_data_vic[" + i + "][target_start_date]']").val(target_vic_data[i]['target_start_date']);
            $("input[name='target_data_vic[" + i + "][target_end_date]']").val(target_vic_data[i]['target_end_date']);
            for (var key in target_vic_data[i]['target']) {
                $("input[name='target_data_vic[" + i + "][target][" + key + "]']").val(target_vic_data[i]['target'][key]);
                $("input[name='target_data_vic[" + i + "][weighting][" + key + "]']").val(target_vic_data[i]['weighting'][key]);
            }
        }

        target_nsw_data = JSON.parse(target_nsw_data);
        for (var i = 0; i < target_nsw_data.length; i++) {
            $('#daterange_'+(i + 3)+' span').html(moment(target_nsw_data[i]['target_start_date']).format('MMMM D, YYYY') + ' - ' + moment(target_nsw_data[i]['target_end_date']).format('MMMM D, YYYY'));
            $("input[name='target_data_nsw[" + i + "][target_start_date]']").val(target_nsw_data[i]['target_start_date']);
            $("input[name='target_data_nsw[" + i + "][target_end_date]']").val(target_nsw_data[i]['target_end_date']);
            for (var key in target_nsw_data[i]['target']) {
                $("input[name='target_data_nsw[" + i + "][target][" + key + "]']").val(target_nsw_data[i]['target'][key]);
                $("input[name='target_data_nsw[" + i + "][weighting][" + key + "]']").val(target_nsw_data[i]['weighting'][key]);
            }
        }
    }, 2000);



</script>

<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="676" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>Check List</strong></td>
      </tr>
      <tr>
        <td height="5"></td>
      </tr>
      <tr>
        <td height="35" style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>Please write all relevant conversations with customers in the Note Section:</strong></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">1.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Have you explained the disclaimers?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">2.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Have you had pricing discussions with the customer. If so what was discussed?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">3.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Have you shown Samples to the client?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">3a.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Do any need to be installed?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">4.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Access issues? including the provisioning of equipment and fees associated with the supply of              faulty or dangerous units by customer. </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">5.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Scissor lift/Boom lift accessibility?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">6.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Has the customer agreed to clear access? Be specific, what areas?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">7.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Are all the fittings to be upgraded working?</td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">8.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Are there any globes missing?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">8a.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Have you asked the customer to replace any missing globes?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">9.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Have you explained the difference betwen Electronic and Magnetic ballast and how this may affect pricing?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox_tick.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">10.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">What are the access times?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">11.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Are there induction requirements?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">12.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Is there someone else we ned to organise this with? i.e. Centre Management</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">13.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Are areas vacant at certain times?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">14.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">Is the customer aware that the payment is required on day of completion?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td style="padding:5px;"><table width="820" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38"><img src="checkbox.jpg" width="28" height="28" /></td>
            <td width="18" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">15.</td>
            <td width="764" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000">If there has been a previous upgrade, have the details been recorded on the RCP?</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="7">&nbsp;</td>
      </tr>
      <tr>
        <td width="676" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px"><strong>Notes</strong></td>
      </tr>
      <tr>
        <td style="padding:5px;"></td>
      </tr>
     <tr>
       <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px" valign="top"><table width="820" border="0" cellspacing="0" cellpadding="0">
         <tr>
           <td><strong>Parking will be made available<br />
             Office Hours 8am - 5pm Mon-Fri</strong></td>
           </tr>
         <tr>
           <td height="5"></td>
           </tr>
         <tr>
           <td><strong>There are a few 600 x 300 twin Fluor fittings not to be upgraded<br />
             $0 cost to customer for upgrading 76 fittings to 1200 x 300 panel lights</strong></td>
           </tr>
         </table></td>
     </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>
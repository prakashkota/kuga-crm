<?php 
$dailyProduction    = number_format($AverageDailyProduction,0,".",",");
$co2                = number_format(($AverageDailyProduction*.87),0,".",",");
$reduction          = ($AverageDailyProduction*.87)/4600;

$weeklyProduction   =  number_format(($AverageDailyProduction*7),0,".",",");
$weeklyCo2          =  number_format(($AverageDailyProduction*.87*7),0,".",",");
$weeklyReduction    =  number_format(($reduction*7),2,".",",");

$annualProduction   =  number_format(($AverageDailyProduction*365),0,".",",");
$annualCo2          =  number_format(($AverageDailyProduction*.87*365),0,".",",");
$annualReduction    =  number_format(($reduction*365),2,".",",");

?>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
    <tbody>
        <tr>
			<td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">9 <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img.jpg" alt="" style="display:inline-block"/></td>
		</tr>
      <tr>
        <td style="padding:0 50px 50px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/heading5.jpg" alt=""/></td>
      </tr>
      <tr>
        <td>
        <table width="656" border="0" align="center" cellpadding="0" cellspacing="0" >
          <tr>
            <td style="background:#58595b;">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td style="padding:40px 0 0 0;background: #fff;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/top-heading1.jpg" alt=""/></td>
              </tr>
              <tr>
                <td style="padding:20px 0 0;text-align:center; color:#fff; font-size:15px; vertical-align:top;"><strong>ENERGY (kWh)</strong><br /><?php echo $dailyProduction;?><br />-<br /><strong>CO<sub>2</sub> (Kg)</strong><br /><?php echo $co2;?></td>
              </tr>
            </table>
            </td>
            <td valign="top" style="background:#404042;">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
              <tr>
                <td style="padding:20px 0 0 0;background: #fff;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/top-heading2.jpg" alt=""/></td>
              </tr>
              <tr>
                <td style="padding:20px 0 0;text-align:center; color:#fff; font-size:15px; vertical-align:top;"><strong>ENERGY (kWh)</strong><br /><?php echo $weeklyProduction;?><br />-<br /><strong>CO<sub>2</sub> (Kg)</strong><br /><?php echo $weeklyCo2;?></td>
              </tr>
            </table>
            </td>
            <td valign="top" style="background:#ca292f;">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td style="background: #fff;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/top-heading3.jpg" alt=""/></td>
              </tr>
              <tr>
                <td style="padding:20px 0 0;text-align:center; color:#fff; font-size:15px; vertical-align:top;"><strong>ENERGY (kWh)</strong><br /><?php echo $annualProduction;?><br />
                -<br /><strong>CO<sub>2</sub> (Kg)</strong><br/><?php echo $annualCo2;?></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td align="center"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/car-bot-img-one.jpg" alt=""></td>
      </tr>
      <tr>
        <td style="text-transform:uppercase; color:#cd2630; font-weight:700; " align="center">Equivalent annual<br> reduction of cars<br> <?php echo $annualReduction;?></td>
      </tr>
      <tr>
        <td align="center"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/car-bot-img-two.jpg" alt=""></td>
      </tr>
      <tr><td> <br/>
  <br/>
  <br/>
  <br/>
  <br/></td></tr>
      <tr>
			<td style="padding:20px 50px 0; text-align:right; font-size:13px; color:#666;">
				WWW.13KUGA.COM.AU   |   <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>
			</td>
		</tr>
    </tbody>
  </table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="100%" height="126" /></td>
    </tr>
    
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
            <tr>
                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Customer Details</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">First Name:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->first_name; ?></strong></td>
                            </tr>
                        </table></td>
                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Last Name:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->last_name; ?></strong></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>

                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Email:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->email; ?></strong></td>
                            </tr>
                        </table></td>

                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Phone Number:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->contact_no; ?></strong></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>

                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Other Number:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $authorised_details->other_contact_no; ?></strong></td>
                            </tr>
                        </table></td>


                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Address:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->address; ?></strong></td>
                            </tr>
                        </table></td>									

                    </tr>
                </table></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
            <tr>
                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; font-size:16px;"><strong>Electricity Bill Information</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Retailer:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_retailer; ?></strong></td>
                            </tr>
                        </table></td>
                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">NMI:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_nmi; ?></strong></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Distributor:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_distributor; ?></strong></td>
                            </tr>
                        </table></td>
                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Meter Number:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_meter_no; ?></strong></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Account Name:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_acc_name; ?></strong></td>
                            </tr>
                        </table></td>
                        <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="122" style="font-family:Calibri, sans-serif; font-size:14px;">Account Number:</td>
                                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $electricity_bill->eb_acc_no; ?></strong></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Connections / Monitoring</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tbody><tr>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <div class="form-check form-check-inline">
                                                    <input style="width:25px; height:25px;" class="form-check-input network_type" type="radio" <?php if($business_details->network_type == '1'){ echo "checked"; }?> id="network_type_1" value="1">
                                                    <label class="form-check-label" for="network_type_1">WI-FI</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input style="width:25px; height:25px;" class="form-check-input network_type" type="radio" <?php if($business_details->network_type == '0'){ echo "checked"; }?> id="network_type_0" value="0">
                                                    <label class="form-check-label" for="network_type_0">Hard Wired</label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table></td>
                </tr>
                <?php if($business_details->network_type == '1'){ ?>
                <tr >
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr class="wifi_result hidden">
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Wifi Username:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->wifi_username; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Wifi Password:</td>
                                            <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px"><strong><?php echo $business_details->wifi_password; ?></strong></td>
                                            
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <?php } ?>
            </table></td>
    </tr>
    <tr>
        <?php if($business_details->network_type == '1'){ ?>
            <td height="400">&nbsp;</td>
        <?php }else{ ?>
            <td height="460">&nbsp;</td>
        <?php } ?>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
            </tr>
            <tr>
                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
            </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
        </tr>
        <tr>
            <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
        </tr>
    </table></td>
    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />2</td>
</tr>
</table></td>
</tr>  
</table>

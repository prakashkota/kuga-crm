<script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<form action="<?php echo site_url('admin/proposal/save_dataimg_to_png'); ?>" method="POST" id="chartForm">
    <input type="hidden"  id="pId" value="<?php echo $led_proposal_data['id']; ?>" />
    <input type="hidden"  name="chart1" value="chart1_<?php echo $led_proposal_data['id']; ?>" />
    <input type="hidden"  name="chart2" value="chart2_<?php echo $led_proposal_data['id']; ?>" />
    <input type="hidden"  name="chart3" value="chart3_<?php echo $led_proposal_data['id']; ?>" />
    <input type="hidden"  name="chart4" value="chart4_<?php echo $led_proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg1" name="dataImg1" value="" />
    <input type="hidden" id="dataImg2" name="dataImg2" value="" />
    <input type="hidden" id="dataImg3" name="dataImg3" value="" />
    <input type="hidden" id="dataImg4" name="dataImg4" value="" />
</form>

<div id="charts"  style="display:none;">
    <div id="curve_chart"></div>
    <div id="chart"></div>
    <div id="chart3"></div>
</div>
<div id="status" style="text-align:center; margin-top:100px; margin:0; left:0; right:0; top:0; bottom:0; ">Conversion of Charts in Progress.....</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
     <script type='text/javascript'>
     google.load('visualization', '1.0', {'packages':['corechart']});
     google.setOnLoadCallback(drawChart);
    function drawChart() {
       var data = google.visualization.arrayToDataTable([
           ['Year', 'No upgrade', 'LED Upgrade'],
		   ['YR 0',  0,   0 ],
		  ['YR 1', <?php echo $withoutUpgrade_yer1; ?>,      <?php echo $withUpgrade_yer1; ?> ],
          ['YR 2',  <?php echo $withoutUpgrade_yer2; ?>,      <?php echo $withUpgrade_yer2; ?>],
		  ['YR 3',  <?php echo $withoutUpgrade_yer3; ?>,      <?php echo $withUpgrade_yer3; ?> ],
		  ['YR 4',  <?php echo $withoutUpgrade_yer4; ?>,      <?php echo $withUpgrade_yer4; ?> ],
		  ['YR 5',  <?php echo $withoutUpgrade_yer5; ?>,      <?php echo $withUpgrade_yer5; ?>]

]);

        var options = {
          title: 'ROI : ' + "<?php echo number_format($calculated_data['roi'], 2, '.', ''); ?>" + " months",
          curveType: 'function',
		  width: 600,
		  height:230,
          legend: { position: 'bottom' }
        };

        var my_div = document.getElementById('curve_chart');
    var my_chart = new google.visualization.LineChart(my_div);

    google.visualization.events.addListener(my_chart, 'ready', function () {
      document.getElementById('dataImg1').value = my_chart.getImageURI();
      //my_div.innerHTML = '<img src='" + my_chart.getImageURI() + '">';
      /**$.ajax({
          type: 'POST',
          url: 'https://kugacrm.com.au/admin/proposal/save_dataimg_to_png',
          datatype: 'json',
          data: $('#chartForm').serialize(),
          success: function (stat) {
            
          },
          error: function (xhr, ajaxOptions, thrownError) {
            
          }
        });*/
    });
    my_chart.draw(data,options);
      }

     function drawVisualization() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Finance', 'New Bill', 'Old Bill' ],
    ['Year 1',  <?php echo $Finance_Yr1; ?>,<?php echo $NewBill_yer1; ?> ,<?php echo $OldBill2_yer1; ?> ],
	['Year 2',  <?php echo $Finance_Yr2; ?>,<?php echo $NewBill_yer2; ?> ,<?php echo $OldBill2_yer2; ?> ],
	['Year 3',  <?php echo $Finance_Yr3; ?>,<?php echo $NewBill_yer3; ?> ,<?php echo $OldBill2_yer3; ?> ],
	['Year 4', <?php echo $Finance_Yr4; ?>,<?php echo $NewBill_yer4; ?> ,<?php echo $OldBill2_yer4; ?> ],
	['Year 5',  <?php echo $Finance_Yr5; ?>,<?php echo $NewBill_yer5; ?> ,<?php echo $OldBill2_yer5; ?> ],
	['Year 6',  '',  <?php echo $NewBill_yer6; ?>, <?php echo $OldBill2_yer6; ?>],
	['Year 7',  '',  <?php echo $NewBill_yer7; ?>, <?php echo $OldBill2_yer7; ?>],
	['Year 8',  '', <?php echo $NewBill_yer8; ?>, <?php echo $OldBill2_yer8; ?>],
	['Year 9',  '', <?php echo $NewBill_yer9; ?>, <?php echo $OldBill2_yer9; ?>],
	['Year 10',  '', <?php echo $NewBill_yer10; ?>, <?php echo $OldBill2_yer10; ?>]
  ]);

 var options = 
           {title:'Get a guaranteed saving on your power bill starting from year one even after financing your investment in your new LED lighting.',
		   titleTextStyle:{
              fontName: 'Open Sans',
			  italic: false,
			  bold:false,
			  fontSize: 14,
              fontStyle: 'normal' //or bold, italic, etc.
          },bar: {groupWidth: '60%'},
		  isStacked:true,
             width: 700,
		  height:300,
			legend: { position: 'bottom' },
            vAxis: {title: '',textStyle : {fontSize: 10}},
            hAxis: {title: '',textStyle : {fontSize: 10}}};
            
      
       var my_div = document.getElementById('chart');
    var my_chart = new google.visualization.ColumnChart(my_div);

    google.visualization.events.addListener(my_chart, 'ready', function () {
      document.getElementById('dataImg3').value = my_chart.getImageURI();
      //my_div.innerHTML = '<img src='" + my_chart.getImageURI() + '">';
     /**$.ajax({
          type: 'POST',
          url: 'https://kugacrm.com.au/admin/proposal/save_dataimg_to_png',
          datatype: 'json',
          data: $('#chartForm').serialize(),
          success: function (stat) {
            
          },
          error: function (xhr, ajaxOptions, thrownError) {
            
          }
        });*/
    });
    my_chart.draw(data,options);
}

google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawVisualization);

     function drawVisualization2() {
  
  var options = 
           {title:'As energy price increases every year, the armount of saving from your LED upgrade will also increase.',
		   titleTextStyle:{
              fontName: 'Open Sans',
			  italic: false,
			  bold:false,
			  fontSize: 14,
              fontStyle: 'normal' //or bold, italic, etc.
          },bar: {groupWidth: '60%'},
           width: 700,
		  height:300,
			legend: { position: 'bottom' },
            vAxis: {title: '',textStyle : {fontSize: 10}},
            hAxis: {title: '',textStyle : {fontSize: 10}}};
  
  var data = google.visualization.arrayToDataTable([
    ['Year',  'Savings' ],
    ['Year 1',  <?php echo $TotalSavings_YR1; ?> ],
	['Year 2',   <?php echo $TotalSavings_YR2; ?>],
	['Year 3',   <?php echo $TotalSavings_YR3; ?>],
	['Year 4',   <?php echo $TotalSavings_YR4; ?>],
	['Year 5',   <?php echo $TotalSavings_YR5; ?>],
	['Year 6',  <?php echo $TotalSavings_YR6; ?>],
	['Year 7',   <?php echo $TotalSavings_YR7; ?>],
	['Year 8',   <?php echo $TotalSavings_YR8; ?>],
	['Year 9',   <?php echo $TotalSavings_YR9; ?>],
	['Year 10',  <?php echo $TotalSavings_YR10; ?>]
  ]);

  var my_div = document.getElementById('chart3');
    var my_chart = new google.visualization.ColumnChart(my_div);

    google.visualization.events.addListener(my_chart, 'ready', function () {
      document.getElementById('dataImg4').value = my_chart.getImageURI();
      //my_div.innerHTML = '<img src='" + my_chart.getImageURI() + '">';
      $.ajax({
          type: 'POST',
          url: 'https://kugacrm.com.au/admin/proposal/save_dataimg_to_png',
          datatype: 'json',
          data: $('#chartForm').serialize(),
          success: function (stat) {
            //$('#charts').html('');
            var pid = $('#pId').val();
            $('#status').html('');
            $('#status').html('Pdf Creation in Progress...');
            setTimeout(function(){
                window.location.href = 'https://kugacrm.com.au/admin/proposal/led_pdf_download/'+ pid + '?view=download';
                //window.open('https://kugacrm.com.au/admin/proposal/led_pdf_download/'+ pid + '?view=download');
                //window.close();
            },300);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            
          }
        });
    });
    my_chart.draw(data,options);
      
}

google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawVisualization2);
</script>
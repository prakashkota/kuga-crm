<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic|Roboto+Mono:400,500|Material+Icons" />
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.css">
<link href='<?php echo site_url(); ?>assets/dropzone/dropzone.min.css' type='text/css' rel='stylesheet'>

<style>
    body{overflow: hidden;}
    .capitalize { text-transform:capitalize;}   
    .page-wrapper{background-color:#ECEFF1; min-height:100%; height: 677px; padding:0px;overflow: hidden;}
    #map {height: calc(100%);width: calc(100%);float: right;overflow: hidden; position: fixed !important;}

    .card-sc {
        width: 100%;
        overflow: scroll;
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
    }

    div.card-sc::-webkit-scrollbar { 
        width: 0;
        height: 10px !important;
    }

    .kg-lead_card {
        margin-left: 5px;
        margin-right: 5px;
        height: 110px;
        width: 350px;
        float: left;
        border-radius: 5px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0);
    }

    .kg-lead_card--center {
        position:fixed;
        bottom:25px;
        right:25%;
        left:50%;
        margin-left:-150px;
    }

    .kg-lead_card--white{
        background: #fff;
    }


    .kg-lead_card--yellow{
        border-color: rgba(255,255,102,1) !important;
        background-color: rgba(255,255,102,1) !important;
        box-shadow: inset 4px 0 0 0 #ffff66 !important;
    }

    .kg-lead_card--orange{
        border-color: rgba(255,109,0,1) !important;
        background-color: rgba(255,109,0,1) !important;
        box-shadow: inset 4px 0 0 0 #FF6D00 !important;
    }


    .kg-lead_card--grey{
        border-color: rgba(211,211,211,1) !important;
        background-color: rgba(211,211,211,1) !important;
        box-shadow: inset 4px 0 0 0 #D3D3D3 !important;
    }

    .kg-lead_card--black{
        border-color: rgba(00,00,00,1) !important;
        background-color: rgba(00,00,00,1) !important;
        box-shadow: inset 4px 0 0 0 #000 !important;
    }

    .kg-lead_card--amber{
        border-color: rgba(255, 191, 0, 1) !important;
        background-color: rgba(255, 191, 0, 1) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }

    .kg-lead_card--blue{
        border-color: rgba(49, 122, 226, 1) !important;
        background-color: rgba(49, 122, 226, 1) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }

    .kg-lead_card--green{
        border-color: rgb(0,255,66) !important;
        background-color: rgb(0,255,66) !important;
        box-shadow: inset 4px 0 0 0 #00FF42 !important;
    }

    .kg-lead_card--red{
        border-color: rgba(255, 0, 0, 1) !important;
        background-color: rgba(255, 0, 0, 1) !important;
        box-shadow: inset 4px 0 0 0 #ff0000 !important;
    }

    .kg-lead_card--pink{
        border-color: rgba(255, 192, 203, 1) !important;
        background-color: rgba(255, 192, 203, 1) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }


    .kg-lead_card__title{
        font-family: poppins;
        font-weight: 600;
        margin-left:10px;
        letter-spacing: .01785714em;
        font-family: 'Google Sans',Roboto,Arial,sans-serif;
        font-size: 1.3rem;
        line-height: 1.25rem;
        color: #3c4043;
        max-height: none;
        position: static;
        word-break: break-word;
    }

    .kg-lead_card__body{
        margin-left:45px;
        margin-top:5px;
        color:rgba(55,55,55,0.8);
        font-family: poppins;
    }

    .kg-lead_card__title--border{
        margin-left:45px;
        border-bottom: 2px solid grey;
        width:80%;
    }

    .kg-lead_card__title_direction_icon{
        position: fixed;
        margin-left: 195px;
        margin-top: -32px;
        z-index: 200000;
        font-size: 40px;
        color: green;
        font-family: poppins;
    }

    .kg-lead_card__body_actions{
        margin-top:10px;
        display: block;
        background: white;
        color: black;
        font-family: poppins;
    }

    .kg-lead_card__body_address{
        border-bottom: 1px solid rgba(55,55,55,0.2);
        margin-bottom: 5px;
        padding: 10px 0px 10px 0px;
        font-family: poppins;
    }

    .kg-lead_card__body_actions_item{
        width:24%;
        font-size:30px;
        display: inline-block;
        vertical-align: top;
        color: #333;
        text-align: center;
        text-decoration: none;
        cursor:pointer;
    }

    .kg-lead_card__body_actions_item_text{
        font-size: 10px;
        display: block;
        margin: auto;
        border: 0;
    }


    .placeholder__card {
        position:fixed;
        bottom:25px;
        right:25%;
        left:50%;
        height: 100px;
        width: 300px;
        margin-left:-150px;
        background: #fff;
    }

    #card_container {
        display:block;
    }

    .material-icons {color:green;}
    .kg-lead_card__body_actions_item_text {color:green;}
    .kg-lead_card__title{color:#000;}

    .kg-lead_card--pink  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--pink  .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--pink .kg-lead_card__body{color:#fff;}
    .kg-lead_card--pink .kg-lead_card__title{color:#fff;}
    .kg-lead_card--pink .kg-lead_card__title--border{border-bottom:2px solid #fff;}

    .kg-lead_card--red  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--red .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--red .kg-lead_card__body{color:#fff;}
    .kg-lead_card--red .kg-lead_card__title{color:#fff;}
    .kg-lead_card--red .kg-lead_card__title--border{border-bottom:2px solid #fff;}

    .kg-lead_card--black  .material-icons {color:rgba(55,55,55,0.5);}
    .kg-lead_card--black  .kg-lead_card__body_actions_item_text {color:rgba(55,55,55,0.7);}
    .kg-lead_card--black .kg-lead_card__body{color:#fff;}
    .kg-lead_card--black .kg-lead_card__title{color:#fff;}
    .kg-lead_card--black .kg-lead_card__title--border{border-bottom:2px solid #fff;}


    .kg-more_lead_btn{
        position: fixed;
        margin: auto;
        left: 200px;
        right: 0px;
        top: 20px;
        /* bottom: 70%; */
        height: 45px;
        /* width: 175px; */
        display: none;
        font-weight: 700;
    }

    .kg-search_filter__block{
        position: fixed;
        /**width: 400px;*/
        z-index: 1;
        margin: 20px 0px 0px 135px;
    }

    @media only screen and (min-width: 768px) {
        #card_container {
            margin-left:300px;
        }
    }


    @media only screen and (max-width: 991px) {
        .kg-search_filter__block{
            position: fixed;
            /**width: 400px;*/
            z-index: 1;
            margin: 8px 0px 0px 15px;
        }
        .page-wrapper{margin-top: 57px;}

        /** .gm-iv-address {
             margin-top: 60px;
         }
 
         .gm-iv-address-link{
             margin-top: 40px;
         }
 
         .gm-iv-container,.gm-iv-marker{
             margin-top: 60px;
         }*/

        .kg-more_lead_btn{
            position: fixed;
            margin: auto;
            left: 15px;
            right: 0px;
            top: 115px;
            /* bottom: 70%; */
            height: 45px;
            /* width: 175px; */
            display: none;
            font-weight: 700;
        }   
    }

    .kgsticky-toolbar {
        width: 46px;
        position: fixed;
        top: 0;
        right: 0;
        list-style: none;
        padding: 5px 0;
        margin: 0;
        z-index: 50;
        background: #fff;
        -webkit-box-shadow: 0px 0px 50px 0px rgba(82,63,105,0.15);
        box-shadow: 0px 0px 50px 0px rgba(82,63,105,0.15);
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        border-radius: 3px 0 0 3px;
    }

    .kgsticky-toolbar .kgsticky-toolbar__item {
        margin: 0;
        padding: 5px;
        text-align: center;
    }

    .kg_lead_card__close_btn{ 
        cursor:pointer;
    }

    .dropdown.dropdown-lg .dropdown-menu {
        margin-top: -1px;
        padding: 6px 20px;
        width: 350px;
    }
    .input-group-btn .btn-group {
        display: flex !important;
    }
    .btn-group .btn {
        border-radius: 0;
        margin-left: -1px;
    }
    .btn-group .btn:last-child {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }
    .btn-group .form-horizontal .btn[type="submit"] {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    .form-horizontal .form-group {
        margin-left: 0;
        margin-right: 0;
    }
    .form-group .form-control:last-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    @media screen and (min-width: 768px) {
        #adv-search {
            width: 500px;
            margin: 0 auto;
        }
        .dropdown.dropdown-lg {
            position: static !important;
        }
        .dropdown.dropdown-lg .dropdown-menu {
            min-width: 500px;
            margin-left:130px;
        }
    }

    .form-block{margin-bottom: 0px !important;}
    .dropzone{
        border-radius: 10px;
    }

    .dropzone .dz-message {
        text-align: center;
        margin: 3em 0;
    }

    .floating_btn--right {
        font-size: 12px;
        text-align: center;
        position: fixed;
        right: 50px;
        bottom: 50px;
        transition: all 0.1s ease-in-out;
    }

    .floating_btn--right:hover {
        box-shadow: 0 6px 14px 0 #666;
        transform: scale(1.05);
    }

    .btn{-webkit-border-radius:0px;}

    .btn_filter{width:45px;height:45px;padding:5px;}

    @media only screen and (min-width: 992px){
        .modal-content {
            width: 450px;
        }
    }
</style>
<div class="page-wrapper d-block clearfix">
    <!--<ul class="kgsticky-toolbar" style="margin-top: 30px;">
        <li class="kgsticky-toolbar__item"  title="" data-placement="left" data-original-title="Filter">
                <a href="javascript:void(0);"><i class="fa fa-cog"></i></a>
        </li>
    </ul>-->
    <div class="kg-search_filter__block">
        <a href="javascript:void(0);" class="btn btn-primary text-white btn_filter" data-toggle="modal" data-target="#deal_map_filter_modal"><span class="fa fa-filter"></span></a>
    </div>
    <div id="map"></div>
    <div class="card-sc">
        <div class="card-group" id="card_container">
        </div>
        <div class="card-group" id="card_container_placeholder">
            <div class="placeholder__card card">
                <div class="bars">
                    <div class="bar bar1 loading"></div>
                    <div class="bar bar2 loading"></div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary kg-more_lead_btn" id="load_more_btn">Load More Deals</button>
    </div>

    <div id="infowindow-content">
        <img id="place-icon" src="" height="16" width="16">
        <span id="place-name"  class="title"></span><br>
        <span id="place-address"></span>
        <div id="place-add-deal" class="mt-3"></div>
    </div>

    <div class="d-block clearfix">
        <form role="form"  action="" method="post" name="customerAdd" id="customerAdd" enctype="multipart/form-data">
            <input type="hidden" name="cust_id" id="company_cust_id">

            <input type="hidden" id="bussiness_name" name="company[company_name]" class="form-control kg-form_field--orange" placeholder="Business Name"  required >

            <input type="hidden" name="company[last_name]" id="lastname"  class="form-control" placeholder="Last Name"  required>

            <input type="hidden" name="company[customer_contact_no]" id="contactMobilePhone"  class="form-control kg-form_field--orange" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required>

            <input type="hidden" id="position" name="company[position]" class="form-control" placeholder="Position"  required >

            <input type="hidden" name="company[customer_email]" id="contactEmailId"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>

            <input type="hidden" name="site[latitude]" id="locationLatitude" />

            <input type="hidden" name="site[longitude]" id="locationLongitude" />

            <input class="locationstreetAddressVal" type="hidden" id="locationstreetAddressVal" name="site[address]" />

            <select name="site[state_id]" id="locationState" class="form-control locationState" required>
                <option value="">Select State</option>
                <?php for ($j = 0; $j < count($states); $j++) { ?>
                    <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                <?php } ?>
            </select>

            <input class="form-control locationPostCode " placeholder="Please Enter Post Code" type="hidden" name="site[postcode]" id="locationPostCode" value=""  required />
        </form>
    </div>
</div>

<div class="modal fade" id="deal_map_filter_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title" id="add_customer_location_modal_title">Filters</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="input-group">  
                                    <select id="search_filter_by" style="height: 45px;" class="form-control border-secondary border-right-0 rounded-0">
                                        <option value="cust.company_name">Search By Business </option>
                                        <option value="cl.address">Search By Address </option>
                                        <!-- <option value="cl.state_name">Search By State </option> -->
                                    </select>
                                    <input id="search_filter" class="form-control border-secondary rounded-0" type="search" placeholder="Search Keyword..">  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select data-placeholder="Select Filter Type" id="filter_by_type" multiple="multiple">
                                            <?php
                                            $types = unserialize(LEAD_TYPE);
                                            if (!empty($types)) {
                                                foreach ($types as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select data-placeholder="Select Filter Stage"  id="filter_by_stage" multiple="multiple">
                                            <?php
                                            if (!empty($lead_stages)) {
                                                foreach ($lead_stages as $lead) {
                                                    ?>
                                                    <option value="<?php echo $lead['id']; ?>"><?php echo $lead['stage_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select data-placeholder="Select Filter Segment"  id="filter_by_segment" multiple="multiple">
                                            <?php
                                            $segments = unserialize(LEAD_SEGMENT);
                                            if (!empty($segments)) {
                                                foreach ($segments as $key => $value) {
                                                    if ($key == 4) {
                                                        //continue;
                                                    }
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select data-placeholder="Select Filter User"  id="filter_by_user" multiple="multiple">
                                            <?php
                                            if (!empty($lead_allocation_users)) {
                                                foreach ($lead_allocation_users as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary border-secondary border-right-0 rounded-0" id="search_filter_btn"><i class="fa fa-search"></i></button>
                            <button type="button" class="btn btn-primary border-secondary border-right-0 rounded-0 float-right" id="load_nearby_deals"> <i class='fa fa-circle-o-notch fa-spin hidden'></i>  Nearby </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url(); ?>assets/dropzone/dropzone.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing,geocode" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/placecomplete.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/marker_cluster.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script src='<?php echo site_url(); ?>common/js/activity.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/lead_map.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="https://cdn.rawgit.com/wenzhixin/multiple-select/e14b36de/multiple-select.js"></script>
<script>
    var ac_context = {};
    ac_context.view = 'map';
    ac_context.lead_data = '';
    var activity_manager_tool = new activity_manager(ac_context);

    var context = {};
    context.user_type = '<?php echo isset($user_details) ? $user_details['group_id'] : ''; ?>';
    context.lead_stages = '<?php echo (isset($lead_stages) && !empty($lead_stages)) ? json_encode($lead_stages) : ''; ?>';
    context.lead_types = '<?php echo json_encode(unserialize(LEAD_TYPE)); ?>';
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.search_log_data = '<?php echo (isset($search_log_data) && !empty($search_log_data)) ? json_encode($search_log_data, JSON_HEX_APOS) : ''; ?>';
    context.logged_in_user_data = '<?php echo (isset($logged_in_user_data) && !empty($logged_in_user_data)) ? json_encode($logged_in_user_data, JSON_HEX_APOS) : ''; ?>';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    var lead_map_manager_tool = new lead_map_manager(context);
    lead_map_manager_tool.initialize_map();
</script>

</body>
</html>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" alt="" width="100%" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:14px;">House Frontage</th>
                                <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px;">Proposed PV array location (allow 1.5 meters from edges) Near Map design</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->business_frontage != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->business_frontage; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->proposed_array_location != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->proposed_array_location; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height:40px;">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:14px;">Building from above photo showing location points of MSB, INVERTER, ACCESS POINT, COMMS ROOM and distances Using Skitch</th>
                                <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Calibri, sans-serif; padding:5px; font-size:14px;">Proposed inverter location photo showing wall measurement in meters Using Skitch</th>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->array_access_point != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->array_access_point; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="391" height="290" />
                                    <?php } ?>
                                </td>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if ($booking_form_image->proposed_inverter_location != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/' . $booking_form_image->proposed_inverter_location; ?>" width="391" height="290" />
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/no_image.jpg'; ?>" width="391" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="120">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />5</td>
                </tr>
            </table></td>
    </tr>
</table>
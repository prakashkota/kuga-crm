<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<style>
    .kg-page__heading{display:flex;width: 100%;}
    .kg-page__heading_title{
        font-size: 16px;
        font-weight: 500;
        margin-bottom: .5rem;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.2;
        color: inherit;
        padding: 5px 0 15px;
    }
    .kg-page__heading_action_left{margin-left:10px;}
</style>
<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="kg-page__heading">
            <div class="kg-page__heading_title">
                <?php echo (isset($meta_title)) ? $meta_title : 'Manage Activity'; ?>
            </div> 
            <?php if (isset($is_sales_rep) && $is_sales_rep == FALSE) { ?>
                <div class="kg-page__heading_action_left">
                    <select class="form-control" name="userid" id="userid" style="width:150px;">
                        <option value="">Select User</option>
                        <?php foreach ($users as $key => $value) { ?>
                            <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            <?php } ?>
        </div>
        <?php if (isset($is_google_account_connected) && $is_google_account_connected == 'false') { ?>
            <div class="alert alert-primary" style="float:left" role="alert">
                    <i class="fa fa-info-circle"></i>  Connect <a href="<?php echo site_url('admin/settings/gcal'); ?>">Google Calendar</a> in order to sync activity data.
            </div>
        <?php } ?>
        <div class="columns columns-right btn-group pull-right">
            <a class="btn btn-default" href="<?php echo site_url('admin/activity/manage?view=calendar'); ?>">
                <i class="fa fa-calendar"></i>
            </a>
        </div>
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="activity_table" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th>Activity Edit</th>
                                        <th>Date Due</th>
                                        <th>Start Time</th>
                                        <th>Duration</th>
                                        <th>Activity Type</th>
                                        <th>Owner</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Phone Number</th>
                                        <th>Activity Status</th>
                                    </tr>
                                </thead>
                                <tbody id="activity_table_body">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div> 

<!-- CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<!-- SCRIPTS -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" ></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/activity.js?v=<?php echo version; ?>"></script>
<script type='text/javascript'>
    var context = {};
    context.view = 'list';
    context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
    var activity_manager_tool = new activity_manager(context);
    function initMap() {}
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry,drawing&callback=initMap"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js' type='text/javascript'></script>
</body>
</html>

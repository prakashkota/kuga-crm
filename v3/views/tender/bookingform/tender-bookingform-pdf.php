<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tender Booking Form</title>
    <style>
        @media print {
            .no-print {
                display: none;
            }

            body {
                background: transparent;
            }

            .black-bg {
                background-color: #010101
            }
            img {
                max-width: 100%;
                height: auto;
                display: inline-block;
            }
            .red {
                color: #c32027
            }

        }

    </style>
</head>
<body style="padding:0; margin:0">
<table align="center"><tr><td style="padding-top: 20px;">
        <table  width="910" class="pdf_page"  border="0" align="center" cellpadding="0" cellspacing="0">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Business Details</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Company Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                        <?php echo @$business_details['company_name']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">ABN/ACN:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$business_details['abn']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Trading Name:</td>
                                                        <td width="283"  style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><?php echo @$business_details['trading_name']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Leased or Owned:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$business_details['leased_or_owned']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Address:</td>
                                            <td width="708"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php echo @$business_details['address']; ?>
                                                </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Project Manager</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['projectManager']['first_name']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['projectManager']['last_name']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['projectManager']['position']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['projectManager']['email']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><?php echo @$contacts['projectManager']['contact_no']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><?php echo @$contacts['projectManager']['landline']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Site Contact 1</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['first_name']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['last_name']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['position']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['email']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['contact_no']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact1']['landline']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Site Contact 2</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['first_name']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['last_name']; ?>
                                                           </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['position']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['email']; ?>
                                                           </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['contact_no']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['siteContact2']['landline']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="100">&nbsp;</td>
                </tr>
                
            </table>
        </td></tr><tr><td style="padding-top: 20px;">
            <table  width="910" class="pdf_page"  border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Accounts</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['first_name']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['last_name']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['position']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['email']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['contact_no']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['accounts']['landline']; ?>
                                                           </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Contracts Administrator</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['first_name']?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['last_name']; ?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['position']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['email']; ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['contact_no']; ?></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contacts['contractsAdministrator']['landline']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Contract Information</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Start Date:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['start_date'];?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Completion Date:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['completion_date'];?>
                                                            </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td >
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Progress Claims Terms:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['claims_terms']?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Payment Terms:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['payment_schedule']?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Liquidated Damages $/Day:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['liquidated']?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Defect Liability Period:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['defect_liability_period']?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td >
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Contract Amount:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['contract_amount']?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>

                            <tr>
                                <td height="10"></td>
                            </tr>

                            <tr>
                                <td >
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="150" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><input type="checkbox" id="retention" <?php if(@$contractInformation['retention']=='on'){?> checked="checked" <?php } ?> name="contractInformation[retention]"/>&nbsp;Retention&nbsp;Amount&nbsp;</td>
                                                        <td width="255" id="retentionTd"  style="<?php  if(@$contractInformation['retention']!='on'){ ?> display: none; <?php } ?> font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['retention_info']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-right:10px">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="150" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><input id="bank" type="checkbox" <?php if(@$contractInformation['bank']=='on'){?> checked="checked" <?php } ?> name="contractInformation[bank]"/>&nbsp;Bank&nbsp;Guarantee&nbsp;</td>
                                                        <td width="255" id="bankTd"  style=" <?php if(@$contractInformation['bank']!='on'){ ?> display: none; <?php } ?> font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$contractInformation['bank_info']; ?>

                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="405" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Electricity Bill Information <input type="radio" <?php if(@$contractInformation['eb_information'] == 'yes') { echo "checked='checked'"; } ?> name="contractInformation[eb_information]" value="yes" /> &nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if(@$contractInformation['eb_information'] == 'no') { echo 'checked'; } ?> type="radio" name="contractInformation[eb_information]" value="no"/> &nbsp;No&nbsp;&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-right:10px">&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php if(@$contractInformation['eb_information'] == 'yes'){?>
                <tr id="edInformationSection1">
                    <td>&nbsp;</td>
                </tr>
                <tr id="edInformationSection2">
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tbody><tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Electricity Bill Information</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Retailer:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$electricity_bill['ed_retailer']?>
                                                           </td>
                                                    </tr>
                                                    </tbody></table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">NMI:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$electricity_bill['eb_nmi'] ?></td>
                                                    </tr>
                                                    </tbody></table></td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td id="eb_information_section" ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Distributor:</td>
                                                        <td width="283"  style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$electricity_bill['eb_distributor']?>
                                                            </td>
                                                    </tr>
                                                    </tbody></table></td>
                                            <td width="415"  style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Meter Number:</td>
                                                        <td width="283"  style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$electricity_bill['eb_meter_no']?>
                                                            </td>
                                                    </tr>
                                                    </tbody>
                                                    </tbody></table></td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>

                            </tbody></table></td>
                </tr>
                <?php } ?>
                <tr>
                    <td height="100">&nbsp;</td>
                </tr>
                
            </table>
        </td></tr><tr><td style="padding-top: 20px;">
            <?php if (@$contractInformation['eb_information'] == 'yes'){ ?>
            <table width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td ><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Front</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height='450px' align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['ec_bill_1'] ==''){?>
                                                    <div  class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['ec_bill_1'] !='') { ?>
                                                <div class="form-block d-block clearfix image_container">
                                                    <img
                                                         src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['ec_bill_1']}"); ?>" />

                                                    </div>

    <?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Back</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="450px" align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['ec_bill_2'] == ''){?>
                                                    <div style="width: 640px !important; height:307px !important;"  class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['ec_bill_2'] !='') { ?>
                                                <div style="width: 640px !important; height:307px !important;" class="form-block d-block clearfix image_container" >
                                                    <img
                                                         src = "<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['ec_bill_2']}"); ?>" />
                                             </div>
<?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="100">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>

            </table>
        </td></tr><tr><td style="padding-top: 20px;">
            <?php } ?>
            <table  width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <?php
                        $productTypes = array(
                            'systemSize'=>'System Size',
                            'panel'=>'Solar Panels',
                            'inverter'=>'Inverters',
                            'battery'=>'Battery',
                            'roofType'=>'Roof Type',
                            'roofbrand'=>'Roof Brand/Style',
                            'rackingType'=>'Racking Type',
                            'rackingBrand'=>'Racking Brand',
                            'minimumTiltDegree'=>'Minimum Tilt Degree',
                            'perlingSpaces'=>'Perling Spaces',
                            'switchboard'=>'Switchboard'
                        );
                        ?>
                        <table id="booking_items" width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="122" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Product Type</th>
                                <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">System</th>
                                <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                                <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                                <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                            </tr>


                            <?php foreach (@$product as $key=>$value) { ?>
                                <tr>
                                    <td height="40" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <?php if (!empty($product)) {
                                            echo @$product[$key]['product_type'];
                                        } ?>
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <?php if (!empty($product)) {
                                            echo @$product[$key]['product_name'];
                                        } ?>
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <?php echo @$product[$key]['product_qty']; ?>
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <?php echo @$product[$key]['product_cost_excGST']; ?>
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <?php echo @$product[$key]['product_total_cost_excGst']; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table></td>
                </tr>
                <tr>
                    <td ><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                                <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                                <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <?php if (!empty($booking_form)) {
                                        echo @$booking_form['total_excGst'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" >
                                    <table width="557" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Is there a forklift on site?</td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <?php echo @$booking_form['total_Gst']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <table width="557" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="345" >
                                                <input id="is_forklift_on_site_yes" <?php if($booking_form['is_forklift_on_site']){ echo 'checked'; } ?> name="booking_form[is_forklift_on_site]" value="1" style="padding:5px; width:20px; height: inherit;" type="radio"> YES
                                                <input id="is_forklift_on_site_no" <?php if(!$booking_form['is_forklift_on_site']){ echo 'checked'; } ?>  name="booking_form[is_forklift_on_site]" value="0" style="padding:5px; width:20px; height: inherit;" type="radio"> NO
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <?php echo @$booking_form['total_incGst']; ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr><tr>
                    <td>&nbsp;</td>
                </tr><tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Array Details</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415"  style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Height:</td>
                                                        <td width="283"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$booking_form['roof_height']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415"  style="padding-left:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td ><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="40"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access&nbsp;Equipments:</td>
                                                        <td width="665"  style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <?php echo @$booking_form['access_equipments']; ?>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>

                        </table>
                    </td>
                </tr>

            </table>
        </td></tr><tr><td style="padding-top: 20px;">
            <table  width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Inverter / Grid Protection Location</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="350px" align="center"  style="padding:13px 0">
                                               <?php if(@$booking_form_image['inverterGridProtection'] ==''){?>
                                                <div class="add-picture">
                                                    <span> </span>
                                                    <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                 </div>
                                                <?php } else if(@$booking_form_image['inverterGridProtection'] !='') { ?>
                                                <div class="form-block d-block clearfix image_container" >
                                                    <img width="391" height="260" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['inverterGridProtection']}")?>"
                                                    />

                                                </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>AC Run</strong></td>
                                        </tr>
                                        <tr>
                                            <td height="350px" width="830" align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['acRun'] ==''){?>
                                                    <div class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['acRun'] !='') { ?>
                                                <div class="form-block d-block clearfix image_container" >
                                                    <img width="391" height="260" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['acRun']}")?>" />
                                                </div>
<?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td height="100px" width="830" align="center"  style="padding:0px 0">
                                                <?php echo @$notes['acRun']; ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>

            </table>
        </td></tr><tr><td style="padding-top: 20px;">
            <table  width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Connection to Switchboard</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="350" align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['connectionToSwitchBoard'] ==''){?>
                                                    <div class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                        </div>
                                                <?php } else if(@$booking_form_image['connectionToSwitchBoard'] !='') { ?>
                                                    <div style="width: 640px !important; height:307px !important;"  class="form-block d-block clearfix image_container" >
                                                        <img width="391" height="260" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['connectionToSwitchBoard']}") ?>"
                                                        />

                                                    </div>
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Additional Photo</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height='350' align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['additionPhoto'] ==''){?>
                                                    <div class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['additionPhoto'] !='') { ?>
                                                    <div class="form-block d-block clearfix image_container">
                                                        <img width="391" height="260" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['additionPhoto']}"); ?>" />
                                                    </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="100px" align="center"  style="padding:0px 0">
                                                <?php echo @$notes['additionPhoto']; ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>

            </table>
        </td></tr>
    <?php if(count($booking_form_image['more'])>0){?>
    </td></tr><tr><td style="padding-top: 20px;">
            <table  width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="10">
                            <?php
                            foreach($booking_form_image['more'] as $eachImage) {
                                $i = 1;
                                while ($i<=2){
                                $nameIdx="name{$i}";
                                $imageIdx="image{$i}";
                                ?>
                                <tr >
                                    <td  class="add-picture"  style="padding:13px 0;margin: 10px 0px; "><table width="830"  border="1" align="center" cellpadding="0" cellspacing="0"  style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                            <tr>
                                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong><?php echo $eachImage[$nameIdx]; ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="830" height='400px' align="center" >
                                                    <?php if(@$eachImage[$imageIdx] ==''){?>
                                                    <div class="add-picture">
                                                            <span> </span>
                                                            <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                    <?php } else if(@$eachImage[$imageIdx] !='') { ?>
                                                        <div class="form-block d-block clearfix image_container">
                                                            <img width="390" height="260" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$eachImage[$imageIdx]}")?>"  />
                                                        </div>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php $i++;} } ?>
                        </table></td>
                </tr>

            </table>
            <?php }
            ?>
    <tr><td style="padding-top: 20px;">
            <table  width="910" class="pdf_page" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Panel Layout</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="350px" align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['panelPalyout'] ==''){?>
                                                    <div class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['panelPalyout'] !='') { ?>
                                                    <div class="form-block d-block clearfix image_container">
                                                        <img height="261" width="390" src="<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['panelPalyout']}"); ?>"
                                                        />
                                                     </div>
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>DC Run</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="350px" align="center"  style="padding:13px 0">
                                                <?php if(@$booking_form_image['dcRun'] ==''){?>
                                                    <div class="add-picture">
                                                        <span> </span>
                                                        <img src="<?php echo site_url('assets/images/small-plus.png'); ?>" />
                                                    </div>
                                                <?php } else if(@$booking_form_image['dcRun'] !='') { ?>
                                                    <div class="form-block d-block clearfix image_container">
                                                        <img height="260" width="391" src = "<?php echo site_url("assets/uploads/tender_booking_form_files/{$booking_form_image['dcRun']}")?>"
                                                        />
                                                    </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" height="100px" align="center"  style="padding:0px 0">
                                                <?php echo @$notes['dcRun']?>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                
            </table>

        </td></tr></table>

</body>
</html>
<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_02.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Front</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="ec_bill_1" />
                                        <input type="hidden" id="ec_bill_1" name="booking_form_image[ec_bill_1]" required="" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Back</strong></td>
                            </tr>
                            <tr>
                                <td width="830" align="center" valign="top" style="padding:13px 0">
                                    <div class="add-picture">
                                        <span>Add picture</span>
                                        <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                        <input type="file" class="image_upload" data-id="ec_bill_2" />
                                        <input type="hidden" id="ec_bill_2" name="booking_form_image[ec_bill_2]" />
                                    </div>
                                    <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                        <div class="add-picture"></div>
                                        <a class="image_close" href="javscript:void(0);"  ></a>
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
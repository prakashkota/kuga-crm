<!-- SIDEBAR STUFF -->
<div class="col-lg-4 padding-20">
    <div class="row margin-bottom-30">
        <div class="col-lg-12 ">
            <div class="support-container">
                <h2 class="support-heading">Need more Support?</h2>
                If you cannot find an answer in the knowledgebase, you can <a href="#">contact us</a> for
                further help.</div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="col-lg-12">
            <div class="fb-heading-small">Latest Articles</div>
            <hr class="style-three">
            <div class="fat-content-small padding-left-10">
                <ul>

                    <?php
                    if (!empty($latest_articles)) {
                        foreach ($latest_articles as $key => $value) {
                            ?>
                            <li> <a href="<?php echo site_url('admin/knowledgebase/'.$value['category_slug'].'/'.$value['article_slug']); ?>"><i class="fa fa-file-text-o"></i> <?php echo $value['article_title']; ?></a></li>
                                <?php
                            }
                        } else {
                            echo "No Articles Found";
                        }
                        ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END SIDEBAR STUFF -->
</div>
</div>
<!-- END MAIN SECTION -->
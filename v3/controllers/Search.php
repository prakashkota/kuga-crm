<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Search Controller
 * @version 1.0
 */
class Search extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->helper("custom_email_helper");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function manage() {
        $data = array();
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('search/main');
        $this->load->view('partials/footer');
    }

    public function search_users() {
        $data = array();
        $search_keyword = $this->input->get('search_keyword');
        $id = $this->input->get('id');
        if (isset($search_keyword) && $search_keyword != '') {
            $data['success'] = TRUE;
            $search_keyword = str_replace("@", " ", $search_keyword);
            $search_keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $search_keyword);
            $data['user_list'] = $this->customer->get_customer_by_filter($search_keyword);
        } else if (isset($id) && $id != '') {
            $data['success'] = TRUE;
            $data['user_list'] = $this->customer->get_customer_by_filter(FALSE, $id);
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    /*     * public function save_search_activity_data() {
      $data = array();
      $cust_data = $this->input->post('cust_data');
      $activity_note = $this->input->post('activity_note');
      $activity_id = $this->input->post('activity_id');
      if (isset($activity_id) && $activity_id != '') {
      $update_data = array();
      $update_data['note'] = $activity_note;
      $stat = $this->common->update_data('tbl_search_activity', array('id' => $activity_id), $update_data);
      if ($stat) {
      $data['success'] = TRUE;
      $data['status'] = 'Activity updated successfully';
      $data['activity_id'] = $activity_id;
      } else {
      $data['status'] = 'Looks like something went wrong.';
      $data['success'] = FALSE;
      }
      } else if (isset($cust_data) && $cust_data != '') {
      $insert_data = array();
      $insert_data['user_id'] = $cust_data['user_id'];
      $insert_data['cust_id'] = $cust_data['cust_id'];
      $insert_data['created_at'] = date('Y-m-d H:i:s');
      $insert_data['note'] = $activity_note;
      $stat = $this->common->insert_data('tbl_search_activity', $insert_data);
      $id = $this->common->insert_id();
      $communication_data = array();
      $communication_data['name'] = $cust_data['name'];
      $communication_data['postcode'] = $cust_data['postcode'];
      $communication_data['address'] = $cust_data['address'];
      $communication_data['contact_no'] = $cust_data['contact_no'];
      $communication_data['activity_note'] = $activity_note;
      $communication_data['franchise_name'] = $cust_data['franchise_name'];
      $communication_data['franchise_email'] = $cust_data['franchise_email'];
      send_customer_leademail_to_franchise($communication_data);
      if ($stat) {
      $data['success'] = TRUE;
      $data['activity_id'] = $id;
      $data['status'] = 'Activity saved successfully, An Email has been sent to '.$cust_data['franchise_name'];
      } else {
      $data['status'] = 'Looks like something went wrong.';
      $data['success'] = FALSE;
      }
      } else {
      $data['success'] = FALSE;
      $data['status'] = 'Invalid Request';
      }
      echo json_encode($data);
      die;
      }* */

    //Previously we where not converting search to lead 
    //Now it is being done
    public function save_search_activity_data() {
        $data = array();
        $cust_data = $this->input->post('cust_data');
        $activity_note = $this->input->post('activity_note');
        $uuid = $this->input->post('uuid');
        if (!empty($this->input->post())) {
            $is_lead_exists = $this->common->fetch_where('tbl_leads', '*', array('uuid' => $uuid));
            $lead_data['user_id'] = 1;
            $lead_data['cust_id'] = $cust_data['cust_id'];
            $lead_data['created_at'] = date('Y-m-d H:i:s');
            $lead_data['note'] = $activity_note;
            if (count($is_lead_exists) > 0) {
                $lead_data['updated_at'] = date('Y-m-d H:i:s');
                $stat = $this->common->update_data('tbl_leads', array('uuid' => $uuid), $lead_data);
                $id = $is_lead_exists[0]['id'];
            } else {
                $lead_data['uuid'] = $uuid;
                $lead_data['created_at'] = date('Y-m-d H:i:s');
                $stat = $this->common->insert_data('tbl_leads', $lead_data);
                $id = $this->common->insert_id();

                $lead_user_insert_data = array();
                $lead_user_insert_data['lead_id'] = $id;
                $lead_user_insert_data['user_id'] = $cust_data['user_id'];
                $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);

                $communication_data = array();
                $communication_data['name'] = $cust_data['name'];
                $communication_data['postcode'] = $cust_data['postcode'];
                $communication_data['address'] = $cust_data['address'];
                $communication_data['contact_no'] = $cust_data['contact_no'];
                $communication_data['activity_note'] = $activity_note;
                $communication_data['franchise_name'] = $cust_data['franchise_name'];
                $communication_data['franchise_email'] = $cust_data['franchise_email'];
                send_customer_leademail_to_franchise($communication_data);
            }

            if ($stat) {
                $data['success'] = TRUE;
                $data['status'] = 'Deal Saved Successfully, An Email has been sent to ' . $cust_data['franchise_name'];
            } else {
                $data['status'] = 'Looks like something went wrong.';
                $data['success'] = FALSE;
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

}

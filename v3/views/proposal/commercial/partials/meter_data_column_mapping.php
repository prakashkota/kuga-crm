<div class="modal" tabindex="-1" role="dialog" id="meter_data_column_mapping">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <ul class="nav nav-tabs" id="meterdataTab" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active show" id="column-mapping-tab" data-toggle="pill" href="#column-mapping" role="tab" aria-controls="column-mapping" aria-selected="false">Column Mapping</a>
          </li>
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="data-extrapolation-tab" data-toggle="pill" href="#data-extrapolation" role="tab" aria-controls="data-extrapolation" aria-selected="false">Data Extrapolation</a>
          </li>
        </ul>
        <div class="d-flex">
          <button type="button" class="btn btn-primary mr-2" id="saving_column_mapping_btn">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
      <div class="modal-body">

        <div class="tab-content" id="meterdataTabContent">
          <div class="tab-pane fade show active  table-responsive" id="column-mapping" role="tabpanel" aria-labelledby="column-mapping-tab">
            <form id="meter_data_column_mapping_form">
              <table class="table table-bordered" id="meter_data_column_mapping_table" style="table-layout: fixed;"></table>
            </form>
          </div>

          <div class="tab-pane fade " id="data-extrapolation" role="tabpanel" aria-labelledby="data-extrapolation-tab">
            <div id="data_extrapolation_table"> </div>
          </div>
        </div>

    </div>
  </div>
</div>
<link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/fileinput.min.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
<script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
<script src="<?php echo site_url(); ?>common/js/sp_meter_data.js?v=<?php echo version; ?>" type="text/javascript"></script>
<script>
  var context = {};
  context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
  context.proposal_data = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? json_encode($proposal_data, JSON_HEX_APOS) : ''; ?>';
  new sp_meter_data_manager(context);
</script>
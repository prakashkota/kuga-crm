<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;}
    .mask{    margin-left: -15px;}
    #proposal_image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 30px;
        right: 2px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }


    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #000 !important;
        opacity: 1; /* Firefox */
    }

    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }

    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }

    .remove_field{
        color:red;
    }

    .monthly-event-list{
        top:22.5em !important ;
        height:calc(100% - 25.5em) !important;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }
    .steps li {
        display: inline-block;
        margin-bottom: 3px;
    }
    .steps li a, .steps li p {
        background: #e5f4fd;
        padding: 8px 20px;
        color: #0077bf;
        display: block;
        font-size: 14px;
        font-weight: bold;
        position: relative;
        text-indent: 12px;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li a:before, .steps li p:before {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #fff;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        left: 0;
        top: 50%;
        width: 0;
        margin-top: -18px;
    }
    .steps li a:after, .steps li p:after {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #e5f4fd;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        /*right: -12px;*/
        left:100%;
        top: 50%;
        width: 0;
        margin-top: -18px;
        z-index: 1;
    }
    .steps li.active a, .steps li.active p {
        background: #5cb85c;
        color: #fff;
    }
    .steps li.active a:after, .steps li.active p:after {
        border-left: 12px solid #5cb85c;
    }
    .steps li.undone a, .steps li.undone p {
        background: #eee;
        color: #333;
    }
    .steps li.undone a:after, .steps li.undone p:after {
        border-left: 12px solid #eee;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }

    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }

    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }

    .sr-no_wrap{
        white-space: nowrap;
    }

    .sr-inactive_user{
        color:red;
    }

    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }

    .card_body__title h2{
        display:inline-flex;
        padding-bottom: 10px;
    }

    .form-block{margin-bottom: 0px !important;}

    .kg-activity{
        border-left-color: #f0ad4e;
        border: 1px solid #f0ad4e;
        border-left-width: .25rem;
        border-radius: .25rem;
    }

    .kg-activity__description{
        background-color:#ffffe0;
        margin: -12px;
        padding: 12px;
    }

    .card_row_1{
        min-height: 429px;
    }

    .sr-switch {
        font-size: 1rem;
        position: relative;
    }
    .sr-switch input {
        position: absolute;
        height: 1px;
        width: 1px;
        background: none;
        border: 0;
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        overflow: hidden;
        padding: 0;
    }
    .sr-switch input + label {
        position: relative;
        min-width: calc(calc(2.375rem * .8) * 2);
        border-radius: calc(2.375rem * .8);
        height: calc(2.375rem * .8);
        line-height: calc(2.375rem * .8);
        display: inline-block;
        cursor: pointer;
        outline: none;
        user-select: none;
        vertical-align: middle;
        text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
    }
    .sr-switch input + label::before,
    .sr-switch input + label::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: calc(calc(2.375rem * .8) * 2);
        bottom: 0;
        display: block;
    }
    .sr-switch input + label::before {
        right: 0;
        background-color: #dee2e6;
        border-radius: calc(2.375rem * .8);
        transition: 0.2s all;
    }
    .sr-switch input + label::after {
        top: 2px;
        left: 2px;
        width: calc(calc(2.375rem * .8) - calc(2px * 2));
        height: calc(calc(2.375rem * .8) - calc(2px * 2));
        border-radius: 50%;
        background-color: white;
        transition: 0.2s all;
    }
    .sr-switch input:checked + label::before {
        background-color: #08d;
    }
    .sr-switch input:checked + label::after {
        margin-left: calc(2.375rem * .8);
    }
    .switch input:focus + label::before {
        outline: none;
        box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
    }
    .sr-switch input:disabled + label {
        color: #868e96;
        cursor: not-allowed;
    }
    .sr-switch input:disabled + label::before {
        background-color: #e9ecef;
    }
    .switch.switch-sm {
        font-size: 0.875rem;
    }
    .sr-switch.switch-sm input + label {
        min-width: calc(calc(1.9375rem * .8) * 2);
        height: calc(1.9375rem * .8);
        line-height: calc(1.9375rem * .8);
        text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
    }
    .sr-switch.switch-sm input + label::before {
        width: calc(calc(1.9375rem * .8) * 2);
    }
    .sr-switch.switch-sm input + label::after {
        width: calc(calc(1.9375rem * .8) - calc(2px * 2));
        height: calc(calc(1.9375rem * .8) - calc(2px * 2));
    }
    .sr-switch.switch-sm input:checked + label::after {
        margin-left: calc(1.9375rem * .8);
    }
    .sr-switch + .sr-switch {
        margin-left: 1rem;
    }
    .tbl-as-inpt tr {
        width: 100%;
        display: block;
    }
    .tbl-as-inpt.table-default table tr td {
        display: block;
        width: 100%;
        padding: 0 !important;
        border: none;
        background: none;
        font-size: 15px;
    }
    .tbl-as-inpt.table-default table tr td .total-price {

        padding: 0;
        font-size: 12px;
        background-color: #ECEFF1;
        border: solid 1px #000;
        padding: 10px 8px;
        margin: 6px 0 15px 0;
        display: block;

    }

    @media only screen and (min-width: 992px){
        .modal-content{width:900px;}
        .modal-dialog{max-width:800px;}
    }

    @media only screen and (max-width: 991px){
        .modal-content{width:360px;}
    }

    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
    .state{width: 70px !important;}
    .card-body{margin-bottom: 0px;}
    #all_activity_container .card-body{margin-bottom: 0px;}
    .mask{    margin-left: -15px;}
    #proposal_image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 30px;
        right: 2px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .table-default table tr td{
        padding: 5px 5px 5px 5px !important;
    }


    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #000 !important;
        opacity: 1; /* Firefox */
    }

    form .form-control{
        border: 1px solid #A9A9A9;
        color:#000;
    }

    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }

    .remove_field{
        color:red;
    }

    .monthly-event-list{
        top:22.5em !important ;
        height:calc(100% - 25.5em) !important;
    }

    .deal_stages_list,.po_stages_list{ overflow:hidden; position:relative;}

    .deal_stages_list:after,.po_stages_list:after{ content:""; position:absolute; top:0; right:0; width:50px; bottom:0; background:#fff;}

    .steps {
        list-style-type: none;
        padding: 0;
        margin: 0 0 0 -8px;
    }
    .steps li {
        display: inline-block;
        margin-bottom: 3px;
        position: relative;
        width: 225px;
        text-align: center;
        transform: skewX(-20deg);
        margin-left: 2px;
    }
    .steps li span{
        transform: skewX(20deg);
        display:block;
    }
    .steps li a, .steps li p {
        background: #fed683;
        padding: 8px 20px;
        color: #313131;
        display: block;
        font-size: 14px;
        font-weight:600;
        position: relative;
        text-transform:uppercase;
    }
    .steps li a:hover, .steps li p:hover {
        text-decoration: none;
    }
    .steps li a:before, .steps li p:before {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #fff;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        left: 0;
        top: 50%;
        width: 0;
        margin-top: -18px;
        display:none;
    }

    .steps li.active a, .steps li.active p {
        background: #fec023;
        color: #000;
    }
    .steps li.active a:after, .steps li.active p:after {
        border-left: 12px solid #5cb85c;
    }
    .steps li.undone a, .steps li.undone p {
        background: #eee;
        color: #333;
    }
    .steps li.undone a:after, .steps li.undone p:after {
        border-left: 12px solid #eee;
    }
    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn{
        background: #ECEFF1;
        color:#000;
    }

    .sr-deal_btn__won:hover{
        background: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn__lost:hover{
        background: #d9534f;
        color:#fff;
    }

    .sr-deal_btn--won{
        background-color: #5cb85c;
        color:#fff;
    }

    .sr-deal_btn--lost{
        background-color: #d9534f;
        color:#fff;
    }

    .sr-no_wrap{
        white-space: nowrap;
    }

    .sr-inactive_user{
        color:red;
    }

    .select2-container{
        height:inherit !important;
        padding:inherit !important;
    }
    .kg-activity__item{
        border: 1px solid;
    }
    .kg-activity__item--default{
        border-color: #f0ad4e !important;
        box-shadow: inset 4px 0 0 0 #fff !important;
    }
    .kg-activity__item--green{
        border-color: rgba(0, 128, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #008000 !important;
    }
    .kg-activity__item--amber{
        border-color: rgba(255, 191, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffbf00 !important;
    }
    .kg-activity__item--pink{
        border-color: rgba(255, 192, 203, 0.24) !important;
        background-color: rgba(255, 192, 203, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ffc0cb !important;
    }
    .kg-activity__item--red{
        border-color: rgba(255, 0, 0, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #ff0000 !important;
    }
    .kg-activity__item--blue{
        border-color: rgba(49, 122, 226, 0.24) !important;
        box-shadow: inset 4px 0 0 0 #317ae2 !important;
    }
    .kg-activity__item__title{
        display:inline-flex;
        margin-bottom: 10px;
        font-size: 16px;
        font-weight: 500;
        margin: 0;
        padding: 0 0 15px;
        line-height: 1.2;
        color: inherit;
        font-family: 'Poppins', sans-serif;
        position: relative;
    }
    .kg-activity__item__description{
        background-color:#ffffe0;
        margin: -12px;
        padding: 12px;
        border-bottom-left-radius: 1.25rem;
        border-bottom-right-radius: 1.25rem;
    }
    .kg-activity__item__action{
        float:right;
        margin: 0;
        padding: 0;
    }
    .kg-activity__item__action:after {
        content: '\2807';
        font-size: 1.5em;
        color: #2e2e2e;
    }

    .facility-list{ margin:0; padding:0;}
    .facility-list ul{ margin:0; padding:0;display: block !important;}
    .facility-list ul li{ margin:0; padding:0; list-style:none; display:inline-block;}
    .facility-list ul li a{ display:block; background:#cfced2; color:#313131; font-size:14px; text-transform:uppercase; padding:8px 25px; font-weight:600;}
    .facility-list ul li a:hover, .facility-list ul li a.active{background:#b92625 !important; color:#fff !important;}
    .solar-only-list{ margin:10px 0 10px 0; padding:0; text-align:center;}
    .solar-only-list ul{ margin:0; padding:0;}
    .solar-only-list ul li{ margin:0 5px; padding:0; list-style:none; display:inline-block;}
    .solar-only-list ul li a{ display:block;color:#000; font-size:14px; text-transform:uppercase; padding:6px 35px; font-weight:600; border:solid 2px #b92625;} 
    .solar-only-list ul li a:hover, .solar-only-list ul li a.active, .facility-list ul li.active a{background:#b92625; color:#000;}
    .vic{ margin:15px 0 25px;}
    .vic span{ font-size:14px; color:#000; font-weight:700; border:solid 2px #e4e7e6; text-transform:uppercase; display:inline-block;padding:8px 15px}
    .vic a{ display:inline-block;padding:10px 15px; font-weight:700; text-transform:uppercase; color:#000; background:#e4e7e6;}
    .vic a:hover, .vic a.active{background:#b92625; color:#fff;}
    .tbl-box{ padding:20px;}
    .tbl-box table{ text-align:center;}
    .tbl-box table th{ background:#f0f0f0; font-size:14px; text-transform:uppercase; color:#000; font-weight:700; padding:10px 10px;border: 1px solid #d6d6d6;border-bottom: 1px solid #d6d6d6;}
    .tbl-box table td{padding:10px 10px;border: 1px solid #d6d6d6;}
    .inpt-typ{ text-align:center; border:none; text-align:center; outline:none;}
    .tbl-box-tow{padding:20px;}
    .tbl-box-tow h3{background:#b92625;text-align:center;text-transform:uppercase;color:#fff;font-size:20px;font-weight:700;padding:10px 0;}
    .tbl-box-tow table{text-align:center;}
    .tbl-box-tow table th{background:#646464;font-size:14px;text-transform:uppercase;color:#fff;font-weight:700;padding:10px 10px;border:1px solid #d1d1d1;border-bottom:1px solid #d1d1d1;}
    .tbl-box-tow table td{padding:10px 10px;border:1px solid #d1d1d1;background:#f0f0f0;font-size:14px;text-transform:uppercase;color:#000;font-weight:700;}
    .str-total .table.table-striped td{border:none;font-size:14px;color:#000;font-weight:600;padding:10px 20px;}
    @media only screen and (max-width:767px){
        .facility-list ul li{padding: 0 0 1px;display:block;}
        .solar-only-list ul li{margin: 0 0 5px;display:block;}
        .vic span{display:block;text-align:center;margin-bottom:5px;}
        .tbl-box-tow h3{font-size:16px;padding:10px;}
    }

    @media only screen and (min-width:767px){
        .facility-list ul li{
            width: 230px;
            text-align: center;
        }
    }


    .sr-action__btn{
        width: 285px;
        margin:2px auto;
    }

</style>

<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Residnetial Solar Proposal</h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" >
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/lead/add?deal_ref=' . $lead_data['uuid']) ?>"><i class="icon-arrow-left"></i> Back to Deal </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form method="post" action="" enctype="multipart/form-data" id="proposal_form">
                    <input type="hidden" id="proposal_id" />
                    <input type="hidden" id="proposal_deal_stage" />
                    <input type="hidden" id="proposal_deal_won_lost"  /> 
                    <input type="hidden" name="proposal[lead_type]" id="proposal_deal_type"  /> 
                    <input type="hidden" name="proposal[is_vic_rebate]" id="is_vic_rebate"  />
                    <input type="hidden" name="proposal[is_vic_loan]" id="is_vic_loan"  />
                    <input type="hidden" name="proposal[cust_id]" id="cust_id"  /> 
                    <input type="hidden" name="proposal[site_id]" id="site_id"  />

                    <div class="row hidden">
                        <div class="col-md-6 col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-12 cus-detail" style="display:inline-flex; padding:0;">
                                        <h2>Customer Details</h2>
                                        <a style="margin-left:20px;" id="addNewCompany" href="javascript:void(0);" ><i class="fa fa-plus"></i> Add New Customer</a>
                                        <a class="hidden" style="margin-left:20px;" id="editCompany" href="javascript:void(0);" ><i class="fa fa-pencil"></i> Edit Customer</a>
                                    </div>


                                    <div class="form-block d-block clearfix">
                                        <div class="form-group">
                                            <input type="text" name="customer_name" id="companyName" placeholder="Customer Name" class="form-control gray-bg" readonly="">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="customer_email" id="customer_email" placeholder="Customer Email" class="form-control gray-bg" readonly>
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" name="customer_phone" placeholder="Customer Phone" class="form-control gray-bg" readonly>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="customer_address" placeholder="Customer Address" class="form-control gray-bg" readonly>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="customer_postcode" id="customer_postcode" placeholder="Postcode" class="form-control gray-bg small" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>

                    
                    <div class="row po_stages_list_container hidden">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-block d-block clearfix">
                                        <div style="display:flex;">
                                            <h2>Purchase Order Stage</h2>
                                            <span class="help-block text-danger ml-3" >(<i class="fa fa-exclamation-circle"></i> Stage shown here is realtime from TED CRM)</span>
                                        </div>
                                        <div id="po_stages" class="po_stages_list">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="card">
                                        <div class="card-body"  id="mapping_tool_actions" style="display: none;">
                                            <div class="map-leftpanel" style="overflow-x: hidden;" >
                                                <div class="row">
                                                    <div class="col-md-5">

                                                        <div id="add-roof-panels-card" style=" text-align:center;">
                                                            <!-- <div class="add-roof-panels-card-inner"> -->
                                                                <div class="addBtn">
                                                                    <button data-toggle="tooltip" data-placement="bottom" title="Click this to draw section to add roof panel." type="button" class=" btn-danger draw-button" data-id="polygon" ><i class="fa fa-plus"></i></button>
                                                                </div>
                                                                <div id="remove-multi-roof-panels-card" class="addBtn dltLine" style="display:block;">
                                                                    <button data-container="body" disabled="disabled" data-toggle="tooltip" title="Click this to draw section to remove roof panel." data-placement="bottom" type="button" class=" btn-success remove-multi-button" data-id="polygon" ><i class="fa fa-trash"></i></button>
                                                                </div>
                                                                <p class="add-more">Add more roof section above</p>
                                                                <!-- </div> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2"> <div class="roof-panels-middle">Or</div> </div>
                                                        <div class="col-md-5">
                                                            <div class="cardDiv" id="pac-card">
                                                                <div>
                                                                    <div id="type-selector" class="pac-controls">
                                                                        <input type="radio" name="type" id="changetype-all" checked="checked">
                                                                        <label for="changetype-all">All</label>

                                                                        <input type="radio" name="type" id="changetype-establishment">
                                                                        <label for="changetype-establishment">Establishments</label>

                                                                        <input type="radio" name="type" id="changetype-address">
                                                                        <label for="changetype-address">Addresses</label>

                                                                        <input type="radio" name="type" id="changetype-geocode">
                                                                        <label for="changetype-geocode">Geocodes</label>
                                                                    </div>
                                                                    <div id="strict-bounds-selector" class="pac-controls">
                                                                        <input type="checkbox" id="use-strict-bounds" value="">
                                                                        <label for="use-strict-bounds">Strict Bounds</label>
                                                                    </div>
                                                                </div>

                                                                <div>
                                                                    <div class="calculate-container">
                                                                        <input type="hidden" id="query-address" value="">
                                                                    <button class="btn btn-calculate" type="button" data-toggle="modal" data-target="#calculate-modal" onClick="calculateNow();" id="reset-all" style="display: block;">Calculate Now</button>
                                                                    <br/>
                                                                    <button class="btn btn-calculate" type="button"   id="save_image_locally" style="display: block;">Create Image</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row"> <div class="col-md-12"><div class="panel-group section-container" id="accordion"></div></div></div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="card"style="padding: 0;background: transparent;box-shadow: none;">
                                        <div class="card-body" style="padding: 0;margin-bottom: 0px;">
                                            <ul class="nav nav-tabs justify-content-left" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#tab_image_upload" id="tab_image_upload_btn" role="tab" data-toggle="tab">
                                                    Upload Image</a>
                                                </li>                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#tab_near_map" id="tab_near_map_btn" role="tab" data-toggle="tab">Near Map</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#tab_mapping_tool" id="tab_mapping_tool_btn" role="tab" data-toggle="tab">Mapping Tool</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active show" id="tab_image_upload">
                                                    <div class="form-block d-block clearfix">
                                                        <div class="form-block d-block clearfix" id="proposal_no_image" >
                                                            <div class="add-picture">
                                                                <span>Add picture</span>
                                                                <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                                <input type="file"  id="proposal_image_upload" />
                                                                <input type="hidden" id="image" name="proposal[image]" />
                                                            </div>
                                                        </div>
                                                        <div class="form-block d-block clearfix" id="proposal_image" style="display:none !important; ">
                                                            <div class="add-picture"></div>
                                                            <a id="proposal_image_close" href="#" ></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade in" id="tab_near_map">
                                                    <input type="hidden" name="proposal[near_map_data]" id="near-map-exported-data"/>
                                                    <?php echo $new_mapping_tool; ?>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade in" id="tab_mapping_tool">
                                                    <div class="form-block d-block clearfix">
                                                        <div class="form-block d-block clearfix"  id="mapping_tool">
                                                            <?php echo $mapping_tool; ?>
                                                        </div>
                                                        <div class="form-block d-block clearfix" id="mapping_tool_image" style="display:none !important; ">
                                                            <div class="add-picture"></div>
                                                            <a id="proposal_image_close" href="#" ></a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                                  
                            </div>

                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="facility-list">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item"><a class="active"  data-toggle="tab" href="#system_details">System Details</a></li>
                                            <li class="nav-item"><a data-toggle="tab" href="#cost_summary">Cost Summary</a></li>
                                            <li class="nav-item"><a data-toggle="tab" href="#savings_calculation">Savings Calculation</a></li>
                                        </ul>
                                    </div>


                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="system_details">

                                            <div class="card">
                                                <div class="card-body">

                                                    <div class="solar-only-list">
                                                        <ul>
                                                            <?php
                                                            $lead_types = unserialize(LEAD_TYPES);
                                                            foreach ($lead_types as $key => $value) {
                                                                ?>
                                                                <li><a class="deal_type deal_type_<?php echo $key; ?>" data-id="<?php echo $key; ?>" href="javascript:void(0);"><?php echo $value; ?></a></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                    <div class="vic">
                                                        <span>Solar vic rebate eligible:</span> 
                                                        <a class="is_vic_rebate is_vic_rebate_1" data-id="1" href="javascript:void(0);" >Yes</a> 
                                                        <a class="is_vic_rebate is_vic_rebate_0 active" data-id="0" href="javascript:void(0);" class="active">No</a>
                                                    </div>
                                                    <div class="vic">
                                                        <span>Solar vic Loan eligible:</span> 
                                                        <a class="is_vic_loan is_vic_loan_1" data-id="1" href="javascript:void(0);" >Yes</a> 
                                                        <a class="is_vic_loan is_vic_loan_0 active" data-id="0" href="javascript:void(0);" class="active">No</a>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control" name="prd_panel" id="prd_panel">
                                                            <option data-item="{}" selected="">Please Fill  Customer Details First</option>
                                                        <?php /** foreach ($panels as $key => $value) { ?>
                                                          <option value="<?php echo $value['id']; ?>" data-item='<?php echo json_encode($value); ?>' ><?php echo $value['name'] . ' [' . $value['variant_state'] . '] ' . ' [' . $value['variant_application'] . '] '; ?></option>
                                                      <?php } */ ?>
                                                  </select>
                                                  <!--<div class="help-block text-danger" id="prd_panel_help" style="display:none;">Please enter system size in order to account panel price.</div>-->
                                                  <div class="help-block text-danger" id="prd_panel_help" style="display:none;"></div>
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control" name="prd_inverter" id="prd_inverter">
                                                    <option data-item="{}" selected="">Please Fill  Customer Details First</option>
                                                        <?php /** foreach ($inverters as $key => $value) { ?>
                                                          <option value="<?php echo $value['id']; ?>" data-item='<?php echo json_encode($value); ?>' ><?php echo $value['name'] . ' [' . $value['variant_state'] . '] ' . ' [' . $value['variant_application'] . '] '; ?></option>
                                                      <?php } */ ?>
                                                  </select>
                                              </div>
                                              <div class="form-group">
                                                <select class="form-control" name="prd_battery" id="prd_battery">
                                                    <option data-item="{}" selected="">Please Fill  Customer Details First</option>
                                                        <?php /** foreach ($battery as $key => $value) { ?>
                                                          <option value="<?php echo $value['id']; ?>" data-item='<?php echo json_encode($value); ?>' ><?php echo $value['name'] . ' [' . $value['variant_state'] . '] ' . ' [' . $value['variant_application'] . '] '; ?></option>
                                                      <?php } */ ?>
                                                  </select>
                                              </div>

                                              <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="proposal[roof_type][]" value="Tile" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="number" oninput="this.value = Math.abs(this.value)" class="form-control roof_type_no_of_panels" name="proposal[roof_type_no_of_panels][]"  value="0"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="proposal[roof_type][]" value="Tin" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="number" oninput="this.value = Math.abs(this.value)" class="form-control roof_type_no_of_panels" name="proposal[roof_type_no_of_panels][]"  value="0"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="proposal[roof_type][]" value="Tilt"  readonly />
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <input type="number" oninput="this.value = Math.abs(this.value)" class="form-control roof_type_no_of_panels" name="proposal[roof_type_no_of_panels][]"   value="0"  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6" style="min-width: 100%!important;">
                                                    <div class="form-group">
                                                        <div class="input-group mb-3"  style="min-width: 100%!important;">
                                                            <input type="number" readonly="" class="form-control" id="system_size" name="proposal[system_size]"  ste="0.01" oninput="this.value = Math.abs(this.value)" placeholder="System Size" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><b>kW</b></span>
                                                            </div>
                                                        </div>
                                                        <small class="form-text text-muted" id="no_of_panels"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <select name="proposal[no_of_stories]" id="no_of_stories" class="form-control">
                                                            <option value="0">No. of Stories</option>
                                                            <option value="1">Single</option>
                                                            <option value="2">Double</option>
                                                            <option value="3">More than 2</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                    <div class="form-group">
                                                        <select name="proposal[phase]" id="phase" class="form-control">
                                                            <option value="0">Phase</option>
                                                            <option value="1">Single Phase</option>
                                                            <option value="3">Three Phase</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row hidden">
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <h2>Warranty Details</h2>
                                                    <div class="str-total">
                                                        <table class="table table-striped" id="warranty_sb">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:150px;"> 
                                                                        <input type="number"  id="sb_warranty_details_1" class="form-control"  min="0" oninput="this.value=Math.abs(this.value)" value="10" />
                                                                    </td>
                                                                    <td>years product warranty on Inverters</td>
                                                                </tr>
                                                                <tr>
                                                                   <td style="width:150px;"> 
                                                                    <input type="number" id="sb_warranty_details_2" class="form-control"  min="0" oninput="this.value=Math.abs(this.value)" value="25"  />
                                                                </td>
                                                                <td>years warranty on PV modules performance</td>
                                                            </tr>
                                                            <tr>
                                                               <td style="width:150px;"> 
                                                                <input type="number" id="sb_warranty_details_3" class="form-control"  min="0" oninput="this.value=Math.abs(this.value)" value="10"  />
                                                            </td>
                                                            <td>years product warranty on PV panels</td>
                                                        </tr>
                                                        <tr>
                                                           <td style="width:150px;"> 
                                                            <input type="number" id="sb_warranty_details_4" class="form-control"  min="0" oninput="this.value=Math.abs(this.value)" value="10" />
                                                        </td>
                                                        <td>years warranty on all workmanship</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table class="table table-striped hidden" id="warranty_b">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:150px;"> 
                                                            <input type="number"  id="b_warranty_details_1" class="form-control"  min="0" oninput="this.value=Math.abs(this.value)" value="10"  />
                                                        </td>
                                                        <td>years product warranty on Battery System</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="cost_summary">

                        <div class="card">
                            <div class="card-body" style="padding-left:0px !important; padding-right:0px !important;">
                                <div class="col-md-12">

                                    <h2>Additional Cost Items</h2>
                                    <div class="table-default">
                                        <table width="100%" border="0">
                                            <thead>
                                                <tr>
                                                    <td>Type</td>
                                                    <td>Product</td>
                                                    <td>Price</td>
                                                    <td>Qty</td>
                                                    <td>Custom Price</td>
                                                    <td>Total</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody id="additional_cost_items_wrapper">

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="col-md-12">

                                    <h2>Add Additional Costs Items</h2>
                                    <div class="table-default">
                                        <table width="100%" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select id="additional_product_type" class="form-control"  required>
                                                            <option value="">Select Product Type </option>
                                                            <?php foreach ($product_type as $key => $value) { ?>
                                                                <option value="<?php echo $value['typeId']; ?>"><?php echo $value['typeName']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="additional_product" class="form-control"  required>
                                                            <option value="" >Please Select Type First</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" id="add_additional_cost_items" style="margin-left:10px;"><i class="fa fa-plus"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                                    <!-- <div class="row">
                                                         <button id="add_additional_cost_items" type="button" class="btn btn-primary" >Add Additional Item </button>
                                                     </div> -->

                                                 </div>

                                                 <div class="col-md-12 mt-5">


                                                    <div class="form-group">
                                                        <h2>Quote Total</h2>
                                                        <div class="str-total">
                                                            <table class="table table-striped">
                                                                <tbody id="total_quote_wrapper">

                                                                </tbody>
                                                            </table>

                                                            <!--<div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>Total System Cost Inc GST</label>
                                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                            </div>
                                                                            <input type="number"name="proposal[price_before_stc_rebate]" id="price_before_stc_rebate" class="form-control" id="total_payable_exGST"  min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label class="required-label">STC Rebate Exc GST</label>
                                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                            </div>
                                                                            <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="proposal[stc_deduction]" id="stc_deduction" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>System Cost after STC</label>
                                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                            </div>
                                                                            <input type="number" name="proposal[system_total]" class="form-control" id="system_total" placeholder="Please Enter System Cost after STC" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row hidden" id="vic_rebate_row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label class="required-label">Solar VIC Rebate Inc GST</label>
                                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                            </div>
                                                                            <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter Solar VIC rebate value" name="proposal[vic_rebate]" id="vic_rebate" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" >
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>Net Payable Inc GST</label>
                                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                            </div>
                                                                            <input type="number" name="proposal[quote_total_inGst]" id="quote_total_inGst" class="form-control"  min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>-->

                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="total_quote_original" style="display:none;">
                                                        <h2>Quote Total (Original)</h2>
                                                        <div class="table-default">
                                                            <table width="100%" border="0">
                                                                <tbody id="total_quote_original_wrapper">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>         
                                        <!-- Line Items -->  
                                        <div class="card" style="display: none;">
                                            <div class="card-body" style="padding-left:0px !important; padding-right:0px !important;">
                                                <div class="col-md-12" id="line_items_div">


                                                    <h2>Line Items</h2>
                                                    <div class="table-default">
                                                        <table width="100%" border="0">
                                                            <thead>
                                                                <tr>
                                                                    <td>Line Item</td>
                                                                    <td>State</td>
                                                                    <td>From - To</td>
                                                                    <td>Custom Price</td>
                                                                    <td></td>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="line_items_wrapper">

                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <!-- Line Items -->
                                        <div class="card" style="display: none">
                                            <div class="card-body">
                                                <div class="col-md-12 hidden" id="quote_items">

                                                    <div class="table-default">
                                                        <h2>Quote Items</h2>
                                                        <div class="table-default">
                                                            <table width="100%" border="0">
                                                                <tbody id="quote_items_wrapper">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="savings_calculation">
                                        <!-- Saving Calculator for Solar only Deal -->
                                        <div class="card" id="sc_solar">
                                            <div class="card-body" style="padding-left:0px !important; padding-right:0px !important;">
                                                <div class="tbl-box">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th scope="col" width="20%">Postcode</th>
                                                                <td width="20%"><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_postcode]" id="sc_postcode"></td>
                                                                <td width="30%" style="border:0;"></td>
                                                                <td width="30%" style="border:0;"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tbl-box">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th scope="col">System size</th>
                                                                <th scope="col">annual production (kwh)</th>
                                                                <th scope="col">electricity rate</th>
                                                                <th scope="col">export tariff</th>
                                                                <th scope="col">percentage export</th>
                                                            </tr>
                                                            <tr>
                                                                <td><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_system_size]" id="sc_system_size"></td>
                                                                <td><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_annual_prod]" id="sc_annual_prod" readonly></td>
                                                                <td><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_electricity_rate]" id="sc_electricity_rate"></td>
                                                                <td><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_export_tariff]" id="sc_export_tariff"></td>
                                                                <td><input type="number" min="1" class="inpt-typ" placeholder="0" name="saving_calculation_details[sc_percentage_export]" id="sc_percentage_export"><span>%</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="tbl-box-tow">
                                                    <h3>Monthly Solar Savings Estimate</h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th scope="col">Ave Production</th>
                                                                <th scope="col">Usage Savings</th>
                                                                <th scope="col">Export Credit</th>
                                                                <th scope="col">Total Savings</th>
                                                                <th scope="col">Annual Savings</th>
                                                            </tr>
                                                            <tr id="monthly_savings_caluclation_data_1">
                                                                <td colspan="5"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                            <!-- Saving Calculator for Solar + Battery -->

                                            <div class="card" id="sc_solar_battery">
                                                <div class="card-body" style="padding-left:0px !important; padding-right:0px !important;">
                                                    <div class="tbl-box">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr>
                                                                    <th scope="col">Average Solar Generation/kWp</th>
                                                                    <th scope="col" class="hidden">KWh/KW/day (average)</th>
                                                                    <th scope="col">TARIFF TYPE</th>
                                                                    <th scope="col">FLAT ELECTRICITY RATE</th>
                                                                </tr>
                                                                <tr>
                                                                    <td width="33%">
                                                                        <select class="inpt-typ form-control" name="saving_calculation_details[sc_area]" id="sc_sb_area">
                                                                            <?php foreach($solar_areas as $key => $value){ ?>
                                                                               <option value="<?php echo $value['areaId']; ?>" data-kwh="<?php echo $value['areaId']; ?>"><?php echo $value['areaName']; ?></option>
                                                                           <?php } ?>
                                                                       </select>
                                                                   </td>
                                                                   <td class="hidden">
                                                                    <input type="number" min="1" class="inpt-typ form-control" value="0" name="saving_calculation_details[sc_sb_kwh_day]" id="sc_sb_kwh_day" readonly>
                                                                </td>
                                                                <td width="33%">
                                                                    <select class="inpt-typ form-control" id="tariff_type" name="saving_calculation_details[tariff_type]">
                                                                        <option value="1">Flat</option>
                                                                        <option value="2">TOU</option>
                                                                    </select>
                                                                </td>
                                                                <td width="33%">
                                                                    <input type="number" min="1" class="inpt-typ form-control sc_sb_input" value="0" name="saving_calculation_details[flat_rate]" id="sc_sb_flat_rate">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="tbl-box" id="tou_rate_section">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr>
                                                                    <th scope="col"></th>
                                                                    <th scope="col">Peak (%oftime)</th>
                                                                    <th scope="col">Off-Peak (%oftime)</th>
                                                                    <th scope="col">Shoulder (%oftime)</th>
                                                                </tr>
                                                                <tr>
                                                                    <td scope="col" width="25%">
                                                                        <strong>TOU RATE</strong>
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[tr_rate][peak]" id="sc_sb_tr_peak">
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[tr_rate][off_peak]" id="sc_sb_tr_off_peak">
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[tr_rate][shoulder]" id="sc_sb_tr_shoulder">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td scope="col" width="25%">
                                                                        <strong>ELECTRICITY RATE</strong>
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[er_rate][peak]" id="sc_sb_er_peak">
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[er_rate][off_peak]" id="sc_sb_er_off_peak">
                                                                    </td>
                                                                    <td width="25%">
                                                                        <input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[er_rate][shoulder]" id="sc_sb_er_shoulder">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="tbl-box">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr>
                                                                    <th scope="col">System size</th>
                                                                    <th scope="col" class="hidden">annual production (kwh)</th>
                                                                    <th scope="col">battery size (kWh)</th>
                                                                    <th scope="col">export tariff</th>
                                                                    <th scope="col">percentage export</th>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25%"><input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[sc_system_size]" id="sc_sb_system_size"></td>
                                                                    <td class="hidden"><input type="number" min="1" class="inpt-typ form-control" placeholder="0" name="saving_calculation_details[sc_annual_prod]" id="sc_sb_annual_prod" readonly></td>
                                                                    <td width="25%"><input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[sc_battery_size]" id="sc_sb_battery_size"></td>
                                                                    <td width="25%"><input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[sc_export_tariff]" id="sc_sb_export_tariff"></td>
                                                                    <td width="100%" style="display: inline-flex;"><input type="number" min="1" class="inpt-typ form-control sc_sb_input" placeholder="0" name="saving_calculation_details[sc_percentage_export]" id="sc_sb_percentage_export"><span class="mt-2">%</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
    
                                                    <div class="tbl-box-tow">
                                                        <h3>Estimated Monthly Savings</h3>
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr>
                                                                    <th scope="col">Ave Production</th>
                                                                    <th scope="col">Usage Savings</th>
                                                                    <th scope="col">Export Credit</th>
                                                                    <th scope="col">Total Savings</th>
                                                                    <th scope="col">Annual Savings</th>
                                                                </tr>
                                                                <tr id="monthly_savings_caluclation_data_2">
                                                                    <td colspan="5"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-8" style="padding-bottom:30px;">
                                        <div class="btn-block">
                                            <a href="javascript:void(0);" id="save_proposal" class="btn mrg-r10 ">Save</a>
                                            <a href="javascript:void(0);" id="create_pdf" class="btn">Save & Create PDF</a> 
                                            <a href="#" id="create_pdf_download" target="_blank" rel="noopener noreferrer" class="hidden"></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<?= $stc_modal ?>
<script src="<?php echo site_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>assets/js/isLoading.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src='<?php echo site_url(); ?>common/js/residential_solar_proposal.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script>

    $(document).ready(function () {
        $('.locationstreetAddress').placecomplete2({
            placeServiceResult2: function (data, status) {
                if (status != 'INVALID_REQUEST') {
                    $('#show_something').html(data.adr_address);

            //Set lat lng
            var lat = data.geometry.location.lat(),
            lng = data.geometry.location.lng();
            if ($('#locationLatitude') != undefined) {
                $('#locationLatitude').val(lat);
            }
            if ($('#locationLongitude') != undefined) {
                $('#locationLongitude').val(lng);
            }

            //Set Address
            if ($('.locationstreetAddressVal') != undefined) {
                $('.locationstreetAddressVal').val(data.formatted_address);
            }

            var address_components = data.address_components;

            for (var i = 0; i < address_components.length; i++) {
                //Set State
                if (address_components[i].types[0] == 'administrative_area_level_1') {
                    var states = [];
                    var selected_id = '';
                    if ($('.locationState') != undefined) {
                        var select = document.querySelector(".locationState");
                        for (var j = 0; j < select.length; j++) {
                            var option = select.options[j];
                            states.push(option.text);
                            if (option.text == address_components[i].long_name) {
                                option.setAttribute('selected', 'selected');
                                selected_id = option.value;
                            }
                        }
                        
                        //For ipad and apple devices selected and prop was not working so had to create State element again
                        document.querySelector(".locationState").innerHTML = '';
                        for (var k = 0; k < states.length; k++) {
                            var option = document.createElement('option');
                            if(k == 0){
                                option.setAttribute('value','');
                                option.innerHTML = states[k];
                                if(selected_id == k){
                                    option.setAttribute('selected','selected');
                                }
                                document.querySelector(".locationState").appendChild(option);
                            }else{
                                option.setAttribute('value',k);
                                option.innerHTML = states[k];
                                if(selected_id == k){
                                    option.setAttribute('selected','selected');
                                }
                                document.querySelector(".locationState").appendChild(option);
                            }
                        }

                    }
                }

                //Set Postal Code
                if (address_components[i].types[0] == 'postal_code') {
                    if ($('.locationPostCode') != undefined) {
                        $('.locationPostCode').val(address_components[i].long_name);
                        $('.locationPostCode').trigger('change');
                    }
                }
            }
        } else{
            $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
        }
    },
    language: 'fr'
});

    });

    var context = {};
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.panel_data = '<?php echo (isset($panel_data) && !empty($panel_data)) ? json_encode($panel_data, JSON_HEX_APOS) : ''; ?>';
    context.lead_uuid = '<?php echo (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : ''; ?>';
    context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
    context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
    context.postcode_rating = '<?php echo (isset($postcode_rating) && $postcode_rating != '') ? $postcode_rating : 0; ?>';


    context.stc_price_assumption = '';
    context.certificate_margin = '';
    context.proposal_id = '<?php echo (isset($proposal_data['uuid'])) ? $proposal_data['uuid'] : ''; ?>';
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';

    var proposal_calculator_obj = new residentail_solar_proposal_calculator(context);
</script>
</body>
</html>


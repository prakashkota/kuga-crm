<style>
    @media print {
        .no-print {display: none;}
        body {background: transparent;}
        .red{color:#c32027}
        .black-bg {background-color:#010101}
        .dark-bg{background-color:#282828}
        .page-wrapper{ min-height:100%;}
    }
    @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 150px;
        }
    }
    table,tr,td{
        margin : auto !important;
    }

    .sr-deal_actions{
        opacity: 1;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: 0px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }

    .pdf_page{
        width: 910px;
        box-shadow: 1px 1px 3px 1px #333;
        border-collapse: separate;
        padding:5px;
    }
    .add-picture{
        padding: 180px 0 !important;
    }



    .signature-pad {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        font-size: 10px;
        width: 100%;
        height: 100%;
        max-height: 460px;
        border: 1px solid #e8e8e8;
        background-color: #fff;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
        border-radius: 4px;
        padding: 16px;
    }

    .signature-pad::before,
    .signature-pad::after {
        position: absolute;
        z-index: -1;
        content: "";
        width: 40%;
        height: 10px;
        bottom: 10px;
        background: transparent;
        box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
    }

    .signature-pad::before {
        left: 20px;
        -webkit-transform: skew(-3deg) rotate(-3deg);
        transform: skew(-3deg) rotate(-3deg);
    }

    .signature-pad::after {
        right: 20px;
        -webkit-transform: skew(3deg) rotate(3deg);
        transform: skew(3deg) rotate(3deg);
    }

    .signature-pad--body {
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #f4f4f4;
    }

    .signature-pad--body
    canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
    }

    .signature-pad--footer {
        color: #C3C3C3;
        text-align: center;
        font-size: 1.2em;
        margin-top: 8px;
    }

    .signature-pad--actions {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-top: 8px;
    }

    #github img {
        border: 0;
    }

    @media (max-width: 990px) {
        #github img {
            width: 90px;
            height: 90px;
        }
        .sr-deal_actions{
            opacity: 1;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            height: 67px;
            background-color: #f7f7f7;
            z-index: 100;
            box-shadow: 0 5px 40px rgba(0,0,0,.4);
            transition: opacity .1s ease-out,bottom .1s ease-out .5s;
            display: flex;
        }


    }

    /** IPAD */
    @media only screen
    and (min-width: 1024px)
    and (max-height: 1366px)
    and (-webkit-min-device-pixel-ratio: 1.5) {
        .page-wrapper {
            padding: 30px 30px 30px 105px;
        }
    }

    .signature_image_close,.image_close{
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: -18px;
        right: -18px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .signature_image_container,.image_container{
        margin: 20px 20px 20px 20px;
        overflow: visible;
        box-shadow: 5px 5px 2px #888888;
        position: relative;
    }

    .is-invalid{border: 1px solid red;}
    .is-valid{border: 1px solid green !important;}
    #signatures{
        height: 400px;
    }
    .sign_close,.sign_undo{display: none;}

    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<div class="page-wrapper d-block clearfix" >
    <div style="padding:0; margin:0">
        <form id="tender_booking_form" enctype="multipart/form-data">
            <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom: 10px !important;">
                <tbody>
                <tr>
                    <td align="left" valign="top" style="display: flex;">
                        <label>Cost Centre</label>
                        <?php $opArr = get_cost_center_by_type('solar','ccid,name');?>
                        <select class="form-control ml-3" style="width: 200px;" name="cost_centre_id" id="cost_centre_id">
                            <?php foreach($opArr as $key => $value){ ?>
                                <option <?php if($value['ccid'] == @$centerId) {?> selected <?php } ?> value="<?php echo $value['ccid']; ?>"><?php echo $value['name']; ?></option>
                            <?php } ?>
                        </select>
                        <!--<input type="hidden" name="cost_centre_id" id="cost_centre_id"  /> -->
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Business Details</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Company Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="company_name" name="business_details[company_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">ABN/ACN:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="abn" name="business_details[abn]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Trading Name:</td>
                                                        <td width="283" valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required id="trading_name" name="business_details[trading_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Leased or Owned:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <select id="leased_or_owned" class="form-control" name="business_details[leased_or_owned]" style="padding:5px; width:inherit; height: inherit;">
                                                                <option value="">Select Leased or Owned</option>
                                                                <?php
                                                                $leased_or_owned = unserialize(BF_LEASED_OR_OWNED);
                                                                if (!empty($leased_or_owned)) {
                                                                    foreach ($leased_or_owned as $key => $value) {
                                                                        ?>
                                                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Address:</td>
                                            <td width="708" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required value="<?php echo @$sites[0]['address']; ?>" id="address" name="business_details[address]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Project Manager</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="first_name" name="contacts[projectManager][first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="last_name" name="contacts[projectManager][last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="position" name="contacts[projectManager][position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="email" name="contacts[projectManager][email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="contact_no" name="contacts[projectManager][contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="landline" name="contacts[projectManager][landline]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Site Contact 1</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="first_name" name="contacts[siteContact1][first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="last_name" name="contacts[siteContact1][last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="position" name="contacts[siteContact1][position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="email" name="contacts[siteContact1][email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="contact_no" name="contacts[siteContact1][contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="landline" name="contacts[siteContact1][landline]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Site Contact 2</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="first_name" name="contacts[siteContact2][first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="last_name" name="contacts[siteContact2][last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="position" name="contacts[siteContact2][position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="email" name="contacts[siteContact2][email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="contact_no" name="contacts[siteContact2][contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="landline" name="contacts[siteContact2][landline]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="100">&nbsp;</td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
            <br/>
            <table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Accounts</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="first_name" name="contacts[accounts][first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="last_name" name="contacts[accounts][last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="position" name="contacts[accounts][position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="email" name="contacts[accounts][email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="contact_no" name="contacts[accounts][contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="landline" name="contacts[accounts][landline]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Contracts Administrator</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="first_name" name="contacts[contractsAdministrator][first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="last_name" name="contacts[contractsAdministrator][last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="position" name="contacts[contractsAdministrator][position]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="email" name="contacts[contractsAdministrator][email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="contact_no" name="contacts[contractsAdministrator][contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="landline" name="contacts[contractsAdministrator][landline]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Contract Information</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Start Date:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="start_date" name="contractInformation[start_date]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Completion Date:</td>
                                                        <td width="251" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="completion_date" name="contractInformation[completion_date]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>


                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Progress Claims Terms:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input  id="email" name="contractInformation[claims_terms]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Payment Terms:</td>
                                                        <td width="251" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input  id="payment_schedule" name="contractInformation[payment_schedule]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>

                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Liquidated Damages $/Day:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input  id="liquidated" name="contractInformation[liquidated]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Defect Liability Period:</td>
                                                        <td width="251" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input  id="defect_liability_period" name="contractInformation[defect_liability_period]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>

                            <tr>
                                <td height="10"></td>
                            </tr>

                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Contract Amount:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input  id="contract_amount" name="contractInformation[contract_amount]" style="padding:5px; width:inherit; height: inherit;" type="number" />
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>

                            <tr>
                                <td height="10"></td>
                            </tr>

                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><input type="checkbox" id="retention" name="contractInformation[retention]"/>&nbsp;&nbsp;&nbsp;Retention Amount&nbsp;&nbsp;</td>
                                                        <td width="240" id="retentionTd" valign="top" style="display: none; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input name="contractInformation[retention_info]" style="padding:5px; width:inherit; height: inherit;" type="text">
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="410" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><input id="bank" type="checkbox" name="contractInformation[bank]"/>&nbsp;&nbsp;&nbsp;Bank Guarantee&nbsp;&nbsp;</td>
                                                        <td width="240" id="bankTd" valign="top" style=" display: none; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                            <input id="bank_text" name="contractInformation[bank_info]" style="padding:5px; width:inherit; height: inherit;" type="text">
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>

                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="410" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Electricity Bill Information <input type="radio" name="contractInformation[eb_information]" value="yes" checked="checked"/> &nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="contractInformation[eb_information]" value="no"/> &nbsp;No&nbsp;&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-right:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr id="edInformationSection1">
                    <td>&nbsp;</td>
                </tr>
                <tr id="edInformationSection2">
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tbody><tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Electricity Bill Information</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Retailer:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="retailer" name="electricity_bill[ed_retailer]" style="padding:5px; width:inherit; height: inherit;" type="text"></td>
                                                    </tr>
                                                    </tbody></table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">NMI:</td>
                                                        <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="abn" name="electricity_bill[eb_nmi]" style="padding:5px; width:inherit; height: inherit;" type="text"></td>
                                                    </tr>
                                                    </tbody></table></td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td id="eb_information_section" valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Distributor:</td>
                                                        <td width="283" valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input id="distributor" name="electricity_bill[eb_distributor]" style="padding:5px; width:inherit; height: inherit;" type="text"></td>
                                                    </tr>
                                                    </tbody></table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tbody><tr>
                                                        <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Meter Number:</td>
                                                        <td width="283" valign="top" style=" height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input id="meter_number" name="electricity_bill[eb_meter_no]" style="padding:5px; width:inherit; height: inherit;" type="text"></td>
                                                    </tr>
                                                    </tbody>
                                                    </tbody></table></td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>

                            </tbody></table></td>
                </tr>
                <tr>
                    <td height="100">&nbsp;</td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
            <br/>
            <table id="ebillImages" class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Front</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="ec_bill_1" />
                                                    <input type="hidden" id="ec_bill_1" name="booking_form_image[ec_bill_1]"  />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Electricity Bill Back</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="ec_bill_2" />
                                                    <input type="hidden" id="ec_bill_2" name="booking_form_image[ec_bill_2]" />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="100">&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>

            <br/>
            <table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <?php
                        $productTypes = array(
                            'systemSize'=>'System Size',
                            'panel'=>'Solar Panels',
                            'inverter'=>'Inverters',
                            'battery'=>'Battery',
                            'roofType'=>'Roof Type',
                            'roofbrand'=>'Roof Brand/Style',
                            'rackingType'=>'Racking Type',
                            'rackingBrand'=>'Racking Brand',
                            'minimumTiltDegree'=>'Minimum Tilt Degree',
                            'perlingSpaces'=>'Perling Spaces',
                            'switchboard'=>'Switchboard'
                        );
                        ?>
                        <table id="booking_items" width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Product Type</th>
                                <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">System</th>
                                <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                                <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                                <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                            </tr>


                            <?php foreach ($productTypes as $key=>$value) { ?>
                                <tr>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                       <?php echo $value; ?>
                                        <input type="hidden" name="product[<?php echo $key ?>][product_type]" value="<?php echo $value; ?>">
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">

                                        <?php if(isset($$key)){?>
                                        <select class="product_name product_name_<?php echo $key; ?>" name="product[<?php echo $key ?>][product_name]" style="padding:5px; width:100%; height: inherit;" id="prd_<?php echo $key; ?>">
                                            <option value="">Select <?php echo $value; ?></option>
                                            <?php if($$key){ foreach($$key as $each){ ?>
                                                <option value="<?php echo $each['name'] ?>" data-item='<?php echo json_encode($each)?>'><?php echo $each['name'] ?></option>
                                            <?php } } ?>
                                        </select>
                                        <?php } else{ ?>
                                        <input class="product_name product_name_<?php echo $key; ?>" name="product[<?php echo $key ?>][product_name]"  style="padding:5px; width:100%; height: inherit;" type="text" />
                                        <?php } ?>
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <input class="product_qty product_qty_<?php echo $key; ?>" name="product[<?php echo $key ?>][product_qty]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <input class="product_cost_excGST product_cost_excGST_<?php echo $key; ?>" name="product[<?php echo $key ?>][product_cost_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                                    </td>
                                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                        <input class="product_total_cost_excGst product_total_cost_excGst_<?php echo $key; ?>" style="padding:5px; width:100%; height: inherit;" name="product[<?php echo $key ?>][product_total_cost_excGst]"  min="1" type="number" />
                                    </td>
                                </tr>
                            <?php } ?>
                        </table></td>
                </tr>
                <tr>
                    <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="557" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                                <td width="115" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                                <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <input  id="total_excGst" readonly name="booking_form[total_excGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <table width="557" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="40" width="345" style="font-family:Arial, Helvetica, sans-serif; font-size:13px">Is there a forklift on site?</td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>GST:</strong></td>
                                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <input  readonly id="total_Gst" name="booking_form[total_Gst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                                </td>
                            </tr>
                            <tr>
                                <td height="40" style="font-family:Arial, Helvetica, sans-serif; font-size:15px">
                                    <table width="557" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="345">
                                                <input id="is_forklift_on_site_yes" name="booking_form[is_forklift_on_site]" value="1" style="padding:5px; width:20px; height: inherit;" type="radio"> YES
                                                <input id="is_forklift_on_site_no" name="booking_form[is_forklift_on_site]" value="0" style="padding:5px; width:20px; height: inherit;" type="radio"> NO
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                                <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                                    <input  readonly id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                            <tr>
                                <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Array Details</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Height:</td>
                                                        <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required id="roof_height" name="booking_form[roof_height]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Equipments:</td>
                                                        <td width="665" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required id="access_equipments" name="booking_form[access_equipments]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>

                        </table></td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
           <br/>
            <table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Inverter / Grid Protection Location</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="inverterGridProtection" />
                                                    <input type="hidden" id="inverterGridProtection" name="booking_form_image[inverterGridProtection]"  />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>AC Run</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="acRun" />
                                                    <input type="hidden" id="acRun" name="booking_form_image[acRun]" />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:0px 0">
                                               <textarea style="width: 829px;height: 100px;" name="notes[acRun]"></textarea>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
            <br/>
            <table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Connection to Switchboard</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="connectionToSwitchBoard" />
                                                    <input type="hidden" id="connectionToSwitchBoard" name="booking_form_image[connectionToSwitchBoard]"  />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr align="center">
                                <td><span><a id="add_more_additional_images_page_btn" href="javascript:void(0);" class="btn btn-kuga mb-2"><img src="https://kugacrm.com.au/job_crm/assets/images/photo_evidance_add.png" alt="" style="width: 20px; height: 20px;"> Add More Additional Images</a></span></td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Additional Photo</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="additionPhoto" />
                                                    <input type="hidden" id="additionPhoto" name="booking_form_image[additionPhoto]" />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:0px 0">
                                                <textarea style="width: 829px;height: 100px;" name="notes[additionPhoto]"></textarea>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
            <div id="extra_additional_image_container">

            </div>
            <br/>
            <table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/header_02.png'); ?>" width="910" height="126" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Panel Layout</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="panelPalyout" />
                                                    <input type="hidden" id="panelPalyout" name="booking_form_image[panelPalyout]"  />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>DC Run</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:13px 0">
                                                <div class="add-picture">
                                                    <span>Add picture</span>
                                                    <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                    <input type="file" class="image_upload" data-id="dcRun" />
                                                    <input type="hidden" id="dcRun" name="booking_form_image[dcRun]" />
                                                </div>
                                                <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                                                    <div class="add-picture"></div>
                                                    <a class="image_close" href="javscript:void(0);"  ></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                                        <tr>
                                            <td height="35" colspan="2" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Notes</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="830" align="center" valign="top" style="padding:0px 0">
                                                <textarea style="width: 829px;height: 100px;" name="notes[dcRun]"></textarea>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td><img src="<?php echo site_url('assets/tender_booking_form/bottom.png'); ?>" width="910" height="89" /></td>
                </tr>
            </table>
        </form>
    </div>
    <div  class="sr-deal_actions">
        <div class="col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="save_booking_form" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save as Draft</a>
        </div>
        <div class=" col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="generate_booking_form_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-file-pdf-o"></i> Generate Booking Form</a>
            <a href="javascript:void(0);" id="generate_booking_form_pdf_link" class="hidden" ></a>
        </div>
    </div>

</div>

<link href='https://code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css' type='text/css' rel='stylesheet'>
<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js' type='text/javascript'></script>
<script src='https://www.jqueryscript.net/demo/Scrollable-Autocomplete-List-jQuery-UI/jquery.ui.autocomplete.scroll.js' type='text/javascript'></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo site_url(); ?>assets/js/signpad.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/tender_booking_form.js?v=<?php echo version; ?>"></script>

<script>
    var context = {};
    context.lead_uuid = '';
    context.siteId = '<?php echo (isset($siteId)) ? $siteId : ''; ?>';
    context.tenderId = '<?php echo (isset($tenderId)) ? $tenderId : ''; ?>';
    context.booking_form_uuid = '<?php echo (isset($uuid)) ? $uuid : ''; ?>';
    context.gst = '<?php echo GST ?>';
    var solar_booking_form_manager = new tender_booking_form_manager(context);
</script>
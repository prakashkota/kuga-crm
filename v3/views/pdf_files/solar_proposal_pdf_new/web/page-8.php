<?php

$o = $t = $s = array();
$tilt = $orientation = '';
$panel_id = '';
$panel_model_type = '';
$inverter_model_type = '';
$battery_model_type = '';

$numberOfPanel = 0;
$panelCapacity = 0;
$system_pricing_inverter_val = 0;

if (count($mappingPanelObjects)) {
    $numberOfPanel = count($mappingPanelObjects);
    for ($i = 0; $i < count($mappingPanelObjects); $i++) {

        $object_properties = json_decode($mappingPanelObjects[$i]['object_properties']);
        $panel_id = $object_properties->sp_panel_id;
        if (!in_array($object_properties->sp_rotation, $o)) {
            $o[] = $object_properties->sp_rotation;
        }
        if (!in_array($object_properties->sp_tilt, $t)) {
            $t[] = $object_properties->sp_tilt;
        }
    }

    $o = empty($o) ? array(0) : $o;
    $t = empty($t) ? array(0) : $t;
    $orientation = implode('&deg;, ', $o);
    $tilt = implode('&deg;, ', $t);
}

// if (count($mappingPanelObjects)) {
//     for ($i = 0; $i < count($mappingPanelObjects); $i++) {

//         $object_properties = json_decode($mappingPanelObjects[$i]['object_properties']);
//         $panel_id = $object_properties->sp_panel_id;
//         if (!in_array($object_properties->sp_rotation, $o)) {
//             $o[] = $object_properties->sp_rotation;
//         }
//         if (!in_array($object_properties->sp_tilt, $t)) {
//             $t[] = $object_properties->sp_tilt;
//         }
//     }

//     $o = empty($o) ? array(0) : $o;
//     $t = empty($t) ? array(0) : $t;
//     $orientation = implode('&deg;/ ', $o);
//     $tilt = implode('&deg;/ ', $t);
// }


if ($proposal_data['system_details'] != null && $proposal_data['system_details'] != "") {
    $SystemDetails = json_decode($proposal_data['system_details']);
    if (!empty($SystemDetails)) {
        $panel_model_type = (isset($SystemDetails->system_pricing_panel) && $SystemDetails->system_pricing_panel != '') ? json_decode($SystemDetails->system_pricing_panel)->name : '';
        $inverter_model_type = (isset($SystemDetails->system_pricing_inverter) && $SystemDetails->system_pricing_inverter != '') ? json_decode($SystemDetails->system_pricing_inverter)->name : '';
        $battery_model_type = (isset($SystemDetails->system_pricing_battery) && $SystemDetails->system_pricing_battery != '') ? json_decode($SystemDetails->system_pricing_battery)->name : '';
        $panelCapacity = (isset($SystemDetails->system_pricing_panel) && $SystemDetails->system_pricing_panel != '') ? json_decode($SystemDetails->system_pricing_panel)->capacity : 0;
        
        
        $system_pricing_inverter_val = $SystemDetails->system_pricing_inverter_val;
    }
}

if ($proposal_data['is_production_file'] == 1 || $proposal_data['is_production_file'] == 2) {
    if (isset($proposal_data['pvwatts_data'])) {
        $d_pvwatts_data = json_decode($proposal_data['pvwatts_data']);
        for ($i = 0; $i < count($d_pvwatts_data); $i++) {
            if (!in_array($d_pvwatts_data[$i]->pv_azimuth, $o)) {
                $o[$i] = $d_pvwatts_data[$i]->pv_azimuth;
            }
            if (!in_array($d_pvwatts_data[$i]->pv_tilt, $t)) {
                $t[$i] = $d_pvwatts_data[$i]->pv_tilt;
            }

            if (!in_array($d_pvwatts_data[$i]->pv_system_size, $s)) {
                $s[$i] = $d_pvwatts_data[$i]->pv_system_size;
            }
        }

        $o = empty($o) ? array(0) : $o;
        $t = empty($t) ? array(0) : $t;
        $orientation = implode('&deg;, ', $o);
        $tilt = implode('&deg;, ', $t);

        $numberOfPanel = (int)($proposal_data['total_system_size'] * 1000 / $panelCapacity);
    }
}


?>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    <title></title>
    <style>
        body {
            font-family: arial;
        }

        .page8 .back-bg {
            background-color: white;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page8 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page8 .top hr {
            width: 65%;
            height: 1px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page8 .text_color {
            color: #D01E2A;
        }

        .page8 .title {
            margin-top: -50px;
        }

        .page8 .title h1 {
            padding-left: 14px;
            margin-left: 70px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page8 .title h2 {
            padding: 10px;
            margin-left: 70px;
            margin-top: 50px;
            background-color: #D01E2A;
            width: 47%;
            color: #ffffff;
            background: linear-gradient(105deg, #D01E2A 87%, transparent 15%);
            font-size: 18px;
        }

        .page8 table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 83%;
            margin-left: 70px;
        }

        .page8 tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page8 table th {
            text-align: left;
            padding-left: 50px;
        }

        .page8 td {
            padding: 15px;
            color: #3D3B3C;
        }


        .page8 .image img {
            /* margin-top: -50px; */
            margin-left: 70px;
            margin-bottom: 30px;
        }

        .page8 .bb-none {
            border-bottom: none;
        }

        /** Footer */
        .page8 .footer {
            display: inline-flex;
        }

        .page8 .fimagee img {
            margin-left: 730px;
            margin-top: 0px;
        }

        .page8 .pb {
            padding-bottom: 235px;
        }

        .page8 .zaspan {
            margin-left: 0px;
        }

        .page8 .footer-p p {
            font-size: 10px;
            margin-left: 48;
            margin-top: -23px;
            display: none;
        }

        .page8 .footer-pp p {
            font-size: 10px;
            margin-left: 46px;
            margin-top: -22px;
        }

        .page8 .footer-pp {
            margin-left: 286px;
            margin-top: -36px;
            display: none;
        }

        .page8 .zafooter-icon img {
            width: 25px;
            margin-right: 10px;
            display: none;
        }

        .page8 .zafooter-iconn img {
            width: 25px;
            margin-right: 10px;
        }

        .page8 .zayear {
            font-size: 17px;
        }

        .page8 .redf {
            height: 10px;
            background: red;
            width: 455px;
        }

        .page8 .greyf {
            height: 10px;
            background: #dadada;
            width: 455px;
        }
    </style>
</head>

<?php
/** Manage Data Here */
$mapping_tool = empty($mapping_tool) ? [] : $mapping_tool;
$mapping_tool['snapshot1'] = isset($mapping_tool['snapshot1']) && $mapping_tool['snapshot1'] != NULL ? $mapping_tool['snapshot1'] : $proposal_data['panel_layout_image'];
$mapping_tool['snapshot2'] = isset($mapping_tool['snapshot2']) && $mapping_tool['snapshot2'] != NULL ? $mapping_tool['snapshot2'] : $proposal_data['panel_layout_image'];
$mapping_tool['notes'] = isset($mapping_tool['notes']) && $mapping_tool['notes'] != NULL ? $mapping_tool['notes'] : '';
?>

<body class="century_gothic">
    <div class="back-bg page8">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>SITE <span class="text_color"> PLAN</span></h1>
            <h2><?php echo $lead_data['customer_company_name']; ?></h2>
        </div>
        <div class="image">
            <div class="add-picture1" data-src="<?php echo $mapping_tool['snapshot1']; ?>" style="background-image: url('<?php echo $mapping_tool['snapshot1']; ?>'); 
                       background-repeat: no-repeat; background-position: 50% 50%; 
                       background-size: 100% 100%; 
                       width: 750;
                       height: 450px;
                       margin-left: 70px; 
                       margin-bottom: 30px;">
            </div>
        </div>
        <div class="title" style="margin-top:0px;">
            <h2><?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></h2>
        </div>
        <div style="padding-bottom: 35px;">
            <table>
                <tr>
                    <th>Manufacturer power rating for array</th>
                    <td><?php echo ($proposal_data['total_system_size']); ?> KW</td>
                </tr>
                <tr>
                    <th>Number of Panels</th>
                    <td><?php echo $numberOfPanel; ?></td>
                </tr>
                <tr>
                    <th>Panel Orientation</th>
                    <td>
                        <?php echo $orientation . '&deg; '; ?>
                    </td>
                </tr>
                <tr>
                    <th>Panel Tilt</th>
                    <td>
                        <?php echo $tilt . '&deg; '; ?>
                    </td>
                </tr>
                <?php if (!empty($panel_model_type)) { ?>
                    <tr>
                        <th>Panel Type/Model</th>
                        <td><?php echo $panel_model_type; ?></td>
                    </tr>
                <?php } ?>
                <?php if (!empty($inverter_model_type)) { ?>
                    <tr>
                        <th>Inverter Type/Model</th>
                        <td><?php echo $system_pricing_inverter_val.' X '.$inverter_model_type; ?></td>
                    </tr>
                <?php } ?>
                <?php if (!empty($battery_model_type)) { ?>
                    <tr>
                        <th>Battery Type/Model</th>
                        <td><?php echo $battery_model_type; ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="fimagee"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
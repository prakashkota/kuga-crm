<?php
$space_type = json_decode(json_encode($space_type), true);
$ceiling_height = json_decode(json_encode($ceiling_height), true);
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/header_01.jpg'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="828" border="1" bordercolor="#e8e8e8" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">&nbsp;</th>
                    <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Area 1</th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Area 2</th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Area 3</th>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px">Space Type:</td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($space_type[0])) ? $space_type[0] : ''; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($space_type[1])) ? $space_type[1] : ''; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($space_type[2])) ? $space_type[2] : ''; ?></strong></td>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px">Celling Height:</td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($ceiling_height[0])) ? $ceiling_height[0] : ''; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($ceiling_height[1])) ? $ceiling_height[1] : ''; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo (isset($ceiling_height[2])) ? $ceiling_height[2] : ''; ?></strong></td>
                </tr>
                <tr>
                    <th width="204" height="40" class="black-bg" style="background:#010101;padding:5px">Zone Classification</th>
                    <th width="204" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:14px; color:#ffffff">Reach Up:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Ceiling Height)</td>
                            </tr>
                        </table>
                    </th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:14px; color:#ffffff">Clearance:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Height of item blocking access)</td>
                            </tr>
                        </table>
                    </th>
                    <th width="205" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:14px; color:#ffffff">Reach Across:</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Calibri, sans-serif; font-size:12px; color:#ffffff">(Distance from the alsle to light)</td>
                            </tr>
                        </table>
                    </th>
                </tr>
                <tr>
                    <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px">Boom Requirements:</td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $boom_req->reach_up; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $boom_req->clearance; ?></strong></td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $boom_req->reach_across; ?></strong></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="30" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:14px">Zone Classificatoin</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Description of Upgrade product</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Units</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php
                $height = 500;
                for ($i = 0; $i < count($product->item_zone_classification); $i++) {
                    $total_cost_excGST = '';
                    if ($product->item_qty[$i] != '' && $product->item_cost_excGST[$i] != '') {
                        $total_cost_excGST = $product->item_qty[$i] * $product->item_cost_excGST[$i];
                    }
                    if($product->item_name[$i] != ''){
                        if($i > 6){
                            $height = $height - 60;
                        }
                    ?>
                    <tr>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $product->item_zone_classification[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $product->item_name[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $product->item_qty[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $product->item_cost_excGST[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php }} ?>
                <?php /*
                <tr>
                    <td height="40" colspan="4" style="padding:5px;">
                       
                        <table  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="380" style="font-family: Calibri, sans-serif; font-size:13px; color:#000000">Further Products quoted can be found on a separate sheet </td>
                                <td width="140">
                                    <?php if (isset($booking_form->is_more_products)) { ?>
                                        <input type="checkbox" checked="" style="margin-left:10px;" />
                                    <?php } else { ?>
                                        <input type="checkbox" style="margin-left:10px;" />
                                    <?php } ?>
                                </td>
                                <td width="147" align="right" style="font-family: Calibri, sans-serif; font-size:13px; color:#000000">Sub Total exc GST:</td>
                            </tr>
                        </table>
                        </td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong>$<?php echo $product->total_excGst; ?></strong></td>
                </tr>
                */ ?>
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family: Calibri, sans-serif; font-size:14px">Access Equipment</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Detailed Description of Access Equipment</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Quantity</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family: Calibri, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>
                <?php
                for ($i = 0; $i < count($ae->ae_name); $i++) {
                    $total_cost_excGST = '';
                    if ($ae->ae_qty[$i] != '' && $ae->ae_cost_excGST[$i] != '') {
                        $total_cost_excGST = $ae->ae_qty[$i] * $ae->ae_cost_excGST[$i];
                    }
                    ?>
                    <tr>
                        <td height="40" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $ae->ae_name[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $ae->ae_description[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $ae->ae_qty[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $ae->ae_cost_excGST[$i]; ?></strong></td>
                        <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong><?php echo $total_cost_excGST; ?></strong></td>
                    </tr>
                <?php } ?>
                <?php /*
                <tr>
                    <td height="40" colspan="4" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:13px;">Sub Total exc GST:</td>
                    <td style="padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong>$<?php echo $ae->total_excGst; ?></strong></td>
                </tr>
                */ ?>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="560" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="147" height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total exc GST:</strong></td>
                    <td width="147" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_excGst; ?></strong></td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <?php /* <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="375" style="font-family: Calibri, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs PANEL</strong> booking form?</td>
                                <td width="185">
                                    <?php if (isset($booking_form->is_heers_panel)) { ?>
                                        <input type="checkbox" checked="" />
                                    <?php } else { ?>
                                        <input type="checkbox" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table> */ ?>
                    </td>
                    <td align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_Gst; ?></strong></td>
                </tr>
                <tr>
                    <td height="40" style="font-family: Calibri, sans-serif; font-size:15px">
                         <?php /*<table  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" width="375" style="font-family: Calibri, sans-serif; font-size:13px">Have you also filled in a <strong>HEERs BATTEN</strong> booking form?</td>
                                <td width="185" style="font-family: Calibri, sans-serif; font-size:15px">
                                    <?php if (isset($booking_form->is_heers_panel)) { ?>
                                        <input type="checkbox" checked=""  />
                                    <?php } else { ?>
                                        <input type="checkbox"  />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table> */?>
                    </td>
                    <td height="40" align="right" style="padding:5px; font-family: Calibri, sans-serif; font-size:15px"><strong>Total inc GST:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family: Calibri, sans-serif; font-size:13px"><strong>$<?php echo $booking_form->total_incGst; ?></strong></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="<?php echo $height; ?>">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family: Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />3</td>
                </tr>
            </table></td>
    </tr>
</table>

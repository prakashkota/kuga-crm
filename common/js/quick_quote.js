
var quick_quote_manager = function (options) {
    var self = this;
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.quote_data = (options.quote_data && options.quote_data != '') ? JSON.parse(options.quote_data) : {};
    this.quote_items = (options.quote_items && options.quote_items != '') ? JSON.parse(options.quote_items) : {};
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.subtotal_excGST = 0;
    this.total_incGST = 0;
    if (Object.keys(self.quote_data).length > 0) {
        $('#quote_no').val(self.quote_data.quote_no);
        $('#customer_company_name').val(self.quote_data.customer_company_name);
        $('#customer_name').val(self.quote_data.customer_name);
        $('#customer_contact_no').val(self.quote_data.customer_contact_no);
        $('#customer_address').val(self.quote_data.customer_address);
        $('#customer_postcode').val(self.quote_data.customer_postcode);
        $('#customer_suburb').val(self.quote_data.customer_suburb);
        $('#rep_name').val(self.quote_data.rep_name);
        $('#quote_expires_at').val(self.quote_data.quote_expires_at);
        $('#quote_no').attr('readonly', 'readonly');

        $("#quote_expires_at").datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date(),
            setDate : new Date()
        });

        //Handle Quote Items
        var quote_items_count = self.quote_items.length;
        if (quote_items_count > 0) {
            for (var i = 0; i < quote_items_count; i++) {
                $(".item_name_" + i).val(self.quote_items[i].item_name)
                var item_cost = parseFloat(self.quote_items[i].item_cost_excGST);
                var item_qty = parseFloat(self.quote_items[i].item_qty);
                var price = $(".item_cost_excGST_" + i).val(item_cost);
                var qty = $(".item_qty_" + i).val(item_qty);
                var total = item_cost * item_qty;
                if (total != '' && total != 0 && !isNaN(total)) {
                    $(".item_total_cost_excGst_" + i).val(total);
                    self.subtotal_excGST = parseFloat(self.subtotal_excGST) + parseFloat(total);
                    self.subtotal_excGST = parseFloat(self.subtotal_excGST).toFixed(2);
                    self.total_incGST = self.subtotal_excGST * self.gst;
                    self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                    $('#sub_total_excGst').val(self.subtotal_excGST);
                    $('#total_incGst').val(self.total_incGST);
                }else if (total == 0 && qty > 0) {
                    $(".item_total_cost_excGst_" + i).val(total);
                    self.subtotal_excGST = parseFloat(self.subtotal_excGST) + parseFloat(total);
                    self.subtotal_excGST = parseFloat(self.subtotal_excGST).toFixed(2);
                    self.total_incGST = self.subtotal_excGST * self.gst;
                    self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                    $('#sub_total_excGst').val(self.subtotal_excGST);
                    $('#total_incGst').val(self.total_incGST);
                }
                $(".item_cost_excGST_" + i).trigger('change');
            }
        }

    } else if (Object.keys(self.lead_data).length > 0) {
        $('#customer_company_name').val(self.lead_data.customer_company_name);
        $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#customer_contact_no').val(self.lead_data.customer_contact_no);
        $('#customer_address').val(self.lead_data.address);
        $('#customer_postcode').val(self.lead_data.postcode);
        $('#rep_name').val(self.lead_data.full_name);

        $("#quote_expires_at").datepicker({
            format: 'dd/mm/yyyy',
            startDate: new Date()
        });
        $("#quote_expires_at").val(moment(new Date()).format('DD/MM/YYYY'));
    }

    /**$('#save_quote').click(function () {
        self.save_quick_quote();
    });*/

    $('#close_quote').click(function () {
        try{
            window.history.back();
            window.close();
        }catch{
            window.history.back();
        }
    });

    $('#generate_quote_pdf').click(function () {
        self.save_quick_quote();
        /**if (self.quote_data.uuid != undefined) {
            var url = base_url + 'admin/lead/quote_pdf_download/' + self.quote_data.uuid;
            document.getElementById('generate_quote_pdf').setAttribute('href', url);
            document.getElementById('generate_quote_pdf').click();
        } else {
            self.save_quick_quote();
        }*/
    });


    self.handle_items();
};

quick_quote_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

quick_quote_manager.prototype.handle_items = function () {
    var self = this;
    $('.item_qty,.item_cost_excGST').change(function () {
        self.subtotal_excGST = 0;
        self.total_incGST = 0;
        $('table#quote_items tr').each(function () {
            var price = $(this).children().find(".item_cost_excGST").val();
            var qty = $(this).children().find(".item_qty").val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(this).children().find(".item_total_cost_excGst").val(total);
                self.subtotal_excGST = parseFloat(self.subtotal_excGST) + parseFloat(total);
                self.subtotal_excGST = parseFloat(self.subtotal_excGST).toFixed(2);
                self.total_incGST = self.subtotal_excGST * self.gst;
                self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#sub_total_excGst').val(self.subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }else if (total == 0 && qty > 0) {
                $(this).children().find(".item_total_cost_excGst").val(total);
                self.subtotal_excGST = parseFloat(self.subtotal_excGST) + parseFloat(total);
                self.subtotal_excGST = parseFloat(self.subtotal_excGST).toFixed(2);
                self.total_incGST = self.subtotal_excGST * self.gst;
                self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#sub_total_excGst').val(self.subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }
        });
    });

};

quick_quote_manager.prototype.validate_quick_quote = function () {
    var self = this;
    var flag = true;
    var elements = document.getElementById("quick_quote_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

quick_quote_manager.prototype.save_quick_quote = function () {
    var self = this;

    var flag = self.validate_quick_quote();
    if (!flag) {
        toastr["error"]('Please fill the fields marked in red');
        return false;
    }

    if (self.quote_data.uuid == undefined) {
        var uuid = self.create_uuid();
        self.quote_data.uuid = uuid;
    }

    var formData = $('#quick_quote_form').serialize();
    formData += '&quote[uuid]=' + self.quote_data.uuid;
    formData += '&quote[lead_id]=' + self.lead_data.id;

    $.ajax({
        url: base_url + 'admin/lead/save_quick_quote',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('Saving Quote Data, Please Wait....');
            $("#save_quick_quote").attr("disabled", "disabled");
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.quote_data.id == undefined) {
                    window.history.pushState(self.quote_data, '', base_url + 'admin/lead/edit_quick_quote/' + self.quote_data.uuid);
                }
                $("#save_quick_quote").removeAttr("disabled");
                var url = base_url + 'admin/lead/quote_pdf_download/' + self.quote_data.uuid;
                window.location.href = url;
                //document.getElementById('generate_quote_pdf').setAttribute('href', url);
                //document.getElementById('generate_quote_pdf').click();
            } else {
                toastr["error"](response.status);
                $("#save_quick_quote").removeAttr("disabled");
                var url = base_url + 'admin/lead/quote_pdf_download/' + self.quote_data.uuid;
                window.location.href = url;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


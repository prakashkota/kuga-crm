
var lead_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? parseInt(options.userid) : '';
    this.user_type = (options.user_type && options.user_type != '') ? options.user_type : '';
    this.is_sales_rep = (options.is_sales_rep && options.is_sales_rep != '') ? options.is_sales_rep : '';
    this.is_lead_allocation_sales_rep = (options.is_lead_allocation_sales_rep && options.is_lead_allocation_sales_rep != '') ? options.is_lead_allocation_sales_rep : '';
    this.uuid = (options.uuid && options.uuid != '') ? (options.uuid) : '';
    this.quote_uuid = (options.quote_uuid && options.quote_uuid != '') ? (options.quote_uuid) : '';;
    this.lead_id = (options.lead_id && options.lead_id != '') ? (options.lead_id) : '';
    this.lead_stages = options.lead_stages;
    this.deal_stage = '';
    this.contact_data = [];
    this.item_count = 0;
    this.site_count = 1;
    this.cust_id = null;
    this.modal_return = '';
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.led_cost_centers = (options.led_cost_centers && options.led_cost_centers != '') ? JSON.parse(options.led_cost_centers) : {};
    this.solar_cost_centers = (options.solar_cost_centers && options.solar_cost_centers != '') ? JSON.parse(options.solar_cost_centers) : {};
    this.residential_cost_centers = (options.residential_cost_centers && options.residential_cost_centers != '') ? JSON.parse(options.residential_cost_centers) : {};

    this.product_types = JSON.parse(options.product_types);
    
    self.add_quote_item_row(0);
    self.add_quote_item_row(0);
    self.add_quote_item_row(0);
    
    
    self.led_cost_centers.splice( 0, 0, {'ccid' : '' , 'name' : 'Select Cost Centre'});
    self.solar_cost_centers.splice( 0, 0, {'ccid' : '' , 'name' : 'Select Cost Centre'});
    self.residential_cost_centers.splice( 0, 0, {'ccid' : '' , 'name' : 'Select Cost Centre'});
    
    if(self.uuid != ''){
        document.getElementById('deal_stages').appendChild(createPlaceHolder(false));
        self.fetch_lead_data();
        self.fetch_tender_lead_contacts();
    }
    
    $('#manual_address_btn').click(function(){
        $(this).addClass('hidden');
        $('#manual_address_hide_btn').removeClass('hidden');
        $('#locationstreetAddress').next().css('display','none');
        $('#locationstreetAddressVal').attr('type','text');
    }); 
 
    $('#manual_address_hide_btn').click(function(){
        $(this).addClass('hidden');
        $('#manual_address_btn').removeClass('hidden');
        $('#locationstreetAddress').next().css('display','block')
        $('#locationstreetAddressVal').attr('type','hidden');
    });
 
    $('#locationstreetAddressVal').keyup(function(){
        $('#select2-locationstreetAddress-container').html($(this).val());
    });

    setTimeout(function(){
        $('.select2-container').addClass('kg-form_field--orange');
    },100);

    $( function() {
        $("#closing_date").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date(),
            autoclose: true,
            onSelect: function(date){
                date = moment(date,'D/M/YYYY').format('M/D/YYYY')
                var tempStartDate = new Date(date);
                var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth()+4, tempStartDate.getDate());
                $('#award_date').datepicker('setDate', default_end); // Set as default 
            }
        });
        $("#closing_date" ).datepicker("setDate", new Date());

        $("#award_date").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date(),
            autoclose: true,
        });
        var end_date = new Date();
        var award_date = new Date(end_date.getFullYear(), end_date.getMonth()+4, end_date.getDate());
        $('#award_date').datepicker('setDate', award_date);
    });
    
    $('#save_lead_details').click(function(){
       self.save_lead_details(); 
    });
    
    //Handle Cusotmer Contact Modal
    $('.add_new_tender_builder_contact_btn').click(function () {
        $('#contact_id').val('');
        $('#contact_name').val('');
        $('#contact_company_name').val('');
        $('#contact_contact_no').val('');
        $('#contact_email').val('');
        $('#add_tender_builder_contact_modal').modal('show');
    });
    


    $('#add_new_quote_btn').click(function(){
        self.prepare_quote_title_and_items();
    });
    
    $(document).on('click','.add_new_product_row_btn', function(){
        var id = $(this).attr('data-site_id');
        self.add_quote_item_row(id);
    });


    $(document).on('click','.delete_item',function(){
        $(this).parent().parent().remove();
    });


    $('body').on('keyup keydown','.item_name',function(){
        var data_id = $(this).attr('data-id');
        var item_type = $('.item_type_' + data_id).val();
        console.log(item_type);
        $(this).autocomplete({
            delay: 300,
            autoSelect: false,
            autoFocus: false,
            minLength: 3,
            cache: false,
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/booking_form/fetch_products",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term,
                        request: 1,
                        cost_centre_id: 1,
                        product_type_id: item_type,
                    },
                    success: function(data) {
                        console.log(data.products);
                        response(data.products);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            search: function(event, ui) {
                $('.item_type_' + data_id).removeClass('is-invalid');
                if (item_type == '') {
                    this.value = '';
                    $('.item_type_' + data_id).addClass('is-invalid');
                    toastr["error"]("Please select product type first.");
                    return false;
                }
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.name);
                return false;
            },
            select: function(event, ui) {
                event.preventDefault();
                
                var type_id = (ui.item.type_id != '' && ui.item.type_id != null) ? parseInt(ui.item.type_id) : 8;
                var item_id = (ui.item.item_id != '' && ui.item.item_id != null) ? ui.item.item_id : '';
                var product_name = ui.item.name;
                var model = (ui.item.model_no != '' && ui.item.model_no != null) ? ui.item.model_no : 'No Model';
                var capacity = (ui.item.capacity != '' && ui.item.capacity != null) ? ui.item.capacity : '';
                var description = (ui.item.description != '' && ui.item.description != null) ? ui.item.description : '';
                var waranty = (ui.item.waranty != '' && ui.item.waranty != null) ? ui.item.waranty : '';
                var price = (ui.item.price != '' && ui.item.price != null) ? ui.item.price : '';
                var data_id = $(this).attr('data-id');

                $('.item_type_' + data_id).val(type_id).attr('required',true);
                $('.item_product_id_' + data_id).val(item_id).attr('required',true);
                $('.item_name_'  + data_id).val(product_name).attr('required',true);
                $('.item_model_' + data_id).val(model).attr('required',true);
                $('.item_description_' + data_id).val(description).attr('required',true);
                $('.item_waranty_' + data_id).val(waranty).attr('required',true);
                $('.item_price_' + data_id).val(price).attr('required',true);
                $('.item_capacity_' + data_id).val(capacity).attr('required',true);
                
                
                return false;
            }
        });
    });

    $(document).on('click','#save_quote',function(){
        self.save_quote_details();
    });

    $(document).on('click','#create_pdf',function(){
        self.save_quote_details(true);
    });

    $(document).on('click','.complete_draft',function(){
        var data = {};
        data.uuid = $(this).attr('data-uuid');
        self.fetch_quotes_details(data);
    });

    $(document).on('click','.download_draft',function(){
        var uuid = $(this).attr('data-uuid');
        self.generate_quote_pdf(uuid);
    });
    
    //Activity Handler
    $('#add_activity').click(function () {
        //Make Activity Attachment Blank
        document.getElementById('activity_attachments').innerHTML = '';
        $('.dz-preview').remove();
        $('.dropzone').removeClass('dz-started');

        var cust_data;
        if (Object.keys(self.lead_data).length > 0) {
            cust_data = self.lead_data;
        } else {
            if (Object.keys(self.cust_data).length > 0) {
                cust_data = self.cust_data;
            } else {
                toastr["error"]('Oops, Please create customer first then add activity.');
                return false;
            }
        }

        $('#scheduler').addClass('hidden');
        $('#attendees').addClass('hidden');
        $('#status').attr('checked','checked');
        $("#activity_type option[value='Scheduled Meeting']").hide();
        $("#scheduled_time_value").val('');
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#mark_as_completed').addClass('hidden');
        $('.datepicker').hide();
        self.activity_action = "Add Activity";
        window.activity_manager_tool.activity_trigger = 1;

        $('#activity_modal_body_left').removeClass('col-md-6');
        $('#activity_modal_body_left').addClass('col-md-12');
    
        $('#activityLocationPostCode').val(cust_data.postcode);
        $('#select2-activityAddress-container').html(cust_data.address);
        $('#activityLocationAddressVal').val(cust_data.address);
        $('#activityLocationState').val(cust_data.state_id).trigger('change');
        $('#activity_modal').modal('show');
    });

    
    // Solar Booking Form
    
    $('#solar_booking_form_btn').click(function () {
        
        if (Object.keys(self.lead_data).length > 0) {
            var url = base_url + 'admin/tender-lead/manage_solar_booking_form?deal_ref=' + self.lead_data.uuid;
            //window.open(url, '_blank');
            
            document.querySelector('#customer_booking_form_location_table_body').innerHTML = "";
            
            var row = self.create_booking_form_customer_locations_table_row(self.lead_data);
            var tbody = document.querySelector('#customer_booking_form_location_table_body');
            tbody.appendChild(row);
            
            $('.customer_booking_form_location_create_btn').attr('data-url', url);
            
            var cost_centre_select_label = document.createElement('label');
            cost_centre_select_label.innerHTML = "Cost Centre";
            cost_centre_select_label.className = "control-label";
            cost_centre_select_label.setAttribute('style','width:150px;');

            var cost_centre_select = document.createElement('select');
            cost_centre_select.className = 'form-control';
            cost_centre_select.setAttribute('id','cost_centre');

            var opArr = self.solar_cost_centers;

            for(var i=0; i<opArr.length;i++){
                var option = document.createElement('option');
                option.setAttribute('value',opArr[i].id);
                option.innerHTML = opArr[i].name;
                cost_centre_select.appendChild(option);
            }

            document.querySelector('#customer_booking_form_cost_centre_body').innerHTML = '';
            document.querySelector('#customer_booking_form_cost_centre_body').appendChild(cost_centre_select_label);
            document.querySelector('#customer_booking_form_cost_centre_body').appendChild(cost_centre_select);
            
            $('#booking_form_choose_customer_location_modal').modal('show');
            
            self.create_booking_form();
        } else {
            toastr["error"]('Oops, Please create customer first then proceed to create a booking form.');
        }
    });
    
    
    //Job Card
    $('#add_job_card').click(function () {
        var is_allowed = self.check_ownership_rules(false,'add_job_card');
        if (!is_allowed) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
            return false;
        }
        var url = base_url + 'admin/tender-lead/add_job_card?deal_ref=' + self.lead_data.uuid;
        window.open(url, '_blank');
    });
    
    self.save_tender_lead_contact();
    self.edit_tender_lead_contact();
    self.fetch_quotes();
    self.fetch_lead_form_data();
};

/** Common Functions **/

lead_manager.prototype.check_ownership_rules = function (msg, action) {
    var self = this;
    var lead_stage = (self.deal_stage == '') ? parseInt(self.lead_data.lead_stage) : parseInt(self.deal_stage);
    var is_sales_rep = parseInt(self.is_sales_rep);
    var lead_user = parseInt(self.lead_data.user_id);

    if(self.is_lead_allocation_sales_rep){
        $('#user_id').attr('disabled', 'dsiabled');
        if(action == 'add_solar_booking_form'){
            if(msg){
               toastr.remove();
                toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
                return false;
            }else{
                return false;
            }
        }
        return true;
    }else if(is_sales_rep){
        if (self.userid == lead_user || self.userid == '29') {
            return true;
        }else{
            //Only prospect ,follow up ,gone cold, won , lost are allowed for any sales rep for other sales rep
            if ((lead_stage == 1 || lead_stage == 2 || lead_stage == 10 || lead_stage == 11 || lead_stage == 12)) {
                return true;
            }else{
                return false;
            }
        }
    } else if (self.userid == lead_user || self.userid == '29') {
        return true;
    }else if (self.user_type == '1' || self.user_type == '6') {
        return true;
    } else {
        if (msg) {
            toastr.remove();
            toastr["error"]('Oops, you dont have appropriate privileges to perform the action.');
        }
        $('#user_id').attr('disabled', 'dsiabled');
        return false;
    }
}

lead_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

lead_manager.prototype.create_deal_stages = function () {
    var self = this;
    var proposal_stages = JSON.parse(self.lead_stages);

    var ul = document.createElement('ul');
    ul.className = "steps";
    var current_lead_stage_data = {};
    for (var i = 0; i < proposal_stages.length; i++) {
        if(proposal_stages[i].id == self.deal_stage){
           current_lead_stage_data = proposal_stages[i];
        }
    }
    for (var i = 0; i < proposal_stages.length; i++) {
        var li = document.createElement('li');
        if (self.deal_stage != '') {
            if (parseInt(proposal_stages[i].order_no) < parseInt(current_lead_stage_data.order_no)) {
                li.className = "done";
            } else if (parseInt(proposal_stages[i].id) == parseInt(self.deal_stage)) {
                li.className = "active";
                self.deal_stage_name = proposal_stages[i].stage_name;
            } else {
                li.className = "undone";
            }
        } else {
            if (i == 0) {
                li.className = "active";
            } else {
                li.className = "undone";
            }
        }
        li.innerHTML = '<a href="javascript:void(0);" class="deal_stage" data-id="' + proposal_stages[i].id + '" data-name="' + proposal_stages[i].stage_name + '">' + proposal_stages[i].stage_name + '</a>';
        ul.appendChild(li);
    }
    if (self.deal_stage == '') {
        self.deal_stage = proposal_stages[0].id;
        self.deal_stage_name = proposal_stages[0].stage_name;
    }

    document.getElementById('deal_stages').innerHTML = '';
    document.getElementById('deal_stages').appendChild(ul);
    self.change_deal_stage();
};

lead_manager.prototype.change_deal_stage = function () {
    var self = this;
    $('.deal_stage').click(function () {
        var id = $(this).attr('data-id');
        self.prev_deal_stage_name = self.deal_stage_name;
        self.deal_stage_name = $(this).attr('data-name');
        self.prev_deal_stage = self.deal_stage;
        self.deal_stage = id;
        self.create_deal_stages();
        self.update_lead_stage();
    });
};

/** Customer Save and Lead Save Related Functions **/

lead_manager.prototype.validate_lead_data = function () {
    var self = this;
    
    var flag = true;
    
    var elements = document.getElementById('tender_lead_form').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }

    return flag;
};

lead_manager.prototype.save_lead_details = function () {
    var self = this;
    
    var flag = self.validate_lead_data();
    
    if(!flag){
        toastr['error']('Please fill the required fields marked in red');
        return false;
    }

    var formData = $('#tender_lead_form').serialize() + '&uuid='+self.uuid;

    $.ajax({
        url: base_url + 'admin/tender-lead/save_lead_details',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr["info"]('Saving Tender Details...');
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                toastr["success"](response.status);
                self.fetch_lead_data(response);
            } else {
                toastr["error"](response.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.update_lead_stage = function () {
    var self = this;
    
    var data = {};
    data.deal_stage = self.deal_stage;
    data.uuid = self.uuid;
    data.action = 'update_proposal';
    
    $.ajax({
        url: base_url + "admin/tender-lead/update_lead_stage",
        type: 'post',
        dataType: "json",
        data: data,
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
            } else {
                toastr["error"](data.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.fetch_lead_data = function () {
    var self = this;

    $.ajax({
        url: base_url + 'admin/tender-lead/fetch_lead_data',
        type: 'get',
        data: {'uuid' : self.uuid},
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                self.handle_lead_data(response);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.handle_lead_data = function (data) {
    var self = this;
    
    var ld = data.lead_data;
    
    $('#contact_lead_id').val(ld['id']);
    $('#dd_company_name').html(ld['project_name']);
    $('#select2-locationstreetAddress-container').html(ld['address']);
    
    if (self.lead_stages != '') {
        self.deal_stage = parseInt(ld['lead_stage']);
        self.create_deal_stages();
    }
    
    for(var key in ld){
        if(key == 'closing_date'){
            var date = moment(ld[key],'YYYY-M-D').format('M/D/YYYY')
            var tempStartDate = new Date(date);
            var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate());
            $('#closing_date').datepicker('setDate', default_end);
        }else if(key == 'award_date'){
            var date = moment(ld[key],'YYYY-M-D').format('M/D/YYYY')
            var tempStartDate = new Date(date);
            var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate());
            $('#award_date').datepicker('setDate', default_end);
        }else{
            $('[name="lead['+key+']"]').val(ld[key]);
        }
    }
}

/** Lead Contact Releated Functions **/

lead_manager.prototype.create_tender_lead_contacts_table_row = function (data) {
    var tr = document.createElement('tr');
    
    var td_company_name = document.createElement('td');
    td_company_name.innerHTML = data.company_name;
    
    var td_contact_name = document.createElement('td');
    td_contact_name.innerHTML = data.name;
    
    var td_phone = document.createElement('td');
    td_phone.innerHTML = data.contact_no;
    
    var td_email = document.createElement('td');
    td_email.innerHTML = data.email;
    
    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm tender_lead_contact_edit_btn";
    action_edit_btn.setAttribute('data-id', data.id);
    action_edit_btn.setAttribute('href', 'javascript:void(0);');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    td_action.appendChild(action_edit_btn);

    
    tr.appendChild(td_company_name);
    tr.appendChild(td_contact_name);
    tr.appendChild(td_phone);
    tr.appendChild(td_email);
    tr.appendChild(td_action);

    return tr;
}

lead_manager.prototype.validate_tender_lead_contact_data = function () {
    var self = this;
    
    var flag = true;
    
    var elements = document.getElementById('customerContactAdd').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }

    return flag;
};

lead_manager.prototype.save_tender_lead_contact = function () {
    var self = this;
    $(document).on('click', '#save_tender_lead_contact', function () {
        var contact_id = $('#contact_id').val();
        var flag = self.validate_tender_lead_contact_data();
        
        if (!flag) {
            toastr.clear();
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        
        if (flag) {
            var customerContactAddForm = $('#customerContactAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/tender-lead/save_tender_lead_contact',
                datatype: 'json',
                data: customerContactAddForm,
                beforeSend: function () {
                    if (contact_id != '' && contact_id != null && contact_id != undefined) {
                        toastr["info"]('Updating Customer Contact please wait....');
                    } else {
                        toastr["info"]('Creating Customer Contact into the system....');
                    }
                    $('#tender_lead_contact_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_tender_lead_contact").attr("disabled", "disabled");
                },
                success: function (stat) {
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        var contact_data = stat.contact;
                        document.querySelector('#tender_lead_contact_table_body').innerHTML = '';
                        for (var key in contact_data) {
                            self.contact_data[contact_data[key].id] = contact_data[key];
                            var row = self.create_tender_lead_contacts_table_row(contact_data[key]);
                            var tbody = document.querySelector('#tender_lead_contact_table_body');
                            tbody.appendChild(row);
                        }
                        $('#add_tender_builder_contact_modal').modal('hide');
                        $("#save_tender_lead_contact").removeAttr("disabled");
                        $('#tender_lead_contact_loader').html('');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_tender_lead_contact").removeAttr("disabled");
                        $('#tender_lead_contact_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_tender_lead_contact").removeAttr("disabled");
                    $('#tender_lead_contact_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};

lead_manager.prototype.fetch_tender_lead_contacts = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/tender-lead/fetch_tender_lead_contacts',
        datatype: 'json',
        data: {lead_id: self.lead_id},
        beforeSend: function () {
            document.getElementById('tender_lead_contact_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('tender_lead_contact_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#tender_lead_contact_table_body').html('');
                if (stat.contacts.length > 0) {
                    for (var i = 0; i < stat.contacts.length; i++) {

                        if(i == 0){
                            self.cust_id = stat.contacts[i].id;
                        }
                        self.contact_data[stat.contacts[i].id] = stat.contacts[i];
                        var row = self.create_tender_lead_contacts_table_row(stat.contacts[i]);
                        var tbody = document.querySelector('#tender_lead_contact_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    $('#tender_lead_contact_table_body').html('<tr><td colspan="5" class="text-center">No additional contacts found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#tender_lead_contact_table_body').html('');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_manager.prototype.edit_tender_lead_contact = function () {
    var self = this;
    $(document).on('click','.tender_lead_contact_edit_btn',function () {
        
        var contact_id = $(this).attr('data-id');
        var data = self.contact_data[contact_id];

        $('#contact_id').val(data.id);
        $('#contact_name').val(data.name);
        $('#contact_company_name').val(data.company_name);
        $('#contact_contact_no').val(data.contact_no);
        $('#contact_email').val(data.email);

        $('#add_tender_builder_contact_modal_title').html('Edit Builder');
        $('#add_tender_builder_contact_modal').modal('show');
        
    });
};


lead_manager.prototype.fetch_lead_form_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/tender-lead/fetch_lead_form_data',
        datatype: 'json',
        data: {lead_id: self.lead_data.id},
        beforeSend: function () {
            //$('#customer_location_table_body').html(createLoader('Please wait while data is being loaded'));
            document.getElementById('lead_forms_data_table_body').appendChild(createPlaceHolder(false));
            document.getElementById('lead_forms_data_table_body').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#lead_forms_data_table_body').html('');
                //Handle Booking Form Rows
                if (stat.booking_form_data.length > 0) {
                    for (var i = 0; i < stat.booking_form_data.length; i++) {
                        var row = self.create_lead_booking_form_table_row(stat.booking_form_data[i]);
                        var tbody = document.querySelector('#lead_forms_data_table_body');
                        tbody.appendChild(row);
                    }
                }
                
                // Handle Job Card Rows
                if (stat.job_card_data.length > 0) {
                    for (var i = 0; i < stat.job_card_data.length; i++) {
                        var row = self.create_job_card_data_form_table_row(stat.job_card_data[i]);
                        var tbody = document.querySelector('#lead_forms_data_table_body');
                        tbody.appendChild(row);
                    }
                } else {
                    //document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td>Quick Quote</td><td colspan="3" class="text-center">No form data found.</td></tr>';
                }
                
                if ((stat.booking_form_data.length == 0) && (stat.meter_data.length == 0) && (stat.quote_data.length == 0)) {
                    document.querySelector('#lead_forms_data_table_body').innerHTML += '<tr><td colspan="4" class="text-center">No form data found.</td></tr>';
                }
                $('[data-toggle="tooltip"]').tooltip();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#lead_forms_data_table_body').html('<tr><td colspan="5" class="text-center">No form data found.</td></tr>');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


lead_manager.prototype.create_lead_booking_form_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');
    
    var business_details = (typeof data.business_details == 'string') ? JSON.parse(data.business_details) : data.business_details;
    var td_site = document.createElement('td');
    td_site.innerHTML = business_details.address;

    var td_bf_type = document.createElement('td');
    td_bf_type.innerHTML = 'Booking Form - ' + data.type + '(' + data.type_name + ')';

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = ((data.kuga_job_id != null && data.kuga_job_id != '')) ? "btn-info btn-sm" : "btn-danger btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        action_edit_btn.setAttribute('href', base_url + 'admin/tender-lead/edit_solar_booking_form/' + data.uuid);
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    if ((data.kuga_job_id != null && data.kuga_job_id != '')) {
        action_edit_btn.innerHTML = '<i class="fa fa-eye"></i> View / Edit';
    } else {
        action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Complete Draft';
    }
    
    var action_generate_pdf_btn = document.createElement('a');
    action_generate_pdf_btn.className = "btn-warning btn-sm mr-2";
    action_generate_pdf_btn.setAttribute('href', base_url + 'admin/tender-lead/solar_booking_form_generate_draft_pdf/' + data.uuid);
    action_generate_pdf_btn.setAttribute('target','__blank');
    action_generate_pdf_btn.innerHTML = '<i class="fa fa-file-pdf-o"></i> Generate Pdf';
    td_action.appendChild(action_generate_pdf_btn);

    var action_btn_group = document.createElement('div');
    action_btn_group.className = 'btn-group';

    var action_btn_group_btn = document.createElement('button');
    action_btn_group_btn.setAttribute('type', 'button');
    action_btn_group_btn.className = 'btn btn-info dropdown-toggle mr-1 mb-1';
    action_btn_group_btn.setAttribute('data-toggle', 'dropdown');
    action_btn_group_btn.setAttribute('aria-haspopup', 'true');
    action_btn_group_btn.setAttribute('aria-expanded', 'false');
    action_btn_group_btn.innerHTML = 'File';

    var action_btn_group_dropdown_menu = document.createElement('div');
    action_btn_group_dropdown_menu.className = 'dropdown-menu';

    if ((data.kuga_job_id != null && data.kuga_job_id != '')) {
        var action_reupload_btn = document.createElement('a');
        action_reupload_btn.className = "btn-warning btn-sm mr-2 upload_simpro_attachment_btn";
        action_reupload_btn.setAttribute('href', 'javascript:void(0);');
        action_reupload_btn.setAttribute('data-id', data.uuid);
        action_reupload_btn.setAttribute('data-type', data.type);
        action_reupload_btn.innerHTML = '<i class="fa fa-upload" aria-hidden="true"></i> ReUpload';
        
        var action_download_btn = document.createElement('a');
        action_download_btn.className = "btn-primary btn-sm mr-2";
        action_download_btn.setAttribute('href', base_url + 'assets/uploads/solar_booking_form_files/' + data.pdf_file);
        action_download_btn.setAttribute('target', '__blank');
        action_download_btn.innerHTML = '<i class="fa fa-download" aria-hidden="true"></i> Download';
        
        if (data.pdf_file != null && data.pdf_file != '') {
            td_action.appendChild(action_download_btn);
        }

        if (data.simpro_attachment_id == null || data.simpro_attachment_id == '' || !data.pdf_file_size.includes('MB')) {
            var error_tooltip = document.createElement('span');
            error_tooltip.innerHTML = '<a href="#" data-toggle="tooltip" title="Looks like the booking form didnt uploaded properly on simPRO. Please try to reupload it agian."  style="color: red; margin-right: 10px;font-size: 13px; "><i class="fa fa-warning"></i></a>';
            error_tooltip.setAttribute('id', 'error_bf_'+ data.uuid);
            action_download_btn.setAttribute('id', 'download_bf_'+ data.uuid);
        }

        if (data.pdf_file != null && data.pdf_file != '') {
            action_download_btn.setAttribute('id', 'download_bf_'+ data.uuid);
            td_action.appendChild(action_download_btn);
        }
    }else{
        var action_download_btn = document.createElement('a');
        action_download_btn.className = "btn-primary btn-sm mr-2";
        action_download_btn.setAttribute('href', base_url + 'assets/uploads/solar_booking_form_files/' + data.pdf_file);
        action_download_btn.setAttribute('target', '__blank');
        action_download_btn.innerHTML = '<i class="fa fa-download" aria-hidden="true"></i> Download';
        
        if (data.pdf_file != null && data.pdf_file != '') {
            td_action.appendChild(action_download_btn);
        }
    }
    
    td_action.appendChild(action_edit_btn);
    tr.appendChild(td_bf_type);
    tr.appendChild(td_site);
    tr.appendChild(td_date);
    tr.appendChild(td_action);

    return tr;
};


lead_manager.prototype.create_job_card_data_form_table_row = function(data){
    var self = this;
    
    var job_card_details = (typeof data.job_form_data == 'string') ? JSON.parse(data.job_form_data) : data.job_form_data;
    var tr = document.createElement('tr');

    var td_site = document.createElement('td');
    td_site.innerHTML = job_card_details.site_address;

    var td_bf_type = document.createElement('td');
    td_bf_type.innerHTML = 'Job Card Form';

    var td_date = document.createElement('td');
    td_date.innerHTML = moment(data.created_at).format('LLL');

    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = (data.status != null && data.status != '' && data.status != 0) ? "btn-info btn-sm" : "btn-danger btn-sm";

    var is_allowed = self.check_ownership_rules();
    if (is_allowed) {
        action_edit_btn.setAttribute('href', base_url + 'admin/lead/edit_job_card_form/' + data.uuid);
    } else {
        action_edit_btn.setAttribute('href', 'javascript:void(0)');
        action_edit_btn.setAttribute('onclick', 'window.lead_manager.check_ownership_rules(true)');
    }

    //action_edit_btn.setAttribute('target', '__blank');
    if ((data.status != null && data.status != '' && data.status != 0)) {
        action_edit_btn.innerHTML = '<i class="fa fa-eye"></i> View / Edit';
    } else {
        action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Complete Draft';
    }
    
    var action_generate_pdf_btn = document.createElement('a');
    action_generate_pdf_btn.className = "btn-warning btn-sm mr-2";
    action_generate_pdf_btn.setAttribute('href', base_url + 'admin/lead/generate_draft_card_pdf/' + data.uuid+'?ref=download');
    action_generate_pdf_btn.setAttribute('target','__blank');
    action_generate_pdf_btn.innerHTML = '<i class="fa fa-file-pdf-o"></i> Generate Pdf';
    td_action.appendChild(action_generate_pdf_btn);

    var action_download_btn = document.createElement('a');
    action_download_btn.className = "btn-primary btn-sm mr-2";
    action_download_btn.setAttribute('href', data.job_form_file);
    action_download_btn.setAttribute('target', '__blank');
    action_download_btn.innerHTML = '<i class="fa fa-download" aria-hidden="true"></i> Download';

    if ((data.status != null && data.status != '' && data.status != 0)) {
        action_download_btn.setAttribute('id', 'download_bf_'+ data.uuid);
        td_action.appendChild(action_download_btn);
    }
    
    td_action.appendChild(action_edit_btn);
    tr.appendChild(td_bf_type);
    tr.appendChild(td_site);
    tr.appendChild(td_date);
    tr.appendChild(td_action);

    return tr;
};



/** Quote Related Functions **/
lead_manager.prototype.prepare_quote_title_and_items = function (item = false) {
    var self = this;
    var br = document.createElement('br');

    var div = document.createElement('div');
    div.className = "row";

    var title_div = document.createElement('div');
    title_div.className = 'col-md-3';

    var title_input_div = document.createElement('div');
    title_input_div.className = 'form-group control-group';

    var title_input = document.createElement('input');
    title_input.className = 'form-control';
    title_input.setAttribute('type','text');
    title_input.setAttribute('name','quote[quote_details]['+self.site_count+'][title]');
    title_input.setAttribute('id','quote_title_'+self.site_count);
    title_input.setAttribute('placeholder','Title');

    var products = '';
    if(item){
        title_input.value = item.title;
        products = item.product_details;
    }

    title_input_div.appendChild(title_input);
    title_div.appendChild(title_input_div);

    var button_div = document.createElement('div');
    button_div.className = 'col-md-9';

    var add_site_button = document.createElement('a');
    add_site_button.className = 'pull-right add_new_product_row_btn';
    add_site_button.setAttribute('href','javascript:void(0);');
    add_site_button.setAttribute('style','margin-left:20px;');
    add_site_button.setAttribute('data-site_id',self.site_count);
    add_site_button.innerHTML = '<i class="fa fa-plus"></i> Add Row';

    button_div.appendChild(add_site_button);
    div.appendChild(title_div);
    div.appendChild(button_div);

    // Table Row
    var table_div = document.createElement('div');
    table_div.className = "table-responsive";

    var table = document.createElement('table');
    table.setAttribute('width','100%');
    table.setAttribute('border','0');
    table.setAttribute('class','table table-striped');
    table.setAttribute('id','quote_items_table_'+self.site_count);

    var thead = document.createElement('thead');
    var tr = document.createElement('tr');

    var type_th = document.createElement('th');
    type_th.innerHTML = 'Product Type';

    var name_th = document.createElement('th');
    name_th.innerHTML = 'Product Name';

    var model_th = document.createElement('th');
    model_th.innerHTML = 'Model';

    var capacity_th = document.createElement('th');
    capacity_th.innerHTML = 'Capacity';

    var desc_th = document.createElement('th');
    desc_th.innerHTML = 'Description';

    var warranty_th = document.createElement('th');
    warranty_th.innerHTML = 'Warranty';

    var price_th = document.createElement('th');
    price_th.innerHTML = 'Price';

    var action_th = document.createElement('th');
    action_th.innerHTML = 'Action';

    tr.appendChild(type_th);
    tr.appendChild(name_th);
    tr.appendChild(model_th);
    tr.appendChild(capacity_th);
    tr.appendChild(desc_th);
    tr.appendChild(warranty_th);
    tr.appendChild(price_th);
    tr.appendChild(action_th);

    var tbody = document.createElement('tbody');
    tbody.setAttribute('id','quote_items_table_body_'+self.site_count);

    thead.appendChild(tr);
    table.appendChild(thead);
    table.appendChild(tbody);

    table_div.appendChild(table);

    document.querySelector('#site_details').appendChild(br);
    document.querySelector('#site_details').appendChild(div);
    document.querySelector('#site_details').appendChild(table_div);

    

    if(products != ''){
        for(var i = 0; i < products.item_name.length; i++){
            if(products.item_name[i] !== ''){
                var data = {};
                data.item_name = products.item_name[i];
                data.item_capacity = products.item_capacity[i];
                data.item_description = products.item_description[i];
                data.item_price = products.item_price[i];
                data.item_waranty = products.item_waranty[i];
                data.model = products.model[i];
                data.product_id = products.product_id[i];
                data.product_type = products.product_type[i];

                self.add_quote_item_row(self.site_count, data);
            }
        }

    }else{
        self.add_quote_item_row(self.site_count);
        self.add_quote_item_row(self.site_count);
        self.add_quote_item_row(self.site_count);
    }

    self.site_count++;
    
};


lead_manager.prototype.add_quote_item_row = function (id, data=false) {
    var self = this;
    
    var tr = document.createElement('tr');
    tr.className = "stock_row_"+self.item_count;
    tr.setAttribute('width','100%');
    tr.setAttribute('style','border-top:solid 1px #000000; background: rgba(55,55,55,0.1); padding:0px;');
    tr.setAttribute('height','30');

    var rhtml = '';
    
    rhtml += '<td style="padding:0px;">';
    rhtml += '<select  class="form-control item_type item_type_'+self.item_count+'" type="text" data-id="'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][product_type][]">';
    rhtml += '<option value="">Select Product Type</option>';
    for(var key in self.product_types){

        var selected = "";
        if(data && data.product_type == self.product_types[key]['type_id']){
            selected = "selected";
        }
        rhtml += '<option '+selected+' value="'+self.product_types[key]['type_id']+'">'+self.product_types[key]['type_name']+'</option>';
    }
    rhtml += '</select></td>';

    var name = product_id = model = description = capacity = warranty = price ='';
    if(data){
        name = data.item_name;
        product_id = data.product_id;
        model = data.model;
        description = data.item_description;
        capacity = data.item_capacity;
        warranty = data.item_waranty;
        price = data.item_price;
    }
    rhtml += '<td style="padding:0px;"><input  class="form-control item_product_id item_product_id_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][product_id][]" type="hidden" value="'+product_id+'" data-id="'+self.item_count+'" /><input  class="form-control item_name item_name_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][item_name][]" type="text" value="'+name+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><input  class="form-control item_model item_model_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][model][]"  type="text" value="'+model+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><input  class="form-control item_capacity item_capacity_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][item_capacity][]"  type="number" value="'+capacity+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><input  class="form-control item_description item_description_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][item_description][]"  type="text" value="'+description+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><input  class="form-control item_waranty item_waranty_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][item_waranty][]"  type="text" value="'+warranty+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><input  class="form-control item_price item_price_'+self.item_count+'" name="quote[quote_details]['+id+'][product_details][item_price][]"  type="text" value="'+price+'" data-id="'+self.item_count+'" /></td>';
    rhtml += '<td style="padding:0px;"><a href="javascript:void(0);" class="delete_item btn kuga-btn" data-id="'+self.item_count+'">Delete</a></td>'
    tr.innerHTML = rhtml;

    document.querySelector('#quote_items_table_body_'+id).appendChild(tr);
    self.item_count++;
};


lead_manager.prototype.validate_quote_details_data = function(){
    var self = this;
    
    var flag = true;
    
    var elements = document.getElementById('tender_quotes_form').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }

    return flag;
};

lead_manager.prototype.generate_quote_pdf = function(uuid = ''){
    var self = this;
    var id = (uuid != '') ? uuid : self.quote_uuid;
    var url = base_url + 'admin/tender-lead/quote_pdf_download/' + id;
    document.getElementById('create_pdf_download').setAttribute('href', url);
    document.getElementById('create_pdf_download').click();
};


lead_manager.prototype.reset_quote_form = function(){
    var self = this;
    self.site_count = 0;
    self.item_count = 0;
    document.querySelector('#site_details').innerHTML = '';
    self.prepare_quote_title_and_items();
    self.add_quote_item_row(self.site_count);
};

lead_manager.prototype.fetch_quotes = function(){
    var self = this;

    $.ajax({
        url : base_url + 'admin/tender-lead/fetch_quotes',
        type: 'post',
        data: { lead_id : self.lead_id},
        dataType: 'json',
        beforeSend: function(){

        },
        success: function(stat){

            if(stat.success){
                document.querySelector('#quotes_table_body').innerHTML = '';
                var quotes = stat.quotes;
                quotes.map(function(value){
                    var row = self.create_quotes_table_row(value);
                    document.querySelector('#quotes_table_body').appendChild(row);
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })

};

lead_manager.prototype.create_quotes_table_row = function(data){
    var self = this;

    var tr = document.createElement('tr');

    var create_at = document.createElement('td');
    create_at.innerHTML = (data.created_at) ? moment(data.created_at).format('M/D/YYYY') : '';

    var auote_id = document.createElement('td');
    auote_id.innerHTML= (data.quote_id) ? '#'+data.quote_id : '';

    var actions = document.createElement('td');
    actions.innerHTML = '<a href="javascript:void(0);" data-uuid="'+data.uuid+'" class="btn-info btn-sm complete_draft" style="margin-right: 10px;">Complete Draft</a><a href="javascript:void(0);" data-uuid="'+data.uuid+'" class="btn-success btn-sm download_draft">Download Draft</a>';

    tr.appendChild(create_at);
    tr.appendChild(auote_id);
    tr.appendChild(actions);

    return tr;
};


lead_manager.prototype.save_quote_details = function(is_pdf = false){

    var self = this;

    var flag = self.validate_quote_details_data();
    
    if(!flag){
        toastr['error']('Please fill the required fields marked in red');
        return false;
    }

    if (self.quote_uuid == '') {
        var quote_uuid = self.create_uuid();
        self.quote_uuid = quote_uuid;
    }

    var formData = $('#tender_quotes_form').serialize() + '&lead_id='+self.lead_id + '&uuid='+self.quote_uuid+ '&cust_id=' +self.cust_id;
    $.ajax({
        url : base_url + 'admin/tender-lead/save_quote_details',
        type: 'post',
        data: formData,
        dataType : 'json',
        beforeSend: function(){
            toastr.remove();
            toastr["info"]("Saving Data to Job CRM. Please Wait...");
            $("#save_quote").attr("disabled", "disabled");
            $("#create_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function(stat){
            toastr.remove();
            $.isLoading("hide");

            if(stat.success){
                toastr["success"](stat.status);
                if(is_pdf){
                    self.generate_quote_pdf();
                    self.reset_quote_form();
                }
                self.fetch_quotes();
            }else{
                toastr["error"](stat.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        } 
    })
};


lead_manager.prototype.fetch_quotes_details = function(data){
    var self = this;

    self.item_count = 0;
    self.site_count = 0;

    $.ajax({
        url : base_url + 'admin/tender-lead/fetch_quotes_details',
        type : 'post',
        data: data,
        dataType : 'json',
        beforeSend : function(){

        },
        success: function(stat){

            if(stat.success){
                self.handle_quote_items(stat.quote_detail);
            }else{
                toastr['error'](stat.status);
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            toastr['error'](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
};

lead_manager.prototype.handle_quote_items = function(data){
    var self = this;

    $('#quote_id').val(data.quote_id);

    self.quote_uuid = data.uuid;

    var quote_details = (typeof data.quote_data == 'string') ? JSON.parse(data.quote_data) : data.quote_data;
    document.querySelector('#site_details').innerHTML = '';
    
    for(var k in quote_details.quote_details){
        var id = self.site_count;
        var items = quote_details.quote_details[k];
        self.prepare_quote_title_and_items(items);
    }

    $('[name="quote[inclusions]"]').val(quote_details.inclusions);
};


lead_manager.prototype.create_booking_form_customer_locations_table_row = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td_address = document.createElement('td');
    td_address.innerHTML = data.address;
    
    var td_action = document.createElement('td');
    var action_create_btn = document.createElement('a');
    action_create_btn.className = "btn-info btn-sm customer_booking_form_location_create_btn";
    action_create_btn.setAttribute('data-id', data.id);
    action_create_btn.setAttribute('data-url', data.id);
    action_create_btn.setAttribute('href', 'javascript:void(0);');
    action_create_btn.innerHTML = '<i class="fa fa-plus"></i> Add';

    td_action.appendChild(action_create_btn);

    tr.appendChild(td_address);
    tr.appendChild(td_action);

    return tr;
};

lead_manager.prototype.create_booking_form = function (data) {
    $('.customer_booking_form_location_create_btn').click(function () {
        var cost_centre = $('#cost_centre').val();
        if(cost_centre == ''){
            toastr['error']('Please Select Cost Centre First in Order to Create Booking Form.');
            return false;
        }
        var site_id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        url += '&cost_centre_id='+cost_centre;
        window.open(url, '_blank');
    });
};
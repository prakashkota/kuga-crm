<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/payment.png'; ?>" alt="" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;">
                                    <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_30" name="booking_form[is_upfront_30]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment</td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <?php /**
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">No Deposit</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_0" name="booking_form[is_upfront_30_deposit_0]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"> 50% on booking in the job</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_50_before" name="booking_form[is_upfront_30_deposit_50_before]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">50% on handover of CES</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_50_after" name="booking_form[is_upfront_30_deposit_50_after]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            */ ?>
                            <tr >
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Amount</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_0" name="booking_form[is_upfront_30_deposit_0]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"> Upon Installation Completion </td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_50_before" name="booking_form[is_upfront_30_deposit_50_before]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr style="display:none;">
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_30_deposit_50_after" name="booking_form[is_upfront_30_deposit_50_after]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_30_excGST" name="booking_form[is_upfront_30_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
                <!--<tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_99" name="booking_form[is_upfront_99]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 30kW - 100kW </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Payment (30%)</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_99_deposit_30" name="booking_form[is_upfront_99_deposit_30]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Due upon handover of Certificate of Electrical Safety (70%)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_99_deposit_70" name="booking_form[is_upfront_99_deposit_70]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_99_excGST" name="booking_form[is_upfront_99_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_upfront_100" name="booking_form[is_upfront_100]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 1 - Upfront Payment for 100kW </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Deposit Payment (20%)</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_20" name="booking_form[is_upfront_100_deposit_20]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Payment on installation commencement (70%)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_70" name="booking_form[is_upfront_100_deposit_70]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Upon providing Certificate of Electrical Safety (10%)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_upfront_100_deposit_10" name="booking_form[is_upfront_100_deposit_10]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable</strong></td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">exc GST:</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input  id="is_upfront_100_excGST" name="booking_form[is_upfront_100_excGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>  -->
                <tr>
                    <td height="30">&nbsp;</td>
                </tr>
           
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_finance" name="booking_form[is_finance]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 2 - Finance</td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <!--<tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">70% on project commencement</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc Gst:</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_70_incGST" name="booking_form[is_finance_70_incGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">30% on handover of CES</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"></td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_30_incGST" name="booking_form[is_finance_30_incGST]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>-->
                            <tr>
                                <td width="540" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Fortnightly Repayments</td>
                                <td width="134" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">inc GST:</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_monthly_repayments" name="booking_form[is_finance_monthly_repayments]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                            <tr>
                                <td height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Term (years)</td>
                                <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">&nbsp;</td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_finance_terms" name="booking_form[is_finance_terms]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" /></td>
                            </tr>
                        </table></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
				<tr>
					<td>
					
					Signing the order form is evidence of your intention to be bound by this agreement. Such signing include signing by the owner, operator,authorised person, landlord or tenant of company or businesses and you warrant that any such signature is binding on you. </br></br>
					
                    Without prejudice to any other rights or remedy, we may terminate this agreement with your if you are in breach of any of these terms and conditions, and you may terminate if we are in material breach of these terms and conditions. </br></br>

                    'This agreement' or 'the agreement' means and includes these provisions, and any and all documents referenced in it and attached to it and/or provided to you in relation to it, including most recent quote, and technical documents.
					
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
                    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="415" valign="top" style="padding-right:10px">
                                <table width="405" border="0" cellspacing="0" cellpadding="0">
                                    
                                    <tr>
                                        <td height="13"></td>
                                    </tr>
                                    <tr>
                                        <td height="23" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">Authorised Person:</td>
                                    </tr>
                                    
        							<!--<tr>
                                        <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                            <input  id="authorised_on_behalf_company_name" name="booking_form[authorised_on_behalf][company_name]" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr> -->
                                    <tr>
                                        <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                                    <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                        <input  id="authorised_on_behalf_name" name="booking_form[authorised_on_behalf][name]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                                    </td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                                    <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                        <input  id="authorised_on_behalf_date" name="booking_form[authorised_on_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                                    </td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">CLIENT SIGNATURE:</td>
                                                    
                                                    <td align="center" style=" padding:5px; height: 180px; ">
                                                        <div class="signature-pad">
                                                            <div class="signature-pad--body">
                                                                <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_1" data-id="1" class="sign_create"></div>
                                                                <input required="" id="authorised_on_behalf_signature" name="booking_form[authorised_on_behalf][signature]" type="hidden" />
                                                            </div>
                                                        </div>
                                                        <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                            <img src="" />
                                                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                        </div>
                                                    </td>
                                                   
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table> 
                            </td>
                            <td width="415" valign="top" style="padding-left:10px">
                                <table width="405" border="0" cellspacing="0" cellpadding="0">
                                    
                                    <tr>
                                        <td height="35" valign="top"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="2" width="116" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#333333">On behalf of Kuga Electrical:</td>
                                                    
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="121" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Name:</td>
                                                    <td width="284" valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                        <select id="authorised_by_behalf_name" name="booking_form[authorised_by_behalf][name]" style="padding:5px; width:100%; height: inherit;" required="" >
                                                            <option value="">Please Select</option>
                                                            <?php
                                                            if (!empty($users)) {
                                                                foreach ($users as $key => $value) {
                                                                    ?>
                                                                    <option value="<?php echo $value['full_name']; ?>" ><?php echo $value['full_name']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Date:</td>
                                                    <td valign="top" style="height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px; color:#333333">
                                                        <input  id="authorised_by_behalf_date" name="booking_form[authorised_by_behalf][date]" style="padding:5px; width:100%; height: inherit;" type="text" required="" /> 
                                                    </td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="405" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; font-weight:bold">Sales Consultant SIGNATURE:</td>
                                                   
                                                    <td align="center" style=" padding:5px; height: 180px; ">
                                                        <div class="signature-pad">
                                                            <div class="signature-pad--body">
                                                                <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_2" data-id="2" class="sign_create"></div>
                                                                 <input required="" id="authorised_by_behalf_sales_rep_signature" name="booking_form[authorised_by_behalf][sales_rep_signature]" type="hidden" />
                                                            </div>
                                                        </div>
                                                        <div class="signature-pad-image signature_image_container" style="display:none !important;">
                                                            <img src="" />
                                                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                                        </div>
                                                    </td>
                                                    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
               <!-- <tr>
                    <td height="30">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                            <tr>
                                <th height="35" colspan="3" align="left" class="black-bg" style="background:#010101;"> <table width="816" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="37" align="center"><input id="is_energy_plan" name="booking_form[is_energy_plan]" style="margin: 5px; width:100%; height: 20px;" type="checkbox" /></td>
                                            <td width="779" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Option 3 - Energy Plan </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_8" name="booking_form[is_energy_plan_8]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 8YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_8_kwh" name="booking_form[is_energy_plan_8_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_12" name="booking_form[is_energy_plan_12]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 12YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_12_kwh" name="booking_form[is_energy_plan_12_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                            <tr>
                                <td width="540" colspan="2" height="35" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_15" name="booking_form[is_energy_plan_15]" style="margin: 5px; width:20px; height: 20px;" type="checkbox" /> 15YR Plan</td>
                                <td width="148" style="font-family:Arial, Helvetica, sans-serif; font-size:14px"><input id="is_energy_plan_15_kwh" name="booking_form[is_energy_plan_15_kwh]" style="padding:5px; width:75%; height: inherit;" min="1" type="number" /> /kWh</td> 
                            </tr>
                        </table></td>
                </tr> -->
            </table>
        </td>
    </tr>
    <tr>
        <td height="400">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
<?php

class Customer_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch franchise
     * @param type $id
     * @return array
     */
    function get_customers($id = FALSE) {
        $sql = "SELECT c.*"
                . " FROM tbl_customers as c";
        $cond = "";
        if ($id) {
            $cond .= " WHERE c.id='" . $id . "'";
            $sql .= " $cond GROUP BY c.id";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

    function get_customer_location($id = FALSE, $cust_id = FALSE, $keyword = FALSE) {
        
        $sp_count = "Coalesce((SELECT COUNT(sp.id) FROM tbl_solar_proposal sp WHERE sp.site_id = cl.id),0) as sp_count";

        $sql = "SELECT cl.*,state.*,pc.rating,$sp_count"
                . " FROM tbl_customer_locations as cl";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_postcodes as pc ON pc.postcode = cl.postcode";
        $cond = "";

        if ($id) {
            $cond .= " WHERE cl.id='" . $id . "'";
            $sql .= " $cond GROUP BY cl.id";
            $query = $this->db->query($sql);
            return $query->row_array();
        }

        if ($keyword) {
            $cond .= " WHERE (cl.site_name LIKE '%" . $keyword . "%')";
        }

        if ($cust_id) {
            $cond .= " WHERE cl.cust_id=" . $cust_id;
        }

        $sql .= " $cond GROUP BY cl.id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_customer_contact($id = FALSE, $cust_id = FALSE) {
        $sql = "SELECT c.*"
                . " FROM tbl_customer_contacts as c";
        $cond = "";
        if ($id) {
            $cond .= " WHERE c.id='" . $id . "'";
            $sql .= " $cond GROUP BY c.id";
            $query = $this->db->query($sql);
            return $query->row_array();
        }

        if ($cust_id) {
            $cond .= " WHERE c.cust_id=" . $cust_id;
        }
        $sql .= " $cond GROUP BY c.id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function delete_led_proposal_data($lead_id, $site_id) {
        //Get Proposal Id
        $proposal_id = $this->common->fetch_cell('tbl_led_proposal', 'id', array('lead_id' => $lead_id, 'site_id' => $site_id));
        //Delete Data from Related Tables
        $this->common->delete_data('tbl_led_proposal_products', array('proposal_id' => $proposal_id));
        $this->common->delete_data('tbl_led_proposal_access_equipments', array('proposal_id' => $proposal_id));
        $this->common->delete_data('tbl_proposal_finance', array('proposal_id' => $proposal_id));
        $this->common->delete_data('tbl_led_proposal', array('id' => $proposal_id));
    }

    function get_customer_by_cond($id = FALSE, $keyword = FALSE, $owner_id = FALSE) {
        $sql = "SELECT c.*,c.id as cust_id,"
                . "cl.id as location_id,cl.state_id,cl.address,cl.postcode,"
                . "lead.id as lead_id,lead.uuid,lead.category_id,lead.sub_category_id,lead.roof_type,lead.no_of_stories,lead.description,lead.description,"
                . "ud.id as assigned_userid,ud.full_name,ud.company_name,ud.company_contact_no,"
                . "ul.address as franchise_address,"
                . "state.*"
                . " FROM tbl_customers as c";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = c.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.cust_id = c.id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON ltu.lead_id = lead.id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $cond = "";
        if ($id) {
            $cond .= " WHERE c.id='" . $id . "'";
            $sql .= " $cond GROUP BY c.id";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
        if ($keyword) {
            $cond .= " WHERE (c.first_name LIKE '%" . $keyword . "%' || c.last_name LIKE '%" . $keyword . "%' || c.company_name LIKE '%" . $keyword . "%')";
        }
        if ($owner_id) {
            //$cond .= " AND c.admin_id=" . $owner_id;
        }
        $sql .= " $cond GROUP BY c.id";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    
    function get_customer_by_site_id($id = FALSE){

        $sql = "SELECT c.*,c.id as cust_id,"
                . "cl.id as location_id,cl.state_id,cl.address,cl.postcode,"
                . "state.*"
                . " FROM tbl_customers as c";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = c.id";
        $sql .= " LEFT JOIN tbl_state as state ON state.state_id = cl.state_id";
        $cond = "";
        if ($id) {
            $cond .= " WHERE cl.id='" . $id . "'";
        }
        $sql .= " $cond";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * Get User list where franchise postcode is found for customer postcode
     * @return array
     */
    function get_user_list($postcode) {
        //ul.postcode='" . $postcode . "' ||
        $if_primary_code = "SELECT count(*) as count FROM tbl_user_locations as ul WHERE find_in_set('" . $postcode . "',ul.workarea_postcodes)";
        $query = $this->db->query($if_primary_code);
        $count = $query->row()->count;
        $limit = ($count > 0) ? "LIMIT " . $count : "";

        $cond = " WHERE (find_in_set('" . $postcode . "',ul.workarea_postcodes) || find_in_set('" . $postcode . "',ul.workarea_postcodes_secondary)) <> 0 AND user_group.group_id IN ('6','7')";
        // $cond .= " AND ud.availability=1";
        $cond .= " AND admin.banned=0";
        
        $sql = "SELECT admin.*,ud.*,user_group.group_id,MAX(ltu.created_at) as last_allocated_lead_at";
        $sql .= " FROM aauth_users as admin";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_to_user ltu ON ltu.user_id = ud.user_id";
        $sql .= " $cond GROUP BY admin.id ORDER BY FIELD(ul.workarea_postcodes, '" . $postcode . "') DESC,last_allocated_lead_at $limit";
        $query1 = $this->db->query($sql);
        return $query1->result_array();
    }

    function get_unmatched_user_list($postcode) {
        $if_primary_code = "SELECT count(*) as count FROM tbl_user_locations as ul WHERE find_in_set('" . $postcode . "',ul.workarea_postcodes)";
        $query = $this->db->query($if_primary_code);
        $count = $query->row()->count;
        $limit = ($count > 0) ? "LIMIT " . $count : "";

        $cond = " WHERE (find_in_set('" . $postcode . "',ul.workarea_postcodes) = 0  and (find_in_set('" . $postcode . "',ul.workarea_postcodes_secondary)) = 0 OR ul.workarea_postcodes_secondary IS NULL)AND user_group.group_id IN ('6','7')";
        //$cond   .= " AND ud.availability=1";
        $cond   .= " AND admin.banned=0";
        $sql = "SELECT admin.*,ud.*,user_group.group_id,MAX(ltu.created_at) as last_allocated_lead_at";
        $sql .= " FROM aauth_users as admin";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = admin.id";
        $sql .= " LEFT JOIN aauth_user_to_group as user_group ON user_group.user_id = admin.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = ud.user_id";
        $sql .= " LEFT JOIN tbl_lead_to_user ltu ON ltu.user_id = ud.user_id";
        $sql .= " $cond GROUP BY admin.id ORDER BY field(admin.id," . DEFAULT_FRANCHISE . ") desc ,last_allocated_lead_at $limit";
        //echo $sql;die;
        $query1 = $this->db->query($sql);
        return $query1->result_array();
    }
    
    function get_customer_by_filter($keyword = FALSE, $id = FALSE) {
        $sql = "SELECT c.id as cust_id,CONCAT(c.first_name,' ',c.last_name) as name,"
                . "c.customer_email,c.customer_contact_no,c.company_name as customer_company_name,"
                . "cl.address,cl.postcode,"
                . "u.id as user_id,u.email as franchise_email,ud.full_name as franchise_name,"
                . "ul.address as franchise_address,ud.company_contact_no as franchise_contact_no,"
                . "lead.uuid"
                . " FROM tbl_customers as c";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = c.id";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.cust_id = c.id";
        $sql .= " LEFT JOIN aauth_users as u ON u.id = lead.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = u.id";
        $sql .= " LEFT JOIN tbl_user_locations as ul ON ul.user_id = u.id";
        

        if ($keyword) {
                $search_by_company = 'c.company_name';
                $search_by_fname = 'c.first_name';
                $search_by_lname = 'c.last_name';
                $search_by_phone = 'c.customer_contact_no';
                $search_by_email = 'c.customer_email';
                $cond =  " WHERE (MATCH($search_by_company, $search_by_fname,$search_by_lname ,$search_by_phone,$search_by_email) AGAINST('$keyword' IN BOOLEAN MODE)) OR $search_by_company LIKE '%$keyword'";
            $sql  .= " $cond ORDER BY lead.id DESC";
            
            //echo $sql;die;
            
            $query = $this->db->query($sql);
            return $query->result_array();
        }
        if ($id) {
            $cond  = " WHERE c.id=$id";
            $sql   .= " $cond GROUP BY c.id";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
    }
    
    
}

<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}
   .sw-switch-active{
        color: #d29d00;
    }
    .sw-switch-text{        text-transform: uppercase;    font-weight: bold;    font-size: 12px;    }
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1;}

    .sr-switch {
        font-size: 1rem;
        position: relative;
    }
    .sr-switch input {
        position: absolute;
        height: 1px;
        width: 1px;
        background: none;
        border: 0;
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        overflow: hidden;
        padding: 0;
    }
    .sr-switch input + label {
        position: relative;
        min-width: calc(calc(2.375rem * .8) * 2);
        border-radius: calc(2.375rem * .8);
        height: calc(2.375rem * .8);
        line-height: calc(2.375rem * .8);
        display: inline-block;
        cursor: pointer;
        outline: none;
        user-select: none;
        vertical-align: middle;
        text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
    }
    .sr-switch input + label::before,
    .sr-switch input + label::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: calc(calc(2.375rem * .8) * 2);
        bottom: 0;
        display: block;
    }
    .sr-switch input + label::before {
        right: 0;
        background-color: #dee2e6;
        border-radius: calc(2.375rem * .8);
        transition: 0.2s all;
    }
    .sr-switch input + label::after {
        top: 2px;
        left: 2px;
        width: calc(calc(2.375rem * .8) - calc(2px * 2));
        height: calc(calc(2.375rem * .8) - calc(2px * 2));
        border-radius: 50%;
        background-color: white;
        transition: 0.2s all;
    }
    .sr-switch input:checked + label::before {
        background-color: #08d;
    }
    .sr-switch input:checked + label::after {
        margin-left: calc(2.375rem * .8);
    }
    .switch input:focus + label::before {
        outline: none;
        box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
    }
    .sr-switch input:disabled + label {
        color: #868e96;
        cursor: not-allowed;
    }
    .sr-switch input:disabled + label::before {
        background-color: #e9ecef;
    }
    .switch.switch-sm {
        font-size: 0.875rem;
    }
    .sr-switch.switch-sm input + label {
        min-width: calc(calc(1.9375rem * .8) * 2);
        height: calc(1.9375rem * .8);
        line-height: calc(1.9375rem * .8);
        text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
    }
    .sr-switch.switch-sm input + label::before {
        width: calc(calc(1.9375rem * .8) * 2);
    }
    .sr-switch.switch-sm input + label::after {
        width: calc(calc(1.9375rem * .8) - calc(2px * 2));
        height: calc(calc(1.9375rem * .8) - calc(2px * 2));
    }
    .sr-switch.switch-sm input:checked + label::after {
        margin-left: calc(1.9375rem * .8);
    }
    .sr-switch + .sr-switch {
        margin-left: 1rem;
    }
    
    .signature-pad {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        font-size: 10px;
        width: 100%;
        height: 100%;
        max-height: 460px;
        border: 1px solid #e8e8e8;
        background-color: #fff;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
        border-radius: 4px;
        padding: 16px;
        cursor:pointer;
    }

    .signature-pad::before,
    .signature-pad::after {
        position: absolute;
        z-index: -1;
        content: "";
        width: 40%;
        height: 10px;
        bottom: 10px;
        background: transparent;
        box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
    }

    .signature-pad::before {
        left: 20px;
        -webkit-transform: skew(-3deg) rotate(-3deg);
        transform: skew(-3deg) rotate(-3deg);
    }

    .signature-pad::after {
        right: 20px;
        -webkit-transform: skew(3deg) rotate(3deg);
        transform: skew(3deg) rotate(3deg);
    }

    .signature-pad--body {
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 1px solid #f4f4f4;
    }

    .signature-pad--body
    canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
    }

    .signature-pad--footer {
        color: #C3C3C3;
        text-align: center;
        font-size: 1.2em;
        margin-top: 8px;
    }

    .signature-pad--actions {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-top: 8px;
    }

    .signature_image_close,.image_close,.remove_image{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: -18px;
        right: -18px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .signature_image_container,.image_container{ 
        margin: 20px 20px 20px 20px;
        overflow: visible;
        box-shadow: 5px 5px 2px #888888;
        position: relative;
        height: 200px;
    }

    #signatures{    
        height: 400px;
    }
    .sign_close,.sign_undo{display: none;}
    
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Team Leader - <?php } ?><?php echo $title; ?></h1>
    </div>
    <div class="row"  id="main_section">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" method="post"  enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Account Details</h4>
                                    <div class="form-group">
                                        <label class="control-label">Team Leader</label>
                                        <select name="user_id"  class="form-control" required>
                                            <option value="">Select Team Leader</option>
                                            <?php foreach ($team_leaders as $key => $value) { ?>
                                                <option value="<?php echo $value['user_id']; ?>" <?php if (!empty($user_id) && $user_id == $value['user_id']) { ?> selected="" <?php } ?>><?php echo  ($value['group_id'] == 6) ? $value['full_name'] . ' (Sales)' : $value['full_name'] . ' (Lead Allocation)'; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" id="email"  class="form-control" placeholder="Email" value="<?php echo (isset($email)) ? $email : ''; ?>"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                        <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <?php if (isset($show_password_field) && $show_password_field == TRUE) { ?>
                                        <div class="form-group">
                                            <label class="control-label">Password</label>
                                            <input type="text" name="users[password]" id="password"  value="<?php echo (!empty($users)) ? $users['password'] : ''; ?>" class="form-control" placeholder="Password"  required>
                                            <?php echo form_error('users[password]', '<div class="text-left text-danger">', '</div>'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Location Details</h4>
                                     <input type="hidden" name="user_locations[latitude]" id="locationLatitude"  value="<?php echo (!empty($user_locations)) ? $user_locations['latitude'] : ''; ?>" />
                                    <input type="hidden" name="user_locations[longitude]" id="locationLongitude"  value="<?php echo (!empty($user_locations)) ? $user_locations['longitude'] : ''; ?>" />
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label class="control-label">Address</label>
                                                <select class="select2 placecomplete form-control" id="locationstreetAddress"  style="height:48px !important;"></select>
                                                <input type="hidden" id="locationstreetAddressVal" name="user_locations[address]" value="<?php echo (!empty($user_locations)) ? $user_locations['address'] : '6 Turbo Road, Kings Park NSW'; ?>" />
                                                <?php echo form_error('user_locations[address]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">State</label>
                                                <select name="user_locations[state_id]" id="locationState" class="form-control" required>
                                                    <option value="">Select State</option>
                                                    <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                        <option value="<?php echo $states[$j]['state_id']; ?>" <?php if (!empty($user_locations) && $user_locations['state_id'] == $states[$j]['state_id']) { ?> selected="" <?php } ?>><?php echo $states[$j]['state_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('user_locations[state_id]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">Post Code</label>
                                                <input class="form-control" placeholder="Please Enter Post Code" type="text" name="user_locations[postcode]" value="<?php echo (!empty($user_locations)) ? $user_locations['postcode'] : ''; ?>" id="locationPostCode" required />
                                                <?php echo form_error('user_locations[postcode]', '<div class="text-left text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label class="control-label">Location Type</label>
                                                <select name="user_locations[location_type]"  class="form-control" required>
                                                        <option value="">Select Location Type</option>
                                                        <?php $location_type = unserialize(LOCATION_TYPE);
                                                        foreach ($location_type as $key => $value) { ?>
                                                            <option value="<?php echo $key; ?>" <?php if (!empty(($user_locations)) && ($user_locations)['location_type'] == $key) { ?> selected="" <?php } ?>><?php echo  $value; ?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Working Location Details <a href="#bulk_upload_modal" data-toggle="modal" class="pull-right"><u>Post codes bulk upload</u></a></h4>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group hidden">
                                                <label class="">Primary Postcodes</label>
                                                <select class="postcodes form-control" name="user_locations[workarea_postcodes][]" style="height:48px !important;" multiple="multiple" required></select>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label class="">Secondary Postcodes</label>
                                                <select id="secondary-post-codes" class="postcodes1 form-control" name="user_locations[workarea_postcodes_secondary][]" style="height:48px !important;" multiple="multiple"></select>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Representative Details (Settings)</h4>

                                    <div class="form-group">
                                        <label class="control-label">Representative Full Name</label>
                                        <input type="text" name="user_details[full_name]" id="full_name"  class="form-control" value="<?php echo (!empty($user_details)) ? $user_details['full_name'] : ''; ?>" placeholder="Team Representative Name"  >
                                        <?php echo form_error('user_details[full_name]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" name="user_details[company_contact_no]" id="mobilePhone" value="<?php echo (!empty($user_details)) ? $user_details['company_contact_no'] : ''; ?>" class="form-control" placeholder="Mobile Phone" pattern="[0-9]{6,}" title="Phone number should be number">
                                        <?php echo form_error('user_details[company_contact_no]', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>simPRO User Id</label>
                                        <input type="text" name="user_details[simpro_user_id]"  value="<?php echo (!empty($user_details)) ? $user_details['simpro_user_id'] : ''; ?>" class="form-control" placeholder="simPRO User Id" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Signature: <small><b>(Note:Once the signature is populated you can again edit it by just clicking on the close button.)</b></small></label>
                                        <div class="signature-pad" <?php echo (!empty($user_details) && $user_details['signature'] != ''&& $user_details['signature'] != NULL) ? 'style="display:none !important;"' : ''; ?>>
                                            <div class="signature-pad--body">
                                                <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_1" data-id="1" class="sign_create"></div>
                                                <input required="" id="user_details_signature" name="user_details[signature]" type="hidden" value="<?php echo (!empty($user_details)) ? $user_details['signature'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="signature-pad-image signature_image_container" <?php echo (!empty($user_details)  && $user_details['signature'] != '' && $user_details['signature'] != NULL) ? $user_details['signature'] : 'style="display:none !important;"'; ?>>
                                          <img src="<?php echo site_url('assets/uploads/user_documents/') ?><?php echo (!empty($user_details)) ? $user_details['signature'] : ''; ?>" />
                                          <a class="signature_image_close" href="javscript:void(0);"  ></a>
                                        </div>
                                    </div>

                                    <?php /**  <div class="form-group">
                                      <label>ABN</label>
                                      <input type="text" name="user_details[company_abn]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_abn'] : ''; ?>" class="form-control" placeholder="ABN" >
                                      </div>

                                      <div class="form-group">
                                      <label>BSB</label>
                                      <input type="text" name="user_details[company_bsb]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_bsb'] : ''; ?>" class="form-control" placeholder="Company BSB" >
                                      </div>

                                      <div class="form-group">
                                      <label>Account Number</label>
                                      <input type="text" name="user_details[company_account]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['company_account'] : ''; ?>" class="form-control" placeholder="Company Account Number" >
                                      </div>

                                      <div class="form-group">
                                      <label>STC Price Asumption</label>
                                      <input type="text" name="user_details[stc_price_assumption]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['stc_price_assumption'] : ''; ?>" class="form-control" placeholder="Stc Price Asumption" >
                                      </div>

                                      <div class="form-group">
                                      <label>Certificate Margin</label>
                                      <input type="text" name="user_details[certificate_margin]" id="abn" value="<?php echo (!empty($user_details)) ? $user_details['certificate_margin'] : ''; ?>" class="form-control" placeholder="Certificate Margin" >
                                      </div> * */ ?>
                                </div>
                            </div>
                            <?php  if(!empty($user_details)) { ?> 
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="form-section">Manage JOB CRM Login </h4>
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <div class="form-group control-group">
                                                    <?php if(!empty($user_details) && ($user_details['job_crm_user_id'] == NULL || $user_details['job_crm_user_id'] == '')){ ?>
                                                        <input type="text"  value="" class="form-control" placeholder="Password" id="job_crm_password" >
                                                        <a href="javascript:void(0);" data-id="<?php echo $user_details['user_id']; ?>" class="btn btn-kuga mt-2" id="create_job_crm_login_btn">Create Job CRM Login </a>
                                                    <?php }else{ ?>
                                                        <label>Job CRM User Id</label>
                                                        <input type="text"  value="<?php echo (!empty($user_details)) ? $user_details['job_crm_user_id'] : ''; ?>" class="form-control" readonly="" >
                                                    <?php } ?>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="buttwrap">
                        <input type="submit" id="addNewCompanyBtn" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                    </div> 
                </form>
            </div>
        </div>
    </div>
    <div id="signatures">
            <div class="signature-pad hidden" id="signpad_create_1">
                <div class="signature-pad--body">
                    <canvas width="664" height="200" style="touch-action: none; border: 1px solid black;" id="signature_pad_1"></canvas>
                </div>
                <div class="signature-pad--footer">
                    <div class="signature-pad--actions">
                        <div>
                            <button type="button" class="btn button sign_clear" data-action="clear" data-id="1">Clear</button>
                            <button type="button" class="btn button sign_undo" data-action="undo" data-id="1">Undo</button>
                        </div>
                        <div>
                            <button type="button" class="btn button sign_close" data-id="1" id="signpad_close_1">Close</button>
                            <button type="button" class="btn button sign_save" data-action="save-png" data-id="1">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="bulk_upload_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Bulk Change Post Codes</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Copy post code from excel file or in CSV format in the below text box and click on change button.</p>
                        <p>After clicking on change button, user must save the changes made by this model by clicking on save button.</p>
                        <div class="form-group">
                            <label>Secondary Post Codes</label>
                            <textarea class="form-control" id="post-code-text-box"></textarea>
                        </div>
                        <div class="form-group">
                            <span class="sr-switch">
                                <label for="postcode-handler" class="sw-switch-text sw-switch-active sw-switch-text-false">Append</label>                                
                                <input type="checkbox" class="sr-switch" id="postcode-handler" onchange="changeAvailability(this);">
                                <label for="postcode-handler" class="sw-switch-text sw-switch-text-true">Overwrite</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="change_post_codes">Change</button>
                </div>
            </form>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing"></script>

<script>
    $(document).ready(function () {
        var workarea_postcodes = JSON.parse('<?php echo (isset($workarea_postcodes) && $workarea_postcodes != '') ? json_encode($workarea_postcodes) : '{}'; ?>');
        var workarea_postcodes_secondary = JSON.parse('<?php echo (isset($workarea_postcodes_secondary) && $workarea_postcodes_secondary != '') ? json_encode($workarea_postcodes_secondary) : '{}'; ?>');
        var initialPropertyOptions = [];
        var initialPropertyOptions1 = [];

        if (workarea_postcodes.length > 0) {
            for (var i = 0; i < workarea_postcodes.length; i++) {
                var obj = {};
                obj.id = workarea_postcodes[i];
                obj.text = workarea_postcodes[i];
                obj.selected = true;
                initialPropertyOptions.push(obj);
            }
        }else{
            var obj = {};
            obj.id = 2148;
            obj.text = 2148;
            obj.selected = true;
            initialPropertyOptions.push(obj);
        }

        $(".postcodes").select2({
            data: initialPropertyOptions,
            ajax: {
                url: base_url + "admin/fetch_postcodes",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.results,
                    };
                },
                cache: true
            },
            placeholder: 'Search for postcodes',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatResults,
        });

        if (workarea_postcodes_secondary.length > 0) {
            for (var i = 0; i < workarea_postcodes_secondary.length; i++) {
                var obj = {};
                obj.id = workarea_postcodes_secondary[i];
                obj.text = workarea_postcodes_secondary[i];
                obj.selected = true;
                initialPropertyOptions1.push(obj);
            }
        }

        $(".postcodes1").select2({
            data: initialPropertyOptions1,
            ajax: {
                url: base_url + "admin/fetch_postcodes",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.results,
                    };
                },
                cache: true
            },
            placeholder: 'Search for postcodes',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatResults,
        });


        function formatResults(data) {
            if (data.loading) {
                return data.text;
            }
            return data.text;
        }

        $('.placecomplete').placecomplete({
            placeServiceResult: function (data, status) {
                if (status != 'INVALID_REQUEST') {

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                    lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('#locationstreetAddressVal') != undefined) {
                        $('#locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            if ($('#locationState') != undefined) {
                                var select = document.getElementById("locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                    }
                                }
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#locationPostCode') != undefined) {
                                $('#locationPostCode').val(address_components[i].long_name);
                            }
                        }
                    }
                }else{
                     $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'fr'
        });

    });

    (function ($) {

        if (typeof $.fn.select2 == 'undefined') {
            //alert("ERROR: Placecomplete need Select2 plugin.");
        }
        setTimeout(function () {
            var location_address = "<?php echo (!empty($user_locations)) ? $user_locations['address'] : ''; ?>";
            $('#select2-locationstreetAddress-container').html(location_address);
        }, 2000);


        //Google services
        var ac = null; //Autocomplete
        var ps = null; //Place

        //Google config
        // https://developers.google.com/maps/documentation/javascript/reference#AutocompletionRequest
        var googleAutocompleteOptions = {
            types: ['establishment', 'geocode'],
            componentRestrictions: {country: 'au'},
        };

        //Google init
        window.initGoogleMapsAPI = function () {
            ac = new google.maps.places.AutocompleteService();
            ps = new google.maps.places.PlacesService($('<div/>')[0]); //Google need a mandatory element to pass html result , we do not need this.

        }

        //Google Loading
        if (window.google && google.maps && google.maps.places) {
            window.initGoogleMapsAPI();
        } else {
            $.ajax({
                url: "https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&libraries=places,geometry,drawing",
                dataType: "script",
                cache: true
            });
        }

        //Google placeservice result map

        var placeServiceResult = function (data, status) {
            var CIVIC = 0, STREET = 1, CITY = 2, SECTEUR = 3, STATE = 4, COUNTRY = 5, ZIPCODE = 6;
            //todo If the result does not have 7 element data map is not the same
            //maybe we will need that html element google put mandatory
            var adrc = data.address_components;
            if (adrc.length != 7)
                return;

            var address = adrc[CIVIC].long_name + ',' + adrc[STREET].long_name + adrc[CIVIC].long_name + ',' + adrc[STREET].long_name

            $('.address input.address').val(adrc[CIVIC].long_name + ' ' + adrc[STREET].long_name);
            $('.address input.city').val(adrc[CITY].long_name);
            $('.address input.state').val(adrc[STATE].long_name);
            $('.address input.country').val(adrc[COUNTRY].long_name);
            $('.address input.zipcode').val(adrc[ZIPCODE].long_name);
        }

        //Select2 default options
        var select2DefaultOptions = {
            closeOnSelect: true,
            debug: false,
            dropdownAutoWidth: false,
            //escapeMarkup: Utils.escapeMarkup,
            language: 'en',
            minimumInputLength: 2,
            maximumInputLength: 0,
            maximumSelectionLength: 0,
            minimumResultsForSearch: 0,
            selectOnClose: false,
            selectOnBlur: true,
            theme: 'default',
            width: '100%',
            placeholder: {
                id: '-1', // the value of the option
                text: 'Search for address'
            },
            ajax: {
                delay: 100
            },
        };

        //jQuery Plugin
        var pluginDefault = {
            placeServiceResult: placeServiceResult
        }
        $.fn.placecomplete = function (options) {
            this.each(function () {
                //Variable by instance
                var $s2 = $(this);
                //Init select2 for $this
                $s2.select2($.extend(true, {
                    ajax: {
                        transport: function (params, success, failure) {

                            // is caching enabled?
                            //TODO(sébastien) ajouter le cache pour google autocomplete
                            if ($s2.data('ajax--cache')) {

                            } else {
                                ac.getPlacePredictions($.extend(googleAutocompleteOptions, params.data), success);
                            }
                        },
                        data: function (params) {
                            return {input: params.term};
                        },
                        processResults: function (data, status) {
                            var response = {results: []}
                            if (data != undefined && data != null) {
                                $.each(data, function (index, item) {
                                    item.id = item.description;
                                    item.text = item.description;
                                    response.results.push(item)
                                });
                            } else {
                                var item = {};
                                item.id = status.term;
                                item.place_id = status.term;
                                item.text = status.term;
                                response.results.push(item)
                            }
                            return response;
                        },
                    }
                }, select2DefaultOptions, options || {}));

                options = $.extend(true, pluginDefault, options);
                $s2.on('select2:select', function (evt) {
                    ps.getDetails({placeId: evt.params.data.place_id}, options.placeServiceResult);
                });
            });
            return this;
        };
        
        //$('#locationState').val(2).trigger('change');
        //$('#locationPostCode').val(2148).trigger('change');
    })(jQuery);


    function changeAvailability(ele) {
        if (ele.checked) {
            $('.sw-switch-text-false').removeClass('sw-switch-active');
            $('.sw-switch-text-true').addClass('sw-switch-active');
        } else {
            $('.sw-switch-text-true').removeClass('sw-switch-active');
            $('.sw-switch-text-false').addClass('sw-switch-active');
        }
    }
    $(document).ready(function () {

         var workarea_postcodes_secondary = JSON.parse('<?php echo (isset($workarea_postcodes_secondary) && $workarea_postcodes_secondary != '') ? json_encode($workarea_postcodes_secondary) : '{}'; ?>');
        var initialPropertyOptions1 = [];
        for (var i = 0; i < workarea_postcodes_secondary.length; i++) {
            var obj = {};
            obj.id = workarea_postcodes_secondary[i];
            obj.text = workarea_postcodes_secondary[i];
            obj.selected = true;
            initialPropertyOptions1.push(obj);
        }

        $(document).on('blur', '#post-code-text-box', function () {
            var postCodeValue = $('#post-code-text-box').val();
            if (postCodeValue.indexOf(',') == -1) {
                postCodeValue = postCodeValue.replace(/(\r\n|\n|\r)/gm, ",");
                var postCodesArray = postCodeValue.split(',');
                var temp = '';
                for (var i = 0; i < postCodesArray.length; i++) {
                    var pc = postCodesArray[i].trim()
                    if (pc != '') {
                        if (temp == '') {
                            temp = pc;
                        } else {
                            temp += ',' + pc;
                        }
                    }
                }
                $('#post-code-text-box').val(temp);
            }
        });
        
        $(document).on('click', '#change_post_codes', function () {
            
            var postCodeValue = $('#post-code-text-box').val();
            var postCodesArray = postCodeValue.split(',');
            var prop = document.getElementById('postcode-handler').checked;

            if (prop) {
                // overwrite
                $('#secondary-post-codes').select2('destroy');
                $('#secondary-post-codes').empty();
                var temp = [];
                for (var i = 0; i < postCodesArray.length; i++) {
                    temp.push({"id": postCodesArray[i], "text": postCodesArray[i], "selected": true});
                }


            } else {
                // append
                $('#secondary-post-codes').select2('destroy');
                $('#secondary-post-codes').empty();
                for (var i = 0; i < postCodesArray.length; i++) {
                    initialPropertyOptions1.push({"id": postCodesArray[i], "text": postCodesArray[i], "selected": true});
                }
                var temp = initialPropertyOptions1;
            }
            $("#secondary-post-codes").select2({
                data: temp,
                ajax: {
                    url: base_url + "admin/franchise/fetch_postcodes",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.results
                        };
                    },
                    cache: true
                },
                placeholder: 'Search for postcodes',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: function (data) {
                    if (data.loading) {
                        return data.text;
                    }
                    return data.text;
                },
                tokenSeparators: [",", " "],
                createTag: function (term, data) {
                    var value = term.term;
                    return {
                        id: value,
                        text: value
                    };
                    return null;
                }
            });
        });
    });
</script>
<script src="<?php echo site_url(); ?>assets/js/signpad.min.js"></script>

<script>
    var signature_pad = [];
    var signature_pad_ids = ['user_details_signature'];

    function remove_signature_image() {
        $('.signature_image_close').click(function () {
            $(this).parent().prev('div').show();
            $(this).parent().prev('div').children('input').val('');
            $(this).parent().attr('style', 'display:none !important;');
            $(this).parent().children('img').attr('src', '');
        });
    }


    function create_signature_pad() {
        var canvas1 = document.getElementById("signature_pad_1");

        var clearButton = $(".sign_clear");
        var undoButton = $(".sign_undo");
        var savePNGButton = $(".sign_save");

        var signaturePad1 = new SignaturePad(canvas1, {
            backgroundColor: 'rgb(255, 255, 255)',
            penColor: '#0300FD'
        });

        var arr = [signaturePad1];
        signature_pad = arr;
        var arr1 = signature_pad_ids;
        
        function resizeCanvas() {
            var ratio = Math.max(window.devicePixelRatio || 1, 1);
            canvas1.width = canvas1.offsetWidth * ratio;
            canvas1.height = canvas1.offsetHeight * ratio;
            canvas1.getContext("2d").scale(ratio, ratio);
            signaturePad1.clear();
        }

        window.addEventListener("resize", function (event) {
            resizeCanvas();
        });

        window.addEventListener("orientationchange", function (event) {
            resizeCanvas();
        });

        function download(dataURL, filename) {
            if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
                window.open(dataURL);
            } else {
                var blob = dataURLToBlob(dataURL);
                var url = window.URL.createObjectURL(blob);

                var a = document.createElement("a");
                a.style = "display: none";
                a.href = url;
                a.download = filename;

                document.body.appendChild(a);
                a.click();

                window.URL.revokeObjectURL(url);
            }
        }

        function dataURLToBlob(dataURL) {
            // Code taken from https://github.com/ebidel/filer.js
            var parts = dataURL.split(';base64,');
            var contentType = parts[0].split(":")[1];
            var raw = window.atob(parts[1]);
            var rawLength = raw.length;
            var uInt8Array = new Uint8Array(rawLength);

            for (var i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }

            return new Blob([uInt8Array], {type: contentType});
        }

        clearButton.on("click", function (event) {
            var id = this.getAttribute('data-id');
            arr[(id - 1)].clear();
        });

        undoButton.on("click", function (event) {
            var id = this.getAttribute('data-id');
            var data = arr[(id - 1)].toData();
            if (data) {
                data.pop(); // remove the last dot or line
                arr[(id - 1)].fromData(data);
            }
        });

        savePNGButton.on("click", function (event) {
            var id = this.getAttribute('data-id');
            if (arr[(id - 1)].isEmpty()) {
                alert("Please provide a signature first.");
            } else {
                $('#signpad_close_' + id).trigger('click');
                var dataURL = arr[(id - 1)].toDataURL();
                var ele = $('#' + arr1[(id - 1)]);
                show_signature_image(ele, dataURL, true);
            }
        });
    }

    function show_signature_image(ele, image, is_data_url) {
        var self = this;
        if (image != '') {
            ele.val(image);
            if (is_data_url) {
                ele.prev().html('<img style="height:95px;" src="' + image + '" />');
            } else {
                ele.prev().html('<img style="height:95px;" src="' + base_url + 'assets/uploads/installer_form_files/' + image + '" />');
            }
        }
    }

    function resizeCanvas() {
        var canvas1 = document.getElementById("signature_pad_1");
        var signaturePad1 = signature_pad[0];
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas1.width = canvas1.offsetWidth * ratio;
        canvas1.height = canvas1.offsetHeight * ratio;
        canvas1.getContext("2d").scale(ratio, ratio);
        signaturePad1.clear();
    };

    $(document).ready(function () {

        create_signature_pad();
        remove_signature_image();

        $(document).on('click','.sign_create',function () {
            var id = $(this).attr('data-id');
            $('#main_section').hide();
            $('#signatures').show();
            for (var i = 0; i < 4; i++) {
                $('#signpad_create_' + i).addClass('hidden');
            }
            $('#signpad_create_' + id).removeClass('hidden');
            window.scrollTo({top: 0, behavior: 'smooth'});
            resizeCanvas();
        });

        $(document).on('click','.sign_close',function () {
            var id = $(this).attr('data-id');
            $('#main_section').show();
            $('#signatures').hide();
            for (var i = 0; i < 3; i++) {
                $('#signpad_create_' + i).addClass('hidden');
            }
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#sign_create_" + id).offset().top
            }, 0);
        });
        
        $(document).on('click','#create_job_crm_login_btn',function () {
            var form_data = {};
            form_data.user_id = $(this).attr('data-id');
            form_data.job_crm_password = $('#job_crm_password').val();
            if(form_data.job_crm_password == ''){
                toastr['error']('Please Enter Password.');
                return false;
            }else if(form_data.job_crm_password.length < 5){
                toastr['error']('Password must be more than 5 characters long.');
                return false;
            }
            create_job_crm_login(form_data);
        });

        function create_job_crm_login(data){
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/create_job_crm_login',
                datatype: 'json',
                data:data,
                beforeSend:function(){
                    toastr.remove();
                },
                success: function (response) {
                    if(response.success){
                        toastr['success'](response.status);
                        window.location.reload();
                    }else{
                        toastr['error'](response.status);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.remove();
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        
    });
</script>
<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/datetimepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<style>

    body{
        overflow-y: scroll;
    }


    .page-wrapper {background-color: #ECEFF1;min-height: 100%;}
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
    .sr-inactive_user{color:red;}

    .ui {
        height: 500px;
        display: grid;
        grid-template-rows: 40px 50px 1fr;
        color: #eee;
    }

    .lists {
        display: flex;
        overflow-x: auto;
        height: inherit;
        position:relative;
    }

    .lists::-webkit-scrollbar {
        width: 5px;
        height:5px;
    }


    .lists::-webkit-scrollbar-thumb {
        background: #666;
        border-radius: 20px;
    }

    .lists::-webkit-scrollbar-track {
        background: #ddd;
        border-radius: 20px;
    }

    .lists > * {
        flex: 0 0 auto;
        margin-left: 10px;
    }

    .lists::after {
        content: '';
        flex: 0 0 10px;
    }

    .list {
        width: 300px;
        height: calc(100% - 10px - 17px);

    }



    .list > * {
        background-color: #e2e4e6;
        color: #333;
        padding: 0 10px;
    }

    .list header {
        line-height: 36px;
        font-size: 16px;
        font-weight: bold;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        text-align: left;
    }

    .list header.fixed {
        width:inherit;
        position:fixed; 
    }

    .list footer {
        line-height: 36px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        color: #888;
    }

    .stage_list {
        list-style: none;
        margin: 0;
        min-height: calc(100% - 36px - 36px);
        overflow-y: scroll;
        height: inherit;
        /**height: calc(100% - 36px - 36px);**/
    }

    .stage_list::-webkit-scrollbar {
        width: 1px;
        height:1px;
    }


    .stage_list::-webkit-scrollbar-thumb {
        background: transparent;
        border-radius: 20px;
    }

    .stage_list::-webkit-scrollbar-track {
        background: transparent;
        border-radius: 20px;
    }

    .stage_list__item {
        background-color: #fff;
        padding: 10px;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        cursor:pointer;
        position:relative;
    }

    .stage_list__item:not(:last-child) {
        margin-bottom: 10px;
    }

    .stage_list__item img {
        display: block;
        width: calc(100% + 2 * 10px);
        margin: -10px 0 10px -10px;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }

    .bg-disabled{
        background:#dddddd !important;
    }

    .sr-deal_actions{
        opacity: 0;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: -67px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }

    .stage_list__item--selected {
        border: solid 1px green;
    }

    .stage_list__item--selected::before,
    .stage_list__item--selected::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        border-color: transparent;
        border-style: solid;
    }

    .stage_list__item--selected::after {
        content: '\2713';
        font-size: 13px;
        line-height: 13px;
        font-weight: bold;
        color: white;
    }

    .stage_list__item--selected::before {
        border-radius: 0;
        border-width: 12px;
        border-left-color: green;
        border-top-color: green;
    }

    #job_detail_container{
        margin-top:50px;
    }
</style>

<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Proposal Job Scheduling</h1>
        <div class="content-header-right col-md-6 col-12"></div>
    </div>
    <section id="job_container" >
        <div class="ui">
            <div class="lists" id="stages">

            </div>
        </div>
    </section>




    <section  id="job_detail_container" class="card hidden">
        <div class="card-content">
            <h5 class="card-header">Job Details</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6" id="job_detail_body">

                    </div>
                    <div class="col-md-6">
                        <div class="form-block d-block clearfix">
                            <div class="btn-block">
                                <a href="javascript:void(0);" id="add_activity" class="btn mrg-r10">Add Activity</a>
                                <a href="javascript:void(0);" id="schedule_activity" class="btn">Schedule Activity </a> 
                            </div>
                            <div class="table-default"  style="margin-top:10px;">
                                <table width="100%" border="0" id="all_activity_table">
                                    <thead>
                                        <tr>
                                            <td>User</td>
                                            <td>Type</td>
                                            <td>Note</td>
                                            <td>Created At</td>
                                            <td>Scheduled At</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody id="all_activity_table_body">

                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal" id="job_schedule_modal" role="dialog"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Schedule</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post"  id="job_schedule_add_form" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Installer </label>
                                        <select name="job[user_id]"  class="form-control" id="job_user_id"  required>
                                            <option value="">Select Installer </option>
                                            <?php foreach ($installers as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value['user_id']; ?>" ><?php echo $value['full_name']; ?></option>   
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="form-group " id="scheduled_date">
                                        <label style="display:block !important;">Schedule Date </label>
                                        <div class=""  style="margin-bottom: 1.5rem;">
                                            <input class="form-control" name="job[scheduled_at]" id="scheduled_at" value="<?php echo date('Y/m/d H:i'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="form-group " id="scheduled_date">
                                        <label style="display:block !important;">Comment </label>
                                        <div class=""  style="margin-bottom: 1.5rem;">
                                            <textarea class="form-control" name="job[comment]" id="comment"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="buttwrap">
                                <input type="button" id="addJobSchedule" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="activity_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add /Schedule Activity</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                            <input type="hidden" name="activity[id]" id="activity_id" class="form-control" >
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Activity Type </label>
                                        <select name="activity[activity_type]"  class="form-control" id="activity_type"  required>
                                            <option value="">Select Actitvity Type </option>
                                            <?php
                                            $activity_type = unserialize(activity_type);
                                            foreach ($activity_type as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>   
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" id="activity_file"  class="custom-file-input" >
                                        <input type="hidden" name="activity[attachment]" id="activity_file_value" class="form-control" >
                                        <span class="custom-file-label" for="activity_file">Choose attachment file</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Note </label>
                                        <textarea name="activity[note]" id="activity_note" class="form-control" placeholder="Note"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" >
                                    <input id="is_scheduled" name="activity[is_scheduled]" type="hidden" value="0">
                                    <div class="form-group " id="scheduled_date">
                                        <label style="display:block !important;">Schedule Date </label>
                                        <div class="input-group date" data-provide="datepicker" style="margin-bottom: 1.5rem;">
                                            <input class="datepicker" name="activity[scheduled_at]"  value="<?php echo date('d-m-Y'); ?>">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="buttwrap">
                                <input type="button" id="addActivity" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src='<?php echo site_url(); ?>assets/js/job.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>assets/js/datetimepicker.js?v=<?php echo version; ?>"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
    var context = {};
    var job_tool = new job(context);
    job_tool.fetch_stage_list();
</script>


var deal_manager = function () {
    var self = this;
    this.filters = '';
    this.loading = false;
    this.page = 1;
    this.per_page = 100;
    this.pagination = false;
    this.keyword = '';

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#lead_list_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        document.getElementById('start_date').value = start.format('YYYY-MM-DD');
        document.getElementById('end_date').value = end.format('YYYY-MM-DD');
        self.pagination = false;
        $('#pagination').pagination('destroy');
        var data = $('#lead_list_filters').serialize();
        self.fetch_lead_stage_data(data);
    }

    var currentDate = moment();
    var weekStart = currentDate.clone().startOf('isoWeek');
    var weekEnd = currentDate.clone().endOf('isoWeek');
    
    var thisWeek = [];
    var lastWeek = [];
    thisWeek.push(moment(weekStart).add(0, 'days'));
    thisWeek.push(moment(weekStart).add(6, 'days'));
    lastWeek.push(moment(weekStart).add(-7, 'days'));
    lastWeek.push(moment(weekStart).add(-1, 'days'));

    $('#lead_list_range').daterangepicker({
        numberOfMonths: 6,
        startDate: start,
        endDate: end,
        ranges: {
            'All': [moment('2016-01-01'), moment()],
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'This Week': thisWeek,
            'Last Week': lastWeek,
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    
    cb(start,end);
    // var data = $('#lead_list_filters').serialize();
    // self.fetch_lead_stage_data(data);

    $('#lead_list_filters :input').change(function () {
        self.pagination = false;
        $('#pagination').pagination('destroy');
        var data = $('#lead_list_filters').serialize();
        self.fetch_lead_stage_data(data);
    });

    $('#export_leads').click(function () {
        var data = $('#lead_list_filters').serialize();
        var url = base_url + 'admin/lead/export_csv?action=export_lead_csv&' + data;
        window.open(url, '_blank');
    });
    
    $('.reset_filter').click(function () {
        var filter_type = $(this).attr('data-id');
        $("." + filter_type).prop("checked", false);
        $("." + filter_type).trigger("change");
    });
        

}


deal_manager.prototype.create_deal_stage_table_element = function (ele, value) {
    var table_ele = document.createElement(ele);
    if (value) {
        table_ele.innerHTML = value;
    }
    return table_ele;
};

deal_manager.prototype.create_deal_stage_table = function (data) {
    var self = this;
    self.loading = false;
    if (data.lead_data.length == 0) {
        self.loading = true;
    }
    var table_body = document.getElementById('lead_list_body');
    if (data.lead_data.length > 0) {
        for (var i = 0; i < data.lead_data.length; i++) {
            var tr = self.create_deal_stage_table_element('tr');
            
            var date_hack = data.lead_data[i].created_at.split('/');
            var date_hack_String = '<span class="hidden">' + date_hack[2] + date_hack[1] + date_hack[0] + '</span>';
        
            var td_state = self.create_deal_stage_table_element('td',data.lead_data[i].customer_state_postal);
            var lt = '';
            if(parseInt(data.lead_data[i].lead_location_type) == 1){
                lt = 'Residential';
            }else if(parseInt(data.lead_data[i].lead_location_type) == 2){
                lt = 'Commercial';
            }else if(parseInt(data.lead_data[i].lead_location_type) == 3){
                lt = 'Residential - Unbranded';
            }else if(parseInt(data.lead_data[i].lead_location_type) == 4){
                lt = 'Commercial- Unbranded';
            }
            
            var td_lead_location_type = self.create_deal_stage_table_element('td',lt);
            var td_deal_stage = self.create_deal_stage_table_element('td', (data.lead_data[i].deal_stage_name) ? data.lead_data[i].deal_stage_name : '-');
            var td_deal_kw = self.create_deal_stage_table_element('td', (data.lead_data[i].total_solar) ? data.lead_data[i].total_solar : '-');
            var td_created_at = self.create_deal_stage_table_element('td', date_hack_String + data.lead_data[i].created_at);
            
            var fname = (data.lead_data[i].first_name != null) ? data.lead_data[i].first_name : '';
            var lname = (data.lead_data[i].last_name != null) ? data.lead_data[i].last_name : '';
            var td_fname = self.create_deal_stage_table_element('td', fname);
            var td_lname = self.create_deal_stage_table_element('td', lname);
            
            var td_phone = self.create_deal_stage_table_element('td', data.lead_data[i].customer_contact_no);
            var td_email = self.create_deal_stage_table_element('td', data.lead_data[i].customer_email);
            
            var td6 = self.create_deal_stage_table_element('td', data.lead_data[i].lead_status);
            td6.setAttribute('class', 'capitalize');
            
            var customer_name = '<a target="__blank" href="' + base_url + 'admin/lead/add?deal_ref=' + data.lead_data[i].uuid + '">' + data.lead_data[i].company_name + '</a>' + '<span class="hidden">' + data.lead_data[i].customer_name + '</span>';
            var td_company_name = self.create_deal_stage_table_element('td', customer_name);
            
            var td_address = self.create_deal_stage_table_element('td', data.lead_data[i].customer_address);
            var td_bill = self.create_deal_stage_table_element('td', data.lead_data[i].current_monthly_electricity_bill);
            
            var td_assigned_to = self.create_deal_stage_table_element('td', data.lead_data[i].created_by);
            
            var td_lead_soruce = self.create_deal_stage_table_element('td', data.lead_data[i].lead_source);
            
            tr.appendChild(td_state);
            tr.appendChild(td_lead_soruce);
            tr.appendChild(td_lead_location_type);
            tr.appendChild(td_assigned_to);
            tr.appendChild(td_deal_stage);
            tr.appendChild(td_deal_kw);
            tr.appendChild(td_created_at);
            tr.appendChild(td_fname);
            tr.appendChild(td_lname);
            tr.appendChild(td_phone);
            tr.appendChild(td_email);
            tr.appendChild(td_company_name);
            tr.appendChild(td_address);
            tr.appendChild(td_bill);
            table_body.appendChild(tr);
        }
    }
};


deal_manager.prototype.fetch_lead_stage_data = function (data) {
    var self = this;
    var stage_id = self.current_stage_id;
    $('#table_loader').html(createLoader('Please wait while data is being fetched.......'));
    $.ajax({
        url: base_url + 'admin/lead/fetch_lead_stage_data',
        type: 'get',
        data: data + '&limit=' + self.per_page + '&view=report&report_type=facebook',
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                if ($('#lead_list')) {
                    $('#lead_list').DataTable().clear();
                    $('#lead_list').DataTable().destroy();
                }
                $('#table_loader').html('');
                self.create_deal_stage_table(response);

                var table = $('#lead_list').DataTable({
                    orderCellsTop: false,
                    fixedHeader: true,
                    columnDefs: [
                        {type: 'date-uk', targets: 0}
                    ],
                    "order": [
                        [6, "desc"]
                    ],
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    paging: false,
                });

                if (self.pagination == false) {
                    $(function () {
                        $('#pagination').pagination({
                            items: response.meta.total,
                            itemsOnPage: self.per_page,
                            cssStyle: "light-theme",
                            onPageClick: function (pageNumber) {
                                var data = $('#lead_list_filters').serialize();
                                self.page = pageNumber;
                                data += '&page=' + self.page;
                                self.fetch_lead_stage_data(data);
                            }
                        });
                    });
                    self.pagination = true;
                }


                //Overriding Search
                //var search_input = '<label>Search:<input type="search" id="search_leads" class="" placeholder="" aria-controls="lead_list"></label>';

                //document.getElementById("lead_list_filter").innerHTML = search_input;
                var table = $('#lead_list').dataTable().api();

                if (self.keyword != '') {
                    document.getElementById("search_leads").value = self.keyword;
                }

                $('#search_leads').change(function () {
                    var search_val = $(this).val();
                    if (search_val == '') {
                        self.pagination = false;
                    } else {
                        $('#pagination').pagination('destroy');
                    }
                    self.keyword = search_val;
                    var data = 'keyword=' + encodeURIComponent(search_val);
                    data += '&page=1';
                    self.fetch_lead_stage_data(data);
                });


                /** $(window).on("scroll", function () {
                 if(!self.loading){
                 var scrollHeight = $(document).height();
                 var scrollPosition = $(window).height() + $(window).scrollTop();
                 if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                 self.loading = true;
                 var data = $('#lead_list_filters').serialize();
                 self.page = self.page + 1;
                 data += '&page='+self.page+'&limit='+self.limit;
                 self.fetch_lead_stage_data(data);
                 }
                 }
                 }); */
            } else {
                if (response.hasOwnProperty('authenticated')) {
                    toastr['error'](response.status);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    toastr["error"]('Something went wrong please try again.');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

deal_manager.prototype.export_lead_data = function (data) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/lead/export_csv',
        type: 'get',
        data: data,
        dataType: 'json',
        success: function (response) {


        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
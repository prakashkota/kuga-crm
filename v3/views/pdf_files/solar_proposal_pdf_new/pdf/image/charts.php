<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }

        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }

        body {
            font-family: 'CenturyGothic';
            margin: 0;
            padding: 0;
        }

        .mt54 {
            margin-top: -54px;
        }

        .page-sp-back-bg {
            background-color: #fff;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page-sp .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page-sp .top hr {
            width: 65%;
            height: 3px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
            border: none;
        }

        .page-sp .text_color {
            color: #D01E2A;
        }

        .page-sp .title {
            display: inline-flex;
            margin-top: -50px;
            margin-left: 70px;
            width: 100%;
        }

        .page-sp .title h1 {
            padding-left: 14px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
        }

        .page-sp .solorproduction {
            width: 50%;
        }

        .page-sp .solorproduction h2 {
            padding: 5px;
            margin-top: 88px;
            font-size: 16px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            background: #D01E2A;
            letter-spacing: 2px;
            display: flex;
            align-items: center;
        }

        .page-sp .solorproductionavg {
            width: 60%;
            margin-right: 200px;
        }

        .page-sp .solorproductionavg h2 {
            padding: 12.5px;
            margin-top: 88px;
            font-size: 16px;
            width: 100%;
            color: #373536;
            border: 2px solid #d01e2a;
            background: #fff;
            margin-left: 30px;
        }

        .page-sp table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 80%;
            margin-left: 46px;
        }

        .page-sp tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page-sp td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page-sp .sectio-image img {
            margin-top: 25px;
            margin-left: 85px;
        }

        .page-sp .fimagee img {
            margin-left: 730px;
            margin-top: 125px;
        }

        .page-sp .sub_titlee h6 {
            font-size: 17px;
            margin-left: 48px;
        }

        .page-sp .firstBoxs {
            display: inline-flex;
            width: 85%;
            margin: 40px 70px 0;
        }

        .page-sp .sub_titlee {
            background-color: #dddddd;
            margin-right: 30px;
        }

        .page-sp .sub_titlee1 {
            margin-right: 30px;
            border: 2px solid #D01E2A;
        }

        .page-sp .sub_titlee1 p {
            margin: 9px 0;
            /* font-size: 18px; */
            color: #D01E2A;
            font-weight: bold;
            text-align: center;
        }

        .page-sp .sub_titlee2 {
            margin-right: 30px;
        }

        .page-sp .sub_titlee2 .sub_titlee2-inner {
            border: 2px solid #D01E2A;
            padding: 0 16px;
        }

        .page-sp .sub_titlee2 p {
            margin: 9px 0;
            /* font-size: 18px; */
            color: #D01E2A;
            font-weight: bold;
            text-align: center;
        }

        .page-sp .sub_titlee p {
            /* color: #D01E2A; */
            font-weight: bold;
            text-align: center;
            margin: 9px 0;
        }

        .page-sp .box {
            background-color: #dddddd;
            padding: 9px 0;
            font-weight: bold;
            text-align: center;
            width: 236px;
            max-width: 236px;
        }

        .page-sp .box1 {
            /* border: 2px solid #D01E2A; */
            padding: 9px 0;
            font-weight: bold;
            text-align: center;
            width: 232px;
            max-width: 232px;
        }

        .page-sp .box2 {
            border: 2px solid #F2B1B5;
            width: 234px;
            height: 60px;
        }

        .page-sp .tittle {
            display: inline-flex;
            margin-top: -50px;
            margin-left: 20px;
        }

        .page-sp .solorproductionn {
            margin-right: 30px;
            margin-bottom: 30px;
            margin-top: 30px;
        }

        .page-sp .solorproductionn h2 {
            font-size: 16px;
            color: #241E20;
            text-align: center;
        }

        .page-sp .footer {
            display: inline-flex;
        }

        .page-sp .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page-sp .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page-sp .f20 p {
            font-size: 100px;
            margin: 0;
            margin-left: 19px;
            color: #D01E2A;
            margin-right: 42px;
        }

        .page-sp .f20 {
            border-right: 2px solid #F2B1B5;
            display: flex;
            align-items: center;
        }

        .page-sp .f20image {
            border-right: 2px solid #F2B1B5;
        }

        .page-sp .f20image img {
            margin-left: 42px;
            margin-right: 42px;
        }

        .page-sp .f20imagee img {
            margin-left: 42px;
            margin-right: 42px;
        }

        .page-sp .f20imagee p {
            text-align: center;
            font-size: 50px;
            margin: 0;
            font-weight: bold;
        }

        .w910 {
            width: 910px;
        }

        body {
            margin: 0px !important;
            overflow-y: hidden;
        }

        .overlay-roadblock {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 10;
            background-color: rgba(0, 0, 0, 0.5);
            display: none;
        }

        .overlay-roadblock__content {
            width: auto;
            height: 80px;
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -100px;
            margin-left: -150px;
            margin-right: 25px;
            background-color: #fff;
            border-radius: 5px;
            text-align: center;
            z-index: 11;
            word-break: break-all;
            padding: 20px;
        }

        .loader {
            border: 5px solid #f3f3f3;
            -webkit-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
            border-top: 5px solid #555;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            margin: 0 auto;
            text-align: center;
        }

        @keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
</head>
<form method="POST" id="chartForm">
    <input type="hidden" id="pId" value="<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg1" name="dataImg1" value="" />
    <input type="hidden" name="filename1" value="page8_<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg2" name="dataImg2" value="" />
    <input type="hidden" name="filename2" value="page9_<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg3" name="dataImg3" value="" />
    <input type="hidden" name="filename3" value="page10_<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg4" name="dataImg4" value="" />
    <input type="hidden" name="filename4" value="page11_<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg5" name="dataImg5" value="" />
    <input type="hidden" name="filename5" value="page12_<?php echo $proposal_data['id']; ?>" />
    <input type="hidden" id="dataImg6" name="dataImg6" value="" />
    <input type="hidden" name="filename6" value="page12_1_<?php echo $proposal_data['id']; ?>" />
</form>
<div id="page1" class="w910">
    <div class="page-sp-back-bg back-bg page-sp">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA </h2>
            <hr>
        </div>
        <div class="title">
            <h1>SYSTEM <span class="text_color"> PERFORMANCE</span></h1>
        </div>
        <div class="title">
            <div class="solorproduction">
                <h2>
                    <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g id="Icon">
                            <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                            <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                            <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                        </g>
                    </svg>
                    <!-- <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/solar-panel.png'); ?>" width="18px">  -->
                    &nbsp &nbsp SOLAR PRODUCTION
                </h2>
            </div>
            <div class="solorproductionavg">
                <h2>SOLAR PRODUCTION (AVG MONTHLY)</h2>
            </div>
        </div>
        <div class="sectio-image">
            <div id="avg_kw_per_day_chart"></div>
            <!-- <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0010.jpg'); ?>" width="650px"> -->
        </div>
        <div class="firstBoxs" style="margin-top:0">
            <div class="sub_titlee">
                <p>Bill Before Solar</p>
                <div class="box" id="final_electricity_calculation">5989</div>
            </div>
            <div class="sub_titlee">
                <p>Bill After Solar</p>
                <div class="box" id="bill_after_solar"></div>
            </div>
            <div class="sub_titlee">
                <p>%of Electricity Bill Saving</p>
                <div class="box" id="bill_saving"></div>
            </div>
        </div>
        <div class="firstBoxs">
            <div class="sub_titlee1">
                <p>Annual Solar Production</p>
                <div class="box1" id="annual_solar_production"></div>
            </div>
            <div class="sub_titlee1">
                <p>Offset Consumption</p>
                <div class="box1" id="offset_consumption"></div>
            </div>
            <div class="sub_titlee1">
                <p>Export To Grid</p>
                <div class="box1" id="export_to_grid"></div>
            </div>
        </div>
        <div class="firstBoxs">
            <div class="sub_titlee2" style="margin-left:104px;">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-1.jpg'); ?>" style="margin-left: 90px; margin-bottom: 15px; width:80px;">
                <div class="sub_titlee2-inner">
                    <p>% of Energy Provided by Solar</p>
                    <div class="box1" id="percantage_engery_provided_solar"></div>
                </div>
            </div>
            <!-- <div class="sub_titlee1">
            <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-2.jpg'); ?>" style="margin-left: 70px; margin-bottom: 20px; width: 70px;">
            <div class="box1"></div>
            <p>% of Bill Reduction</p>
         </div> -->
            <div class="sub_titlee2">
                <img src="<?php echo site_url('assets/solar_proposal_pdf_new/0016-3.jpg'); ?>" style="margin-left: 90px; margin-bottom: 22px; width: 78px;">
                <div class="sub_titlee2-inner">
                    <p>Reduces Carbon Footprint by</p>
                    <div class="box1" id="reduces_carbon"></div>
                </div>
            </div>
        </div>
        <div class="fimagee " style="margin-top:57px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a> </div>
        <div class="footer">
            <div class="footer">
                <div class="redf">
                </div>
                <div class="greyf">
                </div>
            </div>
        </div>
    </div>
    <script>
        var saving_meter_calculation = <?php echo $saving_meter_calculation; ?>;
        var system_performance = <?php echo $system_performance; ?>;
        var dollarUSLocale = Intl.NumberFormat('en-US');

        google.load('visualization', '1', {
            packages: ['corechart']
        });

        var pvwatts_data_result = <?php echo $pvwatts_data_result; ?>;
        var pvSt_month1 = 0;
        var pvSt_month2 = 0;
        var pvSt_month3 = 0;
        var pvSt_month4 = 0;
        var pvSt_month5 = 0;
        var pvSt_month6 = 0;
        var pvSt_month7 = 0;
        var pvSt_month8 = 0;
        var pvSt_month9 = 0;
        var pvSt_month10 = 0;
        var pvSt_month11 = 0;
        var pvSt_month12 = 0;
        var pvtotal_production = 0;
        var pvSt_avg_prev = 0;
        var pvSt_avg = 0;




        $(document).ready(function() {
            $("#final_electricity_calculation").text('$' + dollarUSLocale.format(system_performance.FinalLoadChargesWithRates.toFixed(0)));
            $("#annual_solar_production").text(dollarUSLocale.format(system_performance.AnnualSolarProduction.toFixed(0)) + ' kWh');
            $("#offset_consumption").text(dollarUSLocale.format(saving_meter_calculation.offset_consumption.toFixed(0)) + ' kWh');
            $("#percantage_engery_provided_solar").text(system_performance.FinalPercantageEnergyProvidedBySolar.toFixed(2) + '%');
            $("#bill_after_solar").text('$' + dollarUSLocale.format(system_performance.FinalBillAfterSolar.toFixed(0)));
            $("#export_to_grid").text(system_performance.FinalPercantageOfExport.toFixed(2) + '%');

            $("#bill_saving").text((((system_performance.FinalLoadChargesWithRates.toFixed(2) - system_performance.FinalBillAfterSolar.toFixed(2)) * 100) / system_performance.FinalLoadChargesWithRates.toFixed(2)).toFixed(2) + '%');

            $("#reduces_carbon").text(dollarUSLocale.format(system_performance.FinalReducesCarbon.toFixed(0)) + ' kg');

            console.log(system_performance);

            getPvwattCalcAPI();
        })

        function getPvwattCalcAPI() {

            console.log(pvwatts_data_result.outputs);
            var pv_output = pvwatts_data_result;

            pvSt_month1 = pvSt_month1 + pv_output.outputs.ac_monthly[0];
            pvSt_month2 = pvSt_month2 + pv_output.outputs.ac_monthly[1];
            pvSt_month3 = pvSt_month3 + pv_output.outputs.ac_monthly[2];
            pvSt_month4 = pvSt_month4 + pv_output.outputs.ac_monthly[3];
            pvSt_month5 = pvSt_month5 + pv_output.outputs.ac_monthly[4];
            pvSt_month6 = pvSt_month6 + pv_output.outputs.ac_monthly[5];
            pvSt_month7 = pvSt_month7 + pv_output.outputs.ac_monthly[6];
            pvSt_month8 = pvSt_month8 + pv_output.outputs.ac_monthly[7];
            pvSt_month9 = pvSt_month9 + pv_output.outputs.ac_monthly[8];
            pvSt_month10 = pvSt_month10 + pv_output.outputs.ac_monthly[9];
            pvSt_month11 = pvSt_month11 + pv_output.outputs.ac_monthly[10];
            pvSt_month12 = pvSt_month12 + pv_output.outputs.ac_monthly[11];

            // pvSt_avg = pvSt_avg_prev + (pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12);

            pvSt_avg = pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12
            pvSt_avg = Math.round(pvSt_avg / 12);
            pvSt_avg_prev = pvSt_avg_prev + pvSt_avg;
            pvSt_avg = pvSt_avg_prev;




            //  parseInt(pvtotal_production) + 
            pvtotal_production = parseInt(pv_output.outputs.ac_annual);
            // console.log(pvSt_month1)
            // $("#pv_json_data").html(pvwatts_data_html);

            // $("#pvtotal_production span").text(Math.round(pvtotal_production));

            google.setOnLoadCallback(drawChart);
        }

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Year", "kWh", "Average"],
                ["JAN", Math.round(self.pvSt_month1), self.pvSt_avg],
                ["FEB", Math.round(self.pvSt_month2), self.pvSt_avg],
                ["MAR", Math.round(self.pvSt_month3), self.pvSt_avg],
                ["APR", Math.round(self.pvSt_month4), self.pvSt_avg],
                ["MAY", Math.round(self.pvSt_month5), self.pvSt_avg],
                ["JUN", Math.round(self.pvSt_month6), self.pvSt_avg],
                ["JUL", Math.round(self.pvSt_month7), self.pvSt_avg],
                ["AUG", Math.round(self.pvSt_month8), self.pvSt_avg],
                ["SEP", Math.round(self.pvSt_month9), self.pvSt_avg],
                ["OCT", Math.round(self.pvSt_month10), self.pvSt_avg],
                ["NOV", Math.round(self.pvSt_month11), self.pvSt_avg],
                ["DEC", Math.round(self.pvSt_month12), self.pvSt_avg],
            ]);
            var options = {
                curveType: 'function',
                titleTextStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontStyle: 'normal',
                    fontSize: 12
                },
                bar: {
                    groupWidth: "88%"
                },
                legend: {
                    position: 'bottom',
                    textStyle: {
                        fontName: 'Arial, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 14,
                        fontStyle: 'normal'
                    }
                },
                hAxis: {
                    format: "#'$'",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                vAxis: {
                    format: "#'kWh'",
                    title: "",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                width: 910,
                height: 380,
                legend: {
                    position: 'none',
                    textStyle: {
                        fontSize: 4,
                    }
                },
                seriesType: 'bars',
                series: {
                    0: {
                        color: '#A1CB79'
                    },
                    1: {
                        type: 'line',
                        color: '#000',
                        visibleInLegend: false
                    }
                }
            };
            var chart = new google.visualization.ComboChart(document.getElementById('avg_kw_per_day_chart'));
            chart.draw(data, options);
        }
    </script>
</div>
<div id="page2" class="w910">
    <style>
        .page14 {
            background-color: #FFFFFF;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page14 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page14 .top hr {
            width: 65%;
            height: 3px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
            border: none;
        }

        .page14 .text_color {
            color: #D01E2A;
        }

        .page14 .title {
            margin-top: -50px;
        }

        .page14 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page14 .title h2 {
            padding: 5px 10px;
            margin-left: 48px;
            margin-top: 100px;
            background-color: #D01E2A;
            width: 50%;
            color: #ffffff;
            background: #D01E2A;
            letter-spacing: 3px;
            font-size: 20px;
            border-right: 1px solid white;
            display: flex;
            align-items: center;
        }

        .page14 .footer {
            display: inline-flex;
        }

        .page14 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page14 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page14 .enimage img {
            margin-top: 15px;
            margin-left: 50px;
        }

        .page14 .fimagee img {
            margin-left: 730px;
            margin-top: 33px;
        }

        #avg_load_area_chart2 {
            margin-left: 2rem;
            margin-top: 3rem;
        }

        #avg_load_area_chart3 {
            margin-left: 2rem;
            margin-top: 3rem;
        }
    </style>
    <div class="back-bg page14">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA </h2>
            <hr>
        </div>
        <div class="title">
            <h1>ENERGY <span class="text_color"> CONSUMPTION</span></h1>
            <h2>
                <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                    <defs>
                        <style>
                            .cls-1 {
                                fill: #fff;
                            }
                        </style>
                    </defs>
                    <g id="Icon">
                        <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                        <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                        <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                    </g>
                </svg>
                AVERAGE MONTHLY
            </h2>
        </div>
        <div class=" enimage">
            <div id="avg_load_area_chart2"></div>
        </div>
        <div class="title">
            <h2>
                <svg id="Layer_1" data-name="Layer 1" width="38" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000">
                    <defs>
                        <style>
                            .cls-1 {
                                fill: #fff;
                            }
                        </style>
                    </defs>
                    <g id="Icon">
                        <polygon class="cls-1" points="634.94 200 357.71 539.14 449.9 539.14 365.06 800 642.29 460.86 550.1 460.86 634.94 200" />
                        <path class="cls-1" d="M277.5,500c0-122.69,99.81-222.5,222.5-222.5a222.23,222.23,0,0,1,42.43,4.08L581,234.44a277.67,277.67,0,0,0-217.9,507.05l17.51-53.84C318.64,648.1,277.5,578.77,277.5,500Z" />
                        <path class="cls-1" d="M636.94,258.51l-17.51,53.84C681.36,351.9,722.5,421.23,722.5,500c0,122.69-99.81,222.5-222.5,222.5a222.23,222.23,0,0,1-42.43-4.08L419,765.56a277.67,277.67,0,0,0,217.9-507Z" />
                    </g>
                </svg>
                AVERAGE WEEK DAY
            </h2>
        </div>
        <div class=" enimage">
            <div id="avg_load_area_chart3"></div>
        </div>
        <div class="fimagee" style="margin-top:-5px;">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
    <script>
        var graph_data = <?php echo $saving_meter_calculation; ?>;

        $(document).ready(function() {
            console.log(graph_data);

            show_graph(graph_data.avg_monthly_load_graph_data);
            weekend_graph(graph_data.weekend_load_graph_data);
        })

        function show_graph(avg_load_graph_data) {
            var labels = {};
            Highcharts.chart('avg_load_area_chart2', {
                chart: {
                    width: 780,
                    height: 360,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    type: 'datetime',
                    tickInterval: 3780 * 1000,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value + 'kW';
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 3,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Jan',
                        data: [
                            avg_load_graph_data['jan'][0],
                            avg_load_graph_data['jan'][1],
                            avg_load_graph_data['jan'][2],
                            avg_load_graph_data['jan'][3],
                            avg_load_graph_data['jan'][4],
                            avg_load_graph_data['jan'][5],
                            avg_load_graph_data['jan'][6],
                            avg_load_graph_data['jan'][7],
                            avg_load_graph_data['jan'][8],
                            avg_load_graph_data['jan'][9],
                            avg_load_graph_data['jan'][10],
                            avg_load_graph_data['jan'][11],
                            avg_load_graph_data['jan'][12],
                            avg_load_graph_data['jan'][13],
                            avg_load_graph_data['jan'][14],
                            avg_load_graph_data['jan'][15],
                            avg_load_graph_data['jan'][16],
                            avg_load_graph_data['jan'][17],
                            avg_load_graph_data['jan'][18],
                            avg_load_graph_data['jan'][19],
                            avg_load_graph_data['jan'][20],
                            avg_load_graph_data['jan'][21],
                            avg_load_graph_data['jan'][22],
                            avg_load_graph_data['jan'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Feb',
                        data: [
                            avg_load_graph_data['feb'][0],
                            avg_load_graph_data['feb'][1],
                            avg_load_graph_data['feb'][2],
                            avg_load_graph_data['feb'][3],
                            avg_load_graph_data['feb'][4],
                            avg_load_graph_data['feb'][5],
                            avg_load_graph_data['feb'][6],
                            avg_load_graph_data['feb'][7],
                            avg_load_graph_data['feb'][8],
                            avg_load_graph_data['feb'][9],
                            avg_load_graph_data['feb'][10],
                            avg_load_graph_data['feb'][11],
                            avg_load_graph_data['feb'][12],
                            avg_load_graph_data['feb'][13],
                            avg_load_graph_data['feb'][14],
                            avg_load_graph_data['feb'][15],
                            avg_load_graph_data['feb'][16],
                            avg_load_graph_data['feb'][17],
                            avg_load_graph_data['feb'][18],
                            avg_load_graph_data['feb'][19],
                            avg_load_graph_data['feb'][20],
                            avg_load_graph_data['feb'][21],
                            avg_load_graph_data['feb'][22],
                            avg_load_graph_data['feb'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Mar',
                        data: [
                            avg_load_graph_data['mar'][0],
                            avg_load_graph_data['mar'][1],
                            avg_load_graph_data['mar'][2],
                            avg_load_graph_data['mar'][3],
                            avg_load_graph_data['mar'][4],
                            avg_load_graph_data['mar'][5],
                            avg_load_graph_data['mar'][6],
                            avg_load_graph_data['mar'][7],
                            avg_load_graph_data['mar'][8],
                            avg_load_graph_data['mar'][9],
                            avg_load_graph_data['mar'][10],
                            avg_load_graph_data['mar'][11],
                            avg_load_graph_data['mar'][12],
                            avg_load_graph_data['mar'][13],
                            avg_load_graph_data['mar'][14],
                            avg_load_graph_data['mar'][15],
                            avg_load_graph_data['mar'][16],
                            avg_load_graph_data['mar'][17],
                            avg_load_graph_data['mar'][18],
                            avg_load_graph_data['mar'][19],
                            avg_load_graph_data['mar'][20],
                            avg_load_graph_data['mar'][21],
                            avg_load_graph_data['mar'][22],
                            avg_load_graph_data['mar'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Apr',
                        data: [
                            avg_load_graph_data['apr'][0],
                            avg_load_graph_data['apr'][1],
                            avg_load_graph_data['apr'][2],
                            avg_load_graph_data['apr'][3],
                            avg_load_graph_data['apr'][4],
                            avg_load_graph_data['apr'][5],
                            avg_load_graph_data['apr'][6],
                            avg_load_graph_data['apr'][7],
                            avg_load_graph_data['apr'][8],
                            avg_load_graph_data['apr'][9],
                            avg_load_graph_data['apr'][10],
                            avg_load_graph_data['apr'][11],
                            avg_load_graph_data['apr'][12],
                            avg_load_graph_data['apr'][13],
                            avg_load_graph_data['apr'][14],
                            avg_load_graph_data['apr'][15],
                            avg_load_graph_data['apr'][16],
                            avg_load_graph_data['apr'][17],
                            avg_load_graph_data['apr'][18],
                            avg_load_graph_data['apr'][19],
                            avg_load_graph_data['apr'][20],
                            avg_load_graph_data['apr'][21],
                            avg_load_graph_data['apr'][22],
                            avg_load_graph_data['apr'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'May',
                        data: [
                            avg_load_graph_data['may'][0],
                            avg_load_graph_data['may'][1],
                            avg_load_graph_data['may'][2],
                            avg_load_graph_data['may'][3],
                            avg_load_graph_data['may'][4],
                            avg_load_graph_data['may'][5],
                            avg_load_graph_data['may'][6],
                            avg_load_graph_data['may'][7],
                            avg_load_graph_data['may'][8],
                            avg_load_graph_data['may'][9],
                            avg_load_graph_data['may'][10],
                            avg_load_graph_data['may'][11],
                            avg_load_graph_data['may'][12],
                            avg_load_graph_data['may'][13],
                            avg_load_graph_data['may'][14],
                            avg_load_graph_data['may'][15],
                            avg_load_graph_data['may'][16],
                            avg_load_graph_data['may'][17],
                            avg_load_graph_data['may'][18],
                            avg_load_graph_data['may'][19],
                            avg_load_graph_data['may'][20],
                            avg_load_graph_data['may'][21],
                            avg_load_graph_data['may'][22],
                            avg_load_graph_data['may'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Jun',
                        data: [
                            avg_load_graph_data['jun'][0],
                            avg_load_graph_data['jun'][1],
                            avg_load_graph_data['jun'][2],
                            avg_load_graph_data['jun'][3],
                            avg_load_graph_data['jun'][4],
                            avg_load_graph_data['jun'][5],
                            avg_load_graph_data['jun'][6],
                            avg_load_graph_data['jun'][7],
                            avg_load_graph_data['jun'][8],
                            avg_load_graph_data['jun'][9],
                            avg_load_graph_data['jun'][10],
                            avg_load_graph_data['jun'][11],
                            avg_load_graph_data['jun'][12],
                            avg_load_graph_data['jun'][13],
                            avg_load_graph_data['jun'][14],
                            avg_load_graph_data['jun'][15],
                            avg_load_graph_data['jun'][16],
                            avg_load_graph_data['jun'][17],
                            avg_load_graph_data['jun'][18],
                            avg_load_graph_data['jun'][19],
                            avg_load_graph_data['jun'][20],
                            avg_load_graph_data['jun'][21],
                            avg_load_graph_data['jun'][22],
                            avg_load_graph_data['jun'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Jul',
                        data: [
                            avg_load_graph_data['jul'][0],
                            avg_load_graph_data['jul'][1],
                            avg_load_graph_data['jul'][2],
                            avg_load_graph_data['jul'][3],
                            avg_load_graph_data['jul'][4],
                            avg_load_graph_data['jul'][5],
                            avg_load_graph_data['jul'][6],
                            avg_load_graph_data['jul'][7],
                            avg_load_graph_data['jul'][8],
                            avg_load_graph_data['jul'][9],
                            avg_load_graph_data['jul'][10],
                            avg_load_graph_data['jul'][11],
                            avg_load_graph_data['jul'][12],
                            avg_load_graph_data['jul'][13],
                            avg_load_graph_data['jul'][14],
                            avg_load_graph_data['jul'][15],
                            avg_load_graph_data['jul'][16],
                            avg_load_graph_data['jul'][17],
                            avg_load_graph_data['jul'][18],
                            avg_load_graph_data['jul'][19],
                            avg_load_graph_data['jul'][20],
                            avg_load_graph_data['jul'][21],
                            avg_load_graph_data['jul'][22],
                            avg_load_graph_data['jul'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Aug',
                        data: [
                            avg_load_graph_data['aug'][0],
                            avg_load_graph_data['aug'][1],
                            avg_load_graph_data['aug'][2],
                            avg_load_graph_data['aug'][3],
                            avg_load_graph_data['aug'][4],
                            avg_load_graph_data['aug'][5],
                            avg_load_graph_data['aug'][6],
                            avg_load_graph_data['aug'][7],
                            avg_load_graph_data['aug'][8],
                            avg_load_graph_data['aug'][9],
                            avg_load_graph_data['aug'][10],
                            avg_load_graph_data['aug'][11],
                            avg_load_graph_data['aug'][12],
                            avg_load_graph_data['aug'][13],
                            avg_load_graph_data['aug'][14],
                            avg_load_graph_data['aug'][15],
                            avg_load_graph_data['aug'][16],
                            avg_load_graph_data['aug'][17],
                            avg_load_graph_data['aug'][18],
                            avg_load_graph_data['aug'][19],
                            avg_load_graph_data['aug'][20],
                            avg_load_graph_data['aug'][21],
                            avg_load_graph_data['aug'][22],
                            avg_load_graph_data['aug'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Sep',
                        data: [
                            avg_load_graph_data['sep'][0],
                            avg_load_graph_data['sep'][1],
                            avg_load_graph_data['sep'][2],
                            avg_load_graph_data['sep'][3],
                            avg_load_graph_data['sep'][4],
                            avg_load_graph_data['sep'][5],
                            avg_load_graph_data['sep'][6],
                            avg_load_graph_data['sep'][7],
                            avg_load_graph_data['sep'][8],
                            avg_load_graph_data['sep'][9],
                            avg_load_graph_data['sep'][10],
                            avg_load_graph_data['sep'][11],
                            avg_load_graph_data['sep'][12],
                            avg_load_graph_data['sep'][13],
                            avg_load_graph_data['sep'][14],
                            avg_load_graph_data['sep'][15],
                            avg_load_graph_data['sep'][16],
                            avg_load_graph_data['sep'][17],
                            avg_load_graph_data['sep'][18],
                            avg_load_graph_data['sep'][19],
                            avg_load_graph_data['sep'][20],
                            avg_load_graph_data['sep'][21],
                            avg_load_graph_data['sep'][22],
                            avg_load_graph_data['sep'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Oct',
                        data: [
                            avg_load_graph_data['oct'][0],
                            avg_load_graph_data['oct'][1],
                            avg_load_graph_data['oct'][2],
                            avg_load_graph_data['oct'][3],
                            avg_load_graph_data['oct'][4],
                            avg_load_graph_data['oct'][5],
                            avg_load_graph_data['oct'][6],
                            avg_load_graph_data['oct'][7],
                            avg_load_graph_data['oct'][8],
                            avg_load_graph_data['oct'][9],
                            avg_load_graph_data['oct'][10],
                            avg_load_graph_data['oct'][11],
                            avg_load_graph_data['oct'][12],
                            avg_load_graph_data['oct'][13],
                            avg_load_graph_data['oct'][14],
                            avg_load_graph_data['oct'][15],
                            avg_load_graph_data['oct'][16],
                            avg_load_graph_data['oct'][17],
                            avg_load_graph_data['oct'][18],
                            avg_load_graph_data['oct'][19],
                            avg_load_graph_data['oct'][20],
                            avg_load_graph_data['oct'][21],
                            avg_load_graph_data['oct'][22],
                            avg_load_graph_data['oct'][23]
                        ],
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Nov',
                        data: [
                            avg_load_graph_data['nov'][0],
                            avg_load_graph_data['nov'][1],
                            avg_load_graph_data['nov'][2],
                            avg_load_graph_data['nov'][3],
                            avg_load_graph_data['nov'][4],
                            avg_load_graph_data['nov'][5],
                            avg_load_graph_data['nov'][6],
                            avg_load_graph_data['nov'][7],
                            avg_load_graph_data['nov'][8],
                            avg_load_graph_data['nov'][9],
                            avg_load_graph_data['nov'][10],
                            avg_load_graph_data['nov'][11],
                            avg_load_graph_data['nov'][12],
                            avg_load_graph_data['nov'][13],
                            avg_load_graph_data['nov'][14],
                            avg_load_graph_data['nov'][15],
                            avg_load_graph_data['nov'][16],
                            avg_load_graph_data['nov'][17],
                            avg_load_graph_data['nov'][18],
                            avg_load_graph_data['nov'][19],
                            avg_load_graph_data['nov'][20],
                            avg_load_graph_data['nov'][21],
                            avg_load_graph_data['nov'][22],
                            avg_load_graph_data['nov'][23]
                        ],

                        zIndex: 1,

                        fillOpacity: 0,

                        marker: {

                            lineWidth: 2,

                            enabled: true,

                            symbol: 'circle'

                        }

                    },

                    {
                        name: 'Dec',
                        data: [
                            avg_load_graph_data['dec'][0],

                            avg_load_graph_data['dec'][1],

                            avg_load_graph_data['dec'][2],

                            avg_load_graph_data['dec'][3],

                            avg_load_graph_data['dec'][4],

                            avg_load_graph_data['dec'][5],

                            avg_load_graph_data['dec'][6],

                            avg_load_graph_data['dec'][7],

                            avg_load_graph_data['dec'][8],

                            avg_load_graph_data['dec'][9],

                            avg_load_graph_data['dec'][10],

                            avg_load_graph_data['dec'][11],

                            avg_load_graph_data['dec'][12],

                            avg_load_graph_data['dec'][13],

                            avg_load_graph_data['dec'][14],

                            avg_load_graph_data['dec'][15],

                            avg_load_graph_data['dec'][16],

                            avg_load_graph_data['dec'][17],

                            avg_load_graph_data['dec'][18],

                            avg_load_graph_data['dec'][19],

                            avg_load_graph_data['dec'][20],

                            avg_load_graph_data['dec'][21],

                            avg_load_graph_data['dec'][22],

                            avg_load_graph_data['dec'][23]

                        ],

                        zIndex: 1,

                        fillOpacity: 0,

                        marker: {

                            lineWidth: 2,

                            enabled: true,

                            symbol: 'circle'

                        }

                    },



                ]





            });
        }

        function weekend_graph(weekend_load_graph_data) {
            // console.log(avg_load_graph_data)
            var labels = {};

            Highcharts.chart('avg_load_area_chart3', {

                chart: {
                    width: 780,
                    height: 360,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    type: 'datetime',
                    // step: 24,
                    tickInterval: 3780 * 1000,
                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value + 'kW';
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }

                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        // pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Mon',
                        data: [
                            weekend_load_graph_data['mon'][0],
                            weekend_load_graph_data['mon'][1],
                            weekend_load_graph_data['mon'][2],
                            weekend_load_graph_data['mon'][3],
                            weekend_load_graph_data['mon'][4],
                            weekend_load_graph_data['mon'][5],
                            weekend_load_graph_data['mon'][6],
                            weekend_load_graph_data['mon'][7],
                            weekend_load_graph_data['mon'][8],
                            weekend_load_graph_data['mon'][9],
                            weekend_load_graph_data['mon'][10],
                            weekend_load_graph_data['mon'][11],
                            weekend_load_graph_data['mon'][12],
                            weekend_load_graph_data['mon'][13],
                            weekend_load_graph_data['mon'][14],
                            weekend_load_graph_data['mon'][15],
                            weekend_load_graph_data['mon'][16],
                            weekend_load_graph_data['mon'][17],
                            weekend_load_graph_data['mon'][18],
                            weekend_load_graph_data['mon'][19],
                            weekend_load_graph_data['mon'][20],
                            weekend_load_graph_data['mon'][21],
                            weekend_load_graph_data['mon'][22],
                            weekend_load_graph_data['mon'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Tue',
                        data: [
                            weekend_load_graph_data['tue'][0],
                            weekend_load_graph_data['tue'][1],
                            weekend_load_graph_data['tue'][2],
                            weekend_load_graph_data['tue'][3],
                            weekend_load_graph_data['tue'][4],
                            weekend_load_graph_data['tue'][5],
                            weekend_load_graph_data['tue'][6],
                            weekend_load_graph_data['tue'][7],
                            weekend_load_graph_data['tue'][8],
                            weekend_load_graph_data['tue'][9],
                            weekend_load_graph_data['tue'][10],
                            weekend_load_graph_data['tue'][11],
                            weekend_load_graph_data['tue'][12],
                            weekend_load_graph_data['tue'][13],
                            weekend_load_graph_data['tue'][14],
                            weekend_load_graph_data['tue'][15],
                            weekend_load_graph_data['tue'][16],
                            weekend_load_graph_data['tue'][17],
                            weekend_load_graph_data['tue'][18],
                            weekend_load_graph_data['tue'][19],
                            weekend_load_graph_data['tue'][20],
                            weekend_load_graph_data['tue'][21],
                            weekend_load_graph_data['tue'][22],
                            weekend_load_graph_data['tue'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Wed',
                        data: [
                            weekend_load_graph_data['wed'][0],
                            weekend_load_graph_data['wed'][1],
                            weekend_load_graph_data['wed'][2],
                            weekend_load_graph_data['wed'][3],
                            weekend_load_graph_data['wed'][4],
                            weekend_load_graph_data['wed'][5],
                            weekend_load_graph_data['wed'][6],
                            weekend_load_graph_data['wed'][7],
                            weekend_load_graph_data['wed'][8],
                            weekend_load_graph_data['wed'][9],
                            weekend_load_graph_data['wed'][10],
                            weekend_load_graph_data['wed'][11],
                            weekend_load_graph_data['wed'][12],
                            weekend_load_graph_data['wed'][13],
                            weekend_load_graph_data['wed'][14],
                            weekend_load_graph_data['wed'][15],
                            weekend_load_graph_data['wed'][16],
                            weekend_load_graph_data['wed'][17],
                            weekend_load_graph_data['wed'][18],
                            weekend_load_graph_data['wed'][19],
                            weekend_load_graph_data['wed'][20],
                            weekend_load_graph_data['wed'][21],
                            weekend_load_graph_data['wed'][22],
                            weekend_load_graph_data['wed'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Thu',
                        data: [
                            weekend_load_graph_data['thu'][0],
                            weekend_load_graph_data['thu'][1],
                            weekend_load_graph_data['thu'][2],
                            weekend_load_graph_data['thu'][3],
                            weekend_load_graph_data['thu'][4],
                            weekend_load_graph_data['thu'][5],
                            weekend_load_graph_data['thu'][6],
                            weekend_load_graph_data['thu'][7],
                            weekend_load_graph_data['thu'][8],
                            weekend_load_graph_data['thu'][9],
                            weekend_load_graph_data['thu'][10],
                            weekend_load_graph_data['thu'][11],
                            weekend_load_graph_data['thu'][12],
                            weekend_load_graph_data['thu'][13],
                            weekend_load_graph_data['thu'][14],
                            weekend_load_graph_data['thu'][15],
                            weekend_load_graph_data['thu'][16],
                            weekend_load_graph_data['thu'][17],
                            weekend_load_graph_data['thu'][18],
                            weekend_load_graph_data['thu'][19],
                            weekend_load_graph_data['thu'][20],
                            weekend_load_graph_data['thu'][21],
                            weekend_load_graph_data['thu'][22],
                            weekend_load_graph_data['thu'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Fri',
                        data: [
                            weekend_load_graph_data['fri'][0],
                            weekend_load_graph_data['fri'][1],
                            weekend_load_graph_data['fri'][2],
                            weekend_load_graph_data['fri'][3],
                            weekend_load_graph_data['fri'][4],
                            weekend_load_graph_data['fri'][5],
                            weekend_load_graph_data['fri'][6],
                            weekend_load_graph_data['fri'][7],
                            weekend_load_graph_data['fri'][8],
                            weekend_load_graph_data['fri'][9],
                            weekend_load_graph_data['fri'][10],
                            weekend_load_graph_data['fri'][11],
                            weekend_load_graph_data['fri'][12],
                            weekend_load_graph_data['fri'][13],
                            weekend_load_graph_data['fri'][14],
                            weekend_load_graph_data['fri'][15],
                            weekend_load_graph_data['fri'][16],
                            weekend_load_graph_data['fri'][17],
                            weekend_load_graph_data['fri'][18],
                            weekend_load_graph_data['fri'][19],
                            weekend_load_graph_data['fri'][20],
                            weekend_load_graph_data['fri'][21],
                            weekend_load_graph_data['fri'][22],
                            weekend_load_graph_data['fri'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Sat',
                        data: [
                            weekend_load_graph_data['sat'][0],
                            weekend_load_graph_data['sat'][1],
                            weekend_load_graph_data['sat'][2],
                            weekend_load_graph_data['sat'][3],
                            weekend_load_graph_data['sat'][4],
                            weekend_load_graph_data['sat'][5],
                            weekend_load_graph_data['sat'][6],
                            weekend_load_graph_data['sat'][7],
                            weekend_load_graph_data['sat'][8],
                            weekend_load_graph_data['sat'][9],
                            weekend_load_graph_data['sat'][10],
                            weekend_load_graph_data['sat'][11],
                            weekend_load_graph_data['sat'][12],
                            weekend_load_graph_data['sat'][13],
                            weekend_load_graph_data['sat'][14],
                            weekend_load_graph_data['sat'][15],
                            weekend_load_graph_data['sat'][16],
                            weekend_load_graph_data['sat'][17],
                            weekend_load_graph_data['sat'][18],
                            weekend_load_graph_data['sat'][19],
                            weekend_load_graph_data['sat'][20],
                            weekend_load_graph_data['sat'][21],
                            weekend_load_graph_data['sat'][22],
                            weekend_load_graph_data['sat'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    },

                    {
                        name: 'Sun',
                        data: [
                            weekend_load_graph_data['sun'][0],
                            weekend_load_graph_data['sun'][1],
                            weekend_load_graph_data['sun'][2],
                            weekend_load_graph_data['sun'][3],
                            weekend_load_graph_data['sun'][4],
                            weekend_load_graph_data['sun'][5],
                            weekend_load_graph_data['sun'][6],
                            weekend_load_graph_data['sun'][7],
                            weekend_load_graph_data['sun'][8],
                            weekend_load_graph_data['sun'][9],
                            weekend_load_graph_data['sun'][10],
                            weekend_load_graph_data['sun'][11],
                            weekend_load_graph_data['sun'][12],
                            weekend_load_graph_data['sun'][13],
                            weekend_load_graph_data['sun'][14],
                            weekend_load_graph_data['sun'][15],
                            weekend_load_graph_data['sun'][16],
                            weekend_load_graph_data['sun'][17],
                            weekend_load_graph_data['sun'][18],
                            weekend_load_graph_data['sun'][19],
                            weekend_load_graph_data['sun'][20],
                            weekend_load_graph_data['sun'][21],
                            weekend_load_graph_data['sun'][22],
                            weekend_load_graph_data['sun'][23]
                        ],

                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            lineWidth: 2,
                            enabled: true,
                            symbol: 'circle'
                        }
                    }
                ]
            });
        }
    </script>
</div>
<div id="page3" class="w910">
    <style>
        .page15 {
            background-color: #fff;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page15 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page15 .top hr {
            width: 65%;
            height: 3px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
            border: none;
        }

        .page15 .text_color {
            color: #D01E2A;
        }

        .page15 .title {
            margin-top: -50px;
        }

        .page15 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page15 .plasting h2 {
            font-size: 14px;
            padding: 10px;
            letter-spacing: 3px;
            margin-left: 48px;
            margin-top: 100px;
            background-color: #D01E2A;
            width: 84%;
            display: flex;
            align-items: center;
            color: #ffffff;
            background: #D01E2A;
            border-right: 1px solid white;
        }

        .page15 .plasting h2 svg {
            margin-right: 15px;
            margin-left: 5px;
            fill: #fff;
        }

        .page15 .displayyy {
            display: inline-flex;
        }

        .page15 .mt45 {
            margin-top: -45px;
        }

        .page15 .footer {
            display: inline-flex;
        }

        .page15 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page15 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page15 .iiimage img {
            width: 384px;
            margin-left: 48px;
            margin-top: 19px;
        }

        .page15 .fimagee img {
            margin-left: 730px;
            margin-top: 33px;
        }

        #typical_export_graph,
        #worst_export_graph,
        #high_export_graph,
        #best_export_graph {
            margin-left: 2rem;
            margin-top: 2rem;
        }
    </style>
    <div class="back-bg page15">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
            <hr>
        </div>
        <div class="title">
            <h1>DAILY <span class="text_color"> AVERAGE PRODUCTION</span></h1>
        </div>
        <div class="displayyy mt45">
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    JANUARY - MARCH
                </h2>
                <div id="typical_export_graph"></div>
            </div>
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    APRIL - JUNE
                </h2>
                <div id="worst_export_graph"></div>
            </div>
        </div>
        <div class="displayyy mt45">
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    JULY - SEPTEMBER
                </h2>
                <div id="high_export_graph"></div>
            </div>
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    OCTOBER - DECEMBER
                </h2>
                <div id="best_export_graph"></div>
            </div>
        </div>
        <div class="fimagee" style="margin-top: 10px;">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
    <script>
        var graph_data = <?php echo $saving_meter_calculation; ?>;
        var daily_average_production = <?php echo $saving_graph_calculation; ?>;

        $(document).ready(function() {
            console.log(<?php echo $time_of_use; ?>);
            // console.log(graph_data);
            // console.log(daily_average_production);

            show_production(daily_average_production.typical_graph);
            show_production1(daily_average_production.worst_graph);
            show_production2(daily_average_production.high_graph);
            show_production3(daily_average_production.best_graph);
        })

        function show_production(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var typical_export_load_graph_data = d.typical_export_load_graph_data;
            var typical_export_sol_prd_graph_data = d.typical_export_sol_prd_graph_data;
            var typical_export_export_graph_data = d.typical_export_export_graph_data;
            var typical_export_offset_graph_data = d.typical_export_offset_graph_data;

            Highcharts.chart('typical_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },

                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,
                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 3600,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 3,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        // pointStart: Date.UTC(2018, 11, 1, 0, 0, 0),
                        // pointInterval: 3780 * 1000
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            typical_export_load_graph_data[0],
                            typical_export_load_graph_data[1],
                            typical_export_load_graph_data[2],
                            typical_export_load_graph_data[3],
                            typical_export_load_graph_data[4],
                            typical_export_load_graph_data[5],
                            typical_export_load_graph_data[6],
                            typical_export_load_graph_data[7],
                            typical_export_load_graph_data[8],
                            typical_export_load_graph_data[9],
                            typical_export_load_graph_data[10],
                            typical_export_load_graph_data[11],
                            typical_export_load_graph_data[12],
                            typical_export_load_graph_data[13],
                            typical_export_load_graph_data[14],
                            typical_export_load_graph_data[15],
                            typical_export_load_graph_data[16],
                            typical_export_load_graph_data[17],
                            typical_export_load_graph_data[18],
                            typical_export_load_graph_data[19],
                            typical_export_load_graph_data[20],
                            typical_export_load_graph_data[21],
                            typical_export_load_graph_data[22],
                            typical_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    },
                    {
                        name: 'Solar Output',
                        data: [
                            typical_export_sol_prd_graph_data[0],
                            typical_export_sol_prd_graph_data[1],
                            typical_export_sol_prd_graph_data[2],
                            typical_export_sol_prd_graph_data[3],
                            typical_export_sol_prd_graph_data[4],
                            typical_export_sol_prd_graph_data[5],
                            typical_export_sol_prd_graph_data[6],
                            typical_export_sol_prd_graph_data[7],
                            typical_export_sol_prd_graph_data[8],
                            typical_export_sol_prd_graph_data[9],
                            typical_export_sol_prd_graph_data[10],
                            typical_export_sol_prd_graph_data[11],
                            typical_export_sol_prd_graph_data[12],
                            typical_export_sol_prd_graph_data[13],
                            typical_export_sol_prd_graph_data[14],
                            typical_export_sol_prd_graph_data[15],
                            typical_export_sol_prd_graph_data[16],
                            typical_export_sol_prd_graph_data[17],
                            typical_export_sol_prd_graph_data[18],
                            typical_export_sol_prd_graph_data[19],
                            typical_export_sol_prd_graph_data[20],
                            typical_export_sol_prd_graph_data[21],
                            typical_export_sol_prd_graph_data[22],
                            typical_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            typical_export_export_graph_data[0],
                            typical_export_export_graph_data[1],
                            typical_export_export_graph_data[2],
                            typical_export_export_graph_data[3],
                            typical_export_export_graph_data[4],
                            typical_export_export_graph_data[5],
                            typical_export_export_graph_data[6],
                            typical_export_export_graph_data[7],
                            typical_export_export_graph_data[8],
                            typical_export_export_graph_data[9],
                            typical_export_export_graph_data[10],
                            typical_export_export_graph_data[11],
                            typical_export_export_graph_data[12],
                            typical_export_export_graph_data[13],
                            typical_export_export_graph_data[14],
                            typical_export_export_graph_data[15],
                            typical_export_export_graph_data[16],
                            typical_export_export_graph_data[17],
                            typical_export_export_graph_data[18],
                            typical_export_export_graph_data[19],
                            typical_export_export_graph_data[20],
                            typical_export_export_graph_data[21],
                            typical_export_export_graph_data[22],
                            typical_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            typical_export_offset_graph_data[0],
                            typical_export_offset_graph_data[1],
                            typical_export_offset_graph_data[2],
                            typical_export_offset_graph_data[3],
                            typical_export_offset_graph_data[4],
                            typical_export_offset_graph_data[5],
                            typical_export_offset_graph_data[6],
                            typical_export_offset_graph_data[7],
                            typical_export_offset_graph_data[8],
                            typical_export_offset_graph_data[9],
                            typical_export_offset_graph_data[10],
                            typical_export_offset_graph_data[11],
                            typical_export_offset_graph_data[12],
                            typical_export_offset_graph_data[13],
                            typical_export_offset_graph_data[14],
                            typical_export_offset_graph_data[15],
                            typical_export_offset_graph_data[16],
                            typical_export_offset_graph_data[17],
                            typical_export_offset_graph_data[18],
                            typical_export_offset_graph_data[19],
                            typical_export_offset_graph_data[20],
                            typical_export_offset_graph_data[21],
                            typical_export_offset_graph_data[22],
                            typical_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });
        }

        function show_production1(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var worst_export_load_graph_data = d.worst_export_load_graph_data;
            var worst_export_sol_prd_graph_data = d.worst_export_sol_prd_graph_data;
            var worst_export_export_graph_data = d.worst_export_export_graph_data;
            var worst_export_offset_graph_data = d.worst_export_offset_graph_data;

            Highcharts.chart('worst_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {

                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            worst_export_load_graph_data[0],
                            worst_export_load_graph_data[1],
                            worst_export_load_graph_data[2],
                            worst_export_load_graph_data[3],
                            worst_export_load_graph_data[4],
                            worst_export_load_graph_data[5],
                            worst_export_load_graph_data[6],
                            worst_export_load_graph_data[7],
                            worst_export_load_graph_data[8],
                            worst_export_load_graph_data[9],
                            worst_export_load_graph_data[10],
                            worst_export_load_graph_data[11],
                            worst_export_load_graph_data[12],
                            worst_export_load_graph_data[13],
                            worst_export_load_graph_data[14],
                            worst_export_load_graph_data[15],
                            worst_export_load_graph_data[16],
                            worst_export_load_graph_data[17],
                            worst_export_load_graph_data[18],
                            worst_export_load_graph_data[19],
                            worst_export_load_graph_data[20],
                            worst_export_load_graph_data[21],
                            worst_export_load_graph_data[22],
                            worst_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            worst_export_sol_prd_graph_data[0],
                            worst_export_sol_prd_graph_data[1],
                            worst_export_sol_prd_graph_data[2],
                            worst_export_sol_prd_graph_data[3],
                            worst_export_sol_prd_graph_data[4],
                            worst_export_sol_prd_graph_data[5],
                            worst_export_sol_prd_graph_data[6],
                            worst_export_sol_prd_graph_data[7],
                            worst_export_sol_prd_graph_data[8],
                            worst_export_sol_prd_graph_data[9],
                            worst_export_sol_prd_graph_data[10],
                            worst_export_sol_prd_graph_data[11],
                            worst_export_sol_prd_graph_data[12],
                            worst_export_sol_prd_graph_data[13],
                            worst_export_sol_prd_graph_data[14],
                            worst_export_sol_prd_graph_data[15],
                            worst_export_sol_prd_graph_data[16],
                            worst_export_sol_prd_graph_data[17],
                            worst_export_sol_prd_graph_data[18],
                            worst_export_sol_prd_graph_data[19],
                            worst_export_sol_prd_graph_data[20],
                            worst_export_sol_prd_graph_data[21],
                            worst_export_sol_prd_graph_data[22],
                            worst_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            worst_export_export_graph_data[0],
                            worst_export_export_graph_data[1],
                            worst_export_export_graph_data[2],
                            worst_export_export_graph_data[3],
                            worst_export_export_graph_data[4],
                            worst_export_export_graph_data[5],
                            worst_export_export_graph_data[6],
                            worst_export_export_graph_data[7],
                            worst_export_export_graph_data[8],
                            worst_export_export_graph_data[9],
                            worst_export_export_graph_data[10],
                            worst_export_export_graph_data[11],
                            worst_export_export_graph_data[12],
                            worst_export_export_graph_data[13],
                            worst_export_export_graph_data[14],
                            worst_export_export_graph_data[15],
                            worst_export_export_graph_data[16],
                            worst_export_export_graph_data[17],
                            worst_export_export_graph_data[18],
                            worst_export_export_graph_data[19],
                            worst_export_export_graph_data[20],
                            worst_export_export_graph_data[21],
                            worst_export_export_graph_data[22],
                            worst_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            worst_export_offset_graph_data[0],
                            worst_export_offset_graph_data[1],
                            worst_export_offset_graph_data[2],
                            worst_export_offset_graph_data[3],
                            worst_export_offset_graph_data[4],
                            worst_export_offset_graph_data[5],
                            worst_export_offset_graph_data[6],
                            worst_export_offset_graph_data[7],
                            worst_export_offset_graph_data[8],
                            worst_export_offset_graph_data[9],
                            worst_export_offset_graph_data[10],
                            worst_export_offset_graph_data[11],
                            worst_export_offset_graph_data[12],
                            worst_export_offset_graph_data[13],
                            worst_export_offset_graph_data[14],
                            worst_export_offset_graph_data[15],
                            worst_export_offset_graph_data[16],
                            worst_export_offset_graph_data[17],
                            worst_export_offset_graph_data[18],
                            worst_export_offset_graph_data[19],
                            worst_export_offset_graph_data[20],
                            worst_export_offset_graph_data[21],
                            worst_export_offset_graph_data[22],
                            worst_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });

        }


        function show_production2(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var high_export_load_graph_data = d.high_export_load_graph_data;
            var high_export_sol_prd_graph_data = d.high_export_sol_prd_graph_data;
            var high_export_export_graph_data = d.high_export_export_graph_data;
            var high_export_offset_graph_data = d.high_export_offset_graph_data;


            Highcharts.chart('high_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            high_export_load_graph_data[0],
                            high_export_load_graph_data[1],
                            high_export_load_graph_data[2],
                            high_export_load_graph_data[3],
                            high_export_load_graph_data[4],
                            high_export_load_graph_data[5],
                            high_export_load_graph_data[6],
                            high_export_load_graph_data[7],
                            high_export_load_graph_data[8],
                            high_export_load_graph_data[9],
                            high_export_load_graph_data[10],
                            high_export_load_graph_data[11],
                            high_export_load_graph_data[12],
                            high_export_load_graph_data[13],
                            high_export_load_graph_data[14],
                            high_export_load_graph_data[15],
                            high_export_load_graph_data[16],
                            high_export_load_graph_data[17],
                            high_export_load_graph_data[18],
                            high_export_load_graph_data[19],
                            high_export_load_graph_data[20],
                            high_export_load_graph_data[21],
                            high_export_load_graph_data[22],
                            high_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            high_export_sol_prd_graph_data[0],
                            high_export_sol_prd_graph_data[1],
                            high_export_sol_prd_graph_data[2],
                            high_export_sol_prd_graph_data[3],
                            high_export_sol_prd_graph_data[4],
                            high_export_sol_prd_graph_data[5],
                            high_export_sol_prd_graph_data[6],
                            high_export_sol_prd_graph_data[7],
                            high_export_sol_prd_graph_data[8],
                            high_export_sol_prd_graph_data[9],
                            high_export_sol_prd_graph_data[10],
                            high_export_sol_prd_graph_data[11],
                            high_export_sol_prd_graph_data[12],
                            high_export_sol_prd_graph_data[13],
                            high_export_sol_prd_graph_data[14],
                            high_export_sol_prd_graph_data[15],
                            high_export_sol_prd_graph_data[16],
                            high_export_sol_prd_graph_data[17],
                            high_export_sol_prd_graph_data[18],
                            high_export_sol_prd_graph_data[19],
                            high_export_sol_prd_graph_data[20],
                            high_export_sol_prd_graph_data[21],
                            high_export_sol_prd_graph_data[22],
                            high_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            high_export_export_graph_data[0],
                            high_export_export_graph_data[1],
                            high_export_export_graph_data[2],
                            high_export_export_graph_data[3],
                            high_export_export_graph_data[4],
                            high_export_export_graph_data[5],
                            high_export_export_graph_data[6],
                            high_export_export_graph_data[7],
                            high_export_export_graph_data[8],
                            high_export_export_graph_data[9],
                            high_export_export_graph_data[10],
                            high_export_export_graph_data[11],
                            high_export_export_graph_data[12],
                            high_export_export_graph_data[13],
                            high_export_export_graph_data[14],
                            high_export_export_graph_data[15],
                            high_export_export_graph_data[16],
                            high_export_export_graph_data[17],
                            high_export_export_graph_data[18],
                            high_export_export_graph_data[19],
                            high_export_export_graph_data[20],
                            high_export_export_graph_data[21],
                            high_export_export_graph_data[22],
                            high_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            high_export_offset_graph_data[0],
                            high_export_offset_graph_data[1],
                            high_export_offset_graph_data[2],
                            high_export_offset_graph_data[3],
                            high_export_offset_graph_data[4],
                            high_export_offset_graph_data[5],
                            high_export_offset_graph_data[6],
                            high_export_offset_graph_data[7],
                            high_export_offset_graph_data[8],
                            high_export_offset_graph_data[9],
                            high_export_offset_graph_data[10],
                            high_export_offset_graph_data[11],
                            high_export_offset_graph_data[12],
                            high_export_offset_graph_data[13],
                            high_export_offset_graph_data[14],
                            high_export_offset_graph_data[15],
                            high_export_offset_graph_data[16],
                            high_export_offset_graph_data[17],
                            high_export_offset_graph_data[18],
                            high_export_offset_graph_data[19],
                            high_export_offset_graph_data[20],
                            high_export_offset_graph_data[21],
                            high_export_offset_graph_data[22],
                            high_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });


        }


        function show_production3(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var best_export_load_graph_data = d.best_export_load_graph_data;
            var best_export_sol_prd_graph_data = d.best_export_sol_prd_graph_data;
            var best_export_export_graph_data = d.best_export_export_graph_data;
            var best_export_offset_graph_data = d.best_export_offset_graph_data;

            Highcharts.chart('best_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            best_export_load_graph_data[0],
                            best_export_load_graph_data[1],
                            best_export_load_graph_data[2],
                            best_export_load_graph_data[3],
                            best_export_load_graph_data[4],
                            best_export_load_graph_data[5],
                            best_export_load_graph_data[6],
                            best_export_load_graph_data[7],
                            best_export_load_graph_data[8],
                            best_export_load_graph_data[9],
                            best_export_load_graph_data[10],
                            best_export_load_graph_data[11],
                            best_export_load_graph_data[12],
                            best_export_load_graph_data[13],
                            best_export_load_graph_data[14],
                            best_export_load_graph_data[15],
                            best_export_load_graph_data[16],
                            best_export_load_graph_data[17],
                            best_export_load_graph_data[18],
                            best_export_load_graph_data[19],
                            best_export_load_graph_data[20],
                            best_export_load_graph_data[21],
                            best_export_load_graph_data[22],
                            best_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            best_export_sol_prd_graph_data[0],
                            best_export_sol_prd_graph_data[1],
                            best_export_sol_prd_graph_data[2],
                            best_export_sol_prd_graph_data[3],
                            best_export_sol_prd_graph_data[4],
                            best_export_sol_prd_graph_data[5],
                            best_export_sol_prd_graph_data[6],
                            best_export_sol_prd_graph_data[7],
                            best_export_sol_prd_graph_data[8],
                            best_export_sol_prd_graph_data[9],
                            best_export_sol_prd_graph_data[10],
                            best_export_sol_prd_graph_data[11],
                            best_export_sol_prd_graph_data[12],
                            best_export_sol_prd_graph_data[13],
                            best_export_sol_prd_graph_data[14],
                            best_export_sol_prd_graph_data[15],
                            best_export_sol_prd_graph_data[16],
                            best_export_sol_prd_graph_data[17],
                            best_export_sol_prd_graph_data[18],
                            best_export_sol_prd_graph_data[19],
                            best_export_sol_prd_graph_data[20],
                            best_export_sol_prd_graph_data[21],
                            best_export_sol_prd_graph_data[22],
                            best_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            best_export_export_graph_data[0],
                            best_export_export_graph_data[1],
                            best_export_export_graph_data[2],
                            best_export_export_graph_data[3],
                            best_export_export_graph_data[4],
                            best_export_export_graph_data[5],
                            best_export_export_graph_data[6],
                            best_export_export_graph_data[7],
                            best_export_export_graph_data[8],
                            best_export_export_graph_data[9],
                            best_export_export_graph_data[10],
                            best_export_export_graph_data[11],
                            best_export_export_graph_data[12],
                            best_export_export_graph_data[13],
                            best_export_export_graph_data[14],
                            best_export_export_graph_data[15],
                            best_export_export_graph_data[16],
                            best_export_export_graph_data[17],
                            best_export_export_graph_data[18],
                            best_export_export_graph_data[19],
                            best_export_export_graph_data[20],
                            best_export_export_graph_data[21],
                            best_export_export_graph_data[22],
                            best_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            best_export_offset_graph_data[0],
                            best_export_offset_graph_data[1],
                            best_export_offset_graph_data[2],
                            best_export_offset_graph_data[3],
                            best_export_offset_graph_data[4],
                            best_export_offset_graph_data[5],
                            best_export_offset_graph_data[6],
                            best_export_offset_graph_data[7],
                            best_export_offset_graph_data[8],
                            best_export_offset_graph_data[9],
                            best_export_offset_graph_data[10],
                            best_export_offset_graph_data[11],
                            best_export_offset_graph_data[12],
                            best_export_offset_graph_data[13],
                            best_export_offset_graph_data[14],
                            best_export_offset_graph_data[15],
                            best_export_offset_graph_data[16],
                            best_export_offset_graph_data[17],
                            best_export_offset_graph_data[18],
                            best_export_offset_graph_data[19],
                            best_export_offset_graph_data[20],
                            best_export_offset_graph_data[21],
                            best_export_offset_graph_data[22],
                            best_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });

        }
    </script>
</div>
<div id="page4" class="w910">
    <style>
        .page16 {
            background-color: white;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page16 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 630px;
            padding-top: 30px;
        }

        .page16 .top hr {
            width: 70%;
            height: 3px;
            margin: -24px 0px 0px;
            background-color: #D01E2A;
            border: none;
        }

        .page16 .text_color {
            color: #D01E2A;
        }

        .vkcontainer {
            margin: 0 30px;
        }

        .page16 .title h1 {
            padding-left: 14px;
            margin-top: 40px;
            border-left: 5px solid #D01E2A;
        }

        .page16 .vktitle h2 {
            font-weight: 500;
            letter-spacing: 3px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 99%;
            color: #ffffff;
            background: #D01E2A;
        }

        .page16 table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .page16 tr {
            border-bottom: 1px solid #cac8c8;
        }

        .page16 td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page16 .fimagee img {
            margin-left: 730px;
            margin-top: 33px;
        }

        .page16 .footer {
            display: inline-flex;
        }

        .page16 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page16 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page16 .iiimage img {
            width: 384px;
            margin-left: 48px;
            margin-top: 19px;
        }

        .page16 .vk-innerbox {
            width: 33%;
            float: left;
            text-align: center;
        }

        .page16 .vk-innerbox h4 {
            margin: 0px;
        }

        .page16 .vkid-section {
            padding-top: 40px;
            margin-bottom: 60px;
        }

        .page16 .vktitle h2 span {
            background: #fff;
            padding: 2px 4px;
            margin-right: 15px;
            color: #fff;
            font-size: 12px;
        }

        .page16 .headertable tr {
            border: none;
            text-align: center;
        }

        .page16 .headertable td {
            width: 33.33%;
        }

        .page16 table.headertable {
            margin-top: 75px;
        }

        .page16 .tablefooter tr {
            border: none;
        }

        .page16 .tablefooter tr th {
            text-align: left;
            padding-left: 20px;
            color: #D01E2A;
        }

        .page16 .tablefooter td {
            width: 50%;
            padding-left: 20px;
        }

        .page16 .comfooter {
            border-top: 1px dashed #D01E2A;
            border-bottom: 1px dashed #D01E2A;
            padding: 30px 0;
            margin-top: 70px;
        }

        .page16 .fimagee {
            padding-bottom: 0;
            /*margin-top: 250px;*/
        }
    </style>

    <?php
    $FinancialType = isset($financial_summary_data['type'])? $financial_summary_data['type'] : '';
    $TProjectCost1 = "";
    $TProjectCost2 = "";
    $TVeecDiscount = "";

    if (isset($financial_summary_data['type']) && $financial_summary_data['type'] == 'VEEC') {

        $FinancialType = 'VEEC';

        $TVeecDiscount = number_format((float)($financial_summary_data['veec_discount']), 0, '', ',');

        if (!empty($financial_summary_data['project_cost'])) {
            $TProjectCost = (array)json_decode($financial_summary_data['project_cost']);
            $TProjectCost1 = number_format((float)($TProjectCost[0]), 0, '', ',');
            $TProjectCost2 = number_format((float)($TProjectCost[1]), 0, '', ',');
        }

        if (!empty($financial_summary_data['monthly_payment_plan'])) {
            $TMonthlyPaymentPlan = (array)json_decode($financial_summary_data['monthly_payment_plan']);
            $MonthlyPayment1 = number_format((float)($TMonthlyPaymentPlan[0]), 0, '', ',');
            $MonthlyPayment2 = number_format((float)($TMonthlyPaymentPlan[1]), 0, '', ',');
        }

        if (!empty($financial_summary_data['ppa_rate'])) {
            $TPPARate = (array)json_decode($financial_summary_data['ppa_rate']);
            $TPPARate1 = $TPPARate[0];
            $TPPARate2 = $TPPARate[1];
        }

        if (!empty($financial_summary_data['ppa_checkbox'])) {
            $ppa_checkbox = (array)json_decode($financial_summary_data['ppa_checkbox']);
            $ppa_checkbox1 = $ppa_checkbox[0];
            $ppa_checkbox2 = $ppa_checkbox[1];
        }
        if (!empty($financial_summary_data['ppa_term'])) {
            $TPPATermValue = (array)json_decode($financial_summary_data['ppa_term']);
            $TPPATermValue1 = getTPPATerm($TPPATermValue[0]);
            $TPPATermValue2 = getTPPATerm($TPPATermValue[1]);
        }

        if (!empty($financial_summary_data['term'])) {
            $TTermValue = (array)json_decode($financial_summary_data['term']);
            $TTermValue1 = getTPPATerm($TTermValue[0]);
            $TTermValue2 = getTPPATerm($TTermValue[1]);
        }

        $monthlyPaymentPlanSolar = 0;
        if (isset($MonthlyPayment)) {
            $monthlyPaymentPlanSolar = $TMonthlyPaymentPlan['2'] * 12;
        }
        $AnnualCashflow = number_format((float)($monthlyPaymentPlanSolar), 0, '', ',');
    }

    function getTPPATerm($TPPATerm)
    {
        $ReturnTerm = $TPPATerm / 12;
        return $ReturnTerm;
    }
    $Tilldate = new DateTime($proposal_data['created_at']); // Y-m-d
    $Tilldate->add(new DateInterval('P30D'));
    ?>

    <div class="back-bg page16">
        <div class="vkcontainer">
            <div class="top vk-header" style="width:100%;">
                <div class="headertext">
                    <h2>
                        <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
                </div>
                <hr>
            </div>
            <div class="title">
                <h1 style="text-transform:uppercase;">Project <span class="text_color"> Costs</span></h1>
            </div>
            <div class="vkheader-top">
                <table class="headertable" style="margin-top: 25px;">
                    <tr>
                        <th style="border-right: 2px solid #D01E2A;">Quote ID</th>
                        <th style="border-right: 2px solid #D01E2A;">Date</td>
                        <th>Authorised Person</td>
                    </tr>
                    <tr>
                        <td style="border-right: 2px solid #D01E2A;"><?php echo $proposal_data['id']; ?></td>
                        <td style="border-right: 2px solid #D01E2A;"><?php echo date_format(date_create($proposal_data['created_at']), "l, jS F Y"); ?></td>
                        <td><?php echo $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?></td>
                    </tr>
                </table>
            </div>


            <?php if ($FinancialType != 'VEEC') { ?>
                <div class="vktitle" style="padding-top:20px;">
                    <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 1</b> upfront payment</h2>
                </div>
                <div>
                    <table style="background:#EDEBEC;">
                        <tr>
                            <td>Deposit payment (30%)</td>
                            <td style="text-align: right;">$<?php echo number_format((float)(($proposal_data['price_before_stc_rebate'] * 30 / 100)), 0, '', ','); ?></td>
                        </tr>
                        <tr>
                            <td>Payment on installation commencement (70%)</td>
                            <td style="text-align: right;">$
                                <?php echo number_format((float)(($proposal_data['price_before_stc_rebate'] * 70 / 100)), 0, '', ','); ?></td>
                        </tr>
                        <?php /** <tr>
                            <td>On job completion (10%)</td>
                            <td style="text-align: right;">$
                                <?php echo number_format((float)(($proposal_data['price_before_stc_rebate'] * 10 / 100)) , 0, '', ','); ?></td>
                        </tr> */ ?>
                        <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                            <td><b>Total Payable</b> (excl. GST)</td>
                            <td style="text-align: right;"><strong>$<?php echo number_format((float)($proposal_data['price_before_stc_rebate']), 0, '', ','); ?></strong></td>
                        </tr>
                    </table>
                </div>
                <!-- option 2 -->
                <?php if (count($proposal_finance_data)) { ?>
                    <div class="vktitle">
                        <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> OPTION 2 </b>FINANCE THROUGH ENERGY SAVINGS</h2>
                    </div>
                    <div style="padding-bottom: px;">
                        <table style="background:#EDEBEC;">
                            <tr>
                                <td>Term (years)</td>
                                <td style="text-align: right;"><?php echo $year; ?></td>
                            </tr>
                            <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                                <td><b>Monthly payment plan</b></td>
                                <td style="text-align: right;"><strong>$<?php echo number_format((float)($proposal_finance_data['monthly_payment_plan']), 0, '', ','); ?></strong></td>
                            </tr>
                        </table>
                    </div>

                    <?php if ($financial_summary_data['ppa_checkbox'] == 1) { ?>
                        <div class="vktitle">
                            <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> OPTION 3 </b>POWER PURCHASE AGREEMENT (PPA)</h2>
                        </div>
                        <div style="padding-bottom: 0px;">
                            <table style="background:#EDEBEC;">
                                <tr>
                                    <td>Term (years)</td>
                                    <td style="text-align: right;"><?php echo $ppa_year; ?></td>
                                </tr>
                                <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                                    <td><b>PPA Rate</b> (¢/kWh)</td>
                                    <td style="text-align: right;"><strong><?php echo $financial_summary_data['ppa_rate']; ?></strong></td>
                                </tr>
                            </table>
                        </div>
                    <?php } ?>
                <?php
                } ?>
            <?php
            } else { ?>
                <div class="vktitle" style="padding-top:20px;">
                    <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 1</b> upfront Discount</h2>
                </div>
                <div>
                    <table style="background:#EDEBEC;">
                        <tr>
                            <td>CAPEX</td>
                            <td>FINANCE</td>
                            <?php if($ppa_checkbox1==1){?>
                            <td>PPA</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Term</b>: <?php echo $TTermValue1; ?> Years</td>
                            <?php if($ppa_checkbox1==1){?>
                            <td><b>Term</b>: <?php echo $TPPATermValue1; ?> Years</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><b>Total Payable</b> (excl. GST): <strong>$<?php echo $TProjectCost1; ?></strong></td>
                            <td><b>Monthly Repayments</b>: $<?php echo $MonthlyPayment1; ?></td>
                            <?php if($ppa_checkbox1==1){?>
                            <td><b>¢/kWh</b>: <?php echo $TPPARate1; ?></td>
                            <?php } ?>
                        </tr>
                    </table>
                </div>
                <div class="vktitle" style="padding-top:20px;">
                    <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 2</b> VEEC CLAIM</h2>
                </div>
                <div>
                    <table style="background:#EDEBEC;">
                        <tr>
                            <td>CAPEX</td>
                            <td>FINANCE</td>
                            <?php if($ppa_checkbox2==1){?>
                            <td>PPA</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><b>Project Cost</b>: $<?php echo $TProjectCost2; ?></td>
                            <td></td>
                            <?php if($ppa_checkbox2==1){?>
                            <td></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><b>VEEC Discount</b>: $<?php echo $TVeecDiscount; ?></td>
                            <td><b>Term</b>: <?php echo $TTermValue2; ?> Years</td>
                            <?php if($ppa_checkbox2==1){?>
                            <td><b>Term</b>: <?php echo $TPPATermValue2; ?> Years</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><b>Total Payable</b> (excl. GST): <strong>$<?php echo $TProjectCost2; ?></strong></td>
                            <td><b>Monthly Repayments</b>: $<?php echo $MonthlyPayment2; ?></td>
                            <?php if($ppa_checkbox2==1){?>
                            <td><b>¢/kWh</b>: <?php echo $TPPARate2; ?>¢</td>
                            <?php } ?>
                        </tr>
                    </table>
                </div>
            <?php
            } ?>
            <div class="vaildquote" style="margin-bottom:158px;">
                <h4 style="color:#D01E2A;">This quote is valid till <?php echo $Tilldate->format('d/m/Y'); ?></h4>
                <div class="comfooter" style="margin-top: 10px;">
                    <table class="tablefooter" style="width:100%;">
                        <tr>
                            <th style="border-right: 1px dashed #D01E2A;">Company Name:</th>
                            <th style="padding-left:50px;">Company Address:</td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px dashed #D01E2A;"><?php echo $lead_data['customer_company_name']; ?></td>
                            <td style="padding-left:50px;"><?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="fimagee" style="<?php if($FinancialType == 'LGC' && $financial_summary_data['ppa_checkbox'] != 1){ echo "margin-top: 310px;"; }else{ echo "margin-top: 230px;"; } ?>">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</div>


<div id="page5" class="w910">

    <?php
    $stat = '';
    $STCRebateValue = 0;
    if (isset($financial_summary_data['degradation_data'])) {
        $stat = $financial_summary_data['degradation_data'];
    }

    $SolarEnergyRatewidth = '';
    $TProjectCost = array();
    $FinanceTerm = $year;
    $GraphInvestment = $proposal_data['price_before_stc_rebate'];

    if (isset($proposal_data['stc_rebate_value'])) {
        $STCRebateValue = number_format((float) ($proposal_data['stc_rebate_value']), 0, '', ',');
    }


    $FinancialType = "STC";
    if (isset($financial_summary_data['type'])) {
        $FinancialType = $financial_summary_data['type'];
    }

    $MonthlyPayment = $monthlyPaymentPlanSolar = 0;
    if (isset($proposal_finance_data['monthly_payment_plan'])) {
        $MonthlyPayment = number_format((float)($proposal_finance_data['monthly_payment_plan']), 0, '', ',');
        $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan']*12;
    }
    $AnnualCashflow = number_format((float)($Cashflow), 0, '', ',');
    $SolarEnergyRatewidth = '';

    if (isset($financial_summary_data['type']) && $financial_summary_data['type'] == 'VEEC') {
        $SolarEnergyRatewidth = 'width:40%';

        $TMonthlyPaymentPlan = (array)json_decode($financial_summary_data['monthly_payment_plan']);
        $MonthlyPayment = number_format((float)($TMonthlyPaymentPlan[0]), 0, '', ',');

        $TProjectCost = (array)json_decode($financial_summary_data['project_cost']);
        $TProjectCost1 = number_format((float)($TProjectCost[0]), 0, '', ',');
        $TProjectCost2 = number_format((float)($TProjectCost[1]), 0, '', ',');
        $GraphInvestment = $TProjectCost[0];

        $TPPATerm = (array)json_decode($financial_summary_data['ppa_term']);

        $FinanceTerm = $TPPATerm[0] / 12;
        $monthlyPaymentPlanSolar = 0;
        if (isset($MonthlyPayment)) {
            $monthlyPaymentPlanSolar = $TMonthlyPaymentPlan[0] * 12;
        }
        $MonthlyPayment = number_format((float)($TMonthlyPaymentPlan[0]), 0, '', ',');
        $AnnualCashflow = number_format((float)($monthlyPaymentPlanSolar), 0, '', ',');
    }

    $TenYearsSolarRate = 0;
    if (isset($financial_summary_data['degradation_data'])) {
        $stat = $financial_summary_data['degradation_data'];
        $statJson = json_decode($financial_summary_data['degradation_data']);
        $FinalEnergyProvidedBySolar = $statJson->FinalEnergyProvidedBySolar;

        for ($i = 0; $i < 10; $i++) {
            $TenYearsSolarRate = $TenYearsSolarRate + $FinalEnergyProvidedBySolar[$i];
        }

        if ($financial_summary_data['type'] == 'VEEC') {
            $TenYearsSolarRate = ($TProjectCost[0] / $TenYearsSolarRate) * 100;
        } else {
            $TenYearsSolarRate = $proposal_data['price_before_stc_rebate'] / $TenYearsSolarRate;
        }
    }

    $MeterDataPiaYear = 0;
    $MeterDataPiaPercentage = 0;
    $MeterDataSubsequent = 0;

    if (isset($proposal_data['price_annual_increase'])) {
        $price_annual_increase = json_decode($proposal_data['price_annual_increase']);
        $MeterDataPiaYear = $price_annual_increase->MeterDataPiaYear;
        $MeterDataPiaPercentage = $price_annual_increase->MeterDataPiaPercentage;
        $MeterDataSubsequent = $price_annual_increase->MeterDataSubsequent;
    }

    ?>
    <style>
        .page17 {
            background-color: white;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page17 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 630px;
            padding-top: 30px;
        }

        .page17 .top hr {
            width: 70%;
            height: 3px;
            margin: -24px 0px 0px;
            background-color: #D01E2A;
            border: none;
        }

        .page17 .text_color {
            color: #D01E2A;
        }

        .page17 .vkcontainer {
            margin: 0 30px;
        }

        .page17 .title h1 {
            padding-left: 14px;
            margin-top: 40px;
            border-left: 5px solid #D01E2A;
        }

        .page17 .vktitle h2 {
            font-weight: 500;
            letter-spacing: 1px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            background: #D01E2A;
        }

        .page17 table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .page17 tr {
            border-bottom: 1px solid #cac8c8;
        }

        .page17 td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page17 .footer {
            display: inline-flex;
        }

        .page17 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page17 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page17 .image img {
            margin-top: -50px;
        }

        .page17 .fimagee img {
            margin-top: 9px;
            margin-left: 730px;
        }

        .page17 .vk-innerbox {
            width: 33%;
            float: left;
            text-align: center;
        }

        .page17 .vk-innerbox h4 {
            margin: 0px;
        }

        .page17 .vkid-section {
            padding-top: 40px;
            margin-bottom: 60px;
        }

        .page17 .vktitle h2 span {
            background: #fff;
            padding: 2px 4px;
            margin-right: 15px;
            color: #fff;
            font-size: 12px;
        }

        .page17 .vksaving-box tr {
            border: none;
        }

        .page17 .rightvkbx h3 {
            font-weight: 600;
            letter-spacing: 3px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            text-align: center;
        }

        .page17 .finacetable tr {
            border-bottom: 2px solid #D01E2A;
        }

        .page17 .finacetable tr th {
            text-align: left;
            font-size: 20px;
        }

        .page17 .finacetable tr td {
            text-align: right;
            font-size: 20px;
        }

        .page17 .finacetable tr th span {
            background: #D01E2A;
            padding: 4px 7px;
            margin-right: 15px;
            color: #D01E2A;
            font-size: 12px;
            border-radius: 50%;
        }

        .page17 .footertext {
            margin: 50px 100px 0;
            padding-bottom: 0;
        }

        .page17 .vksaving-box {
            margin-left: 100px;
            margin-right: 100px;
        }

        .page17 .vkftrm {
            margin: 0 30px;
        }

        /*.page17 .fimagee {*/
        /*   padding-bottom: 0;*/
        /*   margin-top: 220px;*/
        /*}*/
    </style>
    <div class="back-bg page17">
        <div class="vkcontainer">
            <div class="top vk-header" style="width:100%;">
                <div class="headertext">
                    <h2>
                        <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
                </div>
                <hr>
            </div>
            <div class="title">
                <h1 style="text-transform:uppercase;">FINANCIAL <span class="text_color"> SUMMARY<?php if ($FinancialType == 'VEEC') {
                                                                                                        echo ' - UPFRONT DISCOUNT';
                                                                                                    } ?></span></h1>
            </div>
            <div class="vktitle" style="padding-top:12px;">
                <h2 class="customBlock" style="text-transform:uppercase; font-size:20px;width: 52%;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon1.png'); ?>" style="width:32px;"><b style="vertical-align: super;"> OUTRIGHT PURCHASE</b></h2>
            </div>
            <div class="vkbargraph">
                <table>
                    <tr style="border:none;">
                        <td>
                            <!-- <img src="<?php echo site_url('assets/solar_proposal_pdf_new/img1.png'); ?>" width="100%"> -->
                            <div id="financial_summery_graph" style="margin-top: -30px;margin-bottom: -18px;"></div>
                        </td>
                        <!-- <td><img src="<?php echo site_url('assets/solar_proposal_pdf_new/img1.png'); ?>" width="100%"></td> -->
                    </tr>
                </table>
            </div>
            <div class="vksaving-box customBlock">
                <table>
                    <tr>
                        <th style="width:50%">SAVINGS PER YEAR</th>
                        <th style="width:50%; padding:0px 0px 0px 20px;">RETURN OF INVESTMENT</th>
                    </tr>
                </table>
            </div>
            <div class="vksaving-box" style="margin-top:15px;">
                <table>
                    <tr>
                        <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                            <h3 style="margin: 0;" id="saving_per_year"></h3>
                        </td>
                        <td class="rightvkbx" style="padding:0px 15px;">
                            <h3 style="margin: 0;" id="return_of_investment"></h3>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- option 2 -->
            <?php if (count($proposal_finance_data)) { ?>
                <div class="vktitle">
                    <h2 style="text-transform:uppercase; font-size:20px;width: 30%;margin-top:30px;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon2.png'); ?>" style="width: 36px;"><b style="vertical-align: super;"> FINANCE </b></h2>
                </div>
                <div class="vkftrm">
                    <table class="finacetable">
                        <tr>
                            <th><span>0</span> FINANCE TERMS:</th>
                            <td><?php echo $FinanceTerm; ?></td>
                        </tr>
                        <tr>
                            <th><span>0</span> MONTHLY PAYMENT:</th>
                            <td>$<?php echo $MonthlyPayment; ?>
                            </td>
                        </tr>
                        <tr>
                            <th><span>0</span> ANNUAL CASHFLOW:</th>
                            <td id="annual_cashflow"></td>
                        </tr>
                    </table>
                </div>
            <?php
            } ?>
            <div class="vksaving-box" style="margin-top:25px; <?php echo $SolarEnergyRatewidth; ?>">
                <table>
                    <tr>
                        <?php if ($FinancialType != 'VEEC') { ?>
                            <th>TOTAL <?php echo $FinancialType; ?> FINANCIAL INCENTIVE</th>
                        <?php
                        } ?>
                        <th style="color:#D01E2A">10 YEARS SOLAR ENERGY RATE</th>
                    </tr>
                </table>
            </div>
            <div class="vksaving-box" style="margin-top:25px; <?php echo $SolarEnergyRatewidth; ?>">
                <table>
                    <tr style="margin-top:15px;">
                        <?php if ($FinancialType != 'VEEC') { ?>
                            <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                                <?php if ($FinancialType == 'STC') { ?>
                                    <h3 style="margin: 0;">$<?php echo $STCRebateValue; ?></h3>
                                <?php } else { ?>
                                    <h3 style="margin: 0;" id="FinalLGCIncentives"></h3>
                                <?php } ?>
                            </td>
                        <?php
                        } ?>
                        <td class="rightvkbx" style="padding:0px 15px;">
                            <h3 style="margin: 0;"><?php echo number_format((float)$TenYearsSolarRate, 2, '.', ''); ?>¢/kWh</h3>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="footertext" style="padding-bottom: 0; margin-bottom: 30px;">
                <p>Total revenue includes potential <?php echo $FinancialType; ?> savings, <?php echo $FinancialType; ?> savings are determined by differing market conditions</p>
                <p>Savings are calculated with <?php echo $MeterDataPiaPercentage; ?>% average annual increase for first <?php echo $MeterDataPiaYear; ?> Years and <?php echo $MeterDataSubsequent; ?>% subseuenltly in energy cost from energy provider</p>
            </div>
        </div>
        <div class="fimagee" style="margin-top: 250px;">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>

    <script>
        var stat = <?php echo $stat; ?>;
        var cashflow1 = <?php echo $monthlyPaymentPlanSolar; ?>;
        var capex_value = <?php echo $proposal_data['price_before_stc_rebate']; ?>;
        var FinancialSummeryGraphCount = 0;
        var FinancialSummeryGraphData = [];
        google.load('visualization', '1', {
            packages: ['corechart']
        });
        $(document).ready(function() {

            var data_length = stat.data_length;
            var FinalCummilativeSavings = stat.FinalCummilativeSavings;
            var ttype = stat.type;
            var FinalLGCIncentives = [];
            var FinalLGCIncentivesSum = 0;
            if (ttype == 'LGC') {
                FinalLGCIncentives = stat.FinalLGCValues;
                FinalLGCIncentivesSum = FinalLGCIncentives.reduce((partial_sum, a) => partial_sum + a, 0);
                $("#FinalLGCIncentives").text('$' + dollarUSLocale.format(FinalLGCIncentivesSum.toFixed(0)));
            }
            if (ttype == 'STC') {
                FinalLGCIncentives = stat.FinalLGCValues;
                FinalLGCIncentivesSum = FinalLGCIncentives.reduce((partial_sum, a) => partial_sum + a, 0);
                $("#FinalLGCIncentives").text('$' + dollarUSLocale.format(FinalLGCIncentivesSum.toFixed(0)));
            }
            FinancialSummeryGraphCount = FinalCummilativeSavings.length;
            FinancialSummeryGraphData = FinalCummilativeSavings;
            if (FinancialSummeryGraphCount != 0) {
                google.setOnLoadCallback(drawChart2);
            }

        });

        function drawChart2() {

            var FinancialSummeryGraphArray = [];
            var GraphInvestment = "<?php echo $GraphInvestment; ?>";
            console.log(FinancialSummeryGraphData);
            FinancialSummeryGraphArray[0] = ["Year", "", "Investement"];
            for (let index = 0; index < FinancialSummeryGraphCount; index++) {
                FinancialSummeryGraphArray[index + 1] = [(index + 1).toString(), FinancialSummeryGraphData[index], parseFloat(GraphInvestment)];
                if (FinancialSummeryGraphData[index] > parseFloat(GraphInvestment)) {
                    break;
                }
            }
            console.log(FinancialSummeryGraphData);
            $("#saving_per_year").text('$' + dollarUSLocale.format(FinancialSummeryGraphData[0].toFixed(0)));
            $("#annual_cashflow").text('$' + dollarUSLocale.format((FinancialSummeryGraphData[0] - cashflow1).toFixed(0)));
            // $("#return_of_investment").text((FinancialSummeryGraphArray.length - 1) + ' Years');
            $("#return_of_investment").text(parseFloat(GraphInvestment /FinancialSummeryGraphData[0]).toFixed(2) + ' Years');
            console.log(FinancialSummeryGraphArray)

            var data = google.visualization.arrayToDataTable(FinancialSummeryGraphArray);
            var options = {
                curveType: 'function',
                titleTextStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontStyle: 'normal',
                    fontSize: 12
                },
                bar: {
                    groupWidth: "50%"
                },
                legend: {
                    position: 'bottom',
                    textStyle: {
                        fontName: 'Arial, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 14,
                        fontStyle: 'normal'
                    }
                },
                hAxis: {
                    format: "#",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                vAxis: {
                    format: "'$'#",
                    title: "",
                    titleTextStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: "normal",
                    },
                    textStyle: {
                        fontName: "Arial, sans-serif;",
                        italic: false,
                        bold: false,
                        fontSize: 11,
                        fontStyle: "normal",
                    },
                },
                width: 810,
                height: 372,
                legend: {
                    position: 'none',
                    textStyle: {
                        fontSize: 4,
                    }
                },
                seriesType: 'bars',
                series: {
                    0: {
                        color: '#A1CB79'
                    },
                    1: {
                        type: 'line',
                        color: '#000',
                        visibleInLegend: false
                    }
                }
            };
            var chart = new google.visualization.ComboChart(document.getElementById('financial_summery_graph'));
            chart.draw(data, options);
        }
    </script>
</div>

<?php if (count($financial_summary_data) && $financial_summary_data['type'] === 'VEEC') { ?>
    <div id="page6" class="w910">
        <?php
        if (isset($financial_summary_data['degradation_data'])) {
            $stat = $financial_summary_data['degradation_data'];
        }

        $SolarEnergyRatewidth = '';
        $GraphInvestment = 0;
        $TProjectCost = array();
        $FinanceTerm = $year;

        if ($financial_summary_data['type'] == 'VEEC') {
            $SolarEnergyRatewidth = 'width:40%';

            $TMonthlyPaymentPlan = (array)json_decode($financial_summary_data['monthly_payment_plan']);
            $MonthlyPayment = number_format((float)($TMonthlyPaymentPlan[1]), 0, '', ',');

            $TProjectCost = (array)json_decode($financial_summary_data['project_cost']);
            $TProjectCost1 = number_format((float)($TProjectCost[0]), 0, '', ',');
            $TProjectCost2 = number_format((float)($TProjectCost[1]), 0, '', ',');
            $GraphInvestment = $TProjectCost[1];

            $TPPATerm = (array)json_decode($financial_summary_data['ppa_term']);
            $TPPATerm = is_numeric($TPPATerm[1]) ? $TPPATerm[1] : 0;

            $FinanceTerm = $TPPATerm / 12;
            $monthlyPaymentPlanSolar = 0;
            if (isset($MonthlyPayment)) {
                $monthlyPaymentPlanSolar = $TMonthlyPaymentPlan[1] * 12;
            }

            $MonthlyPayment = number_format((float)($TMonthlyPaymentPlan[1]), 0, '', ',');
            $AnnualCashflow = number_format((float) ($monthlyPaymentPlanSolar), 0, '', ',');
        } else {
            $MonthlyPayment = number_format((float)($proposal_finance_data['monthly_payment_plan']), 0, '', ',');
            $AnnualCashflow = number_format((float)($Cashflow), 0, '', ',');
            $monthlyPaymentPlanSolar = (float)($proposal_finance_data['monthly_payment_plan'])*12;
        }

        $TenYearsSolarRate = 0;
        if (isset($financial_summary_data['degradation_data'])) {
            $stat = $financial_summary_data['degradation_data'];
            $statJson = json_decode($financial_summary_data['degradation_data']);
            $FinalEnergyProvidedBySolar = $statJson->FinalEnergyProvidedBySolar;

            for ($i = 0; $i < 10; $i++) {
                $TenYearsSolarRate = $TenYearsSolarRate + $FinalEnergyProvidedBySolar[$i];
            }

            if ($financial_summary_data['type'] == 'VEEC') {
                $TenYearsSolarRate = ($TProjectCost[1] / $TenYearsSolarRate) * 100;
            } else {
                $TenYearsSolarRate = $proposal_data['price_before_stc_rebate'] / $TenYearsSolarRate;
            }
        }

        $MeterDataPiaYear = 0;
        $MeterDataPiaPercentage = 0;
        $MeterDataSubsequent = 0;

        if (isset($proposal_data['price_annual_increase'])) {
            $price_annual_increase = json_decode($proposal_data['price_annual_increase']);
            $MeterDataPiaYear = $price_annual_increase->MeterDataPiaYear;
            $MeterDataPiaPercentage = $price_annual_increase->MeterDataPiaPercentage;
            $MeterDataSubsequent = $price_annual_increase->MeterDataSubsequent;
        }
        ?>

        <div class="back-bg page17">
            <div class="vkcontainer">
                <div class="top vk-header" style="width:100%;">
                    <div class="headertext">
                        <h2>
                            <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
                    </div>
                    <hr>
                </div>
                <div class="title">
                    <h1 style="text-transform:uppercase;">FINANCIAL <span class="text_color"> SUMMARY<?php if ($financial_summary_data['type'] == 'VEEC') {
                                                                                                            echo ' - VEEC CLAIM';
                                                                                                        } ?></span></h1>
                </div>
                <div class="vktitle" style="padding-top:12px;">
                    <h2 class="customBlock" style="text-transform:uppercase; font-size:20px;width: 52%;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon1.png'); ?>" style="width:32px;"><b style="vertical-align: super;"> OUTRIGHT PURCHASE</b></h2>
                </div>
                <div class="vkbargraph">
                    <table>
                        <tr style="border:none;">
                            <td>
                                <div id="financial_summery_graph2"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="vksaving-box customBlock">
                    <table>
                        <tr>
                            <th style="width:50%">SAVINGS PER YEAR</th>
                            <th style="width:50%; padding:0px 0px 0px 20px;">RETURN OF INVESTMENT</th>
                        </tr>
                    </table>
                </div>
                <div class="vksaving-box" style="margin-top:20px;">
                    <table>
                        <tr>
                            <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                                <h3 style="margin: 0;" id="saving_per_year2"></h3>
                            </td>
                            <td class="rightvkbx" style="padding:0px 15px;">
                                <h3 style="margin: 0;" id="return_of_investment2"></h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- option 2 -->
                <?php if (count($proposal_finance_data)) { ?>
                    <div class="vktitle">
                        <h2 style="text-transform:uppercase; font-size:20px;width: 30%;margin-top:30px;padding-top: 6px;"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/icon2.png'); ?>" style="width: 36px;"><b style="vertical-align: super;"> FINANCE </b></h2>
                    </div>
                    <div class="vkftrm">
                        <table class="finacetable">
                            <tr>
                                <th><span>0</span> FINANCE TERMS:</th>
                                <td><?php echo $FinanceTerm; ?></td>
                            </tr>
                            <tr>
                                <th><span>0</span> MONTHLY PAYMENT:</th>
                                <td>$<?php echo $MonthlyPayment; ?>
                                </td>
                            </tr>
                            <tr>
                                <th><span>0</span> ANNUAL CASHFLOW:</th>
                                <td id="annual_cashflow2">$<?php echo $AnnualCashflow;?></td>
                            </tr>
                        </table>
                    </div>
                <?php
                } ?>
                <div class="vksaving-box" style="margin-top:30px; <?php echo $SolarEnergyRatewidth; ?>">
                    <table>
                        <tr>
                            <?php if ($financial_summary_data['type'] != 'VEEC') { ?>
                                <th>TOTAL <?php echo $financial_summary_data['type']; ?> FINANCIAL INCENTIVE</th>
                            <?php
                            } ?>
                            <th style="color:#D01E2A">10 YEARS SOLAR ENERGY RATE</th>
                        </tr>
                    </table>
                </div>
                <div class="vksaving-box" style="margin-top:20px; <?php echo $SolarEnergyRatewidth; ?>">
                    <table>
                        <tr style="margin-top:15px;">
                            <?php if ($financial_summary_data['type'] != 'VEEC') { ?>
                                <td class="vksaving" style="text-align:center;padding:0px 15px 0 0;border: 2px solid;
                        width: 50%;">
                                    <h3 style="margin: 0;" id="FinalLGCIncentives"></h3>
                                </td>
                            <?php
                            } ?>
                            <td class="rightvkbx" style="padding:0px 15px;">
                                <h3 style="margin: 0;"><?php echo number_format((float)$TenYearsSolarRate, 2, '.', ''); ?>¢/kWh</h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="footertext" style="padding-bottom: 0; margin-bottom: -16px;">
                    <p>Total revenue includes potential <?php echo $FinancialType; ?> savings, <?php echo $FinancialType; ?> savings are determined by differing market conditions</p>
                    <p>Savings are calculated with <?php echo $MeterDataPiaPercentage; ?>% average annual increase for first <?php echo $MeterDataPiaYear; ?> Years and <?php echo $MeterDataSubsequent; ?>% subseuenltly in energy cost from energy provider</p>
                </div>
            </div>
            <div class="fimagee" style="margin-top: 220px;">
                <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
            </div>
            <div class="footer">
                <div class="redf">
                </div>
                <div class="greyf">
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script>
            var stat = <?php echo $stat; ?>;
            var cashflow = <?php echo $monthlyPaymentPlanSolar; ?>;
            var capex_value = <?php echo $TProjectCost[1]; ?>;
            var FinancialSummeryGraphCount = 0;
            var FinancialSummeryGraphData = [];
            google.load('visualization', '1', {
                packages: ['corechart']
            });
            $(document).ready(function() {

                var data_length = stat.data_length;
                var FinalCummilativeSavings = stat.FinalCummilativeSavings;
                var ttype = stat.type;
                var FinalLGCIncentives = [];
                var FinalLGCIncentivesSum = 0;
                if (ttype == 'LGC') {
                    FinalLGCIncentives = stat.FinalLGCValues;
                    FinalLGCIncentivesSum = FinalLGCIncentives.reduce((partial_sum, a) => partial_sum + a, 0);
                    $("#FinalLGCIncentives").text('$' + dollarUSLocale.format(FinalLGCIncentivesSum.toFixed(0)));
                }
                FinancialSummeryGraphCount = FinalCummilativeSavings.length;
                FinancialSummeryGraphData = FinalCummilativeSavings;
                if (FinancialSummeryGraphCount != 0) {
                    google.setOnLoadCallback(drawChart3);
                }

            });

            function drawChart3() {

                var FinancialSummeryGraphArray = [];
                var GraphInvestment = "<?php echo $GraphInvestment; ?>";
                FinancialSummeryGraphArray[0] = ["Year", "", "Investement"];
                for (let index = 0; index < FinancialSummeryGraphCount; index++) {
                    FinancialSummeryGraphArray[index + 1] = [(index + 1).toString(), FinancialSummeryGraphData[index], parseFloat(GraphInvestment)];
                    if (FinancialSummeryGraphData[index] > parseFloat(GraphInvestment)) {
                        break;
                    }
                }
                console.log(FinancialSummeryGraphData);
                $("#saving_per_year2").text('$' + dollarUSLocale.format(FinancialSummeryGraphData[0].toFixed(0)));
                $("#annual_cashflow2").text('$' + dollarUSLocale.format((FinancialSummeryGraphData[0] - cashflow).toFixed(0)));
                // $("#return_of_investment2").text((FinancialSummeryGraphArray.length - 1) + ' Years');
                $("#return_of_investment2").text(parseFloat(GraphInvestment /FinancialSummeryGraphData[0]).toFixed(2) + ' Years');

                console.log(FinancialSummeryGraphArray)

                var data = google.visualization.arrayToDataTable(FinancialSummeryGraphArray);
                var options = {
                    curveType: 'function',
                    titleTextStyle: {
                        fontName: 'Arial, sans-serif;',
                        italic: false,
                        bold: false,
                        fontStyle: 'normal',
                        fontSize: 12
                    },
                    bar: {
                        groupWidth: "50%"
                    },
                    legend: {
                        position: 'bottom',
                        textStyle: {
                            fontName: 'Arial, sans-serif;',
                            italic: false,
                            bold: false,
                            fontSize: 14,
                            fontStyle: 'normal'
                        }
                    },
                    hAxis: {
                        format: "#",
                        titleTextStyle: {
                            fontName: "Arial, sans-serif;",
                            italic: false,
                            bold: false,
                            fontSize: 11,
                            fontStyle: "normal",
                        },
                        textStyle: {
                            fontName: "Arial, sans-serif;",
                            italic: false,
                            bold: false,
                            fontSize: 11,
                            fontStyle: "normal",
                        },
                    },
                    vAxis: {
                        format: "'$'#",
                        title: "",
                        titleTextStyle: {
                            fontName: "Arial, sans-serif;",
                            italic: false,
                            bold: true,
                            fontSize: 13,
                            fontStyle: "normal",
                        },
                        textStyle: {
                            fontName: "Arial, sans-serif;",
                            italic: false,
                            bold: false,
                            fontSize: 11,
                            fontStyle: "normal",
                        },
                    },
                    width: 810,
                    height: 372,
                    legend: {
                        position: 'none',
                        textStyle: {
                            fontSize: 4,
                        }
                    },
                    seriesType: 'bars',
                    series: {
                        0: {
                            color: '#A1CB79'
                        },
                        1: {
                            type: 'line',
                            color: '#000',
                            visibleInLegend: false
                        }
                    }
                };
                var chart = new google.visualization.ComboChart(document.getElementById('financial_summery_graph2'));
                chart.draw(data, options);
            }
        </script>

    </div>
<?php
} ?>

<div class="overlay-roadblock">
    <div class="overlay-roadblock__content" id="overlay_content">
    </div>
</div>
<div id="canvas_container">
    <canvas id="canvas" width="910"></canvas>
</div>
<script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script>
    var base_url = '<?php echo site_url(); ?>';
</script>
<script>
    $(document).ready(function() {
        /**
         $('#page1').show();
         $('#page2').show();
         $('#page3').show();
         $('#page4').show();
         $('#page5').show();
         $('#page6').show();
         return false;
        */
        var veec = '<?php echo (count($financial_summary_data) && $financial_summary_data['type'] === 'VEEC') ? true : false; ?>';

        $('.overlay-roadblock').show();
        $('#overlay_content').html('<div class="loader"></div>Processing Images for Pdf Creation Please Wait.....');

        setTimeout(function() {
            $('#page1').show();
            var box = document.querySelector('#page1');
            var width = box.offsetWidth;
            var height = box.offsetHeight;

            document.querySelector("#canvas").setAttribute('height', height);
            var canvasd = document.getElementById('canvas')

            html2canvas(document.querySelector("#page1"), {
                canvas: canvas,
                scale: 1,
                width: 1200,
                height: height
            }).then(function(canvas) {
                var dataimg = canvas.toDataURL();
                $('#dataImg1').val(dataimg);
            });

        }, 3000);


        setTimeout(function() {
            $('#page1').hide();
            $('#page2').show();

            var box = document.querySelector('#page2');
            var width = box.offsetWidth;
            var height = box.offsetHeight;

            document.querySelector("#canvas").setAttribute('height', height);
            var canvasd = document.getElementById('canvas')

            html2canvas(document.querySelector("#page2"), {
                canvas: canvas,
                scale: 1,
                width: 1200,
                height: height
            }).then(function(canvas) {
                var dataimg = canvas.toDataURL();
                //console.log(dataimg);
                $('#dataImg2').val(dataimg);
            });

        }, 7000);


        setTimeout(function() {
            $('#page1').hide();
            $('#page2').hide();
            $('#page3').show();

            var box = document.querySelector('#page3');
            var width = box.offsetWidth;
            var height = box.offsetHeight;

            document.querySelector("#canvas").setAttribute('height', height);
            var canvasd = document.getElementById('canvas')

            html2canvas(document.querySelector("#page3"), {
                canvas: canvas,
                scale: 1,
                width: 1200,
                height: height
            }).then(function(canvas) {
                var dataimg = canvas.toDataURL();
                $('#dataImg3').val(dataimg);
            });

        }, 12000);


        setTimeout(function() {
            $('#page1').hide();
            $('#page2').hide();
            $('#page3').hide();
            $('#page4').show();

            var box = document.querySelector('#page4');
            var width = box.offsetWidth;
            var height = box.offsetHeight;

            document.querySelector("#canvas").setAttribute('height', height);
            var canvasd = document.getElementById('canvas')

            html2canvas(document.querySelector("#page4"), {
                canvas: canvas,
                scale: 1,
                width: 1200,
                height: height
            }).then(function(canvas) {
                var dataimg = canvas.toDataURL();
                $('#dataImg4').val(dataimg);
            });

        }, 15000);


        setTimeout(function() {
            $('#page1').hide();
            $('#page2').hide();
            $('#page3').hide();
            $('#page4').hide();
            $('#page5').show();

            var box = document.querySelector('#page5');
            var width = box.offsetWidth;
            var height = box.offsetHeight;

            document.querySelector("#canvas").setAttribute('height', height);
            var canvasd = document.getElementById('canvas')

            html2canvas(document.querySelector("#page5"), {
                canvas: canvas,
                scale: 1,
                width: 1200,
                height: height
            }).then(function(canvas) {
                var dataimg = canvas.toDataURL();
                $('#dataImg5').val(dataimg);
            });

        }, 18000);


        if (veec == 'true' || veec == true) {
            setTimeout(function() {
                $('#page1').hide();
                $('#page2').hide();
                $('#page3').hide();
                $('#page4').hide();
                $('#page5').hide();
                $('#page6').show();

                var box = document.querySelector('#page6');
                var width = box.offsetWidth;
                var height = box.offsetHeight;

                document.querySelector("#canvas").setAttribute('height', height);
                var canvasd = document.getElementById('canvas')

                html2canvas(document.querySelector("#page6"), {
                    canvas: canvas,
                    scale: 1,
                    width: 1200,
                    height: height
                }).then(function(canvas) {
                    var dataimg = canvas.toDataURL();
                    $('#dataImg6').val(dataimg);
                });

            }, 22000);

            setTimeout(function() {
                $.ajax({
                    type: 'POST',
                    url: base_url + 'admin/proposal/commercial/save_dataimg_to_png',
                    datatype: 'json',
                    data: $('#chartForm').serialize(),
                    success: function(stat) {
                        $('.overlay-roadblock').show();
                        $('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
                        var puuid = "<?php echo $proposal_data['proposal_uuid']; ?>";
                        window.location.href = base_url + "admin/proposal/commercial/download_proposal_pdf/" + puuid;
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }, 25000);

        } else {
            setTimeout(function() {
                $.ajax({
                    type: 'POST',
                    url: base_url + 'admin/proposal/commercial/save_dataimg_to_png',
                    datatype: 'json',
                    data: $('#chartForm').serialize(),
                    success: function(stat) {
                        $('.overlay-roadblock').show();
                        $('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
                        var puuid = "<?php echo $proposal_data['proposal_uuid']; ?>";
                        window.location.href = base_url + "admin/proposal/commercial/download_proposal_pdf/" + puuid;
                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                    }
                });
            }, 23000);
        }

    });
</script>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td align="center" valign="top">
                <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="910" valign="top"><img src="<?php echo $this->config->item('live_url') ?>job_crm/assets/nomination_form/nsw_cl/page-1-1.jpg" width="910" height="763" alt=""/></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="455" valign="top">
                                <img src="<?php echo $this->config->item('live_url') ?>job_crm/assets/nomination_form/nsw_cl/page-1-2.jpg" alt="" style="width:100%;"/>
                            </td>
                            <td  valign="top">
                                <table width="455" border="0" align="center" cellpadding="3" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="445" style="border: solid 1px #333333">
                                                <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 20px; color: #333333"><u>Schedule 1</u></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" valign="top">
                                                                <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="70" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">Service Fee: $</td>
                                                                            <td width="40" style="border:none;font-family: Arial, sans-serif; font-size: 11px; color: #333333"><input type="text" style="border: 0px;border-bottom: dashed 1px #333333; " name="get_nomination_form_details[service_fee]" /></td>
                                                                            <td width="200" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">+ GST per ESC or Minimum charge $</td>
                                                                            <td width="40" style="border: none; font-family: Arial, sans-serif; font-size: 11px; color: #333333"><input type="text" style="border: 0px;border-bottom: dashed 1px #333333; " name="get_nomination_form_details[gst_min_charge]" /></td>
                                                                            <td width="39" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">+ GST</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 11px; color: #333333">Agreed Service Fee and any specific additional services and charges to be noted here( eg, Reflected Ceiling Plans, Product Approval ):</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="25" colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="190" height="25" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">The Return will be going to</td>
                                                            <td style="font-family: Arial, sans-serif; font-size: 15px; color: #333333; border-bottom: solid 1px #333333"><input type="text" style="border: 0px;width:100%;" name="get_nomination_form_details[return_going_to]" value="Kuga Australia Pty Ltd"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">(If above left blank, the return will be provided to customer)</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="150">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top"><img src="<?php echo $this->config->item('live_url') ?>job_crm/assets/nomination_form/nsw_cl/page-1-3.jpg" width="100%" height="51" alt=""/></td>
        </tr>
    </tbody>
</table>

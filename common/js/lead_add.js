
var lead_add_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? parseInt(options.userid) : '';
    this.user_group = (options.user_group && options.user_group != '') ? options.user_group : '';
    this.is_sales_rep = (options.is_sales_rep && options.is_sales_rep != '') ? options.is_sales_rep : '';
    this.is_lead_allocation_sales_rep = (options.is_lead_allocation_sales_rep && options.is_lead_allocation_sales_rep != '') ? options.is_lead_allocation_sales_rep : '';
    this.cust_location_data = [];
    this.lead_data = {};
    this.exist_lead_data = {};


    
    //self.handle_dropzone();
    setTimeout(function(){
        $('.select2-container').addClass('kg-form_field--orange');
    },100);
    
    $(document).on('click', '#save_customer', function () {
        toastr.remove();
        self.save_customer();
    });
    
    
    $('#confirm_customer_yes').click(function(){
        window.location.href = base_url + 'admin/lead/edit/'+ self.exist_lead_data.uuid;
    });

    if($('#bill_file')){
        $('#bill_file').change(function(){
            var fileInput = document.getElementById('bill_file');
            var file = fileInput.files[0];
            $('.custom-file-label').html(file.name);
        });
    }
};

/** Common Functions **/

lead_add_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_add_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

/** Customer Save and Lead Save Related Functions **/

lead_add_manager.prototype.validate_lead_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('#locationPostCode').val();
    var locationState = $('#locationState').val();
    var locationstreetAddress = $('#locationstreetAddressVal').val();
    var contactMobilePhone = $('#contactMobilePhone').val();
    var bussiness_name = $('#bussiness_name').val();
    var customer_email = $('#contactEmailId').val();

    //Remove invalid
    $('#locationPostCode').removeClass('is-invalid');
    $('#locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');
    $('#contactMobilePhone').removeClass('is-invalid');
    $('#bussiness_name').removeClass('is-invalid');


    if (bussiness_name == '') {
        flag = false;
        $('#bussiness_name').addClass('is-invalid');
    }
    if (contactMobilePhone == '') {
        flag = false;
        $('#contactMobilePhone').addClass('is-invalid');
    }
    if (locationPostCode == '') {
        flag = false;
        $('#locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('#locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }

    return flag;
};

lead_add_manager.prototype.save_customer = function () {
    var self = this;

    var flag = self.validate_lead_data();
    if (!flag) {
        toastr.clear();
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
    }
    if (flag) {
        var customerAddForm = $('#customerAdd').serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/customer/save_customer',
            datatype: 'json',
            data: customerAddForm + '&action=add',
            beforeSend: function () {
                toastr["info"]('Creating Customer into the system....');
                $('#save_customer').attr('disabled','disabled');
                $('#save_customer').children('i').removeClass('hidden');
            },
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                        //Populate some data in lead data obj
                        self.lead_data.cust_id = stat.customer_id;
                        self.lead_data.uuid = self.create_uuid();
                        //Save Lead Details
                        setTimeout(function () {
                            self.save_lead_details();
                        }, 500);
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer").removeAttr("disabled");
                        $('#save_customer').children('i').addClass('hidden');
                        
                        if(stat.hasOwnProperty('lead_data')){
                            var data = stat.lead_data;
                            self.exist_lead_data = data;
                            var html = 'Customer already exists:';
                            html += '<br/><br/><b>Business Name:</b> ' + data.customer_company_name;
                            html += '<br/><br/><b>Address:</b> ' + data.address;
                            html += '<br/><br/><b>Phone:</b> ' + data.customer_contact_no;
                            html += '<br/><br/><b>Email:</b> ' + data.customer_email;
                            
                            if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                                html += '<br/><br/><b>Sales Rep:</b> '+ data.full_name; 
                            }
                            
                            if(data.company_name != '' && data.company_name != 'null' && data.company_name != null){
                                html += '<br/><br/><b>Sales Rep Name:</b> '+ data.company_name;
                            }
                            
                            if(data.full_name != '' && data.full_name != 'null' && data.full_name != null){
                                html += '<br/><br/><b>Sales Rep Contact Number:</b> '+ data.company_contact_no; 
                            }

                            if (data.full_name != '' && data.full_name != 'null' && data.full_name != null) {
                                html += '<br/><br/>Note: Customer has been allocated to ' + data.full_name;
                            }
                            
                            $('#confirm_customer_modal_body').html('');
                            $('#confirm_customer_modal_body').html(html);
                            $('#confirm_customer_modal').modal('show');
                            return false;
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer").removeAttr("disabled");
                    $('#save_customer').children('i').addClass('hidden');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
    }
};

lead_add_manager.prototype.save_lead_details = function () {
    toastr.clear();
    var self = this;

    if (self.lead_data.cust_id == '' || self.lead_data.cust_id == undefined) {
        toastr["error"]('Customer not Exists in the system');
        return false;
    }

    if (self.lead_data.uuid == '') {
        var uuid = self.create_uuid();
        self.lead_data.uuid = uuid;
    }

    var lead_source = (self.user_group == '8') ? 'Make it Cheaper' : '';
    var lead_stage = 1;
    var lead_type =  (self.user_group == '8') ? 1 : '';
    var lead_segment = null;

    var formData = 'lead[uuid]=' + self.lead_data.uuid +
    '&lead[user_id]=' + self.userid +
    '&lead[cust_id]=' + self.lead_data.cust_id +
    '&lead[lead_source]=' + lead_source +
    '&lead[lead_stage]=' + lead_stage +
    '&lead[lead_type]=' + lead_type +
    '&lead[lead_segment]=' + lead_segment +
    '&lead_to_user=' + self.userid;

    if(self.user_group == '8'){
        var lead_industry = document.getElementById('lead_industry').value;
        var lead_segment = document.getElementById('lead_segment').value;
        formData +=  '&lead[lead_industry]=' + lead_industry + '&lead[lead_segment]=' + lead_segment;
    }

    $.ajax({
        url: base_url + 'admin/lead/save_lead_details',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.clear();
            toastr["info"]('Creating Lead into the system....');
        },
        success: function (response) {
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function () {
                    if(self.user_group != '8'){
                        window.location.href = base_url + 'admin/lead/add?deal_ref=' + self.lead_data.uuid;
                    }else{
                        self.lead_data.id = response.id;
                        var editor_data = tinyMCE.get('notes').getContent().trim();
                        if(document.getElementById("bill_file").files.length == 0){
                            if(editor_data != ''){
                                self.save_note_activity();
                            }else{
                                window.location.reload();
                            }
                        }else{
                            self.upload_activity_file();
                        }
                    }
                }, 500);
            } else {
                toastr["error"](stat.status);
                $("#save_customer").removeAttr("disabled");
                $('#save_customer').children('i').addClass('hidden');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#save_customer').children('i').addClass('hidden');
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_add_manager.prototype.upload_activity_file = function () {
    var self = this;

    var fileInput = document.getElementById('bill_file');
    var file = fileInput.files[0];

    var formData = new FormData();
    formData.append('lead_id', self.lead_data.id);
    formData.append('file', file);

    $.ajax({
        url: base_url + "admin/uploader/upload_attachment_files",
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        type: 'post',
        data: formData,
        beforeSend: function () {
            toastr["info"]('Uploading File, Please wait...');
        },
        success: function (stat) {
            //stat = JSON.parse(stat);
            if (stat.success == true) {
                var formData = new FormData();
                formData.append('lead_id', self.lead_data.id);
                formData.append('activity_action', 'Add Activity');
                formData.append('activity[activity_type]', 'Note');
                formData.append('activity[activity_title]', 'Recent Electricity Bill');
                formData.append('activity[activity_note]', 'Recent Electricity Bill File Attachment');
                formData.append('action','save_activity');
                formData.append('activity_attachments[0]',stat.file_name);
                formData.append('activity_attachment_sizes[0]',stat.file_size);
                self.save_activity(formData,function(data){
                    var editor_data = tinyMCE.get('notes').getContent().trim();
                    if (data.success == true) {
                        toastr["success"]('Electricity Bill Uploaded Successfully');
                    } else {
                        toastr["error"](data.status);
                    }
                    if(editor_data != ''){
                        self.save_note_activity();
                    }else{
                        setTimeout(function(){
                           window.location.href= base_url + 'admin/lead/add';
                        },500);
                    }
                });
            } else {
                toastr["error"](data.status);
                setTimeout(function () {
                    window.location.href= base_url + 'admin/lead/add';
                },500);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


lead_add_manager.prototype.save_note_activity = function () {
    var self = this;

    var editor_data = tinyMCE.get('notes').getContent().trim();

    var formData = new FormData();
    formData.append('lead_id', self.lead_data.id);
    formData.append('activity_action', 'Add Activity');
    formData.append('activity[activity_type]', 'Note');
    formData.append('activity[activity_title]', '');
    formData.append('activity[activity_note]', editor_data);
    formData.append('action','save_activity');
    toastr.clear();
    toastr["info"]('Creating Note, Please wait...');
    self.save_activity(formData,function(data){
        if (data.success == true) {
            toastr["success"]('Note Created Successfully.');
        } else {
            toastr["error"](data.status);
        }
        setTimeout(function(){
            window.location.href= base_url + 'admin/lead/add';
        },500);
    });
};


lead_add_manager.prototype.save_activity = function (data,callback) {
    var self = this;
    $.ajax({
        url: base_url + 'admin/activity/save',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        type: 'post',
        data: data,
        success: function (stat) {
            callback(stat);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            callback({'success' : false , 'status' : 'Looks like something went wrong'});
        }
    });
};
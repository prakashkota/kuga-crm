<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
	</style>
</head>

<body style="padding:0; margin:0">
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td>
					<table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
						<tr>
							<td width="500" style="font-size: 30px; font-weight: 700; padding-top: 20px; color:#808185;">
								Energy Saver Nomination Form 
								<br/>
								<span style="font-size:20px;  font-weight: 500;">Home Energy Efficiency Retrofits </span>
							</td>
							<td width="300" style="font-size: 30px;font-weight: 700;padding-top: 20px; text-align: right;">
								<img style="width:180px;" src="https://kugacrm.com.au/job_crm/assets/images/get-logo.jpg" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding: 5px 0 0 0;">
						<tr>
							<td width="100%"><span style="height:3px; background:#14B37D; display:block;"></span></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td style="color:#14B37D; font-size:14px; font-weight: 600;">
											1. Accredited Certificate Provider Details 
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:0 15px;">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="50%" >
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">ACP (compnay)</td>
																<td style="font-size:13px;">Green Energy Trading Pty Ltd</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">ABN</td>
																<td style="font-size:13px;">21 128 476 406</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">RESA accreditation #</td>
																<td style="font-size:13px;">GHGR01082C</td>
															</tr>
														</table>
													</td>
													<td width="50%">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Contact Name</td>
																<td style="font-size:13px;">Danie Lomas</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Contact Email</td>
																<td style="font-size:13px;">ee@greenenergytrading.com.au</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Contact Phone</td>
																<td style="font-size:13px;">03 9805 0700</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:15px 0 0 0;">
									<tr>
										<td style="color:#14B37D; font-size:14px; font-weight: 600;">
											2. Original Energy Saver Details
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:0 15px;">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="50%" >
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Corporate/Entity name</td>
																<td style="font-size:13px;">
																	<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 95%;" value="<?php echo $energy_saver_details->entity_name; ?>" />
																</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Phone number</td>
																<td style="font-size:13px;">
																	<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 95%;" value="<?php echo $energy_saver_details->contact_no; ?>" />
																</td>
															</tr>
														</table>
													</td>
													<td width="50%">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">ABN/ACN</td>
																<td style="font-size:13px;">
																	<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 95%;" value="<?php echo $energy_saver_details->abn; ?>" />
																</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Email</td>
																<td style="font-size:13px;">
																	<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 95%;" value="<?php echo $energy_saver_details->email; ?>" />
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family"  style="padding:15px 0 0 0;">
									<tr>
										<td >
											<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
												<tr>
													<td style="width:50%; color:#14B37D; font-size:14px; font-weight: 600;" >
														3. Implementation Details
													</td>
													<td style="width:50%; color:#14B37D; font-size:14px; font-weight: 600;">
														3.2 Energy saving activities implemented 
													</td>
												</tr>
											</table>
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:0 15px;">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="50%" >
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">3.1 Implementation sites:</td>
																<td>
																	&nbsp;
																</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Address:</td>
																<td>
																	<p style="border: 0px; border-bottom: 1px solid black; width: 95%; font-size:13px;"><?php echo $certificate_provider_details->address; ?></p>
																</td>
															</tr>
														</table>
													</td>
													<td width="50%">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="160px" style="padding:0 0 5px;">&nbsp;</td>
																<td>
																	&nbsp;
																</td>
															</tr>
															<tr>
																<td width="160px" style="padding:0 0 5px; font-size:13px;">Description</td>
																<td>
																	<p style="border: 0px; border-bottom: 1px solid black; width: 95%; font-size:13px;"><?php echo $certificate_provider_details->description; ?></p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:15px 0 0 0;">
									<tr>
										<td style="color:#14B37D; font-size:14px; font-weight: 600;">
											3.3 Past activities implemented at the site(s)
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="60%" style="padding:0 0 5px;">Have you or, to the best of your knowledge, has any other person, previously nominated an energy saver for this activity or actitivities at this site(s)?</td>
													<td width="40%">
														<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '1'){ ?>
															<span><input type="checkbox" checked="" /> Yes</span>
														<?php } else {  ?>
															<span><input type="checkbox" /> Yes</span>
														<?php } ?>
														<br/>
														<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '0'){ ?>
															<span><input type="checkbox" checked="" /> No</span>
														<?php } else {  ?>
															<span><input type="checkbox" /> No</span>
														<?php } ?>
													</td>
												</tr>
												<tr>
													<td  style="padding:0 0 5px; font-size:13px;">If yes, please provide a reason for the new nomination form:</td>
													<td >
														<p style="border: 0px; border-bottom: 1px solid black; width: 95%; font-size:13px;">
															<?php echo $past_activities->reason; ?>
														</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:15px 0 0 0;">
									<tr>
										<td style="color:#14B37D; font-size:14px; font-weight: 600;">
											4. Nomination by the original energy saver 
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td style="padding:0 0 5px;">
														<p>I <input type="text" style="border: 0px; border-bottom: 1px solid black;" value="<?php echo (isset($energy_saver_details->name)) ? $energy_saver_details->name : ''; ?>" />, on behalf of the original energy saver,</p>
														<p>nominate Green Energy Trading Pty Ltd as the energy saver in respect of the activity or activities at the implementation sites listed above. </p>
														<p>For the purpose of creating Energy Savings Certificates in the ESS, this nomination entitles Green Energy Trading Pty Ltd to create and </p>
														<p>own all energy savings in respect of each of the above activities at the implementation sites listed above, once they have been </p>
														<p>completed and energy savings have commenced. </p>
														<p>This nomination takes effect from the signature date below, and remains in force until the date this nomination is revoked by the original 
														energy saver. </p>
													</td>
												</tr>
												<tr>
													<td style="padding:0 0 5px;">
														<p>I hereby declare that:</p>
														<p>▼ I have received the contact details of the Accredited Certificate Provider.  </p>
														<p>▼ Except as set out above, I have not previously nominated an energy saver under the NSW Energy Savings Scheme (ESS) in respect of the activity or activities at the implementation sites listed above and neither, to the best of my knowledge, has any other person. </p>
														<p>▼ The original energy saver agrees to provide information regarding the above activities and provide reasonable access to the 
														implementation sites to a representative of the ESS or a member of the ESS Audit Services Panel, upon request. </p>
														<p>▼ The date recorded below is the date when I signed this form. </p>
														<p>▼ The information provided in this nomination form is accurate and is not misleading by inclusion or omission. </p>
														<p>▼ I am aware that this nomination form contains information that may be provided to the Scheme Administrator and that there are 
														penalties for providing false or misleading information to the Scheme Administrator. </p>
														<p>▼ I am authorised to execute this nomination form on the original energy saver’s behalf. </p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px; border: 2px solid black;  background: lightgray;">
												<tr>
													<td>
														<p>Note:</p>
														<p>▼ Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading information to the Scheme Administrator.</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td>
								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding: 0px 0 0 15px;">
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td style="padding:0 0 5px; font-weight:700;">
														Signed by the original energy saver’s authorised signatory 
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="50%" >
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="90px" style="padding:0 0 5px; font-size:13px;">Signature</td>
																<td>
																	<img style="width:200px; height:50px;" src="<?php echo site_url('assets/uploads/led_booking_form_files/'.$authorised_details->signature); ?>" width="100px" />
																</td>
															</tr>
														</table>
													</td>
													<td width="50%">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" class="font-family">
															<tr>
																<td width="50px" style="padding:0 0 5px; font-size:13px;">Name</td>
																<td>
																	<p style="border: 0px; border-bottom: 1px solid black; width: 95%; margin:0px; font-size:13px;">
																		<?php echo $authorised_details->name;?>
																	</p>
																</td>
																<td width="50px" style="padding:0 0 5px; font-size:13px;">Position</td>
																<td>
																	<p style="border: 0px; border-bottom: 1px solid black; width: 95%; margin:0px; font-size:13px;">
																		<?php echo $authorised_details->position;?>
																	</p>
																</td>
															</tr>
															<tr>
																<td width="50px" style="padding:0 0 5px; font-size:13px;">Date</td>
																<td>
																	<p style="border: 0px; border-bottom: 1px solid black; width: 95%; margin:0px; font-size:13px;">
																		<?php echo $authorised_details->date;?>
																	</p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td style="padding:5px 0 5px;">
														*This form must be signed by the authorised signatory on or before the completion of the installation 
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--<tr>
				<td>
					<table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background:#6e6c6c;">
						<tr>
							<td align="right" style="font-size:12px; color:#fff; padding:5px 15px;">© Green Energy Trading Pty Ltd 2018 </td>
						</tr>
					</table>
				</td>
			</tr>-->
		</tbody>
	</table>
</body>
</html>
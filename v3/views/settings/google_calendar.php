<style>
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?>Google Calendar Sync</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="card">
                        <div class="card-header">
                            Connect Google Calendar
                        </div>
                        <div class="card-body">
                            <p class="card-text"> Connecting your Kuga CRM account with Google Calendar helps you keep track of what's going on, making activity and customer tracking easy.</p>
                            <a href="#" class="btn btn-primary connect_google_services" data-id="1" data-name="Google Calendar">Connect Google Calendar</a>
                            <a href="#" class="btn btn-primary disconnect_google_services hidden" data-id="1" data-name="Google Calendar">Disconnect Google Calendar</a>
                            <div class="connect_container text-center" ></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src='<?php echo site_url(); ?>assets/js/google_services.js?v=<?php echo version; ?>' type='text/javascript'></script>

<script type='text/javascript'>
    var context = {};
    context.is_google_account_connected = '<?php echo $is_google_account_connected; ?>'
    var google_services_tool = new google_service_manager(context);
</script>
<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;} 
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } else { ?> Franchise - <?php } ?>Add Company Markup</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <form role="form"  action="" class="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Item Details</h4>

                                    <div class="form-group">
                                        <label>State</label>
                                        <select class="form-control" name="state" required>
                                            <option value="All">All</option>
                                            <?php foreach ($states as $key => $value) { ?>
                                                <option value="<?php echo $value['state_postal']; ?>" <?php if (isset($state) && $state == $value['state_postal']) { ?> selected="selected" <?php } ?>><?php echo $value['state_postal']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('state', '<div class="text-left text-danger">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <textarea type="text" name="comments"  class="form-control"   placeholder="Comments"><?php echo isset($comments) ? $comments : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="buttwrap">
                                <input type="submit"  class="btn" value="SAVE">
                            </div>    
                        </div>
                        <div class="col-md-6" >
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="form-section">Pricing Details</h4>
                                    <div class="form-block d-block clearfix">
                                        <div class="table-default">
                                            <table width="100%" border="0">
                                                <thead>
                                                    <tr>
                                                        <td>Qty From</td>
                                                        <td>Qty To</td>
                                                        <td>Price</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="company_markups_wrapper">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button id="add_company_markups" type="button" class="btn btn-primary" >Add Company Markup </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var company_markup_data = function (options) {
        var self = this;
        this.id = (options.id && options.id != '') ? options.id : '';
        this.company_markups_wrapper = 'company_markups_wrapper'
        this.company_markups_btn = 'add_company_markups';

        if (options.id && options.id != '') {
            self.load_item_data();
        }

        $('form button').on("click", function (e) {
            e.preventDefault();
        });

        self.create_company_markups();
        self.remove_company_markups();
    };

    company_markup_data.prototype.load_item_data = function () {
        var self = this;
        var company_markup_id = self.id;
        var form_data = 'company_markup_id=' + company_markup_id;
        $.ajax({
            url: base_url + 'admin/company-markup/fetch_company_markup_data',
            type: 'get',
            data: form_data,
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    for (var i = 0; i < response.company_markup_data.length; i++) {
                        self.create_company_markups(response.company_markup_data[i]);
                    }
                } else {

                }
            }
        });
    };

    company_markup_data.prototype.create_company_markups = function (data) {
        var self = this;
        var company_markups_wrapper = document.getElementById(self.company_markups_wrapper);
        var company_markups_btn = document.getElementById(self.company_markups_btn);
        if (!data) {
            company_markups_btn.addEventListener("click", function (e) { //on add input button click
                e.preventDefault();
                $(company_markups_wrapper).append('<tr>'
                        + '<td><input type="number" class="form-control" name="company_markup_data[qty_from][]" value="1" /></td>'
                        + '<td><input type="number" class="form-control" name="company_markup_data[qty_to][]" value="1" /></td>'
                        + '<td><input type="number" class="form-control" name="company_markup_data[price][]" step="0.001" value="1" /></td>'
                        + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            });
        } else {
            $(company_markups_wrapper).append('<tr>'
                    + '<td><input type="number" class="form-control" name="company_markup_data[qty_from][]" value="' + data.qty_from + '" /></td>'
                    + '<td><input type="number" class="form-control" name="company_markup_data[qty_to][]" value="' + data.qty_to + '" /></td>'
                    + '<td><input type="number" class="form-control" name="company_markup_data[price][]" step="0.001" value="' + data.price + '" /></td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
        }
    };

    company_markup_data.prototype.remove_company_markups = function () {
        var self = this;
        var company_markups_wrapper = document.getElementById(self.company_markups_wrapper);
        $(company_markups_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
            e.preventDefault();
            $(this).parent().parent('tr').remove();
        });
    };

    company_markup_data.prototype.create_select_field = function (key, data, current_value) {
        data = JSON.parse(data);
        var select = '<select name="company_markup_data[' + key + '][]" class="form-control ' + key + '">';
        var sel1 = (current_value && current_value == 'All') ? 'selected="selected"' : '';
        select += '<option value="All" ' + sel1 + '>All</option>';
        for (var i = 0; i < data.length; i++) {
            var value = data[i].state_postal;
            var sel = (current_value && current_value == value) ? 'selected="selected"' : '';
            select += '<option value="' + value + '" ' + sel + '>' + value + '</option>';
        }
        return select;
    };

    var context = {};
    context.id = "<?php echo (isset($lid) && $lid != '') ? $lid : '' ?>";
    new company_markup_data(context);
</script>
</body>

</html>


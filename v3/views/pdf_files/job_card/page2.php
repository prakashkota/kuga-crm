<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Job Card</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            
            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=checkbox]:checked:before {
                color: black !important;
            }
        
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        
        </style>
    </head>
<body style="padding:0; margin:0">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td style="position:relative;top: -18px;"><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:15px"><strong>Panel Layout Photo (Pointing MSB/ Transformer/ Inverter)</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->panel_layout) && $job_card_details->panel_layout != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->panel_layout; ?>"  height="290" width="800"/>
                                    <?php } else { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form_files/no_image.jpg'; ?>" width="800" height="290" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height:50px;">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Calibri, sans-serif; font-size:15px"><strong>Screenshot of KUGA Quoting Tool</strong></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse:collapse; border:solid 1px #e8e8e8; height: 290px; padding:5px 0;">
                                    <?php if (isset($job_card_details->quoting_tool_screenshot) && $job_card_details->quoting_tool_screenshot != '') { ?>
                                        <img src="<?php echo $this->config->item('live_url') . 'assets/uploads/job_card_form_files/' . $job_card_details->quoting_tool_screenshot; ?>" height="290" width="800"/>
                                    <?php }?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="235" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td height="30" valign="bottom" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td valign="bottom" style="position:relative;bottom: -18px;"><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
    
</table>
</body>
</html>
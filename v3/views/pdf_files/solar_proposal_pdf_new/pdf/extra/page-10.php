<html>

<head>
	<title></title>
	<style>
	body {
		font-family: arial;
		b
	}
	
	.page10 .back-bg {
		background-color: #fff;
		background-repeat: no-repeat;
		margin: auto;
		padding: 0;
		background-size: 100% 100%;
		max-width: 910px;
		max-height: 1287px;
	}
	
	.page10 .top h2 {
		font-size: 16px;
		color: #221E1F;
		padding-left: 654px;
		padding-top: 30px;
	}
	
	.page10 .top hr {
		width: 65%;
		height: 1px;
		margin: -24px 0px 0px 52px;
		background-color: #D01E2A;
	}
	
	.page10 .text_color {
		color: #D01E2A;
	}
	
	.page10 .title {
		display: inline-flex;
		margin-top: -50px;
		margin-left: 70px;
		width: 100%;
	}
	
	.page10 .title h1 {
		padding-left: 14px;
		margin-top: 90px;
		border-left: 5px solid #D01E2A;
	}
	
	.page10 .solorproduction {
		width: 50%;
	}
	
	.page10 .solorproduction h2 {
		padding: 10px;
		margin-top: 88px;
		font-size: 16px;
		background-color: #D01E2A;
		width: 100%;
		color: #ffffff;
		background: linear-gradient( 105deg, #D01E2A 87%, transparent 15%);
		letter-spacing: 2px;
	}
	
	.page10 .solorproductionavg {
		width: 60%;
		margin-right: 200px;
	}
	
	.page10 .solorproductionavg h2 {
		padding: 10px;
		margin-top: 88px;
		font-size: 16px;
		width: 100%;
		color: #373536;
		border: 2px solid #d01e2a;
		background: linear-gradient( 300deg, #ffffff 87%, transparent 15%);
	}
	
	.page10 table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 80%;
		margin-left: 46px;
	}
	
	.page10 tr {
		border-bottom: 2px solid #D01E2A;
	}
	
	.page10 td {
		padding: 15px;
		color: #3D3B3C;
	}
	
	.page10 .footer {
		background: #EDEBEC;
		padding: 32px;
		margin-top: -8px;
	}
	
	.page10 .sectio-image img {
		margin-top: 25px;
		margin-left: 85px;
	}
	
	.page10 .fimagee img {
		margin-left: 660px;
		width: 207px;
		margin-top: 5px;
	}
	
	.page10 .sub_titlee h6 {
		font-size: 17px;
		margin-left: 48px;
	}
	
	.page10 .firstBoxs {
		display: inline-flex;
		width: 85%;
		margin: 40px 70px 0;
	}
	
	.page10 .sub_titlee {
		margin-right: 78px;
	}
	
	.page10 .sub_titlee1 {
		margin-right: 30px;
	}
	
	.page10 .sub_titlee1 p {
		font-size: 18px;
		color: #D01E2A;
		font-weight: bold;
		text-align: center;
	}
	
	.page10 .sub_titlee p {
		font-size: 18px;
		color: #D01E2A;
		font-weight: bold;
		text-align: center;
	}
	
	.page10 .box {
		background-color: #dddddd;
		padding: 25px;
		padding-left: 180px;
	}
	
	.page10 .box1 {
		border: 2px solid #D01E2A;
		padding: 25px;
		padding-left: 208px;
	}
	
	.page10 .box2 {
		border: 2px solid #F2B1B5;
		width: 234px;
		height: 60px;
	}
	
	.page10 .tittle {
		display: inline-flex;
		margin-top: -50px;
		margin-left: 20px;
	}
	
	.page10 .solorproductionn {
		margin-right: 30px;
		margin-bottom: 30px;
		margin-top: 30px;
	}
	
	.page10 .solorproductionn h2 {
		font-size: 16px;
		color: #241E20;
		text-align: center;
	}
	
	.page10 .f20 p {
		font-size: 100px;
		margin: 0;
		margin-left: 19px;
		color: #D01E2A;
		margin-right: 42px;
	}
	
	.page10 .f20 {
		border-right: 2px solid #F2B1B5;
		display: flex;
		align-items: center;
	}
	
	.page10 .f20image {
		border-right: 2px solid #F2B1B5;
	}
	
	.page10 .f20image img {
		margin-left: 42px;
		margin-right: 42px;
	}
	
	.page10 .f20imagee img {
		margin-left: 42px;
		margin-right: 42px;
	}
	
	.page10 .f20imagee p {
		text-align: center;
		font-size: 50px;
		margin: 0;
		font-weight: bold;
	}
	</style>
</head>

<body>
	<div class="back-bg page10">
		<div class="top">
			<h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
				<hr>
			</div>
			<div class="title">
				<h1>SYSTEM <span class="text_color"> PERFORMANCE</span></h1>
			</div>
			<div class="title">
				<div class="solorproduction">
					<h2><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/solar-panel.png'); ?>" width="18px"> &nbsp &nbsp SOLAR PRODUCTION</h2> </div>
		<div class="solorproductionavg">
			<h2>SOLAR PRODUCTION (AVG MONTHLY)</h2> </div>
	</div>
	<div class="sectio-image"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/0010.jpg'); ?>" width="650px"> </div>
	<div class="firstBoxs">
		<div class="sub_titlee">
			<div class="box"></div>
			<p>Bill Before Solar</p>
		</div>
		<div class="sub_titlee">
			<div class="box"></div>
			<p>Bill After Solar</p>
		</div>
		<div class="sub_titlee">
			<div class="box"></div>
			<p>%of Electricity
				<br> Bill Saving</p>
		</div>
	</div>
	<div class="firstBoxs">
		<div class="sub_titlee1">
			<div class="box1"></div>
			<p>Solar Production
				<br>per year</p>
		</div>
		<div class="sub_titlee1">
			<div class="box1"></div>
			<p>Offset
				<br>Consumption</p>
		</div>
		<div class="sub_titlee1">
			<div class="box1"></div>
			<p>Export
				<br> To Grid</p>
		</div>
	</div>
	<div class="firstBoxs">
		<div class="solorproductionn">
			<div class="box2">
				<h2>% OF ENERGY PROVIDED <br> BY SOLAR</h2> </div>
		</div>
		<div class="solorproductionn">
			<div class="box2">
				<h2>% OF BILL REDUCTION</h2> </div>
		</div>
		<div class="solorproductionn">
			<div class="box2">
				<h2>TONS OF CO2 <br>REDUCTION ANNUALLY</h2> </div>
		</div>
	</div>
	<div class="" style=" display: inline-flex; margin-left: 63px;">
		<div class="f20">
			<p>20%</p>
		</div>
		<div class="f20image"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/00100.jpg'); ?>" width="160px"> </div>
		<div class="f20imagee"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/00102.jpg'); ?>" width="160px">
			<p>3,998 KG</p>
		</div>
	</div>
	<div class="fimagee" style="margin-bottom: -60px;"> <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo.png'); ?>" width="350px"></a> </div>
	<div class="footer"></div>
	</div>
</body>

</html>
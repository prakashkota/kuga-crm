<?php

$FinancialType = "";
$TProjectCost1 = "";
$TProjectCost2 = "";
$TVeecDiscount = "";
$ppa_year = 0;
$financial_summary_veec_type = [1, 2];

if (isset($financial_summary_data['type']) && $financial_summary_data['type'] == 'VEEC') {

    if (isset($financial_summary_data['veec_type'])) {
        $financial_summary_veec_type = explode(',', $financial_summary_data['veec_type']);
    }

    $FinancialType = 'VEEC';

    $TVeecDiscount = number_format((float) ($financial_summary_data['veec_discount']), 0, '', ',');


    if (!empty($financial_summary_data['project_cost'])) {
        $TProjectCost = (array) json_decode($financial_summary_data['project_cost']);
        $TProjectCost1 = number_format((float) ($TProjectCost[0]), 0, '', ',');
        $TProjectCost2 = number_format((float) ($TProjectCost[1]), 0, '', ',');
    }


    if (!empty($financial_summary_data['monthly_payment_plan'])) {
        $TMonthlyPaymentPlan = (array) json_decode($financial_summary_data['monthly_payment_plan']);
        $MonthlyPayment1 = number_format((float) ($TMonthlyPaymentPlan[0]), 0, '', ',');
        $MonthlyPayment2 = number_format((float) ($TMonthlyPaymentPlan[1]), 0, '', ',');
    }

    if (!empty($financial_summary_data['ppa_rate'])) {
        $TPPARate = (array) json_decode($financial_summary_data['ppa_rate']);
        $TPPARate1 = $TPPARate[0];
        $TPPARate2 = $TPPARate[1];
    }
    if (!empty($financial_summary_data['ppa_checkbox'])) {
        $ppa_checkbox = (array) json_decode($financial_summary_data['ppa_checkbox']);
        $ppa_checkbox1 = $ppa_checkbox[0];
        $ppa_checkbox2 = $ppa_checkbox[1];
    }
    if (!empty($financial_summary_data['ppa_term'])) {
        $TPPATermValue = (array) json_decode($financial_summary_data['ppa_term']);
        $TPPATermValue1 = getTPPATerm($TPPATermValue[0]);
        $TPPATermValue2 = getTPPATerm($TPPATermValue[1]);
    }


    if (!empty($financial_summary_data['term'])) {
        $TTermValue = (array) json_decode($financial_summary_data['term']);
        $TTermValue1 = getTPPATerm($TTermValue[0]);
        $TTermValue2 = getTPPATerm($TTermValue[1]);
    }

    $monthlyPaymentPlanSolar = 0;
    if (isset($MonthlyPayment)) {
        $monthlyPaymentPlanSolar = $TMonthlyPaymentPlan['2'] * 12;
    }
    $AnnualCashflow = number_format((float) ($monthlyPaymentPlanSolar), 0, '', ',');
} else {
    if (!empty($financial_summary_data['ppa_term'])) {
        $ppa_year = $financial_summary_data['ppa_term'];
        $ppa_year = getTPPATerm($ppa_year);
    }
}

function getTPPATerm($TPPATerm)
{
    $ReturnTerm = $TPPATerm / 12;
    return $ReturnTerm;
}

$Tilldate = new DateTime($proposal_data['created_at']); // Y-m-d
$Tilldate->add(new DateInterval('P30D'));
?>

<html>

<head>
    <title></title>
    <style>
        .page16 .back-bg {
            background-color: white;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page16 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 630px;
            padding-top: 30px;
        }

        .page16 .top hr {
            width: 70%;
            height: 2px;
            margin: -24px 0px 0px;
            background-color: #D01E2A;
        }

        .page16 .text_color {
            color: #D01E2A;
        }

        .page16 .vkcontainer {
            margin: 0 30px;
        }

        .page16 .title h1 {
            padding-left: 14px;
            margin-top: 40px;
            border-left: 5px solid #D01E2A;
        }

        .page16 .vktitle h2 {
            font-weight: 500;
            letter-spacing: 3px;
            padding: 12px;
            margin-top: 15px;
            background-color: #D01E2A;
            width: 100%;
            color: #ffffff;
            background: linear-gradient(125deg, #D01E2A 87%, transparent 15%);
        }

        .page16 table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .page16 tr {
            border-bottom: 1px solid #cac8c8;
        }

        .page16 td {
            padding: 15px;
            color: #3D3B3C;
        }

        .page16 .footer {
            display: inline-flex;
        }

        .page16 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page16 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page16 .image img {
            margin-top: -50px;
        }

        .page16 .fimagee img {
            margin-top: 15px;
            margin-left: 730px;
        }

        .page16 .vk-innerbox {
            width: 33%;
            float: left;
            text-align: center;
        }

        .page16 .vk-innerbox h4 {
            margin: 0px;
        }

        .page16 .vkid-section {
            padding-top: 40px;
            margin-bottom: 60px;
        }

        .page16 .vktitle h2 span {
            background: #fff;
            padding: 2px 4px;
            margin-right: 15px;
            color: #fff;
            font-size: 12px;
        }

        .page16 .headertable tr {
            border: none;
            text-align: center;
        }

        .page16 .headertable td {
            width: 33.33%;
        }

        .page16 table.headertable {
            margin-top: 75px;
        }

        .page16 .tablefooter tr {
            border: none;
        }

        .page16 .tablefooter tr th {
            text-align: left;
            padding-left: 20px;
            color: #D01E2A;
        }

        .page16 .tablefooter td {
            width: 50%;
            padding-left: 20px;
        }

        .page16 .comfooter {
            border-top: 1px dashed #D01E2A;
            border-bottom: 1px dashed #D01E2A;
            padding: 30px 0;
            margin-top: 70px;
        }
    </style>
</head>

<body>
    <div class="back-bg page16">
        <div class="vkcontainer">
            <div class="top vk-header" style="width:100%;">
                <div class="headertext">
                    <h2><span class="text_color">SOLAR PROPOSAL</span> | KUGA</h2>
                </div>
                <hr>
            </div>
            <div class="title">
                <h1 style="text-transform:uppercase;">Project <span class="text_color"> Costs</span></h1>
            </div>
            <div class="vkheader-top">
                <table class="headertable" style="margin-top: 25px;">
                    <tr>
                        <th style="border-right: 2px solid #D01E2A;">Quote ID</th>
                        <th style="border-right: 2px solid #D01E2A;">Date</td>
                        <th>Authorised Person</td>
                    </tr>
                    <tr>
                        <td style="border-right: 2px solid #D01E2A;"><?php echo $proposal_data['id']; ?></td>
                        <td style="border-right: 2px solid #D01E2A;"><?php echo date_format(date_create($proposal_data['created_at']), "l, jS F Y"); ?></td>
                        <td><?php echo  $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?></td>
                    </tr>
                </table>
            </div>


            <?php if ($FinancialType != 'VEEC') { ?>
                <div class="vktitle" style="padding-top:20px;">
                    <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 1</b> CAPEX</h2>
                </div>
                <div>
                    <table style="background:#EDEBEC;">
                        <tr>
                            <td>Deposit payment (30%)</td>
                            <td style="text-align: right;">$<?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 30 / 100)), 0, '', ','); ?></td>
                        </tr>
                        <tr>
                            <td>on Project completion (70%)</td>
                            <td style="text-align: right;">$
                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 70 / 100)), 0, '', ','); ?></td>
                        </tr>
                        <!-- <tr>
                            <td>On job completion (10%)</td>
                            <td style="text-align: right;">$
                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 10 / 100)), 0, '', ','); ?></td>
                        </tr> -->
                        <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                            <td><b>Total Payable</b> (excl. GST)</td>
                            <td style="text-align: right;"><strong>$<?php echo number_format((float) ($proposal_data['price_before_stc_rebate']), 0, '', ','); ?></strong></td>
                        </tr>
                    </table>
                </div>
                <!-- option 2 -->
                <?php if (count($proposal_finance_data)) { ?>
                    <div class="vktitle">
                        <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> OPTION 2 </b>FINANCE</h2>
                    </div>
                    <div style="padding-bottom: px;">
                        <table style="background:#EDEBEC;">
                            <tr>
                                <td>Term (years)</td>
                                <td style="text-align: right;"><?php echo $year; ?></td>
                            </tr>
                            <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                                <td><b>Monthly payment plan</b></td>
                                <td style="text-align: right;"><strong>$<?php echo number_format((float) ($proposal_finance_data['monthly_payment_plan']), 0, '', ','); ?></strong></td>
                            </tr>
                        </table>
                    </div>

                    <?php if ($financial_summary_data['ppa_checkbox'] == 1) { ?>
                        <div class="vktitle">
                            <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> OPTION 3 </b>POWER PURCHASE AGREEMENT (PPA)</h2>
                        </div>
                        <div style="padding-bottom: 0px;">
                            <table style="background:#EDEBEC;">
                                <tr>
                                    <td>Term (years)</td>
                                    <td style="text-align: right;"><?php echo $ppa_year; ?></td>
                                </tr>
                                <tr style="border-top: 2px solid #333;  border-bottom: 3px solid #cac8c8;">
                                    <td><b>PPA Rate</b> (¢/kWh)</td>
                                    <td style="text-align: right;"><strong><?php echo $financial_summary_data['ppa_rate']; ?></strong></td>
                                </tr>
                            </table>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>

                <?php if (in_array(1, $financial_summary_veec_type)) { ?>
                    <div class="vktitle" style="padding-top:20px;">
                        <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 1</b> CAPEX</h2>
                    </div>
                    <div>
                        <table style="background:#EDEBEC;">
                            <tr>
                                <td>CAPEX</td>
                                <td>FINANCE</td>
                                <?php if($ppa_checkbox1==1){?>
                                <td>PPA</td>
                        <?php } ?>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Term</b>: <?php echo $TTermValue1; ?> Years</td>
                    <?php if($ppa_checkbox1==1){?>
                                <td><b>Term</b>: <?php echo $TPPATermValue1; ?> Years</td>
                        <?php } ?>
                            </tr>
                            <tr>
                                <td><b>Total Payable</b> (excl. GST): <strong>$<?php echo $TProjectCost1; ?></strong></td>
                                <td><b>Monthly Repayments</b>: $<?php echo $MonthlyPayment1; ?></td>
                    <?php if($ppa_checkbox1==1){?>
                                <td><b>¢/kWh</b>: ¢<?php echo $TPPARate1; ?></td>
                        <?php } ?>
                            </tr>
                        </table>
                    </div>
                <?php } ?>

                <?php if (in_array(2, $financial_summary_veec_type)) { ?>
                    <div class="vktitle" style="padding-top:20px;">
                        <h2 style="text-transform:uppercase; font-size:18px;"><span>0</span><b> option 2</b> VEEC CLAIM</h2>
                    </div>
                    <div>
                        <table style="background:#EDEBEC;">
                            <tr>
                                <td>CAPEX</td>
                                <td>FINANCE</td>
                    <?php if($ppa_checkbox2==1){?>
                                <td>PPA</td>
                        <?php } ?>
                            </tr>
                            <tr>
                                <td><b>Project Cost</b>: $<?php echo $TProjectCost2; ?></td>
                                <td></td>
                    <?php if($ppa_checkbox2==1){?>
                                <td></td>
                        <?php } ?>
                            </tr>
                            <tr>
                                <td><b>VEEC Discount</b>: $<?php echo $TVeecDiscount; ?></td>
                                <td><b>Term</b>: <?php echo $TTermValue2; ?> Years</td>
                    <?php if($ppa_checkbox2==1){?>
                                <td><b>Term</b>: <?php echo $TPPATermValue2; ?> Years</td>
                        <?php } ?>
                            </tr>
                            <tr>
                                <td><b>Total Payable</b> (excl. GST): <strong>$<?php echo $TProjectCost2; ?></strong></td>
                                <td><b>Monthly Repayments</b>: $<?php echo $MonthlyPayment2; ?></td>
                    <?php if($ppa_checkbox2==1){?>
                                <td><b>¢/kWh</b>: ¢<?php echo $TPPARate2; ?></td>
                        <?php } ?>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="vaildquote" style="margin-bottom:50px;">
                <h4 style="color:#D01E2A;">This quote is valid till <?php echo $Tilldate->format('d/m/Y'); ?></h4>
                <div class="comfooter" style="margin-top: 10px;">
                    <table class="tablefooter" style="width:100%;">
                        <tr>
                            <th style="border-right: 1px dashed #D01E2A;">Company Name:</th>
                            <th style="padding-left:50px;">Company Address:</td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px dashed #D01E2A;"><?php echo $lead_data['customer_company_name']; ?></td>
                            <td style="padding-left:50px;"><?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="fimagee">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
</body>

</html>
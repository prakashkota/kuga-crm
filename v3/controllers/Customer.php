<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Customer Controller
 * @version 1.0
 */
class Customer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("Components");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function save_customer() {
        $data = array();
        if (!empty($this->input->post())) {
            $cust_id = $this->input->post('cust_id');
            if (isset($cust_id) && $cust_id != '') {
                //Update Customer
                $customer_update_data = $this->input->post('company');
                //Escape and Sanitize data before update
                $customer_update_data = $this->components->escape_data($customer_update_data);
                $stat = $this->common->update_data('tbl_customers', ['id' => $cust_id], $customer_update_data);
            } else {
                //Insert Customer
                $customer_insert_data = $this->input->post('company');
                //Check Customer Validation Before Saving:
                $validation_data = array();
                $validation_data['company_name'] = isset($customer_insert_data['company_name']) ? $customer_insert_data['company_name'] : '';
                $is_customer_exists = $this->is_customer_exists($validation_data);

                if ($is_customer_exists != '') {
                    $data['success'] = FALSE;
                    $data['status'] = 'Customer already exists in the system. (' . $is_customer_exists . ')';
                    $data['lead_data'] = $this->lead->get_leads(FALSE,FALSE,FALSE,$validation_data['company_name']);
                    echo json_encode($data);
                    die;
                }
                $customer_insert_data['company_name'] = (isset($customer_insert_data['company_name']) && $customer_insert_data['company_name'] != '') ? $customer_insert_data['company_name'] : $customer_insert_data['first_name'] . ' ' . $customer_insert_data['last_name'];
                //Escape and Sanitize data before insert
                $customer_insert_data = $this->components->escape_data($customer_insert_data);
                $stat = $this->common->insert_data('tbl_customers', $customer_insert_data);
                $cust_id = $this->common->insert_id();

                //Insert Customer Site
                $site_insert_data = $this->input->post('site');
                if(isset($site_insert_data) && !empty($site_insert_data)){
                    $site_insert_data['cust_id'] = $cust_id;
                    $site_insert_data['created_at'] = date('Y-m-d H:i:s');
                    $site_insert_data['site_name'] = (isset($site_insert_data['site_name']) && $site_insert_data['site_name'] != '') ? $site_insert_data['site_name'] : $site_insert_data['address'];
                    //Escape and Sanitize data before insert
                    $site_insert_data = $this->components->escape_data($site_insert_data);
                    $stat = $this->common->insert_data('tbl_customer_locations', $site_insert_data);
                    $site_id = $this->common->insert_id();
                }
            }
            if ($stat) {
                if(CACHING){
                    //Delete Cached Leads
                    $this->db->cache_delete('admin','lead');
                }
                $data['success'] = TRUE;
                $data['status'] = 'Customer Saved Successfully.';
                $data['customer_id'] = $cust_id;
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function save_customer_location() {
        $data = array();
        $cust_id = $this->input->post('cust_id');
        if (!empty($this->input->post()) && isset($cust_id) && $cust_id != '') {
            $site_id = $this->input->post('site_id');
            if (isset($site_id) && $site_id != '') {
                //Update Customer Site
                $site_update_data = $this->input->post('site');
                $is_same_address_as_prev = $this->common->num_rows('tbl_customer_locations', array('id' => $site_id, 'address' => $site_update_data['address']));
                if ($is_same_address_as_prev == 0) {
                    $validation_data = array();
                    $validation_data['address'] = isset($site_update_data['address']) ? $site_update_data['address'] : '';
                    //$is_customer_exists = $this->is_customer_contact_exists($validation_data);
                    $is_customer_exists = '';
                    if ($is_customer_exists != '') {
                        $data['success'] = FALSE;
                        $data['status'] = 'Customer already exists in the system. (' . $is_customer_exists . ')';
                        echo json_encode($data);
                        die;
                    }
                }
                //Escape and Sanitize data before update
                $site_update_data = $this->components->escape_data($site_update_data);
                $stat = $this->common->update_data('tbl_customer_locations', ['cust_id' => $cust_id, 'id' => $site_id], $site_update_data);
            } else {
                //Insert Customer Site
                $site_insert_data = $this->input->post('site');

                $validation_data = array();
                $validation_data['address'] = isset($site_insert_data['address']) ? $site_insert_data['address'] : '';
                //$is_customer_exists = $this->is_customer_contact_exists($validation_data);
                $is_customer_exists = '';

                if ($is_customer_exists != '') {
                    $data['success'] = FALSE;
                    $data['status'] = 'Customer already exists in the system. (' . $is_customer_exists . ')';
                    echo json_encode($data);
                    die;
                }

                $site_insert_data['cust_id'] = $cust_id;
                $site_insert_data['created_at'] = date('Y-m-d H:i:s');
                $site_insert_data['site_name'] = (isset($site_insert_data['site_name']) && $site_insert_data['site_name'] != '') ? $site_insert_data['site_name'] : $site_insert_data['address'];
                
                //Escape and Sanitize data before insert
                $site_insert_data = $this->components->escape_data($site_insert_data);
                $stat = $this->common->insert_data('tbl_customer_locations', $site_insert_data);
                $site_id = $this->common->insert_id();
            }
            if ($stat) {
                if(CACHING){
                    //Delete Cached Leads
                    $this->db->cache_delete('admin','lead');
                }
                $data['success'] = TRUE;
                $data['status'] = 'Customer Location Saved Successfully.';
                $data['site_id'] = $site_id;
                $data['location'] = $this->customer->get_customer_location($site_id, FALSE, FALSE);
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function delete_customer_location() {
        $data = array();
        $cust_id = $this->input->post('cust_id');
        $site_id = $this->input->post('site_id');
        $lead_id = $this->input->post('lead_id');
        if (isset($cust_id) && $cust_id != '' && isset($site_id) && $site_id != '' && isset($lead_id) && $lead_id != '') {
            //Start Trancsaction
            $this->db->trans_begin();

            //Delete Customer Site, Led Proposal and Solar Proposal Data
            $this->customer->delete_led_proposal_data($lead_id, $site_id);
            $this->common->delete_data('tbl_customer_locations', array('id' => $site_id, 'cust_id' => $cust_id));

            //Check Transaction status and take necessary action
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $stat = FALSE;
            } else {
                $this->db->trans_commit();
                $stat = TRUE;
            }
            if ($stat) {
                $data['success'] = TRUE;
                $data['status'] = 'Customer Location Deleted Successfully.';
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function fetch_customer_location() {
        if (!empty($this->input->get())) {
            $cust_id = $this->input->get('cust_id');
            $customer = $this->customer->get_customer_location(FALSE, $cust_id, FALSE);
            $data['success'] = TRUE;
            $data['location'] = $customer;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function save_customer_contact() {
        $data = array();
        if (!empty($this->input->post())) {
            $cust_contact_id = $this->input->post('cust_contact_id');
            if (isset($cust_contact_id) && $cust_contact_id != '') {
                //Update Customer
                $customer_update_data = $this->input->post('company');
                $stat = $this->common->update_data('tbl_customer_contacts', ['id' => $cust_contact_id], $customer_update_data);
            } else {
                //Insert Customer
                $customer_insert_data = $this->input->post('company');
                //Check Customer Validation Before Saving in Customer Table:
                $validation_data = array();
                //$validation_data['email'] = isset($customer_insert_data['customer_email']) ? $customer_insert_data['customer_email'] : '';
                //$validation_data['contact_no'] = isset($customer_insert_data['customer_contact_no']) ? $customer_insert_data['customer_contact_no'] : '';
                //$validation_data['address'] = isset($site_insert_data['address']) ? $site_insert_data['address'] : '';
                $validation_data['company_name'] = isset($customer_insert_data['company_name']) ? $customer_insert_data['company_name'] : '';
                $is_customer_exists = $this->is_customer_contact_exists($validation_data);

                if ($is_customer_exists != '') {
                    $data['success'] = FALSE;
                    $data['status'] = 'Customer contact already exists in the system. (' . $is_customer_exists . ')';
                    echo json_encode($data);
                    die;
                }

                $customer_insert_data['company_name'] = (isset($customer_insert_data['company_name']) && $customer_insert_data['company_name'] != '') ? $customer_insert_data['company_name'] : $customer_insert_data['first_name'] . ' ' . $customer_insert_data['last_name'];
                $stat = $this->common->insert_data('tbl_customer_contacts', $customer_insert_data);
                $cust_contact_id = $this->common->insert_id();
            }
            if ($stat) {
                $data['success'] = TRUE;
                $data['status'] = 'Customer Contact Saved Successfully.';
                $data['cust_contact_id'] = $cust_contact_id;
                $data['contact'] = $this->customer->get_customer_contact($cust_contact_id, FALSE, FALSE);
                ;
            } else {
                $data['success'] = FALSE;
                $data['status'] = 'Looks like something went wrong.';
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function fetch_customer_contacts() {
        if (!empty($this->input->get())) {
            $cust_id = $this->input->get('cust_id');
            $customer = $this->customer->get_customer_contact(FALSE, $cust_id, FALSE);
            $data['success'] = TRUE;
            $data['contact'] = $customer;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    public function fetch_customer_and_contacts() {
        if (!empty($this->input->get())) {
            $cust_id = $this->input->get('cust_id');
            $customer = $this->customer->get_customers($cust_id);
            $customer_contacts = $this->customer->get_customer_contact(FALSE, $cust_id);
            $data['success'] = TRUE;
            $data['contact'] = array_merge($customer, $customer_contacts);
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }

    private function is_customer_exists($data) {
        $email = (isset($data['email']) && $data['email'] != '') ? $data['email'] : '';
        $contact_no = (isset($data['contact_no']) && $data['contact_no'] != '') ? $data['contact_no'] : '';
        $address = (isset($data['address']) && $data['address'] != '') ? trim($data['address']) : '';
        $company_name = (isset($data['company_name']) && $data['company_name'] != '') ? $data['company_name'] : '';
        $reason = '';
        $flag = true;
        $is_customer_exists = array();

        if (isset($email) && $email != '') {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', array('customer_email' => $email));
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Email' : '';
        }

        if (isset($contact_no) && $contact_no != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', ['customer_contact_no' => $contact_no]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Contact Number' : '';
        }

        if (isset($address) && $address != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customer_locations', 'cust_id as id', ['address' => $address]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Address' : '';
        }

        if (isset($company_name) && $company_name != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', ['company_name' => $company_name]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Business Name' : '';
        }

        return $reason;
    }

    private function is_customer_contact_exists($data) {
        $email = (isset($data['email']) && $data['email'] != '') ? $data['email'] : '';
        $contact_no = (isset($data['contact_no']) && $data['contact_no'] != '') ? $data['contact_no'] : '';
        $address = (isset($data['address']) && $data['address'] != '') ? trim($data['address']) : '';
        $company_name = (isset($data['company_name']) && $data['company_name'] != '') ? $data['company_name'] : '';
        $reason = '';
        $flag = true;
        $is_customer_exists = array();

        if (isset($email) && $email != '') {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', array('customer_email' => $email));
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Email' : '';
        }

        if (isset($contact_no) && $contact_no != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', ['customer_contact_no' => $contact_no]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Contact Number' : '';
        }

        if (isset($address) && $address != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customer_locations', 'cust_id as id', ['address' => $address]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Address' : '';
        }

        if (isset($company_name) && $company_name != '' && empty($is_customer_exists) && $flag) {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', ['company_name' => $company_name]);
            $flag = (!empty($is_customer_exists)) ? false : true;
            $reason = (!empty($is_customer_exists)) ? 'Duplicate Business Name' : '';
        }

        if ($reason == '') {
            //Now Check in Contacts Table
            if (isset($email) && $email != '') {
                $is_customer_exists = $this->common->fetch_where('tbl_customer_contacts', 'id', array('customer_email' => $email));
                $flag = (!empty($is_customer_exists)) ? false : true;
                $reason = (!empty($is_customer_exists)) ? 'Duplicate Email' : '';
            }

            if (isset($contact_no) && $contact_no != '' && empty($is_customer_exists) && $flag) {
                $is_customer_exists = $this->common->fetch_where('tbl_customer_contacts', 'id', ['customer_contact_no' => $contact_no]);
                $flag = (!empty($is_customer_exists)) ? false : true;
                $reason = (!empty($is_customer_exists)) ? 'Duplicate Contact Number' : '';
            }

            if (isset($company_name) && $company_name != '' && empty($is_customer_exists) && $flag) {
                $is_customer_exists = $this->common->fetch_where('tbl_customer_contacts', 'cust_id as id', ['company_name' => $company_name]);
                $flag = (!empty($is_customer_exists)) ? false : true;
                $reason = (!empty($is_customer_exists)) ? 'Duplicate Business Name' : '';
            }
        }

        return $reason;
    }

    public function fetch_customers_by_keyword() {
        $user_id = $this->aauth->get_user()->id;
        if (!empty($this->input->post())) {
            $search = $this->input->post('search');

            if ($this->aauth->is_member('Admin')) {
                $customers = $this->customer->get_customer_by_cond(FALSE, $search);
            } else if ($this->components->is_team_leader()) {
                $customers = $this->customer->get_customer_by_cond(FALSE, $search, $user_id);
            } else if ($this->components->is_sales_rep()) {
                $data['meta_title'] = 'Franchise Agents - Manage Customer';
                $user_id = $this->aauth->get_user_id();
                $users = $this->user->get_users(FALSE, $user_id, FALSE);
                $user_id = (!empty($users)) ? $users[0]['id'] : $user_id;
                $customers = $this->customer->get_customer_by_cond(FALSE, $search, $user_id);
            }
            
          

            $data['success'] = TRUE;
            foreach ($customers as $key => $value) {
                $data['customers'][] = array(
                    "value" => $value['cust_id'],
                    "label" => $value['first_name'] . ' ' . $value['last_name'] . ' [' . $value['address'] . '] ',
                    "data" => $value,
                    "name" => $value['first_name'] . ' ' . $value['last_name']);
            }
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }

        echo json_encode($data);
        die;
    }
    
    public function assign_user_by_postcode() {
        if (!empty($this->input->get())) {
            $postcode = $this->input->get('postcode');
            $userid = $this->input->get('userid');
            if(isset($userid) && $userid != '' && $userid != NULL){
                $franchise_location_data = $this->common->fetch_where('tbl_user_locations','workarea_postcodes',array('user_id' =>  $userid));
                if(!empty($franchise_location_data)){
                    $postcode = $franchise_location_data[0]['workarea_postcodes'];
                    $franchise_list = $this->customer->get_user_list($postcode);
                }else{
                    $franchise_list = $this->customer->get_user_list($postcode);
                }
                
            }else{
                $franchise_list = $this->customer->get_user_list($postcode);
            }
            
            // This code is added after john's confirmation that he would need all franchise list whether post code are matched or not
            $unmatchedFranchiseList = $this->customer->get_unmatched_user_list($postcode);
            $franchise_list = array_merge($franchise_list,$unmatchedFranchiseList);
            
            
            $data['success'] = TRUE;
            $data['selected_franchise'] = (!empty($franchise_list)) ? $franchise_list[0] : [];
            $default_franchise = $this->customer->get_user_list(DEFAULT_FRANCHISE);
            $data['default_franchise'] = (!empty($default_franchise)) ? $default_franchise[0] : [];
            if (empty($franchise_list)) {
                //$franchise_list = $this->franchise->get_franchises();
            }
            $data['franchise_list'] = $franchise_list;
            $data['status'] = 'Sales Rep Assigned Successfully';
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

}

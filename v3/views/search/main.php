<style>
    .control-label:after {
        content:"*";
        color:red;
    }
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .state{width: 70px !important;}
    #search-nav .nav {
        border-bottom: 2px solid #e0e0e0;
    }

    #search-nav .nav-link.active {
        padding-bottom: 6px;
        border-bottom: 2px solid #00B5B8;
        margin-bottom: -2px; 
    }

    #search-results .media-search {
        padding-top: 5px; 
    }
    /* Input with icon */
    .has-icon-left .form-control {
        padding-left: 2.5rem; }

    .form-control-position {
        position: absolute;
        top: 4px;
        right: 0;
        z-index: 2;
        display: block;
        width: 2.5rem;
        height: 2.5rem;
        line-height: 2.5rem;
        text-align: center; }

    /*---------------------------------
    Input Icon
    ---------------------------------*/
    .position-relative .form-control {
        padding-right: calc(2.75rem + 2px); }
    .position-relative .form-control.form-control-lg ~ .form-control-position {
        top: 10px; }
    .position-relative .form-control.form-control-sm ~ .form-control-position {
        top: -3px; }

    /* Input Icon left */
    .has-icon-left .form-control {
        padding-right: 1rem;
        padding-left: calc(2.75rem + 2px); }

    .has-icon-left .form-control-position {
        right: auto;
        left: inherit; }

    input[type="color"] {
        height: calc(2.75rem + 2px); }
    .position-relative .form-control {
        padding-right: calc(2.75rem + 2px);
    }

    .search_list{
        margin-top:-1.3em;
        overflow-y: scroll;
        max-height: 400px;
    }

    .search_list_item__info{
        display:flex;
    }

    .ui-menu {
        display: block;
        z-index: 9999 !important;
    }
    
    .m5{margin: 5px;}
</style>
<div class="page-wrapper d-block clearfix ">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Search</h1>
    </div>

    <div class="content-body">

        <section id="search-website" class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-group">                
                        <input type="text" class="form-control form-control-xl input-xl" id="search_input" placeholder="Search Kuga CRM...">
                        <div class="form-control-position">
                            <a href="javascript:void(0);" id="add_new_customer"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <ul class="list-group search_list" id="user_list">  

                    </ul>  
                </div>
            </div>
        </section>

        <section id="customer_details" class="card hidden">
            <div class="card-content">
                <h5 class="card-header">Customer Details</h5>
                <div class="card-body" id="customer_details_body">

                </div>
            </div>
        </section>
    </div>
</div>
<!-- Few modals -->
<div class="modal" id="add_new_company_modal" role="dialog" >
    <div class="modal-dialog" style="max-width: 700px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_new_company_modal_title">Add New Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post" name="customerAdd" id="customerAdd" enctype="multipart/form-data">
                            <input type="hidden" name="cust_id" id="company_cust_id">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="form-section">Customer Details</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="hidden" name="company[company_name]" id="companyNameForm"  class="form-control" placeholder="Company Name"  required>
                                                <div id="checkcompany"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="company[first_name]" id="firstname"  class="form-control" placeholder="First Name"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="company[last_name]" id="lastname"  class="form-control" placeholder="Last Name"  required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" name="company[customer_email]" id="contactEmailId"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <input type="text" name="company[customer_contact_no]" id="contactMobilePhone"  class="form-control" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <select name="company[title]" id="cust_title" class="form-control"  required>
                                                    <?php
                                                    $cust_title = unserialize(cust_title);
                                                    foreach ($cust_title as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>   
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Franchise</label>
                                                <select class="form-control"  data-live-search="true" id="businessId" name="company[admin_id]" title="Enter Customer Postcode First" >
                                                    <option>Enter Customer Postcode First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="form-section">Site Details</h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <label>Customer Address</label>
                                                <select class="select2 placecomplete form-control" id="locationstreetAddress"  style="height:48px !important;"></select>
                                                <input type="hidden" id="locationstreetAddressVal" name="site[address]" />
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label>State</label>
                                                <select name="site[state_id]" id="locationState" class="form-control" required>
                                                    <option value="">Select State</option>
                                                    <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                        <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                                    <?php } ?>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group control-group">
                                                <label>Post Code</label>
                                                <input class="form-control" placeholder="Please Enter Post Code" type="text" name="site[postcode]" id="locationPostCode" value=""  required />

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="buttwrap">
                                <input type="button" id="addNewCompanyBtn" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>

    <div class="modal" id="activity_modal" role="dialog" >
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add /Schedule Activity</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="form-block d-block clearfix">
                            <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                                <input type="hidden" name="activity[id]" id="activity_id" class="form-control" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Activity Type </label>
                                            <select name="activity[activity_type]"  class="form-control" id="activity_type"  required>
                                                <option value="">Select Actitvity Type </option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-file">
                                            <input type="file" id="activity_file"  class="custom-file-input" >
                                            <input type="hidden" name="activity[attachment]" id="activity_file_value" class="form-control" >
                                            <span class="custom-file-label" for="activity_file">Choose attachment file</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Note </label>
                                            <textarea name="activity[note]" id="activity_note" class="form-control" placeholder="Note"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" >
                                        <input id="is_scheduled" name="activity[is_scheduled]" type="hidden" value="0">
                                        <div class="form-group hidden" id="scheduled_date">
                                            <label style="display:block !important;">Schedule Date </label>
                                            <div class="input-group date" data-provide="datepicker" style="margin-bottom: 1.5rem;">
                                                <input class="datepicker" name="activity[scheduled_at]"  value="<?php echo date('d-m-Y'); ?>">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttwrap">
                                    <input type="button" id="addActivity" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="<?php echo site_url(); ?>assets/css/jquery-ui.min.css" type="text/css" rel="stylesheet">
    <script src="<?php echo site_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>common/js/search.js?v="<?php echo version; ?>></script>
    <script>
        var context = {};
        var search_tool = new search_manager(context);
    </script>
</body>
</html>


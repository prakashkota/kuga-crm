<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Kuga Electrical CRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#4c4c4c">
    <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/images/apple-touch-icon-180x180.png" />
    <link rel="manifest" href="<?php echo site_url(); ?>/manifest.json">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- STYLE -->
    <?php if (strtolower($this->router->fetch_class()) != 'survey') { ?>
        <link href="<?php echo site_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <?php } ?>

    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/all.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/sidebar-menu.css" type="text/css" media="all">
    <link href="<?php echo site_url(); ?>assets/css/custom.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/style.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/responsive.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/new-style.css" rel="stylesheet" type="text/css">

    <link href="<?php echo site_url(); ?>assets/css/commercial.min.css?v=<?php echo version; ?>" rel="stylesheet" type="text/css">
    <!-- ADDITIONAL -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/toastr.css" />

    <!-- SCRIPTS -->
    <script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
    <?php if ($this->components->is_team_leader()) { ?>
        <script src="<?php echo site_url(); ?>assets/js/notify.js?v=<?php echo version; ?>"></script>
        <script> var notify_manager = new notify_manager();</script>
    <?php } else if ($this->components->is_sales_rep()) { ?>
        <script src="<?php echo site_url(); ?>assets/js/notify.js?v=<?php echo version; ?>"></script>
        <script> var notify_manager = new notify_manager();</script>
    <?php } ?>

    <script src="<?php echo site_url(); ?>assets/js/toastr.js"></script> 
    <script src="<?php echo site_url(); ?>assets/js/custom.js?v=<?php echo version; ?>"></script>


    <style>
        .clearfix:after {
          visibility: hidden;
          display: block;
          font-size: 0;
          content: " ";
          clear: both;
          height: 0;
      }
      .login-box-outer{ height:100vh; position:relative;}
      .login-box{ position:absolute; top:50%; right:0; left:0; max-width:800px; margin:0 auto; transform:translateY(-50%);}
      .login-logo{width: 39%;

          float: left;

          padding-top: 65px;

          border: 1px solid #ccc;

          padding-left: 50px;

          min-height: 232px;

          text-align: center;

          padding-right: 50px;}
          .login-fild {width: 60%;float: right;border: solid 1px #dadada;}
          .login-fild h2 {display: block;margin: 0;padding: 10px 20px;background: #f5f5f5;border-bottom: solid 1px #dadada;font-size: 18px;}
          .form-box{ padding:20px 20px;}
          .form-box .form-group{ position:relative; margin-bottom:15px;}
          .input-group-addon{background:#eeeeee;display:block;position:absolute;top:1px;bottom:1px;left:1px;border-radius:3px 0 0 3px;border-right:solid 1px #dcdcdc;}
          .form-controlInt{ width:100%; border:none;height: 38px;border: solid 1px #dcdcdc; border-radius:3px; padding:0 0 0 50px; box-sizing:border-box;}
          .login-btn{ background:#428bca; border:none; color:#fff; font-size:16px; padding:10px 20px; cursor:pointer;border-radius:3px;}
          .text-left.text-danger {position: absolute;bottom: 11px;right: 10px;font-style: italic;font-size: 11px;letter-spacing: 1px;}

          @media only screen and (max-width:767px){
            .login-box {padding:25px 15px;transform: inherit; position: static;}  
            .login-fild {width:100%;}
            .login-logo {width: 100%;text-align: center;min-height: auto;padding: 15px;margin-bottom: 15px;border: none;}
            .form-box .form-group {margin-bottom: 30px;}
            .text-left.text-danger {right: auto;left: 38px;bottom: -17px;}
        }
        body{
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.428571429;
            color: #333333;
            background-color: #ffffff;
        }
    </style>


    <script>
        var base_url = '<?php echo site_url(); ?>';
    </script>
</head>

<div class="login-box-outer">
    <div class="login-box clearfix">
        <?php $message = $this->session->flashdata('message');
        echo (isset($message) && $message != NULL) ? $message : '';
        ?>
        <div id="error_message"></div>
        <div class="login-logo"><img src="<?php echo site_url(); ?>assets/images/logo.svg" alt="logo"/></div>
        <div class="login-fild" style="min-height: 232px;">
            <h2>Enter Access Code To View Your Proposal</h2>
            <form role="form" action="" method="post" name="frm"  style="padding-top:20px;">
                <div class="form-box clearfix">
                    <div class="form-group">
                        <span class="input-group-addon">
                            <img src="<?php echo site_url(); ?>assets/images/lock-icon.png" alt="lock-icon"/>
                        </span>
                        <input type="text" placeholder="Access Code" name="otp" id="otp" class="form-controlInt">
                        <?php echo form_error('otp', '<div class="text-left text-danger">', '</div>'); ?>
                    </div>
                    <div class="row">
                        <span class="col-md-6"> 
                            <input type="submit" class="login-btn" value="Proceed" id="sales_login_btn"> 
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
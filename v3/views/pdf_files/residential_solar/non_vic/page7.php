<?php
if ($lead_type != 3 && !empty($savings_calculation_data)) {
    $annual_prod = (isset($saving_calculation_details['solar_annual_production'])  && $saving_calculation_details['solar_annual_production'] != '') ?  (float) $saving_calculation_details['solar_annual_production'] : 0;  
    $avg_prod = $annual_prod / 12;
    $avg_prod = round((float) $avg_prod);

    $usage_savings = (isset($saving_calculation_details['usage_savings']) && $saving_calculation_details['usage_savings'] != '') ?  round((float) $saving_calculation_details['usage_savings']): 0;

    $sc_export_credit = (isset($saving_calculation_details['export_credit']) && $saving_calculation_details['export_credit'] != '') ?  round((float) $saving_calculation_details['export_credit']): 0;

    $sc_total_savings = (isset($saving_calculation_details['total_savings']) && $saving_calculation_details['total_savings'] != '') ?  round((float) $saving_calculation_details['total_savings']): 0;

    $sc_annual_savings = (isset($saving_calculation_details['annual_savings']) &&  $saving_calculation_details['annual_savings'] != '') ?  round((float) $saving_calculation_details['annual_savings']): 0;

    $total_system_size = (isset($saving_calculation_details['total_system_size']) &&  $saving_calculation_details['total_system_size'] != '') ?  (float) $saving_calculation_details['total_system_size'] :  0;

    $kwh_day = (isset($saving_calculation_details['kwh_day']) && $saving_calculation_details['kwh_day'] != '') ?  (float) $saving_calculation_details['kwh_day'] :  0;

    $exportTotal = (isset($saving_calculation_details['exportTotal']) && $saving_calculation_details['exportTotal'] != '') ?  (float) $saving_calculation_details['exportTotal'] :  0;

    $TotalSavingsArr = (isset($saving_calculation_details['TotalSavingsArr'])  && $saving_calculation_details['TotalSavingsArr'] != '') ?  (float) $saving_calculation_details['TotalSavingsArr'] :  0;

    $feed_in_tariff = (isset($saving_calculation_details['feed_in_tariff'])  && $saving_calculation_details['feed_in_tariff'] != '') ?  number_format((float) $saving_calculation_details['feed_in_tariff'], 2, '.', '') :  0;

    $export = (isset($saving_calculation_details['export'])  && $saving_calculation_details['export'] != '') ?  $saving_calculation_details['export'] :  0;

    $savings_calculation_data['TotalSavingsArr'];
    $AverageDailyProduction = $total_system_size * $kwh_day;
    $AverageDailyProduction = round((float) $AverageDailyProduction);
    $exportTotal = number_format((float) $exportTotal, 2, '.', '');
    $exportTotal = round((float) $exportTotal);


    $electricity_rate = 0;
    if (isset($saving_calculation_details['electricity_rate'])) {
        $electricity_rate = $saving_calculation_details['electricity_rate'];
    } else if (isset($saving_calculation_details['flat_rate']) && $saving_calculation_details['flat_rate'] > 0) {
        $electricity_rate = $saving_calculation_details['flat_rate'];
    } else if (isset($saving_calculation_details['tr_rate'])) {
        $saving_calculation_details['tr_rate'] = (array) $saving_calculation_details['tr_rate'];
        $electricity_rate = array_sum($saving_calculation_details['tr_rate']);
    }
    $electricity_rate = number_format((float) $electricity_rate, 2, '.', '');

    $width = (strlen($AverageDailyProduction) > 1) ? 409 : 398;
    
    $near_map_data = (isset($proposal['near_map_data'])) ? $proposal['near_map_data'] : [];
    $tilts = [];
    if (!empty($near_map_data)) {
        $near_map_data = json_decode($near_map_data,true);
        if(!empty($near_map_data['tiles'])){
            $near_map_data['tiles']    = $near_map_data['tiles'];
            $no_of_panel_tiles  = count($near_map_data['tiles']);
            for ($i = 0; $i < $no_of_panel_tiles; $i++) {
                $tilt = is_numeric($near_map_data['tiles'][$i]['sp_tilt']) ? $near_map_data['tiles'][$i]['sp_tilt'] . '&#176;' : 0 . '&#176;';
                if(!in_array($tilt , $tilts)){
                    $tilts[] = $tilt;
                }
            }
        }
    }

    $tilt_text = '';
    if(!empty($tilts)){
        $tilt_text = "of " . implode('/',$tilts);
    }
    
    //As in Change on 04-08-2020 via skype feedback by anthony
    $estd_daily_otp = ($annual_prod > 0) ? $annual_prod/365 : 0;
    $estd_daily_otp = round((float) $estd_daily_otp);
    ?>
    <script type="text/javascript" src="https://www.google.com/jsapi" ></script>
    <script type='text/javascript'>
        google.load('visualization', '1', {packages: ['corechart']});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Year', 'kWh', 'Average'],
                ['JAN', <?php echo $savings_calculation_data['averageDailyKWhPerMonth1']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['FEB', <?php echo $savings_calculation_data['averageDailyKWhPerMonth2']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['MAR', <?php echo $savings_calculation_data['averageDailyKWhPerMonth3']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['APR', <?php echo $savings_calculation_data['averageDailyKWhPerMonth4']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['MAY', <?php echo $savings_calculation_data['averageDailyKWhPerMonth5']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['JUN', <?php echo $savings_calculation_data['averageDailyKWhPerMonth6']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['JUL', <?php echo $savings_calculation_data['averageDailyKWhPerMonth7']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['AUG', <?php echo $savings_calculation_data['averageDailyKWhPerMonth8']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['SEP', <?php echo $savings_calculation_data['averageDailyKWhPerMonth9']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['OCT', <?php echo $savings_calculation_data['averageDailyKWhPerMonth10']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['NOV', <?php echo $savings_calculation_data['averageDailyKWhPerMonth11']; ?>,<?php echo $AverageDailyProduction; ?>],
                ['DEC', <?php echo $savings_calculation_data['averageDailyKWhPerMonth12']; ?>,<?php echo $AverageDailyProduction; ?>],
            ]);
            var options = {
                curveType: 'function',
                titleTextStyle: {
                    fontName: 'Helvetica, sans-serif;',
                    italic: false,
                    bold: false,
                    fontStyle: 'normal', //or bold, italic, etc.
                    fontSize: 12
                },
                legend: {position: 'bottom',
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 14,
                        fontStyle: 'normal'
                    }
                },
                hAxis: {
                    format: '#\'$\'',
                    titleTextStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: false,
                        fontSize: 12,
                        fontStyle: 'normal'
                    },
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: 'normal'
                    }
                },
                vAxis: {
                    format: '#\'kWh\'',
                    textStyle: {
                        fontName: 'Helvetica, sans-serif;',
                        italic: false,
                        bold: true,
                        fontSize: 13,
                        fontStyle: 'normal'
                    }
                },
                width: 900,
                height: 330,
                legend: {position: 'none', textStyle: {fontSize: 4, }},
                seriesType: 'bars',
                series: {
                    0: {color: '#ffcd26'},
                    1: {
                        type: 'line', color: '#000',
                        visibleInLegend: false
                    }
                }
            };
            /**var view = new google.visualization.DataView(data);
             view.setColumns([0, 1,
             {calc: "stringify",
             sourceColumn: 1,
             type: "string",
             role: "annotation"},
             2]);*/
            //var chart = new google.visualization.ComboChart(document.getElementById('chart'));
            //chart.draw(data, options);
        }
    </script>
<?php } ?>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:400;">
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:30px; font-weight:900; color:#DE1717;padding-left:50px;">
		<img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/header_lines.jpg" width="45" height="40" alt=""/>
            ESTIMATED <strong style="font-size: 30px; color:#000;">PERFORMANCE</strong>
        </td>
    </tr>
    <tr>
        <td style="padding-top:40px; padding-bottom:60px; padding-left:50px;">&nbsp;</td>
    </tr>

    <tr>
        <td>
            <span style="display:inline-block; padding:10px 80px 10px 50px; font-size:18px; background:#DE1717;color:#fff;"><strong style="margin:0 15px 0 0;"> ENERGY OUTPUT</strong> <?php echo $annual_prod; ?> kWh / YEAR</span>
        </td>
    </tr>

    <tr>
        <td>
            <span style=" max-width:<?php echo $width; ?>px; margin:60px auto; margin-bottom: 0px; display:block; padding:0 0 0 20px; font-size:18px; background:#fff; border-top:solid 2px #DE1717;border-left:solid 2px #DE1717;border-bottom:solid 2px #DE1717; font-weight:700;">ESTIMATED DAILY OUTPUT
            <strong style="background:#DE1717; margin:0 0 0 20px; display:inline-block; padding:15px 20px"><?php echo $estd_daily_otp; ?>kWh AVE.</strong></span>
        </td>
    </tr>

    <tr>
        <td>
            <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <!--<div id="chart"></div>-->
                        <img src="<?php echo site_url('assets/uploads/pdf_files/chart1_'.$proposal['id'].'.png'); ?>"

                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="height:10px;"></td>
    </tr>

    <tr>
        <td>
            <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style=" background:#DE1717; text-transform:uppercase; text-align:center; font-size:22px; font-weight:700; padding:10px 0;">MONTHLY SOLAR SAVINGS ESTIMATE</td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-bottom:40px;">
                            <tbody>
                                <tr>
                                    <th style="background:#848283; text-align:center; color:#fff; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">AVE PRODUCTION</th>
                                    <th style="background:#848283; text-align:center; color:#fff; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">USAGE SAVINGS</th>
                                    <th style="background:#848283; text-align:center; color:#fff; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">EXPORT CREDIT</th>
                                    <th style="background:#848283; text-align:center; color:#fff; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">TOTAL SAVINGS</th>
                                    <th style="background:#848283; text-align:center; color:#fff; padding:10px 0; font-size:15px;">ANNUAL SAVINGS</th>
                                </tr>
                                <tr>
                                    <td style="background:#e5e5e5; text-align:center; color:#000; padding:10px 0; font-size:15px; border-right:solid 1px #fff;"><?php echo $avg_prod; ?></td>
                                    <td style="background:#e5e5e5; text-align:center; color:#000; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">$<?php echo $usage_savings; ?></td>
                                    <td style="background:#e5e5e5; text-align:center; color:#000; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">$<?php echo $sc_export_credit; ?></td>
                                    <td style="background:#e5e5e5; text-align:center; color:#000; padding:10px 0; font-size:15px; border-right:solid 1px #fff;">$<?php echo $sc_total_savings; ?></td>
                                    <td style="background:#e5e5e5; text-align:center; color:#000; padding:10px 0; font-size:15px;">$<?php echo $sc_annual_savings; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height:20px;"></td>
    </tr>
    <tr>
        <td>
            <table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-size:22px; color:#DE1717; font-weight:700;">ASSUMPTIONS</td>
                </tr>

                <tr>
                    <td>
                        <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" style="padding:15px 0;">
                            <tr>
							<?php $orientation_array = [];
								if( isset($customer_data['near_map_data']) && $customer_data['near_map_data'] !=''){
											$cust_data	=	json_decode($customer_data['near_map_data']);
											
											if(isset($cust_data->tiles) && !empty($cust_data->tiles)){
												
												foreach($cust_data->tiles as $tiles){
													$orientation_array[] = $tiles->sp_rotation.'&deg;';
												}
											}
											
											$orient =	implode(',',array_unique($orientation_array));
											
								}?>
                                <td valign="top"><span style="width:10px; height:10px; background:#DE1717; display:inline-block; border-radius:50px; margin:5px 10px 0 0;"></span></td>
                                <td style="font-size:15px; line-height:22px; ">Energy Output is calculated based on historical solar irradiance and temperature data at this location, factoring in panel tilt of <?php echo $tilt_text;?>, Orientation of <?php if(isset($orient) && !empty($orient)){ echo $orient;}?> and all of the System Parameters including System Efficiency. This 
                                    assumption does factor in <?php //echo ($saving_calculation_details['shading'] != '' ) ? $saving_calculation_details['shading'] : 0 ;?>% shading at the premise.</td>
                            </tr>

                            <tr>
                                <td valign="top"><span style="width:10px; height:10px; background:#DE1717; display:inline-block; border-radius:50px; margin:5px 10px 0 0;"></span></td>
                                <td style="font-size:15px; line-height:22px;">Assumes typical ultilisation of generated electricity, and will charge based on usage and feed-in tariffs.</td>
                            </tr>
                            <tr>
                                <td valign="top"><span style="width:10px; height:10px; background:#DE1717; display:inline-block; border-radius:50px; margin:5px 10px 0 0;"></span></td>
                                <td style="font-size:15px; line-height:22px;">Annual Production: <?php echo $annual_prod; ?> kWh | Electricity Rate: $<?php echo $electricity_rate; ?>  | Export Tariff: $<?php echo $feed_in_tariff; ?> | Percentage Export: <?php echo $export; ?>%</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <?php if (!empty($panel_specsheet_data)) { ?>
        <tr>
            <td style="height:130px;"></td>
        </tr>
    <?php } else if(!empty($inverter_specsheet_data)){ ?>
        <tr>
            <td style="height:130px;"></td>
        </tr>
    <?php } else { ?>
        <tr>
            <td style="height:135px;"></td>
        </tr>
    <?php } ?>

    <tr>
        <td align="bottom" style="position:relative;bottom:-60px;">
            <img src="<?php echo $this->config->item('live_url'); ?>assets/pdf_images/residential_solar/footer.jpg" width="100%" alt=""/>
        </td>
    </tr>
</table>

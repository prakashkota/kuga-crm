<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Solar Booking Form</title>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            </style>
        </head>
        <body style="padding:0; margin:0">
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page1'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page2'); ?>
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page4_1'); ?> 
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page3'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page4'); ?>
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page5'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page6'); ?>
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page6_1'); ?>
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page7'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page8'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page9_1'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page10_1'); ?>    
            <?php $this->load->view('pdf_files/solar_booking_form_v2/page11'); ?>
        </body>
    </html>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Agent Controller
 * @version 1.0
 */
class Agent extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Franchise_Model", "franchise");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Agent_Model", "agents");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function manage() {

        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Franchise')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        
        $data = array();
        $owner_id = $this->aauth->get_user()->id;
        $agents = $this->agents->get_agents($owner_id,FALSE);
        $data['agents'] = $agents;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('agent/manage');
        $this->load->view('partials/footer');
    }

    public function add() {
        $data = array();
        $owner_id = $this->aauth->get_user()->id;
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('user_details[full_name]', 'Agent Full Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Contact No', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[aauth_users.email]');
            $this->form_validation->set_rules('users[password]', 'Password', 'required');
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');

            if ($this->form_validation->run() != FALSE) {
                $userdata = $this->input->post('users');
                $userdata['email'] = $this->input->post('email');
                //$username = $this->aauth->generate_username($user_details['full_name'], 10);
                $user_id = $this->aauth->create_user($userdata['email'], $userdata['password']);
                if ($user_id) {
                    //Add user to Specific Group
                    $this->aauth->add_member($user_id, "Franchise Agents");
                     //Add user to owner relation
                    $user_to_owner_data['user_id'] = $user_id;
                    $user_to_owner_data['owner_id'] = $owner_id;
                    $this->common->insert_data('aauth_user_to_user', $user_to_owner_data);
                    //Insert Data to Details Table
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $user_id;
                    $this->common->insert_data('tbl_user_details', $user_details);
                    //Insert Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    $user_locations['user_id'] = $user_id;
                    $this->common->insert_data('tbl_user_locations', $user_locations);
                    
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Agent with Email: <strong>' . $userdata['email'] . '</strong> and Password: <strong>' . $userdata['password'] . '</strong> created successfuly.</div>');
                    redirect('admin/franchise/manage-agents');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
            }
        }
        
        $franchise = $this->franchise->get_franchises($owner_id);
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['franchise'] = $franchise;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('agent/add');
        $this->load->view('partials/footer');
    }

    public function edit($id) {
        /**if ($this->aauth->get_user()->id != $id && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this franchise.</div>');
            redirect('admin/franchise/manage');
        }*/

        $data = array();
        
        $owner_id = $this->aauth->get_user()->id;
        $agents = $this->agents->get_agents($owner_id,$id);

        if (empty($agents)) {
            redirect('/admin/franchise/manage-agents');
        }

        $data['email'] = $agents[0]['email'];
        $data['users'] = $agents[0];
        $data['user_locations'] = $agents[0];
        $data['user_details'] = $agents[0];

        if (!empty($this->input->post())) {
            if ($this->input->post('email') != $agents[0]['email']) {
                $is_unique = '|is_unique[aauth_users.email]';
            } else {
                $is_unique = '';
            }
            $this->form_validation->set_rules('user_details[full_name]', 'Agent Full Name', 'required');
            $this->form_validation->set_rules('user_details[company_contact_no]', 'Contact No', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'.$is_unique);
            $this->form_validation->set_rules('user_locations[address]', 'Location Address', 'required');
            $this->form_validation->set_rules('user_locations[state_id]', 'Location State', 'required');
            $this->form_validation->set_rules('user_locations[postcode]', 'Location Postcode', 'required');

            if ($this->form_validation->run() != FALSE) {
                $email = $this->input->post('email');
                $stat = TRUE;
                if ($this->input->post('email') != $agents[0]['email']) {
                    $stat = $this->aauth->update_user($id, $email);
                }

                if ($stat) {
                    //Update Data to Details Table
                    $user_details = $this->input->post('user_details');
                    $this->common->update_data('tbl_user_details', array('user_id' => $id), $user_details);
                    //Update Data to Location Table
                    $user_locations = $this->input->post('user_locations');
                    $this->common->update_data('tbl_user_locations', array('user_id' => $id), $user_locations);

                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Agent with Email: <strong>' . $email . '</strong> updated successfuly.</div>');
                    redirect('admin/franchise/manage-agents');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the user. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['users'] = $this->input->post('users');
                $data['user_details'] = $this->input->post('user_details');
                $data['user_locations'] = $this->input->post('user_locations');
            }
        }

        $franchise = $this->franchise->get_franchises($owner_id);
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['franchise'] = $franchise;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('agent/edit');
        $this->load->view('partials/footer');
    }

    public function change_password($id) {

        if ($this->aauth->get_user()->id != $id && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have permission to edit this franchise.</div>');
            redirect('admin/franchise/manage');
        }

        $data = array();
        $franchises = $this->franchise->get_franchises($id);

        if (empty($franchises)) {
            redirect('/admin/franchise/manage');
        }

        $data['email'] = $franchises[0]['email'];
        $data['password'] = '';


        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() != FALSE) {
                $pwd = $this->input->post('password');
                $stat = $this->aauth->update_user($id, FALSE, $pwd, FALSE);
                if ($stat) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Franchisee with Email: <strong>' . $data['email'] . '</strong> updated successfuly with new Password: <strong>' . $pwd . '</strong></div>');
                    redirect('admin/franchise/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the password. </div>');
                }
            } else {
                $data['email'] = $this->input->post('email');
                $data['password'] = $this->input->post('password');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('franchise/change_password');
        $this->load->view('partials/footer');
    }

}

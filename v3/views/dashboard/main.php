<style>
   .page-wrapper {background-color: #ECEFF1; min-height: 100%;}
   .error-template {padding: 40px 15px;text-align: center;}
   .error-actions {margin-top:15px;margin-bottom:15px;}
   .error-actions .btn { margin-right:10px; }
   .sr-inactive_user{color:red;}
   .table_target {
   border: 1px solid grey;
   width:100%;
   font-family: arial, sans-serif;
   border-collapse: collapse;
   }
   .table_target td {
   border: 1px solid grey;
   border-bottom: 1px solid #fff;
   position: relative;
   }
   .table_target td input{
   border: none;
   width: 100%;
   }
   .item_title{
   font-size: 12px;
   padding-left: 5px;
   width:20%;
   }   
   /**.group_last_element{border-bottom: 1px solid grey !important;} */
   .css-month {
   border: 1px solid grey;
   width: 105px;
   text-align: center;
   padding: 5px 0;
   }
   .chart {
   width: 100%; 
   min-height: 450px;
   }
   .row {
   margin:0 !important;
   }
   .table_target td, .table_target th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
   }
   .table_target th
   {
   background-color:black;
   color:white;
   }
   .table_target th:first-child, .table_target td:first-child
   {
   position:sticky;
   left:0px;
   }
   .table_target td:first-child
   {
   background-color:#fff;
   z-index: 1;
   }
   .kg-tbl_col--yellow{
   background-color: rgba(250,250,210,1) !important;
   box-shadow: inset 4px 0 0 0 #FAFAD2 !important;
   }
   .kg-tbl_col--green{
   background-color: rgb(144,238,144) !important;
   box-shadow: inset 4px 0 0 0 #90EE90 !important;
   }
   .kg-tbl_col--red{
   background-color: rgba(255, 192, 203, 1) !important;
   box-shadow: inset 4px 0 0 0 #ffc0cb !important;
   }
   .tooltip {
   font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
   font-size: 12px;
   font-style: normal;
   font-weight: normal;
   line-height: 1.42857143;
   text-align: left;
   text-align: start;
   text-decoration: none;
   text-shadow: none;
   text-transform: none;
   letter-spacing: normal;
   word-break: normal;
   word-spacing: normal;
   word-wrap: normal;
   white-space: normal;
   }
   .customReportTable{
       table-layout:fixed;
   }
   .span_abs{
    position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    display: block;
   }
   .span_abs_td{
       position: relative;
   }
   .bb1-black{
       border-bottom: 1px solid black !important;
   }
</style>
<body>
   <!--pagewrapper-->
   <div class="page-wrapper d-block clearfix">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
         <h1 class="h2">Dashboard & Statistics</h1>
         <div class="content-header-right col-md-6 col-12 ">
            <?php if ($this->aauth->is_member('Admin')) { ?>
            <div  class="float-md-right col-md-4" style="display: inline-flex;">
               <select class="" name="userid" id="userid">
                  <option value="">Select User</option>
                  <?php foreach ($users as $key => $value) { ?>
                  <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                  <?php } ?>
               </select>
               <div class="form-check-inline" style="margin-left:5px;">
                  <input type="checkbox" class="form-check-input sr-show_inactive_users" value="1">
                  <i class="fa fa-user-times"></i>
               </div>
            </div>
            <?php } else if ($all_reports_view_permission) { ?>
            <div  class="float-md-right col-md-4" style="display: inline-flex;">
               <select class="" name="userid" id="userid">
                  <option value="">Select User</option>
                  <?php foreach ($users as $key => $value) { ?>
                  <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                  <?php } ?>
               </select>
            </div>
            <?php } ?>
         </div>
      </div>
      <div class="container hidden" id="error_container">
         <div class="row">
            <div class="col-md-12">
               <div class="error-template">
                  <div class="error-details">
                     Sorry, you don't have permissions to access reports!
                  </div>
                  <div class="error-actions">
                     <a href="mailto:service@solarrun.com.au" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="statistics_container">
         <!-- These are not in use for now -->
         <div class="row hidden" id="statistics">
            <div class="col-md-4">
               <div class="placeholder__card card">
                  <div class="image loading">
                  </div>
                  <div class="bars">
                     <div class="bar bar1 loading"></div>
                     <div class="bar bar2 loading"></div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="placeholder__card card">
                  <div class="image loading">
                  </div>
                  <div class="bars">
                     <div class="bar bar1 loading"></div>
                     <div class="bar bar2 loading"></div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="placeholder__card card">
                  <div class="image loading">
                  </div>
                  <div class="bars">
                     <div class="bar bar1 loading"></div>
                     <div class="bar bar2 loading"></div>
                  </div>
               </div>
            </div>
         </div>
         <!-- These are not in use for now -->
         <div class="row hidden">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <h2>Conversion</h2>
                     <div class="table-responsive" >
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>Proposal</th>
                                 <th>Won</th>
                                 <th>Conversion rate (%)</th>
                              </tr>
                           </thead>
                           <tbody id="conversion_table_body">
                              <tr>
                                 <td colspan="3">
                                    <div class="placeholder__card card">
                                       <div class="bars">
                                          <div class="bar bar1 loading"></div>
                                          <div class="bar bar2 loading"></div>
                                       </div>
                                    </div>
                                 <td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row hidden" id="sales_target_container">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <h2>Sales Target Statistics</h2>
                     <div class="row">
                        <div class="col-md-12">
                           <div id="target_table_container" class="table-responsive">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row ">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <!-- LED Statstics -->
                     <h2>
                        LED Statistics
                        <a href="#" data-toggle="tooltip" title="This statistics contains count of Lead where lead type is LED with no filter i.e. all-time data. " style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <div class="row">
                        <div id="led_general_stats" class="col-4 mb-3">
                        </div>
                     </div>
                     <div  class="table-responsive">
                        <table class="table" id="led_statistics_table">
                           <thead id="led_statistics_table_head">
                           </thead>
                           <tbody id="led_statistics_table_body">
                              <tr>
                                 <td colspan="5">
                                    <div class="placeholder__card card">
                                       <div class="bars">
                                          <div class="bar bar1 loading"></div>
                                          <div class="bar bar2 loading"></div>
                                       </div>
                                    </div>
                                 <td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <br/>
                     <!-- Solar Statstics -->
                     <h2>
                        Solar Statistics
                        <a href="#" data-toggle="tooltip" title="This statistics contains count of Lead where lead type is SOLAR with no filter i.e. all-time data. " style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <div class="row">
                        <div id="solar_general_stats" class="col-4 mb-3">
                        </div>
                     </div>
                     <div  class="table-responsive">
                        <table class="table" id="led_statistics_table">
                           <thead id="solar_statistics_table_head">
                           </thead>
                           <tbody id="solar_statistics_table_body">
                              <tr>
                                 <td colspan="5">
                                    <div class="placeholder__card card">
                                       <div class="bars">
                                          <div class="bar bar1 loading"></div>
                                          <div class="bar bar2 loading"></div>
                                       </div>
                                    </div>
                                 <td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <br/>
                     <!-- LED + Solar Statstics -->
                     <h2>
                        LED + Solar Statistics
                        <a href="#" data-toggle="tooltip" title="This statistics contains count of Lead where lead type is LED + SOLAR with no filter i.e. all-time data." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <div class="row">
                        <div id="led_solar_general_stats" class="col-4 mb-3">
                        </div>
                     </div>
                     <div  class="table-responsive">
                        <table class="table" id="led_statistics_table">
                           <thead id="led_solar_statistics_table_head">
                           </thead>
                           <tbody id="led_solar_statistics_table_body">
                              <tr>
                                 <td colspan="5">
                                    <div class="placeholder__card card">
                                       <div class="bars">
                                          <div class="bar bar1 loading"></div>
                                          <div class="bar bar2 loading"></div>
                                       </div>
                                    </div>
                                 <td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row solar_sales_report_container">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom filter">
                        <h2>Solar Sales Report</h2>
                        <form id="reporting_actions_solar" action="javascript:void(0);" class="filter-right-block">
                           <div class="form-row mx-0" style="min-width:440px;">
                               <div class="form-check" style="margin-top: 8px;margin-right: 10px; padding-left: 10px;">
                                    <input type="checkbox" class="form-check-input" name="unassigend" id="unassigend">
                                    <label class="form-check-label">Hide Unassignd</label>
                                </div>
                                <div class="btn-toolbar mb-2 mb-md-0 position-relative mr-2">
                                    <input size="16" type="text" value="<?php echo date('Y'); ?>" name="year" id="year" class="form-control form-control-xl input-xl year_picker">
                                    <span  style="position: absolute; right: 10px; top:8px;"><i class="fa fa-calendar"></i></span>
                                </div>
                                
                                <div class="">
                                    <button type="button" class="filter_btn btn-default dropdown-toggle form-control" data-toggle="dropdown" aria-expanded="true" style="min-width:150px;">
                                      <i class="fa fa-filter"></i>Sales Rep
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" style="min-width:275px; min-height:100px; height:325px;overflow-y: scroll; z-index:1;">
                                        <li>
                                            <label>
                                              <strong>Sales Rep</strong>
                                            </label>
                                        </li>
                                        <!-- <li><label style="margin-bottom:0px; padding:5px;">
                                            <input data-filter="assigned_tos" class="assigned_tos" type="checkbox" name="assigned_tos[]" value=""> All</label>
                                        </li> -->
                                        <?php  foreach($users  as $v){?>
                                            <li <?php if($v['banned'] != 0) { ?> style="background:#C20000; color:#fff !important;" <?php } ?>><label style="margin-bottom:0px; padding:5px;">
                                                <label <?php if($v['banned'] != 0) { ?>style="color:#fff !important;" <?php } ?>><input  <?php if($v['banned'] == 0) { ?> checked="" <?php } ?> data-filter="assigned_tos" class="assigned_tos assigned_tos_<?php echo $v['user_id']; ?>" type="checkbox" name="assigned_tos1[]" value="<?php echo $v['user_id']; ?>"> <?php echo $v['full_name']; ?></label>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                           </div>
                        </form>
                     </div>
                     <div class="row">
                        <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                           <table class="table table-bordered customReportTable">
                                <thead>
                                    <tr>
                                      <th style="width:150px;">Sales Rep</th>
                                      <th style="width:100px;">State</th>
                                      <th style="width:100px;">2020 Results</th>
                                      <th style="width:130px; font-size: 13px;">Not Installed Approved</th>
                                      <th style="width:130px; font-size: 13px;">Not Installed Not Approved</th>
                                      <th>Jan</th>
                                      <th>Feb</th>
                                      <th>Mar</th>
                                      <th>Apr</th>
                                      <th>May</th>
                                      <th>Jun</th>
                                      <th>Jul</th>
                                      <th>Aug</th>
                                      <th>Sep</th>
                                      <th>Oct</th>
                                      <th>Nov</th>
                                      <th>Dec</th>
                                    </tr>
                                </thead>
                                <tbody id="solar_sales_report_table_body">
                        
                                </tbody>
                            </table>
                        </div>
                     </div>
                     
                    <div class="row">
                      <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                        <table class="table table-bordered customReportTable">
                          <thead>
                            <tr>
                              <th style="width:250px;">Sales Team</th>
                              <th style="width:100px;">2020 Results</th>
                              <th style="width:130px; font-size: 13px;">Not Installed Approved</th>
                              <th style="width:130px; font-size: 13px;">Not Installed Not Approved</th>
                              <th>Jan</th>
                              <th>Feb</th>
                              <th>Mar</th>
                              <th>Apr</th>
                              <th>May</th>
                              <th>Jun</th>
                              <th>Jul</th>
                              <th>Aug</th>
                              <th>Sep</th>
                              <th>Oct</th>
                              <th>Nov</th>
                              <th>Dec</th>
                            </tr>
                          </thead>
                          <tbody id="solar_total_sales_report_table_body">
                
                          </tbody>
                        </table>
                      </div>
                    </div>
    
                  </div>
               </div>
            </div>
         </div>
         <div class="row led_sales_report_container">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom filter">
                        <h2>LED Sales Report</h2>
                        <form id="reporting_actions_led" action="javascript:void(0);" class="filter-right-block">
                           <div class="form-row mx-0" style="min-width:440px;">
                                <!--<div class="form-check" style="margin-top: 8px;margin-right: 10px; padding-left: 10px;">
                                    <input type="checkbox" class="form-check-input" name="year1" id="year1">
                                    <label class="form-check-label">Yearwise</label>
                                </div>-->
                                <div class="form-check" style="margin-top: 8px;margin-right: 10px; padding-left: 10px;">
                                    <input type="checkbox" class="form-check-input" name="unassigend" id="unassigend1">
                                    <label class="form-check-label">Hide Unassignd</label>
                                </div>
                                <div class="btn-toolbar position-relative mb-2 mb-md-0">
                                    <input size="16" type="text" value="<?php echo date('Y'); ?>" name="month_year" id="month_year" class="form-control form-control-xl input-xl month_year_picker">
                                    <span  style="position: absolute; right: 10px;top:8px;"><i class="fa fa-calendar"></i></span>
                                </div>
                                
                                <div class="ml-2">
                                    <button type="button" class="filter_btn btn-default dropdown-toggle form-control" data-toggle="dropdown" aria-expanded="true" style="min-width:150px;">
                                      <i class="fa fa-filter"></i>Sales Rep
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" style="min-width:275px; min-height:100px; height:325px;overflow-y: scroll; z-index:1;">
                                        <li>
                                            <label>
                                              <strong>Sales Rep</strong>
                                            </label>
                                        </li>
                                        <!-- <li><label style="margin-bottom:0px; padding:5px;">
                                            <input data-filter="assigned_tos" class="assigned_tos" type="checkbox" name="assigned_tos[]" value=""> All</label>
                                        </li> -->
                                        <?php  foreach($users  as $v){?>
                                            <li <?php if($v['banned'] != 0) { ?> style="background:#C20000; color:#fff !important;" <?php } ?>><label style="margin-bottom:0px; padding:5px;">
                                                <label <?php if($v['banned'] != 0) { ?>style="color:#fff !important;" <?php } ?>><input <?php if($v['banned'] == 0) { ?> checked="" <?php } ?> data-filter="assigned_tos" class="assigned_tos1 assigned_tos_<?php echo $v['user_id']; ?>" type="checkbox" name="assigned_tos1[]" value="<?php echo $v['user_id']; ?>"> <?php echo $v['full_name']; ?></label>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                
                           </div>
                        </form>
                     </div>
                    <div class="row">
                        <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                            <table class="table table-bordered customReportTable">
                                <thead>
                                    <tr>
                                        <th style="width:190px;">Sales Rep</th>
                                        <th style="width:120px;">State</th>
                                        <th style="width:120px;">Products</th>
                                        <th style="width:120px;">2020 Results</th>
                                        <th style="width:140px; font-size: 13px;">Installed But Not Processed</th>
                                        <th style="width:130px; font-size: 13px;">Not Installed Approved</th>
                                        <th style="width:130px; font-size: 13px;">Not Installed Not Approved</th>
                                        <th style="width:120px;">Jan</th>
                                        <th style="width:120px;">Feb</th>
                                        <th style="width:120px;">Mar</th>
                                        <th style="width:120px;">Apr</th>
                                        <th style="width:120px;">May</th>
                                        <th style="width:120px;">Jun</th>
                                        <th style="width:120px;">Jul</th>
                                        <th style="width:120px;">Aug</th>
                                        <th style="width:120px;">Sep</th>
                                        <th style="width:120px;">Oct</th>
                                        <th style="width:120px;">Nov</th>
                                        <th style="width:120px;">Dec</th>
                                    </tr>
                                </thead>
                                <tbody id="led_sales_report_table_body">
                        
                                </tbody>
                            </table>
                        </div>
                     </div>
                     
                     <div class="row">
                      <div class="table-responsive dragscroll" style="cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab">
                        <table class="table table-bordered customReportTable">
                          <thead>
                            <tr>
                              <th style="width:310px;">Sales Team</th>
                              <th style="width:120px;">Products</th>
                              <th style="width:120px;">2020 Results</th>
                              <th style="width:140px; font-size: 13px;">Installed But Not Processed</th>
                              <th style="width:130px; font-size: 13px;">Not Installed Approved</th>
                              <th style="width:130px; font-size: 13px;">Not Installed Not Approved</th>
                              <th style="width:120px;">Jan</th>
                              <th style="width:120px;">Feb</th>
                              <th style="width:120px;">Mar</th>
                              <th style="width:120px;">Apr</th>
                              <th style="width:120px;">May</th>
                              <th style="width:120px;">Jun</th>
                              <th style="width:120px;">Jul</th>
                              <th style="width:120px;">Aug</th>
                              <th style="width:120px;">Sep</th>
                              <th style="width:120px;">Oct</th>
                              <th style="width:120px;">Nov</th>
                              <th style="width:120px;">Dec</th>
                            </tr>
                          </thead>
                          <tbody id="led_total_sales_report_table_body">
                
                          </tbody>
                        </table>
                      </div>
                    </div>
    
                  </div>
               </div>
            </div>
         </div>
         <div class="row activity_graph_container">
            <div class="col-xl-8 col-lg-12">
               <div class="card" >
                  <div class="card-body" >
                     <h2>
                        Activity Statistics
                        <a href="#" data-toggle="tooltip" title="This statistics contains count of activities with no restriction of user or any type within a following date filter range. (Note: It counts both completed and not complted activities)" style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <form id="reporting_actions" style="background:lightgrey; padding:10px;">
                        <?php if ($this->aauth->is_member('Admin')) { ?>
                        <div class="btn-group col-md-7" role="group" >
                           <div id="reportrange" >
                              <i class="fa fa-calendar"></i>&nbsp;
                              <span></span> <i class="fa fa-caret-down"></i>
                           </div>
                           <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                           <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <?php } else if ($all_reports_view_permission) { ?>
                        <div class="btn-group col-md-8" role="group" >
                           <div id="reportrange" >
                              <i class="fa fa-calendar"></i>&nbsp;
                              <span></span> <i class="fa fa-caret-down"></i>
                           </div>
                           <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                           <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <?php } else { ?>
                        <div class="btn-group float-md-right" role="group" >
                           <div id="reportrange" >
                              <i class="fa fa-calendar"></i>&nbsp;
                              <span></span> <i class="fa fa-caret-down"></i>
                           </div>
                           <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                           <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <?php } ?>
                     </form>
                     <div id="activity_statistics" style="height:400px;">
                        <div class="placeholder__card card">
                           <div class="bars">
                              <div class="bar bar1 loading"></div>
                              <div class="bar bar2 loading"></div>
                              <div class="bar bar1 loading"></div>
                              <div class="bar bar2 loading"></div>
                              <div class="bar bar1 loading"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-4 col-lg-12">
               <div class="card" style="min-height: 500px;">
                  <div class="card-body" >
                     <h2>
                        Activity Division
                        <a href="#" data-toggle="tooltip" title="This has same data of activity statistics but shown in grouped manner by activity type. (Note: It counts both completed and not complted activities)" style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <div id="activity_devision"  style="height:400px;">
                        <div class="placeholder__card card">
                           <div class="bars">
                              <div class="bar bar1 loading"></div>
                              <div class="bar bar2 loading"></div>
                              <div class="bar bar1 loading"></div>
                              <div class="bar bar2 loading"></div>
                              <div class="bar bar1 loading"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row ">
            <div class="col-md-12">
               <div class="card" >
                  <div class="card-body">
                     <h2>
                        Completed Activities
                        <a href="#" data-toggle="tooltip" title="This report contains all activities which are marked as completed based on filters." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                     </h2>
                     <div  class="table-responsive">
                        <table class="table" id="scheduled_activities_table">
                           <thead>
                              <tr>
                                 <th>Date of Activity</th>
                                 <th>Business Name</th>
                                 <th>Activity Type</th>
                                 <th>Deal Stage</th>
                                 <!--<th class="text-center">Mark as completed</th>
                                    <th class="text-center">Action</th>-->
                              </tr>
                           </thead>
                           <tbody id="scheduled_activities_table_body">
                              <tr>
                                 <td colspan="5">
                                    <div class="placeholder__card card">
                                       <div class="bars">
                                          <div class="bar bar1 loading"></div>
                                          <div class="bar bar2 loading"></div>
                                       </div>
                                    </div>
                                 <td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <?php echo $this->load->view('report/job_user_modal',NULL,TRUE); ?>
   
   <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
   <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
   <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
   <script src="<?php echo site_url(); ?>common/js/reports/dashboard.js?v=<?php echo version; ?>"></script>
   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFw7fbZ8ivK1NexDxTsh7eA7uiKJnMpVk&libraries=places,geometry"></script>
   <script type="text/javascript">
      var context = {};
      context.user_id = '<?php echo $user_id; ?>';
      context.user_group = '<?php echo $user_group; ?>';
      context.all_reports_view_permission = '<?php echo $all_reports_view_permission; ?>';
      context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
      context.activity_types = '<?php echo json_encode($activity_types); ?>';
      new dashboard_reporting_manager(context);
      
      var coordinates =  '<?php echo (isset($coordinates) && $coordinates['lat'] != NULL) ? json_encode($coordinates) : ''; ?>';
      
      function get_user_current_coordinates(){
          // Try HTML5 geolocation.
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                  var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                  };
      
                  $.ajax({
                      type: 'POST',
                      url: base_url + 'admin/set_current_latlng',
                      datatype: 'json',
                      data: {lat: pos.lat,lng:pos.lng},
                      success: function (stat) {
                          console.log('Coordinates Saved Successfully');
                      }
                   });
              });
          }    
      }
      
      if(coordinates === ''){
          get_user_current_coordinates();
      }
      
      
   </script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
   <script src="<?php echo site_url(); ?>common/js/reports/job_sales.js?v=<?php echo version; ?>"></script>
   <script type="text/javascript">
      var context = {};
      context.user_id = '<?php echo $user_id; ?>';
      context.user_group = '<?php echo $user_group; ?>';
      context.all_reports_view_permission = '<?php echo $all_reports_view_permission; ?>';
      context.is_sales_rep = '<?php echo $is_sales_rep; ?>';
      var sales_reporting_manager = new sales_reporting(context);
      sales_reporting_manager.initialize_sales_report();
   </script>
</body>
</html>
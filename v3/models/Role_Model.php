<?php

class Role_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch franchise
     * @param type $id
     * @return array
     */
    function get_permission_by_roles($id = FALSE) {
        $allowed = 'IF(ptg.perm_id IS NULL,0,1)';
        $sql = "SELECT p.*,$allowed as allowed FROM aauth_perms as p";
        $sql .= " LEFT JOIN  (SELECT perm_id FROM aauth_perm_to_group WHERE group_id = '".$id."') as ptg ON ptg.perm_id = p.id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

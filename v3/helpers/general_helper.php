<?php
	
	// General output function
	if(!function_exists('pr')){

		function pr($data, $die = FALSE){
			echo "<pre>";
			print_r($data);
			echo "</pre>";
			if($die){
				die;
			}
		}
	}


	// Function to get all cost centers
	if(!function_exists('get_cost_centers')){

		function get_cost_centers(){
			$CI = &get_instance();

			$CI->jobcrm_db = $CI->load->database('jobcrm_db',TRUE);
			$cost_centers = $CI->jobcrm_db->where('status','1')->get('tbl_cost_centres')->result_array();
			return $cost_centers;
		}
	}


	// Function to get cost centers by id
	if(!function_exists('get_cost_center_by_id')){

		function get_cost_center_by_id($ccid){
			$CI = &get_instance();

			$CI->jobcrm_db = $CI->load->database('jobcrm_db',TRUE);
			$cost_center = $CI->jobcrm_db->where('ccid',$ccid)->get('tbl_cost_centres')->row_array();
			if(!empty($cost_center)){
				return $cost_center;
			}else{
				return FALSE;
			}
		}
	}


	// Function to get cost centers by id
	if(!function_exists('get_cost_center_by_type')){

		function get_cost_center_by_type($type, $select = ''){
			$CI = &get_instance();

			$CI->jobcrm_db = $CI->load->database('jobcrm_db',TRUE);

			if($select){
				$CI->jobcrm_db->select($select);
			}
			$CI->jobcrm_db->where('status','1');
			$CI->jobcrm_db->where('type',$type);
			$cost_centers = $CI->jobcrm_db->get('tbl_cost_centres');
			if($cost_centers->num_rows() > 0){
				return $cost_centers->result_array();
			}else{
				return FALSE;
			}
		}
	}

	
	// Function to get XERO Account code by cost center
	if(!function_exists('get_xero_account_code')){

		function get_xero_account_code($ccid){
			$CI = &get_instance();

			$CI->jobcrm_db = $CI->load->database('jobcrm_db',TRUE);
			$cost_center = $CI->jobcrm_db->where('ccid',$ccid)->get('tbl_cost_centres')->row_array();

			if(!empty($cost_center)){
				return $cost_center['xero_account_code'];
			}else{
				return FALSE;
			}
		}
	}
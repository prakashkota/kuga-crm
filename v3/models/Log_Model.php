<?php

class Log_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch issue details
     * @param type $id
     * @return array
     */
    function getIssueDetail($id = FALSE) {
        $sql   = "select 
                    i.*,
                    ud.full_name,
                    ud.company_name,
                    ud.company_contact_no,
                    au.email,
                    c.first_name,
                    c.last_name     ,
                    c.customer_contact_no,
                    c.customer_email
                from 
                    tbl_issues  as i
                    left join tbl_customers as c on c.id = i.cust_id
                    left join tbl_user_details as ud on ud.user_id = i.user_id
                    left join aauth_users as au on au.id = i.user_id

                where i.id = {$id}";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getIssueThread($issueId, $lastMessageId = 0) {
        $sql   = "select 
                    im.*,
                    ud.full_name
                from 
                    tbl_issues_messages as im
                    left join tbl_user_details as ud on ud.user_id = im.posted_by_user_id
                where im.issue_id = {$issueId} and (($lastMessageId<>0 and im.id>$lastMessageId) or $lastMessageId = 0) order by im.created_at asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function createMessageAgainstIssue($data) {
        $issueStatus = $data['status'];
        $issueId     = $data['issue_id'];
        unset($data['status']);
        $newRow      = $this->db->insert('tbl_issues_messages', $data);
        $sql         = "update tbl_issues set status = {$issueStatus} where id= {$issueId} ";
        $query       = $this->db->query($sql);
        return true;
    }

    public function threadEmailContent($issueId) {
        $issueData = $this->getIssueDetail($issueId);
        $thread    = $this->getIssueThread($issueId);
        return ['issue' => $issueData, 'thread' => $thread];
    }
    
    /**
     * Fetch Log Issues
     * @param type $id
     * @param type $status
     * @return array
     */
    function get_log_issues($userId = false,$status = FALSE) {
        $sql = "SELECT li.*,"
                . "u.email,u.id as user_id,"
                . "ud.full_name as franchise_name,"
                . "ud.company_name as franchise_company_name"
                . " FROM tbl_issues as li";
        $sql .= " LEFT JOIN aauth_users as u ON u.id = li.user_id";
        $sql .= " LEFT JOIN tbl_user_details as ud ON ud.user_id = u.id";
        if($userId != FALSE && $status != FALSE){
            $sql .= " where li.user_id='" . $userId . "' && li.status IN ($status)";
        }else if($userId == FALSE && $status != FALSE){
            $sql .= " where li.status IN ($status)";
        }else if($userId != FALSE && $status != FALSE){
            $sql .= " where li.user_id='" . $userId . "'";
        }
        
        
        $sql   .= " ORDER BY li.id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}

<style>
    .page-wrapper {background-color: #ECEFF1; min-height: 100%;}
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
    .sr-inactive_user{color:red;}
    .tooltip {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 12px;
        font-style: normal;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: left;
        text-align: start;
        text-decoration: none;
        text-shadow: none;
        text-transform: none;
        letter-spacing: normal;
        word-break: normal;
        word-spacing: normal;
        word-wrap: normal;
        white-space: normal;

    }
</style>
<body>
    <!--pagewrapper-->
    <div class="page-wrapper d-block clearfix">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Simpro Reporting</h1>
            <div >
                <form id="reporting_actions" >

                    <?php if ($this->aauth->is_member('Admin') || $this->components->is_team_leader()) { ?>
                        <button class="btn btn-default" type="button" id="reportrange" >
                            <i class="fa fa-calendar"></i> &nbsp; <span></span> <i class="fa fa-caret-down" ></i>
                        </button>
                        <input type="hidden" name="start_date" id="start_date" value="" />
                        <input type="hidden" name="end_date" id="end_date" value="" />
                        <div style="display:inline-block;">
                            <select class="form-control" name="state_id" id="state_id">
                                <option value="">Select State</option>
                                <?php $states = unserialize(STATE_FILTER);
                                foreach ($states as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div  style="display:inline-block;">
                            <select class="" name="userid[]" id="userid" multiple="multiple">
                                <?php foreach ($users as $key => $value) { ?>
                                    <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    <?php } else if ($all_reports_view_permission) { ?>
                        <div class="btn-group col-md-8" role="group" >
                            <div id="reportrange" >
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                            <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                        <div  class="float-md-right col-md-4" >
                            <select class="" name="userid" id="userid">
                                <option value="">Select User</option>
                                <?php foreach ($users as $key => $value) { ?>
                                    <option class="<?php if ($value['banned'] == 1) { ?>sr-inactive_user hidden<?php } ?>"  value="<?php echo $value['user_id']; ?>"><?php echo $value['full_name']; ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    <?php } else { ?>
                        <div class="btn-group float-md-right" role="group" >
                            <div id="reportrange" >
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="start_date" id="start_date" value="<?php echo date('Y-m-d', strtotime('-29 days')); ?>" />
                            <input type="hidden" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" />
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>


        <div class="container hidden" id="error_container">
            <div class="row">
                <div class="col-md-12">
                    <div class="error-template">
                        <div class="error-details">
                            Sorry, you don't have permissions to access reports!
                        </div>
                        <div class="error-actions">
                            <a href="proposalManage.php" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-pencil"></span>
                            Manage proposal </a><a href="mailto:service@solarrun.com.au" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="statistics_container">
            <?php if ($this->aauth->is_member('Admin') || $this->components->is_team_leader() || $all_reports_view_permission == 1) { ?>        

                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-body">
                                <h2>
                                    Sales Statistics
                                    <a href="#" data-toggle="tooltip" title="Statistics for each Salesperson." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                </h2>
                                <div id="sales_statistics_chart_container"></div>
                                <div  class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Sales Rep</th>
                                                <th>State</th>
                                                <th>Total Proposals</th>
                                                <th>LED Proposals</th>
                                                <th>Solar Proposals</th>
                                                <th>Conversion Rate</th>
                                                <th>Won Deals</th>
                                                <th>kWs Sold</th>
                                                <th>Highbays Sold</th>
                                                <th>Floodlights Sold</th>
                                                <th>Panels Sold</th>
                                                <th>Battens Sold</th>
                                            </tr>
                                        </thead>
                                        <tbody id="sales_rep_performance_report_table_body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
    <script src="<?php echo site_url(); ?>common/js/reports/simpro.js?v=<?php echo version; ?>"></script>


    <script type="text/javascript">
        var context = {};
        context.user_id = '<?php echo $user_id; ?>';
        context.user_group = '<?php echo $user_group; ?>';
        context.all_reports_view_permission = '<?php echo $all_reports_view_permission; ?>';
        var sales_reporting_tool = new sales_reporting(context);

    </script>
</body>
</html>


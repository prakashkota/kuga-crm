<?php /* <div class="login-wrapper d-block clearfix">
  <div class="login-panel d-block vertical-middle clearfix">
  <div class="logo text-center"><img src="<?php echo site_url();?>assets/images/logo.svg" alt="logo"></div>
  <h2>Login with your account</h2>

  <form role="form" action="" method="post" name="frm" >
  <div class="login-form-block text-center d-block clearfix">
  <?php $message = $this->session->flashdata('message');
  echo (isset($message) && $message != NULL) ? $message : ''; ?>
  <div class="form-body d-block clearfix">
  <div class="row">
  <div class="col-12">
  <div class="form-group">
  <input type="text" name="email" id="email"  class="form-control" placeholder="Email" value="<?php echo (isset($email)) ? $email : ''; ?>">
  <?php echo form_error('email', '<div class="text-left text-danger">', '</div>'); ?>
  </div>
  </div>
  <div class="col-12">
  <div class="form-group">
  <input type="password" name="password" id="password"  class="form-control" placeholder="Password">
  <?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
  </div>
  </div>
  <div class="col-12">
  <button type="submit" class="btn">Log in</button>
  </div>
  </div>
  </div>
  </div>
  </form>
  <!-- <a href="#" class="forgot">Forgot password.</a> -->
  </div>
  </div>
  <??php */ ?>
<style>
    .clearfix:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }
    .login-box-outer{ height:100vh; position:relative;}
    .login-box{ position:absolute; top:50%; right:0; left:0; max-width:800px; margin:0 auto; transform:translateY(-50%);}
    .login-logo{width: 39%;

                float: left;

                padding-top: 95px;

                border: 1px solid #ccc;

                padding-left: 50px;

                min-height: 232px;

                text-align: center;

                padding-right: 50px;}
    .login-fild {width: 60%;float: right;border: solid 1px #dadada;}
    .login-fild h2 {display: block;margin: 0;padding: 10px 20px;background: #f5f5f5;border-bottom: solid 1px #dadada;font-size: 18px;}
    .form-box{ padding:20px 20px;}
    .form-box .form-group{ position:relative; margin-bottom:15px;}
    .input-group-addon{background:#eeeeee;display:block;position:absolute;top:1px;bottom:1px;left:1px;border-radius:3px 0 0 3px;border-right:solid 1px #dcdcdc;}
    .form-controlInt{ width:100%; border:none;height: 38px;border: solid 1px #dcdcdc; border-radius:3px; padding:0 0 0 50px; box-sizing:border-box;}
    .login-btn{ background:#428bca; border:none; color:#fff; font-size:16px; padding:10px 20px; cursor:pointer;border-radius:3px;}
    .text-left.text-danger {position: absolute;bottom: 11px;right: 10px;font-style: italic;font-size: 11px;letter-spacing: 1px;}

    @media only screen and (max-width:767px){
        .login-box {padding:25px 15px;transform: inherit; position: static;}	
        .login-fild {width:100%;}
        .login-logo {width: 100%;text-align: center;min-height: auto;padding: 15px;margin-bottom: 15px;border: none;}
        .form-box .form-group {margin-bottom: 30px;}
        .text-left.text-danger {right: auto;left: 38px;bottom: -17px;}
    }
</style>
<div class="login-box-outer">
    <div class="login-box clearfix">
        <?php $message = $this->session->flashdata('message');
        echo (isset($message) && $message != NULL) ? $message : '';
        ?>
        <div class="login-logo"><img src="<?php echo site_url(); ?>assets/images/logo.png" alt="logo"/></div>
        <div class="login-fild">
            <h2>Reset Password</h2>
            <form role="form" action="" method="post" name="frm" >
                <div class="form-box clearfix">
                    <div class="form-group">
                        <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/lock-icon.png" alt="lock-icon"/></span>
                        <input type="password" placeholder="Password" name="password" id="password"  class="form-controlInt">
<?php echo form_error('password', '<div class="text-left text-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                        <span class="input-group-addon"><img src="<?php echo site_url(); ?>assets/images/lock-icon.png" alt="lock-icon"/></span>
                        <input type="password" placeholder="Confirm Password" name="c_password" id="c_password"  class="form-controlInt">
<?php echo form_error('c_password', '<div class="text-left text-danger">', '</div>'); ?>
                    </div>
                    <div class="row">
                        <span class="col-md-6"> <input type="submit" class="login-btn" value="Submit"> </span>
                        <span class="col-md-6 text-right"> <a href="<?php echo site_url('admin/login'); ?>">login</a> </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
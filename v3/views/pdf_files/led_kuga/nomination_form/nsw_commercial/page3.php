<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/header.jpg" width="910" height="127" alt=""/></td>
			</tr>
			<tr>
				<td height="35"></td>
			</tr>
			<tr>
				<td valign="top"><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 4 - Nomination by the original energy saver</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:10px"></td>
						</tr>
						<tr>
							<td height="50"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="170"style="font-size:12px; color:#000000;">I, the original energy saver,</td>
										<td width="632"><input type="text" name="get_nomination_form_details[energy_saver_details][name]" value="<?php echo $energy_saver_details->name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:250px; height:35px; padding-left:5px; padding-right:5px"/> <span style="font-size:12px; color:#000000;">nominate <b>Kuga Electrical Pty Ltd</b> as the energy saver in</span> </td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">respect of the activity or activities at the implementation sites listed above.</td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">For the purpose of creating Energy Savings Certificates in the ESS, this nomination entitles Kuga Electrical Pty Ltd to create and own all<br />
								energy savings in respect of each of the above activities at the implementation sites listed above, once they have been completed and<br />
							energy savings have commenced.</td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">This nomination takes effect from the signature date below, and remains in force until the date this nomination is revoked by the original<br />
							energy saver.</td>
						</tr>
						<tr>
							<td height="20"></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">I hereby declare that:</td>
						</tr>

						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="16"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td width="784" style="font-size:12px; color:#000000;">I have received the contact details of the Accredited Certificate Provider.</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td valign="top" style="font-size:12px; color:#000000;">Except as set out above, I have not previously nominated an energy saver under the NSW Energy Savings Scheme (ESS) in respect of the<br />
										activity or activities at the implementation sites listed above and neither, to the best of my knowledge, has any other person.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td style="font-size:12px; color:#000000;">I agree to provide information regarding the above activities and provide reasonable access to the implementation sites to a<br />
										representative of the ESS or a member of the ESS Audit Services Panel, upon request.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot-space.jpg" width="4" height="7" alt=""/></td>
										<td style="font-size:12px; color:#000000;">The date recorded below is the date when I signed this form.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td style="font-size:12px; color:#000000;">The information provided in this nomination form is accurate and is not misleading by inclusion or omission.</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 3px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot-space.jpg" width="4" height="7" alt=""/></td>
										<td style="font-size:12px; color:#000000;">I am aware that this nomination form contains information that may be provided to the Scheme Administrator and that there are<br />
										penalties for providing false or misleading information to the Scheme Administrator.</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td style="font-size: 12px; color: #000000"><strong>Note:</strong></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly<br />
							providing false or misleading information to the Scheme Administrator.</td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td style="font-size: 12px; color: #000000"><strong>Signed by the original Energy Saver’s Authorised signatory</strong></td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td valign="top"><table align="left" width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top"><table  align="left"width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Signature</td>
												</tr>
												<tr>
													<td  valign="top" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height: 70px; ">
														<img style="width:370px; height:70px;" src="<?php echo site_url('assets/uploads/led_booking_form_files/'.$authorised_details->signature); ?>" width="70px" />
													</td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Name</td>
												</tr>
												<tr>
													<td><input type="text" name="get_nomination_form_details[authorised_details][name]" value="<?php echo $authorised_details->name;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td>
											<table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td style="font-size:12px; color:#6a6a6a; height:20px">Position</td>
													</tr>
													<tr>
														<td><input name="get_nomination_form_details[authorised_details][position]" type="text"  value="<?php echo $authorised_details->position;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td style="font-size:12px; color:#6a6a6a; height:20px">Date</td>
													</tr>
													<tr>
														<td><input name="get_nomination_form_details[authorised_details][date]" type="text"  value="<?php echo $authorised_details->date;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; padding-top:5px;">*This form must be signed <span style="font-style: italic">on or before</span> the installation date</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
					</tbody>
				</table></td>
			</tr>
			<tr>
				<td height="264"></td>
			</tr>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/bottom.jpg" width="910" height="102" alt=""/></td>
			</tr>
		</tbody>
	</table>
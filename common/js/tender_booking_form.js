
var tender_booking_form_manager = function (options) {
    var self = this;
    this.lead_data = {};
    this.lead_uuid = (options.lead_uuid && options.lead_uuid != '') ? options.lead_uuid : {};
    this.booking_form_uuid = (options.booking_form_uuid && options.booking_form_uuid != '') ? options.booking_form_uuid : '';
    this.siteId = (options.siteId && options.siteId != '') ? options.siteId : '';
    this.tenderId = (options.tenderId && options.tenderId != '') ? options.tenderId : '';
    this.gst = (options.gst && options.gst != '') ? parseFloat(options.gst) : 1.1;
    this.product_subtotal_excGST = 0;
    this.ae_subtotal_excGST = 0;
    this.total_incGST = 0;
    this.total_excGST = 0;
    this.total_GST = 0;
    this.additional_images_block_count = 1;
    this.costCenters = options.costCenters;
    this.siteData = options.siteData;
    $("#start_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });


    $("#completion_date").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });

    $("#booked_at").datepicker({
        format: 'dd/mm/yyyy',
        startDate: new Date()
    });
    $('input[name="contractInformation[eb_information]"]').click(function(){

        $('#edInformationSection1,#edInformationSection2,#ebillImages').hide();
        if($(this).val() == 'yes'){
            $('#edInformationSection1,#edInformationSection2,#ebillImages').show();
        }
    });

    $('#retention').click(function(){
        if($(this).is(':checked')){
            $('td#retentionTd').show();
        }else{
            $('td#retentionTd').hide();
        }
    });

    $('#bank').click(function(){
        if($(this).is(':checked')){
            $('td#bankTd').show();
        }else{
            $('td#bankTd').hide();
        }
    });
    $(document).on('click','.remove_add_more_images_page_btn',function(){
        var id = $(this).attr('data-id');
        var image_id1 = $('#additional_image_'+id+'_image1').parent().next('div').next('div').children('.action_delete_1').attr('data-media_id');
        var image_id2 = $('#additional_image_'+id+'_image2').parent().next('div').next('div').children('.action_delete_1').attr('data-media_id');


        if(image_id1 == undefined && image_id2 == undefined ){
            $('#add_more_image_block_'+id).remove();
        }else{
            toastr['info']('Removing Page in Progress...');

            $('#additional_image_'+id+'_image1').parent().prev('div').attr('style', 'display:block !important;');
            $('#additional_image_'+id+'_image1').parent().prev('div').children('input').val('');
            $('#additional_image_'+id+'_image1').parent().attr('style', 'display:none !important;');
            $('#additional_image_'+id+'_image1').parent().children('.add-picture').css('background-image', '');
            $('#additional_image_'+id+'_image1').parent().children('.add-picture').css('background-repeat', '');
            $('#additional_image_'+id+'_image1').parent().children('.add-picture').css('background-position', '');
            $('#additional_image_'+id+'_image1').parent().children('.add-picture').css('background-size', '');


            $('#additional_image_'+id+'_image2').parent().prev('div').attr('style', 'display:block !important;');
            $('#additional_image_'+id+'_image2').parent().prev('div').children('input').val('');
            $('#additional_image_'+id+'_image2').parent().attr('style', 'display:none !important;');
            $('#additional_image_'+id+'_image2').parent().children('.add-picture').css('background-image', '');
            $('#additional_image_'+id+'_image2').parent().children('.add-picture').css('background-repeat', '');
            $('#additional_image_'+id+'_image2').parent().children('.add-picture').css('background-position', '');
            $('#additional_image_'+id+'_image2').parent().children('.add-picture').css('background-size', '');

        }
    });

    $('#add_more_additional_images_page_btn').click(function(){
        self.additional_images_block_count++;
        self.create_additional_images_page();
    });

    $('#booking_form').click(function () {
        if (Object.keys(self.siteData).length > 0) {

            var cost_centre_select_label = document.createElement('label');
            cost_centre_select_label.innerHTML = "Cost Centre";
            cost_centre_select_label.className = "control-label";
            cost_centre_select_label.setAttribute('style','width:150px;');

            var cost_centre_select = document.createElement('select');
            cost_centre_select.className = 'form-control';
            cost_centre_select.setAttribute('id','cost_centre');

            if(Object.keys(self.costCenters).length > 0 ){
                $.each(self.costCenters,function(i,opArr){
                var option = document.createElement('option');
                option.setAttribute('value',opArr.ccid);
                option.innerHTML = opArr.name;
                cost_centre_select.appendChild(option)});
            }

            document.querySelector('#tender_booking_form_cost_centre_body').innerHTML = '';
            document.querySelector('#tender_booking_form_cost_centre_body').appendChild(cost_centre_select_label);
            document.querySelector('#tender_booking_form_cost_centre_body').appendChild(cost_centre_select);
            $('#tender_booking_form_site_table_body').html('');
            for (var key in self.siteData) {
                if (Object.keys(self.siteData[key]).length > 0) {
                    if (self.siteData[key].address == '' || self.siteData[key].address == null || self.siteData[key].address == 'null') {
                        continue;
                    }
                    var row = self.create_tender_site_table_row(self.siteData[key]);
                    var tbody = document.querySelector('#tender_booking_form_site_table_body');
                    tbody.appendChild(row);
                }
            }

            $('#booking_form_choose_customer_location_modal').modal('show');
        } else {
            toastr["error"]('Oops, Please Add Sites then proceed to create a booking form.');
        }
    });

    $('#prd_panel').change(function () {
        var id = $(this).find(':selected').attr('data-id');
        $('.product_id_1').val(id);
        var data_item = $(this).find(':selected').attr('data-item');
        data_item = data_item != null && data_item != '' ? JSON.parse(data_item) : '';
        if(data_item != ''){
            $('.product_capacity_1').val(data_item.capacity);
        }
    });

    $(document).on('click','a.redirectTobookingForm',function(){
        var siteId = $(this).attr('data-siteId');
        if(siteId > 0 ) {
            var tender_site_booking_url = base_url + 'admin/tender/booking-form?centerId='+$('#cost_centre :selected').val()+'&siteId=' +siteId+ '&type_ref=tender' + '&tenderId=' + options.tenderId;
            window.location.href = tender_site_booking_url;
        }

    });
    
    if (self.booking_form_uuid != '') {
        self.fetch_booking_form_data();
    } else if (self.lead_uuid != '') {
        var data = {};
        data.uuid = self.lead_uuid;
        data.siteId = self.siteId;
       // self.fetch_lead_data(data);
    }

    self.initialize();
    self.handle_items();
    self.upload_image();
    self.show_image();
    self.hide_image();

};

tender_booking_form_manager.prototype.fetch_booking_form_data = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/tender/fetch_booking_form_data',
        datatype: 'json',
        data: {uuid: self.booking_form_uuid},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var booking_data = stat.booking_data;
                self.handle_booking_form_data(booking_data);
                if(booking_data.simpro_job_id != null && booking_data.simpro_job_id != ''){
                    $('.sr-deal_actions').hide();
                }else{
                   // self.initialize();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

tender_booking_form_manager.prototype.create_tender_site_table_row = function(data){
    var tr = document.createElement('tr');

    var td_site_name = document.createElement('td');
    td_site_name.innerHTML = data.name;
    tr.appendChild(td_site_name);
    var td_address = document.createElement('td');
    td_address.innerHTML = data.address;
    tr.appendChild(td_address);
    var td_action = document.createElement('td');
    td_action.className = 'text-center';
    var tender_site_booking_add_btn = document.createElement('a');
    tender_site_booking_add_btn.className = "redirectTobookingForm btn-primary btn-sm";
    var tender_site_booking_url = base_url + 'admin/tender/booking-form?centerId='+$('#cost_centre :selected').val()+'&siteId=' +data.id + '&type_ref=solar' + '&tenderId=' + this.tenderId;
    tender_site_booking_add_btn.setAttribute('href', "javascript:void(0);");
    tender_site_booking_add_btn.setAttribute('data-siteId', data.id);
    tender_site_booking_add_btn.innerHTML = '<i class="fa fa-plus"></i> Add';
    td_action.appendChild(tender_site_booking_add_btn);
    tr.appendChild(td_action);
    return tr;
}
tender_booking_form_manager.prototype.initialize = function () {
    var self = this;

    $('#save_booking_form').click(function () {
        self.save_booking_form();
    });

    $('#generate_booking_form_pdf').click(function () {
        self.save_booking_form(true);
    });

    $('.product_qty_panel,#prd_panel').change(function(){
        self.calculate_system_size();
    });



};

tender_booking_form_manager.prototype.fetch_lead_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/lead/fetch_lead_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading lead data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                self.lead_data = stat.lead_data;

                $('#first_name').val(self.lead_data.first_name);
                $('#last_name').val(self.lead_data.last_name);
                $('#contact_no').val(self.lead_data.customer_contact_no);
                $('#position').val(self.lead_data.position);
                $('#email').val(self.lead_data.customer_email);

                $('#entity_name').val(self.lead_data.customer_company_name);
                $('#address').val(self.lead_data.address);
                $('#select2-locationstreetAddress-container').html(self.lead_data.address);
                $('#postcode').val(self.lead_data.postcode);

                $('#authorised_on_behalf_company_name').val(self.lead_data.customer_company_name);
                $('#authorised_on_behalf_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
                $('#authorised_on_behalf_position').val(self.lead_data.position);

                $("#authorised_on_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
                $("#authorised_by_behalf_date").val(moment(new Date()).format('DD/MM/YYYY'));
                $("#booked_at").val(moment(new Date()).format('DD/MM/YYYY'));

                self.initialize();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

tender_booking_form_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

tender_booking_form_manager.prototype.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

tender_booking_form_manager.prototype.handle_items = function () {
    var self = this;
    $('.product_qty,.product_cost_excGST').change(function () {
        self.product_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        $('table#booking_items tr').each(function () {
            var price = $(this).children().find(".product_cost_excGST").val();
            var qty = $(this).children().find(".product_qty").val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(this).children().find(".product_total_cost_excGst").val(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST) + parseFloat(total);
                self.product_subtotal_excGST = parseFloat(self.product_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#product_total_excGst').val(self.product_subtotal_excGST);
                $('#total_incGst').val(self.total_incGST);
            }
        });
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST);
        $('#total_incGst').val(self.total_incGST);
        $('#total_Gst').val(self.total_GST);
    });

    $('.ae_qty,.ae_cost_excGST').change(function () {
        self.ae_subtotal_excGST = 0;
        self.total_excGST = 0;
        self.total_incGST = 0;
        for (var i = 0; i < 2; i++) {
            var price = $(".ae_cost_excGST_" + i).val();
            var qty = $(".ae_qty_" + i).val();
            var total = price * qty;
            if (total != '' && total != 0 && !isNaN(total)) {
                $(".ae_total_cost_excGst_" + i).val(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST) + parseFloat(total);
                self.ae_subtotal_excGST = parseFloat(self.ae_subtotal_excGST).toFixed(2);
                //self.total_incGST = self.product_subtotal_excGST * self.gst;
                //self.total_incGST = parseFloat(self.total_incGST).toFixed(2);
                $('#ae_total_excGst').val(self.ae_subtotal_excGST);
            }
        }
        self.total_excGST = (parseFloat(self.product_subtotal_excGST) + parseFloat(self.ae_subtotal_excGST)).toFixed(2);
        self.total_incGST = (parseFloat(self.total_excGST) * self.gst).toFixed(2);
        self.total_GST = (self.total_incGST - self.total_excGST).toFixed(2);
        $('#total_excGst').val(self.total_excGST);
        $('#total_incGst').val(self.total_incGST);
        $('#total_Gst').val(self.total_GST);
    });


};

tender_booking_form_manager.prototype.upload_image = function () {
    var self = this;
    $(document).on('change','.image_upload',function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'tender_booking_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

tender_booking_form_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/tender_booking_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

tender_booking_form_manager.prototype.hide_image = function () {
    var self = this;
    $(document).on('click','.image_close',function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};

tender_booking_form_manager.prototype.validate_booking_form = function () {
    var self = this;
    var flag = true;
    var eleArr = [];
    $('.invalid-feedback').remove();
    var elements = document.getElementById("tender_booking_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        $(elements[i]).parent().removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
                eleArr.push($(elements[i]).parent());
            } else {
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                        eleArr.push($(elements[i]).parent());
                    }
                } else if (elements[i].getAttribute('type') == 'checkbox') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                        eleArr.push($(elements[i]).parent().parent());
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }

    if(eleArr.length > 0){
        $('html, body').animate({
            scrollTop: eleArr[0].offset().top - 70
        }, 2000);
    }
    
   // flag = (payment_option_flag == false) ? false : flag;

    return flag;
};

tender_booking_form_manager.prototype.save_booking_form = function (download) {

    var self = this;
    if(download){
        var flag = self.validate_booking_form();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Please fill the fields marked in red');
            return false;
        }
    }

    if (self.booking_form_uuid == '') {
        var uuid = self.create_uuid();
        self.booking_form_uuid = uuid;
    }

    var formData = $('#tender_booking_form').serialize();
    formData += '&uuid=' + self.booking_form_uuid;
    formData += '&tenderId=' + self.tenderId;
    formData += '&siteId=' + self.siteId;

    $.ajax({
        url: base_url + 'admin/tender/save_booking_form',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]('Saving Form Data, Please Wait....');
            $("#save_booking_form").attr("disabled", "disabled");
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                self.bookId = response.id;
                //Add Quote ref in url
                if (self.booking_form_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/tender/booking-form/' + self.booking_form_uuid);
                }
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                }else{
                    toastr["success"](response.status);
                    setTimeout(function(){
                        window.location.href = base_url + 'admin/tender/booking-form/' + self.booking_form_uuid
                    },1500);
                }
            } else {
                $("#save_booking_form").removeAttr("disabled");
                if (download && self.booking_form_uuid != '') {
                    self.save_booking_form_to_simpro();
                }else{
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_customer").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


tender_booking_form_manager.prototype.handle_booking_form_data = function (data) {
    var self = this;

    //Handle Business Details
    var business_details = JSON.parse(data.business_details);
    for (var key in business_details) {
        if(key == 'leased_or_owned' || key == 'bca_hours'){
            $("#"+key).val(business_details[key]);
        }else if (key == 'is_firewall') {
            $('#is_firewall_' + business_details[key]).attr('checked', 'checked');
        }else{
            if($("#"+key)){
              $("#"+key).val(business_details[key]);  
            }else{
              $("input[name='business_details[" + key + "]']").val(business_details[key]);
            }
            
            if(key == 'address'){
                $('#select2-locationstreetAddress-container').html(business_details[key]);
            }
        }
    }

    var contacts = JSON.parse(data.contacts);
    for (var mainKey in contacts) {
       for(var key in contacts[mainKey]){
            $("input[name='contacts["+mainKey+"][" + key + "]']").val(contacts[mainKey][key]);
        }
    }
    //alert(data.contractInformation);
    var contractInformation = JSON.parse(data.contractInformation);

    for (var mainKey in contractInformation) {
        if($("input[name='contractInformation["+mainKey+"]']").attr('type') == 'checkbox'){
            $("input[name='contractInformation["+mainKey+"]']").attr('checked',true);
            $("#"+mainKey+"Td").show();
        }else if($("input[name='contractInformation["+mainKey+"]']").attr('type') == 'radio'){
            $('#edInformationSection1,#edInformationSection2').hide();
            $("input[name=mygroup][value=" + contractInformation[mainKey] + "]").attr('checked', 'checked');
           if(contractInformation[mainKey] == 'yes'){
               $('#edInformationSection1,#edInformationSection2').show();
           }
        } else {
            $("input[name='contractInformation[" + mainKey + "]']").val(contractInformation[mainKey]);
        }
    }


    //Handle Electricity Details
    var electricity_bill = JSON.parse(data.electricity_bill);
    for (var key in electricity_bill) {
            $("input[name='electricity_bill[" + key + "]']").val(electricity_bill[key]);
    }


    //Handle Product
    var product = JSON.parse(data.product);
    for (var key in product) {
        for (var eachType in product[key]) {
            $(".product_name_" + key).val(product[key]['product_name']).trigger('change');
            $(".product_qty_" + key).val(product[key]['product_qty']).trigger('change');
            $(".product_total_cost_excGst_" + key).val(product[key]['product_total_cost_excGst']).trigger('change');
            $(".product_cost_excGST_" + key).val(product[key]['product_cost_excGST']).trigger('change');
        }
    }
    $('.product_qty').trigger('change');


    //Handle images
    var booking_form_image = JSON.parse(data.booking_form_image);
    for (var key in booking_form_image) {
        if (key!='more' && booking_form_image[key] != '' ) {
            $("#" + key).val(booking_form_image[key]);
            self.show_image($("#" + key), booking_form_image[key]);
        }else if(key == 'more' && Object.keys(booking_form_image[key]).length>0){
            for (var count in booking_form_image['more']){
                self.additional_images_block_count = count;
                self.create_additional_images_page();
                $("input[name='booking_form_image[more]["+count+"][name1]']").val(booking_form_image[key][count]['name1']);
                $("input[name='booking_form_image[more]["+count+"][image1]']").val(booking_form_image[key][count]['image1']);
                $("input[name='booking_form_image[more]["+count+"][name2]']").val(booking_form_image[key][count]['name2']);
                $("input[name='booking_form_image[more]["+count+"][image2]']").val(booking_form_image[key][count]['image2']);
                self.show_image($("#additional_image_"+count +"_image1"), booking_form_image[key][count]['image1']);
                self.show_image($("#additional_image_"+count +"_image2"), booking_form_image[key][count]['image2']);

            }
        }
    }
    var booking_form = JSON.parse(data.booking_form);
    for (var key in booking_form) {
        if (booking_form[key] != '') {
            $("input[name='booking_form[" + key + "]']").val(booking_form[key]);
        }
    }
    var notes = JSON.parse(data.notes);
    for (var key in notes) {
        if (notes[key] != '') {
            $("textarea[name='notes[" + key + "]']").val(notes[key]);
        }
    }



};

tender_booking_form_manager.prototype.create_additional_images_page = function () {
    var self = this;
    var html = '';
    var c = self.additional_images_block_count;
    html += '<br/><table id="add_more_image_block_'+c+'" width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="pdf_page">';
    html += '<tbody>';
    html += '<tr>';
    html += '<td><img src="'+base_url+'assets/tender_booking_form/header_02.png" width="910" height="121" alt=""/></td>';
    html += '</tr>';
    html += '<tr><td height="25"></td></tr><tr align="center">';
    html += '<td><span><a data-id="'+c+'" href="javascript:void(0);" class="remove_add_more_images_page_btn btn btn-kuga mb-2"><i class="fa fa-close"></i> Remove This Page</a></span></td>';
    html += '</tr>'
    html += '<tr>';
    html += '<td height="25"></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td valign="top" align="center" ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="margin0">';
    html += '<tbody>';
    html += '<tr>';
    html += '<td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="margin0">';
    html += '<tbody>';
    html += '<tr>';
    html += '<td height="35" align="center" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong><input type="text" value="Additional Image" name="booking_form_image[more]['+c+'][name1]" /></strong></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td width="830" align="center" valign="top" style="padding:13px 0">';
    html += '<div class="add-picture">  <span>Add picture</span><i><img src="'+base_url+'assets/images/small-plus.png"></i> <input type="file" class="image_upload" data-id="additional_image_'+c+'_image1" ><input type="hidden" id="additional_image_'+c+'_image1" name="booking_form_image[more]['+c+'][image1]"></div>'
    html += '<div class="form-block d-block clearfix image_container" style="display:none !important; "> <div class="add-picture"></div> <a class="image_close" href="javscript:void(0);"></a> </div>';

    html += '</td>';
    html += '</tr>';
    html += '</tbody>';
    html += '</table></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td>&nbsp;</td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td align="center" ><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">';
    html += '<tbody>';
    html += '<tr>';
    html += '<td height="35" align="center" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong><input type="text" value="Additional Image" name="booking_form_image[more]['+c+'][name2]" /></strong></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td width="830" align="center" valign="top" style="padding:13px 0">';
    html += '<div class="add-picture">  <span>Add picture</span><i><img src="'+base_url+'assets/images/small-plus.png"></i> <input type="file" class="image_upload" data-id="additional_image_'+c+'_image2" ><input type="hidden" id="additional_image_'+c+'_image2" name="booking_form_image[more]['+c+'][image2]" /></div>'
    html += '<div class="form-block d-block clearfix image_container" style="display:none !important; "> <div class="add-picture"></div> <a class="image_close" href="javscript:void(0);"></a> </div>';
    html += '</td>';
    html += '</tr>';
    html += '</tbody>';
    html += '</table></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td height="30"></td>';
    html += '</tr>';
    html += '</tbody>';
    html += '</table></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td><img src="'+base_url+'assets/tender_booking_form/bottom.png" width="910" height="87" alt=""/></td>';
    html += '</tr>';
    html += '</tbody>';
    html += '</table>';


    $('#extra_additional_image_container').append(html);
};
tender_booking_form_manager.prototype.calculate_system_size = function () {
    var self = this;

    var prd_panel = document.getElementById('prd_panel');
    var prd_panel_data = null;
    if (prd_panel.value != '' && prd_panel.value != undefined) {
        prd_panel_data = JSON.parse(prd_panel.options[prd_panel.selectedIndex].getAttribute('data-item'));
    }
    var rt_price = parseFloat($('.product_qty_panel').val());
    var prd_panel_watt = (prd_panel_data !== null && prd_panel_data !== undefined) ? parseFloat(prd_panel_data.capacity) : 0;
    //Get System Size
    rt_price = parseFloat(rt_price) * parseFloat(prd_panel_watt / 1000);

    var  system_size = (rt_price == 0 || isNaN(rt_price)) ? parseFloat(0).toFixed(2) : parseFloat(rt_price).toFixed(2);

    self.system_size = system_size;
    $('.product_name_systemSize').val(system_size);

    return system_size;
};

tender_booking_form_manager.prototype.save_booking_form_to_simpro = function () {
    var self = this;
    var url = base_url + 'admin/tender/booking-form-download/' + self.booking_form_uuid;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]("Saving Data to Job CRM. Please Wait...");
            $("#save_booking_form").attr("disabled", "disabled");
            $("#generate_booking_form_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                setTimeout(function(){
                    window.location.href = base_url + 'admin/tender/booking-form/'+self.booking_form_uuid;
                },1500);
            } else {
                toastr["error"](response.status);
            }
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_booking_form").removeAttr("disabled");
            $("#generate_booking_form_pdf").removeAttr("disabled");
            toastr.remove();
            //Getting this weird issue but still boking form was completed save on simpro 
            if(xhr.statusText == "OK"){
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function(){
                    window.location.href = base_url + 'admin/tender/booking-form/'+self.booking_form_uuid;
                },1500);
            }else{
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};


tender_booking_form_manager.prototype.resizeCanvas = function () {
    var self = this;

    var canvas1 = document.getElementById("signature_pad_1");
    var canvas2 = document.getElementById("signature_pad_2");
    var canvas3 = document.getElementById("signature_pad_3");

    var signaturePad1 = self.signature_pad[0];
    var signaturePad2 = self.signature_pad[1];
    var signaturePad3 = self.signature_pad[2];

    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas1.width = canvas1.offsetWidth * ratio;
    canvas1.height = canvas1.offsetHeight * ratio;
    canvas1.getContext("2d").scale(ratio, ratio);
    signaturePad1.clear();

    canvas2.width = canvas2.offsetWidth * ratio;
    canvas2.height = canvas2.offsetHeight * ratio;
    canvas2.getContext("2d").scale(ratio, ratio);
    signaturePad2.clear();

    canvas3.width = canvas3.offsetWidth * ratio;
    canvas3.height = canvas3.offsetHeight * ratio;
    canvas3.getContext("2d").scale(ratio, ratio);
    signaturePad3.clear();

};

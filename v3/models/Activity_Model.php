<?php

class Activity_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch activity 
     * @param type $id
     * @param type $lead_id
     * @param type $user_id
     * @param type $cond
     * @return array
     */
    function get_activities($id = FALSE,$lead_id = FALSE,$user_id = FALSE,$cond = FALSE) {

        $attachment_count = "(SELECT count(id) from tbl_activity_attachments taa where taa.activity_id=activity.id) as attachment_count";

        $attachment_select = "(SELECT GROUP_CONCAT(file_name ORDER BY created_at DESC SEPARATOR ',') from tbl_activity_attachments taa where taa.activity_id=activity.id) AS multi_attachments";

        $attachment_sizes_select = "(SELECT GROUP_CONCAT(file_size ORDER BY created_at DESC SEPARATOR ',') from tbl_activity_attachments taa where taa.activity_id=activity.id) AS multi_attachment_sizes";

        $attachment_owners = "(SELECT GROUP_CONCAT(tud.full_name ORDER BY created_at DESC SEPARATOR ',') from tbl_activity_attachments taa 
        LEFT JOIN tbl_user_details tud ON taa.user_id=tud.user_id WHERE taa.activity_id=activity.id) AS multi_attachment_owners";

        $attachment_dates = "(SELECT GROUP_CONCAT(created_at ORDER BY created_at DESC SEPARATOR ',') from tbl_activity_attachments taa where taa.activity_id=activity.id) AS multi_attachment_dates";

        $attachment_ids = "(SELECT GROUP_CONCAT(id ORDER BY created_at DESC SEPARATOR ',') from tbl_activity_attachments taa where taa.activity_id=activity.id) AS multi_attachment_ids";

        $sql = "SELECT activity.*,"
                . "DATE_FORMAT(activity.created_at,'%d/%m/%Y') as created_at,"
                . "DATE_FORMAT(activity.scheduled_date,'%d/%m/%Y') as scheduled_date,"
                . "DATE_FORMAT(activity.scheduled_time,'%h:%i %p') as scheduled_time,"
                . "activity.created_at as created_at_calendar,"
                . "activity.scheduled_date as scheduled_date_calendar,"
                . "activity.scheduled_time as scheduled_time_calendar,"
                . "user1.full_name as created_by,"
                . "user2.full_name as assigned_to,"
                . "cust.first_name,cust.last_name,cust.customer_contact_no,cust.company_name,"
                . "cl.address as customer_address,cl.postcode as customer_postcode,cl.state_id as customer_state_id,"
                . "lead.lead_stage,lead.uuid,lead.lead_source,"
                . "$attachment_count,$attachment_select,$attachment_sizes_select,$attachment_owners,$attachment_dates,$attachment_ids"
                . " FROM tbl_activities as activity";
        $sql .= " LEFT JOIN tbl_leads as lead ON lead.id = activity.lead_id";
        $sql .= " LEFT JOIN tbl_lead_to_user as ltu ON lead.id = ltu.lead_id";
        $sql .= " LEFT JOIN tbl_user_details as user ON user.user_id = activity.user_id";
        $sql .= " LEFT JOIN tbl_user_details as user1 ON user1.user_id = lead.user_id";
        $sql .= " LEFT JOIN tbl_user_details as user2 ON user2.user_id = ltu.user_id";
        $sql .= " LEFT JOIN tbl_customers as cust ON cust.id = lead.cust_id";
        $sql .= " LEFT JOIN tbl_customer_locations as cl ON cl.cust_id = cust.id";
        if($id){
            $sql .= " WHERE activity.id='".$id."'";
        }
        if($lead_id){
            $sql .= " WHERE activity.lead_id='".$lead_id."'";
        }
        if($user_id){
            $sql .= " WHERE activity.user_id='".$user_id."'";
        }
        if($cond){
            $sql .= " WHERE $cond";
        }
        
        $sql .= " GROUP BY activity.id ORDER BY activity.created_at DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}

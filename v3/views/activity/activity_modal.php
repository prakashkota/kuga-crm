<link href='<?php echo site_url(); ?>assets/dropzone/dropzone.min.css' type='text/css' rel='stylesheet'>
<script src="<?php echo site_url(); ?>assets/dropzone/dropzone.min.js" type="text/javascript"></script>
<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=q4hj2x73ddhzsvx7zf9mpc8qda8k4e8upc7jmzi6mm158ej9"></script>



<style>
    .am-card__body{
        display: inline-flex;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
    }
    .is-invalid{
        border: 1px solid #dc3545;
    }
    .am-scheduler{
        display:block;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
        padding: 10px;
        padding-bottom: 0px;
        width:100%;
    }

    .am-attendees{
        display:block;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
        margin-top: 10px !important;
        padding: 10px;
        padding-bottom: 0px;
        width: 100%;
    }

    .am-scheduler__item{
        width:100%;
    }
    
    @media only screen and (min-width: 992px){
        .modal-content{width:700px;}
    }
    
    @media only screen and (max-width: 991px){
        /**.modal-content{width:360px;}*/
    }

    @media only screen and (max-width: 764px){
       .am-scheduler{
        display:block;
        width: 100%;
        background-color: #ECEFF1;
        margin-bottom: 0px !important;
        padding: 10px;
        padding-bottom: 0px;
    }
    .am-scheduler__item{
     width:100%;
 }
 #scheduled_time,#scheduled_duration{margin-left:0px;}
}
.form-block{margin-bottom: 0px !important;}
.mark_as_completed{
    position: absolute;
    margin: 10px;
    left: 0;
}
</style>

<div class="modal" id="activity_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <form role="form"  action="" method="post"  id="activity_add_form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $activity_modal_title; ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="form-block d-block clearfix">
                            <input type="hidden" name="activity[id]" id="activity_id" class="form-control" >

                            <div class="row">
                                <div id="activityDiv" class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span>Title</span>
                                                <input class="form-control" placeholder="CALL" type="text" name="activity[activity_title]" id="activity_title" value=""  required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row hidden" id="activity_location">
                                        <div class="col-md-12" >
                                            <div class="form-group control-group">
                                                <span class="control-span"><i class="fa fa-map-marker"></i> Address</span>
                                                <select class="select2 placecomplete form-control" id="activityAddress"  style="height:48px !important;"></select>
                                                <input type="hidden" id="activityLocationAddressVal" name="activity[address]" value="" />
                                                <input type="hidden" id="activityLocationPostCode" name="activity[postcode]" value="" />
                                            </div>
                                            <select name="activity[state_id]" id="activityLocationState" class="form-control hidden" required>
                                                <option value="">Select State</option>
                                                <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                    <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span>Activity Type </span>
                                                <select name="activity[activity_type]"  class="form-control" id="activity_type"  required>
                                                    <option value="">Select Actitvity Type </option>
                                                    <?php
                                                    $activity_type = unserialize(activity_type);
                                                    foreach ($activity_type as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value; ?>" ><?php echo $value; ?></option>   
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span>Attachment </span>
                                                <div class="form-block d-block clearfix">
                                                    <div class="dropzone" id="lead_dropzone">
                                                        <div class="dz-message needsclick">
                                                            Drop files here or click to upload.<br>
                                                        </div>
                                                    </div>
                                                    <div id="activity_attachments"></div>
                                                    <div class="btn-block mt-2">
                                                        <input type="button" id="save_meter_data_btn" onclick="save_solar_proposal();" value="Save" class="btn btn-primary hidden">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span>Note </span>
                                                <textarea name="activity[activity_note]" id="activity_note" class="form-control" placeholder="Note"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row " id="scheduler">
                                        <div class="am-scheduler">

                                            <input id="is_scheduled" type="hidden" value="0">
                                            <div class="form-group" >
                                                <span style="display:block !important;"><i class="fa fa-calendar"></i> Date 
                                                    <a href="#" data-toggle="tooltip" title="Enter the date when the activity will take place." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                </span>
                                                <div class="input-group date" data-provide="datepicker" style="margin-bottom: 1.5rem;">
                                                    <input class="datepicker am-scheduler__item form-control" name="activity[scheduled_date]" id="scheduled_date_value" value="<?php echo date('d-m-Y'); ?>">
                                                    <div class="input-group-addon hidden">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="scheduled_time">
                                                <span style="display:block !important;"><i class="fa fa-clock"></i> Time 
                                                    <a href="#" data-toggle="tooltip" title="Enter the time when the activity will start from." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                </span>
                                                <!--<input class="am-scheduler__item form-control" name="activity[scheduled_time]" id="scheduled_time_value" value=""> -->
                                                <select class="am-scheduler__item form-control" name="activity[scheduled_time]" id="scheduled_time_value">
                                                    <option value="">Select Time</option>
                                                    <!--<option value="12:00 AM">12:00am (0 mins)</option>
                                                    <option value="12:30 AM">12:30am (30 mins)</option>-->
                                                    <?php $TIME_PICKER_VALUES = unserialize(TIME_PICKER_VALUES); 
                                                    $i = 0.5;
                                                    foreach($TIME_PICKER_VALUES as $key => $value){
                                                        $dispaly_hr = $i + 0.5;
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo  strtolower(preg_replace('/\s+/', '', $key)) . ' (' . ($dispaly_hr) . ' hr)'; ?> </option>
                                                        <?php $i = $i + 0.5; } ?>
                                                    </select>
                                                </div>

                                                <div class="form-group" id="scheduled_duration">
                                                    <span style="display:block !important;"><i class="fa fa-clock"></i> Duration 
                                                        <a href="#" data-toggle="tooltip" title="Enter the duration when the activity will end after the specifed duration from specifed time." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                    </span>
                                                    <!-- <input class="am-scheduler__item form-control" name="activity[scheduled_duration]"  id="scheduled_duration_value" value=""> -->
                                                    <select class="am-scheduler__item form-control" name="activity[scheduled_duration]" id="scheduled_duration_value">
                                                        <option value="">Select Duration</option>
                                                        <?php $DURATION_PICKER_VALUES = unserialize(DURATION_PICKER_VALUES);
                                                        foreach($DURATION_PICKER_VALUES as $key => $value){
                                                            $selected = ($key == 0) ? 'selected="selected"' : '';
                                                            ?>
                                                            <option <?php echo $selected; ?> value="<?php echo $value; ?>"><?php echo $value ?> </option>
                                                        <?php  } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="attendees">
                                            <div class="row" >
                                                <div class="am-attendees">
                                                    <div class="form-group " id="reminder_duration">
                                                        <span style="display:block !important;"><i class="fa fa-clock"></i> Reminder 
                                                            <a href="#" data-toggle="tooltip" title="Enter the duration when the reminder should be called before activity start." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                        </span>
                                                        <select name="event[reminders]" class="form-control" id="event_reminders_duration">
                                                            <option value="">Select Reminder</option>
                                                            <option value="10">10 Minutes</option>
                                                            <option value="30">30 Minutes</option>
                                                            <option value="60">1 hour</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row" >
                                                <div class="am-attendees">
                                                    <div class="form-group">
                                                        <span class=""><i class="fa fa-user"></i> Attendees/Guests
                                                            <a href="#" data-toggle="tooltip" title="Enter the list of attendees email you want to invite them for this particular activity/event." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                            <span class="checkbox-inline" style="float:right;"><input type="checkbox" id="sendUpdates" name="event[optional][sendUpdates]" > Send Notification
                                                                <a href="#" data-toggle="tooltip" title="By enabling it this will send event notification to all attendees on saving." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>
                                                            </span>
                                                        </span> 
                                                        <select class="attendees form-control" name="event[attendees][]" style="height:48px !important; width:100%;" multiple="multiple"></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="modal-footer">
                        <span class="checkbox-inline mr-auto" id="mark_as_completed"><input type="checkbox" id="status" name="activity[status]" > Mark as Completed </span>
                        <a class="btn btn-primary" target="__blank" href="#" id="deal_link"><i class="fa fa-link"></i> Visit Deal</a>
                        <button type="button" class="btn btn-primary" id="addActivity">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('#activityAddress').placecomplete2({
                placeServiceResult2: function (data, status) {
                    if (status != 'INVALID_REQUEST') {
                        $('#show_something').html(data.adr_address);
                    //Set Address
                    if ($('#activityLocationAddressVal') != undefined) {
                        $('#activityLocationAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('#activityLocationState') != undefined) {
                                var select = document.getElementById("activityLocationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    console.log(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }
                                
                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.getElementById("activityLocationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if(k == 0){
                                        option.setAttribute('value','');
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("activityLocationState").appendChild(option);
                                    }else{
                                        option.setAttribute('value',k);
                                        option.innerHTML = states[k];
                                        if(selected_id == k){
                                            option.setAttribute('selected','selected');
                                        }
                                        document.getElementById("activityLocationState").appendChild(option);
                                    }
                                }
                               
                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('#activityLocationPostCode') != undefined) {
                                $('#activityLocationPostCode').val(address_components[i].long_name);
                            }
                        }
                    }
                }
            },
            language: 'fr'
        });
        });
    </script>

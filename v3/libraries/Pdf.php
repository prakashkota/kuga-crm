<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pdf {

    public function phptopdf($pdf_options) {

        $pdf_options['api_key'] = API_KEY;

        $pdf_options['api_version'] = PHPTOPDF_API;

        $post_data = http_build_query($pdf_options);

        $post_array = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'cache-control' => 'no-cache',
                'content' => $post_data
            )
        );

        $context = stream_context_create($post_array);

        if (isset($pdf_options['ssl']) && $pdf_options['ssl'] == 'yes') {
            $result = file_get_contents(PHPTOPDF_URL_SSL, false, $context);
        } else if (isset($pdf_options['beta']) && $pdf_options['beta'] == 'yes') {
            $result = file_get_contents(PHPTOPDF_URL_BETA, false, $context);
        } else if (isset($pdf_options['ssl']) && $pdf_options['ssl'] == 'yes' && $pdf_options['beta'] == 'yes') {
            $result = file_get_contents("https://phptopdf.com/generatePDF_beta.php", false, $context);
        } else {
            $result = file_get_contents(PHPTOPDF_URL, false, $context);
        }

        $action = preg_replace('!\s+!', '', $pdf_options['action']);

        if (isset($action) && !empty($action)) {

            switch ($action) {

                case 'view':

                    header('Content-type: application/pdf');

                    echo $result;

                    break;

                case 'save':

                    $this->savePDF($result, $pdf_options['file_name'], $pdf_options['save_directory']);

                    break;

                case 'download':

                    $this->downloadPDF($result, $pdf_options['file_name']);

                    break;
                default:

                    header('Content-type: application/pdf');

                    echo $result;

                    break;
            }
        } else {

            header('Content-type: application/pdf');

            echo $result;
        }
    }

    public function phptopdf_url($source_url, $save_directory, $save_filename) {

        $API_KEY = API_KEY;

        $url = 'https://phptopdf.com/urltopdf?key=' . $API_KEY . '&url=' . urlencode($source_url);

        $resultsXml = file_get_contents(($url));

        file_put_contents($save_directory . $save_filename, $resultsXml);
    }

    public function phptopdf_html($html, $save_directory, $save_filename) {

        $API_KEY = API_KEY;

        $postdata = http_build_query(array(
            'html' => $html,
            'key' => $API_KEY
        ));

        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $resultsXml = file_get_contents('https://phptopdf.com/htmltopdf_legacy', false, $context);

        file_put_contents($save_directory . $save_filename, $resultsXml);
    }

    public function phptopdf_curl_get($url) {
        if (!function_exists('phptopdf_curl_get')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }
    }

    public function phptopdf_get_contents($url) {
        if (!function_exists('phptopdf_get_contents')) {
            if (ini_get('allow_url_fopen') === '1') { // is allow_url_fopen = On ? 
                $contents = file_get_contents($url);
            } else if (function_exists('curl_version')) {
                $contents = $this->phptopdf_curl_get($url);
            } else {
                throw new Exception('You need to set to On: allow_url_fopen=On in php.ini OR enable php cURL.');
            } return $contents;
        }
    }

    public function phptopdf_readfile($pdf) {
        if (!function_exists('phptopdf_readfile')) {
            if (filter_var($pdf, FILTER_VALIDATE_URL) === FALSE) {
                echo $pdf;
            } else {
                if (ini_get('allow_url_fopen') === '1') {
                    readfile($pdf);
                } else {
                    echo $this->phptopdf_curl_get($pdf);
                }
            }
        }
    }

    /**     
     * * Writes PDF File to disk * @param $pdf * @param null $pdf_name * @param null $save_directory 
     */
    public function savePDF($pdf, $pdf_name = NULL, $save_directory = NULL) {
        
        if ($pdf_name == NULL) {
            $pdf_name = API_KEY . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 9)), 0, 9) . ".pdf";
        } 
        if ($save_directory == NULL || $save_directory === '') {
            $save_directory = getcwd();
        } 
        if (DIRECTORY_SEPARATOR == '/') {
            $save_directory = preg_replace('~/+~', '/', $save_directory);
        }
        if (is_dir($save_directory) && is_writable($save_directory)) {
            $full_dir = $save_directory . DIRECTORY_SEPARATOR . $pdf_name;
            if (DIRECTORY_SEPARATOR == '/') {
                $full_dir = preg_replace('~/+~', '/', $full_dir);
            }
            $fp = fopen($full_dir, "w");
            fwrite($fp, $pdf);
            fclose($fp);
        } else {
            header('Content-type: application/pdf');
            echo $pdf;
        }
    }

    /**     
     * * Outputs PDF file in the browser * @param $pdf * @param null $pdf_name 
     */
    public function downloadPDF($pdf, $pdf_name = NULL) {
        if ($pdf_name == NULL) {
            $pdf_name = API_KEY . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 9)), 0, 9) . ".pdf";
        }
        header('Content-type: application/pdf');
        header("Content-disposition: attachment; filename=" . $pdf_name);
        $this->phptopdf_readfile($pdf);
    }

}

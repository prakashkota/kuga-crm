<div class="modal" id="add_customer_contact_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_customer_contact_modal_title">Add New Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <form role="form"  action="" method="post" name="customerContactAdd" id="customerContactAdd" enctype="multipart/form-data">
                            <input type="hidden" name="company[cust_id]" id="contact_cust_id">
                            <input type="hidden" name="cust_contact_id" id="cust_contact_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="checkcompany"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>First Name</label>
                                        <input type="text" name="company[first_name]" id="contact_firstname"  class="form-control" placeholder="First Name"  required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Last Name</label>
                                        <input type="text" name="company[last_name]" id="contact_lastname"  class="form-control" placeholder="Last Name"  required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="company[customer_email]" id="contact_email"  class="form-control" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" name="company[customer_contact_no]" id="contact_phone"  class="form-control" placeholder="Customer Phone" pattern="[0-9]{6,}" title="Phone number should be number" required></div>
                                </div>
                                
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text" id="contact_position" name="company[position]" class="form-control" placeholder="Position"  required >
                                    </div>
                                </div>
                            </div>

                            <div class="buttwrap" style="display:inline-flex;">
                                <input type="button" id="save_customer_contact" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                                <div id="customer_contact_loader"></div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
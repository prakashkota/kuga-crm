<style>
    @media print {
        .no-print {display: none;}
        body {background: transparent;}
        .red{color:#c32027}
        .black-bg {background-color:#010101}
        .dark-bg{background-color:#282828}
        .page-wrapper{ min-height:100%;}
    }
    @media only screen and (min-width: 1025px){
        .page-wrapper {
            padding: 30px 30px 30px 150px;
        }
    }
    table,tr,td{
        margin : auto !important;
    }

    .sr-deal_actions{
        opacity: 1;
        position: fixed;
        left: 100px;
        right: 0;
        bottom: 0px;
        height: 67px;
        background-color: #f7f7f7;
        z-index: 100;
        box-shadow: 0 5px 40px rgba(0,0,0,.4);
        transition: opacity .1s ease-out,bottom .1s ease-out .5s;
        display: flex;
    }

    .sr-deal_actions--visible{
        opacity: 1;
        bottom: 0px;
    }
    .sr-deal_actions__options{
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 10px 10px 10px;
        display: inline-block;
    }

    .sr-deal_actions__options__item{
        position: relative;
        display: inline-block;
        text-align: center;
        height: 100%;
        width: 100%;
        margin-left: 1%;
        overflow: hidden;
        vertical-align: top;
        font: 400 20px/28px Open Sans,sans-serif;
        color: rgba(0,0,0,.5);
        background: #e5e5e5;
        border-radius: 4px;
        box-shadow: inset 0 1px 2px rgba(0,0,0,.3), 0 2px 0 hsla(0,0%,100%,.55);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-clip: padding-box;
    }

    .sr-deal_actions__options__item--header{
        position: relative;
        padding: 0 45px;
        line-height: 45px;
        white-space: nowrap;
        text-overflow: ellipsis;
        color:#fff;
    }

    .pdf_page{
        width: 910px;
        box-shadow: 1px 1px 3px 1px #333;
        border-collapse: separate;
        padding:5px;
    }
    .add-picture{
        padding: 130px 0 !important;
    }
    
    input[type=checkbox] {
        -moz-appearance:none;
        -webkit-appearance:none;
        -o-appearance:none;
        outline: none;
        content: none; 
        margin-left: 5px;
    }

    input[type=checkbox]:before {
        font-family: "FontAwesome";
        content: "\f00c";
        font-size: 25px;
        color: transparent !important;
        background: #fff;
        width: 25px;
        height: 25px;
        border: 2px solid black;
        margin-right: 5px;
    }

    input[type=checkbox]:checked:before {
        color: black !important;
    }

    input[type=radio] {
        -moz-appearance:none;
        -webkit-appearance:none;
        -o-appearance:none;
        outline: none;
        content: none; 
        margin-left: 5px;
    }

    input[type=radio]:before {
        font-family: "FontAwesome";
        content: "\f00c";
        font-size: 25px;
        color: transparent !important;
        background: #fff;
        width: 25px;
        height: 25px;
        border: 2px solid black;
        margin-right: 5px;
    }

    input[type=radio]:checked:before {
        color: black !important;
    }
    .image_container{ 
        margin: 20px 20px 20px 20px;
        overflow: visible;
        box-shadow: 5px 5px 2px #888888;
        position: relative;
    }
    .image_close{ 
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: -18px;
        right: -18px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background:url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }
    
    #github img {
        border: 0;
    }

    @media (max-width: 990px) {
        #github img {
            width: 90px;
            height: 90px;
        }
        .sr-deal_actions{
            opacity: 1;
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0px;
            height: 67px;
            background-color: #f7f7f7;
            z-index: 100;
            box-shadow: 0 5px 40px rgba(0,0,0,.4);
            transition: opacity .1s ease-out,bottom .1s ease-out .5s;
            display: flex;
        }


    }

    /** IPAD */
    @media only screen 
    and (min-width: 1024px) 
    and (max-height: 1366px) 
    and (-webkit-min-device-pixel-ratio: 1.5) {
        .page-wrapper {
            padding: 30px 30px 30px 105px;
        }
    }

    .is-invalid{border: 1px solid red;}
    .is-valid{border: 1px solid green !important;}
    .isloading-wrapper.isloading-right{margin-left:10px;}
    .isloading-overlay{position:relative;text-align:center;}
    .isloading-overlay .isloading-wrapper{
        background:#FFFFFF;
        -webkit-border-radius:7px;
        -webkit-background-clip:padding-box;
        -moz-border-radius:7px;
        -moz-background-clip:padding;
        border-radius:7px;
        background-clip:padding-box;
        display:inline-block;
        margin:0 auto;
        padding:10px 20px;
        top:40%;
        z-index:9000;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<div class="page-wrapper d-block clearfix" >
    <div style="padding:0; margin:0" id="job_card">
        <form id="job_card_form" enctype="multipart/form-data">
            <?php $this->load->view('job_card/page1'); ?><br/>    
            <?php $this->load->view('job_card/page2'); ?><br/>    
            <?php $this->load->view('job_card/page3'); ?><br/>    
            <?php $this->load->view('job_card/page4'); ?><br/>    
            <?php $this->load->view('job_card/page5'); ?><br/>    
            <?php $this->load->view('job_card/page6'); ?><br/>
            <?php $this->load->view('job_card/page6_dynamic'); ?><br/>
            <?php $this->load->view('job_card/page7'); ?><br/>
            <?php $this->load->view('job_card/page7_dynamic'); ?><br/>
        </form>
    </div>
    <div  class="sr-deal_actions">
        <div class="col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="save_job_card" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-save"></i> Save as Draft</a>
        </div>
        <div class=" col-md-6  sr-deal_actions__options">
            <a href="javascript:void(0);" id="generate_job_card_pdf" class="btn sr-deal_actions__options__item  sr-deal_actions__options__item--header text-white bg-danger" ><i class="fa fa-file-pdf-o"></i> Generate Job Card</a>
            <a href="javascript:void(0);" id="generate_job_card_pdf_link" class="hidden" ></a>
        </div>
    </div>
</div>

<link href='https://code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css' type='text/css' rel='stylesheet'>
<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js' type='text/javascript'></script>    
<script src='https://www.jqueryscript.net/demo/Scrollable-Autocomplete-List-jQuery-UI/jquery.ui.autocomplete.scroll.js' type='text/javascript'></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/isLoading.min.js' type='text/javascript'></script>
<script type="text/javascript" src="<?php echo site_url(); ?>common/js/job_card.js?v=<?php echo version; ?>"></script>
<script>
    var context = {};
    context.lead_uuid = '<?php echo (isset($lead_uuid) && !empty($lead_uuid)) ? $lead_uuid : ''; ?>';
    context.job_card_uuid = '<?php echo (isset($job_card_uuid)) ? $job_card_uuid : ''; ?>';
    var job_card_manager = new job_card_manager(context);
</script>

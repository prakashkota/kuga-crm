<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
require APPPATH . "../vendor/autoload.php";
include APPPATH . 'libraries/Mailgun_CI_Email.php';
    
class Email_Manager {

    /**
     * Constructor
     */
    public function __construct() {
        // get main CI object
        $this->lt               = & get_instance();
        /**$this->lt->email_config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.mailgun.org',
            'smtp_port' => '587',
            'smtp_user' => 'postmaster@mg.kugacrm.com.au',
            'smtp_pass' => 'fa346a2ef672cc5dde1e6bc6d24f606e-2ae2c6f3-222ec429',
            'mailtype' => 'html',
            'charset' => 'UTF-8',
            'newline' => "\r\n",
            'crlf' => "\n",
        );
        */
        $this->lt->email = new Mailgun_CI_Email();
        
    }
    
    public function send_test_mail(){
        try{
            $this->lt->email->set_newline("\r\n");
            $this->lt->email->to('j.kurta@mailinator.com');
            $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
            $this->lt->email->subject('Test Mail Kuga CRM Production');
            $this->lt->email->message('Test Production');
            $result = $this->lt->email->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$this->lt->php_mailer->ErrorInfo}";
        }
    }

    public function send_leademail_to_franchise($communication_data = [], $new = true) {
        $tpl_id = 'LEAD-MAIL-TO-F';
        $template = $this->lt->common->fetch_where('tbl_communications', '*', array('template_id' => $tpl_id));

        //Customer Address with removed australia on any occurence
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        $body = $template[0]['template'];
        $body = str_replace("{FRANCHISE_NAME}", $communication_data['franchise_name'], $body);
        $body = str_replace("{SERVICE_NAME}", $communication_data['category_name'] . '/' . $communication_data['sub_category_name'], $body);
        $body = str_replace("{SITE_LOGO}", '<img src="' . $this->lt->config->site_url() . 'assets/images/logo.png" alt="Logo" border="0" width="64" />', $body);
        $body = str_replace("\r\n", '<br/>', $body);

        $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
        $body = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $body);
        $body = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $body);
        $body = str_replace("{CUSTOMER_NOTE}", ($communication_data['note'] == '') ? '-' : $communication_data['note'], $body);
        $body = str_replace("{CUSTOMER_ADDRESS}", $c_address, $body);

        $subject = (isset($new) && $new == TRUE) ? 'NEW LEAD' : "FOLLOW UP";
        $subject = $subject . " " . $template[0]['subject'];
        $subject = str_replace("{SERVICE_NAME}", $communication_data['sub_category_name'], $subject);
        $subject = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $subject);
        $subject = str_replace("{CUSTOMER_ADDRESS}", $c_address, $subject);
        $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $subject);

        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($communication_data['franchise_email']);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }

    public function send_leademail_to_webleads($communication_data = [], $new = true) {
        $type = (isset($new) && $new == TRUE) ? 'NEW LEAD' : "FOLLOW UP";

        //Removinng Australia on any occurence from franchise and customer address
        $f_address = str_ireplace(", Australia", "", $communication_data['franchise_address']);
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        $subject = $type . " {CATEGORY_NAME}/{SUB_CATEGORY_NAME} allocated to {FRANCHISE_COMPANY_NAME} - {CUSTOMER_POSTCODE}.";
        $subject = str_replace("{CATEGORY_NAME}", $communication_data['category_name'], $subject);
        $subject = str_replace("{SUB_CATEGORY_NAME}", $communication_data['sub_category_name'], $subject);
        $subject = str_replace("{FRANCHISE_COMPANY_NAME}", $communication_data['franchise_company_name'], $subject);
        $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $subject);


        $body = "Dear Admin,<br/><br/>
        $type '{CATEGORY_NAME}/{SUB_CATEGORY_NAME}'. Given below is the details.<br/><br/>
        <b>Allocated Franchise:</b> {FRANCHISE_COMPANY_NAME} | {FRANCHISE_NAME}<br/>
        <b>Lead Source:</b> {LEAD_SOURCE}<br/>
        <b>Customer Name:</b> {CUSTOMER_NAME}<br/>
        <b>Address:</b> {CUSTOMER_ADDRESS}<br/>
        <b>Postcode:</b> {CUSTOMER_POSTCODE}<br/>
        <b>Mobile:</b> {CUSTOMER_MOBILE}<br/>
        <b>Note:</b> {LEAD_NOTE}<br/>";

        $body = str_replace("{CATEGORY_NAME}", $communication_data['category_name'], $body);
        $body = str_replace("{SUB_CATEGORY_NAME}", $communication_data['sub_category_name'], $body);
        $body = str_replace("{FRANCHISE_NAME}", $communication_data['franchise_name'], $body);
        $body = str_replace("{FRANCHISE_COMPANY_NAME}", $communication_data['franchise_company_name'], $body);
        $body = str_replace("{LEAD_SOURCE}", $communication_data['lead_source'], $body);
        $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
        $body = str_replace("{CUSTOMER_ADDRESS}", $c_address, $body);
        $body = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $body);
        $body = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $body);
        $body = str_replace("{LEAD_NOTE}", ($communication_data['note'] == '') ? '-' : $communication_data['note'], $body);

        $body = str_replace("{SITE_LOGO}", '<img src="' . $this->lt->config->site_url() . 'assets/images/dashboard-logo.svg" alt="Logo" border="0" width="64" />', $body);
        $body = str_replace("\r\n", '<br/>', $body);

        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to(NOTIFICATION_MAIL);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }

    public function franchise_setttings_change_email($communication_data = [], $to = 'service@kugacrm.com.au') {
        #create an Ancillary Class
        $body = 'A franchise has requested to change the franchise settings.';
        $body .= '<br/><br/>';
        $body .= 'Franchise Name: ' . $communication_data['franchise_name'];
        $body .= '<br/><br/>';
        $body .= 'Franchise System Id: ' . $communication_data['id'];
        $body .= '<br/><br/><br/>';
        $body .= 'Note: Please visit in Franchisor(Admin) Franchise Section > Manage Request Settings to take appropriate action.';

        $subject = 'Kuga - Franchise Settings Change Requested';

        $this->lt->email->to($to);
        $this->lt->email->from('no-reply@kugacrm.com.au', 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }

    public function send_issue_email($communication_data = [], $to = '') {
        $priority = [1 => 'Business Critical', 2 => 'Important', 3 => 'Normal', 4 => 'Low'];
        $subject = "Kuga CRM - New Issue Reported [" . $priority[$communication_data['priority']] . "]";
        $body = "Dear Admin,<br/><br/>
        Given below is the details of new issue:<br/><br/>";
        $body .= "<b>Priority:</b> " . $priority[$communication_data['priority']] . "<br/>";
        if (!empty($communication_data['customer'])) {
            $body .= "<b>Customer:</b> " . $communication_data['customer'][0]['first_name'] . " " . $communication_data['customer'][0]['last_name'] . "<br/>";
            $body .= "<b>Customer Contact number:</b> " . $communication_data['customer'][0]['customer_contact_no'] . "<br/>";
            $body .= "<b>Customer Email:</b> " . $communication_data['customer'][0]['customer_email'] . "<br/>";
        }
        if (!empty($communication_data['franchise'])) {
            $body .= "<b>Franchise:</b> " . $communication_data['franchise'][0]['full_name'] . "<br/>";
            $body .= "<b>Franchise Contact number:</b> " . $communication_data['franchise'][0]['company_contact_no'] . "<br/>";
            $body .= "<b>Franchise Email:</b> " . $communication_data['franchise'][0]['email'] . "<br/>";
        }
        $body .= "<b>Status:</b> " . $communication_data['status'] . "<br/>";
        $body .= "<b>URL:</b> " . $communication_data['url'] . "<br/>";
        if (isset($communication_data['image']) && $communication_data['image'] != '') {
            $body .= "<b>Attachment:</b> <a href='" . $this->lt->config->site_url() . 'assets/uploads/issue_files/' . $communication_data['image'] . "'>" . $this->lt->config->site_url() . 'assets/uploads/issue_files/' . $communication_data['image'] . "</a><br/>";
        }
        $body .= "<b>Description:</b> " . $communication_data['description'] . "<br/>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to);
        $this->lt->email->bcc(DEVELOPER_MAIL);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $this->lt->email->send();
    }

    public function send_survey_email_to_customer($communication_data = [], $to = '') {
        $leadId = $communication_data['id'];
        $customerId = $communication_data['cust_id'];
        $subject = "KugaElectrical customer survey";
        $body = "Hi  " . $communication_data['customer_name'] . ",<br/><br/>";
        if (!empty($communication_data['franchise_name'])) {
            $body .= $communication_data['franchise_name'] . " from " . $communication_data['franchise_company_name'] . " recently provided you with a solar solution.<br/><br/>";
        }
        $body .= "Let us know how you feel about the service by click on the stars.<br/><br/>";
        $feedbackLink = site_url() . 'survey/feedback?cid=' . $customerId . '&lid=' . $leadId . '&rate=';
        $imgLink = site_url() . 'assets/images/';
        $body .= "<table>
        <tr>
        <td><a href='" . $feedbackLink . "5' target='_blank'>Excellent</a></td>
        <td><a href='" . $feedbackLink . "5' target='_blank'><img src='" . $imgLink . "5star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "4' target='_blank'>Good</a></td>
        <td><a href='" . $feedbackLink . "4' target='_blank'><img src='" . $imgLink . "4star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "3' target='_blank'>OK but some issues</a></td>
        <td><a href='" . $feedbackLink . "3' target='_blank'><img src='" . $imgLink . "3star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "2' target='_blank'>Poor</a></td>
        <td><a href='" . $feedbackLink . "2' target='_blank'><img src='" . $imgLink . "2star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "1' target='_blank'>Bad</a></td>
        <td><a href='" . $feedbackLink . "1' target='_blank'><img src='" . $imgLink . "1star.png'/></a></td>
        </tr>
        </table><br/><br/>";
        $body .= "This rating will be made public so potential customers can see " . $communication_data['franchise_name'] . " reviews.<br/><br/>";
        $body .= "Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/><br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . $imgLink . "signature.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($communication_data['customer_email']);
        $this->lt->email->from(SITE_EMAIL, 'KugaElectrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $this->lt->email->send();
    }

    public function send_survey_followup_email_to_customer($communication_data = [], $to = '') {
        $leadId = $communication_data['id'];
        $customerId = $communication_data['cust_id'];
        $subject = "KugaElectrical customer survey";
        $body = "Hi  " . $communication_data['customer_name'] . ",<br/><br/>";
        if (!empty($communication_data['franchise_name'])) {
            $body .= $communication_data['franchise_name'] . " from " . $communication_data['franchise_company_name'] . " recently provided you with a solar solution.<br/><br/>";
        }
        $body .= "Let us know how you feel about the service by click on the stars.<br/><br/>";
        $feedbackLink = site_url() . 'survey/feedback?cid=' . $customerId . '&lid=' . $leadId . '&rate=';
        $imgLink = site_url() . 'assets/images/';
        $body .= "<table>
        <tr>
        <td><a href='" . $feedbackLink . "5' target='_blank'>Excellent</a></td>
        <td><a href='" . $feedbackLink . "5' target='_blank'><img src='" . $imgLink . "5star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "4' target='_blank'>Good</a></td>
        <td><a href='" . $feedbackLink . "4' target='_blank'><img src='" . $imgLink . "4star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "3' target='_blank'>OK but some issues</a></td>
        <td><a href='" . $feedbackLink . "3' target='_blank'><img src='" . $imgLink . "3star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "2' target='_blank'>Poor</a></td>
        <td><a href='" . $feedbackLink . "2' target='_blank'><img src='" . $imgLink . "2star.png'/></a></td>
        </tr>
        <tr>
        <td><a href='" . $feedbackLink . "1' target='_blank'>Bad</a></td>
        <td><a href='" . $feedbackLink . "1' target='_blank'><img src='" . $imgLink . "1star.png'/></a></td>
        </tr>
        </table><br/><br/>";
        $body .= "This rating will be made public so potential customers can see " . $communication_data['franchise_name'] . " reviews.<br/><br/>";
        $body .= "Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/><br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . $imgLink . "signature.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($communication_data['customer_email']);
        $this->lt->email->from(SITE_EMAIL, 'KugaElectrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $this->lt->email->send();
    }

    public function send_issue_email_to_user($communication_data) {
        $imgLink = site_url() . 'assets/images/';
        $priority = [1 => 'Business Critical', 2 => 'Important', 3 => 'Normal', 4 => 'Low'];
        $runningStatus = ['Not Started', 'Running', 'In Review', 'Completed'];
        $subject = "Kuga CRM - Reply on issue [" . $priority[$communication_data['issue']['priority']] . "]";
        $body = "Hi,<br/><br/>
        Received a message for the below issue <br/><br/>";
        $body .= "<b>Priority:</b> " . $priority[$communication_data['issue']['priority']] . "<br/>";
        if ($communication_data['issue']['first_name']) {
            $body .= "<b>Customer:</b> " . $communication_data['issue']['first_name'] . " " . $communication_data['issue']['last_name'] . "<br/>";
            $body .= "<b>Customer Contact number:</b> " . $communication_data['issue']['customer_contact_no'] . "<br/>";
            $body .= "<b>Customer Email:</b> " . $communication_data['issue']['customer_email'] . "<br/>";
        }

        $body .= "<b>Franchise:</b> " . $communication_data['issue']['full_name'] . "<br/>";
        $body .= "<b>Franchise Contact number:</b> " . $communication_data['issue']['company_contact_no'] . "<br/>";
        $body .= "<b>Franchise Email:</b> " . $communication_data['issue']['email'] . "<br/>";
        $body .= "<b>Status:</b> " . $runningStatus[$communication_data['issue']['status']] . "<br/>";
        if ($communication_data['issue']['url']) {
            $body .= "<b>URL:</b> <a href='" . $communication_data['issue']['url'] . "'>" . $communication_data['issue']['url'] . "</a><br/>";
        }
        if (isset($communication_data['issue']['image']) && $communication_data['issue']['image'] != '') {
            $body .= "<b>Attachment:</b> <a href='" . $this->lt->config->site_url() . 'assets/uploads/issue_files/' . $communication_data['issue']['image'] . "'>" . $this->lt->config->site_url() . 'assets/uploads/issue_files/' . $communication_data['issue']['image'] . "</a><br/>";
        }
        $body .= "<b>Description:</b> <p>" . $communication_data['issue']['description'] . "</p><br/>";
        $body .= "<b>Thread : </b>";
        $body .= "<table width='100%' border='0' cellpadding='10' cellspacing='10'>";
        foreach ($communication_data['thread'] as $k => $message) {
            $date = date('d M Y', strtotime($message['created_at'])) . ' at ' . date('h:i A', strtotime($message['created_at']));
            if ($message['file']) {
                $extension = strtolower(pathinfo($message['file'], PATHINFO_EXTENSION));
                $attachmentUrl = site_url('assets/uploads/issue_files/' . $message['file']);
                if (in_array($extension, ['jpeg', 'jpg', 'png', 'gif'])) {
                    $img = "<img src='" . $attachmentUrl . "' style='height:80px;width:80px;'/>";
                    $attchmentHtml = "<p><a target='_blank' href='" . $attachmentUrl . "'>" . $img . "</a> </p>";
                } else {
                    $attchmentHtml = "<p><a target='_blank' href='" . $attachmentUrl . "'>" . $message['file'] . "</a> </p>";
                }
            } else {
                $attchmentHtml = '';
            }
            $background = '#f9f9f9';
            if ($k + 1 == count($communication_data['thread'])) {
                $background = '#fceccd';
            }
            $body .= "<tr style='background: " . $background . ";'><td style='border: 1px solid #ccc;'>"
            . "<p><b>" . $message['full_name'] . "</b></p>"
            . "<p style='font-size:12px;'>" . $date . "</p>"
            . "<hr>"
            . "<p>" . $message['message'] . "</p>"
            . $attchmentHtml
            . "</td></tr>";
        }
        $body .= "</table>";

        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($communication_data['to']);
        //$this->lt->email->bcc(DEVELOPER_MAIL);
        $this->lt->email->from(SITE_EMAIL, 'KugaElectrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $this->lt->email->send();
    }

    public function send_solar_calculated_pdf_email_to_user($dataArray){
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($dataArray['user_email']);
        $this->lt->email->from(SITE_EMAIL, 'Solar Run');
        $this->lt->email->subject("Solar saving calculator");
        $this->lt->email->attach($dataArray['file_path']);
        $this->lt->email->message("Please find the attachment.");
        $this->lt->email->send();
    }   

    public function send_booking_form_success_email($communication_data = [], $to_sales_rep = '',$bcc_team_leader = '') {
        $subject = "Kuga CRM - New " . $communication_data['type'] . " Booking Form Submitted Successfully.";
        $body = "Dear User,<br/><br/>
        Given below is the details of new booking form submission:<br/><br/>";
        if (!empty($communication_data['sales_rep'])) {
            $body .= "<b>Sales Rep Name:</b> " . $communication_data['sales_rep']['name'] . "<br/>";
            $body .= "<b>Sales Rep Email:</b> " . $communication_data['sales_rep']['email'] . "<br/>";
        }
        $body .= "<b>Booking Form Type:</b> " . $communication_data['type'] . "<br/>";
        if (!empty($communication_data['customer'])) {
            $body .= "<b>Customer CompanyName:</b> " . $communication_data['customer']['CompanyName'] . "<br/>";
            $body .= "<b>Customer Phone:</b> " . $communication_data['customer']['Phone'] . "<br/>";
            $body .= "<b>Customer Email:</b> " . $communication_data['customer']['Email'] . "<br/>";
        }
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to_sales_rep);
        $this->lt->email->bcc($bcc_team_leader);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        if(isset($communication_data['attachment'])){
            $this->lt->email->attach($communication_data['attachment']);
        }
        $this->lt->email->send();
    }
    
    public function send_booking_form_success_email_to_john($communication_data = []){
        $subject = "Kuga CRM - New " . $communication_data['type'] . " Booking Form Submitted Successfully.";
        $body = "Dear User,<br/><br/>
        Given below is the details of new booking form submission:<br/><br/>";
        if (!empty($communication_data['sales_rep'])) {
            $body .= "<b>Sales Rep Name:</b> " . $communication_data['sales_rep']['name'] . "<br/>";
            $body .= "<b>Sales Rep Email:</b> " . $communication_data['sales_rep']['email'] . "<br/>";
        }
        $body .= "<b>Booking Form Type:</b> " . $communication_data['type'] . "<br/>";
        if (!empty($communication_data['customer'])) {
            $body .= "<b>Customer CompanyName:</b> " . $communication_data['customer']['CompanyName'] . "<br/>";
            $body .= "<b>Customer Phone:</b> " . $communication_data['customer']['Phone'] . "<br/>";
            $body .= "<b>Customer Email:</b> " . $communication_data['customer']['Email'] . "<br/>";
        }
        if(!empty($communication_data['job_details'])){
            $job_details = $communication_data['job_details'];
            $body .= "<b>kw:</b> ".$job_details['solar_kw'] . ", 
                      <b>HB:</b> ".$job_details['highbays_count'] . ",
                      <b>FL:</b> ".$job_details['flood_lights_count'] . ",
                      <b>PL:</b> ".$job_details['panel_lights_count'] . ",
                      <b>B:</b> ".$job_details['batten_lights_count'] . ",
                      <b>DL:</b> ".$job_details['down_lights_count'] . ",
                      <b>O:</b> ".$job_details['other_count'];

        }
        
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to('j.kurta@13kuga.com.au');
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        
        if(isset($communication_data['attachment'])){
            $this->lt->email->attach($communication_data['attachment']);
        }
        if(isset($communication_data['attachment1'])){
            $this->lt->email->attach($communication_data['attachment1']);
        }
        if(isset($communication_data['attachment2'])){
            $this->lt->email->attach($communication_data['attachment2']);
        }
        $this->lt->email->send();
        
        //print_r($this->lt->email->print_debugger());die;
    }

    public function send_leademail_to_leaders($communication_data = [], $new = true) {

        //Removinng Australia on any occurence from franchise and customer address
        //$f_address = str_ireplace(", Australia", "", $communication_data['franchise_address']);
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        $subject = "New Call Centre Lead {LEAD_ALLOCATION_NAME} - {BUSINESS_NAME} {CUSTOMER_STATE} {CUSTOMER_POSTCODE} {LEAD_TYPE} allocated to {SALES_REP_NAME}.";
        $subject = str_replace("{LEAD_ALLOCATION_NAME}", $communication_data['lead_allocator_name'], $subject);
        $subject = str_replace("{BUSINESS_NAME}", $communication_data['company_name'], $subject);
        $subject = str_replace("{CUSTOMER_STATE}", $communication_data['customer_state'], $subject);
        $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $subject);
        $subject = str_replace("{LEAD_TYPE}", $communication_data['category_name'], $subject);
        $subject = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $subject);


        $body = "New Call Centre Lead:<br/><br/>
        <b>Call Center Rep:</b> {LEAD_ALLOCATION_NAME}<br/>
        <b>Allocated to:</b> {SALES_REP_NAME}<br/>
        <b>Appointment Type:</b> {APPOINTMENT_TYPE}<br/>";
        if($communication_data['is_calendar_meeting'] == 1){
            $body .="<b>Date and Time of Appointment:</b> {DATE_OF_APT} {TIME_OF_APT}<br/>";
            $body = str_replace("{DATE_OF_APT}", date("F j, Y", strtotime($communication_data['scheduled_date'])), $body);
            $body = str_replace("{TIME_OF_APT}", date("g:i a", strtotime($communication_data['scheduled_time'])), $body);
        }
        $body .="<b>Business Name:</b> {BUSINESS_NAME}<br/>
        <b>Address:</b> {CUSTOMER_ADDRESS}<br/>
        <b>Lead Type:</b> {LEAD_TYPE}<br/>
        <b>Daily Usage:</b> {DAILY_USAGE}<br/>
        <b>Highbays:</b> {HIGHBAYS}<br/>
        <b>Panels:</b> {PANELS}<br/>
        <b>Battens:</b> {BATTENS}<br/>
        <b>Floodlights:</b> {FLOODLIGHTS}<br/>";

        $body = str_replace("{LEAD_ALLOCATION_NAME}", $communication_data['lead_allocator_name'], $body);
        $body = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $body);
        $body = str_replace("{APPOINTMENT_TYPE}", ($communication_data['is_calendar_meeting'] == 1) ? 'Scheduled' : 'Not Scheduled', $body);
        $body = str_replace("{BUSINESS_NAME}", $communication_data['company_name'], $body);
        $body = str_replace("{CUSTOMER_ADDRESS}", $c_address, $body);
        $body = str_replace("{LEAD_TYPE}", $communication_data['category_name'], $body);
        $body = str_replace("{DAILY_USAGE}", ($communication_data['daily_average_usage'] == '') ? '-' : $communication_data['daily_average_usage'], $body);
        $body = str_replace("{HIGHBAYS}", ($communication_data['highbay'] == '') ? '-' : $communication_data['highbay'], $body);
        $body = str_replace("{PANELS}", ($communication_data['panel_light'] == '') ? '-' : $communication_data['panel_light'], $body);
        $body = str_replace("{BATTENS}", ($communication_data['batten_light'] == '') ? '-' : $communication_data['batten_light'], $body);
        $body = str_replace("{FLOODLIGHTS}", ($communication_data['flood_light'] == '') ? '-' : $communication_data['flood_light'], $body);

        $body .= "<br/><br/>Best Regards,<br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img style='margin-left:-20px;' src='" . site_url() . "assets/images/logo.png'/></a>";
        $body = str_replace("\r\n", '<br/>', $body);

        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to('j.kurta@13kuga.com.au');
        $this->lt->email->cc('c.doudet@13kuga.com.au,j.trybus@13kuga.com.au,d.tiburcio@13kuga.com.au,g.mccann@13kuga.com.au');
        //$this->lt->email->cc('kuga1@mailinator.com');
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }
    
    
    public function send_meter_data_form_success_email($communication_data = [], $to_sales_rep = '') {
        $subject = "Kuga CRM - New Meter Data Request Submitted Successfully.";
        $body = "Dear User,<br/><br/>
        Please find the meter data form pdf attached with the mail.<br/><br/>";
        
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to_sales_rep);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        if(isset($communication_data['attachment'])){
            $this->lt->email->attach($communication_data['attachment']);
        }
        $this->lt->email->send();
    }
    
    public function send_leademail_by_lead_creator_to_leaders($communication_data = [], $new = true) {
       $lead_industry = unserialize(LEAD_INDSUTRY);
       $type = (isset($new) && $new == TRUE) ? "Make It Cheaper (NEW LEAD)" : "Make It Cheaper (FOLLOW UP)";

        //Removinng Australia on any occurence from customer address
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        $subject = $type . " allocated to {SALES_REP_NAME} - {CUSTOMER_POSTCODE}.";
        $subject = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $subject);
        $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $subject);


        $body = "Dear Admin,<br/><br/>
        $type. Given below is the details.<br/><br/>
        <b>Lead Creator:</b> {LEAD_CREATOR_NAME}<br/>
        <b>Allocated Franchise:</b> {SALES_REP_NAME}<br/>
        <b>Lead Source:</b> {LEAD_SOURCE}<br/>
        <b>Industry Type:</b> {INDUSTRY_TYPE}<br/>
        <b>Business Name:</b> {BUSINESS_NAME}<br/>
        <b>Customer Name:</b> {CUSTOMER_NAME}<br/>
        <b>Address:</b> {CUSTOMER_ADDRESS}<br/>
        <b>Postcode:</b> {CUSTOMER_POSTCODE}<br/>
        <b>Mobile:</b> {CUSTOMER_MOBILE}<br/>";

        $body = str_replace("{LEAD_CREATOR_NAME}", $communication_data['lead_creator_name'], $body);
        $body = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $body);
        $body = str_replace("{LEAD_SOURCE}", ($communication_data['lead_source'] == '' || $communication_data['lead_source'] == 'null') ? '-' : $communication_data['lead_source'], $body);
        $body = str_replace("{INDUSTRY_TYPE}",  ($communication_data['lead_industry'] == '' || $communication_data['lead_industry'] == 0) ? '-' : $lead_industry[$communication_data['lead_industry']], $body);
        $body = str_replace("{BUSINESS_NAME}", $communication_data['company_name'], $body);
        $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
        $body = str_replace("{CUSTOMER_ADDRESS}", $c_address, $body);
        $body = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $body);
        $body = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $body);

        $body .= "<br/><br/>Best Regards,<br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img style='margin-left:-20px;' src='" . site_url() . "assets/images/logo.png'/></a>";
        $body = str_replace("\r\n", '<br/>', $body);

        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to('j.kurta@13kuga.com.au');
        $this->lt->email->cc('c.doudet@13kuga.com.au,j.trybus@13kuga.com.au');
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }
    
    public function send_web_leademail($communication_data = [], $new = true) {
       $type = (isset($new) && $new == TRUE) ? "NEW WEB LEAD" : "FOLLOW UP";

        //Removinng Australia on any occurence from customer address
        $c_address = str_ireplace(", Australia", "", $communication_data['customer_address']);

        $subject = $type . " allocated to {SALES_REP_NAME} - {CUSTOMER_POSTCODE}.";
        $subject = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $subject);
        $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $subject);


        $body = "Dear Admin,<br/><br/>
        $type. Given below is the details.<br/><br/>
        <b>Allocated Franchise:</b> {SALES_REP_NAME}<br/>
        <b>Lead Source:</b> {LEAD_SOURCE}<br/>
        <b>Industry Type:</b> {INDUSTRY_TYPE}<br/>
        <b>Business Name:</b> {BUSINESS_NAME}<br/>
        <b>Customer Name:</b> {CUSTOMER_NAME}<br/>
        <b>Address:</b> {CUSTOMER_ADDRESS}<br/>
        <b>Postcode:</b> {CUSTOMER_POSTCODE}<br/>
        <b>Mobile:</b> {CUSTOMER_MOBILE}<br/>";

        $body = str_replace("{SALES_REP_NAME}", $communication_data['sales_rep_name'], $body);
        $body = str_replace("{LEAD_SOURCE}", ($communication_data['lead_source'] == '' || $communication_data['lead_source'] == 'null') ? '-' : $communication_data['lead_source'], $body);
        $body = str_replace("{INDUSTRY_TYPE}",  ($communication_data['lead_industry'] == '' || $communication_data['lead_industry'] == 0) ? '-' : $lead_industry[$communication_data['lead_industry']], $body);
        $body = str_replace("{BUSINESS_NAME}", $communication_data['company_name'], $body);
        $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
        $body = str_replace("{CUSTOMER_ADDRESS}", $c_address, $body);
        $body = str_replace("{CUSTOMER_POSTCODE}", $communication_data['customer_postcode'], $body);
        $body = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $body);

        $body .= "<br/><br/>Best Regards,<br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img style='margin-left:-20px;' src='" . site_url() . "assets/images/logo.png'/></a>";
        $body = str_replace("\r\n", '<br/>', $body);

        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to('j.kurta@13kuga.com.au');
        $this->lt->email->cc('c.doudet@13kuga.com.au');
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $result = $this->lt->email->send();
    }
    
    public function send_warranty_job_success_email($communication_data = [],$attachment_files = []){
        //$attachment_fields = ['vid','fault_image','invoice','meta_image','certificate_image'];
        //$attachment_fields_name = ['Video of Faulty Light','Picture of Fault Light','Invoice','Model No and Wattage of Fitting','Certificate of Electrical Safety'];

        $subject = "Kuga CRM - New Warranty Job (sIMPRO)";
        $body = "Dear User,<br/><br/>
        Given below is the details of new warranty job submitted by customer:<br/><br/>";
        $body .= "<b>Business Name:</b> " . $communication_data['business_name'] . "<br/>";
        $body .= "<b>Name:</b> " . $communication_data['name'] . "<br/>";
        $body .= "<b>Contact No.:</b> " . $communication_data['number'] . "<br/>";
        $body .= "<b>Address:</b> " . $communication_data['address'] . "<br/><br/>";
        $body .= "Below is the list of attachments uploaded by the customer: <br/><br/>";
        foreach($attachment_files as $key => $value){
            if($value['file_name'] != '' && $value['file_name'] != NULL){
                $body .= "<b>" . $value['name'] .":</b> <a href='https://kugacrm.com.au/assets/uploads/warranty_claim_files/". $value['file_name'] ."'>https://kugacrm.com.au/assets/uploads/warranty_claim_files/". $value['file_name'] ."</a><br/><br/>";
            }
        }
        
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        //$this->lt->email->to('j.kurta@mailinator.com');
        $this->lt->email->to('j.kurta@13kuga.com.au');
        //$this->lt->email->cc('n.disario@13kuga.com.au,a.kurta@13kuga.com.au,r.burton@13kuga.com.au,b.wang@13kuga.com.au,v.sepulveda@13kuga.com.au');
        $this->lt->email->cc('a.kurta@13kuga.com.au,v.sepulveda@13kuga.com.au,f.futsek@13kuga.com.au');
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);

        $this->lt->email->send();
    }
    
    
    public function send_quick_quote_form_success_email($communication_data = [], $to_sales_rep = '') {
        $subject = "Kuga CRM - New Quick Quote Created Successfully.";
        $body = "Dear User,<br/><br/>
        Please find the quick quote pdf attached with the mail.<br/><br/>";
        
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to_sales_rep);
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        if(isset($communication_data['attachment'])){
            $this->lt->email->attach($communication_data['attachment']);
        }
        $this->lt->email->send();
    }
    
    
    
    
    // Send Email for Job Card submission
    public function send_job_card_success_email($communication_data, $to_sales_rep = '',$bcc_team_leader = '') {
        $subject = "Kuga CRM - New  Job Card Submitted Successfully.";
        $body = "Dear User,<br/><br/>
        Given below is the details of new job card form submission:<br/><br/>";
        
        $body .= "<b>Project Name:</b> " . $communication_data->project_name . "<br/>";
        $body .= "<b>Sales Rep Name:</b> " . $communication_data->sale_rep_name . "<br/>";
    
        $body .= "<b>Lead Source:</b> " . $communication_data->lead_source . "<br/>";
        $body .= "<b>System Size:</b> " . $communication_data->system_size . "<br/>";
            
        $body .= "<br/><br/>Best Regards<br/><br/>";
        $body .= "Kuga Electrical<br/>";
        $body .= "<a href='https://www.kugacrm.com.au/' target='_blank'><img src='" . site_url() . "assets/images/logo.png'/></a>";
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to_sales_rep);
        if($bcc_team_leader){
            $this->lt->email->bcc($bcc_team_leader);
        }
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        if(isset($communication_data->attachment)){
            $this->lt->email->attach($communication_data->attachment);
        }
        $this->lt->email->send();
    }


    public function send_commercial_proposal_customer_email($communication_data = []) {
        $subject = $communication_data['email_subject'];
        $body = $communication_data['email_body'];
        $to = trim($communication_data['email_to']);
        $cc = trim($communication_data['cc_emails']);
        if($cc != '' && $cc != NULL){
            $cc = explode(',',$cc);
        }
        $this->lt->email->set_newline("\r\n");
        $this->lt->email->to($to);  
        if(!empty($$cc) && $cc != ''){
            $this->lt->email->bcc($cc);
        }
        $this->lt->email->from(SITE_EMAIL, 'Kuga Electrical');
        $this->lt->email->subject($subject);
        $this->lt->email->message($body);
        $this->lt->email->send();
    }
}

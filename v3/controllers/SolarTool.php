<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Lead Controller
 * @version 1.0
 */
class SolarTool extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Product_Model", "product");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(3) != 'solar_quote_pdf_download') {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            } else {
                redirect('admin/login');
            }
        }
    }

    public function create() {
        $data = array();
        $cond = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=2";
        $data['solar_panels'] = $this->product->get_item_data_by_cond($cond);
        $data['lead_id'] = 16404;
        $data['site_id'] = 17215;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('solar_tool/create',$data);
        $this->load->view('partials/footer');
    }

    public function save_mapping_data() {
        $data = array();
        $tool_id = $this->input->post('tool_id');
        $key = $this->input->post('key');
        $value = $this->input->post('value');

        $stat = FALSE;
        if($key == 'notes'){
           $tool_data =  $this->common->fetch_row('tbl_mapping_tool_data','id',array('id' => $tool_id));
           if(!empty($tool_data)){
               $stat = $this->common->update_data('tbl_mapping_tool_data',array('id' => $tool_id),array($key => $value));
           }else{
               $proposal_id = $this->input->post('proposal_id');
               $stat = $this->common->update_data('tbl_solar_proposal',array('id' => $proposal_id),array('additional_notes' => $value));
           }
        }else{
            $stat = $this->common->update_data('tbl_mapping_tool_data',array('id' => $tool_id),array($key => $value));
        }
    
        if($stat){
            $data['status'] = 'Data Saved Successfully';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Data Saving Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    public function fetch_mapping_tool_data(){
        $data = array();
        $proposal_id = $this->input->get('proposal_id');
        $lead_id = $this->input->get('lead_id');
        $site_id = $this->input->get('site_id');
        $section = $this->input->get('section');
        $stat = FALSE;

        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data','*',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'section' => $section,'status' => 1));

        if(isset($mtool_data['bounds'])){
            $mtool_data['bounds'] = json_decode($mtool_data['bounds'],TRUE);
        }

        if(isset($mtool_data['nm_imagery_surveys'])){
            $mtool_data['nm_imagery_surveys'] = json_decode($mtool_data['nm_imagery_surveys'],TRUE);
        }

        $data['mtool_data'] = $mtool_data;
        $data['customer_site'] = $this->common->fetch_row('tbl_customer_locations','*',array('id' => $site_id));
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function fetch_mapping_tool_objects_data(){
        $data = array();
        $tool_id = $this->input->get('tool_id');
        //PANEL OBJECTS
        $panel_objects = $this->common->fetch_where('tbl_mapping_tool_objects_data','*',array('tool_id' => $tool_id,'object_type' => 'PANEL'));
        foreach($panel_objects as $key => $value){
            $panel_objects[$key]['object_bounds'] = json_decode($value['object_bounds'],TRUE);
            $panel_objects[$key]['object_properties'] = json_decode($value['object_properties'],TRUE);
        }
        $data['panel_objects'] = $panel_objects;
        
        //ANNOTATION OBJECTS
        $annotation_objects = $this->common->fetch_where('tbl_mapping_tool_objects_data','*',array('tool_id' => $tool_id,'object_type' => 'ANNOTATION'));
        foreach($annotation_objects as $key => $value){
            $annotation_objects[$key]['object_bounds'] = json_decode($value['object_bounds'],TRUE);
            $annotation_objects[$key]['object_properties'] = json_decode($value['object_properties'],TRUE);
        }
        $data['annotation_objects'] = $annotation_objects;

        //CUSTOM ANNOTATION OBJECTS
        $custom_annotation_objects = $this->common->fetch_where('tbl_mapping_tool_objects_data','*',array('tool_id' => $tool_id,'object_type' => 'CUSTOM_ANNOTATION'));
        foreach($custom_annotation_objects as $key => $value){
            $custom_annotation_objects[$key]['object_bounds'] = json_decode($value['object_bounds'],TRUE);
            $custom_annotation_objects[$key]['object_properties'] = json_decode($value['object_properties'],TRUE);
        }
        $data['custom_annotation_objects'] = $custom_annotation_objects;
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function save_near_map_imagery() {
        $data = array();
        $stat = FALSE;
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $section = $this->input->post('section');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $address = $this->input->post('address');
        $bounds = $this->input->post('bounds');

        $min_lat = $this->input->post('min_lat');
        $min_lng = $this->input->post('min_lng');
        $max_lat = $this->input->post('max_lat');
        $max_lng = $this->input->post('max_lng');
        $address_id = $this->input->post('address_id');
        $address_id = str_replace('.','_',$address_id);

        //Get Nearmap Imagery
        $surveys = $this->fetch_nearmap_surveys($bounds);
        $nmdate = date('Ymd', strtotime("-31 days"));
        $nm_imagery_selected_date = date('Y-m-d', strtotime("-31 days"));
        if(!empty($surveys)){
            $surveys = json_decode($surveys,true);
            $nmdate = $surveys[0]['captureDate'];
            $nmdate = str_replace('-','', $nmdate);
            $nm_imagery_selected_date = $surveys[0]['captureDate'];
            $surveys = json_encode($surveys);
        }
        $sddate = date('Ymds');
        $address_id_date = $address_id . '_nm_' . $nmdate . '_sd_' . $sddate; 
        
        $NEAR_MAP_API_KEY = 'YjliOTZkZWUtMmI0My00MWQ5LTg5N2EtYWFhYTE3ZjJkN2Zh';

        $bbox = $min_lat.','.$min_lng.','.$max_lat.','.$max_lng;
        $url = 'https://us0.nearmap.com/staticmap?bbox='.$bbox.'&zoom=best&date='.$nmdate.'&httpauth=false&apikey='.$NEAR_MAP_API_KEY;
        $res = file_get_contents($url);

        $uploadPath = FCPATH .'./assets/uploads/near_map_imagery/'. $address_id_date . '.jpg';
        file_put_contents($uploadPath, $res);

        $image_url = site_url() .'./assets/uploads/near_map_imagery/'. $address_id_date . '.jpg';

        //Check if Mapping tool entry present
        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data','id',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'section' => $section,'status' => 1));

        if(empty($mtool_data)){
            $insert_data = array();
            $insert_data['proposal_id'] = $proposal_id;
            $insert_data['lead_id'] = $lead_id;
            $insert_data['site_id'] = $site_id;
            $insert_data['section'] = $section;
            $insert_data['address'] = $address;
            $insert_data['latitude'] = $latitude;
            $insert_data['longitude'] = $longitude;
            $insert_data['bounds'] = json_encode($bounds);
            $insert_data['mbox_address_id'] = $address_id;
            $insert_data['nm_imagery'] = $image_url;
            $insert_data['nm_imagery_surveys'] = $surveys;
            $insert_data['nm_imagery_selected_date'] = $nm_imagery_selected_date;
            $stat = $this->common->insert_data('tbl_mapping_tool_data',$insert_data);
            $insert_id = $this->common->insert_id();
        }
        
        if($stat){
            $data['status'] = 'Entry Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    public function remove_near_map_imagery(){
        $data = array();
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $section = $this->input->post('section');
        $stat = FALSE;
        $stat = $this->common->update_data(
            'tbl_mapping_tool_data',
            array('proposal_id' => $proposal_id, 
                    'lead_id' => $lead_id,
                    'site_id' => $site_id,
                    'section' => $section
                ),
            array('status' => 0)
        );

        if($stat){
            $data['status'] = 'Entry Deleted';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Deletion Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    /** Mapping Object Related Functions */

    public function save_mapping_object() {
        $data = array();
        $stat = FALSE;
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $section = $this->input->post('section');

        //Check if Mapping tool entry present
        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data','id',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'section' => $section,'status' => 1));

        if(!empty($mtool_data)){
            $object_id = $this->input->post('object_id');
            $real_product_id = $this->input->post('real_product_id');
            $object_type = $this->input->post('object_type');
            $object_bounds = $this->input->post('object_bounds');
            $object_properties = $this->input->post('object_properties');

            $mtool_ObjData = $this->common->fetch_row('tbl_mapping_tool_objects_data','id',array('tool_id' => $mtool_data['id'],'object_id' => $object_id));

            $objData = array();
            $objData['object_id'] = $object_id;
            $objData['real_product_id'] = $real_product_id;
            $objData['object_type'] = $object_type;
            $objData['object_bounds'] = json_encode($object_bounds);
            $objData['object_properties'] = json_encode($object_properties);
            if(empty($mtool_ObjData)){
                $objData['tool_id'] = $mtool_data['id'];
                $stat = $this->common->insert_data('tbl_mapping_tool_objects_data',$objData);
            }else{
                $stat = $this->common->update_data('tbl_mapping_tool_objects_data',array('id' => $mtool_ObjData['id']),$objData);
            }
        }
        
        if($stat){
            $data['status'] = 'Entry Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    public function remove_mapping_object(){
        $data = array();
        $object_id = $this->input->post('object_id');
        $stat = FALSE;
        $stat = $this->common->delete_data(
            'tbl_mapping_tool_objects_data',
            array('object_id' => $object_id,)
        );

        if($stat){
            $data['status'] = 'Entry Deleted';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Deletion Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }


    public function save_mapping_object_bulk() {
        $data = array();
        $stat = FALSE;
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $section = $this->input->post('section');

        //Check if Mapping tool entry present
        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data','id',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'section' => $section,'status' => 1));

        if(!empty($mtool_data)){
            $object_action = $this->input->post('object_action');
            $object_data = $this->input->post('object_data');
            $object_data = json_decode($object_data,true);

            if(isset($object_action) && $object_action == 'TILT'){
                $this->common->delete_data('tbl_mapping_tool_objects_data',array('tool_id' => $mtool_data['id'],'object_type' => 'PANEL'));
            }

            $update_data = array();
            $insert_data = array();
            $c1 = 0;
            $c2 = 0;
            foreach($object_data as $key => $value){
                $object_id = $value['object_id'];
                $real_product_id = $value['real_product_id'];
                $object_type = $value['object_type'];
                $object_bounds = $value['object_bounds'];
                $object_properties = $value['object_properties'];

                $mtool_ObjData = $this->common->fetch_row('tbl_mapping_tool_objects_data','id',array('tool_id' => $mtool_data['id'],'object_id' => $object_id));

                if(empty($mtool_ObjData)){
                    $insert_data[$c1] = array();
                    $insert_data[$c1]['object_id'] = $object_id;
                    $insert_data[$c1]['real_product_id'] = $real_product_id;
                    $insert_data[$c1]['object_type'] = $object_type;
                    $insert_data[$c1]['object_bounds'] = json_encode($object_bounds);
                    $insert_data[$c1]['object_properties'] = json_encode($object_properties);
                    $insert_data[$c1]['tool_id'] = $mtool_data['id'];
                    $c1++;
                }else{
                    $update_data[$c2] = array();
                    $update_data[$c2]['object_id'] = $object_id;
                    $update_data[$c2]['real_product_id'] = $real_product_id;
                    $update_data[$c2]['object_type'] = $object_type;
                    $update_data[$c2]['object_bounds'] = json_encode($object_bounds);
                    $update_data[$c2]['object_properties'] = json_encode($object_properties);
                    $update_data[$c2]['id'] = $mtool_ObjData['id'];
                    $c2++;
                }
            }

            if(!empty($insert_data)){
                $stat = $this->common->insert_batch('tbl_mapping_tool_objects_data',$insert_data);
            }

            if(!empty($update_data)){
                $stat = $this->common->update_batch('tbl_mapping_tool_objects_data',$update_data,'id');
            }
        }
        
        if($stat){
            $data['status'] = 'Entry Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }


    public function remove_mapping_object_bulk() {
        $data = array();
        $stat = FALSE;
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $section = $this->input->post('section');

        //Check if Mapping tool entry present
        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data','id',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'section' => $section,'status' => 1));

        $deleted_ids = [];
        if(!empty($mtool_data)){
            $object_action = $this->input->post('object_action');
            $object_data = $this->input->post('object_data');
            $object_data = json_decode($object_data,true);

            foreach($object_data as $key => $value){
                $object_id = $value['object_id'];
                $stat = $this->common->delete_data('tbl_mapping_tool_objects_data',array('tool_id' => $mtool_data['id'],'object_id' => $object_id));
                if($stat){
                    $deleted_ids[] = $object_id;
                }
            }
        }
        
        if($stat){
            $data['deleted_ids'] = $deleted_ids;
            $data['status'] = 'Entry Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Entry Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }


    public function save_snapshot(){
        $data = array();
        $tool_id = $this->input->post('tool_id');
        $snapshot1 = $this->input->post('snapshot1');
        $snapshot2 = $this->input->post('snapshot2');

        //Convert Snapshot1 Base64 to Png
        $img1 = explode(';', $snapshot1);
        $img2 = explode(';', $snapshot2);
        if ($snapshot1 != '' && count($img1) > 1) {
            $image1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $snapshot1));
            $snapShotName = date('Y_m_d_s') . '_' . $tool_id . '_snapshot1' . '.png';
            file_put_contents('./assets/uploads/mapping_tool_snapshots/' . $snapShotName, $image1);
            $snapshotUrl = site_url('./assets/uploads/mapping_tool_snapshots/' . $snapShotName);

            $this->common->update_data('tbl_mapping_tool_data',array('id' => $tool_id),array('snapshot1' => $snapshotUrl));

            $tool_data = $this->common->fetch_row('tbl_mapping_tool_data','proposal_id',array('id' => $tool_id));
            if(!empty($tool_data)){
                $this->common->update_data('tbl_solar_proposal',array('id' => $tool_data['proposal_id']),array('panel_layout_image' => $snapshotUrl));
            }

            $data['status'] = 'Snapshot Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else if ($snapshot2 != '' && count($img2) > 1) {
            $image2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $snapshot2));
            $snapShotName = date('Y_m_d_s') . '_' . $tool_id . '_snapshot2' . '.png';
            file_put_contents('./assets/uploads/mapping_tool_snapshots/' . $snapShotName, $image2);
            $snapshotUrl = site_url('./assets/uploads/mapping_tool_snapshots/' . $snapShotName);

            $this->common->update_data('tbl_mapping_tool_data',array('id' => $tool_id),array('snapshot2' => $snapshotUrl));

            $data['status'] = 'Snapshot Created';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Invalid Request';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    private function fetch_nearmap_surveys($bounds){
        $data = array();
        $c = '';
        foreach($bounds as $k => $v){
            if($k > 0){
                $c .= ",";
            }
            $c .= $v['lng'] . ',' . $v['lat'];
        }
        $c .= "," . $bounds[0]['lng'] . ',' . $bounds[0]['lat'];
        $NEAR_MAP_API_KEY = 'YjliOTZkZWUtMmI0My00MWQ5LTg5N2EtYWFhYTE3ZjJkN2Zh';
        $url = 'https://api.nearmap.com/coverage/v2/poly/' . $c . '?apikey='.$NEAR_MAP_API_KEY.'&since=3Y&limit=20&sort=-lastPhotoTime';
        $res = file_get_contents($url);
        $res = json_decode($res,true);
        $surveys = (isset($res['surveys'])) ? json_encode($res['surveys']) : NULL;

        return $surveys;
    }

    public function update_near_map_imagery() {
        $data = array();
        $stat = FALSE;
        $tool_id = $this->input->post('tool_id');
        $selected_date = $this->input->post('selected_date');

        $tool_data = $this->common->fetch_row('tbl_mapping_tool_data','*',array('id' => $tool_id,'status' => 1));

        if(empty($tool_data)){
            $data['status'] = 'No Mapping Tool Data Found';
            $data['success'] = FALSE;
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }

        //Get Nearmap Imagery
        $address_id = $tool_data['mbox_address_id'];
        $bounds = json_decode($tool_data['bounds'],true);
        $min_lat = $bounds[0]['lat'];
        $min_lng = $bounds[0]['lng'];
        $max_lat = $bounds[2]['lat'];
        $max_lng = $bounds[2]['lng'];

        $nmdate = str_replace('-','', $selected_date);
        $sddate = date('Ymds');
        $address_id_date = $address_id . '_nm_' . $nmdate . '_sd_' . $sddate; 
        
        $NEAR_MAP_API_KEY = 'YjliOTZkZWUtMmI0My00MWQ5LTg5N2EtYWFhYTE3ZjJkN2Zh';

        $bbox = $min_lat.','.$min_lng.','.$max_lat.','.$max_lng;
        $url = 'https://us0.nearmap.com/staticmap?bbox='.$bbox.'&zoom=best&date='.$nmdate.'&httpauth=false&apikey='.$NEAR_MAP_API_KEY;
        $res = file_get_contents($url);

        $uploadPath = FCPATH .'./assets/uploads/near_map_imagery/'. $address_id_date . '.jpg';
        file_put_contents($uploadPath, $res);

        $image_url = site_url() .'./assets/uploads/near_map_imagery/'. $address_id_date . '.jpg';

        $update_data = array();
        $update_data['nm_imagery'] = $image_url;
        $update_data['nm_imagery_selected_date'] = $selected_date;
        $stat = $this->common->update_data('tbl_mapping_tool_data',array('id' => $tool_id),$update_data);
        
        if($stat){
            $data['status'] = 'Imagery Updated Successfully';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Imagery Updation Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }
}

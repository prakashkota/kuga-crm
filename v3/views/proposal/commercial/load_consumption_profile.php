<style>
    .predefined_choose_file {
        cursor: pointer;
    }

    .predefined_img {
        width: 44px;
        height: 44px;
        border: 1px solid #4d4d4d;
        border-radius: 4px;
        position: relative;
        background: #fff;
    }

    .predefined_img img {
        width: 44px;
        height: 44px;
        object-fit: contain;
    }

    .predefined_img .predefined_img_overlay {
        position: absolute;
        right: 42px;
        top: 0;
        visibility: hidden;
        width: max-content;
        border: 1px solid #4d4d4d;
        border-radius: 1rem;
        overflow: hidden;
    }

    .predefined_img .predefined_img_overlay img {
        object-fit: none;
        width: auto;
        height: auto;
    }

    .predefined_img:hover .predefined_img_overlay {
        visibility: visible;
    }
</style>
<div class="modal fade time_use_model" id="loadConsumptionProfile" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="loadConsumptionProfile" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="" id="">Load Consumption Profile</h3>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <table class="table table-borderd">
                            <thead>
                                <tr>
                                    <th>Graph</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="predefined_choose_file" data-id="Florist Wholesale.csv">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_1.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_1.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>Florist Wholesale</td>
                                    <td>Without Kva</td>
                                </tr>
                                <tr class="predefined_choose_file" data-id="Food Products Supplier.csv">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_2.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_2.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>Food Products Supplier</td>
                                    <td>Without Kva</td>
                                </tr>
                                <tr class="predefined_choose_file" data-id="GYM data.csv">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_3.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_3.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>GYM data</td>
                                    <td>Without Kva</td>
                                </tr>
                                <tr class="predefined_choose_file" data-id="supermarket.csv">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_4.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_4.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>supermarket</td>
                                    <td>Without Kva</td>
                                </tr>
                                <tr class="predefined_choose_file" data-id="Daily Peak High Based Load.xlsx">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_5.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_5.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>Daily Peak High Based Load</td>
                                    <td>With Kva</td>
                                </tr>
                                <tr class="predefined_choose_file" data-id="Office_daily_usage_low_load.xls">
                                    <td>
                                        <div class="predefined_img">
                                            <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_6.jpg' ?>" />
                                            <div class="predefined_img_overlay">
                                                <img src="<?php echo base_url() . 'assets/load_consumption_profile/load_cusumption_profile_6.jpg' ?>" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>Office Daily Usage Low Load</td>
                                    <td>Without Kva</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="">Choose</button>
            </div>
        </div>
    </div>
</div>
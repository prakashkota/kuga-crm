<?php
$priority           = [1 => 'Business Critical', 2 => 'Important', 3 => 'Normal', 4 => 'Low'];
$priorityClass      = [1 => 'badge-danger', 2 => 'badge-warning', 3 => 'badge-primary', 4 => 'badge-secondary'];
$runningStatus      = ['Not Started', 'Running', 'In Review', 'Completed'];
$runningStatusClass = ['badge-secondary', 'badge-warning', 'badge-info', 'badge-success'];
?> 
<div class="page-wrapper d-block clearfix badge-">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php if ($this->aauth->is_member('Admin')) { ?>Admin  - <?php } ?> Manage Log Issues</h1>
        <?php if(isset($status) && $status != '3'){?>
            <a class="btn btn-outline-primary" href="<?php echo site_url('admin/page/log-issue/manage?status=3') ?>"><i class="icon-list"></i> Manage Archived Issues </a>
        <?php }else{ ?>
            <a class="btn btn-outline-primary" href="<?php echo site_url('admin/page/log-issue/manage') ?>"><i class="icon-list"></i> Manage UnArchived Issues </a>
        <?php } ?>
    </div>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="franchise_list" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Issue No.</th>
                                        <th>Title</th>
                                        <th>Priority</th>
                                        <th>Franchise Name</th>
                                        <th>Current Status</th>
                                        <th>Created At</th>
                                        <th style="text-align:right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($issues as $key => $value) {
                                        $date_hack = explode(' ',$value['created_at']);
                                        $date_hack = explode('-',$date_hack[0]);
                                        ?>
                                        <tr>
                                            <td align="center"><?php echo 'L-' . $value['id']; ?></td>
                                            <td><?php echo substr($value['title'],0,100); ?></td>
                                            <td><span class="badge badge-pill <?php echo $priorityClass[$value['priority']]; ?>"><?php echo $priority[$value['priority']]; ?></span></td>
                                            <td><?php echo $value['franchise_name']; ?></td>
                                            <td><span class="badge badge-pill <?php echo $runningStatusClass[$value['status']]; ?>"><?php echo $runningStatus[$value['status']]; ?></span></td>
                                            <td><span class="hidden"> <?php echo $date_hack[0] . $date_hack[1] . $date_hack[2]; ?></span><?php echo date('d/m/Y, h:i A',strtotime($value['created_at'])); ?></td>
                                            <td>
                                                <a class="btn btn-primary" href="<?php echo site_url(); ?>admin/log/view/<?php echo $value['id']; ?>"><i class="icon-pencil"></i> View</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME--> 
<!-- JQUERY SCRIPTS --> 
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#franchise_list').DataTable({
            columnDefs: [ {type: 'date-uk', targets: 5}],
        });
    });
</script>
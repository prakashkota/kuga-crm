<link href="https://www.solarrun.com.au/wp-content/themes/themesolarrun/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' href='https://www.solarrun.com.au/wp-content/themes/themesolarrun/style.css' type='text/css'/>
<link type="text/css" rel="stylesheet" href="https://www.solarrun.com.au/wp-content/themes/themesolarrun/css/meanmenu.css" />
<link href="https://www.solarrun.com.au/wp-content/themes/themesolarrun/css/main.css" rel="stylesheet" type="text/css">
<script src="https://www.solarrun.com.au/wp-content/themes/themesolarrun/js/jquery.meanmenu2.js"></script>
<header class="header_wrap">
    <div class="header_area">
        <div class="container">
            <div class="row logo-area">
                <div class="col-sm-3">
                    <div class="logo"><a href="https://www.solarrun.com.au/"><img src="https://www.solarrun.com.au/wp-content/uploads/2018/06/logo-1.png" alt="logo"></a></div>
                </div>
                <div class="col-sm-7 menu-area">
                    <div class="menu">
                        <nav style="display: block;">
                            <ul id="menu-main-menu" class=""><li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="https://www.solarrun.com.au/about-us/">About Us</a></li>
                                <li id="menu-item-420" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-420"><a href="https://www.solarrun.com.au/solar-panels/">Solar Deals</a></li>
                                <li id="menu-item-1070" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1070"><a href="https://www.solarrun.com.au/products/">Products</a></li>
                                <li id="menu-item-1073" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1073"><a href="https://www.solarrun.com.au/franchise-for-sale/">Franchise</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-sm-2 hhhh">
                    <div class="cont-info">
                        Call Now
                        <a href="tel:1300076527"><strong><em>1300076527</em></strong></a>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</header>
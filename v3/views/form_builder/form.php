<style>
    #signature-pad{text-align: center; margin-top:30px;}
    .signature-pad--body canvas{border:1px solid black;}
    .modal-content{height: 362px;}
    .middle-part,.mid-inr{margin:0 !important;}
</style>
<div class="middle-part full-wdth clearfix">
    <div class="mid-inr zoom-box dis-block">
        <div class="top-box2 full-wdth clearfix" style="height:550px;" id="map">
             <link rel="stylesheet" href="<?php echo site_url(); ?>assets/form_builder/uploads/<?php echo $template_data['id']; ?>/base.min.css"/>
                <link rel="stylesheet" href="<?php echo site_url(); ?>assets/form_builder/uploads/<?php echo $template_data['id']; ?>/fancy.min.css"/>
                <link rel="stylesheet" href="<?php echo site_url(); ?>assets/form_builder/uploads/<?php echo $template_data['id']; ?>/<?php echo $template_data['generate_html_file_name']; ?>.css"/>
                <form id="template_form" enctype="multipart/form-data">
                    <?php echo $pdf_html; ?>
                    
                </form>
            <div id="navbar" class="full-wdth clearfix" style="position: inherit; z-index: 10000; bottom: 0px; opacity: 0; display:block !important;" onmouseover="window.form_builder_tool.hoverdiv(event,'navbar','show')" onmouseout="window.form_builder_tool.hoverdiv(event,'navbar','hide')">
                <div id="loader1"></div>
                <input type="submit" class="btn pull-left btn-success" id="save_btn" value="Save" style="color:grey;" />
                <a href="javascript:void(0);" class="btn pull-right btn-warning" id="download_btn" style="background: #fff;">Download Pdf</a>
            </div>
        </div>
        
    </div>
</div>



<div class="modal" id="signature_pad_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div id="signature-pad" class="signature-pad">
                            <div class="signature-pad--body">
                                <canvas></canvas>
                            </div>
                            <div class="signature-pad--footer">
                                <div class="description">Sign above</div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pull-right">
                <div class="col-12 col-sm-12 col-md-12" style="margin: 25px;">
                    <input type="button" id="clear_signature" name="submit" class="btn btn-danger" value="Clear" >
                    <input type="button" id="undo_signature" name="submit" class="btn btn-warning" value="Undo" >
                    <input type="button" id="save_signature" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                </div> 
            </div> 

        </div>
    </div>
</div>
var solar_tool = function (options) {
	var self = this;
	this.section = options.section != undefined ? options.section : '';
	this.lead_id = options.lead_id != undefined ? options.lead_id : '';
	this.site_id = options.site_id != undefined ? options.site_id : '';
	this.annotationList = options.annotationList != undefined ? JSON.parse(options.annotationList) : '';
	this.annotation_context_latLng = null;
	this.proposal_id =
	options.proposal_id != undefined ? options.proposal_id : '';
	this.mtool_data = null;
	this.mtool_data_save = false;

	this.map = null;
	this.google_map = null;
	this.nearmap = null;
	this.map_container_id =
	options.map_container_id != undefined
	? options.map_container_id
	: 'map';
	this.search_box_element_id = 'solar_tool_address_search';
	this.coordinates = { lat: -37.99233116530804, lng: 145.18961320985974 };

	this.selected_address = null;
	this.selected_address_area_rectangle = null;
	this.near_map_imagery = null;
	this.near_map_imagery_overlay = null;
	this.near_map_imagery_rectanlge = null;

	//Panel Config Options
	this.panelConfig = {
		defaultSolarPanelColor: '#384950',
		activeSolarPanelColor: '#209bed',
		overlappedSolarPanelColor: '#ff0000',
		strokeColor: '#e2ebe8',
		fillOpacity: 0.95,
		strokeWeight: 1.5,
		defaultOrientation: 'portrait',
		defaultRotation: 0,
		rotationDirection: 0,
		rotationHeading: 0,
		panelLength: 0.9906,
		panelWidth: 1.651,
		panelId: 0,
		panelWatt: 0,
		rotation: 0,
	};
	this.panel_tool_enable = false;
	this.panel_eraser_enable = false;
	this.gap_between_panel = 0.01;
	this.select_active_tool = null;
	this.select_drawn_tool = null;
	this.enable_select_tool = null;
	this.geometryFactory = new jsts.geom.GeometryFactory();

	//Object Arrays
	this.solarPanels = [];
	this.annotations = [];
	this.panelDragStartReference = null;
	this.select_tool_envelope = null;

	self.handle_mapbox_address_search();

	$(document).on('click', '#save_bounded_area', function () {
		var final_selected_area_bounds =
		self.selected_address_area_rectangle._latlngs[0];

		var bounds = [];
		for (var key in final_selected_area_bounds) {
			bounds[key] = {};
			bounds[key]['lat'] = final_selected_area_bounds[key].lat;
			bounds[key]['lng'] = final_selected_area_bounds[key].lng;
		}

		var formData = {};
		formData.bounds = bounds;
		formData.address_id = self.selected_address.id;
		formData.address = self.selected_address.place_name;
		formData.latitude = final_selected_area_bounds[3].lat;
		formData.longitude = final_selected_area_bounds[3].lng;
		formData.proposal_id = self.proposal_id;
		formData.lead_id = self.lead_id;
		formData.site_id = self.site_id;
		formData.section = self.section;
		formData.min_lat = final_selected_area_bounds[0].lat;
		formData.min_lng = final_selected_area_bounds[0].lng;
		formData.max_lat = final_selected_area_bounds[2].lat;
		formData.max_lng = final_selected_area_bounds[2].lng;

		self.save_near_map_imagery(formData);
	});

	$(document).on('click', '#remove_bounded_area', function () {
		if (
			confirm(
				'This action will result in loss of all the data. Do you want to proceed?'
				)
			) {
			var c = {};
		c.proposal_id = self.proposal_id;
		c.lead_id = self.lead_id;
		c.site_id = self.site_id;
		c.section = self.section;
		self.remove_near_map_imagery(c);
	}
});

	if (self.lead_id != '') {
		var c = {};
		c.proposal_id = self.proposal_id;
		c.lead_id = self.lead_id;
		c.site_id = self.site_id;
		c.section = self.section;
		self.fetch_mapping_tool_data(c);
	}

	$("a[href='#tab_pylon_mapping_tool']").on('shown.bs.tab', function (e) {
		if (self.map == null && self.mtool_data != null) {
			self.fetch_mapping_tool_objects_data();
		} else if (self.map == null) {
			$('#real_map_container').removeClass('d-none');
			$('#loaderContainer').addClass('d-none');
			$('#mtool_left_sidebar_toogle').addClass('d-none');
			self.initalize_map();
		}
	});

	setTimeout(function () {
		const queryString = window.location.search;
		var pattern = /[?&]tab=mapping_tool/;
		if (pattern.test(queryString)) {
			$("a[href='#tab_pylon_mapping_tool']").click();
		}
		self.updateQueryStringParam('tab', null);
	}, 2000);

	$(document).on('click', '.annotation_label_save_btn', function () {
		var aId = $(this).attr('data-id');
		var label = $('.annotation_label_' + aId).val();
		var annotation = null;
		for (var key in self.annotations) {
			var aId1 = self.annotations[key].options.a_id;
			if (aId1 == aId) {
				annotation = self.annotations[key];
			}
		}
		if (annotation != null) {
			annotation.annotation_text.label = label;
			if (annotation._latlng != undefined) {
				self.update_annotation_text_label(annotation);
				self.handle_annotation_crud(annotation);
			} else {
				self.update_custom_annotation_text_label(annotation);
				self.handle_custom_annotation_crud(annotation);
			}
		}
	});

	$(document).on('click', '.annotation_label_delete_btn', function () {
		var aId = $(this).attr('data-id');
		var annotation = null;
		var itemIndex = [];
		for (var key in self.annotations) {
			var aId1 = self.annotations[key].options.a_id;
			if (aId1 == aId) {
				annotation = self.annotations[key];
				itemIndex = key;
			}
		}
		if (annotation != null) {
			if (confirm('Are you sure you want to remove this annotation?')) {
				if (
					annotation['annotation'] != undefined &&
					annotation._latlng != undefined
					) {
					annotation['annotation'].remove();
			}
			if (annotation['annotation_text'] != undefined) {
				annotation['annotation_text'].remove();
			}
			if (annotation['annotation_innerPointCap'] != undefined) {
				annotation['annotation_innerPointCap'].remove();
			}
			if (annotation['annotation_outerPointCap'] != undefined) {
				annotation['annotation_outerPointCap'].remove();
			}
			if (annotation['annotation_line'] != undefined) {
				annotation['annotation_line'].remove();
			}

			if (annotation._latlng != undefined) {
				self.handle_annotation_crud(annotation, 'remove');
			} else {
				self.handle_custom_annotation_crud(annotation, 'remove');
			}
			annotation.remove();
			$('#annotation_list_item_' + aId).remove();
			self.annotations.splice(itemIndex, 1);
		}
	}
});

	$(document).on('click', '#tool_panel_design_notes', function () {
		$('#panel_notes_modal').modal('show');
	});

	$(document).on('click', '#save_panel_notes_btn', function () {
		var editor_data = tinyMCE.get('panel_notes').getContent().trim();
		var data = {};
		data.key = 'notes';
		data.value = editor_data;
		data.tool_id = self.mtool_data.id;
		self.save_mapping_data(data);
	});

	$(document).on('change', '#nm_imagery_datepicker_select', function () {
		var data = {};
		data.selected_date = $(this).val();
		data.tool_id = self.mtool_data.id;
		self.update_near_map_imagery(data);
	});

	$(document).on('click', '#tool_select_reset_btn', function (e) {
		e.preventDefault();
		self.reset_select_tool_envelope();
	});
};

solar_tool.prototype.updateQueryStringParam = function (key, value) {
	var baseUrl = [
	location.protocol,
	'//',
	location.host,
	location.pathname,
	].join(''),
	urlQueryString = document.location.search,
	newParam = key + '=' + value,
	params = '?' + newParam;
	if (urlQueryString) {
		updateRegex = new RegExp('([?&])' + key + '[^&]*');
		removeRegex = new RegExp('([?&])' + key + '=[^&;]+[&;]?');
		if (typeof value == 'undefined' || value == null || value == '') {
			params = urlQueryString.replace(removeRegex, '$1');
			params = params.replace(/[&;]$/, '');
		} else if (urlQueryString.match(updateRegex) !== null) {
			params = urlQueryString.replace(updateRegex, '$1' + newParam);
		} else {
			params = urlQueryString + '&' + newParam;
		}
	}
	window.history.replaceState({}, '', baseUrl + params);
};

//Step 0
solar_tool.prototype.fetch_mapping_tool_data = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/fetch_mapping_tool_data',
		type: 'get',
		data: formData,
		dataType: 'json',
		beforeSend: function () {
			$('#solar_tool_address_search').attr('readonly', 'readonly');
			$('#save_bounded_area').parent().addClass('d-none');
		},
		success: function (data) {
			if ($('#panel_notes')) {
				tinymce.init({
					selector: '#panel_notes',
					branding: false,
					menubar: false,
					statusbar: false,
					plugins: 'lists,link',
					toolbar:
					'bold italic underline backcolor permanentpen formatpainter numlist bullist removeformat link image media pageembed fullscreen',
					mode: 'textareas',
					mobile: {
						theme: 'silver',
					},
					entity_encoding: 'raw',
					height: 500,
				});
			}

			if (data.mtool_data.id != undefined) {
				self.mtool_data = data.mtool_data;
				self.near_map_imagery = data.mtool_data.nm_imagery;
				self.selected_address_area_rectangle = {
					bounds: data.mtool_data.bounds,
					address_id: data.mtool_data.mbox_address_id,
				};

				var features = turf.points([
					[parseFloat(data.mtool_data.bounds[0].lat), parseFloat(data.mtool_data.bounds[0].lng)],
					[parseFloat(data.mtool_data.bounds[1].lat), parseFloat(data.mtool_data.bounds[1].lng)],
					[parseFloat(data.mtool_data.bounds[2].lat), parseFloat(data.mtool_data.bounds[2].lng)],
					[parseFloat(data.mtool_data.bounds[3].lat), parseFloat(data.mtool_data.bounds[3].lng)],
					]);
				var center = turf.center(features).geometry.coordinates;

				if(center.length == 2){
					self.coordinates = {
						lat: center[0],
						lng: center[1],
					};
				}else{
					self.coordinates = {
						lat: data.mtool_data.bounds[3].lat,
						lng: data.mtool_data.bounds[3].lng,
					};
				}

				$('#solar_tool_address_search').val(data.mtool_data.address);
				$('#remove_bounded_area').parent().removeClass('d-none');

				/**
				if(data.mtool_data.snapshot1 != null){
					setTimeout(function(){
						$("#proposal_no_image").attr("style", "display:none !important;");
						$("#proposal_image").attr("style", "display:block !important;");
						$("#proposal_image > .add-picture").css(
							"background-image",
							'url("' + data.mtool_data.snapshot1 + '")'
							);
						$("#proposal_image > .add-picture").css("background-repeat", "no-repeat");
						$("#proposal_image > .add-picture").css("background-position", "50% 50%");
						$("#proposal_image > .add-picture").css("background-size", "100% 100%");
					},2000);
				}
				*/

				if (data.mtool_data.notes != null) {
					tinymce
					.get('panel_notes')
					.setContent(data.mtool_data.notes);
					$('#panel_notes').val(data.mtool_data.notes);
				}
			} else {
				$('#solar_tool_address_search').removeAttr('readonly');
				$('#solar_tool_address_search').val(
					data.customer_site.site_name
					);
			}
			setTimeout(function () {
				//self.fetch_mapping_tool_objects_data();
			}, 3000);
		},
	});
};

//Step 1
solar_tool.prototype.handle_mapbox_address_search = function () {
	var self = this;

	$('#solar_tool_address_search').autocomplete({
		minLength: 2,
		source: function (request, response) {
			var keyword = request.term;
			keyword = encodeURIComponent(keyword);
			$.ajax({
				url:
				'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
				keyword +
				'.json',
				type: 'get',
				data: {
					access_token:
					'pk.eyJ1IjoiZGFuaWVsLXB5bG9uIiwiYSI6ImNqMGx6c3hjbDAwMjczM3A5aWx6aHhmM3MifQ.cA-6ahyjexJyGrc4xpCO8w',
					types: 'address,locality',
					limit: 5,
					proximity: '75.7105,26.9525',
					country: 'au',
				},
				dataType: 'json',
				success: function (data) {
					response(
						$.map(data.features, function (item) {
							return {
								label: item.place_name,
								value: item.place_name,
								data: item,
							};
						})
						);
				},
			});
		},
		select: function (event, ui) {
			self.selected_address = ui.item.data;
			self.coordinates = {
				lat: ui.item.data.center[1],
				lng: ui.item.data.center[0],
			};
			if (self.map == null) {
				self.initalize_map();
			} else {
				self.map.remove();
				self.map = null;
				self.initalize_map();
				if (self.selected_address_area_rectangle != null) {
					self.selected_address_area_rectangle.remove();
				}
			}
			self.add_area_selector_to_map();
			$('#save_bounded_area').parent().removeClass('d-none');
		},
	}).bind('focus', function(){ $(this).autocomplete("search"); } );
};
//diable right click on the left toolabar
$('#mtool_left_sidebar').bind('contextmenu', function(e) {
	return false;
});

//Step 2
solar_tool.prototype.initalize_map = function () {
	var self = this;

	//Load on Default Address
	var options = {
		keyboard: true,
		zoomControl: !1,
		scrollWheelZoom: true,
		attributionControl: !1,
		maxZoom: 23,
		doubleClickZoom: false,
		fullscreenControl: true,
		fullscreenControlOptions: {
			pseudoFullscreen: false,
			position: 'topright',
		},
	};

	if (self.selected_address_area_rectangle != null && self.mtool_data.id != undefined) {
		options['scrollWheelZoom'] = false;
		options['gestureHandling'] = true;
		options['contextmenu'] = true;
		options['contextmenuWidth'] = true;
		var contextmenuItems = [
		{
			cId : '',
			text: 'Center map here',
			callback: function(e){
				self.map.panTo(e.latlng);
			}
		},
		{
			cId : '',
			text: 'Zoom in',
			icon: base_url + 'assets/map-tool/zoom-in.png',
			callback: function(e){
				self.map.zoomIn();
			}
		},
		{
			cId : '',
			text: 'Zoom out',
			icon: base_url + 'assets/map-tool/zoom-out.png',
			callback: function(e){
				self.map.zoomOut();
			}
		},
		'-',
		];

		for(var key in self.annotationList){
			var cmObj = {
				cId : 'annotation_'+self.annotationList[key]['id'],
				text: 'Add ' + self.annotationList[key]['name'],
				icon: base_url + 'assets/map-tool/annotations/' + self.annotationList[key]['icon'],
				callback: function(e){
					self.annotation_context_latLng = e.latlng;
				},
			};
			contextmenuItems.push(cmObj);
		}

		options['contextmenuItems'] = contextmenuItems;
	}

	self.map = L.map(self.map_container_id, options)
	.setView([self.coordinates.lat, self.coordinates.lng], 19, {
		animation: true,
	});

	if(self.map.contextmenu != undefined){

		self.map.on('contextmenu.select', function(el){
			var id = el.el.getAttribute('id');
			id = id.split('_');
			if(id.length > 1){
				if(id[0] == 'annotation'){
					$('#annotation_list').val(id[1]);
					$('#add-annotation').click();
				}
			}
		});
	}

	L.control
	.zoom({
		position: 'bottomright',
	})
	.addTo(self.map);

	//Add All Attributions
	L.control
	.attribution({
		prefix: 'Powered by <a href="http://leafletjs.com" target="_blank">Leaflet</a>',
		position: 'bottomleft',
	})
	.addTo(self.map);

	//Add Google Mutant Layer To Replace Imagery Too Google
	self.google_map = L.gridLayer
	.googleMutant({
		type: 'hybrid',
		maxZoom: 23,
		maxNativeZoom: 21,
		attribution:
		'Imagery &copy; <a href="https://maps.google.com" target="_blank">Google</a>',
	})
	.addTo(self.map);

	//Handle Panel List and Left Toolbar
	if (self.map != null && self.mtool_data != null) {
		$(document).on('change', '#mtool_panel_list', function () {
			var id = $(this).val();
			$('.panel-library-item').addClass('d-none');
			$('#panel_list_item_' + id).removeClass('d-none');
			$('#system_pricing_panel option').hide();
			$('#system_pricing_panel').val($('#mtool_panel_list option:selected').text().trim());
			$('#system_pricing_panel option:selected').show();
			var data_degradation = $('#mtool_panel_list option:selected').attr('data-degradation');

			$('input#pv_degradation_factor').val(data_degradation);

		});
		$('#system_pricing_panel').trigger('change');

		self.handle_left_toolbar();

		//$("#tool_panel_design_notes").detach().appendTo('.leaflet-top.leaflet-right');
		$('#tool_panel_design_notes').removeClass('d-none');
		$('.left-panel-toggle').detach().appendTo('.leaflet-top.leaflet-left');
		$('.designer__sidebar').detach().appendTo('.leaflet-top.leaflet-left');

		$('.left-panel-toggle').click(function () {
			$('.left-panel-toggle').toggleClass('active');
			$('.designer__sidebar').toggleClass('active');
		});
		self.map.doubleClickZoom.disable();
		var mtool_left_sidebar = L.DomUtil.get('mtool_left_sidebar');
		L.DomEvent.disableClickPropagation(mtool_left_sidebar);

		self.create_snapshot();

		if (self.mtool_data.snapshot1 != null) {
			$('#tool_download_snapshot1').removeClass('d-none');
			$('#tool_download_snapshot2').removeClass('d-none');
			$('#tool_download_snapshot1')
			.detach()
			.appendTo('.leaflet-top.leaflet-right');
			$('#tool_download_snapshot2')
			.detach()
			.appendTo('.leaflet-top.leaflet-right');
			$('#tool_download_snapshot2_link').attr(
				'href',
				self.mtool_data.snapshot1
				);
			$('#tool_download_snapshot1_link').attr(
				'href',
				self.mtool_data.snapshot2
				);
		}

		if (self.mtool_data.nm_imagery_surveys != null) {
			$('#nm_imagery_datepicker').removeClass('d-none');
			$('#nm_imagery_datepicker_select').html('');
			var sv = self.mtool_data.nm_imagery_surveys;
			var select = document.getElementById(
				'nm_imagery_datepicker_select'
				);
			for (var key in sv) {
				var option = document.createElement('option');
				if (
					sv[key]['captureDate'] ==
					self.mtool_data.nm_imagery_selected_date
					) {
					option.setAttribute('selected', 'selected');
			}
			option.setAttribute('value', sv[key]['captureDate']);
			option.innerHTML = moment(sv[key]['captureDate']).format('LL');
			select.appendChild(option);
		}
	}
}
};

//Step 3
solar_tool.prototype.add_area_selector_to_map = function () {
	var self = this;
	var circle = new L.Circle(
		[self.coordinates.lat, self.coordinates.lng],
		80
		).addTo(self.map);
	var rectangleBounds = circle.getBounds();
	circle.remove();
	var rectangleOptions = {
		color: 'white',
		weight: 3,
		draggable: true,
		resizeable: true,
		fill: false,
		editable: true,
	};
	self.selected_address_area_rectangle = L.rectangle(
		rectangleBounds,
		rectangleOptions
		).addTo(self.map);
};

//Step 4
solar_tool.prototype.save_near_map_imagery = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/save_near_map_imagery',
		type: 'post',
		data: formData,
		dataType: 'json',
		beforeSend: function () {
			$('#solar_tool_address_search').attr('readonly', 'readonly');
			$('#save_bounded_area').parent().addClass('d-none');
			toastr['info'](
				'Saving Details and creating nearmap image for bounded area.'
				);
		},
		success: function (data) {
			toastr['info']('Address Saved Successfully.');
			self.updateQueryStringParam('tab', 'mapping_tool');
			window.location.reload();
		},
	});
};

//Step 4
solar_tool.prototype.remove_near_map_imagery = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/remove_near_map_imagery',
		type: 'post',
		data: formData,
		dataType: 'json',
		beforeSend: function () {
			$('#solar_tool_address_search').attr('readonly', 'readonly');
			$('#remove_bounded_area').parent().addClass('d-none');
			toastr['info']('Removing Details. Please Wait...');
		},
		success: function (data) {
			toastr['info']('Address Deleted Successfully.');
			window.location.reload();
		},
	});
};

//Step 5 - Initialize Map with nearmap imagery
solar_tool.prototype.initalize_map_with_near_map_imagery = function () {
	var self = this;

	//Display Toolbar
	$('.ltoolbar').removeClass('d-none');
	//$('#tool_select').click();
	//$('#tool_hand').click();

	//Get Rectangle bounds
	var final_selected_area_bounds =
	self.selected_address_area_rectangle.bounds;
	var bounderAreaData = {
		min_lat: final_selected_area_bounds[0].lat,
		min_lng: final_selected_area_bounds[0].lng,
		max_lat: final_selected_area_bounds[2].lat,
		max_lng: final_selected_area_bounds[2].lng,
		address_id: self.selected_address_area_rectangle.address_id,
	};

	//Remove Editable Rectangle
	if (self.selected_address_area_rectangle.address_id == undefined) {
		self.selected_address_area_rectangle.remove();
	}

	//Replace Image
	var imageryBounds = L.latLngBounds(
		L.latLng(bounderAreaData.min_lat, bounderAreaData.min_lng),
		L.latLng(bounderAreaData.max_lat, bounderAreaData.max_lng)
		);
	var c = { opacity: 1, attribution: 'Nearmap', zIndex: 10 };
	self.near_map_imagery_overlay = L.imageOverlay(
		self.near_map_imagery,
		imageryBounds,
		c
		).addTo(self.map);
	self.near_map_imagery_overlay.bringToFront();

	//self.map.fitBounds(imageryBounds);
	//self.map.setMaxBounds(imageryBounds);
	//self.map.setZoom(20);
	//Added A Rectangle to Highlight
	var c = {
		color: 'white',
		weight: 3,
		draggable: false,
		resizeable: false,
		fill: false,
	};
	self.near_map_imagery_rectanlge = L.rectangle(imageryBounds, c).addTo(
		self.map
		);

	var drawnItems = new L.FeatureGroup();
	self.map.addLayer(drawnItems);
	drawnItems.bringToFront();

	self.handle_panel_right_sidebar();
	self.handle_annotation_right_sidebar();



	self.map.on('draw:created', function (e) {
		var type = e.layerType,
		layer = e.layer;

		drawnItems.addLayer(layer);
		self.select_drawn_tool = layer;
		layer.editing.enable();

		if (layer.options.is_select_tool != undefined) {
			var drawnBox = self.createJstsTurfPolygon(
				self.geometryFactory,
				layer
				);

			for (var key in self.solarPanels) {
				var staticPanel = self.createJstsTurfPolygon(
					self.geometryFactory,
					self.solarPanels[key]
					);
				var belongs = turf.booleanContains(drawnBox, staticPanel);

				if (belongs) {
					self.solarPanels[key].setStyle({
						fillColor: self.panelConfig.activeSolarPanelColor,
						sp_selected: true,
					});
				} else {
					self.solarPanels[key].setStyle({
						fillColor: self.panelConfig.defaultSolarPanelColor,
						sp_selected: false,
					});
				}
			}
			layer.editing.disable();
			layer.remove();
			drawnItems.removeLayer(layer);
			self.calculate_system_summary();
			$('#tool_select_reset_btn').removeClass('d-none');

			if(self.select_tool_envelope != null){
				self.select_tool_envelope.remove();
			}

			if(self.selectedPanels > 1){
				var points = [];
				var allDrawnPanels = self.solarPanels;
				for (var key in allDrawnPanels) {
					if (allDrawnPanels[key]['options']['sp_selected'] == true) {
						var c = allDrawnPanels[key].getLatLngs()[0];
						for(var k in c){
							var c1 = [parseFloat(c[k].lat),parseFloat(c[k].lng)];
							points.push(c1);
						}
					}
				}
				var features = turf.points(points);
				var envelope = turf.envelope(features).geometry.coordinates;
				var c = {
					fill: false,
					fillOpacity: 0,
					weight: 2,
					color: self.panelConfig.activeSolarPanelColor,
				};
				self.select_tool_envelope = L.rectangle(envelope, c, { zIndex: 1000 }).addTo(self.map);
			}
		}

		if (layer.options.is_annotation != undefined) {
			layer.editing.disable();
			self.create_custom_annotation_text(layer);
			self.create_custom_annotation_line(layer);
		}
	});
};

/** Drawing Object Related Functions */
solar_tool.prototype.create_panel = function (latLng, panelKey,panelRotation = this.panelConfig.defaultRotation) {
	var self = this;

	if (!self.panel_tool_enable) {
		return false;
	}

	var dimension = document
	.getElementById('mtool_panel_list')
	.options[
	document.getElementById('mtool_panel_list').selectedIndex
	].getAttribute('data-dimension')
	.split(',');
	self.panelConfig.panelLength = dimension[0];
	self.panelConfig.panelWidth = dimension[1];
	self.panelConfig.panelId =
	document.getElementById('mtool_panel_list').value;
	self.panelConfig.panelWatt = document
	.getElementById('mtool_panel_list')
	.options[
	document.getElementById('mtool_panel_list').selectedIndex
	].getAttribute('data-capacity');

	var tilt = document.getElementById('mtool_tilt').value || 0;
	var width = self.panelConfig.panelWidth;
	var height = self.panelConfig.panelLength;
	var bounds = self.get_panel_latLng_by_dimension(latLng, width, height);

	var c = {
		color: self.panelConfig.strokeColor,
		weight: self.panelConfig.strokeWeight,
		fill: true,
		fillOpacity: self.panelConfig.fillOpacity,
		fillColor: self.panelConfig.activeSolarPanelColor,
		sp_id: self.create_uuid(),
		sp_selected: true,
		sp_orientation: self.panelConfig.defaultOrientation,
		sp_rotation: panelRotation,
		sp_tilt: tilt,
		sp_panel_value: width + ',' + height,
		sp_panel_id: self.panelConfig.panelId,
		sp_watt: self.panelConfig.panelWatt,
		transform: false,
		draggable: true,
	};


	var rectangle = L.rectangle(bounds, c).addTo(self.map);
	var coords = [
	{
		lat: rectangle.getBounds().getNorthEast().lat,
		lng: rectangle.getBounds().getNorthEast().lng,
	},
	{
		lat: rectangle.getBounds().getNorthEast().lat,
		lng: rectangle.getBounds().getSouthWest().lng,
	},
	{
		lat: rectangle.getBounds().getSouthWest().lat,
		lng: rectangle.getBounds().getSouthWest().lng,
	},
	{
		lat: rectangle.getBounds().getSouthWest().lat,
		lng: rectangle.getBounds().getNorthEast().lng,
	},
	];
	rectangle.remove();

	var panel = L.polygon(coords, c).addTo(self.map);

	if (panelKey != undefined && panelKey != '' ) {
		var oldPanel = self.solarPanels[panelKey];
		self.handle_panel_crud(oldPanel, 'remove');
		oldPanel.arrow.remove();
		oldPanel.remove();
		//Adding Arrows
		var panelArrow = self.create_panel_arrow(panel);
		panel['arrow'] = panelArrow;

		panel.dragging.enable();
		panel
		.on('dragstart', function (e) {
			e.target.arrow.remove();
			self.panel_tool_enable = false;
			self.panelDragStartReference = e.target.getBounds().getCenter();
		})
		.on('drag', function (e) {
			self.handle_panel_drag_actions(e.target);
		})
		.on('dragend', function (e) {
			self.panel_select_multiple_move(e.target);
			setTimeout(function () {
				self.panel_tool_enable = true;
			}, 200);
			var panelArrow = self.create_panel_arrow(e.target);
			e.target['arrow'] = panelArrow;
		});
		self.handle_panel_click(panel);
		self.solarPanels[panelKey] = panel;
	} else {
		//Adding Arrows
		var panelArrow = self.create_panel_arrow(panel);
		panel['arrow'] = panelArrow;

		panel.dragging.enable();
		panel
		.on('dragstart', function (e) {
			e.target.arrow.remove();
			self.panel_tool_enable = false;
			self.panelDragStartReference = e.target.getBounds().getCenter();
		})
		.on('drag', function (e) {
			self.handle_panel_drag_actions(e.target);
		})
		.on('dragend', function (e) {
			setTimeout(function () {
				self.panel_tool_enable = true;
			}, 200);
			var panelArrow = self.create_panel_arrow(e.target);
			e.target['arrow'] = panelArrow;
			self.handle_panel_crud(e.target);
		});
		self.handle_panel_click(panel);
		self.solarPanels.push(panel);
	}

	self.rotate_solar_panel(
		panel,
		self.panelConfig.defaultRotation,
		self.panelConfig.defaultRotation,
		1
		);
	return panel;
};

solar_tool.prototype.create_panel_arrow = function (panel) {
	var self = this;
	var pointList = self.arrowCords(panel);
	var panelArrow = new L.polyline(pointList, {
		color: '#fff',
		strokeColor: '#fff',
		weight: 1,
		opacity: 1,
	}).addTo(self.map);
	return panelArrow;
};

solar_tool.prototype.remove_solar_panel = function (coordinate) {
	var self = this;

	self.mtool_data_save = false;

	if (coordinate) {
		var deletedIndex = [];
	} else {
		var deletedIndex = [];
		var totalDrawnPanels = self.solarPanels.length;
		var allDrawnPanels = self.solarPanels;

		//Delete All Selected Panels
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				deletedIndex.push(key);
			}
		}
		//If No Panels Deleted Start From Last Drawn
		if (deletedIndex.length == 0) {
			if (allDrawnPanels.length > 0) {
				var key = totalDrawnPanels - 1;
				deletedIndex.push(key);
			}
		}
		//Remove those from array adn database
		var deletePanels = [];
		for (var key in deletedIndex) {
			var panel = self.solarPanels[deletedIndex[key]];
			deletePanels.push(panel);
			//self.handle_panel_crud(panel, 'remove');
		}
	}
	if(deletePanels.length == self.solarPanels.length){
		$('#mtool_panel_list option').show();
	}
	setTimeout(function(){
		self.mtool_data_save = true;
		self.handle_panel_remove_bulk('REMOVE',deletePanels);
	},800);
};

solar_tool.prototype.rotate_solar_panel = function (
	polygon,
	angle,
	actualRotation,
	overridePanel,
	centroid
	) {
	if (
		typeof actualRotation != 'undefined' &&
		(actualRotation >= 360 || actualRotation <= -360)
		) {
		if (actualRotation > 0) {
			actualRotation = actualRotation - 360;
		} else {
			actualRotation = parseInt(actualRotation) + parseInt(360);
		}
	}
	var self = this;
	actualRotation = actualRotation || 0;

	// if the rotation is negetive then change it to equivalent positive value
	if (actualRotation < 0) {
		actualRotation = 360 + actualRotation;
	}

	if(self.selectedPanels > 1 && (centroid != null && centroid != undefined)){
		var origin = centroid;
		//new L.Circle(origin, 0.1).addTo(self.map);
	}else{
		if (overridePanel != null) {
			var origin = polygon.getLatLngs()[0][1];
		} else {
			var origin = polygon.getBounds().getCenter();
		}
	}
	var coords = [];
	var latLngs = polygon.getLatLngs()[0];
	latLngs.forEach(function (latLng) {
		var rotatedLatLng = L.GeometryUtil.rotatePoint(
			self.map,
			latLng,
			angle,
			origin
			);
		coords.push(rotatedLatLng);
	});

	polygon['options']['sp_rotation'] = actualRotation;
	polygon.arrow.remove();
	//Update PanelCords
	polygon.setLatLngs(coords);
	//Update ArrowCords
	var panelArrow = self.create_panel_arrow(polygon);
	polygon['arrow'] = panelArrow;

	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options']['sp_id'] == polygon['options']['sp_id']){
			self.solarPanels[key] = polygon;
		}
	}

	document.getElementById('mtool_individual_rotation').value = actualRotation;

	self.handle_panel_crud(polygon);
};

solar_tool.prototype.find_selected_panel = function () {
	var self = this;
	var firstPanel = null;
	var lastPanel = null;
	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options']['sp_selected'] == true) {
			if(firstPanel == null){
				firstPanel = self.solarPanels[key];
			}
			lastPanel = self.solarPanels[key];
		}
	}
	if (lastPanel == null) {
		var totalDrawnPanels = self.solarPanels.length;
		lastPanel = self.solarPanels[totalDrawnPanels - 1];
	}
	return [firstPanel,lastPanel];
};

solar_tool.prototype.findFirstLastSelectAndUnSelectPanel = function () {
	var self = this;
	var firstPanel = null;
	var lastPanel = null;
	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options']['sp_selected'] == true) {
			if(firstPanel == null){
				firstPanel = self.solarPanels[key];
			}
			lastPanel = self.solarPanels[key];
		}
		self.solarPanels[key].setStyle({
			fillColor: self.panelConfig.defaultSolarPanelColor,
			sp_selected: false,
		});
		self.solarPanels[key]['options']['sp_selected']=false;
	}
	if (lastPanel == null) {
		var totalDrawnPanels = self.solarPanels.length;
		lastPanel = self.solarPanels[totalDrawnPanels - 1];
	}
	return [firstPanel,lastPanel];
};

solar_tool.prototype.select_all_panel = function (action) {
	var self = this;
	for (var key in self.solarPanels) {
		if (action == 'all') {
			self.solarPanels[key].setStyle({
				fillColor: self.panelConfig.activeSolarPanelColor,
				sp_selected: true,
			});
		} else {
			self.solarPanels[key].setStyle({
				fillColor: self.panelConfig.defaultSolarPanelColor,
				sp_selected: false,
			});
		}
	}
};
var ctrlPressed = false;
$(document).keydown(function(evt) {
	if (evt.which == 17) { // ctrl
		ctrlPressed = true;
	}
}).keyup(function(evt) {
	if (evt.which == 17) { // ctrl
		ctrlPressed = false;
	}
});
solar_tool.prototype.handle_panel_click = function (panel) {
	var self = this;

	panel.on('click', function (e) {
		if (self.panel_eraser_enable == true) {
			self.panel_tool_enable = false;
			for (var key in self.solarPanels) {
				self.solarPanels[key].setStyle({
					fillColor: self.panelConfig.defaultSolarPanelColor,
					sp_selected: false,
				});
			}
			var currentPanel = e.target;
			currentPanel.setStyle({
				fillColor: self.panelConfig.activeSolarPanelColor,
				sp_selected: true,
			});

			self.remove_solar_panel();
			self.calculate_system_summary();
		} else if(!self.enable_select_tool) {
			//All Previous Panels to Default color
			if(!ctrlPressed){
				for (var key in self.solarPanels) {
					self.solarPanels[key].setStyle({
						fillColor: self.panelConfig.defaultSolarPanelColor,
						sp_selected: false,
					});
				}
			}
			var currentPanel = e.target;
			currentPanel.setStyle({
				fillColor: self.panelConfig.activeSolarPanelColor,
				sp_selected: true,
			});

			self.panelConfig.defaultRotation = currentPanel['options'].sp_rotation;
			document.getElementById('mtool_individual_rotation').value = self.panelConfig.defaultRotation;
			document.getElementById('mtool_tilt').value = currentPanel['options'].sp_tilt;
		}
		self.calculate_system_summary();
		$('#tool_manage_panel').click();
	});


};

solar_tool.prototype.createJstsTurfPolygon = function (
	geometryFactory,
	polygon
	) {
	var self = this;
	var path = polygon.getLatLngs()[0];
	var coordinates = path.map(function name(coord) {
		return [coord.lat, coord.lng];
	});
	coordinates.push(coordinates[0]);
	return turf.polygon([coordinates]);
};

solar_tool.prototype.handle_panel_drag_actions = function (panel) {
	var self = this;
	if(self.enable_select_tool) {
		return false;
	}
	for (var key in self.solarPanels) {
		if (panel.options.sp_id != self.solarPanels[key].options.sp_id) {
			var movingPanel = self.createJstsTurfPolygon(
				self.geometryFactory,
				panel
				);
			var staticPanel = self.createJstsTurfPolygon(
				self.geometryFactory,
				self.solarPanels[key]
				);
			var intersection = turf.booleanOverlap(movingPanel, staticPanel);
			if (intersection) {
				self.solarPanels[key].setStyle({
					fillColor: self.panelConfig.overlappedSolarPanelColor,
				});
			} else {
				if (self.solarPanels[key].sp_selected) {
					if (
						self.solarPanels[key].fillColor ==
						self.panelConfig.overlappedSolarPanelColor
						) {
						self.solarPanels[key].setStyle({
							fillColor: self.panelConfig.activeSolarPanelColor,
						});
					}
				} else {
					self.solarPanels[key].setStyle({
						fillColor: self.panelConfig.defaultSolarPanelColor,
					});
				}
			}
		}
	}
};

solar_tool.prototype.create_annotation = function (latLng, panelKey) {
	var self = this;

	var annotationId = document.getElementById('annotation_list').value;
	var c = new L.circle(latLng, {
		radius: 2,
		color: 'transparent',
		fill: true,
		fillColor: 'transparent',
		draggable: true,
		a_id: self.create_uuid(),
		id: annotationId,
	});
	c.addTo(self.map);
	c.dragging.enable();

	self.create_annotation_line(c);

	self.create_annotation_icon(c);

	self.create_annotation_text(c);

	c.on('drag', function (e) {
		var newLatLng = e.target.getLatLng();
		var nc = L.circle(newLatLng, {
			radius: 2,
			color: 'transparent',
			fill: true,
			fillColor: 'transparent',
		}).addTo(self.map);
		var newBounds = nc.getBounds();
		nc.remove();
		e.target.annotation.setBounds(newBounds);

		//Update Line Bounds
		var radius = e.target.getRadius();
		var innerCoordinate = e.target.getLatLng();
		var outerCoordinate = e.target.annotation_line.getLatLngs()[0];
		var angleToClip = L.GeometryUtil.angle(
			self.map,
			innerCoordinate,
			outerCoordinate
			);
		innerCoordinate = L.GeometryUtil.destination(
			innerCoordinate,
			angleToClip,
			radius
			);
		var newCoordinates = [outerCoordinate, innerCoordinate];
		e.target.annotation_line.setLatLngs(newCoordinates);
		e.target.annotation_innerPointCap.setLatLng(innerCoordinate);

		var interSect = L.GeometryUtil.belongsSegment(
			outerCoordinate,
			newBounds.getNorthEast(),
			newBounds.getSouthWest(),
			0.2
			);
		if (interSect) {
			e.target.annotation_outerPointCap.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
			e.target.annotation_innerPointCap.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
			e.target.annotation_line.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
		} else {
			e.target.annotation_outerPointCap.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
			e.target.annotation_innerPointCap.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
			e.target.annotation_line.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
		}

		self.update_annotation_text(e.target);
	}).on('dragend', function (e) {
		self.handle_annotation_crud(e.target);
	});

	//c.editing.enable();
	c.on('mouseover', function (event) {
		c.setStyle({ color: '#00FFFF' });
	});

	c.on('mouseout', function (event) {
		c.setStyle({ color: 'transparent' });
	});

	self.annotations.push(c);

	self.handle_annotation_crud(c);
	self.create_annotation_list_item(c);
};

solar_tool.prototype.create_annotation_icon = function (circle) {
	var self = this;
	var circleBounds = circle.getBounds();
	var annotationIconUrl = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-icon');
	var annotationIcon = L.imageOverlay(annotationIconUrl, circleBounds, {
		zIndex: 1000,
	}).addTo(self.map);
	annotationIcon.bringToFront();
	circle['annotation'] = annotationIcon;
};

solar_tool.prototype.create_annotation_line = function (circle) {
	var self = this;

	var annotation_innerPointCapLatLng = circle.getLatLng();

	var annotation_innerPointCap = L.circle(annotation_innerPointCapLatLng, {
		radius: 0.2,
		color: 'transparent',
		fill: true,
		fillColor: 'transparent',
		draggable: true,
		fillOpacity: 1,
	}).addTo(self.map);
	circle['annotation_innerPointCap'] = annotation_innerPointCap;

	var outerCirclePoints = annotation_innerPointCapLatLng;
	var annotation_outerPointCap = L.circle(outerCirclePoints, {
		radius: 0.5,
		color: 'transparent',
		fill: true,
		fillColor: 'transparent',
		draggable: true,
		fillOpacity: 1,
	}).addTo(self.map);
	annotation_outerPointCap.dragging.enable();
	circle['annotation_outerPointCap'] = annotation_outerPointCap;

	annotation_outerPointCap
	.on('drag', function (e) {
			//Circle Show Border
			circle.setStyle({ color: '#CF1124' });

			//Update Line Bounds
			var circleBounds = circle.getBounds();
			var radius = circle.getRadius();
			var innerCoordinate = circle.getLatLng();
			var outerCoordinate = e.target.getLatLng();
			var angleToClip = L.GeometryUtil.angle(
				self.map,
				innerCoordinate,
				outerCoordinate
				);
			innerCoordinate = L.GeometryUtil.destination(
				innerCoordinate,
				angleToClip,
				radius
				);
			var newCoordinates = [outerCoordinate, innerCoordinate];
			circle.annotation_line.setLatLngs(newCoordinates);
			circle.annotation_innerPointCap.setLatLng(innerCoordinate);

			var interSect = L.GeometryUtil.belongsSegment(
				outerCoordinate,
				circleBounds.getNorthEast(),
				circleBounds.getSouthWest(),
				0.2
				);
			if (interSect) {
				circle.annotation_outerPointCap.setStyle({
					color: 'transparent',
					fillColor: 'transparent',
				});
				circle.annotation_innerPointCap.setStyle({
					color: 'transparent',
					fillColor: 'transparent',
				});
				circle.annotation_line.setStyle({
					color: 'transparent',
					fillColor: 'transparent',
				});
			} else {
				circle.annotation_outerPointCap.setStyle({
					color: '#CF1124',
					fillColor: '#CF1124',
				});
				circle.annotation_innerPointCap.setStyle({
					color: '#CF1124',
					fillColor: '#CF1124',
				});
				circle.annotation_line.setStyle({
					color: '#CF1124',
					fillColor: '#CF1124',
				});
			}
		})
	.on('dragend', function (e) {
		circle.setStyle({ color: 'transparent' });
		self.handle_annotation_crud(circle);
	});

	var annotation_outerPointCapLatLng = annotation_outerPointCap.getLatLng();
	var annotationLine = L.polyline(
		[annotation_innerPointCapLatLng, annotation_outerPointCapLatLng],
		{
			color: 'transparent',
			stroke: true,
			weight: 3,
		}
		).addTo(self.map);
	circle['annotation_line'] = annotationLine;
};

solar_tool.prototype.create_annotation_text = function (circle) {
	var self = this;

	var annotationName = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-name');
	annotationName =
	annotationName.charAt(0).toUpperCase() + annotationName.slice(1);

	if (circle.options.annotation_label != undefined) {
		annotationName = circle.options.annotation_label;
	}

	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var annotation_outerPointCap =
	circle['annotation_outerPointCap'].getLatLng();

	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;
	var annotation_innerPointCapLatLng = circle.getLatLng();
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		annotation_innerPointCapLatLng,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);
	var c = {
		fill: true,
		fillOpacity: 1,
		fillColor: '#fff',
		weight: 1,
		color: '#000',
	};

	var canvas = new fabric.Canvas('annotation_text_canvas');
	//canvas.setBackgroundColor('rgba(227, 227, 227, 0.9)', canvas.renderAll.bind(canvas));
	var rect = new fabric.Rect({
		fill: '#fff',
		strokeWidth: 4,
		stroke: 'black',
		borderColor: 'black',
	});

	var text = new fabric.Text(annotationName, {
		fontSize: 70,
		fontFamily: 'Calibri',
		strokeWidth: 0,
		top: 10,
		left: 30,
	});
	rect.set({
		width: text.width + 50,
		height: text.height + 25,
	});

	var group = new fabric.Group([rect, text]);
	canvas.add(group);
	canvas.setWidth(text.width + 100);
	canvas.setHeight(text.height + 100);
	var dataURL = canvas.toDataURL('png');
	$('.canvas-container').addClass('d-none');

	var annotationText = L.imageOverlay(dataURL, bounds, {
		zIndex: 1000,
	}).addTo(self.map);
	annotationText['label'] = annotationName;
	circle['annotation_text'] = annotationText;
	annotationText.bringToFront();
};

solar_tool.prototype.update_annotation_text = function (circle) {
	var self = this;
	var circleBounds = circle.getBounds();
	var radius = circle.getRadius();

	var annotationName = circle.annotation_text.label;
	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var annotation_outerPointCap =
	circle['annotation_outerPointCap'].getLatLng();

	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;
	var annotation_innerPointCapLatLng = circle.getLatLng();
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		annotation_innerPointCapLatLng,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);
	var c = {
		fill: true,
		fillOpacity: 1,
		fillColor: '#fff',
		weight: 1,
		color: '#000',
	};
	var annotationText = L.rectangle(bounds, c, { zIndex: 1000 }).addTo(
		self.map
		);
	var bounds = annotationText.getBounds();
	annotationText.remove();
	circle.annotation_text.setBounds(bounds);
	circle.annotation_text.bringToFront();
};

solar_tool.prototype.update_annotation_text_label = function (circle) {
	var self = this;
	var circleBounds = circle.getBounds();
	var radius = circle.getRadius();

	var annotationName = circle.annotation_text.label;
	if (annotationName.length == 0) {
		circle.annotation_text.remove();
	}
	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var annotation_outerPointCap =
	circle['annotation_outerPointCap'].getLatLng();

	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;
	var annotation_innerPointCapLatLng = circle.getLatLng();
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		annotation_innerPointCapLatLng,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);
	var c = {
		fill: true,
		fillOpacity: 1,
		fillColor: '#fff',
		weight: 1,
		color: '#000',
	};

	circle['annotation_text'].remove();

	var canvas = new fabric.Canvas('annotation_text_canvas');
	//canvas.setBackgroundColor('rgba(227, 227, 227, 0.9)', canvas.renderAll.bind(canvas));
	var rect = new fabric.Rect({
		fill: '#fff',
		strokeWidth: 4,
		stroke: 'black',
		borderColor: 'black',
	});

	var text = new fabric.Text(annotationName, {
		fontSize: 70,
		fontFamily: 'Calibri',
		strokeWidth: 0,
		top: 10,
		left: 30,
	});
	rect.set({
		width: text.width + 50,
		height: text.height + 25,
	});

	var group = new fabric.Group([rect, text]);
	canvas.add(group);
	canvas.setWidth(text.width + 100);
	canvas.setHeight(text.height + 100);
	var dataURL = canvas.toDataURL('png');
	$('.canvas-container').addClass('d-none');

	var annotationText = L.imageOverlay(dataURL, bounds).addTo(self.map);
	annotationText['label'] = annotationName;
	circle['annotation_text'] = annotationText;
	annotationText.bringToFront();
	annotationText.setZIndex(12);
};

solar_tool.prototype.create_line_annotation = function (latLng, panelKey) {
	var self = this;
	var annotationColor = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-color');
	var annotationWidth = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-width');
	annotationWidth = parseInt(annotationWidth);
	var annotationId = document.getElementById('annotation_list').value;
	var la = new L.Draw.Polyline(self.map, {
		shapeOptions: {
			color: annotationColor,
			strokeColor: annotationColor,
			weight: annotationWidth,
			opacity: 1,
			draggable: true,
			is_annotation: true,
			clickable: true,
			a_id: self.create_uuid(),
			id: annotationId,
			lineCap: 'round',
			lineJoin: 'round',
			smoothFactor:3
		},
	});
	la.enable();
	la.editing.enable();
	self.near_map_imagery_overlay.setZIndex(0);
};

solar_tool.prototype.create_polygon_annotation = function (latLng, panelKey) {
	var self = this;
	var annotationColor = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-color');
	var annotationId = document.getElementById('annotation_list').value;
	var la = new L.Draw.Polygon(self.map, {
		shapeOptions: {
			color: annotationColor,
			strokeColor: annotationColor,
			stroke:true,
			weight: 2,
			opacity: 0.5,
			draggable: true,
			is_annotation: true,
			clickable: true,
			fillOpacity: 0.4,
			a_id: self.create_uuid(),
			id: annotationId,
		},
	});
	la.enable();
	la.editing.disable();
};

solar_tool.prototype.create_measuring_tape_annotation = function (latLng, panelKey) {
	var self = this;
	var annotationColor = document.getElementById('annotation_list').options[document.getElementById('annotation_list').selectedIndex].getAttribute('data-color');
	var annotationWidth = document.getElementById('annotation_list').options[document.getElementById('annotation_list').selectedIndex].getAttribute('data-width');
	annotationWidth = parseInt(annotationWidth);
	var annotationId = document.getElementById('annotation_list').value;
	var la = new L.Draw.Polyline(self.map, {
		shapeOptions: {
			color: '#000',
			strokeColor: '#000',
			weight: 2,
			opacity: 1,
			draggable: false,
			clickable: false,
			is_annotation: true,
			is_annoation_measuring_tape: true,
			id: annotationId,
			m_id: self.create_uuid(),
			lineCap: 'square',
			lineJoin: 'square',
		},
	});
	la.enable();
	la.editing.disable();
	self.near_map_imagery_overlay.setZIndex(0);
};

solar_tool.prototype.create_custom_annotation_line = function (layer) {
	var self = this;
	var annotation_innerPointCapLatLng =
	layer._latlngs.length == 1 ? layer._latlngs[0][3] : layer._latlngs[0];
	var annotation_innerPointCap = L.circle(annotation_innerPointCapLatLng, {
		radius: 0.2,
		color: '#CF1124',
		fill: true,
		fillColor: '#CF1124',
		draggable: false,
		fillOpacity: 1,
	}).addTo(self.map);
	layer['annotation_innerPointCap'] = annotation_innerPointCap;

	var annotation_outerPointCapLatLng = layer['annotation_text']
	.getBounds()
	.getCenter();
	var annotation_outerPointCap = L.circle(annotation_outerPointCapLatLng, {
		radius: 0.4,
		color: '#CF1124',
		fill: true,
		fillColor: '#CF1124',
		draggable: true,
		fillOpacity: 1,
	}).addTo(self.map);
	annotation_outerPointCap.dragging.enable();
	layer['annotation_outerPointCap'] = annotation_outerPointCap;

	annotation_outerPointCap
	.on('drag', function (e) {
		var innerCoordinate = layer.annotation_line._latlngs[0];
		var outerCoordinate = e.target.getLatLng();
		var newCoordinates = [innerCoordinate, outerCoordinate];
		layer.annotation_line.setLatLngs(newCoordinates);

		var annotationName = layer.annotation_text.label;
		annotationName =
		annotationName.charAt(0).toUpperCase() +
		annotationName.slice(1);

		var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
		var annotationNameWidth = annotationName.length / widthFraction;
		var angle = annotationName.length < 17 ? 315 : 300;
		var angleDist = annotationName.length < 17 ? 4 : 6;

		var annotation_outerPointCap = layer._latlngs[0];
		var annotation_innerPointCapLatLng = layer._latlngs[0];
		var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
			outerCoordinate,
			angle,
			angleDist
			);

		var bounds = self.get_annotation_latLng_by_dimension(
			annotation_innerPointCapLatLng1,
			annotationNameWidth,
			3
			);
		layer.annotation_text.setBounds(bounds);
	})
	.on('dragend', function (e) {
		self.handle_custom_annotation_crud(layer);
	});

	var annotationLine = L.polyline(
		[annotation_innerPointCapLatLng, annotation_outerPointCapLatLng],
		{
			color: '#CF1124',
			stroke: true,
			weight: 3,
			lineCap: 'square',
			lineJoin: 'square',
		}
		).addTo(self.map);
	layer['annotation_line'] = annotationLine;

	var annotationIcon = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-icon');
	layer['annotation'] = {};
	layer['annotation']['_url'] = annotationIcon;
	self.annotations.push(layer);
	self.handle_custom_annotation_crud(layer);
	self.create_annotation_list_item(layer);
};

solar_tool.prototype.create_custom_annotation_text = function (layer) {
	var self = this;

	var annotationName = document.getElementById('annotation_list').options[document.getElementById('annotation_list').selectedIndex].getAttribute('data-name');
	annotationName = annotationName.charAt(0).toUpperCase() + annotationName.slice(1);

	if (layer.options.annotation_label != undefined) {
		annotationName = layer.options.annotation_label;
	}

	if(layer.options.is_annoation_measuring_tape != undefined){
		var latLngs = layer.getLatLngs();
		var distance = 0;
		for (var i = 0; i < latLngs.length - 1; i++) {
			var firstPoint = latLngs[i];
			var secondPoint = latLngs[i + 1];
			var temp = firstPoint.distanceTo(secondPoint);
			distance = parseFloat(distance) + parseFloat(temp);
		}

		var features = turf.points([
			[latLngs[0].lat, latLngs[0].lng],
			[latLngs[1].lat, latLngs[1].lng],
			]);
		var center = turf.center(features).geometry.coordinates;
		annotationName = parseFloat(distance).toFixed(2) + 'm';
	}

	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;

	var annotation_innerPointCapLatLng =
	layer._bounds != undefined
	? layer._bounds._northEast
	: layer._latlngs[0];
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		annotation_innerPointCapLatLng,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);

	var c = {
		fill: true,
		fillOpacity: 1,
		fillColor: '#fff',
		weight: 1,
		color: '#000',
	};

	var canvas = new fabric.Canvas('annotation_text_canvas');
	//canvas.setBackgroundColor('rgba(227, 227, 227, 0.9)', canvas.renderAll.bind(canvas));
	var rect = new fabric.Rect({
		fill: '#fff',
		strokeWidth: 4,
		stroke: 'black',
		borderColor: 'black',
	});

	var text = new fabric.Text(annotationName, {
		fontSize: 70,
		fontFamily: 'Calibri',
		strokeWidth: 0,
		top: 10,
		left: 30,
	});
	rect.set({
		width: text.width + 50,
		height: text.height + 25,
	});

	var group = new fabric.Group([rect, text]);
	canvas.add(group);
	canvas.setWidth(text.width + 100);
	canvas.setHeight(text.height + 100);
	var dataURL = canvas.toDataURL('png');
	$('.canvas-container').addClass('d-none');

	var annotationText = L.imageOverlay(dataURL, bounds).addTo(self.map);
	annotationText['label'] = annotationName;
	annotationText.bringToFront();
	annotationText.setZIndex(12);
	layer['annotation_text'] = annotationText;
};

solar_tool.prototype.update_custom_annotation_text_label = function (layer) {
	var self = this;

	var annotationName = layer.annotation_text.label;
	if (annotationName.length == 0) {
		layer.annotation_text.remove();
		layer.annotation_line.setStyle({
			color: 'transparent',
			fill: false,
			fillColor: 'transparent',
		});
		layer.annotation_innerPointCap.setStyle({
			color: 'transparent',
			fill: false,
			fillColor: 'transparent',
		});
		layer.annotation_outerPointCap.setStyle({
			color: 'transparent',
			fill: false,
			fillColor: 'transparent',
		});
	} else {
		layer.annotation_line.setStyle({
			color: '#CF1124',
			fill: true,
			fillColor: '#CF1124',
		});
		layer.annotation_innerPointCap.setStyle({
			color: '#CF1124',
			fill: true,
			fillColor: '#CF1124',
		});
		layer.annotation_outerPointCap.setStyle({
			color: '#CF1124',
			fill: true,
			fillColor: '#CF1124',
		});
	}
	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;

	var annotation_innerPointCapLatLng =
	layer._bounds != undefined
	? layer._bounds._northEast
	: layer._latlngs[0];
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		annotation_innerPointCapLatLng,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);

	var c = {
		fill: true,
		fillOpacity: 1,
		fillColor: '#fff',
		weight: 1,
		color: '#000',
	};

	layer.annotation_text.remove();

	var canvas = new fabric.Canvas('annotation_text_canvas');
	//canvas.setBackgroundColor('rgba(227, 227, 227, 0.9)', canvas.renderAll.bind(canvas));
	var rect = new fabric.Rect({
		fill: '#fff',
		strokeWidth: 4,
		stroke: 'black',
		borderColor: 'black',
	});

	var text = new fabric.Text(annotationName, {
		fontSize: 70,
		fontFamily: 'Calibri',
		strokeWidth: 0,
		top: 10,
		left: 30,
	});
	rect.set({
		width: text.width + 50,
		height: text.height + 25,
	});

	var group = new fabric.Group([rect, text]);
	canvas.add(group);
	canvas.setWidth(text.width + 100);
	canvas.setHeight(text.height + 100);
	var dataURL = canvas.toDataURL('png');
	$('.canvas-container').addClass('d-none');

	var annotationText = L.imageOverlay(dataURL, bounds).addTo(self.map);
	annotationText['label'] = annotationName;
	annotationText.bringToFront();
	annotationText.setZIndex(12);
	layer['annotation_text'] = annotationText;

	var innerCoordinate = layer.annotation_line._latlngs[0];
	var outerCoordinate = layer.annotation_outerPointCap.getLatLng();
	var newCoordinates = [innerCoordinate, outerCoordinate];
	layer.annotation_line.setLatLngs(newCoordinates);

	var annotationName = layer.annotation_text.label;
	annotationName =
	annotationName.charAt(0).toUpperCase() + annotationName.slice(1);

	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var angle = annotationName.length < 17 ? 322 : 305;
	var angleDist = annotationName.length < 17 ? 5 : 7;

	var annotation_outerPointCap = layer._latlngs[0];
	var annotation_innerPointCapLatLng = layer._latlngs[0];
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		outerCoordinate,
		angle,
		angleDist
		);

	var bounds = self.get_annotation_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);
	layer.annotation_text.setBounds(bounds);
};

solar_tool.prototype.create_measuring_tape = function (latLng, panelKey) {
	var self = this;

	var mt = new L.Draw.Polyline(self.map, {
		shapeOptions: {
			color: '#000',
			strokeColor: '#000',
			weight: 2,
			opacity: 1,
			draggable: false,
			clickable: false,
			is_measuring_tape: true,
			m_id: self.create_uuid(),
			lineCap: 'square',
			lineJoin: 'square',
		},
	});
	mt.enable();
	self.near_map_imagery_overlay.setZIndex(0);
};

solar_tool.prototype.handle_panel_crud = function (panel, action) {
	var self = this;

	if (action == 'remove') {
		var mObj = {};
		mObj.object_type = 'PANEL';
		mObj.object_id = panel.options.sp_id;
		self.remove_mapping_object(mObj);
	} else {
		var panel_latlngs = panel._latlngs[0];
		var bounds = [];
		for (var key in panel_latlngs) {
			bounds[key] = {};
			bounds[key]['lat'] = panel_latlngs[key].lat;
			bounds[key]['lng'] = panel_latlngs[key].lng;
		}
		var mObj = {};
		mObj.object_type = 'PANEL';
		mObj.object_id = panel.options.sp_id;
		mObj.object_bounds = bounds;
		mObj.object_properties = panel.options;
		mObj.real_product_id = parseInt(panel.options.sp_panel_id);

		self.save_mapping_object(mObj);
	}
};

solar_tool.prototype.handle_panel_crud_bulk = function (action) {
	var self = this;

	var allDrawnPanels = self.solarPanels;

	var objData = [];
	for (var key in allDrawnPanels) {
		var panel = allDrawnPanels[key];
		if (panel['options']['sp_id'] != null) {
			var panel_latlngs = panel._latlngs[0];
			var bounds = [];
			for (var key in panel_latlngs) {
				bounds[key] = {};
				bounds[key]['lat'] = panel_latlngs[key].lat;
				bounds[key]['lng'] = panel_latlngs[key].lng;
			}
			var mObj = {};
			mObj.object_type = 'PANEL';
			mObj.object_bounds = bounds;
			mObj.object_id = panel['options']['sp_id'];
			mObj.object_properties = panel['options'];
			mObj.real_product_id = parseInt(panel['options']['sp_panel_id']);
			objData.push(mObj);
		}
	}
	objData = JSON.stringify(objData);

	var data = 'object_data=' + objData;
	data += '&object_action=' + action;
	self.save_mapping_object_bulk(data);
};

solar_tool.prototype.handle_panel_remove_bulk = function (action,panels) {
	var self = this;

	var objData = [];
	for (var key in panels) {
		var panel = panels[key];
		if (panel['options']['sp_id'] != null) {
			var panel_latlngs = panel._latlngs[0];
			var bounds = [];
			for (var key in panel_latlngs) {
				bounds[key] = {};
				bounds[key]['lat'] = panel_latlngs[key].lat;
				bounds[key]['lng'] = panel_latlngs[key].lng;
			}
			var mObj = {};
			mObj.object_type = 'PANEL';
			mObj.object_bounds = bounds;
			mObj.object_id = panel['options']['sp_id'];
			mObj.object_properties = panel['options'];
			mObj.real_product_id = parseInt(panel['options']['sp_panel_id']);
			objData.push(mObj);
		}
	}
	objData = JSON.stringify(objData);

	var data = 'object_data=' + objData;
	data += '&object_action=' + action;
	self.remove_mapping_object_bulk(data);
};

solar_tool.prototype.handle_annotation_crud = function (annotation, action) {
	var self = this;

	if (action == 'remove') {
		var mObj = {};
		mObj.object_type = 'ANNOTATION';
		mObj.object_id = annotation.options.a_id;
		self.remove_mapping_object(mObj);
	} else {
		var annotationBounds = annotation._latlng;
		var bounds = { lat: annotationBounds.lat, lng: annotationBounds.lng };

		var annotationName = annotation.annotation_text.label;
		annotation.setStyle({ annotation_label: annotationName });

		var annotation_innerPointCap = annotation.annotation_innerPointCap;
		var annotation_innerPointCapBounds = {
			lat: annotation_innerPointCap._latlng.lat,
			lng: annotation_innerPointCap._latlng.lng,
		};
		annotation.setStyle({
			annotation_innerPointCap: annotation_innerPointCapBounds,
		});

		var annotation_outerPointCap = annotation.annotation_outerPointCap;
		var annotation_outerPointCapBounds = {
			lat: annotation_outerPointCap._latlng.lat,
			lng: annotation_outerPointCap._latlng.lng,
		};
		annotation.setStyle({
			annotation_outerPointCap: annotation_outerPointCapBounds,
		});

		var mObj = {};
		mObj.object_type = 'ANNOTATION';
		mObj.object_id = annotation.options.a_id;
		mObj.object_bounds = bounds;
		mObj.object_properties = annotation.options;
		mObj.real_product_id = parseInt(annotation.options.id);

		self.save_mapping_object(mObj);

		annotation.on('click', function (e) {
			$('#tool_annotations').click();
		})
	}
};

solar_tool.prototype.handle_custom_annotation_crud = function (
	annotation,
	action
	) {
	var self = this;

	if (action == 'remove') {
		var mObj = {};
		mObj.object_type = 'CUSTOM_ANNOTATION';
		mObj.object_id = annotation.options.a_id;
		self.remove_mapping_object(mObj);
	} else {
		var annotationBounds =
		annotation._latlngs.length == 1
		? annotation._latlngs[0]
		: annotation._latlngs;
		var bounds = [];
		for (var key in annotationBounds) {
			bounds[key] = {};
			bounds[key]['lat'] = annotationBounds[key].lat;
			bounds[key]['lng'] = annotationBounds[key].lng;
		}

		var annotationName = annotation.annotation_text.label;
		annotation.setStyle({ annotation_label: annotationName });

		var annotation_innerPointCap = annotation.annotation_innerPointCap;
		var annotation_innerPointCapBounds = {
			lat: annotation_innerPointCap._latlng.lat,
			lng: annotation_innerPointCap._latlng.lng,
		};
		annotation.setStyle({
			annotation_innerPointCap: annotation_innerPointCapBounds,
		});

		var annotation_outerPointCap = annotation.annotation_outerPointCap;
		var annotation_outerPointCapBounds = {
			lat: annotation_outerPointCap._latlng.lat,
			lng: annotation_outerPointCap._latlng.lng,
		};
		annotation.setStyle({
			annotation_outerPointCap: annotation_outerPointCapBounds,
		});

		var mObj = {};
		mObj.object_type = 'CUSTOM_ANNOTATION';
		mObj.object_id = annotation.options.a_id;
		mObj.object_bounds = bounds;
		mObj.object_properties = annotation.options;
		mObj.real_product_id = parseInt(annotation.options.id);

		self.save_mapping_object(mObj);
	}
};

solar_tool.prototype.create_panel_from_object_data = function (object_data) {
	var self = this;

	var object_properties = object_data.object_properties;

	var dimension = object_properties.sp_panel_value.split(',');
	self.panelConfig.panelLength = dimension[1];
	self.panelConfig.panelWidth = dimension[0];
	self.panelConfig.panelId = parseInt(object_properties.sp_panel_id);
	self.panelConfig.panelWatt = parseFloat(object_properties.sp_rotation);
	self.panelConfig.defaultRotation = parseInt(object_properties.sp_rotation);
	self.panelConfig.defaultOrientation = object_properties.sp_orientation;
	document.getElementById('mtool_tilt').value = object_properties.sp_tilt;
	document.getElementById('mtool_rotation').value =
	self.panelConfig.defaultRotation;
	document.getElementById('mtool_individual_rotation').value =
	object_properties.sp_rotation;
	document.getElementById('mtool_panel_list').value =
	object_properties.sp_panel_id;

	var width = self.panelConfig.panelWidth;
	var height = self.panelConfig.panelLength;
	var latLng = object_data.object_bounds[1];
	var bounds = self.get_panel_latLng_by_dimension(latLng, width, height);

	var rectangle = L.rectangle(bounds, object_properties).addTo(self.map);
	var coords = [
	{
		lat: rectangle.getBounds().getNorthEast().lat,
		lng: rectangle.getBounds().getNorthEast().lng,
	},
	{
		lat: rectangle.getBounds().getNorthEast().lat,
		lng: rectangle.getBounds().getSouthWest().lng,
	},
	{
		lat: rectangle.getBounds().getSouthWest().lat,
		lng: rectangle.getBounds().getSouthWest().lng,
	},
	{
		lat: rectangle.getBounds().getSouthWest().lat,
		lng: rectangle.getBounds().getNorthEast().lng,
	},
	];
	rectangle.remove();

	var panel = L.polygon(coords, object_properties).addTo(self.map);
	panel.bringToBack();
	//Adding Arrows
	var panelArrow = self.create_panel_arrow(panel);
	panel['arrow'] = panelArrow;
	panel.dragging.enable();
	panel
	.on('dragstart', function (e) {
		e.target.arrow.remove();
		self.panel_tool_enable = false;
		self.panelDragStartReference = e.target.getBounds().getCenter();
	})
	.on('drag', function (e) {
		self.handle_panel_drag_actions(e.target);
	})
	.on('dragend', function (e) {
		self.panel_select_multiple_move(e.target);
		setTimeout(function () {
			self.panel_tool_enable = true;
		}, 200);
		var panelArrow = self.create_panel_arrow(e.target);
		e.target['arrow'] = panelArrow;
		self.handle_panel_crud(e.target);
	});
	self.handle_panel_click(panel);
	self.solarPanels.push(panel);

	self.rotate_solar_panel(
		panel,
		object_properties.sp_rotation,
		object_properties.sp_rotation,
		1
		);
};

solar_tool.prototype.reset_select_tool_envelope = function (panelDragged){
	var self = this;
	self.select_all_panel();
	self.panel_tool_enable = false;
	self.panel_eraser_enable = false;
	self.enable_select_tool = false;
	if (self.select_active_tool != null) {
		self.select_active_tool.remove();
		self.select_active_tool = null;
	}
	if (self.select_drawn_tool != null) {
		self.select_drawn_tool.remove();
		self.select_drawn_tool = null;
	}
	if(self.select_tool_envelope != null){
		self.select_tool_envelope.remove();
	}
	$('#tool_select_reset_btn').addClass('d-none');
	$("#tool_manage_panel").click();
	self.calculate_system_summary();
};

solar_tool.prototype.panel_select_multiple_move = function (panelDragged){
	var self = this;
	if (self.selectedPanels > 1 && panelDragged.options.sp_selected) {

		self.mtool_data_save = false;

		var te = panelDragged.getBounds().getCenter();
		for (var key in self.solarPanels) {
			if (panelDragged.options.sp_id !== self.solarPanels[key].options.sp_id) {
				if (self.solarPanels[key].options.sp_selected) {
					var distance = self.map.distance(self.panelDragStartReference, te);
					var heading = L.GeometryUtil.angle(self.map,self.panelDragStartReference, te);
					var x = L.GeometryUtil.destination(self.solarPanels[key].getBounds().getCenter(),heading,distance);
					self.move_panel_to_latLng(self.solarPanels[key],x);
				}
			}
		}

		setTimeout(function(){
			self.mtool_data_save = true;
			self.handle_panel_crud_bulk('ROTATION');
		},1500);
	}

	if(self.selectedPanels > 1 && self.select_tool_envelope != null){
		var points = [];
		var allDrawnPanels = self.solarPanels;
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				var c = allDrawnPanels[key].getLatLngs()[0];
				for(var k in c){
					var c1 = [parseFloat(c[k].lat),parseFloat(c[k].lng)];
					points.push(c1);
				}
			}
		}
		var features = turf.points(points);
		var envelope = turf.envelope(features).geometry.coordinates;
		self.select_tool_envelope.setLatLngs(envelope);
	}
};

solar_tool.prototype.move_panel_to_latLng = function (panel,latLng){
	var self = this;

	var boundsCenter = panel.getBounds().getCenter(), // center of the polygonbounds
                paths = panel.getLatLngs(), // paths that make up the polygon
                newPoints = [], // array on which we'll store our new points
                newPaths = [];

    // geodesic not enabled: adjust the coordinates pixelwise
    var latlngToPoint = function (map,latlng) {
    	return  map.project(latlng,map.getZoom());
    };

    var pointToLatlng = function (map, point) {
    	return  map.unproject(point,map.getZoom());
    };

    var boundsCenterPx = latlngToPoint(self.map,boundsCenter),
    latLngPx = latlngToPoint(self.map,latLng);

    var dLatPx = (boundsCenterPx.y - latLngPx.y) * (-1),
    dLngPx = (boundsCenterPx.x - latLngPx.x) * (-1);

    var path = null;
    for (var p = 0; p < paths.length; p++) {
    	path = paths[p];
    	newPaths.push([]);
    	for (var i = 0; i < path.length; i++) {
    		var pixels = latlngToPoint(self.map,path[i]);
    		pixels.x += dLngPx;
    		pixels.y += dLatPx;
    		newPaths[newPaths.length - 1].push(pointToLatlng(self.map,pixels));
    	}
    }

    panel.arrow.remove();
	//Update PanelCords
	panel.setLatLngs(newPaths);
	//Update ArrowCords
	var panelArrow = self.create_panel_arrow(panel);
	panel['arrow'] = panelArrow;

};

solar_tool.prototype.create_annotation_from_object_data = function (
	annotation
	) {
	var self = this;

	var object_properties = annotation.object_properties;
	var latLng = annotation.object_bounds;
	var annotationId = (document.getElementById('annotation_list').value =
		object_properties.id);
	var c = new L.circle(latLng, object_properties);
	c.addTo(self.map);
	c.dragging.enable();

	self.create_annotation_line(c);

	self.create_annotation_icon(c);

	self.create_annotation_text(c);

	c.on('drag', function (e) {
		var newLatLng = e.target.getLatLng();
		var nc = L.circle(newLatLng, {
			radius: 2,
			color: 'transparent',
			fill: true,
			fillColor: 'transparent',
		}).addTo(self.map);
		var newBounds = nc.getBounds();
		nc.remove();
		e.target.annotation.setBounds(newBounds);

		//Update Line Bounds
		var radius = e.target.getRadius();
		var innerCoordinate = e.target.getLatLng();
		var outerCoordinate = e.target.annotation_line.getLatLngs()[0];
		var angleToClip = L.GeometryUtil.angle(
			self.map,
			innerCoordinate,
			outerCoordinate
			);
		innerCoordinate = L.GeometryUtil.destination(
			innerCoordinate,
			angleToClip,
			radius
			);
		var newCoordinates = [outerCoordinate, innerCoordinate];
		e.target.annotation_line.setLatLngs(newCoordinates);
		e.target.annotation_innerPointCap.setLatLng(innerCoordinate);

		var interSect = L.GeometryUtil.belongsSegment(
			outerCoordinate,
			newBounds.getNorthEast(),
			newBounds.getSouthWest(),
			0.2
			);
		if (interSect) {
			e.target.annotation_outerPointCap.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
			e.target.annotation_innerPointCap.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
			e.target.annotation_line.setStyle({
				color: 'transparent',
				fillColor: 'transparent',
			});
		} else {
			e.target.annotation_outerPointCap.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
			e.target.annotation_innerPointCap.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
			e.target.annotation_line.setStyle({
				color: '#CF1124',
				fillColor: '#CF1124',
			});
		}

		self.update_annotation_text(e.target);
	}).on('dragend', function (e) {
		self.handle_annotation_crud(e.target);
	});

	//c.editing.enable();
	c.on('mouseover', function (event) {
		c.setStyle({ color: '#00FFFF' });
	});

	c.on('mouseout', function (event) {
		c.setStyle({ color: 'transparent' });
	});

	self.annotations.push(c);
	self.handle_annotation_crud(c);

	//Update Inner Circle, Outer Circle, Line Bounds
	var circleBounds = c.getBounds();
	var radius = c.getRadius();
	var innerCoordinate = c.getLatLng();
	var outerCoordinate = object_properties.annotation_outerPointCap;
	outerCoordinate = L.latLng(outerCoordinate.lat, outerCoordinate.lng);

	var angleToClip = L.GeometryUtil.angle(
		self.map,
		innerCoordinate,
		outerCoordinate
		);
	innerCoordinate = L.GeometryUtil.destination(
		innerCoordinate,
		angleToClip,
		radius
		);
	var newCoordinates = [outerCoordinate, innerCoordinate];
	c.annotation_line.setLatLngs(newCoordinates);
	c.annotation_innerPointCap.setLatLng(innerCoordinate);
	c.annotation_outerPointCap.setLatLng(outerCoordinate);
	c.annotation_outerPointCap.setStyle({
		color: '#CF1124',
		fillColor: '#CF1124',
	});
	c.annotation_innerPointCap.setStyle({
		color: '#CF1124',
		fillColor: '#CF1124',
	});
	c.annotation_line.setStyle({ color: '#CF1124', fillColor: '#CF1124' });
	c.setStyle({ color: 'transparent' });
	self.update_annotation_text(c);

	self.create_annotation_list_item(c);
};

solar_tool.prototype.create_custom_annotation_from_object_data = function (
	annotation
	) {
	var self = this;

	var object_properties = annotation.object_properties;

	var annotationId = (document.getElementById('annotation_list').value =
		object_properties.id);
	var annotationColor = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-color');
	var annotationWidth = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-width');
	annotationWidth = parseInt(annotationWidth);
	var annotationtype = document
	.getElementById('annotation_list')
	.options[
	document.getElementById('annotation_list').selectedIndex
	].getAttribute('data-type');

	if (annotationtype == 'line') {
		delete object_properties['strokeColor'];
		delete object_properties['color'];
		delete object_properties['weight'];
		object_properties.color = annotationColor;
		object_properties.stroke = true;
		object_properties.strokeColor = annotationColor;
		object_properties.weight = annotationWidth;
		object_properties.opacity = 1;
		object_properties.draggable = true;
		object_properties.is_annotation = true;
		object_properties.clickable = true;
		var annotationBounds = annotation.object_bounds;
		if (annotationBounds == null) {
			return true;
		}
		var layer = new L.polyline(annotationBounds, object_properties).addTo(
			self.map
			);
	} else if (annotationtype == 'polygon') {
		delete object_properties['strokeColor'];
		delete object_properties['color'];
		delete object_properties['weight'];
		object_properties.color = annotationColor;
		object_properties.stroke = true;
		object_properties.strokeColor = annotationColor;
		object_properties.draggable = true;
		object_properties.clickable = true;
		var annotationBounds = annotation.object_bounds;
		if (annotationBounds == null) {
			return true;
		}
		var layer = new L.polygon(annotationBounds, object_properties).addTo(
			self.map
			);
	}

	self.create_custom_annotation_text(layer);
	self.create_custom_annotation_line(layer);

	if (layer.options.annotation_label != undefined) {
		annotationName = layer.options.annotation_label;
		if (annotationName.length == 0) {
			layer.annotation_text.remove();
			layer.annotation_line.setStyle({
				color: 'transparent',
				fill: false,
				fillColor: 'transparent',
			});
			layer.annotation_innerPointCap.setStyle({
				color: 'transparent',
				fill: false,
				fillColor: 'transparent',
			});
			layer.annotation_outerPointCap.setStyle({
				color: 'transparent',
				fill: false,
				fillColor: 'transparent',
			});
		} else {
			layer.annotation_line.setStyle({
				color: '#CF1124',
				fill: true,
				fillColor: '#CF1124',
			});
			layer.annotation_innerPointCap.setStyle({
				color: '#CF1124',
				fill: true,
				fillColor: '#CF1124',
			});
			layer.annotation_outerPointCap.setStyle({
				color: '#CF1124',
				fill: true,
				fillColor: '#CF1124',
			});
		}
	}

	var innerCoordinate = layer.annotation_line._latlngs[0];
	var outerCoordinate = object_properties.annotation_outerPointCap;
	outerCoordinate = L.latLng(outerCoordinate.lat, outerCoordinate.lng);

	var newCoordinates = [innerCoordinate, outerCoordinate];
	layer.annotation_line.setLatLngs(newCoordinates);

	var annotationName = layer.annotation_text.label;
	annotationName =
	annotationName.charAt(0).toUpperCase() + annotationName.slice(1);

	var widthFraction = annotationName.length < 15 ? 1.2 : 1.7;
	var annotationNameWidth = annotationName.length / widthFraction;
	var angle = annotationName.length < 17 ? 315 : 300;
	var angleDist = annotationName.length < 17 ? 4 : 6;

	var annotation_outerPointCap = layer._latlngs[0];
	var annotation_innerPointCapLatLng = layer._latlngs[0];
	var annotation_innerPointCapLatLng1 = L.GeometryUtil.destination(
		outerCoordinate,
		angle,
		angleDist
		);

	var bounds = self.get_panel_latLng_by_dimension(
		annotation_innerPointCapLatLng1,
		annotationNameWidth,
		3
		);
	layer.annotation_text.setBounds(bounds);
	layer.annotation_outerPointCap.setLatLng(outerCoordinate);
};

solar_tool.prototype.calculate_system_summary = function (panel) {
	var self = this;

	//Update Panel Count and Wattage
	var total = 0;
	var selected = 0;
	var wattageCapacity = 0;
	for (var key in self.solarPanels) {
		total++;
		wattageCapacity =
		parseFloat(self.solarPanels[key]['options'].sp_watt) +
		parseFloat(wattageCapacity);
		if (self.solarPanels[key]['options'].sp_selected == true) {
			selected++;
		}
	}

	console.log(selected)

	var wattageCapacityNum = wattageCapacity / 1000;
	wattageCapacity = wattageCapacityNum + 'kW';
	self.totalPanels = total;
	self.selectedPanels = selected;
	if (total >= 0) {
		document.getElementById('mtool_selected').innerHTML =
		selected + ' out of ' + total + ' are selected';
		document.getElementById('mtool_watt_capacity').innerHTML =
		wattageCapacity;
		document.getElementById('pv_system_size').value = wattageCapacityNum;
		document.getElementById('system_pricing_panel_val').value =
		parseInt(total);
		document.getElementById('total_system_size').value = wattageCapacityNum;
		document.querySelector('.no_of_panels').value = parseInt(total);
	}
};

/** Handle Toolbar and Sidebar */

solar_tool.prototype.handle_left_toolbar = function () {
	var self = this;

	$(document).on('click', '.toolbar-btn', function () {
		self.left_toolbar_reset();
	});

	//Handle Select
	$(document).on('click', '#tool_select', function () {
		$(this).addClass('toolbar-btn--selected');
		self.enable_select_tool = true;
		if (self.select_drawn_tool != null) {
			self.select_drawn_tool.remove();
		}
		self.select_drawn_tool = new L.Draw.Rectangle(self.map, {
				shapeOptions: {
					is_select_tool : true
				}
			});
		self.select_drawn_tool.enable();

	});

	//Handle Hand
	$(document).on('click', '#tool_hand', function () {
		$(this).addClass('toolbar-btn--selected');
		self.map.dragging.enable();
	});

	//Handle Panel Add
	$(document).on('click', '#tool_manage_panel', function () {
		$(this).addClass('toolbar-btn--selected');
		$('#sidebar_right_panel').removeClass('d-none');
		self.panel_tool_enable = true;
	});

	//Handle Panel Add
	$(document).on('click', '#tool_eraser', function () {
		$(this).addClass('toolbar-btn--selected');
		self.panel_eraser_enable = true;
	});

	//Handle Annontation Section
	$(document).on('click', '#tool_annotations', function () {
		$(this).addClass('toolbar-btn--selected');
		$('#sidebar_right_annotation').removeClass('d-none');
	});

	//Handle Measuring Tape
	$(document).on('click', '#tool_measuring_tape', function () {
		$(this).addClass('toolbar-btn--selected');
		self.create_measuring_tape();
	});
};

solar_tool.prototype.performRotate = function (rotationValue) {
	var self = this;

	self.mtool_data_save = false;

	var centroid = null;
	if(self.selectedPanels > 1){
		var points = [];
		var allDrawnPanels = self.solarPanels;
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				var c = allDrawnPanels[key].getLatLngs()[0][1];
				var point = [parseFloat(c.lat),parseFloat(c.lng)];
				points.push(point);
			}
		}
		var features = turf.points(points);
		centroid = turf.center(features).geometry.coordinates;
	}

	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options'].sp_selected) {
			var netRotation =
			rotationValue - self.solarPanels[key]['options'].sp_rotation;
			self.rotate_solar_panel(
				self.solarPanels[key],
				netRotation,
				rotationValue,
				null,
				centroid
				);
		}
	}

	if(self.selectedPanels > 1 && self.select_tool_envelope != null){
		var points = [];
		var allDrawnPanels = self.solarPanels;
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				var c = allDrawnPanels[key].getLatLngs()[0];
				for(var k in c){
					var c1 = [parseFloat(c[k].lat),parseFloat(c[k].lng)];
					points.push(c1);
				}
			}
		}
		var features = turf.points(points);
		var envelope = turf.envelope(features).geometry.coordinates;
		self.select_tool_envelope.setLatLngs(envelope);
	}

	setTimeout(function(){
		self.mtool_data_save = true;
		self.handle_panel_crud_bulk('ROTATION');
	},1500);
};

solar_tool.prototype.performOrientation = function (orientationValue) {
	var self = this;

	self.mtool_data_save = false;

	var centroid = null;
	if(self.selectedPanels > 1){
		var points = [];
		var allDrawnPanels = self.solarPanels;
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				var c = allDrawnPanels[key].getLatLngs()[0][1];
				var point = [parseFloat(c.lat),parseFloat(c.lng)];
				points.push(point);
			}
		}
		var features = turf.points(points);
		centroid = turf.center(features).geometry.coordinates;
	}

	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options'].sp_selected) {
			if (orientationValue == 'portrait') {
				// rotate 90 right
				var angle = 90;
				var temp =
				parseInt(self.solarPanels[key]['options'].sp_rotation) +
				parseInt(angle);
			} else {
				// rotate 90 left
				var angle = -90;
				var temp =
				parseInt(self.solarPanels[key]['options'].sp_rotation) +
				parseInt(angle);
			}
			document.getElementById('mtool_rotation').value = temp;
			self.panelConfig.defaultRotation = temp;
			self.rotate_solar_panel(self.solarPanels[key], angle, temp, null,centroid);
		}
	}

	if(self.selectedPanels > 1 && self.select_tool_envelope != null){
		var points = [];
		var allDrawnPanels = self.solarPanels;
		for (var key in allDrawnPanels) {
			if (allDrawnPanels[key]['options']['sp_selected'] == true) {
				var c = allDrawnPanels[key].getLatLngs()[0];
				for(var k in c){
					var c1 = [parseFloat(c[k].lat),parseFloat(c[k].lng)];
					points.push(c1);
				}
			}
		}
		var features = turf.points(points);
		var envelope = turf.envelope(features).geometry.coordinates;
		self.select_tool_envelope.setLatLngs(envelope);
	}

	setTimeout(function(){
		self.mtool_data_save = true;
		self.handle_panel_crud_bulk('ROTATION');
	},1500);
};

solar_tool.prototype.performTilt = function (tiltValue) {
	var self = this;

	self.mtool_data_save = false;

	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options'].sp_selected) {
			var totalDrawnPanels = self.solarPanels.length;
			var latLngs = self.solarPanels[key].getLatLngs()[0];
			if (self.panelConfig.defaultOrientation == 'portrait') {
				var northEast = latLngs[1];
			} else {
				var northEast = latLngs[1];
			}
			self.create_panel(northEast, key);
		}
	}

	setTimeout(function(){
		self.mtool_data_save = true;
		self.handle_panel_crud_bulk('TILT');
	},1500);

};

solar_tool.prototype.performMounting = function (mountinValue) {
	var self = this;
	for (var key in self.solarPanels) {
		if (self.solarPanels[key]['options'].sp_selected) {
			if (
				self.solarPanels[key]['options'].sp_orientation == mountinValue
				) {
				continue;
		}
		self.panelConfig.defaultOrientation = mountinValue;
		self.solarPanels[key].setStyle({ sp_orientation: mountinValue });
		self.performTilt(0);
	}
}
};

solar_tool.prototype.left_toolbar_reset = function () {
	var self = this;
	$('.toolbar-btn').removeClass('toolbar-btn--selected');
	$('#sidebar_right_panel').addClass('d-none');
	$('#sidebar_right_annotation').addClass('d-none');
	$('#sidebar_right_panel').addClass('d-none');
	self.map.dragging.enable();
	self.panel_tool_enable = false;
	self.panel_eraser_enable = false;
	self.enable_select_tool = false;
	if (self.select_active_tool != null) {
		self.select_active_tool.disable();
		self.select_active_tool = null;
	}
	if (self.select_drawn_tool != null) {
		self.select_drawn_tool.editing.disable();
	}
};

solar_tool.prototype.handle_panel_right_sidebar = function () {
	var self = this;

	self.map.on('click', function (e) {


		if(self.enable_select_tool && !self.panel_tool_enable) {
			if(self.select_tool_envelope != null && self.select_drawn_tool != null){
				var selectToolBounds = self.select_drawn_tool.getBounds();
				var latlngA = selectToolBounds._northEast;
				var latlngB = selectToolBounds._southWest;
				var belongs = L.GeometryUtil.belongsSegment(e.latlng, latlngA, latlngB, 0.5);
				if(!belongs){
					self.reset_select_tool_envelope();
				}
			}
		}


		if(!self.panel_tool_enable){
			if(self.enable_select_tool == true && self.select_drawn_tool != null){
				var selectToolBounds = self.select_drawn_tool.getBounds();
				var latlngA = selectToolBounds._northEast;
				var latlngB = selectToolBounds._southWest;
				var belongs = L.GeometryUtil.belongsSegment(e.latlng, latlngA, latlngB,0.5);
				if(!belongs){
					self.select_drawn_tool.remove();
					self.select_drawn_tool = null;

				}
			}
			return false;
		}else if(self.panel_tool_enable){

		}


		if (typeof e.originalEvent.target.className != 'string') {
			return false;
		}
		if (
			typeof e.originalEvent.target.className === 'string' &&
			!e.originalEvent.target.className.includes(
				'leaflet-container leaflet-touch'
				)
			) {
			return false;
	}

	self.calculate_system_summary();
});


	self.map.on('dblclick', function (e) {

		if (self.panel_tool_enable) {
			var belongs = false;
			for (var key in self.solarPanels) {
				if (belongs) {
					continue;
				}
				var panelBounds = self.solarPanels[key].getBounds();
				var latlngA = panelBounds._northEast;
				var latlngB = panelBounds._southWest;
				belongs = L.GeometryUtil.belongsSegment(
					e.latlng,
					latlngA,
					latlngB,
					0.2
				);
			}
			if (!belongs) {
				var panelObj = self.create_panel(e.latlng);
				var panelId = panelObj.options.sp_panel_id;
				$('select#mtool_panel_list option[value!=' + panelId + ']').hide();
			}
		}
		self.calculate_system_summary();
		$('#tool_manage_panel').trigger('click');
	});
	self.map.on('keydown',function(e){
		if(e.originalEvent.key == '+'){
			self.map.zoomIn();
		}else if(e.originalEvent.key == '-'){
			self.map.zoomIn();
		}else if(e.originalEvent.key == 'Delete'){
			self.remove_solar_panel();
		}
	});

	self.map['self'] = self;
	self.map.on('mousemove', function (e) {
		var _self = e.target.self;
		return false;
		var latLng = _self.near_map_imagery_overlay.getBounds().getCenter();
		latLng = _self.get_panel_latLng_by_dimension(
			latLng,
			_self.gap_between_panel,
			_self.gap_between_panel
			)._northEast;
		//self.create_panel_drop_target(latLng);

		if (!_self.panel_tool_enable) {
			return false;
		}

		var dimension = document
		.getElementById('mtool_panel_list')
		.options[
		document.getElementById('mtool_panel_list').selectedIndex
		].getAttribute('data-dimension')
		.split(',');
		_self.panelConfig.panelLength = dimension[0];
		_self.panelConfig.panelWidth = dimension[1];
		_self.panelConfig.panelId =
		document.getElementById('mtool_panel_list').value;
		_self.panelConfig.panelWatt = document
		.getElementById('mtool_panel_list')
		.options[
		document.getElementById('mtool_panel_list').selectedIndex
		].getAttribute('data-capacity');

		var tilt = document.getElementById('mtool_tilt').value || 0;
		var width = _self.panelConfig.panelWidth;
		var height = _self.panelConfig.panelLength;

		var c = {
			color: _self.panelConfig.strokeColor,
			weight: _self.panelConfig.strokeWeight,
			fill: true,
			fillOpacity: _self.panelConfig.fillOpacity,
			fillColor: 'white',
		};

		for (var key in self.solarPanels) {
			var currentPanel = self.solarPanels[key];
			var latlng = null;
			var latlngA = currentPanel.getLatLngs()[0][0];
			var latlngB = currentPanel.getLatLngs()[0][1];
			var latlngC = currentPanel.getLatLngs()[0][2];
			var latlngD = currentPanel.getLatLngs()[0][3];

			//L.polyline([latlngB,latlngB],{color:'red'}).addTo(_self.map)
			var belongsA = L.GeometryUtil.belongsSegment(
				e.latlng,
				latlngA,
				latlngD,
				0.6
				);
			var belongsB = L.GeometryUtil.belongsSegment(
				e.latlng,
				latlngB,
				latlngC,
				0.6
				);
			var belongsC = L.GeometryUtil.belongsSegment(
				e.latlng,
				latlngC,
				latlngD,
				0.6
				);
			var belongsD = L.GeometryUtil.belongsSegment(
				e.latlng,
				latlngA,
				latlngB,
				0.6
				);
			if (belongsA && currentPanel['dropTargetA'] == null) {
				var bounds = _self.get_panel_latLng_by_dimension(
					latlngA,
					width,
					height
					);
				var rectangle = L.rectangle(bounds, c).addTo(self.map);
				currentPanel['dropTargetA'] = rectangle;
			} else if (!belongsA && currentPanel['dropTargetA'] != null) {
				currentPanel['dropTargetA'].remove();
				currentPanel['dropTargetA'] = null;
			}
			if (belongsB && currentPanel['dropTargetB'] == null) {
				var bounds = _self.get_panel_latLng_by_dimension(
					latlngB,
					width,
					height
					);
				var rectangle = L.rectangle(bounds, c).addTo(self.map);
				currentPanel['dropTargetB'] = rectangle;
			} else if (!belongsC && currentPanel['dropTargetB'] != null) {
				currentPanel['dropTargetB'].remove();
				currentPanel['dropTargetB'] = null;
			}
			if (belongsC && currentPanel['dropTargetC'] == null) {
				var bounds = _self.get_panel_latLng_by_dimension(
					latlngC,
					width,
					height
					);
				var rectangle = L.rectangle(bounds, c).addTo(self.map);
				currentPanel['dropTargetC'] = rectangle;
			} else if (!belongsC && currentPanel['dropTargetC'] != null) {
				currentPanel['dropTargetC'].remove();
				currentPanel['dropTargetC'] = null;
			}
			if (belongsD && currentPanel['dropTargetD'] == null) {
				var bounds = _self.get_panel_latLng_by_dimension(
					latlngD,
					width,
					height
					);
				var rectangle = L.rectangle(bounds, c).addTo(self.map);
				currentPanel['dropTargetD'] = rectangle;
			} else if (!belongsC && currentPanel['dropTargetD'] != null) {
				currentPanel['dropTargetD'].remove();
				currentPanel['dropTargetD'] = null;
			}
		}
	});

	//Handle Add Panel
	$(document).on('click', '#mtool_add_panel', function (e) {

		if (!self.panel_tool_enable) {
			return false;
		}
		var no_of_panels_to_create = $('#mtool_no_of_panels').val();

		var totalDrawnPanels = self.solarPanels.length;
		if (totalDrawnPanels == 0) {
			for (var i = 0; i < no_of_panels_to_create; i++) {
				if (i != 0) {
					var totalDrawnPanels = self.solarPanels.length;
					var lastPanel = self.solarPanels[totalDrawnPanels - 1];
					var latLng = lastPanel.getLatLngs()[0];
					latLng = latLng[0];
					var sp_rotation = lastPanel.options.sp_rotation;
				} else {
					var latLng = self.near_map_imagery_overlay
					.getBounds()
					.getCenter();
					var sp_rotation = parseInt(document.getElementById('mtool_individual_rotation').value);
				}
				latLng = self.get_panel_latLng_by_dimension(
					latLng,
					self.gap_between_panel,
					self.gap_between_panel
					)._northEast;
				panel = self.create_panel(latLng,'',sp_rotation);
			}
		} else {
			var selectedPanelObj = self.findFirstLastSelectAndUnSelectPanel();
			if(selectedPanelObj[0] == null){
				selectedPanelObj[0] = selectedPanelObj[1];
			}
			var panel = selectedPanelObj[1];
			if($('#mtool_select_toggle').attr('data-selected') == 'all'){
				$('#mtool_select_toggle').trigger('click');
			}
			for (var i = 0; i < no_of_panels_to_create; i++) {

				if(i==0 && no_of_panels_to_create > 1 ) {
					var panel = selectedPanelObj[0];
					var latLngs = panel.getLatLngs()[0];
					var northEast = latLngs[2];
				} else {
					var latLngs = panel.getLatLngs()[0];
					var northEast = latLngs[0];
				}
				var panelObj = self.get_panel_latLng_by_dimension(
					northEast,
					self.gap_between_panel,
					self.gap_between_panel
				)._northEast;

				panel = self.create_panel(panelObj,'',panel.options.sp_rotation);
			}
		}
		if(panel!=null) {
			var panelId = panel.options.sp_panel_id;
			$('select#mtool_panel_list option[value!=' + panelId + ']').hide();
		}
	});

	//Handle Select/Deselect Toogle
	document
	.getElementById('mtool_select_toggle')
	.addEventListener('click', function (e) {
		var action = this.getAttribute('data-selected');
		if (action == null || action == 'none') {
			this.innerHTML = 'DESELECT ALL';
			this.setAttribute('data-selected', 'all');
			self.select_all_panel('all');
		} else if (this.getAttribute('data-selected') == 'all') {
			this.innerHTML = 'SELECT ALL';
			this.setAttribute('data-selected', 'none');
			self.select_all_panel('none');
		}
		self.calculate_system_summary();
	});

	//Handle Delete Panel
	document
	.getElementById('mtool_delete_panel')
	.addEventListener('click', function (e) {
		self.loader = true;
		self.remove_solar_panel();
	});

	//Handle Rotation
	document
	.getElementById('mtool_rotation')
	.addEventListener('change', function (e) {
		self.panelConfig.defaultRotation = this.value;
	});

	document
	.getElementById('mtool_individual_rotation')
	.addEventListener('change', function (e) {
		var rotationValue = parseInt(this.value);
		self.panelConfig.defaultRotation = rotationValue;
		document.getElementById('mtool_rotation').value = rotationValue;
		self.performRotate(rotationValue);
	});

	//Handle Left and Right Rotation
	$(document).on('click', '.mtool_orientation', function () {
		var orientationValue = $(this).val();
		self.performOrientation(orientationValue);
	});

	//Handle Mounting
	$(document).on('click', '.mtool_mounting', function () {
		$('.mtool_mounting').parent().removeClass('active');
		$(this).parent().addClass('active');
		var mountinValue = $(this).val();
		self.panelConfig.defaultOrientation = mountinValue;
		self.performMounting(mountinValue);
	});

	//Handle Tilt
	document
	.getElementById('mtool_tilt')
	.addEventListener('change', function (e) {
		var tiltValue = $(this).val();
		self.performTilt(tiltValue);
	});

	self.handle_panel_drop_target();
};

solar_tool.prototype.handle_panel_drop_target = function () {
	var self = this;
};

solar_tool.prototype.handle_annotation_right_sidebar = function () {
	var self = this;

	//Handle Add Annotation
	$(document).on('click', '#add-annotation', function (e) {
		var latLng = self.near_map_imagery_overlay.getBounds().getCenter();
		if(self.annotation_context_latLng != null){
			latLng = self.annotation_context_latLng;
			self.annotation_context_latLng = null;
		}
		var type = document
		.getElementById('annotation_list')
		.options[
		document.getElementById('annotation_list').selectedIndex
		].getAttribute('data-type');
		if (type == 'icon') {
			self.create_annotation(latLng);
		} else if (type == 'line') {
			self.create_line_annotation(latLng);
		} else if (type == 'polygon') {
			self.create_polygon_annotation(latLng);
		} else if (type == 'measuring-tape') {
			self.create_measuring_tape_annotation(latLng);
		}
	});
};

/** Utitlity Functions and Function to Get Coordinates */

solar_tool.prototype.get_panel_latLng_by_dimension = function (
	coordinate,
	width_meast,
	height_msouth
	) {
	var self = this;
	var mounting = self.panelConfig.defaultOrientation;

	var tiltValue = document.getElementById('mtool_tilt').value;
	tiltValue = tiltValue * (Math.PI / 180);
	height_msouth = height_msouth * Math.cos(tiltValue);

	if (mounting == 'portrait') {
		var northEast = L.GeometryUtil.destination(coordinate, 90, width_meast);
		var southWest = L.GeometryUtil.destination(
			coordinate,
			180,
			height_msouth
			);
	} else {
		var northEast = L.GeometryUtil.destination(
			coordinate,
			90,
			height_msouth
			);
		var southWest = L.GeometryUtil.destination(
			coordinate,
			180,
			width_meast
			);
	}
	return L.latLngBounds(southWest, northEast);
};

solar_tool.prototype.get_annotation_latLng_by_dimension = function (
	coordinate,
	width_meast,
	height_msouth
	) {
	var self = this;
	var northEast = L.GeometryUtil.destination(coordinate, 90, width_meast);
	var southWest = L.GeometryUtil.destination(coordinate, 180, height_msouth);
	return L.latLngBounds(southWest, northEast);
};

solar_tool.prototype.centerOfCoords = function (coords) {
	var self = this;
	var bound = L.latLngBounds();
	for (var i = 0; i < coords.length; i++) {
		bound.extend(coords[i]);
	}
	return bound.getCenter();
};

solar_tool.prototype.arrowCords = function (polygon) {
	var self = this;
	var coords = polygon.getLatLngs()[0];
	var c = self.centerOfCoords(coords);
	var a = self.centerOfCoords([coords[0], c]);
	var b = self.centerOfCoords([coords[1], c]);
	var mid = self.centerOfCoords([coords[0], coords[1]]);
	var mid2 = self.centerOfCoords([mid, c]);
	var absMid = self.centerOfCoords([mid, mid2]);
	return [a, absMid, b, absMid, a];
};

solar_tool.prototype.get_latLng_by_angle = function (
	panel,
	coordinate,
	angle,
	distance
	) {
	var self = this;
	var mounting = panel['options'].sp_orientation
	? panel['options'].sp_orientation
	: 'portrait';
	var rotation = panel['options'].sp_rotation
	? panel['options'].sp_rotation
	: 0;
	var northEast = L.GeometryUtil.destination(
		coordinate,
		angle + rotation,
		distance
		);
	return northEast;
};

solar_tool.prototype.create_uuid = function () {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		.toString(16)
		.substring(1);
	}
	return (
		s4() +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		s4() +
		s4()
		);
};

solar_tool.prototype.setCookie = function (cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	var expires = 'expires=' + d.toUTCString();
	document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
};

solar_tool.prototype.getCookie = function (cname) {
	var name = cname + '=';
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};

solar_tool.prototype.create_annotation_list_item = function (annotation) {
	var imgUrl = annotation.annotation._url;
	var aId = annotation.options.a_id;
	var label = annotation.annotation_text.label;
	var item =
	`<div class="panel-library-item pl-0" id="annotation_list_item_` +
	aId +
	`">
	<div class="panel-library-item__wrapper">
	<div class="panel-library-item__object">
	<img crossorigin="anonymous" src="` +
	imgUrl +
	`" draggable="false" class="panel-library-item__img">
	</div>
	</div>
	<div class="panel-library-item__details">
	<div class="input-group input-group-sm btn-inset" style="width: 185px;">
	<input type="text" name="label" class="annotation_label annotation_label_` +
	aId +
	` form-control" data-id="` +
	aId +
	`" value="` +
	label +
	`">
	<button type="button" class="btn btn-info btn-flat annotation_label_save_btn" data-id="` +
	aId +
	`">Save</button>
	</div>
	</div>
	<div class="panel-library-item__details" style="margin-left: 90px;">
	<button style="width:30px;" class="btn btn-xs btn-info annotation_label_delete_btn" data-id="` +
	aId +
	`" title="Delete"><i class="fa fa-trash"></i></button>
	</div>
	</div>`;
	$('#exisiting_annotations').append(item);
};

/* Object Save Update Destroy Backend Function */

solar_tool.prototype.fetch_mapping_tool_objects_data = function (formData) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/fetch_mapping_tool_objects_data',
		type: 'get',
		data: { tool_id: self.mtool_data.id },
		dataType: 'json',
		beforeSend: function () {
			$('#real_map_container').addClass('d-none');
			$('#loaderContainer').removeClass('d-none');
		},
		success: function (data) {
			$('#real_map_container').removeClass('d-none');
			$('#loaderContainer').addClass('d-none');

			self.initalize_map();
			self.initalize_map_with_near_map_imagery();
			var panelId = '';
			if (data.panel_objects.length > 0) {
				var panel_objects = data.panel_objects;
				for (var key in panel_objects) {
					panel_objects[key]['object_properties'][
					'sp_selected'
					] = false;
					panel_objects[key]['object_properties']['fillColor'] =
					self.panelConfig.defaultSolarPanelColor;
					if (key == panel_objects.length - 1) {
						panel_objects[key]['object_properties'][
						'sp_selected'
						] = true;
						panel_objects[key]['object_properties']['fillColor'] =
						self.panelConfig.activeSolarPanelColor;
					}
					self.create_panel_from_object_data(panel_objects[key]);
					if (key == panel_objects.length - 1) {
						var object_properties =
						panel_objects[key].object_properties;
						var or = object_properties.sp_orientation;
						$('#mtool_' + or).click();
					}
					panelId = panel_objects[key]['object_properties']['sp_panel_id'];
				}
				$('select#mtool_panel_list option[value!='+panelId+']').hide();
				$('#mtool_panel_list').trigger('change');
			}

			if (data.annotation_objects.length > 0) {
				var annotation_objects = data.annotation_objects;
				for (var key in annotation_objects) {
					self.create_annotation_from_object_data(
						annotation_objects[key]
						);
				}
			}

			if (data.custom_annotation_objects.length > 0) {
				var custom_annotation_objects = data.custom_annotation_objects;
				for (var key in custom_annotation_objects) {
					self.create_custom_annotation_from_object_data(
						custom_annotation_objects[key]
						);
				}
			}
			$('#tool_manage_panel').trigger('click');
			setTimeout(function () {
				self.mtool_data_save = true;
				self.calculate_system_summary();
			}, 2000);

		},
		error: function () {
			$('#real_map_container').removeClass('d-none');
			$('#loaderContainer').addClass('d-none');
			self.mtool_data_save = true;
		},
	});
};

solar_tool.prototype.save_mapping_object = function (objectData) {
	var self = this;

	if (!self.mtool_data_save) {
		return false;
	}

	objectData.proposal_id = self.proposal_id;
	objectData.lead_id = self.lead_id;
	objectData.site_id = self.site_id;
	objectData.section = self.section;
	$.ajax({
		url: base_url + 'admin/solar-tool/save_mapping_object',
		type: 'post',
		data: objectData,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
		error: function (data) {
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
	});
};

solar_tool.prototype.remove_mapping_object = function (objectData) {
	var self = this;
	if (!self.mtool_data_save) {
		return false;
	}
	$.ajax({
		url: base_url + 'admin/solar-tool/remove_mapping_object',
		type: 'post',
		data: objectData,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
			if (objectData.object_type == 'PANEL') {
				var allDrawnPanels = self.solarPanels;
				var deletedIndex = null;
				for (var key in allDrawnPanels) {
					if (allDrawnPanels[key]['options']['sp_id'] == objectData.object_id) {
						allDrawnPanels[key]['arrow'].remove();
						allDrawnPanels[key].remove();
						deletedIndex = key;
					}
				}
				if (deletedIndex != null) {
					self.solarPanels.splice(deletedIndex, 1);
				}
			}

			self.calculate_system_summary();
		},
		error: function (data) {
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
	});
};


solar_tool.prototype.save_mapping_object_bulk = function (objectData) {
	var self = this;

	if (!self.mtool_data_save) {
		return false;
	}

	objectData += '&proposal_id=' +  self.proposal_id;
	objectData += '&lead_id=' + self.lead_id;
	objectData += '&site_id=' + self.site_id;
	objectData += '&section=' + self.section;

	$.ajax({
		url: base_url + 'admin/solar-tool/save_mapping_object_bulk',
		type: 'post',
		data: objectData,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
			//$.isLoading('hide');
			//$("#tab_pylon_mapping_tool").isLoading({ text: "Saving Changes. Please Wait....", position:'overlay'});
		},
		success: function (data) {
			//$.isLoading('hide');
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
		error: function (data) {
			$.isLoading('hide');
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
	});
};


solar_tool.prototype.remove_mapping_object_bulk = function (objectData) {
	var self = this;

	if (!self.mtool_data_save) {
		return false;
	}

	objectData += '&proposal_id=' +  self.proposal_id;
	objectData += '&lead_id=' + self.lead_id;
	objectData += '&site_id=' + self.site_id;
	objectData += '&section=' + self.section;

	$.ajax({
		url: base_url + 'admin/solar-tool/remove_mapping_object_bulk',
		type: 'post',
		data: objectData,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
			$.isLoading('hide');
			$("#tab_pylon_mapping_tool").isLoading({ text: "Saving Changes. Please Wait....", position:'overlay'});
		},
		success: function (data) {
			$.isLoading('hide');
			$('#object_loader').addClass('d-none');
			var deleted_ids = data.deleted_ids;
			var allDrawnPanels = self.solarPanels;
			for (var key in deleted_ids) {
				for (var key1 in self.solarPanels) {
					if (self.solarPanels[key1]['options']['sp_id'] == deleted_ids[key]) {
						self.solarPanels[key1]['arrow'].remove();
						self.solarPanels[key1].remove();
						self.solarPanels.splice(key1, 1);
					}
				}
			}
			self.calculate_system_summary();
		},
		error: function (data) {
			$.isLoading('hide');
			$('#object_loader').addClass('d-none');
			self.calculate_system_summary();
		},
	});
};


solar_tool.prototype.create_snapshot = function () {
	var self = this;

	$(document).on('click', '#snapshot_btn1', function () {

		$("#tab_pylon_mapping_tool").isLoading({ text: "Saving Snapshot. Please Wait....", position:'overlay'});

		$('.leaflet-control-container').addClass('d-none');

		//Create 1 Snapshot with all objects
		self.google_map.remove();
		self.near_map_imagery_overlay.setStyle({
			opacity: 1,
		});

		var node = document.getElementById('real_map_container');
		domtoimage
		.toPng(node)
		.then(function (dataUrl) {
			$('#image').attr('src', dataUrl);
			$('.leaflet-control-container').removeClass('d-none');
				//Add Google Mutant Layer To Replace Imagery Too Google
				self.google_map = L.gridLayer
				.googleMutant({
					type: 'hybrid',
					maxZoom: 23,
					maxNativeZoom: 21,
					attribution:
					'Imagery &copy; <a href="https://maps.google.com" target="_blank">Google</a>',
				})
				.addTo(self.map);

				self.near_map_imagery_overlay.setStyle({
					opacity: 0.87,
				});

				var postData = {
					tool_id: self.mtool_data.id,
					snapshot1: $('#image').attr('src'),
				};

				self.save_snapshot(postData);

				$('#snapshot_btn2').click();
			})
		.catch(function (error) {
			console.error('oops, something went wrong!', error);
		});
	});

	$(document).on('click', '#snapshot_btn2', function () {

		$('.leaflet-control-container').addClass('d-none');

		//Create 1 Snapshot with all objects
		self.google_map.remove();
		self.near_map_imagery_overlay.setStyle({
			opacity: 1,
		});

		for (var key in self.annotations) {
			var annotation = self.annotations[key];

			if (
				annotation['annotation'] != undefined &&
				annotation._latlng != undefined
				) {
				annotation['annotation'].remove();
		}
		if (annotation['annotation_text'] != undefined) {
			annotation['annotation_text'].remove();
		}
		if (annotation['annotation_innerPointCap'] != undefined) {
			annotation['annotation_innerPointCap'].remove();
		}
		if (annotation['annotation_outerPointCap'] != undefined) {
			annotation['annotation_outerPointCap'].remove();
		}
		if (annotation['annotation_line'] != undefined) {
			annotation['annotation_line'].remove();
		}
		annotation.remove();
	}

	setTimeout(function () {
		var node = document.getElementById('real_map_container');
		domtoimage
		.toPng(node)
		.then(function (dataUrl) {
			$('#image').attr('src', dataUrl);
			$('.leaflet-control-container').removeClass('d-none');
					//Add Google Mutant Layer To Replace Imagery Too Google
					self.google_map = L.gridLayer
					.googleMutant({
						type: 'hybrid',
						maxZoom: 23,
						maxNativeZoom: 21,
						attribution:
						'Imagery &copy; <a href="https://maps.google.com" target="_blank">Google</a>',
					})
					.addTo(self.map);

					self.near_map_imagery_overlay.setStyle({
						opacity: 0.87,
					});

					var postData = {
						tool_id: self.mtool_data.id,
						snapshot2: $('#image').attr('src'),
					};

					self.save_snapshot(postData, true);
				})
		.catch(function (error) {
			console.error('oops, something went wrong!', error);
		});
	}, 2000);
});
};

solar_tool.prototype.save_snapshot = function (postData, reload) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/save_snapshot',
		type: 'post',
		data: postData,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
			if (reload != undefined) {
				//self.updateQueryStringParam('tab','mapping_tool');
				window.location.reload();
			}
		},
		error: function (data) {
			$('#object_loader').addClass('d-none');
		},
	});
};

solar_tool.prototype.save_mapping_data = function (data) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/save_mapping_data',
		type: 'post',
		data: data,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
			toastr['success'](data.status);
		},
		error: function (data) {
			toastr['success']('Data Saving Failed');
		},
	});
};

solar_tool.prototype.update_near_map_imagery = function (data) {
	var self = this;

	$.ajax({
		url: base_url + 'admin/solar-tool/update_near_map_imagery',
		type: 'post',
		data: data,
		dataType: 'json',
		beforeSend: function () {
			$('#object_loader').removeClass('d-none');
		},
		success: function (data) {
			$('#object_loader').addClass('d-none');
			toastr['success'](data.status);
			self.updateQueryStringParam('tab', 'mapping_tool');
			window.location.reload();
		},
		error: function (data) {
			toastr['success']('Data Saving Failed');
		},
	});
};

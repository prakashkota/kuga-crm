<link href='<?php echo site_url(); ?>assets/css/jquery-ui.min.css' type='text/css' rel='stylesheet'>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<style>
    .control-label:after {
        content: "*";
        color: red;
    }

    .page-wrapper {
        /* background-color: #ECEFF1; */
        background-color: #E4E4E4;
        min-height: 100%;
    }

    .state {
        width: 70px !important;
    }

    .mask {
        margin-left: -15px;
    }

    #proposal_image_close {
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 30px;
        right: 2px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background: url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    #inverter_image_close {
        display: block;
        position: absolute;
        width: 30px;
        height: 30px;
        top: 52px;
        right: 12px;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background: url(https://web.archive.org/web/20110126035650/http://digitalsbykobke.com/images/close.png) no-repeat center center;
    }

    .table-default table tr td {
        padding: 5px 5px 5px 5px !important;
    }


    ::placeholder {
        /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #000 !important;
        opacity: 1;
        /* Firefox */
    }

    form .form-control {
        border: 1px solid #A9A9A9;
        color: #000;
    }

    .isloading-wrapper.isloading-right {
        margin-left: 10px;
    }

    .isloading-overlay {
        position: relative;
        text-align: center;
    }

    .isloading-overlay .isloading-wrapper {
        background: #FFFFFF;
        -webkit-border-radius: 7px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 7px;
        -moz-background-clip: padding;
        border-radius: 7px;
        background-clip: padding-box;
        display: inline-block;
        margin: 0 auto;
        padding: 10px 20px;
        top: 40%;
        z-index: 9000;
    }

    .remove_field {
        color: red;
    }

    .monthly-event-list {
        top: 22.5em !important;
        height: calc(100% - 25.5em) !important;
    }

    .steps {
        list-style-type: none;
        padding: 0;
    }

    .steps li {
        display: inline-block;
        margin-bottom: 3px;
    }

    .steps li a,
    .steps li p {
        background: #e5f4fd;
        padding: 8px 20px;
        color: #0077bf;
        display: block;
        font-size: 14px;
        font-weight: bold;
        position: relative;
        text-indent: 12px;
    }

    .steps li a:hover,
    .steps li p:hover {
        text-decoration: none;
    }

    .steps li a:before,
    .steps li p:before {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #fff;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        left: 0;
        top: 50%;
        width: 0;
        margin-top: -18px;
    }

    .steps li a:after,
    .steps li p:after {
        border-bottom: 18px solid transparent;
        border-left: 12px solid #e5f4fd;
        border-top: 18px solid transparent;
        content: "";
        height: 0;
        position: absolute;
        /*right: -12px;*/
        left: 100%;
        top: 50%;
        width: 0;
        margin-top: -18px;
        z-index: 1;
    }

    .steps li.active a,
    .steps li.active p {
        background: #5cb85c;
        color: #fff;
    }

    .steps li.active a:after,
    .steps li.active p:after {
        border-left: 12px solid #5cb85c;
    }

    .steps li.undone a,
    .steps li.undone p {
        background: #eee;
        color: #333;
    }

    .steps li.undone a:after,
    .steps li.undone p:after {
        border-left: 12px solid #eee;
    }

    .steps li.undone p {
        color: #aaa;
    }

    .sr-deal_btn {
        background: #ECEFF1;
        color: #000;
    }

    .sr-deal_btn__won:hover {
        background: #5cb85c;
        color: #fff;
    }

    .sr-deal_btn__lost:hover {
        background: #d9534f;
        color: #fff;
    }

    .sr-deal_btn--won {
        background-color: #5cb85c;
        color: #fff;
    }

    .sr-deal_btn--lost {
        background-color: #d9534f;
        color: #fff;
    }

    .sr-no_wrap {
        white-space: nowrap;
    }

    .sr-inactive_user {
        color: red;
    }

    .select2-container {
        height: inherit !important;
        padding: inherit !important;
    }

    .card_body__title h2 {
        display: inline-flex;
        padding-bottom: 10px;
    }

    .form-block {
        margin-bottom: 0px !important;
    }

    .kg-activity {
        border-left-color: #f0ad4e;
        border: 1px solid #f0ad4e;
        border-left-width: .25rem;
        border-radius: .25rem;
    }

    .kg-activity__description {
        background-color: #ffffe0;
        margin: -12px;
        padding: 12px;
    }

    .card_row_1 {
        min-height: 429px;
    }

    .sr-switch {
        font-size: 1rem;
        position: relative;
    }

    .sr-switch input {
        position: absolute;
        height: 1px;
        width: 1px;
        background: none;
        border: 0;
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        overflow: hidden;
        padding: 0;
    }

    .sr-switch input+label {
        position: relative;
        min-width: calc(calc(2.375rem * .8) * 2);
        border-radius: calc(2.375rem * .8);
        height: calc(2.375rem * .8);
        line-height: calc(2.375rem * .8);
        display: inline-block;
        cursor: pointer;
        outline: none;
        user-select: none;
        vertical-align: middle;
        text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
    }

    .sr-switch input+label::before,
    .sr-switch input+label::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: calc(calc(2.375rem * .8) * 2);
        bottom: 0;
        display: block;
    }

    .sr-switch input+label::before {
        right: 0;
        background-color: #dee2e6;
        border-radius: calc(2.375rem * .8);
        transition: 0.2s all;
    }

    .sr-switch input+label::after {
        top: 2px;
        left: 2px;
        width: calc(calc(2.375rem * .8) - calc(2px * 2));
        height: calc(calc(2.375rem * .8) - calc(2px * 2));
        border-radius: 50%;
        background-color: white;
        transition: 0.2s all;
    }

    .sr-switch input:checked+label::before {
        background-color: #08d;
    }

    .sr-switch input:checked+label::after {
        margin-left: calc(2.375rem * .8);
    }

    .switch input:focus+label::before {
        outline: none;
        box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
    }

    .sr-switch input:disabled+label {
        color: #868e96;
        cursor: not-allowed;
    }

    .sr-switch input:disabled+label::before {
        background-color: #e9ecef;
    }

    .switch.switch-sm {
        font-size: 0.875rem;
    }

    .sr-switch.switch-sm input+label {
        min-width: calc(calc(1.9375rem * .8) * 2);
        height: calc(1.9375rem * .8);
        line-height: calc(1.9375rem * .8);
        text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
    }

    .sr-switch.switch-sm input+label::before {
        width: calc(calc(1.9375rem * .8) * 2);
    }

    .sr-switch.switch-sm input+label::after {
        width: calc(calc(1.9375rem * .8) - calc(2px * 2));
        height: calc(calc(1.9375rem * .8) - calc(2px * 2));
    }

    .sr-switch.switch-sm input:checked+label::after {
        margin-left: calc(1.9375rem * .8);
    }

    .sr-switch+.sr-switch {
        margin-left: 1rem;
    }

    .tbl-as-inpt tr {
        width: 100%;
        display: block;
    }

    .tbl-as-inpt.table-default table tr td {
        display: block;
        width: 100%;
        padding: 0 !important;
        border: none;
        background: none;
        font-size: 15px;
    }

    .tbl-as-inpt.table-default table tr td .total-price {

        padding: 0;
        font-size: 12px;
        background-color: #ECEFF1;
        border: solid 1px #000;
        padding: 10px 8px;
        margin: 6px 0 15px 0;
        display: block;

    }


    @media only screen and (min-width: 992px) {
        .modal-content {
            width: 900px;
        }

        .modal-dialog {
            max-width: 800px;
        }
    }

    @media only screen and (max-width: 991px) {
        .modal-content {
            width: 360px;
        }
    }


    .page-wrapper-inner {
        background: #EAEEF0;
        padding: 2rem;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    }

    .page-wrapper-inner .page-wrapper-inner-h2 {
        font-weight: 500;
        font-size: 1.6rem;
        margin: 0;
    }

    .page-wrapper-inner .page-tab .nav-item {
        margin-right: 0.5rem;
    }

    .page-wrapper-inner .page-tab .nav-item .nav-link {
        background: #A5A5A5;
        font-weight: 500;
        color: #000;
        padding: 0.8rem 2.2rem;
        border-radius: 0;
    }

    .page-wrapper-inner .page-tab .nav-item .nav-link.active {
        background: #4A4A4A;
        color: #fff;
    }

    .pv_system_losses_details {}

    .pv_estimated_system_losses {
        background: #EAEEF0;
        padding: 1.6rem 2.4rem;
        border-radius: 1rem;
        display: flex;
        align-items: center;
        flex-direction: column;
    }

    .pv_estimated_system_losses strong {
        font-weight: bold;
        font-size: 3.2rem;
    }

    .pv_system_losses_details table {
        width: 100%;
    }

    .pv_system_losses_details tr th {
        padding: 0.2rem 0.4rem;
        width: 60%;
        font-size: 1.6rem;
    }

    .pv_system_losses_details tr td {
        padding: 0.2rem 0rem 0.2rem 0.8rem;
    }

    .pv_system_losses_details input {
        padding: 0.5rem 0.5rem;
        width: 100%;
    }

    .system_losses_model .modal-dialog {
        max-width: 560px;
    }

    .system_losses_model .modal-dialog .modal-content {
        width: 560px;
    }

    .system_losses_model .modal-dialog .modal-content .modal-body {
        padding: 1rem 1.6rem !important;
    }

    .system_losses_model .modal-dialog .modal-content .modal-body h3 {
        font-weight: bold;
        font-size: 2rem;
    }

    .losses_toggle_link {
        color: #b92625 !important;
        font-weight: bold !important;
        font-size: 1.4rem !important;
    }

    .losses_toggle_link:hover {
        color: #000 !important;
    }

    .advanced_paramter_toggle {
        color: #b92625 !important;
        font-weight: bold !important;
        font-size: 1.4rem !important;
        cursor: pointer;
    }

    .advanced_paramter_toggle:hover {
        color: #000 !important;
    }

    .time_use_model .btn-group-toggle .btn {
        display: initial !important;
    }

    .time_use_model table tbody tr td {
        vertical-align: middle;
    }

    .time_use_model .modal-dialog {
        max-width: calc(100vw - 2rem);
        margin-left: auto;
        margin-right: auto;
        display: flex;
        justify-content: center;
    }

    .time_use_model .modal-dialog .modal-content {
        width: inherit;
    }

    #map_near_map {
        height: 920px !important;
    }

    #real_map_container,
    #proposal_image .add-picture {
        height: 920px !important;
    }

    #proposal_no_image .add-picture {
        height: 920px !important;
    }

    #avg_kw_per_day_chart>div>div {
        margin: 0 auto;
    }

    .solarAzimuthTiltInputsControl .solarAzimuthTiltInputsAdd,
    .solarAzimuthTiltInputsControl .solarAzimuthTiltInputsRemove {
        color: #b92625 !important;
        font-weight: bold !important;
        font-size: 1.4rem !important;
        cursor: pointer;
    }
</style>




<div class="page-wrapper d-block clearfix ">
    <!-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $title; ?></h1>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group">
                <a class="btn btn-outline-primary" href="<?php echo site_url('admin/lead/add?deal_ref=' . $lead_data['uuid']) ?>"><i class="icon-arrow-left"></i> Back to Deal </a>
            </div>
        </div>
    </div> -->


    <input type="hidden" id="image" name="solar_proposal[image]" />
    <!-- <input type="hidden" id="inverter_image" name="solar_proposal[inverter_image]" /> -->
    <input type="hidden" name="solar_proposal[pvwatts_data]" id="pvwatts_data" />
    <input type="hidden" name="solar_proposal[near_map_data]" id="near-map-exported-data" />
    <input type="hidden" name="solar_proposal[total_system_size]" id="total_system_size">
    <input type="hidden" name="solar_panel_data[no_of_panels][0]" class="no_of_panels">
    <input type="hidden" name="solar_proposal[total_panel_area]" id="total_panel_area" />
    <input type="hidden" name="solar_proposal[nearmap_toggle]" id="nearmap_toggle_val" />

    <div class="row mb-5">


        <div class="col-12 col-sm-12 col-md-12">
            <!-- page wrapper inner -->
            <div class="page-wrapper-inner">

                <div class="d-block clearfix mb-3 border-bottom">
                    <h1 class="page-wrapper-inner-h2">Commercial Solar Proposal</h1>

                </div>
                <div class="d-block clearfix ">
                    <ul class="nav nav-pills page-tab commerical-panel-tab" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-id="1" id="pills-panel-tab" data-toggle="pill" href="#pills-panel" role="tab" aria-controls="pills-panel" aria-selected="true">Panel Design</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-id="2" id="pills-bill-tab" data-toggle="pill" href="#pills-bill" role="tab" aria-controls="pills-bill" aria-selected="false">Bill Details</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-id="3" id="pills-inverter-tab" data-toggle="pill" href="#pills-inverter" role="tab" aria-controls="pills-inverter" aria-selected="false">Inverter Design</a>
                        </li>
                        <!-- <li class="nav-item" role="presentation">
                            <a class="nav-link" data-id="4" id="pills-system-tab" data-toggle="pill" href="#pills-system" role="tab" aria-controls="pills-system" aria-selected="false">System Details</a>
                        </li> -->
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-id="5" id="pills-financial-tab" data-toggle="pill" href="#pills-financial" role="tab" aria-controls="pills-financial" aria-selected="false">Financial Summary</a>
                        </li>
                    </ul>
                    <input type="hidden" name="tab_name" value="1" id="CommercialPanelTab" />

                    <div class="tab-content mt-5" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-panel" role="tabpanel" aria-labelledby="pills-panel-tab">

                            <!-- PANEL START -->
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="card" style="padding: 0;background: transparent;box-shadow: none;">
                                        <div class="card-body" style="padding: 0;margin-bottom: 0px;">
                                            <ul class="nav nav-tabs justify-content-left" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#tab_image_upload" id="tab_image_upload_btn" role="tab" data-toggle="tab">
                                                        Upload Image</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link d-none" href="#tab_near_map" id="tab_near_map_btn" role="tab" data-toggle="tab">Near Map</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#tab_pylon_mapping_tool" id="tab_pylon_mapping_tool_btn" role="tab" data-toggle="tab">Mapping Tool</a>
                                                </li>
                                                <li class="nav-item d-none">
                                                    <a class="nav-link" href="#tab_mapping_tool" id="tab_mapping_tool_btn" role="tab" data-toggle="tab">Mapping Tool</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active show" id="tab_image_upload">
                                                    <div class="form-block d-block clearfix">
                                                        <div class="form-block d-block clearfix" id="proposal_no_image">
                                                            <div class="add-picture">
                                                                <span>Add picture</span>
                                                                <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                                <input type="file" id="proposal_image_upload" />
                                                            </div>
                                                        </div>
                                                        <div class="form-block d-block clearfix" id="proposal_image" style="display:none !important; ">
                                                            <div class="add-picture"></div>
                                                            <a id="proposal_image_close" href="javascript:void(0);"></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane fade in" id="tab_near_map">
                                                    <?php // echo $new_mapping_tool; 
                                                    ?>
                                                </div>

                                                <div role="tabpanel" class="tab-pane fade in" id="tab_pylon_mapping_tool">
                                                    <?php echo $solar_tool; ?>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade in" id="tab_mapping_tool">
                                                    <div class="form-block d-block clearfix">
                                                        <div class="form-block d-block clearfix" id="mapping_tool">
                                                            <?php // echo $mapping_tool; 
                                                            ?>
                                                        </div>
                                                        <div class="form-block d-block clearfix" id="mapping_tool_image" style="display:none !important; ">
                                                            <div class="add-picture"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row bill-wrapper">
                                <div class="col-12 col-sm-12 col-md-6">
                                    <form role="form" action="" method="post" id="importProductionDataFileSubmit" enctype="multipart/form-data">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card_body__title">
                                                    <h2>System Info</h2>
                                                </div>
                                                <div class="row mb-4">
                                                    <div class="form-group">
                                                        <div class="col-md-12" id="">
                                                            <label class="required-label form-label-bold">Production<span class="text-danger">*</span>: </label>
                                                            <div class="btn-group btn-group-toggle ku-switch-toggle" data-toggle="buttons" style="margin-left: 60px;">
                                                                <label class="btn <?php if ($proposal_data['is_production_file'] == 1) {
                                                                                        echo "active";
                                                                                    } ?>">
                                                                    <input type="radio" class="uploadProductionToggle" name="is_uploadProduction" value="1" <?php if ($proposal_data['is_production_file'] == 1) {
                                                                                                                                                                echo "checked";
                                                                                                                                                            } ?>> Upload
                                                                </label>
                                                                <label class="btn <?php if ($proposal_data['is_production_file'] == 0) {
                                                                                        echo "active";
                                                                                    } ?>">
                                                                    <input type="radio" class="uploadProductionToggle" name="is_uploadProduction" value="0" <?php if ($proposal_data['is_production_file'] == 0) {
                                                                                                                                                                echo "checked";
                                                                                                                                                            } ?>> Mapping Tool
                                                                </label>
                                                                <label class="btn <?php if ($proposal_data['is_production_file'] == 2) {
                                                                                        echo "active";
                                                                                    } ?>">
                                                                    <input type="radio" class="uploadProductionToggle" name="is_uploadProduction" value="2" <?php if ($proposal_data['is_production_file'] == 2) {
                                                                                                                                                                echo "checked";
                                                                                                                                                            } ?>> Azimuth & Tilt
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 productionDataFileURL" style="display:none;">

                                                            <input class="" type="file" id="productionDataFileURL" name="productionDataFileURL" value="">
                                                            <?php if ($proposal_data['is_production_file'] == 1) { ?>
                                                                <a class="d-block mt-2" href="<?php echo site_url() . '/assets/uploads/production_data_files/' . $proposal_data['production_data_file']; ?>" target="_blank"><?php echo $proposal_data['production_data_file']; ?></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row mb-4 solarAzimuthTiltSection" style="display:none">
                                                    <div class="col-md-12">
                                                        <div class="form-row mb-4">
                                                            <div class="col-sm-3 col-md-3"></div>
                                                            <label class="col-sm-3 col-md-3 form-label-bold">Orentation</label>
                                                            <label class="col-sm-3 col-md-3 form-label-bold">Tilt</label>
                                                            <label class="col-sm-3 col-md-3 form-label-bold">Capacity(Kw)</label>
                                                        </div>
                                                        <div class="" id="solarAzimuthTiltInputs"></div>
                                                        <div class="d-flex justify-content-between solarAzimuthTiltInputsControl mt-2">
                                                            <div class="solarAzimuthTiltInputsAdd" data-action="add">+ Add Aspect</div>
                                                            <div class="solarAzimuthTiltInputsRemove" data-action="remove">- Remove Aspect</div>
                                                        </div>
                                                        <input type="hidden" id="noAzimuthTiltRows" value="0" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <form method="post" id="solarSystemDetails">
                                                            <!-- <div class="row"> -->
                                                            <div class="form-row mb-4">
                                                                <label for="" class="col-sm-2 col-md-2 form-label-bold">Panel<span class="text-danger">*</span></label>
                                                                <div class="col-md-8 pl-5">
                                                                    <select class="form-control" name="system_pricing_panel" id="system_pricing_panel">
                                                                        <option value="">Select Panel</option>
                                                                        <?php if ($solar_panels) {
                                                                            foreach ($solar_panels as $panel) { ?>
                                                                                <option value="<?= $panel['name'] ?>" data-item='<?= json_encode($panel, true) ?>' data-degradation='<?= $panel['panel_degradation_per'] ?>' data-id="<?= $panel['id'] ?>"><?= $panel['name'] ?></option>
                                                                        <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="number" class="form-control" min="0" id="system_pricing_panel_val" name="system_pricing_panel_val" value="0" readonly>
                                                                </div>
                                                            </div>

                                                            <div class="form-row mb-4">
                                                                <label for="" class="col-sm-2 col-md-2 form-label-bold">Inverter<span class="text-danger">*</span></label>
                                                                <div class="col-md-8 pl-5">
                                                                    <select class="form-control" required="" name="system_pricing_inverter" id="system_pricing_inverter">
                                                                        <option value="">Select Inverter</option>
                                                                        <?php if ($inverters) {
                                                                            foreach ($inverters as $inverter) { ?>
                                                                                <option value="<?= $inverter['name'] ?>" data-item='<?= json_encode($inverter) ?>' data-id="<?= $inverter['id'] ?>" data-efficiency="<?= $inverter['inverter_efficiency'] ?>"><?= $inverter['name'] ?></option>
                                                                        <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="number" min="0" value="0" name="system_pricing_inverter_val" class="form-control" id="system_pricing_inverter_val">
                                                                </div>
                                                            </div>
                                                            <div class="form-row mb-4">
                                                                <label for="" class="col-sm-2 col-md-2 form-label-bold">Battery</label>
                                                                <div class="col-md-8 pl-5">
                                                                    <select class="form-control" required="" name="system_pricing_battery" id="system_pricing_battery">
                                                                        <option value="">Select Battery</option>
                                                                        <?php if ($battery) {
                                                                            foreach ($battery as $battery) { ?>
                                                                                <option value="<?= $battery['name'] ?>" data-item='<?= json_encode($battery) ?>' data-id="<?= $battery['id'] ?>"><?= $battery['name'] ?></option>
                                                                        <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <input type="number" class="form-control" name="system_pricing_battery_val" min="0" id="system_pricing_battery_val" value="0">
                                                                </div>
                                                            </div>
                                                            <div class="form-row mb-4">
                                                                <label for="" class="col-sm-2 col-md-2 form-label-bold system_bettery_size">Battery Size</label>
                                                                <div class="col-md-3 system_bettery_size pl-5">
                                                                    <div class="input-group mb-3">
                                                                        <input class="form-control" type="text" name="system_pricing_battery_size" value="" aria-describedby="subsequent" id="system_pricing_battery_size" readonly>
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text" id="subsequent">kwh</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12" id="">
                                                        <div class="form-group">
                                                            <label class="required-label">DC System Size (kW):</label>
                                                            <input class="form-control pvwattsCalc" placeholder="" id="pv_system_size" type="text" name="pv_system_size" required="" readonly="true">
                                                        </div>
                                                        <div class="form-group d-solar-production d-none">
                                                            <label class="required-label">Module Type:</label>
                                                            <select name="pv_module_type" id="pv_module_type" class="form-control pvwattsCalc" required="">
                                                                <option value="0">Standard</option>
                                                                <option value="1" selected>Premium</option>
                                                                <option value="2"> Thin film</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group d-solar-production d-none">
                                                            <label class="required-label">Array Type:</label>
                                                            <select name="pv_array_type" id="pv_array_type" class="form-control pvwattsCalc" required="">
                                                                <option value="0">Fixed Open Racking - Tilt</option>
                                                                <option value="1" selected>Fixed Roof Mount - Fixed to Roof</option>
                                                                <!-- <option value="2">1-Axis</option>
                                                                <option value="3">1-Axis Backtracking</option>
                                                                <option value="4">2-Axis</option> -->
                                                            </select>
                                                        </div>
                                                        <div class="form-group d-solar-production">
                                                            <label class="required-label">
                                                                System Losses (%):<div class=system_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#system_system_losses"><i class="fas fa-info-circle"></i></div>
                                                            </label>

                                                            <input type="hidden" id="losses_toggle_value" value="0" />
                                                            <input class="form-control pvwattsCalc" placeholder="" id="pv_system_losses" type="text" name="pv_system_losses" value="17.99" required="">
                                                        </div>
                                                        <div class="form-group d-none">
                                                            <label class="required-label">Tilt (deg):</label>
                                                            <input class="form-control pvwattsCalc" placeholder="" id="pv_tilt" type="text" name="pv_tilt" value="20" required="">
                                                        </div>
                                                        <div class="form-group d-none">
                                                            <label class="required-label">Azimuth (deg):</label>
                                                            <input class="form-control pvwattsCalc" placeholder="" id="pv_azimuth" type="text" name="pv_azimuth" value="180" required="">
                                                        </div>


                                                        <input type="hidden" name="pv_lat" id="pv_lat" />
                                                        <input type="hidden" name="pv_lng" id="pv_lng" />


                                                        <!-- <div class="form-group advanced_paramter_toggle">
                                                            <i class="fa fa-plus mr-2"></i>Advanced Parameter
                                                        </div> -->
                                                        <a type="button" class="losses_toggle_link mb-4 d-solar-production" data-toggle="modal" data-target="#lossesModel">
                                                            Advanced <i class="fa fa-chevron-down ml-2"></i>
                                                        </a>

                                                        <?php include 'system_losses.php' ?>
                                                        <input type="hidden" name="" value="0" id="advanced_paramter_toggle_value" />

                                                        <div class="advanced_parameter_body d-solar-production">
                                                            <div class="form-group">
                                                                <label class="required-label" style="display:inline-flex;">DC to AC Size Ratio: <div style="cursor:pointer;" class="pv_ac_dc_ratio_info" data-toggle="modal" data-target="#pv_ac_dc_ratio_info"><i class="fas fa-info-circle"></i></div></label>
                                                                <input class="form-control pvwattsCalc" placeholder="" id="pv_ac_dc_ratio" type="text" name="pv_ac_dc_ratio" value="1.2" required="">
                                                            </div>
                                                            <div class="form-group d-none">
                                                                <label class="required-label">Inverter Efficiency (%):</label>
                                                                <input class="form-control pvwattsCalc" placeholder="" id="pv_inverter_efficiency" type="text" name="pv_inverter_efficiency" value="" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="required-label" style="display:inline-flex;">Ground Coverage Ratio:<div class="pv_ground_coverage_info" style="cursor:pointer;" data-toggle="modal" data-target="#pv_ground_coverage_info"><i class="fas fa-info-circle"></i></div></label>
                                                                <input class="form-control pvwattsCalc" placeholder="" id="pv_ground_coverage" type="text" name="pv_ground_coverage" value="0.7" required="">
                                                            </div>
                                                            <div class="form-group d-none">
                                                                <label class="required-label">Degradation Factor (%):</label>
                                                                <input class="form-control pvwattsCalc" placeholder="" id="pv_degradation_factor" type="text" name="pv_degradation_factor" value="" required="">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card" style="display:none;">
                                            <div class="card-body">
                                                <div class="card_body__title">
                                                    <h2>Retail Electricity Rate</h2>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12" id="">
                                                        <div class="form-group">
                                                            <label class="required-label">Rate Type:</label>
                                                            <select name="" id="pv_rate_type" class="form-control pvwattsCalc" required="">
                                                                <option value="0" selected>Commercial</option>
                                                                <option value="1">Residential</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="required-label">Rate ($/kWh):</label>
                                                            <input class="form-control pvwattsCalc" placeholder="" id="pv_rate" type="text" name="" value="No Default – Enter Value" required="">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="card_body__title">
                                                <h2>Total Production</h2>
                                            </div>
                                            <div id="avg_kw_per_day_chart"></div>
                                            <div class="d-flex" style="flex-direction: column;align-items: center;">
                                                <div id="pvtotal_production" style="text-align: center;font-size: 1.6rem;font-weight: bold;">Total Production: <span></span></div>
                                                <div id="pvaverage_production" style="text-align: center;font-size: 1.6rem;font-weight: bold;">Average Monthly Production: <span></span></div>
                                                <div id="pvdaily_average_production" style="text-align: center;font-size: 1.6rem;font-weight: bold;">Average Daily Production: <span></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- PANEL END -->

                        </div>
                        <div class="tab-pane fade" id="pills-bill" role="tabpanel" aria-labelledby="pills-bill-tab">

                            <form method="post" id="importMeterDataFileSubmit" action="" enctype="multipart/form-data">

                                <input type="hidden" name="proposal_data_id" id="proposal_data_id" value="<?php echo $proposal_data_id; ?>">
                                <input type="hidden" name="proposal_address" id="proposal_address" value="<?php echo $lead_data['state_postal']; ?>">
                                <div class="row bill-wrapper">
                                    <div class="col-6 col-sm-6 col-md-6">
                                        <div class="card">
                                            <div class="card-body bill-wrapper-form">


                                                <div class="form-row mb-4">
                                                    <label for="" class="col-md-2 form-label-bold">Meter Data<span class="text-danger">*</span></label>
                                                    <div class="col-md-5 pl-5">
                                                        <select name="meter_upload_status" class="form-control" required="" id="meter_upload_status">
                                                            <option value="1" <?php if ($proposal_data['meter_data_type'] == '1') {
                                                                                    echo 'selected';
                                                                                } ?>>No Meter Data</option>
                                                            <option value="2" <?php if ($proposal_data['meter_data_type'] == '2') {
                                                                                    echo 'selected';
                                                                                } ?>>Upload Meter Data</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- id="meterDataFileURLRow" -->

                                                <div class="form-row mb-4" id="meter_data_action_outer">
                                                    <label for="" class="col-md-2 form-label-bold">Upload File<span class="text-danger">*</span></label>
                                                    <div class="col-md-7 pl-5">
                                                        <input class="" type="file" id="meterDataFileURL" name="meterDataFileURL">
                                                        <span id="meter_data_actions" style="display:flex; align-items:center;" class="<?php echo ($proposal_data['is_meter_data'] == 0) ? 'd-none' : ''; ?>">
                                                            <div class="btn-group btn-group-toggle ku-switch-toggle" role="group" aria-label="Basic example">
                                                                <a class="btn mt-2 btn-sm" id="meter_data_file_download_btn" style="border-radius: 0px;" href="<?php echo site_url() . '/assets/uploads/meter_data_files/' . $proposal_data['meter_data_file']; ?>" target="_blank"><i class="fa fa-download"></i> Current File</a>
                                                                <a class="btn mt-2 btn-sm" href="javascript:void(0);" id="meter_data_column_mapping_btn">Column Mapping</a>
                                                            </div>
                                                            <a class="losses_toggle_link ml-2" href="javascript:void(0);" id="meter_data_file_remove"><i class="fa fa-times"></i> Remove Current File</a>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-4" id="predefined_choose_file_outer" style="display:none">
                                                    <label for="" class="col-md-2 form-label-bold">Choose File</label>
                                                    <div class="col-md-5 pl-5">
                                                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#loadConsumptionProfile">Load Cosumption Profile</button>
                                                        <input type="hidden" name="predefined_choose_file" id="predefined_choose_file" />
                                                    </div>
                                                </div>
                                                <?php include "load_consumption_profile.php"; ?>
                                                <input type="hidden" name="is_meter_data" id="is_meter_data" value="<?php echo $proposal_data['is_meter_data']; ?>" />
                                                <input type="hidden" name="is_meter_data_file" id="is_meter_data_file" value="<?php echo $proposal_data['meter_data_file']; ?>" />

                                                <div class="form-row mt-5">
                                                    <label for="" class="col-md-2 form-label-bold">Bill Type</label>
                                                    <div class="col-md-6 pl-5">

                                                        <div class="btn-group btn-group-toggle ku-switch-toggle" data-toggle="buttons">
                                                            <label class="btn">
                                                                <input type="radio" class="is_bill_type" name="is_bill_type" value="flat" id="flat"> Flat
                                                            </label>
                                                            <label class="btn active">
                                                                <input type="radio" class="is_bill_type" name="is_bill_type" value="timeofUse" id="timeofUse" checked> Time of Use
                                                            </label>
                                                        </div>

                                                        <a type="button" class="losses_toggle_link ml-4" data-toggle="modal" data-target="#timeUseModel" id="timeOfUseModelLink">
                                                            Set Peak/Off-Peak Times
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-4 mt-2" id="consumption_inputs_row">
                                                    <label for="" class="col-md-2 form-label-bold">Consumption Inputs<span class="text-danger">*</span></label>
                                                    <div class="col-md-3 pl-5">
                                                        <select class="consumption_inputs" name="consumption_inputs" id="consumption_inputs" style="width: 104px; font-size: 12px; height: 27px;">
                                                            <option value="Simple" selected>Simple</option>
                                                            <option value="Detailed">Detailed</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-4 mt-2" id="daily_average_consumption_row">
                                                    <label for="" class="col-md-2 form-label-bold">Daily Average Consumption<span class="text-danger">*</span></label>
                                                    <div class="col-md-3 pl-5">
                                                        <div class="input-group mb-3">
                                                            <input class="form-control" type="text" id="daily_average_consumption" name="daily_average_consumption" value="0.00">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Kwh</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-4 mt-2" id="billing_month_row" style="display:none;">
                                                    <label for="" class="col-md-2 form-label-bold">Billing Month</label>
                                                    <div class="col-md-3 pl-5">
                                                        <select class="calculate_energy_rate" name="billing_month" id="billing_month" style="width: 104px; font-size: 12px; height: 27px;">
                                                            <option value="Jan">Jan</option>
                                                            <option value="Feb">Feb</option>
                                                            <option value="Mar">Mar</option>
                                                            <option value="Apr">Apr</option>
                                                            <option value="May">May</option>
                                                            <option value="Jun">Jun</option>
                                                            <option value="Jul">Jul</option>
                                                            <option value="Aug">Aug</option>
                                                            <option value="Sep">Sep</option>
                                                            <option value="Oct">Oct</option>
                                                            <option value="Nov">Nov</option>
                                                            <option value="Dec">Dec</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <!-- time of use model start -->
                                                <?php include 'time_of_use_popup.php'; ?>
                                                <!-- time of use model end -->

                                                <div class="bill-wrapper-tb mb-3 mt-4" id="billTypeFlat" style="display:none;">
                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col"></div>
                                                        <div class="bill-wrapper-tb-col without_meter_data"><strong>Charge</strong></div>
                                                        <div class="bill-wrapper-tb-col without_meter_data" style="width: 15.5rem;"><strong>Quantity</strong>
                                                            <select class="calculate_energy_rate" name="quantity_type" id="quantity_type" style="width: 104px; font-size: 12px; height: 27px;margin-left: 6px;">
                                                                <option value="12">Monthly</option>
                                                                <option value="1">Yearly</option>
                                                            </select>
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data"><strong>Energy Rate</strong></div>
                                                    </div>
                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col">
                                                            <strong>Rate</strong>
                                                        </div>
                                                        <div class="bill-wrapper-tb-col">
                                                            <input class="form-control calculate_energy_rate" type="text" id="flat_bill_type_rate" name="flat_bill_type_rate" value="0.000">
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data" style="width: 15.5rem;">
                                                            <input class="form-control calculate_energy_rate" type="text" id="flat_quantity" name="flat_quantity" value="0.000">
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data">
                                                            <input class="form-control" type="text" id="flat_energy_rate" name="flat_energy_rate" value="0.000" readonly="readonly">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="bill-wrapper-tb mb-3 mt-4" id="billTypeTimeOfUse">

                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col"></div>
                                                        <div class="bill-wrapper-tb-col without_meter_data simple_consumption"><strong>Kwh</strong></div>
                                                        <div class="bill-wrapper-tb-col"><strong>Rate ($)</strong></div>
                                                    </div>
                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col">
                                                            <strong>Peak</strong>
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data simple_consumption">
                                                            <input class="form-control" type="text" id="meter_data_peak_kwh" name="meter_data_peak_kwh" value="0.000">
                                                        </div>
                                                        <div class="bill-wrapper-tb-col">
                                                            <input class="form-control" type="text" id="meter_data_peak" name="meter_data_peak" value="0.000">
                                                        </div>
                                                    </div>
                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col">
                                                            <strong>Shoulder</strong>
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data simple_consumption">
                                                            <input class="form-control" type="text" id="meter_data_shoulder_kwh" name="meter_data_shoulder_kwh" value="0.000">
                                                        </div>
                                                        <div class="bill-wrapper-tb-col">
                                                            <input class="form-control" type="text" id="meter_data_shoulder" name="meter_data_shoulder" value="0.000">
                                                        </div>
                                                    </div>
                                                    <div class="bill-wrapper-tb-row">
                                                        <div class="bill-wrapper-tb-col">
                                                            <strong>Off Peak</strong>
                                                        </div>
                                                        <div class="bill-wrapper-tb-col without_meter_data simple_consumption">
                                                            <input class="form-control" type="text" id="meter_data_off_peak_kwh" name="meter_data_off_peak_kwh" value="0.000">
                                                        </div>
                                                        <div class="bill-wrapper-tb-col">
                                                            <input class="form-control" type="text" id="meter_data_off_peak" name="meter_data_off_peak" value="0.000">
                                                        </div>
                                                    </div>
                                                </div>

                                                <h1 class="card-title">Standard Electricity Charges</h1>

                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">Supply Charge<span class="text-danger">*</span></label>
                                                    <div class="col-md-5 pl-5">
                                                        <input class="form-control" type="text" id="meter_data_supply_charge" name="meter_data_supply_charge" value="">
                                                    </div>
                                                    <label for="" class="col-md-1 form-label-bold justify-content-end">Per</label>
                                                    <div class="col-md-2">
                                                        <select name="supply_per_charge" id="supply_per_charge" class="form-control" required="">
                                                            <option value="365">Daily</option>
                                                            <option value="12">Monthly</option>
                                                            <option value="4">Quarterly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">Meter Charge<span class="text-danger">*</span></label>
                                                    <div class="col-md-5 pl-5">
                                                        <input class="form-control" type="text" id="meter_data_meter_charge" name="meter_data_meter_charge" value="">
                                                    </div>
                                                    <label for="" class="col-md-1 form-label-bold justify-content-end">Per</label>
                                                    <div class="col-md-2">
                                                        <select name="meter_per_charge" id="meter_per_charge" class="form-control" required="">
                                                            <option value="365">Daily</option>
                                                            <option value="12">Monthly</option>
                                                            <option value="4">Quarterly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">Other Charge<span class="text-danger">*</span></label>
                                                    <div class="col-md-5 pl-5">
                                                        <input class="form-control" type="text" id="other_charge" name="other_charge" value="0">
                                                    </div>
                                                    <label for="" class="col-md-1 form-label-bold justify-content-end">Per</label>
                                                    <div class="col-md-2">
                                                        <select name="other_per_charge" id="other_per_charge" class="form-control" required="">
                                                            <option value="Kwh">Kwh</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-row mb-4 mt-5 demandChargeWrapper">
                                                    <label for="" class="col-md-2 form-label-bold">Demand Charge</label>
                                                    <div class="col-md-4 pl-5 d-inline-flex align-items-center">

                                                        <div class="btn-group btn-group-toggle ku-switch-toggle " data-toggle="buttons">
                                                            <label class="btn <?php if ($proposal_data['is_demand_charge'] == 1) {
                                                                                    echo "active";
                                                                                } ?>">
                                                                <input type="radio" class="demandChargeToggle" name="is_demandCharge" value="1" <?php if ($proposal_data['is_demand_charge'] == 1) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?>> Yes
                                                            </label>
                                                            <label class="btn <?php if ($proposal_data['is_demand_charge'] != 1) {
                                                                                    echo "active";
                                                                                } ?>">
                                                                <input type="radio" class="demandChargeToggle" name="is_demandCharge" value="0" <?php if ($proposal_data['is_demand_charge'] != 1) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?>> No
                                                            </label>
                                                        </div>
                                                        <!-- <input type="hidden" name="demandChargeToggle" id="demandChargeToggleValue" value="0"> -->

                                                        <div class="ku-link" id="addDemanCharge" <?php if ($proposal_data['is_demand_charge'] != 1) {
                                                                                                        echo 'style="display: none"';
                                                                                                    } ?>>
                                                            + Add More
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="demand-wrapper-tb mb-3 mt-4" id="demandChargeWrapper" style="display: none">
                                                </div>
                                                <a href="" id="demandRemoveRow" style="display:none">- Remove Row</a>

                                                <input type="hidden" name="demand_charge_value" id="demandChargeValue" value="1">

                                                <h1 class="card-title mt-5">Electricity Price Annual Increase<span class="text-danger">*</span></h1>

                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">First
                                                        <select class="calculate_energy_rate" id="meter_data_pia_year" name="meter_data_pia_year" id="quantity_type" style="width: 104px; font-size: 12px; height: 27px;margin-left: 6px;">
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3" selected>Three</option>
                                                            <option value="4">Four</option>
                                                            <option value="5">Five</option>
                                                        </select>
                                                        Years
                                                    </label>
                                                    <div class="col-md-6 pl-5">
                                                        <div class="input-group mb-3">
                                                            <input class="form-control" type="text" id="meter_data_pia_percentage" name="meter_data_pia_percentage" value="" aria-describedby="subsequent">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="subsequent">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">Subsequently<span class="text-danger">*</span></label>
                                                    <div class="col-md-6 pl-5">
                                                        <div class="input-group mb-3">
                                                            <input class="form-control" type="text" id="meter_data_subsequent" name="meter_data_subsequent" value="" aria-describedby="subsequent">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="subsequent">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h1 class="card-title mt-5">Export Electricity</h1>
                                                <div class="form-row mb-4 mt-5">
                                                    <label for="" class="col-md-2 form-label-bold">Feed-in Tariff Type</label>
                                                    <div class="col-md-6 pl-5 d-inline-flex align-items-center">

                                                        <div class="btn-group btn-group-toggle ku-switch-toggle" data-toggle="buttons">
                                                            <label class="btn active">
                                                                <input type="radio" name="options" id="yes"> Constant
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-row mb-4 mt-4">
                                                    <label for="" class="col-md-2 form-label-bold">Export Rate<span class="text-danger">*</span></label>
                                                    <div class="col-md-6 pl-5">
                                                        <div class="input-group mb-3">
                                                            <input class="form-control" type="text" name="utility_value_export" id="utility_value_export" value="" aria-describedby="subsequent">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="subsequent">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-6 col-sm-6 col-md-6">
                                        <div class="card">
                                            <div class="card-body" style="padding:0;">

                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">


                                                    <tr>
                                                        <td style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
                                                            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead1.png" alt="" style="display:inline-block" />
                                                        </td>
                                                        <td style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
                                                            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead2.png" alt="" style="display:inline-block" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="typical_export_graph"></div>
                                                        </td>
                                                        <td>
                                                            <div id="worst_export_graph"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:80px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
                                                            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead3.png" alt="" style="display:inline-block" />
                                                        </td>
                                                        <td style="padding:80px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">
                                                            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/--page-9-subhead4.png" alt="" style="display:inline-block" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="high_export_graph"></div>
                                                        </td>
                                                        <td>
                                                            <div id="best_export_graph"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <!-- <a type="button" class="losses_toggle_link ml-4" data-toggle="modal" data-target="#calculate_degration_model" id="calculate_degration_model">
                                    Set Peak/Off-Peak Times
                                </a> -->
                            </form>


                            <div id="meter_data_calculation"></div>
                            <!-- <div id="avg_load_area_chart"></div>
                            <div id="avg_load_area_chart1"></div>
                            <div id="avg_load_area_chart2"></div>
                            <div id="avg_load_area_chart3"></div>
                            <div id="avg_load_area_chart4"></div> -->

                        </div>
                        <div class="tab-pane fade" id="pills-inverter" role="tabpanel" aria-labelledby="pills-inverter-tab">

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="card">
                                        <div class="card-body">

                                            <ul class="nav nav-tabs justify-content-left" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#iv_tab_image_upload" id="tab_image_upload_btn" role="tab" data-toggle="tab">
                                                        Upload Image</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#iv_tab_mapping_tool" id="tab_mapping_tool_btn" role="tab" data-toggle="tab">Mapping Tool</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active show" id="iv_tab_image_upload">
                                                    <div class="form-block d-block clearfix">
                                                        <div class="form-block d-block clearfix" id="inverter_no_image">
                                                            <div class="add-picture">
                                                                <span>Add picture</span>
                                                                <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                                                                <input type="file" id="inverter_image_upload" />
                                                            </div>
                                                        </div>
                                                        <div class="form-block d-block clearfix" id="inverter_image" style="display:none !important; ">
                                                            <div class="add-picture"></div>
                                                            <a id="inverter_image_close" href="javascript:void(0);"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade in" id="iv_tab_mapping_tool">
                                                    <?php echo isset($inverter_design_tool) ? $inverter_design_tool : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- System Pricing -->
                        <!-- <div class="tab-pane fade" id="pills-system" role="tabpanel" aria-labelledby="pills-system-tab">

                            <div class="row">
                                <div class="col-6 col-sm-6 col-md-6">
                                    <div class="card bill-wrapper">
                                        <div class="card-body bill-wrapper-form">
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div> -->
                        <!-- System Pricing -->
                        <div class="tab-pane fade" id="pills-financial" role="tabpanel" aria-labelledby="pills-financial-tab">

                            <!-- FINANCIAL SUMMERY START -->
                            <div class="row">


                                <div class="col-6 col-sm-6 col-md-6">


                                    <div class="card bill-wrapper financial-summery-wrapper" style="min-height: 599px;">


                                        <!-- STC Dropdown -->
                                        <div class="card-body bill-wrapper-form">
                                            <div class="row justify-content-between">
                                                <div class="col-md-3">
                                                    <select class="form-control" name="" id="financial_selection">
                                                        <option value="STC" selected>STC</option>
                                                        <option value="LGC">LGC</option>
                                                        <option value="VEEC">VEEC</option>
                                                    </select>
                                                </div>
                                                <button class="btn bt-sm btn-primary mr-4" id="calculate_degration_factor_data" type="button" value="">VIEW CASHFLOW</button>
                                            </div>

                                        </div>
                                        <!-- STC Dropdown -->

                                        <!-- Financial STC -->
                                        <div class="card-body bill-wrapper-form" style="margin-top:-38px;" id="financial_stc">
                                            <h2 class="financial-summery-wrapper-h2">CAPEX</h2>
                                            <div class="form-block d-block clearfix">
                                                <form role="form" action="" method="post" name="solarPayments" id="solarPayments">

                                                    <div class="row hidden">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b>STC</b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[stc]" class="form-control" id="stc" readonly="" required="">
                                                                <input type="hidden" name="solar_proposal[stc_year]" class="form-control" id="stc_year" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Exc GST</label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Inc GST</label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">Total Price<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[total_payable_exGST]" class="form-control" id="total_payable_exGST" placeholder="Please Enter Total Payable exc. GST" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="total_payable_inGST" placeholder="Please Enter Total Payable inc. GST" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">STC Rebate</label>
                                                        </div>
                                                        <div class="col-md-5">

                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="solar_proposal[stc_rebate_value]" id="stc_rebate_value" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" placeholder="Please Enter STC rebate value" id="stc_rebate_value_inGST" value="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">Project Cost<span class="text-danger">*</span></label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[price_before_stc_rebate]" class="form-control" id="price_before_stc_rebate" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="price_before_stc_rebate_inGST" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-5">
                                                            <label for="monthly_payment_plan" class=" form-label-bold">Upfront Payment Plan</label>
                                                        </div>
                                                        <div class="col-md-2 d-none">
                                                            <select class="form-control mt-lg-2" name="proposal_finance[upfront_payment_options]" id="stc_upfront_payment_options">
                                                                <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>


                                                    <div class="row mt-4">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control stc_upfront_payment_deposit" id="stc_upfront_payment_opt_per_1" name="solar_financial[stc_upfront_payment_opt_per_1]" value="30" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                Deposit
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="stc_upfront_payment_opt_price_1" name="solar_financial[stc_upfront_payment_opt_price_1]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control stc_upfront_payment_deposit" id="stc_upfront_payment_opt_per_2" name="solar_financial[stc_upfront_payment_opt_per_2]" value="70" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Project completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="stc_upfront_payment_opt_price_2" name="solar_financial[stc_upfront_payment_opt_price_2]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2 d-none">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control stc_upfront_payment_deposit" id="stc_upfront_payment_opt_per_3" name="solar_financial[stc_upfront_payment_opt_per_3]" value="0" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Job Completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="stc_upfront_payment_opt_price_3" name="solar_financial[stc_upfront_payment_opt_price_3]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                Total Payable
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="stc_upfront_payment_opt_price_total" placeholder="Please enter price" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-4 d-none">
                                                        <label class="col-md-3 required-label">Sub Total (exc GST)</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Sub Total" name="" id="system_pricing_sub_total" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <!-- <div class="mt-5" id="solar_proposal_finance_container"> -->
                                            <h2 class="financial-summery-wrapper-h2 mt-5">FINANCE</h2>
                                            <div class="form-block d-block clearfix mt-5">
                                                <form role="form" action="" method="post" id="proposal_finance_form">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <input type="hidden" name="proposal_finance[proposal_type]" value="2" />
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control proposal_finance_term" name="proposal_finance[term]" id="stc_term" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                $term_months = unserialize(TERM_MONTHS);
                                                                foreach ($term_months as $key => $value) {
                                                                ?>
                                                                    <?php if ($key == 0) { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="monthly_payment_plan" class="form-label-bold">Monthly Repayments<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="proposal_finance[monthly_payment_plan]" class="form-control" id="stc_monthly_payment_plan" required="" readonly="true" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div style="display:flex; margin-top:11px; align-items: center;">
                                                                <input type="checkbox" style="margin-right: 8px;" class="mr_manual_entry" value="1" name=""> Enter Manually
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-3" id="finance_loader"></div>
                                                        <div class="btn-block mt-5 text-center">
                                                            <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary d-none">
                                                            <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden d-none">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <form role="form" action="" method="post" name="solarFinancial" id="solarFinancial">
                                                <h2 class="financial-summery-wrapper-h2 d-flex justify-content-between align-items-center">
                                                    <span>Power Purchase Agreement (PPA)</span>
                                                    <div class="d-flex align-items-center" style="font-weight: normal; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.428571429; color: #333333;">
                                                        Show in Proposal
                                                        <input type="checkbox" style="margin-left:8px;" class="ppa_checkbox" value="1" name="proposal_finance[ppa_checkbox]" checked="true">
                                                    </div>
                                                </h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <input type="hidden" name="proposal_finance[proposal_type]" value="2" />
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="solar_financial[ppa_term]" id="ppa_term" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                for ($ppa_term = 7; $ppa_term <= 30; $ppa_term++) {
                                                                ?>
                                                                    <option value="<?php echo $ppa_term * 12; ?>" <?php if ($ppa_term == 15) {
                                                                                                                        echo 'selected';
                                                                                                                    } ?>><?php echo $ppa_term; ?> Years</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">PPA Rate (¢/kWh)<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b style="font-size:15px;">¢</b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[ppa_rate]" class="form-control" id="ppa_rate" required="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Financial STC -->


                                        <!-- Financial LGC -->
                                        <div class="card-body bill-wrapper-form" style="margin-top:-37px;" id="financial_lgc">
                                            <h2 class="financial-summery-wrapper-h2">CAPEX</h2>
                                            <div class="form-block d-block clearfix">
                                                <form role="form" action="" method="post" name="solarPayments2" id="solarPayments2">

                                                    <div class="row hidden">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b>STC</b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[stc]" class="form-control" id="stc2" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Exc GST</label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Inc GST</label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">Project Cost<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[price_before_stc_rebate]" class="form-control" id="price_before_stc_rebate2" placeholder="Please Enter Project Cost" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="price_before_stc_rebate_inGST2" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-4 mt-lg-4">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">LGC Pricing<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <table class="table table-bordered lgc-pricing-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Year</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>4</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>5</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>6</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>7</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>8</td>
                                                                        <td><input type="number" name="solar_financial[lgc_pricing][]" min="0" step="0.01" oninput="validity.valid||(value='');" required="" class="lgc_pricing" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-2">
                                                        <div class="col-md-5">
                                                            <label for="monthly_payment_plan2" class=" form-label-bold">Upfront Payment Plan</label>
                                                        </div>
                                                        <div class="col-md-2 d-none">
                                                            <select class="form-control" name="solar_financial[upfront_payment_options]" id="upfront_payment_options2">
                                                                <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="row mt-4">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control lgc_upfront_payment_deposit" id="lgc_upfront_payment_opt_per_1" name="solar_financial[lgc_upfront_payment_opt_per_1]" value="30" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                Deposit
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="lgc_upfront_payment_opt_price_1" name="solar_financial[lgc_upfront_payment_opt_price_1]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control lgc_upfront_payment_deposit" id="lgc_upfront_payment_opt_per_2" name="solar_financial[lgc_upfront_payment_opt_per_2]" value="70" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Project completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="lgc_upfront_payment_opt_price_2" name="solar_financial[lgc_upfront_payment_opt_price_2]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2 d-none">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control lgc_upfront_payment_deposit" id="lgc_upfront_payment_opt_per_3" name="solar_financial[lgc_upfront_payment_opt_per_3]" value="0" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Job Completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="lgc_upfront_payment_opt_price_3" name="solar_financial[lgc_upfront_payment_opt_price_3]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                Total Payable
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="lgc_upfront_payment_opt_price_total" placeholder="Please enter price" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>





                                                    <div class="row mb-4 d-none">
                                                        <label class="col-md-3 required-label">Sub Total (exc GST)</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Sub Total" name="" id="system_pricing_sub_total2" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- <div class="mt-5" id="solar_proposal_finance_container"> -->
                                            <h2 class="financial-summery-wrapper-h2 mt-5">FINANCE</h2>
                                            <div class="form-block d-block clearfix mt-5">
                                                <form role="form" action="" method="post" id="proposal_finance_form2">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <input type="hidden" name="proposal_finance[proposal_type]" value="2" />
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="proposal_finance[term]" id="lgc_term" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                $term_months = unserialize(TERM_MONTHS);
                                                                foreach ($term_months as $key => $value) {
                                                                ?>
                                                                    <?php if ($key == 0) { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="monthly_payment_plan" class="form-label-bold">Monthly Repayments<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="proposal_finance[monthly_payment_plan]" class="form-control" id="lgc_monthly_payment_plan" required="" readonly="true" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div style="display:flex; margin-top:11px; align-items: center;">
                                                                <input type="checkbox" style="margin-right: 8px;" class="mr_manual_entry2" value="1" name=""> Enter Manually
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-3" id="finance_loader"></div>
                                                        <div class="btn-block mt-5 text-center">
                                                            <!-- <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary d-none">
                                                            <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden d-none"> -->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>


                                            <form role="form" action="" method="post" name="solarFinancial2" id="solarFinancial2">
                                                <h2 class="financial-summery-wrapper-h2 d-flex justify-content-between align-items-center">
                                                    <span>Power Purchase Agreement (PPA)</span>
                                                    <div class="d-flex align-items-center" style="font-weight: normal; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.428571429; color: #333333;">
                                                        Show in Proposal
                                                        <input type="checkbox" style="margin-left:8px;" class="ppa_checkbox" value="2" name="proposal_finance[ppa_checkbox]" checked="true">
                                                    </div>
                                                </h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="solar_financial[ppa_term]" id="lgc_ppa_term" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                for ($lgc_ppa_term = 7; $lgc_ppa_term <= 30; $lgc_ppa_term++) {
                                                                ?>
                                                                    <option value="<?php echo $lgc_ppa_term * 12; ?>" <?php if ($lgc_ppa_term == 15) {
                                                                                                                            echo 'selected';
                                                                                                                        } ?>><?php echo $lgc_ppa_term; ?> Years</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold" name="">PPA Rate<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[ppa_rate]" class="form-control" id="lgc_ppa_rate" required="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Financial LGC -->

                                        <!-- Financial VEEC -->
                                        <div class="card-body bill-wrapper-form" id="financial_veec">
                                            <div class="d-none">
                                                <h1 class="financial-summery-wrapper-h1">
                                                    <span>UPFRONT DISCOUNT</span>
                                                    <input type="checkbox" aria-label="Checkbox for following text input">
                                                </h1>
                                                <h2 class="financial-summery-wrapper-h2">CAPEX</h2>
                                                <div class="form-block d-block clearfix">
                                                    <form role="form" action="" method="post" name="solarPayments3" id="solarPayments3">

                                                        <div class="row hidden">
                                                            <div class="col-12">
                                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b>STC</b></span>
                                                                    </div>
                                                                    <input type="number" name="solar_proposal3[stc]" class="form-control" id="stc3" readonly="" required="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">

                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="" class="form-label-bold">Exc GST</label>

                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="" class="form-label-bold">Inc GST</label>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="" class="form-label-bold">Total Price</label>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" name="solar_proposal3[total_payable_exGST]" class="form-control" id="total_payable_exGST3" placeholder="Please Enter Total Payable exc. GST" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="total_payable_inGST3" placeholder="Please Enter Total Payable inc. GST" required="" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="" class="form-label-bold">VEEC Discount</label>
                                                            </div>
                                                            <div class="col-md-5">

                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="solar_proposal3[stc_rebate_value]" id="stc_rebate_value3" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" class="form-control" placeholder="Please Enter STC rebate value" id="stc_rebate_value_inGST3" value="" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="" class="form-label-bold">Project Cost</label>

                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" name="solar_proposal3[price_before_stc_rebate]" class="form-control" id="price_before_stc_rebate3" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="price_before_stc_rebate_inGST3" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row mb-4 d-none">
                                                            <label class="col-md-3 required-label">Sub Total (exc GST)</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Sub Total" name="" id="system_pricing_sub_total3" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <!-- <div class="mt-5" id="solar_proposal_finance_container"> -->
                                                <h2 class="financial-summery-wrapper-h2 mt-5">FINANCE</h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <form role="form" action="" method="post" id="proposal_finance_form3">

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="term" class="form-label-bold">Term (Months)</label>
                                                                <input type="hidden" name="proposal_finance3[proposal_type]" value="2" />
                                                            </div>
                                                            <div class="col-md-5">
                                                                <select class="form-control" name="proposal_finance3[term]" id="term" required="">
                                                                    <option value="">Select one</option>
                                                                    <?php
                                                                    $term_months = unserialize(TERM_MONTHS);
                                                                    foreach ($term_months as $key => $value) {
                                                                    ?>
                                                                        <?php if ($key == 0) { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                        <?php } else { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5"></div>
                                                        </div>

                                                        <div class="row mt-3">
                                                            <div class="col-md-2">
                                                                <label for="monthly_payment_plan" class="form-label-bold">Monthly Repayments</label>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" name="proposal_finance3[monthly_payment_plan]" class="form-control" id="monthly_payment_plan3" required="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div style="display:flex; margin-top:11px; align-items: center;">
                                                                    <input type="checkbox" style="margin-right: 8px;" class="mr_manual_entry3" value="1" name=""> Enter Manually
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="monthly_payment_plan" class=" form-label-bold">Upfront Payment Plan</label>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <select class="form-control" name="proposal_finance3[upfront_payment_options]" id="upfront_payment_options3">
                                                                    <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                    <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                    <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5"></div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-4 mb-3" id="finance_loader3"></div>
                                                            <div class="btn-block mt-5 text-center">
                                                                <!-- <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary d-none">
                                                            <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden d-none"> -->
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                                <h2 class="financial-summery-wrapper-h2">PPA</h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <form action="">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="term" class="form-label-bold">Term (Months)</label>
                                                                <input type="hidden" name="proposal_finance3[proposal_type]" value="2" />
                                                            </div>
                                                            <div class="col-md-5">
                                                                <select class="form-control" name="proposal_finance3[term]" id="term" required="">
                                                                    <option value="">Select one</option>
                                                                    <?php
                                                                    $term_months = unserialize(TERM_MONTHS);
                                                                    foreach ($term_months as $key => $value) {
                                                                    ?>
                                                                        <?php if ($key == 0) { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                        <?php } else { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5"></div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="col-md-2">
                                                                <label for="term" class="form-label-bold">PPA Rate</label>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="input-group" style="min-width: 100%!important;">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                    </div>
                                                                    <input type="number" name="proposal_finance3[monthly_payment_plan]" class="form-control" id="monthly_payment_plan3" required="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5"></div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <!-- <hr class="financial-summery-hr" />
                                                <h4 class="financial-summery-wrapper-h4">KEY ASSUMPTION</h4>
                                                <ol>
                                                    <li>1. Customer provides daily production data</li>
                                                    <li>2. 0.9546 Emissions Factor</li>
                                                    <li>3. VEEC price of $xxx</li>
                                                    <li>4. 4% export</li>
                                                    <li>5. System annual generation</li>
                                                </ol> -->
                                            </div>

                                            <form role="form" action="" method="post" name="solarPayment_veec_2" id="solarPayment_veec_2">
                                                <h1 class="financial-summery-wrapper-h1 d-flex justify-content-between align-items-center" style="margin-top:-20px;">
                                                    <span>UPFRONT DISCOUNT</span>
                                                    <input type="checkbox" class="veec_type" value="1" name="veec_type[]" checked="true">
                                                </h1>
                                                <h2 class="financial-summery-wrapper-h2">CAPEX</h2>
                                                <div class="form-block d-block clearfix">

                                                    <div class="row hidden">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b>STC</b></span>
                                                                </div>
                                                                <!-- <input type="number" name="solar_proposal4[stc]" class="form-control" id="stc4" readonly="" required=""> -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Exc GST</label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Inc GST</label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">Project Cost<span class="text-danger">*</span></label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[project_cost_2]" class="form-control" id="project_cost_2" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="project_cost_gst_2" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-4">
                                                        <div class="col-md-5">
                                                            <label for="monthly_payment_plan" class=" form-label-bold">Upfront Payment Plan</label>
                                                        </div>
                                                        <div class="col-md-2 d-none">
                                                            <select class="form-control" name="solar_financial[upfront_payment_options_2]" id="upfront_payment_options_2">
                                                                <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>


                                                    <div class="row mt-4">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_2" id="veec_upfront_payment_opt_per_2_1" name="solar_financial[veec_upfront_payment_opt_per_2_1]" value="30" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                Deposit
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_2_1" name="solar_financial[veec_upfront_payment_opt_price_2_1]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_2" id="veec_upfront_payment_opt_per_2_2" name="solar_financial[veec_upfront_payment_opt_per_2_2]" value="70" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Project completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_2_2" name="solar_financial[veec_upfront_payment_opt_price_2_2]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2 d-none">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_2" id="veec_upfront_payment_opt_per_2_3" name="solar_financial[veec_upfront_payment_opt_per_2_3]" value="0" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Job Completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_2_3" name="solar_financial[veec_upfront_payment_opt_price_2_3]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                Total Payable
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_total_2" placeholder="Please enter price" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row mb-4 d-none">
                                                        <label class="col-md-3 required-label">Sub Total (exc GST)</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Sub Total" name="" id="system_pricing_sub_total4" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                            <!-- <div class="mt-5" id="solar_proposal_finance_container"> -->
                                            <h2 class="financial-summery-wrapper-h2 mt-5">FINANCE</h2>
                                            <div class="form-block d-block clearfix mt-5">
                                                <form role="form" action="" method="post" id="proposal_finance_veec_2">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <!-- <input type="hidden" name="proposal_finance[proposal_type]" value="2" /> -->
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="proposal_finance[term_2]" id="term_veec_2" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                $term_months = unserialize(TERM_MONTHS);
                                                                foreach ($term_months as $key => $value) {
                                                                ?>
                                                                    <?php if ($key == 0) { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="monthly_payment_plan" class="form-label-bold">Monthly Repayments<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="proposal_finance[monthly_payment_plan_2]" class="form-control" id="monthly_payment_plan_2" required="" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div style="display:flex; margin-top:11px; align-items: center;">
                                                                <input type="checkbox" style="margin-right: 8px;" class="mr_manual_entry4" value="1" name=""> Enter Manually
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-3" id="finance_loader4"></div>
                                                        <div class="btn-block mt-5 text-center">
                                                            <!-- <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary d-none">
                                                            <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden d-none"> -->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>


                                            <form role="form" action="" method="post" name="solarFinancial_veec_2" id="solarFinancial_veec_2">
                                                <h2 class="financial-summery-wrapper-h2 d-flex justify-content-between align-items-center">
                                                    <span>Power Purchase Agreement (PPA)</span>
                                                    <div class="d-flex align-items-center" style="font-weight: normal; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.428571429; color: #333333;">
                                                        Show in Proposal
                                                        <input type="checkbox" style="margin-left:8px;" class="ppa_checkbox" value="3" name="proposal_finance[ppa_checkbox_2]" checked="true">
                                                    </div>
                                                </h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <!-- <input type="hidden" name="solar_financial[ppa_term_2]" value="2" /> -->
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="solar_financial[ppa_term_2]" id="ppa_term_2" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                for ($ppa_term_2 = 7; $ppa_term_2 <= 30; $ppa_term_2++) {
                                                                ?>

                                                                    <option value="<?php echo $ppa_term_2 * 12; ?>" <?php if ($ppa_term_2 == 15) {
                                                                                                                        echo 'selected';
                                                                                                                    } ?>><?php echo $ppa_term_2; ?> Years</option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">PPA Rate<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[ppa_rate_2]" class="form-control" id="ppa_rate_2" required="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                </div>
                                            </form>

                                            <!-- <hr class="financial-summery-hr" />
                                            <h4 class="financial-summery-wrapper-h4">KEY ASSUMPTION</h4>
                                            <ol>
                                                <li>1. Customer provides daily production data</li>
                                                <li>4. 4% export</li>
                                                <li>5. System annual generation</li>
                                            </ol> -->

                                            <form role="form" action="" method="post" name="solarPayment_veec_3" id="solarPayment_veec_3">
                                                <h1 class="financial-summery-wrapper-h1 mt-5 d-flex justify-content-between align-items-center">
                                                    <span>CLIENT REBATE</span>
                                                    <input type="checkbox" class="veec_type" value="2" name="veec_type[]" checked="true">
                                                </h1>
                                                <h2 class="financial-summery-wrapper-h2">CAPEX</h2>
                                                <div class="form-block d-block clearfix">

                                                    <div class="row hidden">
                                                        <div class="col-12">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b>STC</b></span>
                                                                </div>
                                                                <!-- <input type="number" name="solar_proposal5[stc]" class="form-control" id="stc5" readonly="" required=""> -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Exc GST</label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <label for="" class="form-label-bold">Inc GST</label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">Project Cost<span class="text-danger">*</span></label>

                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[project_cost_3]" class="form-control" id="project_cost_3" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="project_cost_gst_3" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-lg-3">
                                                        <div class="col-md-2">
                                                            <label for="" class="form-label-bold">VEEC Discount (Estimate)<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">

                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="solar_financial[veec_discount]" id="veec_discount" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" placeholder="Please Enter STC rebate value" id="veec_discount_gst" value="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-5">
                                                            <label for="monthly_payment_plan" class=" form-label-bold">Upfront Payment Plan</label>
                                                        </div>
                                                        <div class="col-md-2 d-none">
                                                            <select class="form-control" name="proposal_finance[upfront_payment_options_3]" id="upfront_payment_options_3">
                                                                <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>




                                                    <div class="row mt-4">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_3" id="veec_upfront_payment_opt_per_3_1" name="solar_financial[veec_upfront_payment_opt_per_3_1]" value="30" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                Deposit
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_3_1" name="solar_financial[veec_upfront_payment_opt_price_3_1]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_3" id="veec_upfront_payment_opt_per_3_2" name="solar_financial[veec_upfront_payment_opt_per_3_2]" value="70" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Project completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width:100%!important">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_3_2" name="solar_financial[veec_upfront_payment_opt_price_3_2]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2 d-none">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                <div class="input-group" style="width:80px!important; margin-right:5px;">
                                                                    <input type="number" class="form-control veec_upfront_payment_deposit_3" id="veec_upfront_payment_opt_per_3_3" name="solar_financial[veec_upfront_payment_opt_per_3_3]" value="10" required="">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><b><i class="fa fa-percent"></i></b></span>
                                                                    </div>
                                                                </div>
                                                                on Job Completion
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_3_3" name="solar_financial[veec_upfront_payment_opt_price_3_3]" placeholder="Please enter price" required="" readonly="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-md-6">
                                                            <label class=" form-label-bold d-flex align-items-center">
                                                                Total Payable
                                                            </label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="veec_upfront_payment_opt_price_total_3" placeholder="Please enter price" readonly="" required="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row mb-4 d-none">
                                                        <label class="col-md-3 required-label">Sub Total (exc GST)</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Sub Total" name="" id="system_pricing_sub_total5" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>

                                            <!-- <div class="mt-5" id="solar_proposal_finance_container"> -->
                                            <h2 class="financial-summery-wrapper-h2 mt-5">FINANCE</h2>
                                            <div class="form-block d-block clearfix mt-5">
                                                <form role="form" action="" method="post" id="proposal_finance_veec_3">

                                                    <div class="row mb-3">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <!-- <input type="hidden" name="proposal_finance[term_3]" value="2" /> -->
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="proposal_finance[term_3]" id="term_veec_3" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                $term_months = unserialize(TERM_MONTHS);
                                                                foreach ($term_months as $key => $value) {
                                                                ?>
                                                                    <?php if ($key == 0) { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>

                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="monthly_payment_plan" class="form-label-bold">Monthly Repayments<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="proposal_finance[monthly_payment_plan_3]" class="form-control" id="monthly_payment_plan_3" required="" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div style="display:flex; margin-top:11px; align-items: center;">
                                                                <input type="checkbox" style="margin-right: 8px;" class="mr_manual_entry5" value="1" name=""> Enter Manually
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-3" id="finance_loader6"></div>
                                                        <div class="btn-block mt-5 text-center">
                                                            <!-- <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary d-none">
                                                            <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden d-none"> -->
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>



                                            <form role="form" action="" method="post" name="solarFinancial_veec_3" id="solarFinancial_veec_3">
                                                <h2 class="financial-summery-wrapper-h2 d-flex justify-content-between align-items-center">
                                                    <span>Power Purchase Agreement (PPA)</span>
                                                    <div class="d-flex align-items-center" style="font-weight: normal; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.428571429; color: #333333;">
                                                        Show in Proposal
                                                        <input type="checkbox" style="margin-left:8px;" class="ppa_checkbox" value="4" name="proposal_finance[ppa_checkbox_3]" checked="true">
                                                    </div>
                                                </h2>
                                                <div class="form-block d-block clearfix mt-5">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">Term (Months)<span class="text-danger">*</span></label>
                                                            <!-- <input type="hidden" name="solar_financial[ppa_term_3]" value="2" /> -->
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select class="form-control" name="solar_financial[ppa_term_3]" id="ppa_term_3" required="">
                                                                <option value="">Select one</option>
                                                                <?php
                                                                for ($ppa_term_3 = 7; $ppa_term_3 <= 30; $ppa_term_3++) {
                                                                ?>
                                                                    <option value="<?php echo $ppa_term_3 * 12; ?>" <?php if ($ppa_term_3 == 15) {
                                                                                                                        echo 'selected';
                                                                                                                    } ?>><?php echo $ppa_term_3; ?> Years</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                    <div class="row mt-3">
                                                        <div class="col-md-2">
                                                            <label for="term" class="form-label-bold">PPA Rate<span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="input-group" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_financial[ppa_rate_3]" class="form-control" id="ppa_rate_3" required="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                </div>
                                            </form>

                                            <!-- <hr class="financial-summery-hr" />
                                            <h4 class="financial-summery-wrapper-h4">KEY ASSUMPTION</h4>
                                            <ol>
                                                <li>1. Customer provides daily production data</li>
                                                <li>2. 0.9546 Emissions Factor</li>
                                                <li>3. VEEC price of $xxx</li>
                                                <li>4. 4% export</li>
                                                <li>5. System annual generation</li>
                                            </ol> -->


                                        </div>
                                        <!-- Financial VEEC -->

                                    </div>
                                </div>
                            </div>
                            <!-- FINANCIAL SUMMERY END -->

                        </div>
                    </div>


                    <div class="btn-block mt-2">
                        <input type="button" id="save_proposal_btn" value="Save" class="btn btn-primary custom-btn custom-btn-green">
                        <input type="button" id="view_proposal_action" value="View Proposal" class="btn btn-primary custom-btn custom-btn-gray" style="display:none;">
                        <!-- <button class="btn btn-primary"></button> -->
                        <!-- <a href="">View Proposal</a> -->
                        <!-- <input type="button" id="send_proposal_btn" value="Send Proposal" class="btn btn-primary custom-btn custom-btn-blue"> -->
                    </div>
                </div>


            </div>
            <!-- page wrapper inner -->
        </div>
    </div>









    <div class="row" style="display:none">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="form-block d-block clearfix">
                <?php
                $message = $this->session->flashdata('message');
                echo (isset($message) && $message != NULL) ? $message : '';
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Customer Details</h2>
                                            <a style="margin-left:20px;" id="customer_location_edit_btn" href="javascript:void(0);"><i class="fa fa-pencil"></i> Edit Customer</a>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="form-group" style="margin-bottom:10px;">
                                                <label>Site Name</label>
                                                <input type="text" id="site_name" placeholder="Site Name" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group" style="margin-bottom:10px;">
                                                <label>Site Address</label>
                                                <input type="text" id="site_address" placeholder="Address" class="form-control gray-bg" readonly="">
                                            </div>
                                            <div class="form-group" style="margin-bottom:10px;">
                                                <label>Customer Name</label>
                                                <input type="text" id="customer_name" placeholder="Customer Name" class="form-control gray-bg" readonly>
                                            </div>
                                            <div class="form-group" style="margin-bottom:10px;">
                                                <label>Customer Contact Number</label>
                                                <input type="tel" id="customer_contact_no" placeholder="Cusotmer Contact No." class="form-control gray-bg" readonly>
                                            </div>
                                            <div class="form-group" style="margin-bottom:10px;">
                                                <label>Postcode</label>
                                                <input type="number" id="site_postcode" placeholder="Postcode" class="form-control gray-bg">
                                                <small class="form-text text-danger" id="site_postcode_warning"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Proposal Details</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="table-default tbl-as-inpt">
                                                <table width="100%" border="0">
                                                    <tbody id="proposal_cost_details_wrapper">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="btn-block text-center">
                                            <button id="manage_finance_proposal_btn" class="btn btn-primary hidden"><i class="fa fa-money"></i> Manage Finance</button>
                                            <a id="download_solar_proposal" href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Download Proposal</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4">
                                <div class="card card_row_1">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Financial Summary</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <div class="table-default  tbl-as-inpt">
                                                <table width="100%" border="0">
                                                    <tbody id="proposal_financial_summery">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6">

                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="card" style="min-height: 545px;">
                            <div class="card-body ">
                                <div class="card_body__title">
                                    <h2>Add Product</h2>
                                </div>
                                <div class="form-block d-block clearfix">
                                    <form role="form" action="#" method="post" id="solar_products_form">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label>Product Type</label>
                                                    <select class="form-control" id="type_id" required>
                                                        <option value="">Select one</option>
                                                        <?php
                                                        if (!empty($product_types)) {
                                                            foreach ($product_types as $key => $value) {
                                                        ?>
                                                                <option value="<?php echo $value['type_id']; ?>"><?php echo $value['type_name']; ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label>Item</label>
                                                    <select class="form-control" name="solar_product[prd_id]" name="prd_id" id="prd_id" required>
                                                        <option value="">Please Select Product Type</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group control-group">
                                                    <label class="required-label">Qty</label>
                                                    <input class="form-control" placeholder="Please Enter Qty" type="number" name="solar_product[prd_qty]" id="prd_qty" min="1" oninput="validity.valid||(value='');" value="" required="">

                                                </div>
                                            </div>
                                        </div>

                                        <input type="button" id="add_product_btn" value="Add Product" class="btn btn-primary">
                                        <input type="button" id="update_product_btn" value="Update Product" class="btn btn-primary hidden">
                                        <input type="button" id="cancel_product_btn" value="Cancel" class="btn btn-primary hidden">
                                        <input type="hidden" id="solar_product_id" name="solar_product_id">
                                    </form>
                                </div>

                                <div class="form-block d-block clearfix mt-5">
                                    <div class="table-responsive">
                                        <table width="100%" border="0" class="table table-striped" id="solar_proposal_products_table">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Type</th>
                                                    <th>Qty</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="solar_proposal_products_table_body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-row mt-5">
                                    <div class="col-sm-12">
                                        <label for="feed_in_tariff">Additional Notes</label>
                                        <textarea row="3" name="solar_proposal[additional_notes]" onkeyup="document.getElementById('additional_notes').value = this.value" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6">
                                <form role="form" action="" method="post" name="solarSystemSummary" id="solarSystemSummary">
                                    <div class="card" style="min-height: 560px;">
                                        <div class="card-body row">
                                            <div class="col-sm-12 mb-5">
                                                <div class="card_body__title">
                                                    <h2>Panel setup via</h2>
                                                </div>
                                                <span class="sr-switch">
                                                    <label for="nearmap_toggle">Uploading Image</label>
                                                    <input type="checkbox" class="sr-switch" id="nearmap_toggle" />
                                                    <label for="nearmap_toggle">Using Near Maps on CRM</label>
                                                </span>
                                            </div>
                                            <div class="col-md-6" id="manual-panel-stats">
                                                <div class="card_body__title">
                                                    <h2>Panel Setup</h2>
                                                </div>
                                                <div class="form-block d-block clearfix">
                                                    <!-- <div class="form-group hidden">
                                                        <label for="total_system_size">Panel Wattage </label>
                                                        <input class="form-control" placeholder="Please Enter Total System Size/Array Power Rating (recommended)" type="number" name="solar_proposal[total_system_size]" id="total_system_size" value="">
                                                    </div> -->
                                                    <!-- 
                                                    <div class="form-group hidden">
                                                        <label for="total_panel_area">Total Panel Area</label>
                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                            <input type="number" name="solar_proposal[total_panel_area]" placeholder="Please Enter Total Panel Area" min="1" oninput="validity.valid||(value='');" value="" class="form-control" id="total_panel_area" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><b>m<sup>2</sup></b></span>
                                                            </div>
                                                        </div>
                                                    </div> -->

                                                    <div style="font-size:18px; border-bottom:1px solid black; margin-bottom:5px;">Panel Details - Area1</div>
                                                    <div class="form-group">
                                                        <label>Number of Panels</label>
                                                        <input class="form-control no_of_panels" placeholder="Please Enter Number of Panels" id="js-val-nop" type="number" name="solar_panel_data[no_of_panels][0]" value="" min="1" oninput="validity.valid||(value='');" required="">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="required-label">Panel Orientation</label>
                                                        <select name="solar_panel_data[panel_orientation][0]" class="form-control" required="">
                                                            <option value="0" selected="">0</option>
                                                            <option value="10">10</option>
                                                            <option value="20">20</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                            <option value="80">80</option>
                                                            <option value="90">90</option>
                                                            <option value="100">100</option>
                                                            <option value="110">110</option>
                                                            <option value="120">120</option>
                                                            <option value="130">130</option>
                                                            <option value="140">140</option>
                                                            <option value="150">150</option>
                                                            <option value="160">160</option>
                                                            <option value="170">170</option>
                                                            <option value="180">180</option>
                                                            <option value="190">190</option>
                                                            <option value="200">200</option>
                                                            <option value="210">210</option>
                                                            <option value="220">220</option>
                                                            <option value="230">230</option>
                                                            <option value="240">240</option>
                                                            <option value="250">250</option>
                                                            <option value="260">260</option>
                                                            <option value="270">270</option>
                                                            <option value="280">280</option>
                                                            <option value="290">290</option>
                                                            <option value="300">300</option>
                                                            <option value="310">310</option>
                                                            <option value="320">320</option>
                                                            <option value="330">330</option>
                                                            <option value="340">340</option>
                                                            <option value="350">350</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">Panel Tilt</label>
                                                        <select name="solar_panel_data[panel_tilt][0]" class="form-control" required="">
                                                            <option value="0" selected="">0</option>
                                                            <option value="10">10</option>
                                                            <option value="20">20</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                            <option value="80">80</option>
                                                            <option value="90">90</option>
                                                        </select>
                                                    </div>
                                                    <div id="panels_area_wrapper">
                                                    </div>
                                                    <div class="buttwrap">
                                                        <button id="add_panel_area" type="button" class="btn btn-primary">Add Panel Area </button>
                                                        <input type="button" id="save_proposal_additional_btn" value="Update" class="btn btn-primary pull-right">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- solar panel manual tilt and rotation end -->
                                            <div class="col-md-6" id="inclusion-stats">
                                                <div class="card_body__title">
                                                    <h2>Inclusions / Exclusions</h2>
                                                </div>
                                                <div class="form-block d-block clearfix">
                                                    <div class="form-group">
                                                        <label class="required-label">Tilt System</label>
                                                        <select name="solar_proposal[additional_items][tiltSystem]" id="tiltSystem" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">Additional Switchboard</label>
                                                        <select name="solar_proposal[additional_items][switchBoard]" id="switchBoard" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">Grid Protection Equipment</label>
                                                        <select name="solar_proposal[additional_items][gridProtectionEquipment]" id="gridProtectionEquipment" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">Engineering Report</label>
                                                        <select name="solar_proposal[additional_items][engineeringReport]" id="engineeringReport" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">Network Report</label>
                                                        <select name="solar_proposal[additional_items][networkReport]" id="networkReport" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="required-label">TV Installation</label>
                                                        <select name="solar_proposal[additional_items][TVInstallation]" id="TVInstallation" class="form-control additional_items" required="">
                                                            <option value="0">Not Applicable</option>
                                                            <option value="1">Included</option>
                                                            <option value="2">Not Included</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6">
                                <!-- <div class="card" style="min-height: 599px;">
                                    <div class="card-body">
                                        <div class="card_body__title">
                                            <h2>Payment Summary</h2>
                                        </div>
                                        <div class="form-block d-block clearfix">
                                            <form role="form" action="" method="post" name="solarPayments" id="solarPayments">

                                                <div class="row hidden">
                                                    <div class="col-12">
                                                        <div class="input-group mb-3" style="min-width: 100%!important;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><b>STC</b></span>
                                                            </div>
                                                            <input type="number" name="solar_proposal[stc]" class="form-control" id="stc" readonly="" required="">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label>Total Project Cost exc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[total_payable_exGST]" class="form-control" id="total_payable_exGST" placeholder="Please Enter Total Payable exc. GST" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label>Total Project Cost inc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="total_payable_inGST" placeholder="Please Enter Total Payable inc. GST" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label class="required-label">STC Rebate exc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input style=" cursor: pointer;" type="number" class="form-control" placeholder="Please Enter STC rebate value" name="solar_proposal[stc_rebate_value]" id="stc_rebate_value" value="" min="0" step="0.01" oninput="validity.valid||(value='');" required="" readonly="">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label class="required-label">STC Rebate inc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" placeholder="Please Enter STC rebate value" id="stc_rebate_value_inGST" value="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label>Net Payable exc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" name="solar_proposal[price_before_stc_rebate]" class="form-control" id="price_before_stc_rebate" placeholder="Please Enter Price Before STC Rebate" min="0" step="0.01" oninput="validity.valid||(value='');" required="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label>Net Payable inc GST</label>
                                                            <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                </div>
                                                                <input type="number" class="form-control" id="price_before_stc_rebate_inGST" placeholder="Please Enter Price Before STC Rebate" required="" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="card" id="solar_proposal_finance_container">
                                            <div class="">
                                                <div class="card_body__title">
                                                    <h2>Finance</h2>
                                                </div>
                                                <div class="form-block d-block clearfix">
                                                    <form role="form" action="" method="post" id="proposal_finance_form">
                                                        <div class="form-row">
                                                            <input type="hidden" name="proposal_finance[proposal_type]" value="2" />
                                                            <div class="col-md-6 mb-3">
                                                                <label for="term">Term (Months)</label>
                                                                <select class="form-control" name="proposal_finance[term]" id="term" required="">
                                                                    <option value="">Select one</option>
                                                                    <?php
                                                                    $term_months = unserialize(TERM_MONTHS);
                                                                    foreach ($term_months as $key => $value) {
                                                                    ?>
                                                                        <?php if ($key == 0) { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Year</option>
                                                                        <?php } else { ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo $value / 12; ?> Years</option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <div class="form-group">
                                                                    <label for="monthly_payment_plan">Monthly Payment Plan</label>
                                                                    <div class="input-group mb-3" style="min-width: 100%!important;">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"><b><i class="fa fa-dollar"></i></b></span>
                                                                        </div>
                                                                        <input type="number" name="proposal_finance[monthly_payment_plan]" class="form-control" id="monthly_payment_plan" required="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 mb-3">
                                                                <div class="form-group">
                                                                    <label for="monthly_payment_plan">Upfront Payment Option</label>
                                                                    <select class="form-control" name="proposal_finance[upfront_payment_options]" id="upfront_payment_options">
                                                                        <option value="1" selected="">Option 1 - Upfront Payment upto 39kW</option>
                                                                        <option value="2">Option 1 - Upfront Payment for 40kW - 100kW</option>
                                                                        <option value="3">Option 1 - Upfront Payment for 100kW+</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 mb-3" id="finance_loader"></div>
                                                            <div class="btn-block mt-5 text-center">
                                                                <input type="button" id="save_proposal_finance_btn" value="Update" class="btn btn-primary">
                                                                <input type="button" id="delete_proposal_finance_btn" value="Remove" class="btn btn-primary hidden">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>


                        </div>
                    </div>
                </div>


            </div>




        </div>
    </div>

</div>




<!-- financial summery popup for stc -->
<div class="modal fade" id="stc_calculation_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="margin-bottom: -10px;">Calculate STC </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table width="100%" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <td>System Size (kW)</td>
                                    <td>Postcode</td>
                                    <td>Installation Year</td>
                                    <td># STC</td>
                                    <td>Price Assumption</td>
                                    <td>Rebate Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_system_size" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_postcode" readonly=""></td>
                                    <td>
                                        <!-- <input type="number" min="1" class="form-control stc_calculate_inputs" id="stc_calculate_year" readonly=""> -->
                                        <select class="form-control stc_calculate_inputs" id="stc_calculate_year">
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                    </td>
                                    <td><input type="number" min="1" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_stc" readonly=""></td>
                                    <td><input type="number" min="1" step="0.01" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_price"></td>
                                    <td><input type="number" min="1" step="0.01" oninput="validity.valid||(value='');" class="form-control stc_calculate_inputs" id="stc_calculate_rebate" readonly=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="btn-block mt-2 ">
                        <input type="button" id="save_stc_btn2" value="Save" class="btn btn-primary float-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- financial summery popup for stc -->



<!-- financial summery popup for stc -->
<div class="modal fade" id="veec_calculation_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="margin-bottom: -10px;">Calculate VEEC Discount </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <div class="row mb-3">
                        <div class="col-2 d-flex align-items-center">VEEC Price</div>
                        <div class="col-10">
                            <input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_price" value="65">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table width="100%" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="4">Equation 4</th>
                                </tr>
                                <tr>
                                    <td>System Size (kW)</td>
                                    <td>Annual Production</td>
                                    <td>Percentage of Export</td>
                                    <td>mWh</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_system_size" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_annual_production" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_percentage_of_export" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_mwh_savings" readonly=""></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="4">Equation 2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
                                    <td>Savings</td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_accuracy_savings" readonly=""></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>Accuracy Factor</td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_accuracy_factor" value="1"></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>Decay Factor</td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" value="9.91" id="veer_decay_factor"></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>mWh Savings after decay factor</td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veer_after_decay_factor"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="4">Equation 1</th>
                                </tr>
                                <tr>
                                    <td>Savings</td>
                                    <td>Emmissons Factor</td>
                                    <td>Regional Factor</td>
                                    <td>VEECs</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_savings" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" value="0.9546" id="veec_emissons_factor" readonly=""></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_regional_factor" value="0.98"></td>
                                    <td><input type="number" min="1" class="form-control veec_calculation_inputs" id="veec_value" readonly=""></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>VEEC Discount</td>
                                    <td><input type="number" min="1" class="form-control stc_calculate_inputs" id="veec_discount_val" readonly=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="btn-block mt-2 ">
                        <input type="button" id="save_veec_discount" value="Save" class="btn btn-primary float-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- financial summery popup for stc -->




<div class="modal fade" id="calculate_degration_model" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 90%;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header align-items-center">
                <div class="d-flex justify-content-between align-items-center" style="width: 100%;">
                    <h4 class="modal-title" style="margin-bottom:0; padding: 0;">Cashflow</h4>
                    <!-- export_degradation_data -->
                    <button id="" onclick="exportReport()" class="btn btn-primary" style="margin-right:2rem;">Export</button>
                </div>
                <button type="button" class="close" data-dismiss="modal" style="font-size: 28px;">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive" id="viewTable" style="width:100%; overflow-x:scroll;"></div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo site_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>assets/js/isLoading.min.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>assets/js/jquery.table2excel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src='<?php echo site_url(); ?>assets/js/placecomplete.js?v=<?php echo version; ?>' type='text/javascript'></script>
<script src="<?php echo site_url(); ?>common/js/solar_proposal.js?v=<?php echo version; ?>" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    function exportReport() {

        var table = $("#viewTable table");

        $(table).table2excel({

            // exclude CSS class
            exclude: ".noExl",
            name: "view_cashflow",
            filename: "view_cashflow_" + $.now(), //do not include extension
            fileext: ".xls", // file extension
            preserveColors: true,
            sheetName: "view_cashflow_"

        });

    }

    $(document).ready(function() {

        // $('#system_pricing_tilt').select2();

        $(document).on('change', '#losses_toggle', function() {
            // console.log($("#losses_toggle_value").val())
            if ($("#losses_toggle_value").val() != '0') {
                $("#pv_system_losses").show();
                $("#pv_system_losses_details").hide();

                $("#losses_toggle_value").val('0')
            } else {
                $("#pv_system_losses_details").show();
                $("#pv_system_losses").hide();
                $("#losses_toggle_value").val('1')
            }
        });


        $(document).on("click", ".advanced_paramter_toggle", function() {


            if ($("#advanced_paramter_toggle_value").val() != '0') {
                $(".advanced_parameter_body").hide();
                $("#advanced_paramter_toggle_value").val(0);
                $(".advanced_paramter_toggle i").removeClass('fa-minus');

            } else {
                $(".advanced_parameter_body").show();
                $("#advanced_paramter_toggle_value").val(1);
                $(".advanced_paramter_toggle i").addClass('fa-minus');
            }

        });


        $(document).on("click", "#estimated_losses_save", function() {
            var system_losses_value = $(".system_losses_model_val");

            for (let index = 0; index < system_losses_value.length; index++) {
                $(system_losses_value[index]).attr('oldvalue', $(system_losses_value[index]).val());
                // $(system_losses_value[index]).val(parseFloat($(system_losses_value[index]).attr('oldvalue')));
                // total_estimated_value = parseFloat(total_estimated_value) + parseFloat($(system_losses_value[index]).attr('oldvalue'));
            }

            $("#pv_system_losses").val($("#totoal_estimated_losses_value").val());
        });






        $(document).on("click", "#estimated_losses_reset", function() {
            var total_estimated_value = 0;
            var system_losses_value = $(".system_losses_model_val");
            for (let index = 0; index < system_losses_value.length; index++) {
                $(system_losses_value[index]).val(parseFloat($(system_losses_value[index]).attr('oldvalue')));
                total_estimated_value = parseFloat(total_estimated_value) + parseFloat($(system_losses_value[index]).attr('oldvalue'));
            }
            $("#totoal_estimated_losses_value").val(parseFloat(total_estimated_value).toFixed(2));
            $("#totoal_estimated_losses").text(parseFloat(total_estimated_value).toFixed(2) + '%');
        });


        $('.locationstreetAddress').placecomplete2({
            placeServiceResult2: function(data, status) {
                if (status != 'INVALID_REQUEST') {
                    $('#show_something').html(data.adr_address);

                    //Set lat lng
                    var lat = data.geometry.location.lat(),
                        lng = data.geometry.location.lng();
                    if ($('#locationLatitude') != undefined) {
                        $('#locationLatitude').val(lat);
                    }
                    if ($('#locationLongitude') != undefined) {
                        $('#locationLongitude').val(lng);
                    }

                    //Set Address
                    if ($('.locationstreetAddressVal') != undefined) {
                        $('.locationstreetAddressVal').val(data.formatted_address);
                    }

                    var address_components = data.address_components;

                    for (var i = 0; i < address_components.length; i++) {
                        //Set State
                        if (address_components[i].types[0] == 'administrative_area_level_1') {
                            var states = [];
                            var selected_id = '';
                            if ($('.locationState') != undefined) {
                                var select = document.querySelector(".locationState");
                                for (var j = 0; j < select.length; j++) {
                                    var option = select.options[j];
                                    states.push(option.text);
                                    if (option.text == address_components[i].long_name) {
                                        option.setAttribute('selected', 'selected');
                                        selected_id = option.value;
                                    }
                                }

                                //For ipad and apple devices selected and prop was not working so had to create State element again
                                document.querySelector(".locationState").innerHTML = '';
                                for (var k = 0; k < states.length; k++) {
                                    var option = document.createElement('option');
                                    if (k == 0) {
                                        option.setAttribute('value', '');
                                        option.innerHTML = states[k];
                                        if (selected_id == k) {
                                            option.setAttribute('selected', 'selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    } else {
                                        option.setAttribute('value', k);
                                        option.innerHTML = states[k];
                                        if (selected_id == k) {
                                            option.setAttribute('selected', 'selected');
                                        }
                                        document.querySelector(".locationState").appendChild(option);
                                    }
                                }

                            }
                        }

                        //Set Postal Code
                        if (address_components[i].types[0] == 'postal_code') {
                            if ($('.locationPostCode') != undefined) {
                                $('.locationPostCode').val(address_components[i].long_name);
                                $('.locationPostCode').trigger('change');
                            }
                        }
                    }
                } else {
                    $('#locationstreetAddressVal').val($('#locationstreetAddress').val());
                }
            },
            language: 'fr'
        });

    });

    var context = {};
    context.userid = '<?php echo isset($user_id) ? $user_id : ''; ?>';
    context.proposal_data = '<?php echo (isset($proposal_data) && !empty($proposal_data)) ? json_encode($proposal_data, JSON_HEX_APOS) : ''; ?>';
    context.lead_data = '<?php echo (isset($lead_data) && !empty($lead_data)) ? json_encode($lead_data, JSON_HEX_APOS) : ''; ?>';
    context.panel_data = '<?php echo (isset($panel_data) && !empty($panel_data)) ? json_encode($panel_data, JSON_HEX_APOS) : ''; ?>';
    context.lead_uuid = '<?php echo (isset($lead_uuid) && $lead_uuid != '') ? $lead_uuid : ''; ?>';
    context.financial_summary_data = '<?php echo (isset($financial_summary_data) && !empty($financial_summary_data)) ? json_encode($financial_summary_data, JSON_HEX_APOS) : ''; ?>';
    context.site_id = '<?php echo (isset($site_id) && $site_id != '') ? $site_id : ''; ?>';
    context.lead_id = '<?php echo (isset($lead_id) && $lead_id != '') ? $lead_id : ''; ?>';
    context.postcode_rating = '<?php echo (isset($postcode_rating) && $postcode_rating != '') ? $postcode_rating : 0; ?>';
    var solar_proposal_manager = new solar_proposal_manager(context);






    $(window).load(function() {
        // getPvwattCalc();
    })
</script>
<script type='text/javascript' async>
    google.load('visualization', '1', {
        packages: ['corechart']
    });

    // $("#meterDataFileURL").on('chnage', submit())
    $('#importMeterDataFileSubmit').submit(function(e) {

        e.preventDefault();
        $.ajax({
            url: base_url + 'admin/proposal/commercial/upload_meter_data',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            beforeSend: function() {},
            success: function(data) {
                // $.isLoading("hide");
                // window.location.href = base_url + 'admin/proposal/commercial/view_proposal/' + $('#proposal_data_id').val();
                // window.open(
                //     base_url + 'admin/proposal/commercial/view_proposal/' + $('#proposal_data_id').val(),
                //     '_blank'
                // );
                console.log(JSON.parse(data));

                // console.log(base_url + 'admin/proposal/commercial/view_proposal/' + $('#proposal_data_id').val(),
                //     '_blank'
                // )
            }
        });
    });




    // $(document).on("click", "#view_proposal_action", function(e) {
    //     var formDatat = new FormData($('#importMeterDataFileSubmit')[0]);
    //     $.ajax({
    //         url: base_url + 'admin/proposal/commercial/upload_meter_data',
    //         type: "post",
    //         data: formDatat,
    //         processData: false,
    //         contentType: false,
    //         cache: false,
    //         beforeSend: function() {
    //             $.isLoading({
    //                 text: "Loading lead data, Please Wait....",
    //             });
    //         },
    //         success: function(data) {
    //             console.log(JSON.parse(data));
    //             $.isLoading("hide");
    //         }
    //     });
    // });

    // $(document).on("click", "#save_proposal_btn", function(e) {

    //     if ($(".commerical-panel-tab .nav-link.active").attr('data-id') == 2) {
    //         console.log($(".commerical-panel-tab .nav-link.active").attr('data-id'));
    //         $("#importMeterDataFileSubmit").submit();
    //     }
    // });


    function show_graph(avg_load_graph_data) {

        var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
        var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
        console.log(avg_load_graph_data)
        var labels = {};

        Highcharts.chart('avg_load_area_chart', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

                align: 'center',

                verticalAlign: 'bottom',

                x: 0,

                y: 0,

                itemStyle: {

                    "fontSize": "9px",

                    "color": '#666',

                    "fontWeight": 'normal'

                }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

                {

                    name: 'Jan',

                    data: [

                        avg_load_graph_data['jan'][0],

                        avg_load_graph_data['jan'][1],

                        avg_load_graph_data['jan'][2],

                        avg_load_graph_data['jan'][3],

                        avg_load_graph_data['jan'][4],

                        avg_load_graph_data['jan'][5],

                        avg_load_graph_data['jan'][6],

                        avg_load_graph_data['jan'][7],

                        avg_load_graph_data['jan'][8],

                        avg_load_graph_data['jan'][9],

                        avg_load_graph_data['jan'][10],

                        avg_load_graph_data['jan'][11],

                        avg_load_graph_data['jan'][12],

                        avg_load_graph_data['jan'][13],

                        avg_load_graph_data['jan'][14],

                        avg_load_graph_data['jan'][15],

                        avg_load_graph_data['jan'][16],

                        avg_load_graph_data['jan'][17],

                        avg_load_graph_data['jan'][18],

                        avg_load_graph_data['jan'][19],

                        avg_load_graph_data['jan'][20],

                        avg_load_graph_data['jan'][21],

                        avg_load_graph_data['jan'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Feb',

                    data: [

                        avg_load_graph_data['feb'][0],

                        avg_load_graph_data['feb'][1],

                        avg_load_graph_data['feb'][2],

                        avg_load_graph_data['feb'][3],

                        avg_load_graph_data['feb'][4],

                        avg_load_graph_data['feb'][5],

                        avg_load_graph_data['feb'][6],

                        avg_load_graph_data['feb'][7],

                        avg_load_graph_data['feb'][8],

                        avg_load_graph_data['feb'][9],

                        avg_load_graph_data['feb'][10],

                        avg_load_graph_data['feb'][11],

                        avg_load_graph_data['feb'][12],

                        avg_load_graph_data['feb'][13],

                        avg_load_graph_data['feb'][14],

                        avg_load_graph_data['feb'][15],

                        avg_load_graph_data['feb'][16],

                        avg_load_graph_data['feb'][17],

                        avg_load_graph_data['feb'][18],

                        avg_load_graph_data['feb'][19],

                        avg_load_graph_data['feb'][20],

                        avg_load_graph_data['feb'][21],

                        avg_load_graph_data['feb'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Mar',

                    data: [

                        avg_load_graph_data['mar'][0],

                        avg_load_graph_data['mar'][1],

                        avg_load_graph_data['mar'][2],

                        avg_load_graph_data['mar'][3],

                        avg_load_graph_data['mar'][4],

                        avg_load_graph_data['mar'][5],

                        avg_load_graph_data['mar'][6],

                        avg_load_graph_data['mar'][7],

                        avg_load_graph_data['mar'][8],

                        avg_load_graph_data['mar'][9],

                        avg_load_graph_data['mar'][10],

                        avg_load_graph_data['mar'][11],

                        avg_load_graph_data['mar'][12],

                        avg_load_graph_data['mar'][13],

                        avg_load_graph_data['mar'][14],

                        avg_load_graph_data['mar'][15],

                        avg_load_graph_data['mar'][16],

                        avg_load_graph_data['mar'][17],

                        avg_load_graph_data['mar'][18],

                        avg_load_graph_data['mar'][19],

                        avg_load_graph_data['mar'][20],

                        avg_load_graph_data['mar'][21],

                        avg_load_graph_data['mar'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Apr',

                    data: [

                        avg_load_graph_data['apr'][0],

                        avg_load_graph_data['apr'][1],

                        avg_load_graph_data['apr'][2],

                        avg_load_graph_data['apr'][3],

                        avg_load_graph_data['apr'][4],

                        avg_load_graph_data['apr'][5],

                        avg_load_graph_data['apr'][6],

                        avg_load_graph_data['apr'][7],

                        avg_load_graph_data['apr'][8],

                        avg_load_graph_data['apr'][9],

                        avg_load_graph_data['apr'][10],

                        avg_load_graph_data['apr'][11],

                        avg_load_graph_data['apr'][12],

                        avg_load_graph_data['apr'][13],

                        avg_load_graph_data['apr'][14],

                        avg_load_graph_data['apr'][15],

                        avg_load_graph_data['apr'][16],

                        avg_load_graph_data['apr'][17],

                        avg_load_graph_data['apr'][18],

                        avg_load_graph_data['apr'][19],

                        avg_load_graph_data['apr'][20],

                        avg_load_graph_data['apr'][21],

                        avg_load_graph_data['apr'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'May',

                    data: [

                        avg_load_graph_data['may'][0],

                        avg_load_graph_data['may'][1],

                        avg_load_graph_data['may'][2],

                        avg_load_graph_data['may'][3],

                        avg_load_graph_data['may'][4],

                        avg_load_graph_data['may'][5],

                        avg_load_graph_data['may'][6],

                        avg_load_graph_data['may'][7],

                        avg_load_graph_data['may'][8],

                        avg_load_graph_data['may'][9],

                        avg_load_graph_data['may'][10],

                        avg_load_graph_data['may'][11],

                        avg_load_graph_data['may'][12],

                        avg_load_graph_data['may'][13],

                        avg_load_graph_data['may'][14],

                        avg_load_graph_data['may'][15],

                        avg_load_graph_data['may'][16],

                        avg_load_graph_data['may'][17],

                        avg_load_graph_data['may'][18],

                        avg_load_graph_data['may'][19],

                        avg_load_graph_data['may'][20],

                        avg_load_graph_data['may'][21],

                        avg_load_graph_data['may'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Jun',

                    data: [

                        avg_load_graph_data['jun'][0],

                        avg_load_graph_data['jun'][1],

                        avg_load_graph_data['jun'][2],

                        avg_load_graph_data['jun'][3],

                        avg_load_graph_data['jun'][4],

                        avg_load_graph_data['jun'][5],

                        avg_load_graph_data['jun'][6],

                        avg_load_graph_data['jun'][7],

                        avg_load_graph_data['jun'][8],

                        avg_load_graph_data['jun'][9],

                        avg_load_graph_data['jun'][10],

                        avg_load_graph_data['jun'][11],

                        avg_load_graph_data['jun'][12],

                        avg_load_graph_data['jun'][13],

                        avg_load_graph_data['jun'][14],

                        avg_load_graph_data['jun'][15],

                        avg_load_graph_data['jun'][16],

                        avg_load_graph_data['jun'][17],

                        avg_load_graph_data['jun'][18],

                        avg_load_graph_data['jun'][19],

                        avg_load_graph_data['jun'][20],

                        avg_load_graph_data['jun'][21],

                        avg_load_graph_data['jun'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Jul',

                    data: [

                        avg_load_graph_data['jul'][0],

                        avg_load_graph_data['jul'][1],

                        avg_load_graph_data['jul'][2],

                        avg_load_graph_data['jul'][3],

                        avg_load_graph_data['jul'][4],

                        avg_load_graph_data['jul'][5],

                        avg_load_graph_data['jul'][6],

                        avg_load_graph_data['jul'][7],

                        avg_load_graph_data['jul'][8],

                        avg_load_graph_data['jul'][9],

                        avg_load_graph_data['jul'][10],

                        avg_load_graph_data['jul'][11],

                        avg_load_graph_data['jul'][12],

                        avg_load_graph_data['jul'][13],

                        avg_load_graph_data['jul'][14],

                        avg_load_graph_data['jul'][15],

                        avg_load_graph_data['jul'][16],

                        avg_load_graph_data['jul'][17],

                        avg_load_graph_data['jul'][18],

                        avg_load_graph_data['jul'][19],

                        avg_load_graph_data['jul'][20],

                        avg_load_graph_data['jul'][21],

                        avg_load_graph_data['jul'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Aug',

                    data: [

                        avg_load_graph_data['aug'][0],

                        avg_load_graph_data['aug'][1],

                        avg_load_graph_data['aug'][2],

                        avg_load_graph_data['aug'][3],

                        avg_load_graph_data['aug'][4],

                        avg_load_graph_data['aug'][5],

                        avg_load_graph_data['aug'][6],

                        avg_load_graph_data['aug'][7],

                        avg_load_graph_data['aug'][8],

                        avg_load_graph_data['aug'][9],

                        avg_load_graph_data['aug'][10],

                        avg_load_graph_data['aug'][11],

                        avg_load_graph_data['aug'][12],

                        avg_load_graph_data['aug'][13],

                        avg_load_graph_data['aug'][14],

                        avg_load_graph_data['aug'][15],

                        avg_load_graph_data['aug'][16],

                        avg_load_graph_data['aug'][17],

                        avg_load_graph_data['aug'][18],

                        avg_load_graph_data['aug'][19],

                        avg_load_graph_data['aug'][20],

                        avg_load_graph_data['aug'][21],

                        avg_load_graph_data['aug'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Sep',

                    data: [

                        avg_load_graph_data['sep'][0],

                        avg_load_graph_data['sep'][1],

                        avg_load_graph_data['sep'][2],

                        avg_load_graph_data['sep'][3],

                        avg_load_graph_data['sep'][4],

                        avg_load_graph_data['sep'][5],

                        avg_load_graph_data['sep'][6],

                        avg_load_graph_data['sep'][7],

                        avg_load_graph_data['sep'][8],

                        avg_load_graph_data['sep'][9],

                        avg_load_graph_data['sep'][10],

                        avg_load_graph_data['sep'][11],

                        avg_load_graph_data['sep'][12],

                        avg_load_graph_data['sep'][13],

                        avg_load_graph_data['sep'][14],

                        avg_load_graph_data['sep'][15],

                        avg_load_graph_data['sep'][16],

                        avg_load_graph_data['sep'][17],

                        avg_load_graph_data['sep'][18],

                        avg_load_graph_data['sep'][19],

                        avg_load_graph_data['sep'][20],

                        avg_load_graph_data['sep'][21],

                        avg_load_graph_data['sep'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Oct',

                    data: [

                        avg_load_graph_data['oct'][0],

                        avg_load_graph_data['oct'][1],

                        avg_load_graph_data['oct'][2],

                        avg_load_graph_data['oct'][3],

                        avg_load_graph_data['oct'][4],

                        avg_load_graph_data['oct'][5],

                        avg_load_graph_data['oct'][6],

                        avg_load_graph_data['oct'][7],

                        avg_load_graph_data['oct'][8],

                        avg_load_graph_data['oct'][9],

                        avg_load_graph_data['oct'][10],

                        avg_load_graph_data['oct'][11],

                        avg_load_graph_data['oct'][12],

                        avg_load_graph_data['oct'][13],

                        avg_load_graph_data['oct'][14],

                        avg_load_graph_data['oct'][15],

                        avg_load_graph_data['oct'][16],

                        avg_load_graph_data['oct'][17],

                        avg_load_graph_data['oct'][18],

                        avg_load_graph_data['oct'][19],

                        avg_load_graph_data['oct'][20],

                        avg_load_graph_data['oct'][21],

                        avg_load_graph_data['oct'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Nov',

                    data: [

                        avg_load_graph_data['nov'][0],

                        avg_load_graph_data['nov'][1],

                        avg_load_graph_data['nov'][2],

                        avg_load_graph_data['nov'][3],

                        avg_load_graph_data['nov'][4],

                        avg_load_graph_data['nov'][5],

                        avg_load_graph_data['nov'][6],

                        avg_load_graph_data['nov'][7],

                        avg_load_graph_data['nov'][8],

                        avg_load_graph_data['nov'][9],

                        avg_load_graph_data['nov'][10],

                        avg_load_graph_data['nov'][11],

                        avg_load_graph_data['nov'][12],

                        avg_load_graph_data['nov'][13],

                        avg_load_graph_data['nov'][14],

                        avg_load_graph_data['nov'][15],

                        avg_load_graph_data['nov'][16],

                        avg_load_graph_data['nov'][17],

                        avg_load_graph_data['nov'][18],

                        avg_load_graph_data['nov'][19],

                        avg_load_graph_data['nov'][20],

                        avg_load_graph_data['nov'][21],

                        avg_load_graph_data['nov'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {



                    name: 'Dec',

                    data: [

                        avg_load_graph_data['dec'][0],

                        avg_load_graph_data['dec'][1],

                        avg_load_graph_data['dec'][2],

                        avg_load_graph_data['dec'][3],

                        avg_load_graph_data['dec'][4],

                        avg_load_graph_data['dec'][5],

                        avg_load_graph_data['dec'][6],

                        avg_load_graph_data['dec'][7],

                        avg_load_graph_data['dec'][8],

                        avg_load_graph_data['dec'][9],

                        avg_load_graph_data['dec'][10],

                        avg_load_graph_data['dec'][11],

                        avg_load_graph_data['dec'][12],

                        avg_load_graph_data['dec'][13],

                        avg_load_graph_data['dec'][14],

                        avg_load_graph_data['dec'][15],

                        avg_load_graph_data['dec'][16],

                        avg_load_graph_data['dec'][17],

                        avg_load_graph_data['dec'][18],

                        avg_load_graph_data['dec'][19],

                        avg_load_graph_data['dec'][20],

                        avg_load_graph_data['dec'][21],

                        avg_load_graph_data['dec'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },



            ]





        });
    }

    function show_graph1(avg_load_graph_data) {

        var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
        var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
        console.log(avg_load_graph_data)
        var labels = {};

        Highcharts.chart('avg_load_area_chart1', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

                align: 'center',

                verticalAlign: 'bottom',

                x: 0,

                y: 0,

                itemStyle: {

                    "fontSize": "9px",

                    "color": '#666',

                    "fontWeight": 'normal'

                }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

                {

                    name: 'Jan',

                    data: [

                        avg_load_graph_data['jan'][0],

                        avg_load_graph_data['jan'][1],

                        avg_load_graph_data['jan'][2],

                        avg_load_graph_data['jan'][3],

                        avg_load_graph_data['jan'][4],

                        avg_load_graph_data['jan'][5],

                        avg_load_graph_data['jan'][6],

                        avg_load_graph_data['jan'][7],

                        avg_load_graph_data['jan'][8],

                        avg_load_graph_data['jan'][9],

                        avg_load_graph_data['jan'][10],

                        avg_load_graph_data['jan'][11],

                        avg_load_graph_data['jan'][12],

                        avg_load_graph_data['jan'][13],

                        avg_load_graph_data['jan'][14],

                        avg_load_graph_data['jan'][15],

                        avg_load_graph_data['jan'][16],

                        avg_load_graph_data['jan'][17],

                        avg_load_graph_data['jan'][18],

                        avg_load_graph_data['jan'][19],

                        avg_load_graph_data['jan'][20],

                        avg_load_graph_data['jan'][21],

                        avg_load_graph_data['jan'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Feb',

                    data: [

                        avg_load_graph_data['feb'][0],

                        avg_load_graph_data['feb'][1],

                        avg_load_graph_data['feb'][2],

                        avg_load_graph_data['feb'][3],

                        avg_load_graph_data['feb'][4],

                        avg_load_graph_data['feb'][5],

                        avg_load_graph_data['feb'][6],

                        avg_load_graph_data['feb'][7],

                        avg_load_graph_data['feb'][8],

                        avg_load_graph_data['feb'][9],

                        avg_load_graph_data['feb'][10],

                        avg_load_graph_data['feb'][11],

                        avg_load_graph_data['feb'][12],

                        avg_load_graph_data['feb'][13],

                        avg_load_graph_data['feb'][14],

                        avg_load_graph_data['feb'][15],

                        avg_load_graph_data['feb'][16],

                        avg_load_graph_data['feb'][17],

                        avg_load_graph_data['feb'][18],

                        avg_load_graph_data['feb'][19],

                        avg_load_graph_data['feb'][20],

                        avg_load_graph_data['feb'][21],

                        avg_load_graph_data['feb'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Mar',

                    data: [

                        avg_load_graph_data['mar'][0],

                        avg_load_graph_data['mar'][1],

                        avg_load_graph_data['mar'][2],

                        avg_load_graph_data['mar'][3],

                        avg_load_graph_data['mar'][4],

                        avg_load_graph_data['mar'][5],

                        avg_load_graph_data['mar'][6],

                        avg_load_graph_data['mar'][7],

                        avg_load_graph_data['mar'][8],

                        avg_load_graph_data['mar'][9],

                        avg_load_graph_data['mar'][10],

                        avg_load_graph_data['mar'][11],

                        avg_load_graph_data['mar'][12],

                        avg_load_graph_data['mar'][13],

                        avg_load_graph_data['mar'][14],

                        avg_load_graph_data['mar'][15],

                        avg_load_graph_data['mar'][16],

                        avg_load_graph_data['mar'][17],

                        avg_load_graph_data['mar'][18],

                        avg_load_graph_data['mar'][19],

                        avg_load_graph_data['mar'][20],

                        avg_load_graph_data['mar'][21],

                        avg_load_graph_data['mar'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },
            ]

        });
    }

    function show_graph2(avg_load_graph_data) {

        var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
        var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
        console.log(avg_load_graph_data)
        var labels = {};

        Highcharts.chart('avg_load_area_chart2', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

                align: 'center',

                verticalAlign: 'bottom',

                x: 0,

                y: 0,

                itemStyle: {

                    "fontSize": "9px",

                    "color": '#666',

                    "fontWeight": 'normal'

                }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

                {

                    name: 'Apr',

                    data: [

                        avg_load_graph_data['apr'][0],

                        avg_load_graph_data['apr'][1],

                        avg_load_graph_data['apr'][2],

                        avg_load_graph_data['apr'][3],

                        avg_load_graph_data['apr'][4],

                        avg_load_graph_data['apr'][5],

                        avg_load_graph_data['apr'][6],

                        avg_load_graph_data['apr'][7],

                        avg_load_graph_data['apr'][8],

                        avg_load_graph_data['apr'][9],

                        avg_load_graph_data['apr'][10],

                        avg_load_graph_data['apr'][11],

                        avg_load_graph_data['apr'][12],

                        avg_load_graph_data['apr'][13],

                        avg_load_graph_data['apr'][14],

                        avg_load_graph_data['apr'][15],

                        avg_load_graph_data['apr'][16],

                        avg_load_graph_data['apr'][17],

                        avg_load_graph_data['apr'][18],

                        avg_load_graph_data['apr'][19],

                        avg_load_graph_data['apr'][20],

                        avg_load_graph_data['apr'][21],

                        avg_load_graph_data['apr'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'May',

                    data: [

                        avg_load_graph_data['may'][0],

                        avg_load_graph_data['may'][1],

                        avg_load_graph_data['may'][2],

                        avg_load_graph_data['may'][3],

                        avg_load_graph_data['may'][4],

                        avg_load_graph_data['may'][5],

                        avg_load_graph_data['may'][6],

                        avg_load_graph_data['may'][7],

                        avg_load_graph_data['may'][8],

                        avg_load_graph_data['may'][9],

                        avg_load_graph_data['may'][10],

                        avg_load_graph_data['may'][11],

                        avg_load_graph_data['may'][12],

                        avg_load_graph_data['may'][13],

                        avg_load_graph_data['may'][14],

                        avg_load_graph_data['may'][15],

                        avg_load_graph_data['may'][16],

                        avg_load_graph_data['may'][17],

                        avg_load_graph_data['may'][18],

                        avg_load_graph_data['may'][19],

                        avg_load_graph_data['may'][20],

                        avg_load_graph_data['may'][21],

                        avg_load_graph_data['may'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Jun',

                    data: [

                        avg_load_graph_data['jun'][0],

                        avg_load_graph_data['jun'][1],

                        avg_load_graph_data['jun'][2],

                        avg_load_graph_data['jun'][3],

                        avg_load_graph_data['jun'][4],

                        avg_load_graph_data['jun'][5],

                        avg_load_graph_data['jun'][6],

                        avg_load_graph_data['jun'][7],

                        avg_load_graph_data['jun'][8],

                        avg_load_graph_data['jun'][9],

                        avg_load_graph_data['jun'][10],

                        avg_load_graph_data['jun'][11],

                        avg_load_graph_data['jun'][12],

                        avg_load_graph_data['jun'][13],

                        avg_load_graph_data['jun'][14],

                        avg_load_graph_data['jun'][15],

                        avg_load_graph_data['jun'][16],

                        avg_load_graph_data['jun'][17],

                        avg_load_graph_data['jun'][18],

                        avg_load_graph_data['jun'][19],

                        avg_load_graph_data['jun'][20],

                        avg_load_graph_data['jun'][21],

                        avg_load_graph_data['jun'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },
            ]





        });
    }

    function show_graph3(avg_load_graph_data) {

        var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
        var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
        console.log(avg_load_graph_data)
        var labels = {};

        Highcharts.chart('avg_load_area_chart3', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

                align: 'center',

                verticalAlign: 'bottom',

                x: 0,

                y: 0,

                itemStyle: {

                    "fontSize": "9px",

                    "color": '#666',

                    "fontWeight": 'normal'

                }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

                {

                    name: 'Jul',

                    data: [

                        avg_load_graph_data['jul'][0],

                        avg_load_graph_data['jul'][1],

                        avg_load_graph_data['jul'][2],

                        avg_load_graph_data['jul'][3],

                        avg_load_graph_data['jul'][4],

                        avg_load_graph_data['jul'][5],

                        avg_load_graph_data['jul'][6],

                        avg_load_graph_data['jul'][7],

                        avg_load_graph_data['jul'][8],

                        avg_load_graph_data['jul'][9],

                        avg_load_graph_data['jul'][10],

                        avg_load_graph_data['jul'][11],

                        avg_load_graph_data['jul'][12],

                        avg_load_graph_data['jul'][13],

                        avg_load_graph_data['jul'][14],

                        avg_load_graph_data['jul'][15],

                        avg_load_graph_data['jul'][16],

                        avg_load_graph_data['jul'][17],

                        avg_load_graph_data['jul'][18],

                        avg_load_graph_data['jul'][19],

                        avg_load_graph_data['jul'][20],

                        avg_load_graph_data['jul'][21],

                        avg_load_graph_data['jul'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Aug',

                    data: [

                        avg_load_graph_data['aug'][0],

                        avg_load_graph_data['aug'][1],

                        avg_load_graph_data['aug'][2],

                        avg_load_graph_data['aug'][3],

                        avg_load_graph_data['aug'][4],

                        avg_load_graph_data['aug'][5],

                        avg_load_graph_data['aug'][6],

                        avg_load_graph_data['aug'][7],

                        avg_load_graph_data['aug'][8],

                        avg_load_graph_data['aug'][9],

                        avg_load_graph_data['aug'][10],

                        avg_load_graph_data['aug'][11],

                        avg_load_graph_data['aug'][12],

                        avg_load_graph_data['aug'][13],

                        avg_load_graph_data['aug'][14],

                        avg_load_graph_data['aug'][15],

                        avg_load_graph_data['aug'][16],

                        avg_load_graph_data['aug'][17],

                        avg_load_graph_data['aug'][18],

                        avg_load_graph_data['aug'][19],

                        avg_load_graph_data['aug'][20],

                        avg_load_graph_data['aug'][21],

                        avg_load_graph_data['aug'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Sep',

                    data: [

                        avg_load_graph_data['sep'][0],

                        avg_load_graph_data['sep'][1],

                        avg_load_graph_data['sep'][2],

                        avg_load_graph_data['sep'][3],

                        avg_load_graph_data['sep'][4],

                        avg_load_graph_data['sep'][5],

                        avg_load_graph_data['sep'][6],

                        avg_load_graph_data['sep'][7],

                        avg_load_graph_data['sep'][8],

                        avg_load_graph_data['sep'][9],

                        avg_load_graph_data['sep'][10],

                        avg_load_graph_data['sep'][11],

                        avg_load_graph_data['sep'][12],

                        avg_load_graph_data['sep'][13],

                        avg_load_graph_data['sep'][14],

                        avg_load_graph_data['sep'][15],

                        avg_load_graph_data['sep'][16],

                        avg_load_graph_data['sep'][17],

                        avg_load_graph_data['sep'][18],

                        avg_load_graph_data['sep'][19],

                        avg_load_graph_data['sep'][20],

                        avg_load_graph_data['sep'][21],

                        avg_load_graph_data['sep'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },



            ]





        });
    }

    function show_graph4(avg_load_graph_data) {

        var avg_monthly_load_graph_data = JSON.parse(avg_load_graph_data);
        var avg_load_graph_data = avg_monthly_load_graph_data.avg_monthly_load_graph_data;
        console.log(avg_load_graph_data)
        var labels = {};

        Highcharts.chart('avg_load_area_chart4', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                        "fontSize": "16px",

                        "color": '#000',

                        "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

                align: 'center',

                verticalAlign: 'bottom',

                x: 0,

                y: 0,

                itemStyle: {

                    "fontSize": "9px",

                    "color": '#666',

                    "fontWeight": 'normal'

                }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [{

                    name: 'Oct',

                    data: [

                        avg_load_graph_data['oct'][0],

                        avg_load_graph_data['oct'][1],

                        avg_load_graph_data['oct'][2],

                        avg_load_graph_data['oct'][3],

                        avg_load_graph_data['oct'][4],

                        avg_load_graph_data['oct'][5],

                        avg_load_graph_data['oct'][6],

                        avg_load_graph_data['oct'][7],

                        avg_load_graph_data['oct'][8],

                        avg_load_graph_data['oct'][9],

                        avg_load_graph_data['oct'][10],

                        avg_load_graph_data['oct'][11],

                        avg_load_graph_data['oct'][12],

                        avg_load_graph_data['oct'][13],

                        avg_load_graph_data['oct'][14],

                        avg_load_graph_data['oct'][15],

                        avg_load_graph_data['oct'][16],

                        avg_load_graph_data['oct'][17],

                        avg_load_graph_data['oct'][18],

                        avg_load_graph_data['oct'][19],

                        avg_load_graph_data['oct'][20],

                        avg_load_graph_data['oct'][21],

                        avg_load_graph_data['oct'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {

                    name: 'Nov',

                    data: [

                        avg_load_graph_data['nov'][0],

                        avg_load_graph_data['nov'][1],

                        avg_load_graph_data['nov'][2],

                        avg_load_graph_data['nov'][3],

                        avg_load_graph_data['nov'][4],

                        avg_load_graph_data['nov'][5],

                        avg_load_graph_data['nov'][6],

                        avg_load_graph_data['nov'][7],

                        avg_load_graph_data['nov'][8],

                        avg_load_graph_data['nov'][9],

                        avg_load_graph_data['nov'][10],

                        avg_load_graph_data['nov'][11],

                        avg_load_graph_data['nov'][12],

                        avg_load_graph_data['nov'][13],

                        avg_load_graph_data['nov'][14],

                        avg_load_graph_data['nov'][15],

                        avg_load_graph_data['nov'][16],

                        avg_load_graph_data['nov'][17],

                        avg_load_graph_data['nov'][18],

                        avg_load_graph_data['nov'][19],

                        avg_load_graph_data['nov'][20],

                        avg_load_graph_data['nov'][21],

                        avg_load_graph_data['nov'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

                {



                    name: 'Dec',

                    data: [

                        avg_load_graph_data['dec'][0],

                        avg_load_graph_data['dec'][1],

                        avg_load_graph_data['dec'][2],

                        avg_load_graph_data['dec'][3],

                        avg_load_graph_data['dec'][4],

                        avg_load_graph_data['dec'][5],

                        avg_load_graph_data['dec'][6],

                        avg_load_graph_data['dec'][7],

                        avg_load_graph_data['dec'][8],

                        avg_load_graph_data['dec'][9],

                        avg_load_graph_data['dec'][10],

                        avg_load_graph_data['dec'][11],

                        avg_load_graph_data['dec'][12],

                        avg_load_graph_data['dec'][13],

                        avg_load_graph_data['dec'][14],

                        avg_load_graph_data['dec'][15],

                        avg_load_graph_data['dec'][16],

                        avg_load_graph_data['dec'][17],

                        avg_load_graph_data['dec'][18],

                        avg_load_graph_data['dec'][19],

                        avg_load_graph_data['dec'][20],

                        avg_load_graph_data['dec'][21],

                        avg_load_graph_data['dec'][22]

                    ],

                    zIndex: 1,

                    fillOpacity: 0,

                    marker: {

                        lineWidth: 2,

                        enabled: true,

                        symbol: 'circle'

                    }

                },

            ]





        });
    }


    function show_production(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var typical_export_load_graph_data = d.typical_export_load_graph_data;
        var typical_export_sol_prd_graph_data = d.typical_export_sol_prd_graph_data;
        var typical_export_export_graph_data = d.typical_export_export_graph_data;
        var typical_export_offset_graph_data = d.typical_export_offset_graph_data;

        Highcharts.chart('typical_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },

                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,
                //minTickInterval: 24,  Highcharts.dateFormat('%H:%M-%H:59', this.value)
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 3600,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 3,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    // pointStart: Date.UTC(2018, 11, 1, 0, 0, 0),
                    // pointInterval: 3780 * 1000
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        typical_export_load_graph_data[0],
                        typical_export_load_graph_data[1],
                        typical_export_load_graph_data[2],
                        typical_export_load_graph_data[3],
                        typical_export_load_graph_data[4],
                        typical_export_load_graph_data[5],
                        typical_export_load_graph_data[6],
                        typical_export_load_graph_data[7],
                        typical_export_load_graph_data[8],
                        typical_export_load_graph_data[9],
                        typical_export_load_graph_data[10],
                        typical_export_load_graph_data[11],
                        typical_export_load_graph_data[12],
                        typical_export_load_graph_data[13],
                        typical_export_load_graph_data[14],
                        typical_export_load_graph_data[15],
                        typical_export_load_graph_data[16],
                        typical_export_load_graph_data[17],
                        typical_export_load_graph_data[18],
                        typical_export_load_graph_data[19],
                        typical_export_load_graph_data[20],
                        typical_export_load_graph_data[21],
                        typical_export_load_graph_data[22],
                        typical_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                },
                {
                    name: 'Solar Output',
                    data: [
                        typical_export_sol_prd_graph_data[0],
                        typical_export_sol_prd_graph_data[1],
                        typical_export_sol_prd_graph_data[2],
                        typical_export_sol_prd_graph_data[3],
                        typical_export_sol_prd_graph_data[4],
                        typical_export_sol_prd_graph_data[5],
                        typical_export_sol_prd_graph_data[6],
                        typical_export_sol_prd_graph_data[7],
                        typical_export_sol_prd_graph_data[8],
                        typical_export_sol_prd_graph_data[9],
                        typical_export_sol_prd_graph_data[10],
                        typical_export_sol_prd_graph_data[11],
                        typical_export_sol_prd_graph_data[12],
                        typical_export_sol_prd_graph_data[13],
                        typical_export_sol_prd_graph_data[14],
                        typical_export_sol_prd_graph_data[15],
                        typical_export_sol_prd_graph_data[16],
                        typical_export_sol_prd_graph_data[17],
                        typical_export_sol_prd_graph_data[18],
                        typical_export_sol_prd_graph_data[19],
                        typical_export_sol_prd_graph_data[20],
                        typical_export_sol_prd_graph_data[21],
                        typical_export_sol_prd_graph_data[22],
                        typical_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        typical_export_export_graph_data[0],
                        typical_export_export_graph_data[1],
                        typical_export_export_graph_data[2],
                        typical_export_export_graph_data[3],
                        typical_export_export_graph_data[4],
                        typical_export_export_graph_data[5],
                        typical_export_export_graph_data[6],
                        typical_export_export_graph_data[7],
                        typical_export_export_graph_data[8],
                        typical_export_export_graph_data[9],
                        typical_export_export_graph_data[10],
                        typical_export_export_graph_data[11],
                        typical_export_export_graph_data[12],
                        typical_export_export_graph_data[13],
                        typical_export_export_graph_data[14],
                        typical_export_export_graph_data[15],
                        typical_export_export_graph_data[16],
                        typical_export_export_graph_data[17],
                        typical_export_export_graph_data[18],
                        typical_export_export_graph_data[19],
                        typical_export_export_graph_data[20],
                        typical_export_export_graph_data[21],
                        typical_export_export_graph_data[22],
                        typical_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        typical_export_offset_graph_data[0],
                        typical_export_offset_graph_data[1],
                        typical_export_offset_graph_data[2],
                        typical_export_offset_graph_data[3],
                        typical_export_offset_graph_data[4],
                        typical_export_offset_graph_data[5],
                        typical_export_offset_graph_data[6],
                        typical_export_offset_graph_data[7],
                        typical_export_offset_graph_data[8],
                        typical_export_offset_graph_data[9],
                        typical_export_offset_graph_data[10],
                        typical_export_offset_graph_data[11],
                        typical_export_offset_graph_data[12],
                        typical_export_offset_graph_data[13],
                        typical_export_offset_graph_data[14],
                        typical_export_offset_graph_data[15],
                        typical_export_offset_graph_data[16],
                        typical_export_offset_graph_data[17],
                        typical_export_offset_graph_data[18],
                        typical_export_offset_graph_data[19],
                        typical_export_offset_graph_data[20],
                        typical_export_offset_graph_data[21],
                        typical_export_offset_graph_data[22],
                        typical_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });
    }

    function show_production1(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var worst_export_load_graph_data = d.worst_export_load_graph_data;
        var worst_export_sol_prd_graph_data = d.worst_export_sol_prd_graph_data;
        var worst_export_export_graph_data = d.worst_export_export_graph_data;
        var worst_export_offset_graph_data = d.worst_export_offset_graph_data;

        Highcharts.chart('worst_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {

                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        worst_export_load_graph_data[0],
                        worst_export_load_graph_data[1],
                        worst_export_load_graph_data[2],
                        worst_export_load_graph_data[3],
                        worst_export_load_graph_data[4],
                        worst_export_load_graph_data[5],
                        worst_export_load_graph_data[6],
                        worst_export_load_graph_data[7],
                        worst_export_load_graph_data[8],
                        worst_export_load_graph_data[9],
                        worst_export_load_graph_data[10],
                        worst_export_load_graph_data[11],
                        worst_export_load_graph_data[12],
                        worst_export_load_graph_data[13],
                        worst_export_load_graph_data[14],
                        worst_export_load_graph_data[15],
                        worst_export_load_graph_data[16],
                        worst_export_load_graph_data[17],
                        worst_export_load_graph_data[18],
                        worst_export_load_graph_data[19],
                        worst_export_load_graph_data[20],
                        worst_export_load_graph_data[21],
                        worst_export_load_graph_data[22],
                        worst_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        worst_export_sol_prd_graph_data[0],
                        worst_export_sol_prd_graph_data[1],
                        worst_export_sol_prd_graph_data[2],
                        worst_export_sol_prd_graph_data[3],
                        worst_export_sol_prd_graph_data[4],
                        worst_export_sol_prd_graph_data[5],
                        worst_export_sol_prd_graph_data[6],
                        worst_export_sol_prd_graph_data[7],
                        worst_export_sol_prd_graph_data[8],
                        worst_export_sol_prd_graph_data[9],
                        worst_export_sol_prd_graph_data[10],
                        worst_export_sol_prd_graph_data[11],
                        worst_export_sol_prd_graph_data[12],
                        worst_export_sol_prd_graph_data[13],
                        worst_export_sol_prd_graph_data[14],
                        worst_export_sol_prd_graph_data[15],
                        worst_export_sol_prd_graph_data[16],
                        worst_export_sol_prd_graph_data[17],
                        worst_export_sol_prd_graph_data[18],
                        worst_export_sol_prd_graph_data[19],
                        worst_export_sol_prd_graph_data[20],
                        worst_export_sol_prd_graph_data[21],
                        worst_export_sol_prd_graph_data[22],
                        worst_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        worst_export_export_graph_data[0],
                        worst_export_export_graph_data[1],
                        worst_export_export_graph_data[2],
                        worst_export_export_graph_data[3],
                        worst_export_export_graph_data[4],
                        worst_export_export_graph_data[5],
                        worst_export_export_graph_data[6],
                        worst_export_export_graph_data[7],
                        worst_export_export_graph_data[8],
                        worst_export_export_graph_data[9],
                        worst_export_export_graph_data[10],
                        worst_export_export_graph_data[11],
                        worst_export_export_graph_data[12],
                        worst_export_export_graph_data[13],
                        worst_export_export_graph_data[14],
                        worst_export_export_graph_data[15],
                        worst_export_export_graph_data[16],
                        worst_export_export_graph_data[17],
                        worst_export_export_graph_data[18],
                        worst_export_export_graph_data[19],
                        worst_export_export_graph_data[20],
                        worst_export_export_graph_data[21],
                        worst_export_export_graph_data[22],
                        worst_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        worst_export_offset_graph_data[0],
                        worst_export_offset_graph_data[1],
                        worst_export_offset_graph_data[2],
                        worst_export_offset_graph_data[3],
                        worst_export_offset_graph_data[4],
                        worst_export_offset_graph_data[5],
                        worst_export_offset_graph_data[6],
                        worst_export_offset_graph_data[7],
                        worst_export_offset_graph_data[8],
                        worst_export_offset_graph_data[9],
                        worst_export_offset_graph_data[10],
                        worst_export_offset_graph_data[11],
                        worst_export_offset_graph_data[12],
                        worst_export_offset_graph_data[13],
                        worst_export_offset_graph_data[14],
                        worst_export_offset_graph_data[15],
                        worst_export_offset_graph_data[16],
                        worst_export_offset_graph_data[17],
                        worst_export_offset_graph_data[18],
                        worst_export_offset_graph_data[19],
                        worst_export_offset_graph_data[20],
                        worst_export_offset_graph_data[21],
                        worst_export_offset_graph_data[22],
                        worst_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });

    }


    function show_production2(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var high_export_load_graph_data = d.high_export_load_graph_data;
        var high_export_sol_prd_graph_data = d.high_export_sol_prd_graph_data;
        var high_export_export_graph_data = d.high_export_export_graph_data;
        var high_export_offset_graph_data = d.high_export_offset_graph_data;


        Highcharts.chart('high_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        high_export_load_graph_data[0],
                        high_export_load_graph_data[1],
                        high_export_load_graph_data[2],
                        high_export_load_graph_data[3],
                        high_export_load_graph_data[4],
                        high_export_load_graph_data[5],
                        high_export_load_graph_data[6],
                        high_export_load_graph_data[7],
                        high_export_load_graph_data[8],
                        high_export_load_graph_data[9],
                        high_export_load_graph_data[10],
                        high_export_load_graph_data[11],
                        high_export_load_graph_data[12],
                        high_export_load_graph_data[13],
                        high_export_load_graph_data[14],
                        high_export_load_graph_data[15],
                        high_export_load_graph_data[16],
                        high_export_load_graph_data[17],
                        high_export_load_graph_data[18],
                        high_export_load_graph_data[19],
                        high_export_load_graph_data[20],
                        high_export_load_graph_data[21],
                        high_export_load_graph_data[22],
                        high_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        high_export_sol_prd_graph_data[0],
                        high_export_sol_prd_graph_data[1],
                        high_export_sol_prd_graph_data[2],
                        high_export_sol_prd_graph_data[3],
                        high_export_sol_prd_graph_data[4],
                        high_export_sol_prd_graph_data[5],
                        high_export_sol_prd_graph_data[6],
                        high_export_sol_prd_graph_data[7],
                        high_export_sol_prd_graph_data[8],
                        high_export_sol_prd_graph_data[9],
                        high_export_sol_prd_graph_data[10],
                        high_export_sol_prd_graph_data[11],
                        high_export_sol_prd_graph_data[12],
                        high_export_sol_prd_graph_data[13],
                        high_export_sol_prd_graph_data[14],
                        high_export_sol_prd_graph_data[15],
                        high_export_sol_prd_graph_data[16],
                        high_export_sol_prd_graph_data[17],
                        high_export_sol_prd_graph_data[18],
                        high_export_sol_prd_graph_data[19],
                        high_export_sol_prd_graph_data[20],
                        high_export_sol_prd_graph_data[21],
                        high_export_sol_prd_graph_data[22],
                        high_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        high_export_export_graph_data[0],
                        high_export_export_graph_data[1],
                        high_export_export_graph_data[2],
                        high_export_export_graph_data[3],
                        high_export_export_graph_data[4],
                        high_export_export_graph_data[5],
                        high_export_export_graph_data[6],
                        high_export_export_graph_data[7],
                        high_export_export_graph_data[8],
                        high_export_export_graph_data[9],
                        high_export_export_graph_data[10],
                        high_export_export_graph_data[11],
                        high_export_export_graph_data[12],
                        high_export_export_graph_data[13],
                        high_export_export_graph_data[14],
                        high_export_export_graph_data[15],
                        high_export_export_graph_data[16],
                        high_export_export_graph_data[17],
                        high_export_export_graph_data[18],
                        high_export_export_graph_data[19],
                        high_export_export_graph_data[20],
                        high_export_export_graph_data[21],
                        high_export_export_graph_data[22],
                        high_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        high_export_offset_graph_data[0],
                        high_export_offset_graph_data[1],
                        high_export_offset_graph_data[2],
                        high_export_offset_graph_data[3],
                        high_export_offset_graph_data[4],
                        high_export_offset_graph_data[5],
                        high_export_offset_graph_data[6],
                        high_export_offset_graph_data[7],
                        high_export_offset_graph_data[8],
                        high_export_offset_graph_data[9],
                        high_export_offset_graph_data[10],
                        high_export_offset_graph_data[11],
                        high_export_offset_graph_data[12],
                        high_export_offset_graph_data[13],
                        high_export_offset_graph_data[14],
                        high_export_offset_graph_data[15],
                        high_export_offset_graph_data[16],
                        high_export_offset_graph_data[17],
                        high_export_offset_graph_data[18],
                        high_export_offset_graph_data[19],
                        high_export_offset_graph_data[20],
                        high_export_offset_graph_data[21],
                        high_export_offset_graph_data[22],
                        high_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });


    }


    function show_production3(d) {
        var i = 1;
        var labels = {};
        var graph_data = [];

        var best_export_load_graph_data = d.best_export_load_graph_data;
        var best_export_sol_prd_graph_data = d.best_export_sol_prd_graph_data;
        var best_export_export_graph_data = d.best_export_export_graph_data;
        var best_export_offset_graph_data = d.best_export_offset_graph_data;

        Highcharts.chart('best_export_graph', {
            chart: {
                type: 'area',
                width: 420,
                height: 380,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 1800 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'POWER',
                    style: {
                        "fontSize": "16px",
                        "color": '#000',
                        "fontWeight": 'bold'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                    "fontSize": "9px",
                    "color": '#666',
                    "fontWeight": 'normal'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                }
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 1 * 3600 * 1000
                }
            },
            series: [{
                    name: 'Load',
                    data: [
                        best_export_load_graph_data[0],
                        best_export_load_graph_data[1],
                        best_export_load_graph_data[2],
                        best_export_load_graph_data[3],
                        best_export_load_graph_data[4],
                        best_export_load_graph_data[5],
                        best_export_load_graph_data[6],
                        best_export_load_graph_data[7],
                        best_export_load_graph_data[8],
                        best_export_load_graph_data[9],
                        best_export_load_graph_data[10],
                        best_export_load_graph_data[11],
                        best_export_load_graph_data[12],
                        best_export_load_graph_data[13],
                        best_export_load_graph_data[14],
                        best_export_load_graph_data[15],
                        best_export_load_graph_data[16],
                        best_export_load_graph_data[17],
                        best_export_load_graph_data[18],
                        best_export_load_graph_data[19],
                        best_export_load_graph_data[20],
                        best_export_load_graph_data[21],
                        best_export_load_graph_data[22],
                        best_export_load_graph_data[23]
                    ],
                    color: '#4B4B4B',
                    type: 'area',
                }, {
                    name: 'Solar Output',
                    data: [
                        best_export_sol_prd_graph_data[0],
                        best_export_sol_prd_graph_data[1],
                        best_export_sol_prd_graph_data[2],
                        best_export_sol_prd_graph_data[3],
                        best_export_sol_prd_graph_data[4],
                        best_export_sol_prd_graph_data[5],
                        best_export_sol_prd_graph_data[6],
                        best_export_sol_prd_graph_data[7],
                        best_export_sol_prd_graph_data[8],
                        best_export_sol_prd_graph_data[9],
                        best_export_sol_prd_graph_data[10],
                        best_export_sol_prd_graph_data[11],
                        best_export_sol_prd_graph_data[12],
                        best_export_sol_prd_graph_data[13],
                        best_export_sol_prd_graph_data[14],
                        best_export_sol_prd_graph_data[15],
                        best_export_sol_prd_graph_data[16],
                        best_export_sol_prd_graph_data[17],
                        best_export_sol_prd_graph_data[18],
                        best_export_sol_prd_graph_data[19],
                        best_export_sol_prd_graph_data[20],
                        best_export_sol_prd_graph_data[21],
                        best_export_sol_prd_graph_data[22],
                        best_export_sol_prd_graph_data[23]
                    ],
                    color: '#F8FF34',
                    zIndex: 1,
                    fillOpacity: 0,
                    marker: {
                        fillColor: 'yellow',
                        lineWidth: 2,
                        lineColor: 'yellow',
                        enabled: true,
                        symbol: 'circle'
                    }
                },
                {
                    name: 'Export',
                    data: [
                        best_export_export_graph_data[0],
                        best_export_export_graph_data[1],
                        best_export_export_graph_data[2],
                        best_export_export_graph_data[3],
                        best_export_export_graph_data[4],
                        best_export_export_graph_data[5],
                        best_export_export_graph_data[6],
                        best_export_export_graph_data[7],
                        best_export_export_graph_data[8],
                        best_export_export_graph_data[9],
                        best_export_export_graph_data[10],
                        best_export_export_graph_data[11],
                        best_export_export_graph_data[12],
                        best_export_export_graph_data[13],
                        best_export_export_graph_data[14],
                        best_export_export_graph_data[15],
                        best_export_export_graph_data[16],
                        best_export_export_graph_data[17],
                        best_export_export_graph_data[18],
                        best_export_export_graph_data[19],
                        best_export_export_graph_data[20],
                        best_export_export_graph_data[21],
                        best_export_export_graph_data[22],
                        best_export_export_graph_data[23]
                    ],
                    color: '#FF4B4C',
                    type: 'area',
                }, {
                    name: 'Offset Consumption',
                    data: [
                        best_export_offset_graph_data[0],
                        best_export_offset_graph_data[1],
                        best_export_offset_graph_data[2],
                        best_export_offset_graph_data[3],
                        best_export_offset_graph_data[4],
                        best_export_offset_graph_data[5],
                        best_export_offset_graph_data[6],
                        best_export_offset_graph_data[7],
                        best_export_offset_graph_data[8],
                        best_export_offset_graph_data[9],
                        best_export_offset_graph_data[10],
                        best_export_offset_graph_data[11],
                        best_export_offset_graph_data[12],
                        best_export_offset_graph_data[13],
                        best_export_offset_graph_data[14],
                        best_export_offset_graph_data[15],
                        best_export_offset_graph_data[16],
                        best_export_offset_graph_data[17],
                        best_export_offset_graph_data[18],
                        best_export_offset_graph_data[19],
                        best_export_offset_graph_data[20],
                        best_export_offset_graph_data[21],
                        best_export_offset_graph_data[22],
                        best_export_offset_graph_data[23]
                    ],
                    color: '#81B94C',
                    type: 'area',
                }
            ]


        });

    }
</script>
</body>

</html>
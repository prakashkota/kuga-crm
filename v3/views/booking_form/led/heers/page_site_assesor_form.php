<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
	</style>
</head>

<body style="padding:0; margin:0">
	<table class="pdf_page" width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" id="nomination_form_page">
		<tbody>
			<tr>
				<td>
					<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
						<tr>
							<td width="100%" style="font-size: 30px; font-weight: 700; padding-top: 20px; color:#808185;">
								Site Assessor Declaration
								<br/>
								<span style="font-size:20px;  font-weight: 500;">Home Energy Efficiency Retrofits </span>
							</td>
							<td width="100%" style="font-size: 30px;font-weight: 700;padding-top: 20px; text-align: right;">
								<img style="width:180px;" src="https://kugacrm.com.au/job_crm/assets/images/get-logo.jpg" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
						<tr>
							<td width="100%"><span style="height:3px; background:#14B37D; display:block;"></span></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td style="color:#14B37D; font-size:18px; font-weight:700; padding:10px 0 0 0;">
											Site Assessor Details
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family"	>
									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px; border: 1px solid black;">
												<tr>
													<td width="50%" >
														<table width="810px" border="1" cellpadding="0" cellspacing="0" class="font-family" style="border: 1px solid black;">
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">Name</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[name]" />
																</td>
															</tr>
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">Business name</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[business_name]" />
																</td>
															</tr>
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">ABN</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[abn]" />
																</td>
															</tr>
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">Phone number</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[contact_no]" />
																</td>
															</tr>
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">Site address</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[address]" />
																</td>
															</tr>
															<tr>
																<td width="180px" style="padding: 0px 2px 2px;">Date of site assessment</td>
																<td>
																	<input type="text" style="width: 100%;" name="site_assessor_form_details[date]" />
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="margin-top:10px; background: lightgray;">
									<tr>
										<td>
											<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
												<tr>
													<td style="color: #000;font-size: 14px;font-weight: 700;text-align: center;padding: 10px 50px 10px 50px;">
														Select all of the activities that will be implemented at the site for which the Accredited Certificate Provider is accredited to create Energy Savings Certificates.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="padding: 10px 45px 10px 45px; color:#000; font-size:13px; display:block;">
												<tr>
													<td style="width:65px; ">Activity</td>
													<td>Definition</td>
													<td style="text-align: right;">Tick which apply</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E1</td>
													<td>Replace halogen downlight with an LED luminaire and/or lamp </td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][0]" checked="" />
													</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E2</td>
													<td>Replace a linear halogen floodlight with a high efficiency lamp</td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][1]" />
													</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E3</td>
													<td>Replace parabolic aluminised reflector (PAR) lamp with efficient luminaire and/or lamp</td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][2]" />
													</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E4</td>
													<td>Replace a T8 or T12 luminaire with a T5 luminaire </td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][3]" />
													</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E5</td>
													<td>Replace a T8 or T12 luminaire with an LED luminaire</td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][4]" checked="" />
													</td>
												</tr>

												<tr>
													<td style="width:65px; padding: 5px 0px 5px 0px;">E11</td>
													<td>Replace an Edison screw or bayonet lamp with an led lamp for general lighting purposes </td>
													<td style="text-align: center;">
														<input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][5]" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>


						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding:15px 0 0 0;">
									<tr>
										<td style="color:#14B37D; font-size:18px; font-weight:700; padding:10px 0 0 0;">
											Site assessor declaration
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td style="padding:0 0 5px;">
														<p>I, <input type="text" style="width: 200px; border:0px; border-bottom:1px solid black;" name="site_assessor_form_details[name1]" /> hereby declare that:</p>
														<p>▼ I have undertaken the site assessment of the above site. </p>
														<p>▼ The activities being implemented meet the eligibility requirements specified in Schedules D and E of the ESS Rule and I have collected records to support this (as specified in the relevant table of the Home Energy Efficiency Retrofits Method Guide).</p>
														<p>▼ The information I have provided is complete and accurate and I am aware that there are penalties for providing false or misleading information in this form.</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px; border: 2px solid black;  background: lightgray;">
												<tr>
													<td style="padding:0 0 5px;">
														<p><strong>Note:</strong></p>
														<p>▼ Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading information to the Scheme Administrator.</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="padding: 15px 0 0 15px; margin-top:20px;">

									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="120px" style="padding:0 0 5px;">
														Signature
													</td>
													<td>
														<div class="signature-pad">
															<div class="signature-pad--body">
																<div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_5" data-id="5" class="sign_create">
																</div>
																<input required="" id="site_assessor_form_details_signature" name="site_assessor_form_details[signature]" type="hidden" />
																<input type="hidden" style="border: 0px; border-bottom: 1px solid black; width: 100%;"  name="site_assessor_form_details[custom_signature]" />
															</div>
														</div>
														<div class="signature-pad-image signature_image_container" style="display:none !important;">
															<img src="" />
															<a class="signature_image_close" href="javscript:void(0);"  ></a>
														</div>
													</td>
												</tr>
											</table>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="120px" style="padding:0 0 5px;     margin-top: 10px;">
														Name
													</td>
													<td>
														<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%; margin-top: 10px;" name="site_assessor_form_details[name2]" />
													</td>
													<td width="60px" style="padding:5px 0px 0px;">
														Position
													</td>
													<td>
														<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%;" name="site_assessor_form_details[position]" />
													</td>
												</tr>
											</table>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td width="120px" style="padding:0 0 5px;     margin-top: 10px;">
														Date
													</td>
													<td>
														<input type="text" style="border: 0px; border-bottom: 1px solid black; width: 100%; margin-top: 10px;" name="site_assessor_form_details[date1]" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="810px" border="0"  cellpadding="0" cellspacing="0" class="font-family" style="color:#000; font-size:13px;">
												<tr>
													<td style="padding:5px 0 5px;">
														*This form must be signed by the site assessor on or before the completion of the installation 
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="810px" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background:#6e6c6c;">
						<tr>
							<td align="right" style="font-size:12px; color:#fff; padding:5px 15px;">© Green Energy Trading Pty Ltd <?php echo date('Y'); ?> </td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
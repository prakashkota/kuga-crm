<!-- Modal -->
<div class="modal fade system_losses_model" id="lossesModel" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="lossesModelLabel">System Losses</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">

                        <div id="pv_system_losses_details" class="pv_system_losses_details">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Soiling (%):<div class=soiling_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#soiling_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="nu" class="system_losses_model_val" autocomplete="off" name="system_losses[soiling]" value="3" oldvalue="3" /></td>
                                    </tr>
                                    <tr>
                                        <th>Shading (%):<div class=shading_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#shading_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[shading]" value="3" oldvalue="3" /></td>
                                    </tr>
                                    <tr>
                                        <th>Snow (%):<div class="snow_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#snow_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[snow]" value="0" oldvalue="0" /></td>
                                    </tr>
                                    <tr>
                                        <th>Mismatch (%):<div class="mismatch_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#mismatch_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[mismatch]" value="2" oldvalue="2" /></td>
                                    </tr>
                                    <tr>
                                        <th>Wiring (%):<div class="wiring_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#wiring_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[wiring]" value="5.5" oldvalue="5.5" /></td>
                                    </tr>
                                    <tr>
                                        <th>Connections (%):<div class="connections_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#connections_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[connections]" value="0.5" oldvalue="0.5" /></td>
                                    </tr>
                                    <tr>
                                        <th>Light-Induced Degradation (%):<div class=degradation_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#degradation_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[light_degradation]" value="1.5" oldvalue="1.5" /></td>
                                    </tr>
                                    <tr>
                                        <th>Nameplate Rating (%):<div class="nameplate_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#nameplate_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[nameplate_rating]" value="1" oldvalue="1" /></td>
                                    </tr>
                                    <tr>
                                        <th>Age (%):<div class="age_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#age_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[age]" value="0" oldvalue="0" /></td>
                                    </tr>
                                    <tr>
                                        <th>Availability (%):<div class="availability_system_losses" style="cursor:pointer;display: inline-block;" data-toggle="modal" data-target="#availability_system_losses"><i class="fas fa-info-circle"></i></div>
                                        </th>
                                        <td><input type="text" class="system_losses_model_val" autocomplete="off" name="system_losses[avilability]" value="3" oldvalue="3" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="pv_estimated_system_losses mt-3">
                            <h3 class="p-0">Estimated System Losses</h3>

                            <input type="hidden" value="" id="totoal_estimated_losses_value">
                            <strong class="" id="totoal_estimated_losses">17.99%</strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="estimated_losses_reset">Reset</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="estimated_losses_save">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- system looses model end -->




<!-- Modal -->
<div class="modal fade system_losses_model" id="pv_ac_dc_ratio_info" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="pv_ac_dc_ratio_infoModelLabel">DC to AC Size Ratio</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>The DC to AC size ratio is the ratio of the array's DC rated size to the inverter's AC rated size. For the default value of 1.2, a 4 kW system size would be for a 4 DC kW array at standard test conditions (STC) and 4 DC kW / 1.2 = 3.33 AC kW inverter.</p>
                        <p>For a system with a high DC to AC size ratio, for times when the array's DC power output exceeds the inverter's rated DC input power, the inverter limits the array's power output by increasing the DC operating voltage, which moves the array's operating point down its current-voltage (I-V) curve. PVWatts® models this effect by limiting the inverter\'s power output to its rated AC size. You can see that in the hourly results, by clicking the Hourly link below the table of Results on the Results page.</p>
                        <p>The default value of 1.20 is reasonable for most systems. A typical range is 1.10 to 1.25, although some large-scale systems have ratios as high as 1.50. The optimal value depends on the system's location, array orientation, and module and inverter costs.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- system looses model end -->

<!-- Modal -->
<div class="modal fade system_losses_model" id="pv_ground_coverage_info" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="ppv_ground_coverage_infoModelLabel">Ground Coverage Ratio</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>The ground coverage ratio (GCR) is the ratio of module surface area to the area of the ground or roof occupied by the array. In PVWatts®, the GCR applies only to arrays with one-axis tracking. A GCR of 0.5 means that when the modules are horizontal, half of the surface below the array is occupied by the array. An array with wider spacing between rows of modules has a lower GCR than one with narrower spacing. A GCR of 1 would be for an array with no space between modules, and a GCR of 0 for infinite spacing between rows. The default value is 0.4, and typical values range from 0.3 to 0.6.</p>
                        <p>PVWatts® uses the GCR value to calculate self-shading losses caused by shading of neighboring rows of modules in the array. See also Shading Loss Category.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- system looses model end -->


<!-- Modal -->
<div class="modal fade system_losses_model" id="pv_ground_coverage_info" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="ppv_ground_coverage_infoModelLabel">Ground Coverage Ratio</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>The ground coverage ratio (GCR) is the ratio of module surface area to the area of the ground or roof occupied by the array. In PVWatts®, the GCR applies only to arrays with one-axis tracking. A GCR of 0.5 means that when the modules are horizontal, half of the surface below the array is occupied by the array. An array with wider spacing between rows of modules has a lower GCR than one with narrower spacing. A GCR of 1 would be for an array with no space between modules, and a GCR of 0 for infinite spacing between rows. The default value is 0.4, and typical values range from 0.3 to 0.6.</p>
                        <p>PVWatts® uses the GCR value to calculate self-shading losses caused by shading of neighboring rows of modules in the array. See also Shading Loss Category.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- system looses model end -->



<!-- Soiling -->
<div class="modal fade system_losses_model" id="soiling_system_losses" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="soiling_system_lossesModelLabel">Soiling</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Losses due to dirt and other foreign matter on the surface of the PV module that prevent solar radiation from reaching the cells. Soiling is location- and weather-dependent. There are greater soiling losses in high-traffic, high-pollution areas with infrequent rain. For northern locations, snow reduces the energy produced, depending on the amount of snow and how long it remains on the PV modules. NREL continues to work on improving the modeling of soiling and snow and is working to include historical datasets as available.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- shading -->
<div class="modal fade system_losses_model" id="shading_system_losses" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="shading_system_lossesModelLabel">Shading</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Reduction in the incident solar radiation from shadows caused by objects near the array such as buildings or trees, or by self-shading for modules arranged in rows when modules in one row cause shadows on those in an adjacent row. The default value of 1% represents an array with little or no shading. Shading analysis tools can determine a loss percentage for shading by nearby objects.</p>
                        <p>For one-axis trackers, PVWatts® calculates self-shading losses using the Ground Coverage Ratio you specify under Advanced Parameters, so you should not use the shading loss to account for self-shading with the one-axis tracking option.</p>
                        <p>For fixed arrays or arrays with two-axis tracking, you can use the graph below to estimate a self-shading loss percentage when you know the ground coverage ratio (GCR). The GCR is defined as the ratio of the array area to the ground or roof area occupied by the array.</p>
                        <img src="<?php echo base_url() . 'assets/images/shading_system_losses.JPG'; ?>" alt="A graph that shows specific shading derate factors as a function of tracking type, tilt angle, and ground cover ratio. If you are visually impaired and cannot read this graph, select the Need Help link at the bottom of the page for assistance." style="display: block; margin: 0 auto;">
                        <p>Shading loss versus ground cover ratio (GCR) for different tracking options and tilt angles at a latitude of approximately 35 degrees North.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- shading -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="snow_system_losses" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="snow_system_lossesModelLabel">Shading</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Reduction in the system's annual output due to snow covering the array. The default value is zero, assuming either that there is never snow on the array, or that the array is kept clear of snow.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="mismatch_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="mismatch_system_lossesModelLabel">Mismatch</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Electrical losses due to slight differences caused by manufacturing imperfections between modules in the array that cause the modules to have slightly different current-voltage characteristics. The default value of is 2%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="wiring_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="wiring_system_lossesModelLabel">Wiring</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Resistive losses in the DC and AC wires connecting modules, inverters, and other parts of the system. The default value is 2%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="connections_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="connections_system_lossesModelLabel">Connections</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Resistive losses in electrical connectors in the system. The default value is 0.5%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="degradation_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="degradation_system_lossesModelLabel">Light-Induced Degradation</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Effect of the reduction in the array's power during the first few months of its operation caused by light-induced degradation of photovoltaic cells. The default value is 1.5%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="nameplate_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="nameplate_system_lossesModelLabel">Nameplate Rating</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>The nameplate rating loss accounts for the accuracy of the manufacturer's nameplate rating. Field measurements of the electrical characteristics of photovoltaic modules in the array may show that they differ from their nameplate rating. A nameplate rating loss of 5% would indicates that testing yielded power measurements at Standard Test Conditions (STC) that were 5% less than the manufacturer\'s nameplate rating. The default value is 1%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="age_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="age_system_lossesModelLabel">Age</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Effect of weathering of the photovoltaic modules on the array's performance over time. The default value is zero.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="availability_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="availability_system_lossesModelLabel">Availability</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>Reduction in the system's output cause by scheduled and unscheduled system shutdown for maintenance, grid outages, and other operational factors. The default value is 3%.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->

<!-- Soiling -->
<div class="modal fade system_losses_model" id="system_system_losses" data-backdrop=" static" data-keyboard="false" tabindex="-1" aria-labelledby="lossesModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="" id="system_system_lossesModelLabel">Age</h3>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p>The system losses account for performance losses you would expect in a real system that are not explicitly calculated by the PVWatts® model equations.</p>
                        <p>The default value for the system losses of 14% is based on the categories in the table below, and calculated as follows:</p>

                        <img src="<?php echo base_url() . 'assets/images/system_losses_calcuation_info.JPG'; ?>" alt="" style="display:  block; margin: 0 auto;">
                        <p>The Inverter's DC-to-AC Conversion Efficiency is a separate input under Advanced Parameters with a default value of 96%. Do not include inverter conversion losses in the system loss percentage.</p>
                        <p>PVWatts® calculates temperature-related losses as a function of the cell temperature, so you should not include temperature loss in the system loss percentage. See the Technical Reference for details.</p>
                        <p>If you want to use a value other than the default, you can either type a new value, or use the calculator specify values for the categories listed in the table below. For a description of the categories, see System Losses Categories.</p>

                        <img src=" <?php echo base_url() . 'assets/images/system_losses_info_img.JPG'; ?>" alt="" style="display: block; margin: 0 auto;">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Soiling -->
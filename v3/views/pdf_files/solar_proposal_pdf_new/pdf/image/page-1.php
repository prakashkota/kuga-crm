<html>
<head>
    <title>Solar Proposal</title>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }
        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
            url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }
        body{
            font-family: 'CenturyGothic';
            margin: 0;
            padding: 0;
        }
        .back-bg{
            background-image: url("https://kugacrm.com.au/assets/solar_proposal_pdf_new/image/bg-1.png");
            background-color: #de1e29;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
        }
        .back-bg-main {
            background-color: white;
            background-repeat: no-repeat;
            padding: 0;
            background-size: 100% 100%;
        }

        .page1{
            height: 1287px;
            border-bottom: #fff8f9;
        }

        .page1  .title h1 {
            font-size: 58px;
            color: #fff;
            padding-top:606px;
            padding-left:20px;
        }
        .page1  hr {
            width: 170px;
            height: 7px;
            background-color: #d8d4d2;
            margin-left: 20px;
            margin-top: -17px;
        }
        .page1  .title h3 {
            font-size:20px;
            color: #fff;
            margin-left: 17px;
            margin-top: 53px;
        }
        .page1  .title h6 {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 19px;
            margin-top: 27px;
            font-weight: 400;
        }
        .page1  .title p {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 19px;
            margin-top: -29px;
        }
        .page1  .pretitlee h3 {
            font-size: 17px;
            color: #fff;
            margin-left: 358px;
            margin-top: -117px;
        }
        .page1  .pretitlee h6 {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 355px;
            margin-top: 27px;
        }
        .page1  .pretitlee p {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 356px;
            margin-top: -29px;
        }
        .page1  .zdate h5 {
            font-size: 17px;
            color: #fff;
            margin-left: 18px;
        }
        .page1  .zdate h5 span {
            font-size: 14px;
            color: #f4f9f6;
            margin-left: 18px;
        }
        .page1 .ruga img {
            width: 400px;
            height: 150px;
            margin-left: 450px;
            margin-top: -11px;
        }
    </style>
</head>
<body>
<table width="910"  border="0" cellpadding="0" cellspacing="0" class="page1 back-bg">
    <tr>
        <td>
            <div class="title">
                <h1><?php echo ($proposal_data['total_system_size']); ?> kW <br>SOLAR <br>PROPOSAL</h1>
                <hr>
            </div>
            <div class="">
                <div class="title">
                    <h3>CUSTOMISED FOR</h3>
                    <h6><?php echo $lead_data['customer_company_name']; ?></h6>
                    <p><?php echo $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?><br/>
                        <?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></p>
                </div>
                <div class="pretitlee">
                    <h3>PREPARED BY</h3>
                    <h6><?php echo $lead_data['full_name']; ?></h6>
                    <p><?php echo $lead_data['company_contact_no']; ?>
                        <br> <?php echo $lead_data['user_email']; ?>
                    </p>
                </div>
            </div>
            <div class="zdate">
                <h5>DATE PREPARED <span><br /><?php echo date_format(date_create($proposal_data['created_at']), "l, jS F Y"); ?></span></h5>
            </div>
        </td>
    </tr>
    <tr>
        <td class="ruga">
            <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>">
        </td>
    </tr>
</table>
</body>
</html>
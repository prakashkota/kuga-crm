<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
	</style>
</head>

<body style="padding:0; margin:0">
	
	<!-- Page 1 -->
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td valign="top"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/01.jpg" width="910" height="1287" alt=""/></td>
			</tr>
		</tbody>
	</table>


	<!-- Page 2 -->

	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/header.jpg" width="910" height="127" alt=""/></td>
			</tr>
			<tr>
				<td height="45"></td>
			</tr>
			<tr>
				<td><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 1 - Accredited Certificate Provider Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="400" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ACP (company)</td>
												</tr>
												<tr>
													<td><input readonly="" type="text" name="input" value="Kuga Electrical Pty Ltd" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td width="400" valign="top"><table align="right" width="370" border="0"  cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact Name</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="Louie Latumbo" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ABN</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="39 616 409 584" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact email</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="l.latumbo@13kuga.com.au" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">RESA accreditation</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Contact phone</td>
												</tr>
												<tr>
													<td><input readonly="" type="text"  value="" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 2 - Original Energy Saver Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="400" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Name</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][entity_name]" type="text"  value="<?php echo $energy_saver_details->entity_name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td width="400" valign="top"><table align="right" width="370" border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Phone Number</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][contact_no]" type="text"  value="<?php echo $energy_saver_details->contact_no; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">Email</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][email]" type="text"  value="<?php echo $energy_saver_details->email; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
										<td align="right" valign="top"><table width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:25px">ABN Number</td>
												</tr>
												<tr>
													<td><input name="get_nomination_form_details[energy_saver_details][abn]" type="text"  value="<?php echo $energy_saver_details->abn; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 3 - Implementation Details</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:2px"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.1 - Implementation Sites</td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; height:25px">Address</td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][address]" type="text"  value="<?php echo $certificate_provider_details->address; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
									<tr>
										<td height="7"></td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][address1]" type="text"  value="<?php echo $certificate_provider_details->address1; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="13"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.2 - Energy saving activities implemented</td>
						</tr>
						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; height:25px">Description</td>
									</tr>
									<tr>
										<td><input name="get_nomination_form_details[certificate_provider_details][description]" type="text"  value="<?php echo $certificate_provider_details->description; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="13"></td>
						</tr>
						<tr>
							<td height="30" style="color: #922a27; font-size:18px;">Section 3.3 - Past activities implemented at the site(s)</td>
						</tr>
						<tr>
							<td valign="top">
								<table width="800" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td style="font-size:12px; color:#6a6a6a; height:25px" width="701">Have you or, to the best of your knowledge, has any other person, previously nominated an energy saver for this activity
											or activities at this site(s)?</td>
											<td width="99">
												<table width="99" border="0" cellspacing="0" cellpadding="0">
													<tbody>
														<tr>
															<td width="33" height="30" style="font-size:12px; color:#6a6a6a;">
															<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '1'){ ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } else {  ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } ?>
															</td>
															<td width="66" style="font-size:12px; color:#6a6a6a;">Yes</td>
														</tr>
														<tr>
															<td style="font-size:12px; color:#6a6a6a;">
																<?php if(isset($past_activities->is_nominated) && $past_activities->is_nominated == '0'){ ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox_active.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } else {  ?>
																<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/checkbox.jpg" width="22" height="22" style="border:0; vertical-align:middle" alt=""/>
															<?php } ?>
															</td>
															<td style="font-size:12px; color:#6a6a6a;">No</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>

							</td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td height="25" style="font-size:12px; color:#6a6a6a;">If yes, please provide a reason for the new nomination form</td>
						</tr>
						<tr>
							<td><input type="text"  name="get_nomination_form_details[past_activities][reason]" value="<?php echo $past_activities->reason; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:800px; height:35px; padding-left:5px; padding-right:5px"/></td>
						</tr>
						<tr>
							<td height="90"></td>
						</tr>
					</tbody>
				</table></td>
			</tr>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/bottom.jpg" width="910" height="102" alt=""/></td>
			</tr>
		</tbody>
	</table>


	<!-- Page 3 -->

	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/header.jpg" width="910" height="127" alt=""/></td>
			</tr>
			<tr>
				<td height="35"></td>
			</tr>
			<tr>
				<td valign="top"><table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="40" style="color: #922a27; font-size:20px;"><strong>Section 4 - Nomination by the original energy saver</strong></td>
						</tr>
						<tr>
							<td style="border-top: solid 1px #969697; height:10px"></td>
						</tr>
						<tr>
							<td height="50"><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="170"style="font-size:12px; color:#000000;">I, the original energy saver,</td>
										<td width="632"><input type="text" name="get_nomination_form_details[energy_saver_details][name]" value="<?php echo $energy_saver_details->name; ?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:250px; height:35px; padding-left:5px; padding-right:5px"/> <span style="font-size:12px; color:#000000;">nominate <b>Kuga Electrical Pty Ltd</b> as the energy saver in</span> </td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">respect of the activity or activities at the implementation sites listed above.</td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">For the purpose of creating Energy Savings Certificates in the ESS, this nomination entitles Kuga Electrical Pty Ltd to create and own all<br />
								energy savings in respect of each of the above activities at the implementation sites listed above, once they have been completed and<br />
							energy savings have commenced.</td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">This nomination takes effect from the signature date below, and remains in force until the date this nomination is revoked by the original<br />
							energy saver.</td>
						</tr>
						<tr>
							<td height="20"></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">I hereby declare that:</td>
						</tr>

						<tr>
							<td><table width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="16"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td width="784" style="font-size:12px; color:#000000;">I have received the contact details of the Accredited Certificate Provider.</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 5px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td valign="top" style="font-size:12px; color:#000000;">Except as set out above, I have not previously nominated an energy saver under the NSW Energy Savings Scheme (ESS) in respect of the<br />
										activity or activities at the implementation sites listed above and neither, to the best of my knowledge, has any other person.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td style="font-size:12px; color:#000000;">I agree to provide information regarding the above activities and provide reasonable access to the implementation sites to a<br />
										representative of the ESS or a member of the ESS Audit Services Panel, upon request.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot-space.jpg" width="4" height="7" alt=""/></td>
										<td style="font-size:12px; color:#000000;">The date recorded below is the date when I signed this form.</td>
									</tr>
									<tr>
										<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot.jpg" width="4" height="4" alt=""/></td>
										<td style="font-size:12px; color:#000000;">The information provided in this nomination form is accurate and is not misleading by inclusion or omission.</td>
									</tr>
									<tr>
										<td valign="top" style="padding-top: 3px"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/dot-space.jpg" width="4" height="7" alt=""/></td>
										<td style="font-size:12px; color:#000000;">I am aware that this nomination form contains information that may be provided to the Scheme Administrator and that there are<br />
										penalties for providing false or misleading information to the Scheme Administrator.</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
						<tr>
							<td height="30"></td>
						</tr>
						<tr>
							<td style="font-size: 12px; color: #000000"><strong>Note:</strong></td>
						</tr>
						<tr>
							<td style="font-size:12px; color:#000000;">Section 158 of the Electricity Supply Act 1995 imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly<br />
							providing false or misleading information to the Scheme Administrator.</td>
						</tr>
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td style="font-size: 12px; color: #000000"><strong>Signed by the original Energy Saver’s Authorised signatory</strong></td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td valign="top"><table align="left" width="800" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td valign="top"><table  align="left"width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Signature</td>
												</tr>
												<tr>
													<td  valign="top" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height: 70px; ">
														<img style="width:370px; height:70px;" src="<?php echo site_url('assets/uploads/led_booking_form_files/'.$authorised_details->signature); ?>" width="70px" />
													</td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td><table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="font-size:12px; color:#6a6a6a; height:20px">Name</td>
												</tr>
												<tr>
													<td><input type="text" name="get_nomination_form_details[authorised_details][name]" value="<?php echo $authorised_details->name;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
												</tr>
											</tbody>
										</table></td>
									</tr>
									<tr>
										<td>
											<table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td style="font-size:12px; color:#6a6a6a; height:20px">Position</td>
													</tr>
													<tr>
														<td><input name="get_nomination_form_details[authorised_details][position]" type="text"  value="<?php echo $authorised_details->position;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table align="left" width="370" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr>
														<td style="font-size:12px; color:#6a6a6a; height:20px">Date</td>
													</tr>
													<tr>
														<td><input name="get_nomination_form_details[authorised_details][date]" type="text"  value="<?php echo $authorised_details->date;?>" style="background:#f1f1f2; border:solid 1px #cecdcd; width:370px; height:35px; padding-left:5px; padding-right:5px"/></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td style="font-size:12px; color:#6a6a6a; padding-top:5px;">*This form must be signed by the authorised signatory <span style="font-style: italic">on or before</span> the completion of the installation</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
					</tbody>
				</table></td>
			</tr>
			<tr>
				<td height="202"></td>
			</tr>
			<tr>
				<td><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/bottom.jpg" width="910" height="102" alt=""/></td>
			</tr>
		</tbody>
	</table>



	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td align="center">
					<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/07_1.jpg" alt=""  />
				</td>
			</tr>
		</tbody>
	</table>


	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td align="center"><img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/08_1.jpg" alt="" /></td>
			</tr>
		</tbody>
	</table>


	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
		<tbody>
			<tr>
				<td align="center">
					<img src="<?php echo $this->config->item('live_url'); ?>assets/nomination_form/09_1.jpg" alt=""   />
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
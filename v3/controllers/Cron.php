<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Cron Controller
 * @version 1.0
 */
class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('../core/input');
        $this->load->library('Components');
        $this->load->library("Notify");
        $this->load->library("Email_Manager");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Cron_Model", "cron");
        date_default_timezone_set('Australia/Sydney');
    }

    /**
     * Function to insert solrrun weblead to crm
     * return boolean
     */
    public function push_weblead_to_crm() {
        /*         * $enquiry_data = Array
          (
          'id' => 1194,
          'first_name' => 'John',
          'last_name' => 'Do',
          'referrer' => '',
          'email' => 'john.qa.do@gmail.com',
          'phone' => '0467739332',
          'address' => '1/72 Brewer rd, Bentleigh',
          'post_code' => '3204',
          'suburb' => 'Bentleigh',
          'source' => 'https://www.solarrun.com.au/solar-panels/?gclid=Cj0KCQiA68bhBRCKARIsABYUGiefI-1CMvI-kBx7zXBcs-wovmIxw6czapoA9WgDHkU9rT1saVxulYIaAhIbEALw_wcB',
          'roof_type' => 'Tile',
          'preferred_system' => '4kW',
          'comment' => 'Would like a quote for a battery ready system',
          'enquiry_about' => '{"product":"50% OFF 4kW Solar Panel","price":"$2,100", "misc":["VIC ONLY $2,100 REBATE"]}',
          'created' => '2019-01-07 17:10:15',
          'updated_at' => '0000-00-00 00:00:00',
          'lead_date' => '2019-01-07 17:10:15',
          'lead_allocation_status' => 0
          ); */

//  $enquiry_data = $this->cron->fetch_kuga_web_enquiry(1381);
//             if (empty($enquiry_data)) {
//                 return false;
//             }
//             //If Franchise enquiry dont continue
//             if($enquiry_data['source'] == 'https://www.solarrun.com.au/franchise-for-sale/'){
//                 $this->cron->update_kuga_web_data('enquiry', array('id' => $enquiry_data['id']), array('lead_allocation_status' => 3,'updated_at' => date('Y-m-d H:i:s')));
//                 error_log("SOLAR CRM WEBLEAD: Franchise Enquiry", 0);
//                 return false;
//             }
//             //Set CRM ENTRY IN PROGRESS
//             $this->cron->update_kuga_web_data('enquiry', array('id' => $enquiry_data['id']), array('updated_at' => date('Y-m-d H:i:s')));
//             $stat = $this->save_customer($enquiry_data);
//             $stat = ($stat == TRUE) ? 3 : 1;
//             //Set NEW STATE
//             $this->cron->update_kuga_web_data('enquiry', array('id' => $enquiry_data['id']), array('lead_allocation_status' => $stat, 'updated_at' => date('Y-m-d H:i:s')));
//             error_log("SOLAR CRM WEBLEAD:" . $stat, 0);
// die;
        if ($this->input->is_cli_request()) {
            log_message('error', '------------------------------------------- Cron Initiated ----------------------------------------------');
            //1. Insert customer
            //2. Create Lead
            //3. Assign Franchise to lead

            /**
             * Lead_allocation_status
             * 0 - CRM ENTRY NOT PERFORMED
             * 1 - CRM ENTRY SUCCESFFUL
             * 2 - CRM ENTRY IN PROGRESS
             * 3 - CRM ENTRY FAILED
             */
            $enquiry_data = $this->cron->fetch_kuga_web_enquiry();
            //print_r($enquiry_data);die;
            if (empty($enquiry_data)) {
                log_message('error', 'Enquiry Data is empty');
                log_message('error', '------------------------------------------- Cron Ended ----------------------------------------------');
                return false;
            }
            log_message('error', 'Enquiry Data : ');
            log_message('error', json_encode($enquiry_data));

            //Set CRM ENTRY IN PROGRESS
            log_message('error', 'Setting CRM ENTRY IN PROGRESS');
            $this->cron->update_kuga_web_data('wp_booknow', array('id' => $enquiry_data['id']), array('updated_at' => date('Y-m-d H:i:s')));
            log_message('error', 'Start customer creation and lead insertion');
            $stat = $this->save_customer($enquiry_data);
            $stat = ($stat == TRUE) ? 3 : 1;

            //Set NEW STATE
            log_message('error', 'lets update enquiry table status to 1 that lead is alloacted');
            $this->cron->update_kuga_web_data('wp_booknow', array('id' => $enquiry_data['id']), array('lead_allocation_status' => $stat, 'updated_at' => date('Y-m-d H:i:s')));
            log_message('error', '------------------------------------------- Cron Ended ----------------------------------------------');
            error_log("SOLAR CRM WEBLEAD:" . $stat, 0);
        } else {
            error_log("Cron: Unauthorized Access", 0);
        }
    }

    //1. Insert customer
    public function save_customer($enquiry_data = [],$lSource = 'Kuga Lead') {
        $error              = FALSE;

        //Postcode
        $postcode  = (isset($enquiry_data['postcode'])) ? (int) $enquiry_data['postcode'] : 3173;
        $user_ids = array();
        /**if(isset($enquiry_data['state']) && $enquiry_data['state'] == 'New South Wales'){
            $user_ids[0] = 7; //Jamie
        }else{
            $user_ids[0] = 5; //Clement
        }*/
        
        //$user_ids[0] = 5; //Clement
        
        $user_ids[0] = 8; //Grant Jameison
        
        $users = $this->customer->get_user_list($postcode);
        if(!empty($users)){
            $user_ids[0] = $users[0]['id'];
        }
        
        //Customer Data
        $customer_insert_data                        = array();
        $customer_insert_data['first_name']          = (isset($enquiry_data['fname'])) ? $enquiry_data['fname'] : '';
        $customer_insert_data['last_name']           = (isset($enquiry_data['lname'])) ? $enquiry_data['lname'] : '';
        $customer_insert_data['customer_email']      = (isset($enquiry_data['email'])) ? $enquiry_data['email'] : '';
        $customer_insert_data['customer_contact_no'] = (isset($enquiry_data['phone'])) ? $enquiry_data['phone'] : '';
        $customer_insert_data['company_name']        = (isset($enquiry_data['company']) && $enquiry_data['company'] != '') ? $enquiry_data['company'] : $customer_insert_data['first_name'] . ' ' . $customer_insert_data['last_name'];

        //Customer Site Data
        $site_insert_data              = array();
        $site_insert_data['site_name']   = (isset($enquiry_data['company_address'])) ? $enquiry_data['company_address'] : '';
        $site_insert_data['address']   = (isset($enquiry_data['company_address'])) ? $enquiry_data['company_address'] : '';
        $site_insert_data['state_id']  = (isset($enquiry_data['company_state'])) ? $enquiry_data['company_state'] : $this->components->get_region_by_postcode($postcode);
        $site_insert_data['postcode']  = $postcode;
        
        $quote_type = "";
        if(isset($enquiry_data['quote_type']) && $enquiry_data['quote_type'] != NULL){
            $quote_type = $enquiry_data['quote_type'];
        }
                
        //Check Customer Validation Before Saving:
        $validation_data               = array();
        if($quote_type == "residential"){
            $validation_data['customer_email']      = (isset($enquiry_data['email']) && $enquiry_data['email'] != '') ? $enquiry_data['email'] : '';
        }else{
            $validation_data['company_name']      = (isset($enquiry_data['company']) && $enquiry_data['company'] != '') ? $enquiry_data['company'] : '';
        }
        

        $customer_id = $this->is_customer_exists($validation_data);

        if ($customer_id != '') {
            log_message('error', 'Customer already exisits customer id = ' . $customer_id);
            //If Customer already exists 
            //Currently dont know what to do on already existing
            $is_lead_exists = $this->common->fetch_row('tbl_leads', 'id', array('cust_id' => $customer_id));
            if (!empty($is_lead_exists)) {
                $notification_data                           = [];
                $notification_data['id']                     = $is_lead_exists['id'];
                $this->notify->store_notification('WEB_LEAD_FOLLOW_UP', $notification_data);
                log_message('error', 'Follow up mail send successfully'); 
            }
            return true;
        } else {
            log_message('error', 'Customer does not exists in the system so lets create a new one');
            //If Customer is new create lead and send new lead mails
            $this->common->insert_data('tbl_customers', $customer_insert_data);
            $customer_id = $this->common->insert_id();

            //Insert Site
            $site_insert_data['cust_id']    = $customer_id;
            $site_insert_data['created_at'] = date('Y-m-d H:i:s');
            $this->common->insert_data('tbl_customer_locations', $site_insert_data);

            log_message('error', 'Save customer location');

            $customer_location_data         = $this->common->fetch_where('tbl_customer_locations', '*', array('cust_id' => $customer_id));
            if ($customer_id && !empty($customer_location_data)) {
                log_message('error', 'Customer is created and location is saved now lets start lead allocation');
                $uuid                         = $this->components->uuid();
                $lead_data['uuid']            = $uuid;
                $lead_data['user_id']         = 1;
                $lead_data['cust_id']         = $customer_id;
                $lead_data['lead_source']     = $lSource;
                if(isset($enquiry_data['lead_location_type']) && $enquiry_data['lead_location_type'] != NULL){
                    $lead_data['lead_location_type'] = $enquiry_data['lead_location_type'];
                }
                if(isset($enquiry_data['lead_type']) && $enquiry_data['lead_type'] != NULL){
                    $lead_data['lead_type'] = $enquiry_data['lead_type'];
                }
                
                
                if(isset($enquiry_data['current_monthly_electricity_bill']) && $enquiry_data['current_monthly_electricity_bill'] != NULL){
                    $lead_data['current_monthly_electricity_bill'] = $enquiry_data['current_monthly_electricity_bill'];
                }
                if($lSource != 'Kuga Lead'){
                    $lead_data['lead_stage'] = 3;
                }
                $stat                         = $this->save_lead_details($lead_data, $user_ids, $enquiry_data);
                return $stat;
            } else {
                $error = TRUE;
                return $error;
            }
        }
    }

    //2. Create Lead
    public function save_lead_details($lead_data, $user_ids,$enquiry_data) {
        log_message('error', 'Into the save_lead_details, creating lead');
        $cust_id        = $lead_data['cust_id'];
        $is_lead_exists = $this->common->fetch_where('tbl_leads', '*', array('cust_id' => $cust_id));
        if (!empty($is_lead_exists)) {
            log_message('error', 'Lead already exists with this cutomer return from here');
            $error = FALSE;
            return $error;
        }
        $lead_data['created_at'] = date('Y-m-d H:i:s');
        log_message('error', 'Everything went well now creating lead');
        $stat                    = $this->common->insert_data('tbl_leads', $lead_data);
        log_message('error', 'Lead is created now lets assign to the franchise');
        $id                      = $this->common->insert_id();
        
        //Get Franchise By Postcode
        $postcode  = (isset($enquiry_data['postcode'])) ? (int) $enquiry_data['postcode'] : '';
        if(isset($postcode) && $postcode != '' && $lead_data['lead_source'] == 'Facebook Lead'){
            $fd = $this->lead->get_franchise_list($postcode,$lead_data['uuid']);
            if(!empty($fd)){
                $user_ids[0] = $fd[0]['id'];
            }
        }
        
        if ($stat) {
            log_message('error', 'Trying to assign lead to franchise');
            $stat = $this->assign_lead_to_franchise($id, $user_ids);
            return $stat;
        } else {
            log_message('error', 'LEAD CREATION GONE UNSUCCESSFULL');
        }
    }

    //3. Assign Franchise to lead
    public function assign_lead_to_franchise($lead_id, $user_ids, $follow = FALSE, $enquiry_data = []) {
        log_message('error', 'I am in last stage of the lead allocation procedure');
        $stat = TRUE;

        if (isset($lead_id) && $lead_id != '') {
            //Bulk Update Data
            $lead_user_data = $this->common->fetch_where('tbl_lead_to_user', '*', ['lead_id' => $lead_id]);
            if ($follow == TRUE) {
                //Now we need to send SMS and EMAIL to appropirate Parties
                if (!empty($lead_user_data)) {
                    $notification_data                           = [];
                    $notification_data['id']                     = $lead_id;
                    $notification_data['notification_on_mobile'] = $enquiry_data ? $enquiry_data['notification_on_mobile'] : 1;
                    $this->notify->store_notification('LEAD_FOLLOW_UP', $notification_data);
                    $status                                      = 'Franchise Follow-Up Send Successfully';
                } else {
                    $stat   = FALSE;
                    $status = 'Looks like no franchise is allocated to this lead. Please allocate first and then save.';
                }
            } else {
                $lead_user_insert_data = array();
                $lead_user_insert_data['lead_id'] = $lead_id;
                $lead_user_insert_data['user_id'] = $user_ids[0];
                $stat = $this->common->insert_data('tbl_lead_to_user', $lead_user_insert_data);
                //Now we need to send SMS and EMAIL to appropirate Parties
                $notification_data                           = [];
                $notification_data['id']                     = $lead_id;
                $this->notify->store_notification('NEW_WEB_LEAD', $notification_data);
                log_message('error', 'Franchise assigned successfully');                 
            }

            if ($stat) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }

    public function is_customer_exists($data) {
        $company_name      = $data['company_name'];
        $reason     = '';
        $flag       = true;
        if (isset($company_name) && $company_name != '') {
            $is_customer_exists = $this->common->fetch_where('tbl_customers', 'id', array('company_name' => $company_name));
            $flag               = (!empty($is_customer_exists)) ? false : true;
            $reason             = (!empty($is_customer_exists)) ? $is_customer_exists[0]['id'] : '';
        }
        return $reason;
    }
    
    public function automate_fb_leads(){
        $data = json_decode(file_get_contents('php://input'), true);
        error_log("SOLAR CRM FBLEAD:" . json_encode($data),0);
        
        
        /**{\"email\":\"e.otia@solarrun.com.au\",
        \"firstName\":\"Test\",
        \"lastName\":\"Test\",
        \"phoneNumber\":\"0432073719\",
        \"companyName\":\"Elvis\",\"currentMonthlyElectricityCosts\":\"Melbourne\"}}"
        */
        
        $enquiry_data = [];
        $enquiry_data['fname'] = isset($data['firstName']) ? $data['firstName'] : '';
        $enquiry_data['lname'] = isset($data['lastName']) ? $data['lastName'] : '';
        $enquiry_data['email'] = isset($data['email']) ? $data['email'] : '';
        $enquiry_data['phone'] = isset($data['phoneNumber']) ? $data['phoneNumber'] : '';
        
        if(isset($data['postCode'])){
            $enquiry_data['postcode'] = $data['postCode'];
        }else if(isset($data['postcode'])){
            $enquiry_data['postcode'] = $data['postcode'];
        }else{
            $enquiry_data['postcode'] = '';
        }
        $enquiry_data['company'] = isset($data['companyName']) ? $data['companyName'] : '';
        $enquiry_data['company_address'] = isset($data['companyAddress']) ? $data['companyAddress'] : '';
        $enquiry_data['company_state'] = isset($data['form_state_id']) ? $data['form_state_id'] : '';
        $enquiry_data['current_monthly_electricity_bill'] = isset($data['currentMonthlyElectricityCosts']) ? $data['currentMonthlyElectricityCosts'] : NULL;
        
        $enquiry_data['state'] = 'Victoria';
        if($enquiry_data['company_state'] == 2){
            $enquiry_data['state'] = 'New South Wales';
        }
        
        $form_id = isset($data['form_id']) ? $data['form_id'] : '';
        
        $quote_type = isset($data['quote_type']) ? strtolower($data['quote_type']) : '';
        $enquiry_data['lead_location_type'] = NULL;
        
        if($quote_type != ''){
            if (strpos($quote_type, 'residential') !== false) {
                $enquiry_data['lead_location_type'] = 1;
                if(isset($form_id) && $form_id == '3'){
                    $enquiry_data['lead_location_type'] = 3;
                }
            }else if (strpos($quote_type, 'commercial') !== false) {
                $enquiry_data['lead_location_type'] = 2;
                if(isset($form_id) && $form_id == '4'){
                    $enquiry_data['lead_location_type'] = 4;
                }
            }
        }
        
        if(isset($enquiry_data['form_id']) && ($enquiry_data['form_id'] == 1 || $enquiry_data['form_id'] == 2)){
            $enquiry_data['lead_type'] = 2;
        }
        
        $stat =  $this->save_customer($enquiry_data,'Facebook Lead');
        $stat = ($stat == TRUE) ? 3 : 1;
        
        error_log("SOLAR CRM FBLEAD STATUS:" . $stat,0);
        
        header("HTTP/1.1 200 OK");
        echo json_encode(
            array(
                'success' => true, 
                'status' => 'Data Reeived Successfully', 
                'data' => $data
            )
        );
    }
    
    public function check_franchise_for_postcode(){
        $fd = $this->lead->get_franchise_list(4723,'a6a43179-7d01-4e7e-9f35-114c56f74c3b');
        print_r($fd);die;
    }

}

<table class="pdf_page hidden" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" id="boom_requirement_page">
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="300" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"></th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Reach Up:<br>(Ceiling Height)</th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Clearance:<br>(Height of item blocking access</th>
                    <th class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Reach Access:<br>(Distance form the aisle to Light)</th>
                </tr>
                <tr>
                    <td width="300" align="center" valign="top" style="padding:13px 0; font-size:20px"><strong>Boom Requirement</strong></td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <input id="boom_reach_up" name="booking_form[boom_reach_up]" style="padding:5px; width:100%; height: inherit;" type="number">
                    </td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <input id="boom_clearence" name="booking_form[boom_clearence]" style="padding:5px; width:100%; height: inherit;" type="number">
                    </td>
                    <td align="center" valign="top" style="padding:13px 15px">
                        <input id="boom_reach_access" name="booking_form[boom_reach_access]" style="padding:5px; width:100%; height: inherit;" type="number">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="412" height="35" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Boom Requirement – Area Photo 1</th>
                    <th width="412" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px">Boom Requirement – Area Photo 2</th>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="boom_requirement_area_1" />
                            <input type="hidden" id="boom_requirement_area_1" name="booking_form_image[boom_requirement_area_1]"/>
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                    <td align="center" valign="top" style="padding:13px 0">
                        <div class="add-picture">
                            <span>Add picture</span>
                            <i><img src="<?php echo site_url(); ?>assets/images/small-plus.png"></i>
                            <input type="file" class="image_upload" data-id="boom_requirement_area_2" />
                            <input type="hidden" id="boom_requirement_area_2" name="booking_form_image[boom_requirement_area_2]" />
                        </div>
                        <div class="form-block d-block clearfix image_container" style="display:none !important; ">
                            <div class="add-picture"></div>
                            <a class="image_close" href="javscript:void(0);"  ></a>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
    </tr>
</table>
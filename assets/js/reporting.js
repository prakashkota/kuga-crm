var reporting = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;

    if (self.user_group != '1' && self.all_reports_view_permission == '0') {
        $('#statistics_container').hide();
        $('#error_container').removeClass('hidden');
        $('#reporting_actions').addClass('hidden');
        $('.page-wrapper').attr('style', 'height:100% !important;');
    } else {
        $(function () {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                document.getElementById('start_date').value = start.format('YYYY-MM-DD');
                document.getElementById('end_date').value = end.format('YYYY-MM-DD');
                self.initialize();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });

        $('.sr-show_inactive_users').click(function () {
            $('.sr-inactive_user').addClass('hidden');
            if ($(this).prop("checked") == true) {
                $('.sr-inactive_user').removeClass('hidden');
            }
        });

        $('#userid').on('change', function () {
            self.initialize();
        });
    }
    self.mark_as_completed_activity();
};


reporting.prototype.initialize = function () {
    var self = this;
    document.getElementById('statistics').innerHTML = '';
    document.getElementById('conversion_table_body').innerHTML = '';
    document.getElementById('activity_statistics').innerHTML = '';
    document.getElementById('activity_devision').innerHTML = '';
    document.getElementById('scheduled_activities_table_body').innerHTML = '';
    document.getElementById('statistics').appendChild(createLoader('Fetching statistics information....'));
    document.getElementById('conversion_table_body').appendChild(createLoader('Fetching conversion information....'));
    document.getElementById('activity_statistics').appendChild(createLoader('Fetching activity information....'));
    document.getElementById('activity_devision').appendChild(createLoader('Fetching activity devision information....'));
    document.getElementById('scheduled_activities_table_body').appendChild(createLoader('Fetching scheduled activity information....'));
    self.fetch_statistics_data();
    self.fetch_activity_data();
    if (self.user_group == '1' || self.all_reports_view_permission == '1') {
        //self.fetch_user_specific_report_data();
    }
}

reporting.prototype.create_statisitics_card = function (data) {
    var card = document.createElement('div');
    card.className = 'card';

    var card_body = document.createElement('div');
    card_body.className = 'card-body';

    var card_body_title = document.createElement('div');
    card_body_title.className = "text-center";
    card_body_title.innerHTML = "<h3>" + data.title + "</h3>"

    card_body.appendChild(card_body_title);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'No. Sales: <span class="text-success"> ' + data.deals_data[0].no_of_sales + '</span>';
    card_body.appendChild(no_of_sales);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'Total Revenue: <span class="text-success"> $' + parseFloat(data.deals_data[0].total_revenue).toFixed(2);
    +'</span>';
    card_body.appendChild(no_of_sales);

    var no_of_sales = document.createElement('div');
    no_of_sales.className = "text-muted";
    no_of_sales.innerHTML = 'Total kW\'s: <span class="text-success"> ' + parseFloat(data.deals_data[0].total_kws).toFixed(2) + ' kW</span>';
    card_body.appendChild(no_of_sales);
    card.appendChild(card_body);

    var card_container = document.createElement('div');
    card_container.className = "col-xl-4 col-lg-4 col-12";

    card_container.appendChild(card);

    return card_container;
};

reporting.prototype.create_conversion_row = function (data) {
    var no_of_new_quotes = data.stats[0].deals_data[0].no_of_sales;
    var no_of_won_quotes = data.stats[1].deals_data[0].no_of_sales;
    var no_of_lost_quotes = data.stats[2].deals_data[0].no_of_sales;
    var total_quotes = parseInt(no_of_new_quotes) + parseInt(no_of_won_quotes) + parseInt(no_of_lost_quotes);

    var tr = document.createElement('tr');
    var td1 = document.createElement('td');
    td1.innerHTML = total_quotes;

    var td2 = document.createElement('td');
    td2.innerHTML = no_of_won_quotes;

    var td3 = document.createElement('td');
    td3.className = 'text-center';
    var conv_rate = (total_quotes > 0) ? (parseFloat(no_of_won_quotes / total_quotes) * 100).toFixed(2) : 0;
    var conv_rate_class = (conv_rate > 70) ? 'bg-success' : 'bg-warning';
    td3.innerHTML = conv_rate + '%';

    var progress = document.createElement('div');
    progress.className = 'progress progress-sm mt-1 mb-0';
    var progress_bar = document.createElement('div');
    progress_bar.className = 'progress-bar ' + conv_rate_class;
    progress_bar.setAttribute('style', 'width:' + conv_rate + '%');

    progress.appendChild(progress_bar);
    td3.appendChild(progress);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);

    return tr;

};

reporting.prototype.fetch_statistics_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_statistics_data';

    $.ajax({
        url: base_url + "admin/reports/fetch_statistics_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('statistics').innerHTML = '';
            document.getElementById('conversion_table_body').innerHTML = '';
            if (data.stats.length > 0) {
                for (var i = 0; i < data.stats.length; i++) {
                    var stats = self.create_statisitics_card(data.stats[i]);
                    document.getElementById('statistics').appendChild(stats);
                }
                //Create Conversion table row
                var row = self.create_conversion_row(data);
                document.getElementById('conversion_table_body').appendChild(row);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('conversion_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

reporting.prototype.fetch_activity_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_activity_data';

    $.ajax({
        url: base_url + "admin/reports/fetch_activity_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('activity_statistics').innerHTML = '';
            document.getElementById('activity_devision').innerHTML = '';
            document.getElementById('scheduled_activities_table_body').innerHTML = '';


            google.charts.load('current', {'packages': ['corechart']});

            if (data.activity.length > 0) {
                google.charts.setOnLoadCallback(function () {
                    self.create_activity_chart(data.activity);
                });

                google.charts.setOnLoadCallback(function () {
                    self.create_activity_devision_chart(data.activity);
                });
            }
            //Create Scheduled table rows
            if (data.scheduled_activity.length > 0) {
                if ($('#scheduled_activities_table')) {
                    $('#scheduled_activities_table').DataTable().destroy();
                    document.getElementById('scheduled_activities_table_body').innerHTML = '';
                }
                for (var i = 0; i < data.scheduled_activity.length; i++) {
                    var row = self.create_scheduled_activities_data(data.scheduled_activity[i]);
                    document.getElementById('scheduled_activities_table_body').appendChild(row);
                }
                $('#scheduled_activities_table').DataTable({
                    "order": [
                        [0, "asc"]
                    ],
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": true,
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('activity_statistics').innerHTML = 'Looks Like Something Went Wrong';
            document.getElementById('scheduled_activities_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
}

reporting.prototype.create_activity_chart = function (report_data) {
    var list = [];
    list.push(["Date", "Note", "Visit", "Email", "Phone"]);
    for (var i = 0; i < report_data.length; i++) {
        list[(i + 1)] = [report_data[i].created_at, parseInt(report_data[i].note_count), parseInt(report_data[i].visit_count)
                    , parseInt(report_data[i].email_count), parseInt(report_data[i].phone_count)];
    }


    var data = google.visualization.arrayToDataTable(list, false);
    var options = {
        title: 'Actitivty Statistics'
    };
    var chart = new google.visualization.LineChart(document.getElementById('activity_statistics'));
    chart.draw(data, options);
}

reporting.prototype.create_activity_devision_chart = function (report_data) {
    var list = [];
    list.push(["Activity", "Count"]);
    for (var i = 0; i < report_data.length; i++) {
        list[(i + 1)] = [report_data[i].acitivity_type + " (" + report_data[i].count + ")", parseInt(report_data[i].count)];
    }
    var note_count = 0;
    var visit_count = 0;
    var email_count = 0;
    var phone_count = 0;


    for (var i = 0; i < report_data.length; i++) {
        note_count += parseInt(report_data[i].note_count);
        visit_count += parseInt(report_data[i].visit_count);
        email_count += parseInt(report_data[i].email_count);
        phone_count += parseInt(report_data[i].phone_count);
    }

    list[1] = ['Note', parseInt(note_count)];
    list[2] = ['Visit', parseInt(visit_count)];
    list[3] = ['Email', parseInt(email_count)];
    list[4] = ['Phone', parseInt(phone_count)];

    var data = google.visualization.arrayToDataTable(list, false);
    var options = {
        title: 'Actitivty Division'
    };
    var chart = new google.visualization.PieChart(document.getElementById('activity_devision'));
    chart.draw(data, options);
};


reporting.prototype.create_scheduled_activities_data = function (data) {
    var self = this;

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.first_name + ' ' + data.last_name;

    var td2 = document.createElement('td');
    td2.innerHTML = data.full_name;

    var td3 = document.createElement('td');
    td3.innerHTML = data.scheduled_at;

    var td4 = document.createElement('td');
    td4.innerHTML = data.activity_type;


    var badge_class = (data.status == 1) ? 'success' : ((data.days_left < 0) ? 'danger' : ((data.days_left > 0) ? 'warning' : 'info'));
    var badge_text = (data.status == 1) ? 'Completed' : ((data.days_left < 0) ? 'Past' : ((data.days_left > 0) ? 'Upcoming' : 'Today'));
    var badge = document.createElement('span');
    badge.className = 'badge badge-default badge-' + badge_class;
    badge.innerHTML = badge_text;

    var td5 = document.createElement('td');
    td5.appendChild(badge);

    var td6 = document.createElement('td');
    td6.className = "text-center";
    var mark_as_completed_btn = '<a href="javascript:void(0);" data-id="' + data.id + '" data-userid="' + data.user_id + '"  class="mark_as_completed_activity"> <i class="fa fa-check text-success"></i></a>';
    mark_as_completed_btn = (data.days_left < 0 && data.status == 0) ? mark_as_completed_btn : ((data.days_left == 0 && data.status == 0) ? mark_as_completed_btn : '-');
    td6.innerHTML = mark_as_completed_btn;

    var td7 = document.createElement('td');
    td7.className = "text-center";
    var url = base_url + 'admin/proposal/edit/' + data.uuid;
    td7.innerHTML = ((data.user_id == self.user_id) || self.user_group == 1) ? '<a href="' + url + '" target="__blank"> <i class="fa fa-pencil"></i></a>' : '-';


    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    //tr.appendChild(td6);
    //tr.appendChild(td7);

    return tr;
};

reporting.prototype.mark_as_completed_activity = function () {
    var self = this;
    $(document).on('click', '.mark_as_completed_activity', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        var userid = $this.attr('data-userid');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/proposal/mark_as_completed_proposal_activity',
            datatype: 'json',
            data: 'id=' + id + '&userid=' + userid + '&action=mark_as_completed',
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    var prev_td = $this.parent().closest('td').prev('td');
                    prev_td.html('<span class="badge badge-default badge-success">Completed</span>');
                    $this.parent().closest('td').html('-');
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

reporting.prototype.fetch_user_specific_report_data = function () {
    var self = this;
    var form_data = $('#reporting_actions').serialize();
    form_data += '&action=fetch_user_specific_report_data';

    $.ajax({
        url: base_url + "admin/reports/fetch_user_specific_report_data?" + form_data,
        type: 'get',
        dataType: "json",
        success: function (data) {
            document.getElementById('user_specific_report_table_body').innerHTML = '';
            //Create Scheduled table rows
            if (data.user_report_data.length > 0) {
                for (var i = 0; i < data.user_report_data.length; i++) {
                    var row = self.create_user_specific_report_data(data.user_report_data[i]);
                    document.getElementById('user_specific_report_table_body').appendChild(row);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('user_specific_report_table_body').innerHTML = 'Looks Like Something Went Wrong';
        }
    });
};


reporting.prototype.create_user_specific_report_data = function (data) {

    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.innerHTML = data.first_name + ' ' + data.last_name;

    var td2 = document.createElement('td');
    td2.innerHTML = (data.visit_count) ? data.visit_count : 0;

    var td3 = document.createElement('td');
    td3.innerHTML = (data.email_count) ? data.email_count : 0;

    var td4 = document.createElement('td');
    td4.innerHTML = data.no_of_sales;

    var td5 = document.createElement('td');
    td5.innerHTML = data.total_revenue;

    var td6 = document.createElement('td');
    td6.innerHTML = data.total_kws + ' kW';

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);

    return tr;
};

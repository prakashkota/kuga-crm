<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
	<title>Welcome::</title>
	<style>
		.font-family { font-family: 'Open Sans', sans-serif; }
		.table-border{ border:solid 1px #ccc;}
		@media print {
			.no-print { display: none; }
			body { background: transparent; }
			.red { color: #c32027 }
			.black-bg { background-color: #010101 }
			.dark-bg { background-color: #282828 }
		}
		p{padding: 0 0 0 0px !important; margin:5px !important;}
	</style>
</head>

<body style="padding:0; margin:0">

	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="pdf_page font-family" id="nomination_form_page">
	    <tbody>
	      <tr>
	        <td valign="top">
	          <table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
	            <tbody>
	              <tr>
	                <td height="20"></td>
	              </tr>
	              <tr>
	                <td valign="top">
	                  <table width="910" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="684" valign="top">
	                          <table width="684" border="0" cellspacing="0" cellpadding="0">
	                            <tbody>
	                              <tr>
	                                <td width="580" valign="top" style="padding-top: 8px">
	                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                    <tbody>
	                                      <tr>
	                                        <td>
	                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                            <tbody>
	                                              <tr>
	                                                <td width="110" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">NCBA RESA #</td>
	                                                <td width="50" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #000000"><input type="text" style="width: 200px; border:0px;" name="site_assessor_form_details[ncba_resa]" value=""/></td>
	                                                <td width="150" align="left" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">(office use only)</td>
	                                              </tr>
	                                            </tbody>
	                                          </table>
	                                        </td>
	                                      </tr>
	                                    </tbody>
	                                  </table>
	                                </td>
	                                <td width="380" valign="top" style="padding-top: 8px">&nbsp;</td>
	                              </tr>
	                            </tbody>
	                          </table>
	                        </td>
	                        <td width="186" align="right" valign="top">
	                          <img style="width:180px;" src="<?php echo site_url(); ?>assets/images/ncba.png" />
	                        </td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td align="center" style="font-family: Arial, sans-serif; font-size: 22px; color: #333333;"><strong>Site Assessor Declaration</strong></td>
	              </tr>
	              <tr>
	                <td height="15"></td>
	              </tr>
	              <tr>
	                <td style="font-family: Arial, sans-serif; font-size: 15px; color: #333333;">To be completed by the person who conducts the site assessment:</td>
	              </tr>
	              <tr>
	                <td height="15" style="border-bottom: solid 1px #333333"></td>
	              </tr>
	              <tr>
	                <td height="35" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>Site Assessor details</strong></td>
	              </tr>
	              <tr>
	                <td height="10" style="border-top: solid 1px #333333"></td>
	              </tr>
	              <tr>
	                <td valign="top">
	                  <table width="910" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td valign="top">
	                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tbody>
	                              <tr>
	                                <td width="226" height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #333333; border-bottom: solid 1px #333333">Name:</td>
	                                <td width="644" style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width:100%;border: none" name="site_assessor_form_details[name]" /></td>
	                              </tr>
	                              <tr>
	                                <td height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333">Business name (if applicable):</td>
	                                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width: 100%;border:none;" name="site_assessor_form_details[business_name]" value="Kuga Australia"/></td>
	                              </tr>
	                              <tr>
	                                <td height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #333333">ABN (if applicable):</td>
	                                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width: 100%;border:none;" name="site_assessor_form_details[abn]" value="39 616 409 354"/></td>
	                              </tr>
	                              <tr>
	                                <td height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #333333">Phone number:</td>
	                                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width:100%;border: none" name="site_assessor_form_details[contact_no]" /></td>
	                              </tr>
	                              <tr>
	                                <td height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #333333">Site address:</td>
	                                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width:100%;border: none" name="site_assessor_form_details[address]" /></td>
	                              </tr>
	                              <tr>
	                                <td height="30" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #333333">Date of site assessment:</td>
	                                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #333333;border-bottom: solid 1px #333333"><input type="text" style="width:100%;border: none" name="site_assessor_form_details[date]" /></td>
	                              </tr>
	                            </tbody>
	                          </table>
	                        </td>
	                      </tr>
	                    </tbody>	
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="10"></td>
	              </tr>
	              <tr>
	                <td height="30" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333"><strong>Where are the energy saving activities being undertaken?</strong></td>
	              </tr>
	              <tr>
	                <td height="5"></td>
	              </tr>
	              <tr>
	                <td>
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="50" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">At a residence <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[at_site]" value="1" /></td>
	                        <td width="307" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">At a small business <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[at_site]" value="0" /></td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="10"></td>
	              </tr>
	              <tr>
	                <td height="25">
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="543" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333"><strong>Will the proposed luminaries be installed on a dimmable circuit?:</strong></td>
	                        <td width="327">
	                          <table width="250" border="0" cellspacing="0" cellpadding="0">
	                            <tbody>
	                              <tr>
	                                <td width="64" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">Yes <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[circuit_type]" value="1" /></td>
	                                <td width="186" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">No <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[circuit_type]" value="0" /></td>
	                              </tr>
	                            </tbody>
	                          </table>
	                        </td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="25">
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="544" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333"><strong>Will any existing transformers remain in situ with the new luminaries?:</strong></td>
	                        <td width="326">
	                          <table width="250" border="0" cellspacing="0" cellpadding="0">
	                            <tbody>
	                              <tr>
	                                <td width="64" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">Yes <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[transformers_condition]" value="1" /></td>
	                                <td width="186" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333">No <input type="radio" style="width:20px; height:20px;" name="site_assessor_form_details[transformers_condition]" value="0" /></td>
	                              </tr>
	                            </tbody>
	                          </table>
	                        </td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	               <td style="font-family: Arial, sans-serif; font-size: 15px; color: #333333"><strong>Select all of the activities that will be implemented at the site for which the Accredited Certificate Provider is accredited to 
	               create Energy Savings Certificates.</strong></td>
	             </tr>
	             <tr>
	               <td height="10" style="border-bottom: solid 1px #333333"></td>
	             </tr>
	              <tr>
	                <td>
	                  <table width="910" border="0" cellspacing="0" cellpadding="4">
	                    <tbody>
	                      <tr>
	                        <td width="186" height="30" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333; border-bottom: solid 1px #333333"><strong>Activity Definitions</strong></td>
	                        <td width="540" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333;border-bottom: solid 1px #333333"><strong>Name of Activity</strong></td>
	                        <td width="144" style="font-family: Arial, sans-serif; font-size: 15px; color: #333333;border-bottom: solid 1px #333333"><strong>Tick one or more</strong></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>D3</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace an existing air conditioner with a high efficiency air conditioner</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][0]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>D4</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Install a high efficiency air conditioner</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][1]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E1</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a halogen downlight with a LED luminaire and/or lamp</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][2]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E2</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a linear halogen floodlight with a high efficiency lamp</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][3]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E3</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a parabolic aluminised reflector (PAR) lamp with an efficient luminaire and/or lamp</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][4]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E4</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a T8 or T12 luminaire with a T5 luminaire</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][5]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E5</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a T8 or T12 luminaire with a LED luminaire</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][6]"/></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E11</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace an Edison screw or bayonet lamp with a LED lamp for general lighting purposes</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][7]" /></td>
	                      </tr>
	                      <tr>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><strong>E13</strong></td>
	                        <td style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;">Replace a T5 luminaire with a LED luminaire</td>
	                        <td align="center" style="border-bottom: solid 1px #333333;font-family: Arial, sans-serif; font-size: 15px; color: #333333;"><input type="checkbox" style="width:20px; height:20px;" name="site_assessor_form_details[is_activities][8]"/></td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="15"></td>
	              </tr>
	              <tr>
	                <td style="font-family: Arial, sans-serif; font-size: 15px; color: #000000"><strong>Site assessor declaration:</strong></td>
	              </tr>
	              <tr>
	                <td height="10"></td>
	              </tr>
	              <tr>
	                <td>
	                  <table width="910" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="16" style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">I,</td>
	                        <td width="248" style="font-family: Arial, sans-serif; font-size: 15px; color: #000000;border-bottom: solid 1px #666666"><input type="text" style="border:0px;" name="site_assessor_form_details[name1]" /></td>
	                        <td width="606" style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">hereby declare that:</td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="10"></td>
	              </tr>
	              <tr>
	                <td>
	                  <table width="910" border="0" cellspacing="7" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="18" align="center" valign="top"><img src="<?= base_url('assets/site_assessor_form/dot.jpg')?>" width="7" height="6" alt=""/></td>
	                        <td width="831" style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">I have undertaken the site assessment of the above site.</td>
	                      </tr>
	                      <tr>
	                        <td align="center" valign="top"><img src="<?= base_url('assets/site_assessor_form/dot.jpg')?>" width="7" height="6" alt=""/></td>
	                        <td style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">I have documented and attached a list of all existing equipment at the site that is to be modified or replaced for the purposes of generating energy savings certificates</td>
	                      </tr>
	                      <tr>
	                        <td align="center" valign="top"><img src="<?= base_url('assets/site_assessor_form/dot.jpg')?>" width="7" height="6" alt=""/></td>
	                        <td style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">The activities being implemented meet the eligibility requirements specified in Schedules D and E of the ESS Rule and I have collected records to support this (as specified in the relevant table of the Home Energy Efficiency Retrofits Method Guide).</td>
	                      </tr>
	                      <tr>
	                        <td align="center" valign="top"><img src="<?= base_url('assets/site_assessor_form/dot.jpg')?>" width="7" height="6" alt=""/></td>
	                        <td style="font-family: Arial, sans-serif; font-size: 15px; color: #000000">The information I have provided is complete and accurate and I am aware that there are penalties for providing false or misleading information in this form.</td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td height="10" style="border-bottom: solid 1px #333333"></td>
	              </tr>
	              <tr>
	                <td height="5"></td>
	              </tr>
	              <tr>
	                <td height="20" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000"><strong>Note:</strong></td>
	              </tr>
	              <tr>
	                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000">Section 158 of the <em>Electricity Supply Act 1995</em> imposes a maximum penalty of $11,000 and/or six (6) months imprisonment for knowingly providing false or misleading information to the Scheme Administrator.</td>
	              </tr>
	              <tr>
	                <td height="10"></td>
	              </tr>
	              <tr>
	                <td height="10" style="border-top: solid 1px #333333"></td>
	              </tr>
	              <tr>
	                <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000"><strong>Signed by the site assessor:</strong></td>
	              </tr>
	              <tr>
	                <td height="10" style="border-bottom: solid 1px #333333"></td>
	              </tr>
	              <tr>
	                <td>
	                  <table width="910" border="0" cellspacing="0" cellpadding="0">
	                    <tbody>
	                      <tr>
	                        <td width="136" height="30"  style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #666666">Signature:</td>
	                        <td width="734" style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; border-bottom: solid 1px #000000">
	                          <div class="signature-pad">
	                            <div class="signature-pad--body">
	                              <div style="touch-action: none; border: 1px solid black; height:100px; border-radius:4px;" id="sign_create_5" data-id="5" class="sign_create">
	                              </div>
	                              <input required="" id="site_assessor_form_details_signature" name="site_assessor_form_details[signature]" type="hidden" />
	                              <input type="hidden" style="border: 0px; border-bottom: 1px solid black; width: 100%;"  name="site_assessor_form_details[custom_signature]" />
	                            </div>
	                          </div>
	                          <div class="signature-pad-image signature_image_container" style="display:none !important;">
	                            <img src="" />
	                            <a class="signature_image_close" href="javscript:void(0);"  ></a>
	                          </div>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td height="30"  style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #666666">Name of signatory:</td>
	                        <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #666666"><input type="text" style="border: 0px;" name="site_assessor_form_details[name2]" /></td>
	                      </tr>
	                      <tr>
	                        <td height="30"  style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #666666">Date:</td>
	                        <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;border-bottom: solid 1px #666666"><input type="text" style="border: 0px;" name="site_assessor_form_details[date1]" /></td>
	                      </tr>
	                    </tbody>
	                  </table>
	                </td>
	              </tr>
	            </tbody>
	          </table>
	        </td>
	      </tr>
	    </tbody>
	</table>
</body>
</html>
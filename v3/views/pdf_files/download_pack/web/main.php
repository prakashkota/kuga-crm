<html>

<head>
    <title>Commercial Proposal</title>
    <style>
        @font-face {
            font-family: 'CenturyGothic';
            font-weight: normal;
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothic.otf'); ?>) format('truetype');
        }

        @font-face {
            font-family: 'CenturyGothic';
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>);
            src: url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('embedded-opentype'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('woff'),
                url(<?php echo site_url('assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf'); ?>) format('truetype');
            font-weight: bold;
        }
    div.main > div{
        margin-top: 10px;
        page-break-after: always;
    }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

</head>

<body>



    <div class="main">
        <div id="page_1">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-1', [], TRUE); ?>
        </div>
        <div id="page_2">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-2', [], TRUE); ?>
        </div>
        <div id="page_3">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-3', [], TRUE); ?>
        </div>
        <div id="page_4">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-4', [], TRUE); ?>
        </div>
        <div id="page_5">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-5', [], TRUE); ?>
        </div>
        <div id="page_6">
            <?php echo $this->load->view('pdf_files/download_pack/web/page-6', [], TRUE); ?>
        </div>
    </div>

</body>

</html>
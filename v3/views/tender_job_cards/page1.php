<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>Job Details</strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Project Name*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="project_name" name="job_card[project_name]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Site Address*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="site_address" name="job_card[site_address]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Sales Rep*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="sale_rep_name" name="job_card[sale_rep_name]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Lead Source*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="lead_source" name="job_card[lead_source]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is this for a Tender / Quote*?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input type="radio" id="is_tender_yes" value="yes" name="job_card[is_tender]"/>
                                                <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                <input type="radio" id="is_tender_no" value="no" name="job_card[is_tender]"/> 
                                                <span style="margin-right: 20px;font-size: 16px;">No</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is Site Inspection done*?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input type="radio" id="is_site_incpection_yes" value="yes" name="job_card[is_site_incpection]"/>
                                                <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                <input type="radio" id="is_site_incpection_no" value="no" name="job_card[is_site_incpection]"/> 
                                                <span style="margin-right: 20px;font-size: 16px;">No</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>System Details</strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">System Size*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="system_size" name="job_card[system_size]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Panel Brand*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="panel_brand" name="job_card[panel_brand]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Inverter Brand*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" id="inverter_brand" name="job_card[inverter_brand]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Battery Brand:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input id="battery_brand" name="job_card[battery_brand]" style="padding:5px; width:inherit; height: inherit;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="360" valign="top" style="padding-right:10px">
                                    <table width="350" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="105" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Tilt*:</td>
                                            <td width="200" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input type="radio" id="is_tilt_yes" value="yes" name="job_card[is_tilt]"/>
                                                <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                <input type="radio" id="is_tilt_no" value="no" name="job_card[is_tilt]"/> 
                                                <span style="margin-right: 20px;font-size: 16px;">No</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="455" valign="top" style="padding-left:10px" class="hidden" id="no_if_tilt_panels_td">
                                    <table width="445" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="150" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">No. Of panels on TILT:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input id="no_if_tilt_panels" name="job_card[no_if_tilt_panels]" style="padding:5px; width:inherit; height: inherit;" type="number" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Type*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <select required="" class="form-control" name="job_card[roof_type]" required="" style="padding:5px; width:inherit; height: inherit;">
                                                    <option value="">Select Roof Type</option>
                                                    <option value="Tile">Tile</option>
                                                    <option value="Tin">Tin</option>
                                                    <option value="Clip Lock">Clip Lock</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top" id="addition_note2">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Additional Notes</td>
                                        </tr>
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                <textarea id="additional_notes2" name="job_card[additional_notes2]" style="padding:5px; width:100%; height: 100px;"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="200"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
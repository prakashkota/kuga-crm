<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quick Quote</title>

<style>
@media print {
.no-print {display: none;}
body {background: transparent;}
.red{color:#c32027}
.black-bg {background-color:#010101}
.dark-bg{background-color:#282828}
</style>
</head>

<body style="padding:0; margin:0">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/images/led_quote_form.jpg'; ?>" alt="" width="910" height="126" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
      <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Quote #</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['quote_no'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
                </tr>
              </tbody></table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Bussiness Name:</td>
                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_company_name'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
              </tr>
            </tbody></table></td>
          </tr>
          </tbody></table></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Name:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_name'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
              </tr>
            </tbody></table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Contact No:</td>
                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_contact_no'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Address:</td>
            <td width="708" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_address'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
          </tr>
        </tbody></table></td>
      </tr>
      <tr>
        <td height="13"></td>
      </tr>
      <?php /**<tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Suburb:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_suburb'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
              </tr>
            </tbody></table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Postcode:</td>
                <td width="275" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><input value="<?php echo (isset($quote_data)) ? $quote_data['customer_postcode'] : ''; ?>" style="padding:5px; width:100%; height: 40px; border:1px solid #e8e8e8;"  /></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table></td>
      </tr> */ ?>
       <tr>
        <td height="30"></td>
      </tr>
      <tr>
        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:5px; color:#333333">
		Work Requested:
		</td>
      </tr>
    </tbody></table></td>
  </tr>
    <td valign="top"></td> </tr>
 
  <tr>
    <td valign="top">
	<table width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
      <tr> <th height="40" width="510" colspan="2" class="black-bg" style="background:#010101; padding:5px;  color:#ffffff; font-family:Arial, Helvetica, sans-serif;  font-size:15px">Description of Product/Service</th>
        <th width="100" class="black-bg" style="background:#010101; padding:5px;  color:#ffffff; font-family:Arial, Helvetica, sans-serif;  font-size:15px">Units</th>
        <th width="109" class="black-bg" style="background:#010101; padding:5px;  color:#ffffff; font-family:Arial, Helvetica, sans-serif;  font-size:15px">Cost Per Unit</th>
        <th width="147" class="black-bg" style="background:#010101; padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px">Cost exc GST</th>
      </tr>
            <?php $extra_count = 9 - count($quote_items);
            if(!empty($quote_items)){ 
                foreach($quote_items as $key => $value){
                $total = 0;
                $total = $value['item_cost_excGST'] * $value['item_qty'];
                $total = number_format((float)$total, 2,'.', ''); 
               ?>
        <tr>
	    <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
                <strong style="padding-left:5px;" ><?php echo $value['item_name']; ?></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
            <strong style="padding-left:5px;" ><?php echo $value['item_qty']; ?></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
            <strong style="padding-left:5px;" ><?php echo $value['item_cost_excGST']; ?></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
            <strong style="padding-left:5px;" ><?php echo $total; ?></strong>
		</td>
      </tr>
            <?php }} ?>
            <?php for($i=0; $i < $extra_count; $i++){ ?>
	  <tr>
	    <td height="40" colspan="2"  style=" font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			<strong style="padding-left:5px;" ></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			<strong style="padding-left:5px;" ></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			<strong style="padding-left:5px;" ></strong>
		</td>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			<strong style="padding-left:5px;" ></strong>
		</td>
      </tr>
            <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td valign="top"><table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="510" height="40" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Sales Representative:</td>
        <td width="109" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Sub Total exc GST:</td>
        <td width="147" style="border:1px solid #e8e8e8; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
		<strong style="padding-left:5px;" ><?php echo $quote_data['total_excGST']; ?></strong>
		</td>
      </tr>
      <tr>
        <td align="left" height="40" valign="top" style="padding:5px; border:1px solid #e8e8e8; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">
            <?php echo $quote_data['rep_name']; ?></td>
        <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Total inc GST:</td>
        <td style="border:1px solid #e8e8e8; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			<strong style="padding-left:5px;" ><?php echo $quote_data['total_incGST']; ?></strong> 
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
		  <tr>
            <td height="35" style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333333"><strong>Terms &amp; Conditions</strong></td>
          </tr>
          <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			 * Payment required by Cash, Cheque or Credit Card or Direct Deposit at the completion of the job.
			</td>
		  </tr>
		  <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			 * Payment is due on the day of the completion of the work.
			</td>
		  </tr>
		  <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			 * All materials remain the property of Kuga Electrical until the full balance is paid.
			</td>
		  </tr>
		  <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
			 * The quote is valid for 14 days from  
                         <input style="padding:5px;  height: 40px; border:1px solid #e8e8e8; " value="<?php echo $quote_data['quote_expires_at']; ?>" />
			</td>
		  </tr>
		  <tr>
    <td>&nbsp;</td>
  </tr>
          </tbody>
            </table>
        </td>
      </tr>
  <tr style="height:162px;">
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="25" style="font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>ABN 39 616 409 584</strong></td>
          </tr>
          <tr>
            <td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
          </tr>
        </table></td>
        <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">service@13kuga.com.au</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
          </tr>
          <tr>
            <td align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
          </tr>
        </table></td>
        <td width="273" align="right"><img src="<?php echo $this->config->item('live_url').'assets/images/bottom_right.jpg'; ?>" width="117" height="56" /></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

<div class="modal fade" id="booking_form_choose_customer_location_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="margin-bottom: -10px;">Choose Location  <a style="margin-left:20px;" id="add_new_customer_location_btn_1" href="javascript:void(0);"><i class="fa fa-plus"></i> Add New</a></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div id="customer_booking_form_cost_centre_body" class="mb-4 d-flex"></div>
                        <table width="100%" border="0" class="table table-striped" id="customer_booking_form_location_table" >
                            <thead>
                                <tr>
                                    <td>Address</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody id="customer_booking_form_location_table_body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
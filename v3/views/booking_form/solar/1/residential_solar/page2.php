<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
   
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Customer Details</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="first_name" name="authorised_details[first_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="last_name" name="authorised_details[last_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="email" name="authorised_details[email]" style="padding:5px; width:inherit; height: inherit;" type="email" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Phone Number:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="contact_no" name="authorised_details[contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Other Number:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="other_contact_no" name="authorised_details[other_contact_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Address:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="address" name="business_details[address]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
				
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <!--<tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Suburb:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input  id="suburb" name="business_details[suburb]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Postcode:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="postcode" name="business_details[postcode]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                        </tr>
                                    </table></td>
                            </tr> -->
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Electricity Bill Information</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Retailer:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="eb_retailer" name="electricity_bill[eb_retailer]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">NMI:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="eb_nmi" name="electricity_bill[eb_nmi]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Distributor:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="eb_distributor" name="electricity_bill[eb_distributor]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Meter Number:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input id="eb_meter_no" name="electricity_bill[eb_meter_no]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Account Name:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="eb_acc_name" name="electricity_bill[eb_acc_name]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Account Number:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input id="eb_acc_no" name="electricity_bill[eb_acc_no]" style="padding:5px; width:inherit; height: inherit;" type="number" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="100">&nbsp;</td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
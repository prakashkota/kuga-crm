<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Warranty extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Cron_Model','kuga_web');
    }

    public function upload_evidence() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: *"); 
        $data = array('status'=>false,'message'=>'','data'=>[],'error'=>[]);
        $uuid = $this->input->post('uuid');
        $name = $this->input->post('name');
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            if ($_FILES) {
                foreach($_FILES as $key=>$file){
                    $fileUrl = $this->_upload_evidence($file);
                    if($fileUrl){
                        //Insert File to DB
                        $insert_data = array();
                        $insert_data['uuid'] = $uuid;
                        $insert_data['file_name'] = $fileUrl;
                        $insert_data['name'] = $name;
                        $this->kuga_web->insert_kuga_web_data('web_warranty_enquiry_attachments',$insert_data);
                        $data['status'] = true;
                        $data['message'] = 'uploaded successful';    
                        $data['data'] = ['file_url'=>$fileUrl];    
                    }else{
                        $data['message'] = 'Unable to upload file';    
                        $data['error'] = [$key=>'Invalid request, please try again'];
                    }
                }
            } else {
                $data['message'] = 'Invalid Request';
                $data['error'] = [$key=>'Unable to upload this file, please try again'];
            }
            echo json_encode($data);
            die;
        }
    }
    
    private function _upload_evidence($file){
        $path = FCPATH.'assets/uploads/warranty_claim_files/';
        if(!is_dir($path)){
            mkdir($path,0777);
        }
         
        $imageFileType = strtolower(pathinfo($file["name"],PATHINFO_EXTENSION));
        $baseName = strtolower(pathinfo($file["name"],PATHINFO_FILENAME));
        
        //$check = getimagesize($file["tmp_name"]);
        if(TRUE) {
            $name = $baseName.'-'.date('Ymd-his').'.'.$imageFileType;
            $name = str_replace(' ', '_', $name);
            $target_file = $path .$name;
            if(move_uploaded_file($file["tmp_name"], $target_file)){
                return $name;
            }else{
                return false;
            }
        } else {
            return false;
        }
        return false;
    }
}

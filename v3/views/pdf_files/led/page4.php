<?php
$total_items = count($led_proposal_products) + count($led_proposal_additional_items);
$height = '820px';
$height1 = '810px';
if ($total_items < 15) {
    $html = "    
	<tr>
		<td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Project Summary</td>
	</tr> 
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>                                                        
    <tr style=\"height:" . $height . "\">
    <td align=\"left\" valign=\"top\">
    <table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
    <tr>
    <td width=\"20%\" align=\"left\" valign=\"top\" >
    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#000000;\">
    <tr>
    <td  height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; sans-serif; color:#fff; font-size:14px;\">JOB SUMMARY
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Space Type</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Hours</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty(Old)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Old Fitting</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty (New)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \">New Fitting</td>
    </tr>";

    $Row = count($led_proposal_products);
   
    for ($j = 0; $j < $Row; $j++) {
        $html .= "<tr>
					<td height=\"30\" align=\"left\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['space_type_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['hours'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['ep_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['ep_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['np_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['np_name'] . "</td>
					</tr>";
    }

    if (count($led_proposal_additional_items) > 0) {
        $html .= " <tr>
				<td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Access Equipment</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px;\">Qty</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \">Cost</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				</tr>";

        for ($a = 0; $a < count($led_proposal_additional_items); $a++) {
            $html .= "
			   <tr>
				<td  height=\"30\"  align=\"left\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;  font-size:12px;\">" . $led_proposal_additional_items[$a]['ae_name'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_additional_items[$a]['ae_qty'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\">$" . $led_proposal_additional_items[$a]['ae_cost'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				</tr>";
        }
    }


    $html .= "</table>
			</td>
			</tr>
			
			<tr>
			<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/imagespdf2/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
			</tr>
			";
} else {
    $html = "    
	<tr>
		<td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Project Summary</td>
	</tr> 
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>                                                        
    <tr style=\"height:" . $height1 . "\">
    <td align=\"left\" valign=\"top\">
    <table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
    <tr>
    <td width=\"20%\" align=\"left\" valign=\"top\" >
    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#000000;\">
    <tr>
    <td  height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; sans-serif; color:#fff; font-size:14px;\">JOB SUMMARY
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Space Type</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Hours</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty(Old)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Old Fitting</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty (New)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \">New Fitting</td>
    </tr>";

    $Row = 13;

    for ($j = 0; $j < $Row; $j++) {
        $html .= "<tr>
					<td height=\"30\" align=\"left\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['space_type_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['hours'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['ep_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['ep_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['np_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['np_name'] . "</td>
					</tr>";
    }

    $html .= "
		   
		   <tr>
			<td align=\"right\" colspan=\"6\" style=\"font-size: 14px; padding: 10px 10px 10px 10px;\">More products in annexure. </td>
		  </tr>
			";

    $html .= "</table>
			</td>
			</tr>

			<tr>
			<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/imagespdf2/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
			</tr>
			";

    /** Job Summary page 2 */
    $html.= "    
	<tr>
		<td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; font-family: 'Montserrat', sans-serif;  color: #fff;  padding: 50px 0px 50px 35px;font-weight: bold; \">Project Summary</td>
	</tr> 
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>
    <tr>
        <td align=\"left\" valign=\"top\">&nbsp;</td>
    </tr>                                                        
    <tr style=\"height:" . $height . "\">
    <td align=\"left\" valign=\"top\">
    <table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
    <tr>
    <td width=\"20%\" align=\"left\" valign=\"top\" >
    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#000000;\">
    <tr>
    <td  height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; sans-serif; color:#fff; font-size:14px;\">JOB SUMMARY
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Space Type</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Hours</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty(Old)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; 
     border-right: 1px solid #cccccc; \">Old Fitting</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Qty (New)</td>
    <td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \">New Fitting</td>
    </tr>";

    $RowNextPage = count($led_proposal_products);
    for ($j = 14; $j < $RowNextPage; $j++) {
        $html .= "<tr>
					<td height=\"30\" align=\"left\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['space_type_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['hours'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['ep_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px; 
					 border-right: 1px solid #cccccc;\">" . $led_proposal_products[$j]['ep_name'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_products[$j]['np_qty'] . "</td>
					<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\">" . $led_proposal_products[$j]['np_name'] . "</td>
					</tr>";
    }

    if (count($led_proposal_additional_items) > 0) {
        $html .= " <tr>
				<td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px; \">Access Equipment</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;  font-weight:bold; font-size:12px;\">Qty</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \">Cost</td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				<td align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;   font-weight:bold; font-size:12px; \"></td>
				</tr>";

       
        for ($a = 0; $a < count($led_proposal_additional_items); $a++) {
            $html .= "
			   <tr>
				<td  height=\"30\"  align=\"left\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;  font-size:12px;\">" . $led_proposal_additional_items[$a]['ae_name'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc;font-size:12px;\">" . $led_proposal_additional_items[$a]['ae_qty'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\">$" . $led_proposal_additional_items[$a]['ae_cost'] . "</td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				<td colspan=\"1\"   align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; border-bottom:1px solid #cccccc; font-size:12px;\"></td>
				</tr>";
        }
    }


    $html .= "</table>
			</td>
			</tr>

			<tr>
			<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/imagespdf2/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
			</tr>
			";
}

echo $html;
?>


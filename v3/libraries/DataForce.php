<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class DataForce {

    /**
     * Constructor
     */

    protected static $client_url = 'https://asap.dataforce.com.au/~get';
    protected static $client_id = 'get';
    protected static $client = 'kuga';
    protected static $username = 'Maria_Kurta';
    protected static $password = 'Summer0809';
    protected static $db = 'GET_ESS';

    public function __construct() {
        require __DIR__ . "./../../vendor/autoload.php";
    }

    
    public function list_jobs_by_summary_api() {
        $payload = array(
            'username' => self::$username,
            'password' => self::$password,
            'database' => self::$db,
            'version ' => 1,
            'sReportName' => 'Jobs Created Sources',
            'sSummary' => 14,
            'dFromDate' => '20-10-2019',
            'dToDate' => '23-10-2019',
        );
        //https://asap.dataforce.com.au/~get/asap.PROD/admin/summary_result.php
        $data = array();
        $client = new \GuzzleHttp\Client();
        $api_url = '/asap.PROD/admin/summary_result.php';
        $headers = array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cookie' => 'FFCC-HOSTURL=https%3A%2F%2Fasap.dataforce.com.au; FFCC=561d331d45d2288f106b9b76fb3438991743dfccc60cb1b3d4c04de89b599c45; FFCC-DB=GET_ESS;'
        );
        
        $response = $client->request('POST',self::$client_url . $api_url,[
            'headers' => $headers,
            'query' => $payload,
            'allow_redirects' => false
        ]
    );
        echo $response->getStatusCode();die;
        try{
            if ($response->getStatusCode() === 200) {
                //$xmlstring = $response->getBody()->getContents();
                //$xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
                //$json = json_encode($xml);
                //$data = json_decode($json,TRUE);
                $html =  $response->getBody()->getContents();
                $data =  $this->convert_table_html_to_array($html);
            }
        }catch(GuzzleHttp\Exception\GuzzleException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ClientException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }catch(GuzzleHttp\Exception\ServiceException $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }
        return $data;
    }


    public function list_jobs_by_appointment_api() {

        $data = array();
        $payload = array(
            'client_id' => 'get', 
            'api_guid' => '9087f0f6-9082-4847-95b3-23ebbbada29c',
            'database' => self::$db,
        );

        $headers = array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/xml'
        ); 

        $api_url = '/asap.PROD/api/appointment_list.php';
        
        try{
            $ch = curl_init(self::$client_url . $api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            $response = curl_exec($ch);
            curl_close($ch);

            $xmlstring = $response;
            $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $data = json_decode($json,TRUE);
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }

        return $data;
    }


    public function get_appointment_details($appointment_id = NULL) {
        
        $data = array();
        $payload = array(
            'client_id' => 'get', 
            'api_guid' => '9087f0f6-9082-4847-95b3-23ebbbada29c',
            'database' => self::$db,
            'appointment_id' => $appointment_id,
            'cleint' => self::$client,
            'version' => 28
        );


        $api_url = '/asap.PROD/api/fieldworker_schedule.php';
        
        try{
            $ch = curl_init(self::$client_url . $api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            $response = curl_exec($ch);
            curl_close($ch);

            $xmlstring = $response;
            $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $data = json_decode($json,TRUE);
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }

        return $data;
    }

    public function get_job_files($job_id = NULL) {
        
        $data = array();
        $payload = array(
            'client_id' => 'get', 
            'api_guid' => '9087f0f6-9082-4847-95b3-23ebbbada29c',
            'database' => self::$db,
            'jobdata_id' => $job_id,
            'action' => 'download',
            'version' => 1
        );

        //print_r($payload);die;

        $headers = array(
            'Content-Type:multipart/form-data',
            'Accept:application/xml'
        ); 

        $api_url = '/asap.PROD/api/fieldworker_jobfile.php';
        
        try{
            $ch = curl_init(self::$client_url . $api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($ch);
            curl_close($ch);

            $data = base64_encode($response);
            //$im = imagecreatefromstring($response);
            //$resp = imagepng($im, FCPATH.'assets/uploads/'.date('ymdhis').'.png');
            //imagedestroy($im);
        }catch(Exception $e){
            $data = array('error' => TRUE,'status' => $e->getMessage());
        }

        return $data;
    }


    public function convert_table_html_to_array($html){
        $dom = new DOMDocument();

        $dom->loadHTML($html);

        $dom->preserveWhiteSpace = false;

        $tables = $dom->getElementsByTagName('table');

        //get all rows from the table
        $rows = $tables->item(0)->getElementsByTagName('tr');
        // get each column by tag name
        $cols = $rows->item(0)->getElementsByTagName('th');
        $row_headers = NULL;
        foreach ($cols as $node) {
             //print $node->nodeValue."\n";
            $row_headers[] = $node->nodeValue;
        }

        $table = array();
        //get all rows from the table
        $rows = $tables->item(0)->getElementsByTagName('tr');
        foreach ($rows as $row)
        {
            // get each column by tag name
            $cols = $row->getElementsByTagName('td');
            $row = array();
            $i=0;
            foreach ($cols as $node) {
                # code...
                //print $node->nodeValue."\n";
                if($row_headers==NULL)
                    $row[] = $node->nodeValue;
                else
                    $row[$row_headers[$i]] = $node->nodeValue;
                $i++;
            }
            $table[] = $row;
        }

        return $table;
    }

    public function XML2Array(SimpleXMLElement $parent){
        $array = array();

        foreach ($parent as $name => $element) {
            ($node = & $array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = & $node[];

            $node = $element->count() ? $this->XML2Array($element) : trim($element);
        }

        return $array;
    }




}

<style>
	.font-family{font-family: 'Open Sans', sans-serif;}
	.table-border{border:solid 1px #ccc;}
	.w910{width:910px; }
	body{margin:0px !important; overflow-y: hidden;}

	.overlay-roadblock{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 10;
		background-color: rgba(0,0,0,0.5);
		display: none;
	}

	.overlay-roadblock__content{
		width: auto;
		height: 80px;
		position: fixed;
		top: 50%; 
		left: 50%;
		margin-top: -100px;
		margin-left: -150px;
		margin-right: 25px;
		background-color: #fff;
		border-radius: 5px;
		text-align: center;
		z-index: 11;
		word-break: break-all;
		padding:20px;
	}

	.loader {
	    border: 5px solid #f3f3f3;
	    -webkit-animation: spin 1s linear infinite;
	    animation: spin 1s linear infinite;
	    border-top: 5px solid #555;
	    border-radius: 50%;
	    width: 50px;
	    height: 50px;
	    margin: 0 auto;
	    text-align:center;
	}

	@keyframes spin {
    0% { 
        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}
</style>
<script src="https://code.highcharts.com/highcharts.js"></script>
<form  method="POST" id="chartForm">
	<input type="hidden"  id="pId" value="<?php echo $proposal_data['id']; ?>" />

	<input type="hidden" id="dataImg1" name="dataImg1" value="" />
	<input type="hidden"  name="filename1" value="page8_<?php echo $proposal_data['id']; ?>" />

	<input type="hidden" id="dataImg2" name="dataImg2" value="" />
	<input type="hidden"  name="filename2" value="page9_<?php echo $proposal_data['id']; ?>" />

</form>
<div id="page1" class="w910">
 <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" >

  <tbody>

    <tr>

    <td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">7 <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img.jpg" alt="" style="display:inline-block"/></td>

    </tr>

    <tr>

      <td>

        <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">

    <tr>

      <td style="padding: 0 0 50px;">

        <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-8-head.jpg" alt="">

      </td>

    </tr>  

    <tr>

      <td style="padding: 0 0 50px;">

        <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-8-subhead1.jpg" alt="">

      </td>

    </tr>

    <tr>

  <td>

    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">

      <tr>

        <td>

          <div id="avg_load_area_chart"></div>



        </td>

      </tr>

    </table>

  </td>

</tr>  

<tr>

      <td style="height:20px;">&nbsp;</td>

    </tr>

    <tr>

      <td style="padding: 0 0 50px;">

        <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-8-subhead2.jpg" alt="">

      </td>

    </tr>

    <tr>

  <td>

    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">

      <tr>

        <td>

          <div id="weekend_load_area_chart"></div>



        </td>

      </tr>

    </table>

  </td>

</tr>  

  </table>

</td>

</tr>

  

    <tr>

      <td style="padding:20px 50px 0; text-align:right; font-size:13px; color:#666;">

        WWW.13KUGA.COM.AU   |   <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>

      </td>

    </tr>

  </tbody>

</table>

<script type='text/javascript'>

 var avg_load_graph_data = '<?php echo json_encode($avg_load_graph_data); ?>';

 avg_load_graph_data = JSON.parse(avg_load_graph_data);

 



 /**google.load('visualization', '1.0', {'packages':['corechart']});

 google.setOnLoadCallback(drawChart);

 function drawChart() {

   var date = '02/03';

   var arr = [];

   arr.push(['Year', 'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']);

   for (var i = 0; i < avg_load_graph_data['jan'].length; i++) {

     arr.push([i+':00', 

      parseFloat(avg_load_graph_data['jan'][i]),

      parseFloat(avg_load_graph_data['feb'][i]),

      parseFloat(avg_load_graph_data['mar'][i]),

      parseFloat(avg_load_graph_data['apr'][i]),

      parseFloat(avg_load_graph_data['may'][i]),

      parseFloat(avg_load_graph_data['jun'][i]),

      parseFloat(avg_load_graph_data['jul'][i]),

      parseFloat(avg_load_graph_data['aug'][i]),

      parseFloat(avg_load_graph_data['sep'][i]),

      parseFloat(avg_load_graph_data['oct'][i]),

      parseFloat(avg_load_graph_data['nov'][i]),

      parseFloat(avg_load_graph_data['dec'][i])]);

   }

   var data = google.visualization.arrayToDataTable(arr);



   var options = {

     titleTextStyle:{

      fontName: 'Open Sans',

      italic: false,

      bold:false,

      fontStyle: 'normal' 

    },

    curveType: 'function',

    width: 780,

    height:315,

    legend: { position: 'none', alignment: 'start' , textStyle: {fontSize: 7} },

    series: {0:{color:'#3B8DBC'},1:{color:'#81D7A6'},2:{color:'#EFC614'},3:{color:'#FFB69B'},

    4:{color:'#7E3735'},5:{color:'#7ED3CC'},6:{color:'#C7EA8C'},7:{color:'#FFF58F'},8:{color:'#C99182'},

    9:{color:'#4D2F2F'},10:{color:'#3D8FC1'},11:{color:'#5BAA7D'}},

    pointSize: 4,

    hAxis:{

      showTextEvery:2,

      title: 'Hours',

      titleTextStyle:{ 

        fontName: 'Open Sans',

        italic: false,

        bold:false,

        fontSize:10,

        fontStyle: 'normal'

      },

      textStyle:{ 

        fontName: 'Open Sans',

        italic: false,

        bold:false,

        fontSize:10,

        fontStyle: 'normal'

      }

    },

    vAxis:{

      title: 'Power(kW)',

      titleTextStyle:{ 

        fontName: 'Open Sans',

        italic: false,

        bold:false,

        fontSize:10,

        fontStyle: 'normal'

      },

      textStyle:{ 

        fontName: 'Open Sans',

        italic: false,

        bold:false,

        fontSize:10,

        fontStyle: 'normal'

      }

    }

  };



  var chart = new google.visualization.LineChart(document.getElementById('avg_load_area_chart'));



  chart.draw(data, options);

}*/



        var labels = {};

        Highcharts.chart('avg_load_area_chart', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours',

                    style: {

                      "fontSize": "16px",

                      "color": '#000',

                      "fontWeight": 'bold'

                    }

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'POWER',

                    style: {

                      "fontSize": "16px",

                      "color": '#000',

                      "fontWeight": 'bold'

                    }

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

              align: 'center',

              verticalAlign: 'bottom',

              x: 0,

              y: 0,

              itemStyle: {

                "fontSize": "9px",

                "color": '#666',

                "fontWeight": 'normal'

              }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

              {

                name: 'Jan',

                data: [

                avg_load_graph_data['jan'][0],

                avg_load_graph_data['jan'][1],

                avg_load_graph_data['jan'][2],

                avg_load_graph_data['jan'][3],

                avg_load_graph_data['jan'][4],

                avg_load_graph_data['jan'][5],

                avg_load_graph_data['jan'][6],

                avg_load_graph_data['jan'][7],

                avg_load_graph_data['jan'][8],

                avg_load_graph_data['jan'][9],

                avg_load_graph_data['jan'][10],

                avg_load_graph_data['jan'][11],

                avg_load_graph_data['jan'][12],

                avg_load_graph_data['jan'][13],

                avg_load_graph_data['jan'][14],

                avg_load_graph_data['jan'][15],

                avg_load_graph_data['jan'][16],

                avg_load_graph_data['jan'][17],

                avg_load_graph_data['jan'][18],

                avg_load_graph_data['jan'][19],

                avg_load_graph_data['jan'][20],

                avg_load_graph_data['jan'][21],

                avg_load_graph_data['jan'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Feb',

                data: [

                avg_load_graph_data['feb'][0],

                avg_load_graph_data['feb'][1],

                avg_load_graph_data['feb'][2],

                avg_load_graph_data['feb'][3],

                avg_load_graph_data['feb'][4],

                avg_load_graph_data['feb'][5],

                avg_load_graph_data['feb'][6],

                avg_load_graph_data['feb'][7],

                avg_load_graph_data['feb'][8],

                avg_load_graph_data['feb'][9],

                avg_load_graph_data['feb'][10],

                avg_load_graph_data['feb'][11],

                avg_load_graph_data['feb'][12],

                avg_load_graph_data['feb'][13],

                avg_load_graph_data['feb'][14],

                avg_load_graph_data['feb'][15],

                avg_load_graph_data['feb'][16],

                avg_load_graph_data['feb'][17],

                avg_load_graph_data['feb'][18],

                avg_load_graph_data['feb'][19],

                avg_load_graph_data['feb'][20],

                avg_load_graph_data['feb'][21],

                avg_load_graph_data['feb'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Mar',

                data: [

                avg_load_graph_data['mar'][0],

                avg_load_graph_data['mar'][1],

                avg_load_graph_data['mar'][2],

                avg_load_graph_data['mar'][3],

                avg_load_graph_data['mar'][4],

                avg_load_graph_data['mar'][5],

                avg_load_graph_data['mar'][6],

                avg_load_graph_data['mar'][7],

                avg_load_graph_data['mar'][8],

                avg_load_graph_data['mar'][9],

                avg_load_graph_data['mar'][10],

                avg_load_graph_data['mar'][11],

                avg_load_graph_data['mar'][12],

                avg_load_graph_data['mar'][13],

                avg_load_graph_data['mar'][14],

                avg_load_graph_data['mar'][15],

                avg_load_graph_data['mar'][16],

                avg_load_graph_data['mar'][17],

                avg_load_graph_data['mar'][18],

                avg_load_graph_data['mar'][19],

                avg_load_graph_data['mar'][20],

                avg_load_graph_data['mar'][21],

                avg_load_graph_data['mar'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Apr',

                data: [

                avg_load_graph_data['apr'][0],

                avg_load_graph_data['apr'][1],

                avg_load_graph_data['apr'][2],

                avg_load_graph_data['apr'][3],

                avg_load_graph_data['apr'][4],

                avg_load_graph_data['apr'][5],

                avg_load_graph_data['apr'][6],

                avg_load_graph_data['apr'][7],

                avg_load_graph_data['apr'][8],

                avg_load_graph_data['apr'][9],

                avg_load_graph_data['apr'][10],

                avg_load_graph_data['apr'][11],

                avg_load_graph_data['apr'][12],

                avg_load_graph_data['apr'][13],

                avg_load_graph_data['apr'][14],

                avg_load_graph_data['apr'][15],

                avg_load_graph_data['apr'][16],

                avg_load_graph_data['apr'][17],

                avg_load_graph_data['apr'][18],

                avg_load_graph_data['apr'][19],

                avg_load_graph_data['apr'][20],

                avg_load_graph_data['apr'][21],

                avg_load_graph_data['apr'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'May',

                data: [

                avg_load_graph_data['may'][0],

                avg_load_graph_data['may'][1],

                avg_load_graph_data['may'][2],

                avg_load_graph_data['may'][3],

                avg_load_graph_data['may'][4],

                avg_load_graph_data['may'][5],

                avg_load_graph_data['may'][6],

                avg_load_graph_data['may'][7],

                avg_load_graph_data['may'][8],

                avg_load_graph_data['may'][9],

                avg_load_graph_data['may'][10],

                avg_load_graph_data['may'][11],

                avg_load_graph_data['may'][12],

                avg_load_graph_data['may'][13],

                avg_load_graph_data['may'][14],

                avg_load_graph_data['may'][15],

                avg_load_graph_data['may'][16],

                avg_load_graph_data['may'][17],

                avg_load_graph_data['may'][18],

                avg_load_graph_data['may'][19],

                avg_load_graph_data['may'][20],

                avg_load_graph_data['may'][21],

                avg_load_graph_data['may'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Jun',

                data: [

                avg_load_graph_data['jun'][0],

                avg_load_graph_data['jun'][1],

                avg_load_graph_data['jun'][2],

                avg_load_graph_data['jun'][3],

                avg_load_graph_data['jun'][4],

                avg_load_graph_data['jun'][5],

                avg_load_graph_data['jun'][6],

                avg_load_graph_data['jun'][7],

                avg_load_graph_data['jun'][8],

                avg_load_graph_data['jun'][9],

                avg_load_graph_data['jun'][10],

                avg_load_graph_data['jun'][11],

                avg_load_graph_data['jun'][12],

                avg_load_graph_data['jun'][13],

                avg_load_graph_data['jun'][14],

                avg_load_graph_data['jun'][15],

                avg_load_graph_data['jun'][16],

                avg_load_graph_data['jun'][17],

                avg_load_graph_data['jun'][18],

                avg_load_graph_data['jun'][19],

                avg_load_graph_data['jun'][20],

                avg_load_graph_data['jun'][21],

                avg_load_graph_data['jun'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Jul',

                data: [

                avg_load_graph_data['jul'][0],

                avg_load_graph_data['jul'][1],

                avg_load_graph_data['jul'][2],

                avg_load_graph_data['jul'][3],

                avg_load_graph_data['jul'][4],

                avg_load_graph_data['jul'][5],

                avg_load_graph_data['jul'][6],

                avg_load_graph_data['jul'][7],

                avg_load_graph_data['jul'][8],

                avg_load_graph_data['jul'][9],

                avg_load_graph_data['jul'][10],

                avg_load_graph_data['jul'][11],

                avg_load_graph_data['jul'][12],

                avg_load_graph_data['jul'][13],

                avg_load_graph_data['jul'][14],

                avg_load_graph_data['jul'][15],

                avg_load_graph_data['jul'][16],

                avg_load_graph_data['jul'][17],

                avg_load_graph_data['jul'][18],

                avg_load_graph_data['jul'][19],

                avg_load_graph_data['jul'][20],

                avg_load_graph_data['jul'][21],

                avg_load_graph_data['jul'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Aug',

                data: [

                avg_load_graph_data['aug'][0],

                avg_load_graph_data['aug'][1],

                avg_load_graph_data['aug'][2],

                avg_load_graph_data['aug'][3],

                avg_load_graph_data['aug'][4],

                avg_load_graph_data['aug'][5],

                avg_load_graph_data['aug'][6],

                avg_load_graph_data['aug'][7],

                avg_load_graph_data['aug'][8],

                avg_load_graph_data['aug'][9],

                avg_load_graph_data['aug'][10],

                avg_load_graph_data['aug'][11],

                avg_load_graph_data['aug'][12],

                avg_load_graph_data['aug'][13],

                avg_load_graph_data['aug'][14],

                avg_load_graph_data['aug'][15],

                avg_load_graph_data['aug'][16],

                avg_load_graph_data['aug'][17],

                avg_load_graph_data['aug'][18],

                avg_load_graph_data['aug'][19],

                avg_load_graph_data['aug'][20],

                avg_load_graph_data['aug'][21],

                avg_load_graph_data['aug'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Sep',

                data: [

                avg_load_graph_data['sep'][0],

                avg_load_graph_data['sep'][1],

                avg_load_graph_data['sep'][2],

                avg_load_graph_data['sep'][3],

                avg_load_graph_data['sep'][4],

                avg_load_graph_data['sep'][5],

                avg_load_graph_data['sep'][6],

                avg_load_graph_data['sep'][7],

                avg_load_graph_data['sep'][8],

                avg_load_graph_data['sep'][9],

                avg_load_graph_data['sep'][10],

                avg_load_graph_data['sep'][11],

                avg_load_graph_data['sep'][12],

                avg_load_graph_data['sep'][13],

                avg_load_graph_data['sep'][14],

                avg_load_graph_data['sep'][15],

                avg_load_graph_data['sep'][16],

                avg_load_graph_data['sep'][17],

                avg_load_graph_data['sep'][18],

                avg_load_graph_data['sep'][19],

                avg_load_graph_data['sep'][20],

                avg_load_graph_data['sep'][21],

                avg_load_graph_data['sep'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Oct',

                data: [

                avg_load_graph_data['oct'][0],

                avg_load_graph_data['oct'][1],

                avg_load_graph_data['oct'][2],

                avg_load_graph_data['oct'][3],

                avg_load_graph_data['oct'][4],

                avg_load_graph_data['oct'][5],

                avg_load_graph_data['oct'][6],

                avg_load_graph_data['oct'][7],

                avg_load_graph_data['oct'][8],

                avg_load_graph_data['oct'][9],

                avg_load_graph_data['oct'][10],

                avg_load_graph_data['oct'][11],

                avg_load_graph_data['oct'][12],

                avg_load_graph_data['oct'][13],

                avg_load_graph_data['oct'][14],

                avg_load_graph_data['oct'][15],

                avg_load_graph_data['oct'][16],

                avg_load_graph_data['oct'][17],

                avg_load_graph_data['oct'][18],

                avg_load_graph_data['oct'][19],

                avg_load_graph_data['oct'][20],

                avg_load_graph_data['oct'][21],

                avg_load_graph_data['oct'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Nov',

                data: [

                avg_load_graph_data['nov'][0],

                avg_load_graph_data['nov'][1],

                avg_load_graph_data['nov'][2],

                avg_load_graph_data['nov'][3],

                avg_load_graph_data['nov'][4],

                avg_load_graph_data['nov'][5],

                avg_load_graph_data['nov'][6],

                avg_load_graph_data['nov'][7],

                avg_load_graph_data['nov'][8],

                avg_load_graph_data['nov'][9],

                avg_load_graph_data['nov'][10],

                avg_load_graph_data['nov'][11],

                avg_load_graph_data['nov'][12],

                avg_load_graph_data['nov'][13],

                avg_load_graph_data['nov'][14],

                avg_load_graph_data['nov'][15],

                avg_load_graph_data['nov'][16],

                avg_load_graph_data['nov'][17],

                avg_load_graph_data['nov'][18],

                avg_load_graph_data['nov'][19],

                avg_load_graph_data['nov'][20],

                avg_load_graph_data['nov'][21],

                avg_load_graph_data['nov'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Dec',

                data: [

                avg_load_graph_data['dec'][0],

                avg_load_graph_data['dec'][1],

                avg_load_graph_data['dec'][2],

                avg_load_graph_data['dec'][3],

                avg_load_graph_data['dec'][4],

                avg_load_graph_data['dec'][5],

                avg_load_graph_data['dec'][6],

                avg_load_graph_data['dec'][7],

                avg_load_graph_data['dec'][8],

                avg_load_graph_data['dec'][9],

                avg_load_graph_data['dec'][10],

                avg_load_graph_data['dec'][11],

                avg_load_graph_data['dec'][12],

                avg_load_graph_data['dec'][13],

                avg_load_graph_data['dec'][14],

                avg_load_graph_data['dec'][15],

                avg_load_graph_data['dec'][16],

                avg_load_graph_data['dec'][17],

                avg_load_graph_data['dec'][18],

                avg_load_graph_data['dec'][19],

                avg_load_graph_data['dec'][20],

                avg_load_graph_data['dec'][21],

                avg_load_graph_data['dec'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

            

            ]





        });



</script>



<script type='text/javascript'>

  var weekend_load_graph_data = '<?php echo json_encode($weekend_load_graph_data); ?>';

  weekend_load_graph_data = JSON.parse(weekend_load_graph_data);

  

  /**google.load('visualization', '1.0', {'packages':['corechart']});

  google.setOnLoadCallback(drawChart);

  function drawChart() {

   var date = '02/03';

   var arr = [];

   arr.push(['Year', 'Mon','Tue','Wed','Thu','Fri','Sat','Sun']);



   for (var i =0; i <weekend_load_graph_data['mon'].length; i++) {

    arr.push([i +':00', 

      parseFloat(weekend_load_graph_data['mon'][i]),

      parseFloat(weekend_load_graph_data['tue'][i]),

      parseFloat(weekend_load_graph_data['wed'][i]),

      parseFloat(weekend_load_graph_data['thu'][i]),

      parseFloat(weekend_load_graph_data['fri'][i]),

      parseFloat(weekend_load_graph_data['sat'][i]),

      parseFloat(weekend_load_graph_data['sun'][i])]);

  }



  var data = google.visualization.arrayToDataTable(arr);



  var options = {

   titleTextStyle:{

    fontName: 'Open Sans',

    italic: false,

    bold:false,

    fontStyle: 'normal' 

  },



  curveType: 'function',

  width: 780,

  height:315,

  legend: { position: 'none', alignment: 'start' , textStyle: {fontSize: 10}},

  series: {0:{color:'#3B8DBC'},1:{color:'#81D7A6'},2:{color:'#EFC614'},3:{color:'#FFB69B'},

  4:{color:'#7E3735'},5:{color:'#7ED3CC'},6:{color:'#C7EA8C'}},

  pointSize: 4,

  hAxis:{

    showTextEvery:2,

    title: 'Hours',

    titleTextStyle:{ 

      fontName: 'Open Sans',

      italic: false,

      bold:false,

      fontSize:10,

      fontStyle: 'normal'

    },

    textStyle:{ 

      fontName: 'Open Sans',

      italic: false,

      bold:false,

      fontSize:10,

      fontStyle: 'normal'

    }

  },

  vAxis:{

    title: 'Power(kW)',

    titleTextStyle:{ 

      fontName: 'Open Sans',

      italic: false,

      bold:false,

      fontSize:10,

      fontStyle: 'normal'

    },

    textStyle:{ 

      fontName: 'Open Sans',

      italic: false,

      bold:false,

      fontSize:10,

      fontStyle: 'normal'

    }

  }

};



var chart = new google.visualization.LineChart(document.getElementById('weekend_load_area_chart'));



chart.draw(data, options);

}*/





var labels = {};

        Highcharts.chart('weekend_load_area_chart', {

            chart: {

                width: 780,

                height: 360,

            },

            credits: {

                enabled: false

            },

            navigation: {

                buttonOptions: {

                    enabled: false

                }

            },

            title: {

                style: {

                    display: 'none'

                }

            },

            xAxis: {

                title: {

                    text: 'Hours'

                },

                type: 'datetime',

                //step: 24,

                tickInterval: 3780 * 1000,



                //minTickInterval: 24,

                labels: {

                    formatter: function() {

                        return Highcharts.dateFormat('%H:%M:%S', this.value);



                    },

                },

            },

            yAxis: {

                allowDecimals: true,

                title: {

                    text: 'Power(kW)'

                },

                labels: {

                    formatter: function() {

                        return this.value + 'kW';

                    }

                }

            },

            legend: {

              align: 'center',

              verticalAlign: 'bottom',

              x: 0,

              y: 0,

              itemStyle: {

                 "fontSize": "9px",

                 "color": '#666',

                  "fontWeight": 'normal'

              }

            },

            plotOptions: {

                area: {

                    pointStart: 1940,

                    marker: {

                        enabled: false,

                        symbol: 'circle',

                        radius: 3,

                        states: {

                            hover: {

                                enabled: true

                            }

                        }

                    }

                },

                series: {

                    pointStart: Date.UTC(2016, 0, 17),

                    pointInterval: 3780 * 1000

                }

            },

            series: [

              {

                name: 'Mon',

                data: [

                weekend_load_graph_data['mon'][0],

                weekend_load_graph_data['mon'][1],

                weekend_load_graph_data['mon'][2],

                weekend_load_graph_data['mon'][3],

                weekend_load_graph_data['mon'][4],

                weekend_load_graph_data['mon'][5],

                weekend_load_graph_data['mon'][6],

                weekend_load_graph_data['mon'][7],

                weekend_load_graph_data['mon'][8],

                weekend_load_graph_data['mon'][9],

                weekend_load_graph_data['mon'][10],

                weekend_load_graph_data['mon'][11],

                weekend_load_graph_data['mon'][12],

                weekend_load_graph_data['mon'][13],

                weekend_load_graph_data['mon'][14],

                weekend_load_graph_data['mon'][15],

                weekend_load_graph_data['mon'][16],

                weekend_load_graph_data['mon'][17],

                weekend_load_graph_data['mon'][18],

                weekend_load_graph_data['mon'][19],

                weekend_load_graph_data['mon'][20],

                weekend_load_graph_data['mon'][21],

                weekend_load_graph_data['mon'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Tue',

                data: [

                weekend_load_graph_data['tue'][0],

                weekend_load_graph_data['tue'][1],

                weekend_load_graph_data['tue'][2],

                weekend_load_graph_data['tue'][3],

                weekend_load_graph_data['tue'][4],

                weekend_load_graph_data['tue'][5],

                weekend_load_graph_data['tue'][6],

                weekend_load_graph_data['tue'][7],

                weekend_load_graph_data['tue'][8],

                weekend_load_graph_data['tue'][9],

                weekend_load_graph_data['tue'][10],

                weekend_load_graph_data['tue'][11],

                weekend_load_graph_data['tue'][12],

                weekend_load_graph_data['tue'][13],

                weekend_load_graph_data['tue'][14],

                weekend_load_graph_data['tue'][15],

                weekend_load_graph_data['tue'][16],

                weekend_load_graph_data['tue'][17],

                weekend_load_graph_data['tue'][18],

                weekend_load_graph_data['tue'][19],

                weekend_load_graph_data['tue'][20],

                weekend_load_graph_data['tue'][21],

                weekend_load_graph_data['tue'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Wed',

                data: [

                weekend_load_graph_data['wed'][0],

                weekend_load_graph_data['wed'][1],

                weekend_load_graph_data['wed'][2],

                weekend_load_graph_data['wed'][3],

                weekend_load_graph_data['wed'][4],

                weekend_load_graph_data['wed'][5],

                weekend_load_graph_data['wed'][6],

                weekend_load_graph_data['wed'][7],

                weekend_load_graph_data['wed'][8],

                weekend_load_graph_data['wed'][9],

                weekend_load_graph_data['wed'][10],

                weekend_load_graph_data['wed'][11],

                weekend_load_graph_data['wed'][12],

                weekend_load_graph_data['wed'][13],

                weekend_load_graph_data['wed'][14],

                weekend_load_graph_data['wed'][15],

                weekend_load_graph_data['wed'][16],

                weekend_load_graph_data['wed'][17],

                weekend_load_graph_data['wed'][18],

                weekend_load_graph_data['wed'][19],

                weekend_load_graph_data['wed'][20],

                weekend_load_graph_data['wed'][21],

                weekend_load_graph_data['wed'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Thu',

                data: [

                weekend_load_graph_data['thu'][0],

                weekend_load_graph_data['thu'][1],

                weekend_load_graph_data['thu'][2],

                weekend_load_graph_data['thu'][3],

                weekend_load_graph_data['thu'][4],

                weekend_load_graph_data['thu'][5],

                weekend_load_graph_data['thu'][6],

                weekend_load_graph_data['thu'][7],

                weekend_load_graph_data['thu'][8],

                weekend_load_graph_data['thu'][9],

                weekend_load_graph_data['thu'][10],

                weekend_load_graph_data['thu'][11],

                weekend_load_graph_data['thu'][12],

                weekend_load_graph_data['thu'][13],

                weekend_load_graph_data['thu'][14],

                weekend_load_graph_data['thu'][15],

                weekend_load_graph_data['thu'][16],

                weekend_load_graph_data['thu'][17],

                weekend_load_graph_data['thu'][18],

                weekend_load_graph_data['thu'][19],

                weekend_load_graph_data['thu'][20],

                weekend_load_graph_data['thu'][21],

                weekend_load_graph_data['thu'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Fri',

                data: [

                weekend_load_graph_data['fri'][0],

                weekend_load_graph_data['fri'][1],

                weekend_load_graph_data['fri'][2],

                weekend_load_graph_data['fri'][3],

                weekend_load_graph_data['fri'][4],

                weekend_load_graph_data['fri'][5],

                weekend_load_graph_data['fri'][6],

                weekend_load_graph_data['fri'][7],

                weekend_load_graph_data['fri'][8],

                weekend_load_graph_data['fri'][9],

                weekend_load_graph_data['fri'][10],

                weekend_load_graph_data['fri'][11],

                weekend_load_graph_data['fri'][12],

                weekend_load_graph_data['fri'][13],

                weekend_load_graph_data['fri'][14],

                weekend_load_graph_data['fri'][15],

                weekend_load_graph_data['fri'][16],

                weekend_load_graph_data['fri'][17],

                weekend_load_graph_data['fri'][18],

                weekend_load_graph_data['fri'][19],

                weekend_load_graph_data['fri'][20],

                weekend_load_graph_data['fri'][21],

                weekend_load_graph_data['fri'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Sat',

                data: [

                weekend_load_graph_data['sat'][0],

                weekend_load_graph_data['sat'][1],

                weekend_load_graph_data['sat'][2],

                weekend_load_graph_data['sat'][3],

                weekend_load_graph_data['sat'][4],

                weekend_load_graph_data['sat'][5],

                weekend_load_graph_data['sat'][6],

                weekend_load_graph_data['sat'][7],

                weekend_load_graph_data['sat'][8],

                weekend_load_graph_data['sat'][9],

                weekend_load_graph_data['sat'][10],

                weekend_load_graph_data['sat'][11],

                weekend_load_graph_data['sat'][12],

                weekend_load_graph_data['sat'][13],

                weekend_load_graph_data['sat'][14],

                weekend_load_graph_data['sat'][15],

                weekend_load_graph_data['sat'][16],

                weekend_load_graph_data['sat'][17],

                weekend_load_graph_data['sat'][18],

                weekend_load_graph_data['sat'][19],

                weekend_load_graph_data['sat'][20],

                weekend_load_graph_data['sat'][21],

                weekend_load_graph_data['sat'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

              {

                name: 'Sun',

                data: [

                weekend_load_graph_data['sun'][0],

                weekend_load_graph_data['sun'][1],

                weekend_load_graph_data['sun'][2],

                weekend_load_graph_data['sun'][3],

                weekend_load_graph_data['sun'][4],

                weekend_load_graph_data['sun'][5],

                weekend_load_graph_data['sun'][6],

                weekend_load_graph_data['sun'][7],

                weekend_load_graph_data['sun'][8],

                weekend_load_graph_data['sun'][9],

                weekend_load_graph_data['sun'][10],

                weekend_load_graph_data['sun'][11],

                weekend_load_graph_data['sun'][12],

                weekend_load_graph_data['sun'][13],

                weekend_load_graph_data['sun'][14],

                weekend_load_graph_data['sun'][15],

                weekend_load_graph_data['sun'][16],

                weekend_load_graph_data['sun'][17],

                weekend_load_graph_data['sun'][18],

                weekend_load_graph_data['sun'][19],

                weekend_load_graph_data['sun'][20],

                weekend_load_graph_data['sun'][21],

                weekend_load_graph_data['sun'][22]

                ],

                zIndex: 1,

                fillOpacity: 0,

                marker: {

                    lineWidth: 2,

                    enabled: true,

                    symbol: 'circle'

                }

              },

            

            ]

        });

</script>
</div>

<div id="page2" class="w910"  style="display: none;">
    <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tbody>
    <tr>
      <td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img1.jpg" alt="" style="display:inline-block"/>8</td>
    </tr>

    <tr>
      <td style="padding: 0 0 50px;">
       <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-9-head.jpg" alt="">

          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
   <td  valign="top">
    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
     <tbody>

      <tr>
       <td align="left" valign="top">
        <table width="80%" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000;">
         <tr>
          <td width="50%" height="30" align="center" valign="middle" style="font-family: Arial, Helvetica, sans-serif, sans-serif; color:#fff; font-size:14px; text-transform:uppercase;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-9-subhead1.jpg" alt=""></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
       <tr>
        <td  height="10px">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" style="">
         <div id="typical_export_graph" style="margin: 1px 1px 1px 1px;"></div>

       </td>
     </tr>
   </table>
 </td>
 <td align="right" valign="top">
  <table width="80%" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000;">
   <tr>
    <td width="50%" height="30" align="center" valign="middle" style="font-family: Arial, Helvetica, sans-serif, sans-serif; color:#fff; font-size:14px; text-transform:uppercase;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-9-subhead2.jpg" alt=""></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
  <td  height="10px">&nbsp;</td>
</tr>
<tr>
  <td align="right" valign="top" style="">
   <div id="worst_export_graph" style="margin: 1px 1px 1px 1px;"></div>

 </td>
</tr>
</table>
</td>
</tr>
<tr>
 <td  height="50px">&nbsp;</td>
</tr>
<tr>
 <td align="left" valign="top">
  <table width="80%" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000;">
   <tr>
    <td width="50%" height="30" align="center" valign="middle" style="font-family: Arial, Helvetica, sans-serif, sans-serif; color:#fff; font-size:14px; text-transform:uppercase;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-9-subhead3.jpg" alt=""></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
  <td  height="10px">&nbsp;</td>
</tr>
<tr>
  <td align="left" valign="top" style="">
   <div id="high_export_graph" style="margin: 1px 1px 1px 1px;"></div>

 </td>
</tr>
</table>
</td>
<td align="right" valign="top">
  <table width="80%" border="0" cellspacing="0" cellpadding="0" style="background-color:#000000;">
   <tr>
    <td width="50%" height="30" align="center" valign="middle" style="font-family: Arial, Helvetica, sans-serif, sans-serif; color:#fff; font-size:14px; text-transform:uppercase;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-9-subhead4.jpg" alt=""></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
  <td  height="10px">&nbsp;</td>
</tr>
<tr>
  <td align="right" valign="top" style="">
   <div id="best_export_graph" style="margin: 1px 1px 1px 1px;"></div>

 </td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
  <td  height="160px">&nbsp;</td>
</tr>
<tr>
      <td style="padding:20px 50px 0; text-align:left; font-size:13px; color:#666;">
        WWW.13KUGA.COM.AU   |   <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>
      </td>
    </tr>
</tbody>
</table>

<script type="text/javascript">


  var typical_export_load_graph_data = "<?php echo json_encode($typical_export_load_graph_data); ?>";
  typical_export_load_graph_data = JSON.parse(typical_export_load_graph_data);

  var typical_export_sol_prd_graph_data = "<?php echo json_encode($typical_export_sol_prd_graph_data); ?>";
  typical_export_sol_prd_graph_data = JSON.parse(typical_export_sol_prd_graph_data);

  var typical_export_export_graph_data = "<?php echo json_encode($typical_export_export_graph_data); ?>";
  typical_export_export_graph_data = JSON.parse(typical_export_export_graph_data);

  var typical_export_offset_graph_data = "<?php echo json_encode($typical_export_offset_graph_data); ?>";
  typical_export_offset_graph_data = JSON.parse(typical_export_offset_graph_data);


  var worst_export_load_graph_data = "<?php echo json_encode($worst_export_load_graph_data); ?>";
  worst_export_load_graph_data = JSON.parse(worst_export_load_graph_data);

  var worst_export_sol_prd_graph_data = "<?php echo json_encode($worst_export_sol_prd_graph_data); ?>";
  worst_export_sol_prd_graph_data = JSON.parse(worst_export_sol_prd_graph_data);

  var worst_export_export_graph_data = "<?php echo json_encode($worst_export_export_graph_data); ?>";
  worst_export_export_graph_data = JSON.parse(worst_export_export_graph_data);

  var worst_export_offset_graph_data = "<?php echo json_encode($worst_export_offset_graph_data); ?>";
  worst_export_offset_graph_data = JSON.parse(worst_export_offset_graph_data);


  var high_export_load_graph_data = "<?php echo json_encode($high_export_load_graph_data); ?>";
  high_export_load_graph_data = JSON.parse(high_export_load_graph_data);

  var high_export_sol_prd_graph_data = "<?php echo json_encode($high_export_sol_prd_graph_data); ?>";
  high_export_sol_prd_graph_data = JSON.parse(high_export_sol_prd_graph_data);

  var high_export_export_graph_data = "<?php echo json_encode($high_export_export_graph_data); ?>";
  high_export_export_graph_data = JSON.parse(high_export_export_graph_data);

  var high_export_offset_graph_data = "<?php echo json_encode($high_export_offset_graph_data); ?>";
  high_export_offset_graph_data = JSON.parse(high_export_offset_graph_data);


  var best_export_load_graph_data = "<?php echo json_encode($best_export_load_graph_data); ?>";
  best_export_load_graph_data = JSON.parse(best_export_load_graph_data);

  var best_export_sol_prd_graph_data = "<?php echo json_encode($best_export_sol_prd_graph_data); ?>";
  best_export_sol_prd_graph_data = JSON.parse(best_export_sol_prd_graph_data);

  var best_export_export_graph_data = "<?php echo json_encode($best_export_export_graph_data); ?>";
  best_export_export_graph_data = JSON.parse(best_export_export_graph_data);

  var best_export_offset_graph_data = "<?php echo json_encode($best_export_offset_graph_data); ?>";
  best_export_offset_graph_data = JSON.parse(best_export_offset_graph_data);

  var i = 1;
  var labels = {};
  Highcharts.chart('typical_export_graph', {
    chart: {
      type: 'area',
      width: 320,
      height: 320,
    },
    credits: {
      enabled: false
    },
    navigation: {
      buttonOptions: {
        enabled: false
      }
    },
    title: {
      style: {
        display: 'none'
      }
    },
    xAxis: {
      title: {
        text: 'Hours'
      },
      type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                  formatter: function() {
                    return Highcharts.dateFormat('%H:%M:%S', this.value);

                  },
                },
              },
              yAxis: {
                allowDecimals: true,
                title: {
                 text: 'POWER',
                 style: {
                  "fontSize": "16px",
                  "color": '#000',
                  "fontWeight": 'bold'
                }
              },
              labels: {
                formatter: function() {
                  return this.value;
                }
              }
            },
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              x: 0,
              y: 0,
              itemStyle: {
                "fontSize": "9px",
                "color": '#666',
                "fontWeight": 'normal'
              }
            },
            plotOptions: {
              area: {
                pointStart: 1940,
                marker: {
                  enabled: false,
                  symbol: 'circle',
                  radius: 2,
                  states: {
                    hover: {
                      enabled: true
                    }
                  }
                }
              },
              series: {
                pointStart: Date.UTC(2016, 0, 17),
                pointInterval: 3600 * 1000
              }
            },
            series: [{
              name: 'Load',
              data: [
              typical_export_load_graph_data[0],
              typical_export_load_graph_data[1],
              typical_export_load_graph_data[2],
              typical_export_load_graph_data[3],
              typical_export_load_graph_data[4],
              typical_export_load_graph_data[5],
              typical_export_load_graph_data[6],
              typical_export_load_graph_data[7],
              typical_export_load_graph_data[8],
              typical_export_load_graph_data[9],
              typical_export_load_graph_data[10],
              typical_export_load_graph_data[11],
              typical_export_load_graph_data[12],
              typical_export_load_graph_data[13],
              typical_export_load_graph_data[14],
              typical_export_load_graph_data[15],
              typical_export_load_graph_data[16],
              typical_export_load_graph_data[17],
              typical_export_load_graph_data[18],
              typical_export_load_graph_data[19],
              typical_export_load_graph_data[20],
              typical_export_load_graph_data[21],
              typical_export_load_graph_data[22]
              ],
              color: '#4B4B4B',
              type: 'area',
            }, {
              name: 'Solar Output',
              data: [
              typical_export_sol_prd_graph_data[0],
              typical_export_sol_prd_graph_data[1],
              typical_export_sol_prd_graph_data[2],
              typical_export_sol_prd_graph_data[3],
              typical_export_sol_prd_graph_data[4],
              typical_export_sol_prd_graph_data[5],
              typical_export_sol_prd_graph_data[6],
              typical_export_sol_prd_graph_data[7],
              typical_export_sol_prd_graph_data[8],
              typical_export_sol_prd_graph_data[9],
              typical_export_sol_prd_graph_data[10],
              typical_export_sol_prd_graph_data[11],
              typical_export_sol_prd_graph_data[12],
              typical_export_sol_prd_graph_data[13],
              typical_export_sol_prd_graph_data[14],
              typical_export_sol_prd_graph_data[15],
              typical_export_sol_prd_graph_data[16],
              typical_export_sol_prd_graph_data[17],
              typical_export_sol_prd_graph_data[18],
              typical_export_sol_prd_graph_data[19],
              typical_export_sol_prd_graph_data[20],
              typical_export_sol_prd_graph_data[21],
              typical_export_sol_prd_graph_data[22]
              ],
              color: '#F8FF34',
              zIndex: 1,
              fillOpacity: 0,
              marker: {
                fillColor: 'yellow',
                lineWidth: 2,
                lineColor: 'yellow',
                enabled: true,
                symbol: 'circle'
              }
            },
            {
              name: 'Export',
              data: [
              typical_export_export_graph_data[0],
              typical_export_export_graph_data[1],
              typical_export_export_graph_data[2],
              typical_export_export_graph_data[3],
              typical_export_export_graph_data[4],
              typical_export_export_graph_data[5],
              typical_export_export_graph_data[6],
              typical_export_export_graph_data[7],
              typical_export_export_graph_data[8],
              typical_export_export_graph_data[9],
              typical_export_export_graph_data[10],
              typical_export_export_graph_data[11],
              typical_export_export_graph_data[12],
              typical_export_export_graph_data[13],
              typical_export_export_graph_data[14],
              typical_export_export_graph_data[15],
              typical_export_export_graph_data[16],
              typical_export_export_graph_data[17],
              typical_export_export_graph_data[18],
              typical_export_export_graph_data[19],
              typical_export_export_graph_data[20],
              typical_export_export_graph_data[21],
              typical_export_export_graph_data[22]
              ],
              color: '#FF4B4C',
              type: 'area',
            }, {
              name: 'Offset Consumptio',
              data: [
              typical_export_offset_graph_data[0],
              typical_export_offset_graph_data[1],
              typical_export_offset_graph_data[2],
              typical_export_offset_graph_data[3],
              typical_export_offset_graph_data[4],
              typical_export_offset_graph_data[5],
              typical_export_offset_graph_data[6],
              typical_export_offset_graph_data[7],
              typical_export_offset_graph_data[8],
              typical_export_offset_graph_data[9],
              typical_export_offset_graph_data[10],
              typical_export_offset_graph_data[11],
              typical_export_offset_graph_data[12],
              typical_export_offset_graph_data[13],
              typical_export_offset_graph_data[14],
              typical_export_offset_graph_data[15],
              typical_export_offset_graph_data[16],
              typical_export_offset_graph_data[17],
              typical_export_offset_graph_data[18],
              typical_export_offset_graph_data[19],
              typical_export_offset_graph_data[20],
              typical_export_offset_graph_data[21],
              typical_export_offset_graph_data[22]
              ],
              color: '#81B94C',
              type: 'area',
            }
            ]


          });

var i = 1;
var labels = {};
Highcharts.chart('worst_export_graph', {
  chart: {
    type: 'area',
    width: 320,
    height: 320,
  },
  credits: {
    enabled: false
  },
  navigation: {
    buttonOptions: {
      enabled: false
    }
  },
  title: {
    style: {
      display: 'none'
    }
  },
  xAxis: {
    title: {
      text: 'Hours'
    },
    type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                  formatter: function() {
                    return Highcharts.dateFormat('%H:%M:%S', this.value);

                  },
                },
              },
              yAxis: {
                allowDecimals: true,
                title: {
                  text: 'POWER',
                  style: {
                    "fontSize": "16px",
                    "color": '#000',
                    "fontWeight": 'bold'
                  }
                },
                labels: {
                  formatter: function() {
                    return this.value;
                  }
                }
              },
              legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                  "fontSize": "9px",
                  "color": '#666',
                  "fontWeight": 'normal'
                }
              },
              plotOptions: {
                area: {
                  pointStart: 1940,
                  marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                      hover: {
                        enabled: true
                      }
                    }
                  }
                },
                series: {
                  pointStart: Date.UTC(2016, 0, 17),
                  pointInterval: 3600 * 1000
                }
              },
              series: [{
                name: 'Load',
                data: [
                worst_export_load_graph_data[0],
                worst_export_load_graph_data[1],
                worst_export_load_graph_data[2],
                worst_export_load_graph_data[3],
                worst_export_load_graph_data[4],
                worst_export_load_graph_data[5],
                worst_export_load_graph_data[6],
                worst_export_load_graph_data[7],
                worst_export_load_graph_data[8],
                worst_export_load_graph_data[9],
                worst_export_load_graph_data[10],
                worst_export_load_graph_data[11],
                worst_export_load_graph_data[12],
                worst_export_load_graph_data[13],
                worst_export_load_graph_data[14],
                worst_export_load_graph_data[15],
                worst_export_load_graph_data[16],
                worst_export_load_graph_data[17],
                worst_export_load_graph_data[18],
                worst_export_load_graph_data[19],
                worst_export_load_graph_data[20],
                worst_export_load_graph_data[21],
                worst_export_load_graph_data[22]
                ],
                color: '#4B4B4B',
                type: 'area',
              }, {
                name: 'Solar Output',
                data: [
                worst_export_sol_prd_graph_data[0],
                worst_export_sol_prd_graph_data[1],
                worst_export_sol_prd_graph_data[2],
                worst_export_sol_prd_graph_data[3],
                worst_export_sol_prd_graph_data[4],
                worst_export_sol_prd_graph_data[5],
                worst_export_sol_prd_graph_data[6],
                worst_export_sol_prd_graph_data[7],
                worst_export_sol_prd_graph_data[8],
                worst_export_sol_prd_graph_data[9],
                worst_export_sol_prd_graph_data[10],
                worst_export_sol_prd_graph_data[11],
                worst_export_sol_prd_graph_data[12],
                worst_export_sol_prd_graph_data[13],
                worst_export_sol_prd_graph_data[14],
                worst_export_sol_prd_graph_data[15],
                worst_export_sol_prd_graph_data[16],
                worst_export_sol_prd_graph_data[17],
                worst_export_sol_prd_graph_data[18],
                worst_export_sol_prd_graph_data[19],
                worst_export_sol_prd_graph_data[20],
                worst_export_sol_prd_graph_data[21],
                worst_export_sol_prd_graph_data[22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                  fillColor: 'yellow',
                  lineWidth: 2,
                  lineColor: 'yellow',
                  enabled: true,
                  symbol: 'circle'
                }
              },
              {
                name: 'Export',
                data: [
                worst_export_export_graph_data[0],
                worst_export_export_graph_data[1],
                worst_export_export_graph_data[2],
                worst_export_export_graph_data[3],
                worst_export_export_graph_data[4],
                worst_export_export_graph_data[5],
                worst_export_export_graph_data[6],
                worst_export_export_graph_data[7],
                worst_export_export_graph_data[8],
                worst_export_export_graph_data[9],
                worst_export_export_graph_data[10],
                worst_export_export_graph_data[11],
                worst_export_export_graph_data[12],
                worst_export_export_graph_data[13],
                worst_export_export_graph_data[14],
                worst_export_export_graph_data[15],
                worst_export_export_graph_data[16],
                worst_export_export_graph_data[17],
                worst_export_export_graph_data[18],
                worst_export_export_graph_data[19],
                worst_export_export_graph_data[20],
                worst_export_export_graph_data[21],
                worst_export_export_graph_data[22]
                ],
                color: '#FF4B4C',
                type: 'area',
              }, {
                name: 'Offset Consumptio',
                data: [
                worst_export_offset_graph_data[0],
                worst_export_offset_graph_data[1],
                worst_export_offset_graph_data[2],
                worst_export_offset_graph_data[3],
                worst_export_offset_graph_data[4],
                worst_export_offset_graph_data[5],
                worst_export_offset_graph_data[6],
                worst_export_offset_graph_data[7],
                worst_export_offset_graph_data[8],
                worst_export_offset_graph_data[9],
                worst_export_offset_graph_data[10],
                worst_export_offset_graph_data[11],
                worst_export_offset_graph_data[12],
                worst_export_offset_graph_data[13],
                worst_export_offset_graph_data[14],
                worst_export_offset_graph_data[15],
                worst_export_offset_graph_data[16],
                worst_export_offset_graph_data[17],
                worst_export_offset_graph_data[18],
                worst_export_offset_graph_data[19],
                worst_export_offset_graph_data[20],
                worst_export_offset_graph_data[21],
                worst_export_offset_graph_data[22]
                ],
                color: '#81B94C',
                type: 'area',
              }
              ]


            });

var i = 1;
var labels = {};
Highcharts.chart('high_export_graph', {
  chart: {
    type: 'area',
    width: 320,
    height: 320,
  },
  credits: {
    enabled: false
  },
  navigation: {
    buttonOptions: {
      enabled: false
    }
  },
  title: {
    style: {
      display: 'none'
    }
  },
  xAxis: {
    title: {
      text: 'Hours'
    },
    type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                  formatter: function() {
                    return Highcharts.dateFormat('%H:%M:%S', this.value);

                  },
                },
              },
              yAxis: {
                allowDecimals: true,
                title: {
                  text: 'POWER',
                  style: {
                    "fontSize": "16px",
                    "color": '#000',
                    "fontWeight": 'bold'
                  }
                },
                labels: {
                  formatter: function() {
                    return this.value;
                  }
                }
              },
              legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                  "fontSize": "9px",
                  "color": '#666',
                  "fontWeight": 'normal'
                }
              },
              plotOptions: {
                area: {
                  pointStart: 1940,
                  marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                      hover: {
                        enabled: true
                      }
                    }
                  }
                },
                series: {
                  pointStart: Date.UTC(2016, 0, 17),
                  pointInterval: 3600 * 1000
                }
              },
              series: [{
                name: 'Load',
                data: [
                high_export_load_graph_data[0],
                high_export_load_graph_data[1],
                high_export_load_graph_data[2],
                high_export_load_graph_data[3],
                high_export_load_graph_data[4],
                high_export_load_graph_data[5],
                high_export_load_graph_data[6],
                high_export_load_graph_data[7],
                high_export_load_graph_data[8],
                high_export_load_graph_data[9],
                high_export_load_graph_data[10],
                high_export_load_graph_data[11],
                high_export_load_graph_data[12],
                high_export_load_graph_data[13],
                high_export_load_graph_data[14],
                high_export_load_graph_data[15],
                high_export_load_graph_data[16],
                high_export_load_graph_data[17],
                high_export_load_graph_data[18],
                high_export_load_graph_data[19],
                high_export_load_graph_data[20],
                high_export_load_graph_data[21],
                high_export_load_graph_data[22]
                ],
                color: '#4B4B4B',
                type: 'area',
              }, {
                name: 'Solar Output',
                data: [
                high_export_sol_prd_graph_data[0],
                high_export_sol_prd_graph_data[1],
                high_export_sol_prd_graph_data[2],
                high_export_sol_prd_graph_data[3],
                high_export_sol_prd_graph_data[4],
                high_export_sol_prd_graph_data[5],
                high_export_sol_prd_graph_data[6],
                high_export_sol_prd_graph_data[7],
                high_export_sol_prd_graph_data[8],
                high_export_sol_prd_graph_data[9],
                high_export_sol_prd_graph_data[10],
                high_export_sol_prd_graph_data[11],
                high_export_sol_prd_graph_data[12],
                high_export_sol_prd_graph_data[13],
                high_export_sol_prd_graph_data[14],
                high_export_sol_prd_graph_data[15],
                high_export_sol_prd_graph_data[16],
                high_export_sol_prd_graph_data[17],
                high_export_sol_prd_graph_data[18],
                high_export_sol_prd_graph_data[19],
                high_export_sol_prd_graph_data[20],
                high_export_sol_prd_graph_data[21],
                high_export_sol_prd_graph_data[22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                  fillColor: 'yellow',
                  lineWidth: 2,
                  lineColor: 'yellow',
                  enabled: true,
                  symbol: 'circle'
                }
              },
              {
                name: 'Export',
                data: [
                high_export_export_graph_data[0],
                high_export_export_graph_data[1],
                high_export_export_graph_data[2],
                high_export_export_graph_data[3],
                high_export_export_graph_data[4],
                high_export_export_graph_data[5],
                high_export_export_graph_data[6],
                high_export_export_graph_data[7],
                high_export_export_graph_data[8],
                high_export_export_graph_data[9],
                high_export_export_graph_data[10],
                high_export_export_graph_data[11],
                high_export_export_graph_data[12],
                high_export_export_graph_data[13],
                high_export_export_graph_data[14],
                high_export_export_graph_data[15],
                high_export_export_graph_data[16],
                high_export_export_graph_data[17],
                high_export_export_graph_data[18],
                high_export_export_graph_data[19],
                high_export_export_graph_data[20],
                high_export_export_graph_data[21],
                high_export_export_graph_data[22]
                ],
                color: '#FF4B4C',
                type: 'area',
              }, {
                name: 'Offset Consumptio',
                data: [
                high_export_offset_graph_data[0],
                high_export_offset_graph_data[1],
                high_export_offset_graph_data[2],
                high_export_offset_graph_data[3],
                high_export_offset_graph_data[4],
                high_export_offset_graph_data[5],
                high_export_offset_graph_data[6],
                high_export_offset_graph_data[7],
                high_export_offset_graph_data[8],
                high_export_offset_graph_data[9],
                high_export_offset_graph_data[10],
                high_export_offset_graph_data[11],
                high_export_offset_graph_data[12],
                high_export_offset_graph_data[13],
                high_export_offset_graph_data[14],
                high_export_offset_graph_data[15],
                high_export_offset_graph_data[16],
                high_export_offset_graph_data[17],
                high_export_offset_graph_data[18],
                high_export_offset_graph_data[19],
                high_export_offset_graph_data[20],
                high_export_offset_graph_data[21],
                high_export_offset_graph_data[22]
                ],
                color: '#81B94C',
                type: 'area',
              }
              ]


            });

var i = 1;
var labels = {};
Highcharts.chart('best_export_graph', {
  chart: {
    type: 'area',
    width: 320,
    height: 320,
  },
  credits: {
    enabled: false
  },
  navigation: {
    buttonOptions: {
      enabled: false
    }
  },
  title: {
    style: {
      display: 'none'
    }
  },
  xAxis: {
    title: {
      text: 'Hours'
    },
    type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                  formatter: function() {
                    return Highcharts.dateFormat('%H:%M:%S', this.value);

                  },
                },
              },
              yAxis: {
                allowDecimals: true,
                title: {
                  text: 'POWER',
                  style: {
                    "fontSize": "16px",
                    "color": '#000',
                    "fontWeight": 'bold'
                  }
                },
                labels: {
                  formatter: function() {
                    return this.value;
                  }
                }
              },
              legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
                itemStyle: {
                  "fontSize": "9px",
                  "color": '#666',
                  "fontWeight": 'normal'
                }
              },
              plotOptions: {
                area: {
                  pointStart: 1940,
                  marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                      hover: {
                        enabled: true
                      }
                    }
                  }
                },
                series: {
                  pointStart: Date.UTC(2016, 0, 17),
                  pointInterval: 3600 * 1000
                }
              },
              series: [{
                name: 'Load',
                data: [
                best_export_load_graph_data[0],
                best_export_load_graph_data[1],
                best_export_load_graph_data[2],
                best_export_load_graph_data[3],
                best_export_load_graph_data[4],
                best_export_load_graph_data[5],
                best_export_load_graph_data[6],
                best_export_load_graph_data[7],
                best_export_load_graph_data[8],
                best_export_load_graph_data[9],
                best_export_load_graph_data[10],
                best_export_load_graph_data[11],
                best_export_load_graph_data[12],
                best_export_load_graph_data[13],
                best_export_load_graph_data[14],
                best_export_load_graph_data[15],
                best_export_load_graph_data[16],
                best_export_load_graph_data[17],
                best_export_load_graph_data[18],
                best_export_load_graph_data[19],
                best_export_load_graph_data[20],
                best_export_load_graph_data[21],
                best_export_load_graph_data[22]
                ],
                color: '#4B4B4B',
                type: 'area',
              }, {
                name: 'Solar Output',
                data: [
                best_export_sol_prd_graph_data[0],
                best_export_sol_prd_graph_data[1],
                best_export_sol_prd_graph_data[2],
                best_export_sol_prd_graph_data[3],
                best_export_sol_prd_graph_data[4],
                best_export_sol_prd_graph_data[5],
                best_export_sol_prd_graph_data[6],
                best_export_sol_prd_graph_data[7],
                best_export_sol_prd_graph_data[8],
                best_export_sol_prd_graph_data[9],
                best_export_sol_prd_graph_data[10],
                best_export_sol_prd_graph_data[11],
                best_export_sol_prd_graph_data[12],
                best_export_sol_prd_graph_data[13],
                best_export_sol_prd_graph_data[14],
                best_export_sol_prd_graph_data[15],
                best_export_sol_prd_graph_data[16],
                best_export_sol_prd_graph_data[17],
                best_export_sol_prd_graph_data[18],
                best_export_sol_prd_graph_data[19],
                best_export_sol_prd_graph_data[20],
                best_export_sol_prd_graph_data[21],
                best_export_sol_prd_graph_data[22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                  fillColor: 'yellow',
                  lineWidth: 2,
                  lineColor: 'yellow',
                  enabled: true,
                  symbol: 'circle'
                }
              },
              {
                name: 'Export',
                data: [
                best_export_export_graph_data[0],
                best_export_export_graph_data[1],
                best_export_export_graph_data[2],
                best_export_export_graph_data[3],
                best_export_export_graph_data[4],
                best_export_export_graph_data[5],
                best_export_export_graph_data[6],
                best_export_export_graph_data[7],
                best_export_export_graph_data[8],
                best_export_export_graph_data[9],
                best_export_export_graph_data[10],
                best_export_export_graph_data[11],
                best_export_export_graph_data[12],
                best_export_export_graph_data[13],
                best_export_export_graph_data[14],
                best_export_export_graph_data[15],
                best_export_export_graph_data[16],
                best_export_export_graph_data[17],
                best_export_export_graph_data[18],
                best_export_export_graph_data[19],
                best_export_export_graph_data[20],
                best_export_export_graph_data[21],
                best_export_export_graph_data[22]
                ],
                color: '#FF4B4C',
                type: 'area',
              }, {
                name: 'Offset Consumptio',
                data: [
                best_export_offset_graph_data[0],
                best_export_offset_graph_data[1],
                best_export_offset_graph_data[2],
                best_export_offset_graph_data[3],
                best_export_offset_graph_data[4],
                best_export_offset_graph_data[5],
                best_export_offset_graph_data[6],
                best_export_offset_graph_data[7],
                best_export_offset_graph_data[8],
                best_export_offset_graph_data[9],
                best_export_offset_graph_data[10],
                best_export_offset_graph_data[11],
                best_export_offset_graph_data[12],
                best_export_offset_graph_data[13],
                best_export_offset_graph_data[14],
                best_export_offset_graph_data[15],
                best_export_offset_graph_data[16],
                best_export_offset_graph_data[17],
                best_export_offset_graph_data[18],
                best_export_offset_graph_data[19],
                best_export_offset_graph_data[20],
                best_export_offset_graph_data[21],
                best_export_offset_graph_data[22]
                ],
                color: '#81B94C',
                type: 'area',
              }
              ]


            });
          </script>
</div>

<div class="overlay-roadblock">
    <div class="overlay-roadblock__content" id="overlay_content">
       
    </div>
</div>

<div id="canvas_container">
	<canvas id="canvas" width="910"></canvas>
</div>

<script src="<?php echo site_url(); ?>assets/js/jquery-2.2.3.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script>
	var base_url = '<?php echo site_url(); ?>';
</script>

<script>

	$(document).ready(function(){
		
		$('.overlay-roadblock').show();
		$('#overlay_content').html('<div class="loader"></div>Processing Images for Pdf Creation Please Wait.....');

		setTimeout(function(){
			$('#page1').show();
			var box = document.querySelector('#page1');
			var width = box.offsetWidth;
			var height = box.offsetHeight;

			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')

			html2canvas(document.querySelector("#page1"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				console.log(dataimg);
				$('#dataImg1').val(dataimg);
			});

		},3000);

		setTimeout(function(){
			$('#page1').hide();
			$('#page2').show();

			var box = document.querySelector('#page2');
			var width = box.offsetWidth;
			var height = box.offsetHeight;

			document.querySelector("#canvas").setAttribute('height',height);
			var canvasd = document.getElementById('canvas')

			html2canvas(document.querySelector("#page2"), {canvas: canvas, scale: 1,width:1200,height:height}).then(function(canvas) {
				var dataimg = canvas.toDataURL();
				$('#dataImg2').val(dataimg);
			});

		},4000);


		setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/proposal/save_dataimg_to_png1',
				datatype: 'json',
				data: $('#chartForm').serialize(),
				success: function (stat) {
					$('.overlay-roadblock').show();
					$('#overlay_content').html('<div class="loader"></div>Creating Pdf. Once Downloaded Please close this window.');
					var puuid = "<?php echo $proposal_data['id']; ?>";
					window.location.href = base_url + "admin/proposal/solar_pdf_download/" + puuid + '?view=download';
				},
				error: function (xhr, ajaxOptions, thrownError) {

				}
			});
		},5500);
	});
</script>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Lead Controller
 * @version 1.0
 */
class InverterDesignTool extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Product_Model", "product");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(3) != 'solar_quote_pdf_download') {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status' => 'Unauthorized Access. Looks like you are not logged in.',
                    'authenticated' => FALSE
                ));
                die;
            } else {
                redirect('admin/login');
            }
        }
    }

    public function fetch_design_tool_data(){
        $data = array();
        $proposal_id = $this->input->get('proposal_id');
        $lead_id = $this->input->get('lead_id');
        $site_id = $this->input->get('site_id');
        $tool_data = $this->common->fetch_row('tbl_inverter_design_tool_data','*',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'status' => 1));

        if(!empty($tool_data)){
            $tool_data['last_config'] = json_decode($tool_data['last_config']);
        }

        $data['tool_data'] = $tool_data;
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function save_design_tool_data(){
        $data = array();

        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $key = $this->input->post('key');
        $value = $this->input->post('value');
        $stat = FALSE;
        
        $tool_data = $this->common->fetch_row('tbl_inverter_design_tool_data','*',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'status' => 1));

        if(empty($tool_data)){
            $stat = $this->common->insert_data('tbl_inverter_design_tool_data',
                array(
                    'proposal_id' => $proposal_id,
                    'lead_id' => $lead_id,
                    'site_id' => $site_id,
                     $key => $value
                )
            );
        }else{
            $stat =  $this->common->update_data('tbl_inverter_design_tool_data',
                array(
                    'proposal_id' => $proposal_id,
                    'lead_id' => $lead_id,
                    'site_id' => $site_id,
                ),
                array($key => $value)
            );
        }

       
    
        if($stat){
            $data['status'] = 'Data Saved Successfully';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        }else{
            $data['status'] = 'Data Saving Failed';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }

    public function save_design_tool_snapshot(){
        $data = array();
        $proposal_id = $this->input->post('proposal_id');
        $lead_id = $this->input->post('lead_id');
        $site_id = $this->input->post('site_id');
        $snapshot = $this->input->post('snapshot');

        $tool_data = $this->common->fetch_row('tbl_inverter_design_tool_data','*',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'status' => 1));

        if(empty($tool_data)){
            $this->common->insert_data('tbl_inverter_design_tool_data',
                array(
                    'proposal_id' => $proposal_id,
                    'lead_id' => $lead_id,
                    'site_id' => $site_id,
                )
            );

            $tool_data = $this->common->fetch_row('tbl_inverter_design_tool_data','*',array('proposal_id' => $proposal_id,'lead_id' => $lead_id,'site_id' => $site_id,'status' => 1));
        }

        $img1 = explode(';', $snapshot);
        if ($snapshot != '' && count($img1) > 1) {
            $upload_path = './assets/uploads/inverter_tool_snapshots/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, true);
            }
            $image1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $snapshot));
            $snapShotName = date('Y_m_d_s') . '_' . $tool_data['id'] . '_inverter_design_snapshot' . '.png';
            file_put_contents('./assets/uploads/inverter_tool_snapshots/' . $snapShotName, $image1);
            $snapshotUrl = site_url('assets/uploads/inverter_tool_snapshots/' . $snapShotName);
        }

        $this->common->update_data('tbl_inverter_design_tool_data',
            array(
                'proposal_id' => $proposal_id,
                'lead_id' => $lead_id,
                'site_id' => $site_id,
            ),
            array('snapshot' => $snapshotUrl)
        );

        $data['snapshotUrl'] = $snapshotUrl;
        $data['status'] = 'Snapshot Created Successfully';
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

   
}

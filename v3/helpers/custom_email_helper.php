<?php

function send_customer_leademail($communication_data = []) {

    $CI = &get_instance();
    $tpl_id = 'LEAD-MAIL-TO-C';
    $template = $CI->common->fetch_where('tbl_communications', '*', array('template_id' => $tpl_id));
    $body = $template[0]['template'];

    $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
    $body = str_replace("{SERVICE_NAME}", $communication_data['category_name'], $body);
    $body = str_replace("{SITE_LOGO}", '<img src="' . site_url() . 'assets/images/dashboard-logo.svg" alt="Logo" border="0" width="64" />', $body);
    $body = str_replace("\r\n", '<br/>', $body);

    $subject = $template[0]['subject'];

    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => '111.67.20.163',
        'smtp_port' => '587',
        'smtp_user' => '_mainaccount@solarrunapp.com.au',
        'smtp_pass' => '6mKK7w6KHuVmmAX',
        'mailtype' => 'html',
        'charset' => 'UTF-8',
        'newline' => "\r\n",
        'crlf' => "\n",
    );

    $CI->load->library('email', $config);
    $CI->email->set_newline("\r\n");

    $CI->email->to($communication_data['customer_email']);
    $CI->email->from('no-reply@solarrun.com', 'Solar Run');
    $CI->email->subject($subject);
    $CI->email->message($body);
    $result = $CI->email->send();
    //echo $CI->email->print_debugger();	
}

function send_franchise_leademail($communication_data = [],$new = true) {

    $CI = &get_instance();
    $tpl_id = 'LEAD-MAIL-TO-F';
    $template = $CI->common->fetch_where('tbl_communications', '*', array('template_id' => $tpl_id));
    $body = $template[0]['template'];
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => '111.67.20.163',
        'smtp_port' => '587',
        'smtp_user' => '_mainaccount@solarrunapp.com.au',
        'smtp_pass' => '6mKK7w6KHuVmmAX',
        'mailtype' => 'html',
        'charset' => 'UTF-8',
        'newline' => "\r\n",
        'crlf' => "\n",
    );

    $body = str_replace("{FRANCHISE_NAME}", $communication_data['franchise_name'], $body);
    $body = str_replace("{SERVICE_NAME}", $communication_data['category_name'] . '/' . $communication_data['sub_category_name'], $body);
    $body = str_replace("{SITE_LOGO}", '<img src="' . site_url() . 'assets/images/dashboard-logo.svg" alt="Logo" border="0" width="64" />', $body);
    $body = str_replace("\r\n", '<br/>', $body);

    $body = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $body);
    $body = str_replace("{CUSTOMER_POSTCODE}", $communication_data['postcode'], $body);
    $body = str_replace("{CUSTOMER_MOBILE}", $communication_data['customer_contact_no'], $body);
    $body = str_replace("{CUSTOMER_NOTE}", ($communication_data['note'] == '') ? '_' : $communication_data['note'], $body);
    $body = str_replace("{CUSTOMER_ADDRESS}", "{$communication_data['address']}", $body);
    $body = str_replace("{ROOF_TYPE}", $communication_data['roof_type'], $body);
    $body = str_replace("{STOREYS}", $communication_data['no_of_stories'], $body);
    
    
    $subject = (isset($new) && $new == TRUE) ? 'NEW LEAD' : "FOLLOW UP";
    $subject = $subject . " " . $template[0]['subject'];
    $subject = str_replace("{SERVICE_NAME}", $communication_data['sub_category_name'], $subject);
    $subject = str_replace("{CUSTOMER_NAME}", $communication_data['customer_name'], $subject);
    $subject = str_replace("{CUSTOMER_ADDRESS}", "{$communication_data['address']}", $subject);
    $subject = str_replace("{CUSTOMER_POSTCODE}", $communication_data['postcode'], $subject);
    
    
    $CI->load->library('email', $config);
    $CI->email->set_newline("\r\n");

    $CI->email->to($communication_data['franchise_email']);
    $CI->email->from(SITE_EMAIL, 'Solar Run');
    $CI->email->subject($subject);
    $CI->email->message($body);
    $result = $CI->email->send();
}

function franchise_setttings_change_email($communication_data = [], $to = 'service@solarrun.com.au') {
    #create an Ancillary Class
    $CI = &get_instance();
    $body = 'A franchise has requested to change the franchise settings.';
    $body .= '<br/><br/>';
    $body .= 'Franchise Name: ' . $communication_data['franchise_name'];
    $body .= '<br/><br/>';
    $body .= 'Franchise System Id: ' . $communication_data['id'];
    $body .= '<br/><br/><br/>';
    $body .= 'Note: Please visit in Franchisor(Admin) Franchise Section > Manage Request Settings to take appropriate action.';

    $subject = 'SolarRun - Franchise Settings Change Requested';
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => '111.67.20.163',
        'smtp_port' => '587',
        'smtp_user' => '_mainaccount@solarrunapp.com.au',
        'smtp_pass' => '6mKK7w6KHuVmmAX',
        'mailtype' => 'html',
        'charset' => 'UTF-8',
        'newline' => "\r\n",
        'crlf' => "\n",
    );
    $CI->load->library('email', $config);

    $CI->email->to($to);
    $CI->email->from('no-reply@solarrun.com.au', 'Solar Run');

    $CI->email->subject($subject);
    $CI->email->message($body);
    $result = $CI->email->send();
}


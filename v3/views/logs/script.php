<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<?php
//$runningStatus      = ['Not Started', 'Running', 'In Review', 'Completed'];
//$runningStatusClass = ['badge-secondary', 'badge-warning', 'badge-info', 'badge-success'];
?>
<script>
    var runningStatus = <?php echo json_encode($runningStatus) ?>;
    var runningStatusClass = <?php echo json_encode($runningStatusClass) ?>;
    $(document).ready(function () {
        CKEDITOR.replace('mail-editor', {
            removePlugins: 'sourcearea,image,about,document'
        });
        $('.select2').select2({minimumResultsForSearch: -1});
        loadThread();
    });
    function loadThread() {
        var issue_id = $('#issue_id').val();
        var current_status = $('#curent-issue-status').val();
        $('.js-status-container').html('<span class="badge badge-pill ' + runningStatusClass[current_status] + '">' + runningStatus[current_status] + '</span>');
        var last_message_id = 0;
        if ($('.js-messgae-list').last().attr('data-id')) {
            last_message_id = $('.js-messgae-list').last().attr('data-id');
        }
        var form_data = {'issue_id': issue_id, 'last_message_id': last_message_id};
        $.ajax({
            url: base_url + "admin/log/load-thread",
            type: 'get',
            data: form_data,
            beforeSend: function () {

            },
            success: function (response) {
                if (last_message_id) {
                    $('#time-line-thread').append(response);
                } else {
                    $('#time-line-thread').html(response);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }
    function emptyReviewForm() {
        CKEDITOR.instances['mail-editor'].setData('');
        $('#mail-editor').val('');
        $('#attachment').val('');
    }
    $('#attachment').change(function () {
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'issue_files');
        toastr["info"]("Uploading image please wait...");
        $('#js-submit').attr('disabled', 'disabled');
        $.ajax({
            url: base_url + 'admin/page/upload_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                $('#js-submit').removeAttr('disabled');
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#attachment-file').val(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function () {
                $('#submit_btn').removeAttr('disabled');
                $('#loader').html('');
            }
        });
    });
    $(document).on('submit', '#review-form-handle', function (e) {
        e.preventDefault();
        var formData = $(this).serialize();
        $('#js-submit').hide();
        $('#js-ldr').show();
        $.ajax({
            url: base_url + "admin/log/create-message",
            type: 'post',
            data: formData,
            beforeSend: function () {

            },
            success: function (response) {
                $('#js-ldr').hide();
                $('#js-submit').show();
                emptyReviewForm();
                var output = JSON.parse(response);
                toastr["success"](output.message);
                if (output.status) {
                    loadThread();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#js-ldr').hide();
                $('#js-submit').show();
            }
        });
    });

</script>
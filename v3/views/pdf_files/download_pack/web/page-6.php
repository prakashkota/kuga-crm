<style>
    @font-face {
        font-family: 'CenturyGothic';
        font-weight: normal;
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf');
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('embedded-opentype'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('woff'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothic.otf') format('truetype');
    }

    @font-face {
        font-family: 'CenturyGothic';
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf');
        src: url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('embedded-opentype'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('woff'),
            url('https://kugacrm.com.au/assets/fonts/Century_Gothic/CenturyGothicPro-Bold.otf') format('truetype');
        font-weight: bold;
    }

    .century_gothic,
    .cls_004 {
        font-family: 'CenturyGothic';
    }


    .page-6-back-bg {
        background-color: #FFFFFF;
        background-repeat: no-repeat;
        margin: auto;
        padding: 0;
        background-size: 100% 100%;
        max-width: 1287px;
        max-height: 870px;
        box-shadow: 1px 1px 3px 1px #333;
        border-collapse: separate;

    }
    body {
        margin: 0;
    }
</style>

<body class="century_gothic">
    <div class="page-6-back-bg page-spl">
        <img src="https://kugacrm.com.au/job_crm/assets/evidence_form/page-6.jpg" width="1287px" height="870px">
    </div>
</body>
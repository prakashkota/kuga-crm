
var lead_map_manager = function (options) {
    var self = this;
    this.user_id = options.user_id;
    this.user_group = options.user_group;
    this.all_reports_view_permission = options.all_reports_view_permission;
    this.is_sales_rep = options.is_sales_rep;
    this.map_container = document.getElementById('map');
    this.lead_list = [];
    this.temp_lead_list = [];
    this.locations = [];
    this.markers = [];
    this.markerCluster = '';
    this.map = {};
    this.circle = {};
    this.directionsService = {};
    this.directionsDisplay = {};
    this.drawingManager = {};
    this.loading = true;
    this.next_page = 1;
    this.width = 0;
    this.logged_in_user_data = options.logged_in_user_data;
    this.filter = {};
    this.lead_stages = options.lead_stages;
    this.lead_types = options.lead_types;
    this.limit = 300;
    
    
    $('#load_more_btn').click(function () {
        $(this).slideUp();
        var url = '';
        if(self.filter.kewyord && self.filter.kewyord != ''){
            url = base_url + 'admin/lead/fetch_lead_stage_data?view=map&limit='+self.limit+'&page=' + self.next_page +  '&search_by='+self.filter.search_by + '&keyword=' + self.filter.kewyord 
                    + '&filter_by_type=' + self.filter.filter_by_type + '&filter_by_stage=' + self.filter.filter_by_stage + '&filter_by_segment=' + self.filter.filter_by_segment;;
        }else if(self.filter.lat && self.filter.lat != ''){
            url = base_url + 'admin/lead/fetch_lead_stage_data?view=map&limit='+self.limit+'&page='+ '&lat=' + self.filter.lat + '&lng='+self.filter.lng;
        }
        self.fetch_lead_list(url);
    });

    $('#search_filter_btn').click(function () {
        $('.dropdown-toggle').trigger('click');
        self.next_page = 1;
        $('#load_more_btn').slideUp();
        var search_filter_by = $('#search_filter_by').val();
        var keyword = $('#search_filter').val();
        var filter_by_type = $('#filter_by_type').val();
        console.log(filter_by_type);
        var filter_by_stage = $('#filter_by_stage').val();
        var filter_by_segment = $('#filter_by_segment').val();
        if(keyword == '' && filter_by_type == null && filter_by_stage == null && filter_by_segment == null){
            toastr["error"]('Please Enter a kewyord or Select a filter by option.');
            return false;
        }
        var url = base_url + 'admin/lead/fetch_lead_stage_data?view=map&limit='+self.limit+'&page=' + self.next_page + '&search_by='+search_filter_by+'&keyword=' + keyword 
                + '&filter_by_type=' + filter_by_type + '&filter_by_stage=' + filter_by_stage + '&filter_by_segment=' + filter_by_segment;
        self.filter.search_by = search_filter_by;
        self.filter.kewyord = keyword;
        self.filter.filter_by_type = filter_by_type;
        self.filter.filter_by_stage = filter_by_stage;
        self.filter.filter_by_segment = filter_by_segment;
        self.fetch_lead_list(url);
        if (self.circle != null && self.circle.length > 0) {
            self.circle.setMap(null);
            self.circle = null;
        }
    });
    
    $("#filter_by_type").multipleSelect({
        filter: true,
        keepOpen: false
    });

     $("#filter_by_stage").multipleSelect({
        filter: true,
        keepOpen: false
    });

    $("#filter_by_segment").multipleSelect({
        filter: true,
        keepOpen: false
    });
};


lead_map_manager.prototype.initialize_map = function () {
    var self = this;
    var center = {};
    if (self.logged_in_user_data.latitude != null && self.logged_in_user_data.latitude != 'null' && self.logged_in_user_data.latitude != '') {
        center = {lat: parseFloat(self.logged_in_user_data.latitude), lng: parseFloat(self.logged_in_user_data.longitude)};
    } else {
        center = {lat: -25.344, lng: 131.036};
    }
    self.directionsService = new google.maps.DirectionsService();
    self.directionsDisplay = new google.maps.DirectionsRenderer();
    self.map = new google.maps.Map(self.map_container, {
        zoom: 4,
        center: center,
        zoomControl: false,
        fullscreenControl: false,
        mapTypeControl: true,
        mapTypeControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT,
            
        }
    });

    //Set Drawing manager circle
    /**self.drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT,
            drawingModes: ['circle']
        },
        markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
        circleOptions: {
            fillColor: '#0071cce7',
            strokeColor: '#2271cce7',
            strokeWidth: 0,
            clickable: false,
            editable: true,
        }
    });
    self.drawingManager.setMap(self.map);
    
    google.maps.event.addListener(self.drawingManager, 'circlecomplete', self.on_circle_complete);
    */
   
    self.directionsDisplay.setMap(self.map);
    self.fetch_lead_list();
};

lead_map_manager.prototype.on_circle_complete = function (shape) {
    window.lead_map_manager_tool.drawingManager.setDrawingMode(null);
    
    if (shape == null || (!(shape instanceof google.maps.Circle)))
        return;

    if (this.circle != null) {
        this.circle.setMap(null);
        this.circle = null;
    }

    this.circle = shape;
     window.lead_map_manager_tool.circle = this.circle;
    
    function toRad(degrees){
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    
    //Using Pythogorus theorem and circle bounds
    var lat1 = this.circle.getBounds().getNorthEast().lat();
    var lng1 = this.circle.getBounds().getNorthEast().lng();
    var lat2 = this.circle.getBounds().getSouthWest().lat();
    var lng2 = this.circle.getBounds().getSouthWest().lng();
    var x = toRad(lng2-lng1) ;
    var y = toRad(lat2-lat1);
    var R = 6371000; // gives d in metres
    var d = (Math.sqrt(x*x + y*y) * R) / 1000;
    d = parseInt(d);
    var d1 = Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2));
    d1 = parseInt(d1);
    console.log(d1);
    //console.log('radius',  this.circle.getRadius());
    //console.log('lat',  self.circle.getCenter().lat());
    //console.log('lng',  self.circle.getCenter().lng());
    var url = base_url + 'admin/lead/fetch_lead_stage_data?view=map&limit='+self.limit+'&page='+ window.lead_map_manager_tool.next_page +'&lat=' + this.circle.getCenter().lat() + '&lng='+this.circle.getCenter().lng() +'&d=' + d1;
    window.lead_map_manager_tool.next_page = 1;
    window.lead_map_manager_tool.filter.lat = this.circle.getCenter().lat();
    window.lead_map_manager_tool.filter.lng = this.circle.getCenter().lng();
    window.lead_map_manager_tool.fetch_lead_list(url);
}

lead_map_manager.prototype.fetch_lead_list = function (custom_url) {
    var self = this;
    var url = '';
    if (custom_url && custom_url != '') {
        url = custom_url;
    } else {
        url = base_url + 'admin/lead/fetch_lead_stage_data?view=map&limit='+self.limit+'&page=' + self.next_page;
    }
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            self.loading = true;
            $('#card_container').addClass('hidden');
            $('#card_container_placeholder').removeClass('hidden');
        },
        success: function (response) {
            $('#card_container').removeClass('hidden');
            $('#card_container_placeholder').addClass('hidden');
            $('#lead_list_body').html('');
            if (response.success == true) {
                if (response.lead_data.length > 0) {
                    //Only show load more furthur if their is more data
                    self.loading = false;
                }
                if (custom_url && custom_url != '') {
                    //self.map.setZoom(12);
                    self.lead_list = [];
                    document.getElementById('card_container').innerHTML = '';
                }
                self.next_page = self.next_page + 1;
                self.temp_lead_list = [];
                
                for (var i = 0; i < response.lead_data.length; i++) {
                    var lat = response.lead_data[i].latitude;
                    if (lat != null && lat != 'null' && lat != '') {
                        var uuid = response.lead_data[i].uuid;
                        self.temp_lead_list.push(response.lead_data[i]);
                        self.lead_list[uuid] = response.lead_data[i];
                    }
                }
                self.handle_lead_card(custom_url);
            } else {
                if(response.hasOwnProperty('authenticated')){
                    toastr['error'](response.status);
                    setTimeout(function(){
                        window.location.reload();
                    },2000);
                }else{
                    toastr["error"]('Something went wrong please try again.');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

lead_map_manager.prototype.handle_lead_card = function (custom_url) {
    var self = this;
    for (var key in self.temp_lead_list) {
        var address = self.temp_lead_list[key].customer_address;
        if (address != null && address != 'null' && address != '') {
            var deal_card = self.create_deal_card(self.temp_lead_list[key], key);
            var lead_stage = parseInt(self.temp_lead_list[key].lead_stage);
            document.getElementById('card_container').appendChild(deal_card);
            self.width += 400;
            if (lead_stage > 2 && lead_stage < 8) {
                $('#card_' + self.temp_lead_list[key].uuid).find('.card_header_icon_dark').addClass('hidden');
                $('#card_' + self.temp_lead_list[key].uuid).find('.card_header_icon_light').removeClass('hidden');
            }
        }
    }

    $('#card_container').attr('style', 'width:' + self.width + 'px;');
    //Add Markers for Deals
    self.handle_marker_and_clusters(custom_url);
    //Bind Actions
    self.handle_actions();
    var lastLeftLocation = 0;
    $('.card-sc').scroll(function (e) {
        if (!self.loading) {
            var $elem = $('.card-sc');
            var newScrollLeft = $elem.scrollLeft(),
                    width = $elem.outerWidth(),
                    scrollWidth = $elem.get(0).scrollWidth;
            if (scrollWidth - newScrollLeft == width) {
                $('#load_more_btn').slideDown();
            } else {
                $('#load_more_btn').slideUp();
            }
        }
    });
};


lead_map_manager.prototype.handle_color = function (lead_stage) {
    if (lead_stage > 0 && lead_stage < 3) {
        return 'orange';
    } else if (lead_stage > 2 && lead_stage < 8) {
        return 'black';
    } else if (lead_stage > 7 && lead_stage < 10) {
        return 'amber';
    } else if (lead_stage == 10) {
        return 'blue';
    } else if (lead_stage == 11) {
        return 'green';
    } else if (lead_stage == 12) {
        return 'red';
    }
};


lead_map_manager.prototype.handle_marker_and_clusters = function (custom_url) {
    var self = this;

    //If search via keyword remove all previous markers and clusters
    if (custom_url && custom_url != '') {
        for (var i = 0; i < self.markers.length; i++) {
            self.markers[i].setMap(null);
        }
        //self.markerCluster.clearMarkers();
        self.markers = [];
        //self.markerCluster = '';
    }
    
    var oms = new OverlappingMarkerSpiderfier(self.map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });
    
    var counter = 0;
    var markers = self.temp_lead_list.map(function (lead_data, i) {
        if (lead_data.hasOwnProperty('latitude')) {
            var key = lead_data['uuid'];

            var mk = new google.maps.Marker({
                id: i,
                uuid: key,
                position: {'lat': parseFloat(lead_data['latitude']), lng: parseFloat(lead_data['longitude'])},
                icon: {
                    url: base_url + "assets/images/mk-" + self.handle_color(parseInt(self.lead_list[key].lead_stage)) + ".png?v=0.1"
                },
                selected: false,
            });
            google.maps.event.addListener(mk, 'click', function () {
                /** for (var i = 0; i < self.markers.length; i++) {
                    var key = self.markers[i].uuid;
                    self.markers[i].setAnimation(null);
                    self.markers[i].setIcon(base_url + "assets/images/mk-" + self.handle_color(parseInt(self.lead_list[key].lead_stage)) + ".png");
                    $('.kg-lead_card').removeClass('kg-lead_card--pink');
                    $('#card_' + key).find('.card_header_icon_light').addClass('hidden');
                    $('#card_' + key).find('.card_header_icon_dark').removeClass('hidden');
                    if ($('.kg-lead_card').hasClass('opened')) {
                        $('.kg-lead_card').removeClass('opened');
                        $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function () {
                            $('.kg-lead_card').css('height', '100px');
                        });
                    }
                } */
                
                if(self.markers[this.id].selected){
                    self.markers[this.id].selected = false;
                    
                    var key = self.markers[this.id].uuid;
                    self.markers[i].setAnimation(null);
                    self.markers[i].setIcon(base_url + "assets/images/mk-" + self.handle_color(parseInt(self.lead_list[key].lead_stage)) + ".png?v=0.1");
                    //$('#card_' + key).removeClass('kg-lead_card--pink');
                    //$('#card_' + key).find('.card_header_icon_light').addClass('hidden');
                    //$('#card_' + key).find('.card_header_icon_dark').removeClass('hidden');
                    if ($('.kg-lead_card').hasClass('opened')) {
                        $('.kg-lead_card').removeClass('opened');
                        $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function () {
                            $('.kg-lead_card').css('height', '100px');
                        });
                    }
                    $('#card_' + key).addClass('hidden');
                }else{
                     self.markers[this.id].selected = true;
                     
                     var key = self.markers[this.id].uuid;
                    //self.markers[this.id].setAnimation(google.maps.Animation.BOUNCE);
                    var icon = {
                        url: base_url + "assets/images/mk-pink.png", // url
                        scaledSize: new google.maps.Size(35, 35),
                    };
                    self.markers[this.id].setIcon(icon);
                    //$('#card_' + key).addClass('kg-lead_card--pink');
                    //$('#card_' + key).find('.card_header_icon_light').removeClass('hidden');
                    //$('#card_' + key).find('.card_header_icon_dark').addClass('hidden');
                    document.getElementById('card_' + key).scrollIntoView({ block: "end", inline: "center"});
                    $('#card_' + key).removeClass('hidden');
                }
                
            });
            self.markers.push(mk);
            oms.addMarker(mk);
            //mk.setMap(self.map);
            //So Pick First Marker Position and Centerized and Zoom the Map
            if(counter == 0){
                self.map.setCenter(mk.getPosition());
                self.map.setZoom(11);
            }
            return mk;
        }
    });
    //self.markerCluster = new MarkerClusterer(self.map, markers, {imagePath: base_url + 'assets/images/m'});
};

lead_map_manager.prototype.handle_actions = function () {
    var self = this;

    //Handle Card Toggle
    $('.kg-lead_card').click(function () {
        if ($('.kg-lead_card').hasClass('opened')) {
            $('.kg-lead_card').removeClass('opened');
            $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function() {
                $('.kg-lead_card').css('height','110px');
            });
        } else {
            $('.kg-lead_card').css('height', '218px');
            $('.kg-lead_card').addClass('opened');
            $('.kg-lead_card').find('.kg-lead_card__body_actions').slideDown('slow');
        }
    });

    //Handle Activity
    $('.add_activity').click(function () {
        var id = $(this).attr('data-id');
        var data = self.lead_list[id];
        window.activity_manager_tool.lead_data = data;
        $('#scheduler').removeClass('hidden');
        $('#attendees').removeClass('hidden');
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#mark_as_completed').addClass('hidden');
        $('.datepicker').show();
        self.activity_action = "Add Activity";
        window.activity_manager_tool.activity_trigger = 1;
        $('#activity_modal_body_left').addClass('col-md-6');
        $('#activity_modal_body_left').removeClass('col-md-12');
        $('#activityLocationPostCode').val(data.customer_postcode);
        $('#select2-activityAddress-container').html(data.customer_address);
        $('#activityLocationAddressVal').val(data.customer_address);
        $('#activityLocationState').val(data.state_id).trigger('change');
        var url = base_url + 'admin/lead/add?deal_ref=' + data.uuid;
        $('#deal_link').attr('href', url);
        $('#activity_modal').modal('show');
    });

    //Handle Direction
    $('.get_direction').click(function () {
        var id = $(this).attr('data-id');
        var lead_data = self.lead_list[id];
        var origin = {};
        if (self.logged_in_user_data.latitude != null && self.logged_in_user_data.latitude != 'null' && self.logged_in_user_data.latitude != '') {
            origin = {lat: parseFloat(self.logged_in_user_data.latitude), lng: parseFloat(self.logged_in_user_data.longitude)};
        } else {
            origin = {lat: -37.8232, lng: 144.97290000000001};
        }
        var destination = {lat: parseFloat(lead_data.latitude), lng: parseFloat(lead_data.longitude)};
        self.get_direction_to_destination(origin, destination);
    });
    
    //Handle Close
    $('.close_lead').click(function () {
        $(".kg-lead_card").unbind();
        var id = $(this).attr('data-id');
        for (var i = 0; i < self.markers.length; i++) {
            var key = self.markers[i].uuid;
            if(key == id){
                self.markers[i].selected = false;
                self.markers[i].setAnimation(null);
                self.markers[i].setIcon(base_url + "assets/images/mk-" + self.handle_color(parseInt(self.lead_list[key].lead_stage)) + ".png?v=0.1");
                if ($('.kg-lead_card').hasClass('opened')) {
                    $('.kg-lead_card').removeClass('opened');
                    $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function () {
                        $('.kg-lead_card').css('height', '100px');
                    });
                }
                $('#card_' + key).addClass('hidden');
            }
        }
       setTimeout(function(){
            $('.kg-lead_card').click(function () {
            if ($('.kg-lead_card').hasClass('opened')) {
                $('.kg-lead_card').removeClass('opened');
                $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function() {
                    $('.kg-lead_card').css('height','110px');
                });
            } else {
                $('.kg-lead_card').css('height', '218px');
                $('.kg-lead_card').addClass('opened');
                $('.kg-lead_card').find('.kg-lead_card__body_actions').slideDown('slow');
            }
        });
       },1500)
    });
}

lead_map_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

lead_map_manager.prototype.geocode_address_for_LatLng = function (customer_address, uuid) {
    var self = this;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': customer_address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
        }
        var latlng = {};
        latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        self.temp_lead_list[uuid].coordinates = latlng;
        self.locations.push({coordinates: latlng, uuid: uuid});

    });
};

lead_map_manager.prototype.get_distance = function (p2_lat, p2_lng) {
    var p1_lat = -37.8232;
    var p1_lng = 144.97290000000001;

    var rad = function (x) {
        return x * Math.PI / 180;
    };

    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2_lat - p1_lat);
    var dLong = rad(p2_lng - p1_lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1_lat)) * Math.cos(rad(p2_lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = (R * c) / 1000;
    d = d.toFixed(2);
    return d;

};

lead_map_manager.prototype.create_deal_card = function (data, id) {
    var self = this;
    //var lat = data.latitude;
    //var lng = data.longitude;
    //var distance = self.get_distance(lat, lng);
    var card = document.createElement('div');
    card.className = 'kg-lead_card hidden ';

    var lead_stage = parseInt(data.lead_stage);
    if (lead_stage > 0 && lead_stage < 3) {
        card.className += 'kg-lead_card--orange';
    } else if (lead_stage > 2 && lead_stage < 8) {
        card.className += 'kg-lead_card--black';
    } else if (lead_stage > 7 && lead_stage < 10) {
        card.className += 'kg-lead_card--amber';
    } else if (lead_stage == 10) {
        card.className += 'kg-lead_card--blue';
    } else if (lead_stage == 11) {
        card.className += 'kg-lead_card--green';
    } else if (lead_stage == 12) {
        card.className += 'kg-lead_card--red';
    }
    
    card.setAttribute('id', 'card_' + data.uuid)

    var card_header = document.createElement('div');
    card_header.className = 'card-header';

    var card_header_icon_dark = document.createElement('img');
    card_header_icon_dark.className = 'card_header_icon_dark';
    card_header_icon_dark.setAttribute('src', base_url + 'assets/images/deal_icon_dark_24.png');

    var card_header_icon_light = document.createElement('img');
    card_header_icon_light.className = 'card_header_icon_light hidden';
    card_header_icon_light.setAttribute('src', base_url + 'assets/images/deal_icon_light_24.png');

    var card_header_title = document.createElement('span');
    card_header_title.className = 'kg-lead_card__title';
    card_header_title.innerHTML = data.company_name;

    var card_header_direction = document.createElement('i');
    card_header_direction.className = 'kg-lead_card__title_direction_icon';
    var card_header_direction_icon = document.createElement('i');
    card_header_direction_icon.className = 'fa fa-arrow-circle-right';
    
    var card_header_close_btn = document.createElement('span');
    card_header_close_btn.className = 'kg_lead_card__close_btn pull-right clickable close-icon text-white close_lead';
    card_header_close_btn.setAttribute('data-effect', 'fadeOut');
    card_header_close_btn.setAttribute('data-id',data.uuid);
    card_header_close_btn.innerHTML = '<i class="fa fa-times"></i>';
    
    card_header.appendChild(card_header_icon_dark);
    card_header.appendChild(card_header_icon_light);
    card_header.appendChild(card_header_title);
    card_header.appendChild(card_header_close_btn);
    //card_header_direction.appendChild(card_header_direction_icon);
    //card_header.appendChild(card_header_direction);

    var card_title_border = document.createElement('div');
    card_title_border.className = 'kg-lead_card__title--border';

    var card_body = document.createElement('div');
    card_body.className = 'kg-lead_card__body';

    var card_body_deal_stage = document.createElement('div');
    card_body_deal_stage.innerHTML = '<strong>Stage:</strong> ' + data.deal_stage_name;
    
    var card_body_deal_price = document.createElement('div');
    //card_body_deal_price.className = 'text-muted';
    card_body_deal_price.innerHTML = '<strong>LED:</strong> $' + data.total_led + '<strong class="ml-4">Solar:</strong> $' + data.total_solar;

    var card_body_deal_user_and_site = document.createElement('div');
    //card_body_deal_user_and_site.className = 'text-muted';

    var card_body_deal_user = document.createElement('span');
    card_body_deal_user.innerHTML = '<i class="fa fa-calendar"></i>  ' + data.activity_last_at;

    var card_body_deal_site = document.createElement('span');
    card_body_deal_site.innerHTML = '<span class="ml-3"> <i class="fa fa-user"></i> ' + data.activity_user + '</span>';


    var card_deal_action = document.createElement('div');
    card_deal_action.className = 'kg-lead_card__body_actions';
    card_deal_action.setAttribute('style', 'display:none;');

    var car_deal_address = document.createElement('div');
    car_deal_address.className = 'kg-lead_card__body_address';
    var address = data.customer_address;
    if (address != '' && address != null) {
        address = address.replace(', Australia', '')
    } else {
        address = 'Unknown';
    }
    car_deal_address.innerHTML = '<span style="margin-left: 20px;"> <i class="fa fa-map-marker"></i> ' + address;

    card_deal_action.appendChild(car_deal_address);
    card_deal_action.innerHTML += '<a href="tel:' + data.customer_contact_no + '"><span class="kg-lead_card__body_actions_item"> <i class="material-icons">phone</i> <br/><span class="kg-lead_card__body_actions_item_text">Call</span></span></a>';
    card_deal_action.innerHTML += '<span data-id="' + data.uuid + '" class="kg-lead_card__body_actions_item add_activity"> <i class="material-icons">message</i> <span class="kg-lead_card__body_actions_item_text">Message</span></span>';
    card_deal_action.innerHTML += '<span data-id="' + data.uuid + '" class="kg-lead_card__body_actions_item get_direction"> <i class="material-icons">directions</i> <span class="kg-lead_card__body_actions_item_text">Directions</span></span>';
    /**if (self.is_sales_rep && (lead_stage == 1 || lead_stage == 2 || lead_stage == 10)) {
     card_deal_action.innerHTML += '<a href="' + base_url + 'admin/lead/add?deal_ref=' + data.uuid + '"><span class="kg-lead_card__body_actions_item"> <i class="material-icons">details</i> <span class="kg-lead_card__body_actions_item_text">Detail</span></span>';
     } else if (!self.is_sales_rep) {
     card_deal_action.innerHTML += '<a href="' + base_url + 'admin/lead/add?deal_ref=' + data.uuid + '"><span class="kg-lead_card__body_actions_item"> <i class="material-icons">details</i> <span class="kg-lead_card__body_actions_item_text">Detail</span></span>';
     }*/
    card_deal_action.innerHTML += '<a href="' + base_url + 'admin/lead/add?deal_ref=' + data.uuid + '"><span class="kg-lead_card__body_actions_item"> <i class="material-icons">details</i> <span class="kg-lead_card__body_actions_item_text">Detail</span></span>';
    card_body_deal_user_and_site.appendChild(card_body_deal_user);
    card_body_deal_user_and_site.appendChild(card_body_deal_site);
    card_body.appendChild(card_body_deal_stage);
    card_body.appendChild(card_body_deal_price);
    card_body.appendChild(card_body_deal_user_and_site);


    card.appendChild(card_header);
    card.appendChild(card_title_border);
    card.appendChild(card_body);
    card.appendChild(card_deal_action);

    return card;
};

lead_map_manager.prototype.get_direction_to_destination = function (origin, destination) {
    var self = this;
    var request = {
        origin: origin,
        destination: destination,
        travelMode: 'DRIVING'
    };
    console.log(self.directionsService);
    self.directionsService.route(request, function (result, status) {
        if (status == 'OK') {
            self.directionsDisplay.setDirections(result);
            for (var i = 0; i < self.markers.length; i++) {
                var key = self.markers[i].uuid;
                self.markers[i].setAnimation(null);
                self.markers[i].setIcon(base_url + "assets/images/mk-" + self.handle_color(parseInt(self.lead_list[key].lead_stage)) + ".png?v=0.1");
                $('.kg-lead_card').removeClass('kg-lead_card--pink');
                $('#card_' + key).find('.card_header_icon_light').addClass('hidden');
                $('#card_' + key).find('.card_header_icon_dark').removeClass('hidden');
                if ($('.kg-lead_card').hasClass('opened')) {
                    $('.kg-lead_card').removeClass('opened');
                    $('.kg-lead_card').find('.kg-lead_card__body_actions').slideUp("slow", function () {
                        $('.kg-lead_card').css('height', '110px');
                    });
                }
            }
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
};
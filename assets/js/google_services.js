var google_window;
var google_service_manager = function (options) {
    var self = this;
    this.gcal = options.is_google_account_connected;
    this.service_id = '';
    if (self.gcal == 'true') {
        $('.disconnect_google_services').removeClass('hidden');
        $('.connect_google_services').addClass('hidden');
        $('.connect_container').html('Google Account is connected.');
    }
    $('.connect_google_services').click(function () {
        $this = $(this);
        var service_id = $this.attr('data-id');
        var service_name = $this.attr('data-name');
        self.service_id = service_id;
        self.service_name = service_name;
        self.connect_google_account();
    });

    $('.disconnect_google_services').click(function () {
        $this = $(this);
        var service_id = $this.attr('data-id');
        var service_name = $this.attr('data-name');
        self.service_id = service_id;
        self.service_name = service_name;
        self.disconnect_google_account();
    });
};


google_service_manager.prototype.connect_google_account = function () {
    var self = this;
    $.ajax({
        url: base_url + '/google_oauth_callback',
        type: 'post',
        data: {service_id: self.service_id},
        dataType: 'json',
        beforeSend: function () {
            $('.connect_google_services').addClass('hidden');
            $('.connect_container').html(createLoader('Connecting to google services. Please do check if the popup is allowed in the browser.'));
            var y = window.top.outerHeight / 2 + window.top.screenY - (500 / 2);
            var x = window.top.outerWidth / 2 + window.top.screenX - (400 / 2);
            google_window = window.open('', 'Google Calendar Connect', 'width=400px, height=500px, top=' + y + ', left=' + x);
        },
        success: function (response) {
            var url = response.auth_url;
            google_window.location.href = url;
        }
    });
};

google_service_manager.prototype.disconnect_google_account = function () {
    var self = this;
    $.ajax({
        url: base_url + '/disconnect_google_service_account',
        type: 'post',
        data: {service_id: self.service_id},
        dataType: 'json',
        beforeSend: function () {
            $('.disconnect_google_services').addClass('hidden');
            $('.connect_container').html(createLoader('Disconnecting to google services. Please wait....'));

        },
        success: function (response) {
            if (response.success == true) {
                window.location.reload();
                toastr["success"](response.status);
            } else {
                 toastr["error"](response.status);
            }
        }
    });
};

function google_account_connect_success(connected) {
    window.close();
    google_window.close();
    $('.connect_container').html('');
    if (connected == true) {
        toastr["success"]('Account Connected Successfully');
        setTimeout(function () {
            window.location.href = base_url + 'admin/dashboard';
        }, 2000);
    } else {
        $('.connect_google_services').removeClass('hidden');
        toastr["error"]('Looks like some issue in connecting to google service, please try again later.');
    }

}


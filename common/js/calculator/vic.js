var calculator_manager = function(options) {
    var self = this;
    this.job_uuid = (options.job_uuid && options.job_uuid != '') ? options.job_uuid: '';
    this.logged_in_user_id = (options.logged_in_user_id && options.logged_in_user_id != '') ? options.logged_in_user_id: '';
    this.group_id = (options.group_id && options.group_id != '') ? options.group_id: '';
    this.calculator_items = [];
    
    self.fetch_calculator_items();
    
    $(document).on('click','#add_calculator_row',function(){
       self.create_calculator_row();
    });
};

calculator_manager.prototype.fetch_calculator_items = function() {
    var self = this;
    $.ajax({
        url: base_url + 'assets/calculator/vic.json?v='+Math.random(),
        type: 'get',
        crossDomain: true,
        dataType: "json",
        cahce:false,
        beforeSend:function(){
            document.getElementById('calculator_item_body').innerHTML = '';
        },
        success: function(stat) {
            self.calculator_items = [];
            self.calculator_items = stat;
        },
        error: function(xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


calculator_manager.prototype.create_calculator_row = function() {
   var tr = document.createElement('tr');
   
   var td_room_area = document.createElement('td');
   var room_area_input = document.createElement('input');
   room_area_input.setAttribute('type','text');
   room_area_input.className = 'form-control';
   td_room_area.appendChild(room_area_input);
   
   tr.appendChild(td_room_area);
   
   document.getElementById('calculator_item_body').appendChild(tr);
};
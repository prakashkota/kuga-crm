<div class="modal fade" id="add_customer_location_modal" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_customer_location_modal_title">Add New Customer Location</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                            <form role="form"  action="#" method="post" name="locationAdd" id="locationAdd">
                                <input type="hidden" name="site_id" id="site_id">
                                <input type="hidden" name="site[latitude]" id="locationLatitude" />
                                <input type="hidden" name="site[longitude]" id="locationLongitude" />
                                <div class="row hidden">
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <a id="focuesSite"></a>
                                            <label>Site Name</label>
                                            <input class="form-control" placeholder="Please Enter Location Title" type="text" name="site[site_name]" id="locationTitle" value=""  required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <label>Contact Name</label>
                                            <select class="form-control" name="site[cust_contact_id]" id="site_cust_contact_id">
                                                <option>Please Select Contact Name</option>
                                            </select>
                                            <input class="form-control" type="hidden" name="site[site_contact_name]" id="locationContactPerson"  required />
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <label>Contact Email</label>
                                            <input class="form-control" placeholder="Please Enter Contact Email" type="email" name="site[site_contact_email]" id="locationContactEmail"  required />
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group control-group">
                                            <label>Contact Phone</label>
                                            <input class="form-control" placeholder="Please Enter Contact Phone" type="text"  name="site[site_contact_no]" id="locationContactPhone"  pattern="[0-9]{6,}" title="Phone number should be number"  required /> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12" >
                                    <div class="form-group control-group">
                                        <label>Customer Address</label>
                                        <a href="javscript:void(0);" id="manual_address_btn" style="font-size:12px; float:right;">Edit Manually</a>
                                        <a href="javscript:void(0);" id="manual_address_hide_btn" style="font-size:12px; float:right;" class="hidden">Back to Autocomplete</a>
                                        <select class="select2 placecomplete form-control locationstreetAddress" id="locationstreetAddress"  style="height:48px !important;"></select>
                                        <input class="locationstreetAddressVal form-control" type="hidden" id="locationstreetAddressVal" name="site[address]" />
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group control-group">
                                        <label>State</label>
                                        <select name="site[state_id]" id="locationState" class="form-control locationState" required>
                                            <option value="">Select State</option>
                                            <?php for ($j = 0; $j < count($states); $j++) { ?>
                                                <option value="<?php echo $states[$j]['state_id']; ?>"><?php echo $states[$j]['state_name']; ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group control-group">
                                        <label>Post Code</label>
                                        <input class="form-control locationPostCode" placeholder="Please Enter Post Code" type="text" name="site[postcode]" id="locationPostCode" value=""  required />

                                    </div>
                                </div>
                            </div>
                                <div class="buttwrap" style="display:inline-flex;">
                                    <input type="button" id="save_customer_location" name="submit" data-loading-text="Saving..." class="btn" value="SAVE" >
                                    <div id="customer_site_loader"></div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
 $('#manual_address_btn').click(function(){
     $(this).addClass('hidden');
     $('#manual_address_hide_btn').removeClass('hidden');
     $('#locationstreetAddress').next().css('display','none');
     $('#locationstreetAddressVal').attr('type','text');
 }); 
 
 $('#manual_address_hide_btn').click(function(){
     $(this).addClass('hidden');
     $('#manual_address_btn').removeClass('hidden');
     $('#locationstreetAddress').next().css('display','block')
     $('#locationstreetAddressVal').attr('type','hidden');
 });
 
 $('#locationstreetAddressVal').keyup(function(){
     $('#select2-locationstreetAddress-container').html($(this).val());
 });

</script>

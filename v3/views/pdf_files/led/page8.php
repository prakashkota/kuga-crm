<?php  $html = "<tr>
    <td  style=\" background-color:#cc242e;  width:100%;   font-size: 36px; color: #fff; font-weight: bold;  padding: 50px 0px 50px 35px; font-family: 'Montserrat', sans-serif;\">Proposal Acceptance</td>
  </tr>
  <tr>
	<td height=\"10px\">&nbsp;</td>
	</tr>
	<tr>
		<td align=\"left\" valign=\"top\">
	<table width=\"90%\" align=\"center\" style=\" margin-bottom: 50px;\">
	
	<tr>
	<td align=\"left\" valign=\"top\" ><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tbody>
	<tr>
	<td align=\"left\" colspan=\"2\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#000;\">
	<tbody><tr>
	<td width=\"50%\" height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif;color:#fff; font-size:14px; padding-left:20px; \">CLIENT DETAILS</td>
	</tr>
	</tbody></table></td>
	</tr>
	<tr>
	<td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; padding-left:20px;\">Company name</td>
	<td align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:10px; padding-left:12px; font-weight: bold;\">".$led_proposal_data['customer_company_name']."</td>
	</tr>
	<tr>
	<td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; padding-left:20px;\">Authorised Person</td>
	<td align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:10px; padding-left:12px; font-weight: bold;\">".$led_proposal_data['first_name'] . " " .$led_proposal_data['last_name']."</td>
	</tr>
	<tr>
	<td height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; padding-left:20px;\">Address</td>
	<td align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:10px; padding-left:12px; font-weight: bold;	\">".$led_proposal_data['customer_address']."</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	 <tr>
	<td >&nbsp;</td>
	</tr>
	<tr>
	<td align=\"left\" valign=\"top\">&nbsp;</td>
	</tr>

	<tr>
	<td align=\"left\" valign=\"top\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tbody>
	<tr>
	<td align=\"left\" colspan=\"5\" >
	<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#cc242e;\">
	<tbody><tr>
	<td  height=\"30\" align=\"left\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif;color:#fff; font-size:14px; padding-left:20px;\">PROJECT COST</td>
	</tr>
	</tbody></table>
	</td>

	</tr>
	<tr>
	 <td width=\"40%\" style=\"color: #000; font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px; padding-left:20px;\"  align=\"left\" >Project Cost</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">exc. GST</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">$".$calculated_data['total_project_cost_exGST']."</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">inc. GST</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">$".$calculated_data['total_project_cost_inGST']."</td>
      </tr>
	   <tr>
        <td width=\"40%\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px; padding-left:20px;\"  align=\"left\" >Total Rebate</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">exc. GST</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">$".$calculated_data['total_discount_veec_exGST']."</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">inc. GST</td>
        <td width=\"15%\" align=\"center\" style=\" color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">$".$calculated_data['total_discount_veec_inGST']."</td>
      </tr>
	  <tr>
        <td width=\"40%\"  style=\"color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px; padding-left:20px;\"  align=\"left\" >Total Payable</td>
        <td width=\"15%\" align=\"center\"  style=\"color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">exc. GST</td>
        <td width=\"15%\" align=\"center\"  style=\"color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">$".$calculated_data['total_investment_exGST']."</td>
        <td width=\"15%\" align=\"center\"  style=\"color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px;\">inc. GST</td>
        <td width=\"15%\" align=\"center\"  style=\"color: #000;  font-family: 'Open Sans', sans-serif;font-size: 12px;line-height: 24px; \">$".$calculated_data['total_investment_inGST']."</td>
      </tr>
	  
	
	</tbody>
	</table>
	</td>
	</tr>
	<tr style=\"height:0px\">
    <td>&nbsp;</td>
  </tr>
	<tr>
	<td align=\"left\" valign=\"top\">&nbsp;</td>
	</tr>";

	$html .="<tr>
	<td align=\"left\" valign=\"top\">
	
	<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"0\">
	<tbody>
	<tr>
	
	<td align=\"left\" valign=\"top\" bgcolor=\"#cc242e\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; padding-left: 20px; line-height:24px; color:#fff;\"> UPFRONT PAYMENT</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	<tr>
	<td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"0\">
	<tbody>
	<tr>
	<td width=\"60%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; padding-left:20px; \">Deposit payment (20%)</td>
	<td width=\"20%\" align=\"right\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; \">exc. GST</td>
	<td width=\"20%\" align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; \">$".abs(($calculated_data['total_investment_exGST']/100)*20)."</td>
	</tr>
	<tr>
	<td width=\"60%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; padding-left:20px; \">Payment on installation commencement (50%)</td>
	<td width=\"20%\" align=\"right\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; \">exc. GST</td>
	<td width=\"20%\" align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; \">$".abs(($calculated_data['total_investment_exGST']/100)*50)."</td>
	</tr>
	<tr>
	<td width=\"60%\" align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; padding-left:20px; border-bottom:1px solid #ccc;\">On job completion (30%)</td>
	<td width=\"20%\" align=\"right\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; border-bottom:1px solid #ccc;\">exc. GST</td>
	<td width=\"20%\" align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px; border-bottom:1px solid #ccc;\">$".abs(($calculated_data['total_investment_exGST']/100)*30)."</td>
	</tr>
	<tr>
	<td align=\"left\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px;font-weight: bold; padding-left:20px;\">Total Payable</td>
	<td align=\"right\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px;font-weight: bold;\">exc. GST</td>
	<td align=\"center\" valign=\"top\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; color:#000; line-height:24px;font-weight: bold;\">$".$calculated_data['total_investment_exGST']."</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	<tr>
    <td>&nbsp;</td>
	</tr>
	<tr>
								<td>
                                 <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" ><tbody>
											<tr>
                                            <td bgcolor=\"#404041\" width=\"70%\" style=\"width:70%;font-family: 'Open Sans', sans-serif; font-size:20px; text-align:center; color:#ffffff; text-transform:uppercase; background-color:#404041;\" >Return On Investment</td>
                                            <td bgcolor=\"#d2232b\" width=\"30%\" style=\"width:30%;font-family: 'Open Sans', sans-serif; font-size:20px; text-align:center; color:#ffffff; text-transform:uppercase; background-color:#d2232b;\" ><strong>".number_format($calculated_data['roi'], 2, '.', '')."</strong> Months</td>
                                            </tr>
                                            </tbody>
                                 </table>
                                </td>
							</tr>
	<tr>
	<td align=\"center\" valign=\"top\">
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:2px solid #d2232b; \">
	<tr>
	<td width=\"23%\" align=\"center\" valign=\"top\">
	<table width=\"60%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; \">Total annual saving</td>
	</tr>
	<tr>
	
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:2px solid #d2232b; font-size: 26px; color: #d2232b;\">$".number_format($TotalSavings_YR1)."</td>
	</tr>
	</table>
	</td>
	<td width=\"5%\" >&nbsp;</td>
	<td width=\"23%\"   align=\"center\" >
	<table width=\"60%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tbody>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; \">10 Years Benefit</td>
	</tr>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:2px solid #d2232b; font-size: 26px; color: #d2232b;\">$".number_format($calculated_data['10_year_savings'])."</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	
	<tr>
	<td width=\"23%\" align=\"center\" valign=\"top\">
	<table width=\"60%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tbody>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; \">Year 1 return on investment</td>
	</tr>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:2px solid #d2232b; font-size: 26px; color: #d2232b;\">".floor($calculated_data['roi'] * 100)."%</td>
	</tr>
	<tr><td> &nbsp; </td>
</tr>
	</tbody>
	</table>
	</td>
	<td width=\"5%\">&nbsp;</td>
	
	<td width=\"23%\"  align=\"center\" >
	<table width=\"60%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tbody>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:12px; \">Year 1 cashflow of repayment</td>
	</tr>
	<tr>
	<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:2px solid #d2232b; font-size: 26px; color: #d2232b; \">$".number_format($year_1_cashflow_on_repayment)."</td>
	</tr>
	<tr><td> &nbsp; </td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>";

	
	$html .= "
	 <tr>
	<td align=\"left\" valign=\"top\" height=\"42px\">&nbsp;</td>
	</tr>
	

	</table>
	</td>
	</tr>
	<tr>
		<td align=\"left\" valign=\"top\"><img src=\"http://kugaroi.com.au/dev/admin/imagespdf2/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\" /></td>
	</tr>";
	echo $html;
	?>
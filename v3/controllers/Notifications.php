<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Notifications Controller
 * @version 1.0
 */
class Notifications extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Notification_Model", "notification");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
    }

    public function fetch_notifications() {
        $userid = $this->aauth->get_user()->id;
        $data['notifications'] = array();

        if (!empty($this->input->get('type'))) {
            $type = $this->input->get('type');
            $cond = array('receiver_user_id' => $userid, 'notification_type' => $type, 'read_at' => NULL);
            $notifications = $this->common->fetch_where('tbl_notifications', 'id', $cond);
            $data['notifications'] = $notifications;
            $data['success'] = TRUE;
        } else {
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function change_notification_status() {
        $userid = $this->aauth->get_user()->id;
        $data['notifications'] = array();
        if (!empty($this->input->get('type'))) {
            $type = $this->input->get('type');
            $cond = array('receiver_user_id' => $userid, 'notification_type' => $type, 'read_at' => NULL);
            $update_data = array('read_at' => date('Y-m-d H:i:s'));
            $stat = $this->common->update_data('tbl_notifications', $cond, $update_data);
            if($stat){
                $data['success'] = TRUE;
            }else{
                $data['success'] = FALSE;
            }
        } else {
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}

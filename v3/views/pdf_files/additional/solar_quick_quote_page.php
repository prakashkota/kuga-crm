<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
<style>
    .font-family{font-family: 'Open Sans', sans-serif;}
    .table-border{border:solid 1px #ccc;}
    .w910{width:910px; }
    body{margin:0px !important;}
    .center-image{}
    .w800{width:800px !important; }
</style>
</head>
<?php 
    //$c = substr_count($quote_data['description'],'<p>'); 
    $desc = $quote_data['description'];
    $desc = str_replace('</p>', '', $desc);
    $desc_array = explode('<p>', $desc);
?>
<body style="margin:0px;padding:0px;">
<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #ffffff">
      <tr>
        <td><img src="<?php echo $this->config->item('live_url').'assets/images/solar_quote_form_header.jpg'; ?>" alt="" width="910" height="126" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="770" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
          <tbody>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td valign="top"><table width="770" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                  <td width="385" valign="top" style="padding-right:10px"><table width="385" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Quote #</td>
                      <td width="283" style="padding-left:5px;  border:solid 1px #000; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><?php echo (isset($quote_data)) ? $quote_data['quote_no'] : ''; ?></td>
                    </tr>
                  </tbody></table></td>
                  <td width="385" valign="top" style="padding-left:10px"><table width="385" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Sales Person:</td>
                      <td width="275" style="padding-left:5px;  border:solid 1px #000; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><?php echo (isset($quote_data)) ? $quote_data['rep_name'] : ''; ?></td>
                    </tr>
                  </tbody></table></td>
                </tr>
              </tbody></table></td>
            </tr>
            <tr>
              <td height="10"></td>
            </tr>
            <tr>
              <td valign="top"><table width="770" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                  <td width="385" valign="top" style="padding-right:10px"><table width="385" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Site Contact:</td>
                      <td width="283"  style="padding-left:5px;  border:solid 1px #000; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><?php echo (isset($quote_data)) ? $quote_data['customer_name'] : ''; ?></td>
                    </tr>
                  </tbody></table></td>
                  <td width="385" valign="top" style="padding-left:10px"><table width="385" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td width="130" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#262626">Valid for:</td>
                      <td width="275" style="padding-left:5px;  border:solid 1px #000; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><?php echo (isset($quote_data)) ? $quote_data['valid_for'] : ''; ?></td>
                    </tr>
                  </tbody></table></td>
                </tr>
              </tbody></table></td>
            </tr>
            <tr>
              <td height="13"></td>
            </tr>
            <tr>
              <td valign="top"><table width="790" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                  <td width="120" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">Site Address:</td>
                  <td width="708" style="padding-left:5px; border:solid 1px #000; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333"><?php echo (isset($quote_data)) ? $quote_data['customer_address'] : ''; ?></td>
                </tr>
              </tbody></table></td>
            </tr>
            <tr>
              <td height="30"></td>
            </tr>
            <tr>
              <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;  padding-bottom:5px; ">
                <strong>Description</strong>
              </td>
            </tr>
            <tr valign="top" id="descrip">
              <td style="border:solid 1px #000; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333333;padding:5px;">
                <?php for($i = 0; $i < 20; $i++){ ?>
                    <?php echo isset($desc_array[$i]) ? '<p>' . $desc_array[$i] . '</p>' : ''; ?>
                <?php } ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
      <td valign="top"><table width="790" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="470" height="30" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">&nbsp;</td>
          <td width="150" height="30" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Sub Total exc GST:</td>
          <td width="153" height="30" style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
            <strong style="padding-left:5px;" ><?php echo $quote_data['total_excGST']; ?></strong>
          </td>
        </tr>
        <tr>
         <td align="left" height="30"  valign="top">&nbsp;</td>
         <td width="150"  height="30"  align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">GST:</td>
         <td width="153" height="30"  style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
          <strong style="padding-left:5px;" ><?php echo $quote_data['total_gst']; ?></strong>
        </td>
      </tr>
      <tr>
        <td align="left" height="30"  valign="top">&nbsp;</td>
        <td align="right" height="30"  style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#333333">Total inc GST:</td>
        <td height="30"  style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
          <strong style="padding-left:5px;" ><?php echo $quote_data['total_incGST']; ?></strong> 
        </td>
      </tr>
    </table></td>
  </tr>
<tr>
      <td height="25">&nbsp;</td>
    </tr>
  <tr>
      <td valign="top">
          <table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
              <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/images/solar_quote_form_pay.jpg'; ?>" style="width:190px;"  /></td>
    </tr>
    </tbody>
    </table>
    </td>
  </tr>
 <tr>
      <td>&nbsp;</td>
    </tr>
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/images/solar_quote_form_footer.jpg'; ?>"  /></td>
  </tr>
  <tr>
      <td height="15">&nbsp;</td>
    </tr>
</table></td>
</tr>
</table>
</body>
</html>

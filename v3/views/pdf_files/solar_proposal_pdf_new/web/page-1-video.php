<html>

<head>
    <script>
        var base_url = '<?php echo site_url(); ?>';
    </script>
    <title>Solar Proposal</title>
    <style>
        body {
            font-family: arial;
        }

        .page1 #myVideo {
            right: 0;
            bottom: 0;
            max-width: 910px;
            height: 391px;
        }

        .back-bg {
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
            box-shadow: 1px 1px 3px 1px #333;
            border-collapse: separate;
            margin-bottom: 20px;
        }

        .page1 .title h1 {
            font-size: 70px;
            color: #fff;
            padding-top: 500px;
            padding-left: 20px;
        }

        .page1 hr {
            width: 170px;
            height: 7px;
            background-color: #d8d4d2;
            margin-left: 20px;
            margin-top: -17px;
        }

        .page1 .title h3 {
            font-size: 17px;
            color: #fff;
            margin-left: 17px;
            margin-top: 55px;
        }

        .page1 .title h6 {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 19px;
            margin-top: 27px;
            font-weight: 400;
        }

        .page1 .title p {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 19px;
            margin-top: -29px;
        }

        .page1 .pretitlee h3 {
            font-size: 17px;
            color: #fff;
            margin-left: 358px;
            margin-top: -117px;
        }

        .page1 .pretitlee h6 {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 355px;
            margin-top: 27px;
            font-weight: 400;
        }

        .page1 .pretitlee p {
            font-size: 14px;
            color: #fff8f9;
            margin-left: 356px;
            margin-top: -29px;
        }

        .page1 .zdate h5 {
            font-size: 17px;
            color: #fff;
            margin-left: 18px;
        }

        .page1 .zdate h5 span {
            font-size: 14px;
            color: #f4f9f6;
            margin-left: 18px;
            font-weight: 400;
        }

        .page1 .Ruga img {
            width: 400px;
            margin-left: 450px;
            padding-bottom: 10px;
        }

        .page1 .imghome {
            max-width: 910px;
        }

        .page1 .contentBox {
            position: absolute;
            top: 120px;
        }

        .page1 #myVideo {
            right: 0;
            bottom: 0;
            width: 909px;
            object-fit: cover;
            height: 900px;
        }

        .page1 .zavvideo img {
            position: relative;
            top: -610px;
        }

        .page1 .zavvideo {
            height: 1287px;
            background: #df1824;
        }
    </style>
</head>

<body>
    <?php
        // echo print_r($lead_data);
        // die;
    ?>
    <div class="back-bg page1">
        <div class="zavvideo">
            <video autoplay muted loop id="myVideo">
                <source src="https://kugacrm.com.au/assets/solar_proposal_pdf_new/banner-video-new.mp4" type="video/mp4">
            </video>
            <img src="<?php echo site_url('assets/solar_proposal_pdf_new/page1.png'); ?>" class="imghome">
        </div>

        <div class="contentBox">
            <div class="title">
                <h1><?php echo ($proposal_data['total_system_size']); ?> KW <br>SOLAR <br>PROPOSAL</h1>
                <hr>
            </div>
            <div class="">
                <div class="title">
                    <h3>CUSTOMISED FOR</h3>
                    <h6><?php echo $lead_data['customer_company_name']; ?></h6>
                    <p><?php echo $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?><br/>
                    <?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></p>
                </div>
                <div class="pretitlee">
                    <h3>PREPARED BY</h3>
                    <h6><?php echo $lead_data['full_name']; ?></h6>
                    <p><?php echo $lead_data['company_contact_no']; ?>
                        <br> <?php echo $lead_data['user_email']; ?>
                    </p>
                </div>
            </div>
            <div class="zdate">
                <h5>DATE PREPARED <span><br /><?php echo date_format(date_create($proposal_data['created_at']), "l, jS F Y"); ?></span></h5>
            </div>
            <div class="ruga"> <img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>"> </div>
        </div>
    </div>
</body>

</html>
<?php

class Cron_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->kuga_web_db = $this->load->database('kuga_web_db', TRUE);
        date_default_timezone_set('Australia/Sydney');
    }
    
    /**
     * Fetch data
     * @param type $table
     * @param type $select
     * @param type $cond
     * @return type
     */
    public function fetch_kuga_web_where($table, $select = '*', $cond = NULL) {
        $this->kuga_web_db->select($select);
        $this->kuga_web_db->from($table);
        if ($cond !== NULL) {
            $this->kuga_web_db->where($cond);
        }
        return $this->kuga_web_db->get()->result_array();
    }
    
    
    
    /**
     * Insert data
     * @param type <string> $table
     * @param type <array> $data
     * @return boolean
     */
    function insert_kuga_web_data($table, $data) {
        $this->kuga_web_db->insert($table, $data);
        return $this->kuga_web_db->affected_rows();
    }
    
    
    /**
     * update data
     * @param type $table
     * @param type $value
     * @return boolean
     */
    function update_kuga_web_data($table, $cond, $data) {
        $this->kuga_web_db->where($cond);
        $this->kuga_web_db->update($table, $data);
        return $this->kuga_web_db->affected_rows();
    }
    

    /**
     * Fetch Kuga WEbsite Booking Enquires
     * @param type $id
     * @return array
     */

    public function fetch_kuga_web_enquiry($id = FALSE) {
        $cond = '';
        if($id){
            $cond = 'AND id='.$id;
        }
        $sql = "SELECT * FROM wp_booknow WHERE lead_allocation_status=0 $cond ORDER BY id DESC";
        $query = $this->kuga_web_db->query($sql);
        return $query->row_array();
    }
    
    /**
     * Fetch Dataforce Jobs
     * @param type $id
     * @param type $company_name
     * @return array
     */

    public function fetch_dataforce_jobs($id = FALSE,$company_name  = FALSE,$address = FALSE) {
        $cond = '';
        if($id){
            $cond .= ' AND job_id='.$id;
        }
        if($company_name && $company_name != ''){
            $cond .= " OR company_name like '%".$company_name ."%'";
        }
        if($address && $address != ''){
            $cond .= " OR address = '".$address ."'";
        }
        $sql = "SELECT * FROM tbl_dataforce_jobs WHERE 1=1 $cond";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * Fetch Simpro Jobs
     * @param type $id
     * @param type $company_name
     * @return array
     */

    public function fetch_simpro_jobs($dataforce_job_id = FALSE,$company_name  = FALSE,$address = FALSE) {
        $cond = '';
        if($dataforce_job_id){
            $cond .= ' AND dataforce_job_id='.$dataforce_job_id;
        }
        if($company_name && $company_name != ''){
            $cond .= " OR company_name like '%".$company_name ."%'";
        }
        if($address && $address != ''){
            $cond .= " OR address = '".$address ."'";
        }
        $sql = "SELECT *,job_id as simpro_job_id,dataforce_job_id as job_id,NULL as status,NULL as sub_status FROM tbl_simpro_jobs WHERE 1=1 $cond ORDER BY FIELD(company_name,$company_name)  DESC,FIELD(status_name, 'Stage 8 - Complete') DESC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

}

<?php $html = "<tr>
    <td  style=\" background-color:#cc242e;  width:100%;font-family: 'Montserrat', sans-serif;    font-size: 36px; color: #fff; padding: 50px 0px 50px 35px;  \">Project Cashflow Financed</td>
  </tr>


<tr>
<td align=\"left\" valign=\"top\">&nbsp;</td>
</tr>

<tr>
<td  height=\"30px\">&nbsp;</td>
</tr>

<tr>
<td>
<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" >
<tbody>
<tr>
<td width=\"27%\" align=\"left\" valign=\"top\" >
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#000000;\">
<tbody>
<tr>
<td  height=\"30\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; color:#fff; font-size:14px; \">NET CASHFLOW</td>
</tr>
</tbody>
</table>
</td>
<td width=\"20%\" align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:12px;\">Annual Revenue</td>
<td width=\"5%\" align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:12px;\">&nbsp;</td>
<td width=\"15%\" align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:12px;\">Investment</td>
<td width=\"5%\" align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;\">&nbsp;</td>
<td width=\"15%\" align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:12px;\">Cashflow</td>
</tr>";
    $monthlyPaymentPlanSolar = 0;
    if(isset($proposal_finance_data['monthly_payment_plan'])){ 
     $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'] * 12;
    }else{
        $proposal_finance_data['monthly_payment_plan'] = 0;
        $proposal_finance_data['term'] = 0;
    }
    $Cashflow = $monthlyPaymentPlanSolar;
    $fin = 0;
    $TotalInvestment = 0;
    for ($t = 0; $t < 10; $t++) {
        $TotalSavingsArr[$t] = $TotalSavingsArr[$t] / 1.1;
        if ($t < $year) {
            $fin = $monthlyPaymentPlanSolar;
            $Cashflow = $TotalSavingsArr[$t] - $fin;
        } else {
            $fin = 0;
            $Cashflow = $TotalSavingsArr[$t] - $fin;
        }
        
        $TotalInvestment = $TotalInvestment + $fin;
        $yr = $t + 1;
        //$Cashflow = $TotalSavingsArr[$t] - $Cashflow;

        $html .="
<tr>
<td height=\"35\" align=\"center\" valign=\"middle\" style=\"border-left:1px solid #cccccc; border-bottom:1px solid #cccccc;  padding-left:20px;font-size:14px;\">YR" . $yr . "</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:14px;\">$" . number_format((float) ($TotalSavingsArr[$t]), 2, '.', '') . "</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;\">-</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;font-size:14px;\">$" . number_format((float) ($fin), 2, '.', '') . "</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:1px solid #cccccc;\">=</td>
<td align=\"center\" valign=\"middle\" style=\"border-right:1px solid #cccccc;  border-bottom:1px solid #cccccc;font-size:14px;\"><strong>$" . number_format((float) ($Cashflow), 2, '.', '') . "</strong></td>
</tr>
<tr>";
    }
    if ($proposal_data['rate'] == 1) {
        $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 2, '.', '') . ", Distributor Charge: " . number_format((float) ($proposal_data['distributor_charge']), 2, '.', '');
    }
    if ($proposal_data['rate'] == 2) {
        $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 2, '.', '') . ", Off-Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_off_peak']), 2, '.', '') . ", Distributor Charge: " . number_format((float) ($proposal_data['distributor_charge']), 2, '.', '');
    }
    if ($proposal_data['rate'] == 3) {
        $costRate = "* Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_peak']), 2, '.', '') . ", Off-Peak Rate: " . number_format((float) ($proposal_data['solar_cost_per_kwh_off_peak']), 2, '.', '') . ", Shoulder Rate: " . ($proposal_data['solar_cost_per_kwh_shoulder']) . ", Distributor Charge: " . ($proposal_data['distributor_charge']);
    }
    $total_payable_exGST = $proposal_data['total_payable_exGST'];
    $RateOfReturnFinanced = $TotalSavingsArr[0] / $total_payable_exGST;
    $RateOfReturnFinanced = $RateOfReturnFinanced * 100;
    $html .="
     <tr>
        <td width=\"20%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a; border-left:3px solid #d2232a;font-size:10px;\"></td>
            <td width=\"20%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a; font-size:12px;\">Total Revenue</td>
            <td width=\"5%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a;\">&nbsp;</td>
<td width=\"15%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a; font-size:12px;\">Total Investment</td>
<td width=\"5%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a;\">&nbsp;</td>
<td width=\"25%\" align=\"center\" valign=\"middle\" style=\"border-top:3px solid #d2232a; border-right:3px solid #d2232a; font-size:12px;\">Total Cashflow</td>
</tr>   
<tr>
<td height=\"35\" align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a;  border-left:3px solid #d2232a; padding-left:20px;font-size:14px;\">Total YR10</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a; font-size:14px;\">$" . number_format((float) ($TotalRevenue / 1.1), 2, '.', '') . "</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a; font-size:14px;\">-</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a; font-size:14px;\">$" . number_format((float) ($TotalInvestment), 2, '.', '') . "</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a; font-size:14px;\">=</td>
<td align=\"center\" valign=\"middle\" style=\"border-bottom:3px solid #d2232a; border-right:3px solid #d2232a;font-size:14px;\"><strong>$" . number_format((float) (($TotalRevenue / 1.1) - $TotalInvestment), 2, '.', '') . "</strong></td>
</tr>

</tbody>
</table>
</td>
</tr>
<tr>
<td  height=\"25px\">&nbsp;</td>
</tr>

<tr>
<td align=\"left\" valign=\"top\">
<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"margin-bottom:40px;\">
<tbody>
<tr>
<td width=\"35%\" align=\"left\" valign=\"top\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >
<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; \">Total 10 Year Saving</td>
</tr>
<tr>
<td align=\"center\">
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:1px solid #cccccc; font-size:20px;padding: 3px 20px;font-weight: bold;\">$" . number_format((float) ($TotalRevenue), 2, '.', '') . "</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>

<td width=\"25%\" align=\"left\" valign=\"top\">
<table width=\"50%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; \">Total STC Rebate</td>
</tr>
<tr>
<td align=\"center\">
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:1px solid #cccccc; font-size:20px;padding: 3px 20px;font-weight: bold;\">$" . number_format((float) ($proposal_data['stc_rebate_value']), 2, '.', '') . "</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height=\"20px\">&nbsp;</td>
</tr>
<tr>
<td width=\"35%\" align=\"left\" valign=\"top\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr> 
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; \">25 Years Benefit</td>
</tr>
<tr>
<td align=\"center\">
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:1px solid #d2232a; background-color:#d2232a; font-size:20px; color: #fff;padding: 3px 20px;font-weight: bold;\">$" . number_format((float) ($twentyFiveYearsBenefit), 2, '.', '') . "</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>

<td width=\"25%\" align=\"left\" valign=\"top\">
<table width=\"50%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"font-family: 'Open Sans', sans-serif; font-size:14px; \">10 Years Solar Energy Rate</td>
</tr>
<tr>
<td align=\"center\">
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
<td height=\"40\" align=\"center\" valign=\"middle\" style=\"border:1px solid #d2232a; background-color:#d2232a; font-size:20px; color: #fff;padding: 3px 20px;font-weight: bold;\">9 c/kWh</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td  height=\"110px\">&nbsp;</td>
</tr>
<tr>
<td align=\"left\" valign=\"top\"><img src=\"$baseurl". "assets/pdf_images/solar/footer.jpg\" alt=\"\" title=\"\" style=\"width:100%; display:block;\"></td>
</tr>";
if (!empty($proposal_finance_data) && $proposal_finance_data['term'] != 0) {
    echo $html;	
}
	?>
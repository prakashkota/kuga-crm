
<html>
<head>
    <link href='<?php echo site_url(); ?>assets/dropzone/dropzone.min.css' type='text/css' rel='stylesheet'>
    <link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css" />

    <script src="<?php echo site_url(); ?>assets/dropzone/dropzone.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js" ></script>
    <script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
    <script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
</head>
<body>

    <div class="d-block clearfix ">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center p-2 border-bottom">
            <h1 class="h2">Manage Consumption Profile (Meter Data)</h1>
            <div class="content-header-right col-md-6 col-12 "></div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 p-5">
                <div class="form-block d-block clearfix">
                    <div class="dropzone" id="dropzone">
                        <div class="dz-message needsclick">
                            Drop files here or click to upload.<br>
                        </div>
                    </div>
                    <div class="btn-block mt-2">
                        <input type="button" id="save_meter_data_btn" onclick="save_solar_proposal();" value="Save" class="btn btn-primary hidden">
                    </div>
                    <div class="btn-block mt-2 alert alert-success preview-text" style="display:none;">
                        Please wait we are uploading preview of uplaoded file...
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-12 col-sm-12 col-md-4 p-5 hidden" id="meter_data_table_wrapper" >
                <div class="card">
                    <h2 class="card-header">Current Column Definitions</h2>
                    <div class="card-body">
                        <div class="form-block d-block clearfix">
                            <div class="table-responsive"  id="meter_data_table_container" style="overflow-y:scroll; height: 400px;">

                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-8 p-5 hidden" id="extrapolation_container">
                <div class="card">
                    <h2 class="card-header">
                        Manage Data Extrapolation
                    </h2>
                    <div class="card-body">
                        <div id="mytable"></div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row"> 
            <div class="col-12 col-sm-12 col-md-12 p-5 hidden" id="meter_data_graph_wrapper">
                <div class="card">
                    <h2 class="card-header">Graphical Representation 1</h2>
                    <div class="card-body">
                        <div class="form-block d-block clearfix">
                            <div id="meter_data_graph_container" class="row">
                                <div class="col-12 col-md-6">
                                    <div id="avg_load_area_chart" style="width:100%; height:300px;"></div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div id="weekend_load_area_chart" style="width:100%; height:300px;"></div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-12 col-sm-12 col-md-12 p-5 hidden" id="meter_data_graph_wrapper_2">
                <div class="card">
                    <h2 class="card-header">Graphical Representation 2</h2>
                    <div class="card-body">
                        <div class="form-block d-block clearfix">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <h3>DAILY PRODUCTION - TYPICAL USAGE</h3>
                                    <div class="btn-group col-md-8" role="group" >
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <input type="text" name="reportrange1" id="reportrange1" />
                                    </div>
                                    <div id="typical_export_graph" style="width:100%; height:300px;"></div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <h3>DAILY PRODUCTION - LOW USAGE</h3>
                                    <div class="btn-group col-md-8" role="group" >
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <input type="text" name="reportrange2" id="reportrange2" />
                                    </div>
                                    <div id="worst_export_graph" style="width:100%; height:300px;"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <h3>DAILY PRODUCTION - HIGH USAGE</h3>
                                    <div class="btn-group col-md-8" role="group" >
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <input type="text" name="reportrange3" id="reportrange3" />
                                    </div>
                                    <div id="high_export_graph" style="width:100%; height:300px;"></div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <h3>DAILY PRODUCTION - PEAK USAGE</h3>
                                    <div class="btn-group col-md-8" role="group" >
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <input type="text" name="reportrange4" id="reportrange4" />
                                    </div>
                                    <div id="best_export_graph" style="width:100%; height:300px;"></div>
                                </div>
                            </div> 
                        </div>
                        <div class="btn-block mt-2">
                            <input type="button" id="save_meter_data_dates_btn" onclick="save_meter_data_graph_dates();" value="Save" class="btn btn-primary hidden">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    

    <script>
        var proposal_id = '<?php echo (isset($proposal_id)) ? $proposal_id : '' ?>';
        var meter_data_file = '<?php echo (isset($meter_data_file)) ? $meter_data_file : '' ?>';
        var is_meter_data = '<?php echo (isset($is_meter_data)) ? $is_meter_data : '' ?>';
        var meter_data_graph_dates = '<?php echo (isset($meter_data_graph_dates) && $meter_data_graph_dates != '') ? json_encode($meter_data_graph_dates) : '' ?>';
        var excel_table = '';
        var missing_months = [];
        var meter_data_month_sum = [];
        var meter_data_replicated = [];

        if (is_meter_data == '1') {
            fetch_meter_data_file(meter_data_file);
        }

        var myDropzone = new Dropzone("div#dropzone", {
            url: base_url + "admin/uploader/upload_meter_data_file",
            sending: function(file, xhr, formData) {
                formData.append("proposal_id", proposal_id);
            },
            success: function(file, response) {
                response = JSON.parse(response);
                if (response.success) {
                    toastr['success'](response.status);
                    meter_data_file = response.file_name;
                    fetch_meter_data_file(response.file_name);
                    $(".preview-text").show();
                } else {
                    toastr['error'](response.status);
                }
            }
        }, );


        function fetch_meter_data_file(file) {
            $.ajax({
                url: base_url + 'admin/proposal/fetch_meter_data_file',
                dataType: 'json',
                data: {
                    file_name: file
                },
                type: 'post',
                beforeSend: function() {
                    $('#meter_data_table_wrapper').removeClass('hidden');
                    document.getElementById('meter_data_table_container').innerHTML = '';
                    document.getElementById('meter_data_table_container').appendChild(createPlaceHolder(false));
                    document.getElementById('meter_data_table_container').appendChild(createPlaceHolder(false));
                    document.getElementById('meter_data_table_container').appendChild(createPlaceHolder(false));
                    document.getElementById('meter_data_table_container').appendChild(createPlaceHolder(false));
                },
                success: function(res) {
                    missing_months = res.meter_data_missing_months;
                    var meter_data = res.meter_data;
                    create_meter_data_table(meter_data);
                    google.load('visualization', '1.0', {
                        'packages': ['corechart']
                    });
                    google.charts.setOnLoadCallback(function() {
                        draw_average_monthly_load_chart(res.avg_monthly_load_graph_data);
                        draw_weekend_load_chart(res.weekend_load_graph_data);
                    //draw_summer_load_chart(res.summer_load_graph_data);
                    //draw_winter_load_chart(res.winter_load_graph_data);
                });
                    $('#save_meter_data_btn').removeClass('hidden');
                    $('#save_meter_data_dates_btn').removeClass('hidden');
                    $('#meter_data_graph_wrapper').removeClass('hidden');
            
                    if (missing_months.length > 0) {
                        $('#extrapolation_container').removeClass('hidden');
                        var final_data = [];
                        var months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
                        for (var i = 0; i < 24; i++) {
                            final_data[i] = [];
                            for (var j = 0; j < months.length; j++) {
                                final_data[i].push(parseFloat(res.avg_monthly_load_graph_data[months[j]][i].toFixed(2)));
                            }
                        }
                    //final_data.push(month_meter_data);
                    // console.log(final_data);
                    jexcel.destroy(document.getElementById('mytable'), false);
                    var copyFrom =  [];
                    var source = 0;
                    var destination = 0;
                    excel_table = jexcel(document.getElementById('mytable'), {
                        data: final_data,
                        columns: [{
                            type: 'text',
                            title: 'Jan',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Feb',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Mar',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Apr',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'May',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Jun',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Jul',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Aug',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Sep',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Oct',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Nov',
                            width: 60
                        },
                        {
                            type: 'text',
                            title: 'Dec',
                            width: 60
                        },
                        ],
                        onselection:function(obj, col, val) {
                            if(copyFrom.indexOf(col)==-1){
                                copyFrom.push(col);
                            }
                        },
                        onpaste:function(obj, cell, val) {
                            source = copyFrom[copyFrom.length-2] || 0;
                            destination = cell[0]['col'];
                            meter_data_replicated.push({
                                'replicated_from': parseInt(source)+1,
                                'replicated_to': parseInt(destination)+1
                            });
                            console.log(meter_data_replicated);
                            copyFrom = [];
                        },
                    });
                    
                    //$('#reportrange1').trigger('change');
                    //$('#meter_data_graph_wrapper_2').removeClass('hidden');
                    $('#meter_data_graph_wrapper').removeClass('hidden');
                } else {
                    $('#extrapolation_container').addClass('hidden');
                }
                $('#meter_data_graph_wrapper').removeClass('hidden');
                $(".preview-text").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

}

function create_meter_data_table(meter_data) {
    var table = document.createElement('table');
    table.className = 'table table-striped';

    var table_head = document.createElement('thead');
    var head_array = ['DateTime', 'kW'];
    var tr1 = document.createElement('tr');
    for (var i = 0; i < head_array.length; i++) {
        var th = document.createElement('th');
        th.innerHTML = head_array[i];
        tr1.appendChild(th);
    }
    table_head.appendChild(tr1);

    var table_body = document.createElement('tbody');

    for (var i = 0; i < meter_data.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < meter_data[i].length; j++) {
            var th = document.createElement('th');
            th.innerHTML = meter_data[i][j];
            tr.appendChild(th);
        }

        table_body.appendChild(tr);
    }
    table.appendChild(table_head);
    table.appendChild(table_body);
    document.getElementById('meter_data_table_container').innerHTML = '';
    document.getElementById('meter_data_table_container').appendChild(table);
}

function save_solar_proposal() {
    meter_data_replicated,
    meter_data_month_sum = [];

    if (missing_months.length > 0) {
        var flag = true;
        for (var i = 0; i < 12; i++) {
            var columnSum = excel_table.getColumnData(i).reduce(function(pv, cv) {
                return parseFloat(pv) + parseFloat(cv);
            }, 0);
            var month = missing_months[j];
            if (i != (month - 1)) {
                meter_data_month_sum.push(columnSum);
            } else {
                meter_data_month_sum.push(0);
            }
        }

           //console.log(meter_data_month_sum);
            for (var j in missing_months) {
                var key = parseInt(missing_months[j] - 1);
                var columnSumMissing = excel_table.getColumnData(key).reduce(function(pv, cv) {
                    return parseFloat(pv) + parseFloat(cv);
                }, 0);
                for (var i = 0; i < meter_data_month_sum.length; i++) {
                    if (columnSumMissing == meter_data_month_sum[i] && (i != key)) {
                        // meter_data_replicated.push({
                        //     'replicated_from': (i + 1),
                        //     'replicated_to': missing_months[j]
                        // });
                    } else if (columnSumMissing == 0) {
                        toastr["error"]('Meter Data for month ' + missing_months[j] + ' is missing.');
                        return false;
                    }
                }
            }
        }

        //console.log(meter_data_replicated);

        var formData = 'solar_proposal[is_meter_data]=1';
        formData += '&solar_proposal[meter_data_file]=' + meter_data_file;
        formData += '&proposal_id=' + proposal_id;
        if (meter_data_replicated != '') {
            formData += '&solar_proposal[meter_data_replicated]=' + JSON.stringify(meter_data_replicated);
        }

        $.ajax({
            url: base_url + 'admin/proposal/save_solar_proposal',
            type: 'post',
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                toastr["info"]('Saving Meter Data, Please wait...');
            },
            success: function(stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    window.location.reload();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function save_meter_data_graph_dates() {
        var data = {};
        data.proposal_id = proposal_id;
        data.graph1_date = $('#reportrange1').val();
        data.graph2_date = $('#reportrange2').val();
        data.graph3_date = $('#reportrange3').val();
        data.graph4_date = $('#reportrange4').val();
        $.ajax({
            url: base_url + 'admin/proposal/save_meter_data_graph_dates',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                toastr["info"]('Saving Meter Data Dates, Please wait...');
            },
            success: function(stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function fetch_usage_graph_data(data) {
        $.ajax({
            url: base_url + 'admin/proposal/fetch_usage_graph_data',
            type: 'get',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                document.getElementById('typical_export_graph').innerHTML = '';
                document.getElementById('worst_export_graph').innerHTML = '';
                document.getElementById('high_export_graph').innerHTML = '';
                document.getElementById('best_export_graph').innerHTML = '';
                document.getElementById('typical_export_graph').appendChild(createPlaceHolder(false));
                document.getElementById('worst_export_graph').appendChild(createPlaceHolder(false));
                document.getElementById('high_export_graph').appendChild(createPlaceHolder(false));
                document.getElementById('best_export_graph').appendChild(createPlaceHolder(false));
            },
            success: function(stat) {
                document.getElementById('typical_export_graph').innerHTML = '';
                document.getElementById('worst_export_graph').innerHTML = '';
                document.getElementById('high_export_graph').innerHTML = '';
                document.getElementById('best_export_graph').innerHTML = '';
                create_usage_graph(stat);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function draw_average_monthly_load_chart(data) {
        var date = '02/03';
        var arr = [];
        arr.push(['Year', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        for (var i = 0; i < data['jan'].length; i++) {
            arr.push([i + ':00',
                parseFloat(data['jan'][i]),
                parseFloat(data['feb'][i]),
                parseFloat(data['mar'][i]),
                parseFloat(data['apr'][i]),
                parseFloat(data['may'][i]),
                parseFloat(data['jun'][i]),
                parseFloat(data['jul'][i]),
                parseFloat(data['aug'][i]),
                parseFloat(data['sep'][i]),
                parseFloat(data['oct'][i]),
                parseFloat(data['nov'][i]),
                parseFloat(data['dec'][i])
                ]);
        }

        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            title: 'Average Monthly',
            titleTextStyle: {
                fontName: 'Open Sans',
                italic: false,
                bold: true,
                fontStyle: 'normal' //or bold, italic, etc.
            },
            curveType: 'function',
            legend: {
                position: 'bottom',
                alignment: 'start'
            },
            series: {
                0: {
                    color: '#3B8DBC'
                },
                1: {
                    color: '#81D7A6'
                },
                2: {
                    color: '#EFC614'
                },
                3: {
                    color: '#FFB69B'
                },
                4: {
                    color: '#7E3735'
                },
                5: {
                    color: '#7ED3CC'
                },
                6: {
                    color: '#C7EA8C'
                },
                7: {
                    color: '#FFF58F'
                },
                8: {
                    color: '#C99182'
                },
                9: {
                    color: '#4D2F2F'
                },
                10: {
                    color: '#3D8FC1'
                },
                11: {
                    color: '#5BAA7D'
                }
            },
            pointSize: 4,
            hAxis: {
                showTextEvery: 2,
                title: 'Hours',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            },
            vAxis: {
                title: 'Power(kW)',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('avg_load_area_chart'));

        chart.draw(data, options);
    }

    function draw_weekend_load_chart(data) {
        var arr = [];
        arr.push(['Year', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']);
        for (var i = 0; i < data['mon'].length; i++) {
            arr.push([i + ':00',
                parseFloat(data['mon'][i]),
                parseFloat(data['tue'][i]),
                parseFloat(data['wed'][i]),
                parseFloat(data['thu'][i]),
                parseFloat(data['fri'][i]),
                parseFloat(data['sat'][i]),
                parseFloat(data['sun'][i])
                ]);
        }
        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            title: 'Average Daily',
            titleTextStyle: {
                fontName: 'Open Sans',
                italic: false,
                bold: true,
                fontStyle: 'normal' //or bold, italic, etc.
            },
            curveType: 'function',
            legend: {
                position: 'bottom',
                alignment: 'start'
            },
            series: {
                0: {
                    color: '#3B8DBC'
                },
                1: {
                    color: '#81D7A6'
                },
                2: {
                    color: '#EFC614'
                },
                3: {
                    color: '#FFB69B'
                },
                4: {
                    color: '#7E3735'
                },
                5: {
                    color: '#7ED3CC'
                },
                6: {
                    color: '#C7EA8C'
                }
            },
            pointSize: 4,
            hAxis: {
                showTextEvery: 2,
                title: 'Hours',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            },
            vAxis: {
                title: 'Power(kW)',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('weekend_load_area_chart'));

        chart.draw(data, options);
    }

    function draw_summer_load_chart(data) {
        var arr = [
        ['Year', 'Average Load'],
        ['0:00', parseFloat(data[0])],
        ['1:00', parseFloat(data[1])],
        ['2:00', parseFloat(data[2])],
        ['3:00', parseFloat(data[3])],
        ['4:00', parseFloat(data[4])],
        ['5:00', parseFloat(data[5])],
        ['6:00', parseFloat(data[6])],
        ['7:00', parseFloat(data[7])],
        ['8:00', parseFloat(data[8])],
        ['9:00', parseFloat(data[9])],
        ['10:00', parseFloat(data[10])],
        ['11:00', parseFloat(data[11])],
        ['12:00', parseFloat(data[12])],
        ['13:00', parseFloat(data[13])],
        ['14:00', parseFloat(data[14])],
        ['15:00', parseFloat(data[15])],
        ['16:00', parseFloat(data[16])],
        ['17:00', parseFloat(data[17])],
        ['18:00', parseFloat(data[18])],
        ['20:00', parseFloat(data[19])],
        ['21:00', parseFloat(data[20])],
        ['22:00', parseFloat(data[21])],
        ['23:00', parseFloat(data[22])]
        ];
        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            title: 'Summer Load Representation Based on Meter Data',
            titleTextStyle: {
                fontName: 'Open Sans',
                italic: false,
                bold: true,
                fontStyle: 'normal' //or bold, italic, etc.
            },
            colors: ['#666666', '#DC3912'],
            curveType: 'function',
            legend: 'none',
            hAxis: {
                showTextEvery: 2,
                title: 'Hours',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            },
            vAxis: {
                title: 'Power(kW)',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            }
        };

        var chart = new google.visualization.AreaChart(document.getElementById('summer_load_area_chart'));

        chart.draw(data, options);
    }

    function draw_winter_load_chart(data) {
        var arr = [
        ['Year', 'Average Load'],
        ['0:00', parseFloat(data[0])],
        ['1:00', parseFloat(data[1])],
        ['2:00', parseFloat(data[2])],
        ['3:00', parseFloat(data[3])],
        ['4:00', parseFloat(data[4])],
        ['5:00', parseFloat(data[5])],
        ['6:00', parseFloat(data[6])],
        ['7:00', parseFloat(data[7])],
        ['8:00', parseFloat(data[8])],
        ['9:00', parseFloat(data[9])],
        ['10:00', parseFloat(data[10])],
        ['11:00', parseFloat(data[11])],
        ['12:00', parseFloat(data[12])],
        ['13:00', parseFloat(data[13])],
        ['14:00', parseFloat(data[14])],
        ['15:00', parseFloat(data[15])],
        ['16:00', parseFloat(data[16])],
        ['17:00', parseFloat(data[17])],
        ['18:00', parseFloat(data[18])],
        ['20:00', parseFloat(data[19])],
        ['21:00', parseFloat(data[20])],
        ['22:00', parseFloat(data[21])],
        ['23:00', parseFloat(data[22])]
        ];
        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            title: 'Winter Load Representation Based on Meter Data',
            titleTextStyle: {
                fontName: 'Open Sans',
                italic: false,
                bold: true,
                fontStyle: 'normal' //or bold, italic, etc.
            },
            colors: ['#666666', '#DC3912'],
            curveType: 'function',
            legend: 'none',
            hAxis: {
                showTextEvery: 2,
                title: 'Hours',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            },
            vAxis: {
                title: 'Power(kW)',
                titleTextStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Open Sans',
                    italic: false,
                    bold: false,
                    fontSize: 10,
                    fontStyle: 'normal'
                }
            }
        };

        var chart = new google.visualization.AreaChart(document.getElementById('winter_load_area_chart'));

        chart.draw(data, options);
    }



    function create_usage_graph(data) {
        var i = 1;
        var labels = {};
        Highcharts.chart('typical_export_graph', {
            chart: {
                type: 'area',
                width: 320,
                height: 300,
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            title: {
                style: {
                    display: 'none'
                }
            },
            xAxis: {
                title: {
                    text: 'Hours'
                },
                type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);

                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'Power(kW)'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: false,
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 3600 * 1000
                }
            },
            series: [{
                name: 'Load',
                data: [
                data['typical_export_load_graph_data'][0],
                data['typical_export_load_graph_data'][1],
                data['typical_export_load_graph_data'][2],
                data['typical_export_load_graph_data'][3],
                data['typical_export_load_graph_data'][4],
                data['typical_export_load_graph_data'][5],
                data['typical_export_load_graph_data'][6],
                data['typical_export_load_graph_data'][7],
                data['typical_export_load_graph_data'][8],
                data['typical_export_load_graph_data'][9],
                data['typical_export_load_graph_data'][10],
                data['typical_export_load_graph_data'][11],
                data['typical_export_load_graph_data'][12],
                data['typical_export_load_graph_data'][13],
                data['typical_export_load_graph_data'][14],
                data['typical_export_load_graph_data'][15],
                data['typical_export_load_graph_data'][16],
                data['typical_export_load_graph_data'][17],
                data['typical_export_load_graph_data'][18],
                data['typical_export_load_graph_data'][19],
                data['typical_export_load_graph_data'][20],
                data['typical_export_load_graph_data'][21],
                data['typical_export_load_graph_data'][22]
                ],
                color: '#4B4B4B',
                type: 'area',
            }, {
                name: 'Solar Output',
                data: [
                data['typical_export_sol_prd_graph_data'][0],
                data['typical_export_sol_prd_graph_data'][1],
                data['typical_export_sol_prd_graph_data'][2],
                data['typical_export_sol_prd_graph_data'][3],
                data['typical_export_sol_prd_graph_data'][4],
                data['typical_export_sol_prd_graph_data'][5],
                data['typical_export_sol_prd_graph_data'][6],
                data['typical_export_sol_prd_graph_data'][7],
                data['typical_export_sol_prd_graph_data'][8],
                data['typical_export_sol_prd_graph_data'][9],
                data['typical_export_sol_prd_graph_data'][10],
                data['typical_export_sol_prd_graph_data'][11],
                data['typical_export_sol_prd_graph_data'][12],
                data['typical_export_sol_prd_graph_data'][13],
                data['typical_export_sol_prd_graph_data'][14],
                data['typical_export_sol_prd_graph_data'][15],
                data['typical_export_sol_prd_graph_data'][16],
                data['typical_export_sol_prd_graph_data'][17],
                data['typical_export_sol_prd_graph_data'][18],
                data['typical_export_sol_prd_graph_data'][19],
                data['typical_export_sol_prd_graph_data'][20],
                data['typical_export_sol_prd_graph_data'][21],
                data['typical_export_sol_prd_graph_data'][22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                    fillColor: 'yellow',
                    lineWidth: 2,
                    lineColor: 'yellow',
                    enabled: true
                }
            },
            {
                name: 'Export',
                data: [
                data['typical_export_export_graph_data'][0],
                data['typical_export_export_graph_data'][1],
                data['typical_export_export_graph_data'][2],
                data['typical_export_export_graph_data'][3],
                data['typical_export_export_graph_data'][4],
                data['typical_export_export_graph_data'][5],
                data['typical_export_export_graph_data'][6],
                data['typical_export_export_graph_data'][7],
                data['typical_export_export_graph_data'][8],
                data['typical_export_export_graph_data'][9],
                data['typical_export_export_graph_data'][10],
                data['typical_export_export_graph_data'][11],
                data['typical_export_export_graph_data'][12],
                data['typical_export_export_graph_data'][13],
                data['typical_export_export_graph_data'][14],
                data['typical_export_export_graph_data'][15],
                data['typical_export_export_graph_data'][16],
                data['typical_export_export_graph_data'][17],
                data['typical_export_export_graph_data'][18],
                data['typical_export_export_graph_data'][19],
                data['typical_export_export_graph_data'][20],
                data['typical_export_export_graph_data'][21],
                data['typical_export_export_graph_data'][22]
                ],
                color: '#FF4B4C',
                type: 'area',
            }, {
                name: 'Offset Consumptio',
                data: [
                data['typical_export_offset_graph_data'][0],
                data['typical_export_offset_graph_data'][1],
                data['typical_export_offset_graph_data'][2],
                data['typical_export_offset_graph_data'][3],
                data['typical_export_offset_graph_data'][4],
                data['typical_export_offset_graph_data'][5],
                data['typical_export_offset_graph_data'][6],
                data['typical_export_offset_graph_data'][7],
                data['typical_export_offset_graph_data'][8],
                data['typical_export_offset_graph_data'][9],
                data['typical_export_offset_graph_data'][10],
                data['typical_export_offset_graph_data'][11],
                data['typical_export_offset_graph_data'][12],
                data['typical_export_offset_graph_data'][13],
                data['typical_export_offset_graph_data'][14],
                data['typical_export_offset_graph_data'][15],
                data['typical_export_offset_graph_data'][16],
                data['typical_export_offset_graph_data'][17],
                data['typical_export_offset_graph_data'][18],
                data['typical_export_offset_graph_data'][19],
                data['typical_export_offset_graph_data'][20],
                data['typical_export_offset_graph_data'][21],
                data['typical_export_offset_graph_data'][22]
                ],
                color: '#81B94C',
                type: 'area',
            }
            ]


        });

var i = 1;
var labels = {};
Highcharts.chart('worst_export_graph', {
    chart: {
        type: 'area',
        width: 320,
        height: 300,
    },
    credits: {
        enabled: false
    },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    title: {
        style: {
            display: 'none'
        }
    },
    xAxis: {
        title: {
            text: 'Hours'
        },
        type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);

                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'Power(kW)'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: false,
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 3600 * 1000
                }
            },
            series: [{
                name: 'Load',
                data: [
                data['worst_export_load_graph_data'][0],
                data['worst_export_load_graph_data'][1],
                data['worst_export_load_graph_data'][2],
                data['worst_export_load_graph_data'][3],
                data['worst_export_load_graph_data'][4],
                data['worst_export_load_graph_data'][5],
                data['worst_export_load_graph_data'][6],
                data['worst_export_load_graph_data'][7],
                data['worst_export_load_graph_data'][8],
                data['worst_export_load_graph_data'][9],
                data['worst_export_load_graph_data'][10],
                data['worst_export_load_graph_data'][11],
                data['worst_export_load_graph_data'][12],
                data['worst_export_load_graph_data'][13],
                data['worst_export_load_graph_data'][14],
                data['worst_export_load_graph_data'][15],
                data['worst_export_load_graph_data'][16],
                data['worst_export_load_graph_data'][17],
                data['worst_export_load_graph_data'][18],
                data['worst_export_load_graph_data'][19],
                data['worst_export_load_graph_data'][20],
                data['worst_export_load_graph_data'][21],
                data['worst_export_load_graph_data'][22]
                ],
                color: '#4B4B4B',
                type: 'area',
            }, {
                name: 'Solar Output',
                data: [
                data['worst_export_sol_prd_graph_data'][0],
                data['worst_export_sol_prd_graph_data'][1],
                data['worst_export_sol_prd_graph_data'][2],
                data['worst_export_sol_prd_graph_data'][3],
                data['worst_export_sol_prd_graph_data'][4],
                data['worst_export_sol_prd_graph_data'][5],
                data['worst_export_sol_prd_graph_data'][6],
                data['worst_export_sol_prd_graph_data'][7],
                data['worst_export_sol_prd_graph_data'][8],
                data['worst_export_sol_prd_graph_data'][9],
                data['worst_export_sol_prd_graph_data'][10],
                data['worst_export_sol_prd_graph_data'][11],
                data['worst_export_sol_prd_graph_data'][12],
                data['worst_export_sol_prd_graph_data'][13],
                data['worst_export_sol_prd_graph_data'][14],
                data['worst_export_sol_prd_graph_data'][15],
                data['worst_export_sol_prd_graph_data'][16],
                data['worst_export_sol_prd_graph_data'][17],
                data['worst_export_sol_prd_graph_data'][18],
                data['worst_export_sol_prd_graph_data'][19],
                data['worst_export_sol_prd_graph_data'][20],
                data['worst_export_sol_prd_graph_data'][21],
                data['worst_export_sol_prd_graph_data'][22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                    fillColor: 'yellow',
                    lineWidth: 2,
                    lineColor: 'yellow',
                    enabled: true
                }
            },
            {
                name: 'Export',
                data: [
                data['worst_export_export_graph_data'][0],
                data['worst_export_export_graph_data'][1],
                data['worst_export_export_graph_data'][2],
                data['worst_export_export_graph_data'][3],
                data['worst_export_export_graph_data'][4],
                data['worst_export_export_graph_data'][5],
                data['worst_export_export_graph_data'][6],
                data['worst_export_export_graph_data'][7],
                data['worst_export_export_graph_data'][8],
                data['worst_export_export_graph_data'][9],
                data['worst_export_export_graph_data'][10],
                data['worst_export_export_graph_data'][11],
                data['worst_export_export_graph_data'][12],
                data['worst_export_export_graph_data'][13],
                data['worst_export_export_graph_data'][14],
                data['worst_export_export_graph_data'][15],
                data['worst_export_export_graph_data'][16],
                data['worst_export_export_graph_data'][17],
                data['worst_export_export_graph_data'][18],
                data['worst_export_export_graph_data'][19],
                data['worst_export_export_graph_data'][20],
                data['worst_export_export_graph_data'][21],
                data['worst_export_export_graph_data'][22]
                ],
                color: '#FF4B4C',
                type: 'area',
            }, {
                name: 'Offset Consumptio',
                data: [
                data['worst_export_offset_graph_data'][0],
                data['worst_export_offset_graph_data'][1],
                data['worst_export_offset_graph_data'][2],
                data['worst_export_offset_graph_data'][3],
                data['worst_export_offset_graph_data'][4],
                data['worst_export_offset_graph_data'][5],
                data['worst_export_offset_graph_data'][6],
                data['worst_export_offset_graph_data'][7],
                data['worst_export_offset_graph_data'][8],
                data['worst_export_offset_graph_data'][9],
                data['worst_export_offset_graph_data'][10],
                data['worst_export_offset_graph_data'][11],
                data['worst_export_offset_graph_data'][12],
                data['worst_export_offset_graph_data'][13],
                data['worst_export_offset_graph_data'][14],
                data['worst_export_offset_graph_data'][15],
                data['worst_export_offset_graph_data'][16],
                data['worst_export_offset_graph_data'][17],
                data['worst_export_offset_graph_data'][18],
                data['worst_export_offset_graph_data'][19],
                data['worst_export_offset_graph_data'][20],
                data['worst_export_offset_graph_data'][21],
                data['worst_export_offset_graph_data'][22]
                ],
                color: '#81B94C',
                type: 'area',
            }
            ]


        });

var i = 1;
var labels = {};
Highcharts.chart('high_export_graph', {
    chart: {
        type: 'area',
        width: 320,
        height: 300,
    },
    credits: {
        enabled: false
    },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    title: {
        style: {
            display: 'none'
        }
    },
    xAxis: {
        title: {
            text: 'Hours'
        },
        type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);

                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'Power(kW)'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: false,
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 3600 * 1000
                }
            },
            series: [{
                name: 'Load',
                data: [
                data['high_export_load_graph_data'][0],
                data['high_export_load_graph_data'][1],
                data['high_export_load_graph_data'][2],
                data['high_export_load_graph_data'][3],
                data['high_export_load_graph_data'][4],
                data['high_export_load_graph_data'][5],
                data['high_export_load_graph_data'][6],
                data['high_export_load_graph_data'][7],
                data['high_export_load_graph_data'][8],
                data['high_export_load_graph_data'][9],
                data['high_export_load_graph_data'][10],
                data['high_export_load_graph_data'][11],
                data['high_export_load_graph_data'][12],
                data['high_export_load_graph_data'][13],
                data['high_export_load_graph_data'][14],
                data['high_export_load_graph_data'][15],
                data['high_export_load_graph_data'][16],
                data['high_export_load_graph_data'][17],
                data['high_export_load_graph_data'][18],
                data['high_export_load_graph_data'][19],
                data['high_export_load_graph_data'][20],
                data['high_export_load_graph_data'][21],
                data['high_export_load_graph_data'][22]
                ],
                color: '#4B4B4B',
                type: 'area',
            }, {
                name: 'Solar Output',
                data: [
                data['high_export_sol_prd_graph_data'][0],
                data['high_export_sol_prd_graph_data'][1],
                data['high_export_sol_prd_graph_data'][2],
                data['high_export_sol_prd_graph_data'][3],
                data['high_export_sol_prd_graph_data'][4],
                data['high_export_sol_prd_graph_data'][5],
                data['high_export_sol_prd_graph_data'][6],
                data['high_export_sol_prd_graph_data'][7],
                data['high_export_sol_prd_graph_data'][8],
                data['high_export_sol_prd_graph_data'][9],
                data['high_export_sol_prd_graph_data'][10],
                data['high_export_sol_prd_graph_data'][11],
                data['high_export_sol_prd_graph_data'][12],
                data['high_export_sol_prd_graph_data'][13],
                data['high_export_sol_prd_graph_data'][14],
                data['high_export_sol_prd_graph_data'][15],
                data['high_export_sol_prd_graph_data'][16],
                data['high_export_sol_prd_graph_data'][17],
                data['high_export_sol_prd_graph_data'][18],
                data['high_export_sol_prd_graph_data'][19],
                data['high_export_sol_prd_graph_data'][20],
                data['high_export_sol_prd_graph_data'][21],
                data['high_export_sol_prd_graph_data'][22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                    fillColor: 'yellow',
                    lineWidth: 2,
                    lineColor: 'yellow',
                    enabled: true
                }
            },
            {
                name: 'Export',
                data: [
                data['high_export_export_graph_data'][0],
                data['high_export_export_graph_data'][1],
                data['high_export_export_graph_data'][2],
                data['high_export_export_graph_data'][3],
                data['high_export_export_graph_data'][4],
                data['high_export_export_graph_data'][5],
                data['high_export_export_graph_data'][6],
                data['high_export_export_graph_data'][7],
                data['high_export_export_graph_data'][8],
                data['high_export_export_graph_data'][9],
                data['high_export_export_graph_data'][10],
                data['high_export_export_graph_data'][11],
                data['high_export_export_graph_data'][12],
                data['high_export_export_graph_data'][13],
                data['high_export_export_graph_data'][14],
                data['high_export_export_graph_data'][15],
                data['high_export_export_graph_data'][16],
                data['high_export_export_graph_data'][17],
                data['high_export_export_graph_data'][18],
                data['high_export_export_graph_data'][19],
                data['high_export_export_graph_data'][20],
                data['high_export_export_graph_data'][21],
                data['high_export_export_graph_data'][22]
                ],
                color: '#FF4B4C',
                type: 'area',
            }, {
                name: 'Offset Consumptio',
                data: [
                data['high_export_offset_graph_data'][0],
                data['high_export_offset_graph_data'][1],
                data['high_export_offset_graph_data'][2],
                data['high_export_offset_graph_data'][3],
                data['high_export_offset_graph_data'][4],
                data['high_export_offset_graph_data'][5],
                data['high_export_offset_graph_data'][6],
                data['high_export_offset_graph_data'][7],
                data['high_export_offset_graph_data'][8],
                data['high_export_offset_graph_data'][9],
                data['high_export_offset_graph_data'][10],
                data['high_export_offset_graph_data'][11],
                data['high_export_offset_graph_data'][12],
                data['high_export_offset_graph_data'][13],
                data['high_export_offset_graph_data'][14],
                data['high_export_offset_graph_data'][15],
                data['high_export_offset_graph_data'][16],
                data['high_export_offset_graph_data'][17],
                data['high_export_offset_graph_data'][18],
                data['high_export_offset_graph_data'][19],
                data['high_export_offset_graph_data'][20],
                data['high_export_offset_graph_data'][21],
                data['high_export_offset_graph_data'][22]
                ],
                color: '#81B94C',
                type: 'area',
            }
            ]


        });

var i = 1;
var labels = {};
Highcharts.chart('best_export_graph', {
    chart: {
        type: 'area',
        width: 320,
        height: 300,
    },
    credits: {
        enabled: false
    },
    navigation: {
        buttonOptions: {
            enabled: false
        }
    },
    title: {
        style: {
            display: 'none'
        }
    },
    xAxis: {
        title: {
            text: 'Hours'
        },
        type: 'datetime',
                //step: 24,
                tickInterval: 3600 * 1000,

                //minTickInterval: 24,
                labels: {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);

                    },
                },
            },
            yAxis: {
                allowDecimals: true,
                title: {
                    text: 'Power(kW)'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend: false,
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 0, 17),
                    pointInterval: 3600 * 1000
                }
            },
            series: [{
                name: 'Load',
                data: [
                data['best_export_load_graph_data'][0],
                data['best_export_load_graph_data'][1],
                data['best_export_load_graph_data'][2],
                data['best_export_load_graph_data'][3],
                data['best_export_load_graph_data'][4],
                data['best_export_load_graph_data'][5],
                data['best_export_load_graph_data'][6],
                data['best_export_load_graph_data'][7],
                data['best_export_load_graph_data'][8],
                data['best_export_load_graph_data'][9],
                data['best_export_load_graph_data'][10],
                data['best_export_load_graph_data'][11],
                data['best_export_load_graph_data'][12],
                data['best_export_load_graph_data'][13],
                data['best_export_load_graph_data'][14],
                data['best_export_load_graph_data'][15],
                data['best_export_load_graph_data'][16],
                data['best_export_load_graph_data'][17],
                data['best_export_load_graph_data'][18],
                data['best_export_load_graph_data'][19],
                data['best_export_load_graph_data'][20],
                data['best_export_load_graph_data'][21],
                data['best_export_load_graph_data'][22]
                ],
                color: '#4B4B4B',
                type: 'area',
            }, {
                name: 'Solar Output',
                data: [
                data['best_export_sol_prd_graph_data'][0],
                data['best_export_sol_prd_graph_data'][1],
                data['best_export_sol_prd_graph_data'][2],
                data['best_export_sol_prd_graph_data'][3],
                data['best_export_sol_prd_graph_data'][4],
                data['best_export_sol_prd_graph_data'][5],
                data['best_export_sol_prd_graph_data'][6],
                data['best_export_sol_prd_graph_data'][7],
                data['best_export_sol_prd_graph_data'][8],
                data['best_export_sol_prd_graph_data'][9],
                data['best_export_sol_prd_graph_data'][10],
                data['best_export_sol_prd_graph_data'][11],
                data['best_export_sol_prd_graph_data'][12],
                data['best_export_sol_prd_graph_data'][13],
                data['best_export_sol_prd_graph_data'][14],
                data['best_export_sol_prd_graph_data'][15],
                data['best_export_sol_prd_graph_data'][16],
                data['best_export_sol_prd_graph_data'][17],
                data['best_export_sol_prd_graph_data'][18],
                data['best_export_sol_prd_graph_data'][19],
                data['best_export_sol_prd_graph_data'][20],
                data['best_export_sol_prd_graph_data'][21],
                data['best_export_sol_prd_graph_data'][22]
                ],
                color: '#F8FF34',
                zIndex: 1,
                fillOpacity: 0,
                marker: {
                    fillColor: 'yellow',
                    lineWidth: 2,
                    lineColor: 'yellow',
                    enabled: true
                }
            },
            {
                name: 'Export',
                data: [
                data['best_export_export_graph_data'][0],
                data['best_export_export_graph_data'][1],
                data['best_export_export_graph_data'][2],
                data['best_export_export_graph_data'][3],
                data['best_export_export_graph_data'][4],
                data['best_export_export_graph_data'][5],
                data['best_export_export_graph_data'][6],
                data['best_export_export_graph_data'][7],
                data['best_export_export_graph_data'][8],
                data['best_export_export_graph_data'][9],
                data['best_export_export_graph_data'][10],
                data['best_export_export_graph_data'][11],
                data['best_export_export_graph_data'][12],
                data['best_export_export_graph_data'][13],
                data['best_export_export_graph_data'][14],
                data['best_export_export_graph_data'][15],
                data['best_export_export_graph_data'][16],
                data['best_export_export_graph_data'][17],
                data['best_export_export_graph_data'][18],
                data['best_export_export_graph_data'][19],
                data['best_export_export_graph_data'][20],
                data['best_export_export_graph_data'][21],
                data['best_export_export_graph_data'][22]
                ],
                color: '#FF4B4C',
                type: 'area',
            }, {
                name: 'Offset Consumptio',
                data: [
                data['best_export_offset_graph_data'][0],
                data['best_export_offset_graph_data'][1],
                data['best_export_offset_graph_data'][2],
                data['best_export_offset_graph_data'][3],
                data['best_export_offset_graph_data'][4],
                data['best_export_offset_graph_data'][5],
                data['best_export_offset_graph_data'][6],
                data['best_export_offset_graph_data'][7],
                data['best_export_offset_graph_data'][8],
                data['best_export_offset_graph_data'][9],
                data['best_export_offset_graph_data'][10],
                data['best_export_offset_graph_data'][11],
                data['best_export_offset_graph_data'][12],
                data['best_export_offset_graph_data'][13],
                data['best_export_offset_graph_data'][14],
                data['best_export_offset_graph_data'][15],
                data['best_export_offset_graph_data'][16],
                data['best_export_offset_graph_data'][17],
                data['best_export_offset_graph_data'][18],
                data['best_export_offset_graph_data'][19],
                data['best_export_offset_graph_data'][20],
                data['best_export_offset_graph_data'][21],
                data['best_export_offset_graph_data'][22]
                ],
                color: '#81B94C',
                type: 'area',
            }
            ]


        });
}

$("#reportrange1").datepicker({
    format: 'yyyy-mm-d',
    autoclose: true,
});

$("#reportrange2").datepicker({
    format: 'yyyy-mm-d',
    autoclose: true,
});

$("#reportrange3").datepicker({
    format: 'yyyy-mm-d',
    autoclose: true,
});

$("#reportrange4").datepicker({
    format: 'yyyy-mm-d',
    autoclose: true,
});

meter_data_graph_dates = (meter_data_graph_dates != '') ? JSON.parse(meter_data_graph_dates) : '';
if (meter_data_graph_dates != '') {
    $("#reportrange1").datepicker().datepicker('setDate', meter_data_graph_dates.typical_export);
    $("#reportrange2").datepicker().datepicker('setDate', meter_data_graph_dates.worst_export);
    $("#reportrange3").datepicker().datepicker('setDate', meter_data_graph_dates.high_export);
    $("#reportrange4").datepicker().datepicker('setDate', meter_data_graph_dates.best_export);
} else {
    $("#reportrange1").datepicker().datepicker('setDate', '2019-01-28');
    $("#reportrange2").datepicker().datepicker('setDate', '2019-12-31');
    $("#reportrange3").datepicker().datepicker('setDate', '2019-10-04');
    $("#reportrange4").datepicker().datepicker('setDate', '2019-02-23');
}

$("#reportrange1,#reportrange2,#reportrange3,#reportrange4").on("change", function() {
    var data = {};
    data.proposal_id = proposal_id;
    data.graph1_date = $('#reportrange1').val();
    data.graph2_date = $('#reportrange2').val();
    data.graph3_date = $('#reportrange3').val();
    data.graph4_date = $('#reportrange4').val();
    fetch_usage_graph_data(data);
});

</script>
</body>
</html>


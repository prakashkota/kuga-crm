<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;




/**d
 *
 * @property Commercial Controller
 * @version 1.0
 */
class Commercial extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Components");
        $this->load->library("Email_Manager");
        $this->load->library("NREL");
        $this->load->model("Product_Model", "product");
        $this->load->model("User_Model", "user");
        $this->load->model("Lead_Model", "lead");
        $this->load->model("Proposal_Model", "proposal");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(4) != 'download_proposal_pdf' && $this->uri->segment(4) != 'web_view' && $this->uri->segment(4) != 'save_dataimg_to_png') {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function add()
    {

        $data            = array();
        $data['user_id'] = $user_id = $this->aauth->get_user_id();

        //Check if Lead or Proposal Exists
        $deal_ref        = $this->input->get('deal_ref');
        $type_ref        = $this->input->get('type_ref');
        $site_ref        = $this->input->get('site_ref');

        if (isset($deal_ref) && $deal_ref != '' && isset($type_ref) && $type_ref != '' && isset($site_ref) && $site_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref, FALSE, $site_ref);
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/lead/manage'));
            }
            if (!empty($lead_data) && $lead_data['location_id'] == NULL) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, deal found but no location found with referenced id.</div>');
                redirect(site_url('admin/lead/manage'));
            }

            $lead_id = $lead_data['id'];
            $customer_location_data = $this->common->fetch_row('tbl_customer_locations', '*', array('id' => $site_ref));

            $insert_data            = array();
            $insert_data['proposal_uuid'] = $proposal_uuid = $this->components->uuid();
            $insert_data['lead_id'] = $lead_id;
            $insert_data['site_id'] = $site_ref;
            $insert_data['area_id'] = $this->common->fetch_cell('tbl_solar_areas', 'areaId', array('state_id' => $customer_location_data['state_id'], 'areaStatus' => '1'));
            $this->common->insert_data('tbl_solar_proposal', $insert_data);

            redirect(site_url('admin/proposal/commercial/edit/' . $proposal_uuid));
        } else {
            redirect(site_url('admin/lead/manage'));
        }
    }

    public function edit($proposal_uuid)
    {
        $data = array();

        $data['user_id'] = $user_id = $this->aauth->get_user_id();

        $proposal_data = $this->proposal->get_solar_proposal_data(FALSE, FALSE, FALSE, $proposal_uuid);

        if (empty($proposal_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal not found with reference id.</div>');
            redirect(site_url('admin/lead/manage'));
        }

        $lead_id = $proposal_data['lead_id'];
        $site_ref = $proposal_data['site_id'];
        $lead_data = $this->lead->get_leads(FALSE, $lead_id, $site_ref);

        if (empty($lead_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, no deal found with reference id.</div>');
            redirect(site_url('admin/lead/manage'));
        }

        if (!empty($lead_data) && $lead_data['location_id'] == NULL) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Proposal creation cant be initiated, deal found but no location found with referenced id.</div>');
            redirect(site_url('admin/lead/manage'));
        }

        //Due to Some ParseJSON issue we need to unset note column
        unset($lead_data['note']);
        unset($proposal_data['additional_notes']);
        $data['lead_id'] = $lead_id = $lead_data['id'];
        $data['lead_data'] = $lead_data;
        $data['lead_uuid'] = $lead_data['uuid'];
        $data['site_id'] = $site_ref;

        $customer_location_data = $this->common->fetch_row('tbl_customer_locations', '*', array('id' => $site_ref));
        $proposal_data['additional_items']      = ($proposal_data['additional_items'] != NULL) ? json_decode($proposal_data['additional_items']) : NULL;
        $proposal_data['rate1_data']            = ($proposal_data['rate1_data'] != NULL) ? json_decode($proposal_data['rate1_data']) : NULL;
        $proposal_data['rate2_data']            = ($proposal_data['rate2_data'] != NULL) ? json_decode($proposal_data['rate2_data']) : NULL;
        $proposal_data['rate3_data']            = ($proposal_data['rate3_data'] != NULL) ? json_decode($proposal_data['rate3_data']) : NULL;
        $proposal_data['rate4_data']            = ($proposal_data['rate4_data'] != NULL) ? json_decode($proposal_data['rate4_data']) : NULL;
        $proposal_data['near_map_data'] = json_decode($proposal_data['near_map_data'], true);
        $proposal_data['pvwatts_data']  = json_decode($proposal_data['pvwatts_data'], true);
        $proposal_data['pvwatts_data_result'] = json_decode($proposal_data['pvwatts_data_result'], true);
        $proposal_data['saving_meter_calculation'] = '';
        $proposal_data['saving_graph_calculation'] = ($proposal_data['saving_graph_calculation'] != NULL) ? json_decode($proposal_data['saving_graph_calculation']) : NULL;
        $proposal_data['saving_nrel_calculation']  = '';
        $proposal_data['consumption_data'] = '';
        $proposal_data['standard_charges'] = '';
        $proposal_data['rate_weekends'] = '';
        $proposal_data['rate_weekdays'] = '';
        $proposal_data['time_of_use'] = '';
        $proposal_data['demand_charges'] = '';
        $proposal_data['system_performance'] = '';
        $proposal_data['bill_type'] = '';
        $proposal_data['flat_rates'] = '';
        $proposal_data['price_annual_increase'] = '';
        $proposal_data['system_details'] = '';
        $proposal_data['production_data'] = '';
        // $proposal_data['is_production_file'] = '';
        // $proposal_data['production_data_file'] = '';

        unset($proposal_data['meter_data_column_mapping']);
        $proposal_data['meter_data_replicated'] = ($proposal_data['meter_data_replicated'] != NULL) ? json_decode($proposal_data['meter_data_replicated']) : NULL;
        $data['proposal_data'] = $proposal_data;

        $data['proposal_data_id'] = $proposal_data['id'];
        $data['product_types']  = $this->common->fetch_where('tbl_product_types', 'type_id,type_name,parent_id', array('type_id!=' => 1, 'parent_id' => 2));
        $data['custom_profiles'] = $this->common->fetch_where('tbl_custom_profiles', '*', NULL);
        $data['postcode_rating'] = $this->common->fetch_cell('tbl_postcodes', 'rating', array('postcode' => $customer_location_data['postcode']));
        $data['panel_data'] = $this->common->fetch_where('tbl_solar_proposal_panel_data', '*', array('proposal_id' => $proposal_data['id']));
        $data['stc_modal'] = $this->load->view('proposal/form_solar_stc_modal_partial', $data, TRUE);
        $data['product_mapping'] = $this->common->fetch_where('tbl_mapping_tool_products', '*', NULL);
        $cond1 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=1 AND prd.application IN (2,3)";
        $cond2 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=2 AND prd.application IN (2,3)";
        $cond3 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=3 AND prd.application IN (2,3)";
        $data['inverters'] = $this->product->get_item_data_by_cond_by_attachment($cond1);
        $data['solar_panels'] = $this->product->get_item_data_by_cond($cond2);
        $data['battery'] = $this->product->get_item_data_by_cond($cond3);
        $data['solar_tool']  = $this->load->view('solar_tool/create_inline', $data, TRUE);
        $data['inverter_design_tool']  = $this->load->view('solar_tool/inverter_layout', $data, TRUE);
        $data['title']  = 'Commercial Solar Proposal';
        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;

        $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_data['id']));
        if (!empty($financial_summary_data)) {
            $financial_selection = $financial_summary_data['type'];
            $financial_summary_data['upfront_payment_deposit'] = ($financial_summary_data['upfront_payment_deposit'] != NULL) ? json_decode($financial_summary_data['upfront_payment_deposit']) : NULL;
            $financial_summary_data['lgc_pricing'] = ($financial_summary_data['lgc_pricing'] != NULL) ? json_decode($financial_summary_data['lgc_pricing']) : NULL;
            if ($financial_selection == "VEEC") {
                $financial_summary_data['ppa_rate'] = ($financial_summary_data['ppa_rate'] != NULL) ? json_decode($financial_summary_data['ppa_rate']) : NULL;
                $financial_summary_data['ppa_term'] = ($financial_summary_data['ppa_term'] != NULL) ? json_decode($financial_summary_data['ppa_term']) : NULL;
                $financial_summary_data['project_cost'] = ($financial_summary_data['project_cost'] != NULL) ? json_decode($financial_summary_data['project_cost']) : NULL;
                $financial_summary_data['term'] = ($financial_summary_data['term'] != NULL) ? json_decode($financial_summary_data['term']) : NULL;
                $financial_summary_data['upfront_payment_options'] = ($financial_summary_data['upfront_payment_options'] != NULL) ? json_decode($financial_summary_data['upfront_payment_options']) : NULL;
                $financial_summary_data['monthly_payment_plan'] = ($financial_summary_data['monthly_payment_plan'] != NULL) ? json_decode($financial_summary_data['monthly_payment_plan']) : NULL;
                $financial_summary_data['degradation_data'] = ($financial_summary_data['degradation_data'] != NULL) ? json_decode($financial_summary_data['degradation_data']) : NULL;
                $financial_summary_data['ppa_checkbox'] = json_decode($financial_summary_data['ppa_checkbox']);
            } else {
                $financial_summary_data['degradation_data'] = ($financial_summary_data['degradation_data'] != NULL) ? json_decode($financial_summary_data['degradation_data']) : NULL;
                $financial_summary_data['upfront_payment_options'] = ($financial_summary_data['upfront_payment_options'] != NULL) ? json_decode($financial_summary_data['upfront_payment_options']) : NULL;
                $financial_summary_data['monthly_payment_plan'] = ($financial_summary_data['monthly_payment_plan'] != NULL) ? json_decode($financial_summary_data['monthly_payment_plan']) : NULL;
                $financial_summary_data['project_cost'] = ($financial_summary_data['project_cost'] != NULL) ? json_decode($financial_summary_data['project_cost']) : NULL;
                $financial_summary_data['term'] = ($financial_summary_data['term'] != NULL) ? json_decode($financial_summary_data['term']) : NULL;
                $financial_summary_data['ppa_checkbox'] = $financial_summary_data['ppa_checkbox'];
            }
            $data['financial_summary_data'] = $financial_summary_data;
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('proposal/commercial/form', $data);
        $this->load->view('proposal/commercial/partials/meter_data_column_mapping', $data);
        $this->load->view('customer/customer_proposal_site_modal_partial');
        $this->load->view('partials/footer');
    }

    public function calculate_finance()
    {
        if (!empty($this->input->get())) {
            $stat        = FALSE;
            $term        = $this->input->get('term');
            $proposal_id = $this->input->get('proposal_id');
            $type        = $this->input->get('type');
            $amount        = $this->input->get('amount');

            // if (isset($type) && $type == 2) {
            //     $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));
            //     $amount        = $proposal_data['price_before_stc_rebate'];
            // } else {
            //     $calculated_data = $this->calculate_led_proposal_cost($proposal_id);
            //     $amount          = $calculated_data['total_investment_inGST'];
            // }

            $url     = 'https://webapi.energyease.com.au/payment?token=4fRdpuFrSu7a&applicantName=Testing&internalRef=123&financeType=rent&homeBasedBusiness=false&equipmentCode=sol&amount=' . $amount . '&term=' . $term;
            $content = @file_get_contents($url);
            if ($content === FALSE) {
                $data['status']  = 'Looks like something went wrong.';
                $data['success'] = FALSE;
            } else {
                $response                = json_decode($content, true);
                $data['success']         = TRUE;
                $data['monthly_payment'] = $response['Value'];
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function getMappingToolData()
    {
        $data = array();
        $proposal_id = $this->input->get('proposal_id');
        $mtool_data = $this->common->fetch_row('tbl_mapping_tool_data', '*', array('proposal_id' => $proposal_id, 'status' => 1));
        if (count($mtool_data)) {
            $data['mtool_data'] = $mtool_data;
            $tool_id = $mtool_data['id'];
            $panel_objects = $this->common->fetch_where('tbl_mapping_tool_objects_data', '*', array('tool_id' => $tool_id, 'object_type' => 'PANEL'));
            $data['panel_objects'] = $panel_objects;
            echo json_encode($data);
        }
    }

    public function calculate_veec_discount($proposal_id)
    {
        $data = array();
        $view_proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));
        $system_performance = json_decode($view_proposal_data['system_performance']);
        $data['PercentageExport'] = $system_performance->FinalPercantageOfExport;
        $data['AnnualSolarProduction'] = $system_performance->AnnualSolarProduction;
        $data['proposal_data'] = $view_proposal_data;

        $data['success'] = TRUE;
        echo json_encode($data);
    }

    public function save_veec_discount()
    {
        $data = array();
        $proposal_id = $this->input->post('proposal_id');
        $update_data['decay_factor'] = $this->input->post('decay_factor');
        $update_data['accuracy_factor'] = $this->input->post('accuracy_factor');
        $update_data['regional_factor'] = $this->input->post('regional_factor');
        $update_data['veec_discount'] = $this->input->post('veec_discount');
        $update_data['veec_price'] = $this->input->post('veec_price');

        $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
        if (count($financial_summary_data)) {
            $update_data['type'] = "VEEC";
            $stat2 = $this->common->update_data('tbl_solar_proposal_financial_summary', array('proposal_id' => $proposal_id), $update_data);
        } else {
            $update_data['proposal_id'] = $proposal_id;
            $update_data['type'] = "VEEC";
            $stat2 = $this->common->insert_data('tbl_solar_proposal_financial_summary', $update_data);
        }
    }


    public function pvwatts()
    {
        $pvwatts_data = json_decode($this->input->post('pvwatts'));
        $isProductionFile = $this->input->post('isProductionFile');
        $pvwatts_output = [];
        $data = array();

        $formData = $this->input->post('formData');
        $SystemDetailsArray = array(
            "system_pricing_panel" => $formData['system_pricing_panel'],
            "system_pricing_panel_id" => $formData['system_pricing_panel_id'],
            "system_pricing_panel_val" => $formData['system_pricing_panel_val'],
            "system_pricing_inverter" => $formData['system_pricing_inverter'],
            "system_pricing_inverter_id" => $formData['system_pricing_inverter_id'],
            "system_pricing_inverter_val" => $formData['system_pricing_inverter_val'],
            "system_pricing_battery" => ($formData['system_pricing_battery'] != "undefined") ? "" : $formData['system_pricing_battery'],
            "system_pricing_battery_id" => $formData['system_pricing_battery_id'],
            "system_pricing_battery_val" => $formData['system_pricing_battery_val'],
            "system_pricing_battery_size" => $formData['system_pricing_battery_size']
        );
        $update_data['system_details'] = json_encode($SystemDetailsArray);

        if (count($pvwatts_data) > 0) {
            for ($i = 0; $i < count($pvwatts_data); $i++) {
                $pv_method = 'dataset=intl&system_capacity=' . $pvwatts_data[$i]->pv_system_size . '&module_type=' . $pvwatts_data[$i]->pv_module_type . '&losses=' . $pvwatts_data[$i]->pv_system_losses . '&array_type=' . $pvwatts_data[$i]->pv_array_type . '&tilt=' . $pvwatts_data[$i]->pv_tilt . '&azimuth=' . $pvwatts_data[$i]->pv_azimuth . '&lat=' . $pvwatts_data[$i]->pv_lat . '&lon=' . $pvwatts_data[$i]->pv_lng . '&inv_eff=' . $pvwatts_data[$i]->pv_inverter_efficiency . '&gcr=' . $pvwatts_data[$i]->pv_ground_coverage . '&dc_ac_ratio=' . $pvwatts_data[$i]->pv_ac_dc_ratio . '&timeframe=hourly';
                $data[$i] = $this->nrel->pvwatts($pv_method);
            }

            $ac_annual = 0;
            $ac_output = 0;
            $ac_output_array = [];
            $ac_monthly = 0;
            $ac_monthly_array = [];
            for ($i = 0; $i < count($data); $i++) {
                $json_data = json_decode($data[$i]);
                $ac_annual = $ac_annual + $json_data->outputs->ac_annual;

                for ($j = 0; $j < count($json_data->outputs->ac); $j++) {
                    $ac_output = (empty($ac_output_array[$j])) ? 0 : $ac_output_array[$j];
                    $ac_output_array[$j] = $ac_output + $json_data->outputs->ac[$j];
                }

                for ($j = 0; $j < count($json_data->outputs->ac_monthly); $j++) {
                    $ac_monthly = (empty($ac_monthly_array[$j])) ? 0 : $ac_monthly_array[$j];
                    $ac_monthly_array[$j] = $ac_monthly + $json_data->outputs->ac_monthly[$j];
                }
            }
            $pvwatts_output["outputs"] = array(
                "ac_annual" => $ac_annual,
                "ac_monthly" => $ac_monthly_array,
                "ac" => $ac_output_array,
            );
            $output_data = json_encode($pvwatts_output);
        } else {
            $output_data = $this->nrel->pvwatts($this->input->post('method'));
        }

        $update_data['pvwatts_data_result'] = $output_data;
        $update_data['pvwatts_data'] = $this->input->post('pvwatts');
        $update_data['is_production_file'] = 0;
        $update_data['production_data_file'] = null;
        $update_data['total_system_size'] = $this->input->post('total_system_size');
        if ($isProductionFile == 2) {
            $update_data['is_production_file'] = $isProductionFile;
        }
        $this->common->update_data('tbl_solar_proposal', array('id' => $this->input->post('proposal_data_id')), $update_data);
        echo $output_data;
        // echo json_encode($update_data);
        die;

        // echo json_encode($pvwatts_data);
    }

    public function fetch_financial_summary_data($proposal_id)
    {
        $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
        echo json_encode($financial_summary_data);
    }


    public function solar_proposal_view($proposal_id)
    {
        $data = array();

        // Data for Page 8,9,10
        $view_proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

        $user_id        = $this->aauth->get_user_id();
        $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
        $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
        $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
        $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);

        $data['lead_data']    = $lead_data;

        $data['proposal_data'] = $view_proposal_data;
        $data['proposal_finance_data'] = $proposal_finance_data         = $this->common->fetch_row('tbl_proposal_finance', '*', array('proposal_id' => $proposal_id, 'proposal_type' => 2));
        $year = 0;
        if ($proposal_finance_data['term'] == 12) {
            $year = 1;
        }
        if ($proposal_finance_data['term'] == 24) {
            $year = 2;
        }
        if ($proposal_finance_data['term'] == 36) {
            $year = 3;
        }
        if ($proposal_finance_data['term'] == 48) {
            $year = 4;
        }
        if ($proposal_finance_data['term'] == 60) {
            $year = 5;
        }
        if ($proposal_finance_data['term'] == 72) {
            $year = 6;
        }
        if ($proposal_finance_data['term'] == 84) {
            $year = 7;
        }
        $data['year'] = $year;

        $data['saving_meter_calculation'] = $view_proposal_data['saving_meter_calculation'];
        $data['saving_nrel_calculation'] = $view_proposal_data['saving_nrel_calculation'];
        $data['saving_graph_calculation'] = $view_proposal_data['saving_graph_calculation'];
        $data['system_performance'] = $view_proposal_data['system_performance'];
        $data['pvwatts_data_result'] = $view_proposal_data['pvwatts_data_result'];
        $data['time_of_use'] = $view_proposal_data['time_of_use'];
        $data['inverter_design'] = $this->common->fetch_row(' tbl_inverter_design_tool_data', '*', array('proposal_id' => $proposal_id, 'status' => 1));
        $this->load->view('pdf_files/solar_proposal_pdf_new/web/main', $data);
    }

    public function view_proposal($proposal_id)
    {

        $data = array();

        // Data for Page 8,9,10
        $view_proposal_data = $this->proposal->get_solar_proposal_data(FALSE, FALSE, $proposal_id, FALSE);
        $data['mapping_tool'] = $mapping_tool = $this->common->fetch_row('tbl_mapping_tool_data', '*', array('proposal_id' => $proposal_id));

        if ((int)$view_proposal_data['is_production_file'] != 1 && !empty($mapping_tool)) {
            $data['mappingPanelObjects'] = $mappingPanelObjects = $this->common->fetch_where('tbl_mapping_tool_objects_data', '*', array('tool_id' => $mapping_tool['id'], 'object_type' => "PANEL"));
        } else {
            $data['mappingPanelObjects'] = [];
        }

        $user_id        = $this->aauth->get_user_id();
        $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
        $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
        $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
        $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);
        $data['lead_data']    = $lead_data;

        $data['proposal_data'] = $view_proposal_data;
        $data['proposal_finance_data'] = $proposal_finance_data         = $this->common->fetch_row('tbl_proposal_finance', '*', array('proposal_id' => $proposal_id, 'proposal_type' => 2));

        $data['financial_summary_data'] = $financial_summary_data         = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
        // $data['degradation_data'] = $this->calculate_data_with_degradation_factor($proposal_id, 1);
        // $data['degradation_data'] = $this->calculate_data_with_degradation_factor($proposal_id, 1);
        $Cashflow = 0;
        if (count($proposal_finance_data)) {
            // if ($proposal_finance_data['term'] == 12) {
            //     $year = 1;
            // }
            // if ($proposal_finance_data['term'] == 24) {
            //     $year = 2;
            // }
            // if ($proposal_finance_data['term'] == 36) {
            //     $year = 3;
            // }
            // if ($proposal_finance_data['term'] == 48) {
            //     $year = 4;
            // }
            // if ($proposal_finance_data['term'] == 60) {
            //     $year = 5;
            // }
            // if ($proposal_finance_data['term'] == 72) {
            //     $year = 6;
            // }
            // if ($proposal_finance_data['term'] == 84) {
            //     $year = 7;
            // }

            $year = $proposal_finance_data['term'] / 12;


            $monthlyPaymentPlanSolar = 0;
            if (isset($proposal_finance_data['monthly_payment_plan'])) {
                $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'] * 12;
            } else {
                $proposal_finance_data['monthly_payment_plan'] = 0;
                $proposal_finance_data['term'] = 0;
            }
            $Cashflow = $monthlyPaymentPlanSolar;
        }

        // Financial Summery
        $ppa_year = 0;
        if (count($financial_summary_data)) {
            if ($financial_summary_data['ppa_term'] == 12) {
                $ppa_year = 1;
            }
            if ($financial_summary_data['ppa_term'] == 24) {
                $ppa_year = 2;
            }
            if ($financial_summary_data['ppa_term'] == 36) {
                $ppa_year = 3;
            }
            if ($financial_summary_data['ppa_term'] == 48) {
                $ppa_year = 4;
            }
            if ($financial_summary_data['ppa_term'] == 60) {
                $ppa_year = 5;
            }
            if ($financial_summary_data['ppa_term'] == 72) {
                $ppa_year = 6;
            }
            if ($financial_summary_data['ppa_term'] == 84) {
                $ppa_year = 7;
            }
        }


        $data['year'] = $year;
        $data['ppa_year'] = $ppa_year;
        $data['Cashflow'] = $Cashflow;
        $data['product_data'] = array();
        $data['product_data'] = $product_data         = $this->product->get_solar_proposal_products($proposal_id);

        //Panel ,Inverter Data
        $panel    = array();
        $inverter = array();
        foreach ($product_data as $key => $value) {
            if ($value['type_id'] == 9) {
                $panel = $value;
            } else if ($value['type_id'] == 11 || $value['type_id'] == 12 || $value['type_id'] == 13 || $value['type_id'] == 14) {
                $inverter = $value;
            }
        }
        $cond1 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=1";
        $cond2 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=2";
        $cond3 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=3";
        $data['inverters'] = $this->product->get_item_data_by_cond($cond1);
        $data['solar_panels'] = $this->product->get_item_data_by_cond($cond2);
        $data['battery'] = $this->product->get_item_data_by_cond($cond3);

        $data['panel']                = $panel;
        $data['inverter']             = $inverter;

        $data['saving_meter_calculation'] = $view_proposal_data['saving_meter_calculation'];
        $data['saving_nrel_calculation'] = $view_proposal_data['saving_nrel_calculation'];
        $data['saving_graph_calculation'] = $view_proposal_data['saving_graph_calculation'];
        $data['system_performance'] = $view_proposal_data['system_performance'];
        $data['pvwatts_data_result'] = $view_proposal_data['pvwatts_data_result'];
        $data['time_of_use'] = $view_proposal_data['time_of_use'];
        $data['inverter_design'] = $this->common->fetch_row(' tbl_inverter_design_tool_data', '*', array('proposal_id' => $proposal_id, 'status' => 1));

        if (count($financial_summary_data) && $financial_summary_data['type'] === 'VEEC') {
            $this->load->view('pdf_files/solar_proposal_pdf_new/web/main_veec', $data);
        } else {
            $this->load->view('pdf_files/solar_proposal_pdf_new/web/main', $data);
        }
        $this->load->view('pdf_files/solar_proposal_pdf_new/web/send_proposal_to_customer_modal', $data);
    }

    public function saving_solar_system_details()
    {
        $SystemDetailsArray = array();
        if (!empty($this->input->post())) {
            $ProposalDataId = $this->input->post('proposal_id');

            $SystemDetailsArray = array(
                "system_pricing_panel" => $this->input->post('system_pricing_panel'),
                "system_pricing_panel_id" => $this->input->post('system_pricing_panel_id'),
                "system_pricing_panel_val" => $this->input->post('system_pricing_panel_val'),
                "system_pricing_inverter" => $this->input->post('system_pricing_inverter'),
                "system_pricing_inverter_id" => $this->input->post('system_pricing_inverter_id'),
                "system_pricing_inverter_val" => $this->input->post('system_pricing_inverter_val'),
                "system_pricing_battery" => ($this->input->post('system_pricing_battery') != "undefined") ? "" : $this->input->post('system_pricing_battery'),
                "system_pricing_battery_id" => $this->input->post('system_pricing_battery_id'),
                "system_pricing_battery_val" => $this->input->post('system_pricing_battery_val'),
                "system_pricing_battery_size" => $this->input->post('system_pricing_battery_size')
            );
            $UpdateSolarProposalData['system_details'] = json_encode($SystemDetailsArray);


            $stat = $this->common->update_data('tbl_solar_proposal', array('id' => $ProposalDataId), $UpdateSolarProposalData);

            if ($stat) {
                $data['status']  = 'Panel Design data saved successfully';
                $data['success'] = TRUE;
            } else {
                $data['status']  = 'Looks like nothing to update.';
                $data['success'] = FALSE;
            }
        } else {
            $data['status']  = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
    }

    public function save_solar_proposal_data()
    {

        $UpdateSolarProposalData = [];
        $ProposalDataId = $this->input->post('proposal_data_id');
        $MeterUploadStatus = $this->input->post('meter_upload_status');
        $IsBillType = $this->input->post('is_bill_type');

        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $filename                          = $this->input->post('meterDataFileURL');

        if (isset($_FILES['meterDataFileURL']['name']) && in_array($_FILES['meterDataFileURL']['type'], $file_mimes)) {
            $config['upload_path'] = "./assets/uploads/meter_data_files/";
            $config['allowed_types'] = 'csv|xls|xlsx';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload("meterDataFileURL")) {
                $data['upload'] = $this->upload->data();
                $data['upload'] = $data['upload']['file_name'];

                $UpdateSolarProposalData['meter_data_file'] = $data['upload'];
                $UpdateSolarProposalData['is_meter_data'] = 1;
            } else {
                $data['upload_errors'] = $this->upload->display_errors();
            }
        } else {
            die;
        }

        // Saving Data
        // $update_data['saving_meter_calculation'] = json_encode($saving_meter_calculation);
        // $update_data['saving_nrel_calculation'] = json_encode($saving_nrel_calculation);
        // $update_data['saving_graph_calculation'] = json_encode($saving_graph_calculation);
        $update_data['solar_cost_per_kwh_peak'] = $this->input->post('meter_data_peak');
        $update_data['solar_cost_per_kwh_shoulder'] = $this->input->post('meter_data_shoulder');
        $update_data['solar_cost_per_kwh_off_peak'] = $this->input->post('meter_data_off_peak');
        $update_data['rate'] = $this->input->post('flat_bill_type_rate');
        $update_data['bill_type'] = $this->input->post('is_bill_type');
        // $update_data['standard_charges'] = json_encode($standard_charges);
        // $update_data['time_of_use'] = json_encode($time_of_use);
        $update_data['meter_data_file'] = $data['upload'];
        // $update_data['consumption_data'] = json_encode(array(
        //     "consumption_data" => $data2['consumption_data'],
        //     "consumption_date" => $data2['consumption_date'],
        //     "consumption_time" => $data2['consumption_time']
        // ));

        $this->common->update_data('tbl_solar_proposal', array('id' => $ProposalDataId), $UpdateSolarProposalData);
    }


    public function solar_pdf_download($proposal_id)
    {
        $askingFinancialInformation = isset($_GET['api_invoker']) ? true : false;

        try {
            //Check if downloading user is same as creating user
            $user_id        = $this->aauth->get_user_id();
            $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
            $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
            $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
            $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);
            if (!$is_access) {
                //$this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access Not appropriate User to download the proposal.</div>');
                //redirect($this->redirect_url);
                //exit();
            }
            //Check for validation
            $flag          = true;
            $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

            if ($proposal_data['monthly_electricity_bill'] == NULL || $proposal_data['monthly_electricity_bill'] == 0 || $proposal_data['monthly_electricity_bill'] == '') {
                $flag   = false;
                $status = 'Monthly Electricity Bill Cannot be Zero or left blank';
            } else if ($proposal_data['rate'] == NULL || $proposal_data['rate'] == '') {
                $flag   = false;
                $status = 'Electricity Rate Cannot be left blank';
            } else if ($proposal_data['rate'] == NULL || $proposal_data['rate'] == '') {
                $flag   = false;
                $status = 'Electricity Rate Cannot be left blank';
            } else if ($proposal_data['daily_usage_charge'] == NULL || $proposal_data['daily_usage_charge'] == '') {
                $flag   = false;
                $status = 'Daily Usage Charge Cannot be left blank';
            } else if ($proposal_data['feed_in_tariff'] == NULL || $proposal_data['feed_in_tariff'] == '') {
                $flag   = false;
                $status = 'Feed in Tariff Cannot be left blank';
            } else if ((trim($proposal_data['near_map_data']) == NULL || trim($proposal_data['near_map_data']) == '') && $proposal_data['nearmap_toggle'] == 1) {
                $flag   = false;
                $status = 'Panel setup is enabled using nearmap tool, Please draw panels on nearmap tool to download proposal';
            }

            if ($this->input->is_ajax_request() && !$flag && !$askingFinancialInformation) {
                echo json_encode(array(
                    'success' => FALSE,
                    'status'  => $status,
                ));
                die;
            }
            if ($this->input->is_ajax_request() && $flag && !$askingFinancialInformation) {
                echo json_encode(array(
                    'success' => TRUE
                ));
                die;
            }

            if ($askingFinancialInformation && $flag == false) {
                echo json_encode(array(
                    'status'  => $status,
                    'success' => FALSE
                ));
                die;
            }
            $data                 = array();
            $data                 = $this->calculate_solar_proposal_cost($proposal_id);
            $data['lead_data']    = $lead_data;
            $data['product_data'] = array();
            $data['product_data'] = $product_data         = $this->product->get_solar_proposal_products($proposal_id);

            $data['baseurl']            = $baseurl                    = $this->config->item('live_url');
            //$data['baseurl'] = $baseurl = site_url();
            $data['static_images_path'] = $baseurl . 'assets/pdf_images/';
            $data['image_path']         = $baseurl . 'assets/uploads/proposal_files/';

            //Panel ,Inverter Data
            $panel    = array();
            $inverter = array();
            foreach ($product_data as $key => $value) {
                if ($value['type_id'] == 9) {
                    $panel = $value;
                } else if ($value['type_id'] == 11 || $value['type_id'] == 12 || $value['type_id'] == 13 || $value['type_id'] == 14) {
                    $inverter = $value;
                }
            }
            $data['panel']                = $panel;
            $data['inverter']             = $inverter;
            $data['footer_static_number'] = '13 58 42';
            $data['solar_energy_rate'] = 0;
            if ($data['AverageDailyProduction'] > 0) {
                $data['solar_energy_rate'] = number_format((($data['proposal_data']['price_before_stc_rebate'] / 10) / ($data['AverageDailyProduction'] * 12)), '2', '.', '') . 'c/kWh';
            }
            if ($askingFinancialInformation) {
                echo json_encode(array(
                    'data' => $data,
                    'success' => TRUE
                ));
                die;
            }

            $view = $this->input->get('view');
            if (isset($view) && $view == 'html') {
                $html  = $this->load->view('pdf_files/solar/chart_images', $data);
            } else {
                $html                         = $this->load->view('pdf_files/solar/page1', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page2', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page3', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page4', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page5', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page6', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page7', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page8', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page9', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page10', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page11', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page12', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page13', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page14', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page15', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page16', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page17', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page18', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page19', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page20', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/page21', $data, TRUE);
                $html                         .= $this->load->view('pdf_files/solar/thank_page', $data, TRUE);

                if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
                    //echo '<pre>';print_r($data);die;
                    //echo $html;die;
                }
                //echo $html;die;
                $customer_name = $lead_data['first_name'] . "_" . $lead_data['last_name'];
                $filename      = "KugaElectrical-SOLAR_Quote_" . $customer_name . "-" . date('Y-m-d');

                $filename    = str_replace(array('.', ',', ' '), '_', $filename);
                $h           = '';
                $f           = '';
                $pdf_options = array(
                    "source_type" => 'html',
                    "source"      => $html,
                    "action"      => 'download',
                    //"save_directory" => '',
                    "file_name"   => $filename . '.pdf',
                    "page_size"   => 'A4',
                    "margin"      => array("right" => "0", "left" => '0', "top" => "0", "bottom" => "0"),
                    "header"      => $h,
                    "footer"      => $f
                );
                $this->pdf->phptopdf($pdf_options);
            }
        } catch (Exception $e) {
            echo 'Looks like some error occured while downloading the proposal. Please contact support.';
            die;
        }
    }

    public function degradation_excel_export()
    {
        // $filename = "DownloadDegradationReport";
        // $table    = $_POST['table'];

        // $config['upload_path'] = "./assets/uploads/production_data_files/";
        $temporary_html_file = './assets/uploads/degradation_excel/' . time() . '.html';

        file_put_contents($temporary_html_file, $_POST["file_content"]);

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Html');
        $spreadsheet = $reader->load($temporary_html_file);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $filename =  time() . '.xlsx';
        $fileurl = './assets/uploads/degradation_excel/' . $filename;
        $writer->save($fileurl);

        header('Content-Type: application/x-www-form-urlencoded');
        header("Content-type: application/file");
        // header('Content-Transfer-Encoding: Binary');
        // header("Content-disposition: attachment; filename=\"" . $filename . "\"");
        // header('Content-disposition: attachment; filename="' . basename($fileurl) . '"');
        // readfile($file_name);

        unlink($temporary_html_file);
        // unlink($filename);
        echo $fileurl;
        die();
    }

    public function validate_proposal_data()
    {
        $data = array();
        $data['success'] = false;
        echo json_encode($data);
        die;
    }


    public function upload_production_data($proposal_id)
    {
        $data = array();
        $data_file = array();
        $SumAnnualAc = 0;
        $production_data = [];
        $FinalAcData = [];
        $AcData = [];
        $AcMonthlyData = [];
        $FinalAcMonthlyData = [];

        $SystemDetailsArray = array(
            "system_pricing_panel" => $this->input->post('system_pricing_panel'),
            "system_pricing_panel_id" => $this->input->post('system_pricing_panel_id'),
            "system_pricing_panel_val" => $this->input->post('system_pricing_panel_val'),
            "system_pricing_inverter" => $this->input->post('system_pricing_inverter'),
            "system_pricing_inverter_id" => $this->input->post('system_pricing_inverter_id'),
            "system_pricing_inverter_val" => $this->input->post('system_pricing_inverter_val'),
            "system_pricing_battery" => ($this->input->post('system_pricing_battery') != "undefined") ? "" : $this->input->post('system_pricing_battery'),
            "system_pricing_battery_id" => $this->input->post('system_pricing_battery_id'),
            "system_pricing_battery_val" => $this->input->post('system_pricing_battery_val'),
            "system_pricing_battery_size" => $this->input->post('system_pricing_battery_size')
        );
        $update_data['system_details'] = json_encode($SystemDetailsArray);


        //  = $this->input->post('proposal_id');
        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');


        $filename                          = $this->input->post('productionDataFileURL');
        if (isset($_FILES['productionDataFileURL']['name']) && in_array($_FILES['productionDataFileURL']['type'], $file_mimes)) {
            $arr_file = explode('.', $_FILES['productionDataFileURL']['name']);
            $extension = end($arr_file);
            if ('csv' == $extension) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif ('xls' == $extension) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
            $spreadsheet = $reader->load($_FILES['productionDataFileURL']['tmp_name']);

            $config['upload_path'] = "./assets/uploads/production_data_files/";
            $config['allowed_types'] = 'csv|xls|xlsx';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload("productionDataFileURL")) {
                $data_file['upload'] = $this->upload->data();
                $update_data['production_data_file'] = $data_file['upload']['file_name'];
                $update_data['is_production_file'] = 1;
            } else {
                $data['upload_errors'] = $this->upload->display_errors();
            }
            $final_data = $this->read_production_data_file($spreadsheet->getActiveSheet(), false);


            for ($i = 0; $i < count($final_data); $i++) {
                // $data['production_date'][$i] = $final_data[$i][0];
                $production_data[$i] = $final_data[$i][1];
                $production_time[$i] = explode(" ", $final_data[$i][0])[1];
                // $data['final_data'][$i][0]    = $final_data[$i][0];
                // $data['final_data'][$i][1]    = $final_data[$i][1];

                $timestamp_peak = strtotime($final_data[$i][0]);
                $month_int = date('m', $timestamp_peak);
                $m_month = date("M", $timestamp_peak);
                $n_month = date("n", $timestamp_peak);
                $m_month     = strtolower($m_month);
                $m_year = date("Y", $timestamp_peak);

                $AcMonthlyData[$n_month][] = $production_data[$i];

                $no_of_days_24hr = $this->components->days_in_month($month_int, $m_year);
                if ($final_data[$i][0] == $no_of_days_24hr . '-' . $month_int . '-' . $m_year . ' 23:00:00') {
                    $FinalAcMonthlyData[] = array_sum($AcMonthlyData[$n_month]);
                }
            }

            $data['outputs'] = array(
                "final_data" => $final_data,
                "ac" => $production_data,
                "ac_monthly" => $FinalAcMonthlyData,
                "ac_annual" => array_sum($production_data)
            );
            $stat = true;
        } else {

            $IsProductionFile        = $this->common->fetch_cell('tbl_solar_proposal', 'is_production_file', array('id' =>  $proposal_id));
            $SolarOuputData = $this->common->fetch_cell('tbl_solar_proposal', 'pvwatts_data_result', array('id' =>  $proposal_id));
            $SolarOuput = ($SolarOuputData != null) ? json_decode($SolarOuputData) : '';
            $data['outputs'] = $SolarOuput->outputs;
            if ($IsProductionFile == 1) {
                $stat = true;
            } else {
                $stat = false;
            }
        }

        if ($stat) {
            $data['status']       = 'Solar Production saved successfully';
            $data['success']      = TRUE;

            $pvwatts[] = array(
                "pv_system_size" => $this->input->post('pv_system_size'),
                "pv_tilt" => $this->input->post('pv_tilt'),
                "pv_azimuth" => $this->input->post('pv_azimuth'),
                "pv_lat" => $this->input->post('pv_lat'),
                "pv_lng" => $this->input->post('pv_lng'),
                "pv_module_type" => $this->input->post('pv_module_type'),
                "pv_array_type" => $this->input->post('pv_array_type'),
                "pv_system_losses" => $this->input->post('pv_system_losses'),
                "pv_ac_dc_ratio" => $this->input->post('pv_ac_dc_ratio'),
                "pv_degradation_factor" => $this->input->post('pv_degradation_factor'),
                "pv_inverter_efficiency" => $this->input->post('pv_inverter_efficiency'),
                "pv_ground_coverage" => $this->input->post('pv_ground_coverage')
            );

            $update_data['pvwatts_data'] = $this->input->post('pvwatts');
            $update_data['total_system_size'] = $this->input->post('pv_system_size');
            $update_data['pvwatts_data_result'] = json_encode($data);
            $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_id), $update_data);
        } else {
            $data['status']  = 'Please provide production data.';
            $data['success'] = FALSE;
        }

        echo json_encode($data);
    }




    public function read_production_data_file($datatt)
    {

        $worksheet = $datatt;
        // print_r($worksheet);
        // die;

        $i = $j = 0;
        $date_time_column = $grid_power =  '';
        $is_sheet_column_datetime = false;
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];


            foreach ($cellIterator as  $cell) {
                if ($i ==  0) {
                    //Detect Datetime and Kwh columns
                    if ($date_time_column == '') {
                        if (strtolower($cell->getValue()) == 'timestamp') {
                            $date_time_column = $j;
                        }
                    }

                    if ($grid_power == '') {


                        if (strtolower($cell->getValue()) == 'grid_power') {
                            $grid_power = $j;
                        }
                    }
                    $j++;
                }
                $cells[] = $cell->getValue();
            }


            //If Column not present return false;
            if (($date_time_column == '' || $grid_power == '') && $i == 0 && $date_time_column != 0) {
                echo json_encode(array(
                    'status' => 'Either Date/Time or kWh column is not found on the uploaded file.',
                    'success' => FALSE
                ));
                die;
            }


            //Maintain 2 column table
            //Also check if date is not having dots
            if ($i > 0) {
                $dot_position = strpos($cells[$date_time_column], '.');
                $datetime = ($dot_position != FALSE && $dot_position != 5) ? str_replace('.', '-', $cells[$date_time_column]) : $cells[$date_time_column];
                if (is_numeric($datetime)) {
                    $is_sheet_column_datetime = TRUE;
                }

                $new_datetime = [];
                if ($is_sheet_column_datetime) {
                    $new_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datetime);
                }
                $new_datetime = !empty($new_datetime) ? $new_datetime->format("d-m-Y H:i:s") : $datetime;
                $rows[($i - 1)][0] = $new_datetime;
                $rows[($i - 1)][1] = ($cells[$grid_power] != null) ? $cells[$grid_power] / 1000 : 0;
            }
            $i++;
        }

        // print_r($rows);
        // die;
        return $rows;
    }

    public function remove_meter_data_file()
    {
        $ProposalId = $this->input->post('proposal_id');
        $update_data['meter_data_file'] = NULL;
        $update_data['is_meter_data'] = 0;
        $update_data['meter_data_column_mapping'] = NULL;
        $this->common->update_data('tbl_solar_proposal', array('id' => $ProposalId), $update_data);
        $data['status']  = 'Meter data file removed successfully.';
        $data['success'] = FALSE;
        echo json_encode($data);
    }

    public function upload_meter_data()
    {
        $IsMeterData = false;
        $MeterUploadStatus = $this->input->post('meter_upload_status');
        $IsMeterDataFile = $this->input->post('is_meter_data');
        $IsBillType = $this->input->post('is_bill_type');
        $IsDemandCharge = $this->input->post('is_demandCharge');
        $ProposalDataId = $this->input->post('proposal_data_id');

        $FinalElectricityBillWithoutMeterData = 0;
        $AnnualSolarProduction = 0;
        $FinalBillAfterSolar = 0;
        $FinalEnergyProvidedBySolar = 0;
        $FinalPercantageEnergyProvidedBySolar = 0;
        $FinalPercantageEnergyProvidedBySolar = 0;
        $FinalPercantageOfExport = 0;
        $FinalTotalLoadMeterData = 0;
        $FinalLoadChargesWithRates = 0;
        $FinalReducesCarbon = 0;
        $IsKvaData = 0;


        $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' =>  $this->input->post('proposal_data_id')));
        $meter_data_column_mapping        = $this->common->fetch_cell('tbl_solar_proposal', 'meter_data_column_mapping', array('id' =>  $this->input->post('proposal_data_id')));
        $IsProductionFile        = $this->common->fetch_cell('tbl_solar_proposal', 'is_production_file', array('id' =>  $this->input->post('proposal_data_id')));
        $IsMeterDataFile        = $this->common->fetch_cell('tbl_solar_proposal', 'is_meter_data', array('id' =>  $this->input->post('proposal_data_id')));
        $LoadedMeterDataFile        = $this->common->fetch_cell('tbl_solar_proposal', 'meter_data_file', array('id' =>  $this->input->post('proposal_data_id')));
        $FinacialSummeryStatus        = $this->common->fetch_cell('tbl_solar_proposal_financial_summary', 'degradation_data', array('proposal_id' =>  $this->input->post('proposal_data_id')));
        $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);


        if ($this->input->post('predefined_choose_file') != null) {
            $filetype = "";
            $filename = $this->input->post('predefined_choose_file');
            $ext = explode('.', $filename);
            $file = FCPATH . '/assets/uploads/meter_data_files/' . $filename;

            switch (strtolower($ext[1])) {
                case 'xls':
                    $filetype = 'Xls';
                    break;
                case 'xlsx':
                    $filetype = 'Xlsx';
                    break;
                case 'csv':
                    $filetype = 'Csv';
                    break;
            }

            if ($filetype == '') {
                echo json_encode(array(
                    'status' => 'Meter Data File Extension not supported.',
                    'success' => FALSE
                ));
                die;
            }

            $update_data['meter_data_file'] = $filename;
            $update_data['is_meter_data'] = 1;
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($filetype);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file);
            $worksheet = $spreadsheet->getActiveSheet();

            // Checking if data has kva or not
            if ($IsDemandCharge == 1) {
                foreach ($worksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(FALSE);

                    foreach ($cellIterator as  $cell) {
                        if (strtolower($cell->getValue()) == 'kva') {
                            $IsKvaData = 1;
                        }
                    }
                }

                if ($IsKvaData == 0) {
                    $output['success'] = FALSE;
                    $output['status'] = "Kva data is missing";
                    echo json_encode($output);
                    return false;
                    die;
                }
            }

            $final_data = $this->read_meter_data_file($spreadsheet->getActiveSheet(), $IsDemandCharge, $meter_data_column_mapping);
        } else {
            if ($IsMeterDataFile == "0") {
                $output['success'] = FALSE;
                $output['status'] = "Meter data file is missing";
                echo json_encode($output);
                return false;
                die;
            }
            // Getting file from the path
            $filetype = "";
            $filename = $LoadedMeterDataFile;
            $ext = explode('.', $filename);
            $file = FCPATH . '/assets/uploads/meter_data_files/' . $filename;
            switch (strtolower($ext[1])) {
                case 'xls':
                    $filetype = 'Xls';
                    break;
                case 'xlsx':
                    $filetype = 'Xlsx';
                    break;
                case 'csv':
                    $filetype = 'Csv';
                    break;
            }

            if ($filetype == '') {
                echo json_encode(array(
                    'status' => 'Meter Data File Extension not supported.',
                    'success' => FALSE
                ));
                die;
            }

            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($filetype);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file);
            $worksheet = $spreadsheet->getActiveSheet();
            // Checking if data has kva or not
            if ($IsDemandCharge == 1) {
                foreach ($worksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(FALSE);

                    foreach ($cellIterator as  $cell) {
                        if (strtolower($cell->getValue()) == 'kva') {
                            $IsKvaData = 1;
                        }
                    }
                }

                if ($IsKvaData == 0) {
                    $output['success'] = FALSE;
                    $output['status'] = "Kva data is missing";
                    echo json_encode($output);
                    return false;
                    die;
                }
            }
            $final_data = $this->read_meter_data_file($spreadsheet->getActiveSheet(), $IsDemandCharge, $meter_data_column_mapping);
        }


        $final_data           = $this->handle_two_columns_meter_data([], $final_data, $IsDemandCharge);
        $full_year_meter_data = $final_data['data'];



        // handle_two_columns_meter_data
        for ($i = 0; $i < count($full_year_meter_data); $i++) {
            $data['consumption_date'][$i] = $full_year_meter_data[$i][0];
            $data['consumption_data'][$i] = $full_year_meter_data[$i][1];
            if ($IsDemandCharge == "1") {
                $data['demand_data'][$i] = $full_year_meter_data[$i][2];
            }
            $data['consumption_time'][$i] = explode(" ", $full_year_meter_data[$i][0])[1];
            $data['final_data'][$i][0]    = $full_year_meter_data[$i][0];
            $data['final_data'][$i][1]    = $full_year_meter_data[$i][1];
            // $data['nrel_data'][$i] =
            $month                        = (int) date('m', strtotime($full_year_meter_data[$i][0]));
            if ($month <= 3) {
                $data['consumption_keys_1_3'][] = $i;
            } else if ($month > 3 && $month <= 6) {
                $data['consumption_keys_4_6'][] = $i;
            } else if ($month > 6 && $month <= 9) {
                $data['consumption_keys_7_9'][] = $i;
            } else if ($month > 9 && $month <= 12) {
                $data['consumption_keys_10_12'][] = $i;
            }
        }

        $PvWattsDataResult = $this->common->fetch_row('tbl_solar_proposal', 'pvwatts_data_result', array('id' => $this->input->post('proposal_data_id')));

        $nrel_data  = $this->read_solar_production_data($data, $PvWattsDataResult, $IsProductionFile);
        $update_data['production_data'] = json_encode($nrel_data);

        $update_data['bill_type'] = $this->input->post('is_bill_type');
        $update_data['meter_data_type'] = $MeterUploadStatus;

        // begins:: time of use popup values
        $time_of_use_start_time = [];
        $time_of_use_end_time = [];
        $time_of_use_weekdays = [];
        $time_of_use_sat = [];
        $time_of_use_sun = [];

        for ($i = 1; $i <= 24; $i++) {
            $time_of_use_start_time[] = $this->input->post('time_of_use_start_time' . $i);
            $time_of_use_end_time[] = $this->input->post('time_of_use_end_time' . $i);
            $time_of_use_weekdays[] = $this->input->post('time_of_use_weekdays' . $i);
            $time_of_use_sat[] = $this->input->post('time_of_use_sat' . $i);
            $time_of_use_sun[] = $this->input->post('time_of_use_sun' . $i);
        }

        $time_of_use = array(
            "start_time" => $time_of_use_start_time,
            "end_time" => $time_of_use_end_time,
            "weekdays" => $time_of_use_weekdays,
            "sat" => $time_of_use_sat,
            "sun" => $time_of_use_sun,
            "option" => $this->input->post('option')
        );
        $update_data['time_of_use'] = json_encode($time_of_use);
        // end:: time of use popup values

        $update_data['solar_cost_per_kwh_peak'] = $this->input->post('meter_data_peak');
        $update_data['solar_cost_per_kwh_shoulder'] = $this->input->post('meter_data_shoulder');
        $update_data['solar_cost_per_kwh_off_peak'] = $this->input->post('meter_data_off_peak');

        if (!empty($this->input->post('flat_bill_type_rate'))) {
            $flatRates = array(
                "charge" => $this->input->post('flat_bill_type_rate'),
                "quantity" => $this->input->post('flat_quantity'),
                "energy_rate" => $this->input->post('flat_energy_rate'),
                "quantity_type" => $this->input->post('quantity_type'),
                "billing_month" => $this->input->post('billing_month'),
            );
        }


        if ($MeterUploadStatus == 1) {

            // Time Of Use
            if ($IsBillType == "timeofUse") {
                // $form_inputs['meter_data_peak_kwh'] = $this->input->post('meter_data_peak_kwh');
                // $form_inputs['meter_data_shoulder_kwh'] = $this->input->post('meter_data_shoulder_kwh');
                // $form_inputs['meter_data_off_peak_kwh'] = $this->input->post('meter_data_off_peak_kwh');

                // $TariffPeakTotal = $this->input->post('meter_data_peak') * $this->input->post('meter_data_peak_kwh');
                // $TariffShoulderTotal = $this->input->post('meter_data_shoulder') * $this->input->post('meter_data_shoulder_kwh');
                // $TariffOffPeakTotal = $this->input->post('meter_data_off_peak') * $this->input->post('meter_data_off_peak_kwh');

                // $SupplyCharge = $form_inputs['meter_data_supply_charge'] *  $form_inputs['supply_per_charge'];
                // $MeterCharge = $form_inputs['meter_data_meter_charge'] *  $form_inputs['meter_per_charge'];

                // $FinalElectricityBillWithoutMeterData = (($TariffPeakTotal + $TariffShoulderTotal + $TariffOffPeakTotal) * 12) + $SupplyCharge + $MeterCharge;


                $flatRates = array(
                    "peak_kwh" => $this->input->post('meter_data_peak_kwh'),
                    "peak" => $this->input->post('meter_data_peak'),
                    "shoulder_kwh" => $this->input->post('meter_data_shoulder_kwh'),
                    "shoulder" => $this->input->post('meter_data_shoulder'),
                    "off_peak_kwh" => $this->input->post('meter_data_off_peak_kwh'),
                    "off_peak" => $this->input->post('meter_data_off_peak'),
                    "billing_month" => $this->input->post('billing_month'),
                    "consumption_inputs" => $this->input->post('consumption_inputs'),
                    "daily_average_consumption" => $this->input->post('daily_average_consumption'),
                );
            } else {
                // $form_inputs['meter_data_peak_kwh'] = $this->input->post('meter_data_peak_kwh');
                // $form_inputs['meter_data_shoulder_kwh'] = $this->input->post('meter_data_shoulder_kwh');
                // $form_inputs['meter_data_off_peak_kwh'] = $this->input->post('meter_data_off_peak_kwh');

                // $QuantityType = $this->input->post('quantity_type');
                // $FlatQuantity = $this->input->post('flat_quantity');
                // $FlatBillTypeValue = $this->input->post('flat_bill_type_rate');

                // $SupplyCharge = $form_inputs['meter_data_supply_charge'] *  $form_inputs['supply_per_charge'];
                // $MeterCharge = $form_inputs['meter_data_meter_charge'] *  $form_inputs['meter_per_charge'];

                // $FinalElectricityBillWithoutMeterData = ($FlatBillTypeValue) + $SupplyCharge + $MeterCharge;
            }
            $update_data['flat_rates'] = json_encode($flatRates);
        }

        // demand charges
        $update_data['is_demand_charge'] = $IsDemandCharge;
        if ($IsDemandCharge != 0) {
            $demand_charges_length = $this->input->post('demand_charge_value');
            for ($k = 1; $k <= $demand_charges_length; $k++) {

                if ($MeterUploadStatus == 2) {
                    $meter_data_demand_charge[] = array(
                        "rate" => $this->input->post('meter_data_demand_charge' . $k),
                        "days" => $this->input->post('meter_data_demand_day' . $k),
                        "months" => $this->input->post('meter_data_demand_month' . $k),
                        "start_time" => $this->input->post('meter_data_demand_time_start' . $k),
                        "end_time" => $this->input->post('meter_data_demand_time_end' . $k),
                    );
                } else {
                    $meter_data_demand_charge[] = array(
                        "rate" => $this->input->post('meter_data_demand_rate' . $k),
                        "charge" => $this->input->post('meter_data_demand_charge' . $k),
                        "qty" => $this->input->post('meter_data_demand_qty' . $k),
                        "months" => $this->input->post('meter_data_demand_month' . $k),
                        "start_time" => $this->input->post('meter_data_demand_time_start' . $k),
                        "end_time" => $this->input->post('meter_data_demand_time_end' . $k),
                    );
                }
            }
            $update_data['demand_charges'] = json_encode($meter_data_demand_charge);
        }

        if ($IsBillType == "flat") {
            $update_data['is_demand_charge'] = 0;
            $update_data['demand_charges'] = null;
        }

        // Standard Charges
        $standard_charges = array(
            "supply" => array(
                "charge" => $this->input->post('meter_data_supply_charge'),
                "per" => $this->input->post('supply_per_charge')
            ),
            "meter" => array(
                "charge" => $this->input->post('meter_data_meter_charge'),
                "per" => $this->input->post('meter_per_charge'),
            ),
            "other" => array(
                "charge" => $this->input->post('other_charge'),
                "per" => $this->input->post('other_per_charge'),
            ),
            "utility_value_export" => $this->input->post('utility_value_export')
        );
        $update_data['standard_charges'] = json_encode($standard_charges);


        // Price Annual Increase Calculation
        $MeterDataPiaYear = $this->input->post('meter_data_pia_year');
        $MeterDataPiaPercentage = $this->input->post('meter_data_pia_percentage');
        $MeterDataSubsequent = $this->input->post('meter_data_subsequent');
        $update_data['price_annual_increase'] = json_encode(array(
            "MeterDataPiaYear" =>  $MeterDataPiaYear,
            "MeterDataPiaPercentage" => $MeterDataPiaPercentage,
            "MeterDataSubsequent" => $MeterDataSubsequent
        ));

        if ($IsDemandCharge != "1") {
            $data['demand_data'] = "";
        }

        $update_data['consumption_data'] = json_encode(array(
            "consumption_data" => $data['consumption_data'],
            "demand_data" => $data['demand_data'],
            "consumption_date" => $data['consumption_date'],
            "consumption_time" => $data['consumption_time']
        ));

        // $previous_data = $data['consumption_data'];

        $this->common->update_data('tbl_solar_proposal', array('id' => $this->input->post('proposal_data_id')), $update_data);

        if ($FinacialSummeryStatus != '' && $FinacialSummeryStatus != NULL) {
            $output['calculate_bill_charges'] = $calculate_bill_charges =  $this->calculate_data_with_degradation_factor($ProposalDataId, 1);
        } else {
            $output['calculate_bill_charges'] = $calculate_bill_charges =  $this->calculate_bill_charges($ProposalDataId);
        }
        // print_r($calculate_bill_charges['final_calculation']);
        // die;

        $data['consumption_data'] = $calculate_bill_charges['final_calculation']['consumption_data'];
        $output['graph_data'] = $graph_data  = $this->calcualte_summer_and_weekend_load_graph_data_from_meter_data2($data, $nrel_data, $IsDemandCharge);

        // // saving meter calcualtion (load data)
        $saving_meter_calculation['avg_monthly_load_graph_data'] = $graph_data['avg_monthly_load_graph_data'];
        $saving_meter_calculation['avg_export_prd_graph_data'] = $graph_data['avg_export_prd_graph_data'];
        $saving_meter_calculation['avg_offset_prd_graph_data'] = $graph_data['avg_offset_prd_graph_data'];
        $saving_meter_calculation['avg_sol_prd_graph_data'] = $graph_data['avg_sol_prd_graph_data'];
        $saving_meter_calculation['weekend_load_graph_data'] = $graph_data['weekend_load_graph_data'];
        // $saving_meter_calculation['annual_solar_production'] = $graph_data['meter_data_charges'][0]['total_solar_production'];
        $saving_meter_calculation['offset_consumption'] = $graph_data['offset_consumption'];
        $saving_meter_calculation['export_to_grid'] = $graph_data['export_to_grid'];


        // $graph_data['previous_data'] = $previous_data;
        // $graph_data['calculate_bill_charges'] = $calculate_bill_charges;


        // // saving nrel calculation (solar production)
        // $saving_nrel_calculation['final_nrel_calculation'] = $data2['meter_data_charges'][0]['final_nrel_calculation'];
        // $saving_nrel_calculation['total_solar_production'] = $data2['meter_data_charges'][0]['total_solar_production'];

        // // These are four graph values
        $saving_graph_calculation['typical_graph'] = $graph_data['typical_graph'];
        $saving_graph_calculation['worst_graph'] = $graph_data['worst_graph'];
        $saving_graph_calculation['high_graph'] = $graph_data['high_graph'];
        $saving_graph_calculation['best_graph'] = $graph_data['best_graph'];

        // // Saving Data
        $update_calculation['saving_meter_calculation'] = json_encode($saving_meter_calculation);
        // $update_data['saving_nrel_calculation'] = json_encode($saving_nrel_calculation);
        $update_calculation['saving_graph_calculation'] = json_encode($saving_graph_calculation);


        $AnnualSolarProduction = $calculate_bill_charges['final_calculation']['FinalEnergyProvidedBySolar'][0];
        $FinalEnergyProvidedBySolar = $calculate_bill_charges['final_calculation']['FinalOffsetValue'][0];
        $FinalPercantageEnergyProvidedBySolar = $calculate_bill_charges['final_calculation']['FinalPercantageEnergyProvidedBySolar'][0];
        $FinalPercantageOfExport = $calculate_bill_charges['final_calculation']['FinalPercantageExport'][0];
        $FinalTotalLoadMeterData = $calculate_bill_charges['sum_load_data'];
        $FinalLoadChargesWithRates = $calculate_bill_charges['final_calculation']['FinalLoadChargesWithRates'][0];
        $FinalReducesCarbon = $AnnualSolarProduction * REDUCES_CARBON[$lead_data['state_postal']];
        $FinalBillAfterSolar = $calculate_bill_charges['final_calculation']['FinalTotalBillWithSolar'][0];


        // $AverageDailyConsumption = $calculate_bill_charges['final_calculation']['AverageDailyConsumption'][0];
        // print_r($calculate_bill_charges['final_calculation']);
        $DdemandCharges = 0;
        $DdemandChargesWithSolar = 0;
        if ($IsDemandCharge == '1' && $IsBillType != 'flat') {
            $DdemandCharges = $calculate_bill_charges['final_calculation']['FinalIncreaseDemandData'];
            $DdemandChargesWithSolar = $calculate_bill_charges['final_calculation']['DemandChargeWithSolar'];
        }
        if(is_array($DdemandCharges)){
            $FinalLoadChargesWithRates = $FinalLoadChargesWithRates + $DdemandCharges[0];
        } else {
            $FinalLoadChargesWithRates = $FinalLoadChargesWithRates + $DdemandCharges;
        }
        if(is_array($DdemandChargesWithSolar)){
            $FinalBillAfterSolar =  $FinalBillAfterSolar + $DdemandChargesWithSolar[0];
        } else {
            $FinalBillAfterSolar =  $FinalBillAfterSolar + $DdemandChargesWithSolar;
        }

        $update_calculation['system_performance'] = json_encode(array(
            "AnnualSolarProduction" => $AnnualSolarProduction,
            "FinalBillAfterSolar" => $FinalBillAfterSolar,
            "FinalEnergyProvidedBySolar" => $FinalEnergyProvidedBySolar,
            "FinalPercantageEnergyProvidedBySolar" => $FinalPercantageEnergyProvidedBySolar,
            "FinalPercantageOfExport" => $FinalPercantageOfExport,
            "FinalTotalLoadMeterData" => $FinalTotalLoadMeterData,
            "FinalLoadChargesWithRates" => $FinalLoadChargesWithRates,
            "FinalReducesCarbon" => $FinalReducesCarbon,
            "FinalElectricityBillWithoutMeterData" => $FinalElectricityBillWithoutMeterData,
            // "AverageDailyConsumption" => $AverageDailyConsumption
        ));

        $this->common->update_data('tbl_solar_proposal', array('id' => $this->input->post('proposal_data_id')), $update_calculation);
        $PvWattsDataResult = json_decode($PvWattsDataResult['pvwatts_data_result']);
        $graph_data['PvWattsDataResult'] = $PvWattsDataResult->outputs->ac;
        // $graph_data['PvWattsDataResultSum'] = array_sum($PvWattsDataResult->outputs->ac);
        // $graph_data['PvWattsDataResultCount'] = count($PvWattsDataResult->outputs->ac);
        // $graph_data['consumption'] = $data['consumption_data'];
        // $graph_data['AverageDailyConsumption'] = $AverageDailyConsumption;
        $graph_data['success'] = TRUE;
        echo json_encode($graph_data);
        die;
    }

    public function calculate_price_annual_increase(
        $MeterDataPiaYear,
        $MeterDataPiaPercentage,
        $MeterDataSubsequent,
        $FinalLoadChargesWithRates,
        $FinalBillAfterSolar
    ) {

        $data = array();
        $SubstractBillBeforeAfter = $FinalLoadChargesWithRates - $FinalBillAfterSolar;
        $UpatedPriceIncrease = $SubstractBillBeforeAfter;
        $TotalExcCost = 40000;

        $i = 1;
        $data[$i] = $UpatedPriceIncrease;

        while ($UpatedPriceIncrease <= $TotalExcCost) {

            $i++;

            if ($i <= $MeterDataPiaYear) {
                $UpatedPriceIncrease += (($UpatedPriceIncrease * $MeterDataPiaPercentage) / 100);
                $data[$i] =  $UpatedPriceIncrease;
            } else {
                $UpatedPriceIncrease += (($UpatedPriceIncrease * $MeterDataSubsequent) / 100);
                $data[$i] =  $UpatedPriceIncrease;
            }

            if ($UpatedPriceIncrease >= $TotalExcCost) {
                break;
            }
        }

        return $data;
    }

    public function read_solar_production_data($meter_data, $nrel_data, $IsProductionFile)
    {


        $full_year_meter_data       = $meter_data['consumption_data'];
        $full_year_meter_data_dates = $meter_data['consumption_date'];
        $full_year_meter_data_times = $meter_data['consumption_time'];

        $nrel_d = [];
        $nrel_d = json_decode($nrel_data['pvwatts_data_result']);
        $nrel_d = $nrel_d->outputs->ac;

        // echo count($full_year_meter_data) . "  " . count($nrel_d);
        // die;

        // if (count($full_year_meter_data) === count($nrel_d)) {
        //     return $nrel_d;
        // }

        $ProductionData = [];
        // $ProductionDataCheck = [];
        $feb_month = [];


        $i = 0;
        $j = 0;
        $k = 0;
        $dividedby = 1000;
        if ((int)$IsProductionFile == 1) {
            $dividedby = 1;
        }

        foreach ($full_year_meter_data as $key => $value) {

            $day_int = date('d', strtotime($full_year_meter_data_dates[$key]));
            $month_int = date('m', strtotime($full_year_meter_data_dates[$key]));
            $year      = date('Y', strtotime($full_year_meter_data_dates[$key]));
            $month     = date('M', strtotime($full_year_meter_data_dates[$key]));
            $month     = strtolower($month);
            $hr        = (int) date('H', strtotime($full_year_meter_data_times[$key]));


            // Checking for leap year
            if ($year % 4 == 0) {
                if (count($full_year_meter_data) == count($nrel_d)) {
                    $ProductionData[$i] = $nrel_d[$j] / $dividedby;
                    // $ProductionDataCheck[$i] = "  " . $ProductionData[$i] . "   " . $full_year_meter_data_dates[$i] . "  " . $full_year_meter_data_times[$i];
                    $j++;
                } else {


                    if ($day_int == '28' && $month == 'feb') {
                        $ProductionData[$i] = $nrel_d[$j] / $dividedby;
                        $feb_month[] = $nrel_d[$j] / $dividedby;
                        // $ProductionDataCheck[$i] = "  " . $ProductionData[$i] . "   " . $full_year_meter_data_dates[$i] . "  " . $full_year_meter_data_times[$i];
                        $j++;
                    } else {

                        if ($day_int == '29' && $month == 'feb') {
                            $ProductionData[$i] = $feb_month[$k];
                            // $ProductionDataCheck[$i] = "  " . $ProductionData[$i] . "   " . $full_year_meter_data_dates[$i] . "  " . $full_year_meter_data_times[$i];
                            $k++;
                        } else {
                            $ProductionData[$i] = $nrel_d[$j] / $dividedby;
                            // $ProductionDataCheck[$i] = "  " . $ProductionData[$i] . "   " . $full_year_meter_data_dates[$i] . "  " . $full_year_meter_data_times[$i];
                            $j++;
                        }
                    }
                }
            } else {
                $ProductionData[$i] = $nrel_d[$i] / $dividedby;
                // $ProductionDataCheck[$i] = "  " . $ProductionData[$i] . "   " . $full_year_meter_data_dates[$i] . "  " . $full_year_meter_data_times[$i];
            }

            $i++;
        }


        // print_r($ProductionData);
        // die;

        return $ProductionData;
    }


    public function read_meter_data_file2($datatt)
    {

        $worksheet = $datatt;

        // print_r($worksheet);
        // print_r($worksheet[0]);
        // echo count($worksheet[0]);
        // die;

        $i = $j = 0;
        $date_time_column = $kWh_column =  '';
        $is_sheet_column_datetime = false;


        for ($i = 0; $i < count($worksheet); $i++) {
            $cellIterator = $worksheet[$i];
            $cells = [];

            for ($j = 0; $j < count($cellIterator); $j++) {
                if ($i ==  0) {

                    if ($date_time_column == '') {
                        if (preg_match('/datetime/', strtolower($cellIterator[$j]))) {
                            $date_time_column = $j;
                        } else if (preg_match('/date\/time/', strtolower($cellIterator[$j]))) {
                            $date_time_column = $j;
                        } else if (preg_match('/time/', strtolower($cellIterator[$j]))) {
                            $date_time_column = $j;
                        }
                    }


                    if ($kWh_column == '') {

                        if (strtolower($cellIterator[$j]) == 'kw') {
                            $kWh_column = $j;
                        }
                    }
                }
                $cells[] = $cellIterator[$j];
            }

            //If Column not present return false;
            if (($date_time_column == '' || $kWh_column == '') && $i == 0 && $date_time_column != 0) {
                echo json_encode(array(
                    'status' => 'Either Date/Time or kWh column is not found on the uploaded file.',
                    'success' => FALSE
                ));
                die;
            }

            if ($i > 0) {
                $dot_position = strpos($cells[$date_time_column], '.');
                $datetime = ($dot_position != FALSE && $dot_position != 5) ? str_replace('.', '-', $cells[$date_time_column]) : $cells[$date_time_column];
                if (is_numeric($datetime)) {
                    $is_sheet_column_datetime = TRUE;
                }

                // print_r($datetime);
                // die;
                $new_datetime = [];
                // if ($is_sheet_column_datetime) {
                //     $new_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datetime);
                // }
                // $originalDate = "1/11/2020 0:00";
                $new_datetime = date("d-m-Y H:i:s", strtotime($datetime));
                // $new_datetime = !empty($new_datetime) ? $new_datetime->format("d-m-Y H:i:s") : $datetime;
                $rows[($i - 1)][0] = $new_datetime;
                $rows[($i - 1)][1] = $cells[$kWh_column];
            }
        }
        // print_r($rows);
        // die;
        return $rows;
    }


    public function read_meter_data_file($datatt, $IsDemandCharge, $meter_data_column_mapping)
    {

        $worksheet = $datatt;
        // print_r($worksheet);
        // die;

        $i = $j = 0;
        $date_time_column = $kWh_column = $kva_column = '';
        $is_sheet_column_datetime = false;

        if ($meter_data_column_mapping != NULL) {
            $meter_data_column_mapping = json_decode($meter_data_column_mapping);
        }

        // print_r($meter_data_column_mapping);
        // echo $meter_data_column_mapping[0];
        // echo in_array('datetime', $meter_data_column_mapping);
        // if (in_array('datetime', $meter_data_column_mapping)) {
        //     echo true;
        // } else {
        //     echo false;
        // }
        // die;

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];


            foreach ($cellIterator as  $cell) {
                if ($i ==  0) {
                    //Detect Datetime and Kwh columns
                    if ($date_time_column == '') {
                        if ($meter_data_column_mapping != NULL) {
                            foreach ($meter_data_column_mapping as $key => $value) {
                                if (preg_match('/datetime/', strtolower($value))) {
                                    $date_time_column = $key;
                                }
                                if (preg_match('/date\/time/', strtolower($value))) {
                                    $date_time_column = $key;
                                }
                                if (preg_match('/time/', strtolower($value))) {
                                    $date_time_column = $key;
                                }
                            }
                        } else {
                            if (preg_match('/datetime/', strtolower($cell->getValue()))) {
                                $date_time_column = $j;
                                //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell);
                            } else if (preg_match('/date\/time/', strtolower($cell->getValue()))) {
                                $date_time_column = $j;
                                //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell);
                            } else if (preg_match('/time/', strtolower($cell->getValue()))) {
                                $date_time_column = $j;
                                //$is_sheet_column_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell); kwh
                            }
                        }
                    }

                    if ($kWh_column == '') {


                        // if (preg_match('/kwh/', strtolower($cell->getValue()))) {
                        //     $kWh_column = $j;
                        // } else if (preg_match('/kw/', strtolower($cell->getValue()))) {
                        //     $kWh_column = $j;
                        // } else if (preg_match('/e kwh at meter/', strtolower($cell->getValue()))) {
                        //     $kWh_column = $j;
                        // }

                        if ($meter_data_column_mapping != NULL) {
                            foreach ($meter_data_column_mapping as $key => $value) {
                                if (preg_match('/kw/', strtolower($value))) {
                                    $kWh_column = $key;
                                }
                            }
                        } else {
                            if (strtolower($cell->getValue()) == 'kw') {
                                $kWh_column = $j;
                            }
                        }
                    }
                    if ($IsDemandCharge == "1") {


                        if ($meter_data_column_mapping != NULL) {
                            foreach ($meter_data_column_mapping as $key => $value) {
                                if (preg_match('/kva/', strtolower($value))) {
                                    $kva_column = $key;
                                }
                            }
                        } else {
                            if ($kva_column == '') {
                                if (strtolower($cell->getValue()) == 'kva') {
                                    $kva_column = $j;
                                }
                            }
                        }
                    }

                    $j++;
                }
                $cells[] = $cell->getValue();
            }


            //If Column not present return false;
            if (($date_time_column == '' || $kWh_column == '') && $i == 0 && $date_time_column != 0) {
                echo json_encode(array(
                    'status' => 'Either Date/Time or kWh column is not found on the uploaded file.',
                    'success' => FALSE
                ));
                die;
            }


            //Maintain 2 column table
            //Also check if date is not having dots
            if ($i > 0) {
                $dot_position = strpos($cells[$date_time_column], '.');
                $datetime = ($dot_position != FALSE && $dot_position != 5) ? str_replace('.', '-', $cells[$date_time_column]) : $cells[$date_time_column];
                if (is_numeric($datetime)) {
                    $is_sheet_column_datetime = TRUE;
                }

                $new_datetime = [];
                if ($is_sheet_column_datetime) {
                    $new_datetime = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($datetime);
                }
                $new_datetime = !empty($new_datetime) ? $new_datetime->format("d-m-Y H:i:s") : $datetime;
                $rows[($i - 1)][0] = $new_datetime;
                $rows[($i - 1)][1] = $cells[$kWh_column];
                if ($IsDemandCharge == "1") {
                    $rows[($i - 1)][2] = $cells[$kva_column];
                }
            }
            $i++;
        }

        // print_r($rows);
        // die;
        return $rows;
    }

    private function calculate_consumption_profile_data_by_meter_data($proposal_data = [], $meter_data = [])
    {
        $data                 = [];
        //print_r($proposal_data);die;
        //Check if uploaded csv 2-columns or 3-columns
        $column_count         = count($meter_data[0]);
        $full_year_meter_data = [];
        if ($column_count == 2) {
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $meter_data);
            $full_year_meter_data = $final_data['data'];
        } else if ($column_count == 3) {
            $new_meter_data       = $this->convert_three_column_to_two_columns_meter_data($meter_data);
            $final_data           = $this->handle_two_columns_meter_data($proposal_data, $new_meter_data);
            $full_year_meter_data = $final_data['data'];
        }



        //echo '<pre>';
        //print_r($full_year_meter_data);die;

        for ($i = 0; $i < count($full_year_meter_data); $i++) {
            $data['consumption_date'][$i] = $full_year_meter_data[$i][0];
            $data['consumption_data'][$i] = $full_year_meter_data[$i][1];
            $data['consumption_time'][$i] = explode(" ", $full_year_meter_data[$i][0])[1];
            $data['final_data'][$i][0]    = $full_year_meter_data[$i][0];
            $data['final_data'][$i][1]    = $full_year_meter_data[$i][1];
            $month                        = (int) date('m', strtotime($full_year_meter_data[$i][0]));
            if ($month <= 3) {
                $data['consumption_keys_1_3'][] = $i;
            } else if ($month > 3 && $month <= 6) {
                $data['consumption_keys_4_6'][] = $i;
            } else if ($month > 6 && $month <= 9) {
                $data['consumption_keys_7_9'][] = $i;
            } else if ($month > 9 && $month <= 12) {
                $data['consumption_keys_10_12'][] = $i;
            }
        }
        $data['missing_months'] = $final_data['missing_months'];

        //echo '<pre>';
        //print_r($data);die;

        return $data;
    }


    public function convert_three_column_to_two_columns_meter_data($meter_data)
    {

        /** Dates in the m/d/y or d-m-y formats are disambiguated by looking at the separator between the various components: if the separator is a slash (/), then the American m/d/y is assumed; whereas if the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed. */
        $two_column_meter_data = [];
        foreach ($meter_data as $key => $value) {
            $time_in_24_hour_format = date("H:i", strtotime($value[1]));
            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date = $value[0];
            }
            $proper_date_format             = date('d-m-Y', strtotime($new_date));
            //echo $proper_date_format;die;
            $col1_col2                      = $proper_date_format . ' ' . $time_in_24_hour_format;
            $two_column_meter_data[$key][0] = $col1_col2;
            $two_column_meter_data[$key][1] = $value[2];
        }
        return $two_column_meter_data;
    }

    private function handle_two_columns_meter_data($proposal_data, $meter_data, $IsDemandCharge)
    {

        // print_r($meter_data);
        // die;
        $is_in_1hr_interval = $is_in_30_mins_interval = $is_in_15_mins_interval = false;

        //Check if DateTime is in 1 hr interval then do nothing
        $dateTime = date('i', strtotime($meter_data[1][0]));

        if ($dateTime == 00) {
            $is_in_1hr_interval = true;
            $minimized_meter_data = $meter_data;
        }

        //Check if DateTime is in 30 mins interval then sum and take mean of 2
        if ($dateTime == 30 && $is_in_1hr_interval == false) {

            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_30 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;

                if ($IsDemandCharge == "1") {
                    $dmin_0 = (isset($meter_data[$a])) ? $meter_data[$a][2] : 0;
                    $dmin_30 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][2] : 0;
                }

                $minimized_meter_data[$b][0]  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_30) / 2;

                if ($IsDemandCharge == "1") {
                    $minimized_meter_data[$b][2]  = ($dmin_0 + $dmin_30) / 2;
                }
                $a = $a + 2;
                $b++;
            }
        }

        //Check if DateTime is in 15 mins interval then sum and take mean of 4
        if ($dateTime == 15  && $is_in_1hr_interval == false  && $is_in_30_mins_interval == false) {
            $a = $b = 0;
            while ($a < count($meter_data)) {
                $min_0 = (isset($meter_data[$a])) ? $meter_data[$a][1] : 0;
                $min_15 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][1] : 0;
                $min_30 = (isset($meter_data[($a + 2)])) ? $meter_data[($a + 2)][1] : 0;
                $min_45 = (isset($meter_data[($a + 3)])) ? $meter_data[($a + 3)][1] : 0;

                if ($IsDemandCharge == "1") {
                    $dmin_0 = (isset($meter_data[$a])) ? $meter_data[$a][2] : 0;
                    $dmin_15 = (isset($meter_data[($a + 1)])) ? $meter_data[($a + 1)][2] : 0;
                    $dmin_30 = (isset($meter_data[($a + 2)])) ? $meter_data[($a + 2)][2] : 0;
                    $dmin_45 = (isset($meter_data[($a + 3)])) ? $meter_data[($a + 3)][2] : 0;
                }

                $minimized_meter_data[$b][0] = $s  = $meter_data[$a][0];
                $minimized_meter_data[$b][1]  = ($min_0 + $min_15 + $min_30 + $min_45) / 4;
                $minimized_meter_data[$b][2]  = ($dmin_0 + $dmin_15 + $dmin_30 + $dmin_45) / 4;
                $a = $a + 4;
                $b++;
            }
        }

        //So first get dates and hours based array meter data
        $meter_data_without_years = [];
        $month_years = [];
        foreach ($minimized_meter_data as $key => $value) {

            // echo $value[0];

            if (strpos($value[0], '-') == false) {
                $new_date = str_replace('/', '-', $value[0]);
            } else {
                $new_date =  $value[0];
            }

            $break_date = explode(' ', $new_date);

            $break_date_0 = explode('-', $break_date[0]);
            $break_date_1 = explode(':', $break_date[1]);

            // echo $new_date;


            if (strlen($break_date_0[0]) > 2) {
                $compiled_date = date('d-m H:i', strtotime($new_date));
            } else {
                $compiled_date = ((strlen($break_date_0[0]) == 2) ? $break_date_0[0] : '0' . $break_date_0[0]) . '-' . $break_date_0[1] . ' ' . ((strlen($break_date_1[0]) == 2) ? $break_date_1[0] : '0' . $break_date_1[0]) . ':' . $break_date_1[1];
            }

            // print_r($compiled_date);
            // die;

            $meter_data_without_years[$key][0] =  $compiled_date;
            $meter_data_without_years[$key][1] = $value[1];

            if ($IsDemandCharge == "1") {
                $meter_data_without_years[$key][2] = $value[2];
            }


            $month = (int) date('m', strtotime($new_date));
            if (!isset($month_years[$month])) {
                $year = (int) date('Y', strtotime($new_date));
                $month_years[$month] = $year;
            }
        }

        // echo '<pre>';
        // print_r($meter_data_without_years);
        // die;



        //Now loop for each month * no of days * 24hrs
        $final_meter_data['data'] = [];
        $final_meter_data['missing_months'] = [];
        $month = 1;
        $index = 0;
        while ($month <= 12) {
            $count_month_years = count($month_years);
            $prev_yr = 2021; //($count_month_years > 0) ? $month_years[$count_month_years - 1] : 2018;
            $year = 2022; //(isset($month_years[$month])) ? $month_years[$month] : $prev_yr;
            $no_of_days = $this->components->days_in_month($month, $year);
            for ($i = 1; $i <= $no_of_days; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    $d = ($i < 10) ? '0' . $i : $i;
                    $m = ($month < 10) ? '0' . $month : $month;
                    $h = ($j < 10) ? '0' . $j . ':00' : $j . ':00';
                    // if($m==10){
                    // echo $full_date = $d.'-'.$m.' '.$h;
                    // $key = array_search($full_date, array_column($meter_data_without_years, '0'));
                    // print_r(var_dump($key));die;
                    // }
                    $full_date = $d . '-' . $m . ' ' . $h;
                    $key = array_search($full_date, array_column($meter_data_without_years, '0'));
                    if ($key !== false) {
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = $meter_data_without_years[$key][1];
                        if ($IsDemandCharge == "1") {
                            $final_meter_data['data'][$index][2] = $meter_data_without_years[$key][2];
                        }
                    } else {
                        if (!in_array($m, $final_meter_data['missing_months'])) {
                            $final_meter_data['missing_months'][] = (int) $m;
                        }
                        $final_meter_data['data'][$index][0] = $d . '-' . $m . '-' . $year . ' ' . $h;
                        $final_meter_data['data'][$index][1] = 0;
                        if ($IsDemandCharge == "1") {
                            $final_meter_data['data'][$index][2] = 0;
                        }
                    }
                    $index++;
                }
            }
            $month++;
        }

        // echo "<pre>";
        // print_r($final_meter_data);die;
        // print_r($proposal_data);die;

        /**if (!empty($proposal_data) && $proposal_data['meter_data_replicated'] != NULL) {
        $meter_data_replicated = json_decode($proposal_data['meter_data_replicated']);
        // echo "<pre>";
        // print_r($meter_data_replicated);
        // die;

        if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
        foreach ($meter_data_replicated as $key => $value) {
        $to = (int) $value->replicated_to;
        $from = (int) $value->replicated_from;
        $start = $to_start = $end = $to_end = $j = 0;
        for($i = 1; $i <= $from; $i++){
        $no_of_days = $this->components->days_in_month($i,2018);
        $end += $no_of_days * 24;
        if($i == ($from - 1)){
        $start = ($end + 1);
        }
        }
        for($i = 1; $i <= $to; $i++){
        $no_of_days = $this->components->days_in_month($i,2018);
        if($i == $to){
        $to_start = $to_end;
        }else{
        $to_end += $no_of_days * 24;
        }
        }
        //  echo '<pre>';
        $matchMonth  = [];
        $fromMonth = strlen((string)$from)==1?'0'.$from:$from;
        foreach($final_meter_data['data'] as $k=>$month){
        $pattern = "/[0-9][0-9]-".$fromMonth."-[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]/";
        $toMonth = strlen((string)$to)==1?'0'.$to:$to;
        if(preg_match($pattern, $month[0])){
        $e = explode('-',$month[0]);

        $e[1] = $toMonth;
        $keyString = implode('-',$e);
        $matchMonth[$keyString] = $month[1];
        }
        }
        $previousValue = 0;
        foreach($final_meter_data['data'] as $k=>$month){
        $pattern = "/[0-9][0-9]-".$toMonth."-[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]/";
        if(isset($matchMonth[$month[0]])) {
        if(preg_match($pattern, $month[0])){
        $previousValue = $final_meter_data['data'][$k][1] = $matchMonth[$month[0]];
        }
        }
        // else {
        //     $previousValue = $final_meter_data['data'][$k][1] = $previousValue;
        // }

        }
        // print_r($final_meter_data);die();
        // $in = 0;
        // for ($j = $start; $j < $end; $j++) {
        //     $in = $j-1;
        //     $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
        //     $to_start++;
        // }

        }
        } else {
        foreach ($meter_data_replicated as $key => $value) {
        $to = (int) $value->replicated_to;
        $from = (int) $value->replicated_from;
        $start = $to_start = $end = $to_end = $j = 0;
        for($i = 1; $i <= $from; $i++){
        $no_of_days = $this->components->days_in_month($i,2018);
        $end += $no_of_days * 24;
        if($i == ($from - 1)){
        $start = ($end + 1);
        }
        }
        for($i = 1; $i <= $to; $i++){
        $no_of_days = $this->components->days_in_month($i,2018);
        if($i == $to){
        $to_start = $to_end;
        }else{
        $to_end += $no_of_days * 24;
        }
        }
        //echo $j;
        //echo $to_start;die;
        for ($j = $start; $j < $end; $j++) {
        $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
        //echo $final_meter_data['data'][$j][0];
        //echo $final_meter_data['data'][$to_start][0];
        //echo $final_meter_data['data'][$to_start][1];die;
        $to_start++;
        }
        }
        }

        }*/


        if (!empty($proposal_data) && $proposal_data['meter_data_replicated'] != NULL) {
            $meter_data_replicated = json_decode($proposal_data['meter_data_replicated']);
            foreach ($meter_data_replicated as $key => $value) {
                $to = (int) $value->replicated_to;
                $from = (int) $value->replicated_from;

                $start = $to_start = $end = $to_end = $j = 0;
                for ($i = 1; $i <= $from; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    $end += $no_of_days * 24;
                    if ($i == ($from - 1)) {
                        $start = $end; //($end + 1);
                    }
                }
                for ($i = 1; $i <= $to; $i++) {
                    $no_of_days = $this->components->days_in_month($i, 2018);
                    if ($i == $to) {
                        $to_start = $to_end;
                    } else {
                        $to_end += $no_of_days * 24;
                    }
                }

                for ($j = $start; $j < $end; $j++) {
                    $final_meter_data['data'][$to_start][1] = $final_meter_data['data'][$j][1];
                    $to_start++;
                }
            }
        }

        // echo json_encode($final_meter_data);
        // die;
        return $final_meter_data;
    }


    public function calculate_meter_data_charges($form_inputs, $final_meter_data, $avg_monthly_load_data, $nrel_data)
    {

        $meter_data_demand_charge = array();
        $total_meter_charges = 0;
        $nrel_calculation = [];
        $FinalTotalLoadMeterData = 0;
        $FinalBillAfterSolar = 0;
        $FinalEnergyProvidedBySolar = 0;
        $AnnualSolarProduction = array_sum($nrel_data);

        $supply_per_charge = $form_inputs['supply_per_charge'];
        $supply_charge = $form_inputs['meter_data_supply_charge'] * $supply_per_charge;
        $meter_per_charge = $form_inputs['meter_per_charge'];
        $meter_charge  = $form_inputs['meter_data_meter_charge'] * $meter_per_charge;
        $insert_data = $final_meter_data;

        for ($i = 0; $i < count($insert_data); $i++) {

            // peak charges
            $timestamp_peak = strtotime($insert_data[$i]['0']);
            $month_int = date('m', $timestamp_peak);
            $day_peak = date('D', $timestamp_peak);
            $m_month = date("M", $timestamp_peak);
            $m_month     = strtolower($m_month);

            $m_year = date("Y", $timestamp_peak);

            $splitTimeStamp = explode(" ", $insert_data[$i]['0']);
            $meter_date_time = strtotime($splitTimeStamp[1]);


            // Total Load
            if (!isset($BillAfterSolarValue[$m_month])) {
                $BillAfterSolarValue[$m_month] = [];
            }

            // Total Load
            if (!isset($TotalLoadMeterData[$m_month])) {
                $TotalLoadMeterData[$m_month] = [];
            }
            $TotalLoadMeterData[$m_month][] = $insert_data[$i]['1'];

            //  Percantage Energy Provided By Solar
            if (!isset($EnergyProvidedBySolar[$m_month])) {
                $EnergyProvidedBySolar[$m_month] = [];
            }

            // Bill After Solar
            if (!isset($BillAfterSolar[$m_month])) {
                $BillAfterSolar[$m_month] = [];
            }

            // Flat Bill Type
            // if ($form_inputs['is_bill_type'] === 'flat') {
            //     if (!isset($flat_rate_calculation[$m_month])) {
            //         $flat_rate_calculation[$m_month] = [];
            //     }

            //     $flat_rate_calculation[$m_month][] = $insert_data[$i]['1'] * $form_inputs['flat_bill_type_rate'];
            // }

            // peak
            if (!isset($peak_calculation_weekdays[$m_month])) {
                $peak_calculation_weekdays[$m_month] = [];
            }
            if (!isset($peak_calculation_sat[$m_month])) {
                $peak_calculation_sat[$m_month] = [];
            }
            if (!isset($peak_calculation_sun[$m_month])) {
                $peak_calculation_sun[$m_month] = [];
            }

            // shoulder
            if (!isset($shoulder_calculation_weekdays[$m_month])) {
                $shoulder_calculation_weekdays[$m_month] = [];
            }
            if (!isset($shoulder_calculation_sat[$m_month])) {
                $shoulder_calculation_sat[$m_month] = [];
            }
            if (!isset($shoulder_calculation_sun[$m_month])) {
                $shoulder_calculation_sun[$m_month] = [];
            }

            // off peak
            if (!isset($off_peak_calculation_weekdays[$m_month])) {
                $off_peak_calculation_weekdays[$m_month] = [];
            }
            if (!isset($off_peak_calculation_sat[$m_month])) {
                $off_peak_calculation_sat[$m_month] = [];
            }
            if (!isset($off_peak_calculation_sun[$m_month])) {
                $off_peak_calculation_sun[$m_month] = [];
            }

            //
            if ($insert_data[$i]['1'] > $nrel_data[$i]) {
                $EnergyProvidedBySolar[$m_month][] = $nrel_data[$i];
            } else {
                $EnergyProvidedBySolar[$m_month][] = $insert_data[$i]['1'];
            }


            $findIndex = array_search($splitTimeStamp[1], $form_inputs['time_of_use_start_time']);
            $SubstractForBillAfterSolar = $insert_data[$i]['1'] - $nrel_data[$i];
            $MultiplyValueForBillAfterSolar = 0;


            if ($day_peak != 'Sat' && $day_peak != 'Sun') {

                // Peak -- Weekdays
                if ($form_inputs['time_of_use_weekdays'][$findIndex] == 'Peak') {
                    $peak_calculation_weekdays[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_peak'];

                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_peak'];
                }

                // Shoulder -- Weekdays
                if ($form_inputs['time_of_use_weekdays'][$findIndex] == 'Shoulder') {
                    $shoulder_calculation_weekdays[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_shoulder'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_shoulder'];

                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_shoulder'];
                }

                // Off-Peak -- Weekdays
                if ($form_inputs['time_of_use_weekdays'][$findIndex] == 'Off-Peak') {
                    $off_peak_calculation_weekdays[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_off_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_off_peak'];

                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_off_peak'];
                }
            }

            if ($day_peak == 'Sat') {

                // Peak -- Sat
                if ($form_inputs['time_of_use_sat'][$findIndex] == 'Peak') {
                    $peak_calculation_sat[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_peak'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_peak'];
                }

                // Shoulder -- Sat
                if ($form_inputs['time_of_use_sat'][$findIndex] == 'Shoulder') {
                    $shoulder_calculation_sat[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_shoulder'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_shoulder'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_shoulder'];
                }

                // Off-Peak -- Sat
                if ($form_inputs['time_of_use_sat'][$findIndex] == 'Off-Peak') {
                    $off_peak_calculation_sat[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_off_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_off_peak'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_off_peak'];
                }
            }


            if ($day_peak == 'Sun') {

                // Peak -- Sun
                if ($form_inputs['time_of_use_sun'][$findIndex] == 'Peak') {
                    $peak_calculation_sun[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_peak'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_peak'];
                }

                // Shoulder -- Sun
                if ($form_inputs['time_of_use_sun'][$findIndex] == 'Shoulder') {
                    $shoulder_calculation_sun[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_shoulder'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_shoulder'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_shoulder'];
                }

                // Off-Peak -- Sun
                if ($form_inputs['time_of_use_sun'][$findIndex] == 'Off-Peak') {
                    $off_peak_calculation_sun[$m_month][] = $insert_data[$i]['1'] * $form_inputs['meter_data_off_peak'];

                    $nrel_calculation[$m_month][] = ($nrel_data[$i] / 1000) * $form_inputs['meter_data_off_peak'];
                    $MultiplyValueForBillAfterSolar = $form_inputs['meter_data_off_peak'];
                }
            }

            if ($form_inputs['is_bill_type'] === 'flat') {
                $MultiplyValueForBillAfterSolar = $form_inputs['flat_bill_type_rate'];
            }

            if ($SubstractForBillAfterSolar > 0) {
                // $bill_after_solar[$m_month][$insert_data[$i]['0']][] = $SubstractForBillAfterSolar * 0.1235;
                $BillAfterSolar[$m_month][] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                // $EnergyProvidedBySolar[$m_month][] = $SubstractForBillAfterSolar;
                $BillAfterSolarValue[$m_month][$insert_data[$i]['0']][] = $SubstractForBillAfterSolar;
            } else {
                // $bill_after_solar[$m_month][$insert_data[$i]['0']][] = $SubstractForBillAfterSolar * 0.065;
                $BillAfterSolar[$m_month][] = $SubstractForBillAfterSolar * $form_inputs['utility_value_export'];
                $BillAfterSolarValue[$m_month][$insert_data[$i]['0']][] = $SubstractForBillAfterSolar;
            }


            $no_of_days_24hr = $this->components->days_in_month($month_int, $m_year);



            if ($insert_data[$i]['0'] == $no_of_days_24hr . '-' . $month_int . '-' . $m_year . ' 23:00') {

                $final_meter_charges[$m_month]['peak_calculation'] = array_sum($peak_calculation_weekdays[$m_month]) + array_sum($peak_calculation_sun[$m_month]) + array_sum($peak_calculation_sat[$m_month]);


                $final_meter_charges[$m_month]['shoulder_calculation'] = array_sum($shoulder_calculation_weekdays[$m_month]) + array_sum($shoulder_calculation_sun[$m_month]) + array_sum($shoulder_calculation_sat[$m_month]);

                $final_meter_charges[$m_month]['off_peak_calculation'] = array_sum($off_peak_calculation_weekdays[$m_month]) + array_sum($off_peak_calculation_sat[$m_month]) + array_sum($off_peak_calculation_sun[$m_month]);

                $final_meter_charges[$m_month]['BillAfterSolarValue'] = $BillAfterSolarValue[$m_month];


                $FinalEnergyProvidedBySolar = $FinalEnergyProvidedBySolar + array_sum($EnergyProvidedBySolar[$m_month]);

                $final_nrel_calculation[$m_month] = array_sum($nrel_calculation[$m_month]);
                $FinalBillAfterSolar = $FinalBillAfterSolar + array_sum($BillAfterSolar[$m_month]);
                $FinalTotalLoadMeterData = $FinalTotalLoadMeterData + array_sum($TotalLoadMeterData[$m_month]);
                // $PercantageEnergyProvidedBySolar[$m_month] = array_sum($EnergyProvidedBySolar[$m_month]);
                $final_monthly_meter_charges[$m_month] = $final_meter_charges[$m_month]['peak_calculation'] + $final_meter_charges[$m_month]['shoulder_calculation'] + $final_meter_charges[$m_month]['off_peak_calculation'];
            }
        }


        if ($form_inputs['is_bill_type'] === 'flat') {
            $total_meter_charges = $FinalTotalLoadMeterData * $form_inputs['flat_bill_type_rate'];
        } else {
            foreach ($final_monthly_meter_charges as $key => $value) {
                $total_meter_charges = $total_meter_charges + $value;
            }
        }


        $FinalPercantageEnergyProvidedBySolar = ($FinalEnergyProvidedBySolar * 100) / $FinalTotalLoadMeterData;
        $FinalPercantageOfExport = 100 - (($FinalEnergyProvidedBySolar * 100) / $AnnualSolarProduction);

        $meter_data_calculation[] = array(
            // 'final_meter_charges' => $final_meter_charges,
            // 'final_meter_data' => $final_meter_data,
            // 'nrel_calculation' => $nrel_calculation,
            "FinalEnergyProvidedBySolar" => $FinalEnergyProvidedBySolar,
            "FinalPercantageEnergyProvidedBySolar" => $FinalPercantageEnergyProvidedBySolar,
            "FinalTotalLoadMeterData" => $FinalTotalLoadMeterData,
            "AnnualSolarProduction" => $AnnualSolarProduction,
            "FinalPercantageOfExport" => $FinalPercantageOfExport,
            "FinalBillAfterSolar" => $FinalBillAfterSolar + $supply_charge + $meter_charge,
            'final_nrel_calculation' => $final_nrel_calculation,
            "total_meter_charges" => $total_meter_charges,
            'supply_charge' => $supply_charge,
            'meter_charge' => $meter_charge,
            'final_monthly_meter_charges' => $final_monthly_meter_charges,
            "total_solar_production" => array_sum($nrel_data),
            'FinalLoadChargesWithRates' => $total_meter_charges + $supply_charge + $meter_charge
        );

        return $meter_data_calculation;

        echo json_encode($meter_data_calculation);
        die;
    }

    // public function generate_excel_degradation_factor(){

    // }

    public function calculate_bill_charges($proposal_id)
    {

        $data = array();
        $calculate_years = 1;
        $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));
        $data['bill_type'] = $bill_type = $proposal_data['bill_type'];
        $data['meter_data_type'] = $meter_data_type = $proposal_data['meter_data_type'];
        $data['flat_rates'] = $flat_rates = json_decode($proposal_data['flat_rates']);
        $data['is_demand_charge'] = $IsDemandCharge = $proposal_data['is_demand_charge'];
        $data['is_production_file'] = $is_production_file = $proposal_data['is_production_file'];
        $BillType = $proposal_data['bill_type'];

        $pvwatts_data = json_decode($proposal_data['pvwatts_data']);

        if ($is_production_file == 2) {
            $pvwatts_data_d = (array)$pvwatts_data[0];
            $data['degradation_factor'] = $degradation_factor = 1 - ($pvwatts_data_d['pv_degradation_factor'] / 100);
        } else {
            $data['degradation_factor'] = $degradation_factor = 1 - ($pvwatts_data[0]->pv_degradation_factor / 100);
        }

        // Getting Rates
        $data['meter_data_peak'] = $meter_data_peak = (float)$proposal_data['solar_cost_per_kwh_peak'];
        $data['meter_data_off_peak'] = $meter_data_off_peak = (float)$proposal_data['solar_cost_per_kwh_off_peak'];
        $data['meter_data_shoulder'] = $meter_data_shoulder = (float)$proposal_data['solar_cost_per_kwh_shoulder'];

        $time_of_use = json_decode($proposal_data['time_of_use']);
        $data['tou_time'] = $tou_time = $time_of_use->start_time;
        $data['tou_weekdays'] = $tou_weekdays = $time_of_use->weekdays;
        $data['tou_sat'] = $tou_sat = $time_of_use->sat;
        $data['tou_sun'] = $tou_sun = $time_of_use->sun;

        $type = 'STC';

        $data['financial_summary'] = $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
        if (count($financial_summary_data)) {
            $type = $financial_summary_data['type'];
        }

        $consumption = json_decode($proposal_data['consumption_data']);
        $data['consumption_data'] = $consumption_data = $consumption->consumption_data;
        $data['consumption_time'] = $consumption_time = $consumption->consumption_time;
        $data['consumption_date'] = $consumption_date = $consumption->consumption_date;

        if ($IsDemandCharge) {
            $data['demand_data'] = $demand_data = $consumption->demand_data;
        }
        $data['data_length'] = count($consumption_data);
        // If Demand charges
        if ($IsDemandCharge) {
            $data['demand_charges'] = $demand_charges = (array)json_decode($proposal_data['demand_charges']);
        }

        // Getting Production data
        $data['production_data'] = $production_data = json_decode($proposal_data['production_data']);
        // Getting Standard Charges
        $data['additonal_charges'] = $standard_charges = json_decode($proposal_data['standard_charges']);
        $data['supply_charge'] = $supply_charge = $standard_charges->supply->charge * $standard_charges->supply->per;
        $data['meter_charge'] = $meter_charge  = $standard_charges->meter->charge * $standard_charges->meter->per;
        $other_charge = 0;
        if (isset($standard_charges->other)) {
            $data['other_charge'] = $other_charge  = $standard_charges->other->charge;
        }

        $utility_value_export = (float)$standard_charges->utility_value_export;

        // Getting Subsequent and Years Data
        $data["price_annual_increase"] =  $price_annual_increase = json_decode($proposal_data['price_annual_increase']);
        $MeterDataPiaYear = $price_annual_increase->MeterDataPiaYear;
        $MeterDataPiaPercentage = $price_annual_increase->MeterDataPiaPercentage;
        $MeterDataSubsequent = $price_annual_increase->MeterDataSubsequent;
        $LoadMultiplyValue = 0;

        $FinalIncreaseRate = [];
        $FinalBillReduction = [];
        $FinalCummilativeSavings = [];
        $FinalBillAfterSolar = [];
        $FinalOffsetValue = [];
        $FinalPercantageExport = [];
        $FinalEnergyProvidedBySolar = [];
        $FinalPercantageEnergyProvidedBySolar = [];
        $FinalLoadChargesWithRates = [];
        $FinalSupplyCharge = [];
        $FinalMeterCharge = [];
        $FinalPeakRate = [];
        $FinalFlatRate = [];
        $FinalOffPeakRate = [];
        $FinalShoulderRate = [];
        $FinalExportElectricty = [];
        $FinalTotalBillWithoutSolar = [];
        $DemandMonthlyData = [];
        $DemandChargeRate = [];
        $DemandChargeWithSolar = [];
        $FinalDemandMonthlyData = [];
        $FinalIncreaseDemandData = [];
        $FinalTotalBillWithSolar = [];
        $FinalLGCValues = [];
        $NewLoad = [];


        $data['sum_load_data'] = $sum_load_data = array_sum($consumption_data);
        $production_calculated_data = [];
        $load_calculated_data = [];
        $bill_after_solor = [];
        $offset_value = [];
        $supply_charge_data = [];
        $meter_charge_data = [];
        $AverageDailyConsumption = [];

        $LoadMultiplyValue = ($MeterDataPiaPercentage / 100);
        $LoadRateForMultiply = 0;

        // Checking for No meter data TOU
        $PeakDifference = 0;
        $OffPeakDifference = 0;
        $ShoulderDifference = 0;
        $FlatChargeDifference = 0;
        $loadIncreasePercent = 1;
        if ($meter_data_type == "1") {

            $TOUBillingMonth =  $flat_rates->billing_month;
            if ($BillType != "flat") {
                $TOUPeakRate = $flat_rates->peak_kwh;
                $TOUPeakRateArray = [];
                $TOUShoulderRate =  $flat_rates->shoulder_kwh;
                $TOUShoulderRateArray = [];
                $TOUOffPeakRate =  $flat_rates->off_peak_kwh;
                $TOUOffPeakRateArray = [];
            } else {
                $FlatChargeRate =  (float)$flat_rates->charge;
                $FlatChargeRateArray = [];
            }

            $DailyConsumption = 0;
            for ($i = 0; $i < count($consumption_data); $i++) {
                $timestamp_peak = strtotime($consumption_date[$i]);
                $month_int = date('m', $timestamp_peak);
                $day_peak = date('D', $timestamp_peak);
                $m_month = $d_month = date("M", $timestamp_peak);
                $m_month     = strtolower($m_month);
                $m_year = date("Y", $timestamp_peak);
                $meter_date_time = strtotime($consumption_time[$i]);


                // Getting Difference if consumption inputs 'Simple'
                if ($meter_data_type == 1 && $bill_type != 'flat') {
                    if ($flat_rates->consumption_inputs == "Simple") {
                        $DailyConsumption = $DailyConsumption + $consumption_data[$i];

                        if ($consumption_time[$i] == '23:00') {
                            $AverageDailyConsumption[] = $DailyConsumption;
                            $DailyConsumption = 0;
                        }
                    }
                }

                $findIndex = array_search($consumption_time[$i], $tou_time);

                if ($m_month == strtolower($TOUBillingMonth)) {


                    if ($BillType != "flat") {

                        if ($day_peak != 'Sat' && $day_peak != 'Sun') {
                            // Peak -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Peak') {

                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }
                        if ($day_peak == 'Sat') {
                            // Peak -- Sat
                            if ($tou_sat[$findIndex] == 'Peak') {

                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Sat
                            if ($tou_sat[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Sat
                            if ($tou_sat[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }

                        if ($day_peak == 'Sun') {
                            // Peak -- Sat
                            if ($tou_sun[$findIndex] == 'Peak') {
                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Sat
                            if ($tou_sun[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Sat
                            if ($tou_sun[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }
                    } else {
                        $FlatChargeRateArray[] = $consumption_data[$i];
                    }
                }
            }

            if ($BillType != "flat") {

                if ($meter_data_type == 1 && $flat_rates->consumption_inputs != "Simple") {
                    if ((int)$TOUPeakRateArray) {
                        $PeakDifference = ($TOUPeakRate * 100) / array_sum($TOUPeakRateArray);
                        $PeakDifference = $PeakDifference - 100;
                    }

                    if ((int)$TOUShoulderRateArray) {
                        $ShoulderDifference = ($TOUShoulderRate * 100) / array_sum($TOUShoulderRateArray);
                        $ShoulderDifference = $ShoulderDifference - 100;
                    }

                    if ((int)$TOUOffPeakRateArray) {
                        $OffPeakDifference = ($TOUOffPeakRate * 100) / array_sum($TOUOffPeakRateArray);
                        $OffPeakDifference = $OffPeakDifference - 100;
                    }
                }

                // Replace FlatChargeDifference if consumption inputs 'Simple'
                if ($meter_data_type == 1 && $flat_rates->consumption_inputs == "Simple") {
                    $FlatChargeDifference = ($flat_rates->daily_average_consumption * 100) / (array_sum($AverageDailyConsumption) / 365);
                    $FlatChargeDifference = $FlatChargeDifference - 100;
                    if(array_sum($AverageDailyConsumption) > 0 ) {
                        $loadIncreasePercent = $flat_rates->daily_average_consumption / (array_sum($AverageDailyConsumption) / 365);
                    }
                } else {
                }
            } else {

                if ((int)$FlatChargeRateArray) {
                    $FlatChargeDifference = ($flat_rates->quantity * 100) / array_sum($FlatChargeRateArray);
                    $FlatChargeDifference = $FlatChargeDifference - 100;
                }
            }
        }

        // print_r($AverageDailyConsumption);
        // print_r(array_sum($AverageDailyConsumption));
        // print_r($FlatChargeDifference);
        // print_r((array_sum($AverageDailyConsumption) / 365));
        // die;


        for ($k = 0; $k < $calculate_years; $k++) {

            if ($k > $MeterDataPiaYear) {
                $LoadMultiplyValue = ($MeterDataSubsequent / 100);
            }

            $FinalIncreaseRate[$k] = $LoadMultiplyValue * 100;

            if ($k == 0) {
                $FinalPeakRate[$k] = $meter_data_peak;
                $FinalOffPeakRate[$k] = $meter_data_off_peak;
                $FinalShoulderRate[$k] = $meter_data_shoulder;

                if ($bill_type === 'flat') {
                    if ($meter_data_type == 2) {
                        $FinalFlatRate[$k] = (float)$flat_rates->charge;
                        $flat_rate_charge = (float)$flat_rates->charge;
                    } else {
                        $FinalFlatRate[$k] = (float)$flat_rates->energy_rate;
                        $flat_rate_charge = (float)$flat_rates->energy_rate;
                    }
                }
            } else {
                $FinalPeakRate[$k] = $FinalPeakRate[$k - 1] + ($FinalPeakRate[$k - 1] * $LoadMultiplyValue);
                $FinalOffPeakRate[$k] = $FinalOffPeakRate[$k - 1] + ($FinalOffPeakRate[$k - 1] * $LoadMultiplyValue);
                $FinalShoulderRate[$k] = $FinalShoulderRate[$k - 1] + ($FinalShoulderRate[$k - 1] * $LoadMultiplyValue);

                $meter_data_peak = $FinalPeakRate[$k];
                $meter_data_off_peak = $FinalOffPeakRate[$k];
                $meter_data_shoulder = $FinalShoulderRate[$k];

                if ($bill_type === 'flat') {
                    if ($meter_data_type == 2) {
                        $FinalFlatRate[$k] = $FinalFlatRate[$k - 1] + ($FinalOffPeakRate[$k - 1] * $LoadMultiplyValue);
                        $flat_rate_charge = $FinalFlatRate[$k];
                    } else {
                        $FinalFlatRate[$k] = $FinalFlatRate[$k - 1] + ($FinalFlatRate[$k - 1] * $LoadMultiplyValue);
                        $flat_rate_charge = $FinalFlatRate[$k];
                    }
                }
            }

            for ($i = 0; $i < count($consumption_data); $i++) {

                // peak charges
                $timestamp_peak = strtotime($consumption_date[$i]);
                $month_int = date('m', $timestamp_peak);
                $day_peak = date('D', $timestamp_peak);
                $m_month = $d_month = date("M", $timestamp_peak);
                $m_month     = strtolower($m_month);
                $m_year = date("Y", $timestamp_peak);
                $meter_date_time = strtotime($consumption_time[$i]);


                // begin:: demand charge
                // if ($IsDemandCharge) {

                //     for ($dc = 0; $dc < count($demand_charges); $dc++) {
                //         $temp_array = (array)$demand_charges[$dc];

                //         if ($meter_data_type == 2) {
                //             if (in_array($day_peak, $temp_array['days']) && in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                //                 if (count($DemandMonthlyData) && isset($DemandMonthlyData[$dc][$d_month])) {
                //                     if ($DemandMonthlyData[$dc][$d_month] < $demand_data[$i]) {
                //                         $DemandMonthlyData[$dc][$d_month] = $demand_data[$i];
                //                     }
                //                 } else {
                //                     $DemandMonthlyData[$dc][$d_month] =  $demand_data[$i];
                //                 }

                //                 $FinalIncreaseDemandData[$k][$dc] = array_sum($DemandMonthlyData[$dc]);
                //             }
                //         }
                //     }
                // }

                // begin:: demand charge
                if ($IsDemandCharge) {

                    if ($meter_data_type == 2) {
                        $PowerFactor[$k][$i] = ($demand_data[$i] != 0) ? ($consumption_data[$i] / $demand_data[$i]) : $demand_data[$i];
                    }

                    for ($dc = 0; $dc < count($demand_charges); $dc++) {
                        $temp_array = (array)$demand_charges[$dc];

                        if ($meter_data_type == 2) {
                            if (in_array($day_peak, $temp_array['days']) && in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DemandMonthlyData) && isset($DemandMonthlyData[$dc][$d_month])) {
                                    if ($DemandMonthlyData[$dc][$d_month] < $demand_data[$i]) {
                                        $DemandMonthlyData[$dc][$d_month] = $demand_data[$i];
                                    }
                                } else {
                                    $DemandMonthlyData[$dc][$d_month] =  $demand_data[$i];
                                }

                                if ($k == 0) {
                                    $DemandChargeRate[$k][$dc] = $temp_array['rate'];
                                    $FinalIncreaseDemandData[$k][$dc] = array_sum($DemandMonthlyData[$dc]) * $temp_array['rate'];
                                } else {
                                    $DemandChargeRate[$k][$dc] = $DemandChargeRate[$k - 1][$dc] + ($DemandChargeRate[$k - 1][$dc] * $LoadMultiplyValue);
                                    $FinalIncreaseDemandData[$k][$dc] = $FinalIncreaseDemandData[$k - 1][$dc] + ($FinalIncreaseDemandData[$k - 1][$dc] *  $LoadMultiplyValue);
                                }
                            }
                        } else {
                            if (in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                $DemandMonthlyData[$dc][$d_month] =  $temp_array['charge'];

                                if ($k == 0) {
                                    $DemandChargeRate[$k][$dc] = $temp_array['rate'];
                                    $FinalIncreaseDemandData[$k][$dc] = array_sum($DemandMonthlyData[$dc]);
                                } else {
                                    $DemandChargeRate[$k][$dc] = $DemandChargeRate[$k - 1][$dc] + ($DemandChargeRate[$k - 1][$dc] * $LoadMultiplyValue);
                                    $FinalIncreaseDemandData[$k][$dc] = $FinalIncreaseDemandData[$k - 1][$dc] + ($FinalIncreaseDemandData[$k - 1][$dc] *  $LoadMultiplyValue);
                                }
                            }
                        }
                    }
                }

                // end:: demand charge
                if ($k == 0) {
                    $NewLoad[$i] = $consumption_data[$i];
                }

                // end:: demand charge

                $MultiplyValueForBillAfterSolar = 0;
                $findIndex = array_search($consumption_time[$i], $tou_time);

                if ($day_peak != 'Sat' && $day_peak != 'Sun') {

                    // Peak -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Peak') {

                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;

                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($day_peak == 'Sat') {

                    // Peak -- Sat
                    if ($tou_sat[$findIndex] == 'Peak') {
                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Sat
                    if ($tou_sat[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;


                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Sat
                    if ($tou_sat[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($day_peak == 'Sun') {

                    // Peak -- Sat
                    if ($tou_sun[$findIndex] == 'Peak') {
                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Sat
                    if ($tou_sun[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;

                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Sat
                    if ($tou_sun[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($k == 0) {
                    if ($bill_type == 'flat') {
                        $NewLoad[$i] = $consumption_data[$i];
                        if ($meter_data_type == "1") {
                            if ($FlatChargeDifference > 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            } else {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            }
                        }
                        $LoadRateForMultiply =  $flat_rate_charge;
                    } else {
                        if ($meter_data_type == 1 && $flat_rates->consumption_inputs == "Simple") {
                            if ($FlatChargeDifference > 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            } else {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                } else {
                    if ($bill_type == 'flat') {
                        $LoadRateForMultiply =  $flat_rate_charge;
                    } else {
                        if ($meter_data_type == 1 && $flat_rates->consumption_inputs == "Simple") {
                            if ($FlatChargeDifference > 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            } else {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($k == 0) {


                    // if ($other_charge != '' || $other_charge != 0) {
                    //     $consumption_data[$i] = $consumption_data[$i] * $other_charge;
                    // }

                    $MultiplyValueForBillAfterSolar = $LoadRateForMultiply;
                    $FinalExportElectricty[$k] = $utility_value_export;

                    // Production calculation
                    $data['production_calculated_data'][$k][$i] = $production_calculated_data[$k][$i] = $production_data[$i];

                    // Load calculatoin
                    // if ($other_charge != '') {
                    $load_calculated_data[$k][$i] = ($consumption_data[$i] * $LoadRateForMultiply);
                    $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = ($consumption_data[$i] * $other_charge) + $load_calculated_data[$k][$i];
                    // } else {
                    //     $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = ($consumption_data[$i] * $LoadRateForMultiply);
                    // }

                    $SubstractForBillAfterSolar = $consumption_data[$i] - $production_data[$i];
                    $decidingFactor[$k][$i] = $SubstractForBillAfterSolar;

                    if ($IsDemandCharge) {
                        if ($meter_data_type == 2) {
                            $FinalKAVYearlyData[$k][$i] = ($PowerFactor[$k][$i] != 0) ? ($SubstractForBillAfterSolar / $PowerFactor[$k][$i]) : $PowerFactor[$k][$i];
                        } else {
                            $FinalKAVYearlyData[$k][$i] = $decidingFactor[$k][$i] / 0.95;
                        }
                    }

                    // Bill After Solar
                    if ($SubstractForBillAfterSolar > 0) {
                        // if ($other_charge != '') {
                        $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = ($SubstractForBillAfterSolar * $other_charge) + $bill_after_solor[$k][$i];
                        // } else {
                        //     $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                        // }
                    } else {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $utility_value_export;
                    }

                    // Energy Provided By Solar
                    // This will be (min load and production)
                    if ($consumption_data[$i] > $production_data[$i]) {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $production_data[$i];
                    } else {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $consumption_data[$i];
                    }

                    // Supply Charge
                    $data['supply_charge_data'][$k] = $supply_charge_data[$k] = $supply_charge;

                    // Meter Charge
                    $data['meter_charge_data'][$k] = $meter_charge_data[$k] = $meter_charge;
                } else {
                    $MultiplyValueForBillAfterSolar = $LoadRateForMultiply;
                    $FinalExportElectricty[$k] = $utility_value_export;

                    // Production calculation
                    $data['production_calculated_data'][$k][$i] = $production_calculated_data[$k][$i] = $production_calculated_data[$k - 1][$i] * $degradation_factor;

                    // Load calculatoin
                    $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = $load_calculated_data[$k - 1][$i] + ($load_calculated_data[$k - 1][$i] * $LoadMultiplyValue);

                    $SubstractForBillAfterSolar = $consumption_data[$i] - $production_calculated_data[$k][$i];

                    // Bill After Solar
                    if ($SubstractForBillAfterSolar > 0) {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                    } else {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $utility_value_export;
                    }

                    // Energy Provided By Solar
                    // This will be (min load and production)
                    if ($consumption_data[$i] > $production_calculated_data[$k][$i]) {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $production_calculated_data[$k][$i];
                    } else {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $consumption_data[$i];
                    }

                    // Supply Charge
                    $data['supply_charge_data'][$k] = $supply_charge_data[$k] = $supply_charge_data[$k - 1] + ($supply_charge_data[$k - 1] * $LoadMultiplyValue);

                    // Meter Charge
                    $data['meter_charge_data'][$k] = $meter_charge_data[$k] = $meter_charge_data[$k - 1] + ($meter_charge_data[$k - 1] * $LoadMultiplyValue);
                }
            }
        }


        $FinalLoadChargesWithoutSolar = [];
        $FinalWithoutStanding = [];
        $NetPayBillAmount = [];
        $DCWithSolarMonthlyData = [];
        $MaxDCWithSolar = [];
        for ($k = 0; $k < $calculate_years; $k++) {
            $FinalEnergyProvidedBySolar[$k] = array_sum($production_calculated_data[$k]);
            $FinalLoadChargesWithRates[$k] = array_sum($load_calculated_data[$k]) + $supply_charge + $meter_charge;
            $FinalLoadChargesWithoutSolar[$k] = array_sum($load_calculated_data[$k]);
            $FinalBillAfterSolar[$k] = array_sum($bill_after_solor[$k]);
            $FinalOffsetValue[$k] = array_sum($offset_value[$k]);
            // Bill Reduction
            $FinalBillReduction[$k] = $FinalLoadChargesWithRates[$k] - $FinalBillAfterSolar[$k];

            // % Energy Provided by Solar
            // Formula is (consumption*100)/(sum of load)
            $FinalPercantageEnergyProvidedBySolar[$k] = ($FinalOffsetValue[$k] * 100) / ($sum_load_data*$loadIncreasePercent);


            // % Energy Provided by
            $FinalPercantageExport[$k] = 100 - ($FinalOffsetValue[$k] * 100) / $FinalEnergyProvidedBySolar[$k];

            // % Energy Provided by
            $FinalSupplyCharge[$k] = $supply_charge_data[$k];
            $FinalMeterCharge[$k] = $meter_charge_data[$k];

            $FinalWithoutStanding[$k] = $FinalSupplyCharge[$k] + $FinalMeterCharge[$k];

            $FinalTotalBillWithoutSolar[$k] = $FinalLoadChargesWithoutSolar[$k] + $FinalWithoutStanding[$k];
            $FinalTotalBillWithSolar[$k] = $FinalBillAfterSolar[$k] + $FinalWithoutStanding[$k];

            $NetPayBillAmount[$k] = $FinalTotalBillWithoutSolar[$k] - $FinalTotalBillWithSolar[$k];

            // if ($type == "LGC" && (count($lgc_pricing) - 1) >= $k) {
            //     $FinalLGCValues[$k] = ($FinalEnergyProvidedBySolar[$k] / 1000) * $lgc_pricing[$k];

            //     // Final Cummilative Savings
            //     if ($k == 0) {
            //         $FinalCummilativeSavings[$k] = $NetPayBillAmount[$k] + $FinalLGCValues[$k];
            //     } else {
            //         $FinalCummilativeSavings[$k] = ($FinalCummilativeSavings[$k - 1] + $NetPayBillAmount[$k]) + $FinalLGCValues[$k];
            //     }
            // } else {
            //     $FinalLGCValues[$k] = 0;

            //     // Final Cummilative Savings
            //     if ($k == 0) {
            //         $FinalCummilativeSavings[$k] = $NetPayBillAmount[$k];
            //     } else {
            //         $FinalCummilativeSavings[$k] = $FinalCummilativeSavings[$k - 1] + $NetPayBillAmount[$k];
            //     }
            // }

            if ($IsDemandCharge) {
                for ($i = 0; $i < count($consumption_data); $i++) {

                    $timestamp_peak = strtotime($consumption_date[$i]);
                    $day_peak = date('D', $timestamp_peak);
                    $m_month = $d_month = date("M", $timestamp_peak);
                    $m_month     = strtolower($m_month);

                    for ($dc = 0; $dc < count($demand_charges); $dc++) {
                        $temp_array = (array)$demand_charges[$dc];


                        if ($meter_data_type == 2) {
                            if (in_array($day_peak, $temp_array['days']) && in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DCWithSolarMonthlyData) && isset($DCWithSolarMonthlyData[$dc][$d_month])) {
                                    if ($DCWithSolarMonthlyData[$dc][$d_month] < $FinalKAVYearlyData[$k][$i]) {
                                        $DCWithSolarMonthlyData[$dc][$d_month] = $FinalKAVYearlyData[$k][$i];
                                    }
                                } else {
                                    $DCWithSolarMonthlyData[$dc][$d_month] =  $FinalKAVYearlyData[$k][$i];
                                }

                                $MaxDCWithSolar[$k] = $DCWithSolarMonthlyData[$dc];

                                $DemandChargeWithSolar[$k][$dc] = array_sum($DCWithSolarMonthlyData[$dc]) * $DemandChargeRate[$k][0];
                            }
                        } else {
                            if (in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DCWithSolarMonthlyData) && isset($DCWithSolarMonthlyData[$dc][$d_month])) {
                                    if ($DCWithSolarMonthlyData[$dc][$d_month] < $FinalKAVYearlyData[$k][$i]) {
                                        $DCWithSolarMonthlyData[$dc][$d_month] = $FinalKAVYearlyData[$k][$i];
                                    }
                                } else {
                                    $DCWithSolarMonthlyData[$dc][$d_month] =  $FinalKAVYearlyData[$k][$i];
                                }

                                $MaxDCWithSolar[$k] = $DCWithSolarMonthlyData[$dc];

                                $DemandChargeWithSolar[$k][$dc] = array_sum($DCWithSolarMonthlyData[$dc]) * $temp_array['rate'];
                            }
                        }
                    }
                }
            }
        }

        // $FinalIncreaseDemandData2 = [];
        if ($IsDemandCharge) {

            $FinalIncreaseDemandData =  array_sum($FinalIncreaseDemandData[0]);
            $DemandChargeWithSolar =  array_sum($DemandChargeWithSolar[0]);
        }


        $data["final_calculation"] = array(
            "FinalPercantageEnergyProvidedBySolar" => $FinalPercantageEnergyProvidedBySolar,
            "FinalEnergyProvidedBySolar" => $FinalEnergyProvidedBySolar,
            "FinalLoadChargesWithRates" => $FinalLoadChargesWithRates,
            "FinalBillAfterSolar" => $FinalBillAfterSolar,
            "FinalOffsetValue" => $FinalOffsetValue,
            "FinalPercantageExport" => $FinalPercantageExport,
            "FinalSupplyCharge" => $FinalSupplyCharge,
            "FinalMeterCharge" => $FinalMeterCharge,
            "FinalBillReduction" => $FinalBillReduction,
            "FinalCummilativeSavings" => $FinalCummilativeSavings,
            "FinalIncreaseRate" => $FinalIncreaseRate,
            "FinalWithoutStanding" => $FinalWithoutStanding,
            "FinalLoadChargesWithoutSolar" => $FinalLoadChargesWithoutSolar,
            "FinalPeakRate" => $FinalPeakRate,
            "FinalOffPeakRate" => $FinalOffPeakRate,
            "FinalShoulderRate" => $FinalShoulderRate,
            "FinalExportElectricty" => $FinalExportElectricty,
            "TotalSolar" => array_sum($production_data),
            "NetPayBillAmount" => $NetPayBillAmount,
            "FinalTotalBillWithoutSolar" => $FinalTotalBillWithoutSolar,
            "FinalTotalBillWithSolar" => $FinalTotalBillWithSolar,
            "FinalLGCValues" => $FinalLGCValues,
            "DemandMonthlyData" => $DemandMonthlyData,
            "FinalDemandMonthlyData" => $FinalDemandMonthlyData,
            "IsDemandCharge" => $IsDemandCharge,
            "FinalFlatRate" => $FinalFlatRate,
            "BillType" => $BillType,
            "FinalIncreaseDemandData" => $FinalIncreaseDemandData,
            "DemandChargeWithSolar" => $DemandChargeWithSolar,
            "consumption_data" => $consumption_data,
            "NewLoadDifference" => array(
                "PeakDifference" => $PeakDifference,
                "OffPeakDifference" => $OffPeakDifference,
                "ShoulderDifference" => $ShoulderDifference,
                "FlatChargeDifference" => $FlatChargeDifference
            ),
            "AverageDailyConsumption" => $AverageDailyConsumption,
            "NewLoad" => $NewLoad,
            "type" => $type,
            "data_length" => $data["data_length"]
        );
        // $upload_financial_data = [];
        // $upload_financial_data['degradation_data'] = json_encode($data["final_calculation"]);
        // if (count($financial_summary_data)) {
        //     $stat2 = $this->common->update_data('tbl_solar_proposal_financial_summary', array('proposal_id' => $proposal_id), $upload_financial_data);
        // } else {
        //     $upload_financial_data['proposal_id'] = $proposal_id;
        //     $stat2 = $this->common->insert_data('tbl_solar_proposal_financial_summary', $upload_financial_data);
        // }
        // $data['financial_summary_data'] = count($financial_summary_data);

        return $data;
    }

    public function calculate_data_with_degradation_factor($proposal_id, $return_status)
    {

        $data = array();
        $stat = true;
        $calculate_years = 25;
        $data['calculate_years'] = $calculate_years;
        // Getting Proposal Data
        // $proposal_id =  $this->input->get('proposal_id');
        $proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('id' => $proposal_id));

        $data['bill_type'] = $bill_type = $proposal_data['bill_type'];
        $data['meter_data_type'] = $meter_data_type = $proposal_data['meter_data_type'];
        $data['flat_rates'] = $flat_rates = json_decode($proposal_data['flat_rates']);
        $data['is_demand_charge'] = $IsDemandCharge = $proposal_data['is_demand_charge'];
        $BillType = $proposal_data['bill_type'];
        // Getting Degradation factor

        if (empty($proposal_data['pvwatts_data'])) {
            $data['success']      = false;
            $data['status']      = "Solar Production is missing";
            echo json_encode($data);
            return false;
        }
        $pvwatts_data = json_decode($proposal_data['pvwatts_data']);
        $data['degradation_factor'] = $degradation_factor = 1 - ($pvwatts_data[0]->pv_degradation_factor / 100);

        // Getting Rates
        $data['meter_data_peak'] = $meter_data_peak = (float)$proposal_data['solar_cost_per_kwh_peak'];
        $data['meter_data_off_peak'] = $meter_data_off_peak = (float)$proposal_data['solar_cost_per_kwh_off_peak'];
        $data['meter_data_shoulder'] = $meter_data_shoulder = (float)$proposal_data['solar_cost_per_kwh_shoulder'];

        // Getting Time of use Data
        if (empty($proposal_data['time_of_use'])) {
            $data['success']      = false;
            $data['status']      = "Bill Details data is missing.";
            echo json_encode($data);
            return false;
        }
        $time_of_use = json_decode($proposal_data['time_of_use']);
        $data['tou_time'] = $tou_time = $time_of_use->start_time;
        $data['tou_weekdays'] = $tou_weekdays = $time_of_use->weekdays;
        $data['tou_sat'] = $tou_sat = $time_of_use->sat;
        $data['tou_sun'] = $tou_sun = $time_of_use->sun;

        $type = 'STC';

        $data['financial_summary'] = $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));
        if (count($financial_summary_data)) {
            $type = $financial_summary_data['type'];

            if ($type == "LGC") {
                if (empty($financial_summary_data['lgc_pricing'])) {
                    $data['success']      = false;
                    $data['status']      = "Financial Summary data is missing.";
                    echo json_encode($data);
                    return false;
                }
                $data['lgc_pricing'] = $lgc_pricing = json_decode($financial_summary_data['lgc_pricing']);
            }
        }

        if (empty($proposal_data['time_of_use'])) {
            $data['success']      = false;
            $data['status']      = "Bill Details data is missing.";
            echo json_encode($data);
            return false;
        }

        // Getting Consumption Data
        if (empty($proposal_data['consumption_data'])) {
            $data['success']      = false;
            $data['status']      = "Meter data is missing";
            echo json_encode($data);
            return false;
        }
        $consumption = json_decode($proposal_data['consumption_data']);
        $data['consumption_data'] = $consumption_data = $consumption->consumption_data;
        $data['consumption_time'] = $consumption_time = $consumption->consumption_time;
        $data['consumption_date'] = $consumption_date = $consumption->consumption_date;

        if ($IsDemandCharge) {
            $data['demand_data'] = $demand_data = $consumption->demand_data;
        }
        $data['data_length'] = count($consumption_data);
        $data['old_data'] = $consumption_data;

        // If Demand charges
        if ($IsDemandCharge) {
            $data['demand_charges'] = $demand_charges = (array)json_decode($proposal_data['demand_charges']);
        }

        if (empty($proposal_data['production_data'])) {
            $data['success']      = false;
            $data['status']      = "Solar Production is missing";
            echo json_encode($data);
            return false;
        }

        // Getting Production data
        $data['production_data'] = $production_data = json_decode($proposal_data['production_data']);

        $data['production_data_sum'] = array_sum($production_data);
        // Getting Standard Charges
        $data['additonal_charges'] = $standard_charges = json_decode($proposal_data['standard_charges']);
        $data['supply_charge'] = $supply_charge = $standard_charges->supply->charge * $standard_charges->supply->per;
        $data['meter_charge'] = $meter_charge  = $standard_charges->meter->charge * $standard_charges->meter->per;

        $other_charge = 0;
        if (isset($standard_charges->other)) {
            $data['other_charge'] = $other_charge  = $standard_charges->other->charge;
        }

        $utility_value_export = (float)$standard_charges->utility_value_export;

        // Getting Subsequent and Years Data
        $data["price_annual_increase"] =  $price_annual_increase = json_decode($proposal_data['price_annual_increase']);
        $MeterDataPiaYear = $price_annual_increase->MeterDataPiaYear;
        $MeterDataPiaPercentage = $price_annual_increase->MeterDataPiaPercentage;
        $MeterDataSubsequent = $price_annual_increase->MeterDataSubsequent;
        $LoadMultiplyValue = 0;

        $meter_data_demand_charge = 0;
        $total_meter_charges = 0;
        $nrel_calculation = [];
        $FinalTotalLoadMeterData = 0;
        $FinalIncreaseRate = [];
        $FinalBillReduction = [];
        $FinalCummilativeSavings = [];
        $FinalBillAfterSolar = [];
        $FinalOffsetValue = [];
        $FinalPercantageExport = [];
        $FinalEnergyProvidedBySolar = [];
        $FinalPercantageEnergyProvidedBySolar = [];
        $FinalLoadChargesWithRates = [];
        $FinalSupplyCharge = [];
        $FinalMeterCharge = [];
        $FinalPeakRate = [];
        $FinalFlatRate = [];
        $FinalOffPeakRate = [];
        $FinalShoulderRate = [];
        $FinalExportElectricty = [];
        $FinalTotalBillWithoutSolar = [];
        $DemandMonthlyData = [];
        $DemandChargeRate = [];
        $DemandChargeWithSolar = [];
        $FinalDemandMonthlyData = [];
        $FinalIncreaseDemandData = [];
        $FinalTotalBillWithSolar = [];
        $FinalOtherCharge = [];
        $OtherChargeBeforeSolar = [];
        $OtherChargeAfterSolar = [];
        $FinalLGCValues = [];
        $PowerFactor = [];
        $FinalKAVYearlyData = [];
        $NewLoad = [];
        $AnnualSolarProduction = array_sum($production_data);

        $data['sum_load_data'] = $sum_load_data = array_sum($consumption_data);
        $production_calculated_data = [];
        $load_calculated_data = [];
        $bill_after_solor = [];
        $offset_value = [];
        $supply_charge_data = [];
        $meter_charge_data = [];
        $decidingFactor = [];

        $LoadMultiplyValue = ($MeterDataPiaPercentage / 100);
        $LoadRateForMultiply = 0;

        // Checking for No meter data TOU
        $PeakDifference = 0;
        $OffPeakDifference = 0;
        $ShoulderDifference = 0;
        $FlatChargeDifference = 0;
        $AverageDailyConsumption = [];
        $loadIncreasePercent = 1;
        if ($meter_data_type == "1") {

            $TOUBillingMonth =  $flat_rates->billing_month;
            if ($BillType != "flat") {
                $TOUPeakRate = $flat_rates->peak_kwh;
                $TOUPeakRateArray = [];
                $TOUShoulderRate =  $flat_rates->shoulder_kwh;
                $TOUShoulderRateArray = [];
                $TOUOffPeakRate =  $flat_rates->off_peak_kwh;
                $TOUOffPeakRateArray = [];
            } else {
                $FlatChargeRate =  (float)$flat_rates->charge;
                $FlatChargeRateArray = [];
            }

            $dt = 1;
            $DailyConsumption = 0;
            for ($i = 0; $i < count($consumption_data); $i++) {
                $timestamp_peak = strtotime($consumption_date[$i]);
                $month_int = date('m', $timestamp_peak);
                $day_peak = date('D', $timestamp_peak);
                $m_month = $d_month = date("M", $timestamp_peak);
                $m_month     = strtolower($m_month);
                $m_year = date("Y", $timestamp_peak);
                $meter_date_time = strtotime($consumption_time[$i]);

                // Getting Difference if consumption inputs 'Simple'
                if ($meter_data_type == 1 && $bill_type !== 'flat') {

                    if ($flat_rates->consumption_inputs == "Simple") {
                        $DailyConsumption = $DailyConsumption + $consumption_data[$i];

                        // $no_of_days_24hr = $this->components->days_in_month($month_int, $m_year);

                        // $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00'
                        if ($consumption_time[$i] == '23:00') {
                            $AverageDailyConsumption[] = $DailyConsumption;
                            // $AverageDailyConsumption[$dt] = $dt;
                            $DailyConsumption = 0;
                            $dt++;
                        }
                    }
                }

                $findIndex = array_search($consumption_time[$i], $tou_time);

                if ($m_month == strtolower($TOUBillingMonth)) {


                    if ($BillType != "flat") {

                        if ($day_peak != 'Sat' && $day_peak != 'Sun') {
                            // Peak -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Peak') {

                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Weekdays
                            if ($tou_weekdays[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }
                        if ($day_peak == 'Sat') {
                            // Peak -- Sat
                            if ($tou_sat[$findIndex] == 'Peak') {

                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Sat
                            if ($tou_sat[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Sat
                            if ($tou_sat[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }

                        if ($day_peak == 'Sun') {
                            // Peak -- Sat
                            if ($tou_sun[$findIndex] == 'Peak') {
                                $TOUPeakRateArray[] = $consumption_data[$i];
                            }

                            // Shoulder -- Sat
                            if ($tou_sun[$findIndex] == 'Shoulder') {
                                $TOUShoulderRateArray[] = $consumption_data[$i];
                            }

                            // Off-Peak -- Sat
                            if ($tou_sun[$findIndex] == 'Off-Peak') {
                                $TOUOffPeakRateArray[] = $consumption_data[$i];
                            }
                        }
                    } else {
                        $FlatChargeRateArray[] = $consumption_data[$i];
                    }
                }
            }

            if ($BillType != "flat") {

                // Replace FlatChargeDifference if consumption inputs 'Simple'
                if ($meter_data_type == 1 && $flat_rates->consumption_inputs == "Simple") {
                    $FlatChargeDifference = ($flat_rates->daily_average_consumption * 100) / (array_sum($AverageDailyConsumption) / 365);
                    $FlatChargeDifference = $FlatChargeDifference - 100;

                    if(array_sum($AverageDailyConsumption) > 0 ) {
                        $loadIncreasePercent = $flat_rates->daily_average_consumption / (array_sum($AverageDailyConsumption) / 365) ;
                    }

                } else {

                    if ((int)$TOUPeakRateArray) {
                        $PeakDifference = ($TOUPeakRate * 100) / array_sum($TOUPeakRateArray);
                        $PeakDifference = $PeakDifference - 100;
                    }

                    if ((int)$TOUShoulderRateArray) {
                        $ShoulderDifference = ($TOUShoulderRate * 100) / array_sum($TOUShoulderRateArray);
                        $ShoulderDifference = $ShoulderDifference - 100;
                    }

                    if ((int)$TOUOffPeakRateArray) {
                        $OffPeakDifference = ($TOUOffPeakRate * 100) / array_sum($TOUOffPeakRateArray);
                        $OffPeakDifference = $OffPeakDifference - 100;
                    }
                }
            } else {

                if ((int)$FlatChargeRateArray) {
                    $FlatChargeDifference = ($flat_rates->quantity * 100) / array_sum($FlatChargeRateArray);
                    $FlatChargeDifference = $FlatChargeDifference - 100;
                }
            }
        }



        for ($k = 0; $k < $calculate_years; $k++) {

            if ($k > $MeterDataPiaYear) {
                $LoadMultiplyValue = ($MeterDataSubsequent / 100);
            }

            $FinalIncreaseRate[$k] = $LoadMultiplyValue * 100;

            if ($k == 0) {
                $FinalPeakRate[$k] = $meter_data_peak;
                $FinalOffPeakRate[$k] = $meter_data_off_peak;
                $FinalShoulderRate[$k] = $meter_data_shoulder;

                if ($bill_type === 'flat') {
                    if ($meter_data_type == 2) {
                        $FinalFlatRate[$k] = (float)$flat_rates->charge;
                        $flat_rate_charge = (float)$flat_rates->charge;
                    } else {
                        $FinalFlatRate[$k] = (float)$flat_rates->energy_rate;
                        $flat_rate_charge = (float)$flat_rates->energy_rate;
                    }
                }
            } else {
                $FinalPeakRate[$k] = $FinalPeakRate[$k - 1] + ($FinalPeakRate[$k - 1] * $LoadMultiplyValue);
                $FinalOffPeakRate[$k] = $FinalOffPeakRate[$k - 1] + ($FinalOffPeakRate[$k - 1] * $LoadMultiplyValue);
                $FinalShoulderRate[$k] = $FinalShoulderRate[$k - 1] + ($FinalShoulderRate[$k - 1] * $LoadMultiplyValue);

                $meter_data_peak = $FinalPeakRate[$k];
                $meter_data_off_peak = $FinalOffPeakRate[$k];
                $meter_data_shoulder = $FinalShoulderRate[$k];

                if ($bill_type === 'flat') {
                    if ($meter_data_type == 2) {
                        $FinalFlatRate[$k] = $FinalFlatRate[$k - 1] + ($FinalOffPeakRate[$k - 1] * $LoadMultiplyValue);
                        $flat_rate_charge = $FinalFlatRate[$k];
                    } else {
                        $FinalFlatRate[$k] = $FinalFlatRate[$k - 1] + ($FinalFlatRate[$k - 1] * $LoadMultiplyValue);
                        $flat_rate_charge = $FinalFlatRate[$k];
                    }
                }
            }

            for ($i = 0; $i < count($consumption_data); $i++) {

                // peak charges
                $timestamp_peak = strtotime($consumption_date[$i]);
                $month_int = date('m', $timestamp_peak);
                $day_peak = date('D', $timestamp_peak);
                $m_month = $d_month = date("M", $timestamp_peak);
                $m_month     = strtolower($m_month);
                $m_year = date("Y", $timestamp_peak);
                $meter_date_time = strtotime($consumption_time[$i]);

                // begin:: demand charge
                if ($IsDemandCharge) {

                    if ($meter_data_type == 2) {
                        $PowerFactor[$k][$i] = ($demand_data[$i] != 0) ? ($consumption_data[$i] / $demand_data[$i]) : $demand_data[$i];
                    }

                    for ($dc = 0; $dc < count($demand_charges); $dc++) {
                        $temp_array = (array)$demand_charges[$dc];

                        if ($meter_data_type == 2) {
                            if (in_array($day_peak, $temp_array['days']) && in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DemandMonthlyData) && isset($DemandMonthlyData[$dc][$d_month])) {
                                    if ($DemandMonthlyData[$dc][$d_month] < $demand_data[$i]) {
                                        $DemandMonthlyData[$dc][$d_month] = $demand_data[$i];
                                    }
                                } else {
                                    $DemandMonthlyData[$dc][$d_month] =  $demand_data[$i];
                                }

                                if ($k == 0) {
                                    $DemandChargeRate[$k][$dc] = $temp_array['rate'];
                                    $FinalIncreaseDemandData[$k][$dc] = array_sum($DemandMonthlyData[$dc]) * $temp_array['rate'];
                                } else {
                                    $DemandChargeRate[$k][$dc] = $DemandChargeRate[$k - 1][$dc] + ($DemandChargeRate[$k - 1][$dc] * $LoadMultiplyValue);
                                    $FinalIncreaseDemandData[$k][$dc] = $FinalIncreaseDemandData[$k - 1][$dc] + ($FinalIncreaseDemandData[$k - 1][$dc] *  $LoadMultiplyValue);
                                }
                            }
                        } else {
                            if (in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                $DemandMonthlyData[$dc][$d_month] =  $temp_array['charge'];

                                if ($k == 0) {
                                    $DemandChargeRate[$k][$dc] = $temp_array['rate'];
                                    $FinalIncreaseDemandData[$k][$dc] = array_sum($DemandMonthlyData[$dc]);
                                } else {
                                    $DemandChargeRate[$k][$dc] = $DemandChargeRate[$k - 1][$dc] + ($DemandChargeRate[$k - 1][$dc] * $LoadMultiplyValue);
                                    $FinalIncreaseDemandData[$k][$dc] = $FinalIncreaseDemandData[$k - 1][$dc] + ($FinalIncreaseDemandData[$k - 1][$dc] *  $LoadMultiplyValue);
                                }
                            }
                        }
                    }
                }

                // end:: demand charge
                if ($k == 0) {
                    $NewLoad[$i] = $consumption_data[$i];
                }

                $MultiplyValueForBillAfterSolar = 0;
                $findIndex = array_search($consumption_time[$i], $tou_time);

                if ($day_peak != 'Sat' && $day_peak != 'Sun') {

                    // Peak -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Peak') {

                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;

                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Weekdays
                    if ($tou_weekdays[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($day_peak == 'Sat') {

                    // Peak -- Sat
                    if ($tou_sat[$findIndex] == 'Peak') {
                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Sat
                    if ($tou_sat[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;


                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Sat
                    if ($tou_sat[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }

                if ($day_peak == 'Sun') {

                    // Peak -- Sat
                    if ($tou_sun[$findIndex] == 'Peak') {
                        $LoadRateForMultiply = $meter_data_peak;

                        if ($k == 0) {
                            if ($PeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($PeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Shoulder -- Sat
                    if ($tou_sun[$findIndex] == 'Shoulder') {
                        $LoadRateForMultiply = $meter_data_shoulder;

                        if ($k == 0) {
                            if ($ShoulderDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($ShoulderDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }

                    // Off-Peak -- Sat
                    if ($tou_sun[$findIndex] == 'Off-Peak') {
                        $LoadRateForMultiply = $meter_data_off_peak;

                        if ($k == 0) {
                            if ($OffPeakDifference != 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($OffPeakDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                }


                if ($k == 0) {
                    if ($bill_type === 'flat') {
                        // $NewLoad[$i] = $consumption_data[$i];
                        if ($meter_data_type == "1") {
                            if ($FlatChargeDifference > 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            } else {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            }
                        }
                        $LoadRateForMultiply =  $flat_rate_charge;
                    } else {
                        // $NewLoad[$i] = $consumption_data[$i];
                        if ($meter_data_type == "1") {
                            if ($FlatChargeDifference > 0) {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            } else {
                                $consumption_data[$i] = ($consumption_data[$i] + (($FlatChargeDifference * $consumption_data[$i]) / 100));
                            }
                        }
                    }
                } else {
                    if ($bill_type === 'flat') {
                        $LoadRateForMultiply =  $flat_rate_charge;
                    }
                }

                if ($k == 0) {

                    // if ($other_charge != '' || $other_charge != 0) {
                    //     $consumption_data[$i] = $consumption_data[$i] * $other_charge;
                    // }

                    $MultiplyValueForBillAfterSolar = $LoadRateForMultiply;
                    $FinalExportElectricty[$k] = $utility_value_export;

                    // Production calculation
                    $data['production_calculated_data'][$k][$i] = $production_calculated_data[$k][$i] = $production_data[$i];

                    // Load calculatoin
                    $FinalOtherCharge[$k] = $other_charge;
                    // if ($other_charge != '') {
                    $load_calculated_data[$k][$i] = ($consumption_data[$i] * $LoadRateForMultiply);
                    $OtherChargeBeforeSolar[$k][$i] = $consumption_data[$i] * $other_charge;
                    $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = $OtherChargeBeforeSolar[$k][$i] + $load_calculated_data[$k][$i];
                    // } else {
                    // $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = ($consumption_data[$i] * $LoadRateForMultiply);
                    // }



                    $SubstractForBillAfterSolar = $consumption_data[$i] - $production_data[$i];
                    $decidingFactor[$k][$i] = $SubstractForBillAfterSolar;

                    if ($IsDemandCharge) {
                        if ($meter_data_type == 2) {
                            $FinalKAVYearlyData[$k][$i] = ($PowerFactor[$k][$i] != 0) ? ($SubstractForBillAfterSolar / $PowerFactor[$k][$i]) : $PowerFactor[$k][$i];
                        } else {
                            $FinalKAVYearlyData[$k][$i] = $decidingFactor[$k][$i] / 0.95;
                        }
                    }

                    // Bill After Solar
                    if ($SubstractForBillAfterSolar > 0) {
                        // if ($other_charge != '') {
                        $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                        $OtherChargeAfterSolar[$k][$i] = $SubstractForBillAfterSolar * $other_charge;
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $OtherChargeAfterSolar[$k][$i] + $bill_after_solor[$k][$i];
                        // } else {
                        //     $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                        // }
                    } else {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $utility_value_export;
                    }

                    // Energy Provided By Solar
                    // This will be (min load and production)
                    if ($consumption_data[$i] > $production_data[$i]) {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $production_data[$i];
                    } else {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $consumption_data[$i];
                    }

                    // Supply Charge
                    $data['supply_charge_data'][$k] = $supply_charge_data[$k] = $supply_charge;

                    // Meter Charge
                    $data['meter_charge_data'][$k] = $meter_charge_data[$k] = $meter_charge;
                } else {
                    $MultiplyValueForBillAfterSolar = $LoadRateForMultiply;
                    $FinalExportElectricty[$k] = $utility_value_export;

                    // Production calculation
                    $data['production_calculated_data'][$k][$i] = $production_calculated_data[$k][$i] = $production_calculated_data[$k - 1][$i] * $degradation_factor;

                    // Load calculatoin
                    $FinalOtherCharge[$k] = $other_charge;
                    $OtherChargeBeforeSolar[$k][$i] = $consumption_data[$i] * $other_charge;
                    $data['load_calculated_data'][$k][$i] = $load_calculated_data[$k][$i] = $load_calculated_data[$k - 1][$i] + ($load_calculated_data[$k - 1][$i] * $LoadMultiplyValue);

                    $SubstractForBillAfterSolar = $consumption_data[$i] - $production_calculated_data[$k][$i];
                    $decidingFactor[$k][$i] = $SubstractForBillAfterSolar;

                    if ($IsDemandCharge) {
                        if ($meter_data_type == 2) {
                            $FinalKAVYearlyData[$k][$i] = ($PowerFactor[$k][$i] != 0) ? ($decidingFactor[$k][$i] / $PowerFactor[$k][$i]) : $PowerFactor[$k][$i];
                        } else {
                            $FinalKAVYearlyData[$k][$i] = $decidingFactor[$k][$i] / 0.95;
                        }
                    }

                    // Bill After Solar
                    if ($SubstractForBillAfterSolar > 0) {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $MultiplyValueForBillAfterSolar;
                        $OtherChargeAfterSolar[$k][$i] = $SubstractForBillAfterSolar * $other_charge;
                    } else {
                        $data['bill_after_solor'][$k][$i] = $bill_after_solor[$k][$i] = $SubstractForBillAfterSolar * $utility_value_export;
                    }

                    // Energy Provided By Solar
                    // This will be (min load and production)
                    if ($consumption_data[$i] > $production_calculated_data[$k][$i]) {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $production_calculated_data[$k][$i];
                    } else {
                        $data['offset_value'][$k][$i] = $offset_value[$k][$i] = $consumption_data[$i];
                    }

                    // Supply Charge
                    $data['supply_charge_data'][$k] = $supply_charge_data[$k] = $supply_charge_data[$k - 1] + ($supply_charge_data[$k - 1] * $LoadMultiplyValue);

                    // Meter Charge
                    $data['meter_charge_data'][$k] = $meter_charge_data[$k] = $meter_charge_data[$k - 1] + ($meter_charge_data[$k - 1] * $LoadMultiplyValue);
                }
            }
        }

        // print_r($FinalIncreaseDemandData);
        // die;

        $FinalLoadChargesWithoutSolar = [];
        $FinalWithoutStanding = [];
        $NetPayBillAmount = [];
        $DCWithSolarMonthlyData = [];
        $FinalOtherChargeBeforeSolar = [];
        $FinalOtherChargeAfterSolar = [];
        $MaxDCWithSolar = [];
        for ($k = 0; $k < $calculate_years; $k++) {
            $FinalOtherChargeBeforeSolar[$k] = array_sum($OtherChargeBeforeSolar[$k]);
            $FinalOtherChargeAfterSolar[$k] = array_sum($OtherChargeAfterSolar[$k]);
            $FinalEnergyProvidedBySolar[$k] = array_sum($production_calculated_data[$k]);
            $FinalLoadChargesWithRates[$k] = array_sum($load_calculated_data[$k]) + $supply_charge + $meter_charge;
            $FinalLoadChargesWithoutSolar[$k] = array_sum($load_calculated_data[$k]);
            $FinalBillAfterSolar[$k] = array_sum($bill_after_solor[$k]);
            $FinalOffsetValue[$k] = array_sum($offset_value[$k]);
            // Bill Reduction
            $FinalBillReduction[$k] = $FinalLoadChargesWithRates[$k] - $FinalBillAfterSolar[$k];

            // % Energy Provided by Solar
            // Formula is (consumption*100)/(sum of load)
            $FinalPercantageEnergyProvidedBySolar[$k] = ($FinalOffsetValue[$k] * 100) / ($sum_load_data*$loadIncreasePercent);


            // % Energy Provided by
            $FinalPercantageExport[$k] = 100 - ($FinalOffsetValue[$k] * 100) / $FinalEnergyProvidedBySolar[$k];

            // % Energy Provided by
            $FinalSupplyCharge[$k] = $supply_charge_data[$k];
            $FinalMeterCharge[$k] = $meter_charge_data[$k];

            $FinalWithoutStanding[$k] = $FinalSupplyCharge[$k] + $FinalMeterCharge[$k];

            $FinalTotalBillWithoutSolar[$k] = $FinalLoadChargesWithoutSolar[$k] + $FinalWithoutStanding[$k];
            $FinalTotalBillWithSolar[$k] = $FinalBillAfterSolar[$k] + $FinalWithoutStanding[$k];

            $NetPayBillAmount[$k] = $FinalTotalBillWithoutSolar[$k] - $FinalTotalBillWithSolar[$k];

            if ($type == "LGC" && (count($lgc_pricing) - 1) >= $k) {
                $FinalLGCValues[$k] = ($FinalEnergyProvidedBySolar[$k] / 1000) * $lgc_pricing[$k];

                // Final Cummilative Savings
                if ($k == 0) {
                    $FinalCummilativeSavings[$k] = $NetPayBillAmount[$k] + $FinalLGCValues[$k];
                } else {
                    $FinalCummilativeSavings[$k] = ($FinalCummilativeSavings[$k - 1] + $NetPayBillAmount[$k]) + $FinalLGCValues[$k];
                }
            } else {
                $FinalLGCValues[$k] = 0;

                // Final Cummilative Savings
                if ($k == 0) {
                    $FinalCummilativeSavings[$k] = $NetPayBillAmount[$k];
                } else {
                    $FinalCummilativeSavings[$k] = $FinalCummilativeSavings[$k - 1] + $NetPayBillAmount[$k];
                }
            }



            // if ($IsDemandCharge) {
            //     for ($dc = 0; $dc < count($demand_charges); $dc++) {
            //         $temp_array = (array)$demand_charges[$dc];
            //         foreach ($DemandMonthlyData[$dc] as $key => $value) {
            //             $FinalDemandMonthlyData[$dc][$key] = $value * $temp_array['rate'];
            //         }
            //     }
            // }

            if ($IsDemandCharge) {
                for ($i = 0; $i < count($consumption_data); $i++) {

                    $timestamp_peak = strtotime($consumption_date[$i]);
                    $day_peak = date('D', $timestamp_peak);
                    $m_month = $d_month = date("M", $timestamp_peak);
                    $m_month     = strtolower($m_month);

                    for ($dc = 0; $dc < count($demand_charges); $dc++) {
                        $temp_array = (array)$demand_charges[$dc];


                        if ($meter_data_type == 2) {
                            if (in_array($day_peak, $temp_array['days']) && in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DCWithSolarMonthlyData) && isset($DCWithSolarMonthlyData[$dc][$d_month])) {
                                    if ($DCWithSolarMonthlyData[$dc][$d_month] < $FinalKAVYearlyData[$k][$i]) {
                                        $DCWithSolarMonthlyData[$dc][$d_month] = $FinalKAVYearlyData[$k][$i];
                                    }
                                } else {
                                    $DCWithSolarMonthlyData[$dc][$d_month] =  $FinalKAVYearlyData[$k][$i];
                                }

                                $MaxDCWithSolar[$k] = $DCWithSolarMonthlyData[$dc];

                                $DemandChargeWithSolar[$k][$dc] = array_sum($DCWithSolarMonthlyData[$dc]) * $DemandChargeRate[$k][0];
                            }
                        } else {
                            if (in_array($d_month, $temp_array['months']) && ($temp_array['start_time'] <= $consumption_time[$i] && $consumption_time[$i] <= $temp_array['end_time'])) {
                                if (count($DCWithSolarMonthlyData) && isset($DCWithSolarMonthlyData[$dc][$d_month])) {
                                    if ($DCWithSolarMonthlyData[$dc][$d_month] < $FinalKAVYearlyData[$k][$i]) {
                                        $DCWithSolarMonthlyData[$dc][$d_month] = $FinalKAVYearlyData[$k][$i];
                                    }
                                } else {
                                    $DCWithSolarMonthlyData[$dc][$d_month] =  $FinalKAVYearlyData[$k][$i];
                                }

                                $MaxDCWithSolar[$k] = $DCWithSolarMonthlyData[$dc];

                                $DemandChargeWithSolar[$k][$dc] = array_sum($DCWithSolarMonthlyData[$dc]) * $temp_array['rate'];
                            }
                        }
                    }
                }

                $FinalIncreaseDemandData[$k] = array_sum($FinalIncreaseDemandData[$k]);
                $DemandChargeWithSolar[$k] = array_sum($DemandChargeWithSolar[$k]);
            }
        }


        $data["final_calculation"] = array(
            "FinalPercantageEnergyProvidedBySolar" => $FinalPercantageEnergyProvidedBySolar,
            "FinalEnergyProvidedBySolar" => $FinalEnergyProvidedBySolar,
            "FinalLoadChargesWithRates" => $FinalLoadChargesWithRates,
            "FinalBillAfterSolar" => $FinalBillAfterSolar,
            "FinalOffsetValue" => $FinalOffsetValue,
            "FinalPercantageExport" => $FinalPercantageExport,
            "FinalSupplyCharge" => $FinalSupplyCharge,
            "FinalMeterCharge" => $FinalMeterCharge,
            // "FinalBillReduction" => $FinalBillReduction,
            "FinalCummilativeSavings" => $FinalCummilativeSavings,
            "FinalIncreaseRate" => $FinalIncreaseRate,
            "FinalWithoutStanding" => $FinalWithoutStanding,
            "FinalLoadChargesWithoutSolar" => $FinalLoadChargesWithoutSolar,
            "FinalPeakRate" => $FinalPeakRate,
            "FinalOffPeakRate" => $FinalOffPeakRate,
            "FinalShoulderRate" => $FinalShoulderRate,
            "FinalExportElectricty" => $FinalExportElectricty,
            "TotalSolar" => array_sum($production_data),
            "AverageDailyConsumption" => array_sum($AverageDailyConsumption) / 365,
            "NetPayBillAmount" => $NetPayBillAmount,
            // "FinalTotalBillWithoutSolar" => $FinalTotalBillWithoutSolar,
            "FinalTotalBillWithSolar" => $FinalTotalBillWithSolar,
            // "MaxDCWithSolar" => $MaxDCWithSolar,
            "FinalLGCValues" => $FinalLGCValues,
            "DemandMonthlyData" => $DemandMonthlyData,
            "FinalDemandMonthlyData" => $FinalDemandMonthlyData,
            "IsDemandCharge" => $IsDemandCharge,
            "FinalFlatRate" => $FinalFlatRate,
            "BillType" => $BillType,
            "FinalIncreaseDemandData" => $FinalIncreaseDemandData,
            "DemandChargeRate" => $DemandChargeRate,
            "FinalKAVYearlyData" => $FinalKAVYearlyData,
            "DemandChargeWithSolar" => $DemandChargeWithSolar,
            // "decidingFactor" => $decidingFactor,
            // "consumption_data" => $consumption_data,
            "FinalOtherCharge" => $FinalOtherCharge,
            "FinalOtherChargeBeforeSolar" => $FinalOtherChargeBeforeSolar,
            "FinalOtherChargeAfterSolar" => $FinalOtherChargeAfterSolar,
            "consumption_data" => $consumption_data,
            "type" => $type,
            "data_length" => $data["data_length"],
            // "old_data" => $NewLoad,
            // "FlatChargeDifference" => $FlatChargeDifference
        );

        // "decidingFactor" => $decidingFactor,
        // "PowerFactor" => $PowerFactor,
        // "FinalKAVYearlyData" => $FinalKAVYearlyData,

        // "NewLoadDifference" => array(
        //     "PeakDifference" => $PeakDifference,
        //     "OffPeakDifference" => $OffPeakDifference,
        //     "ShoulderDifference" => $ShoulderDifference,
        //     "FlatChargeDifference" => $FlatChargeDifference
        // ),

        $upload_financial_data['degradation_data'] = json_encode($data["final_calculation"]);
        if (count($financial_summary_data)) {
            $stat2 = $this->common->update_data('tbl_solar_proposal_financial_summary', array('proposal_id' => $proposal_id), $upload_financial_data);
        } else {
            $stat2 = $this->common->insert_data('tbl_solar_proposal_financial_summary', $upload_financial_data);
        }
        $data['financial_summary_data'] = count($financial_summary_data);
        $data['success']      = true;
        $data['calculate_years'] = $calculate_years;

        if ($return_status == 1) {
            return $data;
            die;
        } else {
            echo json_encode($data);
            die;
        }
    }

    public function view_data_with_degradation_factor($proposal_id, $return_status)
    {
        // $proposal_id =  $this->input->post('proposal_id');

        $data['final_calculation'] = $financial_summary_data = json_decode($this->common->fetch_cell('tbl_solar_proposal_financial_summary', 'degradation_data', array('proposal_id' =>  $proposal_id)));
        // $data['financial_summary'] = $financial_summary_data = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));

        // $data['financial_summary_data'] = count($financial_summary_data);
        $data['success']      = true;
        $data['calculate_years']      = 25;
        echo json_encode($data);
        die;
    }

    public function calculate_demand_charge()
    {
        $data = array();

        $highestValue = 0;
    }

    public function calcualte_summer_and_weekend_load_graph_data_from_meter_data2(
        $meter_data,
        $solar_data,
        $IsDemandCharge
    ) {

        $MonthsArray = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
        $full_year_meter_data       = $meter_data['consumption_data'];
        $full_year_meter_data_dates = $meter_data['consumption_date'];
        $full_year_meter_data_times = $meter_data['consumption_time'];

        if ($IsDemandCharge == "1") {
            $full_year_demand_data = $meter_data['demand_data'];
        }

        $DaysInMonth = [];
        $CalculateOffsetConsumption = [];
        $CalculateOffsetConsumptionSum = [];


        $final_avg_load_graph_data = [];
        $avg_load_graph_data = [];

        $final_avg_export_graph_data = [];
        $avg_export_prd_graph_data = [];

        $final_avg_offset_graph_data = [];
        $avg_offset_prd_graph_data = [];

        $final_avg_sol_prd_graph_data = [];
        $avg_sol_prd_graph_data = [];

        foreach ($full_year_meter_data as $key => $value) {

            $month_int = date('m', strtotime($full_year_meter_data_dates[$key]));
            $year      = date('Y', strtotime($full_year_meter_data_dates[$key]));
            $month     = date('M', strtotime($full_year_meter_data_dates[$key]));
            $month     = strtolower($month);
            $hr        = (int) date('H', strtotime($full_year_meter_data_times[$key]));

            if (!isset($avg_load_graph_data[$month][$hr])) {
                $avg_load_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_export_prd_graph_data[$month][$hr])) {
                $avg_export_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_offset_prd_graph_data[$month][$hr])) {
                $avg_offset_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_sol_prd_graph_data[$month][$hr])) {
                $avg_sol_prd_graph_data[$month][$hr] = [];
            }

            $no_of_days_24hr = $this->components->days_in_month($month_int, $year);

            $avg_load_graph_data[$month][$hr][] = $value;


            if (isset($solar_data[$key])) {

                // solar production
                $avg_sol_prd_graph_data[$month][$hr][] = $solar_data[$key];

                // export to grid
                $avg_export_prd_graph_data[$month][$hr][] = $solar_data[$key];
            }

            // Total Offset
            if ($value > $solar_data[$key]) {
                $CalculateOffsetConsumption[$month][$hr][] = $solar_data[$key];
            } else {
                $CalculateOffsetConsumption[$month][$hr][] = $value;
            }


            if ($full_year_meter_data_dates[$key] == $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00') {

                $DaysInMonth[$month] = $no_of_days_24hr;


                foreach ($CalculateOffsetConsumption[$month] as $key => $value) {
                    if (!isset($CalculateOffsetConsumptionSum[$month])) {
                        $CalculateOffsetConsumptionSum[$month] = [];
                    }
                    $CalculateOffsetConsumptionSum[$month][$key] = array_sum($value);
                }

                //Load Final
                if (array_sum($avg_load_graph_data[$month][0]) == 0) {
                    for ($i = 0; $i < 24; $i++) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$i] = 0;
                    }
                } else {

                    foreach ($avg_load_graph_data[$month] as $key => $value) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$key] = (count($value) > 0) ? array_sum($value) / $no_of_days_24hr : 0;
                    }
                }


                //Solar Prd Final
                foreach ($avg_sol_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_sol_prd_graph_data[$month])) {
                        $final_avg_sol_prd_graph_data[$month] = [];
                    }
                    $final_avg_sol_prd_graph_data[$month][$key] = (count($avg_sol_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_sol_prd_graph_data[$month][$key]) / count($avg_sol_prd_graph_data[$month][$key]) : 0;
                }


                //Export final
                foreach ($avg_export_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_export_graph_data[$month])) {
                        $final_avg_export_graph_data[$month] = [];
                    }

                    $final_avg_export_graph_data[$month][$key] = (count($avg_export_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_export_prd_graph_data[$month][$key]) / count($avg_export_prd_graph_data[$month][$key]) : 0;
                }

                //Offset Final
                foreach ($avg_offset_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_offset_graph_data[$month])) {
                        $final_avg_offset_graph_data[$month] = [];
                    }

                    if ($final_avg_load_graph_data[$month][$key] < $final_avg_sol_prd_graph_data[$month][$key]) {
                        $final_avg_offset_graph_data[$month][$key] = $final_avg_load_graph_data[$month][$key];
                    } else {
                        $final_avg_offset_graph_data[$month][$key] = $final_avg_sol_prd_graph_data[$month][$key];
                    }

                    // $CalculateOffsetConsumption[$month] = array_sum($final_avg_offset_graph_data[$month]);
                }
                // }
            }
        }


        // Weekend graph
        $days   = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $counts = [];
        $weekend_load_graph_data  = [];

        foreach ($days as $key1 => $value1) {
            for ($w = 0; $w < count($full_year_meter_data); $w++) {
                $day      = date('D', strtotime($full_year_meter_data_dates[$w]));
                $curr_day = strtolower($day);
                $hr       = (int) date('H', strtotime($full_year_meter_data_times[$w]));

                if ($value1 != $curr_day) {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                    continue;
                } else {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                }

                if (isset($counts[$curr_day])) {
                    $counts[$curr_day] += 1;
                } else {
                    $counts[$curr_day] = 1;
                }


                if (!isset($weekend_load_graph_data[$value1])) {
                    $weekend_load_graph_data[$value1] = [];
                }
                if (!isset($weekend_load_graph_data[$value1][$hr])) {
                    $weekend_load_graph_data[$value1][$hr] = 0;
                }
                $weekend_load_graph_data[$value1][$hr] += $full_year_meter_data[$w];
            }
        }

        $typical_export_load_graph_data = [];
        $typical_export_sol_prd_graph_data = [];
        $typical_export_export_graph_data = [];
        $typical_export_offset_graph_data = [];

        $worst_export_load_graph_data = [];
        $worst_export_sol_prd_graph_data = [];
        $worst_export_export_graph_data = [];
        $worst_export_offset_graph_data = [];

        $high_export_load_graph_data = [];
        $high_export_sol_prd_graph_data = [];
        $high_export_export_graph_data = [];
        $high_export_offset_graph_data = [];

        $best_export_load_graph_data = [];
        $best_export_sol_prd_graph_data = [];
        $best_export_export_graph_data = [];
        $best_export_offset_graph_data = [];

        // $offset_consumption = [];

        // Typical Export Load Graph

        for ($i = 0; $i <= 23; $i++) {

            // Typical
            $typical_export_load_graph_data[$i] = ($final_avg_load_graph_data['jan'][$i] +
                    $final_avg_load_graph_data['feb'][$i] +
                    $final_avg_load_graph_data['mar'][$i]) / 3;

            $typical_export_sol_prd_graph_data[$i] = ($final_avg_sol_prd_graph_data['jan'][$i] +
                    $final_avg_sol_prd_graph_data['feb'][$i] +
                    $final_avg_sol_prd_graph_data['mar'][$i]) / 3;

            $typical_export_export_graph_data[$i] = $typical_export_sol_prd_graph_data[$i];

            if ($typical_export_load_graph_data[$i] > $typical_export_sol_prd_graph_data[$i]) {
                $typical_export_offset_graph_data[$i] = $typical_export_sol_prd_graph_data[$i];
            } else {
                $typical_export_offset_graph_data[$i] = $typical_export_load_graph_data[$i];
            }




            // Worst
            $worst_export_load_graph_data[$i] = ($final_avg_load_graph_data['apr'][$i] +
                    $final_avg_load_graph_data['may'][$i] +
                    $final_avg_load_graph_data['jun'][$i]) / 3;

            $worst_export_sol_prd_graph_data[$i] = ($final_avg_sol_prd_graph_data['apr'][$i] +
                    $final_avg_sol_prd_graph_data['may'][$i] +
                    $final_avg_sol_prd_graph_data['jun'][$i]) / 3;

            $worst_export_export_graph_data[$i] = $worst_export_sol_prd_graph_data[$i];

            if ($worst_export_load_graph_data[$i] > $worst_export_sol_prd_graph_data[$i]) {
                $worst_export_offset_graph_data[$i] = $worst_export_sol_prd_graph_data[$i];
            } else {
                $worst_export_offset_graph_data[$i] = $worst_export_load_graph_data[$i];
            }

            // High
            $high_export_load_graph_data[$i] = ($final_avg_load_graph_data['jul'][$i] +
                    $final_avg_load_graph_data['aug'][$i] +
                    $final_avg_load_graph_data['sep'][$i]) / 3;

            $high_export_sol_prd_graph_data[$i] = ($final_avg_sol_prd_graph_data['jul'][$i] +
                    $final_avg_sol_prd_graph_data['aug'][$i] +
                    $final_avg_sol_prd_graph_data['sep'][$i]) / 3;

            $high_export_export_graph_data[$i] = $high_export_sol_prd_graph_data[$i];

            if ($high_export_load_graph_data[$i] > $high_export_sol_prd_graph_data[$i]) {
                $high_export_offset_graph_data[$i] = $high_export_sol_prd_graph_data[$i];
            } else {
                $high_export_offset_graph_data[$i] = $high_export_load_graph_data[$i];
            }

            // Best
            $best_export_load_graph_data[$i] = ($final_avg_load_graph_data['oct'][$i] +
                    $final_avg_load_graph_data['nov'][$i] +
                    $final_avg_load_graph_data['dec'][$i]) / 3;

            $best_export_sol_prd_graph_data[$i] = ($final_avg_sol_prd_graph_data['oct'][$i] +
                    $final_avg_sol_prd_graph_data['nov'][$i] +
                    $final_avg_sol_prd_graph_data['dec'][$i]) / 3;

            $best_export_export_graph_data[$i] = $best_export_sol_prd_graph_data[$i];

            if ($best_export_load_graph_data[$i] > $best_export_sol_prd_graph_data[$i]) {
                $best_export_offset_graph_data[$i] = $best_export_sol_prd_graph_data[$i];
            } else {
                $best_export_offset_graph_data[$i] = $best_export_load_graph_data[$i];
            }


            // Calculate Offset Sum

        }

        $SumOfOffsetConsumption = 0;
        // $SumOfOffsetConsumption = $CalculateOffsetConsumptionSum[$DaysInMonth[3]][10];
        for ($i = 0; $i < count($CalculateOffsetConsumptionSum); $i++) {
            for ($j = 0; $j <= 23; $j++) {
                $SumOfOffsetConsumption = $SumOfOffsetConsumption + $CalculateOffsetConsumptionSum[$MonthsArray[$i]][$j];
            }
        }



        $data['weekend_load_graph_data']        = $weekend_load_graph_data;
        $data['DaysInMonth'] = $DaysInMonth;
        $data['consumption_data']    = $full_year_meter_data;
        if ($IsDemandCharge == "1") {
            $data['demand_data']    = $full_year_demand_data;
        }
        $data['consumption_date']    = $full_year_meter_data_dates;
        $data['consumption_time']    = $full_year_meter_data_times;
        $data['avg_monthly_load_graph_data']    = $final_avg_load_graph_data;
        $data['avg_load_graph_data']    = $avg_load_graph_data;
        $data['avg_export_prd_graph_data'] = $final_avg_export_graph_data;
        $data['avg_offset_prd_graph_data'] = $final_avg_offset_graph_data;
        $data['avg_sol_prd_graph_data'] = $final_avg_sol_prd_graph_data;

        $data['typical_graph'] = array(
            "typical_export_load_graph_data" => $typical_export_load_graph_data,
            "typical_export_sol_prd_graph_data" => $typical_export_sol_prd_graph_data,
            "typical_export_offset_graph_data" => $typical_export_offset_graph_data,
            "typical_export_export_graph_data" => $typical_export_export_graph_data
        );
        $data['worst_graph'] = array(
            "worst_export_load_graph_data" => $worst_export_load_graph_data,
            "worst_export_sol_prd_graph_data" => $worst_export_sol_prd_graph_data,
            "worst_export_offset_graph_data" => $worst_export_offset_graph_data,
            "worst_export_export_graph_data" => $worst_export_export_graph_data
        );
        $data['high_graph'] = array(
            "high_export_load_graph_data" => $high_export_load_graph_data,
            "high_export_sol_prd_graph_data" => $high_export_sol_prd_graph_data,
            "high_export_offset_graph_data" => $high_export_offset_graph_data,
            "high_export_export_graph_data" => $high_export_export_graph_data
        );
        $data['best_graph'] = array(
            "best_export_load_graph_data" => $best_export_load_graph_data,
            "best_export_sol_prd_graph_data" => $best_export_sol_prd_graph_data,
            "best_export_offset_graph_data" => $best_export_offset_graph_data,
            "best_export_export_graph_data" => $best_export_export_graph_data
        );

        // $CalculateOffsetConsumption = array_sum($typical_export_offset_graph_data) * ($DaysInMonth['jan'] + $DaysInMonth['feb'] + $DaysInMonth['mar']);

        // $CalculateOffsetConsumption = $CalculateOffsetConsumption + (array_sum($worst_export_offset_graph_data) * ($DaysInMonth['apr'] + $DaysInMonth['may'] + $DaysInMonth['jun']));

        // $CalculateOffsetConsumption = $CalculateOffsetConsumption + (array_sum($high_export_offset_graph_data) * ($DaysInMonth['jul'] + $DaysInMonth['aug'] + $DaysInMonth['sep']));

        // $CalculateOffsetConsumption =  $CalculateOffsetConsumption + (array_sum($best_export_offset_graph_data) * ($DaysInMonth['oct'] + $DaysInMonth['nov'] + $DaysInMonth['dec']));


        $data['SumOfOffsetConsumption'] = $SumOfOffsetConsumption;
        $data['OffsetSum'] = $CalculateOffsetConsumptionSum;
        $data['offset_consumption'] = $SumOfOffsetConsumption;
        $data['export_to_grid'] = array_sum($final_avg_export_graph_data);

        return $data;
    }

    public function calcualte_summer_and_weekend_load_graph_data_from_meter_data(
        $meter_data,
        $DataFromAnnualSolarDataXSizeXOrientationXTiltArr = [],
        $SolarConsumptionArr = []
    ) {

        //print_r($DataFromAnnualSolarDataXSizeXOrientationXTiltArr);die;

        $full_year_meter_data       = $meter_data['consumption_data'];
        $full_year_meter_data_dates = $meter_data['consumption_date'];
        $full_year_meter_data_times = $meter_data['consumption_time'];

        //Average Load vs Solar Production Graph Data
        $final_avg_load_graph_data    = $final_avg_sol_prd_graph_data = $final_avg_export_graph_data  = $final_avg_offset_graph_data  = [];
        $avg_load_graph_data          = $avg_sol_prd_graph_data       = $avg_export_prd_graph_data    = $avg_offset_prd_graph_data    = [];
        $is_trading_days              = true;

        foreach ($full_year_meter_data as $key => $value) {

            $month_int = date('m', strtotime($full_year_meter_data_dates[$key]));
            $year      = date('Y', strtotime($full_year_meter_data_dates[$key]));
            $month     = date('M', strtotime($full_year_meter_data_dates[$key]));
            $month     = strtolower($month);
            $hr        = (int) date('H', strtotime($full_year_meter_data_times[$key]));

            if (!isset($avg_load_graph_data[$month][$hr])) {
                $avg_load_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_sol_prd_graph_data[$month][$hr])) {
                $avg_sol_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_export_prd_graph_data[$month][$hr])) {
                $avg_export_prd_graph_data[$month][$hr] = [];
            }

            if (!isset($avg_offset_prd_graph_data[$month][$hr])) {
                $avg_offset_prd_graph_data[$month][$hr] = [];
            }
            // $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
            if ($is_trading_days && $value > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) < 6)) {
                // echo $month."key::".$key."hr::".$hr;print_r($value);
                $load                                     = $sol_prd                                  = 0;
                $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
                $avg_sol_prd_graph_data[$month][$hr][]    = $sol_prd                                  = (isset($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key])) ? $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key] : 0;
                $export                                   = ($sol_prd - $load);
                $avg_export_prd_graph_data[$month][$hr][] = ($export > 0) ? $export : 0;
                $avg_offset_prd_graph_data[$month][$hr][] = (isset($SolarConsumptionArr[$key])) ? $SolarConsumptionArr[$key] : 0;
            } else if (!$is_trading_days && $value[$value] > 0 && (date('N', strtotime($full_year_meter_data_dates[$key])) >= 6)) {
                //  echo "else::";echo $month."key::".$key."hr::".$hr;print_r($value);
                $load                                     = $sol_prd                                  = 0;
                $avg_load_graph_data[$month][$hr][]       = $load                                     = $value;
                $avg_sol_prd_graph_data[$month][$hr][]    = $sol_prd                                  = (isset($DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key])) ? $DataFromAnnualSolarDataXSizeXOrientationXTiltArr[$key] : 0;
                $export                                   = ($sol_prd - $load);
                $avg_export_prd_graph_data[$month][$hr][] = ($export > 0) ? $export : 0;
                $avg_offset_prd_graph_data[$month][$hr][] = (isset($SolarConsumptionArr[$key])) ? $SolarConsumptionArr[$key] : 0;
            }

            $no_of_days_24hr = $this->components->days_in_month($month_int, $year);


            // echo $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00   (' . $full_year_meter_data_dates[$key] . ')   ';

            // print_r($avg_load_graph_data[$month][$hr]);

            if ($full_year_meter_data_dates[$key] == $no_of_days_24hr . '-' . $month_int . '-' . $year . ' 23:00') {

                //Load Final
                if (array_sum($avg_load_graph_data[$month][0]) == 0) {
                    for ($i = 0; $i < 24; $i++) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$i] = 0;
                    }
                } else {

                    foreach ($avg_load_graph_data[$month] as $key => $value) {
                        if (!isset($final_avg_load_graph_data[$month])) {
                            $final_avg_load_graph_data[$month] = [];
                        }
                        $final_avg_load_graph_data[$month][$key] = (count($value) > 0) ? array_sum($value) / $no_of_days_24hr : 0;
                        // echo "<pre>";
                        // echo $key."Hello";print_r(array_sum($value));
                        // print_r(count($value));
                        //print_r($final_avg_load_graph_data[$month][$key]);
                    }
                }




                //Solar Prd Final
                foreach ($avg_sol_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_sol_prd_graph_data[$month])) {
                        $final_avg_sol_prd_graph_data[$month] = [];
                    }
                    $final_avg_sol_prd_graph_data[$month][$key] = (count($avg_sol_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_sol_prd_graph_data[$month][$key]) / count($avg_sol_prd_graph_data[$month][$key]) : 0;
                }

                //Export final
                foreach ($avg_export_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_export_graph_data[$month])) {
                        $final_avg_export_graph_data[$month] = [];
                    }
                    $final_avg_export_graph_data[$month][$key] = (count($avg_export_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_export_prd_graph_data[$month][$key]) / count($avg_export_prd_graph_data[$month][$key]) : 0;
                }

                //Offset Final
                foreach ($avg_offset_prd_graph_data[$month] as $key => $value) {
                    if (!isset($final_avg_offset_graph_data[$month])) {
                        $final_avg_offset_graph_data[$month] = [];
                    }
                    $final_avg_offset_graph_data[$month][$key] = (count($avg_offset_prd_graph_data[$month][$key]) > 0) ? array_sum($avg_offset_prd_graph_data[$month][$key]) / count($avg_offset_prd_graph_data[$month][$key]) : 0;
                }
            }
        }
        //print_r($avg_load_graph_data);
        // die();

        //echo "<pre>";
        //print_r($avg_sol_prd_graph_data);die;


        // $days   = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        // $counts = [];

        // foreach ($days as $key1 => $value1) {
        //     for ($w = 0; $w < count($full_year_meter_data); $w++) {
        //         $day      = date('D', strtotime($full_year_meter_data_dates[$w]));
        //         $curr_day = strtolower($day);
        //         $hr       = (int) date('H', strtotime($full_year_meter_data_times[$w]));

        //         if ($value1 != $curr_day) {
        //             if ($w == count($full_year_meter_data) - 1) {
        //                 $total_days = $counts[$value1];
        //                 foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
        //                     $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
        //                 }
        //             }
        //             continue;
        //         } else {
        //             if ($w == count($full_year_meter_data) - 1) {
        //                 $total_days = $counts[$value1];
        //                 foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
        //                     $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
        //                 }
        //             }
        //         }

        //         if (isset($counts[$curr_day])) {
        //             $counts[$curr_day] += 1;
        //         } else {
        //             $counts[$curr_day] = 1;
        //         }


        //         if (!isset($weekend_load_graph_data[$value1])) {
        //             $weekend_load_graph_data[$value1] = [];
        //         }
        //         if (!isset($weekend_load_graph_data[$value1][$hr])) {
        //             $weekend_load_graph_data[$value1][$hr] = 0;
        //         }
        //         $weekend_load_graph_data[$value1][$hr] += $full_year_meter_data[$w];
        //     }
        // }


        $days   = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $counts = [];

        foreach ($days as $key1 => $value1) {
            for ($w = 0; $w < count($full_year_meter_data); $w++) {
                $day      = date('D', strtotime($full_year_meter_data_dates[$w]));
                $curr_day = strtolower($day);
                $hr       = (int) date('H', strtotime($full_year_meter_data_times[$w]));

                if ($value1 != $curr_day) {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                    continue;
                } else {
                    if ($w == count($full_year_meter_data) - 1) {
                        $total_days = $counts[$value1];
                        foreach ($weekend_load_graph_data[$value1] as $key2 => $value2) {
                            $weekend_load_graph_data[$value1][$key2] = ($value2 / $total_days) * 24;
                        }
                    }
                }

                if (isset($counts[$curr_day])) {
                    $counts[$curr_day] += 1;
                } else {
                    $counts[$curr_day] = 1;
                }


                if (!isset($weekend_load_graph_data[$value1])) {
                    $weekend_load_graph_data[$value1] = [];
                }
                if (!isset($weekend_load_graph_data[$value1][$hr])) {
                    $weekend_load_graph_data[$value1][$hr] = 0;
                }
                $weekend_load_graph_data[$value1][$hr] += $full_year_meter_data[$w];
            }
        }


        $data['weekend_load_graph_data']        = $weekend_load_graph_data;
        $data['avg_monthly_load_graph_data']    = $final_avg_load_graph_data;
        $data['avg_monthly_sol_prd_graph_data'] = $final_avg_sol_prd_graph_data;
        $data['avg_monthly_export_graph_data']  = $final_avg_export_graph_data;
        $data['avg_monthly_offset_graph_data']  = $final_avg_offset_graph_data;
        // if ($_SERVER['REMOTE_ADDR'] == '111.93.41.194') {
        //         echo '<pre>';
        //         print_r($data['avg_monthly_load_graph_data']);
        //         echo '</pre>';
        //         die;
        //     }
        return $data;
    }


    public function download_proposal_pdf($uuid)
    {

        $data = array();

        $view_proposal_data = $solar_proposal  = $this->common->fetch_row('tbl_solar_proposal', '*', array('proposal_uuid' => $uuid));
        $proposal_id = $view_proposal_data['id'];
        $data['mapping_tool'] = $mapping_tool = $this->common->fetch_row('tbl_mapping_tool_data', '*', array('proposal_id' => $proposal_id));

        if ((int)$view_proposal_data['is_production_file'] != 1 && !empty($mapping_tool)) {
            $data['mappingPanelObjects'] = $mappingPanelObjects = $this->common->fetch_where('tbl_mapping_tool_objects_data', '*', array('tool_id' => $mapping_tool['id'], 'object_type' => "PANEL"));
        } else {
            $data['mappingPanelObjects'] = [];
        }

        $user_id        = $this->aauth->get_user_id();
        $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
        $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
        $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
        $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);
        $data['lead_data']    = $lead_data;

        $data['proposal_data'] = $view_proposal_data;
        $data['proposal_finance_data'] = $proposal_finance_data         = $this->common->fetch_row('tbl_proposal_finance', '*', array('proposal_id' => $proposal_id, 'proposal_type' => 2));

        $data['financial_summary_data'] = $financial_summary_data         = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));


        $year = 0;
        $Cashflow = 0;
        if (count($proposal_finance_data)) {
            // if ($proposal_finance_data['term'] == 12) {
            //     $year = 1;
            // }
            // if ($proposal_finance_data['term'] == 24) {
            //     $year = 2;
            // }
            // if ($proposal_finance_data['term'] == 36) {
            //     $year = 3;
            // }
            // if ($proposal_finance_data['term'] == 48) {
            //     $year = 4;
            // }
            // if ($proposal_finance_data['term'] == 60) {
            //     $year = 5;
            // }
            // if ($proposal_finance_data['term'] == 72) {
            //     $year = 6;
            // }
            // if ($proposal_finance_data['term'] == 84) {
            //     $year = 7;
            // }

            $year = $proposal_finance_data['term'] / 12;

            $monthlyPaymentPlanSolar = 0;
            if (isset($proposal_finance_data['monthly_payment_plan'])) {
                $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'] * 12;
            } else {
                $proposal_finance_data['monthly_payment_plan'] = 0;
                $proposal_finance_data['term'] = 0;
            }
            $Cashflow = $monthlyPaymentPlanSolar;
        }

        // Financial Summery
        $ppa_year = 0;
        if (count($financial_summary_data)) {
            // if ($financial_summary_data['ppa_term'] == 12) {
            //     $ppa_year = 1;
            // }
            // if ($financial_summary_data['ppa_term'] == 24) {
            //     $ppa_year = 2;
            // }
            // if ($financial_summary_data['ppa_term'] == 36) {
            //     $ppa_year = 3;
            // }
            // if ($financial_summary_data['ppa_term'] == 48) {
            //     $ppa_year = 4;
            // }
            // if ($financial_summary_data['ppa_term'] == 60) {
            //     $ppa_year = 5;
            // }
            // if ($financial_summary_data['ppa_term'] == 72) {
            //     $ppa_year = 6;
            // }
            // if ($financial_summary_data['ppa_term'] == 84) {
            //     $ppa_year = 7;
            // }
            if ($financial_summary_data['ppa_checkbox'] == "VEEC") {
                $ppa_year = $financial_summary_data['ppa_term'] / 12;
            }
        }


        $data['year'] = $year;
        $data['ppa_year'] = $ppa_year;
        $data['Cashflow'] = $Cashflow;
        $data['product_data'] = array();
        $data['product_data'] = $product_data         = $this->product->get_solar_proposal_products($proposal_id);

        //Panel ,Inverter Data
        $panel    = array();
        $inverter = array();
        foreach ($product_data as $key => $value) {
            if ($value['type_id'] == 9) {
                $panel = $value;
            } else if ($value['type_id'] == 11 || $value['type_id'] == 12 || $value['type_id'] == 13 || $value['type_id'] == 14) {
                $inverter = $value;
            }
        }
        $cond1 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=1";
        $cond2 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=2";
        $cond3 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=3";
        $data['inverters'] = $this->product->get_item_data_by_cond($cond1);
        $data['solar_panels'] = $this->product->get_item_data_by_cond($cond2);
        $data['battery'] = $this->product->get_item_data_by_cond($cond3);

        $data['panel']                = $panel;
        $data['inverter']             = $inverter;

        $data['saving_meter_calculation'] = $view_proposal_data['saving_meter_calculation'];
        $data['saving_nrel_calculation'] = $view_proposal_data['saving_nrel_calculation'];
        $data['saving_graph_calculation'] = $view_proposal_data['saving_graph_calculation'];
        $data['system_performance'] = $view_proposal_data['system_performance'];
        $data['pvwatts_data_result'] = $view_proposal_data['pvwatts_data_result'];
        $data['time_of_use'] = $view_proposal_data['time_of_use'];
        $data['inverter_design'] = $this->common->fetch_row(' tbl_inverter_design_tool_data', '*', array('proposal_id' => $proposal_id, 'status' => 1));

        $view = $this->input->get('view');
        if (isset($view) && $view == 'final_html') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-2', $data, TRUE);
            $html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-3', $data, TRUE);
            //$html .= $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-8',$data,TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page1') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/page-1', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page4') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/page-4', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page5') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-5', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page6') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-6', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page7') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-7', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page8') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-8', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page12') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-12', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page13') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-13', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page14') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-14', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'page15') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/page-15', $data, TRUE);
            echo $html;
            die;
        } else if (isset($view) && $view == 'charts') {
            $html = $this->load->view('pdf_files/solar_proposal_pdf_new/pdf/image/charts', $data, TRUE);
            echo $html;
            die;
        } else {

            //Page 1
            $filename1  = $uuid . "_page1.pdf";
            //$upload_path_img1 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/'.$filename1;
            //$cmd = '/usr/local/bin/wkhtmltoimage --width 910 --height 1295 https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/'.$uuid.'?view=page1 '. $upload_path_img1;
            $upload_path_1 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename1;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page1 ' . $upload_path_1;
            exec($cmd);

            //Page 4
            $filename4  = $uuid . "_page4.pdf";
            $upload_path_4 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename4;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page4 ' . $upload_path_4;
            exec($cmd);


            //Page 8
            $filename8  = $uuid . "_page8.pdf";
            $upload_path_8 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename8;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page8 ' . $upload_path_8;
            exec($cmd);


            $filenameAll  = $uuid . "_all_pages.pdf";
            $upload_path_img_all = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filenameAll;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1  -L 0mm -R 0mm -T 0mm -B 0mm  https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=final_html ' . $upload_path_img_all;
            exec($cmd);


            //Page 5
            $filename5  = $uuid . "_page5.pdf";
            $upload_path_5 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename5;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --page-width 270mm --page-height 205mm  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page5 ' . $upload_path_5;
            exec($cmd);

            //Page 6
            $filename6  = $uuid . "_page6.pdf";
            $upload_path_6 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename6;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --page-width 270mm --page-height 205mm  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page6 ' . $upload_path_6;
            exec($cmd);

            //Page 7
            //$filename7  = $uuid . "_page7.pdf";
            //$upload_path_7 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename7;
            //$cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --page-width 270mm --page-height 205mm  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page7 ' . $upload_path_7;
            //exec($cmd);

            $filename12  = $uuid . "_page12.pdf";
            $upload_path_12 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename12;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --no-stop-slow-scripts -L 0mm -R 0mm -T 0mm -B 0mm  https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page12 ' . $upload_path_12;
            exec($cmd);

            //Page 13
            $filename13  = $uuid . "_page13.pdf";
            $upload_path_13 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename13;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --page-width 268mm --page-height 190mm  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page13 ' . $upload_path_13;
            exec($cmd);

            //Page 14
            $filename14  = $uuid . "_page14.pdf";
            $upload_path_14 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename14;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --page-width 268mm --page-height 190mm  -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page14 ' . $upload_path_14;
            exec($cmd);


            $filename15  = $uuid . "_page15.pdf";
            $upload_path_15 = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename15;
            $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 --no-stop-slow-scripts -L 0mm -R 0mm -T 0mm -B 0mm  https://kugacrm.com.au/admin/proposal/commercial/download_proposal_pdf/' . $uuid . '?view=page15 ' . $upload_path_15;
            exec($cmd);

            $filename  = 'SOLAR_PROPOSAL_' . $solar_proposal['total_system_size'] . 'kW_' . $lead_data['first_name'] . ' ' . $lead_data['last_name'];
            $filename = str_replace(array('.', ',', ' ', '/', '&', "'"), '_', $filename);
            $filename = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_', $filename);
            $filename = $filename . '.pdf';
            $final_upload_path = FCPATH . 'assets/uploads/solar_proposal_pdf_new/' . $filename;
            $cmd = 'gs -dNOPAUSE  -sDEVICE=pdfwrite -sOUTPUTFILE=' . $final_upload_path . ' -dBATCH ' . $upload_path_1 . " " . $upload_path_img_all . " " . $upload_path_4 . " " . $upload_path_5 . " " . $upload_path_6 . " " . $upload_path_8 . " " . $upload_path_12 . " " . $upload_path_13 . " " . $upload_path_14 . " " . $upload_path_15;
            exec($cmd);

            unlink($upload_path_1);
            unlink($upload_path_img_all);
            unlink($upload_path_4);
            unlink($upload_path_5);
            unlink($upload_path_6);
            //unlink($upload_path_7);
            unlink($upload_path_8);
            unlink($upload_path_12);
            unlink($upload_path_13);
            unlink($upload_path_14);
            unlink($upload_path_15);
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page8_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page8_" . $proposal_id . ".jpg");
            }
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page9_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page9_" . $proposal_id . ".jpg");
            }
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page10_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page10_" . $proposal_id . ".jpg");
            }
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page11_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page11_" . $proposal_id . ".jpg");
            }
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page12_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page12_" . $proposal_id . ".jpg");
            }
            if (file_exists(FCPATH . "assets/uploads/solar_proposal_pdf_new/page12_1_" . $proposal_id . ".jpg")) {
                unlink(FCPATH . "assets/uploads/solar_proposal_pdf_new/page12_1_" . $proposal_id . ".jpg");
            }
            header('Location: ' . site_url('assets/uploads/solar_proposal_pdf_new/' . $filename));
            die;
        }
    }

    public function save_dataimg_to_png()
    {
        $dataImg1 =  $this->input->post('dataImg1');
        $filename1 =  $this->input->post('filename1');
        $img = explode(';', $dataImg1);
        if ($dataImg1 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg1));
            $newfilename = $filename1 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }

        $dataImg2 =  $this->input->post('dataImg2');
        $filename2 =  $this->input->post('filename2');
        $img = explode(';', $dataImg2);
        if ($dataImg2 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg2));
            $newfilename = $filename2 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }

        $dataImg3 =  $this->input->post('dataImg3');
        $filename3 =  $this->input->post('filename3');
        $img = explode(';', $dataImg3);
        if ($dataImg3 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg3));
            $newfilename = $filename3 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }

        $dataImg4 =  $this->input->post('dataImg4');
        $filename4 =  $this->input->post('filename4');
        $img = explode(';', $dataImg4);
        if ($dataImg4 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg4));
            $newfilename = $filename4 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }

        $dataImg5 =  $this->input->post('dataImg5');
        $filename5 =  $this->input->post('filename5');
        $img = explode(';', $dataImg5);
        if ($dataImg5 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg5));
            $newfilename = $filename5 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }

        $dataImg6 =  $this->input->post('dataImg6');
        $filename6 =  $this->input->post('filename6');
        $img = explode(';', $dataImg6);
        if ($dataImg6 != '' && count($img) > 1) {
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataImg6));
            $newfilename = $filename6 . '.jpg';
            file_put_contents('./assets/uploads/solar_proposal_pdf_new/' . $newfilename, $image);
        }
    }

    public function send_proposal_to_customer()
    {
        $this->load->library("Sms");
        $proposal_uuid = $this->input->post('uuid');
        $post_data = $this->input->post();
        if (isset($proposal_uuid) && $proposal_uuid != '') {
            $proposal_data = $this->proposal->get_solar_proposal_data(FALSE, FALSE, FALSE, $proposal_uuid);
            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $proposal_data['id']));

            /**
            if($proposal_data['customer_contact_no'] == '' || $proposal_data['customer_contact_no'] == 0 || $proposal_data['customer_contact_no'] == NULL){
            $data['status'] = 'Customer Contact Number Missing.';
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode($data));
            }
            #007607
             */

            $otp = rand(10000, 99999);
            $url = '<a href="https://kugacrm.com.au/admin/proposal/commercial/web_view/' . $proposal_uuid . '" style="padding: 7px 14px; color: white; background: #b92625; border-radius: 8px; text-decoration: none;">Click Here</a>';
            $url2 = '<a href="https://kugacrm.com.au/admin/proposal/commercial/web_view/' . $proposal_uuid . '?action=collegue" style="padding: 7px 14px; color: white; background: #007607; border-radius: 8px; text-decoration: none;">Forward to Colleague</a>';
            $post_data['email_body'] = str_replace("{CLICK_HERE}", $url, $post_data['email_body']);
            $post_data['email_body'] = str_replace("{ACCESS_CODE}", $otp, $post_data['email_body']);
            $post_data['email_body'] = str_replace("{FORWORD_COLLEAGUE}", $url2, $post_data['email_body']);
            $post_data['email_body'] = str_replace("{MOBILE_NUMBER}", $post_data['mobile'], $post_data['email_body']);

            $post_data['cc_emails'] = '';
            if (empty($log_data)) {

                //$bitly_response = $this->createBitLyShortUrl($url);
                if (
                    /**isset($bitly_response->link)*/
                TRUE
                ) {

                    $insert_data = array();
                    $insert_data['proposal_id'] = $proposal_data['id'];
                    $insert_data['url'] = $url; //$bitly_response->link;
                    $insert_data['otp'] = $otp;
                    $insert_data['send_to'] = $post_data['email_to'];
                    $insert_data['payload'] = json_encode($post_data);
                    $this->common->insert_data('tbl_solar_proposal_send_logs', $insert_data);
                } else {
                    $data['status'] = 'Looks like someting went wrong while shortening then web view url';
                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(500)
                        ->set_output(json_encode($data));
                }
            } else {
                $update_data = array();
                //$update_data['send_to'] = $proposal_data['customer_contact_no'];
                $update_data['send_to'] = $post_data['email_to'];
                $update_data['otp'] = $otp;
                $update_data['payload'] = json_encode($post_data);
                $this->common->update_data('tbl_solar_proposal_send_logs', array('id' => $log_data['id']), $update_data);
            }

            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $proposal_data['id']));
            $communication_data = json_decode($log_data['payload'], true);
            $this->email_manager->send_commercial_proposal_customer_email($communication_data);
            $text = "Please access proposal using this link " . $log_data['url'] . ".\r\n";

            $text = "Hi " . $proposal_data['first_name'] . ", Your solar proposal has been emailed to you from " . $proposal_data['sales_person_name'] . " at Kuga Electrical. Your access code is " . $log_data['otp'] . ". Thanks";

            // . "Otp: ". $log_data['otp'];
            // $to = $proposal_data['customer_contact_no'];
            $to = $post_data['mobile'];
            $this->sms->send_custom_sms($text, $to);

            $update_proposal_data['email_status'] = 1;
            $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_data['id']), $update_proposal_data);

            $data['status'] = 'Proposal Email Sent successfully to Customer';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $data['status'] = 'Invalid Request';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }


    public function send_proposal_to_colleague()
    {
        $this->load->library("Sms");
        $proposal_uuid = $this->input->post('uuid');
        $post_data = $this->input->post();
        if (isset($proposal_uuid) && $proposal_uuid != '') {
            $proposal_data = $this->proposal->get_solar_proposal_data(FALSE, FALSE, FALSE, $proposal_uuid);
            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $proposal_data['id'], 'is_customer' => 0));

            /**
            if($proposal_data['customer_contact_no'] == '' || $proposal_data['customer_contact_no'] == 0 || $proposal_data['customer_contact_no'] == NULL){
            $data['status'] = 'Customer Contact Number Missing.';
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode($data));
            }
             */

            if (!empty($log_data)) {
                $otp = $log_data['otp'];
            } else {
                $otp = rand(10000, 99999);
            }

            // $otp = rand(10000, 99999);
            $url = '<a href="https://kugacrm.com.au/admin/proposal/commercial/web_view/' . $proposal_uuid . '" style="padding: 7px 14px; color: white; background: #b92625; border-radius: 8px; text-decoration: none;">Click Here</a>';

            $email_body = "Hi " . $post_data['name'] . ",<br/><br/>                      
            Here's your " . $proposal_data['total_system_size'] . "kW Proposal for " . $proposal_data['customer_site_address'] . "<br/><br/>                           
            <a href='https://kugacrm.com.au/admin/proposal/commercial/web_view/" . $proposal_uuid . "?action=col' style='padding: 7px 14px; color: white; background: #b92625; border-radius: 8px; text-decoration: none;'>Click Here</a> to access the proposal, Access code is sent to " . $post_data['mobile'] . ". 
            <br/><br/>                        
            Best Regards, <br/>
            " . $proposal_data['sales_person_name'] . " <br/>                                  
            <img width='150' src='https://kugacrm.com.au/assets/images/Kuga-Logo-C-01.png' alt='Logo'/><br/>
            <br/><br/>

            <strong style='color:#D52323'>M:</strong> " . $proposal_data['sales_person_contact_no'] . " <br/>
            <strong style='color:#D52323'>P:</strong> 13 KUGA (13 58 42) <br/>
            <strong style='color:#D52323'>W:</strong> <a href='https://kugacrm.com.au/'>www.kugacrm.com.au</a> <br/>
            <strong style='color:#D52323'>A:</strong> 4 Bridge Road, Keysborough, VIC <br/> 6 Turbo Road, Kings Park, NSW <br/> 31 Chetwynd Street, Loganholme, QLD <br/>110 -112 Grand Junction Road, Blair Athol, SA <br/>";
            $post_data['email_body'] = $email_body;

            $post_data['email_subject'] = "Here's your " . $proposal_data['total_system_size'] . "kW Proposal for " . $proposal_data['customer_site_address'];
            $post_data['cc_emails'] = '';
            if (empty($log_data)) {

                //$bitly_response = $this->createBitLyShortUrl($url);
                if (
                    /**isset($bitly_response->link)*/
                TRUE
                ) {
                    $insert_data = array();
                    $insert_data['proposal_id'] = $proposal_data['id'];
                    $insert_data['url'] = $url; //$bitly_response->link;
                    $insert_data['otp'] = $otp;
                    $insert_data['is_customer'] = 0;
                    $insert_data['send_to'] = $post_data['email_to'];
                    $insert_data['payload'] = json_encode($post_data);
                    $this->common->insert_data('tbl_solar_proposal_send_logs', $insert_data);
                } else {
                    $data['status'] = 'Looks like someting went wrong while shortening then web view url';
                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(500)
                        ->set_output(json_encode($data));
                }
            } else {
                $update_data = array();
                // $update_data['send_to'] = $proposal_data['customer_contact_no'];
                $update_data['send_to'] = $post_data['email_to'];
                $update_data['otp'] = $otp;
                $update_data['is_customer'] = 0;
                $update_data['payload'] = json_encode($post_data);
                $this->common->update_data('tbl_solar_proposal_send_logs', array('id' => $log_data['id']), $update_data);
            }

            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $proposal_data['id']));
            $communication_data = json_decode($log_data['payload'], true);
            $this->email_manager->send_commercial_proposal_customer_email($communication_data);
            //$text = "Please access proposal using this link ".$log_data['url'].".\r\n"
            //. "Otp: ". $log_data['otp'];

            $text = "Hi " . $post_data['name'] . ", Your solar proposal has been emailed to you from " . $proposal_data['sales_person_name'] . " at Kuga Electrical. Your access code is " . $log_data['otp'] . ". Thanks";
            // $text = "Working";
            $to = $post_data['mobile'];
            $this->sms->send_custom_sms($text, $to);


            // Sending mail to sales person
            $email_body = "Hi " . $proposal_data['sales_person_name'] . "<br/><br/>" . $proposal_data['customer_name'] . " have forwarded " . $proposal_data['total_system_size'] . "kW proposal for " . $proposal_data['customer_site_address'] . " to " . $post_data['name'] . ", Details are below:
            <br/><br/>
            <table border='1'>
                <tr>
                    <td>Name</td>
                    <td>" . $post_data['name'] . "</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>" . $post_data['email_to'] . "</td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>" . $post_data['mobile'] . "</td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td>" . $post_data['position'] . "</td>
                </tr>
            </table>
            <br/><br/>   
            Best Regards, <br/>
            <strong>Admin</strong> <br/>                                  
            <img width='150' src='https://kugacrm.com.au/assets/images/Kuga-Logo-C-01.png' alt='Logo'/>";
            $s_data['email_body'] = $email_body;
            $s_data['email_subject'] =  "Here's your " . $proposal_data['total_system_size'] . "kW Proposal for " . $proposal_data['customer_site_address'];
            $s_data['cc_emails'] = '';
            $s_data['email_to'] = $proposal_data['sales_person_email'];
            $s_data['payload'] = json_encode($s_data);
            $communication_data = json_decode($s_data['payload'], true);
            $this->email_manager->send_commercial_proposal_customer_email($communication_data);

            $data['status'] = 'Proposal Email Sent successfully to Colleague';



            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($data));
        } else {
            $data['status'] = 'Invalid Request';
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode($data));
        }
    }



    private function createBitLyShortUrl($long_url)
    {
        $apiv4 = 'https://api-ssl.bitly.com/v4/bitlinks';
        $genericAccessToken = '40eb0f1cb2e95de3eec92aa98bd6dc481bc0b0cc';

        $data = array(
            'long_url' => $long_url,
            'domain' => "bit.ly",
        );
        $payload = json_encode($data);

        $header = array(
            'Authorization: Bearer ' . $genericAccessToken,
            'Content-Type: application/json',
        );

        $ch = curl_init($apiv4);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        $resultToJson = json_decode($result);

        return $resultToJson;
    }

    public function web_view($proposal_uuid)
    {
        $data = array();

        $data['action'] = '';
        if ($this->input->get('action', TRUE)) {
            $data['action'] = $this->input->get('action', TRUE);
        }
        $view_proposal_data = $this->common->fetch_row('tbl_solar_proposal', '*', array('proposal_uuid' => $proposal_uuid));
        if ($data['action'] != '' && $data['action'] == 'col') {
            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $view_proposal_data['id'], 'is_customer' => '0'));
        } else {
            $log_data = $this->common->fetch_row('tbl_solar_proposal_send_logs', '*', array('proposal_id' => $view_proposal_data['id'], 'is_customer' => '1'));
        }

        $proposal_data = $this->proposal->get_solar_proposal_data(FALSE, FALSE, FALSE, $proposal_uuid);

        $otp_entered = $this->session->userdata('web_view_' . $proposal_uuid);

        if (!isset($otp_entered)) {
            $otp = $this->input->post('otp');
            if (isset($otp) && $otp == '') {
                $this->form_validation->set_rules('otp', 'Otp', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $data['otp'] = $this->input->post('otp');
                }
            } else if (($log_data['otp'] != $otp) && $otp != '') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Invalid Otp Entered </div>');
                $url = site_url() . 'admin/proposal/commercial/web_view/' . $proposal_uuid;
                redirect($url);
            } else if (($log_data['otp'] == $otp) && $otp != '') {
                $this->session->set_userdata('web_view_' . $proposal_uuid, $otp);
                $url = site_url() . 'admin/proposal/commercial/web_view/' . $proposal_uuid;
                redirect($url);
            }
        }


        if (isset($otp_entered) && $otp_entered != '') {
            $proposal_id = $view_proposal_data['id'];
            $data['mapping_tool'] = $mapping_tool = $this->common->fetch_row('tbl_mapping_tool_data', '*', array('proposal_id' => $proposal_id));

            if ((int)$view_proposal_data['is_production_file'] != 1 && !empty($mapping_tool)) {
                $data['mappingPanelObjects'] = $mappingPanelObjects = $this->common->fetch_where('tbl_mapping_tool_objects_data', '*', array('tool_id' => $mapping_tool['id'], 'object_type' => "PANEL"));
            } else {
                $data['mappingPanelObjects'] = [];
            }



            // Sending mail to sales person after view proposal
            if ($view_proposal_data['email_status'] == 1) {
                $update_proposal_data['email_status'] = 2;
                $this->common->update_data('tbl_solar_proposal', array('id' => $proposal_id), $update_proposal_data);
                $email_body = 'Hi ' . $proposal_data['sales_person_name'] . "<br/><br/><br/> " . $proposal_data['customer_name'] . " viewed the proposal for " . $proposal_data['total_system_size'] . "kW proposal for " . $proposal_data['customer_site_address'] . ".<br/><br/>
                <br/><br/>   
                Best Regards, <br/>
                <strong>Kuga Electrical</strong> <br/>                                  
                <img width='150' src='https://kugacrm.com.au/assets/images/Kuga-Logo-C-01.png' alt='Logo'/>";
                $s_data['email_body'] = $email_body;
                $s_data['email_subject'] =  "Proposal Viewed - " . $proposal_data['total_system_size'] . "kW Proposal for " . $proposal_data['customer_site_address'];
                $s_data['cc_emails'] = '';
                $s_data['email_to'] = $proposal_data['sales_person_email'];
                $s_data['payload'] = json_encode($s_data);
                $communication_data = json_decode($s_data['payload'], true);
                $this->email_manager->send_commercial_proposal_customer_email($communication_data);
            }

            $user_id        = $this->aauth->get_user_id();
            $lead_id        = $this->common->fetch_cell('tbl_solar_proposal', 'lead_id', array('id' => $proposal_id));
            $lead_to_userid = $this->common->fetch_cell('tbl_lead_to_user', 'user_id', array('lead_id' => $lead_id));
            $is_access      = ($lead_to_userid == $user_id) ? TRUE : FALSE;
            $lead_data      = $this->lead->get_leads(FALSE, $lead_id, FALSE);
            $data['lead_data']    = $lead_data;

            $data['proposal_data'] = $view_proposal_data;
            $data['proposal_finance_data'] = $proposal_finance_data         = $this->common->fetch_row('tbl_proposal_finance', '*', array('proposal_id' => $proposal_id, 'proposal_type' => 2));

            $data['financial_summary_data'] = $financial_summary_data         = $this->common->fetch_row('tbl_solar_proposal_financial_summary', '*', array('proposal_id' => $proposal_id));

            $year = 0;
            $Cashflow = 0;
            if (count($proposal_finance_data)) {
                // if ($proposal_finance_data['term'] == 12) {
                //     $year = 1;
                // }
                // if ($proposal_finance_data['term'] == 24) {
                //     $year = 2;
                // }
                // if ($proposal_finance_data['term'] == 36) {
                //     $year = 3;
                // }
                // if ($proposal_finance_data['term'] == 48) {
                //     $year = 4;
                // }
                // if ($proposal_finance_data['term'] == 60) {
                //     $year = 5;
                // }
                // if ($proposal_finance_data['term'] == 72) {
                //     $year = 6;
                // }
                // if ($proposal_finance_data['term'] == 84) {
                //     $year = 7;
                // }

                $year = $proposal_finance_data['term'] / 12;


                $monthlyPaymentPlanSolar = 0;
                if (isset($proposal_finance_data['monthly_payment_plan'])) {
                    $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'] * 12;
                } else {
                    $proposal_finance_data['monthly_payment_plan'] = 0;
                    $proposal_finance_data['term'] = 0;
                }
                $Cashflow = $monthlyPaymentPlanSolar;
            }

            // Financial Summery
            $ppa_year = 0;
            if (count($financial_summary_data)) {
                if ($financial_summary_data['ppa_term'] == 12) {
                    $ppa_year = 1;
                }
                if ($financial_summary_data['ppa_term'] == 24) {
                    $ppa_year = 2;
                }
                if ($financial_summary_data['ppa_term'] == 36) {
                    $ppa_year = 3;
                }
                if ($financial_summary_data['ppa_term'] == 48) {
                    $ppa_year = 4;
                }
                if ($financial_summary_data['ppa_term'] == 60) {
                    $ppa_year = 5;
                }
                if ($financial_summary_data['ppa_term'] == 72) {
                    $ppa_year = 6;
                }
                if ($financial_summary_data['ppa_term'] == 84) {
                    $ppa_year = 7;
                }
            }

            $data['year'] = $year;
            $data['ppa_year'] = $ppa_year;
            $data['Cashflow'] = $Cashflow;
            $data['product_data'] = array();
            $data['product_data'] = $product_data         = $this->product->get_solar_proposal_products($proposal_id);

            //Panel ,Inverter Data
            $panel    = array();
            $inverter = array();
            foreach ($product_data as $key => $value) {
                if ($value['type_id'] == 9) {
                    $panel = $value;
                } else if ($value['type_id'] == 11 || $value['type_id'] == 12 || $value['type_id'] == 13 || $value['type_id'] == 14) {
                    $inverter = $value;
                }
            }
            $cond1 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=1";
            $cond2 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=2";
            $cond3 = " WHERE tpv.state IN ('All','NSW','VIC') AND prd.type_id=3";
            $data['inverters'] = $this->product->get_item_data_by_cond($cond1);
            $data['solar_panels'] = $this->product->get_item_data_by_cond($cond2);
            $data['battery'] = $this->product->get_item_data_by_cond($cond3);

            $data['panel']                = $panel;
            $data['inverter']             = $inverter;

            $data['saving_meter_calculation'] = $view_proposal_data['saving_meter_calculation'];
            $data['saving_nrel_calculation'] = $view_proposal_data['saving_nrel_calculation'];
            $data['saving_graph_calculation'] = $view_proposal_data['saving_graph_calculation'];
            $data['system_performance'] = $view_proposal_data['system_performance'];
            $data['pvwatts_data_result'] = $view_proposal_data['pvwatts_data_result'];
            $data['time_of_use'] = $view_proposal_data['time_of_use'];
            $data['inverter_design'] = $this->common->fetch_row(' tbl_inverter_design_tool_data', '*', array('proposal_id' => $proposal_id, 'status' => 1));

            if (!empty($this->input->get('action'))) {
                $data['action'] = $this->input->get('action');
            }

            if (count($financial_summary_data) && $financial_summary_data['type'] === 'VEEC') {
                $this->load->view('pdf_files/solar_proposal_pdf_new/web/main_veec', $data);
            } else {
                $this->load->view('pdf_files/solar_proposal_pdf_new/web/main_customer', $data);
                $this->load->view('pdf_files/solar_proposal_pdf_new/web/send_proposal_to_colleague_modal.php', $data);
            }
        } else {
            $this->load->view('pdf_files/solar_proposal_pdf_new/web/main_auth', $data);
        }
    }

    public function download_pack_view()
    {
        $this->load->view('pdf_files/download_pack/web/main');
    }

    function generate_pdf()
    {
        // $dompdf = new Dompdf\Dompdf();
        // $this->load->view('pdf_files/download_pack/web/main');
        // $html = $this->output->get_output();
        // // Load pdf library
        // $this->load->library('dpdf');
        // $this->pdf->loadHtml($html);
        // $this->pdf->setPaper('A4', 'landscape');
        // $this->pdf->render();
        // // Output the generated PDF (1 = download and 0 = preview)
        // $this->pdf->stream("html_contents.pdf", array("Attachment" => 0));


        // $html = $this->load->view('welcome_message', [], true);

        // $dompdf->loadHtml($html);

        // $dompdf->setPaper('A4', 'landscape');

        // $dompdf->render();

        // $pdf = $dompdf->output();

        // $dompdf->stream();
        $uuid = 'asdfasdfwe216as5d1f984561asfd56asdf';
        $filename1  = $uuid . "_page1.pdf";
        $upload_path_1 = FCPATH . 'assets/uploads/download_pack/' . $filename1;
        $cmd = '/usr/local/bin/wkhtmltopdf --zoom 1.1 -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/proposal/download_pack_view ' . $upload_path_1;
        exec($cmd);

        $filename  = 'SOLAR_PROPOSAL_1';
        $filename = str_replace(array('.', ',', ' ', '/', '&', "'"), '_', $filename);
        $filename = preg_replace('/[^A-Za-z0-9 ._&\-]/', '_', $filename);
        $filename = $filename . '.pdf';
        $final_upload_path = FCPATH . 'assets/uploads/download_pack/' . $filename;
        $cmd = 'gs -dNOPAUSE  -sDEVICE=pdfwrite -sOUTPUTFILE=' . $final_upload_path . ' -dBATCH ' . $upload_path_1;
        exec($cmd);
        header('Location: ' . site_url('assets/uploads/download_pack/' . $filename));
        die;
    }
}

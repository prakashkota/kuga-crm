<table width="910"border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="background: #e6e6e6">
		<tr>
			<td valign="top" style="border: 0; font: 0"><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td><table width="840" border="0" align="center" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td colspan="2" height="20"></td>
								</tr>
								<tr>
									<td width="58" align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/header_lines.png" width="45" height="40" alt=""/></td>
									<td width="822" style="font-size: 25px; color: #000000; text-transform: uppercase"><strong><span style="color: #DE1717">Important </span>things to know</strong></td>
								</tr>
							</tbody>
						</table></td>
					</tr>
					<tr>
						<td height="50"></td>
					</tr>
					<tr>
						<td height="200" align="left" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/p7.jpg" width="840" alt=""/></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="20" >&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" style="border: 0; font: 0"><table width="910" border="0" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="420" valign="top"><table width="420" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td width="420" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/bottom_top-7.jpg" width="420" height="165" alt=""/></td>
							</tr>
							<tr>
								<td height="73" align="right" valign="top" style="background: #252324">
								    <table width="335" border="0" align="right" cellpadding="0" cellspacing="0">
    									<tbody>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['full_name'] ; ?></td>
    										</tr>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['company_contact_no'] ; ?></td>
    										</tr>
    										<tr>
    											<td height="22" style="color:#ffffff; font-size: 14px"><?php echo $proposal['email'] ; ?></td>
    										</tr>
    									</tbody>
								    </table>
								</td>
							</tr>
							<tr>
								<td valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/bottom-7.jpg" width="420" height="27" alt=""/></td>
							</tr>
						</tbody>
					</table></td>
					<td width="490" align="right" valign="top"><img src="<?php echo site_url('');; ?>assets/pdf_images/residential_solar/right-7.jpg" width="490" height="264" alt=""/></td>
				</tr>
			</tbody>
		</table></td>
	</tr>
	<tr>
		<td height="125" >&nbsp;</td>
	</tr>
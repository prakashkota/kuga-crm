<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @Job Card Controller
 * @version 1.0
 */
class TenderJobCards extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library("pdf");
        $this->load->library("Email_Manager");
        $this->load->model("TenderLead_Model", "lead");
        if (!$this->aauth->is_loggedin() && $this->uri->segment(3) != 'generate_draft_card_pdf') {
            redirect('admin/login');
        }
    }
    
    
    public function add($uuid = FALSE){
        $data = array();
        $deal_ref = $this->input->get('deal_ref');
        if (isset($deal_ref) && $deal_ref != '') {
            $lead_data = $this->lead->get_leads($deal_ref);
            
            if (empty($lead_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Solar Quote creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            //Due to Some ParseJSON issue we need to unset note column
            unset($lead_data['note']);
            $data['lead_uuid'] = $lead_data['uuid'];
            $data['lead_data'] = $lead_data;

            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('tender_job_cards/main');
            
        }else if ($uuid) {
            $job_card_data = $this->common->fetch_row('tbl_tender_job_card_form', '*', array('uuid' => $uuid));
            
            if (empty($job_card_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Job Card creation cant be initiated, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            
            $lead_uuid = $this->common->fetch_cell('tbl_tender_leads','uuid',array('id' => $job_card_data['lead_id']));
            $data['lead_uuid'] = $lead_uuid;
            $data['job_card_uuid'] = $uuid;
            
            $this->load->view('partials/header', $data);
            $this->load->view('partials/sidebar');
            $this->load->view('partials/footer');
            $this->load->view('tender_job_cards/main');
            
        }else {
            redirect('admin/dashboard');
        }
    }
    
    
    public function fetch_job_card_form_data() {
        $data = array();
        if (!empty($this->input->get())) {
            $booking_data = $this->common->fetch_row('tbl_tender_job_card_form', '*', array('uuid' => $this->input->get('uuid')));
            $data['job_card_data'] = $booking_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    
    public function save_job_card_form(){
        $data = array();
        if (!empty($this->input->post())) {
            $is_job_card_exists = $this->common->fetch_row('tbl_tender_job_card_form', '*', array('uuid' => $this->input->post('uuid')));
            
            $job_form_data = json_encode($this->input->post('job_card'));
            $form_data = array();
            
            $form_data['uuid'] = $this->input->post('uuid');
            $form_data['lead_id'] = $this->input->post('lead_id');
            $form_data['user_id'] = $this->aauth->get_user_id();
            $form_data['job_form_data'] = $job_form_data;
            
            if (empty($is_job_card_exists)) {
                $insert_data = array();
                $insert_data = $form_data;
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                $stat = $this->common->insert_data('tbl_tender_job_card_form', $insert_data);

            }else{
                $card_id = $is_job_card_exists['id'];
                $update_data = array();
                $update_data = $form_data;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $stat = $this->common->update_data('tbl_tender_job_card_form', array('id' => $card_id), $update_data);

            }
            
            if ($stat) {
                $data['status'] = 'Job Card Form Data Saved Successfully.';
                $data['success'] = TRUE;
            } else {
                $data['status'] = 'Nothing to Update';
                $data['success'] = FALSE;
            }
            
        } else {
            $data['status'] = 'Invalid Request';
            $data['success'] = FALSE;
        }
        echo json_encode($data);
        die;
    }
    
    
    public function generate_card_pdf($uuid){
        
        $data = array();
        try{
            $job_card_data = $this->common->fetch_row('tbl_tender_job_card_form', '*', array('uuid' => $uuid));
            if (empty($job_card_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Job Card Form pdf cant be created, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            
            $job_card_details = json_decode($job_card_data['job_form_data']);
            $form_data['job_card_details'] = $job_card_details;
            
            $view = $this->input->get('view');
            $ref = $this->input->get('ref');
            
            if(isset($view) && $view == 'page1'){
                $html = $this->load->view('pdf_files/tender_job_cards/page1', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page2'){
                $html = $this->load->view('pdf_files/tender_job_cards/page2', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page3'){
                $html = $this->load->view('pdf_files/tender_job_cards/page3', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page4'){
                $html = $this->load->view('pdf_files/tender_job_cards/page4', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page5'){
                $html = $this->load->view('pdf_files/tender_job_cards/page5', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page6'){
                
                $dataIndex = $this->input->get('dataIndex');
                $mcb = $job_card_details->mcb;

                $data['job_card_details'] = array(
                    "first_heading" => $mcb->first_heading[$dataIndex],
                    "second_heading" => $mcb->second_heading[$dataIndex],
                    "third_heading" => $mcb->third_heading[$dataIndex],
                    "fourth_heading" => $mcb->fourth_heading[$dataIndex],
                    "connection_point_photo" => $mcb->connection_point_photo[$dataIndex],
                    "main_switch_board" => $mcb->main_switch_board[$dataIndex],
                    "additional_eci_one" => $mcb->additional_eci_one[$dataIndex],
                    "additional_eci_two" => $mcb->additional_eci_two[$dataIndex],
                );
                
                
                $html = $this->load->view('pdf_files/tender_job_cards/page6', $data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page7'){
                
                $dataIndex = $this->input->get('dataIndex');
                $roofing = $job_card_details->roofing;

                $data['job_card_details'] = array(
                    "roof_underneath" => $roofing->roof_underneath[$dataIndex],
                    "number_of_purlins" => $roofing->number_of_purlins[$dataIndex],
                    "purlin_spacing" => $roofing->purlin_spacing[$dataIndex],
                    "additional_notes" => $job_card_details->additional_notes,
                    "heading" => 'Roof '.($dataIndex + 1).' from underneath',
                );
                
                if($dataIndex > 0){
                    $html = $this->load->view('pdf_files/tender_job_cards/page7_dynamic', $data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/tender_job_cards/page7', $data, TRUE);
                    echo $html;die;
                }
            }
            
            $html = $this->load->view('pdf_files/tender_job_cards/main', $form_data, TRUE);
            
            $page1 = 'page_1'.$uuid;
            $upload_path_img1 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page1 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page1 '. $upload_path_img1;
            exec($cmd);
            
            $page2 = 'page_2'.$uuid;
            $upload_path_img2 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page2 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page2 '. $upload_path_img2;
            exec($cmd);
            
            $upload_path_img3 = $upload_path_img4 = $upload_path_img5 = $upload_path_img6 = $upload_path_img7 = "";
            if($job_card_details->is_site_incpection == 'yes'){
                $page3 = 'page_3'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page3 '. $upload_path_img3;
                exec($cmd);
                
                $page4 = 'page_4'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page4 '. $upload_path_img4;
                exec($cmd);
                
                $page5 = 'page_5'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page5 '. $upload_path_img5;
                exec($cmd);
                
                $mcb = $job_card_details->mcb;
                for($i = 0; $i < count($mcb->first_heading); $i++){
                    if(!empty($mcb->first_heading[$i])){
                        
                        if($i > 0){
                            $page6 = 'page_6_'.$i.'_'.$uuid;
                        }else{
                            $page6 = 'page_6'.$uuid;
                        }
                        
                        $upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page6 . '.pdf';
                        $cmd = "/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm 'https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/".$uuid."?view=page6&&dataIndex=".$i."'  ". $upload_path;
                        exec($cmd);
                        $upload_path_img6 .= ' '.$upload_path;
                    }
                }
                
                $roofing = $job_card_details->roofing;
                for($i = 0; $i < count($roofing->roof_underneath); $i++){
                    if(!empty($roofing->roof_underneath[$i])){
                        
                        if($i > 0){
                            $page7 = 'page_7_'.$i.'_'.$uuid;
                        }else{
                            $page7 = 'page_7'.$uuid;
                        }
                        
                        $upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page7 . '.pdf';
                        $cmd = "/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm 'https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/".$uuid."?view=page7&dataIndex=".$i."' ". $upload_path;
                        exec($cmd);
                        
                        $upload_path_img7 .= ' '.$upload_path;
                    }
                }
            }
            
            $filename = 'Kuga_Sales_Job_card_'.$form_data['job_card_details']->project_name.'_'.date('m-d-Y_his');
            $filename      = str_replace(array('.', ',', ' '), '_', $filename);
            
            $filename = preg_replace("/[^a-z0-9\_\-\.]/i", '', $filename);
            
            $final_upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/'.$filename . '.pdf';
            $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6.' '.$upload_path_img7;
            exec($cmd);
            
            $nfilename = $filename . '.pdf';
  
            
            $filePath = site_url('assets/uploads/tender_job_card_pdf_files/'.$nfilename);
            $this->common->update_data('tbl_tender_job_card_form', array('id' => $job_card_data['id']), array('job_form_file' =>  $filePath ,'status' => 1));
            
            
            $form_data['job_card_details']->attachment = $filePath;
            // $toEmail = 's.gajjel@runmarketing.com.au';
            
            $user_id = $this->aauth->get_user_id();
            $toEmail = $this->common->fetch_cell('aauth_users','email',['id' => $user_id]);
            $this->email_manager->send_job_card_success_email($form_data['job_card_details'], $toEmail);
            if(isset($ref) && !empty($ref) && $ref == 'download'){
                
                $filename = str_replace('.pdf','',$nfilename);
                $pdf_url = str_replace(' ', '%20', $filename);
                $pdf_url = site_url('assets/uploads/tender_job_card_pdf_files/' . $pdf_url . '.pdf');
                redirect($pdf_url);
                exit();
            }else{
                $data['filePath'] = $filePath;
                $data['status'] = 'Pdf Generated and Saved Successfully';
                $data['success'] = TRUE;
            }
        }catch(Exception $e){
            $data['error'] = $e->getMessage();
            $data['status'] = 'Pdf Generation Failed';
            $data['success'] = False;
        }
        
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
        die;
    }
    
    public function generate_draft_card_pdf($uuid){
        
        $data = array();
        try{
            $job_card_data = $this->common->fetch_row('tbl_tender_job_card_form', '*', array('uuid' => $uuid));
            if (empty($job_card_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Job Card Form pdf cant be created, no deal found with reference id.</div>');
                redirect(site_url('admin/tender-lead/add'));
            }
            
            $job_card_details = json_decode($job_card_data['job_form_data']);
            $form_data['job_card_details'] = $job_card_details;
            
            $view = $this->input->get('view');
            $ref = $this->input->get('ref');
            
            if(isset($view) && $view == 'page1'){
                $html = $this->load->view('pdf_files/tender_job_cards/page1', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page2'){
                $html = $this->load->view('pdf_files/tender_job_cards/page2', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page3'){
                $html = $this->load->view('pdf_files/tender_job_cards/page3', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page4'){
                $html = $this->load->view('pdf_files/tender_job_cards/page4', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page5'){
                $html = $this->load->view('pdf_files/tender_job_cards/page5', $form_data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page6'){
                
                $dataIndex = $this->input->get('dataIndex');
                $mcb = $job_card_details->mcb;

                $data['job_card_details'] = array(
                    "first_heading" => $mcb->first_heading[$dataIndex],
                    "second_heading" => $mcb->second_heading[$dataIndex],
                    "third_heading" => $mcb->third_heading[$dataIndex],
                    "fourth_heading" => $mcb->fourth_heading[$dataIndex],
                    "connection_point_photo" => $mcb->connection_point_photo[$dataIndex],
                    "main_switch_board" => $mcb->main_switch_board[$dataIndex],
                    "additional_eci_one" => $mcb->additional_eci_one[$dataIndex],
                    "additional_eci_two" => $mcb->additional_eci_two[$dataIndex],
                );
                
                
                $html = $this->load->view('pdf_files/tender_job_cards/page6', $data, TRUE);
                echo $html;die;
            }else if(isset($view) && $view == 'page7'){
                
                $dataIndex = $this->input->get('dataIndex');
                $roofing = $job_card_details->roofing;

                $data['job_card_details'] = array(
                    "roof_underneath" => $roofing->roof_underneath[$dataIndex],
                    "number_of_purlins" => $roofing->number_of_purlins[$dataIndex],
                    "purlin_spacing" => $roofing->purlin_spacing[$dataIndex],
                    "additional_notes" => $job_card_details->additional_notes,
                    "heading" => 'Roof '.($dataIndex + 1).' from underneath',
                );
                
                if($dataIndex > 0){
                    $html = $this->load->view('pdf_files/tender_job_cards/page7_dynamic', $data, TRUE);
                    echo $html;die;
                }else{
                    $html = $this->load->view('pdf_files/tender_job_cards/page7', $data, TRUE);
                    echo $html;die;
                }
            }
            
            $html = $this->load->view('pdf_files/tender_job_cards/main', $form_data, TRUE);
            
            $page1 = 'page_1'.$uuid;
            $upload_path_img1 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page1 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page1 '. $upload_path_img1;
            exec($cmd);
            
            $page2 = 'page_2'.$uuid;
            $upload_path_img2 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page2 . '.pdf';
            $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page2 '. $upload_path_img2;
            exec($cmd);
            
            $upload_path_img3 = $upload_path_img4 = $upload_path_img5 = $upload_path_img6 = $upload_path_img7 = "";
            if($job_card_details->is_site_incpection == 'yes'){
                $page3 = 'page_3'.$uuid;
                $upload_path_img3 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page3 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page3 '. $upload_path_img3;
                exec($cmd);
                
                $page4 = 'page_4'.$uuid;
                $upload_path_img4 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page4 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page4 '. $upload_path_img4;
                exec($cmd);
                
                $page5 = 'page_5'.$uuid;
                $upload_path_img5 = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page5 . '.pdf';
                $cmd = '/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/'.$uuid.'?view=page5 '. $upload_path_img5;
                exec($cmd);
                
                $mcb = $job_card_details->mcb;
                for($i = 0; $i < count($mcb->first_heading); $i++){
                    if(!empty($mcb->first_heading[$i])){
                        
                        if($i > 0){
                            $page6 = 'page_6_'.$i.'_'.$uuid;
                        }else{
                            $page6 = 'page_6'.$uuid;
                        }
                        
                        $upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page6 . '.pdf';
                        $cmd = "/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm 'https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/".$uuid."?view=page6&&dataIndex=".$i."'  ". $upload_path;
                        exec($cmd);
                        $upload_path_img6 .= ' '.$upload_path;
                    }
                }
                
                $roofing = $job_card_details->roofing;
                for($i = 0; $i < count($roofing->roof_underneath); $i++){
                    if(!empty($roofing->roof_underneath[$i])){
                        
                        if($i > 0){
                            $page7 = 'page_7_'.$i.'_'.$uuid;
                        }else{
                            $page7 = 'page_7'.$uuid;
                        }
                        
                        $upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/pages/'.$page7 . '.pdf';
                        $cmd = "/usr/local/bin/wkhtmltopdf -L 0mm -R 0mm -T 0mm -B 0mm 'https://kugacrm.com.au/admin/tender-lead/generate_draft_card_pdf/".$uuid."?view=page7&dataIndex=".$i."' ". $upload_path;
                        exec($cmd);
                        
                        $upload_path_img7 .= ' '.$upload_path;
                    }
                }
                
            }
            
            
            $filename = 'Kuga_Sales_Job_card_'.$form_data['job_card_details']->project_name.'_'.date('m-d-Y_his');
            $filename      = str_replace(array('.', ',', ' '), '_', $filename);
            
            $filename = preg_replace("/[^a-z0-9\_\-\.]/i", '', $filename);
            
            $final_upload_path = FCPATH . 'assets/uploads/tender_job_card_pdf_files/'.$filename . '.pdf';
            $cmd = 'gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$final_upload_path.' -dBATCH '. $upload_path_img1 .' '. $upload_path_img2 .' '. $upload_path_img3 .' '. $upload_path_img4 .' '. $upload_path_img5 .' '. $upload_path_img6.' '.$upload_path_img7;
            exec($cmd);
            
            $nfilename = $filename . '.pdf';
  
            
            $filePath = site_url('assets/uploads/tender_job_card_pdf_files/'.$nfilename);
            $this->common->update_data('tbl_tender_job_card_form', array('id' => $job_card_data['id']), array('job_form_file' =>  $filePath));
            
            
            $form_data['job_card_details']->attachment = $filePath;
            // $toEmail = 's.gajjel@runmarketing.com.au';
            
            $user_id = $this->aauth->get_user_id();
            $toEmail = $this->common->fetch_cell('aauth_users','email',['id' => $user_id]);
            $this->email_manager->send_job_card_success_email($form_data['job_card_details'], $toEmail);
            if(isset($ref) && !empty($ref) && $ref == 'download'){
                
                $filename = str_replace('.pdf','',$nfilename);
                $pdf_url = str_replace(' ', '%20', $filename);
                $pdf_url = site_url('assets/uploads/tender_job_card_pdf_files/' . $pdf_url . '.pdf');
                redirect($pdf_url);
                exit();
            }else{
                $data['filePath'] = $filePath;
                $data['status'] = 'Pdf Generated and Saved Successfully';
                $data['success'] = TRUE;
            }
        }catch(Exception $e){
            $data['error'] = $e->getMessage();
            $data['status'] = 'Pdf Generation Failed';
            $data['success'] = False;
        }
        
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data);
        die;
    }
}
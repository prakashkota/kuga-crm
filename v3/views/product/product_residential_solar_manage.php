<div class="page-wrapper d-block clearfix">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">
            <?php echo $this->aauth->get_user_groups()[0]->name; ?> - <?php echo (isset($meta_title)) ? $meta_title : 'Manage Product'; ?></h1>
            <?php /**
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right" role="group" >
                    <a class="btn btn-outline-primary" href="<?php echo site_url('admin/product/residential_solar/add') ?>"><i class="icon-plus"></i> Add New </a>
                </div>
                <div class="btn-group float-md-right hidden">
                    <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="javascript:void(0);" onclick="loadProductImportModal();"><i class="icon-cloud-upload"></i> Import Products </a>
                        <a class="dropdown-item" href="javascript:void(0);" onclick="loadProductVariantImportModal();" ><i class="icon-cloud-upload"></i> Import Product  Variant</a>
                        <a class="dropdown-item"href="<?php echo site_url('admin/product/residential_solar/export_csv?action=export_product_csv'); ?>" target="__blank"><i class="icon-cloud-download"></i> Export Products </a>
                        <a class="dropdown-item" href="<?php echo site_url('admin/product/residential_solar/export_csv?action=export_product_variant_csv'); ?>" target="__blank"><i class="icon-cloud-download"></i> Export Product Variant</a>
                    </div>
                </div>
            </div>
            */ ?>
    </div>
    <!-- BEGIN: message  -->    
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->  
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <div class="form-block d-block clearfix">
                        <div class="table-responsive">
                            <table id="list" class="table table-striped" >
                                <thead>
                                    <tr>
                                        <th class="text-center">Product</th>
                                        <th class="text-center">Added By</th>
                                        <th class="text-center">Application</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Availability</th>
                                        <th class="text-center">Model Number</th>
                                        <th class="text-center">Created At</th>
                                        <th class="text-center">Updated At</th>
                                        <!--<th class="text-center">Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($product_list as $key => $value) {
                                        $date_hack_created_at = explode(' ', $value['created_at']);
                                        $date_hack_created_at = explode('-', $date_hack_created_at[0]);
                                        if ($value['updated_at'] != '' && $value['updated_at'] != NULL) {
                                            $date_hack_updated_at = explode(' ', $value['updated_at']);
                                            $date_hack_updated_at = explode('-', $date_hack_updated_at[0]);
                                        }
                                        ?>
                                        <tr class="text-center">
                                            <td><?php echo $value['name']; ?></td>
                                            <td><?php echo $value['full_name']; ?></td>
                                            <td><?php echo $value['variant_application']; ?></td>
                                            <td><?php echo $value['typeName']; ?></td>
                                            <td><span class="hidden"><?php echo $value['availability']; ?></span><?php echo $value['availability'] ? 'Available' : 'Not Available'; ?></td>
                                            <td><?php echo $value['model_no']; ?></td>
                                            <td><span class="hidden"><?php echo $date_hack_created_at[0] . $date_hack_created_at[1] . $date_hack_created_at[2]; ?></span><?php echo date("d/m/Y", strtotime($value['created_at'])); ?></td>
                                            <td><span class="hidden"><?php echo ($value['updated_at'] != '' ) ? $date_hack_updated_at[0] . $date_hack_updated_at[1] . $date_hack_updated_at[2] : '-'; ?></span><?php echo ($value['updated_at'] != '' ) ? date("d/m/Y", strtotime($value['updated_at'])) : '-'; ?></td>
                                            <?php /**
                                            <td align="right">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item" href="<?php echo site_url('admin/product/residential_solar/edit/' . $value['id']) ?>"><i class="icon-pencil"></i> Edit</a>
                                                        <?php if ($this->aauth->is_member('Admin')) { ?>
                                                            <a class="dropdown-item" data-href="<?php echo site_url('admin/product/residential_solar/delete/' . $value['id']) ?>"  data-toggle="modal" data-target="#confirm-delete"><i class="icon-trash"></i> Delete</a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </td>
                                            */ ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 


    <div class="modal" id="product_csv_import_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Product CSV Importer</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form class="form-horizontal" action="functions.php" method="post" id="product_csv_import_form" enctype="multipart/form-data">
                        <fieldset>
                            <!-- File Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="filebutton">Select File</label>
                                <div class="col-md-4">
                                    <input type="file" name="product_csv_file" id="file" class="input-large">
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-4">
                                    <button type="button" id="product_csv_file_import" name="Import" class="btn btn-primary button-loading" data-loading-text="Importing...">Import</button>
                                </div>
                                <div class="loader1"></div>
                            </div>
                        </fieldset>
                        <div class="form-group" style="margin-top:5px;">
                            <h4 class="text-center">Steps to upload products</h4>
                            <ul>
                                <li> - For import, first export products CSV from export products button on action dropdown.</li>
                                <li> - If you want to update any exisitng products keep them in CSV else remove others.</li>
                                <li> - Important fields to keep in check while importing products , if you want to update existing product do not leave blank the <b>(id)</b> field in CSV. For creating new product just leave <b>(id)</b> field blank and fill required fields.</li>
                                <li> - After doing the above save the CSV and upload it again and press import.</li>
                            </ul>
                        </div>
                        <span class="help-block text-info" style="margin-top:5px;">
                            * This will allow you to upload only product not the variants so for variants refer import variant.
                        </span>

                    </form>
                </div>
            </div>
        </div>
    </div> 

    <div class="modal" id="product_variant_csv_import_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Product Variant CSV Importer</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form class="form-horizontal" action="functions.php" method="post" id="product_variant_csv_import_form" enctype="multipart/form-data">
                        <fieldset>
                            <!-- File Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="filebutton">Select File</label>
                                <div class="col-md-4">
                                    <input type="file" name="product_variant_csv_file" id="file" class="input-large">
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-4">
                                    <button type="button" id="product_variant_csv_file_import" name="Import" class="btn btn-primary button-loading" data-loading-text="Importing...">Import</button>
                                </div>
                                <div class="loader1"></div>
                            </div>
                        </fieldset>
                        <div class="form-group" style="margin-top:5px;">
                            <h4 class="text-center">Steps to upload product variants</h4>
                            <ul>
                                <li> - For import, first export that product variant CSV from export variant button on action dropdown.</li>
                                <li> - If you want to update any exisitng variant keep them in CSV else remove others.</li>
                                <li> - Important fields to keep in check while importing product variant , if you want to update existing variant do not leave blank the <b>(id)</b> field in CSV. For creating new variant just leave <b>(id)</b> field blank and fill all required fields.</li>
                                <li> - After doing the above save the CSV and upload it from import product variant option from action dropdown and press import.</li>
                            </ul>                             
                        </div>
                        <span class="help-block text-info" style="margin-top:5px;">
                            * Please do mention propoer product_id while importing variant for that product. 
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="<?php echo site_url(); ?>assets/js/product.js?v="<?php echo version; ?>></script>
    <script>
                        $(document).ready(function () {
                            $('#list').DataTable({"iDisplayLength": 100});
                            //Filter Product Type
                            var product_types = '<?php echo (isset($product_types) && !empty($product_types)) ? json_encode($product_types, JSON_HEX_APOS) : ''; ?>';
                            console.log(product_types)
                            product_types = JSON.parse(product_types);
                            var prd_type_select = document.createElement('select');
                            prd_type_select.setAttribute('id', 'prd_type_filter');
                            //state_select.className = 'form-control';
                            var option = document.createElement('option');
                            option.setAttribute('value', '');
                            option.innerHTML = 'Select Type';
                            prd_type_select.appendChild(option);
                            for (var i = 0; i < product_types.length; i++) {
                                var option = document.createElement('option');
                                option.setAttribute('value', product_types[i].typeName);
                                option.innerHTML = product_types[i].typeName;
                                prd_type_select.appendChild(option);
                            }
                            var prd_type_select_div = document.createElement('div');
                            prd_type_select_div.className = 'col-md-2';
                            prd_type_select_div.setAttribute('style', 'display:inline-block;');
                            prd_type_select_div.innerHTML = '<label style="margin:0 auto;">Filter By Type:</label>';
                            prd_type_select_div.appendChild(prd_type_select);

                            document.getElementById("list_filter").parentNode.insertBefore(prd_type_select_div, document.getElementById("list_filter"));
                            /* --------------------------------- availability filter -----------------------------------------*/
                            var prd_avail = document.createElement('select');
                            prd_avail.setAttribute('id', 'prd_avail');
                            //state_select.className = 'form-control';
                            var option = document.createElement('option');
                            option.setAttribute('value', '');
                            option.innerHTML = 'Select Availability';
                            prd_avail.appendChild(option);
                            var option = document.createElement('option');
                            option.setAttribute('value', 'Available');
                            option.innerHTML = 'Available';
                            prd_avail.appendChild(option);
                            var option = document.createElement('option');
                            option.setAttribute('value', 'Not Available');
                            option.innerHTML = 'Not Available';
                            prd_avail.appendChild(option);

                            var prd_avail_select_div = document.createElement('div');
                            prd_avail_select_div.className = 'col-md-2';
                            prd_avail_select_div.setAttribute('style', 'display:inline-block;');
                            prd_avail_select_div.innerHTML = '<label style="margin:0 auto;">Filter By Type:</label>';
                            prd_avail_select_div.appendChild(prd_avail);

                            document.getElementById("list_filter").parentNode.insertBefore(prd_avail_select_div, document.getElementById("list_filter"));



                            var table = $('#list').dataTable().api();

                            $('#prd_type_filter').change(function () {
                                table.columns(3)
                                        .search($(this).val(), true, false)
                                        .draw();
                            });
                            $('#prd_avail').change(function () {
                                var av = (this.value == 'Available') ? 1 : 0;
                                table.columns(4)
                                        .search(av, false, false, false)
                                        .draw();
                            });

                        });

                        function loadProductImportModal() {
                            $('#product_csv_import_modal').modal('show');
                        }

                        function loadProductVariantImportModal() {
                            $('#product_variant_csv_import_modal').modal('show');
                        }
    </script>
</body>

</html>

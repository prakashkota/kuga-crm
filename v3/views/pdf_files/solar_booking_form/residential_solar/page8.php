<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/embedded-generation.png'; ?>" alt="" width="910" height="124" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10" style="font-family:Calibri, sans-serif; font-size:14px;">To:</td>
                                <td width="350"  style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                    <strong><?php echo $booking_form->to; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="10" style="font-family:Calibri, sans-serif; font-size:14px;">I,</td>
                                <td width="350"  style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                    <strong><?php echo $booking_form->i; ?></strong>
                                </td>
                                <td width="450" valign="top" style=" height:40px; font-family:Calibri, sans-serif; font-size:13px; padding:5px">
                                    nominate Kuga Electrical for anywork related to the embedded generation
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td width="220" style="font-family:Calibri, sans-serif; font-size:14px;">Connection process of Company,</td>
                                <td width="610" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                    <strong><?php echo $booking_form->company_name; ?></strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td width="100" style="font-family:Calibri, sans-serif; font-size:14px;">at the address</td>
                                <td width="720" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                    <strong><?php echo $booking_form->company_address; ?></strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="165" style="font-family:Calibri, sans-serif; font-size:14px;">NMI:</td>
                                                        <td width="240" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                                            <strong><?php echo $booking_form->nmi; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="154" style="font-family:Calibri, sans-serif; font-size:14px;">Meter Number:</td>
                                                        <td width="251" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                                            <strong><?php echo $booking_form->meter_no; ?></strong>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Calibri, sans-serif; font-size:14px; color:#333333">Regards,</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="border:solid 1px #e4e4e4; padding:5px;">
                        <img width="800" height="200" src="<?php echo $this->config->item('live_url') . 'assets/uploads/solar_booking_form_files/'. $booking_form->signature; ?>" />
                    </td>
                </tr>
                <tr>
                    <td height="35" align="center" style="font-family:Calibri, sans-serif; font-size:14px; color:#333333">Plesae Sign Here</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Calibri, sans-serif; font-size:14px;">Name:</td>
                                            <td width="325" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                                <strong><?php echo $booking_form->customer_name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                                <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Calibri, sans-serif; font-size:14px;">Position:</td>
                                            <td width="325" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                                <strong><?php echo $booking_form->customer_position; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="70" style="font-family:Calibri, sans-serif; font-size:14px;">Company:</td>
                                            <td width="750" style="padding:5px; border:solid 1px #e4e4e4; height:40px; font-family:Calibri, sans-serif; font-size:13px;">
                                                <strong><?php echo $booking_form->customer_company_name; ?></strong>
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="340">&nbsp;</td>
    </tr>
    <tr>
        <td height="90" class="bg-bottom" style="background:#ededed"><table width="819" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" style="font-size:15px; font-family:Calibri, sans-serif"><strong>ABN 39 616 409 584</strong></td>
                            </tr>
                            <tr>
                                <td style="font-size:16px; font-family:Calibri, sans-serif; color:#c92422;"><em><strong>13KUGA.COM.AU</strong></em></td>
                            </tr>
                        </table></td>
                    <td width="273"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">service@13kuga.com.au</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">23 Lionel Rd, Mount Waverley, VIC 3149</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-size:12px; font-family:Calibri, sans-serif">26 Prince william Drive, Seven Hills, NSW 2147</td>
                            </tr>
                        </table></td>
                    <td width="273" align="right"><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom_right.jpg'; ?>" width="117" height="56" />8</td>
                </tr>
            </table></td>
    </tr>
</table>

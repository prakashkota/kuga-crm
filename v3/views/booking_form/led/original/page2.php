<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/led_booking_form/header_01.jpg'; ?>" width="910" height="126" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
      <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Business Details</strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Entity Name:</td>
                	<td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><input required="" id="" name="" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                </tr>
              </table></td>
            <td width="415" valign="top" ><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">ABN/ACN:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><input required="" id="" name="" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
              </tr>
            </table></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Tarading Name:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:35px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Tlogic</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Leased or Owned:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Tenant</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Street Address:</td>
            <td width="708" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Bldg 2, Office 2, 88 Ricketts Rd</strong></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Suburb:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Mt Waverley</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Postcode:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>3149</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
       <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">BCA Hours:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>3000</strong></td>
                </tr>
              </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Descrption:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Office</strong></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
      <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Authorised Details</strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">First Name:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Ben</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Last Name:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Dobdon</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Position:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Director</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Email:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>ben@tlogic.com.au</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Mobile Number:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>0450618000</strong></td>
                </tr>
              </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                <td width="283" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>0395444686</strong></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
      <tr>
        <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Accounts Details</strong></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Accounts First Name:</td>
                <td width="240" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Cassandra</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Accounts First Name:</td>
                <td width="251" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Anastasia</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="415" valign="top" style="padding-right:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Accounts Mobile Name:</td>
                <td width="240" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>0450618000</strong></td>
              </tr>
            </table></td>
            <td width="415" valign="top" style="padding-left:10px"><table width="405" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="154" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Land Line:</td>
                <td width="251" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>0395444686</strong></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Accounts Email Address:</td>
            <td width="665" valign="top" style="border:solid 1px #e4e4e4; height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><strong>Accounts@tlogic.com.au</strong></td>
            </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="100">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?php echo $this->config->item('live_url').'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>

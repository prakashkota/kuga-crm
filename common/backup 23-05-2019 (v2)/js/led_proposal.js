
var led_proposal_manager = function (options) {
    var self = this;
    this.userid = (options.userid && options.userid != '') ? options.userid : '';
    this.proposal_data = (options.proposal_data && options.proposal_data != '') ? JSON.parse(options.proposal_data) : {};
    this.lead_data = (options.lead_data && options.lead_data != '') ? JSON.parse(options.lead_data) : {};
    this.cust_id = '';
    if (Object.keys(self.lead_data).length > 0) {
        self.cust_id = self.lead_data.cust_id;
        //Populate Customer Details
        $('#site_name').val(self.lead_data.site_name);
        $('#site_address').val(self.lead_data.address);
        $('#customer_name').val(self.lead_data.first_name + ' ' + self.lead_data.last_name);
        $('#site_contact_name').val(self.lead_data.site_contact_name);

        //Populdate Energy Usage Data if any
        $('#energy_rate').val(self.proposal_data.energy_rate);
        $('#operatingHoursPerDay').val(self.proposal_data.op_hrs);
        $('#operatingDaysPerWeek').val(self.proposal_data.op_days);
        
        //Populdate Finance Details
        $('#term').val(self.proposal_data.term);
        $('#monthly_payment_plan').val(self.proposal_data.monthly_payment_plan);
        if(self.proposal_data.term != null){
            $('#delete_proposal_finance_btn').removeClass('hidden');
        }
    }

    $('#space_type').change(function () {
        var item_hours = $('#space_type option:selected').attr('data-hours');
        $('#hours').val(item_hours);
        //self.space_type_by_hours(item_hours);
    });

    $('#ep_id').change(function () {
        self.fetch_new_product_by_existing();
    });

    $('#add_product_btn').click(function () {
        self.add_proposal_product();
    });

    $('#update_product_btn').click(function () {
        self.add_proposal_product();
    });

    $('#np_id,#hours,#lifetime,#space_type').change(function () {
        self.fetch_rebate_value();
    });

    $('#save_proposal_btn').click(function () {
        self.save_led_proposal();
    });

    $('#cancel_product_btn').click(function () {
        document.getElementById('led_products_form').reset();
        $(this).addClass('hidden');
        $('#update_product_btn').addClass('hidden');
        $('#add_product_btn').removeClass('hidden');
        document.getElementById('led_product_id').value = '';
    });
    
    $('#ae_id').change(function () {
        var item_cost = $('#ae_id option:selected').attr('data-cost');
        $('#ae_cost').val(item_cost);
    });
    
    $('#add_additional_item_btn').click(function () {
        self.add_proposal_additional_item();
    });
    
    $('#manage_finance_proposal_btn').click(function(){
        $(this).hide();
        $('#led_proposal_finance_container').slideDown();
    });
    
    $('#term').change(function(){
        self.calculate_proposal_finance();
    });
    
    $('#save_proposal_finance_btn').click(function(){
        self.save_proposal_finance();
    });
    
    $('#delete_proposal_finance_btn').click(function(){
        self.delete_proposal_finance();
    });
    
    $('#download_led_proposal').click(function () {
        var flag = self.validate_led_proposal_pdf_data();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Proposal cannot be generated some required data is missing.');
            return false;
        } else {
            var url = base_url + 'admin/proposal/led_pdf_download/' + self.proposal_data.id;
            document.getElementById('download_led_proposal').setAttribute('href', url);
            document.getElementById('download_led_proposal').click();
        }

    });
    
    self.fetch_proposal_products();
    self.fetch_proposal_additional_items();
    self.fetch_led_proposal_details();
    self.edit_customer_location();
    self.save_customer_location();
    self.save_customer();
};

/** Common Functions **/

led_proposal_manager.prototype.toCommas = function (value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

led_proposal_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

led_proposal_manager.prototype.space_type_by_hours = function (item_hours) {
    $("#hours").val(item_hours);
    $('#lifetime').attr('disabled', 'disabled');
    $('#lifetime').val('');
    if (parseInt(item_hours) >= 5000) {
        $('#lifetime').removeAttr('disabled');
    }
    $("#hours").val(item_hours);
};

led_proposal_manager.prototype.fetch_rebate_value = function () {
    if ($('#np_id option:selected').val() != '') {
        var rebate = 0;
        var item_data = $('#np_id option:selected').attr('data-item');
        item_data = JSON.parse(item_data);
        var hours = document.getElementById('hours').value;
        hours = parseInt(hours);
        if (hours == 2000) {
            rebate = item_data['rebate_2000'];
        } else if (hours == 3000) {
            rebate = item_data['rebate_3000'];
        } else if (hours == 5000) {
            var item_data_id = $('#space_type option:selected').val();
            var item_data_text = $('#space_type option:selected').text();
            if (item_data_id == '8' || item_data_text === '5000hrs (lifetime A)') {
                if(item_data['rebate_5000_c'] != null){
                    rebate = item_data['rebate_5000_a'];
                }else{
                    rebate = item_data['rebate_5000'];
                }
            } else if (item_data_id == '7' || item_data_text === '5000hrs (lifetime C)') {
                if(item_data['rebate_5000_c'] != null){
                    rebate = item_data['rebate_5000_c'];
                }else{
                    rebate = item_data['rebate_5000'];
                } 
            } else {
                rebate = item_data['rebate_5000'];
            }
        }
        document.getElementById('veec').value = rebate;
    }
};

led_proposal_manager.prototype.create_led_proposal_details = function (data) {
    var self = this;
    //Highbay Row
    var total_highbay_row = document.createElement('tr');
    var total_highbay = document.createElement('td');
    total_highbay.innerHTML = 'Highbay';
    var total_highbay_value = document.createElement('td');
    total_highbay_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_highbay) + '</span>';
    total_highbay_row.appendChild(total_highbay);
    total_highbay_row.appendChild(total_highbay_value);
    
     //Batten Light Row
    var total_batten_light_row = document.createElement('tr');
    var total_batten_light = document.createElement('td');
    total_batten_light.innerHTML = 'Batten Light';
    var total_batten_light_value = document.createElement('td');
    total_batten_light_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_batten_light) + '</span>';
    total_batten_light_row.appendChild(total_batten_light);
    total_batten_light_row.appendChild(total_batten_light_value);
    
    
     //Panel Light Row
    var total_panel_light_row = document.createElement('tr');
    var total_panel_light = document.createElement('td');
    total_panel_light.innerHTML = 'Panel Light';
    var total_panel_light_value = document.createElement('td');
    total_panel_light_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_panel_light) + '</span>';
    total_panel_light_row.appendChild(total_panel_light);
    total_panel_light_row.appendChild(total_panel_light_value);
    
    
     //Flood Row
    var total_flood_light_row = document.createElement('tr');
    var total_flood_light = document.createElement('td');
    total_flood_light.innerHTML = 'Flood Light';
    var total_flood_light_value = document.createElement('td');
    total_flood_light_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_flood_light) + '</span>';
    total_flood_light_row.appendChild(total_flood_light);
    total_flood_light_row.appendChild(total_flood_light_value);
    
    
     //Down Light Row
    var total_down_light_row = document.createElement('tr');
    var total_down_light = document.createElement('td');
    total_down_light.innerHTML = 'Down Light';
    var total_down_light_value = document.createElement('td');
    total_down_light_value.innerHTML = '<span class="total-price">' + self.toCommas(data.total_down_light) + '</span>';
    total_down_light_row.appendChild(total_down_light);
    total_down_light_row.appendChild(total_down_light_value);

    
    var tr = document.createElement('tr');
    var td1 = document.createElement('td');
    
    td1.appendChild(total_highbay_row);
    td1.appendChild(total_batten_light_row);
    td1.appendChild(total_panel_light_row);
    td1.appendChild(total_flood_light_row);
    td1.appendChild(total_down_light_row);
    
    /**var additional_item_cost_row = document.createElement('tr');
    var additional_item_cost = document.createElement('td');
    additional_item_cost.innerHTML = 'Additional Item Cost (exGst)';
    var additional_item_cost_value = document.createElement('td');
    additional_item_cost_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.total_other) + '</span>';
    additional_item_cost_row.appendChild(additional_item_cost);
    additional_item_cost_row.appendChild(additional_item_cost_value);*/
    
    var total_project_cost_row = document.createElement('tr');
    var total_project_cost = document.createElement('td');
    total_project_cost.innerHTML = 'Total Project Cost (exGst)';
    var total_project_cost_value = document.createElement('td');
    total_project_cost_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.total_project_cost_exGST) + '</span>';
    total_project_cost_row.appendChild(total_project_cost);
    total_project_cost_row.appendChild(total_project_cost_value);
    
    var total_rebate_cost_row = document.createElement('tr');
    var total_rebate_cost = document.createElement('td');
    total_rebate_cost.innerHTML = 'Total Rebate Cost (exGst)';
    var total_rebate_cost_value = document.createElement('td');
    total_rebate_cost_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.total_discount_veec_exGST) + '</span>';
    total_rebate_cost_row.appendChild(total_rebate_cost);
    total_rebate_cost_row.appendChild(total_rebate_cost_value);
    
    var total_invesment_row = document.createElement('tr');
    var total_invesment = document.createElement('td');
    total_invesment.innerHTML = 'Total Investment (exGst)';
    var total_invesment_value = document.createElement('td');
    total_invesment_value.innerHTML = '<span class="total-price">$' + self.toCommas(data.total_investment_exGST) + '</span> <a href="#" data-toggle="tooltip" title="It is sum of all item cost (highbay,batten light etc.) and additional item cost." style="color:#000; font-size: 16px;"><i class="fa fa-question-circle"></i></a>';
    total_invesment_row.appendChild(total_invesment);
    total_invesment_row.appendChild(total_invesment_value);
    
    var total_roi_row = document.createElement('tr');
    var total_roi = document.createElement('td');
    total_roi.innerHTML = 'ROI';
    var total_roi_value = document.createElement('td');
    
    //var roi = (data.roi).toFixed(2) * 100 + '% <span style="font-size:12px;">(' + (data.roi).toFixed(2) +' Months)</span>';
    var roi = (data.roi).toFixed(2) +' Months';
    total_roi_value.innerHTML = '<span class="total-price">' + self.toCommas(roi) + '</span>';
    total_roi_row.appendChild(total_roi);
    total_roi_row.appendChild(total_roi_value);
    
    var td2 = document.createElement('tr');
    //td2.appendChild(additional_item_cost_row);
    td2.appendChild(total_project_cost_row);
    td2.appendChild(total_rebate_cost_row);
    td2.appendChild(total_invesment_row);
    td2.appendChild(total_roi_row);
    
    tr.appendChild(td1);
    tr.appendChild(td2);
    
    document.getElementById('proposal_item_details_wrapper').innerHTML = '';
    document.getElementById('proposal_cost_details_wrapper').innerHTML = '';
    document.getElementById('proposal_item_details_wrapper').appendChild(td1);
    document.getElementById('proposal_cost_details_wrapper').appendChild(td2);
};

led_proposal_manager.prototype.fetch_led_proposal_details = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_led_proposal_details',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        beforeSend: function(){
            //document.getElementById('proposal_cost_details_wrapper').appendChild(createLoader('Fetching Proposal Details....'));
            document.getElementById('proposal_cost_details_wrapper').appendChild(createPlaceHolder(false));
            document.getElementById('proposal_cost_details_wrapper').appendChild(createPlaceHolder(false));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
               self.create_led_proposal_details(stat.proposal_data);
               $('[data-toggle="tooltip"]').tooltip();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.calculate_proposal_finance = function () {
    var self = this;
    var term = document.getElementById('term').value;
    if(term == ''){
        toastr["error"]('Please Select term.');
        return false;
    }
    var data = {};
    data.proposal_id = self.proposal_data.id;
    data.term = term;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/calculate_finance',
        datatype: 'json',
        data: data,
        beforeSend: function(){
            $('#save_proposal_finance_btn').attr('disabled','disabled');
            $('#delete_proposal_finance_btn').attr('disabled','disabled');
            document.getElementById('finance_loader').appendChild(createLoader('Calcualting Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
               document.getElementById('monthly_payment_plan').value = stat.monthly_payment;
            }else{
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabled','disabled');
            $('#delete_proposal_finance_btn').removeAttr('disabled','disabled');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.save_proposal_finance = function () {
    var self = this;
    var term = document.getElementById('term').value;
    
    if(term == ''){
        toastr["error"]('Please Select term.');
        return false;
    }
    var data = '';
    data = $('#proposal_finance_form').serialize();
    data += '&proposal_id='+self.proposal_data.id;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/save_finance',
        datatype: 'json',
        data: data,
        beforeSend: function(){
            $('#save_proposal_finance_btn').attr('disabled','disabled');
            $('#delete_proposal_finance_btn').attr('disabled','disabled');
            document.getElementById('finance_loader').appendChild(createLoader('Saving Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if(stat.success){
                toastr["success"](stat.status);
                $('#delete_proposal_finance_btn').removeClass('hidden');
            }else{
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabled','disabled');
            $('#delete_proposal_finance_btn').removeAttr('disabled','disabled');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.delete_proposal_finance = function () {
    var self = this;
    var data = '';
    data = $('#proposal_finance_form').serialize();
    data += '&proposal_id='+self.proposal_data.id;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/delete_finance',
        datatype: 'json',
        data: data,
        beforeSend: function(){
             $('#save_proposal_finance_btn').attr('disabled','disabled');
             $('#delete_proposal_finance_btn').attr('disabled','disabled');
            document.getElementById('finance_loader').appendChild(createLoader('Deleting Finance Details....'));
        },
        success: function (stat) {
            var stat = JSON.parse(stat);
            if(stat.success){
                toastr["success"](stat.status);
                $('#delete_proposal_finance_btn').addClass('hidden');
                document.getElementById('proposal_finance_form').reset();
            }else{
                toastr["error"](stat.status);
            }
            $('#save_proposal_finance_btn').removeAttr('disabled','disabled');
            $('#delete_proposal_finance_btn').removeAttr('disabled','disabled');
            document.getElementById('finance_loader').innerHTML = '';
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#save_proposal_finance_btn').removeAttr('disabled','disabled');
            $('#delete_proposal_finance_btn').removeAttr('disabled','disabled');
            document.getElementById('finance_loader').innerHTML = '';
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
/** ENERGY USAGE RELATED FUNCTIONS **/

led_proposal_manager.prototype.validate_led_proposal_pdf_data = function () {
    var self = this;
    var flag = true;
    
    var elements = document.getElementById('led_energy_form').elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    
    return flag;
};

led_proposal_manager.prototype.validate_led_proposal_data = function () {
    var flag = true;
    var elements = document.getElementById("led_energy_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

led_proposal_manager.prototype.save_led_proposal = function () {
    var self = this;
    var flag = self.validate_led_proposal_data();
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#led_energy_form').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_led_proposal',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                self.fetch_led_proposal_details();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


/** LED PRODUCT RELATED FUNCTIONS **/

led_proposal_manager.prototype.fetch_new_product_by_existing = function (id) {
    var self = this;

    var ep_id = $('#ep_id').val();
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/product/fetch_led_new_product_by_existing',
        datatype: 'json',
        data: {ep_id: ep_id,state:self.lead_data.state_postal},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                var option;
                document.getElementById('np_id').innerHTML = '';
                if (stat.np_data.length > 0) {
                    for (var i = 0; i < stat.np_data.length; i++) {
                        option = self.create_product_select_option(stat.np_data[i], id);
                        document.getElementById('np_id').appendChild(option);
                    }
                } else {
                    option = document.createElement('option');
                    option.innerHTML = 'No Item Found with Selected Existing Fitting';
                    document.getElementById('np_id').appendChild(option);
                }
                $('#np_id').trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.create_product_select_option = function (data, selected) {
    var option = document.createElement('option');
    var value = data.np_id;
    option.setAttribute('value', value);
    option.setAttribute('data-item', JSON.stringify(data));
    if (data.np_id == selected) {
        option.setAttribute('selected', 'selected');
    }
    option.innerHTML = data.np_name;
    return option;
};

led_proposal_manager.prototype.validate_led_proposal_product_data = function () {
    var self = this;
    var flag = true;
    var space_type = $('#space_type').val();
    var elements = document.getElementById("led_products_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].getAttribute('id') == 'hours') {
                if (space_type != 6) {
                    if (elements[i].value == '') {
                        $(elements[i]).addClass('is-invalid');
                        flag = false;
                    }
                }
            } else if (elements[i].getAttribute('id') != 'lifetime') {
                if (elements[i].value == '') {
                    $(elements[i]).addClass('is-invalid');
                    flag = false;
                }

            }
        }
    }
    return flag;
};

led_proposal_manager.prototype.add_proposal_product = function () {
    var self = this;
    var flag = self.validate_led_proposal_product_data();
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#led_products_form').serialize();
    formData += '&led_product[proposal_id]=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_led_proposal_products',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#led_proposal_products_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_prodcut_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#led_proposal_products_table_body');
                        tbody.appendChild(row);
                    }
                    self.edit_proposal_product();
                    self.delete_proposal_product();
                    self.fetch_led_proposal_details();
                } else {
                    $('#led_proposal_products_table_body').html('<tr><td colspan="9" class="text-center">No product data found.</td></tr>');
                }
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.create_proposal_prodcut_table_row = function (data) {
    var tr = document.createElement('tr');
    var td_array = ['space_type_name', 'hours',  'ep_name', 'ep_qty', 'np_name', 'np_qty', 'veec', 'price'];
    for (var i = 0; i < td_array.length; i++) {
        var td = document.createElement('td');
        td.innerHTML = data[td_array[i]];
        tr.appendChild(td);
    }
    var td_action = document.createElement('td');

    var action_edit_btn = document.createElement('a');
    action_edit_btn.className = "btn-info btn-sm led_proposal_product_edit_btn";
    action_edit_btn.setAttribute('data-id', data.id);
    action_edit_btn.setAttribute('data-item', JSON.stringify(data));
    action_edit_btn.setAttribute('href', 'javascript:void(0);');
    action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

    var action_delete_btn = document.createElement('a');
    action_delete_btn.className = "btn-danger btn-sm ml-2 led_proposal_product_delete_btn";
    action_delete_btn.setAttribute('data-id', data.id);
    action_delete_btn.setAttribute('href', 'javascript:void(0);');
    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

    td_action.appendChild(action_edit_btn);
    td_action.appendChild(action_delete_btn);
    tr.appendChild(td_action);

    return tr;
};

led_proposal_manager.prototype.fetch_proposal_products = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_led_proposal_products',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#led_proposal_products_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_prodcut_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#led_proposal_products_table_body');
                        tbody.appendChild(row);
                    }
                    self.edit_proposal_product();
                    self.delete_proposal_product();
                } else {
                    $('#led_proposal_products_table_body').html('<tr><td colspan="9" class="text-center">No product data found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.edit_proposal_product = function () {
    var self = this;
    $('.led_proposal_product_edit_btn').click(function () {
        var id = $(this).attr('data-id');
        document.getElementById('led_product_id').value = id;

        var data = $(this).attr('data-item');
        data = JSON.parse(data);

        $('#add_product_btn').addClass('hidden');
        $('#cancel_product_btn').removeClass('hidden');
        $('#update_product_btn').removeClass('hidden');

        //Set space Type
        $('#space_type').val(data['space_type_id']);
        $('#space_type').trigger('change');

        //Populate data in form
        var elements = document.getElementById("led_energy_form").elements;
        for (var key in data) {
            $('#' + key).val(data[key]);
        }

        //Fetch and Set New Prorduct
        self.fetch_new_product_by_existing(data['np_id']);
        window.scrollTo({top: 500, behavior: 'smooth'});
    });
};

led_proposal_manager.prototype.delete_proposal_product = function () {
    var self = this;
    $('.led_proposal_product_delete_btn').click(function () {
        $this = $(this);
        var id = $(this).attr('data-id');
        $('#cancel_product_btn').click();
        $.ajax({
            url: base_url + 'admin/proposal/delete_led_proposal_products',
            type: 'post',
            data: {
                led_product_id:id,
                proposal_id:self.proposal_data.id
            },
            dataType: 'json',
            success: function (stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                    self.fetch_led_proposal_details();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};


/** ADDITIONAL ITEM RELATED FUNCTIONS **/

led_proposal_manager.prototype.create_proposal_additional_item_table_row = function (data) {
    var tr = document.createElement('tr');
    var td_array = ['ae_name', 'ae_qty', 'ae_cost'];
    for (var i = 0; i < td_array.length; i++) {
        var td = document.createElement('td');
        td.innerHTML = data[td_array[i]];
        tr.appendChild(td);
    }
    var td_action = document.createElement('td');

    var action_delete_btn = document.createElement('a');
    action_delete_btn.className = "btn-danger btn-sm ml-2 led_proposal_additional_item_delete_btn";
    action_delete_btn.setAttribute('data-id', data.id);
    action_delete_btn.setAttribute('href', 'javascript:void(0);');
    action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

    td_action.appendChild(action_delete_btn);
    tr.appendChild(td_action);

    return tr;
};

led_proposal_manager.prototype.fetch_proposal_additional_items = function () {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/proposal/fetch_led_proposal_additional_items',
        datatype: 'json',
        data: {proposal_id: self.proposal_data.id},
        success: function (stat) {
            var stat = JSON.parse(stat);
            if (stat.success == true) {
                $('#led_proposal_additional_item_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_additional_item_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#led_proposal_additional_item_table_body');
                        tbody.appendChild(row);
                    }
                    self.delete_proposal_additional_item();
                } else {
                    $('#led_proposal_additional_item_table_body').html('<tr><td colspan="4" class="text-center">No additional item found.</td></tr>');
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.validate_led_proposal_additional_item_data = function () {
    var flag = true;
    var elements = document.getElementById("add_additional_item_form").elements;
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).removeClass('is-invalid');
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                $(elements[i]).addClass('is-invalid');
                flag = false;
            }
        }
    }
    return flag;
};

led_proposal_manager.prototype.add_proposal_additional_item = function () {
    var self = this;
    var flag = self.validate_led_proposal_additional_item_data();
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        return false;
    }
    var formData = $('#add_additional_item_form').serialize();
    formData += '&proposal_id=' + self.proposal_data.id;
    $.ajax({
        url: base_url + 'admin/proposal/save_led_proposal_additional_items',
        type: 'post',
        data: formData,
        dataType: 'json',
        success: function (stat) {
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#led_proposal_additional_item_table_body').html('');
                if (stat.product_data.length > 0) {
                    for (var i = 0; i < stat.product_data.length; i++) {
                        var row = self.create_proposal_additional_item_table_row(stat.product_data[i]);
                        var tbody = document.querySelector('#led_proposal_additional_item_table_body');
                        tbody.appendChild(row);
                    }
                    self.delete_proposal_additional_item();
                    self.fetch_led_proposal_details();
                } else {
                    $('#led_proposal_additional_item_table_body').html('<tr><td colspan="4" class="text-center">No additional item found.</td></tr>');
                }
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

led_proposal_manager.prototype.delete_proposal_additional_item = function () {
    var self = this;
    $('.led_proposal_additional_item_delete_btn').click(function () {
        $this = $(this);
        var id = $(this).attr('data-id');
        $.ajax({
            url: base_url + 'admin/proposal/delete_led_proposal_additional_items',
            type: 'post',
            data: {
                item_id:id,
                proposal_id:self.proposal_data.id
            },
            dataType: 'json',
            success: function (stat) {
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.parent().parent('tr').remove();
                    self.fetch_led_proposal_details();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};


led_proposal_manager.prototype.edit_customer_location = function () {
    var self = this;
    $('#customer_location_edit_btn').click(function () {
        $('#site_id').val(self.proposal_data.site_id);
        $('#locationLatitude').val(self.lead_data.latitude);
        $('#locationLongitude').val(self.lead_data.longitude);
        $('.locationstreetAddressVal').val(self.lead_data.address);
        $('.locationPostCode').val(self.lead_data.postcode);
        $('.locationState').val(self.lead_data.state_id);
        $('#select2-cust_locationstreetAddress-container').html(self.lead_data.address);
        

        $('#company_cust_id').val(self.lead_data.cust_id);
        $('#site_name').val(self.lead_data.site_name);
        $('#site_address').val(self.lead_data.address);
        $('#contact_firstname').val(self.lead_data.first_name);
        $('#contact_lastname').val(self.lead_data.last_name);
        $('#contact_phone').val(self.lead_data.customer_contact_no);
        $('#contact_email').val(self.lead_data.customer_email);
        $('#contact_position').val(self.lead_data.position);

        $('#add_customer_location_modal_title').html('Edit Customer Location');
        $('#add_customer_location_modal').modal('show');
    });
}

led_proposal_manager.prototype.validate_customer_location_data = function () {
    var self = this;
    var flag = true;
    var locationPostCode = $('.locationPostCode').val();
    var locationState = $('.locationState').val();
    var locationstreetAddress = $('.locationstreetAddressVal').val();

    //Remove invalid
    $('.locationPostCode').removeClass('is-invalid');
    $('.locationState').removeClass('is-invalid');
    $('.select2-container').removeClass('form-control is-invalid');


    if (locationPostCode == '') {
        flag = false;
        $('.locationPostCode').addClass('is-invalid');
    }
    if (locationState == '') {
        flag = false;
        $('.locationState').addClass('is-invalid');
    }
    if (locationstreetAddress == '') {
        flag = false;
        $('.select2-container').addClass('form-control is-invalid');
    }

    return flag;
};

led_proposal_manager.prototype.save_customer_location = function () {
    var self = this;
    $(document).on('click', '#save_customer_location', function () {
        var cust_id = self.lead_data.cust_id;
        var site_id = document.getElementById('site_id').value;
        var flag = self.validate_customer_location_data();
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var locationAddForm = $('#locationAdd').serialize();
            locationAddForm += '&cust_id=' + cust_id;
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer_location',
                datatype: 'json',
                data: locationAddForm,
                beforeSend: function () {
                    if (site_id != '' && site_id != null && site_id != undefined) {
                        toastr["info"]('Updating Customer Location please wait....');
                    }
                    $('#customer_location_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer_location").attr("disabled", "disabled");
                },
                success: function (stat) {
                    toastr.remove();
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        self.lead_data.address = stat.location.address;
                        self.lead_data.postcode = stat.location.postcode;
                        self.lead_data.state_id = stat.location.state_id;
                        if(self.lead_data.postcode == '0' || self.lead_data.postcode == ''){
                            $('#site_postcode_warning').html('<i class="fa fa-warning"></i> Please provide postcode');
                        }else{
                            $('#site_postcode_warning').html('');
                        }
                        $('#site_address').val(self.lead_data.address);
                        $("#save_customer_location").removeAttr("disabled");
                        $('#add_customer_location_modal').modal('hide');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer_location").removeAttr("disabled");
                        $('#customer_location_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer_location").removeAttr("disabled");
                    $('#customer_location_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};

led_proposal_manager.prototype.validate_customer_data = function () {
    var self = this;
    var flag = true;
    var firstname = $('#contact_firstname').val();
    var lastname = $('#contact_lastname').val();
    var contactMobilePhone = $('#contact_phone').val();
    var customer_email = $('#contact_email').val();

    //Remove invalid
    $('#contact_phone').removeClass('is-invalid');
    $('#contact_firstname').removeClass('is-invalid');
    $('#contact_email').removeClass('is-invalid');

    if (contactMobilePhone == '') {
        flag = false;
        $('#contact_phone').addClass('is-invalid');
    }
    
    if (firstname == '') {
        flag = false;
        $('#contact_firstname').addClass('is-invalid');
    }
   
    if (customer_email == '') {
        flag = false;
        $('#contact_email').addClass('is-invalid');
    }

    return flag;
};

led_proposal_manager.prototype.save_customer = function () {
    var self = this;
    $(document).on('click', '#save_customer', function () {
        var cust_id = document.getElementById('company_cust_id').value;
        var flag = self.validate_customer_data();
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            var customerAddForm = $('#locationAdd').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/customer/save_customer',
                datatype: 'json',
                data: customerAddForm + '&action=add',
                beforeSend: function () {
                    if (cust_id != '' && cust_id != null && cust_id != undefined) {
                        toastr["info"]('Updating Customer please wait....');
                    } 
                    //$('#customer_loader').html(createLoader('Please wait while data is being saved'));
                    $("#save_customer").attr("disabled", "disabled");
                },
                success: function (stat) {
                    toastr.remove();
                    var stat = JSON.parse(stat);
                    if (stat.success == true) {
                        toastr["success"](stat.status);
                        //Populate Data from customer Modal to Front View
                        $('#customer_name').val($('#contact_firstname').val() + ' ' + $('#contact_lastname').val());
                        $('#customer_contact_no').val($('#contact_phone').val());
                        
                        self.lead_data.first_name = $('#contact_firstname').val();
                        self.lead_data.last_name = $('#contact_lastname').val();
                        self.lead_data.customer_contact_no = $('#contact_phone').val();
                        self.lead_data.customer_email = $('#contact_email').val();
                        self.lead_data.position = $('#contact_position').val();
                        
                        $("#save_customer").removeAttr("disabled");
                        $('#customer_loader').html('');
                    } else {
                        toastr["error"](stat.status);
                        $("#save_customer").removeAttr("disabled");
                        $('#customer_loader').html('');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#save_customer").removeAttr("disabled");
                    $('#customer_loader').html('');
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
};
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">

    <tr>
        <td colspan="3" style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/header_img_v3.PNG" alt="" style="display:inline-block" /></td>
    </tr>


    <tr>
        <td colspan="3" style="padding:10px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle;">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/heading_v3_16.PNG" alt="" style="display:inline-block" />
        </td>
    </tr>
    <tr>
        <td colspan="3" style="padding:20px 40px 10px; font-size:14px; line-height:14px; vertical-align:middle;">
            <img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/solar_production.PNG" alt="" style="display:inline-block" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="avg_kw_per_day_chart"></div>
        </td>
    </tr>
</table>

<table width="660" border="0" align="center" cellpadding="0" cellspacing="20" class="font-family">

    <tr>
        <td style="width:10%; padding:20px 40px 20px; background: #D3D2D2; text-align:center; font-weight:bold; font-size: 20px;" id="final_electricity_calculation"></td>
        <td style="width:10%; padding:20px 40px 20px; background: #D3D2D2; text-align:center; font-weight:bold; font-size: 20px;" id="bill_after_solar"></td>
        <td style="width:10%; padding:20px 40px 20px; background: #D3D2D2; text-align:center; font-weight:bold; font-size: 20px;" id="bill_saving"></td>
    </tr>
    <tr>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Bill Before Solar</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Bill After Solar</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">% Electricity Bill Saving</td>
    </tr>
    <tr>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="annual_solar_production"></td>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="offset_consumption"></td>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="export_to_grid">65.2</td>
    </tr>
    <tr>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Annual Solar Production</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Offset Consumption</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Export To Grid</td>
    </tr>
    <tr>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="percantage_engery_provided_solar"></td>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="bill_reduction"></td>
        <td style="width:10%; border:2px solid #EE1D25; padding:20px 40px 20px; text-align:center; font-weight:bold; font-size: 20px;" id="reduces_carbon"></td>
    </tr>
    <tr>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">% of Energy Provided by Solar</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">% of Bill Reduction</td>
        <td align="center" valign="top" style="font-weight: 700; color: #EE1D25;">Reduces Carbon Footprint by</td>
    </tr>
</table>


<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
    <tr>
        <td colspan="3" style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url') . 'assets/pdf_images/solar_v1'; ?>/footer_img_v3.PNG" alt="" style="display:inline-block" /></td>
    </tr>
</table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    var saving_meter_calculation = <?php echo $saving_meter_calculation; ?>;
    var system_performance = <?php echo $system_performance; ?>;


    google.load('visualization', '1', {
        packages: ['corechart']
    });

    var pvwatts_data_result = <?php echo $pvwatts_data_result; ?>;
    var pvSt_month1 = 0;
    var pvSt_month2 = 0;
    var pvSt_month3 = 0;
    var pvSt_month4 = 0;
    var pvSt_month5 = 0;
    var pvSt_month6 = 0;
    var pvSt_month7 = 0;
    var pvSt_month8 = 0;
    var pvSt_month9 = 0;
    var pvSt_month10 = 0;
    var pvSt_month11 = 0;
    var pvSt_month12 = 0;
    var pvtotal_production = 0;
    var pvSt_avg_prev = 0;
    var pvSt_avg = 0;

    $(document).ready(function() {
        $("#final_electricity_calculation").text(system_performance.FinalLoadChargesWithRates.toFixed(2));
        $("#annual_solar_production").text(system_performance.AnnualSolarProduction.toFixed(2));
        $("#offset_consumption").text(Math.round(saving_meter_calculation.offset_consumption));
        $("#percantage_engery_provided_solar").text(system_performance.FinalPercantageEnergyProvidedBySolar.toFixed(2) + '%');
        $("#bill_after_solar").text(system_performance.FinalBillAfterSolar.toFixed(2));
        $("#export_to_grid").text(system_performance.FinalPercantageOfExport.toFixed(2) + '%');

        $("#bill_saving").text((((system_performance.FinalLoadChargesWithRates.toFixed(2) - system_performance.FinalBillAfterSolar.toFixed(2)) * 100) / system_performance.FinalLoadChargesWithRates.toFixed(2)).toFixed(2) + '%');

        $("#reduces_carbon").text(system_performance.FinalReducesCarbon.toFixed(2));

        console.log(system_performance);

        getPvwattCalcAPI();
    })

    function getPvwattCalcAPI() {

        console.log(pvwatts_data_result.outputs);
        var pv_output = pvwatts_data_result;

        pvSt_month1 = pvSt_month1 + pv_output.outputs.ac_monthly[0];
        pvSt_month2 = pvSt_month2 + pv_output.outputs.ac_monthly[1];
        pvSt_month3 = pvSt_month3 + pv_output.outputs.ac_monthly[2];
        pvSt_month4 = pvSt_month4 + pv_output.outputs.ac_monthly[3];
        pvSt_month5 = pvSt_month5 + pv_output.outputs.ac_monthly[4];
        pvSt_month6 = pvSt_month6 + pv_output.outputs.ac_monthly[5];
        pvSt_month7 = pvSt_month7 + pv_output.outputs.ac_monthly[6];
        pvSt_month8 = pvSt_month8 + pv_output.outputs.ac_monthly[7];
        pvSt_month9 = pvSt_month9 + pv_output.outputs.ac_monthly[8];
        pvSt_month10 = pvSt_month10 + pv_output.outputs.ac_monthly[9];
        pvSt_month11 = pvSt_month11 + pv_output.outputs.ac_monthly[10];
        pvSt_month12 = pvSt_month12 + pv_output.outputs.ac_monthly[11];

        // pvSt_avg = pvSt_avg_prev + (pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12);

        pvSt_avg = pvSt_month1 + pvSt_month2 + pvSt_month3 + pvSt_month4 + pvSt_month5 + pvSt_month6 + pvSt_month7 + pvSt_month8 + pvSt_month9 + pvSt_month10 + pvSt_month11 + pvSt_month12
        pvSt_avg = Math.round(pvSt_avg / 12);
        pvSt_avg_prev = pvSt_avg_prev + pvSt_avg;
        pvSt_avg = pvSt_avg_prev;




        //  parseInt(pvtotal_production) + 
        pvtotal_production = parseInt(pv_output.outputs.ac_annual);
        // console.log(pvSt_month1)
        // $("#pv_json_data").html(pvwatts_data_html);

        // $("#pvtotal_production span").text(Math.round(pvtotal_production));

        google.setOnLoadCallback(drawChart);
    }

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'kWh', 'Average'],
            ['JAN', Math.round(pvSt_month1), pvSt_avg],
            ['FEB', Math.round(pvSt_month2), pvSt_avg],
            ['MAR', Math.round(pvSt_month3), pvSt_avg],
            ['APR', Math.round(pvSt_month4), pvSt_avg],
            ['MAY', Math.round(pvSt_month5), pvSt_avg],
            ['JUN', Math.round(pvSt_month6), pvSt_avg],
            ['JUL', Math.round(pvSt_month7), pvSt_avg],
            ['AUG', Math.round(pvSt_month8), pvSt_avg],
            ['SEP', Math.round(pvSt_month9), pvSt_avg],
            ['OCT', Math.round(pvSt_month10), pvSt_avg],
            ['NOV', Math.round(pvSt_month11), pvSt_avg],
            ['DEC', Math.round(pvSt_month12), pvSt_avg],
        ]);
        var options = {
            curveType: 'function',
            titleTextStyle: {
                fontName: 'Arial, sans-serif;',
                italic: false,
                bold: false,
                fontStyle: 'normal',
                fontSize: 12
            },
            legend: {
                position: 'bottom',
                textStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontSize: 14,
                    fontStyle: 'normal'
                }
            },
            hAxis: {
                format: '#\'$\'',
                titleTextStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: 'normal'
                }
            },
            vAxis: {
                format: '#\'kWh\'',
                title: "",
                titleTextStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: true,
                    fontSize: 13,
                    fontStyle: 'normal'
                },
                textStyle: {
                    fontName: 'Arial, sans-serif;',
                    italic: false,
                    bold: false,
                    fontSize: 11,
                    fontStyle: 'normal'
                }
            },
            width: 810,
            height: 400,
            legend: {
                position: 'none',
                textStyle: {
                    fontSize: 4,
                }
            },
            seriesType: 'bars',
            series: {
                0: {
                    color: '#A1CB79'
                },
                1: {
                    type: 'line',
                    color: '#000',
                    visibleInLegend: false
                }
            }
        };
        var chart = new google.visualization.ComboChart(document.getElementById('avg_kw_per_day_chart'));
        chart.draw(data, options);
    }
</script>
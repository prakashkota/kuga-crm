var job_card_manager = function(options){
    var self = this;
    this.lead_data = {};
    this.lead_uuid = (options.lead_uuid && options.lead_uuid !== '') ? options.lead_uuid : '';
    this.job_card_uuid = (options.job_card_uuid && options.job_card_uuid !== '') ? options.job_card_uuid : '';
    this.lead_id = '';
    this.mcb_page_count = 1;
    this.roofing_page_count = 1;
    
    if(self.job_card_uuid){
        var data = {};
        data.uuid = self.job_card_uuid;
        self.fetch_job_form_data(data);
    }else{
        var data = {};
        data.uuid = self.lead_uuid;
        self.fetch_lead_data(data);
    }
    
    $('#save_job_card').click(function () {
        self.save_job_card_form();
    });

    $('#generate_job_card_pdf').click(function () {
        self.save_job_card_form(true);
    });
    
    $('[name="job_card[is_site_incpection]"]').on('change',function(){
        var value = $(this).val();
        if(value == 'yes'){
            $('#page_3').removeClass('hidden');
            $('#page_4').removeClass('hidden');
            $('#page_5').removeClass('hidden');
            $('#page_6').removeClass('hidden');
            $('#page_7').removeClass('hidden');
            $('#addition_note2').addClass('hidden');
        }else{
            $('#page_3').addClass('hidden');
            $('#page_4').addClass('hidden');
            $('#page_5').addClass('hidden');
            $('#page_6').addClass('hidden');
            $('#page_7').addClass('hidden');
            $('#addition_note2').removeClass('hidden');
        }
    });
    
    $('[name="job_card[is_tilt]"]').on('change',function(){
        var value = $(this).val();
        if(value == 'yes'){
            $('#no_if_tilt_panels_td').removeClass('hidden');
            $('#no_if_tilt_panels').attr('required',true);
        }else{
            $('#no_if_tilt_panels_td').addClass('hidden');
            $('#no_if_tilt_panels').attr('required',false);
        }
    });
    
    
    $(document).on('click','#add_more_mcb_page_btn',function(){
        self.add_new_mcb_page();
    });
    
    $(document).on('click','.remove_mcb_page_btn',function(){
        var id = $(this).attr('data-id');
        $('#page_6_'+id).remove();
        self.mcb_page_count--;
    });
    
    $(document).on('click','#add_more_roof_page_btn',function(){
        self.add_new_roofing_page();
    });
    
    $(document).on('click','.remove_roofing_page_btn',function(){
        var id = $(this).attr('data-id');
        $('#page_7_'+id).remove();
        self.roofing_page_count--;
    });
    
    
    self.upload_image();
    self.hide_image();
};

job_card_manager.prototype.create_uuid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


job_card_manager.prototype.fetch_lead_data = function (data) {
    var self = this;
    $.ajax({
        type: 'GET',
        url: base_url + 'admin/tender-lead/fetch_lead_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading lead data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                self.lead_data = stat.lead_data;
        
                self.lead_id = self.lead_data.id;
                
                $('#lead_source').val("Kuga Sales");
                $('#site_address').val(self.lead_data.address);
                $('#project_name').val(self.lead_data.project_name);
                $('#sale_rep_name').val(self.lead_data.full_name);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

job_card_manager.prototype.upload_image = function () {
    var self = this;
    $(document).on('change','.image_upload',function () {
        $this = $(this);
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'tender_job_card_form_files');
        toastr.remove();
        toastr["info"]("Uploading image please wait...");
        $.ajax({
            url: base_url + 'admin/uploader/upload_file', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    var id = $this.attr('data-id');
                    toastr.remove();
                    toastr["success"](res.status);
                    $('#' + id).val(res.file_name);
                    $('.' + id).val(res.file_name);
                    self.show_image($this, res.file_name);
                } else {
                    toastr.remove();
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.remove();
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};

job_card_manager.prototype.show_image = function (ele, file_name) {
    var self = this;
    var image = file_name;
    
    if (image != '') {
        $(ele).parent().attr('style', 'display:none !important;');
        $(ele).parent().next('div').attr('style', 'display:block !important;');
        $(ele).parent().next('div').children('.add-picture').css('background-image', 'url("' + base_url + 'assets/uploads/tender_job_card_form_files/' + image + '")');
        $(ele).parent().next('div').children('.add-picture').css('background-repeat', 'no-repeat');
        $(ele).parent().next('div').children('.add-picture').css('background-position', '50% 50%');
        $(ele).parent().next('div').children('.add-picture').css('background-size', '100% 100%');
    }
};

job_card_manager.prototype.hide_image = function () {
    var self = this;
    $('.image_close').click(function () {
        $(this).parent().prev('div').attr('style', 'display:block !important;');
        $(this).parent().prev('div').children('input').val('');
        $(this).parent().attr('style', 'display:none !important;');
        $(this).parent().children('.add-picture').css('background-image', '');
        $(this).parent().children('.add-picture').css('background-repeat', '');
        $(this).parent().children('.add-picture').css('background-position', '');
        $(this).parent().children('.add-picture').css('background-size', '');
    });
};


job_card_manager.prototype.validate_job_card_form = function () {
    var self = this;
    var flag = true;
    $('.invalid-feedback').remove();
    var elements = document.getElementById("job_card_form").elements;
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].hasAttribute('required')) {
            if (elements[i].value == '') {
                if (elements[i].getAttribute('type') == 'hidden') {
                    $(elements[i]).parent().addClass('is-invalid');
                    $(elements[i]).prev('.item_name').addClass('is-invalid');
                } else {
                    $(elements[i]).addClass('is-invalid');
                    $(elements[i]).parent().append('<div class="invalid-feedback">Please provide some value. </div>');
                }
                flag = false;
            } else {
                $(elements[i]).removeClass('is-invalid');
                $(elements[i]).parent().removeClass('is-invalid');
                $(elements[i]).parent().parent().removeClass('is-invalid');
                if (elements[i].getAttribute('type') == 'email') {
                    if (!self.validateEmail(elements[i].value)) {
                        $(elements[i]).addClass('is-invalid');
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid email. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'number') {
                    if (isNaN(elements[i].value)) {
                        $(elements[i]).parent().append('<div class="invalid-feedback">Please provide a valid number. </div>');
                        flag = false;
                    }
                } else if (elements[i].getAttribute('type') == 'radio') {
                    if (elements[i].checked == false) {
                        $(elements[i]).parent().parent().addClass('is-invalid');
                        flag = false;
                    }
                }
            }
            $('.invalid-feedback').css('display', 'block');
        }
    }
    
    if ($('#panel_layout').val() == '') {
        toastr.error('Please upload panel layout photo')
        flag = false;
    }
    
    if ($('input[name="job_card[is_tender]"]:checked').length == 0) {
        $('input[name="job_card[is_tender]"]').parent().addClass('is-invalid');
        flag = false;
    }
    
    if ($('input[name="job_card[is_site_incpection]"]:checked').length == 0) {
        $('input[name="job_card[is_site_incpection]"]').parent().addClass('is-invalid');
        flag = false;
    }
    
    if ($('input[name="job_card[is_tilt]"]:checked').length == 0) {
        $('input[name="job_card[is_tilt]"]').parent().addClass('is-invalid');
        flag = false;
    }
    
    return flag;
};


job_card_manager.prototype.save_job_card_form = function(download){
    var self = this;
    
    if(download){
        var flag = self.validate_job_card_form();
        if (!flag) {
            toastr.remove();
            toastr["error"]('Please fill the fields marked in red');
            $('html, body').animate({
                'scrollTop' : $(".is-invalid").position().top
            },2000);
            return false;
        }
    }
    
    if (self.job_card_uuid == '') {
        var uuid = self.create_uuid();
        self.job_card_uuid = uuid;
    }
    var formData = $('#job_card_form').serialize();
    formData += '&uuid=' + self.job_card_uuid;
    formData += '&lead_id=' + self.lead_id;
    
    $.ajax({
        url: base_url + 'admin/tender-lead/save_job_card_form',
        type: 'post',
        data: formData,
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            toastr["info"]('Saving Job Card Data, Please Wait....');
            $("#save_job_card").attr("disabled", "disabled");
        },
        success: function (response) {
            toastr.remove();
            if (response.success == true) {
                self.lead_id = response.id;
                //Add Quote ref in url
                if (self.job_card_uuid == '') {
                    window.history.pushState(self.lead_data, '', base_url + 'admin/tender-lead/edit_job_card_form/' + self.job_card_uuid);
                }
                $("#save_job_card").removeAttr("disabled");
                if (download && self.job_card_uuid != '') {
                    self.generate_job_card_pdf();
                } else {
                    toastr["success"](response.status);
                    setTimeout(function(){
                        window.location.href = base_url + 'admin/tender-lead/edit/'+self.lead_uuid;
                    },1500);
                }
            } else {
                $("#save_job_card").removeAttr("disabled");
                if (download && self.job_card_uuid != '') {
                    self.generate_job_card_pdf();
                } else {
                    toastr["error"](response.status);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#save_job_card").removeAttr("disabled");
            $('#customer_loader').html('');
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


job_card_manager.prototype.generate_job_card_pdf = function(){
    var self = this;
    var url = base_url + 'admin/tender-lead/generate_card_pdf/' + self.job_card_uuid;
    
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            toastr.remove();
            $("#save_job_card").attr("disabled", "disabled");
            $("#generate_job_card_pdf").attr("disabled", "disabled");
            $.isLoading({
                text: "Saving, Please Wait this might take some time."
            });
        },
        success: function (response) {
            toastr.remove();
            $.isLoading("hide");
            if (response.success == true) {
                toastr["success"](response.status);
                var win = window.open(response.filePath,'_blank');
                if (win) {
                    win.focus();
                }else{
                    
                }
            } else {
                toastr["error"](response.status);
            }
            $("#save_job_card").removeAttr("disabled");
            $("#generate_job_card_pdf").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.isLoading("hide");
            $("#save_job_card").removeAttr("disabled");
            $("#generate_job_card_pdf").removeAttr("disabled");
            toastr.remove();
            //Getting this weird issue but still boking form was completed save on simpro 
            if(xhr.statusText == "OK"){
                toastr["success"]('Congratulations on your sale. Please contact the office to make sure your sale has been received and processed.');
                setTimeout(function(){
                    window.location.href = base_url + 'admin/tender-lead/edit/'+self.lead_uuid;
                },1500);
            }else{
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        }
    });
};


job_card_manager.prototype.fetch_job_form_data = function(data){
    var self = this;

    $.ajax({
        type: 'GET',
        url: base_url + 'admin/tender-lead/fetch_job_card_form_data',
        datatype: 'json',
        data: data,
        beforeSend: function () {
            $.isLoading({
                text: "Loading Job Card data, Please Wait...."
            });
        },
        success: function (stat) {
            $.isLoading("hide");
            stat = JSON.parse(stat);
            if (stat.success == true) {
                var job_card_data = stat.job_card_data;
                self.handle_job_card_data(job_card_data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.remove();
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};


job_card_manager.prototype.handle_job_card_data = function(data){
    var self = this;
    
    self.lead_id = data.lead_id;
    
    var job_form_data = (typeof data.job_form_data == 'string') ? JSON.parse(data.job_form_data) : data.job_form_data;
    
    var images = ["panel_layout", 
                    "roof_access_one", 
                    "roof_access_two", 
                    "roof_condition_one",
                    "roof_condition_two",
                    "inverter_location_one",
                    "inverter_location_two",
                    "cable_route_one",
                    "cable_route_two",
                    "connection_point_photo",
                    "main_switch_board",
                    "additional_eci_one",
                    "additional_eci_two",
                    "roof_underneath"];
    var options = ["is_tender", "is_site_incpection", "is_tilt", "is_forklift","is_cage","switch_board_condition","switch_board_upgrade"];
    for(var key in job_form_data){

        if(options.includes(key)){
            $('#'+key+'_'+job_form_data[key]).prop('checked',true).trigger('change');
        }else if(images.includes(key)){
            $('#' + key).val(job_form_data[key]);
            var $this = $('*[data-id="'+key+'"]');
            self.show_image($this, job_form_data[key]);
        }else if(key == 'mcb'){
            
            var mcbData = (typeof job_form_data[key] == 'string') ? JSON.parse(job_form_data[key]) : job_form_data[key];
            for(var i = 0; i < mcbData['first_heading'].length; i++){
                if(mcbData['first_heading'][i] != ''){
                    if(i > 0){
                        self.add_new_mcb_page();
                    }
                    for(var k in mcbData){
                        if(images.includes(k)){
                            $('.'+k+'_'+i).val(mcbData[k][i]);
                            var $this = $('*[data-id="'+k+'_'+i+'"]');
                            self.show_image($this, mcbData[k][i]);
                        }else{
                            $('.'+k+'_'+i).val(mcbData[k][i]);
                        }
                    }
                }
            }
        }else if(key == 'roofing'){
            var roofingData = (typeof job_form_data[key] == 'string') ? JSON.parse(job_form_data[key]) : job_form_data[key];
            for(var i = 0; i < roofingData['roof_underneath'].length; i++){
                if(roofingData['roof_underneath'][i] != ''){
                    if(i > 0){
                        self.add_new_roofing_page();
                    }
                    for(var k in roofingData){
                        if(images.includes(k)){
                            $('.'+k+'_'+i).val(roofingData[k][i]);
                            var $this = $('*[data-id="'+k+'_'+i+'"]');
                            self.show_image($this, roofingData[k][i]);
                        }else{
                            $('.'+k+'_'+i).val(roofingData[k][i]);
                        }
                    }
                }
            }
            
        }else{
            $('[name="job_card['+key+']"]').val(job_form_data[key]);
        }
        
        // $('#add_more_mcb_page_btn');
    }
      
};


job_card_manager.prototype.add_new_mcb_page = function(){
    var self = this;
    
    var count = self.mcb_page_count;
    
    var clonedTable = $('#page_6_dynamic').clone();
    $(clonedTable).removeClass('hidden');
    $(clonedTable).attr('id','page_6_'+count);
    $(clonedTable).find('.remove_mcb_page_btn').attr('data-id',count);
    
    $(clonedTable).find('input[name="job_card[mcb][first_heading][]"]').val("Possible Connection Point Photos (MSB)");
    $(clonedTable).find('input[name="job_card[mcb][first_heading][]"]').attr("class",'first_heading_'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][second_heading][]"]').val("Main Switchboard Photo");
    $(clonedTable).find('input[name="job_card[mcb][second_heading][]"]').attr("class",'second_heading_'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][third_heading][]"]').val("Additional Photo of Electrical Infrastructure");
    $(clonedTable).find('input[name="job_card[mcb][third_heading][]"]').attr("class",'third_heading_'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][fourth_heading][]"]').val("Additional Photo of Electrical Infrastructure");
    $(clonedTable).find('input[name="job_card[mcb][fourth_heading][]"]').attr("class",'fourth_heading_'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][connection_point_photo][]"]').attr("class",'connection_point_photo_'+count);
    $(clonedTable).find('input[name="job_card[mcb][connection_point_photo][]"]').prev('input').attr("data-id",'connection_point_photo_'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][main_switch_board][]"]').attr("class",'main_switch_board'+count);
    $(clonedTable).find('input[name="job_card[mcb][main_switch_board][]"]').prev('input').attr("data-id",'main_switch_board'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][additional_eci_one][]"]').attr("class",'additional_eci_one'+count);
    $(clonedTable).find('input[name="job_card[mcb][additional_eci_one][]"]').prev('input').attr("data-id",'additional_eci_one'+count);
    
    $(clonedTable).find('input[name="job_card[mcb][additional_eci_two][]"]').attr("class",'additional_eci_two'+count);
    $(clonedTable).find('input[name="job_card[mcb][additional_eci_two][]"]').prev('input').attr("data-id",'additional_eci_two'+count);
    if($('#page_6_'+(count -1)).length > 0){
        $('#page_6_'+(count -1)).after(clonedTable).after('<br/>');
    }else{
        $('#page_6').after(clonedTable).after('<br/>');
    }
    self.mcb_page_count++;
};

job_card_manager.prototype.add_new_roofing_page = function(){
    var self = this;
    
    var count = self.roofing_page_count;
    
    var clonedTable = $('#page_7_dynamic').clone();
    $(clonedTable).removeClass('hidden');
    $(clonedTable).attr('id','page_7_'+count);
    $(clonedTable).find('.remove_roofing_page_btn').attr('data-id',count);
    
    $(clonedTable).find('.black-bg').text("Roof "+(count+1)+" from underneath");
    
    $(clonedTable).find('input[name="job_card[roofing][number_of_purlins][]"]').attr("class",'number_of_purlins_'+count);
    $(clonedTable).find('input[name="job_card[roofing][purlin_spacing][]"]').attr("class",'purlin_spacing_'+count);
    
    $(clonedTable).find('input[name="job_card[roofing][roof_underneath][]"]').attr("class",'roof_underneath_'+count);
    $(clonedTable).find('input[name="job_card[roofing][roof_underneath][]"]').prev('input').attr("data-id",'roof_underneath_'+count);
    
    
    if($('#page_7_'+(count -1)).length > 0){
        $('#page_7_'+(count -1)).after(clonedTable).after('<br/>');
    }else{
        $('#page_7').after(clonedTable).after('<br/>');
    }
    self.roofing_page_count++;
    
};




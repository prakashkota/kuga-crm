<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Job Card</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style>
            @media print {
                .no-print {display: none;}
                body {background: transparent;}
                .black-bg{background-color:#010101}
                .red{color:#c32027}
            }
            
            input[type=checkbox] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=checkbox]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=checkbox]:checked:before {
                color: black !important;
            }
        
            input[type=radio] {
                -moz-appearance:none;
                -webkit-appearance:none;
                -o-appearance:none;
                outline: none;
                content: none; 
                margin-left: 5px;
            }
        
            input[type=radio]:before {
                font-family: "FontAwesome";
                content: "\f00c";
                font-size: 25px;
                color: transparent !important;
                background: #fff;
                width: 25px;
                height: 25px;
                border: 2px solid black;
                margin-right: 5px;
            }
        
            input[type=radio]:checked:before {
                color: black !important;
            }
        
        </style>
    </head>
<body style="padding:0; margin:0">
<table class="pdf_page" width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/job_card_form/card_header.jpg'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="business_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>Job Details</strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Project Name*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->project_name) ? $job_card_details->project_name : ''; ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Site Address*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <div style="width:100%; border:solid 1px #ccc; padding:13px; outline:none; box-sizing:border-box; margin:5px 0 0 0;"><?php echo isset($job_card_details->site_address) ? $job_card_details->site_address : ''; ?></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Sales Rep*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->sale_rep_name) ? $job_card_details->sale_rep_name : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Lead Source*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->lead_source) ? $job_card_details->lead_source : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is this for a Tender / Quote*?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if(isset($job_card_details->is_tender)){ ?>
                                                    <input type="radio" id="is_tender_yes" <?php if($job_card_details->is_tender == 'yes') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tender_no" <?php if($job_card_details->is_tender == 'no') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php }else{ ?>
                                                    <input type="radio" id="is_tender_yes"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tender_no"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100%" valign="top" style="padding-right:10px">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Is Site Inspection done*?</td>
                                            <td valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <?php if(isset($job_card_details->is_site_incpection)){ ?>
                                                    <input type="radio" id="is_tender_yes" <?php if($job_card_details->is_site_incpection == 'yes') { echo 'checked=""'; }?> />
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tender_no" <?php if($job_card_details->is_site_incpection == 'no') { echo 'checked=""'; }?>/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php }else{ ?>
                                                    <input type="radio" id="is_tender_yes" />
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tender_no"/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30"></td>
                </tr>
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                        <strong>System Details</strong>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">System Size*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->system_size) ? $job_card_details->system_size : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Panel Brand*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->panel_brand) ? $job_card_details->panel_brand : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Inverter Brand*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input required="" value="<?= isset($job_card_details->inverter_brand) ? $job_card_details->inverter_brand : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="415" valign="top" style="padding-left:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Battery Brand:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input value="<?= isset($job_card_details->battery_brand) ? $job_card_details->battery_brand : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="360" valign="top" style="padding-right:10px">
                                    <table width="350" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="105" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Tilt*:</td>
                                            <td width="200" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                
                                                <?php if(isset($job_card_details->is_tilt)){ ?>
                                                    <input type="radio" id="is_tilt_yes" <?php if($job_card_details->is_tilt == 'yes') { echo 'checked=""'; }?>/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tilt_no" <?php if($job_card_details->is_tilt == 'no') { echo 'checked=""'; }?>/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php }else{ ?>
                                                    <input type="radio" id="is_tilt_yes"/>
                                                    <span style="margin-right: 20px;font-size: 16px;">Yes</span>
                                                    <input type="radio" id="is_tilt_no"/> 
                                                    <span style="margin-right: 20px;font-size: 16px;">No</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <?php if(isset($job_card_details->is_tilt) && $job_card_details->is_tilt == 'yes') { ?>
                                <td width="455" valign="top" style="padding-left:10px">
                                    <table width="445" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="150" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">No. Of panels on TILT:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input value="<?= $job_card_details->no_if_tilt_panels ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="number" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <?php } ?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="122" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Type*:</td>
                                            <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                <input value="<?= isset($job_card_details->roof_type) ? $job_card_details->roof_type : '' ?>" style="padding:5px; width:inherit; height: inherit;border:solid 1px #ccc;outline:none; box-sizing:border-box;" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php if(isset($job_card_details->additional_notes2)){ ?>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="830" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="100%" valign="top" style="padding-right:10px">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Additional Notes</td>
                                            </tr>
                                            <tr>
                                                <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                    <textarea style="padding:5px; width:100%;height: 200px; border:solid 1px #ccc;outline:none; box-sizing:border-box;"><?= isset($job_card_details->additional_notes2) ? $job_card_details->additional_notes2 : '' ?></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <?php if(isset($job_card_details->additional_notes2)){ ?>
    <tr>
        <td height="140" valign="top">&nbsp;</td>
    </tr>
    <?php }else{ ?>
        <tr>
            <td height="385" valign="top">&nbsp;</td>
        </tr>
    <?php } ?>
    <tr>
        <td height="30" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;font-style:italic;text-align:center;">This document is the property of Kuga Australia and it cannot be shared to anyone without prior written consent</td>
    </tr>
    <tr>
        <td valign="bottom"><img src="<?php echo $this->config->item('live_url') . 'assets/led_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>
</body>
</html>

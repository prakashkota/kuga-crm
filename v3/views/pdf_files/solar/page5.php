<?php 
$o = $t = array();
$tilt = $orientation = '';
if($proposal_data['nearmap_toggle']==1){
    if(isset($proposal_data['near_map_data'])){
      $panel_data = json_decode($proposal_data['near_map_data'],true);
      if (!empty($panel_data['tiles'])) {
        $panel_data['tiles'] = (array) $panel_data['tiles'];
        $panel_data['tiles'][0] = (array) $panel_data['tiles'][0];
        foreach($panel_data['tiles'] as $tile){
            if(!in_array($tile['sp_rotation'],$o)){
                $o[] = $tile['sp_rotation'];
            }
            if(!in_array($tile['sp_tilt'],$t)){
                $t[] = $tile['sp_tilt'];
            }
        }
      }
    }
}else{
    for ($i = 0; $i < count($solar_client_panel_data); $i++) {  
        $o[] = $solar_client_panel_data[$i]['panel_orientation'];
        $t[] =  $solar_client_panel_data[$i]['panel_tilt'];
    }
}

$o = empty($o)?array(0):$o;
$t = empty($t)?array(0):$t;
$orientation = implode('&deg;/ ',$o);
$tilt = implode('&deg;/ ',$t);


$height = 60 + 252;
if(empty($panel)){
  $height += 40;
}
if(empty($inverter)){
  $height += 40;
}
?>
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
  <tr>
      <td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img1.jpg" alt="" style="display:inline-block"/>4</td>
  </tr>
  <tr>
   <td style="padding:0 50px 50px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/heading.jpg" alt=""/></td>
  </tr>
  <tr>
   <td style="padding:0 50px 30px;">
    <span style="background: #c52428;color: #fff;font-weight: 600;padding: 10px 30px;display: inline-block;">PROPOSED PLACEMENTS</span>
    <img width="810" height="286" src="<?php echo $image_path . $proposal_data['image']; ?>" alt=""/>
   </td>
  </tr>
  <tr>
    <td style="padding:0 50px 20px;"><span style="background: #c52428;color: #fff;font-weight: 600;padding: 10px 30px;display:block;"><?php echo str_ireplace(", Australia", "", $lead_data['address']); ?></span></td>
  </tr>
  <tr>
    <td>
      <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
        <tr>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Manufacturer power rating for array</td>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo ($proposal_data['total_system_size']); ?> kw</td>
        </tr>
        <tr>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Number of Panels</td>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo (isset($panel['prd_qty'])) ? $panel['prd_qty'] : 0; ?></td>
        </tr>
        <tr>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Orientation</td>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $orientation; ?>&deg;</td>
        </tr>
        <tr>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Tilt</td>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $tilt; ?>&deg;</td>
        </tr>
        <?php  if(!empty($panel)){ ?>
        <tr>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px; font-weight:600;">Panel Type/Model</td>
          <td style="border-bottom:solid 1px #c52428; padding:10px 30px;"><?php echo $panel['product_name']; ?></td>
        </tr>
        <?php } ?>
        <?php  if(!empty($inverter)){ ?>
        <tr>
          <td style="padding:10px 30px; font-weight:600;">Inverter Type/Model</td>
          <td style="padding:10px 30px;"><?php echo $inverter['product_name']; ?></td>
        </tr>
        <?php } ?>
      </table>
    </td>
  </tr>

  <tr>
      <td style="height:<?php echo $height; ?>px;">&nbsp;</td>
  </tr>

  <tr>
    <td style="padding:20px 50px 0; text-align:left; font-size:13px; color:#666;">
      WWW.13KUGA.COM.AU   |   <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>
    </td>
  </tr>


</table>
<style>
    .capitalize {
        text-transform:capitalize;
    }   
    .page-wrapper{background-color:#ECEFF1; min-height:100%;}
    .nav.nav-tabs.nav-topline .nav-item a.nav-link.active {
        background: none;
        box-shadow: inset 0 3px 0 #ffd914;
        color: #000;
        border-radius: 0;
        border-top-color: #ffd914;
        border-bottom: none;
    }
    .nav.nav-tabs.nav-topline .nav-item a.nav-link:hover{
        color: #000;
    }
    tfoot{display: block !important;}


    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .dropdown-menu > li > label {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.428571429;
    }
    .deal_view__actions{    
        display: flex;
        box-sizing: border-box;
        align-items: flex-start
    }

    .dt-button-collection button.buttons-columnVisibility:before,
    .dt-button-collection button.buttons-columnVisibility.active span:before {
        display:block;
        position:absolute;
        top:1.2em;
        left:0;
        width:12px;
        height:12px;
        box-sizing:border-box;
    }

    .dt-button-collection button.buttons-columnVisibility:before {
        content:' ';
        margin-top:-6px;
        margin-left:10px;
        border:1px solid black;
        border-radius:3px;
    }

    .dt-button-collection button.buttons-columnVisibility.active span:before {
        content:'\2714';
        margin-top:-11px;
        margin-left:12px;
        text-align:center;
        text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
    }

    .dt-button-collection button.buttons-columnVisibility span {
        margin-left:20px;    
    }

</style>
<div class="page-wrapper d-block clearfix">
    <form id="tender_list_filters">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div class="kg-page__heading">
                <div class="kg-page__heading_title">
                    Manage Tender (List View)
                </div>   

            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="columns columns-right btn-group pull-right">
                    <div class="ml-2">
                        <a class="btn btn-default" style="margin-left:5px;" href="<?php echo site_url('admin/tender/manage-tender?view=pipeline'); ?>">
                            <i class="fa fa-th"></i>
                        </a>
                    </div>

                    <div class="ml-2">
                        <button class="btn btn-default" type="button" id="tender_list_range" >
                            <i class="fa fa-calendar"></i> &nbsp; <span></span> <i class="fa fa-caret-down" ></i>
                        </button>
                        <input type="hidden" name="filter[start_date]" id="start_date" value="" />
                        <input type="hidden" name="filter[end_date]" id="end_date" value="" />
                    </div>


                    
                    <div class="ml-2 hidden">
                        <a href="javascript:void(0);" class="btn btn-default" id="export_tenders">Export</a>
                    </div>

                </div>
            </div>

        </div>
    </form>
    <!-- BEGIN: message  -->	
    <?php
    $message = $this->session->flashdata('message');
    echo (isset($message) && $message != NULL) ? $message : '';
    ?>
    <!-- END: message  -->	
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12"> 
                    <div class="card">
                        <h2 class="card-header">Tenders List</h2>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tender_list" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Create Date</th>
                                                <th>Name</th>
                                                <th>Contact Persion</th>
                                                <th>Client</th>
                                                <th>Email</th>
                                                <th>Contract Number</th>
                                                <th>Status</th>

                                            </tr>
                                        </thead>
                                        <tbody id="tender_list_body" >

                                        </tbody>
                                    </table>
                                </div>
                                <div id="table_loader" ></div>
                                <div  id="pagination" class="mt-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/pagination/simplePagination.css'); ?>" />
    <script type="text/javascript" src="<?php echo site_url('assets/pagination/jquery.simplePagination.js'); ?>"></script>

    <script>

        var deal_manager = function () {
            var self = this;
            this.filters = '';
            this.loading = false;
            this.page = 1;
            this.per_page = 100;
            this.pagination = false;
            this.keyword = '';

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#tender_list_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                document.getElementById('start_date').value = start.format('YYYY-MM-DD');
                document.getElementById('end_date').value = end.format('YYYY-MM-DD');
                self.pagination = false;
                $('#pagination').pagination('destroy');
                var data = $('#tender_list_filters').serialize();
                self.fetch_tender_status_data(data);
            }

            var currentDate = moment();
            var weekStart = currentDate.clone().startOf('isoWeek');
            var weekEnd = currentDate.clone().endOf('isoWeek');
            
            var thisWeek = [];
            var lastWeek = [];
            thisWeek.push(moment(weekStart).add(0, 'days'));
            thisWeek.push(moment(weekStart).add(6, 'days'));
            lastWeek.push(moment(weekStart).add(-7, 'days'));
            lastWeek.push(moment(weekStart).add(-1, 'days'));

            $('#tender_list_range').daterangepicker({
                numberOfMonths: 6,
                startDate: start,
                endDate: end,
                ranges: {
                    'All': [moment('2016-01-01'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': thisWeek,
                    'Last Week': lastWeek,
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            self.fetch_tender_status_data();

            $('#tender_list_filters :input').change(function () {
                self.pagination = false;
                $('#pagination').pagination('destroy');
                var data = $('#tender_list_filters').serialize();
                self.fetch_tender_status_data(data);
            });

            $('#export_tenders').click(function () {
                var data = $('#tender_list_filters').serialize();
                var url = base_url + 'admin/tender/export_csv?action=export_tender_csv&' + data;
                window.open(url, '_blank');
            });
            
            $('.reset_filter').click(function () {
                var filter_type = $(this).attr('data-id');
                $("." + filter_type).prop("checked", false);
                $("." + filter_type).trigger("change");
            });

        }


        deal_manager.prototype.create_tender_table_element = function (ele, value) {
            var table_ele = document.createElement(ele);
            if (value) {
                table_ele.innerHTML = value;
            }
            return table_ele;
        };

        deal_manager.prototype.create_deal_stage_table = function (data) {
            var self = this;
            self.loading = false;
            if (data.tender_data.length == 0) {
                self.loading = true;
            }
            var table_body = document.getElementById('tender_list_body');
            if (data.tender_data.length > 0) {
                for (var i = 0; i < data.tender_data.length; i++) {
                    var tr = self.create_tender_table_element('tr');
                   
                    var td_created_at = self.create_tender_table_element('td',  moment(data.tender_data[i].created_at). format('MM/DD/YYYY'));

                    var _name = '<a target="__blank" href="' + base_url + 'admin/tender/add/' + data.tender_data[i].id + '">' + data.tender_data[i].name + '</a>' + '<span class="hidden">' + data.tender_data[i].name + '</span>';
                    var td_name = self.create_tender_table_element('td', _name);
                    //td_name.className = color;

                    var td_contactPerson = self.create_tender_table_element('td', data.tender_data[i].contactPerson);

                    var td_client = self.create_tender_table_element('td', data.tender_data[i].client);
                    var td_email = self.create_tender_table_element('td', data.tender_data[i].email);
                    var td_contractNumber = self.create_tender_table_element('td', data.tender_data[i].contractNumber);
                    var tenderStatus = '';
                    $.each(data.tender_status,function(m,n){
                        if(n.id == data.tender_data[i].status ){
                            tenderStatus = n.name;
                        }
                    });

                    var td_status = self.create_tender_table_element('td', tenderStatus);
                   
                    tr.appendChild(td_created_at);
                    tr.appendChild(td_name);
                    tr.appendChild(td_contactPerson);
                    tr.appendChild(td_client);
                    tr.appendChild(td_email);
                    tr.appendChild(td_contractNumber);
                    tr.appendChild(td_status);
                    table_body.appendChild(tr);
                }
            }
        };


        deal_manager.prototype.fetch_tender_status_data = function (data) {
            var self = this;
            var stage_id = self.current_stage_id;
            $('#table_loader').html(createLoader('Please wait while data is being fetched.......'));
            $.ajax({
                url: base_url + 'admin/tender/fetch_status_data',
                type: 'get',
                data: data + '&view=list&limit=' + self.per_page,
                dataType: 'json',
                success: function (response) {
                    if (response.success == true) {
                        if ($('#tender_list')) {
                            $('#tender_list').DataTable().clear();
                            $('#tender_list').DataTable().destroy();
                        }
                        $('#table_loader').html('');
                        self.create_deal_stage_table(response);

                        var table = $('#tender_list').DataTable({
                            orderCellsTop: true,
                            fixedHeader: true,
                            columnDefs: [
                                {type: 'date-uk', targets: 0}
                            ],
                            "order1": [
                                [0, "desc"]
                            ],
                            "bInfo": false,
                            "bLengthChange": false,
                            "bFilter": true,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'colvis',
                                    text: 'Columns',
                                    postfixButtons: ['colvisRestore'],
                                }
                            ]
                        });

                        if (self.pagination == false) {
                            $(function () {
                                $('#pagination').pagination({
                                    items: response.meta.total,
                                    itemsOnPage: self.per_page,
                                    cssStyle: "light-theme",
                                    onPageClick: function (pageNumber) {
                                        var data = $('#tender_list_filters').serialize();
                                        self.page = pageNumber;
                                        data += '&page=' + self.page;
                                        self.fetch_tender_status_data(data);
                                    }
                                });
                            });
                            self.pagination = true;
                        }


                        //Overriding Search
                        var search_input = '<label>Search:<input type="search" id="search_tenders" class="" placeholder="" aria-controls="tender_list"></label>';

                        document.getElementById("tender_list_filter").innerHTML = search_input;
                        var table = $('#tender_list').dataTable().api();

                        if (self.keyword != '') {
                            document.getElementById("search_tenders").value = self.keyword;
                        }

                        $('#search_tenders').change(function () {
                            var search_val = $(this).val();
                            if (search_val == '') {
                                self.pagination = false;
                            } else {
                                $('#pagination').pagination('destroy');
                            }
                            self.keyword = search_val;
                            var data = 'keyword=' + encodeURIComponent(search_val);
                            data += '&page=1';
                            self.fetch_tender_status_data(data);
                        });


                    } else {
                        if (response.hasOwnProperty('authenticated')) {
                            toastr['error'](response.status);
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        } else {
                            toastr["error"]('Something went wrong please try again.');
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        deal_manager.prototype.export_tender_data = function (data) {
            var self = this;
            $.ajax({
                url: base_url + 'admin/tender/export_csv',
                type: 'get',
                data: data,
                dataType: 'json',
                success: function (response) {


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        //In Prgoress
        /**
         var table = $('#tender_list').DataTable({
         orderCellsTop: true,
         fixedHeader: true,
         'processing': true,
         'serverSide': true,
         'serverMethod': 'post',
         ajax: base_url + 'admin/tender/fetch_tender_status_data',
         columns: [
         {data: "deal_created"},
         {data: "owner_name"},
         {data: "company_name"},
         {data: "tender_stage"},
         {data: "tender_status"}
         ],
         "bInfo": false,
         "bLengthChange": false,
         "bFilter": true,
         paging: false,
         dom: 'Bfrtip',
         buttons: [
         {
         extend: 'colvis',
         text: 'Columns',
         postfixButtons: ['colvisRestore'],
         }
         ]
         });
         */

        var context = {};
        var deal_manager_tool = new deal_manager(context);
    </script>
</body>

</html>

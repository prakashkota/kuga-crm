<!-- MAIN SECTION -->
  <div class="container featured-area-default padding-30">
    <div class="row">
      <!-- ARTICLE OVERVIEW SECTION -->
      <div class="col-md-8 padding-20">
        <div class="row">
          <?php if(isset($article_category)){ ?>  
            <!-- BREADCRUMBS -->
            <div class="breadcrumb-container">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/knowledgebase'); ?>"><i class="fa fa-home"></i></a></li>
                <li class="breadcrumb-item active"><?php echo ucfirst($article_category['category_name']); ?></li>
              </ol>
            </div>
            <!-- END BREADCRUMBS -->
          <?php } else { ?>
            <div class="fb-heading">
              <i class="fa fa-search"></i> Search Results for: <strong><?php echo ucfirst($keyword); ?></strong>
              <h4 class="padding-left-35"><small><?php echo count($articles); ?> results were found using the search term provided</small></h4>
            </div>
          <?php } ?>
          <!-- ARTICLES -->
          <hr class="style-three">
          <?php if(!empty($articles)) { 
              foreach($articles as $key => $value){ ?>
          <div class="card card-default">
            <div class="article-heading-abb">
              <a href="<?php echo site_url('admin/knowledgebase/'.$value['category_slug'].'/'.$value['article_slug']); ?>"><i class="fa fa-pencil-square-o"></i> <?php echo $value['article_title']; ?></a>
            </div>
            <div class="article-info">
              <div class="art-date"><i class="fa fa-calendar-o"></i> <?php echo date('m/d/Y',strtotime($value['created_at'])); ?></div>
            </div>
            <div class="article-content">
              <p class="block-with-text">
                  <?php echo substr($value['article_description'], 0, 200); ?> 
              </p>
            </div>
            <div class="article-read-more">
              <a href="<?php echo site_url('admin/knowledgebase/'.$value['category_slug'].'/'.$value['article_slug']); ?>" class="btn btn-outline-dark btn-sm btn-wide">Read more...</a>
            </div>
          </div>
          <?php } } else {
             echo "No Articles Found"; 
          }?>
          <!-- END ARTICLES -->

          <!-- PAGINATION 
          <nav class="text-center">
            <ul class="pagination pagination-sm">
              <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-arrow-circle-left"></i> Previous</span></a>
              </li>
              <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
              <li class="page-item enabled"><a class="page-link" href="#">2 <span class="sr-only">(current)</span></a></li>
              <li class="page-item enabled"><a class="page-link" href="#">3 <span class="sr-only">(current)</span></a></li>
              <li class="page-item enabled">
                <a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">Next <i class="fa fa-arrow-circle-right"></i></span></a>
              </li>
            </ul>
          </nav>
          <!-- END PAGINATION -->
        </div>
      </div>
      <!-- END ARTICLES OVERVIEW SECTION-->
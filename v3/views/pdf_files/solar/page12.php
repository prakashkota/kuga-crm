<?php 
   $monthlyPaymentPlanSolar = 0;

   if(isset($proposal_finance_data['monthly_payment_plan'])){ 
    //  $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan']  * $term * 12;
     $monthlyPaymentPlanSolar = $proposal_finance_data['monthly_payment_plan'];
   }else{

     $proposal_finance_data['monthly_payment_plan'] = 0;

     $proposal_finance_data['term'] = 0;

   }

   ?>
    <table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family">
        <tr>
            <td style="padding:50px 40px 50px; font-size:14px; line-height:14px; vertical-align:middle">11 <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/footer-top-img.jpg" alt="" style="display:inline-block" /></td>
        </tr>
        <tr>
            <td style="padding:0 50px 50px;"><img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-head.jpg" alt="" /></td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0 50px;">
                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:50px;">
                    <tbody>
                        <tr>
                            <td align="center" width="270" style="border-right: 1px solid #c52428;"><strong>Quote ID</strong></td>
                            <td align="center" width="270" style="border-right: 1px solid #c52428;"><strong>Date</strong></td>
                            <td align="center" width="270"><strong>Authorised Person</strong></td>
                        </tr>
                        <tr>
                            <td align="center" width="270" style="border-right: 1px solid #c52428;padding-top: 7px;">
                                <?php echo $proposal_data['id']; ?>
                            </td>
                            <td align="center" width="270" style="border-right: 1px solid #c52428;padding-top: 7px;">
                                <?php echo date_format(date_create($lead_data['created_at']), "l jS F Y"); ?>
                            </td>
                            <td align="center" width="270">
                                <?php echo  $lead_data['first_name'] . ' ' . $lead_data['last_name']; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if($proposal_finance_data['upfront_payment_options']==1) { ?>
                    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:40px;">
                        <tr>
                            <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                                <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 1</strong> Upfront Payment 0 - 39kW
                            </th>
                            <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">No Deposit</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$<?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 0 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">50% on booking in the job</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$<?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 50 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">50% on handover of CES</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$<?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 50 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Total Payable</strong> (excl.GST)</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format((float) ($proposal_data['price_before_stc_rebate']), 0, '', ','); ?></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td style="padding:10px 20px;">&nbsp;</td>
                            <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                        </tr>
                    </table>
                <?php }else if($proposal_finance_data['upfront_payment_options']==2){?>
                    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:40px;">
                        <tr>
                            <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                                <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 1</strong> Upfront Payment 39 - 100 kW
                            </th>
                            <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">Deposit Payment (20%)</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 20 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">70% on project commencement</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 70 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">10% on handover of CES</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 10 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Total Payable</strong> (excl.GST)</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format((float) ($proposal_data['price_before_stc_rebate']), 0, '', ','); ?></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td style="padding:10px 20px;">&nbsp;</td>
                            <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                        </tr>
                    </table>
                <?php }else {?>
                    <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:40px;">
                        <tr>
                            <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                                <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 1</strong> Upfront Payment 100kW +
                            </th>
                            <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">Deposit Payment (20%)</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 20 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">70% on project commencement</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 70 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">10% on handover of CES</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">$
                                                <?php echo number_format((float) (($proposal_data['price_before_stc_rebate'] * 10 / 100)), 0, '', ','); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                            <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td colspan="2">
                                <table width="98%" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Total Payable</strong> (excl.GST)</td>
                                            <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format((float) ($proposal_data['price_before_stc_rebate']), 0, '', ','); ?></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="background-color: #ebebeb;">
                            <td style="padding:10px 20px;">&nbsp;</td>
                            <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                        </tr>
                    </table>
                <?php }?>
            </td>
        </tr>
        <tr>
            <td> 
            <?php if($proposal_finance_data['upfront_payment_options']==1) { ?>
                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:25px;">
                    <tr>
                        <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                            <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 2</strong> FINANCE THROUGH ENERGY SAVINGS
                        </th>
                        <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">Terms (Years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">
                                            <?php echo $year; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Monthly Payment Plan excl.GST</strong> (
                                            <?php echo $year; ?> years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format($proposal_finance_data['monthly_payment_plan'], 2, '.', ','); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td style="padding:10px 20px;">&nbsp;</td>
                        <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                    </tr>
                </table>
            <?php }else if($proposal_finance_data['upfront_payment_options']==2){?>
                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:25px;">
                    <tr>
                        <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                            <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 2</strong> FINANCE THROUGH ENERGY SAVINGS
                        </th>
                        <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">Terms (Years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">
                                            <?php echo $year; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Monthly Payment Plan</strong> (
                                            <?php echo $year; ?> years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format((float) ($proposal_finance_data['monthly_payment_plan']), 0, '', ','); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td style="padding:10px 20px;">&nbsp;</td>
                        <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                    </tr>
                </table>
            <?php }else {?>
                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:25px;">
                    <tr>
                        <th style="text-align:left; padding:10px 20px; background:#c52428; color:#fff; font-weight:400; ">
                            <img src="<?php echo $this->config->item('live_url'). 'assets/pdf_images/solar_v1';?>/page-12-table.jpg" alt="" style="margin-right: 15px;" /><strong>OPTION 2</strong> FINANCE THROUGH ENERGY SAVINGS
                        </th>
                        <th style="text-align:center; padding:10px 20px; background:#c52428; color:#fff;">&nbsp;</th>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;">Terms (Years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;">
                                            <?php echo $year; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px;">&nbsp;</td>
                                        <td style="border-bottom:solid 1px #000000; padding:10px 20px; text-align:right;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td colspan="2">
                            <table width="98%" style="margin: 0 auto;">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px;"><strong>Monthly Payment Plan</strong> (
                                            <?php echo $year; ?> years)</td>
                                        <td style="border-bottom:solid 1px #d2d2d2; padding:10px 20px; text-align:right;"><strong>$<?php echo number_format((float) ($proposal_finance_data['monthly_payment_plan']), 0, '', ','); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #ebebeb;">
                        <td style="padding:10px 20px;">&nbsp;</td>
                        <td style="padding:10px 20px; text-align:right;">&nbsp;</td>
                    </tr>
                </table>
            <?php }?>
            </td>
        </tr>
        <tr>
            <td style="padding:0 70px; color:#c52428;"><strong>This quote is valid till 30 days</strong></td>
        </tr>
        <tr>
            <td height="80">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0 50px;">
                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0" class="font-family" style="font-size:15px; margin-bottom:30px;padding: 40px 0;border-top: 2px dotted rgba(197,36,40,0.5);border-bottom: 2px dotted rgba(197,36,40,0.5);">
                    <tbody>
                        <tr>
                            <td style="color:#c52428;border-right: 2px dotted rgba(197,36,40,0.5); padding-left: 20px;"><strong>Company Name:</strong></td>
                            <td style="color:#c52428;padding-left: 40px;"><strong>Company Address:</strong></td>
                        </tr>
                        <tr>
                            <td style="border-right: 2px dotted rgba(197,36,40,0.5); padding-top: 7px; padding-left: 20px;">
                                <?php echo $lead_data['customer_company_name']; ?>
                            </td>
                            <td style="padding-left: 40px; padding-top: 7px;">
                                <?php echo str_ireplace(", Australia", "", $lead_data['address']); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 50px 0; text-align:right; font-size:13px; color:#666;">
                WWW.13KUGA.COM.AU |
                <?php echo $footer_static_number ? $footer_static_number : $lead_data['company_contact_no']; ?>
            </td>
        </tr>
    </table>
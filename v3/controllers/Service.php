<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @property Service Controller
 * @version 1.0
 */
class Service extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("Customer_Model", "customer");
        $this->load->model("Service_Model", "service");
        $this->load->model("CompanyMarkup_Model", "company_markup");
        $this->load->model("User_Model", "user");
        if (!$this->aauth->is_loggedin()) {
            redirect('admin/login');
        }
        $this->redirect_url = ($this->agent->referrer() != '') ? $this->agent->referrer() : 'admin/dashboard';
    }

    public function manage() {
        
        if (!$this->aauth->is_member('Franchise') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        
        $data = array();
        //Restricitng and Fetching of Data based on user logged In group
        $group = $this->input->get('group');
        if (isset($group) && $group != '') {
            $data['meta_title'] = 'Manage Franchise Line Item';
            if (!$this->aauth->is_member($group) && !$this->aauth->is_member('Admin')) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
                redirect($this->redirect_url);
            }
            if ($this->aauth->is_member('Admin')) {
                $line_items = $this->service->get_service_list_by_roles($group);
            } else {
                $line_items = $this->service->get_service_list_by_roles($group, $this->aauth->get_user()->id);
            }
        } else {
            $data['meta_title'] = 'Manage Line Item';
            if ($this->aauth->is_member('Admin')) {
                $line_items = $this->service->get_service_list_by_roles(1);
            } else if (!$this->aauth->is_member('Admin')) {
                $group_id = $this->aauth->get_user_groups()[0]->group_id;
                $line_items = $this->service->get_service_list_by_roles($group_id, $this->aauth->get_user()->id);
            }
        }

        $data['line_items'] = $line_items;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/line_item/manage');
        $this->load->view('partials/footer');
    }

    public function add() {
        
        if (!$this->aauth->is_member('Franchise') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        
        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('line_item', 'Item Name', 'required');
            $this->form_validation->set_rules('per_which_product', 'Per Which Product', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                unset($insert_data['line_item_data']);
                $insert_data['user_id'] = $this->aauth->get_user()->id;
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                //Insert Line Item
                $this->common->insert_data('tbl_service_line_item', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $line_item_insert_data = array();
                    $line_item_data = $this->input->post('line_item_data');
                    if (!empty($line_item_data)) {
                        foreach ($line_item_data['state'] as $key => $value) {
                            $line_item_insert_data[$key]['line_item_id'] = $id;
                            $line_item_insert_data[$key]['state'] = $value;
                            $line_item_insert_data[$key]['qty_from'] = $line_item_data['qty_from'][$key];
                            $line_item_insert_data[$key]['qty_to'] = $line_item_data['qty_to'][$key];
                            $line_item_insert_data[$key]['price'] = $line_item_data['price'][$key];
                            $line_item_insert_data[$key]['created_at'] = date('Y-m-d H:i:s');
                        }
                        $this->common->insert_batch('tbl_service_line_item_data', $line_item_insert_data);
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Line Item created successfuly.</div>');
                    redirect('admin/service/line-item/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the line item. </div>');
                }
            } else {
                $data['line_item'] = $this->input->post('line_item');
                $data['per_which_product'] = $this->input->post('per_which_product');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/line_item/add');
        $this->load->view('partials/footer');
    }

    public function edit($id) {
        
        if (!$this->aauth->is_member('Franchise') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        //Restricitng and Fetching of Data based on user logged In group   
        $line_item_data = $this->common->fetch_where('tbl_service_line_item', '*', array('id' => $id));

        if (empty($line_item_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No Line Item found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        if ($this->aauth->get_user()->id != $line_item_data[0]['user_id'] && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> You dont have access to edit others data.</div>');
            redirect($this->redirect_url);
        }


        $data = array();
        $lid_data = array();

        if (!empty($line_item_data)) {
            $data['lid_data'] = $line_item_data[0];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('line_item', 'Item Name', 'required');
            $this->form_validation->set_rules('per_which_product', 'Per Which Product', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                unset($update_data['line_item_data']);
                $update_data['updated_at'] = date('Y-m-d H:i:s');

                //Update Line Item
                $this->common->update_data('tbl_service_line_item', ['id' => $id], $update_data);

                if ($id) {
                    $line_item_update_data = array();
                    $line_item_data = $this->input->post('line_item_data');
                    //Bulk Update Line Item Data
                    $lid_item_data = $this->common->fetch_where('tbl_service_line_item_data', '*', ['line_item_id' => $id]);
                    $count = count($lid_item_data);
                    if ($count > 0) {
                        for ($i = 0; $i < $count; $i++) {
                            $line_item_update_data[$i]['state'] = $line_item_data['state'][$i];
                            $line_item_update_data[$i]['qty_from'] = $line_item_data['qty_from'][$i];
                            $line_item_update_data[$i]['qty_to'] = $line_item_data['qty_to'][$i];
                            $line_item_update_data[$i]['price'] = $line_item_data['price'][$i];
                            $line_item_update_data_cond[$i]['id'] = $lid_item_data[$i]['id'];
                            $this->common->update_data('tbl_service_line_item_data', $line_item_update_data_cond[$i], $line_item_update_data[$i]);
                        }
                    }

                    //Bulk Insert Line Item Data
                    for ($j = $count, $k = 0; $j < count($line_item_data['state']); $j++, $k++) {
                        $line_item_insert_data[$k]['line_item_id'] = $id;
                        $line_item_insert_data[$k]['state'] = $line_item_data['state'][$j];
                        $line_item_insert_data[$k]['qty_from'] = $line_item_data['qty_from'][$j];
                        $line_item_insert_data[$k]['qty_to'] = $line_item_data['qty_to'][$j];
                        $line_item_insert_data[$k]['price'] = $line_item_data['price'][$j];
                        $stat = $this->common->insert_data('tbl_service_line_item_data', $line_item_insert_data[$k]);
                    }

                    //Check if delete Required on less item data thean already stored
                    if ($count > count($line_item_data['state'])) {
                        for ($l = count($line_item_data['state']), $m = 0; $l < $count; $l++, $m++) {
                            $line_item_cond[$m]['id'] = $lid_item_data[$l]['id'];
                            $this->common->delete_data('tbl_service_line_item_data', $line_item_cond[$m]);
                        }
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Line Item updated successfuly.</div>');
                    redirect('admin/service/line-item/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the line item. </div>');
                }
            } else {
                $data['line_item'] = $this->input->post('line_item');
                $data['per_which_product'] = $this->input->post('per_which_product');
            }
        }

        $states = $this->common->fetch_where('tbl_state', '*', NULL);
        $data['states'] = $states;
        $data['lid'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/line_item/add');
        $this->load->view('partials/footer');
    }
    
    public function delete($id = FALSE) {
        if (!$this->aauth->is_member('Franchise') && !$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        if ($id) {
            $line_item = $this->common->fetch_where('tbl_service_line_item', '*', array('id' => $id));
            if (empty($line_item)) {
                redirect('admin/dashboard');
            } else {
                $stat = $this->common->update_data('tbl_service_line_item',array('id' => $id),array('status' => 0));
                $stat1 = $this->common->update_data('tbl_service_line_item_data',array('line_item_id' => $id),array('status' => 0));
                if ($stat && $stat1) {
                    if($stat == -1){
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Service Line item cannot be deleted it is linked to some proposal.</div>');
                    }else{
                        $this->session->set_flashdata('message', '<div class="alert alert-success">Service Line Item  and Its Variants Deleted Successfuly.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong. </div>');
                }
            }
            redirect('admin/service/line-item/manage');
        }
    }

    public function fetch_line_item_data() {
        $data = array();
        $data['success'] = FALSE;
        $lid = $this->input->get('line_item_id');
        if (isset($lid) && $lid != '') {
            $line_item_data = $this->common->fetch_where('tbl_service_line_item_data', '*', array('line_item_id' => $lid));
            $data['line_item_data'] = $line_item_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }

    public function fetch_line_item_and_markup() {
        $data = array();
        $data['line_item_data'] = array();
        $data['company_markup'] = array();
        $prd_panel_data = $this->input->post('prd_panel_data');
        if (!empty($prd_panel_data)) {
            
            //It Will be based on user looged in owner
            //Check if Looged in user is Admin , Fracnhise or Franchise Agent
            $user_id = $this->aauth->get_user()->id;
            if ($this->aauth->is_member('Admin')) {
                $owner_id = 1;
            }else if($this->aauth->is_member('Franchise')){
                $owner_id = $this->user->get_owner_list($user_id)[0]['user_id'];
            }else if($this->aauth->is_member('Franchise Agents')){
                $users = $this->user->get_users_list_by_owner($user_id);
                $owner_id = $users[0]['owner_id'];
            }
            $owner_id = 1;
            $line_item_data = $this->service->get_service_line_item_data_for_proposal($owner_id,$this->input->post());
            $company_markup = $this->company_markup->get_markup_data_for_proposal($owner_id,$this->input->post());
            
            $data['success'] = TRUE;
            $data['line_item_data'] = $line_item_data;
            $data['company_markup'] = $company_markup;
        } else {
            $data['success'] = FALSE;
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }
    
    /** Service Category Related Functions
     * 
     */

    public function manage_category() {
        
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        $categories = $this->common->fetch_where('tbl_service_category','*',array('parent_id' => 0));
        $data['categories'] = $categories;
        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/category/manage');
        $this->load->view('partials/footer');
    }

    public function add_category() {
        
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }

        $data = array();
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('category_name', 'Item Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $insert_data = array();
                $insert_data = $this->input->post();
                unset($insert_data['category_item_data']);
                $insert_data['category_slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-', $insert_data['category_name']);
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                //Insert Line Item
                $this->common->insert_data('tbl_service_category', $insert_data);
                $id = $this->common->insert_id();
                if ($id) {
                    $category_item_insert_data = array();
                    $category_item_data = $this->input->post('category_item_data');
                    if (!empty($category_item_data)) {
                        foreach ($category_item_data['category_name'] as $key => $value) {
                            $category_item_insert_data[$key]['parent_id'] = $id;
                            $category_item_insert_data[$key]['category_name'] = $value;
                            $category_item_insert_data[$key]['category_slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-', $value);
                            $category_item_insert_data[$key]['status'] = $category_item_data['status'][$key];
                            $category_item_insert_data[$key]['created_at'] = date('Y-m-d H:i:s');
                        }
                        $this->common->insert_batch('tbl_service_category', $category_item_insert_data);
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Category created successfuly.</div>');
                    redirect('admin/service/category/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while creating the category. </div>');
                }
            } else {
                $data['category_name'] = $this->input->post('category_name');
            }
        }

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/category/form');
        $this->load->view('partials/footer');
    }

    public function edit_category($id) {
        
        if (!$this->aauth->is_member('Admin')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> Restriced Access.</div>');
            redirect('admin/dashboard');
        }
        //Restricitng and Fetching of Data based on user logged In group   
        $category_data = $this->common->fetch_where('tbl_service_category', '*', array('id' => $id));

        if (empty($category_data)) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> No category found with specified Id.</div>');
            redirect($this->redirect_url);
        }

        $data = array();

        if (!empty($category_data)) {
            $data['category_name'] = $category_data[0]['category_name'];
        }
        if (!empty($this->input->post())) {
            $this->form_validation->set_rules('category_name', 'Item Name', 'required');

            if ($this->form_validation->run() != FALSE) {
                $update_data = array();
                $update_data = $this->input->post();
                unset($update_data['category_item_data']);
                $update_data['category_slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-', $update_data['category_name']);

                //Update Line Item
                $this->common->update_data('tbl_service_category', ['id' => $id], $update_data);

                if ($id) {
                    $category_item_update_data = array();
                    $cateogry_item_data = $this->input->post('category_item_data');
                    
                    //Bulk Update Line Item Data
                    $cat_item_data = $this->common->fetch_where('tbl_service_category', '*', ['parent_id' => $id]);
                  
                    $count = count($cat_item_data);
                    $count1 = (!empty($cateogry_item_data)) ? count($cateogry_item_data['category_name']) : 0;
                    if ($count > 0 && $count1 > 0) {
                        for ($i = 0; $i < $count; $i++) {
                            $category_item_update_data[$i]['category_name'] = $cateogry_item_data['category_name'][$i];
                            $category_item_update_data[$i]['category_slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-', $cateogry_item_data['category_name'][$i]);
                            $category_item_update_data[$i]['status'] = $cateogry_item_data['status'][$i];
                            $category_item_update_data_cond[$i]['id'] = $cat_item_data[$i]['id'];
                            $this->common->update_data('tbl_service_category', $category_item_update_data_cond[$i], $category_item_update_data[$i]);
                        }
                    }

                    //Bulk Insert Line Item Data
                    $category_item_insert_data = array();
                    if ($count1 > 0) {
                        for ($j = $count, $k = 0; $j < $count1; $j++, $k++) {
                            $category_item_insert_data[$k]['parent_id'] = $id;
                            $category_item_insert_data[$k]['category_name'] = $cateogry_item_data['category_name'][$j];
                            $category_item_insert_data[$k]['category_slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-', $cateogry_item_data['category_name'][$j]);
                            $category_item_insert_data[$k]['status'] = $cateogry_item_data['status'][$j];
                            $category_item_insert_data[$k]['created_at'] = date('Y-m-d H:i:s');
                            $stat = $this->common->insert_data('tbl_service_category', $category_item_insert_data[$k]);
                        }
                    }
                    

                    //Check if delete Required on less item data thean already stored
                    if ($count > $count1) {
                        for ($l = $count1, $m = 0; $l < $count; $l++, $m++) {
                            $cat_item_cond[$m]['id'] = $cat_item_data[$l]['id'];
                            $this->common->delete_data('tbl_service_category', $cat_item_cond[$m]);
                        }
                    }
                    $this->session->set_flashdata('message', '<div class="alert alert-success"> Category updated successfuly.</div>');
                    redirect('admin/service/category/manage');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> Looks like something went wrong while updating the category. </div>');
                }
            } else {
                $data['category_name'] = $this->input->post('category_name');
            }
        }


        $data['id'] = $id;

        $this->load->view('partials/header', $data);
        $this->load->view('partials/sidebar');
        $this->load->view('service/category/form');
        $this->load->view('partials/footer');
    }
    
    public function category_item_data() {
        $data = array();
        $data['success'] = FALSE;
        $category_id = $this->input->get('category_id');
        if (isset($category_id) && $category_id != '' && $category_id != NULL && $category_id != 'null') {
            $category_item_data = $this->common->fetch_where('tbl_service_category', '*', array('parent_id' => $category_id));
            $data['category_item_data'] = $category_item_data;
            $data['success'] = TRUE;
        } else {
            $data['status'] = 'Invalid Request';
        }
        echo json_encode($data);
        die;
    }
}

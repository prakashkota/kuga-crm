$(document).ready(function () {

    $('input[name=product_file]').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $('input[name=product_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: base_url + 'admin/product/upload_datasheet',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('#datasheets').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
        $('#loader').html('');
    });

    $('input[name=is_price_set]').click(function () {
        if ($(this).val() == 1) {
            $('#product_amount_row').show();
        } else {
            $('#product_amount_row').hide();
        }
    });

    if ($('#product_list').length > 0) {
        $('#product_list').DataTable({
            "order": [[1, "asc"], [2, "asc"], [3, "asc"]],
        });
    }

    $('#product_csv_file_import').click(function () {
        var file_data = $('input[name=product_csv_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('led_product_csv_file', file_data);
        form_data.append('action', 'import_led_product_csv');
        import_csv(form_data);
    });

    $('#product_variant_csv_file_import').click(function () {
        var file_data = $('input[name=product_variant_csv_file]').prop('files')[0];
        var form_data = new FormData();
        form_data.append('product_variant_csv_file', file_data);
        form_data.append('action', 'import_product_variant_csv');
        import_csv(form_data);
    });

    function import_csv(form_data) {
        $.ajax({
            url: base_url + 'admin/product/import_csv',
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
                $('.loader1').html(createLoader('Importing file, please wait..'));
            },
            success: function (res) {
                $('.loader1').html('');
                if (res.success == true) {
                    toastr["success"](res.status);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            }
        });
    }
});

//Product Variant Related

var product_variants = function (options) {
    var self = this;
    this.id = (options.id && options.id != '') ? options.id : '';
    this.product_state = (options.product_state && options.product_state != '') ? options.product_state : {};
    this.product_application = (options.product_application && options.product_application != '') ? options.product_application : {};
    this.product_variants_wrapper = 'product_variants_wrapper'
    this.product_variants_btn = 'add_product_variants';

    if (options.id && options.id != '') {
        self.load_variant_data();
    } else {
        setTimeout(function () {
            var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
            var state_select_field = self.create_select_field('state', self.product_state, null);
            var application_select_field = self.create_select_field('application', self.product_application, null);
            var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="0.00">';
            var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="0.00">';
            var amount_set_field = self.create_amount_set_field(null);
            var readonly = 'readonly';
            var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" value="0.00" ' + readonly + '>';
            $(product_variants_wrapper).append('<tr>'
                    + '<td>' + state_select_field + '</td>'
                    + '<td>' + application_select_field + '</td>'
                    + '<td>' + cost_price_field + '</td>'
                    + '<td>' + amount_set_field + '</td>'
                    + '<td>' + amount_field + '</td>'
                    + '<td>' + adn_margin_field + '</td>'
                    + '<td></td>');
        }, 500);
    }

    $('form button').on("click", function (e) {
        e.preventDefault();
    });

    self.create_product_variants();
    self.remove_product_variants();

};

product_variants.prototype.load_variant_data = function () {
    var self = this;
    var product_id = self.id;
    var form_data = 'product_id=' + product_id;
    $.ajax({
        url: base_url + 'admin/product/fetch_product_variant_data',
        type: 'get',
        data: form_data,
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                if (response.product_variant_data.length == 0) {
                    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
                    var state_select_field = self.create_select_field('state', self.product_state);
                    var application_select_field = self.create_select_field('application', self.product_application);
                    var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01"  name="product_variant[cost_price][]">';
                    var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]">';
                    var amount_set_field = self.create_amount_set_field();
                    var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" readonly>';
                    $(product_variants_wrapper).append('<tr>'
                            + '<td>' + state_select_field + '</td>'
                            + '<td>' + application_select_field + '</td>'
                            + '<td>' + cost_price_field + '</td>'
                            + '<td>' + amount_set_field + '</td>'
                            + '<td>' + amount_field + '</td>'
                            + '<td>' + adn_margin_field + '</td>'
                            + '<td></td>');
                    $('.is_price_set').on('change', function () {
                        $(this).closest('tr').find('input.amount').removeAttr('readonly');
                        if ($(this).val() == 0) {
                            $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                        }
                    });
                }
                for (var i = 0; i < response.product_variant_data.length; i++) {
                    if (i == 0) {
                        var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
                        var state_select_field = self.create_select_field('state', self.product_state, response.product_variant_data[i].state);
                        var application_select_field = self.create_select_field('application', self.product_application, response.product_variant_data[i].application);
                        var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="' + response.product_variant_data[i].cost_price + '">';
                        var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="' + response.product_variant_data[i].additional_margin + '">';
                        var amount_set_field = self.create_amount_set_field(response.product_variant_data[i].is_price_set);
                        var readonly = (response.product_variant_data[i].is_price_set == 0) ? 'readonly' : '';
                        var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" value="' + response.product_variant_data[i].amount + '" ' + readonly + '>';
                        $(product_variants_wrapper).append('<tr>'
                                + '<td>' + state_select_field + '</td>'
                                + '<td>' + application_select_field + '</td>'
                                + '<td>' + cost_price_field + '</td>'
                                + '<td>' + amount_set_field + '</td>'
                                + '<td>' + amount_field + '</td>'
                                + '<td>' + adn_margin_field + '</td>'
                                + '<td></td>');
                    } else {
                        self.create_product_variants(response.product_variant_data[i]);
                    }
                    $('.is_price_set').on('change', function () {
                        $(this).closest('tr').find('input.amount').removeAttr('readonly');
                        if ($(this).val() == 0) {
                            $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                        }
                    });
                }
            }
        }
    });
};

product_variants.prototype.create_product_variants = function (data) {
    var self = this;
    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
    var product_variants_btn = document.getElementById(self.product_variants_btn);
    if (!data) {
        product_variants_btn.addEventListener("click", function (e) { //on add input button click
            e.preventDefault();
            var state_select_field = self.create_select_field('state', self.product_state);
            var application_select_field = self.create_select_field('application', self.product_application);
            var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" >';
            var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01" name="product_variant[additional_margin][]">';
            var amount_set_field = self.create_amount_set_field();
            var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01"  name="product_variant[amount][]" readonly>';
            $(product_variants_wrapper).append('<tr>'
                    + '<td>' + state_select_field + '</td>'
                    + '<td>' + application_select_field + '</td>'
                    + '<td>' + cost_price_field + '</td>'
                    + '<td>' + amount_set_field + '</td>'
                    + '<td>' + amount_field + '</td>'
                    + '<td>' + adn_margin_field + '</td>'
                    + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
            $('.is_price_set').on('change', function () {
                $(this).closest('tr').find('input.amount').removeAttr('readonly');
                if ($(this).val() == 0) {
                    $(this).closest('tr').find('input.amount').attr('readonly', 'readonly');
                }
            });
        });
    } else {
        var state_select_field = self.create_select_field('state', self.product_state, data.state);
        var application_select_field = self.create_select_field('application', self.product_application, data.application);
        var cost_price_field = '<input type="number" class="form-control square" placeholder="Cost Price" step="0.01" name="product_variant[cost_price][]" value="' + data.cost_price + '">';
        var adn_margin_field = '<input type="number" class="form-control square" placeholder="Additional Margin" step="0.01"  name="product_variant[additional_margin][]" value="' + data.additional_margin + '">';
        var amount_set_field = self.create_amount_set_field(data.is_price_set);
        var readonly = (data.is_price_set == 0) ? 'readonly' : '';
        var amount_field = '<input type="number" class="form-control square amount" placeholder="Amount" step="0.01" name="product_variant[amount][]" value="' + data.amount + '" ' + readonly + '>';
        $(product_variants_wrapper).append('<tr>'
                + '<td>' + state_select_field + '</td>'
                + '<td>' + application_select_field + '</td>'
                + '<td>' + cost_price_field + '</td>'
                + '<td>' + amount_set_field + '</td>'
                + '<td>' + amount_field + '</td>'
                + '<td>' + adn_margin_field + '</td>'
                + '<td><a href="#" class="remove_lt_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></td>');
    }
};


product_variants.prototype.remove_product_variants = function () {
    var self = this;
    var product_variants_wrapper = document.getElementById(self.product_variants_wrapper);
    $(product_variants_wrapper).on("click", ".remove_lt_field", function (e) {//user click on remove field
        e.preventDefault();
        $(this).parent().parent('tr').remove();
    });
};

product_variants.prototype.create_select_field = function (key, data, current_value) {
    var self = this;
    data = JSON.parse(data);
    var select = '<select name="product_variant[' + key + '][]" class="form-control">';
    for (var i = 0; i < data.length; i++) {
        var value = data[i];
        var sel = (current_value && current_value == value) ? 'selected="selected"' : '';
        select += '<option value="' + value + '" ' + sel + '>' + value + '</option>';
    }
    return select;
};

product_variants.prototype.create_amount_set_field = function (value) {
    var select = '<select name="product_variant[is_price_set][]" class="form-control is_price_set">';
    var selected1 = (value && value == 1) ? 'selected="selected"' : '';
    var selected0 = (value && value == 0) ? 'selected="selected"' : '';
    if (value = '' || value == null) {
        selected0 = 'selected="selected"';
    }
    select += '<option value="1" ' + selected1 + '>Yes</option>';
    select += '<option value="0" ' + selected0 + '>No</option>';
    return select;
};
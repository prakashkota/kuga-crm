

var job = function (options) {
    var self = this;
    this.activity_action = '';
    this.proposal_id = '';
    document.getElementById('stages').appendChild(createLoader('Fetching reporting information....'));
    
    jQuery('#scheduled_at').datetimepicker({
        minDate: moment()
    });
    
    $('#add_activity').click(function () {
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#is_scheduled').val(0);
        $('div#scheduled_date').addClass('hidden');
        self.activity_action = "Added Activity";
        $('#activity_modal').modal('show');
    });

    $('#schedule_activity').click(function () {
        $('#activity_add_form').trigger("reset");
        $('.custom-file-label').html('Choose attachment file');
        $('#activity_id').val('');
        $('#is_scheduled').val(1);
        $('div#scheduled_date').removeClass('hidden');
        $('.datepicker').show();
        self.activity_action = "Scheduled Activity";
        $('#activity_modal').modal('show');
    });
    
    //Activity
    self.save_activity();
    self.upload_activity_file();
    self.delete_activity();
    self.edit_activity();
}


job.prototype.toggle_selected = function (elem) {
  var self = this;
  $(".stage_list__item").removeClass("stage_list__item--selected");
  $(elem).addClass('stage_list__item--selected');
  $(elem).removeAttr('onclick');
  $(elem).attr('onclick','window.job_tool.toggle_unselected(this)');
  var data_item = JSON.parse($(elem).attr('data-item'));
  self.proposal_id =  data_item.uuid;
  self.create_job_details(data_item);
  self.fetch_activity_data();
  $('#job_detail_container').removeClass('hidden');
  if ($('#all_activity_table')) {
    $('#all_activity_table').DataTable().destroy();
  }
  document.getElementById('all_activity_table_body').innerHTML = "";
  $("html, body").animate({ scrollTop: $(document).height() }, 1000);
};

job.prototype.toggle_unselected = function (elem) {
  var self = this;
  $(".stage_list__item").removeClass("stage_list__item--selected");
  $(elem).removeAttr('onclick');
  $(elem).attr('onclick','window.job_tool.toggle_selected(this)');
  $('#job_detail_container').addClass('hidden');
};

job.prototype.create_stage_list = function (data) {
    var self = this;
    var div_list = document.createElement('div');
    div_list.className = "list";
    var div_list_header = document.createElement('header');
    div_list_header.innerHTML = data.stage_name;
    var div_list_ul = document.createElement('div');
    div_list_ul.className = "connectedSortable stage_list";
    div_list_ul.setAttribute('id', 'stage_' + data.id);
    div_list_ul.setAttribute('data-id', data.id);
    div_list_ul.setAttribute('data-name', data.stage_name);
    //var div_list_li = document.createElement('li');
    //div_list_li.setAttribute('onclick', 'loadNoteDetails(' + JSON.stringify(data) + ');');
    //div_list_li.innerHTML = data.notes_point;
    var div_list_footer = document.createElement('footer');
    //var addCardAction = document.createElement('a');
    //addCardAction.setAttribute('onclick', 'addNewTabPoint("' + uid + '","' + notes_tab + '")');
    //addCardAction.setAttribute('href', 'javascript:void(0);');
    //addCardAction.innerHTML = "Add a point..";
    //div_list_footer.appendChild(addCardAction);
    div_list.appendChild(div_list_header);
    div_list.appendChild(div_list_ul);
    div_list.appendChild(div_list_footer);
    return div_list;
}

job.prototype.create_stage_list_item = function (data) {

    var self = this;
    var stage_list_ele;

    stage_list_ele = document.querySelector('#stage_' + data.schedule_stage_id);

    if (typeof (stage_list_ele) != 'undefined' && stage_list_ele != null) {

        var color = '';
        var div_list_item = document.createElement('div');
        div_list_item.setAttribute('id', data.uuid);
        div_list_item.setAttribute('data-job_id', data.job_id);
        div_list_item.setAttribute('data-item', JSON.stringify(data));
        div_list_item.setAttribute('onclick', "window.job_tool.toggle_selected(this)");
        div_list_item.className = "stage_list__item ui-state-default " + color;
        //div_list_li.setAttribute('onclick', 'loadNoteDetails("edit","' + itemCounter + '");');
        var div_list_item_link = document.createElement('a');
        div_list_item_link.href = 'proposalAddNew.php?id=' + data.uuid;
        div_list_item_link.setAttribute('style', 'text-decoration:none; color: inherit;');
        div_list_item_link.innerHTML = '<i class="fas fa-chevron-circle-right pull-right"></i>';

        var div_list_item_heading = document.createElement('h2');
        var fname = (data.first_name != null && data.first_name != '') ? data.first_name : '';
        var lname = (data.last_name != null && data.last_name != '') ? data.last_name : '';
        var custname = (data.company_name != null && data.company_name != '' && data.company_name != '0') ? data.company_name : fname + ' ' + lname;

        div_list_item_heading.innerHTML = custname + ' job';

        var div_list_item_body = document.createElement('span');
        div_list_item_body.innerHTML = '$' + data.quote_total_inGst + ' - ' + data.system_size + 'kW, ' + data.state_postal + '|' + data.postcode;

        var input = document.createElement('input');
        //input.setAttribute('name', 'notes_tab[' + itemCounter + ']');
        //input.className = 'card_' +  uid + '_tab_name';
        input.setAttribute('style', 'display:none;');
        //input.value = notes_tab;
        div_list_item.appendChild(div_list_item_heading);
        div_list_item.appendChild(div_list_item_body);
        div_list_item.appendChild(input);
        stage_list_ele.appendChild(div_list_item);
    }
}

job.prototype.fetch_stage_list = function () {
    var self = this;
    $.ajax({
        url: base_url + "admin/job/fetch_proposal_scheduling_stages",
        type: 'post',
        dataType: "json",
        data: {action: 'fetch_proposal_stages'},
        success: function (data) {
            document.getElementById('stages').innerHTML = '';
            if (data.proposal_stages.length > 0) {
                for (var i = 0; i < data.proposal_stages.length; i++) {
                    var list = self.create_stage_list(data.proposal_stages[i]);
                    document.querySelector('#stages').appendChild(list);
                }

                for (var i = 0; i < data.proposal_data_by_stages.length; i++) {

                    if (data.proposal_data_by_stages[i].length > 0) {

                        for (var j = 0; j < data.proposal_data_by_stages[i].length; j++) {
                            self.create_stage_list_item(data.proposal_data_by_stages[i][j]);
                        }
                    }
                }

                /**Make Header Fixed on Scroll Vertical
                 $('.lists').scroll(function(){
                 var sticky = $('.list > header');
                 var scroll = $('.lists').scrollTop();
                 if (scroll >= 100){
                 sticky.addClass('fixed');
                 }else{ 
                 sticky.removeClass('fixed');
                 }
                 });
                 **/

                var currentlyScrolling = false;
                var SCROLL_AREA_WIDTH = 150; // Distance from container left and bottom right.

                $("div.connectedSortable").sortable({
                    scroll: true,
                    connectWith: ".connectedSortable",
                    cancel: ".disable-sort-item",
                    tolerance: 'pointer',
                    placeholder: 'sortablePlaceholder',
                    forcePlaceholderSize: false,
                    forceHelperSize: false,
                    start: function (event, ui) {

                        $('.sr-deal_actions').addClass('sr-deal_actions--visible');
                    },
                    stop: function (event, ui) {
                        $('.sr-deal_actions').removeClass('sr-deal_actions--visible');
                    },
                    receive: function (event, ui) {
                        var receiver = ui.item.closest('div.stage_list');
                        var receiver_id = ui.item.closest('div.stage_list').attr('data-id');
                        if (receiver_id == 3) {
                            $('#job_schedule_modal').modal('show');
                            $('.close').on('click', function () {
                                $(ui.sender).sortable('cancel');
                            })
                            
                            $("#addJobSchedule").unbind( "click" );
                            $("#addJobSchedule").on("click",function () {
                                self.save_job_schedule(event, ui);
                            });
                        } else {
                            self.update_job_schedule(event, ui);
                        }

                    },
                    sort: function (event, ui) {

                        if (currentlyScrolling) {
                            return;
                        }

                        var continerWidth = $('.lists').width();
                        var mouseXPosition = event.clientX;

                        if ((mouseXPosition - SCROLL_AREA_WIDTH) < SCROLL_AREA_WIDTH) {
                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "-=" + continerWidth / 4 + "px" // Scroll up half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });

                        } else if (mouseXPosition > (continerWidth - SCROLL_AREA_WIDTH)) {

                            currentlyScrolling = true;
                            $('.lists').animate({
                                scrollLeft: "+=" + continerWidth / 4 + "px" // Scroll down half of window height.
                            },
                                    400, // 400ms animation.
                                    function () {
                                        currentlyScrolling = false;
                                    });
                        }
                    }
                }).disableSelection();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

job.prototype.create_job_details = function (data) {
    var self = this;
    var job_data = {};
    job_data.customer_name = data.first_name + ' ' + data.last_name;
    job_data.customer_address = data.address;
    job_data.customer_contact_no = data.customer_contact_no;
    job_data.system_value = '$'+ data.system_total;
    job_data.system_size = data.system_size + 'kW';

    var customer_details_body = document.getElementById('job_detail_body');
    
    var list_group = document.createElement('ul');
    list_group.className = "list-group list-group-flush";

    var list_group_item;

    for (var key in job_data) {
        list_group_item = document.createElement('li');
        list_group_item.className = "list-group-item";
        var item_name = key.split('_');
        var item_name_new = '';
        for (var i = 0; i < item_name.length; i++) {
            if (i == 0) {
                item_name_new = item_name[i].charAt(0).toUpperCase() + item_name[i].slice(1);
            } else {
                item_name_new = item_name_new + " " + item_name[i].charAt(0).toUpperCase() + item_name[i].slice(1);
            }
        }

        list_group_item.innerHTML = "<b>" + item_name_new + "</b>: " + job_data[key];
        list_group.appendChild(list_group_item);
    }
    //Tick Box - Stock Confirmed
    var list_group_item_stock = document.createElement('li');
    list_group_item_stock.className = "list-group-item";
    
    var list_group_item_stock_checkbox = document.createElement('input');
    list_group_item_stock_checkbox.className = "ml-5 mt-2 job_data";
    list_group_item_stock_checkbox.setAttribute('type','checkbox');
    list_group_item_stock_checkbox.setAttribute('name','job[stock_confirmed]');
    list_group_item_stock_checkbox.setAttribute('data-job_id', data.job_id);
    list_group_item_stock_checkbox.setAttribute('data-item', JSON.stringify(data));
    list_group_item_stock_checkbox.setAttribute('onclick', 'window.job_tool.update_job_detail(this);');
    if(data.stock_confirmed == '1'){
        list_group_item_stock_checkbox.setAttribute('checked','checked');
    }
    
    list_group_item_stock.innerHTML = "<b>Stock Confirmed:</b>";
    list_group_item_stock.appendChild(list_group_item_stock_checkbox);
    
    //Tick Box -  Pre-Install paperwork complete
     var list_group_item_paperwork = document.createElement('li');
    list_group_item_paperwork.className = "list-group-item";
    
    var list_group_item_paperwork_checkbox = document.createElement('input');
    list_group_item_paperwork_checkbox.className = "ml-5 mt-2 job_data";
    list_group_item_paperwork_checkbox.setAttribute('name','job[pre_install_paper_work]');
    list_group_item_paperwork_checkbox.setAttribute('type','checkbox');
    list_group_item_paperwork_checkbox.setAttribute('data-job_id', data.job_id);
    list_group_item_paperwork_checkbox.setAttribute('data-item', JSON.stringify(data));
    list_group_item_paperwork_checkbox.setAttribute('onclick', 'window.job_tool.update_job_detail(this);');
    if(data.pre_install_paper_work == '1'){
        list_group_item_paperwork_checkbox.setAttribute('checked','checked');
    }
    
    list_group_item_paperwork.innerHTML = "<b>Pre-Install paperwork complete:</b>";
    list_group_item_paperwork.appendChild(list_group_item_paperwork_checkbox);
    
    list_group.appendChild(list_group_item_stock);
    list_group.appendChild(list_group_item_paperwork);
   
    customer_details_body.innerHTML = '';
    customer_details_body.appendChild(list_group);


    $('#save_search_activity').click(function () {
        self.save_search_activity();
    });
    
    
};

job.prototype.update_job_schedule = function (event, ui) {
    var self = this;
    var receiver_id = ui.item.closest('div.stage_list').attr('data-id');
    var current_item = $(ui.item);
    var current_item_job_id = $(ui.item).attr('data-job_id');
    var current_item_data = $(ui.item).attr('data-item');
    current_item_data = JSON.parse(current_item_data);

    var data = '';
    if (current_item_job_id != '' && current_item_job_id != null && current_item_job_id != 'null') {
        data = 'job_id=' + current_item_job_id + '&job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id;
    } else {
        data = 'job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id;
    }

    $.ajax({
        url: base_url + "admin/job/save_proposal_job_schedule",
        type: 'post',
        dataType: "json",
        data: data,
        beforeSend: function () {
            current_item.removeClass('bg-danger');
            current_item.removeClass('text-white');
            current_item.addClass('disable-sort-item');
            current_item.addClass('bg-disabled');
        },
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                current_item.attr('data-job_id', data.id);
            } else {
                toastr["error"](data.status);
            }
            if (data.success == false) {
                $(ui.sender).sortable('cancel');
            }
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $(ui.sender).sortable('cancel');
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        }
    });
};

job.prototype.update_job_detail = function (ele) {
    var self = this;
    
    var current_item = $(ele);
    var current_item_data = current_item.attr('data-item');
    current_item_data = JSON.parse(current_item_data);
    var receiver_id = current_item_data.schedule_stage_id;
    var current_item_job_id = current_item.attr('data-job_id');
    var name = $(ele).attr('name');
    var name_val = (current_item.is(":checked") == true) ? 1 : 0;

    var data = '';
    if (current_item_job_id != '' && current_item_job_id != null && current_item_job_id != 'null') {
        data = 'job_id=' + current_item_job_id + '&job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id + '&'+name+'=' + name_val;
    } else {
        data = 'job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id + '&'+name+'=' + name_val;
    }

    $.ajax({
        url: base_url + "admin/job/save_proposal_job_schedule",
        type: 'post',
        dataType: "json",
        data: data,
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                current_item.attr('data-job_id', data.id);
                $('.job_data').attr('data-job_id', data.id);
            } else {
                toastr["error"](data.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

job.prototype.save_job_schedule_handler = function (event, ui, data) {
    var self = this;
    var receiver_id = ui.item.closest('div.stage_list').attr('data-id');
    var current_item = $(ui.item);
    var current_item_job_id = $(ui.item).attr('data-job_id');
    var current_item_data = $(ui.item).attr('data-item');
    current_item_data = JSON.parse(current_item_data);


    if (current_item_job_id != '' && current_item_job_id != null && current_item_job_id != 'null') {
        data = data + '&job_id=' + current_item_job_id + '&job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id;
    } else {
        data = data + '&job[proposal_id]=' + current_item_data.id + '&job[schedule_stage_id]=' + receiver_id;
    }

    $.ajax({
        url: base_url + "admin/job/save_proposal_job_schedule",
        type: 'post',
        dataType: "json",
        data: data,
        beforeSend: function () {
            current_item.removeClass('bg-danger');
            current_item.removeClass('text-white');
            current_item.addClass('disable-sort-item');
            current_item.addClass('bg-disabled');
        },
        success: function (data) {
            if (data.success == true) {
                toastr["success"](data.status);
                current_item.attr('data-job_id', data.id);
                $('#job_schedule_modal').modal('hide');
            } else {
                toastr["error"](data.status);
            }
            if (data.success == false) {
                $(ui.sender).sortable('cancel');
            }
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $(ui.sender).sortable('cancel');
            current_item.removeClass('disable-sort-item');
            current_item.removeClass('bg-disabled');
        }
    });
};


job.prototype.save_job_schedule = function (event, ui) {
    var self = this;

    var flag = true;
    var job_user_id = $('#job_user_id').val();
    $('#job_user_id').removeClass('is-invalid');
    if (job_user_id == '') {
        $('#job_user_id').addClass('is-invalid');
        flag = false;
    }
    if (!flag) {
        toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
    }
    if (flag) {
        $("#addJobSchedule").attr("disabled", "disabled");
        var job_schedule_form_data = $('#job_schedule_add_form').serialize();
        self.save_job_schedule_handler(event, ui, job_schedule_form_data);
        $("#addJobSchedule").removeAttr("disabled", "disabled");
    }
};

job.prototype.save_activity = function () {
    var self = this;
    $(document).on('click', '#addActivity', function () {
        var flag = true;
        var activity_type = $('#activity_type').val();
        $('#activity_type').removeClass('is-invalid');
        if (activity_type == '') {
            $('#activity_type').addClass('is-invalid');
            flag = false;
        }
        if (!flag) {
            toastr["error"]('Error ! Required fields missing. Please check fields marked in red.');
        }
        if (flag) {
            $("#addActivity").attr("disabled", "disabled");
            var activity_add_form_data = $('#activity_add_form').serialize();
            self.save_activity_handler(activity_add_form_data);
            $("#addActivity").removeAttr("disabled", "disabled");
        }
    });
};


job.prototype.save_activity_handler = function (data) {
    var self = this;
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/save_proposal_activity',
        datatype: 'json',
        data: data + '&uuid=' + self.proposal_id + '&activity_action=' + self.activity_action + '&action=save_activity',
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success == true) {
                toastr["success"](stat.status);
                $('#activity_add_form').trigger("reset");
                $('.custom-file-label').html('Choose attachment file');
                $('#activity_modal').modal('hide');
                self.fetch_activity_data();
            } else {
                toastr["error"](stat.status);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

job.prototype.edit_activity = function () {
    var self = this;
    $(document).on('click', '.activity_edit', function () {
        if (self.user_type != '1' && self.user_type != '4') {
            toastr["error"]('Users should not be able to edit activities (only admins)');
            return false;
        }
        var $this = $(this);
        var activity_data = JSON.parse($this.attr('data-item'));
        $('#activity_add_form').trigger("reset");
        $('#activity_type').val(activity_data.activity_type);
        $('#activity_note').val(activity_data.note);
        $('#activity_id').val(activity_data.id);
        $('#is_scheduled').val(0);
        $('.datepicker').hide();
        $('#scheduled_date').addClass('hidden');
        $('#activity_file_value').val('');
        $('.custom-file-label').html("Choose attachment file");
        self.activity_action = "Added Activity";
        if (activity_data.attachment != '' && activity_data.attachment != null) {
            $('#activity_file_value').val(activity_data.attachment);
            $('.custom-file-label').html(activity_data.attachment);

        }
        if (activity_data.is_scheduled == 1) {
            $('#is_scheduled').val(1);
            $('#scheduled_date').removeClass('hidden');
            $('.datepicker').datepicker("setDate", activity_data.scheduled_at);
            $('.datepicker').show();
            self.activity_action = "Scheduled Activity";
        }
        $('#activity_modal').modal('show');
    });
};

job.prototype.delete_activity = function () {
    var self = this;
    $(document).on('click', '.activity_delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            type: 'POST',
            url: base_url + 'admin/proposal/delete_proposal_activity',
            datatype: 'json',
            data: 'id=' + id + '&action=delete_activity',
            success: function (stat) {
                stat = JSON.parse(stat);
                if (stat.success == true) {
                    toastr["success"](stat.status);
                    $this.closest("tr").remove();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
};

job.prototype.fetch_activity_data = function () {
    var self = this;
    /**$('#scheduled_activites').empty();
     $('#scheduled_activites').monthly({
     mode: 'event',
     jsonUrl: 'ajaxProposalActivity.php?uuid=' + self.proposal_id + '&action=fetch_scheduled_activity',
     dataType: 'json'
     });*/
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/proposal/fetch_proposal_activity',
        datatype: 'json',
        data: 'uuid=' + self.proposal_id + '&action=fetch_activity',
        success: function (stat) {
            stat = JSON.parse(stat);
            if (stat.success == true) {
                if (stat.all_activity.length > 0) {
                    self.handle_activity_data(stat.all_activity);
                }
            } else {
                toastr["error"](stat.status);
            }
        }
    });
};

job.prototype.handle_activity_data = function (data) {
    var self = this;
    if ($('#all_activity_table')) {
        $('#all_activity_table').DataTable().destroy();
    }
    $('#all_activity_table').show();
    document.getElementById('all_activity_table_body').innerHTML = "";
    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        td1.innerHTML = data[i].full_name;
        var td2 = document.createElement('td');
        td2.innerHTML = data[i].activity_type;
        var td3 = document.createElement('td');
        td3.innerHTML = data[i].note;
        var td4 = document.createElement('td');
        td4.className = 'sr-no_wrap';
        td4.innerHTML = data[i].created_at;
        var td5 = document.createElement('td');
        td5.className = 'sr-no_wrap';
        td5.innerHTML = (data[i].scheduled_at != '00/00/0000') ? data[i].scheduled_at : '<div class="text-center">-</div>';
        var td6 = document.createElement('td');
        td6.innerHTML = '';
        if (data[i].attachment != '' && data[i].attachment != null) {
            var url =  base_url + 'assets/uploads/activity_files/' + data[i].attachment;
            td6.innerHTML += '<a href="'+url+'"  target="__blank" style="margin-left:10px;"><i class="fa fa-paperclip"></i></a>';
        }
        td6.innerHTML += "<a href='javascript:void(0);' class='activity_edit' data-item='" + JSON.stringify(data[i]) + "' style='margin-left:10px;'><i class='fa fa-pencil'></i></a>"
        td6.innerHTML += '<a href="javascript:void(0);" class="activity_delete" data-id="' + data[i].id + '" style="margin-left:10px; color:red;"><i class="fa fa-times"></i></a>';
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);
        tr.appendChild(td5);
        tr.appendChild(td6);
        document.getElementById('all_activity_table_body').appendChild(tr);
    }

    $('#all_activity_table').DataTable({
        "order": [
            [3, "asc"]
        ],
        "bInfo": false,
        "bLengthChange": false,
        "bFilter": true,
    });

};

job.prototype.upload_activity_file = function () {
    var self = this;
    $('#activity_file').change(function () {
        $('#loader').html(createLoader('Uploading file, please wait..'));
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('upload_path', 'activity_files');
        toastr["info"]("Uploading file please wait...");
        $.ajax({
            url: base_url + 'admin/product/upload_activity_file',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (res) {
                if (res.success == true) {
                    toastr["success"](res.status);
                    $('input[name="activity[attachment]"').val(res.file_name);
                    $('.custom-file-label').html(res.file_name);
                } else {
                    toastr["error"]((res.status) ? res.status : 'Looks like something went wrong');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        $('#loader').html('');
    });
};
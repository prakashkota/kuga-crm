var meter_data_window;

var solar_proposal_manager = function (options) {
	var self = this;
	this.commercialTabPanelId = 1;
	this.meterDataUploadStatus = 0;
	this.total_payable_exGST = 0;
	this.isFinance = 1;
	this.isProductionFile = 0;
	this.lead_data =
		options.lead_data && options.lead_data != ''
			? JSON.parse(options.lead_data)
			: {};
	this.lead_uuid =
		options.lead_uuid && options.lead_uuid != '' ? options.lead_uuid : '';
	this.site_id =
		options.lead_uuid && options.site_id != '' ? options.site_id : '';
	this.lead_id =
		options.lead_uuid && options.lead_id != '' ? options.lead_id : '';
	this.userid = options.userid && options.userid != '' ? options.userid : '';
	console.log(options);
	this.proposal_data =
		options.proposal_data && options.proposal_data != ''
			? JSON.parse(options.proposal_data)
			: {};
	this.financial_summary_data =
		options.financial_summary_data && options.financial_summary_data != ''
			? JSON.parse(options.financial_summary_data)
			: '';

	this.panel_data =
		options.panel_data && options.panel_data != ''
			? JSON.parse(options.panel_data)
			: {};
	this.postcode_rating = parseFloat(options.postcode_rating);
	this.cust_id = '';
	this.financial_summary_type = 'STC';
	this.proposal_image = '';
	this.solar_panel_area_count = 1;
	this.gst = 1.1;
	this.total_panels = 0;
	this.total_panels_by_area = 0;
	this.total_system_size = 0;
	this.panel_row_id = '';

	this.pvSt_month1 = 0;
	this.pvSt_month2 = 0;
	this.pvSt_month3 = 0;
	this.pvSt_month4 = 0;
	this.pvSt_month5 = 0;
	this.pvSt_month6 = 0;
	this.pvSt_month7 = 0;
	this.pvSt_month8 = 0;
	this.pvSt_month9 = 0;
	this.pvSt_month10 = 0;
	this.pvSt_month11 = 0;
	this.pvSt_month12 = 0;
	this.pvtotal_production = 0;
	this.pvSt_avg_prev = 0;
	this.pvSt_avg = 0;
	this.restrictLoaderPvwattsApi = 0;
	this.dollarUSLocale = Intl.NumberFormat('en-US');

	// console.log(JSON.parse(options.lead_data));

	$('#financial_stc').show();
	$('#financial_lgc').hide();
	$('#financial_veec').hide();
	$('#financial_selection').val('STC').change();

	// $("#stc_calculate_price").val(37);
	$('.system_bettery_size').hide();

	$('.without_meter_data').hide();

	// Setting for financial dropdown

	$('#export_degradation_data').click(function () {
		var table_content = $('#viewTable').html();
		// table_content += $('#viewTable').html();
		// table_content += '</table>';

		$.ajax({
			url:
				base_url + 'admin/proposal/commercial/degradation_excel_export',
			type: 'post',
			data: {
				file_content: table_content,
			},
			beforeSend: function () {},
			success: function (stat) {
				window.location.href = base_url + stat;
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toastr['error'](
					thrownError +
						'\r\n' +
						xhr.statusText +
						'\r\n' +
						xhr.responseText
				);
			},
		});
	});

	$('#financial_selection').on('change', function () {
		$('#financial_stc').hide();
		$('#financial_lgc').hide();
		$('#financial_veec').hide();

		if ($(this).val() == 'STC') {
			$('#financial_stc').show();
		}

		if ($(this).val() == 'LGC') {
			$('#financial_lgc').show();
		}

		if ($(this).val() == 'VEEC') {
			self.calculate_veec_discount(false);
			$('#financial_veec').show();
		}
	});

	$('.veec_calculation_inputs').on('change', function (e) {
		self.calculate_veec_discount(false);
	});

	$('#save_veec_discount').on('click', function (e) {
		$.ajax({
			url: base_url + 'admin/proposal/commercial/save_veec_discount',
			type: 'post',
			data: {
				veec_discount: $('#veec_discount_val').val(),
				accuracy_factor: $('#veec_accuracy_factor').val(),
				decay_factor: $('#veer_decay_factor').val(),
				regional_factor: $('#veec_regional_factor').val(),
				veec_price: $('#veec_price').val(),
				proposal_id: self.proposal_data.id,
			},
			beforeSend: function () {},
			success: function (stat) {
				$('#veec_calculation_modal').modal('hide');
				// }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$('#site_postcode').removeAttr('readonly');
				toastr['error'](
					thrownError +
						'\r\n' +
						xhr.statusText +
						'\r\n' +
						xhr.responseText
				);
			},
		});
	});

	$('#system_pricing_battery').on('change', function () {
		if ($(this).val() !== '') {
			var systemPriceBattery = JSON.parse(
				$(this).find(':selected').attr('data-item')
			);
			$('.system_bettery_size').show();
		} else {
			$('.system_bettery_size').hide();
		}
		$('#system_pricing_battery_size').val(systemPriceBattery.capacity);
	});

	$('#calculate_degration_factor_data').on('click', function () {
		self.calculate_degration_factor_data(true);
	});

	// Bill Type toggle
	$('.is_bill_type').on('change', function (e) {
		e.preventDefault();

		var isBillType = $("input[name='is_bill_type']:checked").val();
		if (isBillType == 'timeofUse') {
			$('#billTypeTimeOfUse').show();
			$('#timeOfUseModelLink').show();
			$('#billTypeFlat').hide();
		} else {
			$('#billTypeTimeOfUse').hide();
			$('#timeOfUseModelLink').hide();
			$('#billTypeFlat').show();
		}
		//
	});

	$(document).on('change', '.calculate_energy_rate', function (e) {
		var FlatBillTypeRateValue = $('#flat_bill_type_rate').val();
		var FlatQuantityValue = $('#flat_quantity').val();
		var QuantityTypeValue = $('#quantity_type').val();

		if (FlatBillTypeRateValue != '0.000' && FlatQuantityValue != '0.000') {
			$('#flat_energy_rate').val(
				FlatBillTypeRateValue / FlatQuantityValue
			);
		}
	});

	$(document).on('click', '#demandRemoveRow', function (e) {
		e.preventDefault();
		var getDemanChargeRow = $('.demand-charge-row').length;

		if (getDemanChargeRow > 1) {
			$('#demandRemoveRow').show();
			$('.demand-charge-row-' + getDemanChargeRow).remove();

			getDemanChargeRow = $('.demand-charge-row').length;
			if (getDemanChargeRow == 1) {
				$('#demandRemoveRow').hide();
			}
		} else {
			$('#demandRemoveRow').hide();
		}

		$('#demandChargeValue').val($('.demand-charge-row').length);
	});

	$(document).on('change', '#predefined_choose_file', function (e) {
		if ($(this).val() != '') {
			$('#meter_data_action_outer').addClass('d-none');
		} else {
			$('#meter_data_action_outer').removeClass('d-none');
		}
	});

	// This for changing layout according to bill type
	$(document).on('change', '#meter_upload_status', function (e) {
		e.preventDefault();
		self.meterDataUploadStatus = $(this).val();

		if (self.meterDataUploadStatus == 1) {
			$('#meterDataFileURLRow').hide();
			// $("#timeOfUseModelLink").hide();
			$('.without_meter_data').show();

			var meter_data_file_download_btn = $(
				'#meter_data_actions'
			).hasClass('d-none');
			if (meter_data_file_download_btn) {
				$('#predefined_choose_file_outer').show();
			} else {
				$('#predefined_choose_file_outer').hide();
			}

			$('#demandChargeWrapper').html('');
			$('#billing_month_row').show();
			self.demand_charge_without_meter_data();
		} else {
			$('#meterDataFileURLRow').show();
			// $("#timeOfUseModelLink").show();
			$('.without_meter_data').hide();
			$('#predefined_choose_file_outer').hide();

			$('#demandChargeWrapper').html('');
			self.demand_charge_with_meter_data();
			$('#billing_month_row').hide();
		}
	});

	$(document).on('click', '.peak-btn-day-toggle label', function (e) {
		e.preventDefault();

		var dataid = $(this).attr('data-id');
		var labelChildren = $(this).parent().children();
		var currentValue = $(this).children('input').val();

		if (
			currentValue == 'Mon' ||
			currentValue == 'Tue' ||
			currentValue == 'Wed' ||
			currentValue == 'Thu' ||
			currentValue == 'Fri'
		) {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (
							day == 'Mon' ||
							day == 'Tue' ||
							day == 'Wed' ||
							day == 'Thu' ||
							day == 'Fri'
						) {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_peak_interval').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_peak_interval').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}

		if (currentValue == 'Sat' || currentValue == 'Sun') {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (day == 'Sat' || day == 'Sun') {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_peak_interval2').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_peak_interval2').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}
	});

	$(document).on('click', '.shoulder-btn-day-toggle label', function (e) {
		e.preventDefault();

		var dataid = $(this).attr('data-id');
		var labelChildren = $(this).parent().children();
		var currentValue = $(this).children('input').val();

		if (
			currentValue == 'Mon' ||
			currentValue == 'Tue' ||
			currentValue == 'Wed' ||
			currentValue == 'Thu' ||
			currentValue == 'Fri'
		) {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (
							day == 'Mon' ||
							day == 'Tue' ||
							day == 'Wed' ||
							day == 'Thu' ||
							day == 'Fri'
						) {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_shoulder_interval').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_shoulder_interval').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}

		if (currentValue == 'Sat' || currentValue == 'Sun') {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (day == 'Sat' || day == 'Sun') {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_shoulder_interval2').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_shoulder_interval2').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}
	});

	$(document).on('click', '.off-btn-day-toggle label', function (e) {
		e.preventDefault();

		var dataid = $(this).attr('data-id');
		var labelChildren = $(this).parent().children();
		var currentValue = $(this).children('input').val();

		if (
			currentValue == 'Mon' ||
			currentValue == 'Tue' ||
			currentValue == 'Wed' ||
			currentValue == 'Thu' ||
			currentValue == 'Fri'
		) {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (
							day == 'Mon' ||
							day == 'Tue' ||
							day == 'Wed' ||
							day == 'Thu' ||
							day == 'Fri'
						) {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_off_peak_interval').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_off_peak_interval').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}

		if (currentValue == 'Sat' || currentValue == 'Sun') {
			if ($(this).hasClass('active')) {
				for (let index = 0; index < labelChildren.length; index++) {
					var day = $(labelChildren[index]).children('input').val();
					if (day != currentValue) {
						if (day == 'Sat' || day == 'Sun') {
							if ($(labelChildren[index]).hasClass('active')) {
								break;
							} else {
								$('#meter_data_off_peak_interval2').addClass(
									'bill-wrapper-tb-col-disabled'
								);
							}
						}
					}
				}
			} else {
				$('#meter_data_off_peak_interval2').removeClass(
					'bill-wrapper-tb-col-disabled'
				);
			}
		}
	});

	$('.demandChargeToggle').on('change', function (e) {
		e.preventDefault();

		var isDemandCharge = $("input[name='is_demandCharge']:checked").val();
		if (isDemandCharge == '1') {
			$('#demandChargeWrapper').show();
			$('#addDemanCharge').show();

			if ($('.demand-charge-row').length > 1) {
				$('#demandRemoveRow').show();
			}
		} else {
			$('#demandChargeWrapper').hide();
			$('#addDemanCharge').hide();
			$('#demandRemoveRow').hide();
		}
		//
	});

	$('.isFinanceToggle').on('change', function (e) {
		e.preventDefault();

		var isFinance = $("input[name='is_finance']:checked").val();
		self.isFinance = isFinance;
		if (isFinance == '1') {
			$('#proposal_finance_form').show();
		} else {
			$('#proposal_finance_form').hide();
			self.delete_proposal_finance();
		}
		//
	});

	$('.uploadProductionToggle').on('change', function (e) {
		e.preventDefault();

		var isProductionFile = $(
			"input[name='is_uploadProduction']:checked"
		).val();

		if (isProductionFile == '1') {
			$('.productionDataFileURL').show();
			self.isProductionFile = 1;
			$('#system_pricing_panel_val').removeAttr('readonly', false);
			$('.d-solar-production').hide();
		} else {
			$('.productionDataFileURL').hide();
			$('.d-solar-production').show();
			self.isProductionFile = 0;
			$('#system_pricing_panel_val').attr('readonly', true);
		}
		//
	});

	$('#system_pricing_panel').on('change', function (e) {
		var no_of_panel = $('#system_pricing_panel_val').val();

		if ($(this).val() !== '') {
			var system_pricing_panel = $('#system_pricing_panel')
				.find(':selected')
				.attr('data-item');
			system_pricing_panel = JSON.parse(system_pricing_panel);
			if (no_of_panel) {
				$('#pv_system_size').val(
					parseFloat(
						(system_pricing_panel.capacity * no_of_panel) / 1000
					)
				);

				$('#stc_calculate_system_size').val(
					parseFloat(
						(system_pricing_panel.capacity * no_of_panel) / 1000
					)
				);
			}
			if (system_pricing_panel.panel_degradation_per !== null) {
				$('#pv_degradation_factor').val(
					parseFloat(system_pricing_panel.panel_degradation_per)
				);
			} else {
				$('#pv_degradation_factor').val('');
			}
		} else {
			$('#pv_system_size').val('');
			$('#pv_degradation_factor').val('');
		}
	});

	$('#system_pricing_panel_val').on('change', function (e) {
		var no_of_panel = $(this).val();
		var system_pricing_panel = $('#system_pricing_panel')
			.find(':selected')
			.attr('data-item');
		system_pricing_panel = JSON.parse(system_pricing_panel);
		$('#pv_system_size').val(
			parseFloat((system_pricing_panel.capacity * no_of_panel) / 1000)
		);
		$('#stc_calculate_system_size').val(
			parseFloat((system_pricing_panel.capacity * no_of_panel) / 1000)
		);
	});

	$('#system_pricing_inverter').on('change', function (e) {
		if ($(this).val() !== '') {
			var system_pricing_inverter = $('#system_pricing_inverter')
				.find(':selected')
				.attr('data-item');
			system_pricing_inverter = JSON.parse(system_pricing_inverter);
			if (
				system_pricing_inverter.inverter_efficiency != null &&
				system_pricing_inverter.inverter_efficiency != ''
			) {
				$('#pv_inverter_efficiency').val(
					parseFloat(system_pricing_inverter.inverter_efficiency)
				);
			} else {
				$('#pv_inverter_efficiency').val('');
			}
		} else {
			$('#pv_inverter_efficiency').val('');
		}
	});

	self.demand_charge_with_meter_data();
	$('#addDemanCharge').on('click', function (e) {
		e.preventDefault();
		if ($('#meter_upload_status').val() == 1) {
			self.demand_charge_without_meter_data();
		} else {
			self.demand_charge_with_meter_data();
		}
	});

	$('#rate').change(function () {
		var rate = $(this).val();
		if (rate == '1') {
			$('#rate1').removeClass('hidden');
			$('#rate2').addClass('hidden');
			$('#rate3').addClass('hidden');
			$('#rate4').addClass('hidden');
		} else if (rate == '2') {
			$('#rate1').removeClass('hidden');
			$('#rate2').removeClass('hidden');
			$('#rate3').addClass('hidden');
			$('#rate4').addClass('hidden');
		} else if (rate == '3') {
			$('#rate1').removeClass('hidden');
			$('#rate2').removeClass('hidden');
			$('#rate3').removeClass('hidden');
		} else if (rate == '4') {
			$('#rate1').removeClass('hidden');
			$('#rate2').removeClass('hidden');
			$('#rate3').removeClass('hidden');
			$('#rate4').removeClass('hidden');
		}
	});

	$('#custom_profile_id').change(function () {
		var profile_id = $(this).val();
		var thumb_ele = document.getElementById('solarCustomProfileIdThumb');
		if (profile_id) {
			thumb_ele.setAttribute('class', 'custom-profile__thumbnail');
			thumb_ele.setAttribute(
				'src',
				base_url + 'assets/images/ROI-graphics-' + profile_id + '.jpg'
			);
		} else {
			thumb_ele.setAttribute('class', 'hidden custom-profile__thumbnail');
		}
	});

	self.add_panel_area();

	$('#type_id').change(function () {
		self.fetch_solar_product_by_type();
	});

	$('#add_product_btn').click(function () {
		self.add_proposal_product();
	});

	$('#update_product_btn').click(function () {
		self.add_proposal_product();
	});

	$('#cancel_product_btn').click(function () {
		document.getElementById('solar_products_form').reset();
		$(this).addClass('hidden');
		$('#update_product_btn').addClass('hidden');
		$('#add_product_btn').removeClass('hidden');
		document.getElementById('solar_product_id').value = '';
	});

	$(document).on(
		'click',
		'.commerical-panel-tab .nav-item .nav-link',
		function (e) {
			var panelTabId = $(this).attr('data-id');
			self.commercialTabPanelId = panelTabId;
			console.log($('#pv_system_size').val());
			console.log(self.commercialTabPanelId);
			$('#CommercialPanelTab').val(panelTabId);

			// Setting Data for Tab 4
			if (panelTabId == 5) {
				$('#system_pricing_system_size').val(
					$('#pv_system_size').val()
				);
				$('#stc_calculate_system_size').val(
					parseFloat($('#pv_system_size').val())
				);
				self.calculate_stc_rebate(true);
			}
		}
	);

	$(document).on('click', '#save_proposal_btn', function (e) {
		if (self.commercialTabPanelId == 5) {
			if (self.validate_solar_inputs(self.commercialTabPanelId)) {
				self.calculate_payment_summary2(true);

				$('#stc_calculate_system_size').val(
					self.proposal_data.total_system_size
				);

				var monthly_payment_plan = $('#stc_monthly_payment_plan').val();

				var financial_selection = $('#financial_selection').val();
				if (financial_selection != 'VEEC') {
					if (self.isFinance != 0) {
						if (monthly_payment_plan != '0.00') {
							self.save_proposal_finance();
						} else {
							toastr.remove();
							toastr['error'](
								'Monthly payment plan is required.'
							);
							return false;
						}
					}
				}
				self.calculate_degration_factor_data(false);
				$('#view_proposal_action').show();
			}
		} else {
			self.save_bill_details();
		}
	});

	$(document).on('click', '#view_proposal_action', function (e) {
		self.view_proposal_action();
	});

	// $("#save_proposal_btn").click(function () {
	// 	self.save_solar_proposal(true);
	// });

	$('.pvwattsCalc').on('change', function (e) {
		e.preventDefault();
		self.save_solar_proposal(true);
	});

	$('#save_proposal_additional_btn').click(function () {
		self.save_solar_system_summary();
	});

	$('.additional_items').change(function () {
		self.save_solar_system_summary();
	});

	$('#price_before_stc_rebate').change(function () {
		self.calculate_payment_summary();
		self.save_solar_rebate();
	});

	$('.stc_calculate_inputs').change(function () {
		self.calculate_stc_rebate(true);
	});

	$('#save_stc_btn').click(function () {
		self.calculate_payment_summary(true);
	});

	$('#save_stc_btn2').click(function () {
		self.calculate_payment_summary2(true);
	});

	$('#manage_finance_proposal_btn').click(function () {
		$(this).hide();
		$('#solar_proposal_finance_container').slideDown();
	});

	$('#stc_term').change(function () {
		self.calculate_proposal_finance();
	});
	$('#lgc_term').change(function () {
		self.calculate_proposal_finance();
	});
	$('#term_veec_2').change(function () {
		self.calculate_proposal_finance(2);
	});
	$('#term_veec_3').change(function () {
		self.calculate_proposal_finance(3);
	});

	$('#save_proposal_finance_btn').click(function () {
		self.save_proposal_finance();
	});

	$('#delete_proposal_finance_btn').click(function () {
		self.delete_proposal_finance();
	});

	$('#tab_image_upload_btn').click(function () {
		$('#mapping_tool_actions').hide();
	});

	$('#reset-all').click(function () {
		calculateNow();
	});

	$('#is_meter_data').click(function () {
		$this = $(this);
		if ($(this).is(':checked')) {
			$('#no_of_working_days').parent().removeClass('hidden');
			$('#custom_profile_id').parent().removeClass('hidden');
			$('#is_meter_data_val').val(0);
		} else {
			$('#no_of_working_days').parent().addClass('hidden');
			$('#custom_profile_id').parent().addClass('hidden');
			$('#no_of_working_days').val('');
			$('#custom_profile_id').val('');
			$('#is_meter_data_val').val(1);
			self.handle_meter_data();
		}
	});
	$(document).on('change', '#nearmap_toggle', function () {
		if (this.checked) {
			$('#manual-panel-stats').hide();
			$('#inclusion-stats').addClass('col-md-12').removeClass('col-md-6');
			var val = 1;
		} else {
			$('#manual-panel-stats').show();
			$('#inclusion-stats').addClass('col-md-6').removeClass('col-md-12');
			var val = 0;
		}
		$('#nearmap_toggle_val').val(val);
	});
	$('#is_demand_charge').click(function () {
		$this = $(this);
		if ($(this).is(':checked')) {
			$('#demand_charge_container').addClass('hidden');
			$('#is_demand_charge_val').val(0);
		} else {
			$('#demand_charge_container').removeClass('hidden');
			$('#is_demand_charge_val').val(1);
		}
	});

	$('.addRemoveExtraItem').click(function () {
		var extraItemRow = $('.extra-item-row').length;
		var extraItemAction = $(this).attr('data-action');

		var extraItemTemplate = `<div class="form-row mb-4 extra-item-row extra-item-count-${
			extraItemRow + 1
		}">
        <label for="" class="col-sm-2 col-md-2 form-label-bold">Extra Item</label>
        <div class="col-md-8 pl-5">
            <select name="sys_battery" class="form-control" required="" id="meter_upload_status">
                <option value="">Select Tile</option>
                <option value="1">No Meter Data</option>
                <option value="2">Upload Meter Data</option>
                <option value="3">Load consumption profiles</option>
            </select>
        </div>
        <div class="col-sm-2">
            <input type="number" class="form-control" id="">
        </div>
        </div>`;

		if (extraItemAction === 'add') {
			$('#addExtraItemBody ').append(extraItemTemplate);
		} else {
			$('#addExtraItemBody .extra-item-count-' + extraItemRow).remove();
		}

		if ($('.extra-item-row').length > 1) {
			$(".addRemoveExtraItem[data-action='remove']").show();
		} else {
			$(".addRemoveExtraItem[data-action='remove']").hide();
		}
	});

	$('.addRemoveAdditonalCost').click(function () {
		var extraItemRow = $('.additonal-item-row').length;
		var extraItemAction = $(this).attr('data-action');

		var extraItemTemplate = `<tr class="additonal-item-row additional-cost-count-${
			extraItemRow + 1
		}">
        <td>
            <input type="text">
        </td>
        <td>
            <input type="text">
        </td>
        <td>
            <input type="text">
        </td>
        </tr>`;

		if (extraItemAction === 'add') {
			$('#additional-table ').append(extraItemTemplate);
		} else {
			$(
				'#additional-table .additional-cost-count-' + extraItemRow
			).remove();
		}

		if ($('.additonal-item-row').length > 1) {
			$(".addRemoveAdditonalCost[data-action='remove']").show();
		} else {
			$(".addRemoveAdditonalCost[data-action='remove']").hide();
		}
	});

	$('#demand_charge,#demand_charge_kVA').change(function () {
		var dc = $('#demand_charge').val();
		var demand_charge_kVA = $('#demand_charge_kVA').val();
		if (!isNaN(dc) && !isNaN(demand_charge_kVA)) {
			var tc = dc * demand_charge_kVA;
			$('#total_demand_charge').val(tc);
		}
	});

	$(document.body).delegate('.no_of_panels', 'change', function () {
		var total_panels_count = 0;
		var panels_count = document.getElementsByClassName('no_of_panels');
		for (var i = 1; i < panels_count.length; i++) {
			total_panels_count =
				parseInt(total_panels_count) + parseInt(panels_count[i].value);
		}
		self.total_panels_by_area = total_panels_count;
	});

	$('.rate_usage').change(function () {
		var total_usage = 0;
		var usage = document.getElementsByClassName('rate_usage');
		for (var i = 0; i < usage.length; i++) {
			if (!isNaN(parseInt(usage[i].value))) {
				total_usage = parseInt(total_usage) + parseInt(usage[i].value);
			}
		}
		document.getElementById('monthly_electricity_usage').value =
			total_usage;
	});

	$('.rate_days_of_week').change(function () {
		$this = $(this);
		if ($this.val() == 'flat_rate') {
			$this
				.parent()
				.parent()
				.prev('div')
				.prev('div')
				.find('.rate_st')
				.val('00:00');
			$this.parent().parent().prev('div').find('.rate_ft').val('23:59');
		}
	});

	$('#download_solar_proposal').click(function () {
		if ($('#nearmap_toggle_val').val() == 1) {
			var flag = self.validate_solar_proposal_pdf_data();
			if (!flag) {
				toastr.remove();
				toastr['error'](
					'Proposal cannot be generated some required data is missing.'
				);
				return false;
			} else {
				self.validate_proposal_data();
			}
		} else {
			var flag = self.validate_solar_proposal_pdf_data();
			if (self.total_panels_by_area != self.total_panels) {
				toastr.remove();
				toastr['error'](
					'No of panels in panel area does not match the panel count by product.'
				);
				return false;
			} else if (!flag) {
				toastr.remove();
				toastr['error'](
					'Proposal cannot be generated some required data is missing.'
				);
				return false;
			} else {
				self.validate_proposal_data();
			}
		}
	});

	$('#total_payable_exGST').change(function () {
		var total_payable_exGST = $(this).val();
		var stc_rebate_value = $('#stc_rebate_value').val();

		// $("#system_pricing_stc").val(stc_rebate_value);
		// $("#system_pricing_sub_total").val();
		if (total_payable_exGST) {
			var system_subtotal = total_payable_exGST - stc_rebate_value;
			$('#system_pricing_sub_total').val(system_subtotal);
			$('#system_pricing_net_payable').val(system_subtotal);
			$('#price_before_stc_rebate').val(system_subtotal);
			$('#price_before_stc_rebate_inGST').val(system_subtotal * self.gst);
			$('#total_payable_inGST').val(total_payable_exGST * self.gst);

			// STC
			$('#stc_upfront_payment_opt_price_1').val(
				(system_subtotal * $('#stc_upfront_payment_opt_per_1').val()) /
					100
			);
			$('#stc_upfront_payment_opt_price_2').val(
				(system_subtotal * $('#stc_upfront_payment_opt_per_2').val()) /
					100
			);
			$('#stc_upfront_payment_opt_price_3').val(
				(system_subtotal * $('#stc_upfront_payment_opt_per_3').val()) /
					100
			);

			var stc_upfront_payment_opt_price_total =
				($('#price_before_stc_rebate').val() *
					$('#stc_upfront_payment_opt_per_1').val()) /
					100 +
				($('#price_before_stc_rebate').val() *
					$('#stc_upfront_payment_opt_per_2').val()) /
					100 +
				($('#price_before_stc_rebate').val() *
					$('#stc_upfront_payment_opt_per_3').val()) /
					100;

			$('#stc_upfront_payment_opt_price_total').val(
				parseFloat(stc_upfront_payment_opt_price_total).toFixed(2)
			);

			if ($('#stc_term').val() != '') {
				self.calculate_proposal_finance();
			}
		}
	});

	$('.stc_upfront_payment_deposit').change(function () {
		// STC
		$('#stc_upfront_payment_opt_price_1').val(
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_1').val()) /
				100
		);
		$('#stc_upfront_payment_opt_price_2').val(
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_2').val()) /
				100
		);
		$('#stc_upfront_payment_opt_price_3').val(
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_3').val()) /
				100
		);

		var stc_upfront_payment_opt_price_total =
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_1').val()) /
				100 +
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_2').val()) /
				100 +
			($('#price_before_stc_rebate').val() *
				$('#stc_upfront_payment_opt_per_3').val()) /
				100;

		$('#stc_upfront_payment_opt_price_total').val(
			parseFloat(stc_upfront_payment_opt_price_total).toFixed(2)
		);
	});

	$('.lgc_upfront_payment_deposit').change(function () {
		// STC
		$('#lgc_upfront_payment_opt_price_1').val(
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_1').val()) /
				100
		);
		$('#lgc_upfront_payment_opt_price_2').val(
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_2').val()) /
				100
		);
		$('#lgc_upfront_payment_opt_price_3').val(
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_3').val()) /
				100
		);

		var lgc_upfront_payment_opt_price_total =
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_1').val()) /
				100 +
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_2').val()) /
				100 +
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_3').val()) /
				100;

		$('#lgc_upfront_payment_opt_price_total').val(
			parseFloat(lgc_upfront_payment_opt_price_total).toFixed(2)
		);
	});

	$('#price_before_stc_rebate2').change(function () {
		var price_before_stc_rebate2 = $('#price_before_stc_rebate2').val();
		$('#price_before_stc_rebate_inGST2').val(
			parseFloat(price_before_stc_rebate2 * self.gst).toFixed(2)
		);

		// STC
		$('#lgc_upfront_payment_opt_price_1').val(
			(price_before_stc_rebate2 *
				$('#lgc_upfront_payment_opt_per_1').val()) /
				100
		);
		$('#lgc_upfront_payment_opt_price_2').val(
			(price_before_stc_rebate2 *
				$('#lgc_upfront_payment_opt_per_2').val()) /
				100
		);
		$('#lgc_upfront_payment_opt_price_3').val(
			(price_before_stc_rebate2 *
				$('#lgc_upfront_payment_opt_per_3').val()) /
				100
		);

		var lgc_upfront_payment_opt_price_total =
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_1').val()) /
				100 +
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_2').val()) /
				100 +
			($('#price_before_stc_rebate2').val() *
				$('#lgc_upfront_payment_opt_per_3').val()) /
				100;

		$('#lgc_upfront_payment_opt_price_total').val(
			parseFloat(lgc_upfront_payment_opt_price_total).toFixed(2)
		);

		if ($('#lgc_term').val() != '') {
			self.calculate_proposal_finance();
		}
	});
	$('#project_cost_2').change(function () {
		var project_cost_2 = $('#project_cost_2').val();
		$('#project_cost_gst_2').val(
			parseFloat(project_cost_2 * self.gst).toFixed(2)
		);
		// STC
		$('#veec_upfront_payment_opt_price_2_1').val(
			(project_cost_2 * $('#veec_upfront_payment_opt_per_2_1').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_2_2').val(
			(project_cost_2 * $('#veec_upfront_payment_opt_per_2_2').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_2_3').val(
			(project_cost_2 * $('#veec_upfront_payment_opt_per_2_3').val()) /
				100
		);

		var veec_upfront_payment_opt_price_total_2 =
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_1').val()) /
				100 +
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_2').val()) /
				100 +
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_3').val()) /
				100;

		$('#veec_upfront_payment_opt_price_total_2').val(
			parseFloat(veec_upfront_payment_opt_price_total_2).toFixed(2)
		);

		if ($('#term_veec_2').val() != '') {
			self.calculate_proposal_finance(2);
		}
	});

	$('.veec_upfront_payment_deposit_2').change(function () {
		$('#veec_upfront_payment_opt_price_2_1').val(
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_1').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_2_2').val(
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_2').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_2_3').val(
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_3').val()) /
				100
		);

		var veec_upfront_payment_opt_price_total_2 =
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_1').val()) /
				100 +
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_2').val()) /
				100 +
			($('#project_cost_2').val() *
				$('#veec_upfront_payment_opt_per_2_3').val()) /
				100;

		$('#veec_upfront_payment_opt_price_total_2').val(
			parseFloat(veec_upfront_payment_opt_price_total_2).toFixed(2)
		);
	});
	$('#project_cost_3').change(function () {
		var project_cost_3 = $('#project_cost_3').val();
		$('#project_cost_gst_3').val(
			parseFloat(project_cost_3 * self.gst).toFixed(2)
		);

		$('#veec_upfront_payment_opt_price_3_1').val(
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_1').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_3_2').val(
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_2').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_3_3').val(
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_3').val()) /
				100
		);

		var veec_upfront_payment_opt_price_total_3 =
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_1').val()) /
				100 +
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_2').val()) /
				100 +
			(project_cost_3 * $('#veec_upfront_payment_opt_per_3_3').val()) /
				100;

		$('#veec_upfront_payment_opt_price_total_3').val(
			parseFloat(veec_upfront_payment_opt_price_total_3).toFixed(2)
		);

		if ($('#term_veec_3').val() != '') {
			self.calculate_proposal_finance(3);
		}
	});

	$('.veec_upfront_payment_deposit_3').change(function () {
		$('#veec_upfront_payment_opt_price_3_1').val(
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_1').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_3_2').val(
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_2').val()) /
				100
		);
		$('#veec_upfront_payment_opt_price_3_3').val(
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_3').val()) /
				100
		);

		var veec_upfront_payment_opt_price_total_3 =
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_1').val()) /
				100 +
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_2').val()) /
				100 +
			($('#project_cost_3').val() *
				$('#veec_upfront_payment_opt_per_3_3').val()) /
				100;

		$('#veec_upfront_payment_opt_price_total_3').val(
			parseFloat(veec_upfront_payment_opt_price_total_3).toFixed(2)
		);
	});

	/**$('#stc').click(function () {
     var dt = new Date();
     $('#stc_calculation_modal').modal('show');
     $('#stc_calculate_system_size').val(self.total_system_size);
     $('#stc_calculate_postcode').val(self.lead_data.postcode);
     $('#stc_calculate_year').val(dt.getFullYear());
     });*/

	$('#stc_rebate_value').click(function () {
		var dt = new Date();
		$('#stc_calculation_modal').modal('show');

		// if (
		// 	$('#pv_system_size').val() != null &&
		// 	$('#pv_system_size').val() != '0.00' &&
		// 	$('#pv_system_size').val() != '0'
		// ) {
		// 	$('#stc_calculate_system_size').val($('#pv_system_size').val());
		// } else {
		// 	$('#stc_calculate_system_size').val(
		// 		self.proposal_data.total_system_size
		// 	);
		// }

		$('#stc_calculate_postcode').val(self.lead_data.postcode);
		$('#stc_calculate_year').val(dt.getFullYear());
	});
	$('#veec_discount').click(function () {
		self.calculate_veec_discount(true);
	});
	// $("#system_pricing_stc").click(function () {
	// 	var dt = new Date();
	// 	$("#stc_calculation_modal").modal("show");
	// 	$("#stc_calculate_system_size").val(self.proposal_data.total_system_size);
	// 	$("#stc_calculate_postcode").val(self.lead_data.postcode);
	// 	$("#stc_calculate_year").val(dt.getFullYear());
	// });
	if (
		self.financial_summary_data != '' &&
		self.financial_summary_data != null
	) {
		// console.log(self.financial_summary_data);
		self.financial_summary_type = self.financial_summary_data.type;

		$('#financial_selection').val(self.financial_summary_type).change();

		if (self.financial_summary_data.type == 'STC') {
			$('#ppa_term').val(self.financial_summary_data.ppa_term);
			$('#ppa_rate').val(self.financial_summary_data.ppa_rate);

			if (self.financial_summary_data.ppa_rate != null) {
				$('#view_proposal_action').show();
			}
		}
		if (self.financial_summary_data.type == 'LGC') {
			$('#lgc_ppa_term').val(self.financial_summary_data.ppa_term);
			$('#lgc_ppa_rate').val(self.financial_summary_data.ppa_rate);
			if (self.financial_summary_data.ppa_rate != null) {
				$('#view_proposal_action').show();
			}

			for (
				var i = 0;
				i < self.financial_summary_data.lgc_pricing.length;
				i++
			) {
				$('.lgc_pricing:eq(' + i + ')').val(
					self.financial_summary_data.lgc_pricing[i]
				);
			}
		}
		if (self.financial_summary_data.type == 'VEEC') {
			console.log(self.financial_summary_data);
			if (self.financial_summary_data.project_cost) {
				$('#project_cost_2').val(
					parseFloat(
						self.financial_summary_data.project_cost[0]
					).toFixed(2)
				);
				$('#project_cost_gst_2').val(
					parseFloat(
						self.financial_summary_data.project_cost[0] * self.gst
					).toFixed(2)
				);

				if (
					self.financial_summary_data.upfront_payment_deposit != null
				) {
					$('#veec_upfront_payment_opt_per_2_1').val(
						self.financial_summary_data
							.upfront_payment_deposit[0][0]
					);
					$('#veec_upfront_payment_opt_per_2_2').val(
						self.financial_summary_data
							.upfront_payment_deposit[1][0]
					);
					$('#veec_upfront_payment_opt_per_2_3').val(
						self.financial_summary_data
							.upfront_payment_deposit[2][0]
					);

					$('#veec_upfront_payment_opt_price_2_1').val(
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[0][0]) /
							100
					);
					$('#veec_upfront_payment_opt_price_2_2').val(
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[1][0]) /
							100
					);
					$('#veec_upfront_payment_opt_price_2_3').val(
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[2][0]) /
							100
					);

					var veec_upfront_payment_opt_price_total_2 =
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[0][0]) /
							100 +
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[1][0]) /
							100 +
						(self.financial_summary_data.project_cost[0] *
							self.financial_summary_data
								.upfront_payment_deposit[2][0]) /
							100;

					$('#veec_upfront_payment_opt_price_total_2').val(
						parseFloat(
							veec_upfront_payment_opt_price_total_2
						).toFixed(2)
					);

					$('#project_cost_3').val(
						parseFloat(
							self.financial_summary_data.project_cost[1]
						).toFixed(2)
					);
					$('#project_cost_gst_3').val(
						parseFloat(
							self.financial_summary_data.project_cost[1] *
								self.gst
						).toFixed(2)
					);
					$('#term_veec_3').val(
						parseFloat(self.financial_summary_data.term[1])
					);

					$('#veec_upfront_payment_opt_per_3_1').val(
						self.financial_summary_data
							.upfront_payment_deposit[0][0]
					);
					$('#veec_upfront_payment_opt_per_3_2').val(
						self.financial_summary_data
							.upfront_payment_deposit[1][0]
					);
					$('#veec_upfront_payment_opt_per_3_3').val(
						self.financial_summary_data
							.upfront_payment_deposit[2][0]
					);

					$('#veec_upfront_payment_opt_price_3_1').val(
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data.upfront_payment_deposit[
								'3'
							][0]) /
							100
					);
					$('#veec_upfront_payment_opt_price_3_2').val(
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data
								.upfront_payment_deposit[4][0]) /
							100
					);
					$('#veec_upfront_payment_opt_price_3_3').val(
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data
								.upfront_payment_deposit[5][0]) /
							100
					);

					var veec_upfront_payment_opt_price_total_3 =
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data
								.upfront_payment_deposit[3][0]) /
							100 +
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data
								.upfront_payment_deposit[4][0]) /
							100 +
						(self.financial_summary_data.project_cost[1] *
							self.financial_summary_data
								.upfront_payment_deposit[5][0]) /
							100;

					$('#veec_upfront_payment_opt_price_total_3').val(
						parseFloat(
							veec_upfront_payment_opt_price_total_3
						).toFixed(2)
					);
				}
			}
			if (self.financial_summary_data.ppa_term) {
				$('#ppa_term_2').val(self.financial_summary_data.ppa_term[0]);
				$('#ppa_term_3').val(self.financial_summary_data.ppa_term[1]);
			}
			if (self.financial_summary_data.ppa_rate) {
				$('#ppa_rate_2').val(self.financial_summary_data.ppa_rate[0]);
				$('#ppa_rate_3').val(self.financial_summary_data.ppa_rate[1]);

				if (self.financial_summary_data.ppa_rate != null) {
					$('#view_proposal_action').show();
				}
			}
			if (self.financial_summary_data.term) {
				$('#term_veec_2').val(self.financial_summary_data.term[0]);
				$('#term_veec_3').val(self.financial_summary_data.term[1]);
			}
			if (self.financial_summary_data.monthly_payment_plan) {
				$('#monthly_payment_plan_2').val(
					self.financial_summary_data.monthly_payment_plan[0]
				);
				$('#monthly_payment_plan_3').val(
					self.financial_summary_data.monthly_payment_plan[1]
				);
			}
			if (self.financial_summary_data.upfront_payment_options) {
				$('#upfront_payment_options_2').val(
					self.financial_summary_data.upfront_payment_options[0]
				);
				$('#upfront_payment_options_3').val(
					self.financial_summary_data.upfront_payment_options[1]
				);
			}
			$('#veec_accuracy_factor').val(
				self.financial_summary_data.accuracy_factor
			);
			$('#veer_decay_factor').val(
				self.financial_summary_data.decay_factor
			);
			$('#veec_regional_factor').val(
				self.financial_summary_data.regional_factor
			);
			if (self.financial_summary_data.veec_price !== null) {
				$('#veec_price').val(self.financial_summary_data.veec_price);
			}

			self.calculate_veec_discount(false);

			if (self.financial_summary_data.degradation_data !== null) {
				$('#view_proposal_action').show();
			} else {
				$('#view_proposal_action').hide();
			}
		}
	} else {
		$('#financial_selection').val('STC').change();
	}

	if (self.lead_uuid != '') {
		var data = {};
		data.uuid = self.lead_uuid;
		data.site_id = self.site_id;
		self.fetch_lead_data(data);

		//Populdate Proposal Data
		for (var key in self.proposal_data) {
			if (key === 'additional_items') {
				var additional_items = self.proposal_data[key];
				for (var item_key in additional_items) {
					$('#' + item_key).val(additional_items[item_key]);
				}
			} else if (key === 'image') {
				if (
					self.proposal_data[key] != '' &&
					self.proposal_data[key] != null
				) {
					$('#image').val(self.proposal_image);
				}
			} else if (key === 'panel_layout_image') {
				if (
					self.proposal_data[key] != '' &&
					self.proposal_data[key] != null
				) {
					self.proposal_image = self.proposal_data[key];
					self.show_image();
				}
			} else if (key == 'total_payable_exGST') {
				$('#total_payable_exGST').val(
					parseFloat(self.proposal_data[key]).toFixed(2)
				);
				$('#total_payable_inGST').val(
					parseFloat(self.proposal_data[key] * self.gst).toFixed(2)
				);
				$('#system_pricing_project_price').val(
					parseFloat(self.proposal_data[key] * self.gst).toFixed(2)
				);
			} else if (key == 'stc_rebate_value') {
				$('#stc_rebate_value').val(self.proposal_data[key]);
				$('#system_pricing_stc').val(self.proposal_data[key]);
				$('#stc_rebate_value_inGST').val(
					parseFloat(self.proposal_data[key] * self.gst).toFixed(2)
				);
			} else if (key == 'price_before_stc_rebate') {
				var PriceBeforeSTCRebate = (
					parseFloat(self.proposal_data[key]) +
					Number($('#stc_rebate_value').val())
				).toFixed(2);
				$('#system_pricing_net_payable').val(PriceBeforeSTCRebate);
				$('#system_pricing_sub_total').val(PriceBeforeSTCRebate);

				if (self.financial_summary_type == 'STC') {
					$('#price_before_stc_rebate').val(PriceBeforeSTCRebate);
					$('#price_before_stc_rebate_inGST').val(
						parseFloat(PriceBeforeSTCRebate * self.gst).toFixed(2)
					);

					if (
						self.financial_summary_data.upfront_payment_deposit !=
						null
					) {
						$('#stc_upfront_payment_opt_per_1').val(
							self.financial_summary_data
								.upfront_payment_deposit[0][0]
						);
						$('#stc_upfront_payment_opt_per_2').val(
							self.financial_summary_data
								.upfront_payment_deposit[1][0]
						);
						$('#stc_upfront_payment_opt_per_3').val(
							self.financial_summary_data
								.upfront_payment_deposit[2][0]
						);

						// STC
						$('#stc_upfront_payment_opt_price_1').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[0][0]) /
								100
						);
						$('#stc_upfront_payment_opt_price_2').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[1][0]) /
								100
						);
						$('#stc_upfront_payment_opt_price_3').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[2][0]) /
								100
						);

						var stc_upfront_payment_opt_price_total =
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[0][0]) /
								100 +
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[1][0]) /
								100 +
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[2][0]) /
								100;

						$('#stc_upfront_payment_opt_price_total').val(
							parseFloat(
								stc_upfront_payment_opt_price_total
							).toFixed(2)
						);
					}
				}

				if (
					self.financial_summary_data.upfront_payment_deposit != null
				) {
					if (self.financial_summary_type == 'LGC') {
						$('#price_before_stc_rebate2').val(
							PriceBeforeSTCRebate
						);
						$('#price_before_stc_rebate_inGST2').val(
							parseFloat(PriceBeforeSTCRebate * self.gst).toFixed(
								2
							)
						);

						$('#lgc_upfront_payment_opt_per_1').val(
							self.financial_summary_data
								.upfront_payment_deposit[0][0]
						);
						$('#lgc_upfront_payment_opt_per_2').val(
							self.financial_summary_data
								.upfront_payment_deposit[1][0]
						);
						$('#lgc_upfront_payment_opt_per_3').val(
							self.financial_summary_data
								.upfront_payment_deposit[2][0]
						);

						// STC
						$('#lgc_upfront_payment_opt_price_1').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[0][0]) /
								100
						);
						$('#lgc_upfront_payment_opt_price_2').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[1][0]) /
								100
						);
						$('#lgc_upfront_payment_opt_price_3').val(
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[2][0]) /
								100
						);

						var lgc_upfront_payment_opt_price_total =
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[0][0]) /
								100 +
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[1][0]) /
								100 +
							(PriceBeforeSTCRebate *
								self.financial_summary_data
									.upfront_payment_deposit[2][0]) /
								100;

						$('#lgc_upfront_payment_opt_price_total').val(
							parseFloat(
								lgc_upfront_payment_opt_price_total
							).toFixed(2)
						);
					}
				}
			} else if (key === 'rate1_data') {
				for (var key1 in self.proposal_data['rate1_data']) {
					$("[name='solar_proposal[rate1_data][" + key1 + "]']")
						.val(self.proposal_data['rate1_data'][key1])
						.trigger('change');
				}
			} else if (key === 'rate2_data') {
				for (var key1 in self.proposal_data['rate2_data']) {
					$("[name='solar_proposal[rate2_data][" + key1 + "]']")
						.val(self.proposal_data['rate2_data'][key1])
						.trigger('change');
				}
			} else if (key === 'rate3_data') {
				for (var key1 in self.proposal_data['rate3_data']) {
					$("[name='solar_proposal[rate3_data][" + key1 + "]']")
						.val(self.proposal_data['rate3_data'][key1])
						.trigger('change');
				}
			} else if (key === 'rate4_data') {
				for (var key1 in self.proposal_data['rate4_data']) {
					$("[name='solar_proposal[rate4_data][" + key1 + "]']")
						.val(self.proposal_data['rate4_data'][key1])
						.trigger('change');
				}
			} else if (key === 'is_meter_data') {
				$("[name='solar_proposal[" + key + "]']")
					.val(self.proposal_data[key])
					.trigger('change');
				if (self.proposal_data['is_meter_data'] == '1') {
					$('#is_meter_data').click();
					if (meter_data_window) {
						meter_data_window.close();
					}
				}
			} else if (key === 'is_demand_charge') {
				$("[name='solar_proposal[" + key + "]']")
					.val(self.proposal_data[key])
					.trigger('change');
				if (self.proposal_data['is_demand_charge'] == '1') {
					$('#is_demand_charge').click();
				}
			} else if (key === 'nearmap_toggle') {
				if (parseInt(self.proposal_data[key])) {
					$('#nearmap_toggle').prop('checked', 'checked');
					$('#manual-panel-stats').hide();
					$('#inclusion-stats')
						.addClass('col-md-12')
						.removeClass('col-md-6');
				} else {
					$('#manual-panel-stats').show();
					$('#inclusion-stats')
						.addClass('col-md-6')
						.removeClass('col-md-12');
				}
				$("[name='solar_proposal[" + key + "]']").val(
					self.proposal_data[key]
				);
			} else if (key === 'near_map_data') {
				$("[name='solar_proposal[" + key + "]']").val(
					JSON.stringify(self.proposal_data[key])
				);
			} else if (key === 'pvwatts_data') {
				$("[name='solar_proposal[" + key + "]']").val(
					JSON.stringify(self.proposal_data[key])
				);
			} else {
				if (key == 'no_of_working_days' || key == 'custom_profile_id') {
					if (
						self.proposal_data[key] != '' &&
						self.proposal_data[key] != '0' &&
						self.proposal_data[key] != null
					) {
						$("[name='solar_proposal[" + key + "]']")
							.val(self.proposal_data[key])
							.trigger('change');
					}
				} else {
					if (
						self.proposal_data[key] != '' &&
						self.proposal_data[key] != null
					) {
						$("[name='solar_proposal[" + key + "]']")
							.val(self.proposal_data[key])
							.trigger('change');
					}
				}
			}
		}

		//Set Price Assumption
		var no_of_stc = $('#stc').val();
		no_of_stc = parseFloat(no_of_stc).toFixed(2);

		var stc_rebate_value = $('#stc_rebate_value').val();
		stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

		var price_assumption = stc_rebate_value / no_of_stc;
		price_assumption = parseFloat(price_assumption).toFixed(2);

		if (!isNaN(price_assumption)) {
			$('#stc_calculate_price').val(price_assumption);
		} else {
			$('#stc_calculate_price').val(37);
		}

		if (self.proposal_data.total_system_size != 0) {
			self.calculate_stc_rebate(true);
		}

		$('#rate').trigger('change');

		//Populdate Finance Details

		if (self.financial_summary_type == 'STC') {
			$('#stc_term').val(self.proposal_data.term);
			$('#stc_monthly_payment_plan').val(
				self.proposal_data.monthly_payment_plan
			);
			if (self.proposal_data.term != null) {
				$('#delete_proposal_finance_btn').removeClass('hidden');
			}
			if (self.proposal_data.monthly_payment_plan == null) {
				// $('#proposal_finance_form').hide();
				$('.isFinanceToggle').parent().removeClass('active');
				$(".isFinanceToggle[value='1']").removeAttr('checked');
				$(".isFinanceToggle[value='0']").attr('checked', 'checked');
				$(".isFinanceToggle[value='0']").parent().addClass('active');
			}
		}
		if (self.financial_summary_type == 'LGC') {
			$('#lgc_term').val(self.proposal_data.term);
			$('#lgc_monthly_payment_plan').val(
				self.proposal_data.monthly_payment_plan
			);
		}

		//Populdate Proposal Data
		self.solar_panel_area_count = self.panel_data.length;
		for (var i = 0; i < self.panel_data.length; i++) {
			if (i > 0) {
				$('#add_panel_area').click();
			}
			$("[name='solar_panel_data[no_of_panels][" + i + "]']").val(
				parseInt(self.panel_data[i]['no_of_panels'])
			);
			$("[name='solar_panel_data[panel_orientation][" + i + "]']").val(
				parseInt(self.panel_data[i]['panel_orientation'])
			);
			$("[name='solar_panel_data[panel_tilt][" + i + "]']").val(
				parseInt(self.panel_data[i]['panel_tilt'])
			);
		}
		self.fetch_solar_proposal_details();
	}

	self.fetch_proposal_products();
	self.upload_proposal_image();
	self.show_image();
	self.hide_image();
	self.edit_customer_location();
	self.save_customer_location();
	self.save_customer_postcode();
	self.save_customer();
	// self.fetchFinancialSummary();
};

// solar_proposal_manager.prototype.validate_solar_inputs = function (step) {
//     var self = this;

// }

solar_proposal_manager.prototype.validate_solar_inputs = function (step) {
	var self = this;

	if (step == 1) {
		var uploadProductionToggle = $('.uploadProductionToggle:checked').val();
		if (
			$('#productionDataFileURL')[0].files.length == 0 &&
			uploadProductionToggle == 1 &&
			self.isProductionFile == 0
		) {
			toastr.remove();
			toastr['error']('Production file is missing.');
			return false;
		} else if ($('#system_pricing_panel').val() == '') {
			toastr.remove();
			toastr['error']('Panel is missing.');
			return false;
		} else if ($('#system_pricing_inverter').val() == '') {
			toastr.remove();
			toastr['error']('Inverter is missing.');
			return false;
		}
		// else if ($('#system_pricing_battery').val() == '') {
		// 	toastr.remove();
		// 	toastr['error']('Battery is missing.');
		// 	return false;
		// }
		else if (
			$('#pv_system_size').val() == '0.00' ||
			$('#pv_system_size').val() == '0'
		) {
			toastr.remove();
			toastr['error']('System Size is missing.');
			return false;
		} else if (
			$('#pv_inverter_efficiency').val() == '0.00' ||
			$('#pv_inverter_efficiency').val() == ''
		) {
			toastr.remove();
			toastr['error'](
				'Inverter Efficiency Missing, Please contact Admin.'
			);
			return false;
		} else if (
			$('#pv_degradation_factor').val() == '0.00' ||
			$('#pv_degradation_factor').val() == ''
		) {
			toastr.remove();
			toastr['error'](
				'Degradation Factor for panel is missing, Please contact Admin.'
			);
			return false;
		} else {
			return true;
		}
	}

	if (step == 2) {
		if (
			$('#meter_data_supply_charge').val() == '' ||
			$('#meter_data_meter_charge').val() == ''
		) {
			toastr.remove();
			toastr['error']('Standard charges is missing.');
			return false;
		} else if (
			$('#meter_data_pia_percentage').val() == '' ||
			$('#meter_data_subsequent').val() == ''
		) {
			toastr.remove();
			toastr['error']('Price annual increase data is missing.');
			return false;
		} else if ($('#utility_value_export').val() == '') {
			toastr.remove();
			toastr['error']('Export rate required data is missing.');
			return false;
		} else {
			return true;
		}
	}

	if (step == 5) {
		var financialType = $('#financial_selection').val();

		if (financialType === 'STC') {
			if (
				$('#total_payable_exGST').val() == '0.00' ||
				$('#total_payable_exGST').val() == ''
			) {
				toastr.remove();
				toastr['error']('Total Price is missing.');
				return false;
			} else if (
				$('#price_before_stc_rebate').val() == '0.00' ||
				$('#price_before_stc_rebate').val() == ''
			) {
				toastr.remove();
				toastr['error']('Project Cost is missing.');
				return false;
			} else if (
				$('#stc_term').val() == '' ||
				$('#stc_monthly_payment_plan').val() == ''
			) {
				toastr.remove();
				toastr['error']('Finance data is missing.');
				return false;
			} else if (
				$('#ppa_term').val() == '' ||
				$('#ppa_rate').val() == ''
			) {
				toastr.remove();
				toastr['error']('PPA data is missing.');
				return false;
			} else {
				return true;
			}
		}

		if (financialType === 'LGC') {
			if (
				$('#price_before_stc_rebate2').val() == '0.00' ||
				$('#price_before_stc_rebate2').val() == ''
			) {
				toastr.remove();
				toastr['error']('Project Cost is missing.');
				return false;
			} else if (
				$('.lgc_pricing:eq(1)').val() == '' ||
				$('.lgc_pricing:eq(2)').val() == '' ||
				$('.lgc_pricing:eq(3)').val() == '' ||
				$('.lgc_pricing:eq(4)').val() == '' ||
				$('.lgc_pricing:eq(5)').val() == '' ||
				$('.lgc_pricing:eq(6)').val() == '' ||
				$('.lgc_pricing:eq(7)').val() == '' ||
				$('.lgc_pricing:eq(8)').val() == ''
			) {
				toastr.remove();
				toastr['error']('LGC pricing data is missing.');
				return false;
			} else if (
				$('#lgc_term').val() == '' ||
				$('#lgc_monthly_payment_plan').val() == ''
			) {
				toastr.remove();
				toastr['error']('Finance data is missing.');
				return false;
			} else if (
				$('#lgc_ppa_term').val() == '' ||
				$('#lgc_ppa_rate').val() == ''
			) {
				toastr.remove();
				toastr['error']('PPA data is missing.');
				return false;
			} else {
				return true;
			}
		}

		if (financialType === 'VEEC') {
			if (
				$('#project_cost_2').val() == '0.00' ||
				$('#project_cost_2').val() == ''
			) {
				toastr.remove();
				toastr['error']('UPFRONT DISCOUNT Project Cost is missing.');
				return false;
			} else if (
				$('#term_veec_2').val() == '' ||
				$('#monthly_payment_plan_2').val() == ''
			) {
				toastr.remove();
				toastr['error']('UPFRONT DISCOUNT Finance data is missing.');
				return false;
			} else if (
				$('#ppa_term_2').val() == '' ||
				$('#ppa_rate_2').val() == ''
			) {
				toastr.remove();
				toastr['error']('UPFRONT DISCOUNT PPA data is missing.');
				return false;
			} else if (
				$('#project_cost_3').val() == '0.00' ||
				$('#project_cost_3').val() == ''
			) {
				toastr.remove();
				toastr['error']('CLIENT REBATE Project Cost is missing.');
				return false;
			} else if (
				$('#term_veec_3').val() == '' ||
				$('#monthly_payment_plan_3').val() == ''
			) {
				toastr.remove();
				toastr['error']('CLIENT REBATE Finance data is missing.');
				return false;
			} else if (
				$('#ppa_term_3').val() == '' ||
				$('#ppa_rate_3').val() == ''
			) {
				toastr.remove();
				toastr['error']('CLIENT REBATE PPA data is missing.');
				return false;
			} else {
				return true;
			}
		}
	}

	// var url =
	// 	base_url +
	// 	'admin/commercial/validate_proposal_data/' +
	// 	self.proposal_data.id;
	// // 	'?view=html';

	// $.ajax({
	// 	url: url,
	// 	type: 'post',
	// 	dataType: 'json',
	// 	success: function (stat) {
	// 		console.log(stat);
	// 		if (stat.success == true) {
	// 		} else {
	// 			toastr['error'](stat.status);
	// 		}
	// 	},
	// 	error: function (xhr, ajaxOptions, thrownError) {
	// 		toastr['error'](
	// 			thrownError +
	// 				'\r\n' +
	// 				xhr.statusText +
	// 				'\r\n' +
	// 				xhr.responseText
	// 		);
	// 	},
	// });
};

solar_proposal_manager.prototype.validate_proposal_data = function () {
	var self = this;
	var url =
		base_url +
		'admin/proposal/validate_proposal_data/' +
		self.proposal_data.id +
		'?view=html';

	var link = document.createElement('a');
	link.href = url;
	link.target = '_blank';
	document.body.appendChild(link);
	link.click();
	link.remove();

	/**$.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            success: function (stat) {
                if (stat.success == true) {
                    var link = document.createElement('a');
                    link.href = url;
                    link.target = '_blank';
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                } else {
                    toastr["error"](stat.status);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr["error"](thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });*/
};

solar_proposal_manager.prototype.fetchFinancialSummary = function () {
	document
		.getElementById('proposal_financial_summery')
		.appendChild(createPlaceHolder(false));
	document
		.getElementById('proposal_financial_summery')
		.appendChild(createPlaceHolder(false));
	var self = this;
	var url =
		base_url +
		'admin/proposal/solar_pdf_download/' +
		self.proposal_data.id +
		'?api_invoker=solar-proposal';
	$.ajax({
		url: url,
		type: 'post',
		dataType: 'json',
		success: function (stat) {
			if (stat.success == true) {
				self.create_financial_summary_table(stat.data);
			} else {
				toastr['error'](stat.status);
				self.create_financial_summary_table(false);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.fetch_lead_data = function (data) {
	var self = this;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/lead/fetch_lead_data',
		datatype: 'json',
		data: data,
		beforeSend: function () {
			$.isLoading({
				text: 'Loading data, Please Wait....',
			});
		},
		success: function (stat) {
			$.isLoading('hide');
			stat = JSON.parse(stat);
			if (stat.success == true) {
				self.lead_data = stat.lead_data;
				//Populate Customer Details
				self.cust_id = self.lead_data.cust_id;
				// console.log(self.lead_data);
				$('#site_name').val(self.lead_data.site_name);
				$('#site_address').val(self.lead_data.address);
				$('#customer_name').val(
					self.lead_data.first_name + ' ' + self.lead_data.last_name
				);
				$('#customer_contact_no').val(
					self.lead_data.customer_contact_no
				);
				$('#site_postcode').val(self.lead_data.postcode);
				if (
					self.lead_data.postcode == '0' ||
					self.lead_data.postcode == ''
				) {
					$('#site_postcode_warning').html(
						'<i class="fa fa-warning"></i> Please provide postcode'
					);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr.remove();
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype;

/** Common Functions **/

solar_proposal_manager.prototype.toCommas = function (value) {
	// console.log(value);
	return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

solar_proposal_manager.prototype.create_uuid = function () {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return (
		s4() +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		'-' +
		s4() +
		s4() +
		s4()
	);
};

solar_proposal_manager.prototype.space_type_by_hours = function (item_hours) {
	$('#hours').val(item_hours);
	$('#lifetime').attr('disabsolar', 'disabsolar');
	$('#lifetime').val('');
	if (parseInt(item_hours) >= 5000) {
		$('#lifetime').removeAttr('disabsolar');
	}
	$('#hours').val(item_hours);
};

solar_proposal_manager.prototype.fetch_rebate_value = function () {
	if ($('#prd_id option:selected').val() != '') {
		var rebate = 0;
		var item_data = $('#prd_id option:selected').attr('data-item');
		item_data = JSON.parse(item_data);
		var hours = document.getElementById('hours').value;
		hours = parseInt(hours);
		if (hours == 2000) {
			rebate = item_data['rebate_2000'];
		} else if (hours == 3000) {
			rebate = item_data['rebate_3000'];
		} else if (hours == 5000) {
			var item_data_id = $('#space_type option:selected').val();
			var item_data_text = $('#space_type option:selected').text();
			if (
				item_data_id == '8' ||
				item_data_text === '5000hrs (lifetime A)'
			) {
				rebate = item_data['rebate_5000_a'];
			} else if (
				item_data_id == '7' ||
				item_data_text === '5000hrs (lifetime C)'
			) {
				rebate = item_data['rebate_5000_c'];
			} else {
				rebate = item_data['rebate_5000'];
			}
		}
		document.getElementById('veec').value = rebate;
	}
};

solar_proposal_manager.prototype.create_solar_proposal_details = function (
	data
) {
	var self = this;

	// console.log(data);

	var tr = document.createElement('tr');
	var total_project_cost_row = document.createElement('tr');
	var total_project_cost = document.createElement('td');
	total_project_cost.innerHTML = 'Total Project Cost (exGST)';
	var total_project_cost_value = document.createElement('td');
	total_project_cost_value.innerHTML =
		'<span class="total-price">$' +
		self.toCommas(data.total_payable_exGST) +
		'</span>';
	total_project_cost_row.appendChild(total_project_cost);
	total_project_cost_row.appendChild(total_project_cost_value);

	var total_rebate_cost_row = document.createElement('tr');
	var total_rebate_cost = document.createElement('td');
	total_rebate_cost.innerHTML = 'Total Rebate Cost (exGST)';
	var total_rebate_cost_value = document.createElement('td');
	total_rebate_cost_value.innerHTML =
		'<span class="total-price">$' +
		self.toCommas(data.stc_rebate_value) +
		'</span>';
	total_rebate_cost_row.appendChild(total_rebate_cost);
	total_rebate_cost_row.appendChild(total_rebate_cost_value);

	var total_invesment_row = document.createElement('tr');
	var total_invesment = document.createElement('td');
	total_invesment.innerHTML = 'Net Payable (exGST)';
	var total_invesment_value = document.createElement('td');
	total_invesment_value.innerHTML =
		'<span class="total-price">$' +
		self.toCommas(data.price_before_stc_rebate) +
		'</span> </a>';
	total_invesment_row.appendChild(total_invesment);
	total_invesment_row.appendChild(total_invesment_value);

	var total_system_size = document.createElement('tr');
	var system_size = document.createElement('td');
	system_size.innerHTML = 'System Size';
	var total_system_size_value = document.createElement('td');
	total_system_size_value.innerHTML =
		'<span class="total-price">' +
		self.toCommas(data.total_system_size) +
		' kW</span>';
	total_system_size.appendChild(system_size);
	total_system_size.appendChild(total_system_size_value);

	var td2 = document.createElement('tr');
	//td2.appendChild(additional_item_cost_row);
	td2.appendChild(total_project_cost_row);
	td2.appendChild(total_rebate_cost_row);
	td2.appendChild(total_invesment_row);
	td2.appendChild(total_system_size);

	tr.appendChild(td2);

	// Getting system details

	if (data.total_system_size) {
		document.getElementById('pv_system_size').value =
			data.total_system_size;
		document.getElementById('stc_calculate_system_size').value =
			data.total_system_size;
	}

	document.getElementById('proposal_cost_details_wrapper').innerHTML = '';
	document.getElementById('proposal_cost_details_wrapper').appendChild(td2);
	// self.get_pvwatt_calc();
	if (data.is_production_file == 1) {
		self.isProductionFile = 1;
		$('.d-solar-production').hide();
		$('.productionDataFileURL').show();
		$('#system_pricing_panel_val').removeAttr('readonly');
		console.log(data.pvwatts_data_result);
		self.solar_production_graph_ploting(data.pvwatts_data_result);
	} else {
		if (data.system_details != null) {
			self.get_mapping_tool_data(true);
		} else {
			self.get_mapping_tool_data(false);
		}
	}
};
solar_proposal_manager.prototype.create_financial_summary_table = function (
	data
) {
	var self = this;
	if (data == false) {
		var tr = document.createElement('tr');
		var notAvailable = document.createElement('td');
		notAvailable.innerHTML = 'N/A';
		tr.appendChild(notAvailable);
		document.getElementById('proposal_financial_summery').innerHTML = '';
		document.getElementById('proposal_financial_summery').appendChild(tr);
		return false;
	}
	var tr = document.createElement('tr');
	var total_project_cost_row = document.createElement('tr');
	var total_project_cost = document.createElement('td');
	total_project_cost.innerHTML = 'Return of Investment';
	var total_project_cost_value = document.createElement('td');
	var roi = Math.round(data['roi']);
	total_project_cost_value.innerHTML =
		'<span class="total-price">' +
		roi +
		(roi > 1 ? ' Years' : ' Year') +
		'</span>';
	total_project_cost_row.appendChild(total_project_cost);
	total_project_cost_row.appendChild(total_project_cost_value);

	var total_rebate_cost_row = document.createElement('tr');
	var total_rebate_cost = document.createElement('td');
	total_rebate_cost.innerHTML = 'First Year Savings';
	var total_rebate_cost_value = document.createElement('td');
	var total_saving_arr = data['TotalSavingsArr'][0];
	total_saving_arr = parseInt(total_saving_arr);
	total_rebate_cost_value.innerHTML =
		'<span class="total-price">$' +
		self.toCommas(total_saving_arr) +
		'</span>';
	total_rebate_cost_row.appendChild(total_rebate_cost);
	total_rebate_cost_row.appendChild(total_rebate_cost_value);

	var total_invesment_row = document.createElement('tr');
	var total_invesment = document.createElement('td');
	total_invesment.innerHTML = 'Cashflow Over Finance Term';
	var total_invesment_value = document.createElement('td');

	var Cashflow_Term,
		Cashflow_15Yr = 0;
	var fin = 0;
	var TotalInvestment = 0;
	var monthlyPaymentPlanSolar =
		parseFloat(data['proposal_finance_data']['stc_monthly_payment_plan']) *
		parseFloat(data['year']) *
		parseFloat(12);
	var Cashflow_10Yr = 0;
	var savingsSum = [];
	for (var t = 0; t < 15; t++) {
		if (t < data['year']) {
			fin = monthlyPaymentPlanSolar;
			Cashflow_15Yr = data['TotalSavingsArr'][t] - fin;
		} else {
			fin = 0;
			Cashflow_15Yr = data['TotalSavingsArr'][t] - fin;
		}
		if (t == data['year']) {
			Cashflow_Term = Cashflow_15Yr;
		}
		if (t == 9) {
			Cashflow_10Yr = Cashflow_15Yr;
		}

		if (t == 0) {
			savingsSum[t] = 0 + data['TotalSavingsArr'][t];
		} else {
			savingsSum[t] = savingsSum[t - 1] + data['TotalSavingsArr'][t];
		}
	}

	Cashflow_Term =
		parseFloat(savingsSum[data['year'] - 1]) -
		parseFloat(monthlyPaymentPlanSolar);
	Cashflow_Term = parseFloat(Cashflow_Term).toFixed(2);
	total_invesment_value.innerHTML =
		'<span class="total-price">$' +
		self.toCommas(Cashflow_Term) +
		'</span> </a>';
	total_invesment_row.appendChild(total_invesment);
	total_invesment_row.appendChild(total_invesment_value);

	var annualProductionArr1 = [];
	var annualProduction10Yr = 0;
	var annualProduction = data['AverageDailyProduction'] * 365;
	for (i = 0; i < 10; i++) {
		if (i == 0) {
			annualProductionArr1[i] =
				parseFloat(0) + parseFloat(annualProduction);
		} else {
			annualProductionArr1[i] =
				annualProductionArr1[i - 1] -
				annualProductionArr1[i - 1] * 0.006;
		}
		annualProduction10Yr =
			parseFloat(annualProduction10Yr) +
			parseFloat(annualProductionArr1[i]);
	}

	var solar_energy_rate =
		(parseFloat(monthlyPaymentPlanSolar) /
			parseFloat(annualProduction10Yr)) *
		100;
	if (!isNaN(solar_energy_rate)) {
		solar_energy_rate = parseFloat(solar_energy_rate).toFixed(2) + 'c/kWh';
	} else {
		solar_energy_rate = data['solar_energy_rate'];
	}

	var total_system_size = document.createElement('tr');
	var system_size = document.createElement('td');
	system_size.innerHTML = '10 Years Solar Energy Rate';
	var total_system_size_value = document.createElement('td');

	total_system_size_value.innerHTML =
		'<span class="total-price">' + solar_energy_rate + '</span>';
	total_system_size.appendChild(system_size);
	total_system_size.appendChild(total_system_size_value);

	var td2 = document.createElement('tr');
	//td2.appendChild(additional_item_cost_row);
	td2.appendChild(total_project_cost_row);
	td2.appendChild(total_rebate_cost_row);
	td2.appendChild(total_invesment_row);
	td2.appendChild(total_system_size);

	tr.appendChild(td2);

	document.getElementById('proposal_financial_summery').innerHTML = '';
	document.getElementById('proposal_financial_summery').appendChild(td2);
};

solar_proposal_manager.prototype.fetch_solar_proposal_details = function () {
	var self = this;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/proposal/fetch_solar_proposal_details',
		datatype: 'json',
		data: { proposal_id: self.proposal_data.id },
		beforeSend: function () {
			document.getElementById('proposal_cost_details_wrapper').innerHTML =
				'';
			//document.getElementById('proposal_cost_details_wrapper').appendChild(createLoader('Fetching Proposal Details....'));
			document
				.getElementById('proposal_cost_details_wrapper')
				.appendChild(createPlaceHolder(false));
			document
				.getElementById('proposal_cost_details_wrapper')
				.appendChild(createPlaceHolder(false));
		},
		success: function (stat) {
			var stat = JSON.parse(stat);
			// console.log(stat);
			if (stat.success == true) {
				self.create_solar_proposal_details(stat.proposal_data);
				$('[data-toggle="tooltip"]').tooltip();
				//$('#total_payable_exGST').trigger('change');
			}
			self.meter_details(stat);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.calculate_proposal_finance = function (
	veec = null
) {
	var self = this;
	var financial_selection = $('#financial_selection').val();
	var amount = 0;
	var term = '';
	if (financial_selection == 'STC') {
		term = document.getElementById('stc_term').value;
		amount = document.getElementById('price_before_stc_rebate').value;
	}
	if (financial_selection == 'LGC') {
		term = document.getElementById('lgc_term').value;
		amount = document.getElementById('price_before_stc_rebate2').value;
	}
	if (financial_selection == 'VEEC') {
		if (veec == 2) {
			term = document.getElementById('term_veec_2').value;
			amount = document.getElementById('project_cost_2').value;
		}
		if (veec == 3) {
			term = document.getElementById('term_veec_3').value;
			amount = document.getElementById('project_cost_3').value;
		}
	}

	if (term == '') {
		toastr['error']('Please Select term.');
		return false;
	}
	var data = {};
	data.proposal_id = self.proposal_data.id;
	data.term = term;
	data.type = 2;
	data.amount = amount;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/proposal/commercial/calculate_finance',
		datatype: 'json',
		data: data,
		beforeSend: function () {
			$('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			$('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			document
				.getElementById('finance_loader')
				.appendChild(createLoader('Calcualting Finance Details....'));
		},
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success == true) {
				if (financial_selection == 'STC') {
					document.getElementById('stc_monthly_payment_plan').value =
						stat.monthly_payment;
				}
				if (financial_selection == 'LGC') {
					document.getElementById('lgc_monthly_payment_plan').value =
						stat.monthly_payment;
				}
				if (financial_selection == 'VEEC') {
					if (veec == 2) {
						document.getElementById(
							'monthly_payment_plan_2'
						).value = stat.monthly_payment;
					}
					if (veec == 3) {
						document.getElementById(
							'monthly_payment_plan_3'
						).value = stat.monthly_payment;
					}
				}
			} else {
				toastr['error'](stat.status);
			}
			$('#save_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			$('#delete_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			document.getElementById('finance_loader').innerHTML = '';
		},
		error: function (xhr, ajaxOptions, thrownError) {
			document.getElementById('finance_loader').innerHTML = '';
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.save_proposal_finance = function () {
	var self = this;
	var term = document.getElementById('term').value;
	var data = '';
	var financial_selection = document.getElementById(
		'financial_selection'
	).value;
	if (financial_selection == 'STC') {
		data = $('#proposal_finance_form').serialize();
	}
	if (financial_selection == 'LGC') {
		data = $('#proposal_finance_form2').serialize();
	}

	data += '&proposal_id=' + self.proposal_data.id;
	$.ajax({
		type: 'POST',
		url: base_url + 'admin/proposal/save_finance',
		datatype: 'json',
		data: data,
		beforeSend: function () {
			$('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			$('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			document
				.getElementById('finance_loader')
				.appendChild(createLoader('Saving Finance Details....'));
		},
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success) {
				toastr.remove();
				toastr['success'](stat.status);
				$('#delete_proposal_finance_btn').removeClass('hidden');
			} else {
				toastr['error'](stat.status);
			}
			$('#save_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			$('#delete_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			document.getElementById('finance_loader').innerHTML = '';
		},
		error: function (xhr, ajaxOptions, thrownError) {
			document.getElementById('finance_loader').innerHTML = '';
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.delete_proposal_finance = function () {
	var self = this;
	var data = '';
	data = $('#proposal_finance_form').serialize();
	data += '&proposal_id=' + self.proposal_data.id;
	$.ajax({
		type: 'POST',
		url: base_url + 'admin/proposal/delete_finance',
		datatype: 'json',
		data: data,
		beforeSend: function () {
			$('#save_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			$('#delete_proposal_finance_btn').attr('disabsolar', 'disabsolar');
			document
				.getElementById('finance_loader')
				.appendChild(createLoader('Deleting Finance Details....'));
		},
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success) {
				toastr['success'](stat.status);
				$('#delete_proposal_finance_btn').addClass('hidden');
				document.getElementById('proposal_finance_form').reset();
			} else {
				toastr['error'](stat.status);
			}
			$('#save_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			$('#delete_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			document.getElementById('finance_loader').innerHTML = '';
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$('#save_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			$('#delete_proposal_finance_btn').removeAttr(
				'disabsolar',
				'disabsolar'
			);
			document.getElementById('finance_loader').innerHTML = '';
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.add_panel_area = function () {
	var self = this;
	var max_fields = 20; //maximum input boxes allowed
	var wrapper = $('#panels_area_wrapper'); //Fields wrapper
	var add_button = $('#add_panel_area'); //Add button ID
	var count = self.solar_panel_area_count;
	var x = count > 0 ? count : 1; //initlal text box count
	$(add_button).click(function (e) {
		//on add input button click
		e.preventDefault();
		if (x < max_fields) {
			$(wrapper).append(self.create_panel_area(x));
			x++;
		}
	});

	$(wrapper).on('click', '.remove_field', function (e) {
		//user click on remove field
		e.preventDefault();
		$(this).parent().parent('div').remove();
		x--;
		$('.no_of_panels').trigger('change');
	});
};

solar_proposal_manager.prototype.create_panel_area = function (x) {
	var panel_area =
		'<div><div style="font-size:18px; border-bottom:1px solid black; margin-bottom:5px;">Panel Details - Area' +
		(x + 1) +
		'<a href="#" class="remove_field" style="margin-left:10px;"><i class="fa fa-times"></i></a></div>' +
		'<div class="form-group">' +
		'<label>Number of Panels</label>' +
		'<input class="form-control no_of_panels" placeholder="Please Enter Number of Panels" type="number" name="solar_panel_data[no_of_panels][' +
		x +
		']" value="" min="1" oninput="validity.valid||(value="");" required="">' +
		'</div>' +
		'<div class="form-group">' +
		'<label class="required-label">Panel Orientation</label>' +
		'<select name="solar_panel_data[panel_orientation][' +
		x +
		']"  class="form-control" required="">' +
		'<option value="0">0</option>' +
		'<option value="10">10</option>' +
		'<option value="20" >20</option>' +
		'<option value="30">30</option>' +
		'<option value="40">40</option>' +
		'<option value="50">50</option>' +
		'<option value="60">60</option>' +
		'<option value="70">70</option>' +
		'<option value="80">80</option>' +
		'<option value="90">90</option>' +
		'<option value="100">100</option>' +
		'<option value="110">110</option>' +
		'<option value="120">120</option>' +
		'<option value="130">130</option>' +
		'<option value="140">140</option>' +
		'<option value="150">150</option>' +
		'<option value="160">160</option>' +
		'<option value="170">170</option>' +
		'<option value="180">180</option>' +
		'<option value="190">190</option>' +
		'<option value="200">200</option>' +
		'<option value="210">210</option>' +
		'<option value="220">220</option>' +
		'<option value="230">230</option>' +
		'<option value="240">240</option>' +
		'<option value="250">250</option>' +
		'<option value="260">260</option>' +
		'<option value="270">270</option>' +
		'<option value="280">280</option>' +
		'<option value="290">290</option>' +
		'<option value="300">300</option>' +
		'<option value="310">310</option>' +
		'<option value="320">320</option>' +
		'<option value="330">330</option>' +
		'<option value="340">340</option>' +
		'<option value="350">350</option>' +
		'</select>' +
		'</div>' +
		'<div class="form-group">' +
		'<label>Panel Tilt</label>' +
		'<select name="solar_panel_data[panel_tilt][' +
		x +
		']"  class="form-control" required="">' +
		'<option value="0">0</option>' +
		'<option value="10" selected="">10</option>' +
		'<option value="20">20</option>' +
		'<option value="30">30</option>' +
		'<option value="40">40</option>' +
		'<option value="50">50</option>' +
		'<option value="60">60</option>' +
		'<option value="70">70</option>' +
		'<option value="80">80</option>' +
		'<option value="90">90</option>' +
		'</select>' +
		'</div></div>';
	return panel_area;
};

/** ENERGY USAGE RELATED FUNCTIONS **/

solar_proposal_manager.prototype.validate_solar_proposal_pdf_data =
	function () {
		var self = this;
		var flag = true;

		var elements = document.getElementById('solar_energy_form').elements;
		for (var i = 0; i < elements.length; i++) {
			$(elements[i]).removeClass('is-invalid');
			if (elements[i].hasAttribute('required')) {
				if (elements[i].value == '') {
					$(elements[i]).addClass('is-invalid');
					flag = false;
				}
			}
		}

		if ($('#nearmap_toggle_val').val() == 0) {
			var elements1 =
				document.getElementById('solarSystemSummary').elements;
			for (var i = 0; i < elements1.length; i++) {
				$(elements1[i]).removeClass('is-invalid');
				if (elements1[i].hasAttribute('required')) {
					if (elements1[i].value == '') {
						$(elements1[i]).addClass('is-invalid');
						flag = false;
					}
				}
			}
		}
		var is_meter_data = $('#is_meter_data_val').val();
		var no_of_working_days = $('#no_of_working_days').val();
		var custom_profile_id = $('#custom_profile_id').val();
		$('#no_of_working_days').removeClass('is-invalid');
		$('#custom_profile_id').removeClass('is-invalid');
		if (is_meter_data != '1') {
			// console.log(is_meter_data);
			if (no_of_working_days == '') {
				$('#no_of_working_days').addClass('is-invalid');
				flag = false;
			}
			if (custom_profile_id == '') {
				$('#custom_profile_id').addClass('is-invalid');
				flag = false;
			}
		}

		return flag;
	};

solar_proposal_manager.prototype.validate_solar_proposal_data = function (
	form
) {
	var flag = true;
	var elements = document.getElementById(form).elements;
	for (var i = 0; i < elements.length; i++) {
		$(elements[i]).removeClass('is-invalid');
		if (elements[i].hasAttribute('required')) {
			if (elements[i].value == '') {
				$(elements[i]).addClass('is-invalid');
				flag = false;
			}
		}
	}

	if (form == 'solar_energy_form') {
		var is_meter_data = $('#is_meter_data_val').val();
		var no_of_working_days = $('#no_of_working_days').val();
		var custom_profile_id = $('#custom_profile_id').val();
		$('#no_of_working_days').removeClass('is-invalid');
		$('#custom_profile_id').removeClass('is-invalid');
		if (is_meter_data != '1') {
			if (no_of_working_days == '') {
				$('#no_of_working_days').addClass('is-invalid');
				flag = false;
			}
			if (custom_profile_id == '') {
				$('#custom_profile_id').addClass('is-invalid');
				flag = false;
			}
		}
	}

	return flag;
};

solar_proposal_manager.prototype.save_solar_proposal = function (is_validate) {
	var self = this;

	if (is_validate) {
		var flag = self.validate_solar_proposal_data('solar_energy_form');
		if (!flag) {
			toastr['error'](
				'Error ! Required fields missing. Please check fields marked in red.'
			);
			return false;
		}
	}
	var formData = $('#solar_energy_form').serialize();
	formData += '&proposal_id=' + self.proposal_data.id;

	// console.log(formData);
	$.ajax({
		url: base_url + 'admin/proposal/save_solar_proposal',
		type: 'post',
		data: formData,
		dataType: 'json',
		beforeSend: function () {
			if (is_validate) {
				toastr['info']('Saving Proposal Data Please Wait....');
			}
		},
		success: function (stat) {
			toastr.remove();
			if (stat.success == true) {
				if (is_validate) {
					toastr['success'](stat.status);
				}
				self.fetch_solar_proposal_details();
			} else {
				if (is_validate) {
					toastr['error'](stat.status);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.save_solar_system_summary = function () {
	var self = this;
	var flag = self.validate_solar_proposal_data('solarSystemSummary');
	if (!flag) {
		toastr['error'](
			'Error ! Required fields missing. Please check fields marked in red.'
		);
		return false;
	}
	var formData = $('#solarSystemSummary').serialize();
	formData += '&proposal_id=' + self.proposal_data.id;
	$.ajax({
		url: base_url + 'admin/proposal/save_solar_proposal',
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function (stat) {
			toastr.remove();
			if (stat.success == true) {
				toastr['success'](stat.status);
				self.fetch_solar_proposal_details();
			} else {
				toastr['error'](stat.status);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.save_solar_rebate = function (is_save) {
	var self = this;
	// var flag = self.validate_solar_proposal_data('solarPayments');
	// if (!flag) {
	// 	toastr['error'](
	// 		'Error ! Required fields missing. Please check fields marked in red.'
	// 	);
	// 	return false;
	// }
	var financial_selection = document.getElementById(
		'financial_selection'
	).value;
	if (financial_selection == 'STC') {
		var formData = $('#solarPayments,#solarFinancial').serialize();
	}
	if (financial_selection == 'LGC') {
		var formData = $(
			'#solarPayments2,#proposal_finance_form2, #solarFinancial2'
		).serialize();
	}
	if (financial_selection == 'VEEC') {
		var formData = $(
			'#solarPayment_veec_2,#proposal_finance_veec_2, #solarFinancial_veec_2,#solarPayment_veec_3,#proposal_finance_veec_3, #solarFinancial_veec_3'
		).serialize();
	}

	formData += '&proposal_id=' + self.proposal_data.id;
	formData += '&financial_selection=' + $('#financial_selection').val();
	$.ajax({
		url: base_url + 'admin/proposal/save_solar_proposal',
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function (stat) {
			if (stat.success == true) {
				toastr['success'](stat.status);
				if (is_save) {
					$('#stc_calculation_modal').modal('hide');
				}
				self.fetch_solar_proposal_details();
			} else {
				toastr['error'](stat.status);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.fetch_financial_summary_data = function () {
	var self = this;
	$.ajax({
		type: 'GET',
		url:
			base_url +
			'admin/proposal/commercial/fetch_financial_summary_data' +
			self.proposal_data.id,
		datatype: 'json',
		success: function (stat) {
			// console.log(stat);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

/** LED PRODUCT RELATED FUNCTIONS **/

solar_proposal_manager.prototype.fetch_solar_product_by_type = function (id) {
	var self = this;
	var type_id = $('#type_id').val();
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/product/fetch_solar_product_by_type',
		datatype: 'json',
		data: { type_id: type_id, state: self.lead_data.state_postal },
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success == true) {
				var option;
				document.getElementById('prd_id').innerHTML = '';
				if (stat.product_data.length > 0) {
					for (var i = 0; i < stat.product_data.length; i++) {
						option = self.create_product_select_option(
							stat.product_data[i],
							id
						);
						document.getElementById('prd_id').appendChild(option);
					}
				} else {
					option = document.createElement('option');
					option.setAttribute('value', '');
					option.innerHTML =
						'No Item Found with Selected Product Type';
					document.getElementById('prd_id').appendChild(option);
				}
				$('#prd_id').trigger('change');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.create_product_select_option = function (
	data,
	selected
) {
	var option = document.createElement('option');
	var value = data.prd_id;
	option.setAttribute('value', value);
	option.setAttribute('data-item', JSON.stringify(data));
	if (data.prd_id == selected) {
		option.setAttribute('selected', 'selected');
	}
	option.innerHTML = data.product_name;
	return option;
};

solar_proposal_manager.prototype.validate_solar_proposal_product_data =
	function () {
		var self = this;
		var flag = true;
		var space_type = $('#space_type').val();
		var elements = document.getElementById('solar_products_form').elements;
		for (var i = 0; i < elements.length; i++) {
			$(elements[i]).removeClass('is-invalid');
			if (elements[i].hasAttribute('required')) {
				if (elements[i].value == '') {
					$(elements[i]).addClass('is-invalid');
					flag = false;
				}
			}
		}
		return flag;
	};

solar_proposal_manager.prototype.add_proposal_product = function (data) {
	var self = this;
	// console.log(data);
	if (data != '' && data != undefined) {
		formData = data;
	} else {
		var flag = self.validate_solar_proposal_product_data();
		if (!flag) {
			toastr['error'](
				'Error ! Required fields missing. Please check fields marked in red.'
			);
			return false;
		}
		formData = $('#solar_products_form').serialize();
	}
	formData += '&solar_product[proposal_id]=' + self.proposal_data.id;
	$.ajax({
		url: base_url + 'admin/proposal/save_solar_proposal_products',
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function (stat) {
			if (stat.success == true) {
				if (data == '' || data == undefined) {
					toastr['success'](stat.status);
					self.calculate_system_size();
					setTimeout(function () {
						self.save_solar_proposal(false);
					}, 1000);
				}
				$('#solar_proposal_products_table_body').html('');
				if (stat.product_data.length > 0) {
					for (var i = 0; i < stat.product_data.length; i++) {
						if (stat.product_data[i].type_id == 9) {
							self.panel_row_id = stat.product_data[i].id;
						}
						var row = self.create_proposal_prodcut_table_row(
							stat.product_data[i]
						);
						var tbody = document.querySelector(
							'#solar_proposal_products_table_body'
						);
						tbody.appendChild(row);
					}
					self.calculate_system_size();
					self.edit_proposal_product();
					self.delete_proposal_product();
					var prd_typ_id = $('#type_id').val();
					prd_typ_id = parseInt(prd_typ_id);
					if (prd_typ_id == 9) {
						//self.save_solar_rebate();
						//self.save_solar_system_summary();
					}
				} else {
					self.panel_row_id = '';
					$('#solar_proposal_products_table_body').html(
						'<tr><td colspan="4" class="text-center">No product data found.</td></tr>'
					);
				}
			} else {
				if (data == '' || data == undefined) {
					toastr['error'](stat.status);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.create_proposal_prodcut_table_row = function (
	data
) {
	var tr = document.createElement('tr');
	var td_array = ['product_name', 'type_name', 'prd_qty'];
	for (var i = 0; i < td_array.length; i++) {
		var td = document.createElement('td');
		td.innerHTML = data[td_array[i]];
		tr.appendChild(td);
	}
	var td_action = document.createElement('td');

	var action_edit_btn = document.createElement('a');
	action_edit_btn.className =
		'btn-info btn-sm solar_proposal_product_edit_btn';
	action_edit_btn.setAttribute('data-id', data.id);
	action_edit_btn.setAttribute('data-item', JSON.stringify(data));
	action_edit_btn.setAttribute('href', 'javascript:void(0);');
	action_edit_btn.innerHTML = '<i class="fa fa-pencil"></i> Edit';

	var action_delete_btn = document.createElement('a');
	action_delete_btn.className =
		'btn-danger btn-sm ml-2 solar_proposal_product_delete_btn';
	action_delete_btn.setAttribute('data-id', data.id);
	action_delete_btn.setAttribute('href', 'javascript:void(0);');
	action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

	td_action.appendChild(action_edit_btn);
	td_action.appendChild(action_delete_btn);
	tr.appendChild(td_action);

	return tr;
};

solar_proposal_manager.prototype.fetch_proposal_products = function () {
	var self = this;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/proposal/fetch_solar_proposal_products',
		datatype: 'json',
		data: { proposal_id: self.proposal_data.id },
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success == true) {
				$('#solar_proposal_products_table_body').html('');
				if (stat.product_data.length > 0) {
					for (var i = 0; i < stat.product_data.length; i++) {
						if (stat.product_data[i].type_id == 9) {
							self.panel_row_id = stat.product_data[i].id;
						}
						var row = self.create_proposal_prodcut_table_row(
							stat.product_data[i]
						);
						var tbody = document.querySelector(
							'#solar_proposal_products_table_body'
						);
						tbody.appendChild(row);
					}
					self.calculate_system_size();
					self.edit_proposal_product();
					self.delete_proposal_product();
				} else {
					$('.no_of_panels').trigger('change');
					$('#solar_proposal_products_table_body').html(
						'<tr><td colspan="4" class="text-center">No product data found.</td></tr>'
					);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.edit_proposal_product = function () {
	var self = this;
	$('.solar_proposal_product_edit_btn').click(function () {
		var id = $(this).attr('data-id');
		document.getElementById('solar_product_id').value = id;

		var data = $(this).attr('data-item');
		data = JSON.parse(data);

		$('#add_product_btn').addClass('hidden');
		$('#cancel_product_btn').removeClass('hidden');
		$('#update_product_btn').removeClass('hidden');

		//Set space Type
		$('#space_type').val(data['space_type_id']);
		$('#space_type').trigger('change');

		//Populate data in form
		var elements = document.getElementById('solar_energy_form').elements;

		for (var key in data) {
			$('#' + key).val(data[key]);
		}

		if (parseInt(data.type_id) > 9 && parseInt(data.type_id) < 14) {
			$('#type_id').val(10);
		}

		//Fetch and Set New Prorduct
		self.fetch_solar_product_by_type(data['prd_id']);
		//window.scrollTo({top: 10, behavior: 'smooth'});
	});
};

solar_proposal_manager.prototype.delete_proposal_product = function () {
	var self = this;
	$('.solar_proposal_product_delete_btn').click(function () {
		$this = $(this);
		var id = $(this).attr('data-id');
		$('#cancel_product_btn').click();
		$.ajax({
			url: base_url + 'admin/proposal/delete_solar_proposal_products',
			type: 'post',
			data: {
				solar_product_id: id,
				proposal_id: self.proposal_data.id,
			},
			dataType: 'json',
			success: function (stat) {
				if (stat.success == true) {
					toastr['success'](stat.status);
					$this.parent().parent('tr').remove();
					self.calculate_system_size();
					//self.save_solar_rebate();
					//self.save_solar_system_summary();
				} else {
					toastr['error'](stat.status);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toastr['error'](
					thrownError +
						'\r\n' +
						xhr.statusText +
						'\r\n' +
						xhr.responseText
				);
			},
		});
	});
};

/** ADDITIONAL ITEM RELATED FUNCTIONS **/

solar_proposal_manager.prototype.create_proposal_additional_item_table_row =
	function (data) {
		var tr = document.createElement('tr');
		var td_array = ['ae_name', 'ae_qty', 'ae_cost'];
		for (var i = 0; i < td_array.length; i++) {
			var td = document.createElement('td');
			td.innerHTML = data[td_array[i]];
			tr.appendChild(td);
		}
		var td_action = document.createElement('td');

		var action_delete_btn = document.createElement('a');
		action_delete_btn.className =
			'btn-danger btn-sm ml-2 solar_proposal_additional_item_delete_btn';
		action_delete_btn.setAttribute('data-id', data.id);
		action_delete_btn.setAttribute('href', 'javascript:void(0);');
		action_delete_btn.innerHTML = ' <i class="fa fa-trash-o"></i> Delete';

		td_action.appendChild(action_delete_btn);
		tr.appendChild(td_action);

		return tr;
	};

solar_proposal_manager.prototype.fetch_proposal_additional_items = function () {
	var self = this;
	$.ajax({
		type: 'GET',
		url: base_url + 'admin/proposal/fetch_solar_proposal_additional_items',
		datatype: 'json',
		data: { proposal_id: self.proposal_data.id },
		success: function (stat) {
			var stat = JSON.parse(stat);
			if (stat.success == true) {
				$('#solar_proposal_additional_item_table_body').html('');
				if (stat.product_data.length > 0) {
					for (var i = 0; i < stat.product_data.length; i++) {
						var row =
							self.create_proposal_additional_item_table_row(
								stat.product_data[i]
							);
						var tbody = document.querySelector(
							'#solar_proposal_additional_item_table_body'
						);
						tbody.appendChild(row);
					}
					self.delete_proposal_additional_item();
				} else {
					$('#solar_proposal_additional_item_table_body').html(
						'<tr><td colspan="4" class="text-center">No additional item found.</td></tr>'
					);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.validate_solar_proposal_additional_item_data =
	function () {
		var flag = true;
		var elements = document.getElementById(
			'add_additional_item_form'
		).elements;
		for (var i = 0; i < elements.length; i++) {
			$(elements[i]).removeClass('is-invalid');
			if (elements[i].hasAttribute('required')) {
				if (elements[i].value == '') {
					$(elements[i]).addClass('is-invalid');
					flag = false;
				}
			}
		}
		return flag;
	};

solar_proposal_manager.prototype.add_proposal_additional_item = function () {
	var self = this;
	var flag = self.validate_solar_proposal_additional_item_data();
	if (!flag) {
		toastr['error'](
			'Error ! Required fields missing. Please check fields marked in red.'
		);
		return false;
	}
	var formData = $('#add_additional_item_form').serialize();
	formData += '&proposal_id=' + self.proposal_data.id;
	$.ajax({
		url: base_url + 'admin/proposal/save_solar_proposal_additional_items',
		type: 'post',
		data: formData,
		dataType: 'json',
		success: function (stat) {
			if (stat.success == true) {
				toastr['success'](stat.status);
				$('#solar_proposal_additional_item_table_body').html('');
				if (stat.product_data.length > 0) {
					for (var i = 0; i < stat.product_data.length; i++) {
						var row =
							self.create_proposal_additional_item_table_row(
								stat.product_data[i]
							);
						var tbody = document.querySelector(
							'#solar_proposal_additional_item_table_body'
						);
						tbody.appendChild(row);
					}
					self.delete_proposal_additional_item();
				} else {
					$('#solar_proposal_additional_item_table_body').html(
						'<tr><td colspan="4" class="text-center">No additional item found.</td></tr>'
					);
				}
			} else {
				toastr['error'](stat.status);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.delete_proposal_additional_item = function () {
	var self = this;
	$('.solar_proposal_additional_item_delete_btn').click(function () {
		$this = $(this);
		var id = $(this).attr('data-id');
		$.ajax({
			url:
				base_url +
				'admin/proposal/delete_solar_proposal_additional_items',
			type: 'post',
			data: {
				item_id: id,
				proposal_id: self.proposal_data.id,
			},
			dataType: 'json',
			success: function (stat) {
				if (stat.success == true) {
					toastr['success'](stat.status);
					$this.parent().parent('tr').remove();
				} else {
					toastr['error'](stat.status);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toastr['error'](
					thrownError +
						'\r\n' +
						xhr.statusText +
						'\r\n' +
						xhr.responseText
				);
			},
		});
	});
};

solar_proposal_manager.prototype.show_image = function () {
	var self = this;
	var image = self.proposal_image;
	image =
		image.indexOf('http://') == 0 || image.indexOf('https://') == 0
			? image
			: base_url + 'assets/uploads/proposal_files/' + image;

	if (self.proposal_image != '') {
		$('#proposal_no_image').attr('style', 'display:none !important;');
		$('#proposal_image').attr('style', 'display:block !important;');
		$('#proposal_image > .add-picture').css(
			'background-image',
			'url("' + image + '")'
		);
		$('#proposal_image > .add-picture').css(
			'background-repeat',
			'no-repeat'
		);
		$('#proposal_image > .add-picture').css(
			'background-position',
			'50% 50%'
		);
		$('#proposal_image > .add-picture').css('background-size', '100% 100%');
	}
};

solar_proposal_manager.prototype.hide_image = function () {
	$('#proposal_image_close').click(function () {
		$('#proposal_no_image').attr('style', 'display:block !important;');
		$('#proposal_image').attr('style', 'display:none !important;');
		$('#proposal_image > .add-picture').css('background-image', '');
		$('#proposal_image > .add-picture').css('background-repeat', '');
		$('#proposal_image > .add-picture').css('background-position', '');
		$('#proposal_image > .add-picture').css('background-size', '');
		$('#mapping_tool_actions').show();
	});
};

solar_proposal_manager.prototype.upload_proposal_image = function () {
	var self = this;
	$('#proposal_image_upload').change(function () {
		$('#loader').html(createLoader('Uploading file, please wait..'));
		var file_data = $(this).prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		form_data.append('upload_path', 'proposal_files');
		form_data.append('proposal_uuid', self.proposal_data.proposal_uuid);
		toastr['info']('Uploading image please wait...');
		$.ajax({
			url: base_url + 'admin/uploader/upload_proposal_image', // point to server-side PHP script
			dataType: 'json', // what to expect back from the PHP script, if anything
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function (res) {
				if (res.success == true) {
					toastr['success'](res.status);
					$('input[name="solar_proposal[image]"').val(res.file_name);
					self.proposal_image = res.file_name;
					self.show_image();
				} else {
					toastr['error'](
						res.status
							? res.status
							: 'Looks like something went wrong'
					);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				toastr['error'](
					thrownError +
						'\r\n' +
						xhr.statusText +
						'\r\n' +
						xhr.responseText
				);
			},
		});
		$('#loader').html('');
	});
};

solar_proposal_manager.prototype.handle_meter_data = function () {
	var self = this;
	var url = base_url + 'admin/proposal/meter_data/' + self.proposal_data.id;
	var y = window.top.outerHeight / 2 + window.top.screenY - 500 / 2;
	var x = window.top.outerWidth / 2 + window.top.screenX - 400 / 2;
	meter_data_window = window.open(
		'',
		'Manage Meter Data',
		'width=800px, height=500px, top=' + y + ', left=' + x
	);

	if (meter_data_window != null) {
		meter_data_window.location.href = url;
	}
};

solar_proposal_manager.prototype.calculate_system_size = function () {
	var self = this;
	var total_system_size = 0;
	var total_panels = 0;
	var products = document.getElementsByClassName(
		'solar_proposal_product_edit_btn'
	);
	for (var i = 0; i < products.length; i++) {
		var data = JSON.parse(products[i].getAttribute('data-item'));
		var prd_type = parseInt(data.type_id);
		if (prd_type == 9) {
			var prd_qty = parseInt(data.prd_qty);
			var prd_power_wattage = parseFloat(data.power_wattage / 1000);
			var system_size = prd_qty * prd_power_wattage;
			total_system_size =
				parseFloat(total_system_size) + parseFloat(system_size);
			total_panels = parseFloat(total_panels) + parseFloat(prd_qty);
		}
	}
	total_system_size = parseFloat(total_system_size).toFixed(2);
	document.getElementById('total_system_size').value = total_system_size;
	//SET Upfront option
	if (total_system_size <= 39) {
		$('#stc_upfront_payment_options').val(1);
		$('#lgc_upfront_payment_options').val(1);
	} else if (total_system_size > 39 && total_system_size <= 100) {
		$('#stc_upfront_payment_options').val(2);
		$('#lgc_upfront_payment_options').val(1);
	} else {
		$('#stc_upfront_payment_options').val(3);
		$('#lgc_upfront_payment_options').val(1);
	}
	//$("[name='solar_panel_data[no_of_panels][0]']").val(total_panels);
	$('.no_of_panels').trigger('change');
	self.total_panels = total_panels;
	self.total_system_size = total_system_size;
	self.calculate_stc_rebate();
};

solar_proposal_manager.prototype.calculate_stc_rebate = function (is_save) {
	var self = this;
	var dt = new Date();
	var year_val = dt.getYear() && dt.getFullYear() == '2018' ? 13 : 12;
	var system_size = $('#total_system_size').val();
	if (isNaN(system_size)) {
		toastr['error'](
			'Please provide Panel Wattage/System Size in order to calculate payment summary'
		);
		return false;
	}
	var no_of_stc =
		parseFloat(self.postcode_rating) *
		parseFloat(year_val) *
		parseFloat(system_size);
	no_of_stc = Math.floor(no_of_stc);

	var price_assumption = $('#stc_calculate_price').val();
	price_assumption = parseFloat(price_assumption).toFixed(2);

	var stc_rebate_value = no_of_stc * parseFloat(price_assumption);
	stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

	$('#stc').val(no_of_stc);
	$('#stc_rebate_value').val(stc_rebate_value);
	$('#stc_rebate_value_inGST').val(
		parseFloat(stc_rebate_value * self.gst).toFixed(2)
	);
	//Modal Values
	$('#stc_calculate_stc').val(no_of_stc);
	$('#stc_calculate_rebate').val(stc_rebate_value);

	$('#system_pricing_stc').val(stc_rebate_value);
	$('#system_pricing_sub_total').val();
	var system_pricing_project_price = $('#total_payable_exGST').val();
	if (system_pricing_project_price) {
		var system_subtotal = system_pricing_project_price - stc_rebate_value;
		$('#system_pricing_sub_total').val(system_subtotal);
		$('#system_pricing_net_payable').val(system_subtotal);
		$('#price_before_stc_rebate').val(system_subtotal);
		$('#price_before_stc_rebate_inGST').val(
			parseFloat(system_subtotal * self.gst).toFixed(2)
		);
	}
};

solar_proposal_manager.prototype.calculate_payment_summary = function (
	is_save
) {
	var self = this;

	var net_payable = $('#price_before_stc_rebate').val();
	net_payable = parseFloat(net_payable).toFixed(2);

	var stc_rebate_value = $('#stc_rebate_value').val();
	stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

	var total_payable = parseFloat(stc_rebate_value) + parseFloat(net_payable);
	total_payable = parseFloat(total_payable).toFixed(2);

	$('#total_payable_exGST').val(total_payable);
	$('#total_payable_inGST').val(
		parseFloat(total_payable * self.gst).toFixed(2)
	);
	$('#price_before_stc_rebate_inGST').val(
		parseFloat(net_payable * self.gst).toFixed(2)
	);

	var context = {};
	context.total_payable_exGST = total_payable;
	context.stc_rebate_value = stc_rebate_value;
	context.price_before_stc_rebate = net_payable;
	context.total_system_size = self.total_system_size;

	self.create_solar_proposal_details(context);

	if (is_save) {
		self.save_solar_rebate(is_save);
	}
};

solar_proposal_manager.prototype.calculate_veec_discount = function (popup) {
	var self = this;

	$.ajax({
		type: 'GET',
		url:
			base_url +
			'admin/proposal/commercial/calculate_veec_discount/' +
			self.proposal_data.id,
		datatype: 'json',
		beforeSend: function () {},
		success: function (stat) {
			var stat = JSON.parse(stat);
			// if (stat.success) {
			var pv_system_size = $('#pv_system_size').val();
			$('#veec_system_size').val(pv_system_size);
			$('#veec_annual_production').val(
				parseFloat(stat.AnnualSolarProduction).toFixed(2)
			);
			$('#veec_percentage_of_export').val(
				parseFloat(stat.PercentageExport).toFixed(2)
			);
			var veec_mwh_savings =
				(stat.AnnualSolarProduction *
					(1 - stat.PercentageExport / 100)) /
				1000;
			$('#veec_mwh_savings').val(parseFloat(veec_mwh_savings).toFixed(2));
			$('#veec_accuracy_savings').val(
				parseFloat(veec_mwh_savings).toFixed(2)
			);

			var veec_accuracy_factor = $('#veec_accuracy_factor').val();
			var veer_decay_factor = $('#veer_decay_factor').val();
			var after_decay_factor =
				veec_mwh_savings * veec_accuracy_factor * veer_decay_factor;
			$('#veer_after_decay_factor').val(
				parseFloat(after_decay_factor).toFixed(2)
			);
			$('#veec_savings').val(parseFloat(after_decay_factor).toFixed(2));

			var veec_emissons_factor = $('#veec_emissons_factor').val();
			var veec_regional_factor = $('#veec_regional_factor').val();
			var veec_value =
				veec_emissons_factor *
				veec_regional_factor *
				after_decay_factor;
			$('#veec_value').val(parseFloat(veec_value).toFixed(2));

			var veec_price = $('#veec_price').val();
			var veec_discount = veec_value * veec_price;
			$('#veec_discount').val(parseFloat(veec_discount).toFixed(2));
			$('#veec_discount_gst').val(
				parseFloat(veec_discount * self.gst).toFixed(2)
			);
			$('#veec_discount_val').val(parseFloat(veec_discount).toFixed(2));

			if (popup) {
				$('#veec_calculation_modal').modal('show');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$('#site_postcode').removeAttr('readonly');
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.calculate_payment_summary2 = function (
	is_save
) {
	var self = this;

	var net_payable = $('#price_before_stc_rebate').val();
	net_payable = parseFloat(net_payable).toFixed(2);

	var stc_rebate_value = $('#stc_rebate_value').val();
	stc_rebate_value = parseFloat(stc_rebate_value).toFixed(2);

	var totalPayableExGST = $('#total_payable_exGST').val();

	// var total_payable = parseFloat(stc_rebate_value) + parseFloat(net_payable);
	// total_payable = parseFloat(total_payable).toFixed(2);

	// $("#total_payable_exGST").val(total_payable);
	$('#total_payable_inGST').val(
		parseFloat(totalPayableExGST * self.gst).toFixed(2)
	);

	// var total_payable = $("#system_pricing_project_price").val();
	// total_payable = parseFloat(total_payable).toFixed(2);

	// $("#total_payable_inGST").val(parseFloat(total_payable).toFixed(2));
	// $("#total_payable_exGST").val(
	// 	parseFloat(total_payable / self.gst).toFixed(2)
	// );

	$('#price_before_stc_rebate_inGST').val(
		parseFloat(net_payable * self.gst).toFixed(2)
	);

	var context = {};
	context.total_payable_exGST = totalPayableExGST;
	context.stc_rebate_value = stc_rebate_value;
	context.price_before_stc_rebate = net_payable;
	context.total_system_size = self.total_system_size;

	self.create_solar_proposal_details(context);

	if (is_save) {
		self.save_solar_rebate(is_save);
	}
};

solar_proposal_manager.prototype.edit_customer_location = function () {
	var self = this;
	$('#customer_location_edit_btn').click(function () {
		$('#site_id').val(self.proposal_data.site_id);
		$('#locationLatitude').val(self.lead_data.latitude);
		$('#locationLongitude').val(self.lead_data.longitude);
		$('.locationstreetAddressVal').val(self.lead_data.address);
		$('.locationPostCode').val(self.lead_data.postcode);
		$('.locationState').val(self.lead_data.state_id);
		$('#select2-cust_locationstreetAddress-container').html(
			self.lead_data.address
		);

		$('#company_cust_id').val(self.lead_data.cust_id);
		$('#site_name').val(self.lead_data.site_name);
		$('#site_address').val(self.lead_data.address);
		$('#contact_firstname').val(self.lead_data.first_name);
		$('#contact_lastname').val(self.lead_data.last_name);
		$('#contact_phone').val(self.lead_data.customer_contact_no);
		$('#contact_email').val(self.lead_data.customer_email);
		$('#contact_position').val(self.lead_data.position);

		$('#add_customer_location_modal_title').html('Edit Customer Location');
		$('#add_customer_location_modal').modal('show');
	});
};

solar_proposal_manager.prototype.validate_customer_location_data = function () {
	var self = this;
	var flag = true;
	var locationPostCode = $('.locationPostCode').val();
	var locationState = $('.locationState').val();
	var locationstreetAddress = $('.locationstreetAddressVal').val();

	//Remove invalid
	$('.locationPostCode').removeClass('is-invalid');
	$('.locationState').removeClass('is-invalid');
	$('.select2-container').removeClass('form-control is-invalid');

	if (locationPostCode == '') {
		flag = false;
		$('.locationPostCode').addClass('is-invalid');
	}
	if (locationState == '') {
		flag = false;
		$('.locationState').addClass('is-invalid');
	}
	if (locationstreetAddress == '') {
		flag = false;
		$('.select2-container').addClass('form-control is-invalid');
	}

	return flag;
};

solar_proposal_manager.prototype.save_customer_location = function () {
	var self = this;
	$(document).on('click', '#save_customer_location', function () {
		var cust_id = self.lead_data.cust_id;
		var site_id = document.getElementById('site_id').value;
		var flag = self.validate_customer_location_data();
		if (!flag) {
			toastr['error'](
				'Error ! Required fields missing. Please check fields marked in red.'
			);
		}
		if (flag) {
			var locationAddForm = $('#locationAdd').serialize();
			locationAddForm += '&cust_id=' + cust_id;
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/customer/save_customer_location',
				datatype: 'json',
				data: locationAddForm,
				beforeSend: function () {
					if (
						site_id != '' &&
						site_id != null &&
						site_id != undefined
					) {
						toastr['info'](
							'Updating Customer Location please wait....'
						);
					}
					$('#customer_location_loader').html(
						createLoader('Please wait while data is being saved')
					);
					$('#save_customer_location').attr('disabled', 'disabled');
				},
				success: function (stat) {
					toastr.remove();
					var stat = JSON.parse(stat);
					if (stat.success == true) {
						toastr['success'](stat.status);
						self.lead_data.address = stat.location.address;
						self.lead_data.postcode = stat.location.postcode;
						self.lead_data.state_id = stat.location.state_id;
						if (
							self.lead_data.postcode == '0' ||
							self.lead_data.postcode == ''
						) {
							$('#site_postcode_warning').html(
								'<i class="fa fa-warning"></i> Please provide postcode'
							);
						} else {
							$('#site_postcode_warning').html('');
						}
						$('#site_address').val(self.lead_data.address);
						$('#save_customer_location').removeAttr('disabled');
						$('#add_customer_location_modal').modal('hide');
					} else {
						toastr['error'](stat.status);
						$('#save_customer_location').removeAttr('disabled');
						$('#customer_location_loader').html('');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					$('#save_customer_location').removeAttr('disabled');
					$('#customer_location_loader').html('');
					toastr['error'](
						thrownError +
							'\r\n' +
							xhr.statusText +
							'\r\n' +
							xhr.responseText
					);
				},
			});
		}
	});
};

solar_proposal_manager.prototype.save_customer_postcode = function () {
	var self = this;
	$(document).on('change', '#site_postcode', function () {
		var flag = true;
		var cust_id = self.lead_data.cust_id;
		var site_id = self.proposal_data.site_id;
		var site_postcode = document.getElementById('site_postcode').value;
		if (site_postcode == '') {
			toastr['error']('Error ! Postcode can not be left blank.');
			flag = false;
			return flag;
		}
		if (flag) {
			var data =
				'site[postcode]=' +
				site_postcode +
				'&site[address]=' +
				self.lead_data.address +
				'&site[state_id]=' +
				self.lead_data.state_id +
				'&cust_id=' +
				cust_id +
				'&site_id=' +
				site_id;
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/customer/save_customer_location',
				datatype: 'json',
				data: data,
				beforeSend: function () {
					if (
						site_id != '' &&
						site_id != null &&
						site_id != undefined
					) {
						toastr['info'](
							'Updating Customer Location please wait....'
						);
					}
					$('#site_postcode').attr('readonly', 'readonly');
				},
				success: function (stat) {
					toastr.remove();
					var stat = JSON.parse(stat);
					if (stat.success == true) {
						toastr['success'](stat.status);
						self.lead_data.postcode = stat.location.postcode;
						self.postcode_rating = stat.location.rating;
						if (
							self.lead_data.postcode == '0' ||
							self.lead_data.postcode == ''
						) {
							$('#site_postcode_warning').html(
								'<i class="fa fa-warning"></i> Please provide postcode'
							);
						} else {
							$('#site_postcode_warning').html('');
						}
						$('#site_postcode').removeAttr('readonly');
					} else {
						toastr['error'](stat.status);
						$('#site_postcode').removeAttr('readonly');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					$('#site_postcode').removeAttr('readonly');
					toastr['error'](
						thrownError +
							'\r\n' +
							xhr.statusText +
							'\r\n' +
							xhr.responseText
					);
				},
			});
		}
	});
};

solar_proposal_manager.prototype.validate_customer_data = function () {
	var self = this;
	var flag = true;
	var firstname = $('#contact_firstname').val();
	var lastname = $('#contact_lastname').val();
	var contactMobilePhone = $('#contact_phone').val();
	var customer_email = $('#contact_email').val();

	//Remove invalid
	$('#contact_phone').removeClass('is-invalid');
	$('#contact_firstname').removeClass('is-invalid');
	$('#contact_email').removeClass('is-invalid');

	if (contactMobilePhone == '') {
		flag = false;
		$('#contact_phone').addClass('is-invalid');
	}

	if (firstname == '') {
		flag = false;
		$('#contact_firstname').addClass('is-invalid');
	}

	if (customer_email == '') {
		flag = false;
		$('#contact_email').addClass('is-invalid');
	}

	return flag;
};

solar_proposal_manager.prototype.save_customer = function () {
	var self = this;
	$(document).on('click', '#save_customer', function () {
		var cust_id = document.getElementById('company_cust_id').value;
		var flag = self.validate_customer_data();
		if (!flag) {
			toastr['error'](
				'Error ! Required fields missing. Please check fields marked in red.'
			);
		}
		if (flag) {
			var customerAddForm = $('#locationAdd').serialize();
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/customer/save_customer',
				datatype: 'json',
				data: customerAddForm + '&action=add',
				beforeSend: function () {
					if (
						cust_id != '' &&
						cust_id != null &&
						cust_id != undefined
					) {
						toastr['info']('Updating Customer please wait....');
					}
					//$('#customer_loader').html(createLoader('Please wait while data is being saved'));
					$('#save_customer').attr('disabled', 'disabled');
				},
				success: function (stat) {
					toastr.remove();
					var stat = JSON.parse(stat);
					if (stat.success == true) {
						toastr['success'](stat.status);
						//Populate Data from customer Modal to Front View
						$('#customer_name').val(
							$('#contact_firstname').val() +
								' ' +
								$('#contact_lastname').val()
						);
						$('#customer_contact_no').val(
							$('#contact_phone').val()
						);

						self.lead_data.first_name =
							$('#contact_firstname').val();
						self.lead_data.last_name = $('#contact_lastname').val();
						self.lead_data.customer_contact_no =
							$('#contact_phone').val();
						self.lead_data.customer_email =
							$('#contact_email').val();
						self.lead_data.position = $('#contact_position').val();

						$('#save_customer').removeAttr('disabled');
						$('#customer_loader').html('');
					} else {
						toastr['error'](stat.status);
						$('#save_customer').removeAttr('disabled');
						$('#customer_loader').html('');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					$('#save_customer').removeAttr('disabled');
					$('#customer_loader').html('');
					toastr['error'](
						thrownError +
							'\r\n' +
							xhr.statusText +
							'\r\n' +
							xhr.responseText
					);
				},
			});
		}
	});
};

solar_proposal_manager.prototype.meter_details = function (stat) {
	var self = this;

	if (stat.proposal_data.pvwatts_data != null) {
		var pv_degradation_factor = JSON.parse(stat.proposal_data.pvwatts_data);
		// console.log(pv_degradation_factor[0]);
		if (pv_degradation_factor != null) {
			$('#pv_degradation_factor').val(
				pv_degradation_factor[0].pv_degradation_factor
			);
		}
	}
	if (stat.proposal_data.standard_charges) {
		var standard_charges = JSON.parse(stat.proposal_data.standard_charges);
		// console.log(standard_charges);

		$('#meter_data_supply_charge').val(standard_charges.supply.charge);
		$('#meter_data_meter_charge').val(standard_charges.meter.charge);
		$(
			'#meter_per_charge option[value=' + standard_charges.meter.per + ']'
		).attr('selected', 'selected');
		$(
			'#supply_per_charge option[value=' +
				standard_charges.supply.per +
				']'
		).attr('selected', 'selected');
		$('#utility_value_export').val(standard_charges.utility_value_export);
	}

	if (
		stat.proposal_data.system_details !== null &&
		stat.proposal_data.system_details !== "''" &&
		stat.proposal_data.system_details !== ''
	) {
		var solar_system_details = JSON.parse(
			stat.proposal_data.system_details
		);

		$(
			'#system_pricing_panel option[data-id=' +
				solar_system_details.system_pricing_panel_id +
				']'
		).attr('selected', 'selected');

		$('#system_pricing_panel_val').val(
			solar_system_details.system_pricing_panel_val
		);

		if (
			solar_system_details.system_pricing_panel_id != null &&
			solar_system_details.system_pricing_panel_val != null
		) {
			var pv_system_size = JSON.parse(
				solar_system_details.system_pricing_panel
			);
		}

		$(
			'#system_pricing_inverter option[data-id=' +
				solar_system_details.system_pricing_inverter_id +
				']'
		).attr('selected', 'selected');
		$('#system_pricing_inverter_val').val(
			solar_system_details.system_pricing_inverter_val
		);
		var sys_inverter_efficiency = $(
			'#system_pricing_inverter option[data-id=' +
				solar_system_details.system_pricing_inverter_id +
				']'
		).attr('data-efficiency');
		if (sys_inverter_efficiency != null && sys_inverter_efficiency != '') {
			$('#pv_inverter_efficiency').val(
				parseFloat(sys_inverter_efficiency)
			);
		}

		if (
			solar_system_details.system_pricing_battery_id != '' &&
			solar_system_details.system_pricing_battery_id != null
		) {
			$('#system_pricing_battery_val').val(
				solar_system_details.system_pricing_battery_val
			);

			$(
				'#system_pricing_battery option[data-id=' +
					solar_system_details.system_pricing_battery_id +
					']'
			).attr('selected', 'selected');

			if (solar_system_details.system_pricing_battery_size != null) {
				$('.system_bettery_size').show();
				$('#system_pricing_battery_size').val(
					solar_system_details.system_pricing_battery_size
				);
			}
		}
	}

	if (stat.proposal_data.price_annual_increase !== '') {
		var price_annual_increase = JSON.parse(
			stat.proposal_data.price_annual_increase
		);
		// console.log(price_annual_increase);
		$(
			'#meter_data_pia_year option[value=' +
				price_annual_increase.MeterDataPiaYear +
				']'
		).attr('selected', 'selected');
		$('#meter_data_pia_percentage').val(
			price_annual_increase.MeterDataPiaPercentage
		);
		$('#meter_data_subsequent').val(
			price_annual_increase.MeterDataSubsequent
		);
	}

	// Setting Bill type
	if (stat.proposal_data.bill_type != null) {
		// var isBillType = $("input[name='is_bill_type']:checked").val();

		$('input[name=is_bill_type]').parent().removeClass('active');
		var isBillTypeCheckbox =
			'input[name=is_bill_type][value=' +
			stat.proposal_data.bill_type +
			']';
		$(isBillTypeCheckbox).attr('checked', 'checked');
		// console.log(isBillTypeCheckbox);

		$(isBillTypeCheckbox).parent().addClass('active');
		var flat_rates = JSON.parse(stat.proposal_data.flat_rates);

		if (stat.proposal_data.bill_type == 'timeofUse') {
			$('#billTypeTimeOfUse').show();
			$('#timeOfUseModelLink').show();
			$('#billTypeFlat').hide();

			if (stat.proposal_data.meter_data_type == 1) {
				$('.without_meter_data').show();
				$('.timeofuse_with_meter_data').hide();

				if ($('#meter_data_actions').hasClass('d-none')) {
					$('#predefined_choose_file_outer').show();
				} else {
					$('#predefined_choose_file_outer').hide();
				}
				$('#billing_month_row').show();

				if (flat_rates != null) {
					$('#meter_data_peak_kwh').val(flat_rates.peak_kwh);
					$('#meter_data_shoulder_kwh').val(flat_rates.shoulder_kwh);
					$('#meter_data_off_peak_kwh').val(flat_rates.off_peak_kwh);
					$('#meter_data_peak').val(flat_rates.peak);
					$('#meter_data_shoulder').val(flat_rates.shoulder);
					$('#meter_data_off_peak').val(flat_rates.off_peak);
					$('#billing_month').val(flat_rates.billing_month);
				}
			}
			if (stat.proposal_data.meter_data_type == 2) {
				$('.timeofuse_with_meter_data').show();
				$('.without_meter_data').hide();
				$('#predefined_choose_file_outer').hide();

				if (flat_rates != null) {
					$('#meter_data_peak').val(
						stat.proposal_data.solar_cost_per_kwh_peak
					);
					$('#meter_data_shoulder').val(
						stat.proposal_data.solar_cost_per_kwh_shoulder
					);
					$('#meter_data_off_peak').val(
						stat.proposal_data.solar_cost_per_kwh_off_peak
					);
					// $("#meter_data_peak").val(flat_rates.peak);
					// $("#meter_data_shoulder").val(flat_rates.shoulder);
					// $("#meter_data_off_peak").val(flat_rates.off_peak);
				}
			}
		} else {
			$('#billTypeTimeOfUse').hide();
			$('#timeOfUseModelLink').hide();
			$('#billTypeFlat').show();

			if (stat.proposal_data.meter_data_type == 1) {
				$('.without_meter_data').show();
				$('.flat_with_meter_data').hide();
				$('#billing_month_row').show();

				if (flat_rates != null) {
					$('#flat_bill_type_rate').val(flat_rates.charge);
					$('#flat_quantity').val(flat_rates.quantity);
					$('#flat_energy_rate').val(flat_rates.energy_rate);
					$('#quantity_type').val(flat_rates.quantity_type);
					$('#billing_month').val(flat_rates.billing_month);
				}
			}
			if (stat.proposal_data.meter_data_type == 2) {
				$('.timeofuse_with_meter_data').show();
				$('.flat_meter_data').hide();
			}
		}
	}

	if (stat.proposal_data.flat_rates != null) {
		var flat_rates = JSON.parse(stat.proposal_data.flat_rates);
		$('#flat_bill_type_rate').val(flat_rates.charge);
		$('#flat_quantity').val(flat_rates.quantity);
		$('#flat_energy_rate').val(flat_rates.energy_rate);
	}

	// Setting time of use values
	if (stat.proposal_data.time_of_use != null) {
		var time_of_use = JSON.parse(stat.proposal_data.time_of_use);
		for (let t = 0; t < time_of_use.start_time.length; t++) {
			// Weekdays
			var weekdaysCheckbox =
				'input[name=time_of_use_weekdays' +
				(t + 1) +
				'][value=' +
				time_of_use.weekdays[t] +
				']';

			$('input[name=time_of_use_weekdays' + (t + 1) + ']')
				.parent()
				.removeClass('active');
			$(weekdaysCheckbox).parent().addClass('active');

			$(weekdaysCheckbox).attr('checked', 'checked');

			// Sat
			var satDaysCheckbox =
				'input[name=time_of_use_sat' +
				(t + 1) +
				'][value=' +
				time_of_use.sat[t] +
				']';

			$('input[name=time_of_use_sat' + (t + 1) + ']')
				.parent()
				.removeClass('active');
			$(satDaysCheckbox).parent().addClass('active');

			$(satDaysCheckbox).attr('checked', 'checked');

			// Sun
			var satDaysCheckbox =
				'input[name=time_of_use_sun' +
				(t + 1) +
				'][value=' +
				time_of_use.sun[t] +
				']';

			$('input[name=time_of_use_sun' + (t + 1) + ']')
				.parent()
				.removeClass('active');
			$(satDaysCheckbox).parent().addClass('active');

			$(satDaysCheckbox).attr('checked', 'checked');
		}
	}
	// meter data tab data

	// Show graphs if has graph data
	// console.log(self.proposal_data.saving_graph_calculation);
	if (self.proposal_data.saving_graph_calculation != null) {
		var daily_average_production =
			self.proposal_data.saving_graph_calculation;
		show_production(daily_average_production.typical_graph);
		show_production1(daily_average_production.worst_graph);
		show_production2(daily_average_production.high_graph);
		show_production3(daily_average_production.best_graph);
	}

	// if (self.proposal_data.total_system_size != "0.00") {
	// self.get_mapping_tool_data();
	// }
	// getPvwattCalcAPI(stat.proposal_data.pvwatts_data);
};

solar_proposal_manager.prototype.calculate_degration_factor_data = function (
	popup
) {
	var self = this;

	$.ajax({
		url:
			base_url +
			'admin/proposal/commercial/calculate_degration_factor_data',
		type: 'post',
		dataType: 'json',
		data: {
			proposal_id: self.proposal_data.id,
		},
		beforeSend: function () {
			$.isLoading({
				text: 'Loading data, Please Wait....',
			});
		},
		success: function (stat) {
			$.isLoading('hide');
			if (stat.success == true) {
				var data_length = stat.data_length;
				// toastr['success'](stat.status);
				console.log(stat);
				console.log(stat.final_calculation);
				var ttype = stat.final_calculation.type;
				var IsDemandCharge = stat.final_calculation.IsDemandCharge;
				var billType = stat.final_calculation.BillType;
				// var degration_data = JSON.parse(stat.final_calculation);
				// console.log(degration_data);
				var tb_table = document.createElement('table');
				var tb_f_width = 'width:200px;';

				var tb_row1 = document.createElement('tr');
				var tb_row2 = document.createElement('tr');
				var tb_row3 = document.createElement('tr');
				var tb_row4 = document.createElement('tr');
				var tb_row5 = document.createElement('tr');
				var tb_row6 = document.createElement('tr');
				var tb_row7 = document.createElement('tr');
				var tb_row8 = document.createElement('tr');
				var tb_row9 = document.createElement('tr');
				var tb_row10 = document.createElement('tr');
				var tb_row11 = document.createElement('tr');
				var tb_row12 = document.createElement('tr');
				var tb_row13 = document.createElement('tr');
				var tb_row14 = document.createElement('tr');
				var tb_row15 = document.createElement('tr');
				var tb_row16 = document.createElement('tr');
				var tb_row17 = document.createElement('tr');
				var tb_row18 = document.createElement('tr');
				var tb_row22 = document.createElement('tr');

				if (ttype == 'LGC') {
					var tb_row19 = document.createElement('tr');
				}

				if (IsDemandCharge == '1') {
					var tb_row20 = document.createElement('tr');
				}

				if (billType == 'flat') {
					var tb_row21 = document.createElement('tr');
				}

				var tb_row1_colh = document.createElement('td');
				tb_row1_colh.innerHTML = 'Year';
				tb_row1_colh.style = tb_f_width;
				tb_row1.appendChild(tb_row1_colh);

				var tb_row2_colh = document.createElement('td');
				tb_row2_colh.innerHTML = 'Solar Production';
				tb_row2_colh.style = tb_f_width;
				tb_row2.appendChild(tb_row2_colh);

				var tb_row3_colh = document.createElement('td');
				tb_row3_colh.innerHTML = 'Peak Energy';
				tb_row3_colh.style = tb_f_width;
				tb_row3.appendChild(tb_row3_colh);

				var tb_row4_colh = document.createElement('td');
				tb_row4_colh.innerHTML = 'Offset Consumption';
				tb_row4_colh.style = tb_f_width;
				tb_row4.appendChild(tb_row4_colh);

				var tb_row5_colh = document.createElement('th');
				tb_row5_colh.innerHTML = 'Electricity Price Increase';
				tb_row5_colh.colSpan = stat.calculate_years + 1;
				tb_row5_colh.style =
					tb_f_width +
					'background: #ddd;font-size: 14px;font-weight: bold;';
				tb_row5.appendChild(tb_row5_colh);

				var tb_row6_colh = document.createElement('td');
				tb_row6_colh.innerHTML = 'Electricity Price Increase';
				tb_row6_colh.style = tb_f_width;
				tb_row6.appendChild(tb_row6_colh);

				var tb_row7_colh = document.createElement('td');
				tb_row7_colh.innerHTML = 'Projected Electricity Bill: No Solar';
				tb_row7_colh.colSpan = stat.calculate_years + 1;
				tb_row7_colh.style =
					tb_f_width +
					'background: #ddd;font-size: 14px;font-weight: bold;';
				tb_row7.appendChild(tb_row7_colh);

				var tb_row8_colh = document.createElement('td');
				tb_row8_colh.style = tb_f_width;
				tb_row8_colh.innerHTML = 'Energy Charges';
				tb_row8.appendChild(tb_row8_colh);

				var tb_row9_colh = document.createElement('td');
				tb_row9_colh.innerHTML = 'Standing Charges';
				tb_row9_colh.style = tb_f_width;
				tb_row9.appendChild(tb_row9_colh);

				var tb_row10_colh = document.createElement('td');
				tb_row10_colh.innerHTML = 'Total Bill';
				tb_row10_colh.style = tb_f_width;
				tb_row10.appendChild(tb_row10_colh);

				var tb_row11_colh = document.createElement('td');
				tb_row11_colh.innerHTML =
					'Projected Electricity Bill: With Solar';
				tb_row11_colh.colSpan = stat.calculate_years + 1;
				tb_row11_colh.style =
					tb_f_width +
					'background: #ddd;font-size: 14px;font-weight: bold;';
				tb_row11.appendChild(tb_row11_colh);

				var tb_row12_colh = document.createElement('td');
				tb_row12_colh.innerHTML = 'Energy Charges';
				tb_row12_colh.style = tb_f_width;
				tb_row12.appendChild(tb_row12_colh);

				var tb_row13_colh = document.createElement('td');
				tb_row13_colh.innerHTML = 'Standing Charges';
				tb_row13_colh.style = tb_f_width;
				tb_row13.appendChild(tb_row13_colh);

				var tb_row14_colh = document.createElement('td');
				tb_row14_colh.innerHTML = 'Total Bill';
				tb_row14_colh.style = tb_f_width;
				tb_row14.appendChild(tb_row14_colh);

				var tb_row15_colh = document.createElement('td');
				tb_row15_colh.innerHTML = 'Net Bill Reduction savings';
				tb_row15_colh.style = tb_f_width;
				tb_row15.appendChild(tb_row15_colh);

				var tb_row16_colh = document.createElement('td');
				tb_row16_colh.innerHTML = 'Cummulative Savings';
				tb_row16_colh.style = tb_f_width;
				tb_row16.appendChild(tb_row16_colh);

				var tb_row17_colh = document.createElement('td');
				tb_row17_colh.innerHTML = 'Off-Peak Energy:Off-Peak Network';
				tb_row17_colh.style = tb_f_width;
				tb_row17.appendChild(tb_row17_colh);

				var tb_row18_colh = document.createElement('td');
				tb_row18_colh.innerHTML = 'Export Electricity';
				tb_row18_colh.style = tb_f_width;
				tb_row18.appendChild(tb_row18_colh);

				if (ttype == 'LGC') {
					var tb_row19_colh = document.createElement('td');
					tb_row19_colh.innerHTML = 'LGC Financial Incentive';
					tb_row19_colh.style = tb_f_width;
					tb_row19.appendChild(tb_row19_colh);
				}

				if (IsDemandCharge == '1') {
					var tb_row20_colh = document.createElement('td');
					tb_row20_colh.innerHTML = 'Demand Charges';
					tb_row20_colh.style = tb_f_width;
					tb_row20.appendChild(tb_row20_colh);
				}

				if (billType == 'flat') {
					var tb_row21_colh = document.createElement('td');
					tb_row21_colh.innerHTML = 'Flat Rates';
					tb_row21_colh.style = tb_f_width;
					tb_row21.appendChild(tb_row21_colh);
				}

				var tb_row22_colh = document.createElement('td');
				tb_row22_colh.innerHTML = 'Shoulder';
				tb_row22_colh.style = tb_f_width;
				tb_row22.appendChild(tb_row22_colh);

				for (var i = 0; i < stat.calculate_years; i++) {
					var tb_row1_col = document.createElement('td');
					tb_row1_col.innerHTML = i + 1;
					tb_row1.appendChild(tb_row1_col);

					var tb_row2_col = document.createElement('td');
					tb_row2_col.innerHTML = parseFloat(
						stat.final_calculation.FinalEnergyProvidedBySolar[i]
					).toFixed(2);
					tb_row2.appendChild(tb_row2_col);

					var tb_row3_col = document.createElement('td');
					tb_row3_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalPeakRate[i]
						).toFixed(6);
					tb_row3.appendChild(tb_row3_col);

					var tb_row4_col = document.createElement('td');
					tb_row4_col.innerHTML = parseFloat(
						stat.final_calculation.FinalOffsetValue[i]
					).toFixed(2);
					tb_row4.appendChild(tb_row4_col);

					// var tb_row5_col = document.createElement('td');
					// tb_row5_col.innerHTML = '';
					// tb_row5.appendChild(tb_row5_col);

					var tb_row6_col = document.createElement('td');
					tb_row6_col.innerHTML =
						stat.final_calculation.FinalIncreaseRate[i] + '%';
					tb_row6.appendChild(tb_row6_col);

					// var tb_row7_col = document.createElement('td');
					// tb_row7_col.innerHTML = '';
					// tb_row7.appendChild(tb_row7_col);

					var tb_row8_col = document.createElement('td');
					tb_row8_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalLoadChargesWithoutSolar[
								i
							]
						).toFixed(2);
					tb_row8.appendChild(tb_row8_col);

					var tb_row9_col = document.createElement('td');
					tb_row9_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalWithoutStanding[i]
						).toFixed(2);
					tb_row9.appendChild(tb_row9_col);

					var tb_row10_col = document.createElement('td');
					tb_row10_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalLoadChargesWithoutSolar[
								i
							] + stat.final_calculation.FinalWithoutStanding[i]
						).toFixed(2);
					tb_row10.appendChild(tb_row10_col);

					// var tb_row11_col = document.createElement('td');
					// tb_row11_col.innerHTML = '';
					// tb_row11.appendChild(tb_row11_col);

					var tb_row12_col = document.createElement('td');
					tb_row12_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalBillAfterSolar[i]
						).toFixed(2);
					tb_row12.appendChild(tb_row12_col);

					var tb_row13_col = document.createElement('td');
					tb_row13_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalWithoutStanding[i]
						).toFixed(2);
					tb_row13.appendChild(tb_row13_col);

					var tb_row14_col = document.createElement('td');
					tb_row14_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalBillAfterSolar[i] +
								stat.final_calculation.FinalWithoutStanding[i]
						).toFixed(2);
					tb_row14.appendChild(tb_row14_col);

					var tb_row15_col = document.createElement('td');
					tb_row15_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalLoadChargesWithoutSolar[
								i
							] +
								stat.final_calculation.FinalWithoutStanding[i] -
								(stat.final_calculation.FinalBillAfterSolar[i] +
									stat.final_calculation.FinalWithoutStanding[
										i
									])
						).toFixed(2);
					tb_row15.appendChild(tb_row15_col);

					var tb_row16_col = document.createElement('td');
					tb_row16_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalCummilativeSavings[i]
						).toFixed(2);
					tb_row16.appendChild(tb_row16_col);

					var tb_row17_col = document.createElement('td');
					tb_row17_col.innerHTML =
						'$' +
						+parseFloat(
							stat.final_calculation.FinalOffPeakRate[i]
						).toFixed(6);
					tb_row17.appendChild(tb_row17_col);

					var tb_row18_col = document.createElement('td');
					tb_row18_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalExportElectricty[i]
						).toFixed(6);
					tb_row18.appendChild(tb_row18_col);

					if (ttype == 'LGC') {
						var tb_row19_col = document.createElement('td');
						tb_row19_col.innerHTML =
							'$' +
							parseFloat(
								stat.final_calculation.FinalLGCValues[i]
							).toFixed(2);
						tb_row19_col.style = tb_f_width;
						tb_row19.appendChild(tb_row19_col);
					}

					if (IsDemandCharge == '1') {
						var tb_row20_col = document.createElement('td');
						tb_row20_col.innerHTML =
							'$' +
							parseFloat(
								stat.final_calculation.FinalIncreaseDemandData[
									i
								][0]
							).toFixed(2);
						tb_row20_col.style = tb_f_width;
						tb_row20.appendChild(tb_row20_col);
					}

					if (billType == 'flat') {
						var tb_row21_col = document.createElement('td');
						tb_row21_col.innerHTML =
							'$' +
							parseFloat(
								stat.final_calculation.FinalFlatRate[i]
							).toFixed(3);
						tb_row21_col.style = tb_f_width;
						tb_row21.appendChild(tb_row21_col);
					}

					var tb_row22_col = document.createElement('td');
					tb_row22_col.innerHTML =
						'$' +
						parseFloat(
							stat.final_calculation.FinalShoulderRate[i]
						).toFixed(6);
					tb_row22.appendChild(tb_row22_col);
				}

				tb_table.appendChild(tb_row1);
				tb_table.appendChild(tb_row6);
				tb_table.appendChild(tb_row5);
				if (billType == 'flat') {
					tb_table.appendChild(tb_row21);
				}

				if (billType != 'flat') {
					tb_table.appendChild(tb_row3);
					tb_table.appendChild(tb_row22);
					tb_table.appendChild(tb_row17);
				}

				tb_table.appendChild(tb_row18);
				tb_table.appendChild(tb_row2);
				tb_table.appendChild(tb_row4);
				tb_table.appendChild(tb_row7);
				tb_table.appendChild(tb_row8);
				tb_table.appendChild(tb_row9);
				tb_table.appendChild(tb_row10);
				tb_table.appendChild(tb_row11);
				tb_table.appendChild(tb_row12);
				tb_table.appendChild(tb_row13);
				tb_table.appendChild(tb_row14);
				tb_table.appendChild(tb_row15);
				if (ttype == 'LGC') {
					tb_table.appendChild(tb_row19);
				}
				if (IsDemandCharge == '1') {
					tb_table.appendChild(tb_row20);
				}
				tb_table.appendChild(tb_row16);
				tb_table.className = 'tb_view_table';

				document.getElementById('viewTable').innerHTML = '';
				document.getElementById('viewTable').appendChild(tb_table);

				if (popup) {
					$('#calculate_degration_model').modal('show');
				}
				// 	wattageCapacity;
			} else {
				toastr['error'](stat.status);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.saving_solar_system_details = function () {
	var self = this;
	var system_pricing_battery_id = $('#system_pricing_battery')
		.find(':selected')
		.attr('data-id');

	system_pricing_battery_id =
		system_pricing_battery_id === undefined
			? ''
			: system_pricing_battery_id;
	var system_pricing_panel = $('#system_pricing_panel')
		.find(':selected')
		.attr('data-item');
	var system_pricing_panel_id = $('#system_pricing_panel')
		.find(':selected')
		.attr('data-id');
	var system_pricing_battery = $('#system_pricing_battery')
		.find(':selected')
		.attr('data-item');
	system_pricing_battery =
		system_pricing_battery === undefined ? '' : system_pricing_battery;
	var system_pricing_inverter_id = $('#system_pricing_inverter')
		.find(':selected')
		.attr('data-id');
	var system_pricing_inverter = $('#system_pricing_inverter')
		.find(':selected')
		.attr('data-item');
	var formData = $('#solarSystemDetails').serialize();
	formData += '&proposal_id=' + self.proposal_data.id;
	formData += '&system_pricing_battery_id=' + system_pricing_battery_id;
	formData +=
		'&system_pricing_panel_val=' + $('#system_pricing_panel_val').val();
	formData += '&system_pricing_panel_id=' + system_pricing_panel_id;
	formData +=
		'&system_pricing_inverter_val=' +
		$('#system_pricing_inverter_val').val();
	formData += '&system_pricing_inverter_id=' + system_pricing_inverter_id;
	formData +=
		'&system_pricing_battery_val=' + $('#system_pricing_battery_val').val();
	formData +=
		'&system_pricing_battery_size=' +
		$('#system_pricing_battery_size').val();
	formData += '&system_pricing_panel=' + system_pricing_panel;
	formData += '&system_pricing_battery=' + system_pricing_battery;
	formData += '&system_pricing_inverter=' + system_pricing_inverter;
	$.ajax({
		url: base_url + 'admin/proposal/commercial/saving_solar_system_details',
		type: 'post',
		data: formData,
		dataType: 'json',
		beforeSend: function () {
			$.isLoading({
				text: 'Loading data, Please Wait....',
			});
		},
		success: function (stat) {
			$.isLoading('hide');
			if (stat.success == true) {
				toastr['success'](stat.status);
			} else {
				toastr['error'](stat.status);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			toastr['error'](
				thrownError +
					'\r\n' +
					xhr.statusText +
					'\r\n' +
					xhr.responseText
			);
		},
	});
};

solar_proposal_manager.prototype.save_bill_details = function () {
	var self = this;
	// Panel Design
	if (self.commercialTabPanelId == 1) {
		if (self.validate_solar_inputs(self.commercialTabPanelId)) {
			self.saving_solar_system_details();
		}
		if (self.validate_solar_inputs(self.commercialTabPanelId)) {
			if (self.isProductionFile) {
				var formDatat = new FormData(
					$('#importProductionDataFileSubmit')[0]
				);

				$.ajax({
					url:
						base_url +
						'admin/proposal/commercial/upload_production_data/' +
						self.proposal_data.id,
					type: 'post',
					data: formDatat,
					processData: false,
					contentType: false,
					cache: false,
					// async: false,
					beforeSend: function () {
						$.isLoading({
							text: 'Loading data, Please Wait....',
						});
					},
					success: function (stat) {
						var output = JSON.parse(stat);
						// console.log(output.success);
						if (output.success == true) {
							self.solar_production_graph_ploting(stat);
						} else {
							toastr['error'](output.status);
						}
						$.isLoading('hide');
					},
				});
			} else {
				self.get_mapping_tool_data(true);
			}
		}
	}

	// Bill Details
	if (self.commercialTabPanelId == 2) {
		if (self.validate_solar_inputs(self.commercialTabPanelId)) {
			if ($('#utility_value_export').val() == '') {
				toastr.remove();
				toastr['error']('Export rate required data is missing tt.');
				return false;
			} else {
				// console.log($('#meterDataFileURL')[0].files.length);
				var formDatat = new FormData(
					$('#importMeterDataFileSubmit')[0]
				);
				$.ajax({
					url:
						base_url +
						'admin/proposal/commercial/upload_meter_data',
					type: 'post',
					data: formDatat,
					processData: false,
					contentType: false,
					cache: false,
					beforeSend: function () {
						$.isLoading({
							text: 'Loading data, Please Wait....',
						});
					},
					success: function (stat) {
						var stat = JSON.parse(stat);
						console.log(stat);
						if (stat.success == true) {
							$.isLoading('hide');
							var daily_average_production = stat;
							show_production(
								daily_average_production.typical_graph
							);
							show_production1(
								daily_average_production.worst_graph
							);
							show_production2(
								daily_average_production.high_graph
							);
							show_production3(
								daily_average_production.best_graph
							);
						} else {
							$.isLoading('hide');
							toastr['error'](stat.status);
						}
					},
				});
			}
		}
	}

	// if (self.commercialTabPanelId == 4) {
	// 	if (self.validate_solar_inputs(self.commercialTabPanelId)) {
	// 		self.saving_solar_system_details();
	// 	}
	// }
};

solar_proposal_manager.prototype.view_proposal_action = function () {
	var self = this;
	if (self.commercialTabPanelId == 1) {
		if ($('#pv_system_size').val() == '0.00') {
			toastr.remove();
			toastr['error']('Panel Design required data is missing.');
			return false;
		}
	}

	if (self.commercialTabPanelId == 2) {
		if ($('#utility_value_export').val() == '') {
			toastr.remove();
			toastr['error']('Export rate required data is missing.');
			return false;
		} else if (
			$('#meterDataFileURL').val() == '' &&
			$('#is_meter_data').val() == 0
		) {
			toastr.remove();
			toastr['error']('Upload file is missing.');
			return false;
		} else {
		}
	}

	var url =
		base_url +
		'admin/proposal/commercial/view_proposal/' +
		self.proposal_data.id;
	window.open(url, '_blank');
};

solar_proposal_manager.prototype.get_mapping_tool_data = function (
	showing_graph
) {
	var self = this;
	var pvwattsData = [];
	var dc_system_size = 0;
	var url =
		base_url +
		'admin/proposal/commercial/getMappingToolData?proposal_id=' +
		self.proposal_data.id;
	$.ajax({
		url: url,
		type: 'GET',
		async: true,
		beforeSend: function () {},
		success: function (result) {
			if (result) {
				var mapToolData = JSON.parse(result);
				var panelOjects = mapToolData.panel_objects;
				// console.log(panelOjects);
				if (panelOjects.length > 0) {
					var panelObjectsArray = [];
					// panelObjectsArray[0] = JSON.parse(
					// 	panelOjects[0].object_properties
					// );

					var gettingDifferentLayout = [];

					// begin:: Getting Differnt Object Properties array
					var count = 0;
					gettingDifferentLayout[0] = JSON.parse(
						panelOjects[0].object_properties
					);
					for (var i = 0; i < panelOjects.length; i++) {
						var panelObjectsPropertiesData = JSON.parse(
							panelOjects[i].object_properties
						);
						if (
							gettingDifferentLayout[count].sp_tilt !=
								panelObjectsPropertiesData.sp_tilt ||
							gettingDifferentLayout[count].sp_rotation !=
								panelObjectsPropertiesData.sp_rotation
						) {
							count++;
							gettingDifferentLayout[count] = JSON.parse(
								panelOjects[i].object_properties
							);
						}
					}
					// end:: Getting Differnt Object Properties array

					for (
						let index = 0;
						index < gettingDifferentLayout.length;
						index++
					) {
						var arr_count = 0;
						// panelObjectsArray[index] = JSON.parse(
						// 	panelOjects[index].object_properties
						// );

						var panelObjectTilt =
							gettingDifferentLayout[index].sp_tilt;
						var panelObjectRotation =
							gettingDifferentLayout[index].sp_rotation;
						var panelObjectWatt =
							gettingDifferentLayout[index].sp_watt;

						for (let k = 0; k < panelOjects.length; k++) {
							var panelObjectsPropertiesData = JSON.parse(
								panelOjects[k].object_properties
							);
							// console.log(
							// 	panelObjectTilt +
							// 		' === ' +
							// 		panelObjectsPropertiesData.sp_tilt +
							// 		' &&  ' +
							// 		panelObjectRotation +
							// 		'  ===  ' +
							// 		panelObjectsPropertiesData.sp_rotation
							// );
							if (
								panelObjectTilt ===
									panelObjectsPropertiesData.sp_tilt &&
								panelObjectRotation ===
									panelObjectsPropertiesData.sp_rotation
							) {
								arr_count++;
							}

							if (k === panelOjects.length - 1) {
								dc_system_size =
									dc_system_size +
									panelObjectWatt * arr_count;
								pvwattsData.push({
									pv_system_size: parseFloat(
										(panelObjectWatt * arr_count) / 1000
									).toFixed(2),
									pv_tilt: panelObjectTilt,
									pv_azimuth: panelObjectRotation,
									pv_lat: mapToolData.mtool_data.latitude,
									pv_lng: mapToolData.mtool_data.longitude,
									pv_module_type: $('#pv_module_type').val(),
									pv_array_type: $('#pv_array_type').val(),
									pv_system_losses:
										$('#pv_system_losses').val(),
									pv_ac_dc_ratio: $('#pv_ac_dc_ratio').val(),
									pv_degradation_factor: $(
										'#pv_degradation_factor'
									).val(),
									pv_inverter_efficiency: $(
										'#pv_inverter_efficiency'
									).val(),
									pv_ground_coverage: $(
										'#pv_ground_coverage'
									).val(),
								});
							}
						}
					}

					// console.log(gettingDifferentLayout[0].sp_panel_id);

					self.proposal_data.pvwatts_data = pvwattsData;
					$(
						"#system_pricing_panel option[data-id='" +
							gettingDifferentLayout[0].sp_panel_id +
							"']"
					).prop('selected', 'selected');

					var system_data_degradation = $(
						"#system_pricing_panel option[data-id='" +
							gettingDifferentLayout[0].sp_panel_id +
							"']"
					).attr('data-degradation');
					if (system_data_degradation !== null) {
						$('#pv_degradation_factor').val(
							parseFloat(system_data_degradation)
						);
					} else {
						$('#pv_degradation_factor').val('');
					}

					$('#pv_system_size').val(dc_system_size / 1000);
					$('#system_pricing_panel_val').val(panelOjects.length);
					self.restrictLoaderPvwattsApi = 1;
					if (showing_graph) {
						self.fetch_pvwatts_data_api();
					}
					if (self.financial_summary_data.type == 'VEEC') {
						self.calculate_veec_discount(false);
					}
				}

				// if (panelOjects.length > 0) {
				// 	console.log(panelOjects);

				// 	var panelObjectsArray = [];
				// 	panelObjectsArray[0] = JSON.parse(
				// 		panelOjects[0].object_properties
				// 	);

				// 	for (
				// 		let index = 0;
				// 		index < panelObjectsArray.length;
				// 		index++
				// 	) {
				// 		var arr_count = 0;
				// 		panelObjectsArray[0] = JSON.parse(
				// 			panelOjects[0].object_properties
				// 		);

				// 		var panelObjectTilt = panelObjectsArray[index].sp_tilt;
				// 		var panelObjectRotation =
				// 			panelObjectsArray[index].sp_rotation;
				// 		var panelObjectWatt = panelObjectsArray[index].sp_watt;

				// 		for (let k = 0; k < panelOjects.length; k++) {
				// 			var panelObjectsPropertiesData = JSON.parse(
				// 				panelOjects[k].object_properties
				// 			);
				// 			if (
				// 				panelObjectTilt ==
				// 					panelObjectsPropertiesData.sp_tilt &&
				// 				panelObjectRotation ==
				// 					panelObjectsPropertiesData.sp_rotation
				// 			) {
				// 				arr_count++;
				// 			} else {
				// 				panelObjectsArray[index + 1] =
				// 					panelObjectsPropertiesData;
				// 				pvwattsData.push({
				// 					pv_system_size: parseFloat(
				// 						(panelObjectsPropertiesData[k].sp_watt *
				// 							arr_count) /
				// 							1000
				// 					).toFixed(2),
				// 					pv_tilt: panelObjectTilt,
				// 					pv_azimuth: panelObjectRotation,
				// 					pv_lat: mapToolData.mtool_data.latitude,
				// 					pv_lng: mapToolData.mtool_data.longitude,
				// 					pv_module_type: $('#pv_module_type').val(),
				// 					pv_array_type: $('#pv_array_type').val(),
				// 					pv_system_losses:
				// 						$('#pv_system_losses').val(),
				// 					pv_ac_dc_ratio: $('#pv_ac_dc_ratio').val(),
				// 					pv_degradation_factor: $(
				// 						'#pv_degradation_factor'
				// 					).val(),
				// 					pv_inverter_efficiency: $(
				// 						'#pv_inverter_efficiency'
				// 					).val(),
				// 					pv_ground_coverage: $(
				// 						'#pv_ground_coverage'
				// 					).val(),
				// 				});
				// 			}
				// 		}

				// 		if (panelObjectsArray.length == 1) {
				// 			pvwattsData.push({
				// 				pv_system_size: parseFloat(
				// 					(panelObjectWatt * arr_count) / 1000
				// 				).toFixed(2),
				// 				pv_tilt: panelObjectTilt,
				// 				pv_azimuth: panelObjectRotation,
				// 				pv_lat: mapToolData.mtool_data.latitude,
				// 				pv_lng: mapToolData.mtool_data.longitude,
				// 				pv_module_type: $('#pv_module_type').val(),
				// 				pv_array_type: $('#pv_array_type').val(),
				// 				pv_system_losses: $('#pv_system_losses').val(),
				// 				pv_ac_dc_ratio: $('#pv_ac_dc_ratio').val(),
				// 				pv_degradation_factor: $(
				// 					'#pv_degradation_factor'
				// 				).val(),
				// 				pv_inverter_efficiency: $(
				// 					'#pv_inverter_efficiency'
				// 				).val(),
				// 				pv_ground_coverage: $(
				// 					'#pv_ground_coverage'
				// 				).val(),
				// 			});
				// 		}
				// 	}

				// 	console.log(pvwattsData);
				// self.proposal_data.pvwatts_data = pvwattsData;

				// $(
				// 	"#system_pricing_panel[data-id='" +
				// 		panelObjectsArray[0].sp_panel_id +
				// 		"']"
				// ).prop('selected', 'selected');
				// 	$('#system_pricing_panel_val').val(panelOjects.length);
				// 	$(
				// 		"#system_pricing_panel option[data-id='" +
				// 			panelObjectsArray[0].sp_panel_id +
				// 			"']"
				// 	).attr('selected', 'selected');

				// 	self.restrictLoaderPvwattsApi = 1;
				// self.fetch_pvwatts_data_api();

				// 	$('#pvwatts_data').val(JSON.stringify(pvwattsData));
				// } else {
				// 	$('#pv_system_size').val(0.0);
				// }
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError);
		},
	});
};

solar_proposal_manager.prototype.get_data_system_details = function (
	maptool_data
) {
	var self = this;
	// console.log(maptool_data);
};

// getPvwattCalcAPI
solar_proposal_manager.prototype.fetch_pvwatts_data_api = function () {
	var self = this;

	var pv_dd = self.proposal_data.pvwatts_data;
	var proposal_d_id = self.proposal_data.id;
	var pvwatts_data_html = '';

	var url = base_url + 'admin/proposal/commercial/pvwatts';

	if (pv_dd) {
		for (let index = 0; index < pv_dd.length; index++) {
			var pv_method = `dataset=intl&system_capacity=${pv_dd[index].pv_system_size}&module_type=${pv_dd[index].pv_module_type}&losses=${pv_dd[index].pv_system_losses}&array_type=${pv_dd[index].pv_array_type}&tilt=${pv_dd[index].pv_tilt}&azimuth=${pv_dd[index].pv_azimuth}&lat=${pv_dd[index].pv_lat}&lon=${pv_dd[index].pv_lng}&timeframe=hourly`;

			$.ajax({
				url: url,
				type: 'post',
				async: true,
				data: {
					method: pv_method,
					pvwatts: JSON.stringify(pv_dd),
					total_system_size: pv_dd[index].pv_system_size,
					proposal_data_id: proposal_d_id,
				},
				beforeSend: function () {
					if (self.restrictLoaderPvwattsApi === 1) {
						$.isLoading({
							text: 'Loading data, Please Wait....',
						});
					}
					self.restrictLoaderPvwattsApi = 1;
				},
				success: function (result) {
					$.isLoading('hide');
					self.solar_production_graph_ploting(result);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(thrownError);
				},
			});
		}
	}
};

solar_proposal_manager.prototype.solar_production_graph_ploting = function (
	result
) {
	var self = this;
	self.pvSt_avg = 0;
	self.pvSt_month1 = 0;
	self.pvSt_month2 = 0;
	self.pvSt_month3 = 0;
	self.pvSt_month4 = 0;
	self.pvSt_month5 = 0;
	self.pvSt_month6 = 0;
	self.pvSt_month7 = 0;
	self.pvSt_month8 = 0;
	self.pvSt_month9 = 0;
	self.pvSt_month10 = 0;
	self.pvSt_month11 = 0;
	self.pvSt_month12 = 0;
	self.pvtotal_production = 0;
	self.pvSt_avg_prev = 0;
	self.pvSt_avg = 0;

	var pv_output = JSON.parse(result);
	// console.log(pv_output);
	self.pvSt_month1 = self.pvSt_month1 + pv_output.outputs.ac_monthly[0];
	self.pvSt_month2 = self.pvSt_month2 + pv_output.outputs.ac_monthly[1];
	self.pvSt_month3 = self.pvSt_month3 + pv_output.outputs.ac_monthly[2];
	self.pvSt_month4 = self.pvSt_month4 + pv_output.outputs.ac_monthly[3];
	self.pvSt_month5 = self.pvSt_month5 + pv_output.outputs.ac_monthly[4];
	self.pvSt_month6 = self.pvSt_month6 + pv_output.outputs.ac_monthly[5];
	self.pvSt_month7 = self.pvSt_month7 + pv_output.outputs.ac_monthly[6];
	self.pvSt_month8 = self.pvSt_month8 + pv_output.outputs.ac_monthly[7];
	self.pvSt_month9 = self.pvSt_month9 + pv_output.outputs.ac_monthly[8];
	self.pvSt_month10 = self.pvSt_month10 + pv_output.outputs.ac_monthly[9];
	self.pvSt_month11 = self.pvSt_month11 + pv_output.outputs.ac_monthly[10];
	self.pvSt_month12 = self.pvSt_month12 + pv_output.outputs.ac_monthly[11];

	self.pvSt_avg =
		self.pvSt_month1 +
		self.pvSt_month2 +
		self.pvSt_month3 +
		self.pvSt_month4 +
		self.pvSt_month5 +
		self.pvSt_month6 +
		self.pvSt_month7 +
		self.pvSt_month8 +
		self.pvSt_month9 +
		self.pvSt_month10 +
		self.pvSt_month11 +
		self.pvSt_month12;

	self.pvSt_avg = Math.round(self.pvSt_avg / 12);
	self.pvSt_avg_prev = self.pvSt_avg_prev + self.pvSt_avg;
	self.pvSt_avg = self.pvSt_avg_prev;

	//  parseInt(pvtotal_production) +
	self.pvtotal_production = parseInt(pv_output.outputs.ac_annual);
	// console.log(pvSt_month1)

	$('#pvtotal_production span').text(
		self.dollarUSLocale.format(self.pvtotal_production.toFixed(0)) + '  kWh'
	);
	$('#pvaverage_production span').text(
		self.dollarUSLocale.format(self.pvSt_avg.toFixed(0)) + '  kWh'
	);
	$('#pvdaily_average_production span').text(
		self.dollarUSLocale.format((self.pvtotal_production / 365).toFixed(0)) +
			'  kWh'
	);

	google.setOnLoadCallback(self.draw_chart());
};

solar_proposal_manager.prototype.saving_pvwatts_Data = function () {
	var self = this;
	$.ajax({
		url: base_url + 'admin/proposal/commercial/upload_meter_data',
		type: 'post',
		data: formDatat,
		processData: false,
		contentType: false,
		cache: false,
		// async: false,
		beforeSend: function () {
			$.isLoading({
				text: 'Loading data, Please Wait....',
			});
		},
		success: function (data) {
			// console.log(JSON.parse(data));
			$.isLoading('hide');
			var daily_average_production = JSON.parse(data);
			show_production(daily_average_production.typical_graph);
			show_production1(daily_average_production.worst_graph);
			show_production2(daily_average_production.high_graph);
			show_production3(daily_average_production.best_graph);

			// if ($("#meterDataFileURL").val() != "") {
			// 	base_url + "admin/customer/save_customer",
			// 		$("#meterDataFileURL").after(
			// 			'<a class="d-block mt-2" href="http://localhost/kuga//assets/uploads/meter_data_files/166e566f57fddbd638c4b0d157cb9841.xlsx" target="_blank">166e566f57fddbd638c4b0d157cb9841.xlsx</a>'
			// 		);
			// }
		},
	});
};

solar_proposal_manager.prototype.draw_chart = function () {
	var self = this;
	var data = google.visualization.arrayToDataTable([
		['Year', 'kWh', 'Average'],
		['JAN', Math.round(self.pvSt_month1), self.pvSt_avg],
		['FEB', Math.round(self.pvSt_month2), self.pvSt_avg],
		['MAR', Math.round(self.pvSt_month3), self.pvSt_avg],
		['APR', Math.round(self.pvSt_month4), self.pvSt_avg],
		['MAY', Math.round(self.pvSt_month5), self.pvSt_avg],
		['JUN', Math.round(self.pvSt_month6), self.pvSt_avg],
		['JUL', Math.round(self.pvSt_month7), self.pvSt_avg],
		['AUG', Math.round(self.pvSt_month8), self.pvSt_avg],
		['SEP', Math.round(self.pvSt_month9), self.pvSt_avg],
		['OCT', Math.round(self.pvSt_month10), self.pvSt_avg],
		['NOV', Math.round(self.pvSt_month11), self.pvSt_avg],
		['DEC', Math.round(self.pvSt_month12), self.pvSt_avg],
	]);
	var options = {
		curveType: 'function',
		titleTextStyle: {
			fontName: 'Arial, sans-serif;',
			italic: false,
			bold: false,
			fontStyle: 'normal',
			fontSize: 12,
		},
		legend: {
			position: 'bottom',
			textStyle: {
				fontName: 'Arial, sans-serif;',
				italic: false,
				bold: false,
				fontSize: 14,
				fontStyle: 'normal',
			},
		},
		hAxis: {
			format: "#'$'",
			titleTextStyle: {
				fontName: 'Arial, sans-serif;',
				italic: false,
				bold: false,
				fontSize: 11,
				fontStyle: 'normal',
			},
			textStyle: {
				fontName: 'Arial, sans-serif;',
				italic: false,
				bold: false,
				fontSize: 11,
				fontStyle: 'normal',
			},
		},
		vAxis: {
			format: "#'kWh'",
			title: '',
			titleTextStyle: {
				fontName: 'Arial, sans-serif;',
				italic: false,
				bold: true,
				fontSize: 13,
				fontStyle: 'normal',
			},
			textStyle: {
				fontName: 'Arial, sans-serif;',
				italic: false,
				bold: false,
				fontSize: 11,
				fontStyle: 'normal',
			},
		},
		width: 810,
		height: 400,
		legend: {
			position: 'none',
			textStyle: {
				fontSize: 4,
			},
		},
		seriesType: 'bars',
		series: {
			0: {
				color: '#c52428',
			},
			1: {
				type: 'line',
				color: '#000',
				visibleInLegend: false,
			},
		},
	};
	var chart = new google.visualization.ComboChart(
		document.getElementById('avg_kw_per_day_chart')
	);
	chart.draw(data, options);
};

solar_proposal_manager.prototype.demand_charge_with_meter_data = function () {
	var self = this;

	var getDemanChargeRow = $('.demand-charge-row').length;

	if (getDemanChargeRow == 0) {
		var demandChargetTemplateHeader = `<div class="demand-wrapper-tb-row">
		<div class="demand-wrapper-tb-col"></div>
		<div class="demand-wrapper-tb-col"><strong>Rate</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Month</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Days</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Time Interval</strong></div>
		</div>`;
		$('#demandChargeWrapper').append(demandChargetTemplateHeader);
	}

	var demandChargetTemplate = `<div class="demand-wrapper-tb-row demand-charge-row demand-charge-row-${
		getDemanChargeRow + 1
	}">
	<div class="demand-wrapper-tb-col">
	<strong>Demand Charge ${getDemanChargeRow + 1}</strong>
	</div>
	<div class="demand-wrapper-tb-col">
	<input class="form-control" type="text" name="meter_data_demand_charge${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	<div class="demand-wrapper-tb-col">
	<div class="btn-group-toggle btn-month-toggle" data-toggle="buttons">
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Jan"> Jan
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Feb"> Feb
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Mar"> Mar
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Apr"> Apr
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="May"> May
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Jun"> Jun
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Jul"> Jul
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Aug"> Aug
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Sep"> Sep
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Oct"> Oct
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Nov"> Nov
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}[]" value="Dec"> Dec
	</label>
	</div>
	</div>
	<div class="demand-wrapper-tb-col">
	<div class="btn-group-toggle btn-day-toggle" data-toggle="buttons">
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Mon"> M
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Tue"> Tu
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Wed"> W
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Thu"> Th
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Fri"> F
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Sat"> Sa
	</label>
	<label class="btn btn-secondary">
	<input type="checkbox" name="meter_data_demand_day${
		getDemanChargeRow + 1
	}[]" value="Sun"> S
	</label>
	</div>
	</div>
	<div class="demand-wrapper-tb-col">
	<input class="form-control" type="time" name="meter_data_demand_time_start${
		getDemanChargeRow + 1
	}" value="" required="">
	<span class="to">to</span>
	<input class="form-control" type="time" name="meter_data_demand_time_end${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	</div>`;

	// if ($("#meter_upload_status").val() == 1) {
	// }

	$('#demandChargeWrapper').append(demandChargetTemplate);
	// console.log(getDemanChargeRow);
	if ($('.demand-charge-row').length > 1) {
		$('#demandRemoveRow').show();
		$('#demandChargeValue').val($('.demand-charge-row').length);
	}
};

solar_proposal_manager.prototype.demand_charge_without_meter_data =
	function () {
		var self = this;

		var getDemanChargeRow = $('.demand-charge-row').length;

		if (getDemanChargeRow == 0) {
			var demandChargetTemplateHeader = `<div class="demand-wrapper-tb-row">
		<div class="demand-wrapper-tb-col" style="width: 17.5rem;"></div>
		<div class="demand-wrapper-tb-col"><strong>kWh</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Rate</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Months</strong></div>
		<div class="demand-wrapper-tb-col"><strong>Amount</strong></div>
		</div>`;
			$('#demandChargeWrapper').append(demandChargetTemplateHeader);
		}

		var demandChargetTemplate = `<div class="demand-wrapper-tb-row demand-charge-row demand-charge-row-${
			getDemanChargeRow + 1
		}">
	<div class="demand-wrapper-tb-col" style="width: 17.5rem;">
	<strong>Demand Charge ${getDemanChargeRow + 1}</strong>
	</div>
	<div class="demand-wrapper-tb-col">
	<input class="form-control" type="text" name="meter_data_demand_charge${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	<div class="demand-wrapper-tb-col">
	<input class="form-control" type="text" name="meter_data_demand_rate${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	<div class="demand-wrapper-tb-col">

	<input class="form-control" type="text" name="meter_data_demand_month${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	<div class="demand-wrapper-tb-col">
	<input class="form-control" type="text" name="meter_data_demand_amount${
		getDemanChargeRow + 1
	}" value="" required="">
	</div>
	</div>`;

		// if ($("#meter_upload_status").val() == 1) {
		// }

		$('#demandChargeWrapper').append(demandChargetTemplate);
		// console.log(getDemanChargeRow);
		if ($('.demand-charge-row').length > 1) {
			$('#demandRemoveRow').show();
			$('#demandChargeValue').val($('.demand-charge-row').length);
		}
	};

<?php

class GoogleServiceAccount_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    
    
   
    function create_google_account_for_user($data) {
       $userid = $data['user_id'];
       $service_id = $data['service_id'];
       $status = $data['status'];
       $if_exists_google_account = $this->common->fetch_where('google_service_accounts','*',array('service_id' => $service_id, 'user_id' => $userid));
       if(count($if_exists_google_account) > 0){
          $stat = $this->common->update_data('google_service_accounts',array('service_id' => $service_id, 'user_id' => $userid),$data);
       }else{
           $stat = $this->common->insert_data('google_service_accounts',$data);
       }
       return $stat;
    }
    
    
    
   
    

    
}

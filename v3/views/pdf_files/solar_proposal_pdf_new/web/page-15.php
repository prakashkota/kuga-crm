<html>

<head>
    <title></title>
    <style>
        .page15 .back-bg {
            background-color: #fff;
            background-repeat: no-repeat;
            margin: auto;
            padding: 0;
            background-size: 100% 100%;
            max-width: 910px;
            max-height: 1287px;
        }

        .page15 .top h2 {
            font-size: 16px;
            color: #221E1F;
            padding-left: 654px;
            padding-top: 30px;
        }

        .page15 .top hr {
            width: 65%;
            height: 2px;
            margin: -24px 0px 0px 52px;
            background-color: #D01E2A;
        }

        .page15 .text_color {
            color: #D01E2A;
        }

        .page15 .title {
            margin-top: -50px;
        }

        .page15 .title h1 {
            padding-left: 14px;
            margin-left: 48px;
            margin-top: 90px;
            border-left: 5px solid #D01E2A;
            border-height: 10px;
        }

        .page15 .plasting h2 {
            font-size: 14px;
            padding: 10px;
            letter-spacing: 3px;
            margin-left: 48px;
            margin-top: 100px;
            background-color: #D01E2A;
            width: 90%;
            display: flex;
            align-items: center;
            color: #ffffff;
            background: linear-gradient(105deg, #D01E2A 87%, transparent 15%);
        }

        .page15 .plasting h2 svg {
            margin-right: 15px;
            margin-left: 5px;
            fill: #fff;
        }

        .page15 .displayyy {
            display: inline-flex;
        }

        .page15 .mt45 {
            margin-top: -45px;
        }

        .page15 .footer {
            display: inline-flex;
        }

        .page15 .redf {
            height: 10px;
            width: 455px;
            background: red;
        }

        .page15 .greyf {
            height: 10px;
            width: 455px;
            background: #EDEBEC;
        }

        .page15 .iiimage img {
            width: 384px;
            margin-left: 48px;
            margin-top: 19px;
        }

        .page15 .fimagee img {
            margin-left: 730px;
            margin-top: 33px;
        }

        #typical_export_graph,
        #worst_export_graph,
        #high_export_graph,
        #best_export_graph {
            margin-left: 2rem;
            margin-top: 2rem;
        }
    </style>
</head>

<body>
    <div class="back-bg page15">
        <div class="top">
            <h2> <span class="text_color">SOLAR PROPOSAL</span> | KUGA</h1>
                <hr>
        </div>
        <div class="title">
            <h1>DAILY <span class="text_color"> AVERAGE PRODUCTION</span></h1>
        </div>
        <div class="displayyy mt45">
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    JANUARY - MARCH
                </h2>
                <div id="typical_export_graph"></div>
            </div>
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    APRIL - JUNE
                </h2>
                <div id="worst_export_graph"></div>
            </div>
        </div>
        <div class="displayyy mt45">
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    JULY - SEPTEMBER
                </h2>
                <div id="high_export_graph"></div>
            </div>
            <div class="iiimage plasting">
                <h2>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 9c1.654 0 3 1.346 3 3s-1.346 3-3 3v-6zm0-2c-2.762 0-5 2.238-5 5s2.238 5 5 5 5-2.238 5-5-2.238-5-5-5zm-4.184-.599l-3.593-3.594-1.415 1.414 3.595 3.595c.401-.537.876-1.013 1.413-1.415zm4.184-1.401c.34 0 .672.033 1 .08v-5.08h-2v5.08c.328-.047.66-.08 1-.08zm5.598 2.815l3.595-3.595-1.414-1.414-3.595 3.595c.537.402 1.012.878 1.414 1.414zm-12.598 4.185c0-.34.033-.672.08-1h-5.08v2h5.08c-.047-.328-.08-.66-.08-1zm11.185 5.598l3.594 3.593 1.415-1.414-3.594-3.593c-.403.536-.879 1.012-1.415 1.414zm-9.784-1.414l-3.593 3.593 1.414 1.414 3.593-3.593c-.536-.402-1.011-.877-1.414-1.414zm12.519-5.184c.047.328.08.66.08 1s-.033.672-.08 1h5.08v-2h-5.08zm-6.92 8c-.34 0-.672-.033-1-.08v5.08h2v-5.08c-.328.047-.66.08-1 .08z" />
                    </svg>
                    OCTOBER - DECEMBER
                </h2>
                <div id="best_export_graph"></div>
            </div>
        </div>
        <div class="fimagee mt45">
            <a href="https://www.13kuga.com.au/"><img src="<?php echo site_url('assets/solar_proposal_pdf_new/image/logo1.png'); ?>" width="150px"></a>
        </div>
        <div class="footer">
            <div class="redf">
            </div>
            <div class="greyf">
            </div>
        </div>
    </div>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script>
        var graph_data = <?php echo $saving_meter_calculation; ?>;
        var daily_average_production = <?php echo $saving_graph_calculation; ?>;

        $(document).ready(function() {
            console.log(<?php echo $time_of_use; ?>);
            // console.log(graph_data);
            // console.log(daily_average_production);

            show_production(daily_average_production.typical_graph);
            show_production1(daily_average_production.worst_graph);
            show_production2(daily_average_production.high_graph);
            show_production3(daily_average_production.best_graph);
        })

        function show_production(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var typical_export_load_graph_data = d.typical_export_load_graph_data;
            var typical_export_sol_prd_graph_data = d.typical_export_sol_prd_graph_data;
            var typical_export_export_graph_data = d.typical_export_export_graph_data;
            var typical_export_offset_graph_data = d.typical_export_offset_graph_data;

            Highcharts.chart('typical_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },

                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,
                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 3600,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 3,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        // pointStart: Date.UTC(2018, 11, 1, 0, 0, 0),
                        // pointInterval: 3780 * 1000
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            typical_export_load_graph_data[0],
                            typical_export_load_graph_data[1],
                            typical_export_load_graph_data[2],
                            typical_export_load_graph_data[3],
                            typical_export_load_graph_data[4],
                            typical_export_load_graph_data[5],
                            typical_export_load_graph_data[6],
                            typical_export_load_graph_data[7],
                            typical_export_load_graph_data[8],
                            typical_export_load_graph_data[9],
                            typical_export_load_graph_data[10],
                            typical_export_load_graph_data[11],
                            typical_export_load_graph_data[12],
                            typical_export_load_graph_data[13],
                            typical_export_load_graph_data[14],
                            typical_export_load_graph_data[15],
                            typical_export_load_graph_data[16],
                            typical_export_load_graph_data[17],
                            typical_export_load_graph_data[18],
                            typical_export_load_graph_data[19],
                            typical_export_load_graph_data[20],
                            typical_export_load_graph_data[21],
                            typical_export_load_graph_data[22],
                            typical_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    },
                    {
                        name: 'Solar Output',
                        data: [
                            typical_export_sol_prd_graph_data[0],
                            typical_export_sol_prd_graph_data[1],
                            typical_export_sol_prd_graph_data[2],
                            typical_export_sol_prd_graph_data[3],
                            typical_export_sol_prd_graph_data[4],
                            typical_export_sol_prd_graph_data[5],
                            typical_export_sol_prd_graph_data[6],
                            typical_export_sol_prd_graph_data[7],
                            typical_export_sol_prd_graph_data[8],
                            typical_export_sol_prd_graph_data[9],
                            typical_export_sol_prd_graph_data[10],
                            typical_export_sol_prd_graph_data[11],
                            typical_export_sol_prd_graph_data[12],
                            typical_export_sol_prd_graph_data[13],
                            typical_export_sol_prd_graph_data[14],
                            typical_export_sol_prd_graph_data[15],
                            typical_export_sol_prd_graph_data[16],
                            typical_export_sol_prd_graph_data[17],
                            typical_export_sol_prd_graph_data[18],
                            typical_export_sol_prd_graph_data[19],
                            typical_export_sol_prd_graph_data[20],
                            typical_export_sol_prd_graph_data[21],
                            typical_export_sol_prd_graph_data[22],
                            typical_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            typical_export_export_graph_data[0],
                            typical_export_export_graph_data[1],
                            typical_export_export_graph_data[2],
                            typical_export_export_graph_data[3],
                            typical_export_export_graph_data[4],
                            typical_export_export_graph_data[5],
                            typical_export_export_graph_data[6],
                            typical_export_export_graph_data[7],
                            typical_export_export_graph_data[8],
                            typical_export_export_graph_data[9],
                            typical_export_export_graph_data[10],
                            typical_export_export_graph_data[11],
                            typical_export_export_graph_data[12],
                            typical_export_export_graph_data[13],
                            typical_export_export_graph_data[14],
                            typical_export_export_graph_data[15],
                            typical_export_export_graph_data[16],
                            typical_export_export_graph_data[17],
                            typical_export_export_graph_data[18],
                            typical_export_export_graph_data[19],
                            typical_export_export_graph_data[20],
                            typical_export_export_graph_data[21],
                            typical_export_export_graph_data[22],
                            typical_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            typical_export_offset_graph_data[0],
                            typical_export_offset_graph_data[1],
                            typical_export_offset_graph_data[2],
                            typical_export_offset_graph_data[3],
                            typical_export_offset_graph_data[4],
                            typical_export_offset_graph_data[5],
                            typical_export_offset_graph_data[6],
                            typical_export_offset_graph_data[7],
                            typical_export_offset_graph_data[8],
                            typical_export_offset_graph_data[9],
                            typical_export_offset_graph_data[10],
                            typical_export_offset_graph_data[11],
                            typical_export_offset_graph_data[12],
                            typical_export_offset_graph_data[13],
                            typical_export_offset_graph_data[14],
                            typical_export_offset_graph_data[15],
                            typical_export_offset_graph_data[16],
                            typical_export_offset_graph_data[17],
                            typical_export_offset_graph_data[18],
                            typical_export_offset_graph_data[19],
                            typical_export_offset_graph_data[20],
                            typical_export_offset_graph_data[21],
                            typical_export_offset_graph_data[22],
                            typical_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });
        }

        function show_production1(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var worst_export_load_graph_data = d.worst_export_load_graph_data;
            var worst_export_sol_prd_graph_data = d.worst_export_sol_prd_graph_data;
            var worst_export_export_graph_data = d.worst_export_export_graph_data;
            var worst_export_offset_graph_data = d.worst_export_offset_graph_data;

            Highcharts.chart('worst_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {

                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            worst_export_load_graph_data[0],
                            worst_export_load_graph_data[1],
                            worst_export_load_graph_data[2],
                            worst_export_load_graph_data[3],
                            worst_export_load_graph_data[4],
                            worst_export_load_graph_data[5],
                            worst_export_load_graph_data[6],
                            worst_export_load_graph_data[7],
                            worst_export_load_graph_data[8],
                            worst_export_load_graph_data[9],
                            worst_export_load_graph_data[10],
                            worst_export_load_graph_data[11],
                            worst_export_load_graph_data[12],
                            worst_export_load_graph_data[13],
                            worst_export_load_graph_data[14],
                            worst_export_load_graph_data[15],
                            worst_export_load_graph_data[16],
                            worst_export_load_graph_data[17],
                            worst_export_load_graph_data[18],
                            worst_export_load_graph_data[19],
                            worst_export_load_graph_data[20],
                            worst_export_load_graph_data[21],
                            worst_export_load_graph_data[22],
                            worst_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            worst_export_sol_prd_graph_data[0],
                            worst_export_sol_prd_graph_data[1],
                            worst_export_sol_prd_graph_data[2],
                            worst_export_sol_prd_graph_data[3],
                            worst_export_sol_prd_graph_data[4],
                            worst_export_sol_prd_graph_data[5],
                            worst_export_sol_prd_graph_data[6],
                            worst_export_sol_prd_graph_data[7],
                            worst_export_sol_prd_graph_data[8],
                            worst_export_sol_prd_graph_data[9],
                            worst_export_sol_prd_graph_data[10],
                            worst_export_sol_prd_graph_data[11],
                            worst_export_sol_prd_graph_data[12],
                            worst_export_sol_prd_graph_data[13],
                            worst_export_sol_prd_graph_data[14],
                            worst_export_sol_prd_graph_data[15],
                            worst_export_sol_prd_graph_data[16],
                            worst_export_sol_prd_graph_data[17],
                            worst_export_sol_prd_graph_data[18],
                            worst_export_sol_prd_graph_data[19],
                            worst_export_sol_prd_graph_data[20],
                            worst_export_sol_prd_graph_data[21],
                            worst_export_sol_prd_graph_data[22],
                            worst_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            worst_export_export_graph_data[0],
                            worst_export_export_graph_data[1],
                            worst_export_export_graph_data[2],
                            worst_export_export_graph_data[3],
                            worst_export_export_graph_data[4],
                            worst_export_export_graph_data[5],
                            worst_export_export_graph_data[6],
                            worst_export_export_graph_data[7],
                            worst_export_export_graph_data[8],
                            worst_export_export_graph_data[9],
                            worst_export_export_graph_data[10],
                            worst_export_export_graph_data[11],
                            worst_export_export_graph_data[12],
                            worst_export_export_graph_data[13],
                            worst_export_export_graph_data[14],
                            worst_export_export_graph_data[15],
                            worst_export_export_graph_data[16],
                            worst_export_export_graph_data[17],
                            worst_export_export_graph_data[18],
                            worst_export_export_graph_data[19],
                            worst_export_export_graph_data[20],
                            worst_export_export_graph_data[21],
                            worst_export_export_graph_data[22],
                            worst_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            worst_export_offset_graph_data[0],
                            worst_export_offset_graph_data[1],
                            worst_export_offset_graph_data[2],
                            worst_export_offset_graph_data[3],
                            worst_export_offset_graph_data[4],
                            worst_export_offset_graph_data[5],
                            worst_export_offset_graph_data[6],
                            worst_export_offset_graph_data[7],
                            worst_export_offset_graph_data[8],
                            worst_export_offset_graph_data[9],
                            worst_export_offset_graph_data[10],
                            worst_export_offset_graph_data[11],
                            worst_export_offset_graph_data[12],
                            worst_export_offset_graph_data[13],
                            worst_export_offset_graph_data[14],
                            worst_export_offset_graph_data[15],
                            worst_export_offset_graph_data[16],
                            worst_export_offset_graph_data[17],
                            worst_export_offset_graph_data[18],
                            worst_export_offset_graph_data[19],
                            worst_export_offset_graph_data[20],
                            worst_export_offset_graph_data[21],
                            worst_export_offset_graph_data[22],
                            worst_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });

        }


        function show_production2(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var high_export_load_graph_data = d.high_export_load_graph_data;
            var high_export_sol_prd_graph_data = d.high_export_sol_prd_graph_data;
            var high_export_export_graph_data = d.high_export_export_graph_data;
            var high_export_offset_graph_data = d.high_export_offset_graph_data;


            Highcharts.chart('high_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            high_export_load_graph_data[0],
                            high_export_load_graph_data[1],
                            high_export_load_graph_data[2],
                            high_export_load_graph_data[3],
                            high_export_load_graph_data[4],
                            high_export_load_graph_data[5],
                            high_export_load_graph_data[6],
                            high_export_load_graph_data[7],
                            high_export_load_graph_data[8],
                            high_export_load_graph_data[9],
                            high_export_load_graph_data[10],
                            high_export_load_graph_data[11],
                            high_export_load_graph_data[12],
                            high_export_load_graph_data[13],
                            high_export_load_graph_data[14],
                            high_export_load_graph_data[15],
                            high_export_load_graph_data[16],
                            high_export_load_graph_data[17],
                            high_export_load_graph_data[18],
                            high_export_load_graph_data[19],
                            high_export_load_graph_data[20],
                            high_export_load_graph_data[21],
                            high_export_load_graph_data[22],
                            high_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            high_export_sol_prd_graph_data[0],
                            high_export_sol_prd_graph_data[1],
                            high_export_sol_prd_graph_data[2],
                            high_export_sol_prd_graph_data[3],
                            high_export_sol_prd_graph_data[4],
                            high_export_sol_prd_graph_data[5],
                            high_export_sol_prd_graph_data[6],
                            high_export_sol_prd_graph_data[7],
                            high_export_sol_prd_graph_data[8],
                            high_export_sol_prd_graph_data[9],
                            high_export_sol_prd_graph_data[10],
                            high_export_sol_prd_graph_data[11],
                            high_export_sol_prd_graph_data[12],
                            high_export_sol_prd_graph_data[13],
                            high_export_sol_prd_graph_data[14],
                            high_export_sol_prd_graph_data[15],
                            high_export_sol_prd_graph_data[16],
                            high_export_sol_prd_graph_data[17],
                            high_export_sol_prd_graph_data[18],
                            high_export_sol_prd_graph_data[19],
                            high_export_sol_prd_graph_data[20],
                            high_export_sol_prd_graph_data[21],
                            high_export_sol_prd_graph_data[22],
                            high_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            high_export_export_graph_data[0],
                            high_export_export_graph_data[1],
                            high_export_export_graph_data[2],
                            high_export_export_graph_data[3],
                            high_export_export_graph_data[4],
                            high_export_export_graph_data[5],
                            high_export_export_graph_data[6],
                            high_export_export_graph_data[7],
                            high_export_export_graph_data[8],
                            high_export_export_graph_data[9],
                            high_export_export_graph_data[10],
                            high_export_export_graph_data[11],
                            high_export_export_graph_data[12],
                            high_export_export_graph_data[13],
                            high_export_export_graph_data[14],
                            high_export_export_graph_data[15],
                            high_export_export_graph_data[16],
                            high_export_export_graph_data[17],
                            high_export_export_graph_data[18],
                            high_export_export_graph_data[19],
                            high_export_export_graph_data[20],
                            high_export_export_graph_data[21],
                            high_export_export_graph_data[22],
                            high_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            high_export_offset_graph_data[0],
                            high_export_offset_graph_data[1],
                            high_export_offset_graph_data[2],
                            high_export_offset_graph_data[3],
                            high_export_offset_graph_data[4],
                            high_export_offset_graph_data[5],
                            high_export_offset_graph_data[6],
                            high_export_offset_graph_data[7],
                            high_export_offset_graph_data[8],
                            high_export_offset_graph_data[9],
                            high_export_offset_graph_data[10],
                            high_export_offset_graph_data[11],
                            high_export_offset_graph_data[12],
                            high_export_offset_graph_data[13],
                            high_export_offset_graph_data[14],
                            high_export_offset_graph_data[15],
                            high_export_offset_graph_data[16],
                            high_export_offset_graph_data[17],
                            high_export_offset_graph_data[18],
                            high_export_offset_graph_data[19],
                            high_export_offset_graph_data[20],
                            high_export_offset_graph_data[21],
                            high_export_offset_graph_data[22],
                            high_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });


        }


        function show_production3(d) {
            var i = 1;
            var labels = {};
            var graph_data = [];

            var best_export_load_graph_data = d.best_export_load_graph_data;
            var best_export_sol_prd_graph_data = d.best_export_sol_prd_graph_data;
            var best_export_export_graph_data = d.best_export_export_graph_data;
            var best_export_offset_graph_data = d.best_export_offset_graph_data;

            Highcharts.chart('best_export_graph', {
                chart: {
                    type: 'area',
                    width: 420,
                    height: 380,
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                title: {
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Hours'
                    },
                    type: 'datetime',
                    //step: 24,
                    tickInterval: 1800 * 1000,

                    //minTickInterval: 24,
                    labels: {
                        formatter: function() {
                            return Highcharts.dateFormat('%H:%M-%H:59', this.value);
                        },
                    },
                },
                yAxis: {
                    allowDecimals: true,
                    title: {
                        text: 'POWER',
                        style: {
                            "fontSize": "16px",
                            "color": '#000',
                            "fontWeight": 'bold'
                        }
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0,
                    itemStyle: {
                        "fontSize": "9px",
                        "color": '#666',
                        "fontWeight": 'normal'
                    }
                },
                tooltip: {
                    formatter: function() {
                        console.log(this);
                        return '<b>' + this.series.name + ': </b>' + Highcharts.dateFormat('%H:%M-%H:59', this.x) + '<br/><b>' + this.y + '</b>';
                    }
                },
                plotOptions: {
                    area: {
                        pointStart: 1940,
                        marker: {
                            enabled: false,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    },
                    series: {
                        pointStart: Date.UTC(2016, 0, 17),
                        pointInterval: 1 * 3600 * 1000
                    }
                },
                series: [{
                        name: 'Load',
                        data: [
                            best_export_load_graph_data[0],
                            best_export_load_graph_data[1],
                            best_export_load_graph_data[2],
                            best_export_load_graph_data[3],
                            best_export_load_graph_data[4],
                            best_export_load_graph_data[5],
                            best_export_load_graph_data[6],
                            best_export_load_graph_data[7],
                            best_export_load_graph_data[8],
                            best_export_load_graph_data[9],
                            best_export_load_graph_data[10],
                            best_export_load_graph_data[11],
                            best_export_load_graph_data[12],
                            best_export_load_graph_data[13],
                            best_export_load_graph_data[14],
                            best_export_load_graph_data[15],
                            best_export_load_graph_data[16],
                            best_export_load_graph_data[17],
                            best_export_load_graph_data[18],
                            best_export_load_graph_data[19],
                            best_export_load_graph_data[20],
                            best_export_load_graph_data[21],
                            best_export_load_graph_data[22],
                            best_export_load_graph_data[23]
                        ],
                        color: '#4B4B4B',
                        type: 'area',
                    }, {
                        name: 'Solar Output',
                        data: [
                            best_export_sol_prd_graph_data[0],
                            best_export_sol_prd_graph_data[1],
                            best_export_sol_prd_graph_data[2],
                            best_export_sol_prd_graph_data[3],
                            best_export_sol_prd_graph_data[4],
                            best_export_sol_prd_graph_data[5],
                            best_export_sol_prd_graph_data[6],
                            best_export_sol_prd_graph_data[7],
                            best_export_sol_prd_graph_data[8],
                            best_export_sol_prd_graph_data[9],
                            best_export_sol_prd_graph_data[10],
                            best_export_sol_prd_graph_data[11],
                            best_export_sol_prd_graph_data[12],
                            best_export_sol_prd_graph_data[13],
                            best_export_sol_prd_graph_data[14],
                            best_export_sol_prd_graph_data[15],
                            best_export_sol_prd_graph_data[16],
                            best_export_sol_prd_graph_data[17],
                            best_export_sol_prd_graph_data[18],
                            best_export_sol_prd_graph_data[19],
                            best_export_sol_prd_graph_data[20],
                            best_export_sol_prd_graph_data[21],
                            best_export_sol_prd_graph_data[22],
                            best_export_sol_prd_graph_data[23]
                        ],
                        color: '#F8FF34',
                        zIndex: 1,
                        fillOpacity: 0,
                        marker: {
                            fillColor: 'yellow',
                            lineWidth: 2,
                            lineColor: 'yellow',
                            enabled: true,
                            symbol: 'circle'
                        }
                    },
                    {
                        name: 'Export',
                        data: [
                            best_export_export_graph_data[0],
                            best_export_export_graph_data[1],
                            best_export_export_graph_data[2],
                            best_export_export_graph_data[3],
                            best_export_export_graph_data[4],
                            best_export_export_graph_data[5],
                            best_export_export_graph_data[6],
                            best_export_export_graph_data[7],
                            best_export_export_graph_data[8],
                            best_export_export_graph_data[9],
                            best_export_export_graph_data[10],
                            best_export_export_graph_data[11],
                            best_export_export_graph_data[12],
                            best_export_export_graph_data[13],
                            best_export_export_graph_data[14],
                            best_export_export_graph_data[15],
                            best_export_export_graph_data[16],
                            best_export_export_graph_data[17],
                            best_export_export_graph_data[18],
                            best_export_export_graph_data[19],
                            best_export_export_graph_data[20],
                            best_export_export_graph_data[21],
                            best_export_export_graph_data[22],
                            best_export_export_graph_data[23]
                        ],
                        color: '#FF4B4C',
                        type: 'area',
                    }, {
                        name: 'Offset Consumption',
                        data: [
                            best_export_offset_graph_data[0],
                            best_export_offset_graph_data[1],
                            best_export_offset_graph_data[2],
                            best_export_offset_graph_data[3],
                            best_export_offset_graph_data[4],
                            best_export_offset_graph_data[5],
                            best_export_offset_graph_data[6],
                            best_export_offset_graph_data[7],
                            best_export_offset_graph_data[8],
                            best_export_offset_graph_data[9],
                            best_export_offset_graph_data[10],
                            best_export_offset_graph_data[11],
                            best_export_offset_graph_data[12],
                            best_export_offset_graph_data[13],
                            best_export_offset_graph_data[14],
                            best_export_offset_graph_data[15],
                            best_export_offset_graph_data[16],
                            best_export_offset_graph_data[17],
                            best_export_offset_graph_data[18],
                            best_export_offset_graph_data[19],
                            best_export_offset_graph_data[20],
                            best_export_offset_graph_data[21],
                            best_export_offset_graph_data[22],
                            best_export_offset_graph_data[23]
                        ],
                        color: '#81B94C',
                        type: 'area',
                    }
                ]


            });

        }
    </script>
</body>

</html>

</html>
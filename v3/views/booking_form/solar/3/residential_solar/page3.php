<table class="pdf_page" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/header_01.png'; ?>" width="910" height="126" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <?php if($cost_centre_id == 8){?>
        <tr>
        <td valign="top">
            <table width="828" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse;">
                <tr>
                    <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Rebate Eligible</td>
                    <td valign="top">
                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td width="415" valign="top" style="padding-right:10px">
                                        <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                                <tr>
                                                    <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_rebate" type="radio" name="business_details[vic_rebate]" id="vic_rebate_1" value="1">
                                                            <label class="form-check-label" for="vic_rebate_1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_rebate" type="radio" name="business_details[vic_rebate]" id="vic_rebate_0" value="0">
                                                            <label class="form-check-label" for="vic_rebate_0">No</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Loan Eligible</td>
                    <td valign="top">
                        <table width="400" border="0" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td width="415" valign="top" style="padding-right:10px">
                                        <table width="405" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                                <tr>
                                                    <td width="283" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px">
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_loan" type="radio" name="business_details[vic_loan]" id="vic_loan_1" value="1">
                                                            <label class="form-check-label" for="vic_loan_1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input style="width:25px; height:25px;" class="form-check-input vic_loan" type="radio" name="business_details[vic_loan]" id="vic_loan_0" value="0">
                                                            <label class="form-check-label" for="vic_loan_0">No</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="expiry_date_tr hidden">
                    <td width="200" style="font-family:Arial, Helvetica, sans-serif; font-size:14px">Solar VIC Expiry Date</td>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                        <input  class="expiry_date" id="expiry_date" name="business_details[expiry_date]" style="padding:5px; width:200px; height: inherit;" type="text" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td valign="top">
            <table id="booking_items" width="828" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
                <tr>
                    <th width="154" height="40" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px">Product Type</th>
                    <th width="262" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Description</th>
                    <th width="106" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Units</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Unit Cost exc GST</th>
                    <th width="147" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:14px">Total Cost exc GST</th>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_0" name="product[product_type][]" value="System Size" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_0" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" required="" id="system_size"/>
                        <input class="product_id product_id_0" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_0" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_0" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_0" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_1" name="product[product_type][]" value="Solar Panels" style="padding:5px; width:100%; height: inherit;" type="text" required="" />
                        <input  class="product_capacity product_capacity_1" name="product[product_capacity][1]" value="" type="hidden" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_1" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;" id="prd_panel" required="">
                            <option value="">Select Panel</option>
                            <?php if($panel){ foreach($panel as $panel){ ?>
                                <option value="<?= $panel['name'] ?>" data-item='<?= json_encode($panel,true) ?>' data-id="<?= $panel['id']?>"><?= $panel['name'] ?></option>
                            <?php } } ?>
                        </select>
                        <input class="product_id product_id_1" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_1" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_1" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_1" style="padding:5px; width:100%; height: inherit;" min="1" type="number" required="" />
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_2" name="product[product_type][]" value="Inverters" style="padding:5px; width:100%; height: inherit;" type="text" />
                        <input  class="product_capacity product_capacity_2" name="product[product_capacity][2]" value="" type="hidden" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_2" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;" id="prd_inverter">
                            <option value="">Select Inverter</option>
                            <?php if($inverter){ foreach($inverter as $inverter){ ?>
                                <option value="<?= $inverter['name'] ?>" data-item='<?= json_encode($inverter)?>' data-id="<?= $panel['id']?>"><?= $inverter['name'] ?></option>
                            <?php } } ?>
                        </select>
                        <input class="product_id product_id_2" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_2" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_2" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_2" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_3" name="product[product_type][]" value="Battery" style="padding:5px; width:100%; height: inherit;" type="text" />
                        <input  class="product_capacity product_capacity_3" name="product[product_capacity][3]" value="" type="hidden" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_3" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;" id="prd_battery">
                            <option value="">Select Battery</option>
                            <?php if($battery){ foreach($battery as $battery){ ?>
                                <option value="<?= $battery['name'] ?>" data-item='<?= json_encode($battery)?>' data-id="<?= $panel['id']?>"><?= $battery['name'] ?></option>
                            <?php } } ?>
                        </select>
                        <input class="product_id product_id_3" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_3" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_3" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_3" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_4" name="product[product_type][]" value="PV Mounting Kit" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_4" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;" id="pv_mounting" required="">
                            <option value="">Select PV Mounting system</option>
                            <option value="Flat">Flat</option>
                            <option value="Tin/Tile">Tin/Tile</option>
                            <option value="Tilt">Tilt</option>
                            <option value="Not Required">Not Required</option>
                        </select>
                        <input class="product_id product_id_4" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_4" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_4" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_4" style="padding:5px; width:100%; height: inherit;" min="1" type="number"/>
                    </td>
                </tr>

                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_5" name="product[product_type][]" value="Roof Type" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_5" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;" id="roof_type" required="">
                            <option value="">Select Roof Type</option>
                            <option value="Single/Tile">Single/Tile</option>
                            <option value="Double/Tile">Double/Tile</option>
                            <option value="Single/Tin">Single/Tin</option>
                            <option value="Double/Tin">Double/Tin</option>
                            <option value="Single/Clip Lock">Single/Clip Lock</option>
                            <option value="Double/Clip Lock">Double/Clip Lock</option>
                            <option value="Not Required">Not Required</option>
                        </select>
                        <input class="product_id product_id_5" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_5" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_5" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_5" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_6" name="product[product_type][]" value="Switchboard" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_6" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;">
                            <option value="">Select Switchboard</option>
                            <option value="Complete Switchboard Upgrade">Complete Switchboard Upgrade</option>
                            <option value="Modular Switchboard">Modular Switchboard</option>
                            <option value="Switchboard Rewire">Switchboard Rewire</option>
                        </select>
                        <input class="product_id product_id_6" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_6" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_6" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_6" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_7" name="product[product_type][]" value="System Removal" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <select class="product_name product_name_7" name="product[product_name][]" style="padding:5px; width:100%; height: inherit;">
                            <option value="">Select Amount of Panels</option>
                            <option value="Amount of Panels">Amount of Panels</option>
                        </select>
                        <input class="product_id product_id_7" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_7" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number"  />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_7" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_7" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input  class="product_type product_type_8" name="product[product_type][]" value="Extra:" style="padding:5px; width:100%; height: inherit;" type="text" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_name product_name_8" name="product[product_name][]" value="" style="padding:5px; width:100%; height: inherit;" type="text" />
                        <input class="product_id product_id_8" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_qty product_qty_8" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_cost_excGST product_cost_excGST_8" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                    <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input class="product_total_cost_excGst product_total_cost_excGst_8" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                    </td>
                </tr>

				
				
                <?php for ($i = 9; $i < 12; $i++) { ?>
                    <tr>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input  class="product_type product_type_<?php echo $i; ?>" name="product[product_type][]" style="padding:5px; width:100%; height: inherit;" type="text" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_name product_name_<?php echo $i; ?>" name="product[product_name][]"  style="padding:5px; width:100%; height: inherit;" type="text" />
                            <input class="product_id product_id_<?php echo $i; ?>" name="product[product_id][]"  style="padding:5px; width:100%; height: inherit;" type="hidden"/>
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_qty product_qty_<?php echo $i; ?>" name="product[product_qty][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_cost_excGST product_cost_excGST_<?php echo $i; ?>" name="product[product_cost_excGST][]" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                        <td style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                            <input class="product_total_cost_excGst product_total_cost_excGst_<?php echo $i; ?>" style="padding:5px; width:100%; height: inherit;" min="1" type="number" />
                        </td>
                    </tr>   
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="828" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="450" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td width="212" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>System Total (inc GST):</strong></td>
                    <td width="148" style="border:solid 1px #e8e8e8;  border-top:none; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_incGst" name="booking_form[total_incGst]" style="padding:5px; width:100%; height: inherit;" type="number" />
                    </td>
                </tr>
                <tr>
                    <td width="450" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>STC Deduction</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="stc_deduction" name="booking_form[stc_deduction]" style="padding:5px; width:100%; height: inherit;" type="number" />
                    </td>
                </tr>
                <tr class="hidden">
                    <td width="450" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Solar VIC Rebate:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="vic_rebate" name="booking_form[vic_rebate]" style="padding:5px; width:100%; height: inherit;" type="number" />
                    </td>

                </tr>
                <tr class="hidden">
                    <td width="450" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Solar VIC Loan:</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="vic_loan" name="booking_form[vic_loan]" style="padding:5px; width:100%; height: inherit;" type="number" />
                    </td>
                    
                </tr>
                <tr>
                    <td width="450" height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px">&nbsp;</td>
                    <td height="40" align="right" style="padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Total Payable(inc GST):</strong></td>
                    <td style="border:solid 1px #e8e8e8; padding:5px; font-family:Arial, Helvetica, sans-serif; font-size:13px">
                        <input required="" id="total_payable" name="booking_form[total_payable]" style="padding:5px; width:100%; height: inherit;" type="number" />
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="830" border="0" align="center" cellpadding="0" cellspacing="0" class="array_details">
                <tr>
                    <td height="37" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><strong>Access details</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="415" valign="top" style="padding-right:10px">
                                    <table width="405" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Roof Height:</td>
                                            <td width="240" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="roof_height" name="booking_form[roof_height]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="830" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top">
                                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="165" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Access Equipments:</td>
                                            <td width="665" valign="top" style=" height:40px; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:5px"><input required="" id="access_equipments" name="booking_form[access_equipments]" style="padding:5px; width:inherit; height: inherit;" type="text" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>

            </table></td>
    </tr>
    <tr>
        <td><img src="<?php echo $this->config->item('live_url') . 'assets/solar_booking_form/bottom.jpg'; ?>" width="910" height="89" /></td>
    </tr>
</table>

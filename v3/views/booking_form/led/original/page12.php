<div class="pdf_page">
<table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/header_02.jpg'); ?>" alt="" width="910" height="124" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><table width="830" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="35" style="color:#000000; font-size:15px; font-family:Arial, Helvetica, sans-serif"><strong>Additional Space Type Photographs</strong></td>
      </tr>
      <tr>
        <td><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
          <tr>
            <td width="412" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Photo 1</strong></td>
            <td width="412" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px"><strong>Photo 2</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top" style="padding:13px 0"><img src="03.jpg" width="391" height="294" /></td>
            <td align="center" style="padding:13px 0"><img src="04.jpg" width="342" height="255" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="830" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#e8e8e8" style="border-collapse:collapse; border:solid 1px #e8e8e8">
          <tr>
            <td width="412" height="35" align="center" class="black-bg" style="background:#010101;padding:5px; color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:15px"><strong>Supporting BCA Evidence</strong></td>
            <td width="412" align="center" class="black-bg" style="background:#010101; color:#ffffff; font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:15px"><strong>Supporting BCA Evidence</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top" style="padding:13px 0"><img src="supporting_bca.jpg" width="200" height="264" /></td>
            <td align="center" style="padding:13px 0">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="<?php echo site_url('assets/led_booking_form/bottom.jpg'); ?>" width="910" height="89" /></td>
  </tr>
</table>
</div>